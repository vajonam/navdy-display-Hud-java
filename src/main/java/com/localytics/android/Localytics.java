package com.localytics.android;

import java.util.Map;

import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit
public class Localytics {

    @DexIgnore
    Localytics(){}

    @DexReplace
    public static void tagEvent(String var0, Map<String, String> var1) {
//        tagEvent(var0, var1, 0L);
    }

    @DexReplace
    public static void tagEvent(String var0) {
//        tagEvent(var0, (Map)null, 0L);
    }

    @DexReplace
    public static void tagEvent(String var0, Map<String, String> var1, long var2) {
//        LocalyticsManager.getInstance().tagEvent(var0, var1, var2);
    }
}
