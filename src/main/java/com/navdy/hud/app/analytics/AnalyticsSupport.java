package com.navdy.hud.app.analytics;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Handler;
import android.os.SystemClock;

import com.localytics.android.LocalyticsActivityLifecycleCallbacks;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.device.PowerManager;
import com.navdy.hud.app.framework.DriverProfileHelper;
import com.navdy.hud.app.framework.network.NetworkBandwidthController;
import com.navdy.hud.app.framework.notifications.NotificationType;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.obd.ObdManager;
import com.navdy.hud.app.profile.DriverProfile;
import com.navdy.hud.app.screen.BaseScreen;
import com.navdy.hud.app.service.HudConnectionService;
import com.navdy.hud.app.ui.framework.INotificationAnimationListener;
import com.navdy.hud.app.ui.framework.UIStateManager;
import com.navdy.service.library.events.DeviceInfo;
import com.navdy.service.library.events.ui.Screen;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;

import java.util.Map;
import java.util.concurrent.TimeUnit;

import lanchon.dexpatcher.annotation.DexAdd;
import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexReplace;
import lanchon.dexpatcher.annotation.DexAction;
import mortar.Mortar;

@DexEdit(defaultAction = DexAction.ADD)
public class AnalyticsSupport {
    @DexIgnore
    AnalyticsSupport() {}

    @DexIgnore
    private static String ANALYTICS_EVENT_ATTEMPT_TO_CONNECT;
    @DexIgnore
    private static String ANALYTICS_EVENT_MOBILE_APP_CONNECT;
    @DexIgnore
    private static String ATTR_CONN_ATTEMPT_NEW_DEVICE;
    @DexIgnore
    private static String ATTR_CONN_ATTEMPT_TIME;
    @DexIgnore
    private static String ATTR_CONN_DEVICE_ID;
    @DexIgnore
    private static String ATTR_CONN_DEVICE_INFO_DELAY;
    @DexIgnore
    private static String ATTR_CONN_LINK_DELAY;
    @DexIgnore
    private static String ATTR_CONN_REMOTE_CLIENT_VERSION;
    @DexIgnore
    private static String ATTR_CONN_REMOTE_DEVICE_ID;
    @DexIgnore
    private static String ATTR_CONN_REMOTE_DEVICE_NAME;
    @DexIgnore
    private static String ATTR_CONN_REMOTE_PLATFORM;
    @DexIgnore
    private static String ATTR_REMOTE_DEVICE_HARDWARE;
    @DexIgnore
    private static String ATTR_REMOTE_DEVICE_PLATFORM;
    @DexIgnore
    private static String ATTR_REMOTE_DEVICE_SW_VERSION;
    @DexIgnore
    private static String TRUE;
    @DexIgnore
    public static Handler handler;
    @DexIgnore
    private static volatile boolean localyticsInitialized;
    @DexIgnore
    private static boolean newDevicePaired;
    @DexIgnore
    private static boolean phonePaired;
    @DexIgnore
    public static boolean quietMode;
    @DexIgnore
    private static DeviceInfo remoteDevice;
    @DexIgnore
    private static AnalyticsSupport.FuelMonitor sFuelMonitor;
    @DexIgnore
    public static Logger sLogger;
    @DexIgnore
    private static AnalyticsSupport.NotificationReceiver sNotificationReceiver;
    @DexIgnore
    public static ObdManager sObdManager;
    @DexIgnore
    public static String sRemoteDeviceId;
    @DexIgnore
    private static RemoteDeviceManager sRemoteDeviceManager;
    @DexIgnore
    private static long startTime;
    @DexIgnore
    private static long welcomeScreenTime;

    @DexIgnore
    public static java.lang.String ANALYTICS_EVENT_GLANCE_ADVANCE; // = "Glance_Advance";
    @DexIgnore
    public static java.lang.String ANALYTICS_EVENT_GLANCE_DELETE_ALL; // = "Glance_Delete_All";
    @DexIgnore
    public static java.lang.String ANALYTICS_EVENT_GLANCE_DISMISS; // = "Glance_Dismiss";
    @DexIgnore
    public static java.lang.String ANALYTICS_EVENT_GLANCE_OPEN_FULL; // = "Glance_Open_Full";
    @DexIgnore
    public static java.lang.String ANALYTICS_EVENT_GLANCE_OPEN_MINI; // = "Glance_Open_Mini";
    @DexIgnore
    public static java.lang.String ANALYTICS_EVENT_GPS_ACCURACY_STATISTICS; // = "GPS_Accuracy";
    @DexIgnore
    public static java.lang.String ANALYTICS_EVENT_GPS_ACQUIRE_LOCATION; // = "GPS_Acquired_Location";
    @DexIgnore
    public static java.lang.String ANALYTICS_EVENT_GPS_ATTEMPT_ACQUIRE_LOCATION; // = "GPS_Attempt_Acquire_Location";
    @DexIgnore
    public static java.lang.String ANALYTICS_EVENT_GPS_CALIBRATION_IMU_DONE; // = "GPS_Calibration_IMU_Done";
    @DexIgnore
    public static java.lang.String ANALYTICS_EVENT_GPS_CALIBRATION_LOST; // = "GPS_Calibration_Lost";
    @DexIgnore
    public static java.lang.String ANALYTICS_EVENT_GPS_CALIBRATION_SENSOR_DONE; // = "GPS_Calibration_Sensor_Done";
    @DexIgnore
    public static java.lang.String ANALYTICS_EVENT_GPS_CALIBRATION_START; // = "GPS_Calibration_Start";
    @DexIgnore
    public static java.lang.String ANALYTICS_EVENT_GPS_CALIBRATION_VIN_SWITCH; // = "GPS_Calibration_VinSwitch";
    @DexIgnore
    public static java.lang.String ANALYTICS_EVENT_GPS_LOST_LOCATION; // = "GPS_Lost_Signal";
    @DexIgnore
    public static java.lang.String ANALYTICS_EVENT_IAP_FAILURE; // = "Mobile_iAP_Failure";
    @DexIgnore
    public static java.lang.String ANALYTICS_EVENT_IAP_RETRY_SUCCESS; // = "Mobile_iAP_Retry_Success";
    @DexIgnore
    public static java.lang.String ANALYTICS_EVENT_NEW_DEVICE; // = "Analytics_New_Device";
    @DexIgnore
    public static java.lang.String ANALYTICS_INTENT; // = "com.navdy.hud.app.analytics.AnalyticsEvent";
    @DexIgnore
    public static java.lang.String ANALYTICS_INTENT_EXTRA_ARGUMENTS; // = "arguments";
    @DexIgnore
    public static java.lang.String ANALYTICS_INTENT_EXTRA_INCLUDE_REMOTE_DEVICE_INFO; // = "include_device_info";
    @DexIgnore
    public static java.lang.String ANALYTICS_INTENT_EXTRA_TAG_NAME; // = "tag";
    @DexIgnore
    public static java.lang.String ATTR_PLATFORM_ANDROID; // = "Android";
    @DexIgnore
    public static java.lang.String ATTR_PLATFORM_IOS; // = "iOS";

    // renamed from: com.navdy.hud.app.analytics.AnalyticsSupport$1 reason: invalid class name //
    @DexReplace()
    static final class Anon1 implements Runnable {
        @DexIgnore
        Anon1() {
        }

        @DexReplace
        public void run() {
            if (NetworkBandwidthController.getInstance().isNetworkAccessAllowed(NetworkBandwidthController.Component.LOCALYTICS)) {
                AnalyticsSupport.sLogger.v("called Localytics.upload !! DISABLED");
                //Localytics.upload();
            }
            AnalyticsSupport.handler.postDelayed(this, AnalyticsSupport.getUpdateInterval());
        }
    }

    @DexIgnore
    private static long getUpdateInterval() {
        throw null;
    }

    @DexEdit
    private static class EventSender implements Runnable {
        @DexIgnore
        private Event event;

        @DexIgnore
        EventSender(Event event2) {
            this.event = event2;
        }

        @DexReplace
        public void run() {
            //Localytics.tagEvent(this.event.tag, this.event.argMap);
        }
    }

    @DexReplace
    public static void analyticsApplicationInit(Application application) {
        //application.registerActivityLifecycleCallbacks(new LocalyticsActivityLifecycleCallbacks(application));
        //Localytics.setPushDisabled(true);
        setupCustomDimensions();
        //sNotificationReceiver = new AnalyticsSupport.NotificationReceiver();
        startTime = TimeUnit.NANOSECONDS.toSeconds(System.nanoTime());
        //AnalyticsSupport.TemperatureReporter temperatureReporter = new AnalyticsSupport.TemperatureReporter();
        //temperatureReporter.setName("TemperatureReporter");
        //temperatureReporter.start();
        //Mortar.inject(HudApplication.getAppContext(), temperatureReporter);
        sObdManager = ObdManager.getInstance();
        //sFuelMonitor = new AnalyticsSupport.FuelMonitor(null);
        //quietMode = sNotificationReceiver.powerManager.inQuietMode();
        //maybeOpenSession();
        //IntentFilter intentFilter = new IntentFilter("android.bluetooth.device.action.ACL_DISCONNECTED");
        //intentFilter.addAction(HudConnectionService.ACTION_DEVICE_FORCE_RECONNECT);
        //HudApplication.getAppContext().registerReceiver(new AnalyticsSupport.AnonymousClass2(), intentFilter);
    }

    @DexEdit(target = "AnalyticsSupport$Anon2")
    static final class Anon2 extends BroadcastReceiver {
        @DexIgnore
        Anon2 () {}
        @DexIgnore
        public void onReceive(Context context, Intent intent) {
        }
    }

    @DexIgnore
    private static void maybeOpenSession() {
    }

    @DexIgnore
    private static class FuelMonitor implements Runnable {
        @DexIgnore
        FuelMonitor(Object unused) {}

        @DexIgnore
        public void run() {

        }
    }

    @DexIgnore
    public static class TemperatureReporter extends Thread {
    }

    @DexIgnore
    public static class NotificationReceiver implements INotificationAnimationListener {
        @DexIgnore
        public PowerManager powerManager;
        @DexIgnore
        public void onStart(String s, NotificationType notificationType, UIStateManager.Mode mode) {
        }
        @DexIgnore
        public void onStop(String s, NotificationType notificationType, UIStateManager.Mode mode) {
        }
    }

    @DexReplace
    private static void closeSession() {
        synchronized (AnalyticsSupport.class) {
            if (localyticsInitialized) {
                sendDistance();
                sLogger.v("closeSession()");
                //Localytics.closeSession();
                sRemoteDeviceId = "unknown";
            }
        }
    }

    @DexIgnore
    private static void sendDistance() {
    }

    @DexReplace
    private static void localyticsSendEvent(Event event, boolean z) {
        if (z) {
            //Localytics.tagEvent(event.tag, event.argMap);
        } else {
            TaskManager.getInstance().execute(new AnalyticsSupport.EventSender(event), 1);
        }
    }

    @DexReplace
    public static void localyticsSendEvent(String str, String... strArr) {
    }

    @DexReplace
    private static void localyticsSendEvent(String str, boolean z, boolean z2, String... strArr) {
        if (z2 || !deferLocalyticsEvent(str, strArr)) {
            Event event = new Event(str, strArr);
            Map<String, String> map = event.argMap;
            if (z) {
                DeviceInfo remoteDeviceInfo = RemoteDeviceManager.getInstance().getRemoteDeviceInfo();
                if (remoteDeviceInfo != null) {
                    if (remoteDeviceInfo.platform != null) {
                        map.put(ATTR_REMOTE_DEVICE_PLATFORM, remoteDeviceInfo.platform.name());
                    }
                    String sb = remoteDeviceInfo.deviceMake +
                            " " +
                            remoteDeviceInfo.model;
                    map.put(ATTR_REMOTE_DEVICE_HARDWARE, sb);
                    String sb2 = remoteDeviceInfo.deviceMake +
                            " " +
                            remoteDeviceInfo.systemVersion;
                    map.put(ATTR_REMOTE_DEVICE_SW_VERSION, sb2);
                }
            }
            //localyticsSendEvent(event, z2);
        }
    }

    @DexIgnore
    private static boolean deferLocalyticsEvent(String str, String[] strArr) {
        throw null;
    }

    @DexReplace
    private static void openSession() {
        synchronized (AnalyticsSupport.class) {
            if (!localyticsInitialized) {
                sLogger.i("openSession() !! DISABLED");
                //Localytics.openSession();
                //localyticsInitialized = true;
                //sendDeferredLocalyticsEvents();
                //handler.postDelayed(sFuelMonitor, FUEL_REPORTING_INTERVAL);
                //handler.postDelayed(sUploader, UPLOAD_INTERVAL_BOOT);
            }
        }
    }

    @DexReplace
    public static void recordDeviceConnect(DeviceInfo deviceInfo, long j, long j2) {
        String str;
        String str2;
        DeviceInfo deviceInfo2 = deviceInfo;
        remoteDevice = deviceInfo2;
        long elapsedRealtime = SystemClock.elapsedRealtime();
        DriverProfile currentProfile = DriverProfileHelper.getInstance().getCurrentProfile();
        String str3 = null;
        if (!currentProfile.isDefaultProfile()) {
            //Localytics.setCustomerFullName(currentProfile.getDriverName());
            //Localytics.setCustomerEmail(currentProfile.getDriverEmail());
            str3 = currentProfile.getCarMake();
            str = currentProfile.getCarModel();
            str2 = currentProfile.getCarYear();
            //Localytics.setProfileAttribute(ATTR_PROFILE_SERIAL_NUMBER, Build.SERIAL);
            //Localytics.setProfileAttribute("Car_Make", str3);
            //Localytics.setProfileAttribute("Car_Model", str);
            //Localytics.setProfileAttribute("Car_Year", str2);
            //Localytics.setProfileAttribute(ATTR_PROFILE_HUD_VERSION, SystemProperties.get("ro.build.description", ""));
            //Localytics.setProfileAttribute(ATTR_PROFILE_BUILD_TYPE, Build.TYPE);
        } else {
            str2 = null;
            str = null;
        }
        String vin = sObdManager.getVin();
        Event event = new Event(ANALYTICS_EVENT_MOBILE_APP_CONNECT, ATTR_CONN_DEVICE_ID, Build.SERIAL, "Car_Make", str3, "Car_Model", str, "Car_Year", str2, "VIN", vin, ATTR_CONN_LINK_DELAY, Long.toString(j - j2), ATTR_CONN_DEVICE_INFO_DELAY, Long.toString(elapsedRealtime - j2));
        if (deviceInfo2 != null) {
            sRemoteDeviceId = deviceInfo2.deviceId;
            StringBuilder sb = new StringBuilder();
            sb.append(deviceInfo2.deviceMake);
            sb.append(" ");
            sb.append(deviceInfo2.model);
            StringBuilder sb2 = new StringBuilder();
            sb2.append(deviceInfo2.deviceMake);
            sb2.append(" ");
            sb2.append(deviceInfo2.systemVersion);
            event.add(ATTR_CONN_REMOTE_DEVICE_ID, sRemoteDeviceId, ATTR_CONN_REMOTE_DEVICE_NAME, deviceInfo2.deviceName, ATTR_CONN_REMOTE_CLIENT_VERSION, deviceInfo2.clientVersion, ATTR_CONN_REMOTE_PLATFORM, deviceInfo2.platform.toString(), ATTR_REMOTE_DEVICE_HARDWARE, sb.toString(), ATTR_REMOTE_DEVICE_SW_VERSION, sb2.toString());
        } else {
            sRemoteDeviceId = "unknown";
        }
        Logger logger = sLogger;
        StringBuilder sb3 = new StringBuilder();
        sb3.append("recordDeviceConnect: ");
        sb3.append(event);
        logger.i(sb3.toString());
        localyticsSendEvent(event, false);
    }

    @DexReplace
    public static void recordScreen(BaseScreen baseScreen) {
        if (baseScreen.getScreen() == Screen.SCREEN_WELCOME) {
            welcomeScreenTime = SystemClock.elapsedRealtime();
        } else if (baseScreen.getScreen() == Screen.SCREEN_HOME && !phonePaired && welcomeScreenTime != -1 && sRemoteDeviceManager.isRemoteDeviceConnected()) {
            long elapsedRealtime = SystemClock.elapsedRealtime();
            //localyticsSendEvent(ANALYTICS_EVENT_ATTEMPT_TO_CONNECT, ATTR_CONN_ATTEMPT_NEW_DEVICE, newDevicePaired ? TRUE : "false", ATTR_CONN_ATTEMPT_TIME, Long.toString(elapsedRealtime - welcomeScreenTime));
            welcomeScreenTime = -1;
            newDevicePaired = false;
            phonePaired = true;
        }
        if (localyticsInitialized) {
            //Localytics.tagScreen(screenName(baseScreen));
        }
    }

    @DexReplace
    private static void setupCustomDimensions() {
        //Localytics.setCustomDimension(0, getHwVersion());
    }

}
