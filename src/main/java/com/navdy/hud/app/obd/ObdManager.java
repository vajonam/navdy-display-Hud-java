package com.navdy.hud.app.obd;

import android.content.SharedPreferences;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.RemoteException;
import android.text.TextUtils;

import com.here.android.mpa.common.GeoCoordinate;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.analytics.TelemetryDataManager;
import com.navdy.hud.app.config.SettingsManager;
import com.navdy.hud.app.debug.DriveRecorder;
import com.navdy.hud.app.device.PowerManager;
import com.navdy.hud.app.event.DriverProfileChanged;
import com.navdy.hud.app.event.DriverProfileUpdated;
import com.navdy.hud.app.event.DrivingStateChange;
import com.navdy.hud.app.event.RemoteEvent;
import com.navdy.hud.app.event.Shutdown;
import com.navdy.hud.app.event.Wakeup;
import com.navdy.hud.app.framework.trips.TripManager;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.manager.SpeedManager;
import com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder;
import com.navdy.hud.app.maps.here.HereMapsManager;
import com.navdy.hud.app.profile.DriverProfile;
import com.navdy.hud.app.profile.DriverProfileManager;
import com.navdy.obd.ECU;
import com.navdy.obd.ICanBusMonitoringListener;
import com.navdy.obd.ICarService;
import com.navdy.obd.IPidListener;
import com.navdy.obd.Pid;
import com.navdy.obd.PidSet;
import com.navdy.obd.Pids;
import com.navdy.obd.ScanSchedule;
import com.navdy.service.library.events.RequestStatus;
import com.navdy.service.library.events.obd.ObdStatusRequest;
import com.navdy.service.library.events.obd.ObdStatusResponse;
import com.navdy.service.library.events.preferences.DriverProfilePreferences;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.navdy.service.library.util.IOUtils;
import com.squareup.otto.Bus;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;

import lanchon.dexpatcher.annotation.DexAction;
import lanchon.dexpatcher.annotation.DexAdd;
import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import mortar.Mortar;

import static com.navdy.obd.VoltageSettings.DEFAULT_LOW_BATTERY_VOLTAGE;

@DexEdit(defaultAction = DexAction.ADD, staticConstructorAction = DexAction.APPEND)
public final class ObdManager {
    @DexIgnore
    public static /* final */ float BATTERY_CHARGING_VOLTAGE; // = 13.1f;
    @DexIgnore
    public static /* final */ double BATTERY_NO_OBD; // = -1.0d;
    @DexIgnore
    private static /* final */ String BLACKLIST_MODIFICATION_TIME; // = "blacklist_modification_time";
    @DexEdit
    public static double CRITICAL_BATTERY_VOLTAGE; // = ((double) com.navdy.hud.app.util.os.SystemProperties.getFloat(CRITICAL_VOLTAGE_OVERRIDE_PROPERTY, DEFAULT_CRITICAL_BATTERY_VOLTAGE));
    @DexIgnore
    private static /* final */ String CRITICAL_VOLTAGE_OVERRIDE_PROPERTY; // = "persist.sys.voltage.crit";
    @DexIgnore
    private static /* final */ float DEFAULT_CRITICAL_BATTERY_VOLTAGE; // = 12.0f;
    @DexIgnore
    private static /* final */ long DEFAULT_LOW_VOLTAGE_SHUTOFF_DURATION; // = java.util.concurrent.TimeUnit.MINUTES.toMillis(15);
    @DexIgnore
    private static /* final */ String DEFAULT_SET; // = "default_set";
    @DexIgnore
    private static /* final */ int DISTANCE_UPDATE_INTERVAL; // = 60000;
    @DexIgnore
    private static /* final */ int ENGINE_TEMP_UPDATE_INTERVAL; // = 2000;
    @DexIgnore
    public static /* final */ String FIRMWARE_VERSION; // = "4.2.1";
    @DexIgnore
    public static /* final */ String FIRST_ODOMETER_READING_KM; // = "first_odometer_reading";
    @DexIgnore
    private static /* final */ int FLUSH_INTERVAL_MS; // = 30000;
    @DexIgnore
    private static /* final */ int FUEL_UPDATE_INTERVAL; // = 15000;
    @DexIgnore
    private static /* final */ int GENERIC_UPDATE_INTERVAL; // = 1000;
    @DexIgnore
    public static /* final */ String LAST_ODOMETER_READING_KM; // = "last_odometer_reading";
    @DexIgnore
    private static /* final */ int LOW_BATTERY_COUNT_THRESHOLD; // = ((int) java.lang.Math.ceil(((double) LOW_VOLTAGE_SHUTOFF_DURATION) / 2000.0d));
    @DexEdit
    public static double LOW_BATTERY_VOLTAGE; // = ((double) com.navdy.hud.app.util.os.SystemProperties.getFloat(LOW_VOLTAGE_OVERRIDE_PROPERTY, 12.2f));
    @DexIgnore
    public static /* final */ int LOW_FUEL_LEVEL; // = 10;
    @DexIgnore
    private static /* final */ String LOW_VOLTAGE_DURATION_OVERRIDE_PROPERTY; // = "persist.sys.voltage.duration";
    @DexIgnore
    private static /* final */ String LOW_VOLTAGE_OVERRIDE_PROPERTY; // = "persist.sys.voltage.low";
    @DexEdit
    private static long LOW_VOLTAGE_SHUTOFF_DURATION; // = com.navdy.hud.app.util.os.SystemProperties.getLong(LOW_VOLTAGE_DURATION_OVERRIDE_PROPERTY, DEFAULT_LOW_VOLTAGE_SHUTOFF_DURATION);
    @DexIgnore
    public static /* final */ long MINIMUM_TIME_BETWEEN_MILEAGE_REPORTING; // = java.util.concurrent.TimeUnit.MINUTES.toMillis(10);
    @DexIgnore
    private static /* final */ int MPG_UPDATE_INTERVAL; // = 1000;
    @DexIgnore
    public static /* final */ int NOT_AVAILABLE; // = -1;
    @DexIgnore
    private static /* final */ ObdManager.ObdConnectionStatusEvent OBD_CONNECTED; // = new com.navdy.hud.app.obd.ObdManager.ObdConnectionStatusEvent(true);
    @DexIgnore
    private static /* final */ ObdManager.ObdConnectionStatusEvent OBD_NOT_CONNECTED; // = new com.navdy.hud.app.obd.ObdManager.ObdConnectionStatusEvent(false);
    @DexIgnore
    private static /* final */ int RPM_UPDATE_INTERVAL; // = 250;
    @DexIgnore
    public static /* final */ int SAFE_TO_SLEEP_BATTERY_OBSERVED_COUNT_THRESHOLD; // = 5;
    @DexIgnore
    private static /* final */ String SCAN_SETTING; // = "scan_setting";
    @DexIgnore
    private static /* final */ int SLOW_RPM_UPDATE_INTERVAL; // = 2500;
    @DexIgnore
    private static /* final */ int SPEED_UPDATE_INTERVAL; // = 100;
    @DexIgnore
    private static /* final */ int STATE_CONNECTED; // = 2;
    @DexIgnore
    private static /* final */ int STATE_CONNECTING; // = 1;
    @DexIgnore
    private static /* final */ int STATE_DISCONNECTED; // = 0;
    @DexIgnore
    private static /* final */ int STATE_IDLE; // = 4;
    @DexIgnore
    private static /* final */ int STATE_SLEEPING; // = 5;
    @DexIgnore
    public static /* final */ String TELEMETRY_TAG; // = "Telemetry";
    @DexIgnore
    public static /* final */ String TOTAL_DISTANCE_TRAVELLED_WITH_NAVDY_METERS; // = "total_distance_travelled_with_navdy";
    @DexIgnore
    public static /* final */ int VALID_FUEL_DATA_WAIT_TIME; // = 45000;
    @DexIgnore
    public static /* final */ String VEHICLE_VIN; // = "vehicle_vin";
    @DexIgnore
    public static /* final */ int VOLTAGE_REPORT_INTERVAL; // = 2000;
    @DexIgnore
    private static /* final */ Logger sLogger; // = new com.navdy.service.library.log.Logger(com.navdy.hud.app.obd.ObdManager.class);
    @DexIgnore
    private static /* final */ Logger sTelemetryLogger; // = new com.navdy.service.library.log.Logger(TELEMETRY_TAG);
    @DexIgnore
    private static /* final */ ObdManager singleton; // = new com.navdy.hud.app.obd.ObdManager();
    @DexIgnore
    private double batteryVoltage; // = -1.0d;
    // @javax.inject.Inject
    @DexIgnore
    Bus bus;
    @DexIgnore
    private ICanBusMonitoringListener canBusMonitoringListener; // = new com.navdy.hud.app.obd.ObdManager.Anon7();
    @DexIgnore
    private CarServiceConnector carServiceConnector;
    @DexIgnore
    private Runnable checkVoltage; // = new com.navdy.hud.app.obd.ObdManager.Anon5();
    @DexIgnore
    private ObdManager.ConnectionType connectionType; // = com.navdy.hud.app.obd.ObdManager.ConnectionType.POWER_ONLY;
    @DexIgnore
    private DriveRecorder driveRecorder;
    // @javax.inject.Inject
    @DexIgnore
    DriverProfileManager driverProfileManager;
    @DexIgnore
    private List<ECU> ecus;
    @DexIgnore
    private boolean firstScan;
    @DexIgnore
    ObdManager.PidCheck fuelPidCheck;
    @DexIgnore
    private ScanSchedule fullScan;
    @DexIgnore
    private Handler handler;
    @DexIgnore
    private boolean isCheckEngineLightOn; // = false;
    @DexIgnore
    private int lowBatteryCount;
    @DexIgnore
    private double maxBatteryVoltage; // = -1.0d;
    @DexIgnore
    private long mileageEventLastReportTime; // = 0;
    @DexIgnore
    private double minBatteryVoltage; // = -1.0d;
    @DexIgnore
    private ScanSchedule minimalScan;
    @DexIgnore
    private ObdCanBusRecordingPolicy obdCanBusRecordingPolicy;
    @DexIgnore
    private String obdChipFirmwareVersion;
    @DexIgnore
    private volatile boolean obdConnected;
    @DexIgnore
    private ObdDeviceConfigurationManager obdDeviceConfigurationManager;
    @DexIgnore
    private HashMap<Integer, Double> obdPidsCache; // = new java.util.HashMap<>();
    @DexIgnore
    private boolean odoMeterReadingValidated; // = false;
    @DexIgnore
    private Runnable periodicCheckRunnable; // = new com.navdy.hud.app.obd.ObdManager.Anon4();
    @DexIgnore
    private IPidListener pidListener; // = new com.navdy.hud.app.obd.ObdManager.Anon6();
    // @javax.inject.Inject
    @DexIgnore
    PowerManager powerManager;
    @DexIgnore
    private String protocol;
    @DexIgnore
    private IPidListener recordingPidListener;
    @DexIgnore
    private int safeToSleepBatteryLevelObservedCount; // = 0;
    @DexIgnore
    private ScanSchedule schedule;
    // @javax.inject.Inject
    @DexIgnore
    SharedPreferences sharedPreferences;
    @DexIgnore
    private SpeedManager speedManager; // = com.navdy.hud.app.manager.SpeedManager.getInstance();
    @DexIgnore
    private PidSet supportedPidSet; // = new com.navdy.obd.PidSet();
    // @javax.inject.Inject
    @DexIgnore
    public TelemetryDataManager telemetryDataManager;
    @DexIgnore
    private long totalDistanceInCurrentSessionPersisted; // = 0;
    // @javax.inject.Inject
    @DexIgnore
    TripManager tripManager;
    @DexIgnore
    private List<String> troubleCodes;
    @DexIgnore
    private String vin;

    static {
        CRITICAL_BATTERY_VOLTAGE = ((double) SettingsManager.global.getFloat(CRITICAL_VOLTAGE_OVERRIDE_PROPERTY, DEFAULT_CRITICAL_BATTERY_VOLTAGE));
        LOW_BATTERY_VOLTAGE = ((double) SettingsManager.global.getFloat(LOW_VOLTAGE_OVERRIDE_PROPERTY, DEFAULT_LOW_BATTERY_VOLTAGE));
        LOW_VOLTAGE_SHUTOFF_DURATION = SettingsManager.global.getLong(LOW_VOLTAGE_DURATION_OVERRIDE_PROPERTY, DEFAULT_LOW_VOLTAGE_SHUTOFF_DURATION);
    }

    @DexIgnore
    class Anon1 extends ObdManager.PidCheck {
        @DexIgnore
        Anon1(int pid, Handler handler) {
            super(pid, handler);
        }

        @DexIgnore
        public boolean isPidValueValid(double value) {
            return value > 0.0d;
        }

        @DexIgnore
        public void invalidatePid(int pid) {
            // ObdManager.this.obdPidsCache.remove(Pids.FUEL_LEVEL);
            // if (ObdManager.this.supportedPidSet != null) {
            //     ObdManager.this.supportedPidSet.remove(Pids.FUEL_LEVEL);
            //     ObdManager.this.bus.post(new ObdManager.ObdSupportedPidsChangedEvent());
            // }
            // AnalyticsSupport.recordIncorrectFuelData();
        }

        @DexIgnore
        public long getWaitForValidDataTimeout() {
            return 45000;
        }
    }

    @DexIgnore
    class Anon2 implements Runnable {

        @DexIgnore
        class Anon1 implements Runnable {
            @DexIgnore
            Anon1() {
            }

            @DexIgnore
            public void run() {
                Logger.flush();
            }
        }

        @DexIgnore
        Anon2() {
        }

        @DexIgnore
        public void run() {
            TaskManager.getInstance().execute(new ObdManager.Anon2.Anon1(), 6);
            ObdManager.this.handler.postDelayed(this, 30000);
        }
    }

    @DexIgnore
    class Anon3 implements Runnable {
        @DexIgnore
        Anon3() {
        }

        @DexIgnore
        public void run() {
            ObdManager.this.updateListener();
        }
    }

    @DexIgnore
    class Anon4 implements Runnable {
        @DexIgnore
        Anon4() {
        }

        @DexIgnore
        public void run() {
            TaskManager.getInstance().execute(ObdManager.this.checkVoltage, 1);
            ObdManager.this.handler.postDelayed(this, 2000);
        }
    }

    @DexIgnore
    class Anon5 implements Runnable {
        @DexIgnore
        Anon5() {
        }

        @DexIgnore
        public void run() {
            // double prevVoltage = ObdManager.this.batteryVoltage;
            // ObdManager.this.batteryVoltage = ObdManager.this.getBatteryVoltage();
            // if (ObdManager.this.batteryVoltage != -1.0d && prevVoltage == -1.0d) {
            //     ObdManager.this.minBatteryVoltage = ObdManager.this.batteryVoltage;
            //     AnalyticsSupport.recordStartingBatteryVoltage(ObdManager.this.batteryVoltage);
            // }
            // if (ObdManager.this.batteryVoltage == 0.0d || ObdManager.this.batteryVoltage == -1.0d) {
            //     if (ObdManager.this.lowBatteryCount != 0) {
            //         ObdManager.sLogger.d("Battery level data is not available");
            //     }
            //     ObdManager.this.lowBatteryCount = 0;
            //     ObdManager.this.safeToSleepBatteryLevelObservedCount = 0;
            //     return;
            // }
            // if (ObdManager.this.batteryVoltage < ObdManager.this.minBatteryVoltage) {
            //     ObdManager.this.minBatteryVoltage = ObdManager.this.batteryVoltage;
            // }
            // if (ObdManager.this.batteryVoltage > ObdManager.this.maxBatteryVoltage) {
            //     ObdManager.this.maxBatteryVoltage = ObdManager.this.batteryVoltage;
            // }
            // double lowBatteryThreshold = ObdManager.this.powerManager.inQuietMode() ? ObdManager.LOW_BATTERY_VOLTAGE : ObdManager.CRITICAL_BATTERY_VOLTAGE;
            // if (ObdManager.this.batteryVoltage >= 13.100000381469727d && ObdManager.this.powerManager.inQuietMode()) {
            //     ObdManager.sLogger.d("Battery seems to be charging, waking up");
            //     ObdManager.this.powerManager.wakeUp(AnalyticsSupport.WakeupReason.VOLTAGE_SPIKE);
            //     ObdManager.this.safeToSleepBatteryLevelObservedCount = 0;
            // } else if (ObdManager.this.batteryVoltage < lowBatteryThreshold) {
            //     ObdManager.this.safeToSleepBatteryLevelObservedCount = 0;
            //     ObdManager.sLogger.d("Battery voltage (LOW) : " + ObdManager.this.batteryVoltage);
            //     ObdManager.this.lowBatteryCount = ObdManager.this.lowBatteryCount + 1;
            //     if (ObdManager.this.lowBatteryCount > ObdManager.LOW_BATTERY_COUNT_THRESHOLD) {
            //         ObdManager.sLogger.d("Battery Voltage is too low, shutting down");
            //         try {
            //             Shutdown.Reason reason = ObdManager.this.powerManager.inQuietMode() ? Shutdown.Reason.LOW_VOLTAGE : Shutdown.Reason.CRITICAL_VOLTAGE;
            //             ObdManager.this.sleep(true);
            //             ObdManager.this.bus.post(new Shutdown(reason));
            //         } catch (Exception e) {
            //             ObdManager.sLogger.e("error invoking ShutdownMonitor.androidShutdown(): " + e);
            //         }
            //     }
            // } else {
            //     ObdManager.this.lowBatteryCount = 0;
            //     if (ObdManager.this.powerManager.inQuietMode()) {
            //         ObdManager.sLogger.d("In Quiet mode , Battery voltage : " + ObdManager.this.batteryVoltage);
            //         ObdManager.this.safeToSleepBatteryLevelObservedCount = ObdManager.this.safeToSleepBatteryLevelObservedCount + 1;
            //         if (ObdManager.this.safeToSleepBatteryLevelObservedCount >= 5) {
            //             ObdManager.sLogger.d("Safe to put obd chip to sleep, invoking sleep");
            //             ObdManager.this.safeToSleepBatteryLevelObservedCount = 0;
            //             ObdManager.this.handler.removeCallbacks(ObdManager.this.periodicCheckRunnable);
            //             ObdManager.this.sleep(false);
            //         }
            //     }
            // }
        }
    }

    @DexIgnore
    class Anon6 extends IPidListener.Stub {
        @DexIgnore
        Anon6() {
        }

        @DexIgnore
        public synchronized void pidsRead(List<Pid> pidsRead, List<Pid> pidsChanged) throws RemoteException {
            if (ObdManager.this.firstScan && pidsChanged != null) {
                ObdManager.this.firstScan = false;
                try {
                    ICarService api = ObdManager.this.carServiceConnector.getCarApi();
                    ObdManager.this.vin = api.getVIN();
                    ObdManager.this.onVinRead(ObdManager.this.vin);
                    ObdManager.this.ecus = api.getEcus();
                    ObdManager.this.protocol = api.getProtocol();
                    List<Pid> supportedPids = ObdManager.this.carServiceConnector.getCarApi().getSupportedPids();
                    ObdManager.sLogger.d("First scan, got the supported PIDS");
                    if (supportedPids != null) {
                        ObdManager.sLogger.d("Supported PIDS size :" + supportedPids.size());
                    } else {
                        ObdManager.sLogger.d("First scan, got the supported PIDS , list null");
                    }
                    if (supportedPids != null) {
                        PidSet pidSet = new PidSet(supportedPids);
                        if (!pidSet.equals(ObdManager.this.supportedPidSet)) {
                            ObdManager.sTelemetryLogger.v("SUPPORTED VIN: " + (ObdManager.this.vin != null ? ObdManager.this.vin : "") + ", " + "FUEL: " + pidSet.contains(Pids.FUEL_LEVEL) + ", " + "SPEED: " + pidSet.contains(Pids.VEHICLE_SPEED) + ", " + "RPM: " + pidSet.contains(Pids.ENGINE_RPM) + ", " + "DIST: " + pidSet.contains(Pids.DISTANCE_TRAVELLED) + ", " + "MAF: " + pidSet.contains(16) + ", " + "IFC(LPKKM): " + supportedPids.contains(256));
                            ObdManager.this.supportedPidSet = pidSet;
                            ObdManager.this.fuelPidCheck.reset();
                            ObdManager.this.bus.post(new ObdManager.ObdSupportedPidsChangedEvent());
                            ObdManager.sLogger.v("pid-changed event sent");
                        } else {
                            ObdManager.sLogger.v("pid-changed event not sent");
                        }
                    }
                } catch (Throwable t) {
                    ObdManager.sLogger.e(t);
                }
            }
            if (pidsChanged == null) {
                ObdManager.this.bus.post(new ObdManager.ObdPidReadEvent(pidsRead));
            } else {
                for (Pid pid : pidsChanged) {
                    switch (pid.getId()) {
                        case Pids.VEHICLE_SPEED:
                            ObdManager.this.speedManager.setObdSpeed((int) pid.getValue(), pid.getTimeStamp());
                            break;
                        case Pids.FUEL_LEVEL:
                            ObdManager.this.fuelPidCheck.checkPid((double) ((int) pid.getValue()));
                            if (ObdManager.this.fuelPidCheck.hasIncorrectData()) {
                                ObdManager.this.obdPidsCache.remove(Pids.FUEL_LEVEL);
                                break;
                            } else {
                                ObdManager.this.obdPidsCache.put(pid.getId(), pid.getValue());
                                break;
                            }
                        case Pids.DISTANCE_TRAVELLED:
                            ObdManager.this.obdPidsCache.put(pid.getId(), pid.getValue());
                            ObdManager.this.updateCumulativeVehicleDistanceTravelled((long) pid.getValue());
                            break;
                        default:
                            ObdManager.this.obdPidsCache.put(pid.getId(), pid.getValue());
                            break;
                    }
                    ObdManager.this.bus.post(new ObdManager.ObdPidChangeEvent(pid));
                }
                if (!(ObdManager.this.recordingPidListener == null || pidsChanged == null)) {
                    ObdManager.this.recordingPidListener.pidsRead(pidsRead, pidsChanged);
                }
                ObdManager.this.bus.post(new ObdManager.ObdPidReadEvent(pidsRead));
            }
            return;
        }

        @DexIgnore
        public void pidsChanged(List<Pid> list) throws RemoteException {
        }

        @DexIgnore
        public void onConnectionStateChange(int newState) throws RemoteException {
            ObdManager.this.updateConnectionState(newState);
        }
    }

    @DexIgnore
    class Anon7 extends ICanBusMonitoringListener.Stub {
        @DexIgnore
        Anon7() {
        }

        @DexIgnore
        public void onNewDataAvailable(String fileName) throws RemoteException {
            ObdManager.sLogger.d("onNewDataAvailable : " + fileName);
            ObdManager.this.obdCanBusRecordingPolicy.onNewDataAvailable(fileName);
        }

        @DexIgnore
        public void onCanBusMonitoringError(String errorMessage) throws RemoteException {
            ObdManager.sLogger.d("onCanBusMonitoringError, Error : " + errorMessage);
            ObdManager.this.obdCanBusRecordingPolicy.onCanBusMonitoringFailed();
        }

        @DexIgnore
        public void onCanBusMonitorSuccess(int averageAmountOfData) throws RemoteException {
            ObdManager.sLogger.d("onCanBusMonitorSuccess , Average amount of data : " + averageAmountOfData);
            ObdManager.this.obdCanBusRecordingPolicy.onCanBusMonitorSuccess(averageAmountOfData);
        }

        @DexIgnore
        public int getGpsSpeed() throws RemoteException {
            return Math.round(ObdManager.this.speedManager.getGpsSpeedInMetersPerSecond());
        }

        @DexIgnore
        public double getLatitude() throws RemoteException {
            if (HereMapsManager.getInstance().isInitialized()) {
                GeoCoordinate c = HereMapsManager.getInstance().getLocationFixManager().getLastGeoCoordinate();
                if (c != null) {
                    return c.getLatitude();
                }
            }
            return -1.0d;
        }

        @DexIgnore
        public double getLongitude() throws RemoteException {
            if (HereMapsManager.getInstance().isInitialized()) {
                GeoCoordinate c = HereMapsManager.getInstance().getLocationFixManager().getLastGeoCoordinate();
                if (c != null) {
                    return c.getLongitude();
                }
            }
            return -1.0d;
        }

        @DexIgnore
        public boolean isMonitoringLimitReached() throws RemoteException {
            return ObdManager.this.obdCanBusRecordingPolicy.isCanBusMonitoringLimitReached();
        }

        @DexIgnore
        public String getMake() throws RemoteException {
            if (ObdManager.this.driverProfileManager != null) {
                DriverProfile profile = ObdManager.this.driverProfileManager.getCurrentProfile();
                if (profile != null) {
                    return profile.getCarMake();
                }
            }
            return "";
        }

        @DexIgnore
        public String getModel() throws RemoteException {
            if (ObdManager.this.driverProfileManager != null) {
                DriverProfile profile = ObdManager.this.driverProfileManager.getCurrentProfile();
                if (profile != null) {
                    return profile.getCarModel();
                }
            }
            return "";
        }

        @DexIgnore
        public String getYear() throws RemoteException {
            if (ObdManager.this.driverProfileManager != null) {
                DriverProfile profile = ObdManager.this.driverProfileManager.getCurrentProfile();
                if (profile != null) {
                    return profile.getCarYear();
                }
            }
            return "";
        }
    }

    @DexIgnore
    public enum ConnectionType {
        POWER_ONLY,
        OBD
    }

    @DexIgnore
    public enum Firmware {
        OLD_320(R.raw.update3_2_0),
        NEW_421(R.raw.update4_2_1);

        public int resource;

        @DexIgnore
        Firmware(int resource2) {
            this.resource = resource2;
        }
    }

    @DexIgnore
    public static class ObdConnectionStatusEvent {
        public boolean connected;

        @DexIgnore
        ObdConnectionStatusEvent(boolean connected2) {
            this.connected = connected2;
        }
    }

    @DexIgnore
    public static class ObdPidChangeEvent {
        public Pid pid;

        @DexIgnore
        ObdPidChangeEvent(Pid pid2) {
            this.pid = pid2;
        }
    }

    @DexIgnore
    public static class ObdPidReadEvent {
        public PidSet readPids;

        @DexIgnore
        public ObdPidReadEvent(List<Pid> readPids2) {
            this.readPids = new PidSet(readPids2);
        }
    }

    @DexIgnore
    public static class ObdSupportedPidsChangedEvent {
    }

    @DexIgnore
    class PidCheck {
        @DexIgnore
        private Handler handler;
        @DexIgnore
        volatile boolean hasIncorrectData = false;
        @DexIgnore
        private Runnable invalidPidRunnable;
        @DexIgnore
        int pid;
        @DexIgnore
        volatile boolean waitingForValidData = false;

        @DexIgnore
        class Anon1 implements Runnable {
            @DexIgnore
            final /* synthetic */ int val$pid;
            @DexIgnore
            final /* synthetic */ ObdManager val$this$Anon0;

            @DexIgnore
            Anon1(ObdManager obdManager, int i) {
                this.val$this$Anon0 = obdManager;
                this.val$pid = i;
            }

            @DexIgnore
            public void run() {
                if (!ObdManager.PidCheck.this.hasIncorrectData) {
                    ObdManager.PidCheck.this.waitingForValidData = false;
                    ObdManager.PidCheck.this.hasIncorrectData = true;
                    ObdManager.PidCheck.this.invalidatePid(this.val$pid);
                }
            }
        }

        @DexIgnore
        public PidCheck(int pid2, Handler handler2) {
            this.pid = pid2;
            this.handler = handler2;
            this.invalidPidRunnable = new ObdManager.PidCheck.Anon1(ObdManager.this, pid2);
        }

        @DexIgnore
        public boolean isPidValueValid(double value) {
            return true;
        }

        @DexIgnore
        public void invalidatePid(int pid2) {
        }

        @DexIgnore
        public boolean hasIncorrectData() {
            return this.hasIncorrectData;
        }

        @DexIgnore
        public boolean isWaitingForValidData() {
            return this.waitingForValidData;
        }

        @DexIgnore
        public long getWaitForValidDataTimeout() {
            return 0;
        }

        @DexIgnore
        public void reset() {
            this.hasIncorrectData = false;
            this.handler.removeCallbacks(this.invalidPidRunnable);
            this.waitingForValidData = false;
        }

        @DexIgnore
        public void checkPid(double value) {
            if (isPidValueValid(value)) {
                reset();
            } else if (this.hasIncorrectData) {
            } else {
                if (!this.waitingForValidData) {
                    this.handler.removeCallbacks(this.invalidPidRunnable);
                    this.handler.postDelayed(this.invalidPidRunnable, getWaitForValidDataTimeout());
                    this.waitingForValidData = true;
                    return;
                }
                ObdManager.sLogger.d("Already waiting for valid PID data, PID : " + this.pid + " ,  Value : " + value);
            }
        }
    }

    @DexIgnore
    public static ObdManager getInstance() {
        return singleton;
    }

    @DexIgnore
    public ObdManager.ConnectionType getConnectionType() {
        return this.connectionType;
    }

    @DexEdit
    // Original constructor, add tag to allow invoking from new constructor
    private ObdManager(@DexIgnore Void tag) {
        Mortar.inject(HudApplication.getAppContext(), this);
        this.obdCanBusRecordingPolicy = new ObdCanBusRecordingPolicy(HudApplication.getAppContext(), this.sharedPreferences, this, this.tripManager);
        this.carServiceConnector = new CarServiceConnector(HudApplication.getAppContext());
        this.obdDeviceConfigurationManager = new ObdDeviceConfigurationManager(this.carServiceConnector);
        this.minimalScan = new ScanSchedule();
        this.minimalScan.addPid(Pids.VEHICLE_SPEED, 100);
        this.minimalScan.addPid(Pids.FUEL_LEVEL, 15000);
        this.minimalScan.addPid(Pids.DISTANCE_TRAVELLED, 60000);
        this.minimalScan.addPid(Pids.ENGINE_RPM, SLOW_RPM_UPDATE_INTERVAL);
        this.fullScan = new ScanSchedule(this.minimalScan);
        this.fullScan.addPid(Pids.ENGINE_RPM, 250);
        this.fullScan.addPid(16, 1000);
        this.fullScan.addPid(256, 1000);
        this.fullScan.addPid(5, 2000);
        this.fullScan.addPid(Pids.ENGINE_OIL_PRESSURE, 1000);
        this.fullScan.addPid(Pids.ENGINE_TRIP_FUEL, 1000);
        this.fullScan.addPid(Pids.TOTAL_VEHICLE_DISTANCE, 1000);
        this.bus.register(this);
        this.bus.register(this.obdDeviceConfigurationManager);
        this.bus.register(this.obdCanBusRecordingPolicy);
        sTelemetryLogger.v("SESSION started");
        this.handler = new Handler(Looper.getMainLooper());
        this.fuelPidCheck = new ObdManager.Anon1(Pids.FUEL_LEVEL, this.handler);
        this.handler.post(new ObdManager.Anon2());
    }

    @DexAdd
    private ObdManager() {
        this((Void) null);
        this.minimalScan = new ScanSchedule();
        this.minimalScan.addPid(Pids.VEHICLE_SPEED, 100);
        this.minimalScan.addPid(Pids.FUEL_LEVEL, 15000);
        this.minimalScan.addPid(Pids.DISTANCE_TRAVELLED, 60000);
        this.minimalScan.addPid(Pids.ENGINE_RPM, SLOW_RPM_UPDATE_INTERVAL);
        this.fullScan = new ScanSchedule(this.minimalScan);
        this.fullScan.addPid(Pids.MANIFOLD_AIR_PRESSURE, GENERIC_UPDATE_INTERVAL);
        this.fullScan.addPid(Pids.ENGINE_RPM, RPM_UPDATE_INTERVAL);
        this.fullScan.addPid(Pids.MASS_AIRFLOW, GENERIC_UPDATE_INTERVAL);
        this.fullScan.addPid(Pids.ENGINE_OIL_TEMPERATURE, GENERIC_UPDATE_INTERVAL);
        this.fullScan.addPid(Pids.AMBIENT_AIR_TEMRERATURE, GENERIC_UPDATE_INTERVAL);
        this.fullScan.addPid(Pids.INSTANTANEOUS_FUEL_CONSUMPTION, MPG_UPDATE_INTERVAL);
        this.fullScan.addPid(Pids.ENGINE_COOLANT_TEMPERATURE, GENERIC_UPDATE_INTERVAL);
        this.fullScan.addPid(Pids.ENGINE_OIL_PRESSURE, GENERIC_UPDATE_INTERVAL);
        this.fullScan.addPid(Pids.ENGINE_TRIP_FUEL, GENERIC_UPDATE_INTERVAL);
        this.fullScan.addPid(Pids.TOTAL_VEHICLE_DISTANCE, GENERIC_UPDATE_INTERVAL);
    }

    @DexIgnore
    public String getObdChipFirmwareVersion() {
        return this.obdChipFirmwareVersion;
    }

    /* access modifiers changed from: 0000 */
    @DexIgnore
    public void serviceConnected() {
        TaskManager.getInstance().execute(new ObdManager.Anon3(), 6);
    }

    @DexIgnore
    public void updateListener() {
        ICarService carService = this.carServiceConnector.getCarApi();
        if (carService != null) {
            try {
                carService.setCANBusMonitoringListener(this.canBusMonitoringListener);
            } catch (RemoteException e) {
                sLogger.e("RemoteException ", e);
            }
            try {
                this.firstScan = true;
                int connectionState = carService.getConnectionState();
                switch (connectionState) {
                    case 2:
                    case 4:
                        this.obdChipFirmwareVersion = carService.getObdChipFirmwareVersion();
                        sLogger.d("Obd chip firmware version " + this.obdChipFirmwareVersion + ", Connection State :" + connectionState);
                        break;
                    case 5:
                        sLogger.d("ObdService is in Sleeping state, waking it up");
                        carService.wakeup();
                        break;
                }
                updateConnectionState(carService.getConnectionState());
                monitorBatteryVoltage();
                sLogger.i("adding listener for " + this.schedule);
                if (this.schedule != null) {
                    carService.updateScan(this.schedule, this.pidListener);
                }
                if (this.obdCanBusRecordingPolicy.isCanBusMonitoringNeeded()) {
                    sLogger.d("Start CAN bus monitoring");
                    carService.startCanBusMonitoring();
                    return;
                }
                sLogger.d("stop CAN bus monitoring");
                carService.stopCanBusMonitoring();
            } catch (Throwable t) {
                sLogger.e(t);
            }
        }
    }

    @DexIgnore
    public ICarService getCarService() {
        if (this.carServiceConnector != null) {
            return this.carServiceConnector.getCarApi();
        }
        return null;
    }

    // @com.squareup.otto.Subscribe
    @DexIgnore
    public void onWakeUp(Wakeup wakeup) {
        sLogger.d("onWakeUp");
        updateListener();
    }

    @DexIgnore
    public void monitorBatteryVoltage() {
        sLogger.i("monitorBatteryVoltage()");
        this.handler.removeCallbacks(this.periodicCheckRunnable);
        this.handler.postDelayed(this.periodicCheckRunnable, 4000);
    }

    @DexIgnore
    private void setConnected(boolean connected) {
        if (this.obdConnected != connected) {
            if (this.driveRecorder != null) {
                this.driveRecorder.setRealObdConnected(connected);
            }
            this.fuelPidCheck.reset();
            this.obdDeviceConfigurationManager.setConnectionState(connected);
            this.obdConnected = connected;
            if (connected) {
                try {
                    ICarService api = this.carServiceConnector.getCarApi();
                    this.vin = api.getVIN();
                    onVinRead(this.vin);
                    this.ecus = api.getEcus();
                    this.protocol = api.getProtocol();
                    this.isCheckEngineLightOn = api.isCheckEngineLightOn();
                    this.troubleCodes = api.getTroubleCodes();
                    this.obdChipFirmwareVersion = api.getObdChipFirmwareVersion();
                    sLogger.d("Connected, Obd chip firmware version " + this.obdChipFirmwareVersion);
                } catch (Throwable t) {
                    sLogger.d("Failed to read vin/ecu/protocol after connecting", t);
                }
            } else {
                this.firstScan = true;
                this.vin = null;
                this.ecus = null;
                this.protocol = null;
                this.speedManager.setObdSpeed(-1, 0);
                if (this.supportedPidSet != null) {
                    this.supportedPidSet.clear();
                }
                this.obdPidsCache.clear();
            }
            this.bus.post(connected ? OBD_CONNECTED : OBD_NOT_CONNECTED);
            this.obdCanBusRecordingPolicy.onObdConnectionStateChanged(connected);
        }
    }

    /* access modifiers changed from: 0000 */
    @DexIgnore
    public void serviceDisconnected() {
        setConnected(false);
    }

    @DexIgnore
    public boolean isConnected() {
        return this.obdConnected;
    }

    @DexIgnore
    public boolean isSleeping() {
        ICarService carService = this.carServiceConnector.getCarApi();
        if (carService == null) {
            return false;
        }
        try {
            return carService.getConnectionState() == 5;
        } catch (RemoteException e) {
            return false;
        }
    }

    @DexIgnore
    private synchronized void onVinRead(String vin2) {
        SharedPreferences sharedPreferences2 = RemoteDeviceManager.getInstance().getSharedPreferences();
        String savedVin = sharedPreferences2.getString(VEHICLE_VIN, null);
        if (!TextUtils.equals(vin2, savedVin)) {
            sLogger.d("Vin changed , Old :" + savedVin + ", New Vin :" + vin2);
            if (TextUtils.isEmpty(vin2)) {
                sharedPreferences2.edit().remove(VEHICLE_VIN).apply();
            } else {
                sharedPreferences2.edit().putString(VEHICLE_VIN, vin2).apply();
            }
            resetCumulativeMileageData();
        } else {
            sLogger.d("Vin has not changed , Vin " + vin2);
        }
    }

    @DexIgnore
    private void resetCumulativeMileageData() {
        SharedPreferences preferences = RemoteDeviceManager.getInstance().getSharedPreferences();
        sLogger.d("Resetting the cumulative mileage statistics");
        preferences.edit().putLong(FIRST_ODOMETER_READING_KM, 0).putLong(LAST_ODOMETER_READING_KM, 0).putLong(TOTAL_DISTANCE_TRAVELLED_WITH_NAVDY_METERS, 0).apply();
        this.totalDistanceInCurrentSessionPersisted = 0;
        this.odoMeterReadingValidated = false;
    }

    @DexIgnore
    private void reportMileageEvent() {
        // long currentTime = SystemClock.elapsedRealtime();
        // if (this.mileageEventLastReportTime == 0 || currentTime - this.mileageEventLastReportTime > MINIMUM_TIME_BETWEEN_MILEAGE_REPORTING) {
        //     SharedPreferences preferences = RemoteDeviceManager.getInstance().getSharedPreferences();
        //     long firstOdoMeterReadingKms = preferences.getLong(FIRST_ODOMETER_READING_KM, 0);
        //     long lastOdoMeterReadingKms = preferences.getLong(LAST_ODOMETER_READING_KM, 0);
        //     double totalDistanceVehicleTravelledInKms = (double) (lastOdoMeterReadingKms - firstOdoMeterReadingKms);
        //     sLogger.d("Initial reading (KM) : " + firstOdoMeterReadingKms + ", Current reading (KM) : " + lastOdoMeterReadingKms + " , Total (KM) : " + totalDistanceVehicleTravelledInKms);
        //     double totalDistanceTravelledWithNavdyInMeters = (double) preferences.getLong(TOTAL_DISTANCE_TRAVELLED_WITH_NAVDY_METERS, 0);
        //     sLogger.d("Distance travelled with Navdy(Meters) : " + totalDistanceTravelledWithNavdyInMeters);
        //     double totalDistanceTravelledWithNavdyKms = totalDistanceTravelledWithNavdyInMeters / 1000.0d;
        //     if (totalDistanceVehicleTravelledInKms > 0.0d && totalDistanceTravelledWithNavdyKms > 0.0d) {
        //         double usageRate = totalDistanceVehicleTravelledInKms / totalDistanceVehicleTravelledInKms;
        //         sLogger.d("Reporting the Navdy Mileage Vehicle (KMs) : " + totalDistanceVehicleTravelledInKms + " , Navdy (KMs) : " + totalDistanceVehicleTravelledInKms + " , Navdy Usage rate " + usageRate);
        //         AnalyticsSupport.recordNavdyMileageEvent(totalDistanceVehicleTravelledInKms, totalDistanceTravelledWithNavdyKms, usageRate);
        //         this.mileageEventLastReportTime = currentTime;
        //     }
        // }
    }

    @DexIgnore
    private synchronized void updateCumulativeVehicleDistanceTravelled(long odoMeterReadingInKms) {
        if (odoMeterReadingInKms > 0) {
            sLogger.d("New odometer reading (KMs) : " + odoMeterReadingInKms);
            SharedPreferences preferences = RemoteDeviceManager.getInstance().getSharedPreferences();
            if (!this.odoMeterReadingValidated) {
                sLogger.d("Validating the Cumulative mileage data");
                double savedOdoMeterReadingKms = (double) preferences.getLong(LAST_ODOMETER_READING_KM, 0);
                if (savedOdoMeterReadingKms == 0.0d) {
                    sLogger.d("No saved odo meter reading, starting fresh");
                    resetCumulativeMileageData();
                    preferences.edit().putLong(FIRST_ODOMETER_READING_KM, odoMeterReadingInKms).apply();
                } else if (((double) odoMeterReadingInKms) < savedOdoMeterReadingKms) {
                    sLogger.d("Odometer reading mismatch with saved reading, resetting, Saved :" + savedOdoMeterReadingKms + ", New : " + odoMeterReadingInKms);
                    resetCumulativeMileageData();
                    preferences.edit().putLong(FIRST_ODOMETER_READING_KM, odoMeterReadingInKms).apply();
                }
                this.odoMeterReadingValidated = true;
            }
            preferences.edit().putLong(LAST_ODOMETER_READING_KM, odoMeterReadingInKms).apply();
            TripManager tripManager2 = RemoteDeviceManager.getInstance().getTripManager();
            long savedDistanceTravelledWithNavdyMeters = preferences.getLong(TOTAL_DISTANCE_TRAVELLED_WITH_NAVDY_METERS, 0);
            long totalDistanceTravelledMeters = (long) tripManager2.getTotalDistanceTravelled();
            sLogger.d("Saving total distance travelled in current session, before :" + savedDistanceTravelledWithNavdyMeters + ", Current Trip mileage : " + tripManager2.getTotalDistanceTravelled());
            long newDistanceAfterLastSaveMeters = totalDistanceTravelledMeters - this.totalDistanceInCurrentSessionPersisted;
            if (newDistanceAfterLastSaveMeters > 0) {
                preferences.edit().putLong(TOTAL_DISTANCE_TRAVELLED_WITH_NAVDY_METERS, savedDistanceTravelledWithNavdyMeters + newDistanceAfterLastSaveMeters).apply();
                this.totalDistanceInCurrentSessionPersisted = totalDistanceTravelledMeters;
                reportMileageEvent();
            }
        }
    }

    @DexIgnore
    private void updateConnectionState(int newState) {
        if (newState == 2) {
            setConnected(true);
        } else {
            setConnected(false);
        }
        if (this.connectionType == ObdManager.ConnectionType.POWER_ONLY && newState > 1) {
            this.connectionType = ObdManager.ConnectionType.OBD;
        }
    }

    @DexIgnore
    private void setScanSchedule(ScanSchedule schedule2) {
        if (schedule2 != this.schedule) {
            this.schedule = schedule2;
            updateListener();
        }
    }

    // @com.squareup.otto.Subscribe
    @DexIgnore
    public void onDrivingStateChange(DrivingStateChange event) {
        if (event.driving && !isConnected()) {
            ICarService api = this.carServiceConnector.getCarApi();
            if (api != null) {
                try {
                    sLogger.i("Driving started - triggering obd scan");
                    api.rescan();
                } catch (RemoteException e) {
                    sLogger.e("Failed to trigger OBD rescan");
                }
            }
        }
    }

    @DexIgnore
    public void enableInstantaneousMode(boolean enable) {
        this.obdCanBusRecordingPolicy.onInstantaneousModeChanged(enable);
        if (enable) {
            setScanSchedule(this.fullScan);
        } else if (DriveRecorder.isAutoRecordingEnabled()) {
            setScanSchedule(this.fullScan);
        } else {
            setScanSchedule(this.minimalScan);
        }
    }

    @DexIgnore
    public void enableObdScanning(boolean enable) {
        ICarService carService = this.carServiceConnector.getCarApi();
        if (carService != null) {
            try {
                sLogger.i("setting obd scanning enabled:" + enable);
                carService.setObdPidsScanningEnabled(enable);
            } catch (RemoteException e) {
                sLogger.e("Error while " + (enable ? "enabling " : "disabling ") + "Obd PIDs scanning ", e);
            }
        }
    }

    @DexIgnore
    public void sleep(boolean deep) {
        ICarService carService = this.carServiceConnector.getCarApi();
        if (carService != null) {
            try {
                carService.sleep(deep);
            } catch (RemoteException e) {
                sLogger.e("Error during call to sleep Remote Exception " + e.getCause());
            }
        }
    }

    @DexIgnore
    public void wakeup() {
        ICarService carService = this.carServiceConnector.getCarApi();
        if (carService != null) {
            try {
                carService.wakeup();
            } catch (RemoteException e) {
                sLogger.e("Error during call to sleep Remote Exception " + e.getCause());
            }
        }
    }

    // @com.squareup.otto.Subscribe
    @DexIgnore
    public void onShutdown(Shutdown shutdown) {
        if (shutdown.state == Shutdown.State.SHUTTING_DOWN) {
            this.carServiceConnector.shutdown();
        }
    }

    // @com.squareup.otto.Subscribe
    @DexIgnore
    public void onObdStatusRequest(ObdStatusRequest request) {
        int speed = this.speedManager.getObdSpeed();
        this.bus.post(new RemoteEvent(new ObdStatusResponse(RequestStatus.REQUEST_SUCCESS, isConnected(), this.vin, speed != -1 ? speed : null)));
    }

    @DexAdd
    public int getDistanceToEmpty() {
        return (int) Math.round(getPidValue(Pids.DISTANCE_TO_EMPTY));
    }

    @DexAdd
    public int getIntakePressure() {
        return (int) Math.round(getPidValue(Pids.MANIFOLD_AIR_PRESSURE));
    }

    @DexIgnore
    public int getFuelLevel() {
        if (this.fuelPidCheck.hasIncorrectData || this.fuelPidCheck.isWaitingForValidData()) {
            return -1;
        }
        return (int) getPidValue(Pids.FUEL_LEVEL);
    }

    @DexIgnore
    public int getEngineRpm() {
        return (int) getPidValue(Pids.ENGINE_RPM);
    }

    @DexIgnore
    public double getInstantFuelConsumption() {
        return (double) ((int) getPidValue(256));
    }

    @DexIgnore
    public int getDistanceTravelled() {
        return (int) getPidValue(Pids.DISTANCE_TRAVELLED);
    }

    @DexIgnore
    public long getDistanceTravelledWithNavdy() {
        return RemoteDeviceManager.getInstance().getSharedPreferences().getLong(TOTAL_DISTANCE_TRAVELLED_WITH_NAVDY_METERS, -1);
    }

    @DexIgnore
    public String getVin() {
        return this.vin;
    }

    @DexIgnore
    public List<ECU> getEcus() {
        return this.ecus;
    }

    @DexIgnore
    public String getProtocol() {
        return this.protocol;
    }

    @DexIgnore
    public PidSet getSupportedPids() {
        return this.supportedPidSet;
    }

    @DexIgnore
    public double getPidValue(int pid) {
        if (this.obdPidsCache.containsKey(pid)) {
            return this.obdPidsCache.get(pid);
        }
        return -1.0d;
    }

    @DexIgnore
    public double getBatteryVoltage() {
        ICarService carService = this.carServiceConnector.getCarApi();
        if (carService != null) {
            try {
                this.batteryVoltage = carService.getBatteryVoltage();
            } catch (Throwable t) {
                sLogger.e(t);
            }
        }
        return this.batteryVoltage;
    }

    @DexIgnore
    public double getMinBatteryVoltage() {
        return this.minBatteryVoltage;
    }

    @DexIgnore
    public double getMaxBatteryVoltage() {
        return this.maxBatteryVoltage;
    }

    @DexIgnore
    public void setRecordingPidListener(IPidListener recordingPidListener2) {
        this.recordingPidListener = recordingPidListener2;
    }

    @DexIgnore
    public void injectObdData(List<Pid> pidsread, List<Pid> pidschanged) {
        try {
            this.pidListener.pidsRead(pidsread, pidschanged);
        } catch (RemoteException e) {
            sLogger.e("Remote exception when injecting fake obd data");
        }
    }

    @DexIgnore
    public void setSupportedPidSet(PidSet supportedPids) {
        if (this.firstScan) {
            this.firstScan = true;
            this.supportedPidSet = supportedPids;
        } else if (this.supportedPidSet != null) {
            this.supportedPidSet.merge(supportedPids);
        } else {
            this.supportedPidSet = supportedPids;
        }
        this.bus.post(new ObdManager.ObdSupportedPidsChangedEvent());
    }

    @DexIgnore
    public ObdDeviceConfigurationManager getObdDeviceConfigurationManager() {
        return this.obdDeviceConfigurationManager;
    }

    // @com.squareup.otto.Subscribe
    @DexIgnore
    public void onDriverProfileChanged(DriverProfileChanged changed) {
        updateObdScanSetting();
    }

    // @com.squareup.otto.Subscribe
    @DexIgnore
    public void onDriverProfileUpdated(DriverProfileUpdated updated) {
        updateObdScanSetting();
    }

    @DexIgnore
    private void updateObdScanSetting() {
        DriverProfilePreferences.ObdScanSetting savedSetting;
        if (this.driverProfileManager != null) {
            DriverProfile profile = this.driverProfileManager.getCurrentProfile();
            if (profile != null) {
                if (!profile.isAutoOnEnabled()) {
                    this.obdDeviceConfigurationManager.setAutoOnEnabled(false);
                } else if (!TextUtils.isEmpty(profile.getCarMake()) || !TextUtils.isEmpty(profile.getCarModel()) || !TextUtils.isEmpty(profile.getCarYear())) {
                    this.obdCanBusRecordingPolicy.onCarDetailsAvailable(profile.getCarMake(), profile.getCarModel(), profile.getCarYear());
                    this.obdDeviceConfigurationManager.setCarDetails(profile.getCarMake(), profile.getCarModel(), profile.getCarYear());
                }
                DriverProfilePreferences.ObdScanSetting scanSetting = profile.getObdScanSetting();
                sLogger.d("ObdScanSetting received : " + scanSetting);
                if (scanSetting != null) {
                    boolean writeSetting = false;
                    SharedPreferences preferences = RemoteDeviceManager.getInstance().getSharedPreferences();
                    if (preferences.getBoolean(DEFAULT_SET, false)) {
                        writeSetting = true;
                        savedSetting = isObdScanningEnabled() ? DriverProfilePreferences.ObdScanSetting.SCAN_DEFAULT_ON : DriverProfilePreferences.ObdScanSetting.SCAN_DEFAULT_OFF;
                        preferences.edit().remove(DEFAULT_SET).apply();
                    } else {
                        savedSetting = getScanSetting(preferences);
                    }
                    long phoneModificationTime = profile.getObdBlacklistModificationTime();
                    long localModificationTime = preferences.getLong(BLACKLIST_MODIFICATION_TIME, 0);
                    boolean phoneNewer = phoneModificationTime > localModificationTime;
                    sLogger.d("saved setting:" + savedSetting + " blacklist updated: (" + phoneModificationTime + HereManeuverDisplayBuilder.SLASH + localModificationTime + ") (phone/local)");
                    sLogger.d("Is Obd enabled :" + isObdScanningEnabled());
                    boolean isDefault = savedSetting == DriverProfilePreferences.ObdScanSetting.SCAN_DEFAULT_OFF || savedSetting == DriverProfilePreferences.ObdScanSetting.SCAN_DEFAULT_ON;
                    switch (scanSetting) {
                        case SCAN_DEFAULT_ON:
                            if (phoneNewer && isDefault) {
                                sLogger.d("Setting default to on");
                                enableObdScanning(true);
                                writeSetting = true;
                                break;
                            }
                        case SCAN_ON:
                            enableObdScanning(true);
                            writeSetting = true;
                            break;
                        case SCAN_OFF:
                            enableObdScanning(false);
                            writeSetting = true;
                            break;
                        case SCAN_DEFAULT_OFF:
                            if (phoneNewer && isDefault) {
                                enableObdScanning(false);
                                writeSetting = true;
                                break;
                            }
                    }
                    if (writeSetting) {
                        sLogger.i("Writing scan setting:" + scanSetting + " modTime:" + phoneModificationTime);
                        preferences.edit().putString(SCAN_SETTING, scanSetting.name()).putLong(BLACKLIST_MODIFICATION_TIME, phoneModificationTime).apply();
                    }
                }
            }
        }
    }

    @DexIgnore
    private DriverProfilePreferences.ObdScanSetting getScanSetting(SharedPreferences preferences) {
        DriverProfilePreferences.ObdScanSetting defaultSetting = DriverProfilePreferences.ObdScanSetting.SCAN_DEFAULT_ON;
        try {
            return DriverProfilePreferences.ObdScanSetting.valueOf(preferences.getString(SCAN_SETTING, defaultSetting.name()));
        } catch (Exception e) {
            sLogger.e("Failed to parse saved scan setting ", e);
            return defaultSetting;
        }
    }

    @DexIgnore
    public boolean isObdScanningEnabled() {
        ICarService carService = this.carServiceConnector.getCarApi();
        if (carService != null) {
            try {
                return carService.isObdPidsScanningEnabled();
            } catch (RemoteException e) {
                sLogger.e("RemoteException ", e);
            }
        }
        return false;
    }

    @DexIgnore
    public boolean isSpeedPidAvailable() {
        if (this.supportedPidSet != null) {
            return this.supportedPidSet.contains(Pids.VEHICLE_SPEED);
        }
        return false;
    }

    @DexIgnore
    public void setDriveRecorder(DriveRecorder driveRecorder2) {
        this.driveRecorder = driveRecorder2;
    }

    @DexIgnore
    public DriveRecorder getDriveRecorder() {
        return this.driveRecorder;
    }

    @DexIgnore
    public void setMode(int mode, boolean persistent) {
        switch (mode) {
            case 0:
            case 1:
                ICarService carService = this.carServiceConnector.getCarApi();
                if (carService != null) {
                    try {
                        carService.setMode(mode, persistent);
                        return;
                    } catch (RemoteException e) {
                        sLogger.e("RemoteException :" + e);
                        return;
                    }
                } else {
                    return;
                }
            default:
                return;
        }
    }

    @DexIgnore
    public void updateFirmware(ObdManager.Firmware firmware) {
        sLogger.d("Updating the firmware of the OBD chip , Version : " + (firmware == ObdManager.Firmware.OLD_320 ? "3.2.0" : FIRMWARE_VERSION));
        ICarService carService = this.carServiceConnector.getCarApi();
        InputStream is = HudApplication.getAppContext().getResources().openRawResource(firmware.resource);
        String absolutePath = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "update.bin";
        File file = new File(absolutePath);
        if (file.exists()) {
            IOUtils.deleteFile(HudApplication.getAppContext(), file.getAbsolutePath());
        }
        try {
            IOUtils.copyFile(file.getAbsolutePath(), is);
            try {
                carService.updateFirmware(FIRMWARE_VERSION, absolutePath);
            } catch (RemoteException e) {
                sLogger.e("Remote Exception while updating firmware ", e);
            }
        } catch (IOException e2) {
            sLogger.e("Error writing to the file ", e2);
        }
    }

    @DexIgnore
    public ObdCanBusRecordingPolicy getObdCanBusRecordingPolicy() {
        return this.obdCanBusRecordingPolicy;
    }

    @DexIgnore
    public boolean isCheckEngineLightOn() {
        return this.isCheckEngineLightOn;
    }

    @DexIgnore
    public List<String> getTroubleCodes() {
        return this.troubleCodes;
    }
}
