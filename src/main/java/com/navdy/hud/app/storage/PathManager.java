package com.navdy.hud.app.storage;

import android.content.Intent;
import android.os.Environment;

import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.device.gps.GpsConstants;
import com.navdy.hud.app.profile.NavdyPreferences;
import com.navdy.hud.app.util.CrashReportService;
import com.navdy.hud.app.util.DeviceUtil;
import com.navdy.hud.app.util.GenericUtil;
import com.navdy.hud.app.util.ReportIssueService;
import com.navdy.hud.app.util.os.PropsFileUpdater;
import com.navdy.hud.app.util.os.SystemProperties;
import com.navdy.service.library.events.file.FileType;
import com.navdy.service.library.file.IFileTransferAuthority;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.util.IOUtils;
import com.navdy.service.library.util.LogUtils;

import java.io.File;
import java.io.FileFilter;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit
public class PathManager implements IFileTransferAuthority {
    @DexIgnore
    private static /* final */ String ACTIVE_ROUTE_DIR; // = ".activeroute";
    @DexIgnore
    private static /* final */ String CAR_MD_RESPONSE_DISK_CACHE_FILES_DIR; // = "car_md_cache";
    @DexIgnore
    private static String CRASH_INFO_TEXT_FILE_NAME; // = "info.txt";
    @DexIgnore
    private static /* final */ String DATA_LOGS_DIR; // = "DataLogs";
    @DexIgnore
    private static /* final */ String DRIVER_PROFILES_DIR; // = "DriverProfiles";
    @DexIgnore
    private static /* final */ String GESTURE_VIDEOS_SYNC_FOLDER; // = "gesture_videos";
    @DexIgnore
    private static /* final */ String HERE_MAPS_CONFIG_BASE_PATH; // = "/.here-maps";
    @DexIgnore
    private static /* final */ Pattern HERE_MAPS_CONFIG_DIRS_PATTERN; // = java.util.regex.Pattern.compile(HERE_MAPS_CONFIG_DIRS_REGEX);
    @DexIgnore
    private static /* final */ String HERE_MAPS_CONFIG_DIRS_REGEX; // = "[0-9]{10}";
    @DexIgnore
    private static /* final */ String HERE_MAP_DATA_DIR; // = ".here-maps";
    @DexIgnore
    public static /* final */ String HERE_MAP_META_JSON_FILE; // = "meta.json";
    @DexIgnore
    private static /* final */ String HUD_DATABASE_DIR; // = "/.db";
    @DexIgnore
    private static /* final */ String IMAGE_DISK_CACHE_FILES_DIR; // = "img_disk_cache";
    @DexIgnore
    private static /* final */ String KERNEL_CRASH_CONSOLE_RAMOOPS; // = "/sys/fs/pstore/console-ramoops";
    @DexIgnore
    private static /* final */ String KERNEL_CRASH_CONSOLE_RAMOOPS_0; // = "/sys/fs/pstore/console-ramoops-0";
    @DexIgnore
    private static /* final */ String KERNEL_CRASH_DMESG_RAMOOPS; // = "/sys/fs/pstore/dmesg-ramoops-0";
    @DexIgnore
    private static /* final */ String[] KERNEL_CRASH_FILES; // = {KERNEL_CRASH_CONSOLE_RAMOOPS, KERNEL_CRASH_CONSOLE_RAMOOPS_0, KERNEL_CRASH_DMESG_RAMOOPS};
    @DexIgnore
    private static /* final */ String LOGS_FOLDER; // = "templogs";
    @DexIgnore
    private static /* final */ String MUSIC_LIBRARY_DISK_CACHE_FILES_DIR; // = "music_disk_cache";
    @DexIgnore
    private static /* final */ String NAVIGATION_FILES_DIR; // = "navigation_issues";
    @DexIgnore
    private static /* final */ String NON_FATAL_CRASH_REPORT_DIR; // = "/sdcard/.logs/snapshot/";
    @DexIgnore
    private static /* final */ String SYSTEM_CACHE_DIR; // = "/cache";
    @DexIgnore
    private static /* final */ String TEMP_FILE_TIMESTAMP_FORMAT; // = "'display_log'_yyyy_MM_dd-HH_mm_ss'.zip'";
    @DexIgnore
    private static /* final */ String TIMESTAMP_MWCONFIG_LATEST; // = com.navdy.hud.app.util.DeviceUtil.getCurrentHereSdkTimestamp();
    @DexIgnore
    private static SimpleDateFormat format; // = new java.text.SimpleDateFormat(TEMP_FILE_TIMESTAMP_FORMAT);
    @DexIgnore
    private static /* final */ Logger sLogger; // = new com.navdy.service.library.log.Logger(com.navdy.hud.app.storage.PathManager.class);
    @DexIgnore
    private static /* final */ PathManager sSingleton; // = new com.navdy.hud.app.storage.PathManager();
    @DexIgnore
    private String activeRouteInfoDir;
    @DexIgnore
    private String carMdResponseDiskCacheFolder;
    @DexIgnore
    private String databaseDir;
    @DexIgnore
    private String driverProfilesDir;
    @DexIgnore
    private String gestureVideosSyncFolder;
    @DexIgnore
    private File hereMapsConfigDirs;
    @DexIgnore
    private String hereMapsDataDirectory;
    @DexIgnore
    private String hereVoiceSkinsPath;
    @DexIgnore
    private String imageDiskCacheFolder;
    @DexIgnore
    private volatile boolean initialized;
    @DexIgnore
    private final String mapsPartitionPath;
    @DexIgnore
    private String musicDiskCacheFolder;
    @DexIgnore
    private String navigationIssuesFolder;
    @DexIgnore
    private String tempLogsFolder = (com.navdy.hud.app.HudApplication.getAppContext().getCacheDir().getAbsolutePath() + java.io.File.separator + LOGS_FOLDER);

    // @DexIgnore
    // class Anon1 implements FileFilter {
    //     @DexIgnore
    //     Anon1() {
    //     }
    //
    //     @DexIgnore
    //     public boolean accept(File file) {
    //         return file.isDirectory() && PathManager.HERE_MAPS_CONFIG_DIRS_PATTERN.matcher(file.getName()).matches();
    //     }
    // }

    @DexIgnore
    public static PathManager getInstance() {
        return sSingleton;
    }

    @DexReplace
    private PathManager() {
        String externalPath = Environment.getExternalStorageDirectory().getAbsolutePath();
        String internalAppPath = HudApplication.getAppContext().getFilesDir().getAbsolutePath();
        this.mapsPartitionPath = SystemProperties.get("ro.maps_partition", externalPath);
        this.hereMapsConfigDirs = new File(this.mapsPartitionPath + File.separator + HERE_MAPS_CONFIG_BASE_PATH);
        this.databaseDir = internalAppPath + File.separator + HUD_DATABASE_DIR;
        this.activeRouteInfoDir = internalAppPath + File.separator + ACTIVE_ROUTE_DIR;
        this.navigationIssuesFolder = internalAppPath + File.separator + NAVIGATION_FILES_DIR;
        this.carMdResponseDiskCacheFolder = internalAppPath + File.separator + CAR_MD_RESPONSE_DISK_CACHE_FILES_DIR;
        this.imageDiskCacheFolder = internalAppPath + File.separator + IMAGE_DISK_CACHE_FILES_DIR;
        this.musicDiskCacheFolder = internalAppPath + File.separator + MUSIC_LIBRARY_DISK_CACHE_FILES_DIR;
        this.gestureVideosSyncFolder = this.mapsPartitionPath + File.separator + GESTURE_VIDEOS_SYNC_FOLDER;
        File voiceSkinsFilepath = new File(this.mapsPartitionPath + File.separator + ".here-maps/voices-download");
        sLogger.v("voiceSkins path=" + voiceSkinsFilepath);
        this.hereVoiceSkinsPath = voiceSkinsFilepath.getAbsolutePath();
        this.hereMapsDataDirectory = this.mapsPartitionPath + File.separator + HERE_MAP_DATA_DIR;

        // Move driverProfilesDir to maps partition
        this.driverProfilesDir = mapsPartitionPath + File.separator + NavdyPreferences.SETTINGS + File.separator + DRIVER_PROFILES_DIR;
    }

    @DexIgnore
    private void init() {
        if (!this.initialized) {
            this.initialized = true;
            new File(this.tempLogsFolder).mkdirs();
            new File(this.databaseDir).mkdirs();
            new File(this.driverProfilesDir).mkdirs();
            new File(this.activeRouteInfoDir).mkdirs();
            new File(this.navigationIssuesFolder).mkdirs();
            new File(this.carMdResponseDiskCacheFolder).mkdirs();
            new File(NON_FATAL_CRASH_REPORT_DIR).mkdirs();
            new File(this.imageDiskCacheFolder).mkdirs();
            new File(this.musicDiskCacheFolder).mkdirs();
            new File(this.hereVoiceSkinsPath).mkdirs();
            new File(this.gestureVideosSyncFolder).mkdirs();
        }
    }

    @DexIgnore
    public List<String> getHereMapsConfigDirs() {
        throw null;
        // if (!this.initialized) {
        //     init();
        // }
        // List<String> pathList = new ArrayList<>();
        // File[] hereDirs = this.hereMapsConfigDirs.listFiles(new PathManager.Anon1());
        // if (hereDirs != null) {
        //     for (File f : hereDirs) {
        //         pathList.add(f.getAbsolutePath());
        //     }
        // }
        // return pathList;
    }

    @DexIgnore
    public String getLatestHereMapsConfigPath() {
        if (!this.initialized) {
            init();
        }
        return this.hereMapsConfigDirs.getAbsolutePath() + File.separator + TIMESTAMP_MWCONFIG_LATEST;
    }

    @DexIgnore
    public String getHereVoiceSkinsPath() {
        if (!this.initialized) {
            init();
        }
        return this.hereVoiceSkinsPath;
    }

    @DexIgnore
    public String[] getKernelCrashFiles() {
        if (!this.initialized) {
            init();
        }
        return KERNEL_CRASH_FILES;
    }

    @DexIgnore
    public String getDriverProfilesDir() {
        if (!this.initialized) {
            init();
        }
        return this.driverProfilesDir;
    }

    @DexIgnore
    public String getMapsPartitionPath() {
        if (!this.initialized) {
            init();
        }
        return this.mapsPartitionPath;
    }

    @DexIgnore
    public String getHereMapsDataDirectory() {
        if (!this.initialized) {
            init();
        }
        return this.hereMapsDataDirectory;
    }

    @DexIgnore
    public String getGestureVideosSyncFolder() {
        if (!this.initialized) {
            init();
        }
        return this.gestureVideosSyncFolder;
    }

    @DexIgnore
    public boolean isFileTypeAllowed(FileType fileType) {
        switch (fileType) {
            case FILE_TYPE_OTA:
            case FILE_TYPE_LOGS:
            case FILE_TYPE_PERF_TEST:
                return true;
            default:
                return false;
        }
    }

    @DexIgnore
    public String getDirectoryForFileType(FileType fileType) {
        if (!this.initialized) {
            init();
        }
        switch (fileType) {
            case FILE_TYPE_OTA:
                return SYSTEM_CACHE_DIR;
            case FILE_TYPE_LOGS:
                return this.tempLogsFolder;
            default:
                return null;
        }
    }

    @DexIgnore
    public String getFileToSend(FileType fileType) {
        if (fileType == FileType.FILE_TYPE_LOGS) {
            String tempLogFolder = getDirectoryForFileType(fileType);
            String stagingPath = tempLogFolder + File.separator + "stage";
            File stagingDirectory = new File(stagingPath);
            IOUtils.deleteDirectory(HudApplication.getAppContext(), stagingDirectory);
            IOUtils.createDirectory(stagingDirectory);
            try {
                LogUtils.copyComprehensiveSystemLogs(stagingPath);
                collectEnvironmentInfo(stagingPath);
                collectGpsLog(stagingPath);
                DeviceUtil.copyHEREMapsDataInfo(stagingPath);
                DeviceUtil.takeDeviceScreenShot(stagingDirectory.getAbsolutePath() + File.separator + "HUDScreenShot.png");
                List<File> driveLogFiles = ReportIssueService.getLatestDriveLogFiles(1);
                if (driveLogFiles != null && driveLogFiles.size() > 0) {
                    for (File driveLogFile : driveLogFiles) {
                        IOUtils.copyFile(driveLogFile.getAbsolutePath(), stagingDirectory.getAbsolutePath() + File.separator + driveLogFile.getName());
                    }
                }
                File[] logFiles = stagingDirectory.listFiles();
                String absolutePath = tempLogFolder + File.separator + generateTempFileName();
                CrashReportService.compressCrashReportsToZip(logFiles, absolutePath);
                if (new File(absolutePath).exists()) {
                    return absolutePath;
                }
            } catch (Throwable th) {
                return null;
            }
        }
        return null;
    }

    @DexIgnore
    public void onFileSent(FileType fileType) {
        if (fileType == FileType.FILE_TYPE_LOGS) {
            CrashReportService.clearCrashReports();
        }
    }

    @DexIgnore
    public static synchronized String generateTempFileName() {
        String format2;
        synchronized (PathManager.class) {
            format2 = format.format(System.currentTimeMillis());
        }
        return format2;
    }

    @DexIgnore
    public String getDatabaseDir() {
        if (!this.initialized) {
            init();
        }
        return this.databaseDir;
    }

    @DexIgnore
    public String getActiveRouteInfoDir() {
        if (!this.initialized) {
            init();
        }
        return this.activeRouteInfoDir;
    }

    @DexIgnore
    public String getNavigationIssuesDir() {
        if (!this.initialized) {
            init();
        }
        return this.navigationIssuesFolder;
    }

    @DexIgnore
    public String getCarMdResponseDiskCacheFolder() {
        if (!this.initialized) {
            init();
        }
        return this.carMdResponseDiskCacheFolder;
    }

    @DexIgnore
    public String getNonFatalCrashReportDir() {
        if (!this.initialized) {
            init();
        }
        return NON_FATAL_CRASH_REPORT_DIR;
    }

    @DexIgnore
    public String getImageDiskCacheFolder() {
        if (!this.initialized) {
            init();
        }
        return this.imageDiskCacheFolder;
    }

    @DexIgnore
    public String getMusicDiskCacheFolder() {
        if (!this.initialized) {
            init();
        }
        return this.musicDiskCacheFolder;
    }

    @DexIgnore
    private void collectGpsLog(String stagingPath) {
        try {
            Intent intent = new Intent(GpsConstants.GPS_COLLECT_LOGS);
            intent.putExtra(GpsConstants.GPS_EXTRA_LOG_PATH, stagingPath);
            HudApplication.getAppContext().sendBroadcastAsUser(intent, android.os.Process.myUserHandle());
            GenericUtil.sleep(3000);
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }

    @DexIgnore
    public void collectEnvironmentInfo(String stagingPath) {
        // FileWriter fileWriter = null;
        // try {
        //     FileWriter fileWriter2 = new FileWriter(new File(stagingPath + File.separator + CRASH_INFO_TEXT_FILE_NAME));
        //     try {
        //         fileWriter2.write(PropsFileUpdater.readProps());
        //         IOUtils.closeStream(fileWriter2);
        //         FileWriter fileWriter3 = fileWriter2;
        //     } catch (Throwable th) {
        //         th = th;
        //         fileWriter = fileWriter2;
        //         IOUtils.closeStream(fileWriter);
        //         throw th;
        //     }
        // } catch (Throwable th2) {
        //     t = th2;
        //     sLogger.e(t);
        //     IOUtils.closeStream(fileWriter);
        // }
    }
}
