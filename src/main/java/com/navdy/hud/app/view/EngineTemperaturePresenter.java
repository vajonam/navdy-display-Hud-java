package com.navdy.hud.app.view;

// @kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000v\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0006\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u0000 R2\u00020\u0001:\u0001RB\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\n\u0010C\u001a\u0004\u0018\u00010DH\u0016J\b\u0010E\u001a\u00020\u0018H\u0016J\b\u0010F\u001a\u00020\u0018H\u0016J\b\u0010G\u001a\u000206H\u0014J\u0010\u0010H\u001a\u00020I2\u0006\u0010J\u001a\u00020KH\u0007J\u001c\u0010L\u001a\u00020I2\b\u0010M\u001a\u0004\u0018\u00010N2\b\u0010O\u001a\u0004\u0018\u00010PH\u0016J\b\u0010Q\u001a\u00020IH\u0014R\u001a\u0010\u0005\u001a\u00020\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\nR\u001a\u0010\u000b\u001a\u00020\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\f\u0010\b\"\u0004\b\r\u0010\nR\u001a\u0010\u000e\u001a\u00020\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000f\u0010\b\"\u0004\b\u0010\u0010\nR\u001e\u0010\u0011\u001a\u00020\u00128\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0013\u0010\u0014\"\u0004\b\u0015\u0010\u0016R\u001a\u0010\u0017\u001a\u00020\u0018X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0019\u0010\u001a\"\u0004\b\u001b\u0010\u001cR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u001eR\u001e\u0010\u001f\u001a\u00020 8\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b!\u0010\"\"\u0004\b#\u0010$R$\u0010'\u001a\u00020&2\u0006\u0010%\u001a\u00020&@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b(\u0010)\"\u0004\b*\u0010+R\u001c\u0010,\u001a\u0004\u0018\u00010-X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b.\u0010/\"\u0004\b0\u00101R\u001a\u00102\u001a\u00020\u0018X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b3\u0010\u001a\"\u0004\b4\u0010\u001cR\u000e\u00105\u001a\u000206X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u00107\u001a\u000206X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u00108\u001a\u000209X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b:\u0010;\"\u0004\b<\u0010=R\u001a\u0010>\u001a\u000209X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b?\u0010;\"\u0004\b@\u0010=R\u0011\u0010A\u001a\u00020\u0018\u00a2\u0006\b\n\u0000\u001a\u0004\bB\u0010\u001a\u00a8\u0006S"}, d2 = {"Lcom/navdy/hud/app/view/EngineTemperaturePresenter;", "Lcom/navdy/hud/app/view/DashboardWidgetPresenter;", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "STATE_COLD", "", "getSTATE_COLD", "()I", "setSTATE_COLD", "(I)V", "STATE_HOT", "getSTATE_HOT", "setSTATE_HOT", "STATE_NORMAL", "getSTATE_NORMAL", "setSTATE_NORMAL", "bus", "Lcom/squareup/otto/Bus;", "getBus", "()Lcom/squareup/otto/Bus;", "setBus", "(Lcom/squareup/otto/Bus;)V", "celsiusLabel", "", "getCelsiusLabel", "()Ljava/lang/String;", "setCelsiusLabel", "(Ljava/lang/String;)V", "getContext", "()Landroid/content/Context;", "driverProfileManager", "Lcom/navdy/hud/app/profile/DriverProfileManager;", "getDriverProfileManager", "()Lcom/navdy/hud/app/profile/DriverProfileManager;", "setDriverProfileManager", "(Lcom/navdy/hud/app/profile/DriverProfileManager;)V", "value", "", "engineTemperature", "getEngineTemperature", "()D", "setEngineTemperature", "(D)V", "engineTemperatureDrawable", "Lcom/navdy/hud/app/view/drawable/EngineTemperatureDrawable;", "getEngineTemperatureDrawable", "()Lcom/navdy/hud/app/view/drawable/EngineTemperatureDrawable;", "setEngineTemperatureDrawable", "(Lcom/navdy/hud/app/view/drawable/EngineTemperatureDrawable;)V", "fahrenheitLabel", "getFahrenheitLabel", "setFahrenheitLabel", "mLeftOriented", "", "registered", "tempText", "Landroid/widget/TextView;", "getTempText", "()Landroid/widget/TextView;", "setTempText", "(Landroid/widget/TextView;)V", "tempUnitText", "getTempUnitText", "setTempUnitText", "widgetNameString", "getWidgetNameString", "getDrawable", "Landroid/graphics/drawable/Drawable;", "getWidgetIdentifier", "getWidgetName", "isRegisteringToBusRequired", "onSpeedUnitChanged", "", "speedUnitChangedEvent", "Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnitChanged;", "setView", "dashboardWidgetView", "Lcom/navdy/hud/app/view/DashboardWidgetView;", "arguments", "Landroid/os/Bundle;", "updateGauge", "Companion", "app_hudDebug"}, k = 1, mv = {1, 1, 6})
/* compiled from: EngineTemperaturePresenter.kt */
import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.navdy.hud.app.ExtensionsKt;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.manager.SpeedManager;
import com.navdy.hud.app.profile.DriverProfile;
import com.navdy.hud.app.profile.DriverProfileManager;
import com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager;
import com.navdy.hud.app.view.drawable.EngineTemperatureDrawable;
import com.navdy.service.library.events.preferences.DriverProfilePreferences;
import com.squareup.otto.Bus;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import kotlin.NoWhenBranchMatchedException;
import kotlin.TypeCastException;
import kotlin.jvm.internal.Intrinsics;
import lanchon.dexpatcher.annotation.DexAction;
import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import mortar.Mortar;

@DexEdit(defaultAction = DexAction.ADD)
public final class EngineTemperaturePresenter extends DashboardWidgetPresenter {
    @DexIgnore
    public static /* final */ EngineTemperaturePresenter.Companion Companion; // = new com.navdy.hud.app.view.EngineTemperaturePresenter.Companion(null);
    @DexIgnore
    private static /* final */ double TEMPERATURE_GAUGE_COLD_THRESHOLD; // = TEMPERATURE_GAUGE_COLD_THRESHOLD;
    @DexIgnore
    private static /* final */ double TEMPERATURE_GAUGE_HOT_THRESHOLD; // = TEMPERATURE_GAUGE_HOT_THRESHOLD;
    @DexIgnore
    private static /* final */ double TEMPERATURE_GAUGE_LOWER_BOUND_CELSIUS; // = TEMPERATURE_GAUGE_LOWER_BOUND_CELSIUS;
    @DexIgnore
    private static /* final */ double TEMPERATURE_GAUGE_MID_POINT_CELSIUS; // = TEMPERATURE_GAUGE_MID_POINT_CELSIUS;
    @DexIgnore
    private static /* final */ double TEMPERATURE_GAUGE_UPPER_BOUND_CELSIUS; // = TEMPERATURE_GAUGE_UPPER_BOUND_CELSIUS;
    @DexIgnore
    private int STATE_COLD; // = 1;
    @DexIgnore
    private int STATE_HOT; // = 2;
    @DexIgnore
    private int STATE_NORMAL;
    // @javax.inject.Inject
    // @org.jetbrains.annotations.NotNull
    @DexIgnore
    public Bus bus;
    // @org.jetbrains.annotations.NotNull
    @DexIgnore
    private String celsiusLabel; // = "";
    // @org.jetbrains.annotations.NotNull
    @DexIgnore
    private final Context context;
    // @javax.inject.Inject
    // @org.jetbrains.annotations.NotNull
    @DexIgnore
    public DriverProfileManager driverProfileManager;
    @DexIgnore
    private double engineTemperature;
    // @org.jetbrains.annotations.Nullable
    @DexIgnore
    private EngineTemperatureDrawable engineTemperatureDrawable; // = new com.navdy.hud.app.view.drawable.EngineTemperatureDrawable(this.context, com.navdy.hud.app.R.array.smart_dash_engine_temperature_state_colors);
    // @org.jetbrains.annotations.NotNull
    @DexIgnore
    private String fahrenheitLabel; // = "";
    @DexIgnore
    private boolean mLeftOriented; // = true;
    @DexIgnore
    private boolean registered;
    // @org.jetbrains.annotations.NotNull
    @DexIgnore
    public TextView tempText;
    // @org.jetbrains.annotations.NotNull
    @DexIgnore
    public TextView tempUnitText;
    // @org.jetbrains.annotations.NotNull
    @DexIgnore
    private final String widgetNameString;

    // @kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0006\n\u0002\b\u000b\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\u00020\u0004X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u0014\u0010\u0007\u001a\u00020\u0004X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\b\u0010\u0006R\u0014\u0010\t\u001a\u00020\u0004X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u0006R\u0014\u0010\u000b\u001a\u00020\u0004X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\f\u0010\u0006R\u0014\u0010\r\u001a\u00020\u0004X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u0006\u00a8\u0006\u000f"}, d2 = {"Lcom/navdy/hud/app/view/EngineTemperaturePresenter$Companion;", "", "()V", "TEMPERATURE_GAUGE_COLD_THRESHOLD", "", "getTEMPERATURE_GAUGE_COLD_THRESHOLD", "()D", "TEMPERATURE_GAUGE_HOT_THRESHOLD", "getTEMPERATURE_GAUGE_HOT_THRESHOLD", "TEMPERATURE_GAUGE_LOWER_BOUND_CELSIUS", "getTEMPERATURE_GAUGE_LOWER_BOUND_CELSIUS", "TEMPERATURE_GAUGE_MID_POINT_CELSIUS", "getTEMPERATURE_GAUGE_MID_POINT_CELSIUS", "TEMPERATURE_GAUGE_UPPER_BOUND_CELSIUS", "getTEMPERATURE_GAUGE_UPPER_BOUND_CELSIUS", "app_hudDebug"}, k = 1, mv = {1, 1, 6})
    /* compiled from: EngineTemperaturePresenter.kt */
    @DexIgnore
    public static final class Companion {
        @DexIgnore
        private Companion() {
        }

        // @DexIgnore
        // public /* synthetic */ Companion(kotlin.jvm.internal.DefaultConstructorMarker $constructor_marker) {
        //     this();
        // }

        @DexIgnore
        public final double getTEMPERATURE_GAUGE_LOWER_BOUND_CELSIUS() {
            return EngineTemperaturePresenter.TEMPERATURE_GAUGE_LOWER_BOUND_CELSIUS;
        }

        @DexIgnore
        public final double getTEMPERATURE_GAUGE_UPPER_BOUND_CELSIUS() {
            return EngineTemperaturePresenter.TEMPERATURE_GAUGE_UPPER_BOUND_CELSIUS;
        }

        @DexIgnore
        public final double getTEMPERATURE_GAUGE_MID_POINT_CELSIUS() {
            return EngineTemperaturePresenter.TEMPERATURE_GAUGE_MID_POINT_CELSIUS;
        }

        @DexIgnore
        public final double getTEMPERATURE_GAUGE_COLD_THRESHOLD() {
            return EngineTemperaturePresenter.TEMPERATURE_GAUGE_COLD_THRESHOLD;
        }

        @DexIgnore
        public final double getTEMPERATURE_GAUGE_HOT_THRESHOLD() {
            return EngineTemperaturePresenter.TEMPERATURE_GAUGE_HOT_THRESHOLD;
        }
    }

    @DexIgnore
    public EngineTemperaturePresenter(@NotNull Context context2) {
        Intrinsics.checkParameterIsNotNull(context2, "context");
        this.context = context2;
        EngineTemperatureDrawable engineTemperatureDrawable2 = this.engineTemperatureDrawable;
        if (engineTemperatureDrawable2 != null) {
            engineTemperatureDrawable2.setMinValue((float) Companion.getTEMPERATURE_GAUGE_LOWER_BOUND_CELSIUS());
        }
        EngineTemperatureDrawable engineTemperatureDrawable3 = this.engineTemperatureDrawable;
        if (engineTemperatureDrawable3 != null) {
            engineTemperatureDrawable3.setMaxGaugeValue((float) Companion.getTEMPERATURE_GAUGE_UPPER_BOUND_CELSIUS());
        }
        Resources resources = this.context.getResources();
        String string = resources.getString(R.string.temperature_unit_fahrenheit);
        Intrinsics.checkExpressionValueIsNotNull(string, "resources.getString(R.st\u2026perature_unit_fahrenheit)");
        this.fahrenheitLabel = string;
        String string2 = resources.getString(R.string.temperature_unit_celsius);
        Intrinsics.checkExpressionValueIsNotNull(string2, "resources.getString(R.st\u2026temperature_unit_celsius)");
        this.celsiusLabel = string2;
        Mortar.inject(HudApplication.getAppContext(), this);
        String string3 = resources.getString(R.string.widget_engine_temperature);
        Intrinsics.checkExpressionValueIsNotNull(string3, "resources.getString(R.st\u2026idget_engine_temperature)");
        this.widgetNameString = string3;
    }

    // @org.jetbrains.annotations.NotNull
    @DexIgnore
    public final Context getContext() {
        return this.context;
    }

    // @org.jetbrains.annotations.Nullable
    @DexIgnore
    public final EngineTemperatureDrawable getEngineTemperatureDrawable() {
        return this.engineTemperatureDrawable;
    }

    @DexIgnore
    public final void setEngineTemperatureDrawable(@Nullable EngineTemperatureDrawable engineTemperatureDrawable2) {
        this.engineTemperatureDrawable = engineTemperatureDrawable2;
    }

    @DexIgnore
    public final int getSTATE_NORMAL() {
        return this.STATE_NORMAL;
    }

    @DexIgnore
    public final void setSTATE_NORMAL(int i) {
        this.STATE_NORMAL = i;
    }

    @DexIgnore
    public final int getSTATE_COLD() {
        return this.STATE_COLD;
    }

    @DexIgnore
    public final void setSTATE_COLD(int i) {
        this.STATE_COLD = i;
    }

    @DexIgnore
    public final int getSTATE_HOT() {
        return this.STATE_HOT;
    }

    @DexIgnore
    public final void setSTATE_HOT(int i) {
        this.STATE_HOT = i;
    }

    // @org.jetbrains.annotations.NotNull
    @DexIgnore
    public final TextView getTempText() {
        TextView textView = this.tempText;
        if (textView == null) {
            Intrinsics.throwUninitializedPropertyAccessException("tempText");
        }
        return textView;
    }

    @DexIgnore
    public final void setTempText(@NotNull TextView textView) {
        Intrinsics.checkParameterIsNotNull(textView, "<set-?>");
        this.tempText = textView;
    }

    // @org.jetbrains.annotations.NotNull
    @DexIgnore
    public final TextView getTempUnitText() {
        TextView textView = this.tempUnitText;
        if (textView == null) {
            Intrinsics.throwUninitializedPropertyAccessException("tempUnitText");
        }
        return textView;
    }

    @DexIgnore
    public final void setTempUnitText(@NotNull TextView textView) {
        Intrinsics.checkParameterIsNotNull(textView, "<set-?>");
        this.tempUnitText = textView;
    }

    @DexIgnore
    public final double getEngineTemperature() {
        return this.engineTemperature;
    }

    @DexIgnore
    public final void setEngineTemperature(double value) {
        this.engineTemperature = value;
        reDraw();
    }

    // @org.jetbrains.annotations.NotNull
    @DexIgnore
    public final DriverProfileManager getDriverProfileManager() {
        DriverProfileManager driverProfileManager2 = this.driverProfileManager;
        if (driverProfileManager2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("driverProfileManager");
        }
        return driverProfileManager2;
    }

    @DexIgnore
    public final void setDriverProfileManager(@NotNull DriverProfileManager driverProfileManager2) {
        Intrinsics.checkParameterIsNotNull(driverProfileManager2, "<set-?>");
        this.driverProfileManager = driverProfileManager2;
    }

    // @org.jetbrains.annotations.NotNull
    @DexIgnore
    public final Bus getBus() {
        Bus bus2 = this.bus;
        if (bus2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("bus");
        }
        return bus2;
    }

    @DexIgnore
    public final void setBus(@NotNull Bus bus2) {
        Intrinsics.checkParameterIsNotNull(bus2, "<set-?>");
        this.bus = bus2;
    }

    // @org.jetbrains.annotations.NotNull
    @DexIgnore
    public final String getFahrenheitLabel() {
        return this.fahrenheitLabel;
    }

    @DexIgnore
    public final void setFahrenheitLabel(@NotNull String str) {
        Intrinsics.checkParameterIsNotNull(str, "<set-?>");
        this.fahrenheitLabel = str;
    }

    // @org.jetbrains.annotations.NotNull
    @DexIgnore
    public final String getCelsiusLabel() {
        return this.celsiusLabel;
    }

    @DexIgnore
    public final void setCelsiusLabel(@NotNull String str) {
        Intrinsics.checkParameterIsNotNull(str, "<set-?>");
        this.celsiusLabel = str;
    }

    // @org.jetbrains.annotations.NotNull
    @DexIgnore
    public final String getWidgetNameString() {
        return this.widgetNameString;
    }

    // @org.jetbrains.annotations.Nullable
    @DexIgnore
    public Drawable getDrawable() {
        return this.engineTemperatureDrawable;
    }

    @DexIgnore
    public void setView(@Nullable DashboardWidgetView dashboardWidgetView, @Nullable Bundle arguments) {
        int layoutResourceId = R.layout.engine_temp_gauge_left;
        if (arguments != null) {
            switch (arguments.getInt(DashboardWidgetPresenter.EXTRA_GRAVITY)) {
                case 0:
                    layoutResourceId = R.layout.engine_temp_gauge_left;
                    this.mLeftOriented = true;
                    break;
                case 2:
                    layoutResourceId = R.layout.engine_temp_gauge_right;
                    this.mLeftOriented = false;
                    break;
            }
        }
        if (dashboardWidgetView != null) {
            dashboardWidgetView.setContentView(layoutResourceId);
            View findViewById = dashboardWidgetView.findViewById(R.id.txt_value);
            if (findViewById == null) {
                throw new TypeCastException("null cannot be cast to non-null type android.widget.TextView");
            }
            this.tempText = (TextView) findViewById;
            View findViewById2 = dashboardWidgetView.findViewById(R.id.txt_unit);
            if (findViewById2 == null) {
                throw new TypeCastException("null cannot be cast to non-null type android.widget.TextView");
            }
            this.tempUnitText = (TextView) findViewById2;
        }
        super.setView(dashboardWidgetView, arguments);
        reDraw();
    }

    /* access modifiers changed from: protected */
    @DexIgnore
    public boolean isRegisteringToBusRequired() {
        return true;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0019, code lost:
        if (r7 != null) goto L_0x001b;
     */
    @DexIgnore
    public void updateGauge() {
        DriverProfilePreferences.UnitSystem unitSystem;
        CharSequence charSequence;
        CharSequence valueOf;
        if (this.mWidgetView != null) {
            DriverProfileManager driverProfileManager2 = this.driverProfileManager;
            if (driverProfileManager2 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("driverProfileManager");
            }
            if (driverProfileManager2 != null) {
                DriverProfile currentProfile = driverProfileManager2.getCurrentProfile();
                if (currentProfile != null) {
                    unitSystem = currentProfile.getUnitSystem();
                }
            }
            unitSystem = DriverProfilePreferences.UnitSystem.UNIT_SYSTEM_METRIC;
            EngineTemperatureDrawable engineTemperatureDrawable2 = this.engineTemperatureDrawable;
            if (engineTemperatureDrawable2 != null) {
                engineTemperatureDrawable2.setGaugeValue((float) ExtensionsKt.clamp(this.engineTemperature, Companion.getTEMPERATURE_GAUGE_COLD_THRESHOLD(), Companion.getTEMPERATURE_GAUGE_UPPER_BOUND_CELSIUS()));
            }
            EngineTemperatureDrawable engineTemperatureDrawable3 = this.engineTemperatureDrawable;
            if (engineTemperatureDrawable3 != null) {
                engineTemperatureDrawable3.setLeftOriented(this.mLeftOriented);
            }
            int state = this.STATE_NORMAL;
            if (this.engineTemperature <= Companion.getTEMPERATURE_GAUGE_COLD_THRESHOLD()) {
                state = this.STATE_COLD;
            } else if (this.engineTemperature > Companion.getTEMPERATURE_GAUGE_HOT_THRESHOLD()) {
                state = this.STATE_HOT;
            }
            EngineTemperatureDrawable engineTemperatureDrawable4 = this.engineTemperatureDrawable;
            if (engineTemperatureDrawable4 != null) {
                engineTemperatureDrawable4.setState(state);
            }
            TextView textView = this.tempText;
            if (textView == null) {
                Intrinsics.throwUninitializedPropertyAccessException("tempText");
            }
            if (textView != null) {
                switch (unitSystem) {
                    case UNIT_SYSTEM_METRIC:
                        valueOf = String.valueOf((int) this.engineTemperature);
                        break;
                    case UNIT_SYSTEM_IMPERIAL:
                        valueOf = String.valueOf((int) ExtensionsKt.celsiusToFahrenheit(this.engineTemperature));
                        break;
                    default:
                        throw new NoWhenBranchMatchedException();
                }
                textView.setText(valueOf);
            }
            TextView textView2 = this.tempUnitText;
            if (textView2 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("tempUnitText");
            }
            if (textView2 != null) {
                switch (unitSystem) {
                    case UNIT_SYSTEM_METRIC:
                        charSequence = this.celsiusLabel;
                        break;
                    case UNIT_SYSTEM_IMPERIAL:
                        charSequence = this.fahrenheitLabel;
                        break;
                    default:
                        throw new NoWhenBranchMatchedException();
                }
                textView2.setText(charSequence);
            }
        }
    }

    // @com.squareup.otto.Subscribe
    @DexIgnore
    public final void onSpeedUnitChanged(@NotNull SpeedManager.SpeedUnitChanged speedUnitChangedEvent) {
        Intrinsics.checkParameterIsNotNull(speedUnitChangedEvent, "speedUnitChangedEvent");
        reDraw();
    }

    // @org.jetbrains.annotations.NotNull
    @DexIgnore
    public String getWidgetIdentifier() {
        return SmartDashWidgetManager.ENGINE_TEMPERATURE_GAUGE_ID;
    }

    // @org.jetbrains.annotations.NotNull
    @DexIgnore
    public String getWidgetName() {
        return this.widgetNameString;
    }
}
