package com.navdy.hud.app.view;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.navdy.hud.app.ExtensionsKt;
import com.navdy.hud.app.HudApplication;
import alelec.navdy.hud.app.R;

import com.navdy.hud.app.config.SettingsManager;
import com.navdy.hud.app.manager.SpeedManager.SpeedUnitChanged;
import com.navdy.hud.app.profile.DriverProfile;
import com.navdy.hud.app.profile.DriverProfileManager;
import com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager;
import com.navdy.hud.app.view.drawable.IntakePressureDrawable;
import com.navdy.service.library.events.preferences.DriverProfilePreferences.UnitSystem;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.inject.Inject;

// import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.TypeCastException;
import kotlin.jvm.internal.Intrinsics;
import mortar.Mortar;

// @Metadata(bv = {1, 0, 1}, d1 = {"\u0000v\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0006\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u0000 R2\u00020\u0001:\u0001RB\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\n\u0010C\u001a\u0004\u0018\u00010DH\u0016J\b\u0010E\u001a\u00020\u0018H\u0016J\b\u0010F\u001a\u00020\u0018H\u0016J\b\u0010G\u001a\u000206H\u0014J\u0010\u0010H\u001a\u00020I2\u0006\u0010J\u001a\u00020KH\u0007J\u001c\u0010L\u001a\u00020I2\b\u0010M\u001a\u0004\u0018\u00010N2\b\u0010O\u001a\u0004\u0018\u00010PH\u0016J\b\u0010Q\u001a\u00020IH\u0014R\u001a\u0010\u0005\u001a\u00020\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\nR\u001a\u0010\u000b\u001a\u00020\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\f\u0010\b\"\u0004\b\r\u0010\nR\u001a\u0010\u000e\u001a\u00020\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000f\u0010\b\"\u0004\b\u0010\u0010\nR\u001e\u0010\u0011\u001a\u00020\u00128\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0013\u0010\u0014\"\u0004\b\u0015\u0010\u0016R\u001a\u0010\u0017\u001a\u00020\u0018X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0019\u0010\u001a\"\u0004\b\u001b\u0010\u001cR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u001eR\u001e\u0010\u001f\u001a\u00020 8\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b!\u0010\"\"\u0004\b#\u0010$R$\u0010'\u001a\u00020&2\u0006\u0010%\u001a\u00020&@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b(\u0010)\"\u0004\b*\u0010+R\u001c\u0010,\u001a\u0004\u0018\u00010-X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b.\u0010/\"\u0004\b0\u00101R\u001a\u00102\u001a\u00020\u0018X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b3\u0010\u001a\"\u0004\b4\u0010\u001cR\u000e\u00105\u001a\u000206X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u00107\u001a\u000206X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u00108\u001a\u000209X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b:\u0010;\"\u0004\b<\u0010=R\u001a\u0010>\u001a\u000209X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b?\u0010;\"\u0004\b@\u0010=R\u0011\u0010A\u001a\u00020\u0018\u00a2\u0006\b\n\u0000\u001a\u0004\bB\u0010\u001a\u00a8\u0006S"}, d2 = {"Lcom/navdy/hud/app/view/EngineTemperaturePresenter;", "Lcom/navdy/hud/app/view/DashboardWidgetPresenter;", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "STATE_COLD", "", "getSTATE_COLD", "()I", "setSTATE_COLD", "(I)V", "STATE_HOT", "getSTATE_HOT", "setSTATE_HOT", "STATE_NORMAL", "getSTATE_NORMAL", "setSTATE_NORMAL", "bus", "Lcom/squareup/otto/Bus;", "getBus", "()Lcom/squareup/otto/Bus;", "setBus", "(Lcom/squareup/otto/Bus;)V", "kPaLabel", "", "getkPaLabel", "()Ljava/lang/String;", "setkPaLabel", "(Ljava/lang/String;)V", "getContext", "()Landroid/content/Context;", "driverProfileManager", "Lcom/navdy/hud/app/profile/DriverProfileManager;", "getDriverProfileManager", "()Lcom/navdy/hud/app/profile/DriverProfileManager;", "setDriverProfileManager", "(Lcom/navdy/hud/app/profile/DriverProfileManager;)V", "value", "", "intakePressure", "getIntakePressure", "()D", "setIntakePressure", "(D)V", "intakePressureDrawable", "Lcom/navdy/hud/app/view/drawable/EngineTemperatureDrawable;", "getIntakePressureDrawable", "()Lcom/navdy/hud/app/view/drawable/EngineTemperatureDrawable;", "setIntakePressureDrawable", "(Lcom/navdy/hud/app/view/drawable/EngineTemperatureDrawable;)V", "psiLabel", "getPsiLabel", "setPsiLabel", "mLeftOriented", "", "registered", "tempText", "Landroid/widget/TextView;", "getTempText", "()Landroid/widget/TextView;", "setTempText", "(Landroid/widget/TextView;)V", "tempUnitText", "getTempUnitText", "setTempUnitText", "widgetNameString", "getWidgetNameString", "getDrawable", "Landroid/graphics/drawable/Drawable;", "getWidgetIdentifier", "getWidgetName", "isRegisteringToBusRequired", "onSpeedUnitChanged", "", "speedUnitChangedEvent", "Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnitChanged;", "setView", "dashboardWidgetView", "Lcom/navdy/hud/app/view/DashboardWidgetView;", "arguments", "Landroid/os/Bundle;", "updateGauge", "Companion", "app_hudRelease"}, k = 1, mv = {1, 1, 6})
/* compiled from: EngineTemperaturePresenter.kt */
public final class IntakePressurePresenter extends DashboardWidgetPresenter {

    private static final String INTAKE_PRESSURE_GAUGE_COLD_KPA_OVERRIDE_PROPERTY = "persist.sys.ip.low";
    private static final String INTAKE_PRESSURE_GAUGE_HOT_KPA_OVERRIDE_PROPERTY = "persist.sys.ip.high";
    private static final String INTAKE_PRESSURE_GAUGE_LOWER_BOUND_KPA_OVERRIDE_PROPERTY = "persist.sys.ip.lower";
    private static final String INTAKE_PRESSURE_GAUGE_MID_POINT_KPA_OVERRIDE_PROPERTY = "persist.sys.ip.mid";
    private static final String INTAKE_PRESSURE_GAUGE_UPPER_BOUND_KPA_OVERRIDE_PROPERTY = "persist.sys.ip.upper";

    private static final float DEFAULT_INTAKE_PRESSURE_GAUGE_GAUGE_COLD_KPA = 95.0f;
    private static final float DEFAULT_INTAKE_PRESSURE_GAUGE_HOT_KPA = 110.0f;
    private static final float DEFAULT_INTAKE_PRESSURE_GAUGE_LOWER_BOUND_KPA = 80.0f;
    private static final float DEFAULT_INTAKE_PRESSURE_GAUGE_MID_POINT_KPA = 150.0f;
    private static final float DEFAULT_INTAKE_PRESSURE_GAUGE_UPPER_BOUND_KPA = 220.0f;

    private static final double INTAKE_PRESSURE_GAUGE_COLD_KPA =  ((double) SettingsManager.global.getFloat(INTAKE_PRESSURE_GAUGE_COLD_KPA_OVERRIDE_PROPERTY, DEFAULT_INTAKE_PRESSURE_GAUGE_GAUGE_COLD_KPA));
    private static final double INTAKE_PRESSURE_GAUGE_HOT_KPA =  ((double) SettingsManager.global.getFloat(INTAKE_PRESSURE_GAUGE_HOT_KPA_OVERRIDE_PROPERTY, DEFAULT_INTAKE_PRESSURE_GAUGE_HOT_KPA));
    private static final double INTAKE_PRESSURE_GAUGE_LOWER_BOUND_KPA =  ((double) SettingsManager.global.getFloat(INTAKE_PRESSURE_GAUGE_LOWER_BOUND_KPA_OVERRIDE_PROPERTY, DEFAULT_INTAKE_PRESSURE_GAUGE_LOWER_BOUND_KPA));
    private static final double INTAKE_PRESSURE_GAUGE_MID_POINT_KPA =  ((double) SettingsManager.global.getFloat(INTAKE_PRESSURE_GAUGE_MID_POINT_KPA_OVERRIDE_PROPERTY, DEFAULT_INTAKE_PRESSURE_GAUGE_MID_POINT_KPA));
    private static final double INTAKE_PRESSURE_GAUGE_UPPER_BOUND_KPA =  ((double) SettingsManager.global.getFloat(INTAKE_PRESSURE_GAUGE_UPPER_BOUND_KPA_OVERRIDE_PROPERTY, DEFAULT_INTAKE_PRESSURE_GAUGE_UPPER_BOUND_KPA));


    public static final Companion Companion = new Companion();
    private int STATE_COLD = 1;
    private int STATE_HOT = 2;
    private int STATE_NORMAL;
    @Inject
    @NotNull
    public Bus bus;
    @NotNull
    private String kPaLabel = "";
    @NotNull
    private final Context context;
    @Inject
    @NotNull
    public DriverProfileManager driverProfileManager;
    private double intakePressure;
    @Nullable
    private IntakePressureDrawable intakePressureDrawable;
    @NotNull
    private String psiLabel = "";
    private boolean mLeftOriented = true;
    private boolean registered;
    @NotNull
    public TextView tempText;
    @NotNull
    public TextView tempUnitText;
    @NotNull
    private final String widgetNameString;

    // @Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0006\n\u0002\b\u000b\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\u00020\u0004X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u0014\u0010\u0007\u001a\u00020\u0004X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\b\u0010\u0006R\u0014\u0010\t\u001a\u00020\u0004X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u0006R\u0014\u0010\u000b\u001a\u00020\u0004X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\f\u0010\u0006R\u0014\u0010\r\u001a\u00020\u0004X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u0006\u00a8\u0006\u000f"}, d2 = {"Lcom/navdy/hud/app/view/EngineTemperaturePresenter$Companion;", "", "()V", "TEMPERATURE_GAUGE_COLD_KPA", "", "getINTAKE_PRESSURE_GAUGE_COLD_THRESHOLD", "()D", "TEMPERATURE_GAUGE_HOT_THRESHOLD", "getINTAKE_PRESSURE_GAUGE_HOT_THRESHOLD", "TEMPERATURE_GAUGE_LOWER_BOUND_KPA", "getINTAKE_PRESSURE_GAUGE_LOWER_BOUND_CELSIUS", "TEMPERATURE_GAUGE_MID_POINT_KPA", "getINTAKE_PRESSURE_GAUGE_MID_POINT_CELSIUS", "TEMPERATURE_GAUGE_UPPER_BOUND_KPA", "getINTAKE_PRESSURE_GAUGE_UPPER_BOUND_CELSIUS", "app_hudRelease"}, k = 1, mv = {1, 1, 6})
    /* compiled from: EngineTemperaturePresenter.kt */
    public static final class Companion {
        private Companion() {
        }

//        public /* synthetic */ Companion(DefaultConstructorMarker $constructor_marker) {
//            this();
//        }

        public final double getINTAKE_PRESSURE_GAUGE_LOWER_BOUND_CELSIUS() {
            return IntakePressurePresenter.INTAKE_PRESSURE_GAUGE_LOWER_BOUND_KPA;
        }

        public final double getINTAKE_PRESSURE_GAUGE_UPPER_BOUND_CELSIUS() {
            return IntakePressurePresenter.INTAKE_PRESSURE_GAUGE_UPPER_BOUND_KPA;
        }

        public final double getINTAKE_PRESSURE_GAUGE_MID_POINT_CELSIUS() {
            return IntakePressurePresenter.INTAKE_PRESSURE_GAUGE_MID_POINT_KPA;
        }

        public final double getINTAKE_PRESSURE_GAUGE_COLD_THRESHOLD() {
            return IntakePressurePresenter.INTAKE_PRESSURE_GAUGE_COLD_KPA;
        }

        public final double getINTAKE_PRESSURE_GAUGE_HOT_THRESHOLD() {
            return IntakePressurePresenter.INTAKE_PRESSURE_GAUGE_HOT_KPA;
        }
    }

    public IntakePressurePresenter(@NotNull Context context) {
        Intrinsics.checkParameterIsNotNull(context, "context");
        this.context = context;
        this.intakePressureDrawable = new IntakePressureDrawable(this.context, R.array.smart_dash_intake_pressure_state_colors);
        if (this.intakePressureDrawable != null) {
            this.intakePressureDrawable.setMinValue((float) Companion.getINTAKE_PRESSURE_GAUGE_LOWER_BOUND_CELSIUS());
            this.intakePressureDrawable.setMaxGaugeValue((float) Companion.getINTAKE_PRESSURE_GAUGE_UPPER_BOUND_CELSIUS());
        }
        Resources resources = this.context.getResources();
        String string = resources.getString(R.string.pressure_unit_psi);
        Intrinsics.checkExpressionValueIsNotNull(string, "resources.getString(R.st\u2026perature_unit_fahrenheit)");
        this.psiLabel = string;
        string = resources.getString(R.string.pressure_unit_kpa);
        Intrinsics.checkExpressionValueIsNotNull(string, "resources.getString(R.st\u2026temperature_unit_celsius)");
        this.kPaLabel = string;
        Mortar.inject(HudApplication.getAppContext(), this);
        string = resources.getString(R.string.widget_intake_pressure);
        Intrinsics.checkExpressionValueIsNotNull(string, "resources.getString(R.st\u2026idget_intake_pressure)");
        this.widgetNameString = string;
    }

    @NotNull
    public final Context getContext() {
        return this.context;
    }

    @Nullable
    public final IntakePressureDrawable getIntakePressureDrawable() {
        return this.intakePressureDrawable;
    }

    public final void setIntakePressureDrawable(@Nullable IntakePressureDrawable set) {
        this.intakePressureDrawable = set;
    }

    public final int getSTATE_NORMAL() {
        return this.STATE_NORMAL;
    }

    public final void setSTATE_NORMAL(int set) {
        this.STATE_NORMAL = set;
    }

    public final int getSTATE_COLD() {
        return this.STATE_COLD;
    }

    public final void setSTATE_COLD(int set) {
        this.STATE_COLD = set;
    }

    public final int getSTATE_HOT() {
        return this.STATE_HOT;
    }

    public final void setSTATE_HOT(int set) {
        this.STATE_HOT = set;
    }

    @NotNull
    public final TextView getTempText() {
        TextView textView = this.tempText;
        if (textView == null) {
            Intrinsics.throwUninitializedPropertyAccessException("tempText");
        }
        return textView;
    }

    public final void setTempText(@NotNull TextView set) {
        Intrinsics.checkParameterIsNotNull(set, "set");
        this.tempText = set;
    }

    @NotNull
    public final TextView getTempUnitText() {
        TextView textView = this.tempUnitText;
        if (textView == null) {
            Intrinsics.throwUninitializedPropertyAccessException("tempUnitText");
        }
        return textView;
    }

    public final void setTempUnitText(@NotNull TextView set) {
        Intrinsics.checkParameterIsNotNull(set, "set");
        this.tempUnitText = set;
    }

    public final double getIntakePressure() {
        return this.intakePressure;
    }

    public final void setIntakePressure(double value) {
        this.intakePressure = value;
        reDraw();
    }

    @NotNull
    public final DriverProfileManager getDriverProfileManager() {
        DriverProfileManager driverProfileManager = this.driverProfileManager;
        if (driverProfileManager == null) {
            Intrinsics.throwUninitializedPropertyAccessException("driverProfileManager");
        }
        return driverProfileManager;
    }

    public final void setDriverProfileManager(@NotNull DriverProfileManager set) {
        Intrinsics.checkParameterIsNotNull(set, "set");
        this.driverProfileManager = set;
    }

    @NotNull
    public final Bus getBus() {
        Bus bus = this.bus;
        if (bus == null) {
            Intrinsics.throwUninitializedPropertyAccessException("bus");
        }
        return bus;
    }

    public final void setBus(@NotNull Bus set) {
        Intrinsics.checkParameterIsNotNull(set, "set");
        this.bus = set;
    }

    @NotNull
    public final String getPsiLabel() {
        return this.psiLabel;
    }

    public final void setPsiLabel(@NotNull String set) {
        Intrinsics.checkParameterIsNotNull(set, "set");
        this.psiLabel = set;
    }

    @NotNull
    public final String getkPaLabel() {
        return this.kPaLabel;
    }

    public final void setkPaLabel(@NotNull String set) {
        Intrinsics.checkParameterIsNotNull(set, "set");
        this.kPaLabel = set;
    }

    @NotNull
    public final String getWidgetNameString() {
        return this.widgetNameString;
    }

    @Nullable
    public Drawable getDrawable() {
        return this.intakePressureDrawable;
    }

    public void setView(@Nullable DashboardWidgetView dashboardWidgetView, @Nullable Bundle arguments) {
        int layoutResourceId = R.layout.intake_press_gauge_left;
        if (arguments != null) {
            switch (arguments.getInt(DashboardWidgetPresenter.EXTRA_GRAVITY)) {
                case 0:
                    layoutResourceId = R.layout.intake_press_gauge_left;
                    this.mLeftOriented = true;
                    break;
                case 2:
                    layoutResourceId = R.layout.intake_press_gauge_right;
                    this.mLeftOriented = false;
                    break;
            }
        }
        if (dashboardWidgetView != null) {
            dashboardWidgetView.setContentView(layoutResourceId);
            View findViewById = dashboardWidgetView.findViewById(R.id.txt_value);
            if (findViewById == null) {
                throw new TypeCastException("null cannot be cast to non-null type android.widget.TextView");
            }
            this.tempText = (TextView) findViewById;
            findViewById = dashboardWidgetView.findViewById(R.id.txt_unit);
            if (findViewById == null) {
                throw new TypeCastException("null cannot be cast to non-null type android.widget.TextView");
            }
            this.tempUnitText = (TextView) findViewById;
        }
        super.setView(dashboardWidgetView, arguments);
        reDraw();
    }

    protected boolean isRegisteringToBusRequired() {
        return true;
    }

    protected void updateGauge() {
        if (this.mWidgetView != null) {
            UnitSystem unitSystem = UnitSystem.UNIT_SYSTEM_METRIC;
            CharSequence valueOf;
            DriverProfileManager driverProfileManager = this.driverProfileManager;
            if (driverProfileManager == null) {
                Intrinsics.throwUninitializedPropertyAccessException("driverProfileManager");
            } else {
                DriverProfile currentProfile = driverProfileManager.getCurrentProfile();
                if (currentProfile != null) {
                    unitSystem = currentProfile.getUnitSystem();
                }
            }
            IntakePressureDrawable intakePressureDrawable = this.intakePressureDrawable;
            if (intakePressureDrawable != null) {
                intakePressureDrawable.setGaugeValue((float) ExtensionsKt.clamp(this.intakePressure, Companion.getINTAKE_PRESSURE_GAUGE_COLD_THRESHOLD(), Companion.getINTAKE_PRESSURE_GAUGE_UPPER_BOUND_CELSIUS()));
                intakePressureDrawable.setLeftOriented(this.mLeftOriented);
            }
            int state = this.STATE_NORMAL;
            if (this.intakePressure <= Companion.getINTAKE_PRESSURE_GAUGE_COLD_THRESHOLD()) {
                state = this.STATE_COLD;
            } else if (this.intakePressure > Companion.getINTAKE_PRESSURE_GAUGE_HOT_THRESHOLD()) {
                state = this.STATE_HOT;
            }
            if (intakePressureDrawable != null) {
                intakePressureDrawable.setState(state);
            }
            TextView textView = this.tempText;
            if (textView == null) {
                Intrinsics.throwUninitializedPropertyAccessException("tempText");
            } else {
                switch (unitSystem) {
                    case UNIT_SYSTEM_METRIC:
                        valueOf = String.valueOf((int) this.intakePressure) ;
                        break;
                    case UNIT_SYSTEM_IMPERIAL:
                        valueOf = String.valueOf((int) ExtensionsKt.kPaToPsi(this.intakePressure));
                        break;
                    default:
                        throw new NoWhenBranchMatchedException();
                }
                textView.setText(valueOf);
            }
            textView = this.tempUnitText;
            if (textView == null) {
                Intrinsics.throwUninitializedPropertyAccessException("tempUnitText");
            } else {
                switch (unitSystem) {
                    case UNIT_SYSTEM_METRIC:
                        valueOf = this.kPaLabel;
                        break;
                    case UNIT_SYSTEM_IMPERIAL:
                        valueOf = this.psiLabel;
                        break;
                    default:
                        throw new NoWhenBranchMatchedException();
                }
                textView.setText(valueOf);
            }
        }
    }

    @Subscribe
    public final void onSpeedUnitChanged(@NotNull SpeedUnitChanged speedUnitChangedEvent) {
        Intrinsics.checkParameterIsNotNull(speedUnitChangedEvent, "speedUnitChangedEvent");
        reDraw();
    }

    @NotNull
    public String getWidgetIdentifier() {
        return SmartDashWidgetManager.INTAKE_PRESSURE_GAUGE_ID;
    }

    @NotNull
    public String getWidgetName() {
        return this.widgetNameString;
    }
}
