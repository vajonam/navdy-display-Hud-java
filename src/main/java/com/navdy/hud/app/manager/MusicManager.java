package com.navdy.hud.app.manager;

import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Handler;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.navdy.hud.app.config.SettingsManager;
import com.navdy.hud.app.event.DriverProfileChanged;
import com.navdy.hud.app.event.LocalSpeechRequest;
import com.navdy.hud.app.event.RemoteEvent;
import com.navdy.hud.app.framework.DriverProfileHelper;
import com.navdy.hud.app.framework.glance.GlanceHelper;
import com.navdy.hud.app.framework.music.MusicNotification;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import com.navdy.hud.app.service.pandora.PandoraManager;
import com.navdy.hud.app.storage.cache.MessageCache;
import com.navdy.hud.app.ui.component.mainmenu.MusicMenu2;
import com.navdy.hud.app.ui.framework.UIStateManager;
import com.navdy.hud.app.util.MusicArtworkCache;
import com.navdy.service.library.events.Capabilities;
import com.navdy.service.library.events.DeviceInfo;
import com.navdy.service.library.events.DeviceInfo.Platform;
import com.navdy.service.library.events.audio.MusicArtworkRequest;
import com.navdy.service.library.events.audio.MusicArtworkResponse;
import com.navdy.service.library.events.audio.MusicCapabilitiesRequest.Builder;
import com.navdy.service.library.events.audio.MusicCapabilitiesResponse;
import com.navdy.service.library.events.audio.MusicCapability;
import com.navdy.service.library.events.audio.MusicCollectionResponse;
import com.navdy.service.library.events.audio.MusicCollectionSource;
import com.navdy.service.library.events.audio.MusicCollectionSourceUpdate;
import com.navdy.service.library.events.audio.MusicCollectionType;
import com.navdy.service.library.events.audio.MusicDataSource;
import com.navdy.service.library.events.audio.MusicEvent.Action;
import com.navdy.service.library.events.audio.MusicPlaybackState;
import com.navdy.service.library.events.audio.MusicShuffleMode;
import com.navdy.service.library.events.audio.MusicTrackInfo;
import com.navdy.service.library.events.audio.MusicTrackInfoRequest;
import com.navdy.service.library.events.audio.ResumeMusicRequest;
import com.navdy.service.library.events.callcontrol.PhoneEvent;
import com.navdy.service.library.events.callcontrol.PhoneStatus;
import com.navdy.service.library.events.connection.ConnectionStateChange;
import com.navdy.service.library.events.connection.ConnectionStateChange.ConnectionState;
import com.navdy.service.library.events.input.KeyEvent;
import com.navdy.service.library.events.input.MediaRemoteKey;
import com.navdy.service.library.events.input.MediaRemoteKeyEvent;
import com.navdy.service.library.events.photo.PhotoType;
import com.navdy.service.library.events.photo.PhotoUpdate;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.navdy.service.library.util.MusicDataUtils;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import com.squareup.wire.Wire;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

import lanchon.dexpatcher.annotation.DexAction;
import lanchon.dexpatcher.annotation.DexAdd;
import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexReplace;
import okio.ByteString;

@DexEdit(defaultAction = DexAction.ADD, staticConstructorAction = DexAction.APPEND)
public class MusicManager {
    @DexIgnore
    public static /* final */ MediaControl[] CONTROLS; // = com.navdy.hud.app.manager.MusicManager.MediaControl.values();
    @DexIgnore
    public static /* final */ int DEFAULT_ARTWORK_SIZE; // = 200;
    @DexIgnore
    public static /* final */ MusicTrackInfo EMPTY_TRACK; // = new com.navdy.service.library.events.audio.MusicTrackInfo.Builder().playbackState(com.navdy.service.library.events.audio.MusicPlaybackState.PLAYBACK_NONE).name(com.navdy.hud.app.HudApplication.getAppContext().getString(com.navdy.hud.app.R.string.music_init_title)).author("").album("").isPreviousAllowed(java.lang.Boolean.valueOf(true)).isNextAllowed(java.lang.Boolean.valueOf(true)).build();
    @DexIgnore
    public static /* final */ long PAUSED_DUE_TO_HFP_EXPIRY_TIME; // = java.util.concurrent.TimeUnit.MINUTES.toMillis(10);
    @DexIgnore
    public static /* final */ String PREFERENCE_MUSIC_CACHE_COLLECTION_SOURCE; // = "PREFERENCE_MUSIC_CACHE_COLLECTION_SOURCE";
    @DexIgnore
    public static /* final */ String PREFERENCE_MUSIC_CACHE_PROFILE; // = "PREFERENCE_MUSIC_CACHE_PROFILE";
    @DexIgnore
    public static /* final */ String PREFERENCE_MUSIC_CACHE_SERIAL; // = "PREFERENCE_MUSIC_CACHE_SERIAL";
    @DexIgnore
    private static /* final */ int PROGRESS_BAR_UPDATE_ON_ANDROID; // = 2000;
    @DexIgnore
    public static /* final */ int THRESHOLD_TIME_BETWEEN_PAUSE_AND_POSSIBLE_CAUSE_FOR_HFP; // = 10000;
    @DexIgnore
    private static /* final */ Logger sLogger; // = new com.navdy.service.library.log.Logger(com.navdy.hud.app.manager.MusicManager.class);
    @DexIgnore
    private boolean acceptingResumes; // = false;
    @DexIgnore
    private MusicArtworkCache artworkCache;
    @DexIgnore
    private Bus bus;
    @DexIgnore
    private boolean clientSupportsArtworkCaching;
    @DexIgnore
    private Set<MediaControl> currentControls;
    @DexIgnore
    private ByteString currentPhoto;
    @DexIgnore
    private int currentPosition;
    /* access modifiers changed from: private */
    // @android.support.annotation.NonNull
    @DexIgnore
    public MusicTrackInfo currentTrack;
    @DexIgnore
    private Handler handler; // = new android.os.Handler();
    @DexIgnore
    private volatile long interestingEventTime;
    @DexIgnore
    private AtomicBoolean isLongPress; // = new java.util.concurrent.atomic.AtomicBoolean(false);
    @DexIgnore
    private boolean isMusicLibraryCachingEnabled; // = false;
    @DexIgnore
    private volatile long lastPausedTime;
    @DexIgnore
    private long lastTrackUpdateTime;
    @DexIgnore
    private MusicCapabilitiesResponse musicCapabilities;
    @DexIgnore
    private Map<MusicCollectionSource, List<MusicCollectionType>> musicCapabilityTypeMap;
    @DexIgnore
    private MessageCache<MusicCollectionResponse> musicCollectionResponseMessageCache;
    @DexIgnore
    private String musicMenuPath; // = null;
    @DexIgnore
    private MusicNotification musicNotification;
    @DexIgnore
    private Set<MusicUpdateListener> musicUpdateListeners; // = new java.util.HashSet();
    @DexIgnore
    private /* final */ Runnable openNotificationRunnable; // = new com.navdy.hud.app.manager.MusicManager.Anon3();
    @DexIgnore
    private PandoraManager pandoraManager;
    @DexIgnore
    private boolean pausedByUs; // = false;
    @DexIgnore
    private volatile long pausedDueToHfpDetectedTime; // = 0;
    // @android.support.annotation.NonNull
    @DexIgnore
    private MusicDataSource previousDataSource; // = com.navdy.service.library.events.audio.MusicDataSource.MUSIC_SOURCE_NONE;
    @DexIgnore
    private Runnable progressBarUpdater; // = new com.navdy.hud.app.manager.MusicManager.Anon2();
    @DexIgnore
    private UIStateManager uiStateManager;
    @DexIgnore
    private boolean updateListeners; // = true;
    @DexIgnore
    private volatile boolean wasPossiblyPausedDueToHFP; // = false;

    @DexAdd
    private static boolean glanceArtFadeFromBlack = SettingsManager.global.getBoolean("persist.sys.music_glance_fade_black", true);

    // @DexIgnore
    // class Anon1 implements java.lang.Runnable {
    //     final /* synthetic */ com.navdy.service.library.events.audio.MusicTrackInfo val$trackInfo;
    //
    //     /* renamed from: com.navdy.hud.app.manager.MusicManager$Anon1$Anon1 reason: collision with other inner class name */
    //     class C0013Anon1 implements com.navdy.hud.app.util.MusicArtworkCache.Callback {
    //         C0013Anon1() {
    //         }
    //
    //         public void onHit(@android.support.annotation.NonNull byte[] artwork) {
    //             com.navdy.hud.app.manager.MusicManager.sLogger.d("CACHE HIT");
    //             com.navdy.hud.app.manager.MusicManager.this.currentPhoto = okio.ByteString.of(artwork);
    //             com.navdy.hud.app.manager.MusicManager.this.callAlbumArtCallbacks();
    //         }
    //
    //         public void onMiss() {
    //             com.navdy.hud.app.manager.MusicManager.sLogger.d("CACHE MISS");
    //             com.navdy.service.library.events.audio.MusicArtworkRequest musicArtworkRequest = new com.navdy.service.library.events.audio.MusicArtworkRequest.Builder().name(com.navdy.hud.app.manager.MusicManager.Anon1.this.val$trackInfo.name).album(com.navdy.hud.app.manager.MusicManager.Anon1.this.val$trackInfo.album).author(com.navdy.hud.app.manager.MusicManager.Anon1.this.val$trackInfo.author).size(java.lang.Integer.valueOf(200)).build();
    //             com.navdy.hud.app.manager.MusicManager.sLogger.d("requesting artwork: " + musicArtworkRequest);
    //             com.navdy.hud.app.manager.MusicManager.this.bus.post(new com.navdy.hud.app.event.RemoteEvent(musicArtworkRequest));
    //         }
    //     }
    //
    //     @DexIgnore
    //     Anon1(com.navdy.service.library.events.audio.MusicTrackInfo musicTrackInfo) {
    //         this.val$trackInfo = musicTrackInfo;
    //     }
    //
    //     @DexIgnore
    //     public void run() {
    //         com.navdy.hud.app.manager.MusicManager.this.artworkCache.getArtwork(this.val$trackInfo.author, this.val$trackInfo.album, this.val$trackInfo.name, new com.navdy.hud.app.manager.MusicManager.Anon1.C0013Anon1());
    //     }
    // }

    // @DexIgnore
    // class Anon2 implements java.lang.Runnable {
    //     @DexIgnore
    //     Anon2() {
    //     }
    //
    //     @DexIgnore
    //     public void run() {
    //         if (com.navdy.service.library.events.audio.MusicPlaybackState.PLAYBACK_PLAYING.equals(com.navdy.hud.app.manager.MusicManager.this.currentTrack.playbackState) && com.navdy.hud.app.manager.MusicManager.this.currentTrack.currentPosition != null) {
    //             com.navdy.hud.app.manager.MusicManager.this.currentPosition = com.navdy.hud.app.manager.MusicManager.this.currentTrack.currentPosition.intValue() + ((int) (android.os.SystemClock.elapsedRealtime() - com.navdy.hud.app.manager.MusicManager.this.lastTrackUpdateTime));
    //             com.navdy.hud.app.manager.MusicManager.this.musicNotification.updateProgressBar(com.navdy.hud.app.manager.MusicManager.this.currentPosition);
    //             com.navdy.hud.app.manager.MusicManager.this.handler.postDelayed(com.navdy.hud.app.manager.MusicManager.this.progressBarUpdater, 2000);
    //         }
    //     }
    // }

    // @DexIgnore
    // class Anon3 implements java.lang.Runnable {
    //     @DexIgnore
    //     Anon3() {
    //     }
    //
    //     @DexIgnore
    //     public void run() {
    //         com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().addNotification(com.navdy.hud.app.manager.MusicManager.this.musicNotification, com.navdy.hud.app.manager.MusicManager.this.uiStateManager.getMainScreensSet());
    //     }
    // }

    // @DexIgnore
    // class Anon4 implements java.lang.Runnable {
    //     final /* synthetic */ com.navdy.hud.app.manager.MusicManager.MusicUpdateListener val$listener;
    //
    //     class Anon1 implements java.lang.Runnable {
    //         Anon1() {
    //         }
    //
    //         public void run() {
    //             com.navdy.hud.app.manager.MusicManager.Anon4.this.val$listener.onAlbumArtUpdate(com.navdy.hud.app.manager.MusicManager.this.currentPhoto, false);
    //             com.navdy.hud.app.manager.MusicManager.Anon4.this.val$listener.onTrackUpdated(com.navdy.hud.app.manager.MusicManager.this.getCurrentTrack(), com.navdy.hud.app.manager.MusicManager.this.getCurrentControls(), false);
    //         }
    //     }
    //
    //     @DexIgnore
    //     Anon4(com.navdy.hud.app.manager.MusicManager.MusicUpdateListener musicUpdateListener) {
    //         this.val$listener = musicUpdateListener;
    //     }
    //
    //     @DexIgnore
    //     public void run() {
    //         com.navdy.hud.app.manager.MusicManager.this.handler.post(new com.navdy.hud.app.manager.MusicManager.Anon4.Anon1());
    //         com.navdy.hud.app.manager.MusicManager.sLogger.v("album art listeners:");
    //         for (com.navdy.hud.app.manager.MusicManager.MusicUpdateListener l : com.navdy.hud.app.manager.MusicManager.this.musicUpdateListeners) {
    //             com.navdy.hud.app.manager.MusicManager.sLogger.d("- " + l);
    //         }
    //     }
    // }

    // @DexIgnore
    // class Anon5 implements java.lang.Runnable {
    //
    //     class Anon1 implements java.lang.Runnable {
    //         Anon1() {
    //         }
    //
    //         public void run() {
    //             for (com.navdy.hud.app.manager.MusicManager.MusicUpdateListener listener : com.navdy.hud.app.manager.MusicManager.this.musicUpdateListeners) {
    //                 listener.onAlbumArtUpdate(com.navdy.hud.app.manager.MusicManager.this.currentPhoto, true);
    //             }
    //         }
    //     }
    //
    //     @DexIgnore
    //     Anon5() {
    //     }
    //
    //     @DexIgnore
    //     public void run() {
    //         com.navdy.hud.app.manager.MusicManager.this.handler.post(new com.navdy.hud.app.manager.MusicManager.Anon5.Anon1());
    //     }
    // }

    // @DexIgnore
    // class Anon6 implements java.lang.Runnable {
    //     final /* synthetic */ com.navdy.service.library.events.input.MediaRemoteKey val$key;
    //
    //     @DexIgnore
    //     Anon6(com.navdy.service.library.events.input.MediaRemoteKey mediaRemoteKey) {
    //         this.val$key = mediaRemoteKey;
    //     }
    //
    //     @DexIgnore
    //     public void run() {
    //         com.navdy.hud.app.manager.MusicManager.this.bus.post(new com.navdy.hud.app.event.RemoteEvent(new com.navdy.service.library.events.input.MediaRemoteKeyEvent(this.val$key, com.navdy.service.library.events.input.KeyEvent.KEY_UP)));
    //     }
    // }

    @DexIgnore
    public enum MediaControl {
        PLAY,
        PAUSE,
        NEXT,
        PREVIOUS,
        MUSIC_MENU,
        MUSIC_MENU_DEEP
    }

    @DexIgnore
    public interface MusicUpdateListener {
        void onAlbumArtUpdate(@Nullable ByteString byteString, boolean z);

        void onTrackUpdated(MusicTrackInfo musicTrackInfo, Set<MediaControl> set, boolean z);
    }

    @DexIgnore
    public MusicManager(MessageCache<MusicCollectionResponse> musicCollectionResponseMessageCache2, Bus bus2, Resources resources, UIStateManager uiStateManager2, PandoraManager pandoraManager2, MusicArtworkCache artworkCache2) {
        this.musicCollectionResponseMessageCache = musicCollectionResponseMessageCache2;
        this.bus = bus2;
        this.uiStateManager = uiStateManager2;
        this.pandoraManager = pandoraManager2;
        this.currentTrack = EMPTY_TRACK;
        this.currentControls = new HashSet<>();
        this.currentControls.add(MediaControl.PLAY);
        this.musicNotification = new MusicNotification(this, bus2);
        this.artworkCache = artworkCache2;
    }

    @DexIgnore
    public MusicTrackInfo getCurrentTrack() {
        return this.currentTrack;
    }

    @DexIgnore
    public Set<MediaControl> getCurrentControls() {
        return this.currentControls;
    }

    @DexIgnore
    public int getCurrentPosition() {
        return this.currentPosition;
    }

    @DexIgnore
    public static boolean tryingToPlay(MusicPlaybackState playbackState) {
        return !MusicPlaybackState.PLAYBACK_NONE.equals(playbackState) && !MusicPlaybackState.PLAYBACK_CONNECTING.equals(playbackState) && !MusicPlaybackState.PLAYBACK_ERROR.equals(playbackState) && !MusicPlaybackState.PLAYBACK_PAUSED.equals(playbackState) && !MusicPlaybackState.PLAYBACK_STOPPED.equals(playbackState);
    }

    // @com.squareup.otto.Subscribe
    @Subscribe
    @DexReplace
    public void onTrackInfo(final MusicTrackInfo currentTrack) {
        MusicManager.sLogger.d("updateState " + currentTrack);
        final boolean is_playing = MusicPlaybackState.PLAYBACK_PLAYING.equals(currentTrack.playbackState);
        final boolean was_playing = MusicPlaybackState.PLAYBACK_PLAYING.equals(this.currentTrack.playbackState);
        if (currentTrack.collectionSource == MusicCollectionSource.COLLECTION_SOURCE_UNKNOWN) {
            this.setMusicMenuPath(null);
        }
        if (!is_playing && was_playing && !MusicDataSource.MUSIC_SOURCE_PANDORA_API.equals(this.currentTrack.dataSource) && MusicDataSource.MUSIC_SOURCE_PANDORA_API.equals(currentTrack.dataSource)) {
            MusicManager.sLogger.d("Ignoring update from non playing Pandora");
        }
        else {

//            int new_track;
//            if (!TextUtils.equals(MusicDataUtils.songIdentifierFromTrackInfo(currentTrack), MusicDataUtils.songIdentifierFromTrackInfo(this.currentTrack))) {
//                new_track = 1;
//            }
//            else {
//                new_track = 0;
//            }
            boolean new_track;
            new_track = !TextUtils.equals(MusicDataUtils.songIdentifierFromTrackInfo(currentTrack), MusicDataUtils.songIdentifierFromTrackInfo(this.currentTrack));
            final boolean playing_new_track = is_playing && new_track;
            boolean new_album = !TextUtils.equals(currentTrack.album, this.currentTrack.album);
            if (playing_new_track && new_album && glanceArtFadeFromBlack) {
                // clear the notification artwork ahead of new one coming in
                this.musicNotification.clearImage();
            }


            if (new_track && this.clientSupportsArtworkCaching) {
                MusicManager.this.requestArtwork(currentTrack);

                //Add delay for Spotify, force reloading the album art and save to cache
                final Handler delayHandler = new Handler();
                delayHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        MusicManager.this.requestArtwork(currentTrack, true);
                    }
                }, 500);
            }
            final boolean do_playing = is_playing && !was_playing;
            final boolean is_paused = MusicPlaybackState.PLAYBACK_PAUSED.equals(currentTrack.playbackState);
            final boolean was_paused = MusicPlaybackState.PLAYBACK_PAUSED.equals(this.currentTrack.playbackState);
            boolean do_paused = is_paused && !was_paused;
            if ((do_playing || do_paused) && DeviceInfo.Platform.PLATFORM_iOS.equals(RemoteDeviceManager.getInstance().getRemoteDevicePlatform())) {
                this.bus.post(new RemoteEvent(currentTrack));
            }
            if (do_playing) {
                MusicManager.sLogger.d("Music started playing");
            }
            if (do_playing && this.pausedByUs) {
                MusicManager.sLogger.d("User started playback after we paused - setting pauseByUs = false");
                this.pausedByUs = false;
            }
            if (do_paused) {
                MusicManager.sLogger.d("New state is paused");
                final long elapsedRealtime = SystemClock.elapsedRealtime();
                if (!this.wasPossiblyPausedDueToHFP) {
                    if (elapsedRealtime - this.interestingEventTime < 10000L) {
                        MusicManager.sLogger.d("Interesting event happened just before the song was paused, possibly playing through HFP");
                        this.wasPossiblyPausedDueToHFP = true;
                        this.pausedDueToHfpDetectedTime = elapsedRealtime;
                    }
                    else {
                        this.lastPausedTime = elapsedRealtime;
                    }
                }
            }
            this.lastTrackUpdateTime = SystemClock.elapsedRealtime();
            boolean suppress_notif = do_playing && this.wasPossiblyPausedDueToHFP && this.lastTrackUpdateTime - this.pausedDueToHfpDetectedTime < MusicManager.PAUSED_DUE_TO_HFP_EXPIRY_TIME;
            if (suppress_notif) {
                MusicManager.sLogger.d("Should suppress Notification");
            }
            if (suppress_notif || this.lastTrackUpdateTime - this.pausedDueToHfpDetectedTime > MusicManager.PAUSED_DUE_TO_HFP_EXPIRY_TIME) {
                this.wasPossiblyPausedDueToHFP = false;
                this.pausedDueToHfpDetectedTime = 0L;
            }
            if (currentTrack.dataSource != null && !MusicDataSource.MUSIC_SOURCE_NONE.equals(currentTrack.dataSource)) {
                this.previousDataSource = currentTrack.dataSource;
            }
            this.updateControls(currentTrack);
            boolean b5 = currentTrack.duration != null && currentTrack.duration > 0 && currentTrack.currentPosition != null;
            if (b5) {
                this.currentPosition = currentTrack.currentPosition;
                this.musicNotification.updateProgressBar(this.currentPosition);
            }
            if (DeviceInfo.Platform.PLATFORM_Android.equals(RemoteDeviceManager.getInstance().getRemoteDevicePlatform())) {
                this.handler.removeCallbacks(this.progressBarUpdater);
                if (MusicPlaybackState.PLAYBACK_PLAYING.equals(currentTrack.playbackState) && b5) {
                    this.handler.postDelayed(this.progressBarUpdater, 2000L);
                }
            }
            int n2;
            if (MusicPlaybackState.PLAYBACK_PAUSED.equals(this.currentTrack.playbackState) || MusicPlaybackState.PLAYBACK_NONE.equals(this.currentTrack.playbackState)) {
                n2 = 1;
            }
            else {
                n2 = 0;
            }
            int n3;
            if (do_paused && !new_track) {
                n3 = 1;
            }
            else {
                n3 = 0;
            }
            int n4;
            if (b5 && currentTrack.currentPosition.equals(currentTrack.duration)) {
                n4 = 1;
            }
            else {
                n4 = 0;
            }
            int n5;
            if (is_paused && n4 == 0 && (n3 != 0 || (was_paused && new_track) || n2 != 0)) {
                n5 = 1;
            }
            else {
                n5 = 0;
            }
            int n6;
            if (this.currentTrack.shuffleMode != currentTrack.shuffleMode) {
                n6 = 1;
            }
            else {
                n6 = 0;
            }
            this.updateListeners = (is_playing || n5 != 0 || n6 != 0);
            if (this.updateListeners) {
                this.currentTrack = currentTrack;
                boolean b6 = playing_new_track || do_playing;
                final boolean showGlance = !suppress_notif && GlanceHelper.isMusicNotificationEnabled() && b6;
                if (b6 || n5 != 0 || n6 != 0) {
                    this.callTrackUpdateCallbacks(showGlance);
                    if (!this.clientSupportsArtworkCaching) {
                        this.callAlbumArtCallbacks(null);
                    }
                }
                if (showGlance) {
                    MusicManager.sLogger.d("Showing notification New Song ? : " + playing_new_track + ", New state playing : " + do_playing);
                    this.handler.removeCallbacks(this.openNotificationRunnable);
                    this.handler.post(this.openNotificationRunnable);
                }
            }
            else {
                MusicManager.sLogger.d("Ignoring updates");
            }
        }
    }

    @DexIgnore
    public boolean isShuffling() {
        return this.currentTrack.shuffleMode != null && this.currentTrack.shuffleMode != MusicShuffleMode.MUSIC_SHUFFLE_MODE_UNKNOWN && this.currentTrack.shuffleMode != MusicShuffleMode.MUSIC_SHUFFLE_MODE_OFF;
    }

    // @DexIgnore
    // private void requestArtwork(com.navdy.service.library.events.audio.MusicTrackInfo trackInfo) {
    //     sLogger.d("requestArtwork: " + trackInfo);
    //     if (!android.text.TextUtils.isEmpty(trackInfo.author) || !android.text.TextUtils.isEmpty(trackInfo.album) || !android.text.TextUtils.isEmpty(trackInfo.name)) {
    //         com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.manager.MusicManager.Anon1(trackInfo), 22);
    //         return;
    //     }
    //     this.currentPhoto = null;
    //     callAlbumArtCallbacks();
    //     sLogger.d("Empty track, returning");
    // }
    @DexReplace
    private void requestArtwork(final MusicTrackInfo musicTrackInfo) {
        this.requestArtwork(musicTrackInfo, false);
    }

    @DexAdd
    void requestArtwork(final MusicTrackInfo musicTrackInfo, final boolean forceReload) {
        MusicManager.sLogger.d("requestArtwork: " + musicTrackInfo);
        if (TextUtils.isEmpty(musicTrackInfo.author) && TextUtils.isEmpty(musicTrackInfo.album) && TextUtils.isEmpty(musicTrackInfo.name)) {
            this.currentPhoto = null;
            this.callAlbumArtCallbacks(null);
            MusicManager.sLogger.d("Empty track, returning");
        }
        else {
            TaskManager.getInstance().execute(new Runnable() {
                @Override
                public void run() {
                    MusicManager.this.artworkCache.getArtwork(musicTrackInfo.author, musicTrackInfo.album, musicTrackInfo.name, new MusicArtworkCache.Callback() {
                        @Override
                        public void onHit(@NonNull final byte[] array) {
                            if (forceReload){
                                onMiss();
                                return;
                            }
                            MusicManager.sLogger.d("CACHE HIT");
                            MusicManager.this.currentPhoto = ByteString.of(array);
                            MusicManager.this.callAlbumArtCallbacks(null);
                        }

                        @Override
                        public void onMiss() {
                            MusicManager.sLogger.d("CACHE MISS");
                            final MusicArtworkRequest build = new MusicArtworkRequest.Builder().name(musicTrackInfo.name).album(musicTrackInfo.album).author(musicTrackInfo.author).size(DEFAULT_ARTWORK_SIZE).build();
                            MusicManager.sLogger.d("requesting artwork: " + build);
                            MusicManager.this.bus.post(new RemoteEvent(build));
                        }
                    });
                }
            }, 22);
        }
    }

    // @com.squareup.otto.Subscribe
    @DexIgnore
    public void onMusicArtworkResponse(MusicArtworkResponse musicArtworkResponse) {
        sLogger.d("onMusicArtworkResponse " + musicArtworkResponse);
        if (musicArtworkResponse == null || musicArtworkResponse.photo == null) {
            this.currentPhoto = null;
        } else {
            cacheAlbumArt(musicArtworkResponse);
            if (TextUtils.equals(MusicDataUtils.songIdentifierFromTrackInfo(this.currentTrack), MusicDataUtils.songIdentifierFromArtworkResponse(musicArtworkResponse))) {
                this.currentPhoto = musicArtworkResponse.photo;
            }
        }
        callAlbumArtCallbacks();
    }

    @DexIgnore
    private void updateControls(MusicTrackInfo trackInfo) {
        this.currentControls = new HashSet<>();
        if (Boolean.TRUE.equals(trackInfo.isPreviousAllowed)) {
            this.currentControls.add(MediaControl.PREVIOUS);
        }
        if (tryingToPlay(trackInfo.playbackState)) {
            this.currentControls.add(MediaControl.PAUSE);
        } else {
            this.currentControls.add(MediaControl.PLAY);
        }
        if (Boolean.TRUE.equals(trackInfo.isNextAllowed)) {
            this.currentControls.add(MediaControl.NEXT);
        }
    }

    @DexIgnore
    public void handleKeyEvent(android.view.KeyEvent event, MediaControl control, boolean isKeyPressedDown) {
        Platform remotePlatform = RemoteDeviceManager.getInstance().getRemoteDevicePlatform();
        if (remotePlatform != null && InputManager.isCenterKey(event.getKeyCode())) {
            recordAnalytics(event, control, isKeyPressedDown);
            switch (remotePlatform) {
                case PLATFORM_Android:
                    switch (event.getAction()) {
                        case 0:
                            if (event.isLongPress() && this.isLongPress.compareAndSet(false, true)) {
                                switch (control) {
                                    case NEXT:
                                        if (canSeekBackward()) {
                                            runAndroidAction(Action.MUSIC_ACTION_FAST_FORWARD_START);
                                            return;
                                        } else {
                                            sLogger.w("Fast-forward is not allowed");
                                            return;
                                        }
                                    case PREVIOUS:
                                        if (canSeekForward()) {
                                            runAndroidAction(Action.MUSIC_ACTION_REWIND_START);
                                            return;
                                        } else {
                                            sLogger.w("Rewind is not allowed");
                                            return;
                                        }
                                    default:
                                        return;
                                }
                            } else {
                                return;
                            }
                        case 1:
                            if (!isKeyPressedDown) {
                                return;
                            }
                            if (this.isLongPress.compareAndSet(true, false)) {
                                switch (control) {
                                    case NEXT:
                                        runAndroidAction(Action.MUSIC_ACTION_FAST_FORWARD_STOP);
                                        return;
                                    case PREVIOUS:
                                        runAndroidAction(Action.MUSIC_ACTION_REWIND_STOP);
                                        return;
                                    default:
                                        return;
                                }
                            } else {
                                switch (control) {
                                    case NEXT:
                                        if (Boolean.TRUE.equals(this.currentTrack.isNextAllowed)) {
                                            runAndroidAction(Action.MUSIC_ACTION_NEXT);
                                            return;
                                        } else {
                                            sLogger.w("Next song action is not allowed");
                                            return;
                                        }
                                    case PREVIOUS:
                                        if (Boolean.TRUE.equals(this.currentTrack.isPreviousAllowed)) {
                                            runAndroidAction(Action.MUSIC_ACTION_PREVIOUS);
                                            return;
                                        } else {
                                            sLogger.w("Previous song action is not allowed");
                                            return;
                                        }
                                    default:
                                        return;
                                }
                            }
                        default:
                            return;
                    }
                case PLATFORM_iOS:
                    switch (event.getAction()) {
                        case 0:
                            if (isKeyPressedDown) {
                                return;
                            }
                            if (control == MediaControl.PLAY || control == MediaControl.PAUSE) {
                                sendMediaKeyEvent(control, true);
                                sendMediaKeyEvent(control, false);
                                return;
                            }
                            sendMediaKeyEvent(control, true);
                            return;
                        case 1:
                            if (isKeyPressedDown) {
                                sendMediaKeyEvent(control, false);
                                return;
                            }
                            return;
                        default:
                            return;
                    }
                default:
                    return;
            }
        }
    }

    @DexIgnore
    public void executeMediaControl(MediaControl control, boolean isKeyPressedDown) {
        Platform remotePlatform = RemoteDeviceManager.getInstance().getRemoteDevicePlatform();
        if (remotePlatform != null) {
            recordAnalytics(null, control, isKeyPressedDown);
            switch (remotePlatform) {
                case PLATFORM_Android:
                    switch (control) {
                        case PLAY:
                            runAndroidAction(Action.MUSIC_ACTION_PLAY);
                            return;
                        case PAUSE:
                            runAndroidAction(Action.MUSIC_ACTION_PAUSE);
                            return;
                        default:
                            return;
                    }
                case PLATFORM_iOS:
                    sendMediaKeyEvent(control, true);
                    sendMediaKeyEvent(control, false);
                    return;
                default:
                    return;
            }
        }
    }

    @DexIgnore
    private void recordAnalytics(@Nullable android.view.KeyEvent event, MediaControl control, boolean isKeyPressedDown) {
        // if (event == null) {
        //     switch (control) {
        //         case PLAY:
        //             AnalyticsSupport.recordMusicAction("Play");
        //             return;
        //         case PAUSE:
        //             AnalyticsSupport.recordMusicAction("Pause");
        //             return;
        //         default:
        //             return;
        //     }
        // } else {
        //     switch (event.getAction()) {
        //         case 0:
        //             if (event.isLongPress() && !this.isLongPress.get()) {
        //                 switch (control) {
        //                     case NEXT:
        //                         AnalyticsSupport.recordMusicAction("Fast-forward");
        //                         return;
        //                     case PREVIOUS:
        //                         AnalyticsSupport.recordMusicAction("Rewind");
        //                         return;
        //                     default:
        //                         return;
        //                 }
        //             } else {
        //                 return;
        //             }
        //         case 1:
        //             if (isKeyPressedDown && !this.isLongPress.get()) {
        //                 switch (control) {
        //                     case NEXT:
        //                         AnalyticsSupport.recordMusicAction("Next");
        //                         return;
        //                     case PREVIOUS:
        //                         AnalyticsSupport.recordMusicAction("Previous");
        //                         return;
        //                     default:
        //                         return;
        //                 }
        //             } else {
        //                 return;
        //             }
        //         default:
        //             return;
        //     }
        // }
    }

    @DexIgnore
    private boolean canSeekForward() {
        return Boolean.TRUE.equals(this.currentTrack.isNextAllowed) && !Boolean.TRUE.equals(this.currentTrack.isOnlineStream);
    }

    @DexIgnore
    private boolean canSeekBackward() {
        return Boolean.TRUE.equals(this.currentTrack.isPreviousAllowed) && !Boolean.TRUE.equals(this.currentTrack.isOnlineStream);
    }

    @DexIgnore
    private boolean runAndroidAction(Action action) {
        MusicDataSource targetDataSource = dataSourceToUse();
        if (!Action.MUSIC_ACTION_PLAY.equals(action) || !MusicDataSource.MUSIC_SOURCE_PANDORA_API.equals(targetDataSource)) {
            sLogger.d("Media action: " + action);
            this.bus.post(new RemoteEvent(new com.navdy.service.library.events.audio.MusicEvent.Builder().action(action).dataSource(targetDataSource).build()));
        } else {
            this.pandoraManager.startAndPlay();
        }
        return true;
    }

    @DexIgnore
    private MusicDataSource dataSourceToUse() {
        MusicTrackInfo currentTrackAvailable = this.currentTrack;
        if (currentTrackAvailable == null || currentTrackAvailable.dataSource == null || MusicDataSource.MUSIC_SOURCE_NONE.equals(currentTrackAvailable.dataSource)) {
            return this.previousDataSource;
        }
        return currentTrackAvailable.dataSource;
    }

    @DexIgnore
    private boolean sendMediaKeyEvent(MediaControl control, boolean down) {
        MediaRemoteKey key = null;
        switch (control) {
            case NEXT:
                key = MediaRemoteKey.MEDIA_REMOTE_KEY_NEXT;
                break;
            case PREVIOUS:
                key = MediaRemoteKey.MEDIA_REMOTE_KEY_PREV;
                break;
            case PLAY:
            case PAUSE:
                key = MediaRemoteKey.MEDIA_REMOTE_KEY_PLAY;
                break;
        }
        this.bus.post(new RemoteEvent(new MediaRemoteKeyEvent.Builder().key(key).action(down ? KeyEvent.KEY_DOWN : KeyEvent.KEY_UP).build()));
        return true;
    }

    @DexIgnore
    public void initialize() {
        this.bus.post(new RemoteEvent(new MusicTrackInfoRequest()));
        sLogger.d("Request for music track info sent to the client.");
    }

    @DexIgnore
    public void addMusicUpdateListener(@NonNull MusicUpdateListener listener) {
        // sLogger.d("addMusicUpdateListener");
        // if (!this.musicUpdateListeners.contains(listener)) {
        //     if (this.musicUpdateListeners.size() == 0) {
        //         sLogger.d("Enabling album art updates (PhotoUpdate)");
        //         requestPhotoUpdates(true);
        //     }
        //     this.musicUpdateListeners.add(listener);
        // } else {
        //     sLogger.w("Tried to add a listener that's already listening");
        // }
        // TaskManager.getInstance().execute(new MusicManager.Anon4(listener), 1);
    }

    @DexIgnore
    public void removeMusicUpdateListener(@NonNull MusicUpdateListener listener) {
        sLogger.d("removeMusicUpdateListener");
        if (this.musicUpdateListeners.contains(listener)) {
            this.musicUpdateListeners.remove(listener);
            if (this.musicUpdateListeners.size() == 0) {
                sLogger.d("Disabling album art updates");
                requestPhotoUpdates(false);
            }
        } else {
            sLogger.w("Tried to remove a non-existent listener");
        }
        sLogger.d("album art listeners:");
        for (MusicUpdateListener l : this.musicUpdateListeners) {
            sLogger.v("- " + l);
        }
    }

    @DexIgnore
    private void requestPhotoUpdates(boolean start) {
        if (!start || !this.clientSupportsArtworkCaching) {
            this.bus.post(new RemoteEvent(new com.navdy.service.library.events.photo.PhotoUpdatesRequest.Builder().start(Boolean.valueOf(start)).photoType(PhotoType.PHOTO_ALBUM_ART).build()));
        } else {
            sLogger.d("client supports caching, no need to request photo updates");
        }
    }

    // @com.squareup.otto.Subscribe
    @DexIgnore
    public void onPhotoUpdate(PhotoUpdate photoUpdate) {
        sLogger.d("onPhotoUpdate " + photoUpdate);
        if (photoUpdate == null || photoUpdate.photo == null) {
            this.currentPhoto = null;
        } else {
            this.currentPhoto = photoUpdate.photo;
            String currentTrackPhotoId = MusicDataUtils.songIdentifierFromTrackInfo(this.currentTrack);
            if (TextUtils.equals(currentTrackPhotoId, photoUpdate.identifier)) {
                cacheAlbumArt(this.currentPhoto.toByteArray(), this.currentTrack);
            } else {
                sLogger.w("Received PhotoUpdate with wrong identifier: " + currentTrackPhotoId + " != " + photoUpdate.identifier);
            }
        }
        callAlbumArtCallbacks();
    }

    @DexIgnore
    private void callTrackUpdateCallbacks(boolean willOpenNotification) {
        if (this.updateListeners) {
            sLogger.d("callTrackUpdateCallbacks");
            for (MusicUpdateListener listener : this.musicUpdateListeners) {
                listener.onTrackUpdated(this.currentTrack, this.currentControls, willOpenNotification);
            }
            return;
        }
        sLogger.w("callTrackUpdateCallbacks but updateListeners was false");
    }

    @DexIgnore
    private void callAlbumArtCallbacks() {
        // if (this.updateListeners) {
        //     sLogger.d("callAlbumArtCallbacks");
        //     TaskManager.getInstance().execute(new MusicManager.Anon5(), 1);
        //     return;
        // }
        // sLogger.w("callAlbumArtCallbacks but updateListeners was false");
    }

    @DexAdd
    void callAlbumArtCallbacks(Object tag) {
        callAlbumArtCallbacks();
    }

    @DexIgnore
    public void showMusicNotification() {
        NotificationManager.getInstance().addNotification(this.musicNotification);
    }

    @DexIgnore
    public boolean isPandoraDataSource() {
        return MusicDataSource.MUSIC_SOURCE_PANDORA_API.equals(this.currentTrack.dataSource);
    }

    // @com.squareup.otto.Subscribe
    @DexIgnore
    public void onPhoneEvent(PhoneEvent phoneEvent) {
        if (phoneEvent != null && phoneEvent.status != null) {
            sLogger.d("PhoneEvent : new status " + phoneEvent.status);
            if (phoneEvent.status == PhoneStatus.PHONE_DIALING || phoneEvent.status == PhoneStatus.PHONE_RINGING || phoneEvent.status == PhoneStatus.PHONE_OFFHOOK) {
                long currentTime = SystemClock.elapsedRealtime();
                if (this.wasPossiblyPausedDueToHFP) {
                    return;
                }
                if (this.currentTrack == null || !this.currentTrack.playbackState.equals(MusicPlaybackState.PLAYBACK_PAUSED) || this.lastPausedTime - currentTime >= 10000) {
                    sLogger.d("Recording the phone event as interesting event");
                    this.interestingEventTime = currentTime;
                    return;
                }
                sLogger.d("Current state is paused recently, possibly due to call");
                this.wasPossiblyPausedDueToHFP = true;
                this.pausedDueToHfpDetectedTime = currentTime;
            }
        }
    }

    // @com.squareup.otto.Subscribe
    @DexIgnore
    public void onLocalSpeechRequest(LocalSpeechRequest localSpeechRequest) {
        long currentTime = SystemClock.elapsedRealtime();
        if (!this.wasPossiblyPausedDueToHFP) {
            sLogger.d("Recording the speech request as an interesting event");
            this.interestingEventTime = currentTime;
        }
    }

    // @com.squareup.otto.Subscribe
    @DexIgnore
    public void onDriverProfileChanged(DriverProfileChanged driverProfileChanged) {
        sLogger.d("onDriverProfileChanged - " + driverProfileChanged);
        if (!DriverProfileHelper.getInstance().getDriverProfileManager().getCurrentProfile().isDefaultProfile() && this.musicUpdateListeners.size() > 0) {
            requestPhotoUpdates(true);
        }
    }

    // @com.squareup.otto.Subscribe
    @DexIgnore
    public void onConnectionStateChange(ConnectionStateChange event) {
        if (event.state == ConnectionState.CONNECTION_DISCONNECTED) {
            NotificationManager.getInstance().removeNotification(this.musicNotification.getId());
            this.currentTrack = EMPTY_TRACK;
            this.currentPhoto = null;
            this.currentControls = null;
            this.updateListeners = true;
            callAlbumArtCallbacks();
            callTrackUpdateCallbacks(false);
            this.musicCapabilities = null;
            this.musicCapabilityTypeMap = null;
            this.musicMenuPath = null;
            MusicMenu2.clearMenuData();
        }
    }

    @DexIgnore
    private void cacheAlbumArt(MusicArtworkResponse musicArtworkResponse) {
        if (musicArtworkResponse.author == null && musicArtworkResponse.album == null && musicArtworkResponse.name == null) {
            sLogger.d("Not a now playing update, returning");
        } else {
            this.artworkCache.putArtwork(musicArtworkResponse.author, musicArtworkResponse.album, musicArtworkResponse.name, musicArtworkResponse.photo.toByteArray());
        }
    }

    @DexIgnore
    private void cacheAlbumArt(byte[] photo, MusicTrackInfo musicTrackInfo) {
        if (musicTrackInfo != null) {
            this.artworkCache.putArtwork(musicTrackInfo.author, musicTrackInfo.album, musicTrackInfo.name, photo);
        }
    }

    // @com.squareup.otto.Subscribe
    @DexIgnore
    public void onDeviceInfo(DeviceInfo deviceInfo) {
        sLogger.d("onDeviceInfo - " + deviceInfo);
        this.bus.post(new RemoteEvent(new Builder().build()));
        Capabilities capabilities = deviceInfo.capabilities;
        if (deviceInfo.platform == Platform.PLATFORM_iOS || (capabilities != null && Wire.get(capabilities.musicArtworkCache, Boolean.valueOf(false)))) {
            this.clientSupportsArtworkCaching = true;
        }
    }

    // @com.squareup.otto.Subscribe
    @DexIgnore
    public void onMusicCollectionSourceUpdate(MusicCollectionSourceUpdate musicCollectionSourceUpdate) {
        sLogger.d("onMusicCollectionSourceUpdate " + musicCollectionSourceUpdate);
        if (musicCollectionSourceUpdate.collectionSource != null && musicCollectionSourceUpdate.serial_number != null) {
            checkCache(musicCollectionSourceUpdate.collectionSource.name(), musicCollectionSourceUpdate.serial_number);
        }
    }

    @DexIgnore
    private void checkCache(String musicCollectionSource, long serialNumber) {
        SharedPreferences sharedPreferences = RemoteDeviceManager.getInstance().getSharedPreferences();
        String profileName = DriverProfileHelper.getInstance().getCurrentProfile().getProfileName();
        String cacheProfileName = sharedPreferences.getString(PREFERENCE_MUSIC_CACHE_PROFILE, null);
        long cacheSerialNumber = sharedPreferences.getLong(PREFERENCE_MUSIC_CACHE_SERIAL, 0);
        String cachedMusicCollectionSource = sharedPreferences.getString(PREFERENCE_MUSIC_CACHE_COLLECTION_SOURCE, null);
        sLogger.d("Current Cache details " + cacheProfileName + ", Source : " + cachedMusicCollectionSource + ", Serial : " + cacheSerialNumber);
        sLogger.d("Current Profile details " + profileName + ", Source : " + musicCollectionSource + ", Serial : " + serialNumber);
        if (!TextUtils.equals(profileName, cacheProfileName) || cacheSerialNumber != serialNumber || !TextUtils.equals(musicCollectionSource, cachedMusicCollectionSource)) {
            sLogger.d("Clearing the Music data cache");
            this.musicCollectionResponseMessageCache.clear();
            sharedPreferences.edit().putString(PREFERENCE_MUSIC_CACHE_PROFILE, profileName).putLong(PREFERENCE_MUSIC_CACHE_SERIAL, serialNumber).putString(PREFERENCE_MUSIC_CACHE_COLLECTION_SOURCE, musicCollectionSource).apply();
        }
    }

    @DexIgnore
    public boolean hasMusicCapabilities() {
        return this.musicCapabilities != null && this.musicCapabilities.capabilities != null && this.musicCapabilities.capabilities.size() != 0;
    }

    @DexIgnore
    public MusicCapabilitiesResponse getMusicCapabilities() {
        return this.musicCapabilities;
    }

    @DexIgnore
    public List<MusicCollectionType> getCollectionTypesForSource(MusicCollectionSource collectionSource) {
        return this.musicCapabilityTypeMap.get(collectionSource);
    }

    // @com.squareup.otto.Subscribe
    @DexIgnore
    public void onMusicCapabilitiesResponse(MusicCapabilitiesResponse musicCapabilitiesResponse) {
        sLogger.d("onMusicCapabilitiesResponse " + musicCapabilitiesResponse);
        this.musicCapabilities = musicCapabilitiesResponse;
        String musicCollectionSource = null;
        this.isMusicLibraryCachingEnabled = false;
        long serialNumber = -1;
        if (hasMusicCapabilities()) {
            this.musicCapabilityTypeMap = new HashMap<>();
            for (MusicCapability capability : this.musicCapabilities.capabilities) {
                this.musicCapabilityTypeMap.put(capability.collectionSource, capability.collectionTypes);
                if (!this.isMusicLibraryCachingEnabled && capability.serial_number != null) {
                    this.isMusicLibraryCachingEnabled = true;
                    musicCollectionSource = capability.collectionSource.name();
                    serialNumber = capability.serial_number;
                    sLogger.d("Enabling caching with Source " + musicCollectionSource + ", Serial :" + serialNumber);
                }
            }
        }
        if (this.isMusicLibraryCachingEnabled) {
            checkCache(musicCollectionSource, serialNumber);
        } else {
            sLogger.d("Cache is not enabled");
        }
    }

    // @com.squareup.otto.Subscribe
    @DexIgnore
    public void onMediaKeyEventFromClient(MediaRemoteKeyEvent mediaRemoteKeyEvent) {
        this.bus.post(new RemoteEvent(mediaRemoteKeyEvent));
    }

    @DexIgnore
    public String getMusicMenuPath() {
        return this.musicMenuPath;
    }

    @DexIgnore
    public void setMusicMenuPath(String musicMenuPath2) {
        sLogger.d("setMusicMenuPath: " + musicMenuPath2);
        this.musicMenuPath = musicMenuPath2;
    }

    @DexIgnore
    public void softPause() {
        if (this.currentTrack.playbackState == MusicPlaybackState.PLAYBACK_PLAYING) {
            sLogger.d("Pausing music");
            this.pausedByUs = true;
            this.acceptingResumes = false;
            postKeyDownUp(MediaRemoteKey.MEDIA_REMOTE_KEY_PLAY);
        }
    }

    @DexIgnore
    public void acceptResumes() {
        this.acceptingResumes = true;
        sLogger.d("Now accepting music resume events");
    }

    // @com.squareup.otto.Subscribe
    @DexIgnore
    public void onResumeMusicRequest(ResumeMusicRequest resumeMusicRequest) {
        softResume();
    }

    @DexIgnore
    public void softResume() {
        if (this.pausedByUs && this.acceptingResumes) {
            postKeyDownUp(MediaRemoteKey.MEDIA_REMOTE_KEY_PLAY);
            this.pausedByUs = false;
            sLogger.d("Unpausing music (resuming)");
        }
    }

    @DexIgnore
    public boolean isMusicLibraryCachingEnabled() {
        return this.isMusicLibraryCachingEnabled;
    }

    @DexIgnore
    private void postKeyDownUp(MediaRemoteKey key) {
        // this.bus.post(new RemoteEvent(new MediaRemoteKeyEvent(key, KeyEvent.KEY_DOWN)));
        // this.handler.postDelayed(new MusicManager.Anon6(key), 100);
    }
}
