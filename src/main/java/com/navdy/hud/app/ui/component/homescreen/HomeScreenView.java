package com.navdy.hud.app.ui.component.homescreen;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.event.DriverProfileChanged;
import com.navdy.hud.app.framework.DriverProfileHelper;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import com.navdy.hud.app.gesture.GestureDetector;
import com.navdy.hud.app.manager.InputManager;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.manager.SpeedManager;
import com.navdy.hud.app.maps.MapEvents;
import com.navdy.hud.app.maps.NavigationMode;
import com.navdy.hud.app.maps.here.HereNavigationManager;
import com.navdy.hud.app.obd.ObdManager;
import com.navdy.hud.app.profile.NavdyPreferences;
import com.navdy.hud.app.screen.BaseScreen;
import com.navdy.hud.app.ui.activity.Main;
import com.navdy.hud.app.ui.component.tbt.TbtViewContainer;
import com.navdy.hud.app.ui.framework.DefaultAnimationListener;
import com.navdy.hud.app.ui.framework.IScreenAnimationListener;
import com.navdy.hud.app.ui.framework.UIStateManager;
import com.navdy.hud.app.view.DashboardWidgetPresenter;
import com.navdy.hud.app.view.DashboardWidgetView;
import com.navdy.hud.app.view.DriveScoreGaugePresenter;
import com.navdy.hud.app.view.ICustomAnimator;
import com.navdy.hud.app.view.MainView;
import com.navdy.hud.app.view.SpeedLimitSignView;
import com.navdy.service.library.events.input.Gesture;
import com.navdy.service.library.events.input.GestureEvent;
import com.navdy.service.library.events.ui.Screen;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Bus;

import java.util.List;

import butterknife.ButterKnife;
import lanchon.dexpatcher.annotation.DexAdd;
import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexReplace;
import mortar.Mortar;

@DexEdit
public class HomeScreenView extends RelativeLayout implements ICustomAnimator, InputManager.IInputHandler, GestureDetector.GestureListener {
    @DexIgnore
    private static /* final */ int MIN_SPEED_LIMIT_VISIBLE_DURATION; // = ((int) java.util.concurrent.TimeUnit.SECONDS.toMillis(3));
    @DexIgnore
    static /* final */ String SYSPROP_ALWAYS_SHOW_SPEED_LIMIT_SIGN; // = "persist.sys.speedlimit.always";
    @DexIgnore
    private static /* final */ List<HomeScreenView.HomeScreenWidgetInfo> editableHomeScreenWidgetInfos; // = new java.util.ArrayList();
    @DexIgnore
    static /* final */ Logger sLogger; // = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.homescreen.HomeScreenView.class);
    @DexIgnore
    private boolean alwaysShowSpeedLimitSign;
    // @javax.inject.Inject
    @DexIgnore
    Bus bus;
    @DexIgnore
    private NavigationMode currentNavigationMode;
    @DexIgnore
    DriveScoreGaugePresenter driveScoreGaugePresenter;
    // @butterknife.InjectView(2131624329)
    @DexIgnore
    DashboardWidgetView driveScoreWidgetView;
    @DexAdd
    private NavdyPreferences driverPreferences;
    // @butterknife.InjectView(2131624332)
    @DexIgnore
    EtaView etaClockView;
    // @javax.inject.Inject
    @DexIgnore
    SharedPreferences globalPreferences;
    @DexIgnore
    private Handler handler;
    @DexIgnore
    private Runnable hideSpeedLimitRunnable;
    @DexIgnore
    private boolean isRecalculating;
    @DexIgnore
    private boolean isTopInit;
    @DexIgnore
    private boolean isTopViewAnimationOut;
    @DexIgnore
    private boolean isTopViewAnimationRunning;
    // @butterknife.InjectView(2131624331)
    @DexIgnore
    ImageView laneGuidanceIconIndicator;
    // @butterknife.InjectView(2131624335)
    @DexIgnore
    LaneGuidanceView laneGuidanceView;
    @DexIgnore
    private long lastSpeedLimitShown;
    // @butterknife.InjectView(2131624326)
    @DexIgnore
    HomeScreenRightSectionView mainscreenRightSection;
    // @butterknife.InjectView(2131624336)
    @DexIgnore
    NavigationView mapContainer;
    // @butterknife.InjectView(2131624121)
    @DexIgnore
    ImageView mapMask;
    // @butterknife.InjectView(2131624327)
    @DexIgnore
    ViewGroup mapViewSpeedContainer;
    @DexIgnore
    private HomeScreen.DisplayMode mode;
    // @butterknife.InjectView(2131624330)
    @DexIgnore
    protected RelativeLayout navigationViewsContainer;
    @DexIgnore
    private NotificationManager notificationManager;
    // @butterknife.InjectView(2131624349)
    @DexIgnore
    OpenRoadView openMapRoadInfoContainer;
    @DexIgnore
    private boolean paused;
    // @javax.inject.Inject
    @DexIgnore
    HomeScreen.Presenter presenter;
    // @butterknife.InjectView(2131624361)
    @DexIgnore
    RecalculatingView recalcRouteContainer;
    @DexIgnore
    private AnimatorSet rightScreenAnimator;
    @DexIgnore
    private IScreenAnimationListener screenAnimationListener;
    @DexIgnore
    private boolean showCollapsedNotif;
    // @butterknife.InjectView(2131624365)
    @DexIgnore
    BaseSmartDashView smartDashContainer;
    @DexIgnore
    private int speedLimit;
    // @butterknife.InjectView(2131624328)
    @DexIgnore
    SpeedLimitSignView speedLimitSignView;
    @DexIgnore
    private SpeedManager speedManager;
    // @butterknife.InjectView(2131624343)
    @DexIgnore
    SpeedView speedView;
    // @butterknife.InjectView(2131624377)
    @DexIgnore
    TbtViewContainer tbtView;
    @DexIgnore
    private Runnable topViewAnimationRunnable;
    @DexIgnore
    private AnimatorSet topViewAnimator;
    @DexIgnore
    private UIStateManager uiStateManager;

    // @DexIgnore
    // class Anon1 implements Runnable {
    //     @DexIgnore
    //     Anon1() {
    //     }
    //
    //     @DexIgnore
    //     public void run() {
    //         HomeScreenView.sLogger.v("out animation");
    //         HomeScreenView.this.animateTopViewsOut();
    //     }
    // }
    //
    // @DexIgnore
    // class Anon10 implements Runnable {
    //     @DexIgnore
    //     Anon10() {
    //     }
    //
    //     @DexIgnore
    //     public void run() {
    //         HomeScreenView.this.setDisplayMode(true);
    //     }
    // }
    //
    // @DexIgnore
    // class Anon11 implements Runnable {
    //     @DexIgnore
    //     Anon11() {
    //     }
    //
    //     @DexIgnore
    //     public void run() {
    //         HomeScreenView.this.updateSpeedLimitSign();
    //     }
    // }
    //
    // @DexIgnore
    // class Anon2 implements IScreenAnimationListener {
    //     @DexIgnore
    //     Anon2() {
    //     }
    //
    //     @DexIgnore
    //     public void onStart(BaseScreen in, BaseScreen out) {
    //     }
    //
    //     @DexIgnore
    //     public void onStop(BaseScreen in, BaseScreen out) {
    //         if (HomeScreenView.this.showCollapsedNotif && in != null && in.getScreen() == Screen.SCREEN_HYBRID_MAP) {
    //             HomeScreenView.this.showCollapsedNotif = false;
    //             HomeScreenView.sLogger.v("expanding collapse notifi");
    //             NotificationManager notificationManager = NotificationManager.getInstance();
    //             if (notificationManager.makeNotificationCurrent(false)) {
    //                 notificationManager.showNotification();
    //             }
    //         }
    //     }
    // }
    //
    // @DexIgnore
    // class Anon3 implements Runnable {
    //     @DexIgnore
    //     Anon3() {
    //     }
    //
    //     @DexIgnore
    //     public void run() {
    //         HomeScreenView.this.hideSpeedLimit();
    //     }
    // }
    //
    // @DexIgnore
    // class Anon4 extends DefaultAnimationListener {
    //     @DexIgnore
    //     Anon4() {
    //     }
    //
    //     @DexIgnore
    //     public void onAnimationEnd(Animator animation) {
    //         HomeScreenView.this.setWidgetVisibility(HomeScreenView.this.etaClockView, false);
    //     }
    // }
    //
    // @DexIgnore
    // class Anon5 extends DefaultAnimationListener {
    //     @DexIgnore
    //     Anon5() {
    //     }
    //
    //     @DexIgnore
    //     public void onAnimationEnd(Animator animation) {
    //         HomeScreenView.this.setWidgetVisibility(HomeScreenView.this.driveScoreWidgetView, false);
    //     }
    // }
    //
    // @DexIgnore
    // class Anon6 extends DefaultAnimationListener {
    //     @DexIgnore
    //     Anon6() {
    //     }
    //
    //     @DexIgnore
    //     public void onAnimationStart(Animator animation) {
    //         HomeScreenView.this.updateClockWidgetsVisibility(true);
    //     }
    //
    //     @DexIgnore
    //     public void onAnimationEnd(Animator animation) {
    //     }
    // }
    //
    // @DexIgnore
    // class Anon7 extends DefaultAnimationListener {
    //     @DexIgnore
    //     Anon7() {
    //     }
    //
    //     @DexIgnore
    //     public void onAnimationEnd(Animator animation) {
    //         HomeScreenView.this.isTopViewAnimationRunning = false;
    //         HomeScreenView.this.isTopViewAnimationOut = true;
    //     }
    // }
    //
    // @DexIgnore
    // class Anon8 extends DefaultAnimationListener {
    //     @DexIgnore
    //     Anon8() {
    //     }
    //
    //     @DexIgnore
    //     public void onAnimationEnd(Animator animation) {
    //         HomeScreenView.this.isTopViewAnimationRunning = false;
    //         HomeScreenView.this.isTopViewAnimationOut = false;
    //         HomeScreenView.this.handler.removeCallbacks(HomeScreenView.this.topViewAnimationRunnable);
    //         HomeScreenView.this.handler.postDelayed(HomeScreenView.this.topViewAnimationRunnable, 10000);
    //     }
    // }
    //
    // @DexIgnore
    // class Anon9 extends DefaultAnimationListener {
    //     @DexIgnore
    //     Anon9() {
    //     }
    //
    //     @DexIgnore
    //     public void onAnimationEnd(Animator animation) {
    //         HomeScreenView.this.mainscreenRightSection.ejectRightSection();
    //     }
    // }

    @DexIgnore
    public interface HomeScreenWidgetConstants {
        String HOME_SCREEN_ALWAYS_SHOW_SPEED_LIMIT_WIDGET = "home_screen_always_show_speed_limit";
        String HOME_SCREEN_DRIVE_SCORE_WIDGET = "home_screen_drive_score_widget";
        String HOME_SCREEN_ETA_WIDGET = "home_screen_eta_widget";
        String HOME_SCREEN_SPEED_LIMIT_WIDGET = "home_screen_speed_limit_widget";
        String HOME_SCREEN_SPEED_WIDGET = "home_screen_speed_widget";
        String HOME_SCREEN_TIME_WIDGET = "home_screen_time_widget";
    }

    @DexIgnore
    public static class HomeScreenWidgetInfo {
        public int id;
        public int labelResourceId;

        @DexIgnore
        public HomeScreenWidgetInfo(int id2, int labelResourceId2) {
            this.id = id2;
            this.labelResourceId = labelResourceId2;
        }
    }

    @DexIgnore
    static class HomeScreenWidgetsPreferenceUpdated {
        @DexIgnore
        HomeScreenWidgetsPreferenceUpdated() {
        }
    }

    /* @DexIgnore
    static {
        editableHomeScreenWidgetInfos.add(new com.navdy.hud.app.ui.component.homescreen.HomeScreenView.HomeScreenWidgetInfo(com.navdy.hud.app.R.id.activeEtaContainer, com.navdy.hud.app.R.string.eta_clock_widget));
        editableHomeScreenWidgetInfos.add(new com.navdy.hud.app.ui.component.homescreen.HomeScreenView.HomeScreenWidgetInfo(com.navdy.hud.app.R.id.drive_score_events, com.navdy.hud.app.R.string.drive_score_widget));
    } */

    @DexIgnore
    public HomeScreenView(Context context) {
        this(context, null);
    }

    @DexIgnore
    public HomeScreenView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    @DexIgnore
    public HomeScreenView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        // this.isTopInit = true;
        // this.speedManager = SpeedManager.getInstance();
        // this.speedLimit = 0;
        // this.alwaysShowSpeedLimitSign = false;
        // this.topViewAnimationRunnable = new HomeScreenView.Anon1();
        // this.handler = new Handler();
        // this.screenAnimationListener = new HomeScreenView.Anon2();
        // this.mode = HomeScreen.DisplayMode.MAP;
        // this.hideSpeedLimitRunnable = new HomeScreenView.Anon3();
        // this.uiStateManager = RemoteDeviceManager.getInstance().getUiStateManager();
        // this.notificationManager = NotificationManager.getInstance();
        // this.alwaysShowSpeedLimitSign = isAlwaysShowSpeedLimitSign();
        // if (!isInEditMode()) {
        //     this.driveScoreGaugePresenter = new DriveScoreGaugePresenter(context, R.layout.drive_score_map_layout, false);
        //     Mortar.inject(context, this);
        // }
    }

    /* access modifiers changed from: protected */
    @DexIgnore
    public void onFinishInflate() {
        super.onFinishInflate();
        LayoutInflater.from(getContext()).inflate(R.layout.screen_home_smartdash, (ViewGroup) findViewById(R.id.smart_dash_place_holder), true);
        ButterKnife.inject(this);
        updateDriverPrefs();
        setDisplayMode(false);
        this.mapMask.setImageResource(R.drawable.map_mask);
        this.mapContainer.init(this);
        this.smartDashContainer.init(this);
        this.recalcRouteContainer.init(this);
        this.tbtView.init(this);
        this.openMapRoadInfoContainer.init(this);
        this.etaClockView.init(this);
        this.laneGuidanceView.init(this);
        this.mainscreenRightSection.setX((float) HomeScreenResourceValues.mainScreenRightSectionX);
        setViews(this.uiStateManager.getCustomAnimateMode());
        sLogger.v("setting navigation mode to MAP");
        setMode(NavigationMode.MAP);
        if (this.presenter != null) {
            this.presenter.takeView(this);
        }
        Bundle args = new Bundle();
        args.putInt(DashboardWidgetPresenter.EXTRA_GRAVITY, 0);
        args.putBoolean(DashboardWidgetPresenter.EXTRA_IS_ACTIVE, true);
        this.driveScoreGaugePresenter.setView(this.driveScoreWidgetView, args);
        this.driveScoreGaugePresenter.setWidgetVisibleToUser(true);
        this.uiStateManager.addScreenAnimationListener(this.screenAnimationListener);
        this.bus.register(this);
    }

    @DexIgnore
    private void setViews(MainView.CustomAnimationMode mode2) {
        this.etaClockView.setView(mode2);
        this.openMapRoadInfoContainer.setView(mode2);
        MainView.CustomAnimationMode mainScreenMode = mode2;
        if (mode2 == MainView.CustomAnimationMode.EXPAND && isModeVisible()) {
            mainScreenMode = MainView.CustomAnimationMode.SHRINK_MODE;
        }
        this.mapContainer.setView(mainScreenMode);
        this.smartDashContainer.setView(mainScreenMode);
        setSpeedWidgets(mode2);
        this.tbtView.setView(mode2);
        this.recalcRouteContainer.setView(mode2);
        this.laneGuidanceView.setView(mode2);
    }

    @DexIgnore
    public boolean isNavigationActive() {
        return this.currentNavigationMode == NavigationMode.MAP_ON_ROUTE || this.currentNavigationMode == NavigationMode.TBT_ON_ROUTE;
    }

    @DexIgnore
    public HomeScreen.DisplayMode getDisplayMode() {
        return this.mode;
    }

    @DexIgnore
    public void setDisplayMode(HomeScreen.DisplayMode mode2) {
        if (this.mode != mode2) {
            this.mode = mode2;
            setDisplayMode(true);
        }
    }

    @DexIgnore
    private void setDisplayMode(boolean resetTopViews) {
        sLogger.v("setDisplayMode:" + this.mode);
        switch (this.mode) {
            case MAP:
                sLogger.v("dash invisible");
                setViewsBackground(-16777216);
                this.mapContainer.onResume();
                this.laneGuidanceView.onResume();
                ObdManager.getInstance().enableInstantaneousMode(false);
                this.smartDashContainer.setVisibility(4);
                this.smartDashContainer.onPause();
                setWidgetVisibility(this.driveScoreWidgetView, true);
                if (getIsWidgetEnabled(this.driveScoreWidgetView.getId())) {
                    this.driveScoreGaugePresenter.onResume();
                }
                updateClockWidgetsVisibility(false);
                this.navigationViewsContainer.setVisibility(0);
                setWidgetVisibility(this.speedView, true);
                this.laneGuidanceView.showLastEvent();
                resumeNavigationViewContainer();
                break;
            case SMART_DASH:
                sLogger.v("dash visible");
                setViewsBackground(0);
                this.driveScoreGaugePresenter.onPause();
                ObdManager.getInstance().enableInstantaneousMode(true);
                this.smartDashContainer.onResume();
                this.smartDashContainer.setVisibility(0);
                removeWidget(this.driveScoreWidgetView);
                removeWidget(this.speedView);
                removeWidget(this.speedLimitSignView);
                removeWidget(this.etaClockView);
                this.laneGuidanceView.setVisibility(8);
                this.laneGuidanceIconIndicator.setVisibility(8);
                this.mapContainer.onPause();
                this.laneGuidanceView.onPause();
                this.navigationViewsContainer.setVisibility(0);
                resumeNavigationViewContainer();
                break;
        }
        updateRoadInfoView();
        if (resetTopViews) {
            resetTopViewsAnimator();
        }
    }

    // @com.squareup.otto.Subscribe
    @DexIgnore
    public void onNavigationModeChange(MapEvents.NavigationModeChange navigationModeChange) {
        setMode(HereNavigationManager.getInstance().getCurrentNavigationMode());
    }

    // @com.squareup.otto.Subscribe
    @DexIgnore
    public void onMapEvent(MapEvents.ManeuverDisplay event) {
        this.speedLimit = Math.round((float) SpeedManager.convert((double) event.currentSpeedLimit, SpeedManager.SpeedUnit.METERS_PER_SECOND, SpeedManager.getInstance().getSpeedUnit()));
        this.speedView.setSpeedLimit(this.speedLimit);
        updateSpeedLimitSign();
    }

    // @com.squareup.otto.Subscribe
    @DexIgnore
    public void onSpeedUnitChanged(SpeedManager.SpeedUnitChanged speedUnitChanged) {
        updateSpeedLimitSign();
    }

    // @com.squareup.otto.Subscribe
    @DexIgnore
    public void ObdPidChangeEvent(ObdManager.ObdPidChangeEvent event) {
        switch (event.pid.getId()) {
            case 13:
                updateSpeedLimitSign();
                return;
            default:
                return;
        }
    }

    // @com.squareup.otto.Subscribe
    @DexIgnore
    public void GPSSpeedChangeEvent(MapEvents.GPSSpeedEvent event) {
        updateSpeedLimitSign();
    }

    // @com.squareup.otto.Subscribe
    @DexIgnore
    public void onSpeedDataExpired(SpeedManager.SpeedDataExpired speedDataExpired) {
        updateSpeedLimitSign();
    }

    @DexIgnore
    private void updateSpeedLimitSign() {
        if (this.speedLimit <= 0) {
            hideSpeedLimit();
        } else if (this.speedManager.getCurrentSpeed() > this.speedLimit || this.alwaysShowSpeedLimitSign) {
            this.handler.removeCallbacks(this.hideSpeedLimitRunnable);
            this.speedLimitSignView.setSpeedLimitUnit(this.speedManager.getSpeedUnit());
            this.speedLimitSignView.setSpeedLimit(this.speedLimit);
            if (this.mode == HomeScreen.DisplayMode.MAP) {
                setWidgetVisibility(this.speedLimitSignView, true);
                this.lastSpeedLimitShown = SystemClock.elapsedRealtime();
            }
        } else {
            hideSpeedLimit();
        }
    }

    @DexIgnore
    private void hideSpeedLimit() {
        this.handler.removeCallbacks(this.hideSpeedLimitRunnable);
        long diff = SystemClock.elapsedRealtime() - this.lastSpeedLimitShown;
        if (diff > ((long) MIN_SPEED_LIMIT_VISIBLE_DURATION)) {
            setWidgetVisibility(this.speedLimitSignView, false);
            this.speedLimitSignView.setSpeedLimit(0);
            this.lastSpeedLimitShown = 0;
            return;
        }
        this.handler.postDelayed(this.hideSpeedLimitRunnable, diff);
    }

    // @com.squareup.otto.Subscribe
    @DexIgnore
    public void onDriverProfileChanged(DriverProfileChanged event) {
        updateDriverPrefs();
        setDisplayMode(true);
    }

    // @com.squareup.otto.Subscribe
    @DexIgnore
    public void onArrivalEvent(MapEvents.ArrivalEvent event) {
        if (getDisplayMode() == HomeScreen.DisplayMode.MAP) {
            updateClockWidgetsVisibility(false);
        }
        resetTopViewsAnimator();
    }

    @DexIgnore
    public TbtViewContainer getTbtView() {
        return this.tbtView;
    }

    @DexIgnore
    public NavigationView getNavigationView() {
        return this.mapContainer;
    }

    @DexIgnore
    public BaseSmartDashView getSmartDashView() {
        return this.smartDashContainer;
    }

    @DexIgnore
    public RecalculatingView getRecalculatingView() {
        return this.recalcRouteContainer;
    }

    @DexIgnore
    public EtaView getEtaView() {
        return this.etaClockView;
    }

    @DexIgnore
    public OpenRoadView getOpenRoadView() {
        return this.openMapRoadInfoContainer;
    }

    @DexAdd
    public NavdyPreferences getDriverPreferences() {
        return this.driverPreferences;
    }

    /* access modifiers changed from: 0000 */
    @DexReplace
    public void updateDriverPrefs() {
        this.driverPreferences = DriverProfileHelper.getInstance().getDriverProfileManager().getLocalPreferencesForCurrentDriverProfile(HudApplication.getAppContext());
    }

    @DexIgnore
    public void setMode(NavigationMode navigationMode) {
        if (this.currentNavigationMode != navigationMode || this.mapContainer.isOverviewMapMode()) {
            sLogger.v("navigation mode changed to " + navigationMode);
            this.currentNavigationMode = navigationMode;
            updateLayoutForMode(navigationMode);
            this.mapContainer.layoutMap();
            this.bus.post(navigationMode);
        }
    }

    @DexIgnore
    private void updateLayoutForMode(NavigationMode navigationMode) {
        int time;
        this.handler.removeCallbacks(this.topViewAnimationRunnable);
        this.isRecalculating = false;
        boolean navigationActive = isNavigationActive();
        this.mapContainer.updateLayoutForMode(navigationMode);
        this.smartDashContainer.updateLayoutForMode(navigationMode, false);
        this.mainscreenRightSection.updateLayoutForMode(navigationMode, this);
        this.tbtView.clearState();
        this.mapContainer.clearState();
        this.speedView.clearState();
        this.etaClockView.clearState();
        if (this.mode != HomeScreen.DisplayMode.MAP) {
            removeWidget(this.speedView);
            removeWidget(this.etaClockView);
            removeWidget(this.speedLimitSignView);
            removeWidget(this.driveScoreWidgetView);
        }
        updateClockWidgetsVisibility(false);
        if (navigationActive) {
            this.tbtView.setVisibility(0);
        } else {
            this.recalcRouteContainer.hideRecalculating();
            this.tbtView.clearState();
            this.tbtView.setVisibility(8);
        }
        updateRoadInfoView();
        resetTopViewsAnimator();
        if (this.isTopInit) {
            time = 30000;
            this.isTopInit = false;
        } else {
            time = 10000;
        }
        this.handler.postDelayed(this.topViewAnimationRunnable, (long) time);
    }

    @DexIgnore
    private void updateRoadInfoView() {
        if (isNavigationActive()) {
            this.openMapRoadInfoContainer.setVisibility(8);
            return;
        }
        this.openMapRoadInfoContainer.setRoad();
        this.openMapRoadInfoContainer.setVisibility(0);
    }

    @DexIgnore
    private void updateClockWidgetsVisibility(boolean expanding) {
        if (this.mode != HomeScreen.DisplayMode.MAP) {
            removeWidget(this.etaClockView);
        } else if (!this.uiStateManager.isMainUIShrunk() || expanding) {
            if (!isNavigationActive() || HereNavigationManager.getInstance().hasArrived()) {
                this.etaClockView.setShowEta(false);
            } else {
                this.etaClockView.setShowEta(true);
            }
            setWidgetVisibility(this.etaClockView, true);
        } else {
            setWidgetVisibility(this.etaClockView, false);
        }
    }

    @DexIgnore
    public void onClick() {
    }

    @DexIgnore
    public void onTrackHand(float normalized) {
    }

    @DexIgnore
    public boolean onGesture(GestureEvent event) {
        if (event.gesture != Gesture.GESTURE_SWIPE_RIGHT || !isModeVisible() || !hasModeView() || (this.rightScreenAnimator != null && this.rightScreenAnimator.isRunning())) {
            switch (this.mode) {
                case MAP:
                    return this.mapContainer.onGesture(event);
                case SMART_DASH:
                    return this.smartDashContainer.onGesture(event);
                default:
                    return false;
            }
        } else {
            sLogger.v("swipe_right animateOutModeView");
            animateOutModeView(true);
            return true;
        }
    }

    @DexIgnore
    public boolean onKey(InputManager.CustomKeyEvent event) {
        if (shouldAnimateTopViews()) {
            switch (event) {
                case LEFT:
                    animateTopViewsIn();
                    break;
                case RIGHT:
                    animateTopViewsOut();
                    break;
            }
        }
        switch (this.mode) {
            case MAP:
                return this.mapContainer.onKey(event);
            case SMART_DASH:
                return this.smartDashContainer.onKey(event);
            default:
                return false;
        }
    }

    @DexIgnore
    public InputManager.IInputHandler nextHandler() {
        return InputManager.nextContainingHandler(this);
    }

    @DexIgnore
    public Animator getCustomAnimator(MainView.CustomAnimationMode customAnimationMode) {
        switch (customAnimationMode) {
            case SHRINK_LEFT:
                return getShrinkLeftAnimator();
            case EXPAND:
                return getExpandAnimator();
            default:
                return null;
        }
    }

    @DexIgnore
    private Animator getShrinkLeftAnimator() {
        throw null;
        // boolean isNavigating = isNavigationActive();
        // AnimatorSet shrinkAnimatorSet = new AnimatorSet();
        // AnimatorSet.Builder builder = shrinkAnimatorSet.play(ValueAnimator.ofFloat(1.0f, 100.0f));
        // this.mapContainer.getCustomAnimator(MainView.CustomAnimationMode.SHRINK_LEFT, builder);
        // if (this.mode == HomeScreen.DisplayMode.MAP) {
        //     builder.with(HomeScreenUtils.getXPositionAnimator(this.mapViewSpeedContainer, (float) HomeScreenResourceValues.speedShrinkLeftX));
        // }
        // this.smartDashContainer.getCustomAnimator(MainView.CustomAnimationMode.SHRINK_LEFT, builder);
        // if (!isNavigating) {
        //     this.tbtView.setView(MainView.CustomAnimationMode.SHRINK_LEFT);
        //     this.openMapRoadInfoContainer.getCustomAnimator(MainView.CustomAnimationMode.SHRINK_LEFT, builder);
        // } else {
        //     this.openMapRoadInfoContainer.setView(MainView.CustomAnimationMode.SHRINK_LEFT);
        //     this.tbtView.getCustomAnimator(MainView.CustomAnimationMode.SHRINK_LEFT, builder);
        //     this.laneGuidanceView.getCustomAnimator(MainView.CustomAnimationMode.SHRINK_LEFT, builder);
        // }
        // if (this.mode == HomeScreen.DisplayMode.MAP) {
        //     if (getIsWidgetEnabled(this.etaClockView.getId())) {
        //         AnimatorSet etaAnimator = new AnimatorSet();
        //         etaAnimator.playTogether(ObjectAnimator.ofFloat(this.etaClockView, View.ALPHA, 1.0f, 0.0f));
        //         etaAnimator.addListener(new HomeScreenView.Anon4());
        //         builder.with(etaAnimator);
        //     }
        //     if (getIsWidgetEnabled(this.driveScoreWidgetView.getId())) {
        //         Animator driveScoreEventsAnimator = ObjectAnimator.ofFloat(this.driveScoreWidgetView, View.ALPHA, 1.0f, 0.0f);
        //         driveScoreEventsAnimator.addListener(new HomeScreenView.Anon5());
        //         builder.with(driveScoreEventsAnimator);
        //     }
        // }
        // if (this.isRecalculating) {
        //     builder.with(HomeScreenUtils.getXPositionAnimator(this.recalcRouteContainer, (float) HomeScreenResourceValues.recalcShrinkLeftX));
        // } else {
        //     this.recalcRouteContainer.setView(MainView.CustomAnimationMode.SHRINK_LEFT);
        // }
        // animateTopViewsOut();
        // return shrinkAnimatorSet;
    }

    @DexIgnore
    private Animator getExpandAnimator() {
        throw null;
        // boolean isNavigating = isNavigationActive();
        // AnimatorSet expandAnimatorSet = new AnimatorSet();
        // AnimatorSet.Builder builder = expandAnimatorSet.play(ValueAnimator.ofFloat(1.0f, 100.0f));
        // if (!this.isTopViewAnimationOut) {
        // }
        // if (this.mode == HomeScreen.DisplayMode.MAP) {
        //     if (this.etaClockView.getAlpha() == 0.0f) {
        //         builder.with(ObjectAnimator.ofFloat(this.etaClockView, ALPHA, 0.0f, 1.0f));
        //     }
        //     setWidgetVisibility(this.driveScoreWidgetView, true);
        //     if (this.driveScoreWidgetView.getAlpha() == 0.0f) {
        //         builder.with(ObjectAnimator.ofFloat(this.driveScoreWidgetView, View.ALPHA, 0.0f, 1.0f));
        //     }
        // }
        // this.mapContainer.getCustomAnimator(MainView.CustomAnimationMode.EXPAND, builder);
        // if (this.mode == HomeScreen.DisplayMode.MAP) {
        //     this.mapViewSpeedContainer.setVisibility(0);
        //     if (this.mapViewSpeedContainer.getX() != ((float) HomeScreenResourceValues.speedX)) {
        //         builder.with(HomeScreenUtils.getXPositionAnimator(this.mapViewSpeedContainer, (float) HomeScreenResourceValues.speedX));
        //     }
        // }
        // this.smartDashContainer.getCustomAnimator(MainView.CustomAnimationMode.EXPAND, builder);
        // if (!isNavigating) {
        //     this.tbtView.setView(MainView.CustomAnimationMode.EXPAND);
        //     this.openMapRoadInfoContainer.getCustomAnimator(MainView.CustomAnimationMode.EXPAND, builder);
        // } else {
        //     this.openMapRoadInfoContainer.setView(MainView.CustomAnimationMode.EXPAND);
        //     this.tbtView.getCustomAnimator(MainView.CustomAnimationMode.EXPAND, builder);
        //     this.laneGuidanceView.getCustomAnimator(MainView.CustomAnimationMode.EXPAND, builder);
        // }
        // if (this.isRecalculating) {
        //     builder.with(HomeScreenUtils.getXPositionAnimator(this.recalcRouteContainer, (float) HomeScreenResourceValues.recalcX));
        // } else {
        //     this.recalcRouteContainer.setView(MainView.CustomAnimationMode.EXPAND);
        // }
        // expandAnimatorSet.addListener(new HomeScreenView.Anon6());
        // animateTopViewsIn();
        // return expandAnimatorSet;
    }

    @DexIgnore
    public boolean isRecalculating() {
        return this.isRecalculating;
    }

    @DexIgnore
    public void setRecalculating(boolean val) {
        this.isRecalculating = val;
    }

    @DexIgnore
    public boolean isShowCollapsedNotification() {
        return this.showCollapsedNotif;
    }

    @DexIgnore
    public void setShowCollapsedNotification(boolean val) {
        this.showCollapsedNotif = val;
    }

    /* access modifiers changed from: 0000 */
    @DexIgnore
    public void animateTopViewsOut() {
        // this.handler.removeCallbacks(this.topViewAnimationRunnable);
        // if (!this.isTopViewAnimationRunning && !this.isTopViewAnimationOut) {
        //     this.topViewAnimator = new AnimatorSet();
        //     AnimatorSet.Builder builder = this.topViewAnimator.play(ValueAnimator.ofInt(0, 0));
        //     switch (this.mode) {
        //         case MAP:
        //             this.mapContainer.getTopAnimator(builder, true);
        //             if (getIsWidgetEnabled(this.speedView.getId())) {
        //                 this.speedView.getTopAnimator(builder, true);
        //                 break;
        //             }
        //             break;
        //         case SMART_DASH:
        //             this.smartDashContainer.getTopAnimator(builder, true);
        //             break;
        //     }
        //     this.isTopViewAnimationRunning = true;
        //     this.topViewAnimator.addListener(new HomeScreenView.Anon7());
        //     this.topViewAnimator.start();
        //     sLogger.v("started out animation");
        // }
    }

    /* access modifiers changed from: 0000 */
    @DexIgnore
    public boolean isTopViewAnimationOut() {
        return this.isTopViewAnimationOut;
    }

    /* access modifiers changed from: 0000 */
    @DexIgnore
    public void animateTopViewsIn() {
        // if (!this.isTopViewAnimationRunning && shouldAnimateTopViews() && this.isTopViewAnimationOut) {
        //     this.topViewAnimator = new AnimatorSet();
        //     AnimatorSet.Builder builder = this.topViewAnimator.play(ValueAnimator.ofInt(0, 0));
        //     switch (this.mode) {
        //         case MAP:
        //             this.mapContainer.getTopAnimator(builder, false);
        //             if (getIsWidgetEnabled(this.speedView.getId())) {
        //                 this.speedView.getTopAnimator(builder, false);
        //                 break;
        //             }
        //             break;
        //         case SMART_DASH:
        //             this.smartDashContainer.getTopAnimator(builder, false);
        //             break;
        //     }
        //     this.isTopViewAnimationRunning = true;
        //     this.topViewAnimator.addListener(new HomeScreenView.Anon8());
        //     this.topViewAnimator.start();
        //     sLogger.v("started in animation");
        // }
    }

    @DexIgnore
    private void resetTopViewsAnimator() {
        sLogger.v("resetTopViewsAnimator");
        if (this.topViewAnimator != null && this.topViewAnimator.isRunning()) {
            this.topViewAnimator.removeAllListeners();
            this.topViewAnimator.cancel();
        }
        this.mapContainer.resetTopViewsAnimator();
        if (getIsWidgetEnabled(this.speedView.getId())) {
            this.speedView.resetTopViewsAnimator();
        }
        this.smartDashContainer.resetTopViewsAnimator();
        if (!shouldAnimateTopViews()) {
            this.handler.removeCallbacks(this.topViewAnimationRunnable);
            animateTopViewsOut();
            return;
        }
        this.etaClockView.setView(MainView.CustomAnimationMode.EXPAND);
        this.etaClockView.setAlpha(1.0f);
        this.isTopViewAnimationOut = false;
        this.isTopViewAnimationRunning = false;
        this.handler.removeCallbacks(this.topViewAnimationRunnable);
        this.handler.postDelayed(this.topViewAnimationRunnable, 10000);
    }

    @DexIgnore
    public boolean shouldAnimateTopViews() {
        return this.mode != HomeScreen.DisplayMode.SMART_DASH || this.smartDashContainer.shouldShowTopViews();
    }

    @DexIgnore
    public ImageView getLaneGuidanceIconIndicator() {
        return this.laneGuidanceIconIndicator;
    }

    @DexIgnore
    public void onPause() {
        if (!this.paused) {
            this.paused = true;
            this.smartDashContainer.onPause();
            this.mapContainer.onPause();
            pauseNavigationViewContainer();
            sLogger.v("::onPause");
        }
    }

    @DexIgnore
    public void onResume() {
        if (this.paused) {
            this.paused = false;
            switch (this.mode) {
                case MAP:
                    this.mapContainer.onResume();
                    this.laneGuidanceView.onResume();
                    this.smartDashContainer.onPause();
                    break;
                case SMART_DASH:
                    this.mapContainer.onPause();
                    this.laneGuidanceView.onPause();
                    this.smartDashContainer.onResume();
                    break;
            }
            resumeNavigationViewContainer();
            sLogger.v("::onResume");
        }
    }

    @DexIgnore
    private void pauseNavigationViewContainer() {
        sLogger.v("::onPause:pauseNavigationViewContainer");
        this.tbtView.onPause();
        this.recalcRouteContainer.onPause();
    }

    @DexIgnore
    private void resumeNavigationViewContainer() {
        sLogger.v("::onResume:resumeNavigationViewContainer");
        this.tbtView.onResume();
        this.recalcRouteContainer.onResume();
    }

    @DexIgnore
    private void setViewsBackground(int color) {
        sLogger.v("setViewsBackground:" + color);
        this.tbtView.setBackgroundColor(color);
        this.recalcRouteContainer.setBackgroundColor(color);
    }

    @DexIgnore
    public void injectRightSection(View view) {
        // sLogger.v("injectRightSection");
        // this.mainscreenRightSection.injectRightSection(view);
        // clearRightSectionAnimation();
        // Main mainScreen = this.uiStateManager.getRootScreen();
        // if (this.mainscreenRightSection.isViewVisible()) {
        //     sLogger.v("injectRightSection: right section view already visible");
        // } else if (mainScreen.isNotificationExpanding() || mainScreen.isNotificationViewShowing()) {
        //     sLogger.v("injectRightSection: cannot animate");
        // } else {
        //     animateInModeView();
        // }
    }

    @DexIgnore
    public void ejectRightSection() {
        // sLogger.v("ejectRightSection");
        // clearRightSectionAnimation();
        // if (this.mainscreenRightSection.isViewVisible()) {
        //     Main mainScreen = this.uiStateManager.getRootScreen();
        //     if (mainScreen.isNotificationExpanding() || mainScreen.isNotificationViewShowing()) {
        //         sLogger.v("ejectRightSection: cannot animate");
        //         this.mainscreenRightSection.ejectRightSection();
        //         return;
        //     }
        //     animateOutModeView(true);
        //     return;
        // }
        // this.mainscreenRightSection.ejectRightSection();
    }

    @DexIgnore
    private void clearRightSectionAnimation() {
        if (this.rightScreenAnimator != null && this.rightScreenAnimator.isRunning()) {
            this.rightScreenAnimator.removeAllListeners();
            this.rightScreenAnimator.cancel();
            sLogger.v("stopped rightscreen animation");
        }
        this.rightScreenAnimator = null;
    }

    @DexIgnore
    public boolean hasModeView() {
        return this.mainscreenRightSection.hasView();
    }

    @DexIgnore
    public boolean isModeVisible() {
        return this.mainscreenRightSection.isViewVisible();
    }

    @DexIgnore
    public void animateInModeView() {
        if (hasModeView() && !isModeVisible()) {
            sLogger.v("mode-view animateInModeView");
            this.rightScreenAnimator = new AnimatorSet();
            AnimatorSet.Builder builder = this.rightScreenAnimator.play(ValueAnimator.ofFloat(0.0f, 0.0f));
            this.mapContainer.getCustomAnimator(MainView.CustomAnimationMode.SHRINK_MODE, builder);
            this.smartDashContainer.getCustomAnimator(MainView.CustomAnimationMode.SHRINK_MODE, builder);
            this.mainscreenRightSection.getCustomAnimator(MainView.CustomAnimationMode.SHRINK_MODE, builder);
            this.rightScreenAnimator.start();
        }
    }

    @DexIgnore
    public void animateOutModeView(boolean removeView) {
        // if (isModeVisible()) {
        //     sLogger.v("mode-view animateOutModeView:" + removeView);
        //     this.rightScreenAnimator = new AnimatorSet();
        //     AnimatorSet.Builder builder = this.rightScreenAnimator.play(ValueAnimator.ofFloat(0.0f, 0.0f));
        //     this.mapContainer.getCustomAnimator(MainView.CustomAnimationMode.EXPAND, builder);
        //     this.smartDashContainer.getCustomAnimator(MainView.CustomAnimationMode.EXPAND, builder);
        //     this.mainscreenRightSection.getCustomAnimator(MainView.CustomAnimationMode.EXPAND, builder);
        //     if (removeView) {
        //         this.rightScreenAnimator.addListener(new HomeScreenView.Anon9());
        //     }
        //     this.rightScreenAnimator.start();
        // }
    }

    @DexIgnore
    public void resetModeView() {
        if (isModeVisible()) {
            sLogger.v("mode-view resetModeView");
            this.mapContainer.setView(MainView.CustomAnimationMode.EXPAND);
            this.smartDashContainer.setView(MainView.CustomAnimationMode.EXPAND);
            this.mainscreenRightSection.setView(MainView.CustomAnimationMode.EXPAND);
        }
    }

    @DexIgnore
    private void setSpeedWidgets(MainView.CustomAnimationMode mode2) {
        switch (mode2) {
            case SHRINK_LEFT:
                this.mapViewSpeedContainer.setX((float) HomeScreenResourceValues.speedShrinkLeftX);
                return;
            case EXPAND:
                this.mapViewSpeedContainer.setX((float) HomeScreenResourceValues.speedX);
                return;
            default:
                return;
        }
    }

    @DexIgnore
    private void removeWidget(View widget) {
        widget.setVisibility(8);
    }

    @DexIgnore
    private void setWidgetVisibility(View widget, boolean visible) {
        if (!getIsWidgetEnabled(widget.getId())) {
            removeWidget(widget);
        } else if (visible) {
            widget.setVisibility(0);
        } else if (widget.getVisibility() != 8) {
            widget.setVisibility(4);
        }
    }

    @DexIgnore
    public boolean getIsWidgetEnabled(int id) {
        String widgetPersistentId = getWidgetPersistentId(id);
        if (widgetPersistentId != null) {
            return DriverProfileHelper.getInstance().getCurrentProfilePreferences().getBoolean(widgetPersistentId, true);
        }
        return false;
    }

    @DexIgnore
    private String getWidgetPersistentId(int id) {
        switch (id) {
            case R.id.home_screen_speed_limit_sign /*2131624328*/:
                return HomeScreenView.HomeScreenWidgetConstants.HOME_SCREEN_SPEED_LIMIT_WIDGET;
            case R.id.drive_score_events /*2131624329*/:
                return HomeScreenView.HomeScreenWidgetConstants.HOME_SCREEN_DRIVE_SCORE_WIDGET;
            case R.id.activeEtaContainer /*2131624332*/:
                return HomeScreenView.HomeScreenWidgetConstants.HOME_SCREEN_TIME_WIDGET;
            case R.id.speedContainer /*2131624343*/:
                return HomeScreenView.HomeScreenWidgetConstants.HOME_SCREEN_SPEED_WIDGET;
            default:
                return null;
        }
    }

    @DexIgnore
    public List<HomeScreenView.HomeScreenWidgetInfo> getHomeScreenWidgetInfoList() {
        return editableHomeScreenWidgetInfos;
    }

    @DexIgnore
    public void setIsWidgetEnabled(int id, boolean enabled) {
        // String widgetPersistentId = getWidgetPersistentId(id);
        // if (widgetPersistentId != null) {
        //     DriverProfileHelper.getInstance().getCurrentProfilePreferences().edit().putBoolean(widgetPersistentId, enabled).apply();
        // }
        // this.handler.post(new HomeScreenView.Anon10());
    }

    @DexIgnore
    public boolean isAHomeScreenWidgetId(int id) {
        switch (id) {
            case R.id.home_screen_speed_limit_sign /*2131624328*/:
            case R.id.drive_score_events /*2131624329*/:
            case R.id.activeEtaContainer /*2131624332*/:
            case R.id.speedContainer /*2131624343*/:
                return true;
            default:
                return false;
        }
    }

    @DexIgnore
    public void setAlwaysShowSpeedLimitSign(boolean alwaysShowSpeedLimitSign2) {
        // if (alwaysShowSpeedLimitSign2) {
        //     setIsWidgetEnabled(R.id.home_screen_speed_limit_sign, true);
        // }
        // DriverProfileHelper.getInstance().getCurrentProfilePreferences().edit().putBoolean(HomeScreenView.HomeScreenWidgetConstants.HOME_SCREEN_ALWAYS_SHOW_SPEED_LIMIT_WIDGET, alwaysShowSpeedLimitSign2).apply();
        // this.alwaysShowSpeedLimitSign = alwaysShowSpeedLimitSign2;
        // this.handler.post(new HomeScreenView.Anon11());
    }

    @DexIgnore
    public boolean isAlwaysShowSpeedLimitSign() {
        return DriverProfileHelper.getInstance().getCurrentProfilePreferences().getBoolean(HomeScreenView.HomeScreenWidgetConstants.HOME_SCREEN_ALWAYS_SHOW_SPEED_LIMIT_WIDGET, false);
    }
}
