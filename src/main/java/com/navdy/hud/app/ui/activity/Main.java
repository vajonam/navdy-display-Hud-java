package com.navdy.hud.app.ui.activity;

import android.os.Handler;

import com.navdy.hud.app.gesture.MultipleClickGestureDetector;
import com.navdy.hud.app.manager.InputManager;
import com.navdy.hud.app.screen.BaseScreen;
import com.navdy.hud.app.ui.component.homescreen.HomeScreen;
import com.navdy.hud.app.ui.component.homescreen.HomeScreenView;
import com.navdy.hud.app.ui.framework.BasePresenter;
import com.navdy.hud.app.ui.framework.UIStateManager;
import com.navdy.hud.app.util.DeviceUtil;
import com.navdy.hud.app.util.ReportIssueService;
import com.navdy.hud.app.view.MainView;
import com.navdy.service.library.events.input.GestureEvent;
import com.navdy.service.library.events.ui.Screen;
import com.navdy.service.library.events.ui.ShowScreen;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Bus;

import javax.inject.Singleton;

import flow.Backstack;
import flow.Flow;
import lanchon.dexpatcher.annotation.DexAction;
import lanchon.dexpatcher.annotation.DexAdd;
import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexRemove;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit
public class Main extends BaseScreen {

    @DexIgnore
    public static final int SNAPSHOT_TITLE_PICKER_DELAY_MILLIS = 2000;

    @DexEdit
    static Logger sLogger;

    @DexIgnore
    public Screen getScreen() {
        return null;
    }

    @DexIgnore
    public Object getDaggerModule() {
        return null;
    }

    @DexIgnore
    public String getMortarScopeName() {
        return null;
    }

    @DexAdd
    static void _showBrightnessNotification() {
        showBrightnessNotification();
    }

    @DexIgnore
    /* access modifiers changed from: private */
    private static void showBrightnessNotification() {
        if (com.navdy.hud.app.settings.HUDSettings.USE_ADAPTIVE_AUTOBRIGHTNESS) {
            sLogger.v("Use adaptive autobrightness");
            com.navdy.hud.app.framework.AdaptiveBrightnessNotification.showNotification();
            return;
        }
        sLogger.v("Use non-adaptive autobrightness");
        com.navdy.hud.app.framework.BrightnessNotification.showNotification();
    }

    @SuppressWarnings("WeakerAccess")
    @DexEdit
    @Singleton
    public static class Presenter extends BasePresenter<MainView> implements Flow.Listener, InputManager.IInputHandler {

        @DexIgnore
        Bus bus;

        @DexIgnore
        UIStateManager uiStateManager;

        @DexIgnore
        private Handler handler;

        @DexIgnore
        private MultipleClickGestureDetector innerEventsDetector;


        @DexEdit
        // Dummy source constructor
        public Presenter(@DexIgnore Void tag) { throw null; }

        @DexAdd
        public Presenter() {
            this(null);

            // Increase max click count to 5
            innerEventsDetector = new MultipleClickGestureDetector(5, new MainMultipleClickGestureDetector(Presenter.this));
        }

//        @DexReplace(target = "Main$Presenter$1")
        private class MainMultipleClickGestureDetector implements MultipleClickGestureDetector.IMultipleClickKeyGesture {

            Presenter _presenter;

            public MainMultipleClickGestureDetector(Presenter _pres) {
                _presenter = _pres;
            }

            public InputManager.IInputHandler nextHandler() {
                return null;
            }

            public boolean onGesture(GestureEvent gestureEvent) {
                return false;
            }

            public boolean onKey(InputManager.CustomKeyEvent customKeyEvent) {
                return _presenter.executeKeyEvent(customKeyEvent);
            }

            public void onMultipleClick(int i) {
                _presenter.executeMultipleKeyEvent(i);
            }
        }

        @DexReplace
        public void executeMultipleKeyEvent(int i) {
            if (i == 2) {
                Main.sLogger.v("Double click");
                takeNotificationAction();
            } else if (i == 3) {
                Main.sLogger.v("Triple click, switch view");
                HomeScreenView homescreenView = this.uiStateManager.getHomescreenView();
                if (homescreenView.getDisplayMode() == HomeScreen.DisplayMode.MAP) {
                    this.bus.post(new ShowScreen.Builder().screen(Screen.SCREEN_DASHBOARD).build());
                } else {
                    this.bus.post(new ShowScreen.Builder().screen(Screen.SCREEN_HYBRID_MAP).build());
                }
            } else if (i == 4) {
                Main.sLogger.v("Four clicks, brightness");
                Main._showBrightnessNotification();
            } else {
                if (i >= 5) {
                    Main.sLogger.d("5 clicks, dumping a snapshot");
                    ReportIssueService.dumpSnapshotAsync();
                    // if (!DeviceUtil.isUserBuild()) {
                    //     this.handler.postDelayed(
                    //             new Runnable() {
                    //                 @Override
                    //                 public void run() {
                    //                     ReportIssueService.showSnapshotMenu();
                    //                 }
                    //
                    //                 }, SNAPSHOT_TITLE_PICKER_DELAY_MILLIS);
                    // }
                }
            }
        }

        @DexEdit
        public boolean executeKeyEvent(final InputManager.CustomKeyEvent customKeyEvent) {
            throw null;
        }

        @DexIgnore
        public InputManager.IInputHandler nextHandler() {
            return null;
        }

        @DexIgnore
        public boolean onGesture(GestureEvent gestureEvent) {
            return false;
        }

        @DexIgnore
        public boolean onKey(InputManager.CustomKeyEvent customKeyEvent) {
            return false;
        }

        @DexIgnore
        public void go(Backstack backstack, Flow.Direction direction, Flow.Callback callback) {

        }

        @DexIgnore
        private void takeNotificationAction() {
        }
    }
}
