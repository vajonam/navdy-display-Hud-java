package com.navdy.hud.app.ui.component.mainmenu;

// @kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u00d2\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\n\u0018\u0000 r2\u00020\u0001:\u0002rsB-\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u0001\u00a2\u0006\u0002\u0010\u000bJ\u0010\u00109\u001a\u00020:2\u0006\u0010;\u001a\u00020<H\u0007J\"\u0010=\u001a\u0004\u0018\u00010\u00012\u0006\u0010\n\u001a\u00020\u00012\u0006\u0010>\u001a\u00020?2\u0006\u0010@\u001a\u00020?H\u0016J\b\u0010A\u001a\u00020\rH\u0016J\u0010\u0010B\u001a\n\u0012\u0004\u0012\u00020D\u0018\u00010CH\u0016J\u0012\u0010E\u001a\u0004\u0018\u00010D2\u0006\u0010F\u001a\u00020\rH\u0016J\n\u0010G\u001a\u0004\u0018\u00010HH\u0016J\n\u0010I\u001a\u0004\u0018\u00010JH\u0016J\b\u0010K\u001a\u00020'H\u0016J\b\u0010L\u001a\u00020'H\u0016J\u0018\u0010M\u001a\u00020'2\u0006\u0010N\u001a\u00020\r2\u0006\u0010F\u001a\u00020\rH\u0016J(\u0010O\u001a\u00020:2\u0006\u0010P\u001a\u00020D2\u0006\u0010Q\u001a\u00020R2\u0006\u0010F\u001a\u00020\r2\u0006\u0010S\u001a\u00020TH\u0016J\u0010\u0010U\u001a\u00020:2\u0006\u0010V\u001a\u00020WH\u0007J\u0010\u0010X\u001a\u00020:2\u0006\u0010Y\u001a\u00020ZH\u0007J\b\u0010[\u001a\u00020:H\u0016J\b\u0010\\\u001a\u00020:H\u0016J\u0010\u0010]\u001a\u00020:2\u0006\u0010^\u001a\u00020_H\u0007J\u0010\u0010`\u001a\u00020:2\u0006\u0010a\u001a\u00020bH\u0016J\u0010\u0010c\u001a\u00020:2\u0006\u0010;\u001a\u00020dH\u0007J\u0010\u0010e\u001a\u00020:2\u0006\u0010;\u001a\u00020fH\u0007J\b\u0010g\u001a\u00020:H\u0016J\u0010\u0010h\u001a\u00020:2\u0006\u0010i\u001a\u00020jH\u0016J\u0010\u0010k\u001a\u00020'2\u0006\u0010a\u001a\u00020bH\u0016J\u0010\u0010l\u001a\u00020:2\u0006\u0010N\u001a\u00020\rH\u0016J\u0010\u0010m\u001a\u00020:2\u0006\u0010n\u001a\u00020\rH\u0016J\b\u0010o\u001a\u00020:H\u0016J\u000e\u0010p\u001a\u00020:2\u0006\u0010F\u001a\u00020\rJ\b\u0010q\u001a\u00020:H\u0016R\u000e\u0010\f\u001a\u00020\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R$\u0010\u0011\u001a\u00020\u00102\u0006\u0010\u000f\u001a\u00020\u0010@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0012\u0010\u0013\"\u0004\b\u0014\u0010\u0015R\u001a\u0010\u0016\u001a\u00020\u0017X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0018\u0010\u0019\"\u0004\b\u001a\u0010\u001bR\u0011\u0010\u001c\u001a\u00020\u001d\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001e\u0010\u001fR\u001a\u0010 \u001a\u00020!X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\"\u0010#\"\u0004\b$\u0010%R\u000e\u0010\n\u001a\u00020\u0001X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010&\u001a\u00020'X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b(\u0010)\"\u0004\b*\u0010+R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R \u0010,\u001a\b\u0018\u00010-R\u00020.X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b/\u00100\"\u0004\b1\u00102R\u0011\u00103\u001a\u00020.\u00a2\u0006\b\n\u0000\u001a\u0004\b4\u00105R\u001a\u00106\u001a\u00020'X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b7\u0010)\"\u0004\b8\u0010+R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006t"}, d2 = {"Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;", "Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;", "bus", "Lcom/squareup/otto/Bus;", "sharedPreferences", "Landroid/content/SharedPreferences;", "vscrollComponent", "Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;", "presenter", "Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;", "parent", "(Lcom/squareup/otto/Bus;Landroid/content/SharedPreferences;Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;)V", "backSelection", "", "backSelectionId", "value", "Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;", "gaugeType", "getGaugeType", "()Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;", "setGaugeType", "(Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;)V", "gaugeView", "Lcom/navdy/hud/app/view/DashboardWidgetView;", "getGaugeView", "()Lcom/navdy/hud/app/view/DashboardWidgetView;", "setGaugeView", "(Lcom/navdy/hud/app/view/DashboardWidgetView;)V", "gaugeViewContainer", "Landroid/widget/FrameLayout;", "getGaugeViewContainer", "()Landroid/widget/FrameLayout;", "headingDataUtil", "Lcom/navdy/hud/app/util/HeadingDataUtil;", "getHeadingDataUtil", "()Lcom/navdy/hud/app/util/HeadingDataUtil;", "setHeadingDataUtil", "(Lcom/navdy/hud/app/util/HeadingDataUtil;)V", "registered", "", "getRegistered", "()Z", "setRegistered", "(Z)V", "smartDashWidgetCache", "Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;", "Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;", "getSmartDashWidgetCache", "()Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;", "setSmartDashWidgetCache", "(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;)V", "smartDashWidgetManager", "getSmartDashWidgetManager", "()Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;", "userPreferenceChanged", "getUserPreferenceChanged", "setUserPreferenceChanged", "ObdPidChangeEvent", "", "event", "Lcom/navdy/hud/app/obd/ObdManager$ObdPidChangeEvent;", "getChildMenu", "args", "", "path", "getInitialSelection", "getItems", "", "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;", "getModelfromPos", "pos", "getScrollIndex", "Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;", "getType", "Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;", "isBindCallsEnabled", "isFirstItemEmpty", "isItemClickable", "id", "onBindToView", "model", "view", "Landroid/view/View;", "state", "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;", "onDriveScoreUpdated", "driveScoreUpdated", "Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;", "onDriverProfileChanged", "profileChanged", "Lcom/navdy/hud/app/event/DriverProfileChanged;", "onFastScrollEnd", "onFastScrollStart", "onGpsLocationChanged", "location", "Landroid/location/Location;", "onItemSelected", "selection", "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;", "onLocationFixChangeEvent", "Lcom/navdy/hud/app/maps/MapEvents$LocationFix;", "onMapEvent", "Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;", "onScrollIdle", "onUnload", "level", "Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;", "selectItem", "setBackSelectionId", "setBackSelectionPos", "n", "setSelectedIcon", "showGauge", "showToolTip", "Companion", "GaugeType", "app_hudDebug"}, k = 1, mv = {1, 1, 6})
/* compiled from: DashGaugeConfiguratorMenu.kt */
import android.content.SharedPreferences;
import android.location.Location;
import android.view.View;
import android.widget.FrameLayout;

import com.glympse.android.hal.NotificationListener;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.analytics.TelemetryDataManager;
import com.navdy.hud.app.event.DriverProfileChanged;
import com.navdy.hud.app.manager.SpeedManager;
import com.navdy.hud.app.maps.MapEvents;
import com.navdy.hud.app.maps.MapSettings;
import com.navdy.hud.app.obd.ObdManager;
import com.navdy.hud.app.ui.component.homescreen.SmartDashView;
import com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager;
import com.navdy.hud.app.ui.component.vlist.VerticalList;
import com.navdy.hud.app.ui.component.vlist.viewholder.SwitchViewHolder;
import com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex;
import com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent;
import com.navdy.hud.app.ui.framework.UIStateManager;
import com.navdy.hud.app.util.HeadingDataUtil;
import com.navdy.hud.app.view.DashboardWidgetPresenter;
import com.navdy.hud.app.view.DashboardWidgetView;
import com.navdy.obd.PidSet;
import com.navdy.obd.Pids;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

import kotlin.NoWhenBranchMatchedException;
import kotlin.jvm.internal.Intrinsics;
import lanchon.dexpatcher.annotation.DexAction;
import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit(defaultAction = DexAction.ADD)
public final class DashGaugeConfiguratorMenu implements IMenu {
    // @DexIgnore
    // public static /* final */ DashGaugeConfiguratorMenu.Companion Companion; // = new com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu.Companion(null);
    @DexIgnore
    private static /* final */ VerticalList.Model back;
    @DexIgnore
    private static /* final */ int backColor;
    @DexIgnore
    private static /* final */ int bkColorUnselected;
    @DexIgnore
    private static /* final */ int centerGaugeBackgroundColor;
    @DexIgnore
    private static /* final */ ArrayList<VerticalList.Model> centerGaugeOptionsList; // = new java.util.ArrayList<>();
    @DexIgnore
    private static /* final */ String centerGaugeTitle;
    @DexIgnore
    private static /* final */ String offLabel;
    @DexIgnore
    private static /* final */ String onLabel;
    @DexIgnore
    private static /* final */ Logger sLogger; // = new com.navdy.service.library.log.Logger(Companion.getClass());
    @DexIgnore
    private static /* final */ ArrayList<VerticalList.Model> sideGaugesOptionsList; // = new java.util.ArrayList<>();
    @DexIgnore
    private static /* final */ String sideGaugesTitle;
    @DexIgnore
    private static /* final */ VerticalList.Model speedoMeter;
    @DexIgnore
    private static /* final */ VerticalList.Model tachoMeter;
    @DexIgnore
    private static /* final */ UIStateManager uiStateManager;
    @DexIgnore
    private static /* final */ String unavailableLabel;
    @DexIgnore
    private int backSelection;
    @DexIgnore
    private int backSelectionId;
    @DexIgnore
    private /* final */ Bus bus;
    // @org.jetbrains.annotations.NotNull
    @DexIgnore
    private DashGaugeConfiguratorMenu.GaugeType gaugeType;
    // @org.jetbrains.annotations.NotNull
    @DexIgnore
    private DashboardWidgetView gaugeView;
    // @org.jetbrains.annotations.NotNull
    @DexIgnore
    private /* final */ FrameLayout gaugeViewContainer;
    // @org.jetbrains.annotations.NotNull
    @DexIgnore
    private HeadingDataUtil headingDataUtil; // = new com.navdy.hud.app.util.HeadingDataUtil();
    @DexIgnore
    private /* final */ IMenu parent;
    @DexIgnore
    private /* final */ MainMenuScreen2.Presenter presenter;
    @DexIgnore
    private boolean registered;
    @DexIgnore
    private /* final */ SharedPreferences sharedPreferences;
    // @org.jetbrains.annotations.Nullable
    @DexIgnore
    private SmartDashWidgetManager.SmartDashWidgetCache smartDashWidgetCache;
    // @org.jetbrains.annotations.NotNull
    @DexIgnore
    private /* final */ SmartDashWidgetManager smartDashWidgetManager;
    @DexIgnore
    private boolean userPreferenceChanged;
    @DexIgnore
    private /* final */ VerticalMenuComponent vscrollComponent;

    // // @kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u0010\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\b\u0005"}, d2 = {"<anonymous>", "", "identifier", "", "kotlin.jvm.PlatformType", "filter"}, k = 3, mv = {1, 1, 6})
    // /* compiled from: DashGaugeConfiguratorMenu.kt */
    // @DexIgnore
    // static final class Anon1 implements SmartDashWidgetManager.IWidgetFilter {
    //     public static final DashGaugeConfiguratorMenu.Anon1 INSTANCE = new DashGaugeConfiguratorMenu.Anon1();
    //
    //     @DexIgnore
    //     Anon1() {
    //     }
    //
    //     @DexIgnore
    //     public final boolean filter(String identifier) {
    //         if (identifier != null) {
    //             switch (identifier.hashCode()) {
    //                 case 811991446:
    //                     if (identifier.equals(SmartDashWidgetManager.EMPTY_WIDGET_ID)) {
    //                         return true;
    //                     }
    //                     break;
    //                 case 1872543020:
    //                     if (identifier.equals(SmartDashWidgetManager.TRAFFIC_INCIDENT_GAUGE_ID)) {
    //                         if (MapSettings.isTrafficDashWidgetsEnabled()) {
    //                             return false;
    //                         }
    //                         return true;
    //                     }
    //                     break;
    //             }
    //         }
    //         return false;
    //     }
    // }
    //
    // // @kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u0019\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\b\n\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0007\u00a8\u0006\u0007"}, d2 = {"com/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Anon2", "", "(Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;)V", "onReload", "", "reload", "Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$Reload;", "app_hudDebug"}, k = 1, mv = {1, 1, 6})
    // /* compiled from: DashGaugeConfiguratorMenu.kt */
    // @DexIgnore
    // public static final class Anon2 {
    //     final /* synthetic */ DashGaugeConfiguratorMenu this$Anon0;
    //
    //     @DexIgnore
    //     Anon2(DashGaugeConfiguratorMenu $outer) {
    //         this.this$Anon0 = $outer;
    //     }
    //
    //     @Subscribe
    //     @DexIgnore
    //     public final void onReload(@NotNull SmartDashWidgetManager.Reload reload) {
    //         // kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(reload, "reload");
    //         // switch (com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu.WhenMappings.$EnumSwitchMapping$Anon0[reload.ordinal()]) {
    //         //     case 1:
    //         //         this.this$Anon0.presenter.updateCurrentMenu(this.this$Anon0);
    //         //         return;
    //         //     case 2:
    //         //         this.this$Anon0.setSmartDashWidgetCache(this.this$Anon0.getSmartDashWidgetManager().buildSmartDashWidgetCache(0));
    //         //         this.this$Anon0.presenter.updateCurrentMenu(this.this$Anon0);
    //         //         return;
    //         //     default:
    //         //         return;
    //         // }
    //     }
    // }

    // @kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u0014\u0010\u0007\u001a\u00020\bX\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0014\u0010\u000b\u001a\u00020\bX\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\f\u0010\nR\u0014\u0010\r\u001a\u00020\bX\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\nR\u001a\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00040\u0010X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012R\u0014\u0010\u0013\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0016R\u0014\u0010\u0017\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0016R\u0014\u0010\u0019\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u0016R\u0014\u0010\u001b\u001a\u00020\u001cX\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u001eR\u001a\u0010\u001f\u001a\b\u0012\u0004\u0012\u00020\u00040\u0010X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b \u0010\u0012R\u0014\u0010!\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\"\u0010\u0016R\u0014\u0010#\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b$\u0010\u0006R\u0014\u0010%\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b&\u0010\u0006R\u0014\u0010'\u001a\u00020(X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b)\u0010*R\u0014\u0010+\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b,\u0010\u0016\u00a8\u0006-"}, d2 = {"Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;", "", "()V", "back", "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;", "getBack", "()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;", "backColor", "", "getBackColor", "()I", "bkColorUnselected", "getBkColorUnselected", "centerGaugeBackgroundColor", "getCenterGaugeBackgroundColor", "centerGaugeOptionsList", "Ljava/util/ArrayList;", "getCenterGaugeOptionsList", "()Ljava/util/ArrayList;", "centerGaugeTitle", "", "getCenterGaugeTitle", "()Ljava/lang/String;", "offLabel", "getOffLabel", "onLabel", "getOnLabel", "sLogger", "Lcom/navdy/service/library/log/Logger;", "getSLogger", "()Lcom/navdy/service/library/log/Logger;", "sideGaugesOptionsList", "getSideGaugesOptionsList", "sideGaugesTitle", "getSideGaugesTitle", "speedoMeter", "getSpeedoMeter", "tachoMeter", "getTachoMeter", "uiStateManager", "Lcom/navdy/hud/app/ui/framework/UIStateManager;", "getUiStateManager", "()Lcom/navdy/hud/app/ui/framework/UIStateManager;", "unavailableLabel", "getUnavailableLabel", "app_hudDebug"}, k = 1, mv = {1, 1, 6})
    /* compiled from: DashGaugeConfiguratorMenu.kt */
    // @DexIgnore
    // public static final class Companion {
    //     @DexIgnore
    //     private Companion() {
    //     }
    //
    //     // @DexIgnore
    //     // public /* synthetic */ Companion(kotlin.jvm.internal.DefaultConstructorMarker $constructor_marker) {
    //     //     this();
    //     // }
    //
    //     @DexIgnore
    //     private final VerticalList.Model getBack() {
    //         return DashGaugeConfiguratorMenu.back;
    //     }
    //
    //     @DexIgnore
    //     private final VerticalList.Model getTachoMeter() {
    //         return DashGaugeConfiguratorMenu.tachoMeter;
    //     }
    //
    //     @DexIgnore
    //     private final VerticalList.Model getSpeedoMeter() {
    //         return DashGaugeConfiguratorMenu.speedoMeter;
    //     }
    //
    //     @DexIgnore
    //     private final int getBackColor() {
    //         return DashGaugeConfiguratorMenu.backColor;
    //     }
    //
    //     @DexIgnore
    //     private final int getBkColorUnselected() {
    //         return DashGaugeConfiguratorMenu.bkColorUnselected;
    //     }
    //
    //     @DexIgnore
    //     private final ArrayList<VerticalList.Model> getCenterGaugeOptionsList() {
    //         return DashGaugeConfiguratorMenu.centerGaugeOptionsList;
    //     }
    //
    //     @DexIgnore
    //     private final ArrayList<VerticalList.Model> getSideGaugesOptionsList() {
    //         return DashGaugeConfiguratorMenu.sideGaugesOptionsList;
    //     }
    //
    //     @DexIgnore
    //     private final Logger getSLogger() {
    //         return DashGaugeConfiguratorMenu.sLogger;
    //     }
    //
    //     @DexIgnore
    //     private final String getCenterGaugeTitle() {
    //         return DashGaugeConfiguratorMenu.centerGaugeTitle;
    //     }
    //
    //     @DexIgnore
    //     private final String getSideGaugesTitle() {
    //         return DashGaugeConfiguratorMenu.sideGaugesTitle;
    //     }
    //
    //     @DexIgnore
    //     private final UIStateManager getUiStateManager() {
    //         return DashGaugeConfiguratorMenu.uiStateManager;
    //     }
    //
    //     @DexIgnore
    //     private final int getCenterGaugeBackgroundColor() {
    //         return DashGaugeConfiguratorMenu.centerGaugeBackgroundColor;
    //     }
    //
    //     @DexIgnore
    //     private final String getOnLabel() {
    //         return DashGaugeConfiguratorMenu.onLabel;
    //     }
    //
    //     @DexIgnore
    //     private final String getOffLabel() {
    //         return DashGaugeConfiguratorMenu.offLabel;
    //     }
    //
    //     @DexIgnore
    //     private final String getUnavailableLabel() {
    //         return DashGaugeConfiguratorMenu.unavailableLabel;
    //     }
    // }

    // @kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0004\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004\u00a8\u0006\u0005"}, d2 = {"Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;", "", "(Ljava/lang/String;I)V", "CENTER", "SIDE", "app_hudDebug"}, k = 1, mv = {1, 1, 6})
    /* compiled from: DashGaugeConfiguratorMenu.kt */
    @DexIgnore
    public enum GaugeType {
        CENTER,
        SIDE
    }

    // @kotlin.Metadata(bv = {1, 0, 1}, k = 3, mv = {1, 1, 6})
    // @DexIgnore
    // public final /* synthetic */ class WhenMappings {
    //     @DexIgnore
    //     public static final /* synthetic */ int[] $EnumSwitchMapping$Anon0 = new int[com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.Reload.values().length];
    //
    //     // static {
    //     //     $EnumSwitchMapping$Anon0[com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.Reload.RELOADED.ordinal()] = 1;
    //     //     $EnumSwitchMapping$Anon0[com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.Reload.RELOAD_CACHE.ordinal()] = 2;
    //     //     $EnumSwitchMapping$Anon1 = new int[com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu.GaugeType.values().length];
    //     //     $EnumSwitchMapping$Anon1[com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu.GaugeType.CENTER.ordinal()] = 1;
    //     //     $EnumSwitchMapping$Anon1[com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu.GaugeType.SIDE.ordinal()] = 2;
    //     //     $EnumSwitchMapping$Anon2 = new int[com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu.GaugeType.values().length];
    //     //     $EnumSwitchMapping$Anon2[com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu.GaugeType.CENTER.ordinal()] = 1;
    //     //     $EnumSwitchMapping$Anon2[com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu.GaugeType.SIDE.ordinal()] = 2;
    //     //     $EnumSwitchMapping$Anon3 = new int[com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu.GaugeType.values().length];
    //     //     $EnumSwitchMapping$Anon3[com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu.GaugeType.CENTER.ordinal()] = 1;
    //     //     $EnumSwitchMapping$Anon3[com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu.GaugeType.SIDE.ordinal()] = 2;
    //     //     $EnumSwitchMapping$Anon4 = new int[com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu.GaugeType.values().length];
    //     //     $EnumSwitchMapping$Anon4[com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu.GaugeType.CENTER.ordinal()] = 1;
    //     //     $EnumSwitchMapping$Anon4[com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu.GaugeType.SIDE.ordinal()] = 2;
    //     // }
    // }

    @DexIgnore
    public DashGaugeConfiguratorMenu(@NotNull Bus bus2, @NotNull SharedPreferences sharedPreferences2, @NotNull VerticalMenuComponent vscrollComponent2, @NotNull MainMenuScreen2.Presenter presenter2, @NotNull IMenu parent2) {
        // Intrinsics.checkParameterIsNotNull(bus2, "bus");
        // Intrinsics.checkParameterIsNotNull(sharedPreferences2, "sharedPreferences");
        // Intrinsics.checkParameterIsNotNull(vscrollComponent2, "vscrollComponent");
        // Intrinsics.checkParameterIsNotNull(presenter2, "presenter");
        // Intrinsics.checkParameterIsNotNull(parent2, "parent");
        // this.bus = bus2;
        // this.sharedPreferences = sharedPreferences2;
        // this.vscrollComponent = vscrollComponent2;
        // this.presenter = presenter2;
        // this.parent = parent2;
        // FrameLayout frameLayout = this.vscrollComponent.selectedCustomView;
        // Intrinsics.checkExpressionValueIsNotNull(frameLayout, "vscrollComponent.selectedCustomView");
        // this.gaugeViewContainer = frameLayout;
        // this.gaugeView = new DashboardWidgetView(this.gaugeViewContainer.getContext());
        // this.gaugeView.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        // this.gaugeViewContainer.removeAllViews();
        // this.gaugeViewContainer.addView(this.gaugeView);
        // this.smartDashWidgetManager = new SmartDashWidgetManager(this.sharedPreferences, HudApplication.getAppContext());
        // this.smartDashWidgetManager.onResume();
        // this.smartDashWidgetManager.setFilter(DashGaugeConfiguratorMenu.Anon1.INSTANCE);
        // this.smartDashWidgetManager.reLoadAvailableWidgets(true);
        // this.smartDashWidgetManager.registerForChanges(new DashGaugeConfiguratorMenu.Anon2(this));
        // this.gaugeType = DashGaugeConfiguratorMenu.GaugeType.CENTER;
    }

    // @org.jetbrains.annotations.NotNull
    @DexIgnore
    public final SmartDashWidgetManager getSmartDashWidgetManager() {
        return this.smartDashWidgetManager;
    }

    // @org.jetbrains.annotations.Nullable
    @DexIgnore
    public final SmartDashWidgetManager.SmartDashWidgetCache getSmartDashWidgetCache() {
        return this.smartDashWidgetCache;
    }

    @DexIgnore
    public final void setSmartDashWidgetCache(@Nullable SmartDashWidgetManager.SmartDashWidgetCache smartDashWidgetCache2) {
        this.smartDashWidgetCache = smartDashWidgetCache2;
    }

    // @org.jetbrains.annotations.NotNull
    @DexIgnore
    public final FrameLayout getGaugeViewContainer() {
        return this.gaugeViewContainer;
    }

    // @org.jetbrains.annotations.NotNull
    @DexIgnore
    public final DashboardWidgetView getGaugeView() {
        return this.gaugeView;
    }

    @DexIgnore
    public final void setGaugeView(@NotNull DashboardWidgetView dashboardWidgetView) {
        Intrinsics.checkParameterIsNotNull(dashboardWidgetView, "<set-?>");
        this.gaugeView = dashboardWidgetView;
    }

    // @org.jetbrains.annotations.NotNull
    @DexIgnore
    public final HeadingDataUtil getHeadingDataUtil() {
        return this.headingDataUtil;
    }

    @DexIgnore
    public final void setHeadingDataUtil(@NotNull HeadingDataUtil headingDataUtil2) {
        Intrinsics.checkParameterIsNotNull(headingDataUtil2, "<set-?>");
        this.headingDataUtil = headingDataUtil2;
    }

    @DexIgnore
    public final boolean getUserPreferenceChanged() {
        return this.userPreferenceChanged;
    }

    @DexIgnore
    public final void setUserPreferenceChanged(boolean z) {
        this.userPreferenceChanged = z;
    }

    /* @DexIgnore
    static {
        android.content.res.Resources resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        backColor = resources.getColor(com.navdy.hud.app.R.color.mm_back);
        bkColorUnselected = resources.getColor(com.navdy.hud.app.R.color.icon_bk_color_unselected);
        java.lang.CharSequence text = resources.getText(com.navdy.hud.app.R.string.carousel_menu_smartdash_select_center_gauge);
        if (text == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type kotlin.String");
        }
        centerGaugeTitle = (java.lang.String) text;
        java.lang.CharSequence text2 = resources.getText(com.navdy.hud.app.R.string.carousel_menu_smartdash_side_gauges);
        if (text2 == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type kotlin.String");
        }
        sideGaugesTitle = (java.lang.String) text2;
        java.lang.CharSequence text3 = resources.getText(com.navdy.hud.app.R.string.on);
        if (text3 == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type kotlin.String");
        }
        onLabel = (java.lang.String) text3;
        java.lang.CharSequence text4 = resources.getText(com.navdy.hud.app.R.string.off);
        if (text4 == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type kotlin.String");
        }
        offLabel = (java.lang.String) text4;
        java.lang.CharSequence text5 = resources.getText(com.navdy.hud.app.R.string.unavailable);
        if (text5 == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type kotlin.String");
        }
        unavailableLabel = (java.lang.String) text5;
        com.navdy.hud.app.ui.framework.UIStateManager uiStateManager2 = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getUiStateManager();
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(uiStateManager2, "remoteDeviceManager.uiStateManager");
        uiStateManager = uiStateManager2;
        centerGaugeBackgroundColor = resources.getColor(com.navdy.hud.app.R.color.mm_options_scroll_gauge);
        int fluctuatorColor = Companion.getBackColor();
        com.navdy.hud.app.ui.component.vlist.VerticalList.Model buildModel = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.menu_back, com.navdy.hud.app.R.drawable.icon_mm_back, fluctuatorColor, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, fluctuatorColor, resources.getString(com.navdy.hud.app.R.string.back), null);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(buildModel, "IconBkColorViewHolder.bu\u2026tuatorColor, title, null)");
        back = buildModel;
        java.lang.String title = resources.getString(com.navdy.hud.app.R.string.tachometer);
        com.navdy.hud.app.ui.component.vlist.VerticalList.Model buildModel2 = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.main_menu_options_tachometer, com.navdy.hud.app.R.drawable.icon_options_dash_tachometer_2, -16777216, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, resources.getColor(com.navdy.hud.app.R.color.mm_options_tachometer), title, null);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(buildModel2, "IconBkColorViewHolder.bu\u2026tuatorColor, title, null)");
        tachoMeter = buildModel2;
        java.lang.String title2 = resources.getString(com.navdy.hud.app.R.string.speedometer);
        com.navdy.hud.app.ui.component.vlist.VerticalList.Model buildModel3 = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.main_menu_options_speedometer, com.navdy.hud.app.R.drawable.icon_options_dash_speedometer_2, -16777216, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, resources.getColor(com.navdy.hud.app.R.color.mm_options_speedometer), title2, null);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(buildModel3, "IconBkColorViewHolder.bu\u2026tuatorColor, title, null)");
        speedoMeter = buildModel3;
        Companion.getCenterGaugeOptionsList().add(Companion.getBack());
        Companion.getCenterGaugeOptionsList().add(Companion.getTachoMeter());
        Companion.getCenterGaugeOptionsList().add(Companion.getSpeedoMeter());
    } */

    @DexIgnore
    public final boolean getRegistered() {
        return this.registered;
    }

    @DexIgnore
    public final void setRegistered(boolean z) {
        this.registered = z;
    }

    // @org.jetbrains.annotations.NotNull
    @DexIgnore
    public final DashGaugeConfiguratorMenu.GaugeType getGaugeType() {
        return this.gaugeType;
    }

    @DexIgnore
    public final void setGaugeType(@NotNull DashGaugeConfiguratorMenu.GaugeType value) {
        Intrinsics.checkParameterIsNotNull(value, NotificationListener.INTENT_EXTRA_VALUE);
        this.gaugeType = value;
        if (Intrinsics.areEqual((Object) value, (Object) DashGaugeConfiguratorMenu.GaugeType.SIDE)) {
            if (!this.registered) {
                this.bus.register(this);
                this.registered = true;
            }
        } else if (this.registered) {
            this.bus.unregister(this);
            this.registered = false;
        }
    }

    @DexReplace
    @Nullable
    @Override
    public List<VerticalList.Model> getItems() {
        switch (this.gaugeType) {
            case CENTER:
                return centerGaugeOptionsList;
            case SIDE:
                this.smartDashWidgetCache = this.smartDashWidgetManager.buildSmartDashWidgetCache(0);
                sideGaugesOptionsList.clear();
                sideGaugesOptionsList.add(back);
                if (this.smartDashWidgetCache != null) {
                    SmartDashWidgetManager.SmartDashWidgetCache $receiver = this.smartDashWidgetCache;
                    int fluctuatorColor = HudApplication.getAppContext().getResources().getColor(R.color.mm_options_side_gauges_halo);

                    // Minus 1 to hide the "empty" widget
                    int widgetsCount = $receiver.getWidgetsCount() - 1;
                    for (int i = 0; i < widgetsCount; i++) {
                        String title = null;
                        boolean isGaugeEnabled = true;
                        String subtitle;
                        DashboardWidgetPresenter widgetPresenter = $receiver.getWidgetPresenter(i);
                        if (widgetPresenter != null) {
                            title = widgetPresenter.getWidgetName();
                        }
                        if (title == null) {
                            title = "";
                        }
                        widgetPresenter = $receiver.getWidgetPresenter(i);
                        String identifier = widgetPresenter != null ? widgetPresenter.getWidgetIdentifier() : null;
                        boolean isGaugeOn = this.smartDashWidgetManager.isGaugeOn(identifier);
                        PidSet pidSet = ObdManager.getInstance().getSupportedPids();
                        if (identifier != null) {
                            boolean z;
                            switch (identifier) {
                                case SmartDashWidgetManager.MPG_AVG_WIDGET_ID:
                                    z = pidSet != null && pidSet.contains(Pids.INSTANTANEOUS_FUEL_CONSUMPTION);
                                    isGaugeEnabled = z;
                                    break;
                                case SmartDashWidgetManager.ENGINE_TEMPERATURE_GAUGE_ID:
                                    z = pidSet != null && pidSet.contains(Pids.ENGINE_COOLANT_TEMPERATURE);
                                    isGaugeEnabled = z;
                                    break;
                                case SmartDashWidgetManager.FUEL_GAUGE_ID:
                                    z = pidSet != null && pidSet.contains(Pids.FUEL_LEVEL);
                                    isGaugeEnabled = z;
                                    break;
                                case SmartDashWidgetManager.INTAKE_PRESSURE_GAUGE_ID:
                                    z = pidSet != null && pidSet.contains(Pids.MANIFOLD_AIR_PRESSURE);
                                    isGaugeEnabled = z;
                                    break;
                                case SmartDashWidgetManager.ENGINE_OIL_TEMPERATURE_GAUGE_ID:
                                    z = pidSet != null && pidSet.contains(Pids.ENGINE_OIL_TEMPERATURE);
                                    isGaugeEnabled = z;
                                    break;
                                case SmartDashWidgetManager.AMBIENT_TEMPERATURE_GAUGE_ID:
                                    z = pidSet != null && pidSet.contains(Pids.AMBIENT_AIR_TEMRERATURE);
                                    isGaugeEnabled = z;
                                    break;
                                default:
                            }
                            if (isGaugeEnabled) {
                                subtitle = isGaugeOn ? onLabel : offLabel;
                            } else {
                                subtitle = unavailableLabel;
                            }
                            sideGaugesOptionsList.add(SwitchViewHolder.Companion.buildModel(i, fluctuatorColor, title, subtitle, isGaugeOn, isGaugeEnabled));
                        }
                    }
                }
                return sideGaugesOptionsList;
            default:
                throw new NoWhenBranchMatchedException();
        }
    }

    @DexIgnore
    public int getInitialSelection() {
        return 1;
    }

    // @org.jetbrains.annotations.Nullable
    @DexIgnore
    public VerticalFastScrollIndex getScrollIndex() {
        return null;
    }

    @DexIgnore
    public void setBackSelectionPos(int n) {
        this.backSelection = n;
    }

    @DexIgnore
    public void setBackSelectionId(int id) {
        this.backSelectionId = id;
    }

    @DexIgnore
    public void setSelectedIcon() {
        // switch (this.gaugeType) {
        //     case CENTER:
        //         this.vscrollComponent.setSelectedIconColorImage(R.drawable.icon_center_gauge, Companion.getCenterGaugeBackgroundColor(), null, 1.0f);
        //         this.vscrollComponent.selectedText.setText(Companion.getCenterGaugeTitle());
        //         return;
        //     case SIDE:
        //         this.vscrollComponent.showSelectedCustomView();
        //         this.vscrollComponent.selectedText.setText(null);
        //         if (this.smartDashWidgetCache == null) {
        //             getItems();
        //         }
        //         showGauge(1);
        //         return;
        //     default:
        //         return;
        // }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:0x00a2, code lost:
        if (r4 != null) goto L_0x00a4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00d4, code lost:
        if (r4 != null) goto L_0x00d6;
     */
    @DexIgnore
    public boolean selectItem(@NotNull VerticalList.ItemSelectionState selection) {
        throw null;
        // String str;
        // String str2;
        // Intrinsics.checkParameterIsNotNull(selection, "selection");
        // Companion.getSLogger().v("select id:" + selection.id + " pos:" + selection.pos);
        // switch (this.gaugeType) {
        //     case CENTER:
        //         switch (selection.id) {
        //             case R.id.main_menu_options_speedometer /*2131623990*/:
        //                 this.presenter.close();
        //                 SmartDashView smartDashView = Companion.getUiStateManager().getSmartDashView();
        //                 if (smartDashView != null) {
        //                     smartDashView.onSpeedoMeterSelected();
        //                     break;
        //                 }
        //                 break;
        //             case R.id.main_menu_options_tachometer /*2131623991*/:
        //                 this.presenter.close();
        //                 SmartDashView smartDashView2 = Companion.getUiStateManager().getSmartDashView();
        //                 if (smartDashView2 != null) {
        //                     smartDashView2.onTachoMeterSelected();
        //                     break;
        //                 }
        //                 break;
        //             case R.id.menu_back /*2131624007*/:
        //                 this.presenter.loadMenu(this.parent, IMenu.MenuLevel.BACK_TO_PARENT, this.backSelection, 0);
        //                 break;
        //         }
        //     case SIDE:
        //         switch (selection.id) {
        //             case R.id.menu_back /*2131624007*/:
        //                 this.presenter.loadMenu(this.parent, IMenu.MenuLevel.BACK_TO_PARENT, this.backSelection, 0);
        //                 break;
        //             default:
        //                 VerticalList.Model model = selection.model;
        //                 int id = selection.id;
        //                 SmartDashWidgetManager.SmartDashWidgetCache smartDashWidgetCache2 = this.smartDashWidgetCache;
        //                 DashboardWidgetPresenter widgetPresenter = smartDashWidgetCache2 != null ? smartDashWidgetCache2.getWidgetPresenter(id) : null;
        //                 if (model.isOn) {
        //                     model.isOn = false;
        //                     model.subTitle = Companion.getOffLabel();
        //                     SmartDashWidgetManager smartDashWidgetManager2 = this.smartDashWidgetManager;
        //                     if (widgetPresenter != null) {
        //                         str2 = widgetPresenter.getWidgetIdentifier();
        //                         break;
        //                     }
        //                     str2 = "";
        //                     smartDashWidgetManager2.setGaugeOn(str2, false);
        //                 } else {
        //                     model.isOn = true;
        //                     model.subTitle = Companion.getOnLabel();
        //                     SmartDashWidgetManager smartDashWidgetManager3 = this.smartDashWidgetManager;
        //                     if (widgetPresenter != null) {
        //                         str = widgetPresenter.getWidgetIdentifier();
        //                         break;
        //                     }
        //                     str = "";
        //                     smartDashWidgetManager3.setGaugeOn(str, true);
        //                 }
        //                 this.userPreferenceChanged = true;
        //                 this.presenter.refreshDataforPos(selection.pos);
        //                 break;
        //         }
        // }
        // return false;
    }

    // @org.jetbrains.annotations.Nullable
    @DexIgnore
    public IMenu.Menu getType() {
        return IMenu.Menu.MAIN_OPTIONS;
    }

    @DexIgnore
    public boolean isItemClickable(int id, int pos) {
        return true;
    }

    // @org.jetbrains.annotations.Nullable
    @DexIgnore
    public VerticalList.Model getModelfromPos(int pos) {
        throw null;
        // switch (this.gaugeType) {
        //     case CENTER:
        //         return (VerticalList.Model) Companion.getCenterGaugeOptionsList().get(pos);
        //     case SIDE:
        //         return (VerticalList.Model) Companion.getSideGaugesOptionsList().get(pos);
        //     default:
        //         throw new NoWhenBranchMatchedException();
        // }
    }

    @DexIgnore
    public boolean isBindCallsEnabled() {
        return false;
    }

    @DexIgnore
    public void onBindToView(@NotNull VerticalList.Model model, @NotNull View view, int pos, @NotNull VerticalList.ModelState state) {
        // Intrinsics.checkParameterIsNotNull(model, "model");
        // Intrinsics.checkParameterIsNotNull(view, "view");
        // Intrinsics.checkParameterIsNotNull(state, com.amazonaws.mobileconnectors.s3.transferutility.TransferTable.COLUMN_STATE);
    }

    // @org.jetbrains.annotations.Nullable
    @DexIgnore
    public IMenu getChildMenu(@NotNull IMenu parent2, @NotNull String args, @NotNull String path) {
        Intrinsics.checkParameterIsNotNull(parent2, "parent");
        Intrinsics.checkParameterIsNotNull(args, "args");
        Intrinsics.checkParameterIsNotNull(path, "path");
        return null;
    }

    @DexIgnore
    public void onUnload(@NotNull IMenu.MenuLevel level) {
        throw null;
        // Intrinsics.checkParameterIsNotNull(level, "level");
        // DashboardWidgetView dashboardWidgetView = this.gaugeView;
        // Object obj = dashboardWidgetView != null ? dashboardWidgetView.getTag() : null;
        // if (!(obj instanceof DashboardWidgetPresenter)) {
        //     obj = null;
        // }
        // DashboardWidgetPresenter dashboardWidgetPresenter = (DashboardWidgetPresenter) obj;
        // if (dashboardWidgetPresenter != null) {
        //     dashboardWidgetPresenter.setView(null, null);
        // }
        // if (this.userPreferenceChanged) {
        //     Companion.getSLogger().d("User preference has changed " + this.userPreferenceChanged);
        //     this.bus.post(new SmartDashWidgetManager.UserPreferenceChanged());
        // } else {
        //     Companion.getSLogger().d("User preference has not been changed " + this.userPreferenceChanged);
        // }
        // if (this.registered) {
        //     this.bus.unregister(this);
        //     this.registered = false;
        // }
    }

    @DexIgnore
    public void onItemSelected(@NotNull VerticalList.ItemSelectionState selection) {
        Intrinsics.checkParameterIsNotNull(selection, "selection");
        if (Intrinsics.areEqual((Object) this.gaugeType, (Object) DashGaugeConfiguratorMenu.GaugeType.SIDE)) {
            showGauge(selection.pos);
        }
    }

    @DexIgnore
    public final void showGauge(int pos) {
        // DashboardWidgetPresenter newPresenter;
        // if (pos > 0) {
        //     this.vscrollComponent.showSelectedCustomView();
        //     this.vscrollComponent.selectedText.setText(null);
        //     Object tag = this.gaugeView.getTag();
        //     if (!(tag instanceof DashboardWidgetPresenter)) {
        //         tag = null;
        //     }
        //     DashboardWidgetPresenter oldPresenter = (DashboardWidgetPresenter) tag;
        //     SmartDashWidgetManager.SmartDashWidgetCache smartDashWidgetCache2 = this.smartDashWidgetCache;
        //     if (smartDashWidgetCache2 != null) {
        //         newPresenter = smartDashWidgetCache2.getWidgetPresenter(pos - 1);
        //     } else {
        //         newPresenter = null;
        //     }
        //     if (!(oldPresenter == null || oldPresenter == this.presenter || oldPresenter.getWidgetView() != this.gaugeView)) {
        //         oldPresenter.setView(null, null);
        //     }
        //     if (newPresenter != null) {
        //         Bundle args = new Bundle();
        //         args.putInt(DashboardWidgetPresenter.EXTRA_GRAVITY, 0);
        //         args.putBoolean(DashboardWidgetPresenter.EXTRA_IS_ACTIVE, true);
        //         newPresenter.setView(this.gaugeView, args);
        //         newPresenter.setWidgetVisibleToUser(true);
        //     }
        //     this.gaugeView.setTag(newPresenter);
        //     return;
        // }
        // this.vscrollComponent.setSelectedIconColorImage(R.drawable.icon_side_gauges, Companion.getCenterGaugeBackgroundColor(), null, 1.0f);
        // this.vscrollComponent.selectedText.setText(Companion.getSideGaugesTitle());
    }

    // @com.squareup.otto.Subscribe
    @DexIgnore
    public final void onMapEvent(@NotNull MapEvents.ManeuverDisplay event) {
        Intrinsics.checkParameterIsNotNull(event, "event");
        this.smartDashWidgetManager.updateWidget(SmartDashWidgetManager.SPEED_LIMIT_SIGN_GAUGE_ID, Math.round((float) SpeedManager.convert((double) event.currentSpeedLimit, SpeedManager.SpeedUnit.METERS_PER_SECOND, SpeedManager.getInstance().getSpeedUnit())));
    }

    // @com.squareup.otto.Subscribe
    @DexIgnore
    public final void ObdPidChangeEvent(@NotNull ObdManager.ObdPidChangeEvent event) {
        Intrinsics.checkParameterIsNotNull(event, "event");
        switch (event.pid.getId()) {
            case 5:
                this.smartDashWidgetManager.updateWidget(SmartDashWidgetManager.ENGINE_TEMPERATURE_GAUGE_ID, event.pid.getValue());
                return;
            case 47:
                this.smartDashWidgetManager.updateWidget(SmartDashWidgetManager.FUEL_GAUGE_ID, event.pid.getValue());
                return;
            case 256:
                this.smartDashWidgetManager.updateWidget(SmartDashWidgetManager.MPG_AVG_WIDGET_ID, event.pid.getValue());
                return;
            default:
                return;
        }
    }

    // @com.squareup.otto.Subscribe
    @DexIgnore
    public final void onGpsLocationChanged(@NotNull Location location) {
        Intrinsics.checkParameterIsNotNull(location, "location");
        this.headingDataUtil.setHeading((double) location.getBearing());
        this.smartDashWidgetManager.updateWidget(SmartDashWidgetManager.COMPASS_WIDGET_ID, this.headingDataUtil.getHeading());
    }

    // @com.squareup.otto.Subscribe
    @DexIgnore
    public final void onLocationFixChangeEvent(@NotNull MapEvents.LocationFix event) {
        Intrinsics.checkParameterIsNotNull(event, "event");
        if (!event.locationAvailable) {
            this.headingDataUtil.reset();
            this.smartDashWidgetManager.updateWidget(SmartDashWidgetManager.COMPASS_WIDGET_ID, 0);
        }
    }

    // @com.squareup.otto.Subscribe
    @DexIgnore
    public final void onDriverProfileChanged(@NotNull DriverProfileChanged profileChanged) {
        // Intrinsics.checkParameterIsNotNull(profileChanged, "profileChanged");
        // Companion.getSLogger().d("onDriverProfileChanged, reloading the widgets");
        // SmartDashWidgetManager smartDashWidgetManager2 = this.smartDashWidgetManager;
        // if (smartDashWidgetManager2 != null) {
        //     smartDashWidgetManager2.reLoadAvailableWidgets(false);
        // }
    }

    // @com.squareup.otto.Subscribe
    @DexIgnore
    public final void onDriveScoreUpdated(@NotNull TelemetryDataManager.DriveScoreUpdated driveScoreUpdated) {
        Intrinsics.checkParameterIsNotNull(driveScoreUpdated, "driveScoreUpdated");
        this.smartDashWidgetManager.updateWidget(SmartDashWidgetManager.DRIVE_SCORE_GAUGE_ID, driveScoreUpdated);
    }

    @DexIgnore
    public void onScrollIdle() {
    }

    @DexIgnore
    public void onFastScrollStart() {
    }

    @DexIgnore
    public void onFastScrollEnd() {
    }

    @DexIgnore
    public void showToolTip() {
    }

    @DexIgnore
    public boolean isFirstItemEmpty() {
        return false;
    }
}
