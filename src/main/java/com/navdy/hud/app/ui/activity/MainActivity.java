package com.navdy.hud.app.ui.activity;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.util.Base64;
import android.view.View;

import com.navdy.hud.app.util.ReportIssueService;

import java.io.ByteArrayOutputStream;

import lanchon.dexpatcher.annotation.DexAdd;
import lanchon.dexpatcher.annotation.DexAppend;
import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;

@DexEdit
public class MainActivity extends com.navdy.hud.app.ui.activity.HudBaseActivity {
    @DexIgnore
    private static java.lang.String ACTION_PAIRING_CANCEL; // = "android.bluetooth.device.action.PAIRING_CANCEL";
    @DexIgnore
    private android.content.BroadcastReceiver bluetoothReceiver; // = new com.navdy.hud.app.ui.activity.MainActivity.AnonymousClass2();
    //@butterknife.InjectView(2131624246)
    @DexIgnore
    com.navdy.hud.app.view.ContainerView container;
    @DexIgnore
    android.widget.TextView debugText;
    //@javax.inject.Inject
    @DexIgnore
    com.navdy.hud.app.ui.activity.Main.Presenter mainPresenter;
    //@javax.inject.Inject
    @DexIgnore
    com.navdy.hud.app.manager.PairingManager pairingManager;
    //@javax.inject.Inject
    @DexIgnore
    com.navdy.hud.app.device.PowerManager powerManager;

    // @DexAppend
    // MainActivity() {
    //
    // }

    // /* renamed from: com.navdy.hud.app.ui.activity.MainActivity$1 reason: invalid class name */
    // class AnonymousClass1 implements java.lang.Runnable {
    //     AnonymousClass1() {
    //     }
    //
    //     public void run() {
    //         ((com.navdy.hud.app.HudApplication) com.navdy.hud.app.ui.activity.MainActivity.this.getApplication()).initHudApp();
    //     }
    // }
    //
    // /* renamed from: com.navdy.hud.app.ui.activity.MainActivity$2 reason: invalid class name */
    // class AnonymousClass2 extends android.content.BroadcastReceiver {
    //     AnonymousClass2() {
    //     }
    //
    //     public void onReceive(android.content.Context context, android.content.Intent intent) {
    //         java.lang.Object obj;
    //         java.lang.String action = intent.getAction();
    //         android.bluetooth.BluetoothDevice bluetoothDevice = (android.bluetooth.BluetoothDevice) intent.getParcelableExtra("android.bluetooth.device.extra.DEVICE");
    //         com.squareup.otto.Bus bus = com.navdy.hud.app.ui.activity.MainActivity.this.mainPresenter.bus;
    //         if (action.equals("android.bluetooth.device.action.BOND_STATE_CHANGED")) {
    //             int intExtra = intent.getIntExtra("android.bluetooth.device.extra.BOND_STATE", com.navdy.obd.Pid.NO_DATA);
    //             int intExtra2 = intent.getIntExtra("android.bluetooth.device.extra.PREVIOUS_BOND_STATE", com.navdy.obd.Pid.NO_DATA);
    //             com.navdy.service.library.log.Logger access$000 = com.navdy.hud.app.ui.activity.MainActivity.this.logger;
    //             java.lang.StringBuilder sb = new java.lang.StringBuilder();
    //             sb.append("Bond state change - device:");
    //             sb.append(bluetoothDevice);
    //             sb.append(" state:");
    //             sb.append(intExtra);
    //             sb.append(" prevState:");
    //             sb.append(intExtra2);
    //             access$000.d(sb.toString());
    //             bus.post(new com.navdy.hud.app.ui.component.bluetooth.BluetoothPairing.BondStateChange(bluetoothDevice, intExtra2, intExtra));
    //             return;
    //         }
    //         boolean z = false;
    //         if (action.equals("android.bluetooth.adapter.action.STATE_CHANGED")) {
    //             int intExtra3 = intent.getIntExtra("android.bluetooth.adapter.extra.STATE", com.navdy.obd.Pid.NO_DATA);
    //             if (intExtra3 == 12 || intExtra3 == 10) {
    //                 if (intExtra3 == 12) {
    //                     z = true;
    //                 }
    //                 obj = new com.navdy.hud.app.event.InitEvents.BluetoothStateChanged(z);
    //             }
    //         } else if (action.equals("android.bluetooth.device.action.PAIRING_REQUEST")) {
    //             abortBroadcast();
    //             if (com.navdy.hud.app.device.dial.DialManager.getInstance().isDialDevice(bluetoothDevice)) {
    //                 com.navdy.service.library.log.Logger access$100 = com.navdy.hud.app.ui.activity.MainActivity.this.logger;
    //                 java.lang.StringBuilder sb2 = new java.lang.StringBuilder();
    //                 sb2.append("Pairing request from dial device, ignore [");
    //                 sb2.append(bluetoothDevice.getName());
    //                 sb2.append("]");
    //                 access$100.w(sb2.toString());
    //                 return;
    //             }
    //             int intExtra4 = intent.getIntExtra("android.bluetooth.device.extra.PAIRING_VARIANT", com.navdy.obd.Pid.NO_DATA);
    //             int intExtra5 = intent.getIntExtra("android.bluetooth.device.extra.PAIRING_KEY", com.navdy.obd.Pid.NO_DATA);
    //             com.navdy.service.library.log.Logger access$200 = com.navdy.hud.app.ui.activity.MainActivity.this.logger;
    //             java.lang.StringBuilder sb3 = new java.lang.StringBuilder();
    //             sb3.append("Showing pairing request from ");
    //             sb3.append(bluetoothDevice);
    //             sb3.append(" of type:");
    //             sb3.append(intExtra4);
    //             sb3.append(" with pin:");
    //             sb3.append(intExtra5);
    //             access$200.d(sb3.toString());
    //             android.os.Bundle bundle = new android.os.Bundle();
    //             bundle.putParcelable(com.navdy.hud.app.ui.component.bluetooth.BluetoothPairing.ARG_DEVICE, bluetoothDevice);
    //             bundle.putInt(com.navdy.hud.app.ui.component.bluetooth.BluetoothPairing.ARG_VARIANT, intExtra4);
    //             bundle.putInt(com.navdy.hud.app.ui.component.bluetooth.BluetoothPairing.ARG_PIN, intExtra5);
    //             boolean isAutoPairing = com.navdy.hud.app.ui.activity.MainActivity.this.pairingManager.isAutoPairing();
    //             android.os.Bundle resultExtras = getResultExtras(false);
    //             if (!isAutoPairing && resultExtras != null) {
    //                 isAutoPairing = resultExtras.getBoolean("auto", false);
    //             }
    //             bundle.putBoolean("auto", isAutoPairing);
    //             com.navdy.hud.app.ui.component.bluetooth.BluetoothPairing.showBluetoothPairingToast(bundle);
    //             return;
    //         } else {
    //             if (action.equals(com.navdy.hud.app.ui.activity.MainActivity.ACTION_PAIRING_CANCEL)) {
    //                 abortBroadcast();
    //                 obj = new com.navdy.hud.app.ui.component.bluetooth.BluetoothPairing.PairingCancelled(bluetoothDevice);
    //             }
    //         }
    //         bus.post(obj);
    //     }
    // }
    //
    // protected mortar.Blueprint getBlueprint() {
    //     return new com.navdy.hud.app.ui.activity.Main();
    // }
    //
    // public void onConfigurationChanged(android.content.res.Configuration configuration) {
    //     com.navdy.service.library.log.Logger logger = this.logger;
    //     java.lang.StringBuilder sb = new java.lang.StringBuilder();
    //     sb.append("onConfigurationChanged:");
    //     sb.append(configuration);
    //     logger.v(sb.toString());
    //     super.onConfigurationChanged(configuration);
    // }
    //
    // protected void onCreate(android.os.Bundle bundle) {
    //     super.onCreate(bundle);
    //     if (com.navdy.hud.app.util.DeviceUtil.isNavdyDevice()) {
    //         setContentView(com.navdy.hud.app.R.layout.main_view_lyt);
    //     } else {
    //         setContentView(com.navdy.hud.app.R.layout.activity_fullscreen);
    //         this.debugText = (android.widget.TextView) findViewById(com.navdy.hud.app.R.id.debug_text);
    //         android.content.res.Resources resources = getResources();
    //         int dimension = (int) resources.getDimension(com.navdy.hud.app.R.dimen.dashboard_width);
    //         int dimension2 = (int) resources.getDimension(com.navdy.hud.app.R.dimen.dashboard_height);
    //         java.lang.StringBuilder sb = new java.lang.StringBuilder();
    //         sb.append("Device:  ");
    //         sb.append(android.os.Build.DEVICE);
    //         sb.append(com.navdy.hud.app.framework.glance.GlanceConstants.COLON_SEPARATOR);
    //         sb.append(android.os.Build.MODEL);
    //         sb.append(" width = ");
    //         sb.append(dimension);
    //         sb.append(" height = ");
    //         sb.append(dimension2);
    //         sb.append(" scale factor = ");
    //         sb.append(dimension / 640);
    //         this.debugText.setText(sb.toString());
    //     }
    //     butterknife.ButterKnife.inject(this);
    //     android.preference.PreferenceManager.setDefaultValues(this, "HUD", 0, com.navdy.hud.app.R.xml.developer_preferences, false);
    //     makeImmersive(!this.powerManager.inQuietMode());
    //     android.content.IntentFilter intentFilter = new android.content.IntentFilter();
    //     intentFilter.addAction("android.bluetooth.device.action.PAIRING_REQUEST");
    //     intentFilter.addAction("android.bluetooth.device.action.BOND_STATE_CHANGED");
    //     intentFilter.addAction("android.bluetooth.adapter.action.STATE_CHANGED");
    //     intentFilter.addAction(ACTION_PAIRING_CANCEL);
    //     registerReceiver(this.bluetoothReceiver, intentFilter);
    //     this.mainPresenter.bus.register(this);
    // }
    //
    // protected void onDestroy() {
    //     super.onDestroy();
    //     unregisterReceiver(this.bluetoothReceiver);
    // }
    //
    // @com.squareup.otto.Subscribe
    // public void onInitEvent(com.navdy.hud.app.event.InitEvents.InitPhase initPhase) {
    //     if (initPhase.phase == com.navdy.hud.app.event.InitEvents.Phase.POST_START) {
    //         com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.ui.activity.MainActivity.AnonymousClass1(), 1);
    //     }
    // }
    //
    // @com.squareup.otto.Subscribe
    // public void onWakeup(com.navdy.hud.app.event.Wakeup wakeup) {
    //     this.logger.v("enableNormalMode(): FLAG_KEEP_SCREEN_ON set");
    //     getWindow().addFlags(128);
    // }

    @SuppressLint("MissingSuperCall")
    @DexAppend
    protected void onCreate(android.os.Bundle bundle) {
        ReportIssueService.mainActivity = this;
    }

    @DexIgnore
    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
        if (z) {
            makeImmersive(!this.powerManager.inQuietMode());
        }
    }

    @DexAdd
    public String takeScreenshot() {
        try {
            View view = getWindow().getDecorView();
            Bitmap bitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bitmap);
            view.layout(view.getLeft(), view.getTop(), view.getRight(), view.getBottom());
            view.draw(canvas);
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);
            return Base64.encodeToString(outputStream.toByteArray(), Base64.DEFAULT);
        }catch (Exception e){
            //failed to print screen, return N/A
        }
        return null;
    }

}
