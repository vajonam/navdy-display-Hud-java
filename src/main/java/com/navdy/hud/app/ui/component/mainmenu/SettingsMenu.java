package com.navdy.hud.app.ui.component.mainmenu;

import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;

import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.hud.app.device.dial.DialManager;
import com.navdy.hud.app.event.Shutdown;
import com.navdy.hud.app.manager.UpdateReminderManager;
import com.navdy.hud.app.screen.WelcomeScreen;
import com.navdy.hud.app.ui.component.vlist.VerticalList;
import com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder;
import com.navdy.hud.app.ui.component.vlist.viewholder.IconsTwoViewHolder;
import com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils;
import com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex;
import com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent;
import com.navdy.hud.app.util.OTAUpdateService;
import com.navdy.service.library.events.ui.Screen;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Bus;

import java.util.ArrayList;
import java.util.List;

import alelec.navdy.hud.app.R;
import lanchon.dexpatcher.annotation.DexAdd;
import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit
public class SettingsMenu implements IMenu {
    @DexIgnore
    private static /* final */ VerticalList.Model back;
    @DexIgnore
    private static /* final */ VerticalList.Model brightness;
    @DexIgnore
    private static /* final */ VerticalList.Model connectPhone;
    @DexIgnore
    private static /* final */ VerticalList.Model dialFwUpdate;
    @DexIgnore
    private static /* final */ VerticalList.Model displayFwUpdate;
    @DexIgnore
    private static /* final */ VerticalList.Model factoryReset;
    @DexIgnore
    private static /* final */ VerticalList.Model learningGestures;
    @DexIgnore
    private static /* final */ Resources resources; // = com.navdy.hud.app.HudApplication.getAppContext().getResources();
    @DexIgnore
    private static /* final */ Logger sLogger; // = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.mainmenu.SettingsMenu.class);
    @DexIgnore
    private static /* final */ String setting; // = resources.getString(com.navdy.hud.app.R.string.carousel_menu_settings_title);
    @DexIgnore
    private static /* final */ int settingColor; // = resources.getColor(com.navdy.hud.app.R.color.mm_settings);
    @DexIgnore
    private static /* final */ VerticalList.Model shutdown;
    @DexIgnore
    private static /* final */ VerticalList.Model systemInfo; // = com.navdy.hud.app.ui.component.vlist.viewholder.IconsTwoViewHolder.buildModel(com.navdy.hud.app.R.id.main_menu_system_info, com.navdy.hud.app.R.drawable.icon_settings_navdy_data, com.navdy.hud.app.R.drawable.icon_settings_navdy_data_sm, resources.getColor(com.navdy.hud.app.R.color.mm_settings_sys_info), -1, resources.getString(com.navdy.hud.app.R.string.carousel_menu_system_info_title), null);
    @DexAdd
    private LanguageMenu languageMenu;
    @DexAdd
    private static final VerticalList.Model language = IconBkColorViewHolder.buildModel(
            R.id.settings_menu_language,
            R.drawable.icon_software_update_2,
            resources.getColor(R.color.mm_settings_update_display),
            MainMenu.bkColorUnselected,
            resources.getColor(R.color.mm_settings_update_display),
            resources.getString(R.string.language),
            null);
    @DexIgnore
    private int backSelection;
    @DexIgnore
    private int backSelectionId;
    @DexIgnore
    private Bus bus;
    @DexIgnore
    private List<VerticalList.Model> cachedList;
    @DexIgnore
    private IMenu parent;
    @DexIgnore
    private MainMenuScreen2.Presenter presenter;
    @DexIgnore
    private SystemInfoMenu systemInfoMenu;
    @DexIgnore
    private VerticalMenuComponent vscrollComponent;

    /* @DexIgnore
    static {
        java.lang.String title = resources.getString(com.navdy.hud.app.R.string.back);
        int fluctuatorColor = resources.getColor(com.navdy.hud.app.R.color.mm_back);
        back = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.menu_back, com.navdy.hud.app.R.drawable.icon_mm_back, fluctuatorColor, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, fluctuatorColor, title, null);
        java.lang.String title2 = resources.getString(com.navdy.hud.app.R.string.carousel_settings_brightness);
        int fluctuatorColor2 = resources.getColor(com.navdy.hud.app.R.color.mm_settings_brightness);
        brightness = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.settings_menu_brightness, com.navdy.hud.app.R.drawable.icon_settings_brightness_2, fluctuatorColor2, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, fluctuatorColor2, title2, null);
        java.lang.String title3 = resources.getString(com.navdy.hud.app.R.string.carousel_settings_connect_phone_title);
        int fluctuatorColor3 = resources.getColor(com.navdy.hud.app.R.color.mm_connnect_phone);
        connectPhone = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.settings_menu_connect_phone, com.navdy.hud.app.R.drawable.icon_settings_connect_phone_2, fluctuatorColor3, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, fluctuatorColor3, title3, null);
        java.lang.String title4 = resources.getString(com.navdy.hud.app.R.string.carousel_settings_learning_gesture_title);
        int fluctuatorColor4 = resources.getColor(com.navdy.hud.app.R.color.mm_settings_learn_gestures);
        learningGestures = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.settings_menu_learning_gestures, com.navdy.hud.app.R.drawable.icon_settings_learn_gestures_2, fluctuatorColor4, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, fluctuatorColor4, title4, null);
        java.lang.String title5 = resources.getString(com.navdy.hud.app.R.string.carousel_settings_software_update_title);
        int fluctuatorColor5 = resources.getColor(com.navdy.hud.app.R.color.mm_settings_update_display);
        displayFwUpdate = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.settings_menu_software_update, com.navdy.hud.app.R.drawable.icon_software_update_2, fluctuatorColor5, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, fluctuatorColor5, title5, null);
        java.lang.String title6 = resources.getString(com.navdy.hud.app.R.string.carousel_settings_dial_update_title);
        int fluctuatorColor6 = resources.getColor(com.navdy.hud.app.R.color.mm_settings_update_dial);
        dialFwUpdate = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.settings_menu_dial_update, com.navdy.hud.app.R.drawable.icon_dial_update_2, fluctuatorColor6, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, fluctuatorColor6, title6, null);
        java.lang.String title7 = resources.getString(com.navdy.hud.app.R.string.carousel_settings_shutdown_title);
        int fluctuatorColor7 = resources.getColor(com.navdy.hud.app.R.color.mm_settings_shutdown);
        shutdown = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.settings_menu_shutdown, com.navdy.hud.app.R.drawable.icon_settings_shutdown_2, fluctuatorColor7, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, fluctuatorColor7, title7, null);
        java.lang.String title8 = resources.getString(com.navdy.hud.app.R.string.carousel_settings_factory_reset_title);
        int fluctuatorColor8 = resources.getColor(com.navdy.hud.app.R.color.mm_settings_factory_reset);
        factoryReset = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.settings_menu_factory_reset, com.navdy.hud.app.R.drawable.icon_settings_factory_reset_2, fluctuatorColor8, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, fluctuatorColor8, title8, null);
    } */

    @DexIgnore
    public SettingsMenu(Bus bus2, VerticalMenuComponent vscrollComponent2, MainMenuScreen2.Presenter presenter2, IMenu parent2) {
        this.bus = bus2;
        this.vscrollComponent = vscrollComponent2;
        this.presenter = presenter2;
        this.parent = parent2;
    }

    @DexReplace
    public List<VerticalList.Model> getItems() {
        if (this.cachedList != null) {
            return this.cachedList;
        }
        List<VerticalList.Model> list = new ArrayList<>();
        list.add(back);
        list.add(brightness);
        list.add(language);
        list.add(connectPhone);
        list.add(learningGestures);
        // if (OTAUpdateService.isUpdateAvailable()) {
        //     list.add(displayFwUpdate);
        // }
        DialManager dialManager = DialManager.getInstance();
        if (dialManager.isDialConnected() && dialManager.getDialFirmwareUpdater().getVersions().isUpdateAvailable()) {
            list.add(dialFwUpdate);
        }
        list.add(shutdown);
        list.add(factoryReset);
        list.add(systemInfo);
        this.cachedList = list;
        return list;
    }

    @DexIgnore
    public int getInitialSelection() {
        return 1;
    }

    @DexIgnore
    public VerticalFastScrollIndex getScrollIndex() {
        return null;
    }

    @DexIgnore
    public void setBackSelectionPos(int n) {
        this.backSelection = n;
    }

    @DexIgnore
    public void setBackSelectionId(int id) {
        this.backSelectionId = id;
    }

    @DexIgnore
    public void setSelectedIcon() {
        this.vscrollComponent.setSelectedIconColorImage(R.drawable.icon_mm_settings_2, settingColor, null, 1.0f);
        this.vscrollComponent.selectedText.setText(setting);
    }

    @DexIgnore
    public boolean isItemClickable(int id, int pos) {
        return true;
    }

    @DexIgnore
    public VerticalList.Model getModelfromPos(int pos) {
        if (this.cachedList == null || this.cachedList.size() <= pos) {
            return null;
        }
        return this.cachedList.get(pos);
    }

    @DexIgnore
    public boolean isBindCallsEnabled() {
        return false;
    }

    @DexIgnore
    public void onBindToView(VerticalList.Model model, View view, int pos, VerticalList.ModelState state) {
    }

    @DexIgnore
    public IMenu getChildMenu(IMenu parent2, String args, String path) {
        return null;
    }

    @DexIgnore
    public void onUnload(IMenu.MenuLevel level) {
    }

    @DexIgnore
    public void onItemSelected(VerticalList.ItemSelectionState selection) {
    }

    @DexIgnore
    public void onScrollIdle() {
    }

    @DexIgnore
    public void onFastScrollStart() {
    }

    @DexIgnore
    public void onFastScrollEnd() {
    }

    @DexIgnore
    public void showToolTip() {
    }

    @DexIgnore
    public boolean isFirstItemEmpty() {
        return true;
    }

    @DexReplace
    public boolean selectItem(VerticalList.ItemSelectionState selection) {
        sLogger.v("select id:" + selection.id + " pos:" + selection.pos);
        switch (selection.id) {
            case R.id.main_menu_system_info /*2131624003*/:
                sLogger.v("system info");
                // AnalyticsSupport.recordMenuSelection("system_info");
                if (this.systemInfoMenu == null) {
                    this.systemInfoMenu = new SystemInfoMenu(this.bus, this.vscrollComponent, this.presenter, this);
                }
                this.presenter.loadMenu(this.systemInfoMenu, IMenu.MenuLevel.SUB_LEVEL, selection.pos, 1, true);
                break;
            case R.id.menu_back /*2131624007*/:
                sLogger.v("back");
                // AnalyticsSupport.recordMenuSelection("back");
                this.presenter.loadMenu(this.parent, IMenu.MenuLevel.BACK_TO_PARENT, this.backSelection, this.backSelectionId);
                this.backSelectionId = 0;
                break;
            case R.id.settings_menu_auto_brightness /*2131624058*/:
                sLogger.v("auto brightness");
                // AnalyticsSupport.recordMenuSelection("auto_brightness");
                this.presenter.performSelectionAnimation(new VerticalAnimationUtils.ActionRunnable(this.bus, Screen.SCREEN_AUTO_BRIGHTNESS));
                break;
            case R.id.settings_menu_brightness /*2131624060*/:
                sLogger.v("brightness");
                // AnalyticsSupport.recordMenuSelection("brightness");
                this.presenter.performSelectionAnimation(new VerticalAnimationUtils.ActionRunnable(this.bus, Screen.SCREEN_BRIGHTNESS));
                break;
            case R.id.settings_menu_connect_phone /*2131624061*/:
                sLogger.v("connect phone");
                // AnalyticsSupport.recordMenuSelection("connect_phone");
                Bundle args = new Bundle();
                args.putString(WelcomeScreen.ARG_ACTION, WelcomeScreen.ACTION_SWITCH_PHONE);
                this.presenter.performSelectionAnimation(new VerticalAnimationUtils.ActionRunnable(this.bus, Screen.SCREEN_WELCOME, args, false));
                break;
            case R.id.settings_menu_dial_pairing /*2131624062*/:
                sLogger.v("Dial pairing");
                // AnalyticsSupport.recordMenuSelection("dial_pairing");
                this.presenter.performSelectionAnimation(new VerticalAnimationUtils.ActionRunnable(this.bus, Screen.SCREEN_DIAL_PAIRING));
                break;
            case R.id.settings_menu_dial_update /*2131624063*/:
                sLogger.v("dial update");
                // AnalyticsSupport.recordMenuSelection("dial_update");
                Bundle args2 = new Bundle();
                args2.putBoolean(UpdateReminderManager.EXTRA_UPDATE_REMINDER, false);
                this.presenter.performSelectionAnimation(new VerticalAnimationUtils.ActionRunnable(this.bus, Screen.SCREEN_DIAL_UPDATE_CONFIRMATION, args2, false));
                break;
            case R.id.settings_menu_factory_reset /*2131624065*/:
                sLogger.v("factory reset");
                // AnalyticsSupport.recordMenuSelection("factory_reset");
                this.presenter.performSelectionAnimation(new VerticalAnimationUtils.ActionRunnable(this.bus, Screen.SCREEN_FACTORY_RESET));
                break;
            case R.id.settings_menu_learning_gestures /*2131624066*/:
                sLogger.v("Learning gestures");
                // AnalyticsSupport.recordMenuSelection("learning_gestures");
                this.presenter.performSelectionAnimation(new VerticalAnimationUtils.ActionRunnable(this.bus, Screen.SCREEN_GESTURE_LEARNING));
                break;
            case R.id.settings_menu_shutdown /*2131624067*/:
                sLogger.v("shutdown");
                // AnalyticsSupport.recordMenuSelection("shutdown");
                this.presenter.performSelectionAnimation(new VerticalAnimationUtils.ActionRunnable(this.bus, Screen.SCREEN_SHUTDOWN_CONFIRMATION, Shutdown.Reason.MENU.asBundle(), false));
                break;
            case R.id.settings_menu_software_update /*2131624068*/:
                sLogger.v("software update");
                // AnalyticsSupport.recordMenuSelection("software_update");
                Bundle args3 = new Bundle();
                args3.putBoolean(UpdateReminderManager.EXTRA_UPDATE_REMINDER, false);
                this.presenter.performSelectionAnimation(new VerticalAnimationUtils.ActionRunnable(this.bus, Screen.SCREEN_OTA_CONFIRMATION, args3, false));
                break;
            case R.id.settings_menu_language:
                sLogger.v("language settings");
                if (this.languageMenu == null) {
                    this.languageMenu = new LanguageMenu(this.bus, this.vscrollComponent, this.presenter, this);
                }
                this.presenter.loadMenu(this.languageMenu, IMenu.MenuLevel.SUB_LEVEL, selection.pos, 0, false);

                // createChildMenu(com.navdy.hud.app.ui.component.mainmenu.IMenu.Menu.SETTINGS, null);
                // this.presenter.loadMenu(this.settingsMenu, com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel.SUB_LEVEL, selection.pos, 0);

                break;
        }
        return true;
    }

    @DexIgnore
    public IMenu.Menu getType() {
        return IMenu.Menu.SETTINGS;
    }
}
