package com.navdy.hud.app.ui.component;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;

import com.navdy.hud.app.ui.framework.DefaultAnimationListener;

import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit
public class ShrinkingBorderView extends View {
    @DexIgnore
    private static /* final */ int FORCE_COLLAPSE_INTERVAL; // = 300;
    @DexIgnore
    private static /* final */ int RESET_TIMEOUT_INITIAL_INTERVAL; // = 300;
    @DexIgnore
    private static /* final */ int RESET_TIMEOUT_INTERVAL; // = 100;
    @DexIgnore
    private ValueAnimator animator;
    @DexIgnore
    private Animator.AnimatorListener animatorListener;
    @DexIgnore
    private Animator.AnimatorListener animatorResetListener;
    @DexIgnore
    private ValueAnimator.AnimatorUpdateListener animatorResetUpdateListener;
    @DexIgnore
    private ValueAnimator.AnimatorUpdateListener animatorUpdateListener;
    @DexIgnore
    private Runnable forceCompleteCallback;
    @DexIgnore
    private Animator.AnimatorListener forceCompleteListener;
    @DexIgnore
    private Interpolator interpolator;
    @DexIgnore
    private ShrinkingBorderView.IListener listener;
    @DexIgnore
    private Interpolator restoreInterpolator;
    @DexIgnore
    private int timeoutVal;

    // @DexIgnore
    // class Anon1 extends DefaultAnimationListener {
    //     @DexIgnore
    //     Anon1() {
    //     }
    //
    //     @DexIgnore
    //     public void onAnimationEnd(Animator animation) {
    //         ShrinkingBorderView.this.animator = null;
    //         if (ShrinkingBorderView.this.forceCompleteCallback != null) {
    //             ShrinkingBorderView.this.forceCompleteCallback.run();
    //             ShrinkingBorderView.this.forceCompleteCallback = null;
    //         }
    //     }
    // }
    //
    // @DexIgnore
    // class Anon2 extends DefaultAnimationListener {
    //     @DexIgnore
    //     Anon2() {
    //     }
    //
    //     @DexIgnore
    //     public void onAnimationEnd(Animator animation) {
    //         ShrinkingBorderView.this.animator = null;
    //         if (ShrinkingBorderView.this.listener != null) {
    //             ShrinkingBorderView.this.listener.timeout();
    //         }
    //     }
    // }
    //
    // @DexIgnore
    // class Anon3 implements ValueAnimator.AnimatorUpdateListener {
    //     @DexIgnore
    //     Anon3() {
    //     }
    //
    //     @DexIgnore
    //     public void onAnimationUpdate(ValueAnimator animation) {
    //         ViewGroup.MarginLayoutParams lytParams = (ViewGroup.MarginLayoutParams) ShrinkingBorderView.this.getLayoutParams();
    //         int val = (Integer) animation.getAnimatedValue();
    //         if (lytParams.height != val) {
    //             lytParams.height = val;
    //             ShrinkingBorderView.this.requestLayout();
    //         }
    //     }
    // }
    //
    // @DexIgnore
    // class Anon4 extends DefaultAnimationListener {
    //     @DexIgnore
    //     Anon4() {
    //     }
    //
    //     @DexIgnore
    //     public void onAnimationEnd(Animator animation) {
    //         ShrinkingBorderView.this.animator = null;
    //         ShrinkingBorderView.this.startTimeout(ShrinkingBorderView.this.timeoutVal, true);
    //     }
    // }
    //
    // @DexIgnore
    // class Anon5 implements ValueAnimator.AnimatorUpdateListener {
    //     @DexIgnore
    //     Anon5() {
    //     }
    //
    //     @DexIgnore
    //     public void onAnimationUpdate(ValueAnimator animation) {
    //         ((ViewGroup.MarginLayoutParams) ShrinkingBorderView.this.getLayoutParams()).height = (Integer) animation.getAnimatedValue();
    //         ShrinkingBorderView.this.requestLayout();
    //     }
    // }

    @DexIgnore
    public interface IListener {
        void timeout();
    }

    @DexIgnore
    public ShrinkingBorderView(Context context) {
        this(context, null);
    }

    @DexIgnore
    public ShrinkingBorderView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    @DexIgnore
    public ShrinkingBorderView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        // this.restoreInterpolator = new LinearInterpolator();
        // this.interpolator = new AccelerateInterpolator();
        // this.forceCompleteListener = new ShrinkingBorderView.Anon1();
        // this.animatorListener = new ShrinkingBorderView.Anon2();
        // this.animatorUpdateListener = new ShrinkingBorderView.Anon3();
        // this.animatorResetListener = new ShrinkingBorderView.Anon4();
        // this.animatorResetUpdateListener = new ShrinkingBorderView.Anon5();
    }

    @DexIgnore
    public void setListener(ShrinkingBorderView.IListener listener2) {
        this.listener = listener2;
    }

    @DexIgnore
    public void startTimeout(int timeout) {
        startTimeout(timeout, false);
    }

    @DexReplace
    private void startTimeout(int timeout, boolean reset) {
        if (timeout != 0 && this.animator == null) {
            this.timeoutVal = timeout;
            if (reset) {
                this.animator = ValueAnimator.ofInt(((ViewGroup.MarginLayoutParams) getLayoutParams()).height, 0);
                this.animator.addUpdateListener(this.animatorUpdateListener);
                this.animator.addListener(this.animatorListener);
                this.animator.setDuration((long) timeout);
                this.animator.setInterpolator(this.interpolator);
                this.animator.start();
                return;
            }
            ((ViewGroup.MarginLayoutParams) getLayoutParams()).height = 0;
            requestLayout();
            resetTimeout(true);
        }
    }

    @DexIgnore
    public void resetTimeout() {
        resetTimeout(false);
    }

    @DexReplace
    public void resetTimeout(boolean initial) {
        int interval;
        clearAnimator();
        if (initial) {
            interval = 300;
        } else {
            interval = 100;
        }
        ViewGroup.MarginLayoutParams lytParams = (ViewGroup.MarginLayoutParams) getLayoutParams();
        this.animator = ValueAnimator.ofInt(lytParams.height, ((ViewGroup) getParent()).getHeight());
        this.animator.addUpdateListener(this.animatorResetUpdateListener);
        this.animator.addListener(this.animatorResetListener);
        this.animator.setDuration((long) interval);
        this.animator.setInterpolator(this.restoreInterpolator);
        if (initial) {
            this.animator.setStartDelay(300);
        }
        this.animator.start();
    }

    @DexIgnore
    public void stopTimeout(boolean force, Runnable forceCompleteCallback2) {
        if (force) {
            clearAnimator();
            if (forceCompleteCallback2 != null) {
                this.forceCompleteCallback = forceCompleteCallback2;
                this.animator = ValueAnimator.ofInt(((ViewGroup.MarginLayoutParams) getLayoutParams()).height, 0);
                this.animator.addUpdateListener(this.animatorResetUpdateListener);
                this.animator.addListener(this.forceCompleteListener);
                this.animator.setDuration(300);
                this.animator.setInterpolator(this.interpolator);
                this.animator.start();
                return;
            }
            ((ViewGroup.MarginLayoutParams) getLayoutParams()).height = 0;
            requestLayout();
        } else if (this.animator == null) {
            ((ViewGroup.MarginLayoutParams) getLayoutParams()).height = 0;
            requestLayout();
        }
    }

    @DexIgnore
    public boolean isVisible() {
        return ((ViewGroup.MarginLayoutParams) getLayoutParams()).height != 0;
    }

    @DexIgnore
    private void clearAnimator() {
        if (this.animator != null) {
            this.animator.removeAllListeners();
            this.animator.cancel();
            this.animator = null;
            this.forceCompleteCallback = null;
        }
    }
}
