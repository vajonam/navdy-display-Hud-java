package com.navdy.hud.app.ui.component.homescreen;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.Resources;
import android.location.Location;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.navdy.hud.app.R;
import com.navdy.hud.app.config.SettingsManager;
import com.navdy.hud.app.device.gps.GpsUtils;
import com.navdy.hud.app.event.DriverProfileChanged;
import com.navdy.hud.app.framework.DriverProfileHelper;
import com.navdy.hud.app.manager.InputManager;
import com.navdy.hud.app.manager.SpeedManager;
import com.navdy.hud.app.maps.MapEvents;
import com.navdy.hud.app.maps.MapSettings;
import com.navdy.hud.app.maps.NavigationMode;
import com.navdy.hud.app.obd.ObdManager;
import com.navdy.hud.app.profile.NavdyPreferences;
import com.navdy.hud.app.ui.component.dashboard.GaugeViewPager;
import com.navdy.hud.app.ui.component.dashboard.IDashboardOptionsAdapter;
import com.navdy.hud.app.ui.framework.DefaultAnimationListener;
import com.navdy.hud.app.util.HeadingDataUtil;
import com.navdy.hud.app.view.DashboardWidgetPresenter;
import com.navdy.hud.app.view.DashboardWidgetView;
import com.navdy.hud.app.view.GaugeView;
import com.navdy.hud.app.view.MainView;
import com.navdy.hud.app.view.SpeedometerGaugePresenter2;
import com.navdy.hud.app.view.TachometerGaugePresenter;
import com.navdy.obd.PidSet;
import com.navdy.obd.Pids;
import com.navdy.service.library.events.preferences.DashboardPreferences;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Subscribe;

import static com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.AMBIENT_TEMPERATURE_GAUGE_ID;
import static com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.ANALOG_CLOCK_WIDGET_ID;
import static com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.COMPASS_WIDGET_ID;
import static com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.ENGINE_OIL_TEMPERATURE_GAUGE_ID;
import static com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.ENGINE_TEMPERATURE_GAUGE_ID;
import static com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.FUEL_GAUGE_ID;
import static com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.FUEL_GAUGE_RANGE_ID;
import static com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.INTAKE_PRESSURE_GAUGE_ID;
import static com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.MPG_AVG_WIDGET_ID;
import static com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.TRAFFIC_INCIDENT_GAUGE_ID;

import lanchon.dexpatcher.annotation.DexAction;
import lanchon.dexpatcher.annotation.DexAdd;
import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit(defaultAction = DexAction.ADD)
public class SmartDashView extends BaseSmartDashView implements IDashboardOptionsAdapter {
    @DexIgnore
    public static /* final */ int HEADING_EXPIRE_INTERVAL; // = 3000;
    @DexIgnore
    public static /* final */ long MINIMUM_INTERVAL_BETWEEN_RECORDING_USER_PREFERENCE; // = java.util.concurrent.TimeUnit.MINUTES.toMillis(15);
    @DexIgnore
    public static /* final */ float NAVIGATION_MODE_SCALE_FACTOR; // = 0.8f;
    @DexIgnore
    private static /* final */ boolean SHOW_ETA; // = false;
    @DexIgnore
    private static /* final */ Logger sLogger; // = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.homescreen.SmartDashView.class);
    @DexIgnore
    private int activeModeGaugeMargin;
    @DexIgnore
    private String activeRightGaugeId;
    @DexIgnore
    private String activeleftGaugeId;
    // @butterknife.InjectView(2131624372)
    @DexIgnore
    TextView etaAmPm;
    // @butterknife.InjectView(2131624371)
    @DexIgnore
    TextView etaText;
    @DexIgnore
    private int fullModeGaugeTopMargin;
    @DexAdd
    private NavdyPreferences globalPreferences;
    @DexIgnore
    private HeadingDataUtil headingDataUtil; // = new com.navdy.hud.app.util.HeadingDataUtil();
    @DexIgnore
    public boolean isShowingDriveScoreGauge;
    @DexIgnore
    private String lastETA;
    @DexIgnore
    private String lastETA_AmPm;
    @DexIgnore
    private double lastHeading; // = 0.0d;
    @DexIgnore
    private long lastHeadingSampleTime; // = 0;
    @DexIgnore
    private long lastUserPreferenceRecordedTime; // = 0;
    // @butterknife.InjectView(2131624370)
    @DexIgnore
    ViewGroup mEtaLayout;
    @DexIgnore
    SmartDashView.GaugeViewPagerAdapter mLeftAdapter;
    // @butterknife.InjectView(2131624366)
    @DexIgnore
    GaugeViewPager mLeftGaugeViewPager;
    // @butterknife.InjectView(2131624368)
    @DexIgnore
    GaugeView mMiddleGaugeView;
    @DexIgnore
    SmartDashView.GaugeViewPagerAdapter mRightAdapter;
    // @butterknife.InjectView(2131624369)
    @DexIgnore
    GaugeViewPager mRightGaugeViewPager;
    @DexIgnore
    private SmartDashWidgetManager mSmartDashWidgetManager;
    @DexIgnore
    private int middleGauge; // = 1;
    @DexIgnore
    private int middleGaugeShrinkEarlyTbtOffset;
    @DexIgnore
    private int middleGaugeShrinkMargin;
    @DexIgnore
    private int middleGaugeShrinkModeMargin;
    @DexIgnore
    private boolean paused;
    @DexIgnore
    private int scrollableSide; // = 1;
    @DexIgnore
    SpeedometerGaugePresenter2 speedometerGaugePresenter2;
    @DexIgnore
    TachometerGaugePresenter tachometerPresenter;

    @DexIgnore
    class Anon2 {
        final /* synthetic */ HomeScreenView val$homeScreenView;

        @DexIgnore
        Anon2(HomeScreenView homeScreenView) {
            this.val$homeScreenView = homeScreenView;
        }

        @Subscribe
        @DexIgnore
        public void onGpsLocationChanged(Location location) {
            SmartDashView.this.headingDataUtil.setHeading((double) location.getBearing());
            double newHeadingValue = SmartDashView.this.headingDataUtil.getHeading();
            if (SmartDashView.sLogger.isLoggable(2)) {
                SmartDashView.sLogger.v("SmartDash New: Heading :" + newHeadingValue + ", " + GpsUtils.getHeadingDirection(newHeadingValue) + ", Provider :" + location.getProvider());
            }
            SmartDashView.this.mSmartDashWidgetManager.updateWidget(SmartDashWidgetManager.COMPASS_WIDGET_ID, newHeadingValue);
        }

        @Subscribe
        @DexIgnore
        public void onLocationFixChangeEvent(MapEvents.LocationFix event) {
            if (!event.locationAvailable) {
                SmartDashView.this.headingDataUtil.reset();
                SmartDashView.this.mSmartDashWidgetManager.updateWidget(SmartDashWidgetManager.COMPASS_WIDGET_ID, 0.0f);
            }
        }

        @Subscribe
        @DexIgnore
        public void onDriverProfileChanged(DriverProfileChanged profileChanged) {
            this.val$homeScreenView.updateDriverPrefs();
            if (SmartDashView.this.mSmartDashWidgetManager != null) {
                SmartDashView.this.mSmartDashWidgetManager.reLoadAvailableWidgets(false);
            }
        }

        @Subscribe
        @DexIgnore
        public void updateETA(MapEvents.ManeuverDisplay event) {
            if (this.val$homeScreenView.isNavigationActive()) {
                if (event.etaDate == null) {
                    SmartDashView.sLogger.w("etaDate is not set");
                    return;
                }
                if (!TextUtils.equals(event.eta, SmartDashView.this.lastETA)) {
                    SmartDashView.this.lastETA = event.eta;
                    SmartDashView.this.etaText.setText(SmartDashView.this.lastETA);
                }
                if (!TextUtils.equals(event.etaAmPm, SmartDashView.this.lastETA_AmPm)) {
                    SmartDashView.this.lastETA_AmPm = event.etaAmPm;
                    SmartDashView.this.etaAmPm.setText(SmartDashView.this.lastETA_AmPm);
                }
            }
        }

        @Subscribe
        @DexIgnore
        public void ObdStateChangeEvent(ObdManager.ObdConnectionStatusEvent event) {
            ObdManager obdManager = ObdManager.getInstance();
            if (SmartDashView.this.tachometerPresenter != null) {
                int rpm = obdManager.getEngineRpm();
                if (rpm < 0) {
                    rpm = 0;
                }
                SmartDashView.this.tachometerPresenter.setRPM(rpm);
            }
            if (SmartDashView.this.mSmartDashWidgetManager != null) {
                SmartDashView.this.mSmartDashWidgetManager.reLoadAvailableWidgets(false);
            }
        }

        @Subscribe
        @DexIgnore
        public void onSupportedPidEventsChange(ObdManager.ObdSupportedPidsChangedEvent event) {
            SmartDashView.this.mSmartDashWidgetManager.reLoadAvailableWidgets(false);
        }

        @Subscribe
        @DexIgnore
        public void onUserGaugePreferencesChanged(SmartDashWidgetManager.UserPreferenceChanged userPreferenceChanged) {
            SmartDashView.this.mSmartDashWidgetManager.reLoadAvailableWidgets(false);
        }

        @Subscribe
        @DexIgnore
        public void onDashboardPreferences(DashboardPreferences dashboardPreferences) {
        //     if (dashboardPreferences != null) {
        //         if (dashboardPreferences.middleGauge != null) {
        //             switch (SmartDashView.Anon8.$SwitchMap$com$navdy$service$library$events$preferences$MiddleGauge[dashboardPreferences.middleGauge.ordinal()]) {
        //                 case 1:
        //                     SmartDashView.this.savePreference(IDashboardOptionsAdapter.PREFERENCE_MIDDLE_GAUGE, 1);
        //                     break;
        //                 case 2:
        //                     SmartDashView.this.savePreference(IDashboardOptionsAdapter.PREFERENCE_MIDDLE_GAUGE, 0);
        //                     break;
        //             }
        //         }
        //         if (dashboardPreferences.scrollableSide != null) {
        //             switch (SmartDashView.Anon8.$SwitchMap$com$navdy$service$library$events$preferences$ScrollableSide[dashboardPreferences.scrollableSide.ordinal()]) {
        //                 case 1:
        //                     SmartDashView.this.savePreference(IDashboardOptionsAdapter.PREFERENCE_SCROLLABLE_SIDE, 0);
        //                     break;
        //                 case 2:
        //                     SmartDashView.this.savePreference(IDashboardOptionsAdapter.PREFERENCE_SCROLLABLE_SIDE, 1);
        //                     break;
        //             }
        //         }
        //         if (SmartDashWidgetManager.isValidGaugeId(dashboardPreferences.leftGaugeId)) {
        //             SmartDashView.this.savePreference(IDashboardOptionsAdapter.PREFERENCE_LEFT_GAUGE_ID, dashboardPreferences.leftGaugeId);
        //         }
        //         if (SmartDashWidgetManager.isValidGaugeId(dashboardPreferences.rightGaugeId)) {
        //             SmartDashView.this.savePreference(IDashboardOptionsAdapter.PREFERENCE_RIGHT_GAUGE_ID, dashboardPreferences.rightGaugeId);
        //         }
        //         SmartDashView.this.showViewsAccordingToUserPreferences();
        //     }
        }
    }

    @DexIgnore
    class Anon3 implements GaugeViewPager.ChangeListener {
        @DexIgnore
        Anon3() {
        }

        @DexIgnore
        public void onGaugeChanged(int position) {
            String identifier = SmartDashView.this.mSmartDashWidgetManager.getWidgetIdentifierForIndex(position);
            SmartDashView.sLogger.d("onGaugeChanged, (Left) , Widget ID : " + identifier + " , Position : " + position);
            SmartDashView.this.savePreference(IDashboardOptionsAdapter.PREFERENCE_LEFT_GAUGE_ID, identifier);
            SmartDashView.this.mRightAdapter.setExcludedPosition(position);
            SmartDashView.this.mRightGaugeViewPager.updateState();
        }
    }

    @DexIgnore
    class Anon4 implements GaugeViewPager.ChangeListener {
        @DexIgnore
        Anon4() {
        }

        @DexIgnore
        public void onGaugeChanged(int position) {
            String identifier = SmartDashView.this.mSmartDashWidgetManager.getWidgetIdentifierForIndex(position);
            SmartDashView.sLogger.d("onGaugeChanged, (Right) , Widget ID : " + identifier + " , Position : " + position);
            SmartDashView.this.savePreference(IDashboardOptionsAdapter.PREFERENCE_RIGHT_GAUGE_ID, identifier);
            SmartDashView.this.mLeftAdapter.setExcludedPosition(position);
            SmartDashView.this.mLeftGaugeViewPager.updateState();
        }
    }

    @DexIgnore
    class Anon5 implements ValueAnimator.AnimatorUpdateListener {
        final /* synthetic */ ViewGroup.MarginLayoutParams val$marginParams;

        @DexIgnore
        Anon5(ViewGroup.MarginLayoutParams marginLayoutParams) {
            this.val$marginParams = marginLayoutParams;
        }

        @DexIgnore
        public void onAnimationUpdate(ValueAnimator animation) {
            this.val$marginParams.leftMargin = (Integer) animation.getAnimatedValue();
            SmartDashView.this.mMiddleGaugeView.setLayoutParams(this.val$marginParams);
        }
    }

    @DexIgnore
    class Anon6 extends DefaultAnimationListener {
        @DexIgnore
        Anon6() {
        }

        @DexIgnore
        public void onAnimationEnd(Animator animation) {
            SmartDashView.this.mLeftGaugeViewPager.setVisibility(0);
            SmartDashView.this.mRightGaugeViewPager.setVisibility(0);
            if (SmartDashView.this.homeScreenView.isNavigationActive()) {
                SmartDashView.this.showEta(true);
            }
        }
    }

    @DexIgnore
    class Anon7 implements ValueAnimator.AnimatorUpdateListener {
        final /* synthetic */ ViewGroup.MarginLayoutParams val$marginParams;

        @DexIgnore
        Anon7(ViewGroup.MarginLayoutParams marginLayoutParams) {
            this.val$marginParams = marginLayoutParams;
        }

        @DexIgnore
        public void onAnimationUpdate(ValueAnimator animation) {
            this.val$marginParams.leftMargin = (Integer) animation.getAnimatedValue();
            SmartDashView.this.mMiddleGaugeView.setLayoutParams(this.val$marginParams);
        }
    }

    // @DexIgnore
    // static /* synthetic */ class Anon8 {
    //     static final /* synthetic */ int[] $SwitchMap$com$navdy$service$library$events$preferences$MiddleGauge = new int[MiddleGauge.values().length];
    //     static final /* synthetic */ int[] $SwitchMap$com$navdy$service$library$events$preferences$ScrollableSide = new int[ScrollableSide.values().length];
    //
    //     static {
    //         $SwitchMap$com$navdy$hud$app$view$MainView$CustomAnimationMode = new int[MainView.CustomAnimationMode.values().length];
    //         try {
    //             $SwitchMap$com$navdy$hud$app$view$MainView$CustomAnimationMode[MainView.CustomAnimationMode.EXPAND.ordinal()] = 1;
    //         } catch (NoSuchFieldError e) {
    //         }
    //         try {
    //             $SwitchMap$com$navdy$hud$app$view$MainView$CustomAnimationMode[MainView.CustomAnimationMode.SHRINK_LEFT.ordinal()] = 2;
    //         } catch (NoSuchFieldError e2) {
    //         }
    //         try {
    //             $SwitchMap$com$navdy$hud$app$view$MainView$CustomAnimationMode[MainView.CustomAnimationMode.SHRINK_MODE.ordinal()] = 3;
    //         } catch (NoSuchFieldError e3) {
    //         }
    //         $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent = new int[InputManager.CustomKeyEvent.values().length];
    //         try {
    //             $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[InputManager.CustomKeyEvent.LEFT.ordinal()] = 1;
    //         } catch (NoSuchFieldError e4) {
    //         }
    //         try {
    //             $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[InputManager.CustomKeyEvent.RIGHT.ordinal()] = 2;
    //         } catch (NoSuchFieldError e5) {
    //         }
    //         try {
    //             $SwitchMap$com$navdy$service$library$events$preferences$ScrollableSide[ScrollableSide.LEFT.ordinal()] = 1;
    //         } catch (NoSuchFieldError e6) {
    //         }
    //         try {
    //             $SwitchMap$com$navdy$service$library$events$preferences$ScrollableSide[ScrollableSide.RIGHT.ordinal()] = 2;
    //         } catch (NoSuchFieldError e7) {
    //         }
    //         try {
    //             $SwitchMap$com$navdy$service$library$events$preferences$MiddleGauge[MiddleGauge.SPEEDOMETER.ordinal()] = 1;
    //         } catch (NoSuchFieldError e8) {
    //         }
    //         try {
    //             $SwitchMap$com$navdy$service$library$events$preferences$MiddleGauge[MiddleGauge.TACHOMETER.ordinal()] = 2;
    //         } catch (NoSuchFieldError e9) {
    //         }
    //     }
    // }

    @DexIgnore
    class GaugeViewPagerAdapter implements GaugeViewPager.Adapter {
        private int mExcludedPosition = -1;
        private boolean mLeft;
        private SmartDashWidgetManager mSmartDashWidgetManager;
        private SmartDashWidgetManager.SmartDashWidgetCache mWidgetCache;

        @DexIgnore
        public GaugeViewPagerAdapter(SmartDashWidgetManager smartDashWidgetManager, boolean left) {
            this.mSmartDashWidgetManager = smartDashWidgetManager;
            this.mLeft = left;
            this.mWidgetCache = this.mSmartDashWidgetManager.buildSmartDashWidgetCache(this.mLeft ? 0 : 1);
            this.mSmartDashWidgetManager.registerForChanges(this);
        }

        @Subscribe
        @DexIgnore
        public void onWidgetsReload(SmartDashWidgetManager.Reload reload) {
            if (reload == SmartDashWidgetManager.Reload.RELOAD_CACHE) {
                this.mWidgetCache = this.mSmartDashWidgetManager.buildSmartDashWidgetCache(this.mLeft ? 0 : 1);
            }
        }

        @DexIgnore
        public int getCount() {
            return this.mWidgetCache.getWidgetsCount();
        }

        @DexIgnore
        public int getExcludedPosition() {
            return this.mExcludedPosition;
        }

        @DexIgnore
        public void setExcludedPosition(int excludedPosition) {
            if (this.mSmartDashWidgetManager.getIndexForWidgetIdentifier(SmartDashWidgetManager.EMPTY_WIDGET_ID) == excludedPosition) {
                this.mExcludedPosition = -1;
            } else {
                this.mExcludedPosition = excludedPosition;
            }
        }

        @DexIgnore
        public View getView(int position, View recycledView, ViewGroup parent, boolean isActive) {
            DashboardWidgetView gaugeView;
            boolean z = false;
            if (recycledView == null) {
                gaugeView = new DashboardWidgetView(parent.getContext());
                gaugeView.setLayoutParams(new ViewGroup.LayoutParams(parent.getResources().getDimensionPixelSize(R.dimen.gauge_frame_width), parent.getResources().getDimensionPixelSize(R.dimen.gauge_frame_height)));
            } else {
                gaugeView = (DashboardWidgetView) recycledView;
            }
            DashboardWidgetPresenter oldPresenter = (DashboardWidgetPresenter) gaugeView.getTag();
            DashboardWidgetPresenter presenter = this.mWidgetCache.getWidgetPresenter(position);
            if (!(oldPresenter == null || oldPresenter == presenter || oldPresenter.getWidgetView() != gaugeView)) {
                oldPresenter.setView(null, null);
            }
            if (presenter != null) {
                Bundle args = new Bundle();
                args.putInt(DashboardWidgetPresenter.EXTRA_GRAVITY, this.mLeft ? 0 : 2);
                args.putBoolean(DashboardWidgetPresenter.EXTRA_IS_ACTIVE, isActive);
                if (!isActive) {
                    presenter.setWidgetVisibleToUser(false);
                } else {
                    if (this.mLeft) {
                        SmartDashView.this.activeleftGaugeId = presenter.getWidgetIdentifier();
                    } else {
                        SmartDashView.this.activeRightGaugeId = presenter.getWidgetIdentifier();
                    }
                    SmartDashView smartDashView = SmartDashView.this;
                    if (SmartDashWidgetManager.DRIVE_SCORE_GAUGE_ID.equals(SmartDashView.this.activeleftGaugeId) || SmartDashWidgetManager.DRIVE_SCORE_GAUGE_ID.equals(SmartDashView.this.activeRightGaugeId)) {
                        z = true;
                    }
                    smartDashView.isShowingDriveScoreGauge = z;
                    presenter.setWidgetVisibleToUser(true);
                }
                presenter.setView(gaugeView, args);
            }
            gaugeView.setTag(presenter);
            return gaugeView;
        }

        @DexIgnore
        public DashboardWidgetPresenter getPresenter(int position) {
            return this.mWidgetCache.getWidgetPresenter(position);
        }
    }

    @DexIgnore
    public SmartDashView(Context context) {
        super(context);
    }

    @DexIgnore
    public SmartDashView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @DexIgnore
    public SmartDashView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    /* access modifiers changed from: protected */
    @DexIgnore
    public void onFinishInflate() {
        super.onFinishInflate();
        this.fullModeGaugeTopMargin = getResources().getDimensionPixelSize(R.dimen.gauge_frame_top_margin_full_mode);
        this.activeModeGaugeMargin = getResources().getDimensionPixelSize(R.dimen.gauge_frame_top_margin_active_mode);
    }

    @DexReplace
    public void init(HomeScreenView homeScreenView) {
        this.globalPreferences = SettingsManager.global;
        this.mSmartDashWidgetManager = new SmartDashWidgetManager(null, getContext());
        NavdyPreferences driverPreferences = homeScreenView.getDriverPreferences();
        this.mSmartDashWidgetManager.setFilter(identifier -> {
            boolean z = true;
            NavdyPreferences sharedPreferences = homeScreenView.getDriverPreferences();
            String leftGaugeId = sharedPreferences.getString(IDashboardOptionsAdapter.PREFERENCE_LEFT_GAUGE_ID, ANALOG_CLOCK_WIDGET_ID);
            String rightGaugeId = sharedPreferences.getString(IDashboardOptionsAdapter.PREFERENCE_RIGHT_GAUGE_ID, COMPASS_WIDGET_ID);
            if (!TextUtils.isEmpty(identifier) && (identifier.equals(leftGaugeId) || identifier.equals(rightGaugeId))) {
                return false;
            }
            switch (identifier) {
                case TRAFFIC_INCIDENT_GAUGE_ID:
                    if (MapSettings.isTrafficDashWidgetsEnabled()) {
                        z = false;
                    }
                    return z;
                case FUEL_GAUGE_ID:
                    PidSet pidSet = ObdManager.getInstance().getSupportedPids();
                    return pidSet == null || !pidSet.contains(47);
                case ENGINE_TEMPERATURE_GAUGE_ID:
                    PidSet pidSet2 = ObdManager.getInstance().getSupportedPids();
                    return pidSet2 == null || !pidSet2.contains(5);
                case MPG_AVG_WIDGET_ID:
                    PidSet pidSet3 = ObdManager.getInstance().getSupportedPids();
                    return pidSet3 == null || !pidSet3.contains(256);
                case INTAKE_PRESSURE_GAUGE_ID: {
                    final PidSet supportedPids = ObdManager.getInstance().getSupportedPids();
                    if (supportedPids != null) {
                        return !supportedPids.contains(Pids.MANIFOLD_AIR_PRESSURE);
                    }
                    return true;
                }
                case ENGINE_OIL_TEMPERATURE_GAUGE_ID: {
                    final PidSet supportedPids2 = ObdManager.getInstance().getSupportedPids();
                    if (supportedPids2 != null) {
                        return !supportedPids2.contains(Pids.ENGINE_OIL_TEMPERATURE);
                    }
                    return true;
                }
                case AMBIENT_TEMPERATURE_GAUGE_ID: {
                    final PidSet supportedPids2 = ObdManager.getInstance().getSupportedPids();
                    if (supportedPids2 != null) {
                        return !supportedPids2.contains(Pids.AMBIENT_AIR_TEMRERATURE);
                    }
                    return true;
                }

                default:
                    return false;
            }
        });
        this.mSmartDashWidgetManager.registerForChanges(this);
        this.tachometerPresenter = new TachometerGaugePresenter(getContext());
        this.speedometerGaugePresenter2 = new SpeedometerGaugePresenter2(getContext());
        this.bus.register(new SmartDashView.Anon2(homeScreenView));
        Resources resources = getResources();
        this.middleGaugeShrinkMargin = resources.getDimensionPixelSize(R.dimen.middle_gauge_shrink_margin);
        this.middleGaugeShrinkModeMargin = resources.getDimensionPixelSize(R.dimen.middle_gauge_shrink_mode_margin);
        this.middleGaugeShrinkEarlyTbtOffset = getResources().getDimensionPixelSize(R.dimen.middle_gauge_shrink_early_tbt_offset);
        super.init(homeScreenView);
    }

    // @com.squareup.otto.Subscribe
    @DexIgnore
    public void onReload(SmartDashWidgetManager.Reload reload) {
        if (reload == SmartDashWidgetManager.Reload.RELOADED) {
            showViewsAccordingToUserPreferences();
        }
    }

    @DexReplace
    private void showMiddleGaugeAccordingToUserPreferences() {
        this.middleGauge = (DriverProfileHelper.getInstance().getCurrentProfile().isDefaultProfile() ? this.globalPreferences : this.homeScreenView.getDriverPreferences()).getInt(IDashboardOptionsAdapter.PREFERENCE_MIDDLE_GAUGE, 1);
        sLogger.d("Middle Gauge : " + this.middleGauge);
        if (this.middleGauge == IDashboardOptionsAdapter.TACHOMETER_GAUGE) {
            showTachometer();
        } else {
            showSpeedometerGauge();
        }
    }

    @DexReplace
    private void showViewsAccordingToUserPreferences() {
        sLogger.d("showViewsAccordingToUserPreferences");
        boolean isDefaultProfile = DriverProfileHelper.getInstance().getCurrentProfile().isDefaultProfile();
        NavdyPreferences sharedPreferences = isDefaultProfile ? this.globalPreferences : this.homeScreenView.getDriverPreferences();
        this.scrollableSide = sharedPreferences.getInt(IDashboardOptionsAdapter.PREFERENCE_SCROLLABLE_SIDE, 1);
        sLogger.v("scrollableSide:" + this.scrollableSide);
        showMiddleGaugeAccordingToUserPreferences();
        String leftGaugeId = sharedPreferences.getString(IDashboardOptionsAdapter.PREFERENCE_LEFT_GAUGE_ID, SmartDashWidgetManager.ANALOG_CLOCK_WIDGET_ID);
        int leftGaugeIndex = this.mSmartDashWidgetManager.getIndexForWidgetIdentifier(leftGaugeId);
        sLogger.d("Left Gauge ID : " + leftGaugeId + ", Index : " + leftGaugeIndex);
        if (leftGaugeIndex < 0) {
            leftGaugeIndex = this.mSmartDashWidgetManager.getIndexForWidgetIdentifier(SmartDashWidgetManager.EMPTY_WIDGET_ID);
        }
        this.mLeftGaugeViewPager.setSelectedPosition(leftGaugeIndex);
        String rightGaugeId = sharedPreferences.getString(IDashboardOptionsAdapter.PREFERENCE_RIGHT_GAUGE_ID, SmartDashWidgetManager.COMPASS_WIDGET_ID);
        int rightGaugeIndex = this.mSmartDashWidgetManager.getIndexForWidgetIdentifier(rightGaugeId);
        sLogger.d("Right Gauge ID : " + rightGaugeId + ", Index : " + rightGaugeIndex);
        if (rightGaugeIndex < 0) {
            rightGaugeIndex = this.mSmartDashWidgetManager.getIndexForWidgetIdentifier(SmartDashWidgetManager.EMPTY_WIDGET_ID);
        }
        this.mRightGaugeViewPager.setSelectedPosition(rightGaugeIndex);
        if (!isDefaultProfile) {
            this.globalPreferences.edit().putInt(IDashboardOptionsAdapter.PREFERENCE_MIDDLE_GAUGE, this.middleGauge).putInt(IDashboardOptionsAdapter.PREFERENCE_SCROLLABLE_SIDE, this.scrollableSide).putString(IDashboardOptionsAdapter.PREFERENCE_LEFT_GAUGE_ID, leftGaugeId).putString(IDashboardOptionsAdapter.PREFERENCE_RIGHT_GAUGE_ID, rightGaugeId).apply();
        }
        this.mLeftAdapter.setExcludedPosition(rightGaugeIndex);
        this.mRightAdapter.setExcludedPosition(leftGaugeIndex);
        recordUsersWidgetPreference();
    }

    @DexIgnore
    private void showTachometer() {
        this.speedometerGaugePresenter2.onPause();
        this.speedometerGaugePresenter2.setWidgetVisibleToUser(false);
        this.speedometerGaugePresenter2.setView(null);
        if (!this.paused) {
            this.tachometerPresenter.onResume();
        }
        this.tachometerPresenter.setWidgetVisibleToUser(true);
        this.tachometerPresenter.setView(this.mMiddleGaugeView);
        int engineRpm = ObdManager.getInstance().getEngineRpm();
        if (!(engineRpm == -1 || this.tachometerPresenter == null)) {
            this.tachometerPresenter.setRPM(engineRpm);
        }
        int speed = SpeedManager.getInstance().getCurrentSpeed();
        if (this.tachometerPresenter != null) {
            this.tachometerPresenter.setSpeed(speed);
        }
    }

    @DexIgnore
    private void showSpeedometerGauge() {
        this.tachometerPresenter.onPause();
        this.tachometerPresenter.setWidgetVisibleToUser(false);
        this.tachometerPresenter.setView(null);
        if (!this.paused) {
            this.speedometerGaugePresenter2.onResume();
        }
        this.speedometerGaugePresenter2.setWidgetVisibleToUser(true);
        this.speedometerGaugePresenter2.setView(this.mMiddleGaugeView);
        this.speedometerGaugePresenter2.setSpeed(SpeedManager.getInstance().getCurrentSpeed());
    }

    /* access modifiers changed from: protected */
    @DexIgnore
    public void initializeView() {
        super.initializeView();
        this.mSmartDashWidgetManager.reLoadAvailableWidgets(false);
        if (this.mLeftAdapter == null) {
            this.mLeftAdapter = new SmartDashView.GaugeViewPagerAdapter(this.mSmartDashWidgetManager, true);
            this.mLeftGaugeViewPager.setAdapter(this.mLeftAdapter);
            this.mLeftGaugeViewPager.setChangeListener(new SmartDashView.Anon3());
        }
        if (this.mRightAdapter == null) {
            this.mRightAdapter = new SmartDashView.GaugeViewPagerAdapter(this.mSmartDashWidgetManager, false);
            this.mRightGaugeViewPager.setAdapter(this.mRightAdapter);
            this.mRightGaugeViewPager.setChangeListener(new SmartDashView.Anon4());
        }
        showViewsAccordingToUserPreferences();
    }

    @DexIgnore
    public boolean shouldShowTopViews() {
        return false;
    }

    @DexIgnore
    public int getMiddleGaugeOption() {
        return this.middleGauge == 0 ? 1 : 0;
    }

    @DexIgnore
    public void onTachoMeterSelected() {
        if (this.middleGauge != 0) {
            this.middleGauge = 0;
            savePreference(IDashboardOptionsAdapter.PREFERENCE_MIDDLE_GAUGE, 0);
            showMiddleGaugeAccordingToUserPreferences();
        }
    }

    @DexIgnore
    public void onSpeedoMeterSelected() {
        if (this.middleGauge != 1) {
            this.middleGauge = 1;
            savePreference(IDashboardOptionsAdapter.PREFERENCE_MIDDLE_GAUGE, 1);
            showMiddleGaugeAccordingToUserPreferences();
        }
    }

    @DexIgnore
    public int getCurrentScrollableSideOption() {
        return this.scrollableSide == 0 ? 1 : 0;
    }

    @DexIgnore
    public void onScrollableSideOptionSelected() {
        switch (this.scrollableSide) {
            case 0:
                this.scrollableSide = 1;
                savePreference(IDashboardOptionsAdapter.PREFERENCE_SCROLLABLE_SIDE, 1);
                return;
            case 1:
                this.scrollableSide = 0;
                savePreference(IDashboardOptionsAdapter.PREFERENCE_SCROLLABLE_SIDE, 0);
                return;
            default:
                return;
        }
    }

    @DexReplace
    private void savePreference(String key, Object value) {
        boolean isDefaultProfile = DriverProfileHelper.getInstance().getCurrentProfile().isDefaultProfile();
        NavdyPreferences sharedPreferences = this.homeScreenView.getDriverPreferences();
        if (!isDefaultProfile) {
            if (value instanceof String) {
                sharedPreferences.edit().putString(key, value).apply();
            } else if (value instanceof Integer) {
                sharedPreferences.edit().putInt(key, value).apply();
            }
        }
        if (value instanceof String) {
            this.globalPreferences.edit().putString(key, value).apply();
        } else if (value instanceof Integer) {
            this.globalPreferences.edit().putInt(key, value).apply();
        }
    }

    @DexIgnore
    public boolean onKey(InputManager.CustomKeyEvent event) {
        GaugeViewPager gaugeViewPager = this.scrollableSide == 1 ? this.mRightGaugeViewPager : this.mLeftGaugeViewPager;
        switch (event) {
            case LEFT:
                gaugeViewPager.moveNext();
                break;
            case RIGHT:
                gaugeViewPager.movePrevious();
                break;
        }
        return false;
    }

    @DexReplace
    public void ObdPidChangeEvent(final ObdManager.ObdPidChangeEvent obdPidChangeEvent) {
        super.ObdPidChangeEvent(obdPidChangeEvent);
        switch (obdPidChangeEvent.pid.getId()) {
            case 12:
                if (this.tachometerPresenter != null) {
                    this.tachometerPresenter.setRPM((int)obdPidChangeEvent.pid.getValue());
                    break;
                }
                break;
            case 47:
                this.mSmartDashWidgetManager.updateWidget(FUEL_GAUGE_ID, obdPidChangeEvent.pid.getValue());
                break;
            case 256:
                this.mSmartDashWidgetManager.updateWidget(MPG_AVG_WIDGET_ID, obdPidChangeEvent.pid.getValue());
                break;
            case 5:
                this.mSmartDashWidgetManager.updateWidget(ENGINE_TEMPERATURE_GAUGE_ID, obdPidChangeEvent.pid.getValue());
                break;
            case Pids.ENGINE_OIL_TEMPERATURE:
                this.mSmartDashWidgetManager.updateWidget(ENGINE_OIL_TEMPERATURE_GAUGE_ID, obdPidChangeEvent.pid.getValue());
                break;
            case Pids.AMBIENT_AIR_TEMRERATURE:
                this.mSmartDashWidgetManager.updateWidget(AMBIENT_TEMPERATURE_GAUGE_ID, obdPidChangeEvent.pid.getValue());
                break;
            case Pids.MANIFOLD_AIR_PRESSURE:
                this.mSmartDashWidgetManager.updateWidget(INTAKE_PRESSURE_GAUGE_ID, obdPidChangeEvent.pid.getValue());
                break;
            case Pids.DISTANCE_TO_EMPTY:
                this.mSmartDashWidgetManager.updateWidget(FUEL_GAUGE_RANGE_ID, obdPidChangeEvent.pid.getValue());
                break;
        }
    }

    @DexIgnore
    public void updateLayoutForMode(NavigationMode navigationMode, boolean forced) {
        boolean navigationActive = this.homeScreenView.isNavigationActive();
        sLogger.v("updateLayoutForMode:" + navigationMode + " forced=" + forced + " nav:" + navigationActive);
        if (navigationActive) {
            sLogger.v("updateLayoutForMode: active");
            ((ViewGroup.MarginLayoutParams) this.mLeftGaugeViewPager.getLayoutParams()).topMargin = this.activeModeGaugeMargin;
            ((ViewGroup.MarginLayoutParams) this.mRightGaugeViewPager.getLayoutParams()).topMargin = this.activeModeGaugeMargin;
            ((ViewGroup.MarginLayoutParams) this.mMiddleGaugeView.getLayoutParams()).topMargin = getResources().getDimensionPixelSize(R.dimen.tachometer_tbt_mode_margin_top);
            this.mMiddleGaugeView.setScaleX(0.8f);
            this.mMiddleGaugeView.setScaleY(0.8f);
            this.mEtaLayout.setVisibility(0);
            invalidate();
            requestLayout();
        } else {
            sLogger.v("updateLayoutForMode: not active");
            ((ViewGroup.MarginLayoutParams) this.mLeftGaugeViewPager.getLayoutParams()).topMargin = this.fullModeGaugeTopMargin;
            ((ViewGroup.MarginLayoutParams) this.mRightGaugeViewPager.getLayoutParams()).topMargin = this.fullModeGaugeTopMargin;
            ((ViewGroup.MarginLayoutParams) this.mMiddleGaugeView.getLayoutParams()).topMargin = getResources().getDimensionPixelSize(R.dimen.tachometer_full_mode_margin_top);
            this.mMiddleGaugeView.setScaleX(1.0f);
            this.mMiddleGaugeView.setScaleY(1.0f);
            this.mEtaLayout.setVisibility(8);
            invalidate();
            requestLayout();
        }
        showEta(navigationActive);
        invalidate();
        requestLayout();
    }

    @DexIgnore
    public void adjustDashHeight(int height) {
        super.adjustDashHeight(height);
    }

    /* access modifiers changed from: protected */
    @DexIgnore
    public void updateSpeed(int speed) {
        super.updateSpeed(speed);
        if (this.tachometerPresenter != null) {
            this.tachometerPresenter.setSpeed(speed);
        }
        if (this.speedometerGaugePresenter2 != null) {
            this.speedometerGaugePresenter2.setSpeed(speed);
        }
    }

    /* access modifiers changed from: protected */
    @DexIgnore
    public void updateSpeedLimit(int speedLimit) {
        super.updateSpeedLimit(speedLimit);
        if (this.tachometerPresenter != null) {
            this.tachometerPresenter.setSpeedLimit(speedLimit);
        }
        if (this.speedometerGaugePresenter2 != null) {
            this.speedometerGaugePresenter2.setSpeedLimit(speedLimit);
        }
        this.mSmartDashWidgetManager.updateWidget(SmartDashWidgetManager.SPEED_LIMIT_SIGN_GAUGE_ID, speedLimit);
    }

    /* access modifiers changed from: protected */
    @DexIgnore
    public void animateToFullMode() {
        super.animateToFullMode();
    }

    /* access modifiers changed from: protected */
    @DexIgnore
    public void animateToFullModeInternal(AnimatorSet.Builder builder) {
        super.animateToFullModeInternal(builder);
    }

    @DexIgnore
    public void setView(MainView.CustomAnimationMode mode) {
        sLogger.v("setview: " + mode);
        super.setView(mode);
        switch (mode) {
            case EXPAND:
                this.mLeftGaugeViewPager.setVisibility(0);
                this.mRightGaugeViewPager.setVisibility(0);
                if (this.homeScreenView.isNavigationActive()) {
                    showEta(true);
                }
                ((ViewGroup.MarginLayoutParams) this.mMiddleGaugeView.getLayoutParams()).leftMargin = 0;
                return;
            case SHRINK_LEFT:
                this.mLeftGaugeViewPager.setVisibility(8);
                this.mRightGaugeViewPager.setVisibility(8);
                showEta(false);
                ((ViewGroup.MarginLayoutParams) this.mMiddleGaugeView.getLayoutParams()).leftMargin = getResources().getDimensionPixelSize(R.dimen.middle_gauge_shrink_margin);
                return;
            default:
                return;
        }
    }

    @DexIgnore
    public void getCustomAnimator(MainView.CustomAnimationMode mode, AnimatorSet.Builder mainBuilder) {
        int targetMargin;
        sLogger.v("getCustomAnimator" + mode);
        super.getCustomAnimator(mode, mainBuilder);
        AnimatorSet set = new AnimatorSet();
        switch (mode) {
            case EXPAND:
                ViewGroup.MarginLayoutParams marginParams = (ViewGroup.MarginLayoutParams) this.mMiddleGaugeView.getLayoutParams();
                int srcMargin = marginParams.leftMargin;
                ValueAnimator valueAnimator = new ValueAnimator();
                valueAnimator.setIntValues(srcMargin, 0);
                valueAnimator.addUpdateListener(new SmartDashView.Anon5(marginParams));
                valueAnimator.addListener(new SmartDashView.Anon6());
                set.playTogether(valueAnimator);
                break;
            case SHRINK_LEFT:
            case SHRINK_MODE:
                this.mLeftGaugeViewPager.setVisibility(8);
                this.mRightGaugeViewPager.setVisibility(8);
                showEta(false);
                ViewGroup.MarginLayoutParams marginParams2 = (ViewGroup.MarginLayoutParams) this.mMiddleGaugeView.getLayoutParams();
                int srcMargin2 = marginParams2.leftMargin;
                if (mode == MainView.CustomAnimationMode.SHRINK_LEFT) {
                    targetMargin = this.middleGaugeShrinkMargin;
                } else {
                    targetMargin = this.middleGaugeShrinkModeMargin;
                }
                ValueAnimator valueAnimator2 = new ValueAnimator();
                valueAnimator2.setIntValues(srcMargin2, targetMargin);
                valueAnimator2.addUpdateListener(new SmartDashView.Anon7(marginParams2));
                set.playTogether(valueAnimator2);
                break;
        }
        mainBuilder.with(set);
    }

    @DexIgnore
    private void showEta(boolean show) {
        if (this.mEtaLayout.getVisibility() == VISIBLE) {
            this.mEtaLayout.setVisibility(GONE);
        }
    }

    @DexIgnore
    public void onPause() {
        if (!this.paused) {
            sLogger.v("::onPause");
            this.paused = true;
            if (this.mSmartDashWidgetManager != null) {
                sLogger.v("widgets disabled");
                this.mSmartDashWidgetManager.onPause();
                if (this.tachometerPresenter != null) {
                    this.tachometerPresenter.onPause();
                }
                if (this.speedometerGaugePresenter2 != null) {
                    this.speedometerGaugePresenter2.onPause();
                }
            }
        }
    }

    @DexIgnore
    public void onResume() {
        if (this.paused) {
            sLogger.v("::onResume");
            this.paused = false;
            if (this.mSmartDashWidgetManager != null) {
                sLogger.v("widgets enabled");
                this.mSmartDashWidgetManager.onResume();
                if (this.tachometerPresenter != null && this.tachometerPresenter.isWidgetActive()) {
                    this.tachometerPresenter.onResume();
                }
                if (this.speedometerGaugePresenter2 != null && this.speedometerGaugePresenter2.isWidgetActive()) {
                    this.speedometerGaugePresenter2.onResume();
                }
            }
            recordUsersWidgetPreference();
        }
    }

    @DexReplace
    public void recordUsersWidgetPreference() {
        sLogger.d("Recording the users Widget Preference");
        long currentTime = SystemClock.elapsedRealtime();
        if (DriverProfileHelper.getInstance().getCurrentProfile().isDefaultProfile()) {
            return;
        }
        if (this.lastUserPreferenceRecordedTime <= 0 || currentTime - this.lastUserPreferenceRecordedTime >= MINIMUM_INTERVAL_BETWEEN_RECORDING_USER_PREFERENCE) {
            NavdyPreferences sharedPreferences = this.homeScreenView.getDriverPreferences();
            String leftGaugeId = sharedPreferences.getString(IDashboardOptionsAdapter.PREFERENCE_LEFT_GAUGE_ID, SmartDashWidgetManager.ANALOG_CLOCK_WIDGET_ID);
            String rightGaugeId = sharedPreferences.getString(IDashboardOptionsAdapter.PREFERENCE_RIGHT_GAUGE_ID, SmartDashWidgetManager.COMPASS_WIDGET_ID);
            // AnalyticsSupport.recordDashPreference(leftGaugeId, true);
            // AnalyticsSupport.recordDashPreference(rightGaugeId, false);
            this.lastUserPreferenceRecordedTime = currentTime;
        }
    }

    @DexIgnore
    public String getActiveleftGaugeId() {
        return this.activeleftGaugeId;
    }

    @DexIgnore
    public String getActiveRightGaugeId() {
        return this.activeRightGaugeId;
    }
}
