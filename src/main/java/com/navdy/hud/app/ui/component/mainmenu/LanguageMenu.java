package com.navdy.hud.app.ui.component.mainmenu;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.view.View;

import com.makeramen.RoundedTransformationBuilder;
import com.navdy.hud.app.HudApplication;

import com.navdy.hud.app.event.InitEvents;
import com.navdy.hud.app.framework.DriverProfileHelper;
import com.navdy.hud.app.profile.HudLocale;
import com.navdy.hud.app.ui.component.image.CrossFadeImageView;
import com.navdy.hud.app.ui.component.image.InitialsImageView;
import com.navdy.hud.app.ui.component.vlist.VerticalList;
import com.navdy.hud.app.ui.component.vlist.VerticalModelCache;
import com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder;
import com.navdy.hud.app.ui.component.vlist.viewholder.IconsTwoViewHolder;
import com.navdy.hud.app.ui.component.vlist.viewholder.TitleSubtitleViewHolder;
import com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder;
import com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex;
import com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent;
import com.navdy.hud.app.util.picasso.PicassoUtil;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.squareup.otto.Bus;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.squareup.picasso.Transformation;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.TreeMap;

import alelec.navdy.hud.app.R;

class LanguageMenu implements IMenu {
    private static final Resources resources = HudApplication.getAppContext().getResources();
    private static final float CONTACT_PHOTO_OPACITY = 0.6f;
    private static final VerticalList.Model back;
    private static final VerticalList.Model auto;
    private static final int backColor = resources.getColor(R.color.mm_back);
    private static final int contactColor = resources.getColor(R.color.mm_contacts);
    private static final String contactStr = resources.getString(R.string.carousel_menu_contacts);
    /* access modifiers changed from: private */
    public static final Handler handler = new Handler(Looper.getMainLooper());
    private static final int noContactColor = resources.getColor(R.color.icon_user_bg_4);
    private static final String recentContactStr = resources.getString(R.string.carousel_menu_recent_contacts_title);
    private static final Transformation roundTransformation = new RoundedTransformationBuilder().oval(true).build();
    private static final Logger sLogger = new Logger(LanguageMenu.class);
    private int backSelection;
    private int backSelectionId;
    private Bus bus;
    private List<VerticalList.Model> cachedList;
    private IMenu parent;
    /* access modifiers changed from: private */
    public MainMenuScreen2.Presenter presenter;
    private List<VerticalList.Model> returnToCacheList;
    private VerticalMenuComponent vscrollComponent;
    private TreeMap<String, ArrayList<Locale>> language_maps;
    private String lang_submenu;

    class Anon1 implements Runnable {
        final /* synthetic */ File val$imagePath;
        final /* synthetic */ InitialsImageView val$imageView;
        final /* synthetic */ VerticalList.Model val$model;
        final /* synthetic */ int val$pos;

        Anon1(File file, InitialsImageView initialsImageView, VerticalList.Model model, int i) {
            this.val$imagePath = file;
            this.val$imageView = initialsImageView;
            this.val$model = model;
            this.val$pos = i;
        }

        public void run() {
            if (this.val$imagePath.exists()) {
                LanguageMenu.handler.post(() -> {
                    if (Anon1.this.val$imageView.getTag() == Anon1.this.val$model.state) {
                        LanguageMenu.this.setImage(Anon1.this.val$imagePath, VerticalViewHolder.selectedIconSize, VerticalViewHolder.selectedIconSize, Anon1.this.val$pos);
                    }
                });
            }
        }
    }

    static {
        String title = resources.getString(R.string.back);
        int fluctuatorColor = backColor;
        back = IconBkColorViewHolder.buildModel(R.id.menu_back, R.drawable.icon_mm_back, fluctuatorColor, MainMenu.bkColorUnselected, fluctuatorColor, title, null);
        title = resources.getString(R.string.gps_phone);
        auto = IconBkColorViewHolder.buildModel(R.id.menu_auto, R.drawable.icon_mm_contacts_2, fluctuatorColor, MainMenu.bkColorUnselected, fluctuatorColor, title, null);
    }

    LanguageMenu(Bus bus2, VerticalMenuComponent vscrollComponent2, MainMenuScreen2.Presenter presenter2, IMenu parent2) {
        this(bus2, vscrollComponent2, presenter2, parent2, null);
    }

    LanguageMenu(Bus bus2, VerticalMenuComponent vscrollComponent2, MainMenuScreen2.Presenter presenter2, IMenu parent2, String lang) {
        this.bus = bus2;
        this.vscrollComponent = vscrollComponent2;
        this.presenter = presenter2;
        this.parent = parent2;
        this.lang_submenu = lang;
    }

    public List<VerticalList.Model> getItems() {
        if (this.cachedList != null) {
            return this.cachedList;
        }

        language_maps = new TreeMap<>();

        List<VerticalList.Model> list = new ArrayList<>();
        this.returnToCacheList = new ArrayList<>();

        list.add(back);

        if (lang_submenu == null) {
            list.add(auto);
        }

        int counter = 0;

        Locale[] locales = Locale.getAvailableLocales();
        for(Locale locale:locales) {
            String lang = locale.getDisplayLanguage();
            if (!language_maps.containsKey(lang)) {
                language_maps.put(lang, new ArrayList<>());
            }
            language_maps.get(lang).add(locale);
        }

        if (this.lang_submenu != null) {
            for (Locale locale: language_maps.get(this.lang_submenu)) {
                int counter2 = counter + 1;
                VerticalList.Model model = buildModel(
                        counter,
                        locale.getDisplayLanguage() + " - " + locale.getDisplayCountry(),
                        locale.toLanguageTag());
                model.state = locale;
                list.add(model);
                this.returnToCacheList.add(model);
                counter = counter2;

            }
        } else {
            for (String lang: language_maps.keySet()) {
                int counter2 = counter + 1;
                VerticalList.Model model = buildModel(
                        counter,
                        lang ,
                        null
                );
                model.state = lang;
                list.add(model);
                this.returnToCacheList.add(model);
                counter = counter2;

            }

        }

        this.cachedList = list;
        return list;

    }

    public int getInitialSelection() {
        if (this.cachedList == null) {
            return 0;
        }
        String current = HudLocale.getOverrideLocale();
        if (TextUtils.isEmpty(current)) {
            return 1;
        }
        for (VerticalList.Model m: cachedList) {
            if (current.equals(m.subTitle)) {
                return cachedList.indexOf(m);
            }
        }
        return 0;
    }

    public VerticalFastScrollIndex getScrollIndex() {
        return null;
    }

    public void setBackSelectionPos(int n) {
        this.backSelection = n;
    }

    public void setBackSelectionId(int id) {
        this.backSelectionId = id;
    }

    public void setSelectedIcon() {
        this.vscrollComponent.setSelectedIconColorImage(R.drawable.icon_mm_contacts_2, contactColor, null, 1.0f);
        this.vscrollComponent.selectedText.setText(contactStr);
    }

    public VerticalList.Model getModelfromPos(int pos) {
        if (this.cachedList == null || this.cachedList.size() <= pos) {
            return null;
        }
        return this.cachedList.get(pos);
    }

    public boolean isBindCallsEnabled() {
        return true;
    }

    public void onBindToView(VerticalList.Model model, View view, int pos, VerticalList.ModelState state) {
        // String number;
        // if (model.state != null) {
        //     InitialsImageView imageView = (InitialsImageView) VerticalList.findImageView(view);
        //     InitialsImageView smallImageView = (InitialsImageView) VerticalList.findSmallImageView(view);
        //     CrossFadeImageView crossFadeImageView = (CrossFadeImageView) VerticalList.findCrossFadeImageView(view);
        //     imageView.setTag(null);
        //     // if (model.state instanceof RecentCall) {
        //     //     number = ((RecentCall) model.state).number;
        //     // } else {
        //     //     number = ((Contact) model.state).number;
        //     // }
        //     // File imagePath = PhoneImageDownloader.getInstance().getImagePath(number, PhotoType.PHOTO_CONTACT);
        //     // Bitmap bitmap = PicassoUtil.getBitmapfromCache(imagePath);
        //     // if (bitmap != null) {
        //     //     imageView.setTag(null);
        //     //     imageView.setInitials(null, InitialsImageView.Style.DEFAULT);
        //     //     imageView.setImageBitmap(bitmap);
        //     //     smallImageView.setInitials(null, InitialsImageView.Style.DEFAULT);
        //     //     smallImageView.setImageBitmap(bitmap);
        //     //     smallImageView.setAlpha(0.6f);
        //     //     crossFadeImageView.setSmallAlpha(0.6f);
        //     //     state.updateImage = false;
        //     //     state.updateSmallImage = false;
        //     //     return;
        //     // }
        //     imageView.setTag(model.state);
        //     // TaskManager.getInstance().execute(new LanguageMenu.Anon1(imagePath, imageView, model, pos), 1);
        // }
    }

    public IMenu getChildMenu(IMenu parent2, String args, String path) {
        return null;
    }

    public void onUnload(IMenu.MenuLevel level) {
        switch (level) {
            case CLOSE:
                if (this.returnToCacheList != null) {
                    sLogger.v("cm:unload add to cache");
                    VerticalModelCache.addToCache(this.returnToCacheList);
                    this.returnToCacheList = null;
                }
                return;
            default:
                return;
        }
    }

    public void onItemSelected(VerticalList.ItemSelectionState selection) {
    }

    public void onScrollIdle() {
    }

    public void onFastScrollStart() {
    }

    public void onFastScrollEnd() {
    }

    public void showToolTip() {
    }

    public boolean isFirstItemEmpty() {
        return true;
    }

    public boolean selectItem(VerticalList.ItemSelectionState selection) {
        sLogger.v("select id:" + selection.id + " pos:" + selection.pos);
        switch (selection.id) {
            case R.id.menu_back /*2131624007*/:
                sLogger.v("back");
                break;
            case R.id.menu_auto:
                sLogger.v("auto language");
                setLocale(null);
                break;
            default:
                if (lang_submenu == null) {
                    String lang = (String) this.cachedList.get(selection.pos).state;
                    LanguageMenu sub = new LanguageMenu(this.bus, this.vscrollComponent, this.presenter, this, lang);
                    this.presenter.loadMenu(sub, IMenu.MenuLevel.SUB_LEVEL, selection.pos, 0, false);
                    return true;

                } else {
                    sLogger.v("set language options");
                    setLocale((Locale) this.cachedList.get(selection.pos).state);
                    this.presenter.loadMenu(this.parent, MenuLevel.BACK_TO_PARENT, this.backSelection, this.backSelectionId);
                    this.backSelectionId = 0;
                    return true;
                }
        }
        this.presenter.loadMenu(this.parent, IMenu.MenuLevel.BACK_TO_PARENT, this.backSelection, this.backSelectionId);
        this.backSelectionId = 0;
        return true;
    }

    private void setLocale(Locale locale) {
        HudLocale.setOverrideLocale(locale);
        if (locale == null) {
            locale = DriverProfileHelper.getInstance().getDriverProfileManager().getCurrentLocale();
        }
        final Locale l = locale;
        if (HudLocale.getCurrentLocale(HudApplication.getAppContext()) != locale) {
            TaskManager.getInstance().execute(new Runnable() {
                @Override
                public void run() {
                    if (HudLocale.switchLocale(HudApplication.getAppContext(), l)) {
                        HudLocale.showLocaleChangeToast();
                        LanguageMenu.this.bus.post(new InitEvents.InitPhase(InitEvents.Phase.SWITCHING_LOCALE));
                    }
                }
            }, 10);
        }

    }

    public IMenu.Menu getType() {
        return IMenu.Menu.SETTINGS;
    }

    public boolean isItemClickable(int id, int pos) {
        return true;
    }

    /* access modifiers changed from: 0000 */
    public VerticalList.Model buildModel(int id, String displayname, String displayid) {
        String subTitle;
        VerticalList.Model model;
        // ContactImageHelper contactImageHelper = ContactImageHelper.getInstance();

        // int largeImageRes = contactImageHelper.getResourceId(defaultImageIndex);
        // int fluctuator = contactImageHelper.getResourceColor(defaultImageIndex);
        // if (numberType != NumberType.OTHER) {
        //     subTitle = numberTypeStr;
        // } else {
        //     subTitle = formattedNumber;
        // }
        // model = TitleSubtitleViewHolder.buildModel(displayname, displayid);
        // com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.settings_menu_learning_gestures, com.navdy.hud.app.R.drawable.icon_settings_learn_gestures_2, fluctuatorColor4, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, fluctuatorColor4, title4, null);
        // model = IconsTwoViewHolder.buildModel(id, largeImageRes, com.navdy.hud.app.R.drawable.icon_user_grey, fluctuator, -1, contactName, subTitle);
        model = IconsTwoViewHolder.buildModel(
                id,
                R.drawable.icon_user_bg_4,
                R.drawable.icon_user_numberonly,
                noContactColor,
                -1,
                displayname,
                displayid);
        model.extras = new HashMap<>();
        // model.extras.put(VerticalList.Model.INITIALS, initials);
        return model;
    }

    // /* access modifiers changed from: private */
    private void setImage(File path, int width, int height, int position) {
        PicassoUtil.getInstance().load(path).resize(width, height).transform(roundTransformation).into(new Target() {
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                LanguageMenu.this.presenter.refreshDataforPos(position);
            }

            public void onBitmapFailed(Drawable errorDrawable) {
            }

            public void onPrepareLoad(Drawable placeHolderDrawable) {
            }
        });
    }

}
