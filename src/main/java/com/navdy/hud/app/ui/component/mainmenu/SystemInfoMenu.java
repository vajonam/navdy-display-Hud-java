package com.navdy.hud.app.ui.component.mainmenu;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.service.ConnectionHandler;
import com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState;
import com.navdy.hud.app.ui.component.vlist.VerticalList.Model;
import com.navdy.hud.app.ui.component.vlist.VerticalList.ModelState;
import com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex;
import com.navdy.hud.app.util.CrashReportService;
import com.navdy.service.library.events.DeviceInfo;

import java.util.List;

import lanchon.dexpatcher.annotation.DexAdd;
import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit
public class SystemInfoMenu implements com.navdy.hud.app.ui.component.mainmenu.IMenu {

    @DexIgnore
    private static int UPDATE_FREQUENCY;  // = 2000;
    @DexIgnore
    private static com.navdy.hud.app.ui.component.vlist.VerticalList.Model back;
    @DexIgnore
    private static java.text.SimpleDateFormat dateFormat;  // = new java.text.SimpleDateFormat("MM/dd/yyyy");
    @DexIgnore
    private static java.lang.String fuelUnitText;  // = resources.getString(com.navdy.hud.app.R.string.litre);
    @DexIgnore
    private static com.navdy.hud.app.ui.component.vlist.VerticalList.Model infoModel;  // = com.navdy.hud.app.ui.component.vlist.viewholder.ScrollableViewHolder.buildModel(com.navdy.hud.app.R.layout.system_info_lyt);
    @DexIgnore
    private static java.lang.String infoTitle;  // = resources.getString(com.navdy.hud.app.R.string.carousel_menu_system_info_title);
    @DexIgnore
    private static java.lang.String noData;  // = resources.getString(com.navdy.hud.app.R.string.si_no_data);
    @DexIgnore
    private static java.lang.String noFixStr;  // = resources.getString(com.navdy.hud.app.R.string.gps_no_fix);
    @DexIgnore
    private static java.lang.String off;  // = resources.getString(com.navdy.hud.app.R.string.si_off);
    @DexIgnore
    private static java.lang.String on;  // = resources.getString(com.navdy.hud.app.R.string.si_on);
    @DexIgnore
    private static java.lang.String pressureUnitText;  // = resources.getString(com.navdy.hud.app.R.string.pressure_unit);
    @DexIgnore
    private static android.content.res.Resources resources;  // = com.navdy.hud.app.HudApplication.getAppContext().getResources();
    @DexIgnore
    private static com.navdy.service.library.log.Logger sLogger;  // = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.mainmenu.SystemInfoMenu.class);
    // @butterknife.InjectView(2131624424)
    @DexIgnore
    protected android.widget.TextView autoOn;
    @DexIgnore
    private int backSelection;
    @DexIgnore
    private com.squareup.otto.Bus bus;
    @DexIgnore
    private java.util.List<com.navdy.hud.app.ui.component.vlist.VerticalList.Model> cachedList;
    // @butterknife.InjectView(2131624443)
    @DexIgnore
    protected android.widget.TextView carModel;
    // @butterknife.InjectView(2131624454)
    @DexIgnore
    protected android.widget.TextView checkEngineLight;
    // @butterknife.InjectView(2131624440)
    @DexIgnore
    protected android.widget.TextView deviceMileage;
    // @butterknife.InjectView(2131624464)
    @DexIgnore
    protected android.widget.TextView dialBt;
    // @butterknife.InjectView(2131624462)
    @DexIgnore
    protected android.widget.TextView dialFw;
    // @butterknife.InjectView(2131624465)
    @DexIgnore
    protected android.widget.TextView displayBt;
    // @butterknife.InjectView(2131624461)
    @DexIgnore
    protected android.widget.TextView displayFw;
    // @butterknife.InjectView(2131624458)
    @DexIgnore
    protected android.widget.TextView engineOilPressure;
    // @butterknife.InjectView(2131624457)
    @DexIgnore
    protected android.view.ViewGroup engineOilPressureLayout;
    // @butterknife.InjectView(2131624452)
    @DexIgnore
    protected android.widget.TextView engineTemp;
    // @butterknife.InjectView(2131624455)
    @DexIgnore
    protected android.widget.TextView engineTroubleCodes;
    // @butterknife.InjectView(2131624438)
    @DexIgnore
    protected android.widget.TextView fix;
    // @butterknife.InjectView(2131624446)
    @DexIgnore
    protected android.widget.TextView fuel;
    // @butterknife.InjectView(2131624423)
    @DexIgnore
    protected android.widget.TextView gestures;
    // @butterknife.InjectView(2131624435)
    @DexIgnore
    protected android.widget.TextView gps;
    // @butterknife.InjectView(2131624441)
    @DexIgnore
    protected android.widget.TextView gpsSpeed;
    /* access modifiers changed from: private */
    @DexIgnore
    public android.os.Handler handler;
    @DexIgnore
    private com.navdy.hud.app.maps.here.HereLocationFixManager hereLocationFixManager;
    // @butterknife.InjectView(2131624431)
    @DexIgnore
    protected android.widget.TextView hereMapVerified;
    // @butterknife.InjectView(2131624429)
    @DexIgnore
    protected android.widget.TextView hereMapsVersion;
    // @butterknife.InjectView(2131624430)
    @DexIgnore
    protected android.widget.TextView hereOfflineMaps;
    // @butterknife.InjectView(2131624427)
    @DexIgnore
    protected android.widget.TextView hereSdk;
    // @butterknife.InjectView(2131624428)
    @DexIgnore
    protected android.widget.TextView hereUpdated;
    // @butterknife.InjectView(2131624432)
    @DexIgnore
    protected android.widget.TextView hereVoiceVerified;
    // @butterknife.InjectView(2131624451)
    @DexIgnore
    protected android.widget.TextView maf;
    // @butterknife.InjectView(2131624442)
    @DexIgnore
    protected android.widget.TextView make;
    // @butterknife.InjectView(2131624425)
    @DexIgnore
    protected android.widget.TextView obdData;
    // @butterknife.InjectView(2131624445)
    @DexIgnore
    protected android.view.ViewGroup obdDataLayout;
    // @butterknife.InjectView(2131624463)
    @DexIgnore
    protected android.widget.TextView obdFw;
    @DexIgnore
    private com.navdy.hud.app.obd.ObdManager obdManager;
    // @butterknife.InjectView(2131624450)
    @DexIgnore
    protected android.widget.TextView odo;
    // @butterknife.InjectView(2131624449)
    @DexIgnore
    protected android.view.ViewGroup odometerLayout;
    @DexIgnore
    private com.navdy.hud.app.ui.component.mainmenu.IMenu parent;
    @DexIgnore
    private com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.Presenter presenter;
    @DexIgnore
    protected boolean registered;
    // @butterknife.InjectView(2131624433)
    @DexIgnore
    protected android.widget.TextView routeCalc;
    // @butterknife.InjectView(2131624434)
    @DexIgnore
    protected android.widget.TextView routePref;
    // @butterknife.InjectView(2131624447)
    @DexIgnore
    protected android.widget.TextView rpm;
    // @butterknife.InjectView(2131624437)
    @DexIgnore
    com.navdy.hud.app.ui.component.systeminfo.GpsSignalView satelliteView;
    // @butterknife.InjectView(2131624436)
    @DexIgnore
    protected android.widget.TextView satellites;
    // @butterknife.InjectView(2131624456)
    @DexIgnore
    protected android.view.ViewGroup specialPidsLayout;
    // @butterknife.InjectView(2131624448)
    @DexIgnore
    protected android.widget.TextView speed;
    @DexIgnore
    private com.navdy.hud.app.manager.SpeedManager speedManager;
    // @butterknife.InjectView(2131624439)
    @DexIgnore
    protected android.widget.TextView temperature;
    // @butterknife.InjectView(2131624460)
    @DexIgnore
    protected android.widget.TextView tripFuel;
    // @butterknife.InjectView(2131624459)
    @DexIgnore
    protected android.view.ViewGroup tripFuelLayout;
    @DexIgnore
    protected java.lang.String ubloxFixType;
    // @butterknife.InjectView(2131624426)
    @DexIgnore
    protected android.widget.TextView uiScaling;
    @DexIgnore
    private java.lang.Runnable updateRunnable;  // = new com.navdy.hud.app.ui.component.mainmenu.SystemInfoMenu.AnonymousClass1();
    // @butterknife.InjectView(2131624453)
    @DexIgnore
    protected android.widget.TextView voltage;
    @DexIgnore
    private com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent vscrollComponent;
    // @butterknife.InjectView(2131624444)
    @DexIgnore
    protected android.widget.TextView year;


    @DexEdit
    SystemInfoMenu(com.squareup.otto.Bus bus2, com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent verticalMenuComponent, com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.Presenter presenter2, com.navdy.hud.app.ui.component.mainmenu.IMenu iMenu, @DexIgnore Void tag) {
        throw null;
    }

    @DexAdd
    private static SystemInfoMenu sInstance;

    @DexAdd
    public SystemInfoMenu(com.squareup.otto.Bus bus2, com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent verticalMenuComponent, com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.Presenter presenter2, com.navdy.hud.app.ui.component.mainmenu.IMenu iMenu) {
        this(bus2, verticalMenuComponent, presenter2, iMenu, (Void) null);
        SystemInfoMenu.sInstance = this;
    }

    @DexAdd
    public static SystemInfoMenu getInstance() {
        return sInstance;
    }


    @DexAdd
    private String deviceInfoString() {
        String deviceInfo = "";
        ConnectionHandler connectionHandler = RemoteDeviceManager.getInstance().getConnectionHandler();
        if (connectionHandler != null) {
            DeviceInfo lastConnectedDeviceInfo = connectionHandler.getLastConnectedDeviceInfo();
            if (lastConnectedDeviceInfo != null) {
                deviceInfo += CrashReportService.printDeviceInfo(lastConnectedDeviceInfo);
            }
        }
        return deviceInfo;
    }

    @DexAdd
    public String getDiagnosticText(Context context) {
        this.displayFw = new TextView(context);
        this.dialFw = new TextView(context);
        this.obdFw = new TextView(context);
        this.dialBt = new TextView(context);
        this.displayBt = new TextView(context);
        this.gps = new TextView(context);
        this.gpsSpeed = new TextView(context);
        this.satellites = new TextView(context);
        this.fix = new TextView(context);
        this.temperature = new TextView(context);
        this.deviceMileage = new TextView(context);
        this.hereSdk = new TextView(context);
        this.hereUpdated = new TextView(context);
        this.hereMapsVersion = new TextView(context);
        this.hereOfflineMaps = new TextView(context);
        this.hereMapVerified = new TextView(context);
        this.hereVoiceVerified = new TextView(context);
        this.make = new TextView(context);
        this.carModel = new TextView(context);
        this.year = new TextView(context);
        this.fuel = new TextView(context);
        this.rpm = new TextView(context);
        this.speed = new TextView(context);
        this.odo = new TextView(context);
        this.maf = new TextView(context);
        this.engineTemp = new TextView(context);
        this.voltage = new TextView(context);
        this.gestures = new TextView(context);
        this.autoOn = new TextView(context);
        this.obdData = new TextView(context);
        this.uiScaling = new TextView(context);
        this.routeCalc = new TextView(context);
        this.routePref = new TextView(context);
        this.checkEngineLight = new TextView(context);
        this.engineTroubleCodes = new TextView(context);
        this.engineOilPressure = new TextView(context);
        this.tripFuel = new TextView(context);

        ViewGroup placeholder = new ViewGroup(context) {
            @Override
            protected void onLayout(boolean b, int i, int i1, int i2, int i3) { }
        };
        this.obdDataLayout = placeholder;
        this.specialPidsLayout = placeholder;
        this.odometerLayout = placeholder;
        this.tripFuelLayout = placeholder;

        update();

        String diag =
                "Versions\n" +
                        "--------\n" +
                        "Display Fw:           " + displayFw.getText() + "\n" +
                        "Display Bt:           " + displayBt.getText() + "\n" +
                        "Dial Fw:              " + dialFw.getText() + "\n" +
                        "Dial Bt:              " + dialBt.getText() + "\n" +
                        "Obd Fw:               " + obdFw.getText() + "\n" +
                        "Device Mileage:       " + deviceMileage.getText() + "\n" +
                        "\n" +
                        "Settings\n" +
                        "--------\n" +
                        "Gestures:             " + gestures.getText() + "\n" +
                        "Ui Scaling:           " + uiScaling.getText() + "\n" +
                        "Auto On:              " + autoOn.getText() + "\n" +
                        "\n" +
                        "GPS Stats\n" +
                        "---------\n" +
                        "Gps:                  " + gps.getText() + "\n" +
                        "Fix:                  " + fix.getText() + "\n" +
                        "Satellites:           " + satellites.getText() + "\n" +
                        "Gps Speed:            " + gpsSpeed.getText() + "\n" +
                        "\n" +
                        "Maps\n" +
                        "----\n" +
                        "Here Sdk:             " + hereSdk.getText() + "\n" +
                        "Here Maps Version:    " + hereMapsVersion.getText() + "\n" +
                        "Here Updated:         " + hereUpdated.getText() + "\n" +
                        "Here Map Verified:    " + hereMapVerified.getText() + "\n" +
                        "Here Voice Verified:  " + hereVoiceVerified.getText() + "\n" +
                        "Here Offline Maps:    " + hereOfflineMaps.getText() + "\n" +
                        "\n" +
                        "Navigation\n" +
                        "----------\n" +
                        "Route Calc:           " + routeCalc.getText() + "\n" +
                        "Route Pref:           " + routePref.getText() + "\n" +
                        "\n" +
                        "Vehicle\n" +
                        "-------\n" +
                        "Make:                 " + make.getText() + "\n" +
                        "Car Model:            " + carModel.getText() + "\n" +
                        "Year:                 " + year.getText() + "\n" +
                        "Obd Data:             " + obdData.getText() + "\n" +
                        "Engine Oil Pressure:  " + engineOilPressure.getText() + "\n" +
                        "Engine Temp:          " + engineTemp.getText() + "\n" +
                        "Fuel:                 " + fuel.getText() + "\n" +
                        "Maf:                  " + maf.getText() + "\n" +
                        "Odo:                  " + odo.getText() + "\n" +
                        "Rpm:                  " + rpm.getText() + "\n" +
                        "Speed:                " + speed.getText() + "\n" +
                        "Temperature:          " + temperature.getText() + "\n" +
                        "Trip Fuel:            " + tripFuel.getText() + "\n" +
                        "Voltage:              " + voltage.getText() + "\n" +
                        "Check Engine Light:   " + checkEngineLight.getText() + "\n" +
                        "Engine Trouble Codes: " + engineTroubleCodes.getText() + "\n" +
                        deviceInfoString();
        this.displayFw = null;
        this.dialFw = null;
        this.obdFw = null;
        this.dialBt = null;
        this.displayBt = null;
        this.gps = null;
        this.satellites = null;
        this.fix = null;
        this.temperature = null;
        this.deviceMileage = null;
        this.hereSdk = null;
        this.hereUpdated = null;
        this.hereMapsVersion = null;
        this.hereOfflineMaps = null;
        this.hereMapVerified = null;
        this.hereVoiceVerified = null;
        this.make = null;
        this.carModel = null;
        this.year = null;
        this.fuel = null;
        this.rpm = null;
        this.speed = null;
        this.odo = null;
        this.maf = null;
        this.engineTemp = null;
        this.voltage = null;
        this.gestures = null;
        this.autoOn = null;
        this.obdData = null;
        this.uiScaling = null;
        this.routeCalc = null;
        this.routePref = null;
        this.checkEngineLight = null;
        this.engineTroubleCodes = null;
        this.engineOilPressure = null;
        this.tripFuel = null;
        this.obdDataLayout = null;
        this.specialPidsLayout = null;
        this.odometerLayout = null;
        this.tripFuelLayout = null;
        return diag;
    }

    @DexIgnore
    private void update() {
        // updateVersions();
        // updateStats();
        // updateHere();
        // updateVehicleInfo();
        // updateGenericPref();
        // updateNavPref();
    }

    @DexReplace
    private void updateVersions() {
        java.lang.String str = alelec.navdy.hud.app.BuildConfig.VERSION_NAME;
        if (com.navdy.hud.app.util.DeviceUtil.isUserBuild()) {
            str = com.navdy.hud.app.util.OTAUpdateService.shortVersion(alelec.navdy.hud.app.BuildConfig.VERSION_NAME);
        }
        this.displayFw.setText(str);
        com.navdy.hud.app.device.dial.DialManager instance = com.navdy.hud.app.device.dial.DialManager.getInstance();
        if (instance.isDialConnected()) {
            try {
                com.navdy.hud.app.device.dial.DialFirmwareUpdater dialFirmwareUpdater = instance.getDialFirmwareUpdater();
                java.lang.String str2 = null;
                if (dialFirmwareUpdater != null) {
                    int i = dialFirmwareUpdater.getVersions().dial.incrementalVersion;
                    if (i != -1) {
                        str2 = java.lang.String.valueOf(i);
                    }
                    if (!android.text.TextUtils.isEmpty(str2)) {
                        this.dialFw.setText(str2);
                    }
                }
            } catch (Throwable th) {
                sLogger.e(th);
            }
            java.lang.String dialName = instance.getDialName();
            if (!android.text.TextUtils.isEmpty(dialName)) {
                this.dialBt.setText(removeBrackets(dialName));
            }
        }
        java.lang.String name = android.bluetooth.BluetoothAdapter.getDefaultAdapter().getName();
        if (!android.text.TextUtils.isEmpty(name)) {
            this.displayBt.setText(removeBrackets(name));
        }
        java.lang.String obdChipFirmwareVersion = this.obdManager.getObdChipFirmwareVersion();
        if (!android.text.TextUtils.isEmpty(obdChipFirmwareVersion)) {
            this.obdFw.setText(obdChipFirmwareVersion);
        }
    }

    @DexIgnore
    private java.lang.String removeBrackets(java.lang.String str) {
        return str.replace(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.OPEN_BRACKET, "").replace(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.CLOSE_BRACKET, "");
    }

    @DexIgnore
    public IMenu getChildMenu(IMenu iMenu, String s, String s1) {
        return null;
    }

    @DexIgnore
    public int getInitialSelection() {
        return 0;
    }

    @DexIgnore
    public List<Model> getItems() {
        return null;
    }

    @DexIgnore
    public Model getModelfromPos(int i) {
        return null;
    }

    @DexIgnore
    public VerticalFastScrollIndex getScrollIndex() {
        return null;
    }

    @DexIgnore
    public Menu getType() {
        return null;
    }

    @DexIgnore
    public boolean isBindCallsEnabled() {
        return false;
    }

    @DexIgnore
    public boolean isFirstItemEmpty() {
        return false;
    }

    @DexIgnore
    public boolean isItemClickable(int i, int i1) {
        return false;
    }

    @DexIgnore
    public void onBindToView(Model model, View view, int i, ModelState modelState) {

    }

    @DexIgnore
    public void onFastScrollEnd() {

    }

    @DexIgnore
    public void onFastScrollStart() {

    }

    @DexIgnore
    public void onItemSelected(ItemSelectionState itemSelectionState) {

    }

    @DexIgnore
    public void onScrollIdle() {

    }

    @DexIgnore
    public void onUnload(MenuLevel menuLevel) {

    }

    @DexIgnore
    public boolean selectItem(ItemSelectionState itemSelectionState) {
        return false;
    }

    @DexIgnore
    public void setBackSelectionId(int i) {

    }

    @DexIgnore
    public void setBackSelectionPos(int i) {

    }

    @DexIgnore
    public void setSelectedIcon() {

    }

    @DexIgnore
    public void showToolTip() {

    }
}
