package com.navdy.hud.app.ui.component;

import com.navdy.hud.app.config.SettingsManager;

import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit
public class UISettings {
    @DexIgnore
    private static /* final */ java.lang.String ADVANCED_GPS_STATS; // = "persist.sys.gps.stats";
    @DexIgnore
    private static /* final */ java.lang.String DIAL_LONG_PRESS_ACTION_PLACE_SEARCH; // = "persist.sys.dlpress_search";
    @DexIgnore
    private static /* final */ boolean dialLongPressPlaceSearchAction; // = com.navdy.hud.app.util.os.SystemProperties.getBoolean(DIAL_LONG_PRESS_ACTION_PLACE_SEARCH, false);

    @DexIgnore
    public static boolean isMusicBrowsingEnabled() {
        return true;
    }

    @DexIgnore
    public static boolean isVerticalListNoCloseTimeout() {
        return true;
    }

    @DexReplace
    public static boolean isLongPressActionPlaceSearch() {
        return SettingsManager.global.getBoolean(DIAL_LONG_PRESS_ACTION_PLACE_SEARCH, false);
    }

    @DexIgnore
    public static boolean supportsIosSms() {
        return true;
    }

    @DexReplace
    public static boolean advancedGpsStatsEnabled() {
        return SettingsManager.global.getBoolean(ADVANCED_GPS_STATS, false);
    }
}
