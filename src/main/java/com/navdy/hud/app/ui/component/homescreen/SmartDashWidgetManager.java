package com.navdy.hud.app.ui.component.homescreen;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.config.SettingsManager;
import com.navdy.hud.app.framework.DriverProfileHelper;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.obd.ObdManager;
import com.navdy.hud.app.profile.NavdyPreferences;
import com.navdy.hud.app.view.AmbientTemperaturePresenter;
import com.navdy.hud.app.view.CompassPresenter;
import com.navdy.hud.app.view.DashboardWidgetPresenter;
import com.navdy.hud.app.view.DriveScoreGaugePresenter;
import com.navdy.hud.app.view.EngineOilTemperaturePresenter;
import com.navdy.hud.app.view.EngineTemperaturePresenter;
import com.navdy.hud.app.view.FuelGaugePresenter2;
import com.navdy.hud.app.view.IntakePressurePresenter;
import com.navdy.hud.app.view.MPGGaugePresenter;
import com.navdy.hud.app.view.SpeedLimitSignPresenter;
import com.navdy.service.library.log.Logger;
import com.navdy.obd.Pids;
import com.squareup.otto.Bus;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexAction;
import lanchon.dexpatcher.annotation.DexAdd;
import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit(staticConstructorAction = DexAction.REPLACE)
public class SmartDashWidgetManager {
    @DexEdit
    public static final String ANALOG_CLOCK_WIDGET_ID = "ANALOG_CLOCK_WIDGET";
    @DexEdit
    public static final String CALENDAR_WIDGET_ID = "CALENDAR_WIDGET";
    @DexEdit
    public static final String COMPASS_WIDGET_ID = "COMPASS_WIDGET";
    @DexEdit
    public static final String DIGITAL_CLOCK_2_WIDGET_ID = "DIGITAL_CLOCK_2_WIDGET";
    @DexEdit
    public static final String DIGITAL_CLOCK_WIDGET_ID = "DIGITAL_CLOCK_WIDGET";
    @DexEdit
    public static final String DRIVE_SCORE_GAUGE_ID = "DRIVE_SCORE_GAUGE_ID";
    @DexEdit
    public static final String EMPTY_WIDGET_ID = "EMPTY_WIDGET";
    @DexEdit
    public static final String ENGINE_TEMPERATURE_GAUGE_ID = "ENGINE_TEMPERATURE_GAUGE_ID";
    @DexEdit
    public static final String ETA_GAUGE_ID = "ETA_GAUGE_ID";
    @DexEdit
    public static final String FUEL_GAUGE_ID = "FUEL_GAUGE_ID";
    @DexEdit
    private static List<String> GAUGES = new java.util.ArrayList<>();
    @DexEdit
    private static HashSet<String> GAUGE_NAME_LOOKUP = new java.util.HashSet<>();
    @DexEdit
    public static final String GFORCE_WIDGET_ID = "GFORCE_WIDGET";
    @DexEdit
    public static final String MPG_AVG_WIDGET_ID = "MPG_AVG_WIDGET";
    @DexEdit
    public static final String MPG_GRAPH_WIDGET_ID = "MPG_GRAPH_WIDGET";
    @DexEdit
    public static final String MUSIC_WIDGET_ID = "MUSIC_WIDGET";
    @DexEdit
    public static final String PREFERENCE_GAUGE_ENABLED = "PREF_GAUGE_ENABLED_";
    @DexEdit
    public static final String SPEED_LIMIT_SIGN_GAUGE_ID = "SPEED_LIMIT_SIGN_GAUGE_ID";
    @DexEdit
    public static final String TRAFFIC_INCIDENT_GAUGE_ID = "TRAFFIC_INCIDENT_GAUGE_ID";
    @DexEdit
    public static final String WEATHER_WIDGET_ID = "WEATHER_GRAPH_WIDGET";
    @DexEdit
    private static final Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.class);
    @DexIgnore
    private Bus bus;
    @DexAdd
    private NavdyPreferences currentUserPreferences;
    @DexIgnore
    private SmartDashWidgetManager.IWidgetFilter filter;
    @DexAdd
    private NavdyPreferences globalPreferences;
    @DexIgnore
    private SmartDashWidgetManager.LifecycleEvent lastLifeCycleEvent = null;
    @DexIgnore
    private Context mContext;
    @DexIgnore
    private List<String> mGaugeIds;
    @DexIgnore
    private boolean mLoaded = false;
    @DexIgnore
    private HashMap<String, Integer> mWidgetIndexMap;
    @DexIgnore
    private HashMap<Integer, SmartDashWidgetManager.SmartDashWidgetCache> mWidgetPresentersCache = new java.util.HashMap<>();

    @DexAdd
    public static final String INTAKE_PRESSURE_GAUGE_ID = "INTAKE_PRESSURE_GAUGE_ID";
    @DexAdd
    public static final String ENGINE_OIL_TEMPERATURE_GAUGE_ID = "ENGINE_OIL_TEMPERATURE_GAUGE_ID";
    @DexAdd
    public static final String AMBIENT_TEMPERATURE_GAUGE_ID = "AMBIENT_TEMPERATURE_GAUGE_ID";
    @DexAdd
    public static final String FUEL_GAUGE_RANGE_ID = "FUEL_GAUGE_RANGE_ID";

    static {
        GAUGES.add(CALENDAR_WIDGET_ID);
        GAUGES.add(COMPASS_WIDGET_ID);
        GAUGES.add(ANALOG_CLOCK_WIDGET_ID);
        GAUGES.add(DIGITAL_CLOCK_WIDGET_ID);
        GAUGES.add(DIGITAL_CLOCK_2_WIDGET_ID);
        GAUGES.add(DRIVE_SCORE_GAUGE_ID);
        GAUGES.add(ENGINE_TEMPERATURE_GAUGE_ID);
        GAUGES.add(ENGINE_OIL_TEMPERATURE_GAUGE_ID);
        GAUGES.add(AMBIENT_TEMPERATURE_GAUGE_ID);
        GAUGES.add(FUEL_GAUGE_ID);
        GAUGES.add(GFORCE_WIDGET_ID);
        GAUGES.add(MUSIC_WIDGET_ID);
        GAUGES.add(MPG_AVG_WIDGET_ID);
        GAUGES.add(SPEED_LIMIT_SIGN_GAUGE_ID);
        GAUGES.add(TRAFFIC_INCIDENT_GAUGE_ID);
        GAUGES.add(INTAKE_PRESSURE_GAUGE_ID);
        GAUGES.add(ETA_GAUGE_ID);

        GAUGES.add(EMPTY_WIDGET_ID);

        GAUGE_NAME_LOOKUP.addAll(GAUGES);
    }
    @DexIgnore
    public interface IWidgetFilter {
        boolean filter(String str);
    }

    @DexIgnore
    public enum LifecycleEvent {
        PAUSE,
        RESUME
    }

    @DexIgnore
    public enum Reload {
        RELOAD_CACHE,
        RELOADED
    }

    @DexEdit(defaultAction = DexAction.ADD)
    public class SmartDashWidgetCache {
        @DexIgnore
        private Context mContext;
        @DexIgnore
        private List<String> mGaugeIds = new ArrayList<>();
        @DexIgnore
        private HashMap<String, Integer> mWidgetIndexMap = new HashMap<>();
        @DexIgnore
        private HashMap<String, DashboardWidgetPresenter> mWidgetPresentersMap = new HashMap<>();
        @DexIgnore
        public SmartDashWidgetCache(Context context) {
            this.mContext = context;
        }

        @DexIgnore
        public void add(String identifier) {
            if (!this.mWidgetIndexMap.containsKey(identifier)) {
                this.mGaugeIds.add(identifier);
                this.mWidgetIndexMap.put(identifier, this.mGaugeIds.size() - 1);
                this.mWidgetPresentersMap.put(identifier, DashWidgetPresenterFactory.createDashWidgetPresenter(this.mContext, identifier));
                initializeWidget(identifier);
            }
        }

        @DexIgnore
        public int getIndexForWidget(String identifier) {
            if (this.mWidgetIndexMap.containsKey(identifier)) {
                return this.mWidgetIndexMap.get(identifier);
            }
            return -1;
        }

        @DexIgnore
        public DashboardWidgetPresenter getWidgetPresenter(int index) {
            if (index < 0 || index >= this.mGaugeIds.size()) {
                return null;
            }
            return this.mWidgetPresentersMap.get(this.mGaugeIds.get(index));
        }

        @DexIgnore
        public DashboardWidgetPresenter getWidgetPresenter(String identifier) {
            return getWidgetPresenter(getIndexForWidget(identifier));
        }

        @DexIgnore
        public int getWidgetsCount() {
            return this.mGaugeIds.size();
        }

        @DexReplace
        private void initializeWidget(String identifier) {
            switch (identifier) {
                case SmartDashWidgetManager.FUEL_GAUGE_ID:
                    FuelGaugePresenter2 fuelGaugePresenter2 = (FuelGaugePresenter2) getWidgetPresenter(identifier);
                    int fuelLevel = ObdManager.getInstance().getFuelLevel();
                    if (fuelGaugePresenter2 != null) {
                        fuelGaugePresenter2.setFuelLevel(fuelLevel);
                        fuelGaugePresenter2.setFuelRange(0);
                        break;
                    }
                    break;
                case SmartDashWidgetManager.DRIVE_SCORE_GAUGE_ID:
                    DriveScoreGaugePresenter driveScoreGaugePresenter = (DriveScoreGaugePresenter) getWidgetPresenter(identifier);
                    if (driveScoreGaugePresenter != null) {
                        driveScoreGaugePresenter.setDriveScoreUpdated(RemoteDeviceManager.getInstance().getTelemetryDataManager().getDriveScoreUpdatedEvent());
                        break;
                    }
                    break;
                case SmartDashWidgetManager.ENGINE_TEMPERATURE_GAUGE_ID:
                    EngineTemperaturePresenter engineTemperaturePresenter = (EngineTemperaturePresenter) getWidgetPresenter(identifier);
                    double engineTemperature = ObdManager.getInstance().getPidValue(5);
                    if (engineTemperaturePresenter != null) {
                        if (engineTemperature == -1.0d) {
                            engineTemperature = EngineTemperaturePresenter.Companion.getTEMPERATURE_GAUGE_MID_POINT_CELSIUS();
                        }
                        engineTemperaturePresenter.setEngineTemperature(engineTemperature);
                        break;
                    }
                    break;
                case SmartDashWidgetManager.ENGINE_OIL_TEMPERATURE_GAUGE_ID: {
                    final EngineOilTemperaturePresenter engineOilTemperaturePresenter = (EngineOilTemperaturePresenter) getWidgetPresenter(identifier);
                    double engineOilTemperature = ObdManager.getInstance().getPidValue(Pids.ENGINE_OIL_TEMPERATURE);
                    if (engineOilTemperaturePresenter != null) {
                        if (engineOilTemperature == -1.0) {
                            engineOilTemperature = EngineOilTemperaturePresenter.Companion.getTEMPERATURE_GAUGE_MID_POINT_CELSIUS();
                        }
                        engineOilTemperaturePresenter.setEngineTemperature(engineOilTemperature);
                        break;
                    }
                    break;
                }
                case SmartDashWidgetManager.AMBIENT_TEMPERATURE_GAUGE_ID: {
                    final AmbientTemperaturePresenter ambientTemperaturePresenter = (AmbientTemperaturePresenter) getWidgetPresenter(identifier);
                    double ambientTemperature = ObdManager.getInstance().getPidValue(Pids.AMBIENT_AIR_TEMRERATURE);
                    if (ambientTemperaturePresenter != null) {
                        if (ambientTemperature == -1.0) {
                            ambientTemperature = AmbientTemperaturePresenter.Companion.getTEMPERATURE_GAUGE_MID_POINT_CELSIUS();
                        }
                        ambientTemperaturePresenter.setEngineTemperature(ambientTemperature);
                        break;
                    }
                    break;
                }
                case SmartDashWidgetManager.INTAKE_PRESSURE_GAUGE_ID: {
                    final IntakePressurePresenter intakePressurePresenter = (IntakePressurePresenter) getWidgetPresenter(identifier);
                    double intakePressure = ObdManager.getInstance().getPidValue(Pids.MANIFOLD_AIR_PRESSURE);
                    if (intakePressurePresenter != null) {
                        if (intakePressure == -1.0) {
                            intakePressure = 0.0f;
                        }
                        intakePressurePresenter.setIntakePressure(intakePressure);
                        break;
                    }
                    break;
                }
                default:
                    // return;
            }
        }

        @DexReplace
        public void updateWidget(final String s, final Object o) {
            if (!TextUtils.isEmpty(s)) {
                switch (s) {
                    case FUEL_GAUGE_ID: {
                        final FuelGaugePresenter2 fuelGaugePresenter2 = (FuelGaugePresenter2)this.getWidgetPresenter(s);
                        if (fuelGaugePresenter2 != null && o instanceof Double) {
                            fuelGaugePresenter2.setFuelLevel(((Number)o).intValue());

                            break;
                        }
                        break;
                    }
                    case FUEL_GAUGE_RANGE_ID: {
                        final FuelGaugePresenter2 fuelGaugePresenter2 = (FuelGaugePresenter2)this.getWidgetPresenter(s);
                        if (fuelGaugePresenter2 != null && o instanceof Double) {
                            fuelGaugePresenter2.setFuelRange(((Number)o).intValue());
                            break;
                        }
                        break;
                    }
                    case COMPASS_WIDGET_ID: {
                        final CompassPresenter compassPresenter = (CompassPresenter)this.getWidgetPresenter(s);
                        if (compassPresenter != null) {
                            compassPresenter.setHeadingAngle(((Number)o).doubleValue());
                            break;
                        }
                        break;
                    }
                    case MPG_AVG_WIDGET_ID: {
                        final MPGGaugePresenter mpgGaugePresenter = (MPGGaugePresenter)this.getWidgetPresenter(s);
                        if (mpgGaugePresenter != null) {
                            mpgGaugePresenter.setCurrentMPG(((Number)o).doubleValue());
                            break;
                        }
                        break;
                    }
                    case SPEED_LIMIT_SIGN_GAUGE_ID: {
                        final SpeedLimitSignPresenter speedLimitSignPresenter = (SpeedLimitSignPresenter)this.getWidgetPresenter(s);
                        if (speedLimitSignPresenter != null) {
                            speedLimitSignPresenter.setSpeedLimit(((Number)o).intValue());
                            break;
                        }
                        break;
                    }
                    case ENGINE_OIL_TEMPERATURE_GAUGE_ID: {
                        final EngineOilTemperaturePresenter engineOilTemperaturePresenter = (EngineOilTemperaturePresenter)this.getWidgetPresenter(s);
                        if (engineOilTemperaturePresenter != null) {
                            engineOilTemperaturePresenter.setEngineTemperature(((Number)o).intValue());
                            break;
                        }
                        break;
                    }
                    case ENGINE_TEMPERATURE_GAUGE_ID: {
                        final EngineTemperaturePresenter engineTemperaturePresenter = (EngineTemperaturePresenter)this.getWidgetPresenter(s);
                        if (engineTemperaturePresenter != null) {
                            engineTemperaturePresenter.setEngineTemperature(((Number)o).intValue());
                            break;
                        }
                        break;
                    }
                    case AMBIENT_TEMPERATURE_GAUGE_ID: {
                        final AmbientTemperaturePresenter ambientTemperaturePresenter = (AmbientTemperaturePresenter)this.getWidgetPresenter(s);
                        if (ambientTemperaturePresenter != null) {
                            ambientTemperaturePresenter.setEngineTemperature(((Number)o).intValue());
                            break;
                        }
                        break;
                    }
                    case INTAKE_PRESSURE_GAUGE_ID: {
                        final IntakePressurePresenter intakePressurePresenter = (IntakePressurePresenter)this.getWidgetPresenter(s);
                        if (intakePressurePresenter != null) {
                            intakePressurePresenter.setIntakePressure(((Number)o).intValue());
                            break;
                        }
                        break;
                    }
                }
            }
        }

        @DexIgnore
        public HashMap<String, DashboardWidgetPresenter> getWidgetPresentersMap() {
            return this.mWidgetPresentersMap;
        }

        @DexIgnore
        public void clear() {
            this.mGaugeIds.clear();
            this.mWidgetPresentersMap.clear();
            this.mWidgetIndexMap.clear();
        }
    }

    @DexIgnore
    public static class UserPreferenceChanged {
    }

    @DexReplace
    public boolean isGaugeOn(String gaugeIdentifier) {
        if (this.currentUserPreferences == null) {
            sLogger.d("isGaugeOn : Current Preferences is not set, using the global preferences");
            this.currentUserPreferences = this.globalPreferences;
        }
        return this.currentUserPreferences.getBoolean(PREFERENCE_GAUGE_ENABLED + gaugeIdentifier, true);
    }

    @DexReplace
    public void setGaugeOn(String identifier, boolean on) {
        if (this.currentUserPreferences == null) {
            sLogger.d("setGaugeOn : Current Preferences is not set, using the global preferences");
            this.currentUserPreferences = this.globalPreferences;
        }
        this.currentUserPreferences.edit().putBoolean(PREFERENCE_GAUGE_ENABLED + identifier, on).apply();
    }

    @DexIgnore
    public static boolean isValidGaugeId(String gaugeName) {
        return !TextUtils.isEmpty(gaugeName) && GAUGE_NAME_LOOKUP.contains(gaugeName);
    }

    @DexReplace
    public SmartDashWidgetManager(SharedPreferences globalPreferences2, Context context) {
        this.globalPreferences = SettingsManager.global;
        this.mContext = context;
        this.mGaugeIds = new ArrayList<>();
        this.mWidgetIndexMap = new HashMap<>();
        this.bus = new Bus();
    }

    @DexReplace
    public void reLoadAvailableWidgets(boolean loadAll) {
        sLogger.v("reLoadAvailableWidgets");
        this.mGaugeIds.clear();
        this.mWidgetIndexMap.clear();
        boolean isDefaultProfile = DriverProfileHelper.getInstance().getCurrentProfile().isDefaultProfile();
        sLogger.d("Default profile ? " + isDefaultProfile);
        this.currentUserPreferences = isDefaultProfile ? this.globalPreferences : DriverProfileHelper.getInstance().getDriverProfileManager().getLocalPreferencesForCurrentDriverProfile(HudApplication.getAppContext());
        int i = 0;
        for (String identifier : GAUGES) {
            if ((loadAll || isGaugeOn(identifier)) && this.filter != null && !this.filter.filter(identifier)) {
                this.mGaugeIds.add(identifier);
                sLogger.d("adding Gauge : " + identifier + ", at : " + i);
                int i2 = i + 1;
                this.mWidgetIndexMap.put(identifier, i);
                i = i2;
            } else {
                sLogger.d("Not adding Gauge, as its filtered out , ID : " + identifier);
            }
        }
        if (this.mGaugeIds.size() == 0) {
            this.mGaugeIds.add(EMPTY_WIDGET_ID);
            this.mWidgetIndexMap.put(EMPTY_WIDGET_ID, 0);
        }
        if (this.mLoaded) {
            this.bus.post(SmartDashWidgetManager.Reload.RELOAD_CACHE);
            this.bus.post(SmartDashWidgetManager.Reload.RELOADED);
            return;
        }
        this.mLoaded = true;
    }

    @DexIgnore
    public int getIndexForWidgetIdentifier(String identifier) {
        if (this.mWidgetIndexMap.containsKey(identifier)) {
            return this.mWidgetIndexMap.get(identifier);
        }
        return -1;
    }

    @DexIgnore
    public String getWidgetIdentifierForIndex(int index) {
        if (index < 0 || index >= this.mGaugeIds.size()) {
            return null;
        }
        return this.mGaugeIds.get(index);
    }

    @DexIgnore
    public void registerForChanges(Object object) {
        this.bus.register(object);
    }

    @DexIgnore
    public SmartDashWidgetManager.SmartDashWidgetCache buildSmartDashWidgetCache(int clientIdentifier) {
        SmartDashWidgetManager.SmartDashWidgetCache cache;
        if (this.mWidgetPresentersCache.containsKey(clientIdentifier)) {
            cache = this.mWidgetPresentersCache.get(clientIdentifier);
            cache.clear();
            sLogger.v("widget::: cache cleared for " + clientIdentifier);
        } else {
            cache = new SmartDashWidgetManager.SmartDashWidgetCache(this.mContext);
            this.mWidgetPresentersCache.put(clientIdentifier, cache);
            sLogger.v("widget::: cache created for " + clientIdentifier);
        }
        for (String gaugeId : this.mGaugeIds) {
            cache.add(gaugeId);
        }
        if (this.lastLifeCycleEvent != null) {
            sendLifecycleEvent(this.lastLifeCycleEvent, cache.getWidgetPresentersMap().values().iterator());
        }
        return cache;
    }

    @DexIgnore
    public void updateWidget(String identifier, Object data) {
        Collection<SmartDashWidgetManager.SmartDashWidgetCache> cacheCollection = this.mWidgetPresentersCache.values();
        if (cacheCollection != null) {
            for (SmartDashWidgetManager.SmartDashWidgetCache cache : cacheCollection) {
                if (cache != null) {
                    cache.updateWidget(identifier, data);
                }
            }
        }
    }

    @DexIgnore
    public void onPause() {
        this.lastLifeCycleEvent = SmartDashWidgetManager.LifecycleEvent.PAUSE;
        sendLifecycleEvent(SmartDashWidgetManager.LifecycleEvent.PAUSE);
    }

    @DexIgnore
    public void onResume() {
        this.lastLifeCycleEvent = SmartDashWidgetManager.LifecycleEvent.RESUME;
        sendLifecycleEvent(SmartDashWidgetManager.LifecycleEvent.RESUME);
    }

    @DexIgnore
    private void sendLifecycleEvent(SmartDashWidgetManager.LifecycleEvent event) {
        for (SmartDashWidgetManager.SmartDashWidgetCache cache : this.mWidgetPresentersCache.values()) {
            sendLifecycleEvent(event, cache.getWidgetPresentersMap().values().iterator());
        }
    }

    @DexIgnore
    private void sendLifecycleEvent(SmartDashWidgetManager.LifecycleEvent event, Iterator<DashboardWidgetPresenter> iterator) {
        while (iterator.hasNext()) {
            switch (event) {
                case PAUSE:
                    iterator.next().onPause();
                    break;
                case RESUME:
                    iterator.next().onResume();
                    break;
            }
        }
    }

    @DexIgnore
    public void setFilter(SmartDashWidgetManager.IWidgetFilter filter2) {
        this.filter = filter2;
    }
}
