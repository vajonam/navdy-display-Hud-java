package com.navdy.hud.app.settings;

import android.content.SharedPreferences;

import com.navdy.hud.app.config.SettingsManager;
import com.navdy.hud.app.profile.NavdyPreferences;
import com.navdy.service.library.events.settings.Setting;
import com.navdy.service.library.log.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import lanchon.dexpatcher.annotation.DexAction;
import lanchon.dexpatcher.annotation.DexAdd;
import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit(staticConstructorAction = DexAction.REPLACE)
public class HUDSettings {
    @DexIgnore
    private static final String ADAPTIVE_AUTOBRIGHTNESS_PROP = "persist.sys.autobright_adaptive";
    @DexIgnore
    public static final String AUTO_BRIGHTNESS = "screen.auto_brightness";
    @DexIgnore
    public static final String AUTO_BRIGHTNESS_ADJUSTMENT = "screen.auto_brightness_adj";
    @DexIgnore
    public static final String BRIGHTNESS = "screen.brightness";
    @DexIgnore
    public static final float BRIGHTNESS_SCALE = 255.0f;
    @DexIgnore
    public static final String GESTURE_ENGINE = "gesture.engine";
    @DexIgnore
    public static final String GESTURE_PREVIEW = "gesture.preview";
    @DexIgnore
    public static final String LED_BRIGHTNESS = "screen.led_brightness";
    @DexIgnore
    public static final String MAP_ANIMATION_MODE = "map.animation.mode";
    @DexIgnore
    public static final String MAP_SCHEME = "map.scheme";
    @DexIgnore
    public static final String MAP_TILT = "map.tilt";
    @DexIgnore
    public static final String MAP_ZOOM = "map.zoom";
    @DexEdit
    public static boolean USE_ADAPTIVE_AUTOBRIGHTNESS;
    @DexIgnore
    private static Logger sLogger = new Logger(HUDSettings.class);

    @DexAdd
    private NavdyPreferences preferences;

    @DexReplace
    public HUDSettings(SharedPreferences preferences2) {
        this.preferences = SettingsManager.global;
        USE_ADAPTIVE_AUTOBRIGHTNESS = preferences.getBoolean(ADAPTIVE_AUTOBRIGHTNESS_PROP, true);
    }

    @DexReplace
    public List<String> availableSettings() {
        return new ArrayList<>(this.preferences.getAll().keySet());
    }

    @DexReplace
    public void updateSettings(List<Setting> newSettings) {
        NavdyPreferences editor = this.preferences.edit();
        for (int i = 0; i < newSettings.size(); i++) {
            Setting newSetting = newSettings.get(i);
            if (newSetting.value.equals("-1")) {
                switch (newSetting.key) {
                    case MAP_TILT:
                        set(editor, newSetting.key, String.valueOf(60.0f));
                        break;
                    case MAP_ZOOM:
                        set(editor, newSetting.key, String.valueOf(16.5f));
                        break;
                }
            } else {
                set(editor, newSetting.key, newSetting.value);
            }
        }
        editor.apply();
    }

    @DexReplace
    public List<Setting> readSettings(List<String> keys) {
        List<Setting> result = new ArrayList<>();
        Map<String, ?> allPreferences = this.preferences.getAll();
        for (int i = 0; i < keys.size(); i++) {
            String key = keys.get(i);
            Object object = allPreferences.get(key);
            if (object != null) {
                result.add(new Setting(key, object.toString()));
            }
        }
        return result;
    }

    @DexReplace
    private Object get(String key) {
        return this.preferences.getAll().get(key);
    }

    @DexAdd
    private void set(NavdyPreferences editor, String key, String value) {
        Object oldValue = get(key);
        if (oldValue instanceof String) {
            editor.putString(key, value);
        } else if (oldValue instanceof Boolean) {
            editor.putBoolean(key, Boolean.valueOf(value));
        }
    }
}
