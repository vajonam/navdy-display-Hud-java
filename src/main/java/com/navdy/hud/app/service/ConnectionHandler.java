package com.navdy.hud.app.service;

import android.app.AlarmManager;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.text.TextUtils;

import com.glympse.android.lib.StaticConfig;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.hud.app.common.TimeHelper;
import com.navdy.hud.app.device.PowerManager;
import com.navdy.hud.app.device.dial.DialManager;
import com.navdy.hud.app.event.DeviceInfoAvailable;
import com.navdy.hud.app.event.Disconnect;
import com.navdy.hud.app.event.DriverProfileChanged;
import com.navdy.hud.app.event.LocalSpeechRequest;
import com.navdy.hud.app.event.RemoteEvent;
import com.navdy.hud.app.event.ShowScreenWithArgs;
import com.navdy.hud.app.event.Shutdown;
import com.navdy.hud.app.event.Wakeup;
import com.navdy.hud.app.framework.DriverProfileHelper;
import com.navdy.hud.app.framework.network.NetworkStateManager;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import com.navdy.hud.app.framework.phonecall.CallUtils;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.profile.DriverProfile;
import com.navdy.hud.app.profile.DriverProfileManager;
import com.navdy.hud.app.screen.WelcomeScreen;
import com.navdy.hud.app.ui.framework.UIStateManager;
import com.navdy.hud.app.util.DeviceUtil;
import com.navdy.hud.app.util.OTAUpdateService;
import com.navdy.hud.app.util.picasso.PicassoUtil;
import com.navdy.service.library.Version;
import com.navdy.service.library.device.NavdyDeviceId;
import com.navdy.service.library.device.RemoteDevice;
import com.navdy.service.library.events.Capabilities;
import com.navdy.service.library.events.DeviceInfo;
import com.navdy.service.library.events.LegacyCapability;
import com.navdy.service.library.events.NavdyEventUtil;
import com.navdy.service.library.events.audio.NowPlayingUpdateRequest;
import com.navdy.service.library.events.callcontrol.CallStateUpdateRequest;
import com.navdy.service.library.events.connection.ConnectionRequest;
import com.navdy.service.library.events.connection.ConnectionStateChange;
import com.navdy.service.library.events.connection.LinkPropertiesChanged;
import com.navdy.service.library.events.connection.NetworkLinkReady;
import com.navdy.service.library.events.debug.StartDriveRecordingEvent;
import com.navdy.service.library.events.preferences.LocalPreferences;
import com.navdy.service.library.events.preferences.NotificationPreferences;
import com.navdy.service.library.events.settings.DateTimeConfiguration;
import com.navdy.service.library.events.ui.Screen;
import com.navdy.service.library.events.ui.ShowScreen;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Bus;
import com.squareup.wire.Message;

import java.util.List;
import java.util.TimeZone;

import alelec.navdy.hud.app.BuildConfig;
import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit
public class ConnectionHandler {
    @DexIgnore
    static Capabilities CAPABILITIES; // = new com.navdy.service.library.events.Capabilities.Builder().compactUi(java.lang.Boolean.valueOf(true)).localMusicBrowser(java.lang.Boolean.valueOf(true)).voiceSearch(java.lang.Boolean.valueOf(true)).voiceSearchNewIOSPauseBehaviors(java.lang.Boolean.valueOf(true)).musicArtworkCache(java.lang.Boolean.valueOf(true)).searchResultList(java.lang.Boolean.valueOf(true)).cannedResponseToSms(java.lang.Boolean.valueOf(com.navdy.hud.app.ui.component.UISettings.supportsIosSms())).customDialLongPress(java.lang.Boolean.valueOf(true)).build();
    @DexIgnore
    private static /* final */ ConnectionHandler.DeviceSyncEvent DEVICE_FULL_SYNC_EVENT; // = new com.navdy.hud.app.service.ConnectionHandler.DeviceSyncEvent(1);
    @DexIgnore
    private static /* final */ ConnectionHandler.DeviceSyncEvent DEVICE_MINIMAL_SYNC_EVENT; // = new com.navdy.hud.app.service.ConnectionHandler.DeviceSyncEvent(0);
    @DexIgnore
    public static /* final */ int IOS_APP_LAUNCH_TIMEOUT; // = 5000;
    @DexIgnore
    private static List<LegacyCapability> LEGACY_CAPABILITIES; // = new java.util.ArrayList();
    @DexIgnore
    private static /* final */ int LINK_LOST_TIME_OUT_AFTER_DISCONNECTED; // = 1000;
    @DexIgnore
    private static /* final */ int REDOWNLOAD_THRESHOLD; // = 30000;
    @DexIgnore
    private static /* final */ Logger sLogger; // = new com.navdy.service.library.log.Logger(com.navdy.hud.app.service.ConnectionHandler.class);
    @DexIgnore
    private Bus bus;
    @DexIgnore
    protected Context context;
    @DexIgnore
    protected boolean disconnecting;
    @DexIgnore
    private volatile boolean forceFullUpdate; // = false;
    @DexIgnore
    private volatile boolean isNetworkLinkReady; // = false;
    @DexIgnore
    private long lastConnectTime;
    @DexIgnore
    private String lastConnectedDeviceId;
    @DexIgnore
    protected DeviceInfo lastConnectedDeviceInfo;
    @DexIgnore
    private long lastDisconnectTime;
    @DexIgnore
    private long lastLinkTime;
    @DexIgnore
    private boolean mAppClosed; // = true;
    @DexIgnore
    private boolean mAppLaunchAttempted; // = false;
    @DexIgnore
    private Runnable mAppLaunchAttemptedRunnable;
    @DexIgnore
    protected CustomNotificationServiceHandler mCustomNotificationServiceHandler;
    @DexIgnore
    protected Runnable mDisconnectRunnable;
    @DexIgnore
    protected DriverProfileManager mDriverProfileManager;
    @DexIgnore
    protected Handler mHandler;
    @DexIgnore
    private PowerManager mPowerManager;
    @DexIgnore
    protected Runnable mReconnectTimedOutRunnable;
    @DexIgnore
    protected RemoteDeviceProxy mRemoteDevice;
    @DexIgnore
    protected ConnectionServiceProxy proxy;
    @DexIgnore
    private TimeHelper timeHelper;
    @DexIgnore
    private UIStateManager uiStateManager;

    // @DexIgnore
    // class Anon1 implements Runnable {
    //     @DexIgnore
    //     Anon1() {
    //     }
    //
    //     @DexIgnore
    //     public void run() {
    //         ConnectionHandler.this.handleDisconnectWithoutLinkLoss();
    //     }
    // }
    //
    // @DexIgnore
    // class Anon2 implements Runnable {
    //     @DexIgnore
    //     Anon2() {
    //     }
    //
    //     @DexIgnore
    //     public void run() {
    //         ConnectionHandler.this.sendConnectionNotification();
    //     }
    // }
    //
    // @DexIgnore
    // class Anon3 implements Runnable {
    //     @DexIgnore
    //     Anon3() {
    //     }
    //
    //     @DexIgnore
    //     public void run() {
    //         ConnectionHandler.sLogger.d("No response after the timeout after App launch attempt");
    //         ConnectionHandler.this.sendConnectionNotification();
    //     }
    // }

    @DexIgnore
    public static class ApplicationLaunchAttempted {
    }

    @DexIgnore
    public static class DeviceSyncEvent {
        public static final int FULL = 1;
        public static final int MINIMAL = 0;
        public final int amount;

        @DexIgnore
        public DeviceSyncEvent(int amount2) {
            this.amount = amount2;
        }
    }

    /* @DexIgnore
    static {
        LEGACY_CAPABILITIES.add(com.navdy.service.library.events.LegacyCapability.CAPABILITY_VOICE_SEARCH);
        LEGACY_CAPABILITIES.add(com.navdy.service.library.events.LegacyCapability.CAPABILITY_COMPACT_UI);
        LEGACY_CAPABILITIES.add(com.navdy.service.library.events.LegacyCapability.CAPABILITY_LOCAL_MUSIC_BROWSER);
    } */

    @DexIgnore
    public ConnectionHandler(Context context2, ConnectionServiceProxy proxy2, PowerManager powerManager, DriverProfileManager driverProfileManager, UIStateManager uiStateManager2, TimeHelper timeHelper2) {
        // this.context = context2;
        // this.proxy = proxy2;
        // this.mPowerManager = powerManager;
        // this.bus = this.proxy.getBus();
        // this.bus.register(this);
        // this.mCustomNotificationServiceHandler = new CustomNotificationServiceHandler(this.bus);
        // this.mCustomNotificationServiceHandler.start();
        // this.mDriverProfileManager = driverProfileManager;
        // this.uiStateManager = uiStateManager2;
        // this.timeHelper = timeHelper2;
        // sLogger.i("Creating connectionHandler:" + this);
        // this.mHandler = new Handler(Looper.getMainLooper());
        // this.mDisconnectRunnable = new ConnectionHandler.Anon1();
        // this.mReconnectTimedOutRunnable = new ConnectionHandler.Anon2();
        // this.mAppLaunchAttemptedRunnable = new ConnectionHandler.Anon3();
    }

    @DexIgnore
    public boolean serviceConnected() {
        return this.proxy.connected();
    }

    @DexIgnore
    public void connect() {
        this.proxy.connect();
    }

    @DexIgnore
    public void shutdown() {
        disconnect();
        this.proxy.disconnect();
    }

    @DexIgnore
    public RemoteDevice getRemoteDevice() {
        return this.mRemoteDevice;
    }

    @DexIgnore
    public NavdyDeviceId getConnectedDevice() {
        if (this.mRemoteDevice != null) {
            return this.mRemoteDevice.getDeviceId();
        }
        return null;
    }

    @DexIgnore
    public void connectToDevice(NavdyDeviceId deviceId) {
        sendLocalMessage(new ConnectionRequest(ConnectionRequest.Action.CONNECTION_SELECT, deviceId != null ? deviceId.toString() : null));
    }

    @DexIgnore
    public void searchForDevices() {
        sendLocalMessage(new ConnectionRequest(ConnectionRequest.Action.CONNECTION_START_SEARCH, null));
    }

    @DexIgnore
    public void stopSearch() {
        sendLocalMessage(new ConnectionRequest(ConnectionRequest.Action.CONNECTION_STOP_SEARCH, null));
    }

    @DexIgnore
    public void sendLocalMessage(Message message) {
        try {
            this.proxy.postRemoteEvent(NavdyDeviceId.getThisDevice(this.context), NavdyEventUtil.eventFromMessage(message));
        } catch (Exception e) {
            sLogger.e("Failed to send local event", e);
        }
    }

    @DexIgnore
    public void disconnect() {
        if (this.mRemoteDevice != null) {
            this.lastConnectedDeviceInfo = null;
            this.disconnecting = true;
            connectToDevice(null);
        }
    }

    // @com.squareup.otto.Subscribe
    @DexIgnore
    public void onDisconnect(Disconnect event) {
        disconnect();
    }

    // @com.squareup.otto.Subscribe
    @DexIgnore
    public void onShutdown(Shutdown event) {
        if (event.state == Shutdown.State.SHUTTING_DOWN) {
            shutdown();
        }
    }

    // // @com.squareup.otto.Subscribe
    // @DexIgnore
    // public void onIncrementalOTAFailureDetected(OTAUpdateService.IncrementalOTAFailureDetected event) {
    //     sLogger.d("Incremental OTA failure detected, indicate the remote device to force full update");
    //     this.forceFullUpdate = true;
    // }

    // @com.squareup.otto.Subscribe
    @DexIgnore
    public void onLinkPropertiesChanged(LinkPropertiesChanged linkPropertiesChanged) {
        if (linkPropertiesChanged != null && linkPropertiesChanged.bandwidthLevel != null && this.mRemoteDevice != null) {
            this.mRemoteDevice.setLinkBandwidthLevel(linkPropertiesChanged.bandwidthLevel.intValue());
        }
    }

    // @com.squareup.otto.Subscribe
    @DexIgnore
    public void onConnectionStateChange(ConnectionStateChange event) {
        sLogger.d("ConnectionStateChange - " + event.state);
        String remoteDeviceId = event.remoteDeviceId;
        NavdyDeviceId deviceId = TextUtils.isEmpty(remoteDeviceId) ? NavdyDeviceId.UNKNOWN_ID : new NavdyDeviceId(remoteDeviceId);
        this.mAppLaunchAttempted = false;
        this.mHandler.removeCallbacks(this.mAppLaunchAttemptedRunnable);
        switch (event.state) {
            case CONNECTION_LINK_ESTABLISHED:
                this.mAppClosed = true;
                onLinkEstablished(deviceId);
                return;
            case CONNECTION_CONNECTED:
                onConnect(deviceId);
                this.mAppClosed = false;
                return;
            case CONNECTION_DISCONNECTED:
                this.lastDisconnectTime = SystemClock.elapsedRealtime();
                this.mAppClosed = true;
                onDisconnect(deviceId);
                return;
            case CONNECTION_LINK_LOST:
                this.lastDisconnectTime = SystemClock.elapsedRealtime();
                this.mAppClosed = true;
                onLinkLost(deviceId);
                return;
            default:
                return;
        }
    }

    // @com.squareup.otto.Subscribe
    @DexIgnore
    public void onApplicationLaunchAttempted(ConnectionHandler.ApplicationLaunchAttempted applicationLaunchAttempted) {
        sLogger.d("Attempting to launch the App, Will wait and see");
        if (this.mAppClosed) {
            this.mAppLaunchAttempted = true;
            this.mHandler.removeCallbacks(this.mAppLaunchAttemptedRunnable);
            this.mHandler.postDelayed(this.mAppLaunchAttemptedRunnable, 5000);
        }
    }

    @DexIgnore
    private void onLinkEstablished(NavdyDeviceId connectingDevice) {
        // this.lastLinkTime = SystemClock.elapsedRealtime();
        // this.mPowerManager.wakeUp(AnalyticsSupport.WakeupReason.PHONE);
        // if (this.mRemoteDevice != null && com.navdy.hud.app.BuildConfig.DEBUG) {
        //     sLogger.w(String.format("Seeing a connect for %s while connected to %s!", connectingDevice.getDisplayName(), this.mRemoteDevice.getDeviceId().getDisplayName()));
        // }
        // this.mRemoteDevice = new RemoteDeviceProxy(this.proxy, this.context, connectingDevice);
        // this.mHandler.removeCallbacks(this.mReconnectTimedOutRunnable);
    }

    @DexReplace
    private void onConnect(NavdyDeviceId navdyDeviceId) {
        this.lastConnectTime = SystemClock.elapsedRealtime();
        if (this.mRemoteDevice == null) {
            this.mRemoteDevice = new RemoteDeviceProxy(this.proxy, this.context, navdyDeviceId);
        }
        this.mDriverProfileManager.loadProfileForId(navdyDeviceId);
        NavdyDeviceId thisDevice = NavdyDeviceId.getThisDevice(this.context);
        DeviceInfo build = new DeviceInfo.Builder().deviceId(thisDevice.toString()).clientVersion(BuildConfig.VERSION_NAME).protocolVersion(Version.PROTOCOL_VERSION.toString()).deviceName(thisDevice.getDeviceName()).systemVersion(getSystemVersion()).model(Build.MODEL).deviceUuid(Build.SERIAL).systemApiLevel(Build.VERSION.SDK_INT).kernelVersion(System.getProperty("os.version")).platform(DeviceInfo.Platform.PLATFORM_Android).buildType(Build.TYPE).deviceMake("Navdy").forceFullUpdate(this.forceFullUpdate).legacyCapabilities(LEGACY_CAPABILITIES).capabilities(CAPABILITIES).build();
        Logger logger = sLogger;
        logger.d("onConnect - sending device info: " + build);
        this.mRemoteDevice.postEvent(build);
    }

    // @com.squareup.otto.Subscribe
    @DexIgnore
    public void onNetworkLinkReady(NetworkLinkReady networkLinkReady) {
        sLogger.v("DummyNetNetworkFactory:onConnect");
        this.isNetworkLinkReady = true;
        NetworkStateManager.getInstance().networkAvailable();
        GestureVideosSyncService.scheduleWithDelay(30000);
        ObdCANBusDataUploadService.scheduleWithDelay(45000);
    }

    @DexReplace
    private String getSystemVersion() {
        return !String.valueOf(BuildConfig.VERSION_CODE).equals(Build.VERSION.INCREMENTAL) ? BuildConfig.VERSION_NAME : Build.VERSION.INCREMENTAL;
    }

    @DexIgnore
    private synchronized void onDisconnect(NavdyDeviceId navdyDeviceId) {
        sLogger.v("onDisconnect:" + navdyDeviceId);
        this.isNetworkLinkReady = false;
        NetworkStateManager.getInstance().networkNotAvailable();
        this.mDriverProfileManager.setCurrentProfile(null);
        this.mHandler.postDelayed(this.mDisconnectRunnable, 1000);
    }

    @DexIgnore
    private synchronized void onLinkLost(NavdyDeviceId deviceId) {
        boolean wasConnected = true;
        synchronized (this) {
            sLogger.v("onLinkLost:" + deviceId);
            this.isNetworkLinkReady = false;
            this.mHandler.removeCallbacks(this.mDisconnectRunnable);
            this.mHandler.removeCallbacks(this.mReconnectTimedOutRunnable);
            if (this.mRemoteDevice == null) {
                wasConnected = false;
            }
            this.mRemoteDevice = null;
            PicassoUtil.clearCache();
            if (wasConnected) {
                NotificationManager.getInstance().handleDisconnect(true);
                if (!this.disconnecting) {
                    this.mHandler.postDelayed(this.mReconnectTimedOutRunnable, StaticConfig.PERMISSION_CHECK_INTERVAL);
                }
            }
            this.disconnecting = false;
        }
    }

    @DexIgnore
    private void handleDisconnectWithoutLinkLoss() {
        DeviceInfo.Platform remoteDevicePlatform = RemoteDeviceManager.getInstance().getRemoteDevicePlatform();
        if (remoteDevicePlatform != null && remoteDevicePlatform.equals(DeviceInfo.Platform.PLATFORM_iOS)) {
            sendConnectionNotification();
        }
    }

    // @com.squareup.otto.Subscribe
    @DexIgnore
    public void onDeviceInfo(DeviceInfo deviceInfo) {
        // sLogger.i("Received deviceInfo:" + deviceInfo);
        // if (this.mRemoteDevice != null) {
        //     boolean triggerDownload = true;
        //     if (this.lastConnectedDeviceId != null) {
        //         long time = SystemClock.elapsedRealtime() - this.lastDisconnectTime;
        //         if (time < 30000) {
        //             sLogger.v("threshold met=" + time);
        //             if (TextUtils.equals(this.lastConnectedDeviceId, deviceInfo.deviceId)) {
        //                 triggerDownload = false;
        //                 sLogger.v("same device connected:" + deviceInfo.deviceId);
        //             } else {
        //                 sLogger.v("different device connected last[" + this.lastConnectedDeviceId + "] current[" + deviceInfo.deviceId + "]");
        //                 this.lastConnectedDeviceId = deviceInfo.deviceId;
        //             }
        //         } else {
        //             sLogger.v("threshold not met=" + time);
        //             this.lastConnectedDeviceId = deviceInfo.deviceId;
        //         }
        //     } else {
        //         sLogger.v("no last device");
        //         this.lastConnectedDeviceId = deviceInfo.deviceId;
        //     }
        //     String currentDeviceId = this.mRemoteDevice.getDeviceId().toString();
        //     DeviceInfo deviceInfo2 = new DeviceInfo.Builder(deviceInfo).deviceId(currentDeviceId).build();
        //     boolean newDevice = !currentDeviceId.equals(this.lastConnectedDeviceInfo != null ? this.lastConnectedDeviceInfo.deviceId : null);
        //     this.lastConnectedDeviceInfo = deviceInfo2;
        //     this.mRemoteDevice.setDeviceInfo(deviceInfo2);
        //     AnalyticsSupport.recordDeviceConnect(deviceInfo2, this.lastConnectTime, this.lastLinkTime);
        //     this.bus.post(new DeviceInfoAvailable(deviceInfo2));
        //     if (triggerDownload) {
        //         sLogger.v("trigger device sync");
        //         this.bus.post(DEVICE_FULL_SYNC_EVENT);
        //     } else if (deviceInfo2.platform == DeviceInfo.Platform.PLATFORM_iOS) {
        //         sLogger.v("triggering minimal sync");
        //         this.bus.post(DEVICE_MINIMAL_SYNC_EVENT);
        //     } else {
        //         sLogger.v("don't trigger device sync");
        //     }
        //     if (newDevice) {
        //         sendConnectionNotification();
        //         return;
        //     }
        //     NotificationManager.getInstance().handleConnect();
        //     NotificationManager.getInstance().hideConnectionToast(true);
        //     long elapsedTime = SystemClock.elapsedRealtime() - this.lastDisconnectTime;
        //     sLogger.i("Tracking a reconnect that took " + elapsedTime + "ms");
        //     AnalyticsSupport.recordPhoneDisconnect(true, String.valueOf(elapsedTime));
        // }
    }

    // @com.squareup.otto.Subscribe
    @DexIgnore
    public void onRemoteEvent(RemoteEvent event) {
        if (this.mRemoteDevice != null) {
            this.mRemoteDevice.postEvent(event.getMessage());
        }
    }

    // @com.squareup.otto.Subscribe
    @DexIgnore
    public void onLocalSpeechRequest(LocalSpeechRequest event) {
        if (!CallUtils.isPhoneCallInProgress()) {
            if (sLogger.isLoggable(2)) {
                sLogger.v("[tts-outbound] [" + event.speechRequest.category.name() + "] [" + event.speechRequest.words + "]");
            }
            onRemoteEvent(new RemoteEvent(event.speechRequest));
        }
    }

    // @com.squareup.otto.Subscribe
    @DexIgnore
    public void onDateTimeConfiguration(DateTimeConfiguration configurationEvent) {
        // sLogger.i("Received timestamp from the client: " + configurationEvent.timestamp + " " + configurationEvent.timezone);
        // try {
        //     if (DeviceUtil.isNavdyDevice()) {
        //         AlarmManager am = (AlarmManager) this.context.getSystemService("alarm");
        //         sLogger.v("setting date timezone[" + configurationEvent.timezone + "] time[" + configurationEvent.timestamp + "]");
        //         if (configurationEvent.timezone == null) {
        //             sLogger.w("timezone not provided");
        //         } else if (TimeZone.getDefault().getID().equalsIgnoreCase(configurationEvent.timezone)) {
        //             sLogger.v("timezone already set to " + configurationEvent.timezone);
        //         } else {
        //             TimeZone timeZone = TimeZone.getTimeZone(configurationEvent.timezone);
        //             if (!timeZone.getID().equalsIgnoreCase(configurationEvent.timezone)) {
        //                 sLogger.e("timezone not found on HUD:" + configurationEvent.timezone);
        //             } else {
        //                 sLogger.v("timezone found on HUD:" + timeZone.getID());
        //                 am.setTimeZone(configurationEvent.timezone);
        //             }
        //         }
        //         sLogger.v("setting time [" + configurationEvent.timestamp + "]");
        //         am.setTime(configurationEvent.timestamp.longValue());
        //         sLogger.v("set time [" + configurationEvent.timestamp + "]");
        //         this.bus.post(TimeHelper.DATE_TIME_AVAILABLE_EVENT);
        //         sLogger.v("post date complete");
        //     }
        //     DriverProfileManager driverProfileManager = DriverProfileHelper.getInstance().getDriverProfileManager();
        //     driverProfileManager.getSessionPreferences().setClockConfiguration(configurationEvent);
        //     driverProfileManager.updateLocalPreferences(new LocalPreferences.Builder(driverProfileManager.getCurrentProfile().getLocalPreferences()).clockFormat(configurationEvent.format).build());
        // } catch (Throwable t) {
        //     sLogger.e("Cannot update time on device", t);
        // }
    }

    // @com.squareup.otto.Subscribe
    @DexIgnore
    public void onDriverProfileUpdated(DriverProfileChanged profileChanged) {
        DriverProfile profile = this.mDriverProfileManager.getCurrentProfile();
        requestIAPUpdateBasedOnPreference(profile != null ? profile.getNotificationPreferences() : null);
    }

    // @com.squareup.otto.Subscribe
    @DexIgnore
    public void onNotificationPreference(NotificationPreferences preferences) {
        requestIAPUpdateBasedOnPreference(preferences);
    }

    // @com.squareup.otto.Subscribe
    @DexIgnore
    public void onWakeup(Wakeup event) {
        if (DialManager.getInstance().getBondedDialCount() == 0) {
            sLogger.v("go to dial pairing screen");
            this.bus.post(new ShowScreen.Builder().screen(Screen.SCREEN_DIAL_PAIRING).build());
        } else if (this.mRemoteDevice == null) {
            sLogger.v("go to welcome screen");
            Bundle args = new Bundle();
            args.putString(WelcomeScreen.ARG_ACTION, WelcomeScreen.ACTION_RECONNECT);
            this.bus.post(new ShowScreenWithArgs(Screen.SCREEN_WELCOME, args, false));
        } else {
            this.bus.post(new ShowScreen.Builder().screen(this.uiStateManager.getDefaultMainActiveScreen()).build());
        }
    }

    @DexIgnore
    private void requestIAPUpdateBasedOnPreference(NotificationPreferences preferences) {
        if (preferences == null) {
            return;
        }
        if (Boolean.TRUE.equals(preferences.enabled)) {
            this.bus.post(new RemoteEvent(new CallStateUpdateRequest(Boolean.TRUE)));
            this.bus.post(new RemoteEvent(new NowPlayingUpdateRequest(Boolean.TRUE)));
            return;
        }
        this.bus.post(new RemoteEvent(new CallStateUpdateRequest(Boolean.FALSE)));
        this.bus.post(new RemoteEvent(new NowPlayingUpdateRequest(Boolean.FALSE)));
    }

    @DexIgnore
    public void sendConnectionNotification() {
        NotificationManager notificationManager = NotificationManager.getInstance();
        if (!RemoteDeviceManager.getInstance().isRemoteDeviceConnected()) {
            notificationManager.handleDisconnect(true);
            notificationManager.showHideToast(false);
        } else if (isAppClosed()) {
            notificationManager.handleDisconnect(false);
            notificationManager.showHideToast(false);
        } else {
            notificationManager.handleConnect();
            notificationManager.showHideToast(true);
        }
    }

    @DexIgnore
    public DeviceInfo getLastConnectedDeviceInfo() {
        return this.lastConnectedDeviceInfo;
    }

    @DexIgnore
    public boolean isAppClosed() {
        return this.mAppClosed;
    }

    @DexIgnore
    public boolean isNetworkLinkReady() {
        return this.isNetworkLinkReady;
    }

    @DexIgnore
    public StartDriveRecordingEvent getDriverRecordingEvent() {
        if (this.proxy != null) {
            return this.proxy.getDriverRecordingEvent();
        }
        return null;
    }

    @DexIgnore
    public boolean isAppLaunchAttempted() {
        return this.mAppLaunchAttempted;
    }
}
