package com.navdy.hud.app;

// @kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u001a\n\u0000\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0006\n\u0002\b\u0003\n\u0002\u0010\u0007\n\u0000\u001a\n\u0010\u0000\u001a\u00020\u0001*\u00020\u0002\u001a\n\u0010\u0003\u001a\u00020\u0004*\u00020\u0004\u001a\u001a\u0010\u0005\u001a\u00020\u0004*\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\u0004\u001a\u001a\u0010\u0005\u001a\u00020\b*\u00020\b2\u0006\u0010\u0006\u001a\u00020\b2\u0006\u0010\u0007\u001a\u00020\b\u00a8\u0006\t"}, d2 = {"cacheKey", "", "Lcom/navdy/service/library/events/audio/MusicCollectionRequest;", "celsiusToFahrenheit", "", "clamp", "min", "max", "", "app_hudDebug"}, k = 2, mv = {1, 1, 6})
/* compiled from: Extensions.kt */
import com.navdy.service.library.events.MessageStore;
import com.navdy.service.library.events.audio.MusicCollectionRequest;

import org.jetbrains.annotations.NotNull;

import kotlin.jvm.internal.Intrinsics;
import lanchon.dexpatcher.annotation.DexAdd;
import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;

@DexEdit
public final class ExtensionsKt {
    @DexIgnore
    public static final float clamp(float $receiver, float min, float max) {
        if ($receiver < min) {
            return min;
        }
        if ($receiver > max) {
            return max;
        }
        return $receiver;
    }

    @DexIgnore
    public static final double clamp(double $receiver, double min, double max) {
        if ($receiver < min) {
            return min;
        }
        if ($receiver > max) {
            return max;
        }
        return $receiver;
    }

    @DexIgnore
    public static final double celsiusToFahrenheit(double $receiver) {
        return ((((double) 9) * $receiver) / ((double) 5)) + ((double) 32);
    }

    @DexAdd
    public static final double kPaToPsi(final double n) {
        return n * 0.145038;
    }

    // @org.jetbrains.annotations.NotNull
    @DexIgnore
    public static final String cacheKey(@NotNull MusicCollectionRequest $receiver) {
        Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        MusicCollectionRequest defaults = MessageStore.removeNulls($receiver);
        return defaults.collectionSource + "#" + defaults.collectionType + "#" + defaults.collectionId + "#" + defaults.offset;
    }
}
