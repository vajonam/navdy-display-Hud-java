package com.navdy.hud.app.util;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import alelec.navdy.hud.app.BuildConfig;

import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit
public class OTAUpdateService extends Service
{
    @DexReplace
    public static String getCurrentVersion() {
        return shortVersion(BuildConfig.VERSION_NAME);
    }

    @DexIgnore
    public static String shortVersion(String s) {
        throw null;
    }

    @DexIgnore
    public IBinder onBind(Intent intent) {
        return null;
    }
}

