package com.navdy.hud.app.util;

import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;

import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.framework.DriverProfileHelper;
import com.navdy.hud.app.framework.glance.GlanceConstants;
import com.navdy.hud.app.framework.network.NetworkBandwidthController;
import com.navdy.hud.app.framework.network.NetworkBandwidthController.Component;
import com.navdy.hud.app.profile.DriverProfileManager;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.util.IOUtils;

import net.hockeyapp.android.CrashManager;
import net.hockeyapp.android.CrashManagerListener;
import net.hockeyapp.android.ExceptionHandler;
import net.hockeyapp.android.metrics.MetricsManager;

import java.io.File;
import java.lang.ref.WeakReference;
import java.lang.reflect.Field;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import lanchon.dexpatcher.annotation.DexAdd;
import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit
public final class CrashReporter {
    @DexIgnore
    private static /* final */ int ANR_LOG_LIMIT; // = 32768;
    @DexIgnore
    private static /* final */ String ANR_PATH; // = "/data/anr/traces.txt";
    @DexIgnore
    public static /* final */ String HOCKEY_APP_ID; // = "20625a4f769df641ecf46825a71c1911";
    @DexIgnore
    public static /* final */ String HOCKEY_APP_STABLE_ID; // = "4dd8d996ee304ef2994965aa7f6d4f15";
    @DexIgnore
    public static /* final */ String HOCKEY_APP_UNSTABLE_ID; // = "ad4b9dbcbb3b4083a49af48878026e5a";
    @DexIgnore
    private static /* final */ int KERNEL_CRASH_INFO_LIMIT; // = 32768;
    @DexIgnore
    private static /* final */ String LOG_ANR_MARKER; // = "Wrote stack traces to '/data/anr/traces.txt'";
    @DexIgnore
    private static /* final */ String LOG_BEGIN_MARKER; // = "--------- beginning of main";
    @DexIgnore
    private static /* final */ int PERIOD_DELIVERY_CHECK_INITIAL_INTERVAL; // = ((int) java.util.concurrent.TimeUnit.MINUTES.toMillis(2));
    @DexIgnore
    private static /* final */ int PERIOD_DELIVERY_CHECK_INTERVAL; // = ((int) java.util.concurrent.TimeUnit.MINUTES.toMillis(5));
    @DexIgnore
    private static /* final */ Pattern START_OF_OOPS; // = java.util.regex.Pattern.compile("(Unable to handle)|(Internal error:)");
    @DexIgnore
    private static /* final */ int SYSTEM_LOG_LIMIT; // = 32768;
    @DexIgnore
    private static /* final */ String TOMBSTONE_FILE_PATTERN; // = "tombstone_light_";
    @DexIgnore
    private static /* final */ String TOMBSTONE_PATH; // = "/data/tombstones";
    @DexIgnore
    public static /* final */ Pattern USER_REBOOT; // = java.util.regex.Pattern.compile("(reboot: Restarting system with command)|(do_powerctl:)");
    @DexIgnore
    private static DriverProfileManager driverProfileManager;
    @DexIgnore
    private static /* final */ CrashReporter sInstance; // = new com.navdy.hud.app.util.CrashReporter();
    @DexIgnore
    private static /* final */ Logger sLogger; // = new com.navdy.service.library.log.Logger(com.navdy.hud.app.util.CrashReporter.class);
    @DexIgnore
    private static volatile boolean stopCrashReporting;
    @DexIgnore
    private static String userId; // = android.os.Build.SERIAL;
    @DexIgnore
    private NavdyCrashListener crashManagerListener; // = new com.navdy.hud.app.util.CrashReporter.NavdyCrashListener();
    @DexIgnore
    private Runnable deliveryRunnable; // = new com.navdy.hud.app.util.CrashReporter.Anon2();
    @DexIgnore
    private Handler handler; // = new android.os.Handler(android.os.Looper.getMainLooper());
    @DexIgnore
    private boolean installed;
    // @DexIgnore
    // private OTAUpdateService.OTAFailedException otaFailure; // = null;
    @DexIgnore
    private Runnable periodicRunnable; // = new com.navdy.hud.app.util.CrashReporter.Anon1();

    // @DexIgnore
    // class Anon1 implements Runnable {
    //     @DexIgnore
    //     Anon1() {
    //     }
    //
    //     @DexIgnore
    //     public void run() {
    //         TaskManager.getInstance().execute(CrashReporter.this.deliveryRunnable, 1);
    //     }
    // }
    //
    // @DexIgnore
    // class Anon2 implements Runnable {
    //     @DexIgnore
    //     Anon2() {
    //     }
    //
    //     @DexIgnore
    //     public void run() {
    //         boolean checkAgain = true;
    //         try {
    //             if (CrashReporter.this.checkForCrashes()) {
    //                 CrashReporter.this.handler.postDelayed(CrashReporter.this.periodicRunnable, (long) CrashReporter.PERIOD_DELIVERY_CHECK_INTERVAL);
    //             }
    //         } catch (Throwable th) {
    //             if (checkAgain) {
    //                 CrashReporter.this.handler.postDelayed(CrashReporter.this.periodicRunnable, (long) CrashReporter.PERIOD_DELIVERY_CHECK_INTERVAL);
    //             }
    //             // throw th;
    //         }
    //     }
    // }

    @DexAdd
    class UncaughtExceptionHandler implements Thread.UncaughtExceptionHandler {
        final /* synthetic */ CrashReporter val$this;
        final /* synthetic */ Thread.UncaughtExceptionHandler val$hockeyUncaughtHandler;

        UncaughtExceptionHandler(CrashReporter _this, Thread.UncaughtExceptionHandler uncaughtExceptionHandler) {
            this.val$this = _this;
            this.val$hockeyUncaughtHandler = uncaughtExceptionHandler;
        }

        public void uncaughtException(Thread thread, Throwable exception) {
            this.val$this.handleUncaughtException(thread, exception, this.val$hockeyUncaughtHandler);
        }
    }

    // @DexIgnore
    // class Anon4 implements Runnable {
    //     @DexIgnore
    //     final /* synthetic */ String[] val$kernelCrashFiles;
    //
    //     @DexIgnore
    //     Anon4(String[] strArr) {
    //         this.val$kernelCrashFiles = strArr;
    //     }
    //
    //     @DexIgnore
    //     public void run() {
    //         try {
    //             CrashReporter.sLogger.v("checking for kernel crashes");
    //             File file = null;
    //             String[] strArr = this.val$kernelCrashFiles;
    //             int length = strArr.length;
    //             int i = 0;
    //             while (true) {
    //                 if (i >= length) {
    //                     break;
    //                 }
    //                 File f = new File(strArr[i]);
    //                 if (f.exists()) {
    //                     file = f;
    //                     CrashReporter.sLogger.v("found file:" + f.getAbsolutePath());
    //                     break;
    //                 }
    //                 i++;
    //             }
    //             if (file != null) {
    //                 String str = CrashReporter.this.filterKernelCrash(file);
    //                 if (!TextUtils.isEmpty(str)) {
    //                     CrashReporter.this.reportNonFatalException(new KernelCrashException(str));
    //                     CrashReporter.sLogger.v("kernel crash created");
    //                 }
    //                 CrashReporter.this.cleanupKernelCrashes(this.val$kernelCrashFiles);
    //                 return;
    //             }
    //             CrashReporter.sLogger.v("no kernel crash files");
    //         } catch (Throwable t) {
    //             CrashReporter.sLogger.e(t);
    //         }
    //     }
    // }
    //
    // @DexIgnore
    // class Anon5 implements FilenameFilter {
    //     @DexIgnore
    //     Anon5() {
    //     }
    //
    //     @DexIgnore
    //     public boolean accept(File dir, String filename) {
    //         if (filename.contains(CrashReporter.TOMBSTONE_FILE_PATTERN)) {
    //             return true;
    //         }
    //         return false;
    //     }
    // }
    //
    // @DexIgnore
    // class Anon6 implements Runnable {
    //     @DexIgnore
    //     final /* synthetic */ Throwable val$throwable;
    //
    //     @DexIgnore
    //     Anon6(Throwable th) {
    //         this.val$throwable = th;
    //     }
    //
    //     @DexIgnore
    //     public void run() {
    //         try {
    //             CrashReporter.this.crashManagerListener.saveException(this.val$throwable);
    //         } catch (Throwable t) {
    //             CrashReporter.sLogger.e(t);
    //         }
    //     }
    // }

    @DexIgnore
    public static class AnrException extends Exception {
        @DexIgnore
        public AnrException(String str) {
            super(str);
        }
    }

    @DexIgnore
    public static class CrashException extends Exception {
        @DexIgnore
        public CrashException(String detailMessage) {
            super(detailMessage);
        }

        @DexIgnore
        public Pattern getLocationPattern() {
            return null;
        }

        @DexIgnore
        public String filter(String msg) {
            return msg;
        }

        @DexIgnore
        public String getCrashLocation() {
            return extractCrashLocation(getLocalizedMessage());
        }

        @DexIgnore
        public String toString() {
            String name = getClass().getSimpleName();
            String msg = getLocalizedMessage();
            return name + ": " + getCrashLocation() + (msg != null ? filter(msg) : "");
        }

        /* access modifiers changed from: 0000 */
        @DexIgnore
        public String extractCrashLocation(String msg) {
            String crashLocation = "";
            Pattern pattern = getLocationPattern();
            if (pattern == null || msg == null) {
                return crashLocation;
            }
            Matcher match = pattern.matcher(msg);
            if (!match.find()) {
                return crashLocation;
            }
            if (match.groupCount() < 1) {
                return match.group();
            }
            for (int i = 1; i <= match.groupCount(); i++) {
                if (match.group(i) != null) {
                    return match.group(i);
                }
            }
            return crashLocation;
        }
    }

    @DexIgnore
    public static class KernelCrashException extends CrashException {
        @DexIgnore
        private static final Pattern stackMatcher = Pattern.compile("PC is at\\s+([^\n]+\n)");

        @DexIgnore
        public KernelCrashException(String str) {
            super(str);
        }

        @DexIgnore
        public Pattern getLocationPattern() {
            return stackMatcher;
        }
    }

    @DexIgnore
    public static class NavdyCrashListener extends CrashManagerListener {
        @DexIgnore
        public Throwable exception = null;

        @DexIgnore
        public boolean shouldAutoUploadCrashes() {
            return true;
        }

        @DexIgnore
        public String getUserID() {
            throw null;
            // return CrashReporter.userId;
        }

        @DexIgnore
        public String getContact() {
            throw null;
            // return CrashReporter.driverProfileManager.getLastUserEmail();
        }

        @DexIgnore
        public String getDescription() {
            throw null;
            // if (this.exception instanceof TombstoneException) {
            //     return ((TombstoneException) this.exception).description;
            // }
            // if (this.exception instanceof AnrException) {
            //     return LogUtils.systemLogStr(32768, CrashReporter.LOG_ANR_MARKER);
            // }
            // if (this.exception instanceof KernelCrashException) {
            //     return LogUtils.systemLogStr(32768, CrashReporter.LOG_BEGIN_MARKER);
            // }
            // if (!(this.exception instanceof OTAUpdateService.OTAFailedException)) {
            //     return LogUtils.systemLogStr(32768, null);
            // }
            // OTAUpdateService.OTAFailedException e = (OTAUpdateService.OTAFailedException) this.exception;
            // return e.last_install + GlanceConstants.NEWLINE + e.last_log;
        }

        @DexIgnore
        public void saveException(Throwable t) {
            // this.exception = t;
            // ExceptionHandler.saveException(t, this);
            // this.exception = null;
        }
    }

    @DexIgnore
    public static class TombstoneException extends CrashException {
        @DexIgnore
        private static /* final */ Pattern stackMatcher; // = Pattern.compile("Abort message: ('[^']+'\n)|#00 pc \\w+\\s+([^\n]+\n)");
        @DexIgnore
        public final String description;

        @DexIgnore
        public TombstoneException(String str) {
            super("");
            this.description = str;
        }

        @DexIgnore
        public String getCrashLocation() {
            throw null;
            // return extractCrashLocation(this.description);
        }

        @DexIgnore
        public Pattern getLocationPattern() {
            return stackMatcher;
        }
    }

    @DexIgnore
    public static boolean isEnabled() {
        return DeviceUtil.isNavdyDevice() && !HudApplication.isDeveloperBuild();
    }

    @DexIgnore
    public static CrashReporter getInstance() {
        return sInstance;
    }

    @DexIgnore
    private CrashReporter() {
        this.handler.postDelayed(this.periodicRunnable, (long) PERIOD_DELIVERY_CHECK_INITIAL_INTERVAL);
        driverProfileManager = DriverProfileHelper.getInstance().getDriverProfileManager();
    }

    @DexReplace
    public synchronized void installCrashHandler(Context context) {
        if (!this.installed) {
            if (!DeviceUtil.isUserBuild()) {
                userId = Build.SERIAL + "-eng";
            }
            Thread.UncaughtExceptionHandler defaultUncaughtHandler = Thread.getDefaultUncaughtExceptionHandler();
            if (defaultUncaughtHandler != null) {
                sLogger.v("default uncaught handler:" + defaultUncaughtHandler.getClass().getName());
            } else {
                sLogger.v("default uncaught handler is null");
            }
            setHockeyAppCrashUploading(false);
            // String appId = DeviceUtil.isUserBuild() ? HOCKEY_APP_ID : BuildConfig.DEBUG ? HOCKEY_APP_UNSTABLE_ID : HOCKEY_APP_STABLE_ID;
            String appId = "94c05da80f9f4219a02cad7b7604b330";
            CrashManager.register(context, appId, this.crashManagerListener);
            MetricsManager.register(context, HudApplication.getApplication(), appId);
            sLogger.v("enabled metrics manager");
            setHockeyAppCrashUploading(true);
            Thread.UncaughtExceptionHandler hockeyUncaughtHandler = Thread.getDefaultUncaughtExceptionHandler();
            if (hockeyUncaughtHandler != null) {
                sLogger.v("hockey uncaught handler:" + hockeyUncaughtHandler.getClass().getName());
            } else {
                sLogger.v("default hockey handler is null");
            }
            Thread.setDefaultUncaughtExceptionHandler(new UncaughtExceptionHandler(this, hockeyUncaughtHandler));
            this.installed = true;
        }
    }

    @DexIgnore
    public void reportNonFatalException(Throwable throwable) {
        if (this.installed) {
            try {
                saveHockeyAppException(throwable);
            } catch (Throwable t) {
                sLogger.e(t);
            }
        }
    }

    @DexIgnore
    public void log(String msg) {
    }

    @DexEdit
    void handleUncaughtException(Thread thread, Throwable exception, Thread.UncaughtExceptionHandler exceptionHandler) {
        if (stopCrashReporting) {
            sLogger.i("FATAL-reporting-turned-off", exception);
            return;
        }
        stopCrashReporting = true;
        String tag = "FATAL-CRASH";
        Log.e(tag, tag + " Uncaught exception - " + thread.getName() + GlanceConstants.COLON_SEPARATOR + thread.getId() + " ui thread id:" + Looper.getMainLooper().getThread().getId(), exception);
        sLogger.i("closing logger");
        Logger.close();
        if (exceptionHandler != null) {
            exceptionHandler.uncaughtException(thread, exception);
        }
    }

    @DexIgnore
    public void checkForKernelCrashes(String[] kernelCrashFiles) {
        // if (kernelCrashFiles != null && kernelCrashFiles.length != 0) {
        //     TaskManager.getInstance().execute(new Anon4(kernelCrashFiles), 1);
        // }
    }

    @DexIgnore
    private String filterKernelCrash(File file) {
        try {
            String str = IOUtils.convertFileToString(file.getAbsolutePath());
            if (str == null) {
                return str;
            }
            if (!USER_REBOOT.matcher(str).find()) {
                Matcher match = START_OF_OOPS.matcher(str);
                int start = str.length() - 32768;
                if (match.find() && match.start() < start) {
                    start = match.start();
                }
                int start2 = Math.max(0, start);
                return str.substring(start2, Math.min(str.length() - start2, 32768));
            }
            sLogger.v("Ignoring user reboot log");
            return null;
        } catch (Throwable t) {
            sLogger.e(t);
            return null;
        }
    }

    @DexIgnore
    private void cleanupKernelCrashes(String[] kernelCrashFiles) {
        for (String crashFile : kernelCrashFiles) {
            File f = new File(crashFile);
            if (f.exists()) {
                sLogger.v("Trying to delete " + crashFile);
                if (!f.delete()) {
                    sLogger.e("Unable to delete kernel crash file: " + crashFile);
                }
            }
        }
    }

    @DexIgnore
    public void checkForTombstones() {
        // GenericUtil.checkNotOnMainThread();
        // try {
        //     sLogger.v("checking for tombstones");
        //     File f = new File(TOMBSTONE_PATH);
        //     if (!f.exists() || !f.isDirectory()) {
        //         sLogger.v("no tombstones");
        //         return;
        //     }
        //     String[] list = f.list(new Anon5());
        //     if (list == null || list.length == 0) {
        //         sLogger.v("no tombstones");
        //         return;
        //     }
        //     for (String s : list) {
        //         File tombstoneFile = new File(TOMBSTONE_PATH + File.separator + s);
        //         if (tombstoneFile.exists()) {
        //             String fileName = tombstoneFile.getAbsolutePath();
        //             String str = IOUtils.convertFileToString(fileName);
        //             if (!TextUtils.isEmpty(str)) {
        //                 reportNonFatalException(new TombstoneException(str));
        //                 sLogger.v("tombstone exception logged file[" + fileName + "] size[" + str.length() + "]");
        //             }
        //             sLogger.v("tombstone deleted: " + tombstoneFile.delete());
        //         }
        //     }
        // } catch (Throwable t) {
        //     sLogger.e(t);
        // }
    }

    @DexIgnore
    public void stopCrashReporting(boolean b) {
        setCrashReporting(b);
    }

    @DexIgnore
    private static void setCrashReporting(boolean b) {
        stopCrashReporting = b;
    }

    @DexIgnore
    private boolean checkForCrashes() throws Throwable {
        if (NetworkBandwidthController.getInstance().isNetworkAccessAllowed(Component.HOCKEY)) {
            try {
                CrashManager.submitStackTraces(new WeakReference(HudApplication.getAppContext()), this.crashManagerListener);
            } catch (Throwable t) {
                sLogger.e(t);
            }
        }
        return true;
    }

    @DexIgnore
    public void checkForAnr() {
        GenericUtil.checkNotOnMainThread();
        try {
            sLogger.v("checking for anr's");
            if (new File(ANR_PATH).exists()) {
                String str = IOUtils.convertFileToString(ANR_PATH);
                if (!TextUtils.isEmpty(str)) {
                    reportNonFatalException(new AnrException(str));
                    sLogger.v("anr crash created size[" + str.length() + "]");
                }
                cleanupAnrFiles();
                return;
            }
            sLogger.v("no anr files");
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }

    // @DexIgnore
    // public synchronized void reportOTAFailure(OTAUpdateService.OTAFailedException failure) {
    //     if (this.installed) {
    //         reportNonFatalException(failure);
    //     } else {
    //         if (this.otaFailure != null) {
    //             sLogger.e("multiple OTAFailedException reports");
    //         }
    //         this.otaFailure = failure;
    //     }
    // }

    @DexIgnore
    public synchronized void checkForOTAFailure() {
        // if (this.otaFailure != null) {
        //     reportNonFatalException(this.otaFailure);
        //     this.otaFailure = null;
        // }
    }

    @DexIgnore
    private void cleanupAnrFiles() {
        File f = new File(ANR_PATH);
        if (f.exists()) {
            sLogger.v("delete /data/anr/traces.txt :" + f.delete());
        }
    }

    @DexIgnore
    private void saveHockeyAppException(Throwable throwable) {
        // try {
        //     TaskManager.getInstance().execute(new Anon6(throwable), 1);
        // } catch (Throwable t) {
        //     sLogger.e(t);
        // }
    }

    @DexIgnore
    public boolean isInstalled() {
        return this.installed;
    }

    @DexIgnore
    private void setHockeyAppCrashUploading(boolean enable) {
        boolean z = true;
        try {
            sLogger.v("setHockeyAppCrashUploading: try to enabled:" + enable);
            Field f = CrashManager.class.getDeclaredField("submitting");
            f.setAccessible(true);
            if (enable) {
                z = false;
            }
            f.set(null, z);
            sLogger.v("setHockeyAppCrashUploading: enabled=" + enable);
        } catch (Throwable t) {
            sLogger.e("setHockeyAppCrashUploading", t);
        }
    }
}
