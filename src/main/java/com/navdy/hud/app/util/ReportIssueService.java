package com.navdy.hud.app.util;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.TextUtils;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonParser;
import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.common.GeoPosition;
import com.here.android.mpa.common.RoadElement;
import com.here.android.mpa.routing.Maneuver;
import com.here.android.mpa.routing.Route;
import com.here.android.mpa.routing.Route.TrafficPenaltyMode;
import com.here.android.mpa.routing.RouteTta;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R.string;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.hud.app.analytics.NavigationQualityTracker;
import com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient;
import com.navdy.hud.app.bluetooth.vcard.VCardConfig;
import com.navdy.hud.app.debug.DriveRecorder;
import com.navdy.hud.app.device.gps.GpsDeadReckoningManager;
import com.navdy.hud.app.device.light.HUDLightUtils;
import com.navdy.hud.app.device.light.LightManager;
import com.navdy.hud.app.event.ShowScreenWithArgs;
import com.navdy.hud.app.framework.DriverProfileHelper;
import com.navdy.hud.app.framework.glance.GlanceConstants;
import com.navdy.hud.app.framework.network.NetworkBandwidthController;
import com.navdy.hud.app.framework.network.NetworkBandwidthController.Component;
import com.navdy.hud.app.framework.network.NetworkStateManager;
import com.navdy.hud.app.gesture.GestureServiceConnector;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder;
import com.navdy.hud.app.maps.here.HereMapUtil;
import com.navdy.hud.app.maps.here.HereMapsManager;
import com.navdy.hud.app.maps.here.HereNavController;
import com.navdy.hud.app.maps.here.HereNavigationManager;
import com.navdy.hud.app.profile.DriverProfile;
import com.navdy.hud.app.profile.HudLocale;
import com.navdy.hud.app.service.ConnectionHandler;
import com.navdy.hud.app.storage.PathManager;
import com.navdy.hud.app.ui.activity.MainActivity;
import com.navdy.hud.app.ui.component.homescreen.HomeScreen.DisplayMode;
import com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2;
import com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.MenuMode;
import com.navdy.hud.app.ui.component.mainmenu.SystemInfoMenu;
import com.navdy.hud.app.ui.framework.UIStateManager;
import com.navdy.service.library.device.NavdyDeviceId;
import com.navdy.service.library.events.DeviceInfo;
import com.navdy.service.library.events.DeviceInfo.Platform;
import com.navdy.service.library.events.preferences.NavigationPreferences;
import com.navdy.service.library.events.preferences.NavigationPreferences.RoutingType;
import com.navdy.service.library.events.ui.Screen;
import com.navdy.service.library.events.ui.ShowScreen;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.network.http.HttpUtils;
import com.navdy.service.library.network.http.IHttpManager;
import com.navdy.service.library.network.http.services.JiraClient;
import com.navdy.service.library.network.http.services.JiraClient.Attachment;
import com.navdy.service.library.network.http.services.JiraClient.ResultCallback;
import com.navdy.service.library.util.CredentialUtil;
import com.navdy.service.library.util.IOUtils;
import com.navdy.service.library.util.LogUtils;
import com.navdy.service.library.util.MusicDataUtils;
import com.squareup.otto.Bus;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.inject.Inject;

import alelec.navdy.hud.app.BuildConfig;
import lanchon.dexpatcher.annotation.DexAdd;
import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexReplace;
import mortar.Mortar;

@DexEdit
public class ReportIssueService extends IntentService {
    @DexIgnore
    private static String ACTION_DUMP;  // = "Dump";
    @DexIgnore
    private static String ACTION_SNAPSHOT;  // = "Snapshot";
    @DexIgnore
    private static String ACTION_SUBMIT_JIRA;  // = "Jira";
    @DexIgnore
    private static String ACTION_SYNC;  // = "Sync";
    @DexIgnore
    public static String ASSIGNEE;  // = "assignee";
    @DexIgnore
    public static String ASSIGNEE_EMAIL;  // = "assigneeEmail";
    @DexIgnore
    public static String ATTACHMENT;  // = "attachment";
    @DexIgnore
    private static SimpleDateFormat DATE_FORMAT;  // = new SimpleDateFormat("yyyy-MM-dd'_'HH:mm:ss.SSS", Locale.US);
    @DexIgnore
    public static String ENVIRONMENT;  // = "Environment";
    @DexIgnore
    public static String EXTRA_ISSUE_TYPE;  // = "EXTRA_ISSUE_TYPE";
    @DexIgnore
    public static String EXTRA_SNAPSHOT_TITLE;  // = "EXTRA_SNAPSHOT_TITLE";
    @DexIgnore
    private static String HUD_ISSUE_TYPE;  // = "Issue";
    @DexIgnore
    private static String HUD_PROJECT;  // = "HUD";
    @DexIgnore
    private static String ISSUE_TYPE;  // = "Task";
    @DexIgnore
    private static String JIRA_CREDENTIALS_META;  // = "JIRA_CREDENTIALS";
    @DexIgnore
    private static int MAX_FILES_OUT_STANDING;  // = 10;
    @DexIgnore
    private static String NAME;  // = "REPORT_ISSUE";
    @DexIgnore
    private static String PROJECT_NAME;  // = "NP";
    @DexIgnore
    public static long RETRY_INTERVAL;  // = TimeUnit.MINUTES.toMillis(3);
    @DexIgnore
    private static SimpleDateFormat SNAPSHOT_DATE_FORMAT;  // = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss", Locale.US);
    @DexIgnore
    private static SimpleDateFormat SNAPSHOT_JIRA_TICKET_DATE_FORMAT;  // = new SimpleDateFormat("MMM,dd hh:mm a", Locale.US);
    @DexIgnore
    public static String SUMMARY;  // = "Summary";
    @DexIgnore
    public static int SYNC_REQ;  // = 128;
    @DexIgnore
    public static String TICKET_ID;  // = "ticketId";
    @DexIgnore
    private static File lastSnapShotFile;
    @DexIgnore
    private static boolean mIsInitialized;  // = false;
    @DexIgnore
    private static PriorityBlockingQueue<File> mIssuesToSync;  // = new PriorityBlockingQueue<>(5, new FilesModifiedTimeComparator());
    @DexIgnore
    private static long mLastIssueSubmittedAt;  // = 0;
    @DexIgnore
    private static AtomicBoolean mSyncing;  // = new AtomicBoolean(false);
    @DexIgnore
    private static String navigationIssuesFolder;
    /* access modifiers changed from: private */
    @DexIgnore
    public static Logger sLogger;  // = new Logger(ReportIssueService.class);
    // @Inject
    @DexIgnore
    Bus bus;
    // @Inject
    @DexIgnore
    DriveRecorder driveRecorder;
    // @Inject
    @DexIgnore
    GestureServiceConnector gestureService;
    // @Inject
    @DexIgnore
    IHttpManager mHttpManager;
    @DexIgnore
    JiraClient mJiraClient;


    @DexAdd
    private static Report lastReport;
    @DexAdd
    public static MainActivity mainActivity = null;

    @DexAdd
    static class Report {
        public File logcat;
        public File logcat_warn;
        public String deviceInfo;
        public File screenshot;
        public File settings;
    }

    // /* renamed from: com.navdy.hud.app.util.ReportIssueService$1 reason: invalid class name */
    // class AnonymousClass1 implements ResultCallback {
    //     final /* synthetic */ File val$fileToSync;
    //
    //     AnonymousClass1(File file) {
    //         this.val$fileToSync = file;
    //     }
    //
    //     public void onError(Throwable th) {
    //         ReportIssueService.sLogger.e("onError, error uploading the files ", th);
    //         ReportIssueService.this.syncLater();
    //     }
    //
    //     public void onSuccess(Object obj) {
    //         ReportIssueService.sLogger.d("Files attached successfully");
    //         ReportIssueService.this.onSyncComplete(this.val$fileToSync);
    //     }
    // }
    //
    // /* renamed from: com.navdy.hud.app.util.ReportIssueService$2 reason: invalid class name */
    // class AnonymousClass2 implements ResultCallback {
    //     final /* synthetic */ String val$assigneeEmail;
    //     final /* synthetic */ String val$assigneeName;
    //     final /* synthetic */ File val$attachmentFile;
    //     final /* synthetic */ File val$fileToSync;
    //     final /* synthetic */ JSONObject val$jsonObject;
    //
    //     /* renamed from: com.navdy.hud.app.util.ReportIssueService$2$1 reason: invalid class name */
    //     class AnonymousClass1 implements ResultCallback {
    //
    //         /* renamed from: com.navdy.hud.app.util.ReportIssueService$2$1$1 reason: invalid class name */
    //         class AnonymousClass1 implements ResultCallback {
    //             AnonymousClass1() {
    //             }
    //
    //             public void onError(Throwable th) {
    //                 ReportIssueService.sLogger.e("Error Assigning the ticket", th);
    //                 ReportIssueService.this.onSyncComplete(AnonymousClass2.this.val$fileToSync);
    //             }
    //
    //             public void onSuccess(Object obj) {
    //                 Logger access$000 = ReportIssueService.sLogger;
    //                 StringBuilder sb = new StringBuilder();
    //                 sb.append("Assigned the ticket successfully to ");
    //                 sb.append(obj);
    //                 access$000.d(sb.toString());
    //                 ReportIssueService.this.onSyncComplete(AnonymousClass2.this.val$fileToSync);
    //             }
    //         }
    //
    //         AnonymousClass1() {
    //         }
    //
    //         public void onError(Throwable th) {
    //             ReportIssueService.this.syncLater();
    //         }
    //
    //         public void onSuccess(Object obj) {
    //             Logger access$000 = ReportIssueService.sLogger;
    //             StringBuilder sb = new StringBuilder();
    //             sb.append("Files successfully attached, Assigning the ticket ot ");
    //             sb.append(AnonymousClass2.this.val$assigneeName);
    //             sb.append(", Email : ");
    //             sb.append(AnonymousClass2.this.val$assigneeEmail);
    //             access$000.d(sb.toString());
    //             ReportIssueService.this.mJiraClient.assignTicketForName((String) obj, AnonymousClass2.this.val$assigneeName, AnonymousClass2.this.val$assigneeEmail, new AnonymousClass2.AnonymousClass1.AnonymousClass1());
    //         }
    //     }
    //
    //     AnonymousClass2(JSONObject jSONObject, File file, File file2, String str, String str2) {
    //         this.val$jsonObject = jSONObject;
    //         this.val$fileToSync = file;
    //         this.val$attachmentFile = file2;
    //         this.val$assigneeName = str;
    //         this.val$assigneeEmail = str2;
    //     }
    //
    //     public void onError(Throwable th) {
    //         Logger access$000 = ReportIssueService.sLogger;
    //         StringBuilder sb = new StringBuilder();
    //         sb.append("Error during sync ");
    //         sb.append(th);
    //         access$000.e(sb.toString());
    //         ReportIssueService.this.syncLater();
    //     }
    //
    //     public void onSuccess(Object obj) {
    //         String str;
    //         FileWriter fileWriter = null;
    //         if (obj instanceof String) {
    //             str = (String) obj;
    //             Logger access$000 = ReportIssueService.sLogger;
    //             StringBuilder sb = new StringBuilder();
    //             sb.append("Issue reported ");
    //             sb.append(str);
    //             access$000.d(sb.toString());
    //         } else {
    //             str = null;
    //         }
    //         try {
    //             this.val$jsonObject.put(ReportIssueService.TICKET_ID, str);
    //             String jSONObject = this.val$jsonObject.toString();
    //             try {
    //                 FileWriter fileWriter2 = new FileWriter(this.val$fileToSync);
    //                 try {
    //                     fileWriter2.write(jSONObject);
    //                     fileWriter2.close();
    //                     IOUtils.closeStream(fileWriter2);
    //                 } catch (IOException e) {
    //                     e = e;
    //                     fileWriter = fileWriter2;
    //                     try {
    //                         Logger access$0002 = ReportIssueService.sLogger;
    //                         StringBuilder sb2 = new StringBuilder();
    //                         sb2.append("Error while dumping the issue ");
    //                         sb2.append(e.getMessage());
    //                         access$0002.e(sb2.toString(), e);
    //                         IOUtils.closeStream(fileWriter);
    //                         ReportIssueService.sLogger.d("Attaching files to the submitted ticket");
    //                         ReportIssueService.this.attachFilesToTheJiraTicket(str, this.val$attachmentFile, new AnonymousClass2.AnonymousClass1());
    //                     } catch (Throwable th) {
    //                         th = th;
    //                         fileWriter2 = fileWriter;
    //                         IOUtils.closeStream(fileWriter2);
    //                         throw th;
    //                     }
    //                 } catch (Throwable th2) {
    //                     th = th2;
    //                     IOUtils.closeStream(fileWriter2);
    //                     throw th;
    //                 }
    //             } catch (IOException e2) {
    //                 e = e2;
    //                 Logger access$00022 = ReportIssueService.sLogger;
    //                 StringBuilder sb22 = new StringBuilder();
    //                 sb22.append("Error while dumping the issue ");
    //                 sb22.append(e.getMessage());
    //                 access$00022.e(sb22.toString(), e);
    //                 IOUtils.closeStream(fileWriter);
    //                 ReportIssueService.sLogger.d("Attaching files to the submitted ticket");
    //                 ReportIssueService.this.attachFilesToTheJiraTicket(str, this.val$attachmentFile, new AnonymousClass2.AnonymousClass1());
    //             }
    //         } catch (JSONException e3) {
    //             ReportIssueService.sLogger.e("JSONException while writing the meta data to the file");
    //         }
    //         ReportIssueService.sLogger.d("Attaching files to the submitted ticket");
    //         ReportIssueService.this.attachFilesToTheJiraTicket(str, this.val$attachmentFile, new AnonymousClass2.AnonymousClass1());
    //     }
    // }
    //
    // /* renamed from: com.navdy.hud.app.util.ReportIssueService$3 reason: invalid class name */
    // class AnonymousClass3 implements ResultCallback {
    //     final /* synthetic */ File val$fileToSync;
    //
    //     AnonymousClass3(File file) {
    //         this.val$fileToSync = file;
    //     }
    //
    //     public void onError(Throwable th) {
    //         Logger access$000 = ReportIssueService.sLogger;
    //         StringBuilder sb = new StringBuilder();
    //         sb.append("Error during sync ");
    //         sb.append(th);
    //         access$000.e(sb.toString());
    //         ReportIssueService.this.syncLater();
    //     }
    //
    //     public void onSuccess(Object obj) {
    //         if (obj instanceof String) {
    //             Logger access$000 = ReportIssueService.sLogger;
    //             StringBuilder sb = new StringBuilder();
    //             sb.append("Issue reported ");
    //             sb.append(obj);
    //             access$000.d(sb.toString());
    //         }
    //         ReportIssueService.sLogger.d("Sync succeeded");
    //         ReportIssueService.this.onSyncComplete(this.val$fileToSync);
    //     }
    // }
    //
    // /* renamed from: com.navdy.hud.app.util.ReportIssueService$4 reason: invalid class name */
    // static final class AnonymousClass4 implements Comparator<File> {
    //     AnonymousClass4() {
    //     }
    //
    //     public int compare(File file, File file2) {
    //         return (file.lastModified() > file2.lastModified() ? 1 : (file.lastModified() == file2.lastModified() ? 0 : -1));
    //     }
    // }
    //
    @DexIgnore
    static class FilesModifiedTimeComparator implements Comparator<File> {
        @DexIgnore
        FilesModifiedTimeComparator() {
        }

        @DexIgnore
        public int compare(File file, File file2) {
            return (int) (file.lastModified() - file2.lastModified());
        }
    }

    @DexIgnore
    public enum IssueType {
        INEFFICIENT_ROUTE_ETA_TRAFFIC(string.eta_inaccurate_traffic, 0, 107),
        INEFFICIENT_ROUTE_SELECTED(string.route_inefficient, 0, BluetoothPbapClient.EVENT_PULL_VCARD_LISTING_SIZE_ERROR),
        ROAD_CLOSED(string.road_closed, string.road_closed_message, BluetoothPbapClient.EVENT_PULL_VCARD_LISTING_ERROR),
        WRONG_DIRECTION(string.wrong_direction, 0, BluetoothPbapClient.EVENT_PULL_PHONE_BOOK_ERROR),
        ROAD_NAME(string.road_name, 0, BluetoothPbapClient.EVENT_PULL_VCARD_ENTRY_ERROR),
        ROAD_CLOSED_PERMANENT(string.permanent_closure, 0, 108),
        OTHER(string.other_navigation_issue, 0, BluetoothPbapClient.EVENT_PULL_PHONE_BOOK_SIZE_ERROR);
        
        @DexIgnore
        private int issueTypeCode;
        @DexIgnore
        private int messageStringResource;
        @DexIgnore
        private int titleStringResource;

        @DexIgnore
        private IssueType(int i, int i2, int i3) {
            this.titleStringResource = i;
            this.messageStringResource = i2;
            this.issueTypeCode = i3;
        }

        @DexIgnore
        public int getIssueTypeCode() {
            return this.issueTypeCode;
        }

        @DexIgnore
        public int getMessageStringResource() {
            return this.messageStringResource;
        }

        @DexIgnore
        public int getTitleStringResource() {
            return this.titleStringResource;
        }
    }

    @DexIgnore
    public ReportIssueService() {
        super(NAME);
    }

    /* access modifiers changed from: private */
    @DexIgnore
    public void attachFilesToTheJiraTicket(String str, File file, ResultCallback resultCallback) {
        if (file.exists() && file.getName().endsWith(".zip")) {
            File file2 = new File(file.getParentFile(), "Staging");
            if (file2.exists()) {
                if (file2.isDirectory()) {
                    IOUtils.deleteDirectory(HudApplication.getAppContext(), file2);
                } else {
                    IOUtils.deleteFile(HudApplication.getAppContext(), file2.getAbsolutePath());
                }
            }
            file2.mkdir();
            try {
                IOUtils.deCompressZipToDirectory(HudApplication.getAppContext(), file, file2);
                File[] listFiles = file2.listFiles();
                ArrayList arrayList = new ArrayList();
                ArrayList arrayList2 = new ArrayList();
                for (File file3 : listFiles) {
                    if (file3.getName().endsWith(".png")) {
                        arrayList2.add(file3);
                    } else {
                        arrayList.add(file3);
                    }
                }
                File[] fileArr = (File[]) arrayList.toArray(new File[arrayList.size()]);
                File file4 = new File(file2, "logs.zip");
                IOUtils.compressFilesToZip(HudApplication.getAppContext(), fileArr, file4.getAbsolutePath());
                ArrayList arrayList3 = new ArrayList();
                Attachment attachment = new Attachment();
                attachment.filePath = file4.getAbsolutePath();
                attachment.mimeType = HttpUtils.MIME_TYPE_ZIP;
                arrayList3.add(attachment);
                Iterator it = arrayList2.iterator();
                while (it.hasNext()) {
                    File file5 = (File) it.next();
                    Attachment attachment2 = new Attachment();
                    attachment2.filePath = file5.getAbsolutePath();
                    attachment2.mimeType = HttpUtils.MIME_TYPE_IMAGE_PNG;
                    arrayList3.add(attachment2);
                }
                this.mJiraClient.attachFilesToTicket(str, arrayList3, resultCallback);
            } catch (IOException e) {
                sLogger.e("Error decompressing the zip file to get the attachments", e);
            }
        }
    }

    @DexIgnore
    private String buildDescription() {
        String str;
        String str2;
        String str3;
        String str4;
        String str5;
        NavdyDeviceId thisDevice = NavdyDeviceId.getThisDevice(this);
        StringBuilder sb = new StringBuilder();
        sb.append("\n#############\n");
        sb.append("HERE metadata: ");
        sb.append("\n#############\n\n");
        String str6 = "";
        String currentHereSdkVersion = DeviceUtil.getCurrentHereSdkVersion();
        String hEREMapsDataInfo = DeviceUtil.getHEREMapsDataInfo();
        if (hEREMapsDataInfo != null) {
            try {
                str6 = new GsonBuilder().setPrettyPrinting().create().toJson(new JsonParser().parse(hEREMapsDataInfo));
            } catch (Throwable th) {
                sLogger.e(th);
            }
        }
        if (!TextUtils.isEmpty(currentHereSdkVersion)) {
            sb.append("HERE mSDK for Android v");
            sb.append(currentHereSdkVersion);
            sb.append(GlanceConstants.NEWLINE);
        }
        if (!TextUtils.isEmpty(str6)) {
            sb.append("HERE Map Data:\n");
            sb.append(hEREMapsDataInfo);
            sb.append(GlanceConstants.NEWLINE);
        }
        sb.append("\n#############\n");
        sb.append("Navigation details:");
        sb.append("\n#############\n\n");
        GeoPosition lastGeoPosition = HereMapsManager.getInstance().getLastGeoPosition();
        if (lastGeoPosition == null || lastGeoPosition.getCoordinate() == null) {
            str = "Current Location: N/A\n";
        } else {
            sb.append("Current Location: ");
            sb.append(lastGeoPosition.getCoordinate().getLatitude());
            sb.append(HereManeuverDisplayBuilder.COMMA);
            sb.append(lastGeoPosition.getCoordinate().getLongitude());
            str = GlanceConstants.NEWLINE;
        }
        sb.append(str);
        sb.append("Current Road: ");
        sb.append(HereMapUtil.getCurrentRoadName());
        sb.append(GlanceConstants.NEWLINE);
        RoadElement currentRoadElement = HereMapUtil.getCurrentRoadElement();
        if (currentRoadElement != null) {
            sb.append("Current road element ID: ");
            sb.append(currentRoadElement.getIdentifier());
            sb.append(GlanceConstants.NEWLINE);
        }
        DriverProfileHelper instance = DriverProfileHelper.getInstance();
        DriverProfile currentProfile = instance.getCurrentProfile();
        if (HereMapsManager.getInstance().isNavigationModeOn()) {
            HereNavigationManager instance2 = HereNavigationManager.getInstance();
            Route currentRoute = instance2.getCurrentRoute();
            HereNavController navController = instance2.getNavController();
            if (currentRoute != null) {
                RouteTta tta = navController.getTta(TrafficPenaltyMode.OPTIMAL, true);
                GeoCoordinate startLocation = instance2.getStartLocation();
                if (startLocation != null) {
                    sb.append("Route Starting point: ");
                    sb.append(startLocation.getLatitude());
                    sb.append(HereManeuverDisplayBuilder.COMMA);
                    sb.append(startLocation.getLongitude());
                    str4 = GlanceConstants.NEWLINE;
                } else {
                    str4 = "Route Starting point: N/A\n";
                }
                sb.append(str4);
                GeoCoordinate destination = currentRoute.getDestination();
                if (destination != null) {
                    sb.append("Route Ending point: ");
                    sb.append(destination.getLatitude());
                    sb.append(HereManeuverDisplayBuilder.COMMA);
                    sb.append(destination.getLongitude());
                    str5 = GlanceConstants.NEWLINE;
                } else {
                    str5 = "Route Ending point: N/A\n";
                }
                sb.append(str5);
                boolean isTrafficConsidered = instance2.isTrafficConsidered(instance2.getCurrentRouteId());
                sb.append("Traffic Used: ");
                sb.append(isTrafficConsidered);
                sb.append(GlanceConstants.NEWLINE);
                long tripStartUtc = NavigationQualityTracker.getInstance().getTripStartUtc();
                if (tripStartUtc > 0) {
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMMM dd HH:mm:ss zzzz yyyy Z", Locale.US);
                    sb.append("Trip started: ");
                    sb.append(simpleDateFormat.format(new Date(tripStartUtc)));
                    sb.append(GlanceConstants.NEWLINE);
                }
                NavigationQualityTracker instance3 = NavigationQualityTracker.getInstance();
                int actualDurationSoFar = instance3.getActualDurationSoFar();
                int expectedDuration = instance3.getExpectedDuration();
                int expectedDistance = instance3.getExpectedDistance();
                long destinationDistance = navController.getDestinationDistance();
                if (expectedDuration > 0) {
                    sb.append("Expected initial ETA according to HERE: ");
                    sb.append(expectedDuration);
                    sb.append(" seconds\n");
                }
                if (expectedDistance > 0) {
                    sb.append("Expected initial distance according to HERE: ");
                    sb.append(expectedDistance);
                    sb.append(" meters\n");
                }
                if (tta != null) {
                    int duration = tta.getDuration();
                    sb.append("Time To Arrival remaining according to HERE: ");
                    sb.append(duration);
                    sb.append(" seconds\n");
                }
                if (destinationDistance > 0) {
                    sb.append("Distance remaining according to HERE: ");
                    sb.append(destinationDistance);
                    sb.append(" meters\n");
                }
                if (actualDurationSoFar > 0) {
                    sb.append("Duration of the trip so far: ");
                    sb.append(actualDurationSoFar);
                    sb.append(" seconds\n");
                }
            }
            String destinationLabel = instance2.getDestinationLabel();
            sb.append("Destination: ");
            sb.append(destinationLabel);
            sb.append(", ");
            sb.append(instance2.getDestinationStreetAddress());
            sb.append(GlanceConstants.NEWLINE);
            sb.append("\n#############\n");
            sb.append("User route preferences:");
            sb.append("\n#############\n\n");
            NavigationPreferences navigationPreferences = currentProfile.getNavigationPreferences();
            String str7 = navigationPreferences.routingType == RoutingType.ROUTING_FASTEST ? "fastest" : "shortest";
            sb.append("Route calculation: ");
            sb.append(str7);
            sb.append(GlanceConstants.NEWLINE);
            sb.append("Highways: ");
            sb.append(navigationPreferences.allowHighways);
            sb.append(GlanceConstants.NEWLINE);
            sb.append("Toll roads: ");
            sb.append(navigationPreferences.allowTollRoads);
            sb.append(GlanceConstants.NEWLINE);
            sb.append("Ferries: ");
            sb.append(navigationPreferences.allowFerries);
            sb.append(GlanceConstants.NEWLINE);
            sb.append("Tunnels: ");
            sb.append(navigationPreferences.allowTunnels);
            sb.append(GlanceConstants.NEWLINE);
            sb.append("Unpaved Roads: ");
            sb.append(navigationPreferences.allowUnpavedRoads);
            sb.append(GlanceConstants.NEWLINE);
            sb.append("Auto Trains: ");
            sb.append(navigationPreferences.allowAutoTrains);
            sb.append(GlanceConstants.NEWLINE);
            sb.append("\n#############\n");
            sb.append("Maneuvers (entire route):");
            sb.append("\n#############\n\n");
            HereMapUtil.printRouteDetails(currentRoute, destinationLabel, null, sb);
            sb.append("\n#############\n");
            sb.append("Waypoints (entire route):");
            sb.append("\n#############\n\n");
            int i = 0;
            for (Maneuver coordinate : currentRoute.getManeuvers()) {
                GeoCoordinate coordinate2 = coordinate.getCoordinate();
                if (coordinate2 != null) {
                    double latitude = coordinate2.getLatitude();
                    double longitude = coordinate2.getLongitude();
                    sb.append("waypoint");
                    sb.append(i);
                    sb.append(": ");
                    sb.append(latitude);
                    sb.append(HereManeuverDisplayBuilder.COMMA);
                    sb.append(longitude);
                    sb.append(GlanceConstants.NEWLINE);
                    i++;
                }
            }
            sb.append("\n#############\n");
            sb.append("Maneuvers (remaining route):");
            sb.append("\n#############\n\n");
            HereMapUtil.printRouteDetails(currentRoute, destinationLabel, null, sb, instance2.getNextManeuver());
        }
        sb.append("\n#############\n");
        sb.append("Navdy info:");
        sb.append("\n#############\n\n");
        sb.append("Device ID: ");
        sb.append(thisDevice.toString());
        sb.append(GlanceConstants.NEWLINE);
        sb.append("HUD app version: ").append(BuildConfig.VERSION_NAME).append(GlanceConstants.NEWLINE);
        sb.append("Serial number: ");
        sb.append(Build.SERIAL);
        sb.append(GlanceConstants.NEWLINE);
        sb.append("Model: ");
        sb.append(Build.MODEL);
        sb.append(GlanceConstants.NEWLINE);
        sb.append("API Level: ");
        sb.append(VERSION.SDK_INT);
        sb.append(GlanceConstants.NEWLINE);
        sb.append("Build type: ");
        sb.append(Build.TYPE);
        sb.append(GlanceConstants.NEWLINE);
        if (!currentProfile.isDefaultProfile()) {
            str3 = currentProfile.getDriverName();
            str2 = currentProfile.getDriverEmail();
        } else {
            str3 = instance.getDriverProfileManager().getLastUserName();
            str2 = instance.getDriverProfileManager().getLastUserEmail();
        }
        if (!TextUtils.isEmpty(str3) && !TextUtils.isEmpty(str2)) {
            sb.append("Username: ");
            sb.append(str3);
            sb.append(GlanceConstants.NEWLINE);
            sb.append("Email: ");
            sb.append(str2);
            sb.append(GlanceConstants.NEWLINE);
        }
        return sb.toString();
    }

    @DexIgnore
    public static boolean canReportIssue() {
        return mIssuesToSync.size() <= 10;
    }

    @DexIgnore
    public static void dispatchReportNewIssue(IssueType issueType) {
        Intent intent = new Intent(HudApplication.getAppContext(), ReportIssueService.class);
        intent.setAction(ACTION_DUMP);
        intent.putExtra(EXTRA_ISSUE_TYPE, issueType);
        HudApplication.getAppContext().startService(intent);
    }

    @DexIgnore
    public static void dispatchSync() {
        Intent intent = new Intent(HudApplication.getAppContext(), ReportIssueService.class);
        intent.setAction(ACTION_SYNC);
        HudApplication.getAppContext().startService(intent);
    }

    @DexIgnore
    private void dumpIssue(IssueType issueType) {
        if (HereMapsManager.getInstance().isNavigationModeOn()) {
            StringBuilder sb = new StringBuilder();
            StringBuilder sb2 = new StringBuilder();
            sb2.append(issueType.ordinal());
            sb2.append(GlanceConstants.NEWLINE);
            sb.append(sb2.toString());
            sb.append(buildDescription());
            saveJiraTicketDescriptionToFile(sb.toString(), false);
            mLastIssueSubmittedAt = System.currentTimeMillis();
        }
    }

    @DexAdd
    private String deviceInfoString(String extra) {
        String deviceInfo = "";
        ConnectionHandler connectionHandler = RemoteDeviceManager.getInstance().getConnectionHandler();
        if (connectionHandler != null) {
            DeviceInfo lastConnectedDeviceInfo = connectionHandler.getLastConnectedDeviceInfo();
            if (lastConnectedDeviceInfo != null) {
                deviceInfo += CrashReportService.printDeviceInfo(lastConnectedDeviceInfo).replace("\n", "<br/>");
            }
            if (extra != null) {
                deviceInfo += "<hr/>" + extra;
            }
        }
        return deviceInfo;
    }

    @DexAdd
    private File deviceInfo(String extra) {
        try {
            String folder = PathManager.getInstance().getNonFatalCrashReportDir();
            File deviceInfoTextFile = new File(folder + File.separator + "deviceInfo.txt");
            if (deviceInfoTextFile.exists()) {
                IOUtils.deleteFile(this, deviceInfoTextFile.getAbsolutePath());
            }
            FileWriter fileWriter = new FileWriter(deviceInfoTextFile);
            ConnectionHandler connectionHandler = RemoteDeviceManager.getInstance().getConnectionHandler();
            if (connectionHandler != null) {
                DeviceInfo lastConnectedDeviceInfo = connectionHandler.getLastConnectedDeviceInfo();
                if (lastConnectedDeviceInfo != null) {
                    fileWriter.write(CrashReportService.printDeviceInfo(lastConnectedDeviceInfo));
                }
                if (extra != null) {
                    fileWriter.write("\n\n----------\n\n");
                    fileWriter.write(extra);
                }
            }
            fileWriter.flush();
            fileWriter.close();
            return deviceInfoTextFile;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    @DexAdd
    private String logcat() {
        return logcat(false);
    }

    @DexAdd
    private String logcat(Boolean filtered) {
        String ret = null;
        try {
            String filename = (filtered) ? "logcat_warn.txt" : "logcat.txt";
            String folder = PathManager.getInstance().getNonFatalCrashReportDir();
            File logcatf = new File(folder+"/"+filename);
            if (logcatf.exists()) {
                IOUtils.deleteFile(this, logcatf.getAbsolutePath());
            }
            String cmd = "logcat -d -f"+logcatf.getAbsolutePath();
            if (filtered) {
                cmd += " *:W";
            }
            Process proc = Runtime.getRuntime().exec(cmd);
            try {
                proc.waitFor();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            ret = org.apache.commons.io.FileUtils.readFileToString(logcatf);
            IOUtils.deleteFile(this, logcatf.getAbsolutePath());

        } catch (IOException e) {
            e.printStackTrace();
        }
        return ret;
    }

    @DexAdd
    private File logcatf(Boolean filtered) {
        File ret = null;
        try {
            String filename = (filtered) ? "logcat_warn.txt" : "logcat.txt";
            String folder = PathManager.getInstance().getNonFatalCrashReportDir();
            File logcatf = new File(folder+"/"+filename);
            if (logcatf.exists()) {
                IOUtils.deleteFile(this, logcatf.getAbsolutePath());
            }
            String cmd = "logcat -d -f"+logcatf.getAbsolutePath();
            if (filtered) {
                cmd += " *:W";
            }
            Process proc = Runtime.getRuntime().exec(cmd);
            try {
                proc.waitFor();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            ret = logcatf;

        } catch (IOException e) {
            e.printStackTrace();
        }
        return ret;
    }

    @DexAdd
    public Report generateReport() {
        HUDLightUtils.showSnapshotCollection(HudApplication.getAppContext(), LightManager.getInstance(), false);
        try {
            Report report = new Report();
            String folder = PathManager.getInstance().getNonFatalCrashReportDir();

            report.logcat = logcatf(false);
            report.logcat_warn = logcatf(true);

            DeviceUtil.takeDeviceScreenShot(folder + File.separator + "screen.png");
            report.screenshot = new File(folder + File.separator + "screen.png");

            SystemInfoMenu info = new SystemInfoMenu(RemoteDeviceManager.getInstance().getBus(),
                    null, null, null);
            if (info != null) {
                report.deviceInfo = info.getDiagnosticText(HudApplication.getAppContext());
            } else {
                report.deviceInfo = deviceInfoString(null);
            }

            report.settings = new File("/maps/settings.txt");

            lastReport = report;
            return report;

        } finally {
            HUDLightUtils.showSnapshotCollection(HudApplication.getAppContext(), LightManager.getInstance(), true);
        }
    }

    @DexReplace  // from 3062
    private void dumpSnapshot() throws Throwable {
        sLogger.d("Creating the snapshot of the HUD for support ticket");
        HUDLightUtils.showSnapshotCollection(HudApplication.getAppContext(), LightManager.getInstance(), false);
        String folder = PathManager.getInstance().getNonFatalCrashReportDir();
        DeviceUtil.takeDeviceScreenShot(folder + File.separator + "screen.png");
        List<File> snapshotFiles = new ArrayList<>();
        if (Build.TYPE.equals("eng")) {
            this.gestureService.dumpRecording();
        }
        this.driveRecorder.flushRecordings();
        this.gestureService.takeSnapShot(GestureServiceConnector.SNAPSHOT_PATH);
        File deviceInfoTextFile = new File(folder + File.separator + "deviceInfo.txt");
        if (deviceInfoTextFile.exists()) {
            IOUtils.deleteFile(this, deviceInfoTextFile.getAbsolutePath());
        }
        FileWriter fileWriter = new FileWriter(deviceInfoTextFile);
        ConnectionHandler connectionHandler = RemoteDeviceManager.getInstance().getConnectionHandler();
        if (connectionHandler != null) {
            DeviceInfo lastConnectedDeviceInfo = connectionHandler.getLastConnectedDeviceInfo();
            if (lastConnectedDeviceInfo != null) {
                fileWriter.write(CrashReportService.printDeviceInfo(lastConnectedDeviceInfo));
            }
        }
        fileWriter.flush();
        try {
            fileWriter.close();
        } catch (IOException e) {
            sLogger.e("Error closing the file writer for log file", e);
        } catch (Throwable t) {
            try {
                sLogger.e("Error creating a snapshot ", t);
                return;
            } finally {
                HUDLightUtils.showSnapshotCollection(HudApplication.getAppContext(), LightManager.getInstance(), true);
            }
        }

        File tempdir = new File(folder + File.separator + "temp");
        if (tempdir.exists()) {
            IOUtils.deleteDirectory(this, tempdir);
        }
        if (tempdir.mkdirs()) {
            String stagingPath = tempdir.getAbsolutePath();
            LogUtils.copySnapshotSystemLogs(stagingPath);
            GpsDeadReckoningManager.getInstance().dumpGpsInfo(stagingPath);
            PathManager.getInstance().collectEnvironmentInfo(stagingPath);
            String cmd = "logcat -d -f"+tempdir+"/logcat"; // Grab logcat
            Runtime.getRuntime().exec(cmd);
            File[] logFiles = tempdir.listFiles();
            tempdir = new File(folder + File.separator + "route.log");
            if (tempdir.exists()) {
                IOUtils.deleteFile(this, tempdir.getAbsolutePath());
            }
            String routeDescription = buildDescription();
            fileWriter = new FileWriter(tempdir);
            fileWriter.write(routeDescription);
            fileWriter.flush();
            try {
                fileWriter.close();
            } catch (IOException e2) {
                sLogger.e("Error closing the file writer for route log file", e2);
            }
            snapshotFiles.add(new File(GestureServiceConnector.SNAPSHOT_PATH));
            snapshotFiles.add(new File(folder + File.separator + "screen.png"));
            snapshotFiles.add(tempdir);
            snapshotFiles.add(deviceInfoTextFile);
            if (logFiles != null) {
                Collections.addAll(snapshotFiles, logFiles);
            }
            HashSet<File> driveLogFilesSet = new HashSet<>();
            List<File> latestFiles = getLatestDriveLogFiles(1);
            if (latestFiles != null && latestFiles.size() > 0) {
                snapshotFiles.addAll(latestFiles);
                for (File latestFile : latestFiles) {
                    driveLogFilesSet.add(latestFile);
                }
            }
            File zipfile = new File(folder + File.separator + ("snapshot-" + Build.SERIAL + "-" + Build.VERSION.INCREMENTAL + MusicDataUtils.ALTERNATE_SEPARATOR + SNAPSHOT_DATE_FORMAT.format(new Date(System.currentTimeMillis())) + ".zip"));
            if (zipfile.exists()) {
                IOUtils.deleteFile(HudApplication.getAppContext(), zipfile.getAbsolutePath());
            }
            File[] files = new File[snapshotFiles.size()];
            snapshotFiles.toArray(files);
            if (zipfile.createNewFile()) {
                IOUtils.compressFilesToZip(this, files, zipfile.getAbsolutePath());
                for (File file2 : snapshotFiles) {
                    if (!driveLogFilesSet.contains(file2)) {
                        IOUtils.deleteFile(HudApplication.getAppContext(), file2.getAbsolutePath());
                    }
                }
                IOUtils.deleteDirectory(this, tempdir);
                lastSnapShotFile = zipfile;
                sLogger.d("Snapshot File created " + lastSnapShotFile);
                // alelec: CrashReportService attaches this file to a crash report, zips the report and puts it
                // in a queue for a FileTransferRequest, then deletes it. I think this is to send it to phone, but
                // I can't see what happens to it then (nothing much useful to us)
                CrashReportService.addSnapshotAsync(zipfile.getAbsolutePath());
                HUDLightUtils.showSnapshotCollection(HudApplication.getAppContext(), LightManager.getInstance(), true);
                return;
            }
            sLogger.e("snapshot file could not be created");
            HUDLightUtils.showSnapshotCollection(HudApplication.getAppContext(), LightManager.getInstance(), true);

//            ErrorReporter reporter = ACRA.getErrorReporter();
//            reporter.handleException(null);


            return;
        }
        sLogger.e("could not create tempDirectory");
        HUDLightUtils.showSnapshotCollection(HudApplication.getAppContext(), LightManager.getInstance(), true);
    }

    @DexAdd
    private void dumpSnapshotOrig() throws Throwable {
        String nonFatalCrashReportDir = "";
        ArrayList<File> arrayList = new ArrayList<>();;
        File file = null;
        Logger logger = sLogger;;
        String str = "";
        sLogger.d("Creating the snapshot of the HUD for support ticket");
        HUDLightUtils.showSnapshotCollection(HudApplication.getAppContext(), LightManager.getInstance(), false);
        try {
            nonFatalCrashReportDir = PathManager.getInstance().getNonFatalCrashReportDir();
            StringBuilder sb = new StringBuilder();
            sb.append(nonFatalCrashReportDir);
            sb.append(File.separator);
            sb.append("screen.png");
            DeviceUtil.takeDeviceScreenShot(sb.toString());
            arrayList = new ArrayList<>();
            if (Build.TYPE.equals("eng")) {
                this.gestureService.dumpRecording();
            }
            this.driveRecorder.flushRecordings();
            this.gestureService.takeSnapShot(GestureServiceConnector.SNAPSHOT_PATH);
            StringBuilder sb2 = new StringBuilder();
            sb2.append(nonFatalCrashReportDir);
            sb2.append(File.separator);
            sb2.append("deviceInfo.txt");
            file = new File(sb2.toString());
            if (file.exists()) {
                IOUtils.deleteFile(this, file.getAbsolutePath());
            }
            FileWriter fileWriter = new FileWriter(file);
            ConnectionHandler connectionHandler = RemoteDeviceManager.getInstance().getConnectionHandler();
            if (connectionHandler != null) {
                DeviceInfo lastConnectedDeviceInfo = connectionHandler.getLastConnectedDeviceInfo();
                if (lastConnectedDeviceInfo != null) {
                    fileWriter.write(CrashReportService.printDeviceInfo(lastConnectedDeviceInfo));
                }
            }
            fileWriter.flush();
            fileWriter.close();
        } catch (IOException e) {
            sLogger.e("Error closing the file writer for log file", e);
        } catch (Throwable th) {
            try {
                sLogger.e("Error creating a snapshot ", th);
            } catch (Throwable th2) {
                HUDLightUtils.showSnapshotCollection(HudApplication.getAppContext(), LightManager.getInstance(), true);
                throw th2;
            }
        }
        StringBuilder sb3 = new StringBuilder();
        sb3.append(nonFatalCrashReportDir);
        sb3.append(File.separator);
        sb3.append("temp");
        File file2 = new File(sb3.toString());
        if (file2.exists()) {
            IOUtils.deleteDirectory(this, file2);
        }
        if (!file2.mkdirs()) {
            logger = sLogger;
            str = "could not create tempDirectory";
        } else {
            String absolutePath = file2.getAbsolutePath();
            LogUtils.copySnapshotSystemLogs(absolutePath);
            GpsDeadReckoningManager.getInstance().dumpGpsInfo(absolutePath);
            PathManager.getInstance().collectEnvironmentInfo(absolutePath);
            File[] listFiles = file2.listFiles();
            StringBuilder sb4 = new StringBuilder();
            sb4.append(nonFatalCrashReportDir);
            sb4.append(File.separator);
            sb4.append("route.log");
            File file3 = new File(sb4.toString());
            if (file3.exists()) {
                IOUtils.deleteFile(this, file3.getAbsolutePath());
            }
            String buildDescription = buildDescription();
            FileWriter fileWriter2 = new FileWriter(file3);
            fileWriter2.write(buildDescription);
            fileWriter2.flush();
            try {
                fileWriter2.close();
            } catch (IOException e2) {
                sLogger.e("Error closing the file writer for route log file", e2);
            }
            arrayList.add(new File(GestureServiceConnector.SNAPSHOT_PATH));
            StringBuilder sb5 = new StringBuilder();
            sb5.append(nonFatalCrashReportDir);
            sb5.append(File.separator);
            sb5.append("screen.png");
            arrayList.add(new File(sb5.toString()));
            arrayList.add(file3);
            arrayList.add(file);
            if (listFiles != null) {
                Collections.addAll(arrayList, listFiles);
            }
            HashSet hashSet = new HashSet();
            List<File> latestDriveLogFiles = getLatestDriveLogFiles(1);
            if (latestDriveLogFiles != null && latestDriveLogFiles.size() > 0) {
                arrayList.addAll(latestDriveLogFiles);
                for (File add : latestDriveLogFiles) {
                    hashSet.add(add);
                }
            }
            String format = SNAPSHOT_DATE_FORMAT.format(new Date(System.currentTimeMillis()));
            StringBuilder sb6 = new StringBuilder();
            sb6.append("snapshot-");
            sb6.append(Build.SERIAL);
            sb6.append("-");
            sb6.append(VERSION.INCREMENTAL);
            sb6.append(MusicDataUtils.ALTERNATE_SEPARATOR);
            sb6.append(format);
            sb6.append(".zip");
            String sb7 = sb6.toString();
            StringBuilder sb8 = new StringBuilder();
            sb8.append(nonFatalCrashReportDir);
            sb8.append(File.separator);
            sb8.append(sb7);
            File file4 = new File(sb8.toString());
            if (file4.exists()) {
                IOUtils.deleteFile(HudApplication.getAppContext(), file4.getAbsolutePath());
            }
            File[] fileArr = new File[arrayList.size()];
            arrayList.toArray(fileArr);
            if (!file4.createNewFile()) {
                logger = sLogger;
                str = "snapshot file could not be created";
            } else {
                IOUtils.compressFilesToZip(this, fileArr, file4.getAbsolutePath());
                for (File file5 : arrayList) {
                    if (!hashSet.contains(file5)) {
                        IOUtils.deleteFile(HudApplication.getAppContext(), file5.getAbsolutePath());
                    }
                }
                IOUtils.deleteDirectory(this, file2);
                lastSnapShotFile = file4;
                Logger logger2 = sLogger;
                StringBuilder sb9 = new StringBuilder();
                sb9.append("Snapshot File created ");
                sb9.append(lastSnapShotFile);
                logger2.d(sb9.toString());
                CrashReportService.addSnapshotAsync(file4.getAbsolutePath());
                HUDLightUtils.showSnapshotCollection(HudApplication.getAppContext(), LightManager.getInstance(), true);
            }
        }
        logger.e(str);
        HUDLightUtils.showSnapshotCollection(HudApplication.getAppContext(), LightManager.getInstance(), true);
    }

    @DexIgnore
    public static void dumpSnapshotAsync() {
        Intent intent = new Intent(HudApplication.getAppContext(), ReportIssueService.class);
        intent.setAction(ACTION_SNAPSHOT);
        HudApplication.getAppContext().startService(intent);
    }

    @DexIgnore
    public static IssueType getIssueTypeForId(int i) {
        IssueType[] values = IssueType.values();
        if (i < 0 || i >= values.length) {
            return null;
        }
        return values[i];
    }

    @DexIgnore
    public static List<File> getLatestDriveLogFiles(int i) {
        // if (i > 0) {
        //     File[] listFiles = DriveRecorder.getDriveLogsDir("").listFiles();
        //     if (!(listFiles == null || listFiles.length == 0)) {
        //         ArrayList arrayList = new ArrayList();
        //         Arrays.sort(listFiles, new AnonymousClass4());
        //         for (int length = listFiles.length - 1; length >= 0 && arrayList.size() < i * 4; length--) {
        //             arrayList.add(listFiles[length]);
        //         }
        //         return arrayList;
        //     }
        // }
        return null;
    }

    @DexIgnore
    private String getSummary(IssueType issueType) {
        StringBuilder sb;
        int titleStringResource;
        if (issueType.getMessageStringResource() != 0) {
            sb = new StringBuilder();
            sb.append("Navigation issue: [");
            sb.append(issueType.getIssueTypeCode());
            sb.append("] ");
            sb.append(getString(issueType.getTitleStringResource()));
            sb.append(": ");
            titleStringResource = issueType.getMessageStringResource();
        } else {
            sb = new StringBuilder();
            sb.append("Navigation issue: [");
            sb.append(issueType.getIssueTypeCode());
            sb.append("] ");
            titleStringResource = issueType.getTitleStringResource();
        }
        sb.append(getString(titleStringResource));
        return sb.toString();
    }

    @DexIgnore
    public static void initialize() {
        if (!mIsInitialized) {
            sLogger.d("Initializing the service statically");
            navigationIssuesFolder = PathManager.getInstance().getNavigationIssuesDir();
            File[] listFiles = new File(navigationIssuesFolder).listFiles();
            Logger logger = sLogger;
            StringBuilder sb = new StringBuilder();
            sb.append("Number of Files waiting for sync :");
            sb.append(listFiles != null ? listFiles.length : 0);
            logger.d(sb.toString());
            if (listFiles != null) {
                for (File file : listFiles) {
                    Logger logger2 = sLogger;
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("File ");
                    sb2.append(file.getName());
                    logger2.d(sb2.toString());
                    mIssuesToSync.add(file);
                    if (mIssuesToSync.size() == 10) {
                        File file2 = (File) mIssuesToSync.poll();
                        Logger logger3 = sLogger;
                        StringBuilder sb3 = new StringBuilder();
                        sb3.append("Deleting the old file ");
                        sb3.append(file2.getName());
                        logger3.d(sb3.toString());
                        IOUtils.deleteFile(HudApplication.getAppContext(), file2.getAbsolutePath());
                    }
                }
            }
            mIsInitialized = true;
        }
    }

    /* access modifiers changed from: private */
    @DexIgnore
    public void onSyncComplete(File file) {
        if (file != null) {
            Logger logger = sLogger;
            StringBuilder sb = new StringBuilder();
            sb.append("Removed the file from list :");
            sb.append(mIssuesToSync.remove(file));
            logger.d(sb.toString());
            IOUtils.deleteFile(this, file.getAbsolutePath());
            dispatchSync();
        }
        mSyncing.set(false);
    }

    @DexIgnore
    private void saveJiraTicketDescriptionToFile(String str, boolean z) {
        // long currentTimeMillis = System.currentTimeMillis();
        // StringBuilder sb = new StringBuilder();
        // sb.append(navigationIssuesFolder);
        // sb.append(File.separator);
        // sb.append(DATE_FORMAT.format(new Date(currentTimeMillis)));
        // sb.append(z ? ".json" : "");
        // File file = new File(sb.toString());
        // if (file.exists()) {
        //     IOUtils.deleteFile(this, file.getAbsolutePath());
        // }
        // FileWriter fileWriter = null;
        // try {
        //     if (file.createNewFile()) {
        //         FileWriter fileWriter2 = new FileWriter(file);
        //         try {
        //             fileWriter2.write(str);
        //             fileWriter2.close();
        //             mIssuesToSync.add(file);
        //             dispatchSync();
        //             fileWriter = fileWriter2;
        //         } catch (IOException e) {
        //             e = e;
        //             fileWriter = fileWriter2;
        //             try {
        //                 Logger logger = sLogger;
        //                 StringBuilder sb2 = new StringBuilder();
        //                 sb2.append("Error while dumping the issue ");
        //                 sb2.append(e.getMessage());
        //                 logger.e(sb2.toString(), e);
        //                 IOUtils.closeStream(fileWriter);
        //             } catch (Throwable th) {
        //                 th = th;
        //                 fileWriter2 = fileWriter;
        //             }
        //         } catch (Throwable th2) {
        //             th = th2;
        //             IOUtils.closeStream(fileWriter2);
        //             throw th;
        //         }
        //     }
        // } catch (IOException e2) {
        //     e = e2;
        //     Logger logger2 = sLogger;
        //     StringBuilder sb22 = new StringBuilder();
        //     sb22.append("Error while dumping the issue ");
        //     sb22.append(e.getMessage());
        //     logger2.e(sb22.toString(), e);
        //     IOUtils.closeStream(fileWriter);
        // }
        // IOUtils.closeStream(fileWriter);
    }

    @DexIgnore
    public static void scheduleSync() {
        // Intent intent = new Intent(HudApplication.getAppContext(), ReportIssueService.class);
        // intent.setAction(ACTION_SYNC);
        // ((AlarmManager) HudApplication.getAppContext().getSystemService(Context.ALARM_SERVICE)).set(3, SystemClock.elapsedRealtime() + RETRY_INTERVAL, PendingIntent.getService(HudApplication.getAppContext(), 128, intent, VCardConfig.FLAG_REFRAIN_QP_TO_NAME_PROPERTIES));
    }

    @DexReplace
    public static void showSnapshotMenu() {
        // Switch to HOME first quickly, in case we're already in menu as switching to snapshot from within menu doesn't work
        RemoteDeviceManager.getInstance().getBus().post(new ShowScreen.Builder().screen(Screen.SCREEN_HOME).build());

        final Bundle bundle = new Bundle();
        bundle.putInt("MENU_MODE", MainMenuScreen2.MenuMode.SNAPSHOT_TITLE_PICKER.ordinal());
        RemoteDeviceManager.getInstance().getBus().post(new ShowScreenWithArgs(Screen.SCREEN_MAIN_MENU, bundle, null, false));

    }

    @DexReplace
    private void submitJiraTicket(String s) {
        try {
            final DriverProfile currentProfile = DriverProfileHelper.getInstance().getCurrentProfile();

            ReportIssueService.sLogger.d("Submitting HockeyApp ticket");

            Boolean sent;

            if (lastReport != null) {

//                sent = HockeySender.uploadReport(lastReport.logcat, Build.VERSION.INCREMENTAL,
//                        currentProfile.getDriverName(), currentProfile.getDriverEmail(),
//                        lastReport.deviceInfo,
//                        lastReport.screenshot,
//                        lastReport.settings);
                sent = GitlabSender.uploadReport(lastReport.logcat, lastReport.logcat_warn, Build.VERSION.INCREMENTAL,
                        currentProfile.getDriverName(), currentProfile.getDriverEmail(),
                        lastReport.deviceInfo,
                        lastReport.screenshot,
                        lastReport.settings);
            }

        } catch (Throwable throwable) {
            throwable.printStackTrace();
            sLogger.e("Error while submitting the issue " + throwable.getMessage(), throwable);
        }
    }

    @DexAdd
    private void submitJiraTicketOrig(String str) {
        // Logger logger = sLogger;
        // StringBuilder sb = new StringBuilder();
        // sb.append("Submitting jira ticket with title ");
        // sb.append(str);
        // logger.d(sb.toString());
        // Date date = new Date(System.currentTimeMillis());
        // ConnectionHandler connectionHandler = RemoteDeviceManager.getInstance().getConnectionHandler();
        // JSONObject jSONObject = new JSONObject();
        // jSONObject.put(ATTACHMENT, lastSnapShotFile.getAbsolutePath());
        // StringBuilder sb2 = new StringBuilder();
        // UIStateManager uiStateManager = RemoteDeviceManager.getInstance().getUiStateManager();
        // String format = SNAPSHOT_JIRA_TICKET_DATE_FORMAT.format(date);
        // DriverProfile currentProfile = DriverProfileHelper.getInstance().getCurrentProfile();
        // StringBuilder sb3 = new StringBuilder();
        // sb3.append("[Snapshot ");
        // sb3.append(str);
        // sb3.append(" ");
        // sb3.append(format);
        // sb3.append("] ");
        // try {
        //     sb3.append(DisplayMode.MAP.equals(uiStateManager.getHomescreenView().getDisplayMode()) ? "Map" : "Dash");
        //     sb2.append(sb3.toString());
        //     String currentRoadName = HereMapUtil.getCurrentRoadName();
        //     if (!TextUtils.isEmpty(currentRoadName)) {
        //         sb2.append(" ");
        //         sb2.append(currentRoadName);
        //     }
        //     if (!currentProfile.isDefaultProfile()) {
        //         sb2.append(" , From ");
        //         sb2.append(currentProfile.getDriverEmail());
        //     }
        //     jSONObject.put(SUMMARY, sb2.toString());
        //     if (!currentProfile.isDefaultProfile()) {
        //         String driverName = currentProfile.getDriverName();
        //         if (!TextUtils.isEmpty(driverName)) {
        //             String trim = driverName.split(" ")[0].trim();
        //             Logger logger2 = sLogger;
        //             StringBuilder sb4 = new StringBuilder();
        //             sb4.append("Assignee name for the snapshot ");
        //             sb4.append(trim);
        //             logger2.d(sb4.toString());
        //             jSONObject.put("assignee", trim);
        //         }
        //         String driverEmail = currentProfile.getDriverEmail();
        //         if (!TextUtils.isEmpty(driverEmail)) {
        //             jSONObject.put(ASSIGNEE_EMAIL, driverEmail);
        //         }
        //     }
        //     StringBuilder sb5 = new StringBuilder();
        //     StringBuilder sb6 = new StringBuilder();
        //     sb6.append("HUD ");
        //     sb6.append(VERSION.INCREMENTAL);
        //     sb6.append(GlanceConstants.NEWLINE);
        //     sb5.append(sb6.toString());
        //     if (connectionHandler != null) {
        //         DeviceInfo lastConnectedDeviceInfo = connectionHandler.getLastConnectedDeviceInfo();
        //         if (lastConnectedDeviceInfo != null) {
        //             sb5.append(Platform.PLATFORM_iOS.equals(lastConnectedDeviceInfo.platform) ? "iPhone" : AnalyticsSupport.ATTR_PLATFORM_ANDROID);
        //             StringBuilder sb7 = new StringBuilder();
        //             sb7.append(" V");
        //             sb7.append(lastConnectedDeviceInfo.clientVersion);
        //             sb7.append(GlanceConstants.NEWLINE);
        //             sb5.append(sb7.toString());
        //         }
        //     }
        //     StringBuilder sb8 = new StringBuilder();
        //     sb8.append("Language : ");
        //     sb8.append(HudLocale.getCurrentLocale(HudApplication.getAppContext()).getLanguage());
        //     sb5.append(sb8.toString());
        //     jSONObject.put(ENVIRONMENT, sb5.toString());
        //     saveJiraTicketDescriptionToFile(jSONObject.toString(), true);
        // } catch (JSONException e) {
        //     e.printStackTrace();
        // }
    }

    @DexIgnore
    public static void submitJiraTicketAsync(String str) {
        Intent intent = new Intent(HudApplication.getAppContext(), ReportIssueService.class);
        intent.setAction(ACTION_SUBMIT_JIRA);
        intent.putExtra(EXTRA_SNAPSHOT_TITLE, str);
        HudApplication.getAppContext().startService(intent);
    }

    @DexIgnore
    private void sync() {
        // if (NetworkBandwidthController.getInstance().isNetworkAccessAllowed(Component.JIRA)) {
        //     boolean isRemoteDeviceConnected = RemoteDeviceManager.getInstance().isRemoteDeviceConnected();
        //     boolean isConnectedToNetwork = NetworkStateManager.isConnectedToNetwork(this);
        //     Logger logger = sLogger;
        //     StringBuilder sb = new StringBuilder();
        //     sb.append("Trying to sync , already syncing:");
        //     sb.append(mSyncing.get());
        //     sb.append(", Navigation mode on:");
        //     sb.append(HereMapsManager.getInstance().isNavigationModeOn());
        //     sb.append(", Device connected:");
        //     sb.append(isRemoteDeviceConnected);
        //     sb.append(", Internet connected :");
        //     sb.append(isConnectedToNetwork);
        //     logger.d(sb.toString());
        //     if (!isRemoteDeviceConnected || !isConnectedToNetwork) {
        //         sLogger.d("Already sync is running, scheduling for a later time");
        //     } else if (mSyncing.compareAndSet(false, true)) {
        //         File file = (File) mIssuesToSync.peek();
        //         if (file == null || !file.exists()) {
        //             sLogger.d("Ticket file does not exist anymore");
        //         } else if (file.getName().endsWith(".json")) {
        //             sLogger.d("Submitting ticket from JSON file");
        //             JSONObject jSONObject = new JSONObject(IOUtils.convertFileToString(file.getAbsolutePath()));
        //             String string = jSONObject.getString(SUMMARY);
        //             String string2 = jSONObject.has(ENVIRONMENT) ? jSONObject.getString(ENVIRONMENT) : "";
        //             String string3 = jSONObject.has("assignee") ? jSONObject.getString("assignee") : "";
        //             String string4 = jSONObject.has(ASSIGNEE_EMAIL) ? jSONObject.getString(ASSIGNEE_EMAIL) : "";
        //             try {
        //                 if (TextUtils.isEmpty(string3)) {
        //                     DriverProfile currentProfile = DriverProfileHelper.getInstance().getCurrentProfile();
        //                     String driverName = currentProfile.getDriverName();
        //                     if (!TextUtils.isEmpty(driverName)) {
        //                         string3 = driverName.trim().split(" ")[0];
        //                     }
        //                     string4 = currentProfile.getDriverEmail();
        //                 }
        //                 String str = string3;
        //                 String str2 = string4;
        //                 Logger logger2 = sLogger;
        //                 StringBuilder sb2 = new StringBuilder();
        //                 sb2.append("Assignee name ");
        //                 sb2.append(str);
        //                 logger2.d(sb2.toString());
        //                 String string5 = jSONObject.has(ATTACHMENT) ? jSONObject.getString(ATTACHMENT) : "";
        //                 Logger logger3 = sLogger;
        //                 StringBuilder sb3 = new StringBuilder();
        //                 sb3.append("Attachment ");
        //                 sb3.append(string5);
        //                 logger3.d(sb3.toString());
        //                 File file2 = new File(string5);
        //                 if (!file2.exists()) {
        //                     sLogger.e("Snapshot file has been deleted, not creating a ticket ");
        //                     onSyncComplete(file);
        //                     return;
        //                 }
        //                 String string6 = jSONObject.has(TICKET_ID) ? jSONObject.getString(TICKET_ID) : "";
        //                 if (!TextUtils.isEmpty(string6)) {
        //                     Logger logger4 = sLogger;
        //                     StringBuilder sb4 = new StringBuilder();
        //                     sb4.append("Ticket already exists ");
        //                     sb4.append(string6);
        //                     sb4.append(" Attaching the files to the ticket");
        //                     logger4.d(sb4.toString());
        //                     attachFilesToTheJiraTicket(string6, file2, new AnonymousClass1(file));
        //                     return;
        //                 }
        //                 sLogger.d("Submitting a new ticket from JSON file");
        //                 JiraClient jiraClient = this.mJiraClient;
        //                 AnonymousClass2 r3 = new AnonymousClass2(jSONObject, file, file2, str, str2);
        //                 jiraClient.submitTicket(HUD_PROJECT, ISSUE_TYPE, string, "Snapshot, fill in the details", string2, r3);
        //                 return;
        //             } catch (IOException | JSONException e) {
        //                 sLogger.e("Not able to read json file ", e);
        //                 syncLater();
        //                 return;
        //             }
        //         } else {
        //             FileReader fileReader = null;
        //             try {
        //                 FileReader fileReader2 = new FileReader(file);
        //                 try {
        //                     BufferedReader bufferedReader = new BufferedReader(fileReader2);
        //                     IssueType issueTypeForId = getIssueTypeForId(Integer.parseInt(bufferedReader.readLine().trim()));
        //                     if (issueTypeForId == null) {
        //                         throw new IOException("Bad file format");
        //                     }
        //                     String summary = getSummary(issueTypeForId);
        //                     StringBuilder sb5 = new StringBuilder();
        //                     while (true) {
        //                         String readLine = bufferedReader.readLine();
        //                         if (readLine != null) {
        //                             sb5.append(readLine);
        //                             sb5.append(GlanceConstants.NEWLINE);
        //                         } else {
        //                             IOUtils.closeStream(fileReader2);
        //                             this.mJiraClient.submitTicket(PROJECT_NAME, ISSUE_TYPE, summary, sb5.toString(), new AnonymousClass3(file));
        //                             return;
        //                         }
        //                     }
        //                 } catch (Exception e2) {
        //                     fileReader = fileReader2;
        //                     IOUtils.closeStream(fileReader);
        //                     onSyncComplete(file);
        //                     return;
        //                 }
        //             } catch (Exception e3) {
        //                 IOUtils.closeStream(fileReader);
        //                 onSyncComplete(file);
        //                 return;
        //             }
        //         }
        //         onSyncComplete(file);
        //         return;
        //     } else {
        //         return;
        //     }
        // }
        // scheduleSync();
    }

    /* access modifiers changed from: private */
    @DexIgnore
    public void syncLater() {
        mSyncing.set(false);
        scheduleSync();
    }

    @DexIgnore
    public void onCreate() {
        super.onCreate();
        Mortar.inject(HudApplication.getAppContext(), this);
        sLogger.d("ReportIssue service started");
        this.mJiraClient = new JiraClient(this.mHttpManager.getClientCopy().readTimeout(120, TimeUnit.SECONDS).connectTimeout(120, TimeUnit.SECONDS).build());
        this.mJiraClient.setEncodedCredentials(CredentialUtil.getCredentials(this, JIRA_CREDENTIALS_META));
    }

    @DexReplace
    protected void onHandleIntent(Intent intent) {
        try {
            Logger logger = sLogger;
            StringBuilder sb = new StringBuilder();
            sb.append("onHandleIntent ");
            sb.append(intent.toString());
            logger.d(sb.toString());
            if (ACTION_DUMP.equals(intent.getAction())) {
                IssueType issueType = (IssueType) intent.getSerializableExtra(EXTRA_ISSUE_TYPE);
                Logger logger2 = sLogger;
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Dumping an issue ");
                sb2.append(issueType);
                logger2.d(sb2.toString());
                if (issueType != null) {
                    dumpIssue(issueType);
                }
            } else if (ACTION_SNAPSHOT.equals(intent.getAction())) {
                // dumpSnapshot();
                generateReport();
                showSnapshotMenu();
            } else if (ACTION_SUBMIT_JIRA.equals(intent.getAction())) {
                String stringExtra = intent.getStringExtra(EXTRA_SNAPSHOT_TITLE);
                if (TextUtils.isEmpty(stringExtra)) {
                    stringExtra = HUD_PROJECT;
                }
                submitJiraTicket(stringExtra);
            } else {
                sLogger.d("Handling Sync request");
                sync();
            }
        } catch (Throwable th) {
            sLogger.e("Exception while handling intent ", th);
        }
    }
}
