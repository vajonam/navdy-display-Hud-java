package com.navdy.hud.app.common;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.navdy.hud.app.R;
import com.navdy.hud.app.analytics.TelemetryDataManager;
import com.navdy.hud.app.ancs.AncsServiceConnector;
import com.navdy.hud.app.config.SettingsManager;
import com.navdy.hud.app.debug.DriveRecorder;
import com.navdy.hud.app.device.PowerManager;
import com.navdy.hud.app.device.dial.DialSimulatorMessagesHandler;
import com.navdy.hud.app.framework.phonecall.CallManager;
import com.navdy.hud.app.framework.trips.TripManager;
import com.navdy.hud.app.framework.voice.VoiceSearchHandler;
import com.navdy.hud.app.gesture.GestureServiceConnector;
import com.navdy.hud.app.manager.InputManager;
import com.navdy.hud.app.manager.MusicManager;
import com.navdy.hud.app.manager.PairingManager;
import com.navdy.hud.app.profile.DriverProfileManager;
import com.navdy.hud.app.service.ConnectionHandler;
import com.navdy.hud.app.service.ConnectionServiceProxy;
import com.navdy.hud.app.service.pandora.PandoraManager;
import com.navdy.hud.app.storage.PathManager;
import com.navdy.hud.app.storage.cache.DiskLruCacheAdapter;
import com.navdy.hud.app.storage.cache.MessageCache;
import com.navdy.hud.app.ui.framework.UIStateManager;
import com.navdy.hud.app.util.FeatureUtil;
import com.navdy.hud.app.util.MusicArtworkCache;
import com.navdy.service.library.events.audio.MusicCollectionResponse;
import com.navdy.service.library.network.http.HttpManager;
import com.navdy.service.library.network.http.IHttpManager;
import com.squareup.otto.Bus;

import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;

@DexEdit
// @dagger.Module(injects = {com.navdy.hud.app.service.ConnectionServiceProxy.class, com.navdy.hud.app.maps.here.HereMapsManager.class, com.navdy.hud.app.maps.here.HereRegionManager.class, com.navdy.hud.app.obd.ObdManager.class, com.navdy.hud.app.util.OTAUpdateService.class, com.navdy.hud.app.maps.MapsEventHandler.class, com.navdy.hud.app.manager.RemoteDeviceManager.class, com.navdy.hud.app.framework.DriverProfileHelper.class, com.navdy.hud.app.framework.notifications.NotificationManager.class, com.navdy.hud.app.framework.trips.TripManager.class, com.navdy.hud.app.ui.activity.Main.Presenter.DeferredServices.class, com.navdy.hud.app.framework.DriverProfileHelper.class, com.navdy.hud.app.debug.DebugReceiver.class, com.navdy.hud.app.framework.network.NetworkStateManager.class, com.navdy.hud.app.service.ShutdownMonitor.class, com.navdy.hud.app.util.ReportIssueService.class, com.navdy.hud.app.obd.CarMDVinDecoder.class, com.navdy.hud.app.debug.DriveRecorder.class, com.navdy.hud.app.HudApplication.class, com.navdy.hud.app.manager.SpeedManager.class, com.navdy.hud.app.view.MusicWidgetPresenter.class, com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.class, com.navdy.hud.app.manager.UpdateReminderManager.class, com.navdy.hud.app.analytics.AnalyticsSupport.TemperatureReporter.class, com.navdy.hud.app.analytics.AnalyticsSupport.NotificationReceiver.class, com.navdy.hud.app.service.GestureVideosSyncService.class, com.navdy.hud.app.service.ObdCANBusDataUploadService.class, com.navdy.hud.app.view.EngineTemperaturePresenter.class, EngineOilTemperaturePresenter.class, IntakePressurePresenter.class, AmbientTemperaturePresenter.class}, library = true)
public class ProdModule {
    @DexIgnore
    public static /* final */ int MUSIC_DATA_CACHE_SIZE; // = 2000000;
    @DexIgnore
    public static /* final */ String MUSIC_RESPONSE_CACHE; // = "MUSIC_RESPONSE_CACHE";
    @DexIgnore
    public static /* final */ String PREF_NAME; // = "App";
    @DexIgnore
    private Context context;

    @DexIgnore
    public ProdModule(Context context2) {
        this.context = context2;
    }

    /* access modifiers changed from: 0000 */
    // @javax.inject.Singleton
    // @dagger.Provides
    @DexIgnore
    public Bus provideBus() {
        return new MainThreadBus();
    }

    /* access modifiers changed from: 0000 */
    // @javax.inject.Singleton
    // @dagger.Provides
    @DexIgnore
    public SharedPreferences provideSharedPreferences() {
        PreferenceManager.setDefaultValues(this.context, PREF_NAME, 0, R.xml.developer_preferences, true);
        return this.context.getSharedPreferences(PREF_NAME, 0);
    }

    /* access modifiers changed from: 0000 */
    // @javax.inject.Singleton
    // @dagger.Provides
    @DexIgnore
    public ConnectionServiceProxy provideConnectionServiceProxy(Bus bus) {
        return new ConnectionServiceProxy(this.context, bus);
    }

    /* access modifiers changed from: 0000 */
    // @javax.inject.Singleton
    // @dagger.Provides
    @DexIgnore
    public TimeHelper provideTimeHelper(Bus bus) {
        return new TimeHelper(this.context, bus);
    }

    /* access modifiers changed from: 0000 */
    // @javax.inject.Singleton
    // @dagger.Provides
    @DexIgnore
    public PowerManager providePowerManager(Bus bus, SharedPreferences preferences) {
        return new PowerManager(bus, this.context, preferences);
    }

    /* access modifiers changed from: 0000 */
    // @javax.inject.Singleton
    // @dagger.Provides
    @DexIgnore
    public SettingsManager provideSettingsManager(Bus bus, SharedPreferences preferences) {
        return new SettingsManager(bus, preferences);
    }

    /* access modifiers changed from: 0000 */
    // @javax.inject.Singleton
    // @dagger.Provides
    @DexIgnore
    public AncsServiceConnector provideAncsServiceConnector(Bus bus) {
        return new AncsServiceConnector(this.context, bus);
    }

    /* access modifiers changed from: 0000 */
    // @javax.inject.Singleton
    // @dagger.Provides
    @DexIgnore
    public InputManager provideInputManager(Bus bus, PowerManager powerManager, UIStateManager uiStateManager) {
        return new InputManager(bus, powerManager, uiStateManager);
    }

    /* access modifiers changed from: 0000 */
    // @javax.inject.Singleton
    // @dagger.Provides
    @DexIgnore
    public DialSimulatorMessagesHandler provideDialSimulatorMessagesHandler() {
        return new DialSimulatorMessagesHandler(this.context);
    }

    /* access modifiers changed from: 0000 */
    // @javax.inject.Singleton
    // @dagger.Provides
    @DexIgnore
    public CallManager provideCallManager(Bus bus) {
        return new CallManager(bus, this.context);
    }

    /* access modifiers changed from: 0000 */
    // @javax.inject.Singleton
    // @dagger.Provides
    @DexIgnore
    public PandoraManager providePandoraManager(Bus bus) {
        return new PandoraManager(bus, this.context.getResources());
    }

    /* access modifiers changed from: 0000 */
    // @javax.inject.Singleton
    // @dagger.Provides
    @DexIgnore
    public MusicManager provideMusicManager(MessageCache<MusicCollectionResponse> musicCollectionResponseMessageCache, Bus bus, UIStateManager uiStateManager, PandoraManager pandoraManager, MusicArtworkCache artworkCache) {
        throw null;
        // return new com.navdy.hud.app.manager.MusicManager(musicCollectionResponseMessageCache, bus, this.context.getResources(), uiStateManager, pandoraManager, artworkCache);
    }

    /* access modifiers changed from: 0000 */
    // @javax.inject.Singleton
    // @dagger.Provides
    @DexIgnore
    public ConnectionHandler provideConnectionHandler(ConnectionServiceProxy connectionServiceProxy, PowerManager powerManager, DriverProfileManager driverProfileManager, UIStateManager uiStateManager, TimeHelper timeHelper) {
        throw null;
        // return new com.navdy.hud.app.service.ConnectionHandler(this.context, connectionServiceProxy, powerManager, driverProfileManager, uiStateManager, timeHelper);
    }

    /* access modifiers changed from: 0000 */
    // @javax.inject.Singleton
    // @dagger.Provides
    @DexIgnore
    public GestureServiceConnector provideGestureServiceConnector(Bus bus, PowerManager powerManager) {
        return new GestureServiceConnector(bus, powerManager);
    }

    /* access modifiers changed from: 0000 */
    // @javax.inject.Singleton
    // @dagger.Provides
    @DexIgnore
    public UIStateManager provideUIStateManager() {
        return new UIStateManager();
    }

    /* access modifiers changed from: 0000 */
    // @javax.inject.Singleton
    // @dagger.Provides
    @DexIgnore
    public PathManager providePathManager() {
        return PathManager.getInstance();
    }

    /* access modifiers changed from: 0000 */
    // @javax.inject.Singleton
    // @dagger.Provides
    @DexIgnore
    public DriverProfileManager provideDriverProfileManager(Bus bus, PathManager pathManager, TimeHelper timeHelper) {
        return new DriverProfileManager(bus, pathManager, timeHelper);
    }

    /* access modifiers changed from: 0000 */
    // @javax.inject.Singleton
    // @dagger.Provides
    @DexIgnore
    public TripManager provideTripManager(Bus bus, SharedPreferences preferences) {
        return new TripManager(bus, preferences);
    }

    /* access modifiers changed from: 0000 */
    // @javax.inject.Singleton
    // @dagger.Provides
    @DexIgnore
    public PairingManager providePairingManager() {
        return new PairingManager();
    }

    /* access modifiers changed from: 0000 */
    // @javax.inject.Singleton
    // @dagger.Provides
    @DexIgnore
    public IHttpManager provideHttpManager() {
        return new HttpManager();
    }

    /* access modifiers changed from: 0000 */
    // @javax.inject.Singleton
    // @dagger.Provides
    @DexIgnore
    public DriveRecorder provideObdDataReceorder(Bus bus, ConnectionHandler connectionHandler) {
        return new DriveRecorder(bus, connectionHandler);
    }

    /* access modifiers changed from: 0000 */
    // @javax.inject.Singleton
    // @dagger.Provides
    @DexIgnore
    public MusicArtworkCache provideArtworkCache() {
        return new MusicArtworkCache();
    }

    /* access modifiers changed from: 0000 */
    // @javax.inject.Singleton
    // @dagger.Provides
    @DexIgnore
    public VoiceSearchHandler provideVoiceSearchHandler(Bus bus, FeatureUtil featureUtil) {
        return new VoiceSearchHandler(this.context, bus, featureUtil);
    }

    /* access modifiers changed from: 0000 */
    // @javax.inject.Singleton
    // @dagger.Provides
    @DexIgnore
    public FeatureUtil provideFeatureUtil(SharedPreferences sharedPreferences) {
        return new FeatureUtil(sharedPreferences);
    }

    /* access modifiers changed from: 0000 */
    // @javax.inject.Singleton
    // @dagger.Provides
    @DexIgnore
    public TelemetryDataManager provideTelemetryDataManager(UIStateManager uiStateManager, PowerManager powerManager, Bus bus, SharedPreferences sharedPreferences, TripManager tripManager) {
        return new TelemetryDataManager(uiStateManager, powerManager, bus, sharedPreferences, tripManager);
    }

    /* access modifiers changed from: 0000 */
    // @javax.inject.Singleton
    // @dagger.Provides
    @DexIgnore
    public MessageCache<MusicCollectionResponse> provideMusicCollectionResponseMessageCache(PathManager pathManager) {
        return new MessageCache<>(new DiskLruCacheAdapter(MUSIC_RESPONSE_CACHE, pathManager.getMusicDiskCacheFolder(), MUSIC_DATA_CACHE_SIZE), MusicCollectionResponse.class);
    }
}
