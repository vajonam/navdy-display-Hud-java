package com.navdy.hud.app.common;

import android.content.SharedPreferences;

import com.navdy.hud.app.analytics.TelemetryDataManager;
import com.navdy.hud.app.ancs.AncsServiceConnector;
import com.navdy.hud.app.config.SettingsManager;
import com.navdy.hud.app.debug.DriveRecorder;
import com.navdy.hud.app.device.PowerManager;
import com.navdy.hud.app.device.dial.DialSimulatorMessagesHandler;
import com.navdy.hud.app.framework.phonecall.CallManager;
import com.navdy.hud.app.framework.trips.TripManager;
import com.navdy.hud.app.framework.voice.VoiceSearchHandler;
import com.navdy.hud.app.gesture.GestureServiceConnector;
import com.navdy.hud.app.manager.InputManager;
import com.navdy.hud.app.manager.MusicManager;
import com.navdy.hud.app.manager.PairingManager;
import com.navdy.hud.app.profile.DriverProfileManager;
import com.navdy.hud.app.service.ConnectionHandler;
import com.navdy.hud.app.service.ConnectionServiceProxy;
import com.navdy.hud.app.service.pandora.PandoraManager;
import com.navdy.hud.app.storage.PathManager;
import com.navdy.hud.app.storage.cache.MessageCache;
import com.navdy.hud.app.ui.framework.UIStateManager;
import com.navdy.hud.app.util.FeatureUtil;
import com.navdy.hud.app.util.MusicArtworkCache;
import com.navdy.service.library.events.audio.MusicCollectionResponse;
import com.navdy.service.library.network.http.IHttpManager;
import com.squareup.otto.Bus;

import java.util.Set;

import javax.inject.Provider;

import dagger.internal.Binding;
import dagger.internal.BindingsGroup;
import dagger.internal.Linker;
import dagger.internal.ModuleAdapter;
import dagger.internal.ProvidesBinding;
import lanchon.dexpatcher.annotation.DexAction;
import lanchon.dexpatcher.annotation.DexAdd;
import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit(staticConstructorAction = DexAction.REPLACE)
public final class ProdModule$$ModuleAdapter extends ModuleAdapter<ProdModule> {
    @DexIgnore
    private static final Class<?>[] INCLUDES = new java.lang.Class[0];
    @DexEdit
    private static final String[] INJECTS = {
            "members/com.navdy.hud.app.service.ConnectionServiceProxy",
            "members/com.navdy.hud.app.maps.here.HereMapsManager",
            "members/com.navdy.hud.app.maps.here.HereRegionManager",
            "members/com.navdy.hud.app.obd.ObdManager",
            "members/com.navdy.hud.app.util.OTAUpdateService",
            "members/com.navdy.hud.app.maps.MapsEventHandler",
            "members/com.navdy.hud.app.manager.RemoteDeviceManager",
            "members/com.navdy.hud.app.framework.DriverProfileHelper",
            "members/com.navdy.hud.app.framework.notifications.NotificationManager",
            "members/com.navdy.hud.app.framework.trips.TripManager",
            "members/com.navdy.hud.app.ui.activity.Main$Presenter$DeferredServices",
            "members/com.navdy.hud.app.framework.DriverProfileHelper",
            "members/com.navdy.hud.app.debug.DebugReceiver",
            "members/com.navdy.hud.app.framework.network.NetworkStateManager",
            "members/com.navdy.hud.app.service.ShutdownMonitor",
            "members/com.navdy.hud.app.util.ReportIssueService",
            "members/com.navdy.hud.app.obd.CarMDVinDecoder",
            "members/com.navdy.hud.app.debug.DriveRecorder",
            "members/com.navdy.hud.app.HudApplication",
            "members/com.navdy.hud.app.manager.SpeedManager",
            "members/com.navdy.hud.app.view.MusicWidgetPresenter",
            "members/com.navdy.hud.app.ui.component.mainmenu.MusicMenu2",
            "members/com.navdy.hud.app.manager.UpdateReminderManager",
            "members/com.navdy.hud.app.analytics.AnalyticsSupport$TemperatureReporter",
            "members/com.navdy.hud.app.analytics.AnalyticsSupport$NotificationReceiver",
            "members/com.navdy.hud.app.service.GestureVideosSyncService",
            "members/com.navdy.hud.app.service.ObdCANBusDataUploadService",
            "members/com.navdy.hud.app.view.EngineTemperaturePresenter",
            "members/com.navdy.hud.app.view.EngineOilTemperaturePresenter",
            "members/com.navdy.hud.app.view.IntakePressurePresenter",
            "members/com.navdy.hud.app.view.AmbientTemperaturePresenter"
    };
    @DexIgnore
    private static final Class<?>[] STATIC_INJECTIONS = new java.lang.Class[0];

    /* compiled from: ProdModule$$ModuleAdapter */
    @DexIgnore
    public static final class ProvideAncsServiceConnectorProvidesAdapter extends ProvidesBinding<AncsServiceConnector> implements Provider<AncsServiceConnector> {
        @DexIgnore
        private Binding<Bus> bus;
        @DexIgnore
        private final ProdModule module;

        @DexIgnore
        public ProvideAncsServiceConnectorProvidesAdapter(ProdModule module2) {
            super("com.navdy.hud.app.ancs.AncsServiceConnector", true, "com.navdy.hud.app.common.ProdModule", "provideAncsServiceConnector");
            this.module = module2;
            setLibrary(true);
        }

        @DexIgnore
        public void attach(Linker linker) {
            this.bus = (Binding<Bus>)linker.requestBinding("com.squareup.otto.Bus", ProdModule.class, getClass().getClassLoader());
        }

        @DexIgnore
        public void getDependencies(Set<Binding<?>> getBindings, Set<Binding<?>> set) {
            getBindings.add(this.bus);
        }

        @DexIgnore
        public AncsServiceConnector get() {
            return this.module.provideAncsServiceConnector(this.bus.get());
        }
    }

    /* compiled from: ProdModule$$ModuleAdapter */
    @DexIgnore
    public static final class ProvideArtworkCacheProvidesAdapter extends ProvidesBinding<MusicArtworkCache> implements Provider<MusicArtworkCache> {
        @DexIgnore
        private final ProdModule module;

        @DexIgnore
        public ProvideArtworkCacheProvidesAdapter(ProdModule module2) {
            super("com.navdy.hud.app.util.MusicArtworkCache", true, "com.navdy.hud.app.common.ProdModule", "provideArtworkCache");
            this.module = module2;
            setLibrary(true);
        }

        @DexIgnore
        public MusicArtworkCache get() {
            return this.module.provideArtworkCache();
        }
    }

    /* compiled from: ProdModule$$ModuleAdapter */
    @DexIgnore
    public static final class ProvideBusProvidesAdapter extends ProvidesBinding<Bus> implements Provider<Bus> {
        @DexIgnore
        private final ProdModule module;

        @DexIgnore
        public ProvideBusProvidesAdapter(ProdModule module2) {
            super("com.squareup.otto.Bus", true, "com.navdy.hud.app.common.ProdModule", "provideBus");
            this.module = module2;
            setLibrary(true);
        }

        @DexIgnore
        public Bus get() {
            return this.module.provideBus();
        }
    }

    /* compiled from: ProdModule$$ModuleAdapter */
    @DexIgnore
    public static final class ProvideCallManagerProvidesAdapter extends ProvidesBinding<CallManager> implements Provider<CallManager> {
        @DexIgnore
        private Binding<Bus> bus;
        @DexIgnore
        private final ProdModule module;

        @DexIgnore
        public ProvideCallManagerProvidesAdapter(ProdModule module2) {
            super("com.navdy.hud.app.framework.phonecall.CallManager", true, "com.navdy.hud.app.common.ProdModule", "provideCallManager");
            this.module = module2;
            setLibrary(true);
        }

        @DexIgnore
        public void attach(Linker linker) {
            this.bus = (Binding<Bus>)linker.requestBinding("com.squareup.otto.Bus", ProdModule.class, getClass().getClassLoader());
        }

        @DexIgnore
        public void getDependencies(Set<Binding<?>> getBindings, Set<Binding<?>> set) {
            getBindings.add(this.bus);
        }

        @DexIgnore
        public CallManager get() {
            return this.module.provideCallManager(this.bus.get());
        }
    }

    /* compiled from: ProdModule$$ModuleAdapter */
    @DexIgnore
    public static final class ProvideConnectionHandlerProvidesAdapter extends ProvidesBinding<ConnectionHandler> implements Provider<ConnectionHandler> {
        @DexIgnore
        private Binding<ConnectionServiceProxy> connectionServiceProxy;
        @DexIgnore
        private Binding<DriverProfileManager> driverProfileManager;
        @DexIgnore
        private final ProdModule module;
        @DexIgnore
        private Binding<PowerManager> powerManager;
        @DexIgnore
        private Binding<TimeHelper> timeHelper;
        @DexIgnore
        private Binding<UIStateManager> uiStateManager;

        @DexIgnore
        public ProvideConnectionHandlerProvidesAdapter(ProdModule module2) {
            super("com.navdy.hud.app.service.ConnectionHandler", true, "com.navdy.hud.app.common.ProdModule", "provideConnectionHandler");
            this.module = module2;
            setLibrary(true);
        }

        @DexIgnore
        public void attach(Linker linker) {
            this.connectionServiceProxy = (Binding<ConnectionServiceProxy>)linker.requestBinding("com.navdy.hud.app.service.ConnectionServiceProxy", ProdModule.class, getClass().getClassLoader());
            this.powerManager = (Binding<PowerManager>)linker.requestBinding("com.navdy.hud.app.device.PowerManager", ProdModule.class, getClass().getClassLoader());
            this.driverProfileManager = (Binding<DriverProfileManager>)linker.requestBinding("com.navdy.hud.app.profile.DriverProfileManager", ProdModule.class, getClass().getClassLoader());
            this.uiStateManager = (Binding<UIStateManager>)linker.requestBinding("com.navdy.hud.app.ui.framework.UIStateManager", ProdModule.class, getClass().getClassLoader());
            this.timeHelper = (Binding<TimeHelper>)linker.requestBinding("com.navdy.hud.app.common.TimeHelper", ProdModule.class, getClass().getClassLoader());
        }

        @DexIgnore
        public void getDependencies(Set<Binding<?>> getBindings, Set<Binding<?>> set) {
            getBindings.add(this.connectionServiceProxy);
            getBindings.add(this.powerManager);
            getBindings.add(this.driverProfileManager);
            getBindings.add(this.uiStateManager);
            getBindings.add(this.timeHelper);
        }

        @DexIgnore
        public ConnectionHandler get() {
            return this.module.provideConnectionHandler(this.connectionServiceProxy.get(), this.powerManager.get(), this.driverProfileManager.get(), this.uiStateManager.get(), this.timeHelper.get());
        }
    }

    /* compiled from: ProdModule$$ModuleAdapter */
    @DexIgnore
    public static final class ProvideConnectionServiceProxyProvidesAdapter extends ProvidesBinding<ConnectionServiceProxy> implements Provider<ConnectionServiceProxy> {
        @DexIgnore
        private Binding<Bus> bus;
        @DexIgnore
        private final ProdModule module;

        @DexIgnore
        public ProvideConnectionServiceProxyProvidesAdapter(ProdModule module2) {
            super("com.navdy.hud.app.service.ConnectionServiceProxy", true, "com.navdy.hud.app.common.ProdModule", "provideConnectionServiceProxy");
            this.module = module2;
            setLibrary(true);
        }

        @DexIgnore
        public void attach(Linker linker) {
            this.bus = (Binding<Bus>)linker.requestBinding("com.squareup.otto.Bus", ProdModule.class, getClass().getClassLoader());
        }

        @DexIgnore
        public void getDependencies(Set<Binding<?>> getBindings, Set<Binding<?>> set) {
            getBindings.add(this.bus);
        }

        @DexIgnore
        public ConnectionServiceProxy get() {
            return this.module.provideConnectionServiceProxy(this.bus.get());
        }
    }

    /* compiled from: ProdModule$$ModuleAdapter */
    @DexIgnore
    public static final class ProvideDialSimulatorMessagesHandlerProvidesAdapter extends ProvidesBinding<DialSimulatorMessagesHandler> implements Provider<DialSimulatorMessagesHandler> {
        @DexIgnore
        private final ProdModule module;

        @DexIgnore
        public ProvideDialSimulatorMessagesHandlerProvidesAdapter(ProdModule module2) {
            super("com.navdy.hud.app.device.dial.DialSimulatorMessagesHandler", true, "com.navdy.hud.app.common.ProdModule", "provideDialSimulatorMessagesHandler");
            this.module = module2;
            setLibrary(true);
        }

        @DexIgnore
        public DialSimulatorMessagesHandler get() {
            return this.module.provideDialSimulatorMessagesHandler();
        }
    }

    /* compiled from: ProdModule$$ModuleAdapter */
    @DexIgnore
    public static final class ProvideDriverProfileManagerProvidesAdapter extends ProvidesBinding<DriverProfileManager> implements Provider<DriverProfileManager> {
        @DexIgnore
        private Binding<Bus> bus;
        @DexIgnore
        private final ProdModule module;
        @DexIgnore
        private Binding<PathManager> pathManager;
        @DexIgnore
        private Binding<TimeHelper> timeHelper;

        @DexIgnore
        public ProvideDriverProfileManagerProvidesAdapter(ProdModule module2) {
            super("com.navdy.hud.app.profile.DriverProfileManager", true, "com.navdy.hud.app.common.ProdModule", "provideDriverProfileManager");
            this.module = module2;
            setLibrary(true);
        }

        @DexIgnore
        public void attach(Linker linker) {
            this.bus = (Binding<Bus>)linker.requestBinding("com.squareup.otto.Bus", ProdModule.class, getClass().getClassLoader());
            this.pathManager = (Binding<PathManager>)linker.requestBinding("com.navdy.hud.app.storage.PathManager", ProdModule.class, getClass().getClassLoader());
            this.timeHelper = (Binding<TimeHelper>)linker.requestBinding("com.navdy.hud.app.common.TimeHelper", ProdModule.class, getClass().getClassLoader());
        }

        @DexIgnore
        public void getDependencies(Set<Binding<?>> getBindings, Set<Binding<?>> set) {
            getBindings.add(this.bus);
            getBindings.add(this.pathManager);
            getBindings.add(this.timeHelper);
        }

        @DexIgnore
        public DriverProfileManager get() {
            return this.module.provideDriverProfileManager(this.bus.get(), this.pathManager.get(), this.timeHelper.get());
        }
    }

    /* compiled from: ProdModule$$ModuleAdapter */
    @DexIgnore
    public static final class ProvideFeatureUtilProvidesAdapter extends ProvidesBinding<FeatureUtil> implements Provider<FeatureUtil> {
        @DexIgnore
        private final ProdModule module;
        @DexIgnore
        private Binding<SharedPreferences> sharedPreferences;

        @DexIgnore
        public ProvideFeatureUtilProvidesAdapter(ProdModule module2) {
            super("com.navdy.hud.app.util.FeatureUtil", true, "com.navdy.hud.app.common.ProdModule", "provideFeatureUtil");
            this.module = module2;
            setLibrary(true);
        }

        @DexIgnore
        public void attach(Linker linker) {
            this.sharedPreferences = (Binding<SharedPreferences>)linker.requestBinding("android.content.SharedPreferences", ProdModule.class, getClass().getClassLoader());
        }

        @DexIgnore
        public void getDependencies(Set<Binding<?>> getBindings, Set<Binding<?>> set) {
            getBindings.add(this.sharedPreferences);
        }

        @DexIgnore
        public FeatureUtil get() {
            return this.module.provideFeatureUtil(this.sharedPreferences.get());
        }
    }

    /* compiled from: ProdModule$$ModuleAdapter */
    @DexIgnore
    public static final class ProvideGestureServiceConnectorProvidesAdapter extends ProvidesBinding<GestureServiceConnector> implements Provider<GestureServiceConnector> {
        @DexIgnore
        private Binding<Bus> bus;
        @DexIgnore
        private final ProdModule module;
        @DexIgnore
        private Binding<PowerManager> powerManager;

        @DexIgnore
        public ProvideGestureServiceConnectorProvidesAdapter(ProdModule module2) {
            super("com.navdy.hud.app.gesture.GestureServiceConnector", true, "com.navdy.hud.app.common.ProdModule", "provideGestureServiceConnector");
            this.module = module2;
            setLibrary(true);
        }

        @DexIgnore
        public void attach(Linker linker) {
            this.bus = (Binding<Bus>)linker.requestBinding("com.squareup.otto.Bus", ProdModule.class, getClass().getClassLoader());
            this.powerManager = (Binding<PowerManager>)linker.requestBinding("com.navdy.hud.app.device.PowerManager", ProdModule.class, getClass().getClassLoader());
        }

        @DexIgnore
        public void getDependencies(Set<Binding<?>> getBindings, Set<Binding<?>> set) {
            getBindings.add(this.bus);
            getBindings.add(this.powerManager);
        }

        @DexIgnore
        public GestureServiceConnector get() {
            return this.module.provideGestureServiceConnector(this.bus.get(), this.powerManager.get());
        }
    }

    /* compiled from: ProdModule$$ModuleAdapter */
    @DexIgnore
    public static final class ProvideHttpManagerProvidesAdapter extends ProvidesBinding<IHttpManager> implements Provider<IHttpManager> {
        @DexIgnore
        private final ProdModule module;

        @DexIgnore
        public ProvideHttpManagerProvidesAdapter(ProdModule module2) {
            super("com.navdy.service.library.network.http.IHttpManager", true, "com.navdy.hud.app.common.ProdModule", "provideHttpManager");
            this.module = module2;
            setLibrary(true);
        }

        @DexIgnore
        public IHttpManager get() {
            return this.module.provideHttpManager();
        }
    }

    /* compiled from: ProdModule$$ModuleAdapter */
    @DexIgnore
    public static final class ProvideInputManagerProvidesAdapter extends ProvidesBinding<InputManager> implements Provider<InputManager> {
        @DexIgnore
        private Binding<Bus> bus;
        @DexIgnore
        private final ProdModule module;
        @DexIgnore
        private Binding<PowerManager> powerManager;
        @DexIgnore
        private Binding<UIStateManager> uiStateManager;

        @DexIgnore
        public ProvideInputManagerProvidesAdapter(ProdModule module2) {
            super("com.navdy.hud.app.manager.InputManager", true, "com.navdy.hud.app.common.ProdModule", "provideInputManager");
            this.module = module2;
            setLibrary(true);
        }

        @DexIgnore
        public void attach(Linker linker) {
            this.bus = (Binding<Bus>)linker.requestBinding("com.squareup.otto.Bus", ProdModule.class, getClass().getClassLoader());
            this.powerManager = (Binding<PowerManager>)linker.requestBinding("com.navdy.hud.app.device.PowerManager", ProdModule.class, getClass().getClassLoader());
            this.uiStateManager = (Binding<UIStateManager>)linker.requestBinding("com.navdy.hud.app.ui.framework.UIStateManager", ProdModule.class, getClass().getClassLoader());
        }

        @DexIgnore
        public void getDependencies(Set<Binding<?>> getBindings, Set<Binding<?>> set) {
            getBindings.add(this.bus);
            getBindings.add(this.powerManager);
            getBindings.add(this.uiStateManager);
        }

        @DexIgnore
        public InputManager get() {
            return this.module.provideInputManager(this.bus.get(), this.powerManager.get(), this.uiStateManager.get());
        }
    }

    /* compiled from: ProdModule$$ModuleAdapter */
    @DexIgnore
    public static final class ProvideMusicCollectionResponseMessageCacheProvidesAdapter extends ProvidesBinding<MessageCache<MusicCollectionResponse>> implements Provider<MessageCache<MusicCollectionResponse>> {
        @DexIgnore
        private final ProdModule module;
        @DexIgnore
        private Binding<PathManager> pathManager;

        @DexIgnore
        public ProvideMusicCollectionResponseMessageCacheProvidesAdapter(ProdModule module2) {
            super("com.navdy.hud.app.storage.cache.MessageCache<com.navdy.service.library.events.audio.MusicCollectionResponse>", true, "com.navdy.hud.app.common.ProdModule", "provideMusicCollectionResponseMessageCache");
            this.module = module2;
            setLibrary(true);
        }

        @DexIgnore
        public void attach(Linker linker) {
            this.pathManager = (Binding<PathManager>)linker.requestBinding("com.navdy.hud.app.storage.PathManager", ProdModule.class, getClass().getClassLoader());
        }

        @DexIgnore
        public void getDependencies(Set<Binding<?>> getBindings, Set<Binding<?>> set) {
            getBindings.add(this.pathManager);
        }

        @DexIgnore
        public MessageCache<MusicCollectionResponse> get() {
            return this.module.provideMusicCollectionResponseMessageCache(this.pathManager.get());
        }
    }

    /* compiled from: ProdModule$$ModuleAdapter */
    @DexIgnore
    public static final class ProvideMusicManagerProvidesAdapter extends ProvidesBinding<MusicManager> implements Provider<MusicManager> {
        @DexIgnore
        private Binding<MusicArtworkCache> artworkCache;
        @DexIgnore
        private Binding<Bus> bus;
        @DexIgnore
        private final ProdModule module;
        @DexIgnore
        private Binding<MessageCache<MusicCollectionResponse>> musicCollectionResponseMessageCache;
        @DexIgnore
        private Binding<PandoraManager> pandoraManager;
        @DexIgnore
        private Binding<UIStateManager> uiStateManager;

        @DexIgnore
        public ProvideMusicManagerProvidesAdapter(ProdModule module2) {
            super("com.navdy.hud.app.manager.MusicManager", true, "com.navdy.hud.app.common.ProdModule", "provideMusicManager");
            this.module = module2;
            setLibrary(true);
        }

        @DexIgnore
        public void attach(Linker linker) {
            this.musicCollectionResponseMessageCache = (Binding<MessageCache<MusicCollectionResponse>>)linker.requestBinding("com.navdy.hud.app.storage.cache.MessageCache<com.navdy.service.library.events.audio.MusicCollectionResponse>", ProdModule.class, getClass().getClassLoader());
            this.bus = (Binding<Bus>)linker.requestBinding("com.squareup.otto.Bus", ProdModule.class, getClass().getClassLoader());
            this.uiStateManager = (Binding<UIStateManager>)linker.requestBinding("com.navdy.hud.app.ui.framework.UIStateManager", ProdModule.class, getClass().getClassLoader());
            this.pandoraManager = (Binding<PandoraManager>)linker.requestBinding("com.navdy.hud.app.service.pandora.PandoraManager", ProdModule.class, getClass().getClassLoader());
            this.artworkCache = (Binding<MusicArtworkCache>)linker.requestBinding("com.navdy.hud.app.util.MusicArtworkCache", ProdModule.class, getClass().getClassLoader());
        }

        @DexIgnore
        public void getDependencies(Set<Binding<?>> getBindings, Set<Binding<?>> set) {
            getBindings.add(this.musicCollectionResponseMessageCache);
            getBindings.add(this.bus);
            getBindings.add(this.uiStateManager);
            getBindings.add(this.pandoraManager);
            getBindings.add(this.artworkCache);
        }

        @DexIgnore
        public MusicManager get() {
            return this.module.provideMusicManager(this.musicCollectionResponseMessageCache.get(), this.bus.get(), this.uiStateManager.get(), this.pandoraManager.get(), this.artworkCache.get());
        }
    }

    /* compiled from: ProdModule$$ModuleAdapter */
    @DexIgnore
    public static final class ProvideObdDataReceorderProvidesAdapter extends ProvidesBinding<DriveRecorder> implements Provider<DriveRecorder> {
        @DexIgnore
        private Binding<Bus> bus;
        @DexIgnore
        private Binding<ConnectionHandler> connectionHandler;
        @DexIgnore
        private final ProdModule module;

        @DexIgnore
        public ProvideObdDataReceorderProvidesAdapter(ProdModule module2) {
            super("com.navdy.hud.app.debug.DriveRecorder", true, "com.navdy.hud.app.common.ProdModule", "provideObdDataReceorder");
            this.module = module2;
            setLibrary(true);
        }

        @DexIgnore
        public void attach(Linker linker) {
            this.bus = (Binding<Bus>)linker.requestBinding("com.squareup.otto.Bus", ProdModule.class, getClass().getClassLoader());
            this.connectionHandler = (Binding<ConnectionHandler>)linker.requestBinding("com.navdy.hud.app.service.ConnectionHandler", ProdModule.class, getClass().getClassLoader());
        }

        @DexIgnore
        public void getDependencies(Set<Binding<?>> getBindings, Set<Binding<?>> set) {
            getBindings.add(this.bus);
            getBindings.add(this.connectionHandler);
        }

        @DexIgnore
        public DriveRecorder get() {
            return this.module.provideObdDataReceorder(this.bus.get(), this.connectionHandler.get());
        }
    }

    /* compiled from: ProdModule$$ModuleAdapter */
    @DexIgnore
    public static final class ProvidePairingManagerProvidesAdapter extends ProvidesBinding<PairingManager> implements Provider<PairingManager> {
        @DexIgnore
        private final ProdModule module;

        @DexIgnore
        public ProvidePairingManagerProvidesAdapter(ProdModule module2) {
            super("com.navdy.hud.app.manager.PairingManager", true, "com.navdy.hud.app.common.ProdModule", "providePairingManager");
            this.module = module2;
            setLibrary(true);
        }

        @DexIgnore
        public PairingManager get() {
            return this.module.providePairingManager();
        }
    }

    /* compiled from: ProdModule$$ModuleAdapter */
    @DexIgnore
    public static final class ProvidePandoraManagerProvidesAdapter extends ProvidesBinding<PandoraManager> implements Provider<PandoraManager> {
        @DexIgnore
        private Binding<Bus> bus;
        @DexIgnore
        private final ProdModule module;

        @DexIgnore
        public ProvidePandoraManagerProvidesAdapter(ProdModule module2) {
            super("com.navdy.hud.app.service.pandora.PandoraManager", true, "com.navdy.hud.app.common.ProdModule", "providePandoraManager");
            this.module = module2;
            setLibrary(true);
        }

        @DexIgnore
        public void attach(Linker linker) {
            this.bus = (Binding<Bus>)linker.requestBinding("com.squareup.otto.Bus", ProdModule.class, getClass().getClassLoader());
        }

        @DexIgnore
        public void getDependencies(Set<Binding<?>> getBindings, Set<Binding<?>> set) {
            getBindings.add(this.bus);
        }

        @DexIgnore
        public PandoraManager get() {
            return this.module.providePandoraManager(this.bus.get());
        }
    }

    /* compiled from: ProdModule$$ModuleAdapter */
    @DexIgnore
    public static final class ProvidePathManagerProvidesAdapter extends ProvidesBinding<PathManager> implements Provider<PathManager> {
        @DexIgnore
        private final ProdModule module;

        @DexIgnore
        public ProvidePathManagerProvidesAdapter(ProdModule module2) {
            super("com.navdy.hud.app.storage.PathManager", true, "com.navdy.hud.app.common.ProdModule", "providePathManager");
            this.module = module2;
            setLibrary(true);
        }

        @DexIgnore
        public PathManager get() {
            return this.module.providePathManager();
        }
    }

    /* compiled from: ProdModule$$ModuleAdapter */
    @DexIgnore
    public static final class ProvidePowerManagerProvidesAdapter extends ProvidesBinding<PowerManager> implements Provider<PowerManager> {
        @DexIgnore
        private Binding<Bus> bus;
        @DexIgnore
        private final ProdModule module;
        @DexIgnore
        private Binding<SharedPreferences> preferences;

        @DexIgnore
        public ProvidePowerManagerProvidesAdapter(ProdModule module2) {
            super("com.navdy.hud.app.device.PowerManager", true, "com.navdy.hud.app.common.ProdModule", "providePowerManager");
            this.module = module2;
            setLibrary(true);
        }

        @DexIgnore
        public void attach(Linker linker) {
            this.bus = (Binding<Bus>)linker.requestBinding("com.squareup.otto.Bus", ProdModule.class, getClass().getClassLoader());
            this.preferences = (Binding<SharedPreferences>)linker.requestBinding("android.content.SharedPreferences", ProdModule.class, getClass().getClassLoader());
        }

        @DexIgnore
        public void getDependencies(Set<Binding<?>> getBindings, Set<Binding<?>> set) {
            getBindings.add(this.bus);
            getBindings.add(this.preferences);
        }

        @DexIgnore
        public PowerManager get() {
            return this.module.providePowerManager(this.bus.get(), this.preferences.get());
        }
    }

    /* compiled from: ProdModule$$ModuleAdapter */
    @DexIgnore
    public static final class ProvideSettingsManagerProvidesAdapter extends ProvidesBinding<SettingsManager> implements Provider<SettingsManager> {
        @DexIgnore
        private Binding<Bus> bus;
        @DexIgnore
        private final ProdModule module;
        @DexIgnore
        private Binding<SharedPreferences> preferences;

        @DexIgnore
        public ProvideSettingsManagerProvidesAdapter(ProdModule module2) {
            super("com.navdy.hud.app.config.SettingsManager", true, "com.navdy.hud.app.common.ProdModule", "provideSettingsManager");
            this.module = module2;
            setLibrary(true);
        }

        @DexIgnore
        public void attach(Linker linker) {
            this.bus = (Binding<Bus>)linker.requestBinding("com.squareup.otto.Bus", ProdModule.class, getClass().getClassLoader());
            this.preferences = (Binding<SharedPreferences>)linker.requestBinding("android.content.SharedPreferences", ProdModule.class, getClass().getClassLoader());
        }

        @DexIgnore
        public void getDependencies(Set<Binding<?>> getBindings, Set<Binding<?>> set) {
            getBindings.add(this.bus);
            getBindings.add(this.preferences);
        }

        @DexIgnore
        public SettingsManager get() {
            return this.module.provideSettingsManager(this.bus.get(), this.preferences.get());
        }
    }

    /* compiled from: ProdModule$$ModuleAdapter */
    @DexIgnore
    public static final class ProvideSharedPreferencesProvidesAdapter extends ProvidesBinding<SharedPreferences> implements Provider<SharedPreferences> {
        @DexIgnore
        private final ProdModule module;

        @DexIgnore
        public ProvideSharedPreferencesProvidesAdapter(ProdModule module2) {
            super("android.content.SharedPreferences", true, "com.navdy.hud.app.common.ProdModule", "provideSharedPreferences");
            this.module = module2;
            setLibrary(true);
        }

        @DexIgnore
        public SharedPreferences get() {
            return this.module.provideSharedPreferences();
        }
    }

    /* compiled from: ProdModule$$ModuleAdapter */
    @DexIgnore
    public static final class ProvideTelemetryDataManagerProvidesAdapter extends ProvidesBinding<TelemetryDataManager> implements Provider<TelemetryDataManager> {
        @DexIgnore
        private Binding<Bus> bus;
        @DexIgnore
        private final ProdModule module;
        @DexIgnore
        private Binding<PowerManager> powerManager;
        @DexIgnore
        private Binding<SharedPreferences> sharedPreferences;
        @DexIgnore
        private Binding<TripManager> tripManager;
        @DexIgnore
        private Binding<UIStateManager> uiStateManager;

        @DexIgnore
        public ProvideTelemetryDataManagerProvidesAdapter(ProdModule module2) {
            super("com.navdy.hud.app.analytics.TelemetryDataManager", true, "com.navdy.hud.app.common.ProdModule", "provideTelemetryDataManager");
            this.module = module2;
            setLibrary(true);
        }

        @DexIgnore
        public void attach(Linker linker) {
            this.uiStateManager = (Binding<UIStateManager>)linker.requestBinding("com.navdy.hud.app.ui.framework.UIStateManager", ProdModule.class, getClass().getClassLoader());
            this.powerManager = (Binding<PowerManager>)linker.requestBinding("com.navdy.hud.app.device.PowerManager", ProdModule.class, getClass().getClassLoader());
            this.bus = (Binding<Bus>)linker.requestBinding("com.squareup.otto.Bus", ProdModule.class, getClass().getClassLoader());
            this.sharedPreferences = (Binding<SharedPreferences>)linker.requestBinding("android.content.SharedPreferences", ProdModule.class, getClass().getClassLoader());
            this.tripManager = (Binding<TripManager>)linker.requestBinding("com.navdy.hud.app.framework.trips.TripManager", ProdModule.class, getClass().getClassLoader());
        }

        @DexIgnore
        public void getDependencies(Set<Binding<?>> getBindings, Set<Binding<?>> set) {
            getBindings.add(this.uiStateManager);
            getBindings.add(this.powerManager);
            getBindings.add(this.bus);
            getBindings.add(this.sharedPreferences);
            getBindings.add(this.tripManager);
        }

        @DexIgnore
        public TelemetryDataManager get() {
            return this.module.provideTelemetryDataManager(this.uiStateManager.get(), this.powerManager.get(), this.bus.get(), this.sharedPreferences.get(), this.tripManager.get());
        }
    }

    /* compiled from: ProdModule$$ModuleAdapter */
    @DexIgnore
    public static final class ProvideTimeHelperProvidesAdapter extends ProvidesBinding<TimeHelper> implements Provider<TimeHelper> {
        @DexIgnore
        private Binding<Bus> bus;
        @DexIgnore
        private final ProdModule module;

        @DexIgnore
        public ProvideTimeHelperProvidesAdapter(ProdModule module2) {
            super("com.navdy.hud.app.common.TimeHelper", true, "com.navdy.hud.app.common.ProdModule", "provideTimeHelper");
            this.module = module2;
            setLibrary(true);
        }

        @DexIgnore
        public void attach(Linker linker) {
            this.bus = (Binding<Bus>)linker.requestBinding("com.squareup.otto.Bus", ProdModule.class, getClass().getClassLoader());
        }

        @DexIgnore
        public void getDependencies(Set<Binding<?>> getBindings, Set<Binding<?>> set) {
            getBindings.add(this.bus);
        }

        @DexIgnore
        public TimeHelper get() {
            return this.module.provideTimeHelper(this.bus.get());
        }
    }

    /* compiled from: ProdModule$$ModuleAdapter */
    @DexIgnore
    public static final class ProvideTripManagerProvidesAdapter extends ProvidesBinding<TripManager> implements Provider<TripManager> {
        @DexIgnore
        private Binding<Bus> bus;
        @DexIgnore
        private final ProdModule module;
        @DexIgnore
        private Binding<SharedPreferences> preferences;

        @DexIgnore
        public ProvideTripManagerProvidesAdapter(ProdModule module2) {
            super("com.navdy.hud.app.framework.trips.TripManager", true, "com.navdy.hud.app.common.ProdModule", "provideTripManager");
            this.module = module2;
            setLibrary(true);
        }

        @DexIgnore
        public void attach(Linker linker) {
            this.bus = (Binding<Bus>)linker.requestBinding("com.squareup.otto.Bus", ProdModule.class, getClass().getClassLoader());
            this.preferences = (Binding<SharedPreferences>)linker.requestBinding("android.content.SharedPreferences", ProdModule.class, getClass().getClassLoader());
        }

        @DexIgnore
        public void getDependencies(Set<Binding<?>> getBindings, Set<Binding<?>> set) {
            getBindings.add(this.bus);
            getBindings.add(this.preferences);
        }

        @DexIgnore
        public TripManager get() {
            return this.module.provideTripManager(this.bus.get(), this.preferences.get());
        }
    }

    /* compiled from: ProdModule$$ModuleAdapter */
    @DexIgnore
    public static final class ProvideUIStateManagerProvidesAdapter extends ProvidesBinding<UIStateManager> implements Provider<UIStateManager> {
        @DexIgnore
        private final ProdModule module;

        @DexIgnore
        public ProvideUIStateManagerProvidesAdapter(ProdModule module2) {
            super("com.navdy.hud.app.ui.framework.UIStateManager", true, "com.navdy.hud.app.common.ProdModule", "provideUIStateManager");
            this.module = module2;
            setLibrary(true);
        }

        @DexIgnore
        public UIStateManager get() {
            return this.module.provideUIStateManager();
        }
    }

    /* compiled from: ProdModule$$ModuleAdapter */
    @DexIgnore
    public static final class ProvideVoiceSearchHandlerProvidesAdapter extends ProvidesBinding<VoiceSearchHandler> implements Provider<VoiceSearchHandler> {
        @DexIgnore
        private Binding<Bus> bus;
        @DexIgnore
        private Binding<FeatureUtil> featureUtil;
        @DexIgnore
        private final ProdModule module;

        @DexIgnore
        public ProvideVoiceSearchHandlerProvidesAdapter(ProdModule module2) {
            super("com.navdy.hud.app.framework.voice.VoiceSearchHandler", true, "com.navdy.hud.app.common.ProdModule", "provideVoiceSearchHandler");
            this.module = module2;
            setLibrary(true);
        }

        @DexIgnore
        public void attach(Linker linker) {
            this.bus = (Binding<Bus>)linker.requestBinding("com.squareup.otto.Bus", ProdModule.class, getClass().getClassLoader());
            this.featureUtil = (Binding<FeatureUtil>)linker.requestBinding("com.navdy.hud.app.util.FeatureUtil", ProdModule.class, getClass().getClassLoader());
        }

        @DexIgnore
        public void getDependencies(Set<Binding<?>> getBindings, Set<Binding<?>> set) {
            getBindings.add(this.bus);
            getBindings.add(this.featureUtil);
        }

        @DexIgnore
        public VoiceSearchHandler get() {
            return this.module.provideVoiceSearchHandler(this.bus.get(), this.featureUtil.get());
        }
    }

    @DexIgnore
    public ProdModule$$ModuleAdapter() {
        super(ProdModule.class, INJECTS, STATIC_INJECTIONS, false, INCLUDES, true, true);
    }

    @DexIgnore
    public void getBindings(BindingsGroup bindings, ProdModule module) {
        bindings.contributeProvidesBinding("com.squareup.otto.Bus", new ProdModule$$ModuleAdapter.ProvideBusProvidesAdapter(module));
        bindings.contributeProvidesBinding("android.content.SharedPreferences", new ProdModule$$ModuleAdapter.ProvideSharedPreferencesProvidesAdapter(module));
        bindings.contributeProvidesBinding("com.navdy.hud.app.service.ConnectionServiceProxy", new ProdModule$$ModuleAdapter.ProvideConnectionServiceProxyProvidesAdapter(module));
        bindings.contributeProvidesBinding("com.navdy.hud.app.common.TimeHelper", new ProdModule$$ModuleAdapter.ProvideTimeHelperProvidesAdapter(module));
        bindings.contributeProvidesBinding("com.navdy.hud.app.device.PowerManager", new ProdModule$$ModuleAdapter.ProvidePowerManagerProvidesAdapter(module));
        bindings.contributeProvidesBinding("com.navdy.hud.app.config.SettingsManager", new ProdModule$$ModuleAdapter.ProvideSettingsManagerProvidesAdapter(module));
        bindings.contributeProvidesBinding("com.navdy.hud.app.ancs.AncsServiceConnector", new ProdModule$$ModuleAdapter.ProvideAncsServiceConnectorProvidesAdapter(module));
        bindings.contributeProvidesBinding("com.navdy.hud.app.manager.InputManager", new ProdModule$$ModuleAdapter.ProvideInputManagerProvidesAdapter(module));
        bindings.contributeProvidesBinding("com.navdy.hud.app.device.dial.DialSimulatorMessagesHandler", new ProdModule$$ModuleAdapter.ProvideDialSimulatorMessagesHandlerProvidesAdapter(module));
        bindings.contributeProvidesBinding("com.navdy.hud.app.framework.phonecall.CallManager", new ProdModule$$ModuleAdapter.ProvideCallManagerProvidesAdapter(module));
        bindings.contributeProvidesBinding("com.navdy.hud.app.service.pandora.PandoraManager", new ProdModule$$ModuleAdapter.ProvidePandoraManagerProvidesAdapter(module));
        bindings.contributeProvidesBinding("com.navdy.hud.app.manager.MusicManager", new ProdModule$$ModuleAdapter.ProvideMusicManagerProvidesAdapter(module));
        bindings.contributeProvidesBinding("com.navdy.hud.app.service.ConnectionHandler", new ProdModule$$ModuleAdapter.ProvideConnectionHandlerProvidesAdapter(module));
        bindings.contributeProvidesBinding("com.navdy.hud.app.gesture.GestureServiceConnector", new ProdModule$$ModuleAdapter.ProvideGestureServiceConnectorProvidesAdapter(module));
        bindings.contributeProvidesBinding("com.navdy.hud.app.ui.framework.UIStateManager", new ProdModule$$ModuleAdapter.ProvideUIStateManagerProvidesAdapter(module));
        bindings.contributeProvidesBinding("com.navdy.hud.app.storage.PathManager", new ProdModule$$ModuleAdapter.ProvidePathManagerProvidesAdapter(module));
        bindings.contributeProvidesBinding("com.navdy.hud.app.profile.DriverProfileManager", new ProdModule$$ModuleAdapter.ProvideDriverProfileManagerProvidesAdapter(module));
        bindings.contributeProvidesBinding("com.navdy.hud.app.framework.trips.TripManager", new ProdModule$$ModuleAdapter.ProvideTripManagerProvidesAdapter(module));
        bindings.contributeProvidesBinding("com.navdy.hud.app.manager.PairingManager", new ProdModule$$ModuleAdapter.ProvidePairingManagerProvidesAdapter(module));
        bindings.contributeProvidesBinding("com.navdy.service.library.network.http.IHttpManager", new ProdModule$$ModuleAdapter.ProvideHttpManagerProvidesAdapter(module));
        bindings.contributeProvidesBinding("com.navdy.hud.app.debug.DriveRecorder", new ProdModule$$ModuleAdapter.ProvideObdDataReceorderProvidesAdapter(module));
        bindings.contributeProvidesBinding("com.navdy.hud.app.util.MusicArtworkCache", new ProdModule$$ModuleAdapter.ProvideArtworkCacheProvidesAdapter(module));
        bindings.contributeProvidesBinding("com.navdy.hud.app.framework.voice.VoiceSearchHandler", new ProdModule$$ModuleAdapter.ProvideVoiceSearchHandlerProvidesAdapter(module));
        bindings.contributeProvidesBinding("com.navdy.hud.app.util.FeatureUtil", new ProdModule$$ModuleAdapter.ProvideFeatureUtilProvidesAdapter(module));
        bindings.contributeProvidesBinding("com.navdy.hud.app.analytics.TelemetryDataManager", new ProdModule$$ModuleAdapter.ProvideTelemetryDataManagerProvidesAdapter(module));
        bindings.contributeProvidesBinding("com.navdy.hud.app.storage.cache.MessageCache<com.navdy.service.library.events.audio.MusicCollectionResponse>", new ProdModule$$ModuleAdapter.ProvideMusicCollectionResponseMessageCacheProvidesAdapter(module));
    }
}
