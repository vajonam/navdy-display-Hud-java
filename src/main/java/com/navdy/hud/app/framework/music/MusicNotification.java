package com.navdy.hud.app.framework.music;

import android.animation.AnimatorSet;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R.dimen;
import com.navdy.hud.app.R.id;
import com.navdy.hud.app.R.layout;
import com.navdy.hud.app.framework.notifications.INotification;
import com.navdy.hud.app.framework.notifications.INotificationController;
import com.navdy.hud.app.framework.notifications.NotificationId;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import com.navdy.hud.app.framework.notifications.NotificationType;
import com.navdy.hud.app.manager.InputManager;
import com.navdy.hud.app.manager.InputManager.CustomKeyEvent;
import com.navdy.hud.app.manager.InputManager.IInputHandler;
import com.navdy.hud.app.manager.MusicManager;
import com.navdy.hud.app.manager.MusicManager.MediaControl;
import com.navdy.hud.app.manager.MusicManager.MusicUpdateListener;
import com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder;
import com.navdy.hud.app.ui.component.ChoiceLayout.Choice;
import com.navdy.hud.app.ui.component.mainmenu.IMenu.Menu;
import com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2;
import com.navdy.hud.app.ui.framework.UIStateManager.Mode;
import com.navdy.service.library.events.audio.MusicTrackInfo;
import com.navdy.service.library.events.input.GestureEvent;
import com.navdy.service.library.events.ui.Screen;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.navdy.service.library.util.MusicDataUtils;
import com.navdy.service.library.util.ScalingUtilities;
import com.navdy.service.library.util.ScalingUtilities.ScalingLogic;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

import lanchon.dexpatcher.annotation.DexAction;
import lanchon.dexpatcher.annotation.DexAdd;
import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexReplace;
import okio.ByteString;

@DexEdit(defaultAction = DexAction.ADD)
public class MusicNotification implements INotification, MusicUpdateListener {
    @DexIgnore
    private static /* final */ float ARTWORK_ALPHA_NOT_PLAYING; // = 0.5f;
    @DexIgnore
    private static /* final */ float ARTWORK_ALPHA_PLAYING; // = 1.0f;
    @DexIgnore
    private static /* final */ String EMPTY; // = "";
    @DexIgnore
    private static /* final */ int MUSIC_TIMEOUT; // = 5000;
    @DexIgnore
    private static /* final */ Logger sLogger; // = new com.navdy.service.library.log.Logger(com.navdy.hud.app.framework.music.MusicNotification.class);
    @DexIgnore
    private /* final */ ScalingLogic FIT; // = com.navdy.service.library.util.ScalingUtilities.ScalingLogic.FIT;
    @DexIgnore
    private Bus bus;
    @DexIgnore
    private MediaControllerLayout choiceLayout;
    @DexIgnore
    private ViewGroup container;
    @DexIgnore
    private INotificationController controller;
    @DexIgnore
    private Handler handler;
    @DexIgnore
    private AlbumArtImageView image;
    @DexIgnore
    private AtomicBoolean isKeyPressedDown; // = new java.util.concurrent.atomic.AtomicBoolean(false);
    @DexIgnore
    private String lastEventIdentifier;
    @DexIgnore
    private MusicManager musicManager;
    @DexIgnore
    private ProgressBar progressBar;
    @DexIgnore
    private TextView subTitle;
    @DexIgnore
    private TextView title;
    @DexIgnore
    private MusicTrackInfo trackInfo;

    // @DexIgnore
    // class Anon1 implements Runnable {
    //     final /* synthetic */ boolean val$animate;
    //     final /* synthetic */ ByteString val$photo;
    //
    //     @DexIgnore
    //     Anon1(ByteString byteString, boolean z) {
    //         this.val$photo = byteString;
    //         this.val$animate = z;
    //     }
    //
    //     @DexIgnore
    //     public void run() {
    //         MusicNotification.sLogger.d("PhotoUpdate runnable " + this.val$photo);
    //         byte[] byteArray = this.val$photo.toByteArray();
    //         if (byteArray == null || byteArray.length <= 0) {
    //             MusicNotification.sLogger.i("Received photo has null or empty byte array");
    //             MusicNotification.this.setDefaultImage(true);
    //             return;
    //         }
    //         int size = HudApplication.getAppContext().getResources().getDimensionPixelSize(dimen.music_notification_image_size);
    //         Bitmap artwork = null;
    //         Bitmap scaled = null;
    //         try {
    //             artwork = ScalingUtilities.decodeByteArray(byteArray, size, size, MusicNotification.this.FIT);
    //             scaled = ScalingUtilities.createScaledBitmap(artwork, size, size, MusicNotification.this.FIT);
    //             MusicNotification.this.setImage(scaled, this.val$animate);
    //             if (artwork != null && artwork != scaled) {
    //                 artwork.recycle();
    //             }
    //         } catch (Exception e) {
    //             MusicNotification.sLogger.e("Error updating the artwork received ", e);
    //             MusicNotification.this.setDefaultImage(this.val$animate);
    //             if (artwork != null && artwork != scaled) {
    //                 artwork.recycle();
    //             }
    //         } catch (Throwable th) {
    //             if (!(artwork == null || artwork == scaled)) {
    //                 artwork.recycle();
    //             }
    //             throw th;
    //         }
    //     }
    // }

    // @DexIgnore
    // class Anon2 implements Runnable {
    //     final /* synthetic */ boolean val$animate;
    //
    //     @DexIgnore
    //     Anon2(boolean z) {
    //         this.val$animate = z;
    //     }
    //
    //     @DexIgnore
    //     public void run() {
    //         MusicNotification.this.image.setArtworkBitmap(null, this.val$animate);
    //     }
    // }

    // @DexIgnore
    // class Anon3 implements Runnable {
    //     final /* synthetic */ boolean val$animate;
    //     final /* synthetic */ Bitmap val$artwork;
    //
    //     @DexIgnore
    //     Anon3(Bitmap bitmap, boolean z) {
    //         this.val$artwork = bitmap;
    //         this.val$animate = z;
    //     }
    //
    //     @DexIgnore
    //     public void run() {
    //         MusicNotification.this.image.setArtworkBitmap(this.val$artwork, this.val$animate);
    //     }
    // }

    @DexIgnore
    public MusicNotification(MusicManager musicManager2, Bus bus2) {
        this.musicManager = musicManager2;
        this.bus = bus2;
        this.handler = new Handler();
    }

    @DexAdd
    public void clearImage() {
        if (MusicNotification.this.image != null) {
            MusicNotification.this.image.clearImmediately();
        }
    }

    @DexIgnore
    public NotificationType getType() {
        return NotificationType.MUSIC;
    }

    @DexIgnore
    public String getId() {
        return NotificationId.MUSIC_NOTIFICATION_ID;
    }

    @DexIgnore
    public View getView(Context context) {
        if (this.container == null) {
            this.container = (ViewGroup) LayoutInflater.from(context).inflate(layout.notification_music, null);
            this.title = (TextView) this.container.findViewById(id.title);
            this.subTitle = (TextView) this.container.findViewById(id.subTitle);
            this.image = (AlbumArtImageView) this.container.findViewById(id.image);
            this.progressBar = (ProgressBar) this.container.findViewById(id.music_progress);
            this.choiceLayout = (MediaControllerLayout) this.container.findViewById(id.choiceLayout);
        }
        return this.container;
    }

    @DexIgnore
    public View getExpandedView(Context context, Object data) {
        return null;
    }

    @DexIgnore
    public int getExpandedViewIndicatorColor() {
        return 0;
    }

    @DexIgnore
    public void onStart(INotificationController controller2) {
        // this.controller = controller2;
        // this.bus.register(this);
        // this.musicManager.addMusicUpdateListener(this);
        // updateProgressBar(this.musicManager.getCurrentPosition());
    }

    @DexIgnore
    public void onUpdate() {
        // onTrackUpdated(this.musicManager.getCurrentTrack(), this.musicManager.getCurrentControls(), false);
        // updateProgressBar(this.musicManager.getCurrentPosition());
    }

    @DexIgnore
    public void onStop() {
        // this.musicManager.removeMusicUpdateListener(this);
        // this.bus.unregister(this);
        // this.controller = null;
    }

    @DexReplace
    public int getTimeout() {
        return 4500;
    }

    @DexIgnore
    public boolean isAlive() {
        return false;
    }

    @DexIgnore
    public boolean isPurgeable() {
        return false;
    }

    @DexIgnore
    public boolean canAddToStackIfCurrentExists() {
        return false;
    }

    @DexIgnore
    public int getColor() {
        return 0;
    }

    @DexIgnore
    public void onNotificationEvent(Mode mode) {
    }

    @DexIgnore
    public void onExpandedNotificationEvent(Mode mode) {
    }

    @DexIgnore
    public void onExpandedNotificationSwitched() {
    }

    @DexIgnore
    public AnimatorSet getViewSwitchAnimation(boolean viewIn) {
        return null;
    }

    @DexIgnore
    public boolean expandNotification() {
        return false;
    }

    @DexIgnore
    public boolean supportScroll() {
        return false;
    }

    @DexReplace
    public boolean onKey(CustomKeyEvent event) {
        if (this.controller == null) {
            return false;
        }
        if (this.choiceLayout != null) {
            switch (event) {
                case LEFT:
                    restartTimeout();
                    this.choiceLayout.moveSelectionLeft();
                    return true;
                case RIGHT:
                    restartTimeout();
                    this.choiceLayout.moveSelectionRight();
                    return true;
                case SELECT:
                    restartTimeout();
                    Choice choice = this.choiceLayout.getSelectedItem();
                    if (choice == null) {
                        sLogger.w("Choice layout selected item is null, returning");
                        return false;
                    }
                    MediaControl action = MusicManager.CONTROLS[choice.id];
                    switch (action) {
                        case PLAY:
                        case PAUSE:
                            if (this.trackInfo != null) {
                                this.musicManager.executeMediaControl(action, this.isKeyPressedDown.get());
                            }
                            this.choiceLayout.executeSelectedItem(true);
                            return true;
                        case MUSIC_MENU:
                            this.choiceLayout.executeSelectedItem(true);
                            Bundle args = new Bundle();
                            String path = this.musicManager.getMusicMenuPath();
                            if (path == null) {
                                path = HereManeuverDisplayBuilder.SLASH + Menu.MUSIC.name();
                            }
                            args.putString(MainMenuScreen2.ARG_MENU_PATH, path);
                            NotificationManager.getInstance().removeNotification(getId(), true, Screen.SCREEN_MAIN_MENU, args, null);
                            return true;
                        default:
                            return false;
                    }
                case LONG_PRESS:
                    restartTimeout();
                    return true;
            }
        }
        return false;
    }

    @Subscribe
    @DexReplace
    public void onKeyEvent(KeyEvent event) {
        if (this.controller != null && this.choiceLayout != null && InputManager.isCenterKey(event.getKeyCode())) {
            Choice selectedItem = this.choiceLayout.getSelectedItem();
            if (selectedItem == null) {
                sLogger.w("Choice layout selected item is null, returning");
                return;
            }
            MediaControl action = MusicManager.CONTROLS[selectedItem.id];
            if (action == MediaControl.NEXT || action == MediaControl.PREVIOUS) {
                restartTimeout();
                if (this.trackInfo != null) {
                    this.musicManager.handleKeyEvent(event, action, this.isKeyPressedDown.get());
                }
                if (event.getAction() == 0 && this.isKeyPressedDown.compareAndSet(false, true)) {
                    this.choiceLayout.keyDownSelectedItem();
                } else if (event.getAction() == 1 && this.isKeyPressedDown.compareAndSet(true, false)) {
                    this.choiceLayout.keyUpSelectedItem();
                }
            }
        }
    }

    @DexIgnore
    public void updateProgressBar(int value) {
        if (this.controller != null) {
            this.progressBar.setProgress(value);
        }
    }

    @DexReplace
    public void onTrackUpdated(@NonNull MusicTrackInfo trackInfo2, Set<MediaControl> mediaControls, boolean willOpenNotification) {
        this.trackInfo = trackInfo2;
        if (this.controller != null) {
            String songIdentifier = MusicDataUtils.songIdentifierFromTrackInfo(trackInfo2);
            if (this.lastEventIdentifier != null && !this.lastEventIdentifier.equals(songIdentifier)) {
                restartTimeout();
            }
            this.lastEventIdentifier = songIdentifier;
            if (trackInfo2.name != null) {
                this.subTitle.setText(trackInfo2.name);
            } else {
                this.subTitle.setText("");
            }
            if (trackInfo2.author != null) {
                this.title.setText(trackInfo2.author);
            } else {
                this.title.setText("");
            }
            if (!(trackInfo2.duration == null || trackInfo2.currentPosition == null)) {
                this.progressBar.setMax(trackInfo2.duration);
                this.progressBar.setSecondaryProgress(trackInfo2.duration);
            }
            if (MusicManager.tryingToPlay(trackInfo2.playbackState)) {
                this.image.setAlpha(1.0f);
            } else {
                this.image.setAlpha(0.5f);
            }
            this.choiceLayout.updateControls(mediaControls);
        }
    }

    // @DexIgnore
    // public void onAlbumArtUpdate(@Nullable ByteString photo, boolean animate) {
    //     if (photo == null) {
    //         sLogger.v("ByteString image to set in notification is null");
    //         setDefaultImage(animate);
    //         return;
    //     }
    //     TaskManager.getInstance().execute(new Anon1(photo, animate), 1);
    // }

    @DexReplace
    public void onAlbumArtUpdate(@Nullable final ByteString byteString, final boolean defaultImage) {
        if (byteString == null) {
            MusicNotification.sLogger.v("ByteString image to set in notification is null");
            this.setDefaultImage(defaultImage);
        }
        else {
//            TaskManager.getInstance().execute(new Runnable() {
//                @DexIgnore
//                public void run() {
            this.restartTimeout();
            MusicNotification.sLogger.d("PhotoUpdate runnable " + byteString);
            final byte[] byteArray = byteString.toByteArray();
            if (byteArray == null || byteArray.length <= 0) {
                MusicNotification.sLogger.i("Received photo has null or empty byte array");
                MusicNotification.this.setDefaultImage(true);
            }
            else {
                final int dimensionPixelSize = HudApplication.getAppContext().getResources().getDimensionPixelSize(com.navdy.hud.app.R.dimen.music_notification_image_size);
                Bitmap bitmap = null;
                Bitmap decodeByteArray = null;
                Bitmap bitmap4;
                bitmap4 = null;
                Bitmap bitmap5 = null;
                try {
                    final Bitmap bitmap6 = decodeByteArray = ScalingUtilities.decodeByteArray(byteArray, dimensionPixelSize, dimensionPixelSize, MusicNotification.this.FIT);
                    bitmap4 = null;
                    bitmap = bitmap6;
                    bitmap5 = null;
                    final Bitmap scaledBitmap = ScalingUtilities.createScaledBitmap(bitmap6, dimensionPixelSize, dimensionPixelSize, MusicNotification.this.FIT);
                    decodeByteArray = bitmap6;
                    bitmap4 = scaledBitmap;
                    bitmap = bitmap6;
                    bitmap5 = scaledBitmap;
                    MusicNotification.this.setImage(scaledBitmap, defaultImage);
                    MusicNotification.this.restartTimeout();
                }
                catch (Exception ex) {
                    MusicNotification.sLogger.e("Error updating the artwork received ", ex);
                    bitmap = decodeByteArray;
                    bitmap5 = bitmap4;
                    MusicNotification.this.setDefaultImage(defaultImage);
                }
                finally {
                    if (bitmap != null && bitmap != bitmap5) {
                        bitmap.recycle();
                    }
                }
            }
//                }
//            }, 1);
        }
    }

    @DexAdd
    void restartTimeout() {
        if (controller != null) {
            controller.startTimeout(getTimeout());
            controller.resetTimeout();
        }
    }

    @DexReplace
    public void setDefaultImage(final boolean animate) {
        setImage(null, animate);
    }

    @DexReplace
    private void setImage(final Bitmap bitmap, final boolean animate) {
        this.handler.post(() -> {
            if (MusicNotification.this.image != null) {
                MusicNotification.this.image.setArtworkBitmap(bitmap, animate);
            }
        });
    }

    @DexIgnore
    public void onClick() {
    }

    @DexIgnore
    public void onTrackHand(float normalized) {
    }

    @DexIgnore
    public boolean onGesture(GestureEvent event) {
        return false;
    }

    @DexIgnore
    public IInputHandler nextHandler() {
        return null;
    }
}
