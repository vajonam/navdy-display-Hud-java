package com.navdy.hud.app.framework.glance;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.common.TimeHelper;
import com.navdy.hud.app.framework.DriverProfileHelper;
import com.navdy.hud.app.framework.contacts.ContactUtil;
import com.navdy.hud.app.framework.toast.ToastManager;
import com.navdy.hud.app.framework.toast.ToastPresenter;
import com.navdy.hud.app.gesture.GestureServiceConnector;
import com.navdy.service.library.events.glances.CalendarConstants;
import com.navdy.service.library.events.glances.EmailConstants;
import com.navdy.service.library.events.glances.FuelConstants;
import com.navdy.service.library.events.glances.GenericConstants;
import com.navdy.service.library.events.glances.GlanceEvent;
import com.navdy.service.library.events.glances.KeyValue;
import com.navdy.service.library.events.glances.MessageConstants;
import com.navdy.service.library.events.glances.SocialConstants;
import com.navdy.service.library.log.Logger;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit
public class GlanceHelper {
    @DexIgnore
    private static /* final */ HashMap<String, GlanceApp> APP_ID_MAP; // = new java.util.HashMap<>();
    @DexIgnore
    public static /* final */ String DELETE_ALL_GLANCES_TOAST_ID; // = "glance-deleted";
    @DexIgnore
    public static /* final */ String FUEL_PACKAGE; // = "com.navdy.fuel";
    @DexIgnore
    private static /* final */ AtomicLong ID; // = new java.util.concurrent.atomic.AtomicLong(1);
    @DexIgnore
    public static /* final */ String IOS_PHONE_PACKAGE; // = "com.apple.mobilephone";
    @DexIgnore
    public static /* final */ String MICROSOFT_OUTLOOK; // = "com.microsoft.Office.Outlook";
    @DexIgnore
    public static /* final */ String MUSIC_PACKAGE; // = "com.navdy.music";
    @DexIgnore
    public static /* final */ String PHONE_PACKAGE; // = "com.navdy.phone";
    @DexIgnore
    public static /* final */ String SMS_PACKAGE; // = "com.navdy.sms";
    @DexIgnore
    public static /* final */ String TRAFFIC_PACKAGE; // = "com.navdy.traffic";
    @DexIgnore
    public static /* final */ String VOICE_SEARCH_PACKAGE; // = "com.navdy.voice.search";
    @DexIgnore
    private static /* final */ StringBuilder builder; // = new java.lang.StringBuilder();
    @DexIgnore
    private static /* final */ GestureServiceConnector gestureServiceConnector; // = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getGestureServiceConnector();
    @DexIgnore
    private static /* final */ Resources resources; // = com.navdy.hud.app.HudApplication.getAppContext().getResources();
    @DexIgnore
    private static /* final */ Logger sLogger; // = new com.navdy.service.library.log.Logger(com.navdy.hud.app.framework.glance.GlanceHelper.class);

    /* @DexIgnore
    static {
        APP_ID_MAP.put(FUEL_PACKAGE, com.navdy.hud.app.framework.glance.GlanceApp.FUEL);
        APP_ID_MAP.put("com.google.android.calendar", com.navdy.hud.app.framework.glance.GlanceApp.GOOGLE_CALENDAR);
        APP_ID_MAP.put("com.google.android.gm", com.navdy.hud.app.framework.glance.GlanceApp.GOOGLE_MAIL);
        APP_ID_MAP.put("com.google.android.talk", com.navdy.hud.app.framework.glance.GlanceApp.GOOGLE_HANGOUT);
        APP_ID_MAP.put("com.Slack", com.navdy.hud.app.framework.glance.GlanceApp.SLACK);
        APP_ID_MAP.put("com.whatsapp", com.navdy.hud.app.framework.glance.GlanceApp.WHATS_APP);
        APP_ID_MAP.put("com.facebook.orca", com.navdy.hud.app.framework.glance.GlanceApp.FACEBOOK_MESSENGER);
        APP_ID_MAP.put("com.facebook.katana", com.navdy.hud.app.framework.glance.GlanceApp.FACEBOOK);
        APP_ID_MAP.put("com.twitter.android", com.navdy.hud.app.framework.glance.GlanceApp.TWITTER);
        APP_ID_MAP.put(SMS_PACKAGE, com.navdy.hud.app.framework.glance.GlanceApp.SMS);
        APP_ID_MAP.put("com.google.android.apps.inbox", com.navdy.hud.app.framework.glance.GlanceApp.GOOGLE_INBOX);
        APP_ID_MAP.put("com.google.calendar", com.navdy.hud.app.framework.glance.GlanceApp.GOOGLE_CALENDAR);
        APP_ID_MAP.put("com.google.Gmail", com.navdy.hud.app.framework.glance.GlanceApp.GOOGLE_MAIL);
        APP_ID_MAP.put("com.google.hangouts", com.navdy.hud.app.framework.glance.GlanceApp.GOOGLE_HANGOUT);
        APP_ID_MAP.put("com.tinyspeck.chatlyio", com.navdy.hud.app.framework.glance.GlanceApp.SLACK);
        APP_ID_MAP.put("net.whatsapp.WhatsApp", com.navdy.hud.app.framework.glance.GlanceApp.WHATS_APP);
        APP_ID_MAP.put("com.facebook.Messenger", com.navdy.hud.app.framework.glance.GlanceApp.FACEBOOK_MESSENGER);
        APP_ID_MAP.put("com.facebook.Facebook", com.navdy.hud.app.framework.glance.GlanceApp.FACEBOOK);
        APP_ID_MAP.put("com.atebits.Tweetie2", com.navdy.hud.app.framework.glance.GlanceApp.TWITTER);
        APP_ID_MAP.put(com.navdy.hud.app.profile.NotificationSettings.APP_ID_SMS, com.navdy.hud.app.framework.glance.GlanceApp.IMESSAGE);
        APP_ID_MAP.put(com.navdy.hud.app.profile.NotificationSettings.APP_ID_MAIL, com.navdy.hud.app.framework.glance.GlanceApp.APPLE_MAIL);
        APP_ID_MAP.put(com.navdy.hud.app.profile.NotificationSettings.APP_ID_CALENDAR, com.navdy.hud.app.framework.glance.GlanceApp.APPLE_CALENDAR);
        APP_ID_MAP.put(MICROSOFT_OUTLOOK, com.navdy.hud.app.framework.glance.GlanceApp.GENERIC_MAIL);
    } */

    @DexIgnore
    public static String getId() {
        return String.valueOf(ID.getAndIncrement());
    }

    @DexIgnore
    public static GlanceApp getGlancesApp(String appId) {
        return APP_ID_MAP.get(appId);
    }

    @DexIgnore
    public static GlanceApp getGlancesApp(GlanceEvent event) {
        if (event == null) {
            return null;
        }
        GlanceApp app = APP_ID_MAP.get(event.provider);
        if (app != null) {
            return app;
        }
        switch (event.glanceType) {
            case GLANCE_TYPE_EMAIL:
                return GlanceApp.GENERIC_MAIL;
            case GLANCE_TYPE_CALENDAR:
                return GlanceApp.GENERIC_CALENDAR;
            case GLANCE_TYPE_MESSAGE:
                return GlanceApp.GENERIC_MESSAGE;
            case GLANCE_TYPE_SOCIAL:
                return GlanceApp.GENERIC_SOCIAL;
            default:
                return null;
        }
    }

    @DexIgnore
    public static Map<String, String> buildDataMap(GlanceEvent event) {
        Map<String, String> map = new HashMap<>();
        if (event.glanceData != null) {
            for (KeyValue keyValue : event.glanceData) {
                map.put(keyValue.key, ContactUtil.sanitizeString(keyValue.value));
            }
        }
        return map;
    }

    @DexIgnore
    public static String getNotificationId(GlanceEvent event) {
        return getNotificationId(event.glanceType, event.id);
    }

    @DexIgnore
    public static String getNotificationId(GlanceEvent.GlanceType glanceType, String id) {
        String prefix;
        switch (glanceType) {
            case GLANCE_TYPE_EMAIL:
                prefix = GlanceConstants.EMAIL_PREFIX;
                break;
            case GLANCE_TYPE_CALENDAR:
                prefix = GlanceConstants.CALENDAR_PREFIX;
                break;
            case GLANCE_TYPE_MESSAGE:
                prefix = GlanceConstants.MESSAGE_PREFIX;
                break;
            case GLANCE_TYPE_SOCIAL:
                prefix = GlanceConstants.SOCIAL_PREFIX;
                break;
            case GLANCE_TYPE_FUEL:
                prefix = GlanceConstants.FUEL_PREFIX;
                break;
            default:
                prefix = GlanceConstants.GENERIC_PREFIX;
                break;
        }
        return prefix + id;
    }

    @DexReplace
    public static String getTitle(GlanceApp app, Map<String, String> data) {
        String title;
        String title2 = "";
        if (data == null) {
            return title2;
        }
        switch (app) {
            case GOOGLE_HANGOUT:
                title = data.get(MessageConstants.MESSAGE_FROM.name());
                break;
            case SLACK:
            case WHATS_APP:
            case FACEBOOK_MESSENGER:
            case GENERIC_MESSAGE:
                title = data.get(MessageConstants.MESSAGE_FROM.name());
                break;
            case FACEBOOK:
                title = data.get(SocialConstants.SOCIAL_FROM.name());
                if (TextUtils.isEmpty(title)) {
                    title = GlanceConstants.facebook;
                    break;
                }
                break;
            case TWITTER:
            case GENERIC_SOCIAL:
                title = data.get(SocialConstants.SOCIAL_FROM.name());
                break;
            case IMESSAGE:
                title = data.get(MessageConstants.MESSAGE_FROM.name());
                if (title == null) {
                    title = data.get(MessageConstants.MESSAGE_FROM_NUMBER.name());
                    break;
                }
                break;
            case GOOGLE_CALENDAR:
            case APPLE_CALENDAR:
            case GENERIC_CALENDAR:
                title = data.get(CalendarConstants.CALENDAR_TITLE.name());
                break;
            case GOOGLE_MAIL:
            case APPLE_MAIL:
            case GENERIC_MAIL:
            case GOOGLE_INBOX:
                title = data.get(EmailConstants.EMAIL_FROM_NAME.name());
                if (title == null) {
                    title = data.get(EmailConstants.EMAIL_FROM_EMAIL.name());
                    break;
                }
                break;
            case SMS:
                title = data.get(MessageConstants.MESSAGE_FROM.name());
                if (title == null) {
                    title = data.get(MessageConstants.MESSAGE_FROM_NUMBER.name());
                    break;
                }
                break;
            case FUEL:
                if (data.get(FuelConstants.FUEL_LEVEL.name()) == null) {
                    title = resources.getString(R.string.gas_station);
                    break;
                } else {
                    title = String.format("%s %s%%", GlanceConstants.fuelNotificationTitle, data.get(FuelConstants.FUEL_LEVEL.name()));
                    break;
                }
            case GENERIC:
                title = data.get(GenericConstants.GENERIC_TITLE.name()).split("\n")[0];
                if (TextUtils.isEmpty(title)) {
                    title = GlanceConstants.notification;
                }
                break;
            default:
                title = GlanceConstants.notification;
                break;
        }
        if (title == null) {
            title = "";
        }
        return title;
    }

    @DexReplace
    public static String getSubTitle(GlanceApp app, Map<String, String> data) {
        String subTitle = "";
        if (data == null) {
            return subTitle;
        }
        switch (app) {
            case SLACK:
                subTitle = data.get(MessageConstants.MESSAGE_DOMAIN.name());
                if (subTitle != null) {
                    subTitle = resources.getString(R.string.slack_team, subTitle);
                    break;
                }
                break;
            case GOOGLE_CALENDAR:
            case APPLE_CALENDAR:
            case GENERIC_CALENDAR:
                subTitle = data.get(CalendarConstants.CALENDAR_TIME_STR.name());
                break;
            case GOOGLE_MAIL:
            case APPLE_MAIL:
            case GENERIC_MAIL:
            case GOOGLE_INBOX:
                subTitle = data.get(EmailConstants.EMAIL_SUBJECT.name());
                break;
            case FUEL:
                if (data.get(FuelConstants.NO_ROUTE.name()) == null) {
                    subTitle = GlanceConstants.fuelNotificationSubtitle;
                    break;
                }
                break;
            case GENERIC:
                String[] titles = data.get(GenericConstants.GENERIC_TITLE.name()).split("\n");
                if (titles.length >= 2) {
                    subTitle = titles[1];
                }
                break;
        }
        if (subTitle == null) {
            subTitle = "";
        }
        return subTitle;
    }

    @DexIgnore
    public static String getNumber(GlanceApp app, Map<String, String> data) {
        String number = null;
        if (data == null) {
            return null;
        }
        switch (app) {
            case GOOGLE_HANGOUT:
            case IMESSAGE:
            case SMS:
                number = data.get(MessageConstants.MESSAGE_FROM_NUMBER.name());
                break;
        }
        return number;
    }

    @DexIgnore
    public static String getFrom(GlanceApp app, Map<String, String> data) {
        String from = null;
        if (data == null) {
            return null;
        }
        switch (app) {
            case GOOGLE_HANGOUT:
            case IMESSAGE:
            case SMS:
                from = data.get(MessageConstants.MESSAGE_FROM.name());
                break;
        }
        return from;
    }

    @DexIgnore
    public static boolean isPhotoCheckRequired(GlanceApp app) {
        switch (app) {
            case GOOGLE_HANGOUT:
            case IMESSAGE:
            case SMS:
                return true;
            default:
                return false;
        }
    }

    @DexIgnore
    public static synchronized String getGlanceMessage(GlanceApp glanceApp, Map<String, String> dataMap) {
        String message;
        synchronized (GlanceHelper.class) {
            switch (glanceApp) {
                case GOOGLE_HANGOUT:
                case SLACK:
                case WHATS_APP:
                case FACEBOOK_MESSENGER:
                case GENERIC_MESSAGE:
                case IMESSAGE:
                case SMS:
                    String body = dataMap.get(MessageConstants.MESSAGE_BODY.name());
                    if (body != null) {
                        builder.append(body);
                        break;
                    }
                    break;
                case FACEBOOK:
                case TWITTER:
                case GENERIC_SOCIAL:
                    String body2 = dataMap.get(SocialConstants.SOCIAL_MESSAGE.name());
                    if (body2 != null) {
                        builder.append(body2);
                        break;
                    }
                    break;
                case GOOGLE_MAIL:
                case APPLE_MAIL:
                case GENERIC_MAIL:
                case GOOGLE_INBOX:
                    String body3 = dataMap.get(EmailConstants.EMAIL_BODY.name());
                    if (body3 != null) {
                        builder.append(body3);
                        break;
                    }
                    break;
                case GENERIC:
                    String body4 = dataMap.get(GenericConstants.GENERIC_MESSAGE.name());
                    if (body4 != null) {
                        builder.append(body4);
                        break;
                    }
                    break;
            }
            message = builder.toString();
            builder.setLength(0);
        }
        return message;
    }

    @DexIgnore
    public static synchronized String getTtsMessage(GlanceApp glanceApp, Map<String, String> dataMap, StringBuilder stringBuilder1, StringBuilder stringBuilder2, TimeHelper timeHelper) {
        String tts;
        synchronized (GlanceHelper.class) {
            switch (glanceApp) {
                case GOOGLE_HANGOUT:
                case SLACK:
                case WHATS_APP:
                case FACEBOOK_MESSENGER:
                case GENERIC_MESSAGE:
                case IMESSAGE:
                case SMS:
                    String body = dataMap.get(MessageConstants.MESSAGE_BODY.name());
                    if (body != null) {
                        builder.append(body);
                        break;
                    }
                    break;
                case FACEBOOK:
                case TWITTER:
                case GENERIC_SOCIAL:
                    String body2 = dataMap.get(SocialConstants.SOCIAL_MESSAGE.name());
                    if (body2 != null) {
                        builder.append(body2);
                        break;
                    }
                    break;
                case GOOGLE_CALENDAR:
                case APPLE_CALENDAR:
                case GENERIC_CALENDAR:
                    String location = dataMap.get(CalendarConstants.CALENDAR_LOCATION.name());
                    String title = dataMap.get(CalendarConstants.CALENDAR_TITLE.name());
                    long millis = 0;
                    String str = dataMap.get(CalendarConstants.CALENDAR_TIME.name());
                    if (str != null) {
                        try {
                            millis = Long.parseLong(str);
                        } catch (Throwable t) {
                            sLogger.e(t);
                        }
                    }
                    String time = null;
                    if (millis > 0) {
                        String data = getCalendarTime(millis, stringBuilder1, stringBuilder2, timeHelper);
                        if (TextUtils.isEmpty(stringBuilder1.toString())) {
                            String pmMarker = stringBuilder2.toString();
                            if (!TextUtils.isEmpty(pmMarker)) {
                                time = resources.getString(R.string.tts_calendar_time_at, data, TextUtils.equals(pmMarker, GlanceConstants.pmMarker) ? GlanceConstants.pm : GlanceConstants.am);
                            } else {
                                time = data + GlanceConstants.PERIOD;
                            }
                        } else {
                            try {
                                time = Long.parseLong(data) > 1 ? resources.getString(R.string.tts_calendar_time_mins, data) : resources.getString(R.string.tts_calendar_time_min, data);
                            } catch (Throwable th) {
                                sLogger.e(th);
                            }
                        }
                    }
                    if (title != null) {
                        builder.append(resources.getString(R.string.tts_calendar_title, title));
                    }
                    if (time != null) {
                        builder.append(" ");
                        builder.append(time);
                    }
                    if (location != null) {
                        builder.append(" ");
                        builder.append(resources.getString(R.string.tts_calendar_location, location));
                        break;
                    }
                    break;
                case GOOGLE_MAIL:
                case APPLE_MAIL:
                case GENERIC_MAIL:
                case GOOGLE_INBOX:
                    String subject = dataMap.get(EmailConstants.EMAIL_SUBJECT.name());
                    if (subject != null) {
                        builder.append(subject);
                    }
                    String body3 = dataMap.get(EmailConstants.EMAIL_BODY.name());
                    if (body3 != null) {
                        if (builder.length() > 0) {
                            builder.append(GlanceConstants.PERIOD);
                            builder.append(" ");
                        }
                        builder.append(body3);
                        break;
                    }
                    break;
                case FUEL:
                    if (dataMap.containsKey(FuelConstants.FUEL_LEVEL.name())) {
                        builder.append(resources.getString(R.string.your_fuel_level_is_low_tts));
                    }
                    builder.append(resources.getString(R.string.i_can_add_trip_gas_station, dataMap.get(FuelConstants.GAS_STATION_NAME.name())));
                    builder.append(GlanceConstants.PERIOD);
                    break;
                case GENERIC:
                    String body4 = dataMap.get(GenericConstants.GENERIC_MESSAGE.name());
                    if (body4 != null) {
                        builder.append(body4);
                        break;
                    }
                    break;
            }
            tts = builder.toString();
            builder.setLength(0);
        }
        return tts;
    }

    @DexIgnore
    public static GlanceViewCache.ViewType getSmallViewType(GlanceApp glanceApp) {
        switch (glanceApp) {
            case GOOGLE_CALENDAR:
            case APPLE_CALENDAR:
            case GENERIC_CALENDAR:
                return GlanceViewCache.ViewType.SMALL_SIGN;
            default:
                return GlanceViewCache.ViewType.SMALL_GLANCE_MESSAGE;
        }
    }

    @DexIgnore
    public static GlanceViewCache.ViewType getLargeViewType(GlanceApp glanceApp) {
        switch (glanceApp) {
            case GOOGLE_CALENDAR:
            case APPLE_CALENDAR:
            case GENERIC_CALENDAR:
                return GlanceViewCache.ViewType.BIG_CALENDAR;
            case FUEL:
                return GlanceViewCache.ViewType.BIG_MULTI_TEXT;
            default:
                return GlanceViewCache.ViewType.BIG_GLANCE_MESSAGE_SINGLE;
        }
    }

    @DexIgnore
    public static boolean usesMessageLayout(GlanceApp glanceApp) {
        switch (glanceApp) {
            case GOOGLE_HANGOUT:
            case SLACK:
            case WHATS_APP:
            case FACEBOOK_MESSENGER:
            case GENERIC_MESSAGE:
            case FACEBOOK:
            case TWITTER:
            case GENERIC_SOCIAL:
            case IMESSAGE:
            case GOOGLE_MAIL:
            case APPLE_MAIL:
            case GENERIC_MAIL:
            case GOOGLE_INBOX:
            case SMS:
            case GENERIC:
                return true;
            default:
                return false;
        }
    }

    @DexIgnore
    public static String getTimeStr(long now, Date date, TimeHelper timeHelper) {
        if (now - date.getTime() <= 60000) {
            return GlanceConstants.nowStr;
        }
        return timeHelper.formatTime(date, null);
    }

    @DexIgnore
    public static boolean isDeleteAllView(View view) {
        return view != null && view.getTag() == GlanceConstants.DELETE_ALL_VIEW_TAG;
    }

    @DexIgnore
    public static void showGlancesDeletedToast() {
        Bundle bundle = new Bundle();
        bundle.putInt(ToastPresenter.EXTRA_TIMEOUT, 1000);
        bundle.putInt(ToastPresenter.EXTRA_MAIN_IMAGE, R.drawable.icon_qm_glances_grey);
        bundle.putString(ToastPresenter.EXTRA_MAIN_TITLE_2, resources.getString(R.string.glances_deleted));
        bundle.putInt(ToastPresenter.EXTRA_MAIN_TITLE_2_STYLE, R.style.Glances_1);
        ToastManager toastManager = ToastManager.getInstance();
        toastManager.dismissCurrentToast();
        toastManager.clearAllPendingToast();
        toastManager.addToast(new ToastManager.ToastParams(DELETE_ALL_GLANCES_TOAST_ID, bundle, null, true, true));
    }

    @DexIgnore
    public static boolean isTrafficNotificationEnabled() {
        return isPackageEnabled(TRAFFIC_PACKAGE);
    }

    @DexIgnore
    public static boolean isFuelNotificationEnabled() {
        return isPackageEnabled(FUEL_PACKAGE);
    }

    @DexIgnore
    public static boolean isPhoneNotificationEnabled() {
        return isPackageEnabled(PHONE_PACKAGE) || isPackageEnabled("com.apple.mobilephone");
    }

    @DexIgnore
    public static boolean isMusicNotificationEnabled() {
        return isPackageEnabled(MUSIC_PACKAGE);
    }

    @DexIgnore
    public static boolean isPackageEnabled(String packageName) {
        if (!DriverProfileHelper.getInstance().getCurrentProfile().getNotificationPreferences().enabled) {
            return false;
        }
        return Boolean.TRUE.equals(DriverProfileHelper.getInstance().getCurrentProfile().getNotificationSettings().enabled(packageName));
    }

    @DexIgnore
    public static boolean isCalendarApp(GlanceApp glanceApp) {
        switch (glanceApp) {
            case GOOGLE_CALENDAR:
            case APPLE_CALENDAR:
            case GENERIC_CALENDAR:
                return true;
            default:
                return false;
        }
    }

    @DexIgnore
    public static String getCalendarTime(long time, StringBuilder timeUnit, StringBuilder ampmMarker, TimeHelper timeHelper) {
        long minutes = (long) Math.round(((float) (time - System.currentTimeMillis())) / 60000.0f);
        timeUnit.setLength(0);
        ampmMarker.setLength(0);
        if (minutes >= 60 || minutes < -5) {
            return timeHelper.formatTime12Hour(new Date(time), ampmMarker, true);
        }
        if (minutes <= 0) {
            return GlanceConstants.nowStr;
        }
        String str = String.valueOf(minutes);
        timeUnit.append(GlanceConstants.minute);
        return str;
    }

    @DexIgnore
    public static void initMessageAttributes() {
        Context context = HudApplication.getAppContext();
        TextView textView = new TextView(context);
        textView.setTextAppearance(context, R.style.glance_message_title);
        StaticLayout layout = new StaticLayout("RockgOn", textView.getPaint(), GlanceConstants.messageWidthBound, Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);
        int height = layout.getHeight() + GlanceConstants.messageTitleMarginTop + GlanceConstants.messageTitleMarginBottom;
        sLogger.v("message-title-ht = " + height + " layout=" + layout.getHeight());
        GlanceConstants.setMessageTitleHeight(height);
    }

    @DexIgnore
    public static boolean needsScrollLayout(String text) {
        boolean needsScroll;
        if (TextUtils.isEmpty(text)) {
            return false;
        }
        Context context = HudApplication.getAppContext();
        TextView textView = new TextView(context);
        textView.setTextAppearance(context, R.style.glance_message_body);
        float left = ((float) (GlanceConstants.messageHeightBound / 2)) - ((float) (new StaticLayout(text, textView.getPaint(), GlanceConstants.messageWidthBound, Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false).getHeight() / 2));
        int titleHt = GlanceConstants.getMessageTitleHeight();
        needsScroll = !(left > ((float) titleHt));
        sLogger.v("message-tittle-calc left[" + left + "] titleHt [" + titleHt + "]");
        return needsScroll;
    }

    @DexIgnore
    public static GestureServiceConnector getGestureService() {
        return gestureServiceConnector;
    }

    @DexIgnore
    public static int getIcon(String str) {
        if (TextUtils.isEmpty(str)) {
            return -1;
        }
        switch (str) {
            case "GLANCE_ICON_NAVDY_MAIN":
                return R.drawable.icon_user_navdy;
            case "GLANCE_ICON_MESSAGE_SIDE_BLUE":
                return R.drawable.icon_message_blue;
            default:
                return -1;
        }
    }
}
