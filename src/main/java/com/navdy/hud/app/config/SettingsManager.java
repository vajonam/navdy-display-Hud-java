package com.navdy.hud.app.config;

import android.content.SharedPreferences;

import com.navdy.hud.app.profile.NavdyPreferences;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Bus;

import lanchon.dexpatcher.annotation.DexAdd;
import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexRemove;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit
public class SettingsManager implements Setting.IObserver {
    @DexIgnore
    private static /* final */ Logger sLogger; // = new com.navdy.service.library.log.Logger(com.navdy.hud.app.config.SettingsManager.class);
    @DexIgnore
    private final Bus bus;

    @DexAdd
    public static final NavdyPreferences global = new NavdyPreferences("settings");

    @DexIgnore
    public static class SettingsChanged {
        public final String path;
        public final Setting setting;

        @DexIgnore
        public SettingsChanged(String path2, Setting setting2) {
            this.path = path2;
            this.setting = setting2;
        }
    }

    @DexReplace
    public SettingsManager(Bus bus2, SharedPreferences preferences2) {
        this.bus = bus2;
    }

    @DexReplace
    public void addSetting(Setting setting) {
        sLogger.i("Adding setting " + setting);
        setting.setObserver(this);
    }

    @DexReplace
    public void onChanged(Setting setting) {
        sLogger.i("Setting changed:" + setting);
        this.bus.post(new SettingsManager.SettingsChanged(setting.getPath(), setting));
    }


}
