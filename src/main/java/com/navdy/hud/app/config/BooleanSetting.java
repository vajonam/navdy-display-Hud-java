package com.navdy.hud.app.config;

import com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder;
import com.navdy.hud.app.util.DeviceUtil;

import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit
public class BooleanSetting extends Setting {
    @DexIgnore
    private boolean changed;
    @DexIgnore
    private boolean defaultValue;
    @DexIgnore
    private boolean enabled;
    @DexIgnore
    private BooleanSetting.Scope scope;

    @DexIgnore
    public enum Scope {
        NEVER,
        ENG,
        BETA,
        ALWAYS,
        CUSTOM
    }

    @DexIgnore
    public BooleanSetting(String name, BooleanSetting.Scope scope2, String path, String description) {
        this(name, scope2, false, path, description);
    }

    @DexIgnore
    public BooleanSetting(String name, BooleanSetting.Scope scope2, boolean defaultValue2, String path, String description) {
        super(name, path, description);
        this.scope = scope2;
        this.defaultValue = defaultValue2;
        load();
    }

    @DexIgnore
    public boolean isEnabled() {
        boolean z = true;
        switch (this.scope) {
            case CUSTOM:
                return this.enabled;
            case ENG:
                if (DeviceUtil.isUserBuild()) {
                    z = false;
                }
                return z;
            case ALWAYS:
                return true;
            default:
                return false;
        }
    }

    @DexIgnore
    public void setEnabled(boolean enabled2) {
        if (this.scope != BooleanSetting.Scope.CUSTOM) {
            this.scope = BooleanSetting.Scope.CUSTOM;
        }
        if (this.enabled != enabled2) {
            this.enabled = enabled2;
            this.changed = true;
            changed();
            save();
        }
    }

    @DexReplace
    public void save() {
        if (this.changed) {
            this.changed = false;
            String propName = getProperty();
            if ((propName != null) && (observer != null)) {
                observer.global.putBoolean(propName, defaultValue);
            }
        }
    }

    @DexReplace
    public void load() {
        String propName = getProperty();
        if ((propName != null) && (observer != null)) {
            this.enabled = observer.global.getBoolean(propName, defaultValue);
        } else {
            this.enabled = this.defaultValue;
        }
    }

    @DexIgnore
    public String toString() {
        return "BooleanSetting{" + "name=" + getName() +
                HereManeuverDisplayBuilder.COMMA +
                "enabled=" + this.enabled +
                '}';
    }

    // @DexAdd
    // public boolean getValue() {
    //     return enabled;
    // }
}
