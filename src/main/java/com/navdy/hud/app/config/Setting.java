package com.navdy.hud.app.config;

import lanchon.dexpatcher.annotation.DexAdd;
import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexRemove;

@DexEdit
public class Setting {
    @DexIgnore
    private static int MAX_PROP_LEN; // = 31;
    @DexIgnore
    private String description;
    @DexIgnore
    private String name;
    @DexAdd
    SettingsManager observer;
    @DexIgnore
    private String path;
    @DexIgnore
    private String property;

    @DexAdd
    private String value;

    @DexIgnore
    interface IObserver {
        void onChanged(Setting setting);
    }

    @DexIgnore
    public Setting(String name2, String path2, String description2) {
        this(name2, path2, description2, "persist.sys." + path2);
    }

    @DexIgnore
    public Setting(String name2, String path2, String description2, String property2) {
        this.name = name2;
        this.path = path2;
        this.description = description2;
        if (property2 == null || property2.length() <= MAX_PROP_LEN) {
            this.property = property2;
            return;
        }
        throw new IllegalArgumentException("property name (" + property2 + ") too long");
    }

    @DexIgnore
    public String getName() {
        return this.name;
    }

    @DexIgnore
    public String getPath() {
        return this.path;
    }

    @DexIgnore
    public String getDescription() {
        return this.description;
    }

    @DexIgnore
    public String getProperty() {
        return this.property;
    }

    @DexIgnore
    public boolean isEnabled() {
        return true;
    }

    /* access modifiers changed from: protected */
    @DexIgnore
    public void changed() {
        if (this.observer != null) {
            this.observer.onChanged(this);
        }
    }

    @DexRemove
    public void setObserver(Setting.IObserver observer2) {
    }

    @DexAdd
    public void setObserver(SettingsManager observer2) {
        this.observer = observer2;
    }

}
