package com.navdy.hud.app.maps.widget;
import android.graphics.drawable.Drawable;

import com.navdy.hud.app.HudApplication;
import alelec.navdy.hud.app.R;
import com.squareup.otto.Subscribe;

import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit
public class TrafficIncidentWidgetPresenter extends com.navdy.hud.app.view.DashboardWidgetPresenter {
    @DexIgnore
    private static android.os.Handler handler;
    @DexIgnore
    private static com.navdy.service.library.log.Logger sLogger;
    @DexIgnore
    private com.squareup.otto.Bus bus;
    @DexIgnore
    private boolean registered;
    @DexIgnore
    private android.widget.TextView textView;
    @DexIgnore
    private String widgetName;

    @DexReplace
    public String getWidgetName() {
//        return null;
        return HudApplication.getAppContext().getResources().getString(R.string.widget_traffic_incident);
    }

    @DexIgnore
    public Drawable getDrawable() {
        return null;
    }

    @DexIgnore
    public String getWidgetIdentifier() {
        return null;
    }

    @DexIgnore
    protected void updateGauge() {
    }

//
//    private void setText() {
//        this.textView.setTextAppearance(com.navdy.hud.app.HudApplication.getAppContext(), R.style.incident_1);
//        boolean b = com.navdy.hud.app.maps.here.HereMapsManager.getInstance().isInitialized();
//        label1: {
//            label0: {
//                if (!b) {
//                    break label0;
//                }
//                if (!com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().isTrafficUpdaterRunning()) {
//                    break label0;
//                }
//                this.textView.setText(R.string.widget_traffic_normal);
//                break label1;
//            }
//            this.textView.setText(R.string.widget_traffic_inactive);
//        }
//    }

    @DexReplace
    @Subscribe
    public void onDisplayTrafficIncident(com.navdy.hud.app.maps.MapEvents.DisplayTrafficIncident a) {
        try {
            if (this.textView != null) {

                sLogger.v("DisplayTrafficIncident type:" + a.type + ", category:" + a.category + ", title:" + a.title + ", description:" + a.description + ", affectedStreet:" + a.affectedStreet + ", distanceToIncident:" + a.distanceToIncident + ", reported:" + a.reported + ", updated:" + a.updated + ", icon:" + a.icon);

                switch(a.type) {

                    case BLOCKING: {
                        this.textView.setTextAppearance(com.navdy.hud.app.HudApplication.getAppContext(), R.style.incident_1);
                        this.textView.setText(R.string.widget_traffic_blocking);
                        break;
                    }
                    case VERY_HIGH: {

                        this.textView.setTextAppearance(com.navdy.hud.app.HudApplication.getAppContext(), R.style.incident_1);
                        this.textView.setText(R.string.widget_traffic_very_high);
                        break;
                    }
                    case HIGH: {
                        this.textView.setTextAppearance(com.navdy.hud.app.HudApplication.getAppContext(), R.style.incident_1);
                        this.textView.setText(R.string.widget_traffic_high);
                        break;
                    }
                    case FAILED: {
                        this.textView.setTextAppearance(com.navdy.hud.app.HudApplication.getAppContext(), R.style.incident_1);
                        this.textView.setText(R.string.widget_traffic_failed);
                        break;
                    }
                    case INACTIVE: {
                        this.textView.setTextAppearance(com.navdy.hud.app.HudApplication.getAppContext(), R.style.incident_1);
                        this.textView.setText(R.string.widget_traffic_inactive);
                        break;
                    }
                    case NORMAL: {
                        this.textView.setTextAppearance(com.navdy.hud.app.HudApplication.getAppContext(), R.style.incident_1);
                        this.textView.setText(R.string.widget_traffic_normal);
                        break;
                    }
                    default: {
                        com.navdy.service.library.task.TaskManager.getInstance().execute(new Anon1(this, a), 1);
                    }
                }
            }
        } catch(Throwable a0) {
            sLogger.e(a0);
        }
    }

    @DexIgnore
    class Anon1 implements Runnable {
        @DexIgnore
        Anon1(com.navdy.hud.app.maps.MapEvents.DisplayTrafficIncident displayTrafficIncident) {
//            this.val$event = displayTrafficIncident;
        }
        @DexIgnore
        Anon1(TrafficIncidentWidgetPresenter _this, com.navdy.hud.app.maps.MapEvents.DisplayTrafficIncident displayTrafficIncident) {
//            this.val$event = displayTrafficIncident;
        }

        @DexIgnore
        public void run() {
        }
    }
}
