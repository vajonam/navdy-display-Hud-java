package com.navdy.hud.app.maps.here;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.SystemClock;

import com.amazonaws.services.s3.internal.Constants;
import com.here.android.mpa.common.ApplicationContext;
import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.common.GeoPosition;
import com.here.android.mpa.common.MapEngine;
import com.here.android.mpa.common.MapSettings;
import com.here.android.mpa.common.OnEngineInitListener;
import com.here.android.mpa.common.OnEngineInitListener.Error;
import com.here.android.mpa.common.PositioningManager;
import com.here.android.mpa.common.PositioningManager.OnPositionChangedListener;
import com.here.android.mpa.common.Version;
import com.here.android.mpa.mapping.Map;
import com.here.android.mpa.mapping.Map.Animation;
import com.here.android.mpa.mapping.Map.LayerCategory;
import com.here.android.mpa.mapping.MapTrafficLayer;
import com.here.android.mpa.mapping.MapTrafficLayer.RenderLayer;
import com.here.android.mpa.mapping.MapTransitLayer.Mode;
import com.here.android.mpa.mapping.MapView;
import com.here.android.mpa.odml.MapLoader;
import com.here.android.mpa.odml.MapLoader.Listener;
import com.here.android.mpa.odml.MapLoader.ResultCode;
import com.here.android.mpa.odml.MapPackage;
import com.here.android.mpa.odml.MapPackage.InstallationState;
import com.here.android.mpa.routing.RouteOptions;
import com.here.android.mpa.routing.RouteOptions.TransportMode;
import com.here.android.mpa.routing.RouteOptions.Type;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.config.SettingsManager;
import com.navdy.hud.app.debug.DebugReceiver;
import com.navdy.hud.app.device.PowerManager;
import com.navdy.hud.app.device.gps.GpsConstants;
import com.navdy.hud.app.device.gps.GpsUtils;
import com.navdy.hud.app.event.Wakeup;
import com.navdy.hud.app.framework.glance.GlanceHelper;
import com.navdy.hud.app.manager.SpeedManager;
import com.navdy.hud.app.maps.MapsEventHandler;
import com.navdy.hud.app.maps.NavigationMode;
import com.navdy.hud.app.maps.here.CustomScheme.HereMapsScheme;
import com.navdy.hud.app.maps.here.HereMapAnimator.AnimationMode;
import com.navdy.hud.app.maps.notification.RouteCalculationEventHandler;
import com.navdy.hud.app.profile.DriverProfileManager;
import com.navdy.hud.app.storage.PathManager;
import com.navdy.hud.app.ui.component.homescreen.NavigationView;
import com.navdy.hud.app.util.os.SystemProperties;
import com.navdy.service.library.events.connection.LinkPropertiesChanged;
import com.navdy.service.library.events.preferences.NavigationPreferences;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.navdy.service.library.util.SystemUtils;
import com.nokia.maps.MapSettings.b;
import com.squareup.otto.Bus;

import java.io.File;
import java.lang.ref.WeakReference;
import java.lang.reflect.Field;
import java.util.EnumSet;
import java.util.List;

import lanchon.dexpatcher.annotation.DexAdd;
import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexReplace;
import lanchon.dexpatcher.annotation.DexAction;
import lanchon.dexpatcher.annotation.DexTarget;
import mortar.Mortar;

@DexEdit(defaultAction = DexAction.ADD)
public class HereMapsManager {
    @DexIgnore
    public static double DEFAULT_LATITUDE; // = 37.802086d;
    @DexIgnore
    public static double DEFAULT_LONGITUDE; // = -122.419015d;
    @DexIgnore
    public static float DEFAULT_TILT; // = 60.0f;
    @DexIgnore
    public static float DEFAULT_ZOOM_LEVEL; // = 16.5f;
    @DexIgnore
    private static String DISABLE_MAPS_PROPERTY; // = "persist.sys.map.disable";
    @DexIgnore
    private static boolean ENABLE_MAPS; // = (!com.navdy.hud.app.util.os.SystemProperties.getBoolean(DISABLE_MAPS_PROPERTY, false));
    @DexIgnore
    private static String ENROUTE_MAP_SCHEME; // = "guidance.hud";
    @DexIgnore
    private static int GPS_SPEED_LAST_LOCATION_THRESHOLD; // = 2000;
    @DexIgnore
    static int MIN_SAME_POSITION_UPDATE_THRESHOLD; // = 500;
    @DexIgnore
    private static String MW_CONFIG_EXCEPTION_MSG; // = "Invalid configuration file. Check MWConfig!";
    @DexIgnore
    private static String NAVDY_HERE_MAP_SERVICE_NAME; // = "com.navdy.HereMapService";
    @DexIgnore
    private static String NEW_DISKCACHE; // = "diskcache-v5";
    @DexIgnore
    private static String OLD_DISKCACHE; // = "diskcache-v4";
    @DexIgnore
    public static float ROUTE_CALC_START_ZOOM_LEVEL; // = 15.5f;
    @DexIgnore
    private static String TRACKING_MAP_SCHEME; // = "assistance.hud";
    @DexIgnore
    private static String TRAFFIC_REROUTE_PROPERTY; // = "persist.sys.hud_traffic_reroute";
    @DexIgnore
    private static boolean recalcCurrentRouteForTraffic; // = false;
    @DexIgnore
    private static Logger sLogger; // = new com.navdy.service.library.log.Logger(com.navdy.hud.app.maps.here.HereMapsManager.class);
    @DexIgnore
    private static HereMapsManager singleton; // = new com.navdy.hud.app.maps.here.HereMapsManager();
    @DexIgnore
    private final HandlerThread bkLocationReceiverHandlerThread;
    // @javax.inject.Inject
    @DexIgnore
    Bus bus;
    @DexIgnore
    private Context context; // = com.navdy.hud.app.HudApplication.getAppContext();
    @DexIgnore
    private OnEngineInitListener engineInitListener; // = new com.navdy.hud.app.maps.here.HereMapsManager.Anon1();
    @DexIgnore
    private volatile boolean extrapolationOn;
    @DexIgnore
    private Handler handler; // = new android.os.Handler(android.os.Looper.getMainLooper());
    @DexIgnore
    private HereLocationFixManager hereLocationFixManager;
    @DexIgnore
    private HereMapAnimator hereMapAnimator;
    @DexIgnore
    private Object initLock; // = new java.lang.Object();
    @DexIgnore
    private boolean initialMapRendering; // = true;
    @DexIgnore
    private GeoPosition lastGeoPosition;
    @DexIgnore
    private long lastGeoPositionTime;
    @DexIgnore
    private OnSharedPreferenceChangeListener listener; // = new com.navdy.hud.app.maps.here.HereMapsManager.Anon4();
    @DexIgnore
    private volatile boolean lowBandwidthDetected; // = false;
    // @javax.inject.Inject
    @DexIgnore
    DriverProfileManager mDriverProfileManager;
    @DexIgnore
    private Map map;
    @DexIgnore
    private HereMapController mapController;
    @DexIgnore
    private boolean mapDataVerified;
    @DexIgnore
    private MapEngine mapEngine;
    @DexIgnore
    private long mapEngineStartTime;
    @DexIgnore
    private Error mapError;
    @DexIgnore
    private boolean mapInitLoaderComplete;
    @DexIgnore
    private boolean mapInitialized;
    @DexIgnore
    private boolean mapInitializing; // = true;
    @DexIgnore
    private MapLoader mapLoader;
    @DexIgnore
    private int mapPackageCount;
    @DexIgnore
    private MapView mapView;
    @DexIgnore
    private NavigationView navigationView;
    @DexIgnore
    private HereOfflineMapsVersion offlineMapsVersion;
    @DexIgnore
    private OnPositionChangedListener positionChangedListener; // = new com.navdy.hud.app.maps.here.HereMapsManager.Anon3();
    @DexIgnore
    private PositioningManager positioningManager;
    @DexIgnore
    private boolean positioningManagerInstalled;
    // @javax.inject.Inject
    @DexIgnore
    PowerManager powerManager;
    @DexIgnore
    private RouteCalculationEventHandler routeCalcEventHandler;
    @DexIgnore
    private GeoCoordinate routeStartPoint;
    // @javax.inject.Inject
    @DexIgnore
    SharedPreferences sharedPreferences;
    @DexIgnore
    private SpeedManager speedManager;
    @DexIgnore
    private volatile boolean turnEngineOn; // = false;
    @DexIgnore
    private boolean voiceSkinsLoaded;


    @DexAdd
    final private static String DEFAULT_MAP_SCHEME = Map.Scheme.CARNAV_TRAFFIC_NIGHT;
    @DexAdd
    final private static String MAPS_SCHEME_TRACKING_PROPERTY = "persist.sys.map.scheme.tracking";
    @DexAdd
    final private static String MAPS_SCHEME_ENROUTE_PROPERTY = "persist.sys.map.scheme.enroute";
    @DexAdd
    private static String mapsSchemeEnroute = SystemProperties.get(MAPS_SCHEME_TRACKING_PROPERTY, DEFAULT_MAP_SCHEME).replace('_', '.').toLowerCase();
    @DexAdd
    private static String mapsSchemeTracking = SystemProperties.get(MAPS_SCHEME_ENROUTE_PROPERTY, DEFAULT_MAP_SCHEME).replace('_', '.').toLowerCase();

    @DexAdd
    boolean mapsUpdateAvailable;


    @DexEdit(target = DexTarget.STATIC_CONSTRUCTOR)
    private static void source_static() { throw null; }
    static {
        // When this static block is invoked, the patch static fields have
        // already been initialized. Manually invoking the source static
        // constructor initializes the source static fields. The static
        // fields that are declared on both classes are initialized twice,
        // and end up with the values set later by the source.
        source_static();
    }



    @DexIgnore
    class Anon1 implements OnEngineInitListener {

        // /* renamed from: com.navdy.hud.app.maps.here.HereMapsManager$Anon1$Anon1 reason: collision with other inner class name */
        // class C0016Anon1 implements Runnable {
        //     final /* synthetic */ long val$t1;
        //
        //     /* renamed from: com.navdy.hud.app.maps.here.HereMapsManager$Anon1$Anon1$Anon1 reason: collision with other inner class name */
        //     class C0017Anon1 implements Runnable {
        //         final /* synthetic */ Throwable val$t;
        //
        //         C0017Anon1(Throwable th) {
        //             this.val$t = th;
        //         }
        //
        //         public void run() {
        //             throw new MWConfigCorruptException(this.val$t);
        //         }
        //     }
        //
        //     /* renamed from: com.navdy.hud.app.maps.here.HereMapsManager$Anon1$Anon1$Anon2 */
        //     class Anon2 implements Runnable {
        //         final /* synthetic */ long val$t2;
        //
        //         /* renamed from: com.navdy.hud.app.maps.here.HereMapsManager$Anon1$Anon1$Anon2$Anon1 reason: collision with other inner class name */
        //         class C0018Anon1 implements Runnable {
        //             final /* synthetic */ long val$t3;
        //
        //             C0018Anon1(long j) {
        //                 this.val$t3 = j;
        //             }
        //
        //             public void run() {
        //                 AnimationMode animationMode;
        //                 HereNavigationManager hereNavigationManager = HereNavigationManager.getInstance();
        //                 HereMapsManager.this.registerConnectivityReceiver();
        //                 HereMapsManager.sLogger.v("map created");
        //                 TrafficNotificationManager.getInstance();
        //                 if (com.navdy.hud.app.maps.MapSettings.isCustomAnimationEnabled()) {
        //                     animationMode = AnimationMode.ANIMATION;
        //                 } else if (com.navdy.hud.app.maps.MapSettings.isFullCustomAnimatonEnabled()) {
        //                     animationMode = AnimationMode.ZOOM_TILT_ANIMATION;
        //                 } else {
        //                     animationMode = AnimationMode.NONE;
        //                 }
        //                 HereMapsManager.sLogger.v("HereMapAnimator mode is [" + animationMode + "]");
        //                 HereMapsManager.this.hereMapAnimator = new HereMapAnimator(animationMode, HereMapsManager.this.mapController, hereNavigationManager.getNavController());
        //                 if (!HereMapsManager.this.initialMapRendering) {
        //                     HereMapsManager.sLogger.v("HereMapAnimator MapRendering initial disabled");
        //                     HereMapsManager.this.hereMapAnimator.stopMapRendering();
        //                 }
        //                 HereMapCameraManager.getInstance().initialize(HereMapsManager.this.mapController, HereMapsManager.this.bus, HereMapsManager.this.hereMapAnimator);
        //                 HereMapsManager.this.hereLocationFixManager = new HereLocationFixManager(HereMapsManager.this.bus);
        //                 HereMapsManager.sLogger.v("MAP-ENGINE-INIT-4 took [" + (SystemClock.elapsedRealtime() - this.val$t3) + "]");
        //                 HereMapsManager.this.routeCalcEventHandler = new RouteCalculationEventHandler(HereMapsManager.this.bus);
        //                 HereMapsManager.this.initMapLoader();
        //                 HereMapsManager.this.offlineMapsVersion = new HereOfflineMapsVersion(HereMapsManager.this.getOfflineMapsData());
        //                 synchronized (HereMapsManager.this.initLock) {
        //                     HereMapsManager.this.mapInitializing = false;
        //                     HereMapsManager.this.mapInitialized = true;
        //                     HereMapsManager.this.bus.post(new MapEngineInitialize(HereMapsManager.this.mapInitialized));
        //                     HereMapsManager.sLogger.v("MAP-ENGINE-INIT event sent");
        //                 }
        //                 HereMapsManager.sLogger.v("setEngineOnlineState initial");
        //                 HereMapsManager.this.setEngineOnlineState();
        //                 if (!HereMapsManager.this.powerManager.inQuietMode()) {
        //                     HereMapsManager.this.startTrafficUpdater();
        //                 }
        //             }
        //         }
        //
        //         Anon2(long j) {
        //             this.val$t2 = j;
        //         }
        //
        //         public void run() {
        //             HereMapsManager.this.positioningManager.start(LocationMethod.GPS_NETWORK);
        //             HereMapsManager.sLogger.v("position manager started");
        //             HereMapsManager.sLogger.v("MAP-ENGINE-INIT-3 took [" + (SystemClock.elapsedRealtime() - this.val$t2) + "]");
        //             TaskManager.getInstance().execute(new Anon1.C0016Anon1.Anon2.C0018Anon1(SystemClock.elapsedRealtime()), 3);
        //         }
        //     }
        //
        //     C0016Anon1(long j) {
        //         this.val$t1 = j;
        //     }
        //
        //     public void run() {
        //         HereMapsManager.this.mapEngine.onResume();
        //         HereMapsManager.sLogger.v(":initializing maps config");
        //         HereMapsConfigurator.getInstance().updateMapsConfig();
        //         HereMapsManager.sLogger.v("creating geopos");
        //         new GeoPosition(new GeoCoordinate(37.802086d, -122.419015d));
        //         HereMapsManager.sLogger.v("created geopos");
        //         if (!DeviceUtil.isUserBuild()) {
        //             long l1 = SystemClock.elapsedRealtime();
        //             HereLaneInfoBuilder.init();
        //             HereMapsManager.sLogger.v("time to init HereLaneInfoBuilder [" + (SystemClock.elapsedRealtime() - l1) + "]");
        //         }
        //         HereMapsManager.sLogger.v("engine initialized refcount:" + HereMapsManager.this.mapEngine.getResourceReferenceCount());
        //         try {
        //             HereMapsManager.this.map = new Map();
        //         } catch (Throwable t) {
        //             if (t.getMessage().contains(HereMapsManager.MW_CONFIG_EXCEPTION_MSG)) {
        //                 HereMapsManager.sLogger.e("MWConfig is corrupted! Cleaning up and restarting HUD app.");
        //                 HereMapsConfigurator.getInstance().removeMWConfigFolder();
        //             }
        //             HereMapsManager.this.handler.post(new Anon1.C0016Anon1.C0017Anon1(t));
        //         }
        //         HereMapsManager.this.map.setProjectionMode(Projection.MERCATOR);
        //         HereMapsManager.this.map.setTrafficInfoVisible(true);
        //         HereMapsManager.this.mapController = new HereMapController(HereMapsManager.this.map, State.NONE);
        //         HereMapsManager.this.setMapAttributes(new GeoCoordinate(37.802086d, -122.419015d));
        //         HereMapsManager.sLogger.v("setting default map attributes");
        //         HereMapsManager.this.setMapTraffic();
        //         HereMapsManager.this.setMapPoiLayer(HereMapsManager.this.map);
        //         HereMapsManager.this.positioningManager = PositioningManager.getInstance();
        //         HereMapsManager.sLogger.v("MAP-ENGINE-INIT-2 took [" + (SystemClock.elapsedRealtime() - this.val$t1) + "]");
        //         HereMapsManager.this.handler.post(new Anon1.C0016Anon1.Anon2(SystemClock.elapsedRealtime()));
        //     }
        // }

        @DexIgnore
        Anon1() {
        }

        @DexIgnore
        public void onEngineInitializationCompleted(Error error) {
        //     long t1 = SystemClock.elapsedRealtime();
        //     HereMapsManager.sLogger.v("MAP-ENGINE-INIT took [" + (t1 - HereMapsManager.this.mapEngineStartTime) + "]");
        //     HereMapsManager.sLogger.v("MAP-ENGINE-INIT HERE-SDK version:" + Version.getSdkVersion());
        //     if (error == Error.NONE) {
        //         TaskManager.getInstance().execute(new Anon1.C0016Anon1(t1), 3);
        //         return;
        //     }
        //     String errString = error.toString();
        //     AnalyticsSupport.recordKeyFailure("HereMaps", errString);
        //     HereMapsManager.sLogger.e("MAP-ENGINE-INIT engine NOT initialized:" + error);
        //     HereMapsManager.this.mapError = error;
        //     synchronized (HereMapsManager.this.initLock) {
        //         HereMapsManager.this.mapInitializing = false;
        //         HereMapsManager.this.bus.post(new MapEngineInitialize(HereMapsManager.this.mapInitialized));
        //     }
        //     if (error == Error.USAGE_EXPIRED || error == Error.MISSING_APP_CREDENTIAL) {
        //         Main.handleLibraryInitializationError("Here Maps initialization failure: " + errString);
        //     }
        }
    }

    // @DexIgnore
    // class Anon10 implements Runnable {
    //     @DexIgnore
    //     Anon10() {
    //     }
    //
    //     @DexIgnore
    //     public void run() {
    //         HereMapsManager.this.map.setTrafficInfoVisible(false);
    //         MapTrafficLayer trafficLayer = HereMapsManager.this.map.getMapTrafficLayer();
    //         trafficLayer.setDisplayFilter(Severity.NORMAL);
    //         trafficLayer.setEnabled(RenderLayer.ONROUTE, false);
    //         trafficLayer.setEnabled(RenderLayer.FLOW, false);
    //         trafficLayer.setEnabled(RenderLayer.INCIDENT, false);
    //     }
    // }
    //
    // @DexIgnore
    // class Anon11 extends BroadcastReceiver {
    //     Anon11() {
    //     }
    //
    //     public void onReceive(Context context, Intent intent) {
    //         if ("android.net.conn.CONNECTIVITY_CHANGE".equals(intent.getAction())) {
    //             HereMapsManager.sLogger.v("setEngineOnlineState n/w state change");
    //             HereMapsManager.this.setEngineOnlineState();
    //         }
    //     }
    // }

    // @DexIgnore
    // class Anon12 implements Runnable {
    //     Anon12() {
    //     }
    //
    //     public void run() {
    //         HereMapsManager.this.mapController.setTrafficInfoVisible(false);
    //         HereMapsManager.sLogger.v("hidetraffic: map off");
    //         MapRoute mapRoute = HereNavigationManager.getInstance().getCurrentMapRoute();
    //         if (mapRoute != null) {
    //             mapRoute.setTrafficEnabled(false);
    //             HereMapsManager.sLogger.v("hidetraffic: route off");
    //         }
    //         HereMapsManager.sLogger.v("hidetraffic: traffic is disabled");
    //     }
    // }

    // @DexIgnore
    // class Anon13 implements Runnable {
    //     Anon13() {
    //     }
    //
    //     public void run() {
    //         HereNavigationManager.getInstance().setTrafficToMode();
    //         HereMapsManager.sLogger.v("showTraffic: traffic is enabled");
    //     }
    // }

    @DexIgnore
    class Anon2 implements Runnable {

        @DexEdit(defaultAction = DexAction.ADD)
        class Anon1 implements Listener {

            /* renamed from: com.navdy.hud.app.maps.here.HereMapsManager$Anon2$Anon1$Anon1 reason: collision with other inner class name */
            @DexIgnore
            class C0019Anon1 implements Runnable {
                final /* synthetic */ MapPackage val$mapPackage;

                C0019Anon1(MapPackage mapPackage) {
                    this.val$mapPackage = mapPackage;
                }

                public void run() {
                    HereMapsManager.this.printMapPackages(this.val$mapPackage, EnumSet.of(InstallationState.INSTALLED, InstallationState.PARTIALLY_INSTALLED));
                    HereMapsManager.sLogger.v("initMapLoader: map package count:" + HereMapsManager.this.mapPackageCount);
                    HereMapsManager.this.mapDataVerified = true;
                }
            }

            @DexIgnore
            Anon1() {
            }

            @DexIgnore
            public void onProgress(int progressPercentage) {
                HereMapsManager.sLogger.v("MapLoader: progress[" + progressPercentage + "]");
            }

            @DexIgnore
            public void onInstallationSize(long diskSize, long networkSize) {
                HereMapsManager.sLogger.v("MapLoader: onInstallationSize disk[" + diskSize + "] network[" + networkSize + "]");
            }

            @DexIgnore
            public void onGetMapPackagesComplete(MapPackage mapPackage, ResultCode resultCode) {
                if (resultCode != ResultCode.OPERATION_SUCCESSFUL) {
                    HereMapsManager.sLogger.e("initMapLoader: get map package failed:" + resultCode.name());
                    return;
                }
                HereMapsManager.sLogger.v("initMapLoader: get map package success");
                TaskManager.getInstance().execute(new Anon2.Anon1.C0019Anon1(mapPackage), 2);
            }

            @DexReplace
            public void onCheckForUpdateComplete(boolean updateAvailable, String currentMapVersion, String newestMapVersion, ResultCode mapLoaderResultCode) {
                HereMapsManager.sLogger.v("MapLoader: updateAvailable[" + updateAvailable + "] currentMapVersion[" + currentMapVersion + "] newestMapVersion[" + newestMapVersion + "] resultCode[" + mapLoaderResultCode + "]");
                // Added
                HereMapsManager.this.mapsUpdateAvailable = updateAvailable;
                HereMapsManager.this.offlineMapsVersion.setUpdateAvailable(updateAvailable);
                HereMapsManager.this.offlineMapsVersion.setVersion(currentMapVersion);
            }

            @DexIgnore
            public void onPerformMapDataUpdateComplete(MapPackage mapPackage, ResultCode resultCode) {
                HereMapsManager.sLogger.v("MapLoader: onPerformMapDataUpdateComplete result[" + resultCode.name() + "] package[" + (mapPackage != null ? mapPackage.getTitle() : Constants.NULL_VERSION_ID) + "]");
            }

            @DexIgnore
            public void onInstallMapPackagesComplete(MapPackage mapPackage, ResultCode resultCode) {
                HereMapsManager.sLogger.v("MapLoader: onInstallMapPackagesComplete result[" + resultCode.name() + "] package[" + (mapPackage != null ? mapPackage.getTitle() : Constants.NULL_VERSION_ID) + "]");
            }

            @DexIgnore
            public void onUninstallMapPackagesComplete(MapPackage mapPackage, ResultCode resultCode) {
            }
        }

        @DexIgnore
        Anon2() {
        }

        @DexIgnore
        public void run() {
            long l1 = SystemClock.elapsedRealtime();
            HereMapsManager.this.mapLoader = MapLoader.getInstance();
            HereMapsManager.sLogger.v("initMapLoader took [" + (SystemClock.elapsedRealtime() - l1) + "]");
            HereMapsManager.this.mapLoader.addListener(new Anon2.Anon1());
            HereMapsManager.this.mapPackageCount = 0;
            HereMapsManager.this.invokeMapLoader();
        }
    }

    // @DexIgnore
    // class Anon3 implements OnPositionChangedListener {
    //
    //     @DexIgnore
    //     class Anon1 implements Runnable {
    //         final /* synthetic */ GeoPosition val$geoPosition;
    //         final /* synthetic */ boolean val$isMapMatched;
    //         final /* synthetic */ LocationMethod val$locationMethod;
    //
    //         @DexIgnore
    //         Anon1(GeoPosition geoPosition, LocationMethod locationMethod, boolean z) {
    //             this.val$geoPosition = geoPosition;
    //             this.val$locationMethod = locationMethod;
    //             this.val$isMapMatched = z;
    //         }
    //
    //         @DexIgnore
    //         public void run() {
    //         //     try {
    //         //         if (!(this.val$geoPosition instanceof MatchedGeoPosition)) {
    //         //             GeoCoordinate coordinate = this.val$geoPosition.getCoordinate();
    //         //             HereMapsManager.sLogger.v("position not map-matched lat:" + coordinate.getLatitude() + " lng:" + coordinate.getLongitude() + " speed:" + this.val$geoPosition.getSpeed());
    //         //             return;
    //         //         }
    //         //         if (HereMapsManager.this.extrapolationOn) {
    //         //             if (!HereMapUtil.isInTunnel(HereMapsManager.this.positioningManager.getRoadElement())) {
    //         //                 HereMapsManager.sLogger.i("TUNNEL extrapolation off");
    //         //                 HereMapsManager.this.extrapolationOn = false;
    //         //                 HereMapsManager.this.sendExtrapolationEvent();
    //         //             } else {
    //         //                 HereMapsManager.this.sendExtrapolationEvent();
    //         //             }
    //         //         }
    //         //         if (HereMapsManager.this.lastGeoPosition != null) {
    //         //             long t = SystemClock.elapsedRealtime() - HereMapsManager.this.lastGeoPositionTime;
    //         //             if (t < 500 && HereMapUtil.isCoordinateEqual(HereMapsManager.this.lastGeoPosition.getCoordinate(), this.val$geoPosition.getCoordinate())) {
    //         //                 if (HereMapsManager.sLogger.isLoggable(2)) {
    //         //                     HereMapsManager.sLogger.v("GEO-Here same pos as last:" + t);
    //         //                     return;
    //         //                 }
    //         //                 return;
    //         //             }
    //         //         }
    //         //         HereMapsManager.this.lastGeoPosition = this.val$geoPosition;
    //         //         HereMapsManager.this.lastGeoPositionTime = SystemClock.elapsedRealtime();
    //         //         HereMapsManager.this.hereMapAnimator.setGeoPosition(this.val$geoPosition);
    //         //         HereMapsManager.this.hereLocationFixManager.onHerePositionUpdated(this.val$locationMethod, this.val$geoPosition, this.val$isMapMatched);
    //         //         HereMapCameraManager.getInstance().onGeoPositionChange(this.val$geoPosition);
    //         //         if (HereMapsManager.this.speedManager.getObdSpeed() == -1 && SystemClock.elapsedRealtime() - HereMapsManager.this.hereLocationFixManager.getLastLocationTime() >= 2000 && (this.val$geoPosition instanceof MatchedGeoPosition) && this.val$geoPosition.isExtrapolated() && HereMapsManager.this.speedManager.setGpsSpeed((float) this.val$geoPosition.getSpeed(), SystemClock.elapsedRealtimeNanos() / 1000000)) {
    //         //             HereMapsManager.this.bus.post(new GPSSpeedEvent());
    //         //         }
    //         //         HereMapsManager.this.bus.post(this.val$geoPosition);
    //         //         if (HereMapsManager.sLogger.isLoggable(2)) {
    //         //             GeoCoordinate geoCoordinate = this.val$geoPosition.getCoordinate();
    //         //             HereMapsManager.sLogger.v("GEO-Here speed-mps[" + this.val$geoPosition.getSpeed() + "] " + "] lat=[" + geoCoordinate.getLatitude() + "] lon=[" + geoCoordinate.getLongitude() + "] provider=[" + this.val$locationMethod.name() + "] timestamp:[" + this.val$geoPosition.getTimestamp().getTime() + "]");
    //         //         }
    //         //     } catch (Throwable t2) {
    //         //         HereMapsManager.sLogger.e(t2);
    //         //     }
    //         }
    //     }
    //
    //     @DexIgnore
    //     class Anon2 implements Runnable {
    //         @DexIgnore
    //         final /* synthetic */ LocationMethod val$locationMethod;
    //         @DexIgnore
    //         final /* synthetic */ LocationStatus val$locationStatus;
    //
    //         @DexIgnore
    //         Anon2(LocationMethod locationMethod, LocationStatus locationStatus) {
    //             this.val$locationMethod = locationMethod;
    //             this.val$locationStatus = locationStatus;
    //         }
    //
    //         @DexIgnore
    //         public void run() {
    //             try {
    //                 if (HereMapsManager.sLogger.isLoggable(2)) {
    //                     HereMapsManager.sLogger.d("onPositionFixChanged: method:" + this.val$locationMethod + " status: " + this.val$locationStatus);
    //                 }
    //                 if (this.val$locationMethod == LocationMethod.GPS && HereMapUtil.isInTunnel(HereMapsManager.this.positioningManager.getRoadElement()) && !HereMapsManager.this.extrapolationOn) {
    //                     HereMapsManager.sLogger.d("TUNNEL extrapolation on");
    //                     HereMapsManager.this.extrapolationOn = true;
    //                     HereMapsManager.this.sendExtrapolationEvent();
    //                 }
    //             } catch (Throwable t) {
    //                 HereMapsManager.sLogger.e(t);
    //             }
    //         }
    //     }
    //
    //     @DexIgnore
    //     Anon3() {
    //     }
    //
    //     @DexIgnore
    //     public void onPositionUpdated(LocationMethod locationMethod, GeoPosition geoPosition, boolean isMapMatched) {
    //         if (geoPosition != null) {
    //             TaskManager.getInstance().execute(new Anon3.Anon1(geoPosition, locationMethod, isMapMatched), 18);
    //         }
    //     }
    //
    //     @DexIgnore
    //     public void onPositionFixChanged(LocationMethod locationMethod, LocationStatus locationStatus) {
    //         TaskManager.getInstance().execute(new Anon3.Anon2(locationMethod, locationStatus), 18);
    //     }
    // }

    // @DexIgnore
    // class Anon4 implements OnSharedPreferenceChangeListener {
    //
    //     class Anon1 implements Runnable {
    //         Anon1() {
    //         }
    //
    //         public void run() {
    //             if (!HereMapsManager.this.mapController.getMapScheme().equals(HereMapsManager.ENROUTE_MAP_SCHEME)) {
    //                 HereMapsManager.this.mapController.setMapScheme(HereMapsManager.this.getTrackingMapScheme());
    //             }
    //         }
    //     }
    //
    //     Anon4() {
    //     }
    //
    //     public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
    //         HereMapsManager.sLogger.d("onSharedPreferenceChanged: " + key);
    //         if (!HereMapsManager.this.isInitialized()) {
    //             HereMapsManager.sLogger.w("onSharedPreferenceChanged: map engine not intialized:" + key);
    //             return;
    //         }
    //         char c = 65535;
    //         switch (key.hashCode()) {
    //             case -1094616929:
    //                 if (key.equals(HUDSettings.MAP_ANIMATION_MODE)) {
    //                     c = 3;
    //                     break;
    //                 }
    //                 break;
    //             case -285821321:
    //                 if (key.equals(HUDSettings.MAP_SCHEME)) {
    //                     c = 2;
    //                     break;
    //                 }
    //                 break;
    //             case 133816335:
    //                 if (key.equals(HUDSettings.MAP_TILT)) {
    //                     c = 0;
    //                     break;
    //                 }
    //                 break;
    //             case 134000933:
    //                 if (key.equals(HUDSettings.MAP_ZOOM)) {
    //                     c = 1;
    //                     break;
    //                 }
    //                 break;
    //         }
    //         switch (c) {
    //             case 0:
    //                 String val = sharedPreferences.getString(key, null);
    //                 if (val == null) {
    //                     HereMapsManager.sLogger.v("onSharedPreferenceChanged:no tilt");
    //                     return;
    //                 }
    //                 try {
    //                     float tilt = Float.parseFloat(val);
    //                     HereMapsManager.sLogger.w("onSharedPreferenceChanged:tilt:" + tilt);
    //                     HereMapsManager.this.mapController.setTilt(tilt);
    //                     return;
    //                 } catch (Throwable t) {
    //                     HereMapsManager.sLogger.e("onSharedPreferenceChanged:tilt", t);
    //                     return;
    //                 }
    //             case 1:
    //                 String val2 = sharedPreferences.getString(key, null);
    //                 if (val2 == null) {
    //                     HereMapsManager.sLogger.v("onSharedPreferenceChanged:no zoom");
    //                     return;
    //                 }
    //                 try {
    //                     float zoom = Float.parseFloat(val2);
    //                     HereMapsManager.sLogger.w("onSharedPreferenceChanged:zoom:" + zoom);
    //                     HereMapsManager.this.mapController.setZoomLevel((double) zoom);
    //                     return;
    //                 } catch (Throwable t2) {
    //                     HereMapsManager.sLogger.e("onSharedPreferenceChanged:zoom", t2);
    //                     return;
    //                 }
    //             case 2:
    //                 TaskManager.getInstance().execute(new Anon4.Anon1(), 2);
    //                 return;
    //             default:
    //                 return;
    //         }
    //     }
    // }
    //
    // @DexIgnore
    // class Anon5 implements Runnable {
    //
    //     class Anon1 implements Runnable {
    //         Anon1() {
    //         }
    //
    //         public void run() {
    //             HereMapsManager.this.initialize();
    //         }
    //     }
    //
    //     Anon5() {
    //     }
    //
    //     public void run() {
    //         boolean checkAgain = false;
    //         try {
    //             if (((LocationManager) HudApplication.getAppContext().getSystemService(Context.LOCATION_SERVICE)).getProvider("network") == null) {
    //                 HereMapsManager.sLogger.v("n/w provider not found yet, check again");
    //                 checkAgain = true;
    //             } else {
    //                 HereMapsManager.sLogger.v("n/w provider found, initialize here");
    //                 TaskManager.getInstance().execute(new Anon5.Anon1(), 2);
    //             }
    //             if (checkAgain) {
    //                 HereMapsManager.this.handler.postDelayed(this, 1000);
    //             }
    //         } catch (Throwable th) {
    //             if (0 != 0) {
    //                 HereMapsManager.this.handler.postDelayed(this, 1000);
    //             }
    //             throw th;
    //         }
    //     }
    // }
    //
    // @DexIgnore
    // class Anon6 implements Runnable {
    //     Anon6() {
    //     }
    //
    //     public void run() {
    //         HereMapsManager.this.turnOnline();
    //     }
    // }
    //
    // @DexIgnore
    // class Anon7 implements Runnable {
    //     Anon7() {
    //     }
    //
    //     public void run() {
    //         HereMapsManager.this.turnOffline();
    //     }
    // }
    //
    // @DexIgnore
    // class Anon8 implements Runnable {
    //     Anon8() {
    //     }
    //
    //     public void run() {
    //         MapTrafficLayer trafficLayer = HereMapsManager.this.map.getMapTrafficLayer();
    //         trafficLayer.setDisplayFilter(Severity.NORMAL);
    //         trafficLayer.setEnabled(RenderLayer.FLOW, true);
    //         trafficLayer.setEnabled(RenderLayer.INCIDENT, true);
    //         trafficLayer.setEnabled(RenderLayer.ONROUTE, true);
    //         HereMapsManager.this.map.setTrafficInfoVisible(true);
    //     }
    // }
    //
    // @DexIgnore
    // class Anon9 implements Runnable {
    //     Anon9() {
    //     }
    //
    //     public void run() {
    //         MapTrafficLayer trafficLayer = HereMapsManager.this.map.getMapTrafficLayer();
    //         trafficLayer.setDisplayFilter(Severity.NORMAL);
    //         trafficLayer.setEnabled(RenderLayer.ONROUTE, true);
    //         trafficLayer.setEnabled(RenderLayer.FLOW, true);
    //         trafficLayer.setEnabled(RenderLayer.INCIDENT, true);
    //         HereMapsManager.this.map.setTrafficInfoVisible(true);
    //     }
    // }

    @DexIgnore
    public static class MWConfigCorruptException extends RuntimeException {
        @DexIgnore
        public MWConfigCorruptException(Throwable t) {
            super(t);
        }
    }

    @DexIgnore
    public static HereMapsManager getInstance() {
        return singleton;
    }

    @DexIgnore
    private void startTrafficUpdater() {
        try {
            HereNavigationManager.getInstance().startTrafficUpdater();
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }

    @DexIgnore
    public HereOfflineMapsVersion getOfflineMapsVersion() {
        return this.offlineMapsVersion;
    }

    @DexReplace
    private void initMapLoader() {
        TaskManager.getInstance().execute(new Anon2(), 2);
    }

    @DexIgnore
    private void invokeMapLoader() {
        if (this.mapLoader == null) {
            sLogger.v("invokeMapLoader: no maploader");
        } else if (!SystemUtils.isConnectedToNetwork(HudApplication.getAppContext())) {
            sLogger.v("invokeMapLoader: not connected to n/w");
        } else if (!this.mapInitLoaderComplete) {
            this.mapInitLoaderComplete = true;
            sLogger.v("initMapLoader called getMapPackages:" + this.mapLoader.getMapPackages());
        } else {
            sLogger.v("invokeMapLoader: already complete");
        }
    }

    @DexIgnore
    public synchronized void installPositionListener() {
        if (!this.positioningManagerInstalled) {
            this.positioningManager.addListener(new WeakReference(this.positionChangedListener));
            sLogger.v("position manager listener installed");
            this.positioningManagerInstalled = true;
        }
    }

    @DexIgnore
    private HereMapsManager() {
        Mortar.inject(this.context, this);
        this.speedManager = SpeedManager.getInstance();
        if (ENABLE_MAPS) {
            checkforNetworkProvider();
        }
        this.bkLocationReceiverHandlerThread = new HandlerThread("here_bk_location");
        this.bkLocationReceiverHandlerThread.start();
        this.bus.register(this);
    }

    @DexIgnore
    private void checkforNetworkProvider() {
        // this.handler.post(new Anon5());
    }

    @DexIgnore
    private void migrateDiskCache(String diskCacheRootPath) {
        File oldDiskCache = new File(diskCacheRootPath, OLD_DISKCACHE);
        File newDiskCache = new File(diskCacheRootPath, NEW_DISKCACHE);
        if (!oldDiskCache.exists()) {
            sLogger.i("No old cache found");
        } else if (!newDiskCache.exists()) {
            sLogger.i("Migrating v4 diskcache to v5");
            if (!oldDiskCache.renameTo(newDiskCache)) {
                sLogger.i("Failed to rename diskcache");
            }
        } else {
            sLogger.i("Not migrating diskcache - v5 already exists");
        }
    }

    @DexReplace
    private void initialize() {
        sLogger.v(":initializing voice skins");
        VoiceSkinsConfigurator.updateVoiceSkins();
        sLogger.v(":initializing event handlers");
        //noinspection ResultOfMethodCallIgnored
        MapsEventHandler.getInstance();
        if (com.navdy.hud.app.maps.MapSettings.isDebugHereLocation()) {
            DebugReceiver.setHereDebugLocation(true);
        }
        Map.setMaximumFps(com.navdy.hud.app.maps.MapSettings.getMapFps());
        Map.enableMaximumFpsLimit(true);
        sLogger.v("map-fps [" + Map.getMaximumFps() + "] enabled[" + Map.isMaximumFpsLimited() + "]");
        try {
            Field f = com.nokia.maps.MapSettings.class.getDeclaredField("h");
            f.setAccessible(true);
            sLogger.e("enable worker thread before is " + f.get(null));
            f.set(null, b.a);
            sLogger.e("enable worker thread after is " + f.get(null));
        } catch (Throwable t) {
            sLogger.e("enable worker thread", t);
        }
        this.mapEngine = MapEngine.getInstance();
        sLogger.v("calling maps engine init");
        this.sharedPreferences.registerOnSharedPreferenceChangeListener(this.listener);
        recalcCurrentRouteForTraffic = SettingsManager.global.getBoolean(TRAFFIC_REROUTE_PROPERTY, false);
        sLogger.v("persist.sys.hud_traffic_reroute=" + recalcCurrentRouteForTraffic);
        String mapsPartition = PathManager.getInstance().getMapsPartitionPath();
        if (!mapsPartition.isEmpty()) {
            String diskCacheRootPath = mapsPartition + File.separator + ".here-maps";
            migrateDiskCache(diskCacheRootPath);
            try {
                if (!MapSettings.setIsolatedDiskCacheRootPath(diskCacheRootPath, NAVDY_HERE_MAP_SERVICE_NAME)) {
                    sLogger.e("setIsolatedDiskCacheRootPath() failed");
                }
            } catch (Exception e) {
                sLogger.e("exception in setIsolatedDiskCacheRootPath()", e);
            }
        }
        this.mapEngineStartTime = SystemClock.elapsedRealtime();
        // this.mapEngine.init(this.context, this.engineInitListener);
        ApplicationContext appContext = new ApplicationContext(context);
        this.mapEngine.init(appContext, this.engineInitListener);
    }

    @DexIgnore
    public boolean isInitialized() {
        boolean z;
        synchronized (this.initLock) {
            z = this.mapInitialized;
        }
        return z;
    }

    @DexIgnore
    public boolean isInitializing() {
        boolean z;
        synchronized (this.initLock) {
            z = this.mapInitializing;
        }
        return z;
    }

    @DexIgnore
    public Error getError() {
        return this.mapError;
    }

    @DexIgnore
    public boolean isRenderingAllowed() {
        return !this.powerManager.inQuietMode();
    }

    @DexIgnore
    public HereMapController getMapController() {
        return this.mapController;
    }

    @DexIgnore
    public MapView getMapView() {
        return this.mapView;
    }

    @DexIgnore
    public void setMapView(MapView mapView2, NavigationView navigationView2) {
        this.mapView = mapView2;
        this.navigationView = navigationView2;
        HereNavigationManager.getInstance().setMapView(mapView2, navigationView2);
        AnimationMode mode = this.hereMapAnimator.getAnimationMode();
        if (mode != AnimationMode.NONE) {
            sLogger.v("installing map render listener:" + mode);
            this.mapView.addOnMapRenderListener(this.hereMapAnimator.getMapRenderListener());
            return;
        }
        sLogger.v("not installing map render listener:" + mode);
    }

    @DexIgnore
    public RouteOptions getRouteOptions() {
        RouteOptions routeOptions = new RouteOptions();
        routeOptions.setTransportMode(TransportMode.CAR);
        NavigationPreferences preferences = this.mDriverProfileManager.getCurrentProfile().getNavigationPreferences();
        switch (preferences.routingType) {
            case ROUTING_FASTEST:
                routeOptions.setRouteType(Type.FASTEST);
                break;
            case ROUTING_SHORTEST:
                routeOptions.setRouteType(Type.SHORTEST);
                break;
        }
        routeOptions.setHighwaysAllowed(Boolean.TRUE.equals(preferences.allowHighways));
        routeOptions.setTollRoadsAllowed(Boolean.TRUE.equals(preferences.allowTollRoads));
        routeOptions.setFerriesAllowed(false);
        routeOptions.setTunnelsAllowed(Boolean.TRUE.equals(preferences.allowTunnels));
        routeOptions.setDirtRoadsAllowed(Boolean.TRUE.equals(preferences.allowUnpavedRoads));
        routeOptions.setCarShuttleTrainsAllowed(Boolean.TRUE.equals(preferences.allowAutoTrains));
        routeOptions.setCarpoolAllowed(false);
        return routeOptions;
    }

    @DexIgnore
    public GeoPosition getLastGeoPosition() {
        return this.lastGeoPosition;
    }

    // /* access modifiers changed from: 0000 */
    // @DexIgnore
    // public String getTrackingMapScheme() {
    //     return TRACKING_MAP_SCHEME;
    // }
    //
    // /* access modifiers changed from: 0000 */
    // @DexIgnore
    // public String getEnrouteMapScheme() {
    //     return ENROUTE_MAP_SCHEME;
    // }

    @DexReplace
    String getTrackingMapScheme() {
        return mapsSchemeTracking;
    }

    @DexReplace
    String getEnrouteMapScheme() {
        return mapsSchemeEnroute;
    }

    @DexIgnore
    private double getDefaultZoomLevel() {
        return 16.5d;
    }

    @DexIgnore
    private float getDefaultTiltLevel() {
        return 60.0f;
    }

    @DexAdd
    private void setCustomScheme() {
        try {
            HereMapsScheme customScheme = new HereMapsScheme(map, TRACKING_MAP_SCHEME);
            if (customScheme.TrackingSchemeName != null) {
                mapsSchemeEnroute = customScheme.TrackingSchemeName;
                mapsSchemeTracking = customScheme.EnrouteSchemeName;
                this.map.setMapScheme(mapsSchemeTracking);  // alelec: new line
                // } else {
                //     this.map.setMapScheme(this.getTrackingMapScheme());
                sLogger.v("map scheme set to " + this.map.getMapScheme());
            }
        } catch (Exception e) {
            sLogger.e("Failed to customise scheme ", e);
            // this.map.setMapScheme(getTrackingMapScheme());
        }

    }

    @DexReplace
    private void setMapAttributes(GeoCoordinate geoCoordinate) {
        // Map Settings
        // https://developer.here.com/documentation/android-premium/api_reference_java/com/here/android/mpa/mapping/Map.html

        this.map.setExtrudedBuildingsVisible(false);
        this.map.setLandmarksVisible(false);
        this.map.setStreetLevelCoverageVisible(false);
        this.map.setSafetySpotsVisible(true);
        this.map.setMapScheme(TRACKING_MAP_SCHEME);
        try {
            setCustomScheme();
        } catch (Throwable th) {
            sLogger.e("Failed on the custom scheme" + th);
        }
        sLogger.v("map scheme set to " + HereMapsManager.this.map.getMapScheme());
        if (geoCoordinate != null) {
            this.map.setCenter(geoCoordinate, Animation.NONE, getDefaultZoomLevel(), -1.0f, getDefaultTiltLevel());
            sLogger.v("setcenterDefault:" + geoCoordinate);
            return;
        }
        sLogger.v("geoCoordinate not available");

    }

    @DexIgnore
    public void setTrafficOverlay(NavigationMode navigationMode) {
        if (navigationMode == NavigationMode.MAP) {
            setMapTraffic();
        } else if (navigationMode == NavigationMode.MAP_ON_ROUTE) {
            setMapOnRouteTraffic();
        }
    }

    @DexIgnore
    public void clearTrafficOverlay() {
        clearMapTraffic();
    }

    @DexIgnore
    public PositioningManager getPositioningManager() {
        return this.positioningManager;
    }

    @DexIgnore
    private void setEngineOnlineState() {
        // boolean online = NetworkStateManager.isConnectedToNetwork(this.context);
        // this.lowBandwidthDetected = false;
        // sLogger.i("setEngineOnlineState:setting maps engine online state:" + online);
        // if (online) {
        //     TaskManager.getInstance().execute(new Anon6(), 1);
        // } else {
        //     TaskManager.getInstance().execute(new Anon7(), 1);
        // }
    }

    @DexIgnore
    private void setMapPoiLayer(Map map2) {
        map2.setCartoMarkersVisible(false);
        map2.getMapTransitLayer().setMode(Mode.NOTHING);
        map2.setVisibleLayers(EnumSet.of(LayerCategory.ICON_PUBLIC_TRANSIT_STATION, new LayerCategory[]{LayerCategory.PUBLIC_TRANSIT_LINE, LayerCategory.LABEL_PUBLIC_TRANSIT_STATION, LayerCategory.LABEL_PUBLIC_TRANSIT_LINE, LayerCategory.BEACH, LayerCategory.WOODLAND, LayerCategory.DESERT, LayerCategory.GLACIER, LayerCategory.AMUSEMENT_PARK, LayerCategory.ANIMAL_PARK, LayerCategory.BUILTUP, LayerCategory.CEMETERY, LayerCategory.BUILDING, LayerCategory.NEIGHBORHOOD_AREA, LayerCategory.NATIONAL_PARK, LayerCategory.NATIVE_RESERVATION}), false);
        EnumSet<LayerCategory> visibleLayers = map2.getVisibleLayers();
        sLogger.v("==== visible layers == [" + visibleLayers.size() + "]");
        StringBuilder layers = new StringBuilder();
        boolean first = true;
        for (LayerCategory layerCategory : visibleLayers) {
            if (!first) {
                layers.append(", ");
            }
            layers.append(layerCategory.name());
            first = false;
        }
        sLogger.v(layers.toString());
    }

    @DexIgnore
    private void setMapTraffic() {
        // this.mapController.execute(new Anon8());
    }

    @DexIgnore
    private void setMapOnRouteTraffic() {
        // this.mapController.execute(new Anon9());
    }

    /* access modifiers changed from: 0000 */
    @DexIgnore
    public void clearMapTraffic() {
        // this.mapController.execute(new Anon10());
    }

    @DexIgnore
    private boolean isTrafficOverlayVisible() {
        return isTrafficOverlayEnabled(HereNavigationManager.getInstance().getCurrentNavigationMode());
    }

    @DexIgnore
    private boolean isTrafficOverlayEnabled(NavigationMode navigationMode) {
        MapTrafficLayer trafficLayer = this.map.getMapTrafficLayer();
        if (navigationMode == NavigationMode.MAP) {
            if (!trafficLayer.isEnabled(RenderLayer.FLOW) || !trafficLayer.isEnabled(RenderLayer.INCIDENT)) {
                return false;
            }
            return true;
        } else if (navigationMode == NavigationMode.MAP_ON_ROUTE) {
            return trafficLayer.isEnabled(RenderLayer.ONROUTE);
        } else {
            return false;
        }
    }

    @DexIgnore
    public boolean isEngineOnline() {
        return isInitialized() && this.mapEngine != null && MapEngine.isOnlineEnabled();
    }

    @DexIgnore
    private void registerConnectivityReceiver() {
        // IntentFilter filter = new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE");
        // this.context.registerReceiver(new Anon11(), filter);
    }

    @DexIgnore
    public HereLocationFixManager getLocationFixManager() {
        return this.hereLocationFixManager;
    }

    @DexIgnore
    public RouteCalculationEventHandler getRouteCalcEventHandler() {
        return this.routeCalcEventHandler;
    }

    @DexIgnore
    public synchronized void turnOnline() {
        if (!isInitialized()) {
            sLogger.i("turnOnline: engine not yet initialized");
        } else {
            sLogger.i("turnOnline: enabling traffic info");
            requestMapsEngineOnlineStateChange(true);
            if (GlanceHelper.isTrafficNotificationEnabled()) {
                sLogger.i("turnOnline: enabling traffic notifications");
                HereNavigationManager.getInstance().startTrafficRerouteListener();
            } else {
                sLogger.i("turnOnline: not enabling traffic notifications, glance off");
            }
            sLogger.v("turnOnline invokeMapLoader");
            invokeMapLoader();
            sLogger.v("isOnline:" + MapEngine.isOnlineEnabled());
        }
    }

    @DexIgnore
    public synchronized void turnOffline() {
        if (!isInitialized()) {
            sLogger.i("turnOffline: engine not yet initialized");
        } else {
            requestMapsEngineOnlineStateChange(false);
            HereNavigationManager.getInstance().stopTrafficRerouteListener();
            sLogger.v("isOnline:" + MapEngine.isOnlineEnabled());
        }
    }

    @DexIgnore
    public void requestMapsEngineOnlineStateChange(boolean turnOn) {
        sLogger.d("Request to set online state " + turnOn);
        this.turnEngineOn = turnOn;
        updateEngineOnlineState();
    }

    @DexIgnore
    public synchronized void updateEngineOnlineState() {
        boolean z;
        boolean z2 = true;
        synchronized (this) {
            sLogger.d("Updating the engine online state LowBandwidthDetected :" + this.lowBandwidthDetected + ", TurnEngineOn :" + this.turnEngineOn);
            if (isInitialized()) {
                Logger logger = sLogger;
                StringBuilder append = new StringBuilder().append("Turning engine on :");
                if (this.lowBandwidthDetected || !this.turnEngineOn) {
                    z = false;
                } else {
                    z = true;
                }
                logger.d(append.append(z).toString());
                if (this.lowBandwidthDetected || !this.turnEngineOn) {
                    z2 = false;
                }
                MapEngine.setOnline(z2);
            }
        }
    }

    // @com.squareup.otto.Subscribe
    @DexIgnore
    public void onWakeup(Wakeup event) {
        if (isInitialized()) {
            startTrafficUpdater();
        }
    }

    // @com.squareup.otto.Subscribe
    @DexIgnore
    public void onLinkPropertiesChanged(LinkPropertiesChanged propertiesChanged) {
        if (propertiesChanged != null && propertiesChanged.bandwidthLevel != null) {
            sLogger.d("Link Properties changed");
            if (propertiesChanged.bandwidthLevel.intValue() <= 0) {
                sLogger.d("Switching to low bandwidth mode");
                this.lowBandwidthDetected = true;
                updateEngineOnlineState();
                return;
            }
            sLogger.d("Switching back to normal bandwidth mode");
            this.lowBandwidthDetected = false;
            updateEngineOnlineState();
        }
    }

    @DexIgnore
    public GeoCoordinate getRouteStartPoint() {
        return this.routeStartPoint;
    }

    @DexIgnore
    public void setRouteStartPoint(GeoCoordinate geoCoordinate) {
        this.routeStartPoint = geoCoordinate;
    }

    @DexIgnore
    public boolean isRecalcCurrentRouteForTrafficEnabled() {
        return recalcCurrentRouteForTraffic;
    }

    @DexIgnore
    private void sendExtrapolationEvent() {
        Bundle bundle = new Bundle();
        bundle.putBoolean(GpsConstants.EXTRAPOLATION_FLAG, this.extrapolationOn);
        GpsUtils.sendEventBroadcast(GpsConstants.EXTRAPOLATION, bundle);
    }

    @DexIgnore
    public void postManeuverDisplay() {
        if (isInitialized()) {
            HereNavigationManager.getInstance().postManeuverDisplay();
        }
    }

    @DexIgnore
    public Looper getBkLocationReceiverLooper() {
        return this.bkLocationReceiverHandlerThread.getLooper();
    }

    @DexReplace
    public String getOfflineMapsData() {
        return null;
        // try {
        //     java.io.File dir = new java.io.File(com.navdy.hud.app.storage.PathManager.getInstance().getHereMapsDataDirectory());
        //     if (!dir.exists()) {
        //         return null;
        //     }
        //     java.io.File metaData = new java.io.File(dir, com.navdy.hud.app.storage.PathManager.HERE_MAP_META_JSON_FILE);
        //     if (metaData.exists()) {
        //         return com.navdy.service.library.util.IOUtils.convertFileToString(metaData.getAbsolutePath());
        //     }
        //     return null;
        // } catch (Throwable t) {
        //     sLogger.e(t);
        //     return null;
        // }
    }

    @DexIgnore
    public void startMapRendering() {
        if (this.hereMapAnimator != null) {
            this.hereMapAnimator.startMapRendering();
        } else {
            this.initialMapRendering = true;
        }
    }

    @DexIgnore
    public void stopMapRendering() {
        if (this.hereMapAnimator != null) {
            this.hereMapAnimator.stopMapRendering();
        } else {
            this.initialMapRendering = false;
        }
    }

    @DexIgnore
    public void hideTraffic() {
        // if (!isInitialized()) {
        //     return;
        // }
        // if (DriverProfileHelper.getInstance().getDriverProfileManager().isTrafficEnabled()) {
        //     TaskManager.getInstance().execute(new Anon12(), 3);
        // } else {
        //     sLogger.v("hidetraffic: traffic is not enabled");
        // }
    }

    @DexIgnore
    public void showTraffic() {
        // if (!isInitialized()) {
        //     return;
        // }
        // if (DriverProfileHelper.getInstance().getDriverProfileManager().isTrafficEnabled()) {
        //     TaskManager.getInstance().execute(new Anon13(), 3);
        // } else {
        //     sLogger.v("showTraffic: traffic is not enabled");
        // }
    }

    @DexIgnore
    public HereMapAnimator getMapAnimator() {
        return this.hereMapAnimator;
    }

    @DexIgnore
    public void initNavigation() {
        if (isInitialized()) {
            HereNavigationManager.getInstance().getNavController().initialize();
        }
    }

    @DexIgnore
    public void handleBandwidthPreferenceChange() {
        if (isInitialized()) {
            HereNavigationManager.getInstance().setBandwidthPreferences();
        }
    }

    @DexIgnore
    public boolean isNavigationModeOn() {
        if (isInitialized()) {
            return HereNavigationManager.getInstance().isNavigationModeOn();
        }
        return false;
    }

    @DexIgnore
    public boolean isNavigationPossible() {
        if (!isInitialized() || this.hereLocationFixManager.getLastGeoCoordinate() == null) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public String getVersion() {
        if (isInitialized()) {
            return Version.getSdkVersion();
        }
        return null;
    }

    @DexIgnore
    public boolean isVoiceSkinsLoaded() {
        return this.voiceSkinsLoaded;
    }

    @DexIgnore
    public void setVoiceSkinsLoaded() {
        this.voiceSkinsLoaded = true;
    }

    @DexIgnore
    public boolean isMapDataVerified() {
        return this.mapDataVerified;
    }

    @DexIgnore
    public int getMapPackageCount() {
        return this.mapPackageCount;
    }

    @DexReplace
    private void printMapPackages(MapPackage mapPackage, EnumSet<InstallationState> enumSet) {
        if (mapPackage != null) {
            if (enumSet.contains(mapPackage.getInstallationState())) {
                this.mapPackageCount++;
                // Added
                this.offlineMapsVersion.addPackage(mapPackage.getTitle());
            }
            List<MapPackage> children = mapPackage.getChildren();
            if (children != null) {
                for (MapPackage printMapPackages : children) {
                    printMapPackages(printMapPackages, enumSet);
                }
            }
        }
    }

    @DexIgnore
    public void checkForMapDataUpdate() {
        try {
            if (!isInitialized()) {
                sLogger.v("checkForMapDataUpdate MapLoader: engine not initialized");
            } else if (this.mapLoader == null) {
                sLogger.v("checkForMapDataUpdate MapLoader: not available");
            } else if (!this.mapInitLoaderComplete) {
                sLogger.v("checkForMapDataUpdate MapLoader: loader not initialized");
            } else {
                sLogger.v("checkForMapDataUpdate MapLoader: called checkForMapDataUpdate:" + this.mapLoader.checkForMapDataUpdate());
            }
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }
}
