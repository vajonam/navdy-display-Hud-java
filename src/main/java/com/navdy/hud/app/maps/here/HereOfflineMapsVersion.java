package com.navdy.hud.app.maps.here;

import java.util.ArrayList;

import lanchon.dexpatcher.annotation.DexReplace;


@DexReplace
public class HereOfflineMapsVersion {
    final static com.navdy.service.library.log.Logger sLogger;
    private Boolean updateAvail;
    private ArrayList<String> packages = new ArrayList<>();
    private String version = "unknown";
    private String rawDate = "";
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.maps.here.HereOfflineMapsVersion.class);
    }
    
    public HereOfflineMapsVersion(String ignored) {
    }

    public java.util.Date getDate() {
        return null;
    }
    
    public Boolean updateAvailable() {
        return this.updateAvail;
    }

    public java.util.List getPackages() {
        return this.packages;
    }
    
    public String getRawDate() {
        return this.rawDate;
    }
    
    public String getVersion() {
        return this.version;
    }

    public void setVersion(String v) {
        this.version = v;
    }

    public void setUpdateAvailable(Boolean a) {
        this.updateAvail = a;
    }

    public void addPackage(String p) {
        packages.add(p);
    }

}
