package com.navdy.hud.app.maps;

import com.navdy.hud.app.config.SettingsManager;
import com.navdy.hud.app.framework.glympse.GlympseManager;
import com.navdy.hud.app.util.DeviceUtil;

import java.util.concurrent.TimeUnit;

import lanchon.dexpatcher.annotation.DexReplace;

@DexReplace
public class MapSettings {
    private static final String CUSTOM_ANIMATION = "persist.sys.custom_animation";
    private static final String DEBUG_HERE_LOC = "persist.sys.dbg_here_loc";
    private static final int DEFAULT_MAP_FPS = 5;
    private static final String FULL_CUSTOM_ANIMATION = "persist.sys.full_animation";
    private static final String GENERATE_ROUTE_ICONS = "persist.sys.route_icons";
    private static final String GLYMPSE_DURATION = "persist.sys.glympse_dur";
    private static final String LANE_GUIDANCE_ENABLED = "persist.sys.lane_info_enabled";
    private static final String MAP_FPS = "persist.sys.map_fps";
    private static final String NO_TURN_TEXT_IN_TBT = "persist.sys.no_turn_text";
    private static final String TBT_ONTO_DISABLED = "persist.sys.map.tbt.disableonto";
    private static final String TRAFFIC_WIDGETS_ENABLED = "persist.sys.map.traffic.widgets";
    private static final String TTS_RECALCULATION_DISABLED = "persist.sys.map.tts.norecalc";
    private static final boolean customAnimationEnabled = SettingsManager.global.getBoolean(CUSTOM_ANIMATION, false);
    private static final boolean debugHereLocation = SettingsManager.global.getBoolean(DEBUG_HERE_LOC, false);
    private static final boolean dontShowTurnText = SettingsManager.global.getBoolean(NO_TURN_TEXT_IN_TBT, false);
    private static final int fps = SettingsManager.global.getInt(MAP_FPS, 5);
    private static final boolean fullCustomAnimatonEnabled = SettingsManager.global.getBoolean(FULL_CUSTOM_ANIMATION, false);
    private static final boolean generateRouteIcons = SettingsManager.global.getBoolean(GENERATE_ROUTE_ICONS, false);
    private static final int glympseDuration;
    private static final boolean laneGuidanceEnabled = SettingsManager.global.getBoolean(LANE_GUIDANCE_ENABLED, false);
    private static int simulationSpeed;
    private static final boolean tbtOntoDisabled = SettingsManager.global.getBoolean(TBT_ONTO_DISABLED, false);
    private static final boolean trafficDashWidgetsEnabled = SettingsManager.global.getBoolean(TRAFFIC_WIDGETS_ENABLED, true);
    private static final boolean ttsDisableRecalculating = SettingsManager.global.getBoolean(TTS_RECALCULATION_DISABLED, false);

    static {
        int minutes = SettingsManager.global.getInt(GLYMPSE_DURATION, 0);
        if (minutes <= 0 || minutes > 1440) {
            glympseDuration = 0;
        } else {
            glympseDuration = (int) TimeUnit.MINUTES.toMillis((long) minutes);
        }
    }

    public static boolean isTrafficDashWidgetsEnabled() {
        return trafficDashWidgetsEnabled;
    }

    public static boolean isTbtOntoDisabled() {
        return tbtOntoDisabled || !DeviceUtil.isUserBuild();
    }

    public static boolean isTtsRecalculationDisabled() {
        return ttsDisableRecalculating;
    }

    public static boolean isCustomAnimationEnabled() {
        return customAnimationEnabled;
    }

    public static boolean isFullCustomAnimatonEnabled() {
        return fullCustomAnimatonEnabled;
    }

    public static int getMapFps() {
        return fps;
    }

    public static boolean isLaneGuidanceEnabled() {
        return laneGuidanceEnabled;
    }

    public static void setSimulationSpeed(int n) {
        simulationSpeed = n;
    }

    public static int getSimulationSpeed() {
        return simulationSpeed;
    }

    public static boolean isGenerateRouteIcons() {
        return generateRouteIcons;
    }

    public static boolean isDebugHereLocation() {
        return debugHereLocation;
    }

    public static boolean doNotShowTurnTextInTBT() {
        return true;
    }

    public static int getGlympseDuration() {
        if (glympseDuration > 0) {
            return glympseDuration;
        }
        return GlympseManager.GLYMPSE_DURATION;
    }
}
