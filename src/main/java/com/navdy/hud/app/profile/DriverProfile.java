package com.navdy.hud.app.profile;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.TextUtils;

import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.framework.DriverProfileHelper;
import com.navdy.hud.app.framework.glance.GlanceConstants;
import com.navdy.hud.app.ui.component.UISettings;
import com.navdy.service.library.events.MessageStore;
import com.navdy.service.library.events.NavdyEventUtil;
import com.navdy.service.library.events.glances.CannedMessagesUpdate;
import com.navdy.service.library.events.preferences.AudioPreferences;
import com.navdy.service.library.events.preferences.DisplaySpeakerPreferences;
import com.navdy.service.library.events.preferences.DriverProfilePreferences;
import com.navdy.service.library.events.preferences.InputPreferences;
import com.navdy.service.library.events.preferences.LocalPreferences;
import com.navdy.service.library.events.preferences.NavigationPreferences;
import com.navdy.service.library.events.preferences.NotificationPreferences;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.navdy.service.library.util.IOUtils;
import com.squareup.wire.Message;
import com.squareup.wire.Wire;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;

import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;

@DexEdit
public class DriverProfile {
    @DexIgnore
    private static /* final */ String AUDIO_PREFERENCES; // = "AudioPreferences";
    @DexIgnore
    private static /* final */ String CANNED_MESSAGES; // = "CannedMessages";
    @DexIgnore
    private static /* final */ String CONTACTS_IMAGE_CACHE_DIR; // = "contacts";
    @DexIgnore
    public static /* final */ String DRIVER_PROFILE_IMAGE; // = "DriverImage";
    @DexIgnore
    private static /* final */ String DRIVER_PROFILE_PREFERENCES; // = "DriverProfilePreferences";
    @DexIgnore
    private static /* final */ String INPUT_PREFERENCES; // = "InputPreferences";
    @DexIgnore
    private static /* final */ String LOCALE_SEPARATOR; // = "_";
    @DexIgnore
    private static /* final */ String LOCAL_PREFERENCES; // = "LocalPreferences";
    @DexIgnore
    private static /* final */ String MUSIC_IMAGE_CACHE_DIR; // = "music";
    @DexIgnore
    private static /* final */ String NAVIGATION_PREFERENCES; // = "NavigationPreferences";
    @DexIgnore
    private static /* final */ String NOTIFICATION_PREFERENCES; // = "NotificationPreferences";
    @DexIgnore
    private static /* final */ String PLACES_IMAGE_CACHE_DIR; // = "places";
    @DexIgnore
    private static /* final */ String PREFERENCES_DIRECTORY; // = "Preferences";
    @DexIgnore
    private static /* final */ String SPEAKER_PREFERENCES; // = "SpeakerPreferences";
    @DexIgnore
    private static /* final */ Logger sLogger; // = new com.navdy.service.library.log.Logger(com.navdy.hud.app.profile.DriverProfile.class);
    @DexIgnore
    AudioPreferences mAudioPreferences;
    @DexIgnore
    CannedMessagesUpdate mCannedMessages;
    @DexIgnore
    private File mContactsImageDirectory;
    @DexIgnore
    private Bitmap mDriverImage;
    @DexIgnore
    DriverProfilePreferences mDriverProfilePreferences;
    @DexIgnore
    InputPreferences mInputPreferences;
    @DexIgnore
    private LocalPreferences mLocalPreferences;
    @DexIgnore
    private Locale mLocale;
    @DexIgnore
    private MessageStore mMessageStore; // = new com.navdy.service.library.events.MessageStore(this.mPreferencesDirectory);
    @DexIgnore
    private File mMusicImageDirectory;
    @DexIgnore
    NavigationPreferences mNavigationPreferences;
    @DexIgnore
    NotificationPreferences mNotificationPreferences;
    @DexIgnore
    private NotificationSettings mNotificationSettings;
    @DexIgnore
    private File mPlacesImageDirectory;
    @DexIgnore
    private File mPreferencesDirectory;
    @DexIgnore
    private File mProfileDirectory;
    @DexIgnore
    private String mProfileName;
    @DexIgnore
    private DisplaySpeakerPreferences mSpeakerPreferences;
    @DexIgnore
    private boolean mTrafficEnabled;

    // @DexIgnore
    // class Anon1 extends NavdyEventUtil.Initializer<DriverProfilePreferences> {
    //     @DexIgnore
    //     Anon1() {
    //     }
    //
    //     @DexIgnore
    //     public DriverProfilePreferences build(Message.Builder<DriverProfilePreferences> builder) {
    //         return ((DriverProfilePreferences.Builder) builder).device_name("Phone " + DriverProfile.this.mProfileDirectory.getName()).build();
    //     }
    // }
    //
    // @DexIgnore
    // class Anon2 implements Runnable {
    //     @DexIgnore
    //     Anon2() {
    //     }
    //
    //     @DexIgnore
    //     public void run() {
    //         IOUtils.deleteFile(HudApplication.getAppContext(), new File(DriverProfile.this.mPreferencesDirectory, DriverProfile.DRIVER_PROFILE_IMAGE).getAbsolutePath());
    //     }
    // }

    @DexIgnore
    static DriverProfile createProfileForId(String profileName, String profileDirectoryName) throws IOException {
        if (profileName.contains(File.separator) || profileName.startsWith(GlanceConstants.PERIOD)) {
            throw new IllegalArgumentException("Profile names can't refer to directories");
        }
        String profilePath = profileDirectoryName + File.separator + profileName;
        File profileDirectory = new File(profilePath);
        if (profileDirectory.mkdir() || profileDirectory.isDirectory()) {
            File preferencesDirectory = new File(profilePath + File.separator + PREFERENCES_DIRECTORY);
            if (preferencesDirectory.mkdir() || preferencesDirectory.isDirectory()) {
                return new DriverProfile(profileDirectory);
            }
            throw new IOException("could not create preferences directory");
        }
        throw new IOException("could not create profile");
    }

    @DexIgnore
    protected DriverProfile(File profileDirectory) throws IOException {
        // this.mProfileName = profileDirectory.getName();
        // this.mProfileDirectory = profileDirectory;
        // this.mPreferencesDirectory = new File(profileDirectory, PREFERENCES_DIRECTORY);
        // this.mPlacesImageDirectory = new File(profileDirectory, PLACES_IMAGE_CACHE_DIR);
        // IOUtils.createDirectory(this.mPlacesImageDirectory);
        // this.mContactsImageDirectory = new File(profileDirectory, CONTACTS_IMAGE_CACHE_DIR);
        // IOUtils.createDirectory(this.mContactsImageDirectory);
        // this.mMusicImageDirectory = new File(profileDirectory, "music");
        // IOUtils.createDirectory(this.mMusicImageDirectory);
        // this.mDriverProfilePreferences = readPreference(DRIVER_PROFILE_PREFERENCES, DriverProfilePreferences.class, new Anon1());
        // this.mNavigationPreferences = readPreference(NAVIGATION_PREFERENCES, NavigationPreferences.class);
        // setTraffic();
        // this.mInputPreferences = readPreference(INPUT_PREFERENCES, InputPreferences.class);
        // this.mSpeakerPreferences = readPreference(SPEAKER_PREFERENCES, DisplaySpeakerPreferences.class);
        // this.mAudioPreferences = readPreference(AUDIO_PREFERENCES, AudioPreferences.class);
        // this.mNotificationPreferences = readPreference(NOTIFICATION_PREFERENCES, NotificationPreferences.class);
        // this.mNotificationSettings = new NotificationSettings(this.mNotificationPreferences);
        // this.mLocalPreferences = readPreference(LOCAL_PREFERENCES, LocalPreferences.class);
        // this.mCannedMessages = readPreference(CANNED_MESSAGES, CannedMessagesUpdate.class);
        // InputStream driverImageStream = null;
        // try {
        //     InputStream driverImageStream2 = new FileInputStream(new File(this.mPreferencesDirectory, DRIVER_PROFILE_IMAGE));
        //     try {
        //         this.mDriverImage = BitmapFactory.decodeStream(driverImageStream2);
        //         IOUtils.closeStream(driverImageStream2);
        //         FileInputStream fileInputStream = driverImageStream2;
        //     } catch (FileNotFoundException e) {
        //         driverImageStream = driverImageStream2;
        //         try {
        //             this.mDriverImage = null;
        //             IOUtils.closeStream(driverImageStream);
        //             setLocale();
        //         } catch (Throwable th) {
        //             th = th;
        //             IOUtils.closeStream(driverImageStream);
        //             throw th;
        //         }
        //     } catch (Throwable th2) {
        //         th = th2;
        //         driverImageStream = driverImageStream2;
        //         IOUtils.closeStream(driverImageStream);
        //         throw th;
        //     }
        // } catch (FileNotFoundException e2) {
        //     this.mDriverImage = null;
        //     IOUtils.closeStream(driverImageStream);
        //     setLocale();
        // }
        // setLocale();
    }

    @DexIgnore
    private <T extends Message> T readPreference(String filename, Class<T> type) throws IOException {
        return this.mMessageStore.readMessage(filename, type);
    }

    @DexIgnore
    private <T extends Message> T readPreference(String filename, Class<T> type, NavdyEventUtil.Initializer<T> initializer) throws IOException {
        return this.mMessageStore.readMessage(filename, type, initializer);
    }

    /* access modifiers changed from: 0000 */
    @DexIgnore
    public void setDriverProfilePreferences(DriverProfilePreferences preferences) {
        this.mDriverProfilePreferences = removeNulls(preferences);
        setLocale();
        sLogger.i("[" + this.mProfileName + "] updating driver profile preferences to ver[" + preferences.serial_number + "] " + this.mDriverProfilePreferences);
        writeMessageToFile(this.mDriverProfilePreferences, DRIVER_PROFILE_PREFERENCES);
    }

    @DexIgnore
    public NavigationPreferences getNavigationPreferences() {
        return this.mNavigationPreferences;
    }

    @DexIgnore
    public InputPreferences getInputPreferences() {
        return this.mInputPreferences;
    }

    @DexIgnore
    public NotificationPreferences getNotificationPreferences() {
        return this.mNotificationPreferences;
    }

    @DexIgnore
    public NotificationSettings getNotificationSettings() {
        return this.mNotificationSettings;
    }

    @DexIgnore
    public DisplaySpeakerPreferences getSpeakerPreferences() {
        return this.mSpeakerPreferences;
    }

    @DexIgnore
    public AudioPreferences getAudioPreferences() {
        return this.mAudioPreferences;
    }

    @DexIgnore
    public LocalPreferences getLocalPreferences() {
        return this.mLocalPreferences;
    }

    @DexIgnore
    public CannedMessagesUpdate getCannedMessages() {
        return this.mCannedMessages;
    }

    /* access modifiers changed from: 0000 */
    @DexIgnore
    public void setNavigationPreferences(NavigationPreferences preferences) {
        this.mNavigationPreferences = removeNulls(preferences);
        sLogger.i("[" + this.mProfileName + "] updating nav preferences to ver[" + preferences.serial_number + "] " + this.mNavigationPreferences);
        writeMessageToFile(this.mNavigationPreferences, NAVIGATION_PREFERENCES);
        setTraffic();
    }

    /* access modifiers changed from: 0000 */
    @DexIgnore
    public void setInputPreferences(InputPreferences preferences) {
        this.mInputPreferences = removeNulls(preferences);
        sLogger.i("[" + this.mProfileName + "] updating input preferences to ver[" + preferences.serial_number + "] " + this.mInputPreferences);
        writeMessageToFile(this.mInputPreferences, INPUT_PREFERENCES);
    }

    /* access modifiers changed from: 0000 */
    @DexIgnore
    public void setSpeakerPreferences(DisplaySpeakerPreferences preferences) {
        this.mSpeakerPreferences = removeNulls(preferences);
        sLogger.i("[" + this.mProfileName + "] updating speaker preferences to ver[" + preferences.serial_number + "] " + this.mSpeakerPreferences);
        writeMessageToFile(this.mSpeakerPreferences, SPEAKER_PREFERENCES);
    }

    /* access modifiers changed from: 0000 */
    @DexIgnore
    public void setAudioPreferences(AudioPreferences preferences) {
        this.mAudioPreferences = removeNulls(preferences);
        sLogger.i("[" + this.mProfileName + "] updating input preferences to ver[" + preferences.serial_number + "] " + this.mAudioPreferences);
        writeMessageToFile(this.mAudioPreferences, AUDIO_PREFERENCES);
    }

    /* access modifiers changed from: 0000 */
    @DexIgnore
    public void setNotificationPreferences(NotificationPreferences preferences) {
        this.mNotificationPreferences = removeNulls(preferences);
        sLogger.i("[" + this.mProfileName + "] updating notification preferences to ver[" + preferences.serial_number + "] " + this.mNotificationPreferences);
        writeMessageToFile(this.mNotificationPreferences, NOTIFICATION_PREFERENCES);
        this.mNotificationSettings.update(this.mNotificationPreferences);
    }

    @DexIgnore
    public void setLocalPreferences(LocalPreferences preferences) {
        this.mLocalPreferences = removeNulls(preferences);
        sLogger.i("[" + this.mProfileName + "] updating local preferences to " + this.mLocalPreferences);
        writeMessageToFile(this.mLocalPreferences, LOCAL_PREFERENCES);
    }

    /* access modifiers changed from: 0000 */
    @DexIgnore
    public void setCannedMessages(CannedMessagesUpdate cannedMessages) {
        this.mCannedMessages = removeNulls(cannedMessages);
        sLogger.i("[" + this.mProfileName + "] updating canned messages preferences to ver[" + cannedMessages.serial_number + "]");
        writeMessageToFile(this.mCannedMessages, CANNED_MESSAGES);
    }

    @DexIgnore
    private void writeMessageToFile(Message message, String fileName) {
        this.mMessageStore.writeMessage(message, fileName);
    }

    @DexIgnore
    private <T extends Message> T removeNulls(T message) {
        return NavdyEventUtil.applyDefaults(message);
    }

    @DexIgnore
    public String getDriverName() {
        return this.mDriverProfilePreferences.driver_name;
    }

    @DexIgnore
    public String getFirstName() {
        String driverName = this.mDriverProfilePreferences.driver_name;
        if (TextUtils.isEmpty(driverName)) {
            return driverName;
        }
        String driverName2 = driverName.trim();
        int firstWordEnd = driverName2.indexOf(" ");
        return firstWordEnd != -1 ? driverName2.substring(0, firstWordEnd) : driverName2;
    }

    @DexIgnore
    public String getDriverEmail() {
        return this.mDriverProfilePreferences.driver_email;
    }

    @DexIgnore
    public String getDeviceName() {
        return this.mDriverProfilePreferences.device_name;
    }

    @DexIgnore
    public String getCarMake() {
        return this.mDriverProfilePreferences.car_make;
    }

    @DexIgnore
    public String getCarModel() {
        return this.mDriverProfilePreferences.car_model;
    }

    @DexIgnore
    public String getCarYear() {
        return this.mDriverProfilePreferences.car_year;
    }

    @DexIgnore
    public DriverProfilePreferences.DisplayFormat getDisplayFormat() {
        return this.mDriverProfilePreferences.display_format;
    }

    @DexIgnore
    public DriverProfilePreferences.DialLongPressAction getLongPressAction() {
        if (UISettings.isLongPressActionPlaceSearch()) {
            return DriverProfilePreferences.DialLongPressAction.DIAL_LONG_PRESS_PLACE_SEARCH;
        }
        return this.mDriverProfilePreferences.dial_long_press_action;
    }

    @DexIgnore
    public DriverProfilePreferences.FeatureMode getFeatureMode() {
        return this.mDriverProfilePreferences.feature_mode;
    }

    @DexIgnore
    public DriverProfilePreferences.UnitSystem getUnitSystem() {
        return this.mDriverProfilePreferences.unit_system;
    }

    @DexIgnore
    public Locale getLocale() {
        if (this.mLocale != null) {
            return this.mLocale;
        }
        return Locale.getDefault();
    }

    @DexIgnore
    private void setLocale() {
        // try {
        //     this.mLocale = null;
        //     String localeStr = this.mDriverProfilePreferences.locale;
        //     if (!TextUtils.isEmpty(localeStr)) {
        //         Locale locale = HudLocale.getLocaleForID(localeStr);
        //         sLogger.v("locale string:" + localeStr + " locale:" + locale + " profile:" + getProfileName() + " email:" + getDriverEmail());
        //         if (TextUtils.isEmpty(locale.getISO3Language())) {
        //             sLogger.v("locale no language");
        //         } else {
        //             this.mLocale = locale;
        //         }
        //     } else {
        //         sLogger.v("no locale string");
        //     }
        // } catch (Throwable t) {
        //     sLogger.e("setLocale", t);
        // }
    }

    @DexIgnore
    public boolean isTrafficEnabled() {
        return this.mTrafficEnabled;
    }

    @DexIgnore
    public void setTrafficEnabled(boolean b) {
        this.mTrafficEnabled = b;
    }

    @DexIgnore
    private void setTraffic() {
        this.mTrafficEnabled = true;
    }

    @DexIgnore
    public boolean isAutoOnEnabled() {
        return Wire.get(this.mDriverProfilePreferences.auto_on_enabled, DriverProfilePreferences.DEFAULT_AUTO_ON_ENABLED).booleanValue();
    }

    @DexIgnore
    public DriverProfilePreferences.ObdScanSetting getObdScanSetting() {
        return this.mDriverProfilePreferences.obdScanSetting;
    }

    @DexIgnore
    public long getObdBlacklistModificationTime() {
        if (this.mDriverProfilePreferences.obdBlacklistLastModified != null) {
            return this.mDriverProfilePreferences.obdBlacklistLastModified.longValue();
        }
        return 0;
    }

    @DexIgnore
    public boolean isLimitBandwidthModeOn() {
        return Boolean.TRUE.equals(this.mDriverProfilePreferences.limit_bandwidth);
    }

    @DexIgnore
    public boolean isProfilePublic() {
        return Boolean.TRUE.equals(this.mDriverProfilePreferences.profile_is_public);
    }

    @DexIgnore
    public Bitmap getDriverImage() {
        return this.mDriverImage;
    }

    @DexIgnore
    public File getDriverImageFile() {
        return new File(this.mPreferencesDirectory, DRIVER_PROFILE_IMAGE);
    }

    @DexIgnore
    private void removeDriverImage() {
        // this.mDriverImage = null;
        // TaskManager.getInstance().execute(new DriverProfile.Anon2(), 1);
    }

    @DexIgnore
    public String getProfileName() {
        return this.mProfileName;
    }

    @DexIgnore
    public File getPlacesImageDir() {
        return this.mPlacesImageDirectory;
    }

    @DexIgnore
    public File getContactsImageDir() {
        return this.mContactsImageDirectory;
    }

    @DexIgnore
    public File getMusicImageDir() {
        return this.mMusicImageDirectory;
    }

    @DexIgnore
    public File getPreferencesDirectory() {
        return this.mPreferencesDirectory;
    }

    @DexIgnore
    public boolean isDefaultProfile() {
        return DriverProfileHelper.getInstance().getDriverProfileManager().isDefaultProfile(this);
    }

    @DexIgnore
    public String toString() {
        return "DriverProfile{mProfileName='" + this.mProfileName + '\'' + '}';
    }

    /* access modifiers changed from: 0000 */
    @DexIgnore
    public void copy(DriverProfile profile) {
        this.mDriverProfilePreferences = new DriverProfilePreferences.Builder(profile.mDriverProfilePreferences).build();
        writeMessageToFile(this.mDriverProfilePreferences, DRIVER_PROFILE_PREFERENCES);
        this.mNavigationPreferences = new NavigationPreferences.Builder(profile.mNavigationPreferences).build();
        writeMessageToFile(this.mNavigationPreferences, NAVIGATION_PREFERENCES);
        this.mInputPreferences = new InputPreferences.Builder(profile.mInputPreferences).build();
        writeMessageToFile(this.mInputPreferences, INPUT_PREFERENCES);
        this.mSpeakerPreferences = new DisplaySpeakerPreferences.Builder(profile.mSpeakerPreferences).build();
        writeMessageToFile(this.mSpeakerPreferences, SPEAKER_PREFERENCES);
        this.mNotificationPreferences = new NotificationPreferences.Builder(profile.mNotificationPreferences).build();
        writeMessageToFile(this.mNotificationPreferences, NOTIFICATION_PREFERENCES);
        this.mLocalPreferences = new LocalPreferences.Builder(profile.mLocalPreferences).build();
        writeMessageToFile(this.mLocalPreferences, LOCAL_PREFERENCES);
        sLogger.v("copy done:" + this.mPreferencesDirectory);
    }
}
