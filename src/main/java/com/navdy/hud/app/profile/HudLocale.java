package com.navdy.hud.app.profile;

import android.content.Context;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;

import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.config.SettingsManager;

import java.util.Locale;

import lanchon.dexpatcher.annotation.DexAdd;
import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit
public class HudLocale {
    @DexIgnore
    public static /* final */ java.lang.String DEFAULT_LANGUAGE; // = "en";
    @DexIgnore
    public static /* final */ java.lang.String DEFAULT_LOCALE_ID; // = "en_US";
    @DexIgnore
    private static /* final */ java.lang.String LOCALE_SEPARATOR; // = "_";
    @DexIgnore
    private static /* final */ java.lang.String LOCALE_TOAST_ID; // = "#locale#toast";
    @DexIgnore
    private static java.lang.String MAP_ENGINE_PROCESS_NAME; // = "global.Here.Map.Service.v3";
    @DexIgnore
    private static /* final */ java.lang.String SELECTED_LOCALE; // = "Locale.Helper.Selected.Language";
    @DexIgnore
    private static /* final */ java.lang.String TAG; // = "[HUD-locale]";
    @DexIgnore
    private static /* final */ java.util.HashSet<java.lang.String> hudLanguages; // = new java.util.HashSet<>();

    @DexAdd
    final private static String OVERRIDE_SYSTEM_LOCALE = "persist.sys.override_locale";

    // @DexIgnore
    // static class Anon1 implements com.navdy.hud.app.framework.toast.IToastCallback {
    //     android.animation.ObjectAnimator animator;
    //     final /* synthetic */ android.content.res.Resources val$resources;
    //
    //     /* renamed from: com.navdy.hud.app.profile.HudLocale$Anon1$Anon1 reason: collision with other inner class name */
    //     class C0030Anon1 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
    //         C0030Anon1() {
    //         }
    //
    //         public void onAnimationEnd(android.animation.Animator animation) {
    //             if (com.navdy.hud.app.profile.HudLocale.Anon1.this.animator != null) {
    //                 com.navdy.hud.app.profile.HudLocale.Anon1.this.animator.setStartDelay(33);
    //                 com.navdy.hud.app.profile.HudLocale.Anon1.this.animator.start();
    //             }
    //         }
    //     }
    //
    //     @DexIgnore
    //     Anon1(android.content.res.Resources resources) {
    //         this.val$resources = resources;
    //     }
    //
    //     @DexIgnore
    //     public void onStart(com.navdy.hud.app.view.ToastView view) {
    //         com.navdy.hud.app.ui.component.ConfirmationLayout lyt = view.getView();
    //         android.view.ViewGroup.MarginLayoutParams layoutParams = (android.view.ViewGroup.MarginLayoutParams) lyt.screenImage.getLayoutParams();
    //         layoutParams.width = this.val$resources.getDimensionPixelSize(com.navdy.hud.app.R.dimen.locale_change_icon_size);
    //         layoutParams.height = layoutParams.width;
    //         lyt.title1.setVisibility(8);
    //         lyt.title3.setVisibility(8);
    //         lyt.title4.setVisibility(8);
    //         ((android.widget.FrameLayout.LayoutParams) lyt.screenImage.getLayoutParams()).gravity = 17;
    //         lyt.findViewById(com.navdy.hud.app.R.id.infoContainer).setPadding(0, 0, 0, 0);
    //         if (this.animator == null) {
    //             this.animator = android.animation.ObjectAnimator.ofFloat(lyt.screenImage, android.view.View.ROTATION, new float[]{360.0f});
    //             this.animator.setDuration(500);
    //             this.animator.setInterpolator(new android.view.animation.AccelerateDecelerateInterpolator());
    //             this.animator.addListener(new com.navdy.hud.app.profile.HudLocale.Anon1.C0030Anon1());
    //         }
    //         if (!this.animator.isRunning()) {
    //             this.animator.start();
    //         }
    //     }
    //
    //     @DexIgnore
    //     public void onStop() {
    //         if (this.animator != null) {
    //             this.animator.removeAllListeners();
    //             this.animator.cancel();
    //         }
    //     }
    //
    //     @DexIgnore
    //     public boolean onKey(com.navdy.hud.app.manager.InputManager.CustomKeyEvent event) {
    //         return false;
    //     }
    //
    //     @DexIgnore
    //     public void executeChoiceItem(int pos, int id) {
    //     }
    // }

    // @DexIgnore
    // static class Anon2 implements java.lang.Runnable {
    //     @DexIgnore
    //     Anon2() {
    //     }
    //
    //     @DexIgnore
    //     public void run() {
    //         com.navdy.hud.app.profile.HudLocale.killSelfAndMapEngine();
    //     }
    // }

    /* @DexIgnore
    static {
        hudLanguages.add(DEFAULT_LANGUAGE);
        hudLanguages.add("fr");
        hudLanguages.add("de");
        hudLanguages.add("it");
        hudLanguages.add("es");
    } */

    @DexIgnore
    public static boolean isLocaleSupported(java.util.Locale locale) {
        if (locale == null) {
            return false;
        }
        return hudLanguages.contains(getBaseLanguage(locale.getLanguage()));
    }

    @DexIgnore
    public static android.content.Context onAttach(android.content.Context context) {
        return setLocale(context, getCurrentLocale(context));
    }

    // @DexIgnore
    // private static android.content.Context setLocale(android.content.Context context, java.util.Locale locale) {
    //     android.util.Log.e(TAG, "setLocale [" + locale + "]");
    //     if (android.os.Build.VERSION.SDK_INT >= 24) {
    //         return updateResources(context, locale);
    //     }
    //     return updateResourcesLegacy(context, locale);
    // }

    @DexAdd
    public static void setOverrideLocale(Locale locale) {
        if (locale == null) {
            SettingsManager.global.set(OVERRIDE_SYSTEM_LOCALE, "");
        } else {
            String languageTag = locale.toLanguageTag();
            SettingsManager.global.set(OVERRIDE_SYSTEM_LOCALE, languageTag);
        }
    }

    @DexAdd
    public static String getOverrideLocale() {
        return (String)SettingsManager.global.get(OVERRIDE_SYSTEM_LOCALE, "");
    }

    @DexReplace
    private static Context setLocale(Context context, Locale locale) {
        String languageTag = (String) SettingsManager.global.get(OVERRIDE_SYSTEM_LOCALE, "");
        if (TextUtils.isEmpty(languageTag)) {
            languageTag = locale.toLanguageTag();
        }
        Locale finalLocale = Locale.forLanguageTag(languageTag);
        Log.e(TAG, "setLocale [" + finalLocale + "]");
        if (context.getResources() == null) {
            Log.e(TAG, "setLocale context is broken");
        } else {
            if (Build.VERSION.SDK_INT >= 24) {
                return updateResources(context, finalLocale);
            }
            return updateResourcesLegacy(context, finalLocale);
        }
        return context;
    }

    // @android.annotation.TargetApi(24)
    @DexIgnore
    private static android.content.Context updateResources(android.content.Context context, java.util.Locale locale) {
        java.util.Locale.setDefault(locale);
        android.content.res.Configuration configuration = context.getResources().getConfiguration();
        configuration.setLocale(locale);
        return context.createConfigurationContext(configuration);
    }

    @DexIgnore
    private static android.content.Context updateResourcesLegacy(android.content.Context context, java.util.Locale locale) {
        java.util.Locale.setDefault(locale);
        android.content.res.Resources resources = context.getResources();
        android.content.res.Configuration configuration = resources.getConfiguration();
        configuration.locale = locale;
        resources.updateConfiguration(configuration, resources.getDisplayMetrics());
        return context;
    }

    @DexReplace
    public static java.util.Locale getCurrentLocale(android.content.Context context) {
        try {
            NavdyPreferences preferences = SettingsManager.global;
            String localeId = (String) preferences.get(OVERRIDE_SYSTEM_LOCALE, "");
            if (TextUtils.isEmpty(localeId)) {
                localeId = preferences.getString(SELECTED_LOCALE, "en_US");
            }
            if (localeId.equals(DEFAULT_LANGUAGE)) {
                localeId = "en_US";
            }
            return getLocaleForID(localeId);
        } catch (Throwable t) {
            android.util.Log.e(TAG, "getCurrentLocale", t);
            return getLocaleForID("en_US");
        }
    }

    @DexReplace
    private static void setCurrentLocale(android.content.Context context, java.util.Locale locale) {
        NavdyPreferences preferences = SettingsManager.global;
        preferences.putString(SELECTED_LOCALE, locale.toString());
        preferences.apply_immediate();
    }

    @DexIgnore
    public static boolean switchLocale(android.content.Context context, java.util.Locale locale) {
        throw null;
        // try {
        //     com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        //     if (!isLocaleSupported(locale)) {
        //         android.util.Log.e(TAG, "switchLocale hud does not support locale:" + locale);
        //         return false;
        //     }
        //     java.util.Locale currentLocale = getCurrentLocale(context);
        //     if (locale.equals(currentLocale)) {
        //         android.util.Log.e(TAG, "switchLocale language not changed:" + locale);
        //         return false;
        //     }
        //     setCurrentLocale(context, locale);
        //     android.util.Log.e(TAG, "switchLocale changed language from [" + currentLocale + "] to [" + locale + "], restarting hud app");
        //     return true;
        // } catch (Throwable t) {
        //     android.util.Log.e(TAG, "switchLocale", t);
        //     return false;
        // }
    }

    @DexIgnore
    public static void showLocaleNotSupportedToast(java.lang.String language) {
        // dismissToast();
        // android.content.res.Resources resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        // android.os.Bundle bundle = new android.os.Bundle();
        // bundle.putBoolean(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_DEFAULT_CHOICE, true);
        // bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_IMAGE, com.navdy.hud.app.R.drawable.icon_not_supported);
        // bundle.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_2, resources.getString(com.navdy.hud.app.R.string.locale_not_supported, new java.lang.Object[]{language}));
        // bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_2_STYLE, com.navdy.hud.app.R.style.Glances_1);
        // com.navdy.hud.app.framework.toast.ToastManager.getInstance().addToast(new com.navdy.hud.app.framework.toast.ToastManager.ToastParams(LOCALE_TOAST_ID, bundle, null, true, true));
    }

    @DexIgnore
    public static void dismissToast() {
        // com.navdy.hud.app.framework.toast.ToastManager.getInstance().dismissToast(LOCALE_TOAST_ID);
    }

    @DexIgnore
    public static void showLocaleChangeToast() {
        // com.navdy.hud.app.framework.toast.ToastManager toastManager = com.navdy.hud.app.framework.toast.ToastManager.getInstance();
        // android.content.res.Resources resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        // android.os.Bundle bundle = new android.os.Bundle();
        // toastManager.clearAllPendingToast();
        // toastManager.disableToasts(false);
        // toastManager.dismissCurrentToast(LOCALE_TOAST_ID);
        // bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_IMAGE, com.navdy.hud.app.R.drawable.icon_sm_spinner);
        // bundle.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_2, resources.getString(com.navdy.hud.app.R.string.updating_language));
        // bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_2_STYLE, com.navdy.hud.app.R.style.Glances_1);
        // toastManager.addToast(new com.navdy.hud.app.framework.toast.ToastManager.ToastParams(LOCALE_TOAST_ID, bundle, new com.navdy.hud.app.profile.HudLocale.Anon1(resources), true, true));
        // new android.os.Handler(android.os.Looper.getMainLooper()).postDelayed(new com.navdy.hud.app.profile.HudLocale.Anon2(), 2000);
    }

    @DexIgnore
    private static void killSelfAndMapEngine() {
        java.util.Iterator it = ((android.app.ActivityManager) com.navdy.hud.app.HudApplication.getAppContext().getSystemService(Context.ACTIVITY_SERVICE)).getRunningAppProcesses().iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            android.app.ActivityManager.RunningAppProcessInfo processInfo = (android.app.ActivityManager.RunningAppProcessInfo) it.next();
            if (processInfo.processName.equals(MAP_ENGINE_PROCESS_NAME)) {
                android.os.Process.killProcess(processInfo.pid);
                break;
            }
        }
        java.lang.System.exit(0);
    }

    @DexIgnore
    public static java.lang.String getBaseLanguage(java.lang.String language) {
        int index = 0;
        while (index < language.length()) {
            char c = language.charAt(index);
            if (c == '_' || c == '-') {
                break;
            }
            index++;
        }
        if (index <= 0 || index >= language.length()) {
            return language;
        }
        return language.substring(0, index);
    }

    @DexIgnore
    public static java.util.Locale getLocaleForID(java.lang.String localeID) {
        if (android.text.TextUtils.isEmpty(localeID)) {
            return null;
        }
        int index = localeID.indexOf("_");
        if (index < 0) {
            return new java.util.Locale(localeID);
        }
        java.lang.String language = localeID.substring(0, index);
        java.lang.String localeID2 = localeID.substring(index + 1);
        int index2 = localeID2.indexOf("_");
        if (index2 > 0) {
            return new java.util.Locale(language, localeID2.substring(0, index2), localeID2.substring(index2 + 1));
        }
        return new java.util.Locale(language, localeID2);
    }
}
