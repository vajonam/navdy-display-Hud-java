package com.navdy.hud.app.profile;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.hud.app.common.TimeHelper;
import com.navdy.hud.app.event.DriverProfileChanged;
import com.navdy.hud.app.event.DriverProfileUpdated;
import com.navdy.hud.app.event.InitEvents;
import com.navdy.hud.app.event.RemoteEvent;
import com.navdy.hud.app.framework.contacts.PhoneImageDownloader;
import com.navdy.hud.app.service.ConnectionHandler;
import com.navdy.hud.app.storage.PathManager;
import com.navdy.hud.app.util.GenericUtil;
import com.navdy.service.library.device.NavdyDeviceId;
import com.navdy.service.library.events.RequestStatus;
import com.navdy.service.library.events.glances.CannedMessagesRequest;
import com.navdy.service.library.events.glances.CannedMessagesUpdate;
import com.navdy.service.library.events.photo.PhotoType;
import com.navdy.service.library.events.preferences.AudioPreferencesRequest;
import com.navdy.service.library.events.preferences.AudioPreferencesUpdate;
import com.navdy.service.library.events.preferences.DisplaySpeakerPreferencesUpdate;
import com.navdy.service.library.events.preferences.DriverProfilePreferences;
import com.navdy.service.library.events.preferences.DriverProfilePreferencesRequest;
import com.navdy.service.library.events.preferences.DriverProfilePreferencesUpdate;
import com.navdy.service.library.events.preferences.InputPreferencesRequest;
import com.navdy.service.library.events.preferences.InputPreferencesUpdate;
import com.navdy.service.library.events.preferences.LocalPreferences;
import com.navdy.service.library.events.preferences.NavigationPreferences;
import com.navdy.service.library.events.preferences.NavigationPreferencesRequest;
import com.navdy.service.library.events.preferences.NavigationPreferencesUpdate;
import com.navdy.service.library.events.preferences.NotificationPreferencesRequest;
import com.navdy.service.library.events.preferences.NotificationPreferencesUpdate;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.squareup.otto.Bus;
import com.squareup.wire.Message;
import com.squareup.wire.Wire;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Objects;
import java.util.Set;

import lanchon.dexpatcher.annotation.DexAdd;
import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit
public class DriverProfileManager {
    @DexIgnore
    private static /* final */ String DEFAULT_PROFILE_NAME; // = "DefaultProfile";
    @DexIgnore
    private static /* final */ DriverProfileChanged DRIVER_PROFILE_CHANGED; // = new com.navdy.hud.app.event.DriverProfileChanged();
    @DexIgnore
    private static /* final */ String LOCAL_PREFERENCES_SUFFIX; // = "_preferences";
    @DexIgnore
    private static /* final */ NavdyDeviceId MOCK_PROFILE_DEVICE_ID; // = com.navdy.service.library.device.NavdyDeviceId.UNKNOWN_ID;
    @DexIgnore
    private static /* final */ Logger sLogger; // = new com.navdy.service.library.log.Logger(com.navdy.hud.app.profile.DriverProfileManager.class);
    @DexIgnore
    private String lastUserEmail;
    @DexIgnore
    private String lastUserName;
    @DexIgnore
    private HashMap<String, DriverProfile> mAllProfiles; // = new java.util.HashMap<>();
    @DexIgnore
    private Bus mBus;
    @DexIgnore
    private DriverProfile mCurrentProfile;
    @DexIgnore
    private String mProfileDirectoryName;
    @DexIgnore
    private Set<DriverProfile> mPublicProfiles; // = new java.util.HashSet();
    @DexIgnore
    private DriverSessionPreferences mSessionPrefs;
    @DexIgnore
    private DriverProfile theDefaultProfile;

    // @DexIgnore
    // class Anon1 implements FileFilter {
    //     @DexIgnore
    //     Anon1() {
    //     }
    //
    //     @DexIgnore
    //     public boolean accept(File pathname) {
    //         return pathname.isDirectory();
    //     }
    // }
    //
    // @DexIgnore
    // class Anon10 implements Runnable {
    //     final /* synthetic */ NotificationPreferencesUpdate val$update;
    //
    //     @DexIgnore
    //     Anon10(NotificationPreferencesUpdate notificationPreferencesUpdate) {
    //         this.val$update = notificationPreferencesUpdate;
    //     }
    //
    //     @DexIgnore
    //     public void run() {
    //         // DriverProfileManager.this.mCurrentProfile.setNotificationPreferences(this.val$update.preferences);
    //         // DriverProfileManager.this.theDefaultProfile.setNotificationPreferences(this.val$update.preferences);
    //         // AnalyticsSupport.recordPreferenceChange(DriverProfileManager.this.mCurrentProfile);
    //         // DriverProfileManager.this.mBus.post(DriverProfileManager.this.mCurrentProfile.getNotificationPreferences());
    //     }
    // }
    //
    // @DexIgnore
    // class Anon11 implements Runnable {
    //     final /* synthetic */ CannedMessagesUpdate val$update;
    //
    //     @DexIgnore
    //     Anon11(CannedMessagesUpdate cannedMessagesUpdate) {
    //         this.val$update = cannedMessagesUpdate;
    //     }
    //
    //     @DexIgnore
    //     public void run() {
    //         DriverProfileManager.this.mCurrentProfile.setCannedMessages(this.val$update);
    //     }
    // }
    //
    // @DexIgnore
    // class Anon12 implements Runnable {
    //     final /* synthetic */ LocalPreferences val$preferences;
    //
    //     @DexIgnore
    //     Anon12(LocalPreferences localPreferences) {
    //         this.val$preferences = localPreferences;
    //     }
    //
    //     @DexIgnore
    //     public void run() {
    //         // DriverProfileManager.this.mCurrentProfile.setLocalPreferences(this.val$preferences);
    //         // DriverProfileManager.this.theDefaultProfile.setLocalPreferences(this.val$preferences);
    //         // AnalyticsSupport.recordPreferenceChange(DriverProfileManager.this.mCurrentProfile);
    //         // DriverProfileManager.this.mBus.post(this.val$preferences);
    //     }
    // }
    //
    // @DexIgnore
    // class Anon13 implements Runnable {
    //     @DexIgnore
    //     Anon13() {
    //     }
    //
    //     @DexIgnore
    //     public void run() {
    //         DriverProfileManager.sLogger.v("copyCurrentProfileToDefault");
    //         DriverProfileManager.this.theDefaultProfile.copy(DriverProfileManager.this.mCurrentProfile);
    //     }
    // }
    //
    // @DexIgnore
    // class Anon2 implements Runnable {
    //     final /* synthetic */ NavdyDeviceId val$deviceId;
    //
    //     @DexIgnore
    //     Anon2(NavdyDeviceId navdyDeviceId) {
    //         this.val$deviceId = navdyDeviceId;
    //     }
    //
    //     @DexIgnore
    //     public void run() {
    //         DriverProfileManager.sLogger.v("loading profile:" + this.val$deviceId);
    //         DriverProfile profile = DriverProfileManager.this.getProfileForId(this.val$deviceId);
    //         if (profile == null) {
    //             DriverProfileManager.sLogger.w("profile not loaded:" + this.val$deviceId);
    //             profile = DriverProfileManager.this.createProfileForId(this.val$deviceId);
    //         }
    //         DriverProfileManager.this.setCurrentProfile(profile);
    //     }
    // }
    //
    // @DexIgnore
    // class Anon3 implements Runnable {
    //     @DexIgnore
    //     Anon3() {
    //     }
    //
    //     @DexIgnore
    //     public void run() {
    //         DriverProfileManager.this.changeLocale(false);
    //         DriverProfileManager.this.mBus.post(new DriverProfileUpdated(DriverProfileUpdated.State.UP_TO_DATE));
    //     }
    // }
    //
    // @DexIgnore
    // class Anon4 implements Runnable {
    //     final /* synthetic */ DriverProfilePreferences val$preferences;
    //
    //     @DexIgnore
    //     Anon4(DriverProfilePreferences driverProfilePreferences) {
    //         this.val$preferences = driverProfilePreferences;
    //     }
    //
    //     @DexIgnore
    //     public void run() {
    //         DriverProfileManager.this.refreshProfileImageIfNeeded(this.val$preferences.photo_checksum);
    //     }
    // }
    //
    // @DexIgnore
    // class Anon5 implements Runnable {
    //     final /* synthetic */ DriverProfilePreferences val$preferences;
    //
    //     @DexIgnore
    //     Anon5(DriverProfilePreferences driverProfilePreferences) {
    //         this.val$preferences = driverProfilePreferences;
    //     }
    //
    //     @DexIgnore
    //     public void run() {
    //         DriverProfilePreferences driverProfilePreferences = DriverProfileManager.this.findSupportedLocale(this.val$preferences);
    //         Locale oldLocale = DriverProfileManager.this.mCurrentProfile.getLocale();
    //         DriverProfileManager.this.mCurrentProfile.setDriverProfilePreferences(driverProfilePreferences);
    //         DriverProfileManager.this.theDefaultProfile.setDriverProfilePreferences(driverProfilePreferences);
    //         Locale newLocale = DriverProfileManager.this.mCurrentProfile.getLocale();
    //         boolean localeChanged = !oldLocale.equals(newLocale);
    //         DriverProfileManager.sLogger.i("[HUD-locale] changed[" + localeChanged + "] current[" + oldLocale + "] new[" + newLocale + "]");
    //         DriverProfileManager.this.changeLocale(localeChanged);
    //         DriverProfileManager.this.mBus.post(new DriverProfileUpdated(DriverProfileUpdated.State.UPDATED));
    //     }
    // }
    //
    // @DexIgnore
    // class Anon6 implements Runnable {
    //     final /* synthetic */ NavigationPreferencesUpdate val$update;
    //
    //     @DexIgnore
    //     Anon6(NavigationPreferencesUpdate navigationPreferencesUpdate) {
    //         this.val$update = navigationPreferencesUpdate;
    //     }
    //
    //     @DexIgnore
    //     public void run() {
    //         // DriverProfileManager.this.mCurrentProfile.setNavigationPreferences(this.val$update.preferences);
    //         // DriverProfileManager.this.theDefaultProfile.setNavigationPreferences(this.val$update.preferences);
    //         // NavigationPreferences preferences = DriverProfileManager.this.mCurrentProfile.getNavigationPreferences();
    //         // AnalyticsSupport.recordNavigationPreferenceChange(DriverProfileManager.this.mCurrentProfile);
    //         // DriverProfileManager.this.mSessionPrefs.getNavigationSessionPreference().setDefault(preferences);
    //         // DriverProfileManager.this.mBus.post(preferences);
    //     }
    // }
    //
    // @DexIgnore
    // class Anon7 implements Runnable {
    //     final /* synthetic */ AudioPreferencesUpdate val$update;
    //
    //     @DexIgnore
    //     Anon7(AudioPreferencesUpdate audioPreferencesUpdate) {
    //         this.val$update = audioPreferencesUpdate;
    //     }
    //
    //     @DexIgnore
    //     public void run() {
    //         // DriverProfileManager.this.mCurrentProfile.setAudioPreferences(this.val$update.preferences);
    //         // DriverProfileManager.this.theDefaultProfile.setAudioPreferences(this.val$update.preferences);
    //         // AnalyticsSupport.recordPreferenceChange(DriverProfileManager.this.mCurrentProfile);
    //         // DriverProfileManager.this.mBus.post(DriverProfileManager.this.mCurrentProfile.getAudioPreferences());
    //     }
    // }
    //
    // @DexIgnore
    // class Anon8 implements Runnable {
    //     final /* synthetic */ InputPreferencesUpdate val$update;
    //
    //     @DexIgnore
    //     Anon8(InputPreferencesUpdate inputPreferencesUpdate) {
    //         this.val$update = inputPreferencesUpdate;
    //     }
    //
    //     @DexIgnore
    //     public void run() {
    //         // DriverProfileManager.this.mCurrentProfile.setInputPreferences(this.val$update.preferences);
    //         // DriverProfileManager.this.theDefaultProfile.setInputPreferences(this.val$update.preferences);
    //         // AnalyticsSupport.recordPreferenceChange(DriverProfileManager.this.mCurrentProfile);
    //         // DriverProfileManager.this.mBus.post(DriverProfileManager.this.mCurrentProfile.getInputPreferences());
    //     }
    // }
    //
    // @DexIgnore
    // class Anon9 implements Runnable {
    //     final /* synthetic */ DisplaySpeakerPreferencesUpdate val$update;
    //
    //     @DexIgnore
    //     Anon9(DisplaySpeakerPreferencesUpdate displaySpeakerPreferencesUpdate) {
    //         this.val$update = displaySpeakerPreferencesUpdate;
    //     }
    //
    //     @DexIgnore
    //     public void run() {
    //         // DriverProfileManager.this.mCurrentProfile.setSpeakerPreferences(this.val$update.preferences);
    //         // DriverProfileManager.this.theDefaultProfile.setSpeakerPreferences(this.val$update.preferences);
    //         // AnalyticsSupport.recordPreferenceChange(DriverProfileManager.this.mCurrentProfile);
    //         // DriverProfileManager.this.mBus.post(DriverProfileManager.this.mCurrentProfile.getSpeakerPreferences());
    //     }
    // }

    // @javax.inject.Inject
    @DexIgnore
    public DriverProfileManager(Bus bus, PathManager pathManager, TimeHelper timeHelper) {
        // this.mProfileDirectoryName = pathManager.getDriverProfilesDir();
        // this.mBus = bus;
        // this.mBus.register(this);
        // sLogger.i("initializing in " + this.mProfileDirectoryName);
        // File[] profileDirectories = new File(this.mProfileDirectoryName).listFiles(new DriverProfileManager.Anon1());
        // int length = profileDirectories.length;
        // int i = 0;
        // while (i < length) {
        //     File profileDirectory = profileDirectories[i];
        //     try {
        //         DriverProfile profile = new DriverProfile(profileDirectory);
        //         String profileName = profileDirectory.getName();
        //         this.mAllProfiles.put(profileName, profile);
        //         if (profile.isProfilePublic()) {
        //             this.mPublicProfiles.add(profile);
        //         }
        //         if (profileName.equals(profileNameForId(MOCK_PROFILE_DEVICE_ID))) {
        //             this.theDefaultProfile = profile;
        //         }
        //         i++;
        //     } catch (Exception e) {
        //         sLogger.e("could not load profile from " + profileDirectory, e);
        //     }
        // }
        // if (this.theDefaultProfile == null) {
        //     this.theDefaultProfile = createProfileForId(MOCK_PROFILE_DEVICE_ID);
        // }
        // NavigationPreferences navigationPreferences = this.theDefaultProfile.getNavigationPreferences();
        // this.mSessionPrefs = new DriverSessionPreferences(this.mBus, this.theDefaultProfile, timeHelper);
        // setCurrentProfile(this.theDefaultProfile);
    }

    @DexIgnore
    public DriverProfile getCurrentProfile() {
        return this.mCurrentProfile;
    }

    @DexIgnore
    public void loadProfileForId(NavdyDeviceId deviceId) {
        // TaskManager.getInstance().execute(new DriverProfileManager.Anon2(deviceId), 10);
    }

    @DexIgnore
    public void setCurrentProfile(DriverProfile profile) {
        sLogger.i("setting current profile to " + profile);
        if (profile == null) {
            profile = this.theDefaultProfile;
        }
        if (this.mCurrentProfile != profile) {
            this.mCurrentProfile = profile;
            this.mSessionPrefs.setDefault(this.mCurrentProfile, isDefaultProfile(this.mCurrentProfile));
            this.theDefaultProfile.setTrafficEnabled(this.mCurrentProfile.isTrafficEnabled());
            copyCurrentProfileToDefault();
            this.mBus.post(DRIVER_PROFILE_CHANGED);
        }
        this.lastUserName = this.mCurrentProfile.getDriverName();
        this.lastUserEmail = this.mCurrentProfile.getDriverEmail();
        sLogger.v("lastUserEmail:" + this.lastUserEmail);
    }

    @DexIgnore
    public DriverProfile getProfileForId(NavdyDeviceId deviceId) {
        return this.mAllProfiles.get(profileNameForId(deviceId));
    }

    @DexIgnore
    public Collection<DriverProfile> getPublicProfiles() {
        return this.mPublicProfiles;
    }

    @DexIgnore
    public DriverProfile createProfileForId(NavdyDeviceId deviceId) {
        String profileName = profileNameForId(deviceId);
        try {
            DriverProfile newProfile = DriverProfile.createProfileForId(profileName, this.mProfileDirectoryName);
            this.mAllProfiles.put(profileName, newProfile);
            return newProfile;
        } catch (IOException e) {
            sLogger.e("could not create new profile: " + e);
            return null;
        }
    }

    @DexIgnore
    private String profileNameForId(NavdyDeviceId deviceId) {
        String profileName = deviceId.getBluetoothAddress();
        if (profileName == null) {
            profileName = DEFAULT_PROFILE_NAME;
        }
        return GenericUtil.normalizeToFilename(profileName);
    }

    @DexIgnore
    private void requestPreferenceUpdates(DriverProfile profile) {
        sLogger.v("requestPreferenceUpdates:" + profile.getProfileName());
        sendEventToClient(new DriverProfilePreferencesRequest.Builder().serial_number(profile.mDriverProfilePreferences.serial_number).build());
        sLogger.v("requestPreferenceUpdates: nav:" + profile.mNavigationPreferences.serial_number);
        sendEventToClient(new NavigationPreferencesRequest.Builder().serial_number(profile.mNavigationPreferences.serial_number).build());
        sLogger.v("requestPreferenceUpdates: notif:" + profile.mNotificationPreferences.serial_number);
        sendEventToClient(new NotificationPreferencesRequest.Builder().serial_number(profile.mNotificationPreferences.serial_number).build());
        sLogger.v("requestPreferenceUpdates: input:" + profile.mInputPreferences.serial_number);
        sendEventToClient(new InputPreferencesRequest.Builder().serial_number(profile.mInputPreferences.serial_number).build());
        sLogger.v("requestPreferenceUpdates: audio:" + profile.mAudioPreferences.serial_number);
        sendEventToClient(new AudioPreferencesRequest.Builder().serial_number(profile.mAudioPreferences.serial_number).build());
    }

    @DexIgnore
    private void requestUpdates(DriverProfile profile) {
        sLogger.v("requestUpdates(canned-msgs) [" + profile.mCannedMessages.serial_number + "]");
        sendEventToClient(new CannedMessagesRequest.Builder().serial_number(profile.mCannedMessages.serial_number).build());
    }

    @DexIgnore
    private void sendEventToClient(Message message) {
        this.mBus.post(new RemoteEvent(message));
    }

    // @com.squareup.otto.Subscribe
    @DexIgnore
    public void onDriverProfileChange(DriverProfileChanged changed) {
        this.mBus.post(this.mCurrentProfile.getNavigationPreferences());
    }

    // @com.squareup.otto.Subscribe
    @DexIgnore
    public void onDeviceSyncRequired(ConnectionHandler.DeviceSyncEvent event) {
        sLogger.v("calling requestPreferenceUpdates");
        requestPreferenceUpdates(this.mCurrentProfile);
        sLogger.v("calling requestUpdates");
        requestUpdates(this.mCurrentProfile);
    }

    // @com.squareup.otto.Subscribe
    @DexIgnore
    public void onDriverProfilePreferencesUpdate(DriverProfilePreferencesUpdate update) {
        // if (this.mCurrentProfile != this.theDefaultProfile) {
        //     switch (update.status) {
        //         case REQUEST_VERSION_IS_CURRENT:
        //             sLogger.i("prefs are up to date!");
        //             TaskManager.getInstance().execute(new DriverProfileManager.Anon3(), 10);
        //             return;
        //         case REQUEST_SUCCESS:
        //             sLogger.i("Received profile update, applying");
        //             if (this.mCurrentProfile != null) {
        //                 DriverProfilePreferences preferences = update.preferences;
        //                 if (preferences != null) {
        //                     TaskManager.getInstance().execute(new DriverProfileManager.Anon4(preferences), 10);
        //                     if (Wire.get(preferences.profile_is_public, DriverProfilePreferences.DEFAULT_PROFILE_IS_PUBLIC)) {
        //                         this.mPublicProfiles.add(this.mCurrentProfile);
        //                     } else {
        //                         this.mPublicProfiles.remove(this.mCurrentProfile);
        //                     }
        //                     TaskManager.getInstance().execute(new DriverProfileManager.Anon5(preferences), 10);
        //                     return;
        //                 }
        //                 sLogger.i("preferences update with no preferences!");
        //                 return;
        //             }
        //             return;
        //         default:
        //             sLogger.i("profile status was " + update.status + ", which I didn't expect; ignoring.");
        //             return;
        //     }
        // }
    }

    @DexIgnore
    private DriverProfilePreferences findSupportedLocale(DriverProfilePreferences preferences) {
        DriverProfilePreferences driverProfilePreferences = preferences;
        // if (HudLocale.isLocaleSupported(HudLocale.getLocaleForID(preferences.locale)) || driverProfilePreferences.additionalLocales == null) {
        //     return driverProfilePreferences;
        // }
        // Locale supportedAdditionalLocale = null;
        // Iterator it = driverProfilePreferences.additionalLocales.iterator();
        // while (true) {
        //     if (!it.hasNext()) {
        //         break;
        //     }
        //     String additionalLocaleStr = (String) it.next();
        //     if (!TextUtils.isEmpty(additionalLocaleStr)) {
        //         Locale additionalLocale = HudLocale.getLocaleForID(additionalLocaleStr);
        //         if (HudLocale.isLocaleSupported(additionalLocale)) {
        //             sLogger.v("HUD-locale additional locale supported:" + additionalLocale);
        //             supportedAdditionalLocale = additionalLocale;
        //             break;
        //         }
        //         sLogger.v("HUD-locale additional locale not supported:" + additionalLocale);
        //     }
        // }
        // if (supportedAdditionalLocale != null) {
        //     return new DriverProfilePreferences.Builder(driverProfilePreferences).locale(supportedAdditionalLocale.toString()).build();
        // }
        return driverProfilePreferences;
    }

    @DexIgnore
    private void refreshProfileImageIfNeeded(String remoteChecksum) {
        PhoneImageDownloader downloader = PhoneImageDownloader.getInstance();
        String localChecksum = downloader.hashFor(DriverProfile.DRIVER_PROFILE_IMAGE, PhotoType.PHOTO_DRIVER_PROFILE);
        if (!Objects.equals(remoteChecksum, localChecksum)) {
            sLogger.d("asking for new photo - local:" + localChecksum + " remote:" + remoteChecksum);
            downloader.submitDownload(DriverProfile.DRIVER_PROFILE_IMAGE, PhoneImageDownloader.Priority.NORMAL, PhotoType.PHOTO_DRIVER_PROFILE, null);
        }
    }

    // @com.squareup.otto.Subscribe
    @DexIgnore
    public void onNavigationPreferencesUpdate(NavigationPreferencesUpdate update) {
        // handleUpdateInBackground(update, update.status, update.preferences, new DriverProfileManager.Anon6(update));
    }

    // @com.squareup.otto.Subscribe
    @DexIgnore
    public void onAudioPreferencesUpdate(AudioPreferencesUpdate update) {
        // handleUpdateInBackground(update, update.status, update.preferences, new DriverProfileManager.Anon7(update));
    }

    // @com.squareup.otto.Subscribe
    @DexIgnore
    public void onInputPreferencesUpdate(InputPreferencesUpdate update) {
        // handleUpdateInBackground(update, update.status, update.preferences, new DriverProfileManager.Anon8(update));
    }

    // @com.squareup.otto.Subscribe
    @DexIgnore
    public void onDisplaySpeakerPreferencesUpdate(DisplaySpeakerPreferencesUpdate update) {
        // handleUpdateInBackground(update, update.status, update.preferences, new DriverProfileManager.Anon9(update));
    }

    // @com.squareup.otto.Subscribe
    @DexIgnore
    public void onNotificationPreferencesUpdate(NotificationPreferencesUpdate update) {
        // handleUpdateInBackground(update, update.status, update.preferences, new DriverProfileManager.Anon10(update));
    }

    // @com.squareup.otto.Subscribe
    @DexIgnore
    public void onCannedMessagesUpdate(CannedMessagesUpdate update) {
        // handleUpdateInBackground(update, update.status, update, new DriverProfileManager.Anon11(update));
    }

    @DexIgnore
    public void updateLocalPreferences(LocalPreferences preferences) {
        // if (this.mCurrentProfile != null) {
        //     TaskManager.getInstance().execute(new DriverProfileManager.Anon12(preferences), 10);
        // }
    }

    @DexIgnore
    public LocalPreferences getLocalPreferences() {
        return this.mCurrentProfile.getLocalPreferences();
    }

    @DexIgnore
    private void handleUpdateInBackground(Message request, RequestStatus status, Message preferences, Runnable runnable) {
        if (this.mCurrentProfile != this.theDefaultProfile) {
            String requestType = request.getClass().getSimpleName();
            switch (status) {
                case REQUEST_VERSION_IS_CURRENT:
                    sLogger.i(requestType + " returned up to date!");
                    return;
                case REQUEST_SUCCESS:
                    sLogger.i("Received " + requestType + ", applying");
                    if (this.mCurrentProfile == null) {
                        return;
                    }
                    if (preferences != null) {
                        TaskManager.getInstance().execute(runnable, 10);
                        return;
                    } else {
                        sLogger.i(requestType + " with no preferences!");
                        return;
                    }
                default:
                    sLogger.i("profile status for " + requestType + " was " + status + ", which I didn't expect; ignoring.");
                    return;
            }
        }
    }

    // @com.squareup.otto.Subscribe
    @DexIgnore
    public void onPhotoDownload(PhoneImageDownloader.PhotoDownloadStatus photoStatus) {
        if (photoStatus.photoType != PhotoType.PHOTO_DRIVER_PROFILE || photoStatus.alreadyDownloaded || this.mCurrentProfile == this.theDefaultProfile) {
            return;
        }
        if (photoStatus.success) {
            sLogger.d("applying new photo");
        } else {
            sLogger.i("driver photo not downloaded");
        }
    }

    /* access modifiers changed from: 0000 */
    @DexIgnore
    public boolean isDefaultProfile(DriverProfile profile) {
        return profile == this.theDefaultProfile;
    }

    @DexAdd
    public NavdyPreferences getLocalPreferencesForDriverProfile(Context context, DriverProfile profile) {
        return new NavdyPreferences("profile_" + profile.getProfileName().replace(" ", "_").toLowerCase());
    }

    @DexAdd
    public NavdyPreferences getLocalPreferencesForCurrentDriverProfile(Context context) {
        return getLocalPreferencesForDriverProfile(context, getCurrentProfile());
    }

    @DexIgnore
    public DriverSessionPreferences getSessionPreferences() {
        return this.mSessionPrefs;
    }

    @DexIgnore
    public boolean isManualZoom() {
        LocalPreferences localPreferences = getLocalPreferences();
        return localPreferences != null && Boolean.TRUE.equals(localPreferences.manualZoom);
    }

    @DexIgnore
    public Locale getCurrentLocale() {
        return this.mCurrentProfile.getLocale();
    }

    @DexIgnore
    public String getLastUserName() {
        return this.lastUserName;
    }

    @DexIgnore
    public String getLastUserEmail() {
        return this.lastUserEmail;
    }

    @DexIgnore
    public void enableTraffic() {
        this.mCurrentProfile.setTrafficEnabled(true);
        this.theDefaultProfile.setTrafficEnabled(true);
    }

    @DexIgnore
    public void disableTraffic() {
        this.mCurrentProfile.setTrafficEnabled(false);
        this.theDefaultProfile.setTrafficEnabled(false);
    }

    @DexIgnore
    public boolean isTrafficEnabled() {
        return this.mCurrentProfile.isTrafficEnabled();
    }

    @DexIgnore
    private void copyCurrentProfileToDefault() {
        // if (this.mCurrentProfile != this.theDefaultProfile) {
        //     TaskManager.getInstance().execute(new DriverProfileManager.Anon13(), 10);
        // }
    }

    @DexIgnore
    private void changeLocale(boolean showLanguageNotSupportedToast) {
        // try {
        //     Locale locale = getCurrentLocale();
        //     sLogger.v("[HUD-locale] changeLocale current=" + locale);
        //     if (HudLocale.switchLocale(HudApplication.getAppContext(), locale)) {
        //         sLogger.v("[HUD-locale] change, restarting...");
        //         this.mBus.post(new InitEvents.InitPhase(InitEvents.Phase.SWITCHING_LOCALE));
        //         HudLocale.showLocaleChangeToast();
        //         return;
        //     }
        //     sLogger.v("[HUD-locale] not changed");
        //     this.mBus.post(new InitEvents.InitPhase(InitEvents.Phase.LOCALE_UP_TO_DATE));
        //     if (HudLocale.isLocaleSupported(locale)) {
        //         HudLocale.dismissToast();
        //     } else if (showLanguageNotSupportedToast) {
        //         HudLocale.showLocaleNotSupportedToast(locale.getDisplayLanguage());
        //     }
        // } catch (Throwable t) {
        //     sLogger.e("[HUD-locale] changeLocale", t);
        // }
    }
}
