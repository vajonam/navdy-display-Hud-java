package com.navdy.hud.app.profile;

import android.os.Handler;
import android.os.HandlerThread;
import android.util.ArrayMap;

import com.navdy.service.library.log.Logger;
import com.navdy.service.library.util.IOUtils;

import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.Yaml;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

import lanchon.dexpatcher.annotation.DexIgnore;

public class NavdyPreferences {
    private static final Logger sLogger = new Logger(DriverProfileManager.class);
    public static final String SETTINGS = "/settings";

    private static final Object fileLock = new Object();
    private static final Map<String, ReentrantLock> lock = new ArrayMap<>();
    private static final Map<String, LinkedHashMap<String, Object>> stores = new ArrayMap<>();

    private LinkedHashMap<String, Object> store;
    private String filename;

    private List<OnNavdyPreferenceChangeListener> listeners;

    private Handler handler;
    private HandlerThread handlerThread;

    public boolean modified;


    public NavdyPreferences(String filename) {
        String directoryName = com.navdy.hud.app.storage.PathManager.getInstance().getMapsPartitionPath() + SETTINGS;

        File directory = new File(directoryName);
        if (!directory.exists()) {
            //noinspection ResultOfMethodCallIgnored
            directory.mkdir();
        }

        this.filename = directoryName + "/" + filename + ".yaml";

        ReentrantLock flock;
        synchronized (fileLock) {
            if (!lock.containsKey(this.filename)) {
                this.lock.put(this.filename, new ReentrantLock());
            }

            flock = this.lock.get(this.filename);
        }

        try {
            if (flock.tryLock(10L, TimeUnit.SECONDS)) {
                try {

                    if (stores.containsKey(this.filename)) {
                        this.store = stores.get(this.filename);
                    } else {

                        FileInputStream fileInputStream = null;
                        try {
                            fileInputStream = new FileInputStream(this.filename);

                            Yaml yaml = new Yaml();
                            this.store = yaml.load(fileInputStream);

                        } catch (FileNotFoundException e) {
                            sLogger.v("Settings file doesn't exist yet: " + this.filename);
                        } catch (Exception e) {
                            sLogger.e("Exception with " + this.filename, e);
                        } finally {
                            IOUtils.closeObject(fileInputStream);
                        }

                        if (this.store == null) {
                            this.store = new LinkedHashMap<>();
                        }

                        stores.put(this.filename, this.store);
                    }

                    listeners = new ArrayList<>();

                } finally {
                    flock.unlock();
                }
            } else {
                sLogger.e("NavdyPreferences lock blocked");
            }
        } catch (InterruptedException ex) {
            sLogger.e("NavdyPreferences Interrupted");
        }

        this.handlerThread = new HandlerThread("gpsDeadReckoningHandler");
        this.handlerThread.start();
        this.handler = new Handler(this.handlerThread.getLooper());
    }

    private void write() {
        try {
            ReentrantLock flock = lock.get(this.filename);
            try {
                if (flock.tryLock(10L, TimeUnit.SECONDS)) {
                    FileWriter writer = null;
                    try {
                        writer = new FileWriter(this.filename);

                        DumperOptions options = new DumperOptions();
                        options.setDefaultFlowStyle(DumperOptions.FlowStyle.BLOCK);
                        options.setIndent(4);

                        Yaml yaml = new Yaml(options);
                        yaml.dump(store, writer);

                    } finally {
                        IOUtils.closeObject(writer);
                        flock.unlock();
                    }
                } else {
                    sLogger.e("NavdyPreferences lock blocked");
                }
            } catch (InterruptedException ex) {
                sLogger.e("NavdyPreferences Interrupted");
            }
        } catch (IOException e) {
            sLogger.e("IO Exception ", e);
        }
    }

    public NavdyPreferences apply() {
        this.handler.removeCallbacksAndMessages(null);
        this.handler.postDelayed(this::write, 10000);
        return this;
    }

    public NavdyPreferences apply_immediate() {
        this.handler.removeCallbacksAndMessages(null);
        this.write();
        return this;
    }

    public NavdyPreferences commit() {
        return apply();
    }

    public NavdyPreferences edit() {
        return this;
    }

    public NavdyPreferences put(String key, Object val) {
        store.put(key, val);
        for (OnNavdyPreferenceChangeListener listener: listeners)
        {
            listener.onNavdyPreferenceChanged(this, key);
        }
        modified = true;
        return this;
    }

    public NavdyPreferences putInt(String key, Object val) {
        return put(key, val);
    }

    public NavdyPreferences putLong(String key, Object val) {
        return put(key, val);
    }

    public NavdyPreferences putBoolean(String key, Object val) {
        return put(key, val);
    }

    public NavdyPreferences putFloat(String key, Object val) {
        return put(key, val);
    }

    public NavdyPreferences putString(String key, Object val) {
        return put(key, val);
    }

    public Map<String, Object> getAll() {
        return store;
    }

    public Object get(String key, Object def) {
        if (store.containsKey(key)) {
            return store.get(key);
        } else {
            put(key, def).apply();
            return get(key, def);
        }
    }

    public Integer getInt(String key, Integer def) {
        return (int) get(key, def);
    }

    public Long getLong(String key, long def) {
        try {
            return Long.valueOf((long) get(key, def));
        } catch (java.lang.ClassCastException ex) {
            return Long.valueOf((int) get(key, def));
        }
    }

    public Boolean getBoolean(String key, Boolean def) {
        return (Boolean) get(key, def);
    }

    public Float getFloat(String key, Float def) {
        try {
            return (Float) get(key, def);
        } catch (ClassCastException ex) {
            return ((Double) get(key, def)).floatValue();
        }
    }

    public String getString(String key, String def) {
        return String.valueOf(get(key, def));
    }

    public NavdyPreferences remove(String key) {
        store.remove(key);
        return this;
    }

    public interface OnNavdyPreferenceChangeListener {
        void onNavdyPreferenceChanged(NavdyPreferences prefs, String key);
    }

    public void registerOnSharedPreferenceChangeListener(OnNavdyPreferenceChangeListener listener) {
        listeners.add(listener);
    }

    public void unregisterOnSharedPreferenceChangeListener(OnNavdyPreferenceChangeListener listener) {
        listeners.remove(listener);
    }

    public void set(String key, Object val) {
        putString(key, val);
    }

}
