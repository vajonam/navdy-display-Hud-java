package com.navdy.hud.app.device.gps;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.os.SystemClock;
import android.os.UserHandle;

import com.glympse.android.lib.StaticConfig;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.config.SettingsManager;
import com.navdy.hud.app.debug.RouteRecorder;
import com.navdy.hud.app.service.HudConnectionService;
import com.navdy.service.library.events.DeviceInfo;
import com.navdy.service.library.events.audio.SpeechRequest;
import com.navdy.service.library.events.location.Coordinate;
import com.navdy.service.library.events.location.TransmitLocation;
import com.navdy.service.library.log.Logger;

import java.util.ArrayList;

import lanchon.dexpatcher.annotation.DexAction;
import lanchon.dexpatcher.annotation.DexAdd;
import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;

@DexEdit(staticConstructorAction = DexAction.APPEND)
public class GpsManager {
    @DexIgnore
    private static /* final */ int ACCEPTABLE_THRESHOLD; // = 2;
    @DexIgnore
    private static /* final */ String DISCARD_TIME; // = "DISCARD_TIME";
    @DexIgnore
    private static /* final */ String GPS_SYSTEM_PROPERTY; // = "persist.sys.hud_gps";
    @DexIgnore
    public static /* final */ long INACCURATE_GPS_REPORT_INTERVAL; // = java.util.concurrent.TimeUnit.SECONDS.toMillis(30);
    @DexIgnore
    private static /* final */ int INITIAL_INTERVAL; // = 15000;
    @DexIgnore
    private static /* final */ int KEEP_PHONE_GPS_ON; // = -1;
    @DexIgnore
    private static /* final */ int LOCATION_ACCURACY_THROW_AWAY_THRESHOLD; // = 30;
    @DexIgnore
    private static /* final */ int LOCATION_TIME_THROW_AWAY_THRESHOLD; // = 2000;
    @DexIgnore
    public static /* final */ double MAXIMUM_STOPPED_SPEED; // = 0.22353333333333333d;
    @DexIgnore
    private static /* final */ double METERS_PER_MILE; // = 1609.44d;
    @DexIgnore
    public static /* final */ long MINIMUM_DRIVE_TIME; // = 3000;
    @DexIgnore
    public static /* final */ double MINIMUM_DRIVING_SPEED; // = 2.2353333333333336d;
    @DexIgnore
    static /* final */ int MSG_NMEA; // = 1;
    @DexIgnore
    static /* final */ int MSG_SATELLITE_STATUS; // = 2;
    @DexIgnore
    private static /* final */ int SECONDS_PER_HOUR; // = 3600;
    @DexIgnore
    private static /* final */ String SET_LOCATION_DISCARD_TIME_THRESHOLD; // = "SET_LOCATION_DISCARD_TIME_THRESHOLD";
    @DexIgnore
    private static /* final */ String SET_UBLOX_ACCURACY; // = "SET_UBLOX_ACCURACY";
    @DexIgnore
    private static /* final */ TransmitLocation START_SENDING_LOCATION; // = new com.navdy.service.library.events.location.TransmitLocation(java.lang.Boolean.valueOf(true));
    @DexIgnore
    private static /* final */ String START_UBLOX; // = "START_REPORTING_UBLOX_LOCATION";
    @DexIgnore
    private static /* final */ TransmitLocation STOP_SENDING_LOCATION; // = new com.navdy.service.library.events.location.TransmitLocation(java.lang.Boolean.valueOf(false));
    @DexIgnore
    private static /* final */ String STOP_UBLOX; // = "STOP_REPORTING_UBLOX_LOCATION";
    @DexIgnore
    private static /* final */ String TAG_GPS; // = "[Gps-i] ";
    @DexIgnore
    private static /* final */ String TAG_GPS_LOC; // = "[Gps-loc] ";
    @DexIgnore
    private static /* final */ String TAG_GPS_LOC_NAVDY; // = "[Gps-loc-navdy] ";
    @DexIgnore
    private static /* final */ String TAG_GPS_NMEA; // = "[Gps-nmea] ";
    @DexIgnore
    private static /* final */ String TAG_GPS_STATUS; // = "[Gps-stat] ";
    @DexIgnore
    private static /* final */ String TAG_PHONE_LOC; // = "[Phone-loc] ";
    @DexIgnore
    private static /* final */ String UBLOX_ACCURACY; // = "UBLOX_ACCURACY";
    @DexIgnore
    private static /* final */ float UBLOX_MIN_ACCEPTABLE_THRESHOLD; // = 5.0f;
    @DexIgnore
    private static /* final */ long WARM_RESET_INTERVAL; // = java.util.concurrent.TimeUnit.SECONDS.toMillis(160);
    @DexIgnore
    private static /* final */ Logger sLogger; // = new com.navdy.service.library.log.Logger(com.navdy.hud.app.device.gps.GpsManager.class);
    @DexIgnore
    private static /* final */ GpsManager singleton; // = new com.navdy.hud.app.device.gps.GpsManager();
    @DexIgnore
    private GpsManager.LocationSource activeLocationSource;
    @DexIgnore
    private HudConnectionService connectionService;
    @DexIgnore
    private boolean driving;
    @DexIgnore
    private BroadcastReceiver eventReceiver; // = new com.navdy.hud.app.device.gps.GpsManager.Anon11();
    @DexIgnore
    private volatile boolean extrapolationOn;
    @DexIgnore
    private volatile long extrapolationStartTime;
    @DexIgnore
    private boolean firstSwitchToUbloxFromPhone;
    @DexIgnore
    private float gpsAccuracy;
    @DexIgnore
    private long gpsLastEventTime;
    @DexIgnore
    private long gpsManagerStartTime;
    @DexIgnore
    private Handler handler; // = new android.os.Handler(android.os.Looper.getMainLooper());
    @DexIgnore
    private long inaccurateGpsCount;
    @DexIgnore
    private boolean isDebugTTSEnabled;
    @DexIgnore
    private boolean keepPhoneGpsOn;
    @DexIgnore
    private long lastInaccurateGpsTime;
    @DexIgnore
    private Coordinate lastPhoneCoordinate;
    @DexIgnore
    private long lastPhoneCoordinateTime;
    @DexIgnore
    private Location lastUbloxCoordinate;
    @DexIgnore
    private long lastUbloxCoordinateTime;
    @DexIgnore
    private Runnable locationFixRunnable; // = new com.navdy.hud.app.device.gps.GpsManager.Anon8();
    @DexIgnore
    private LocationManager locationManager;
    @DexIgnore
    private LocationListener navdyGpsLocationListener; // = new com.navdy.hud.app.device.gps.GpsManager.Anon3();
    @DexIgnore
    private Handler.Callback nmeaCallback; // = new com.navdy.hud.app.device.gps.GpsManager.Anon9();
    @DexIgnore
    private Handler nmeaHandler;
    @DexIgnore
    private HandlerThread nmeaHandlerThread;
    @DexIgnore
    private GpsNmeaParser nmeaParser;
    @DexIgnore
    private Runnable noLocationUpdatesRunnable; // = new com.navdy.hud.app.device.gps.GpsManager.Anon6();
    @DexIgnore
    private Runnable noPhoneLocationFixRunnable; // = new com.navdy.hud.app.device.gps.GpsManager.Anon5();
    @DexIgnore
    private ArrayList<Intent> queue; // = new java.util.ArrayList<>();
    @DexIgnore
    private Runnable queueCallback; // = new com.navdy.hud.app.device.gps.GpsManager.Anon10();
    @DexIgnore
    private volatile boolean startUbloxCalled;
    @DexIgnore
    private boolean switchBetweenPhoneUbloxAllowed;
    @DexIgnore
    private long transitionStartTime;
    @DexIgnore
    private boolean transitioning;
    @DexIgnore
    private LocationListener ubloxGpsLocationListener; // = new com.navdy.hud.app.device.gps.GpsManager.Anon2();
    @DexIgnore
    private GpsStatus.NmeaListener ubloxGpsNmeaListener; // = new com.navdy.hud.app.device.gps.GpsManager.Anon1();
    @DexIgnore
    private GpsStatus.Listener ubloxGpsStatusListener; // = new com.navdy.hud.app.device.gps.GpsManager.Anon4();
    @DexIgnore
    private boolean warmResetCancelled;
    @DexIgnore
    private Runnable warmResetRunnable; // = new com.navdy.hud.app.device.gps.GpsManager.Anon7();

    @DexAdd
    private static final String GPS_PHONE_SWITCH_PROPERTY;

    static {
        GPS_PHONE_SWITCH_PROPERTY = "persist.sys.hud_phone_gps_active";
    }

    // @DexIgnore
    // class Anon1 implements GpsStatus.NmeaListener {
    //     @DexIgnore
    //     Anon1() {
    //     }
    //
    //     @DexIgnore
    //     public void onNmeaReceived(long timestamp, String nmea) {
    //         try {
    //             if (GpsManager.sLogger.isLoggable(2)) {
    //                 GpsManager.sLogger.v(GpsManager.TAG_GPS_NMEA + nmea);
    //             }
    //             GpsManager.this.processNmea(nmea);
    //         } catch (Throwable t) {
    //             GpsManager.sLogger.e(t);
    //         }
    //     }
    // }
    //
    // @DexIgnore
    // class Anon10 implements Runnable {
    //     @DexIgnore
    //     Anon10() {
    //     }
    //
    //     @DexIgnore
    //     public void run() {
    //         try {
    //             Context context = HudApplication.getAppContext();
    //             UserHandle handle = android.os.Process.myUserHandle();
    //             int len = GpsManager.this.queue.size();
    //             if (len > 0) {
    //                 for (int i = 0; i < len; i++) {
    //                     Intent intent = (Intent) GpsManager.this.queue.get(i);
    //                     context.sendBroadcastAsUser(intent, handle);
    //                     GpsManager.sLogger.v("sent-queue gps event user:" + intent.getAction());
    //                 }
    //                 GpsManager.this.queue.clear();
    //             }
    //         } catch (Throwable t) {
    //             GpsManager.sLogger.e(t);
    //         }
    //     }
    // }
    //
    // @DexIgnore
    // class Anon11 extends BroadcastReceiver {
    //     @DexIgnore
    //     Anon11() {
    //     }
    //
    //     @DexIgnore
    //     public void onReceive(Context context, Intent intent) {
    //         char c = 0;
    //         try {
    //             String action = intent.getAction();
    //             if (action != null) {
    //                 RouteRecorder routeRecorder = RouteRecorder.getInstance();
    //                 switch (action.hashCode()) {
    //                     case -1801456507:
    //                         if (action.equals(GpsConstants.GPS_EVENT_DEAD_RECKONING_STARTED)) {
    //                             break;
    //                         }
    //                     case -1788590639:
    //                         if (action.equals(GpsConstants.GPS_EVENT_DEAD_RECKONING_STOPPED)) {
    //                             c = 1;
    //                             break;
    //                         }
    //                     case -47674184:
    //                         if (action.equals(GpsConstants.EXTRAPOLATION)) {
    //                             c = 2;
    //                             break;
    //                         }
    //                 }
    //                 c = 65535;
    //                 switch (c) {
    //                     case 0:
    //                         if (routeRecorder.isRecording()) {
    //                             String marker = intent.getStringExtra(GpsConstants.GPS_EXTRA_DR_TYPE);
    //                             if (marker == null) {
    //                                 marker = "";
    //                             }
    //                             routeRecorder.injectMarker("GPS_DR_STARTED " + marker);
    //                             return;
    //                         }
    //                         return;
    //                     case 1:
    //                         if (routeRecorder.isRecording()) {
    //                             routeRecorder.injectMarker(GpsConstants.GPS_EVENT_DEAD_RECKONING_STOPPED);
    //                             return;
    //                         }
    //                         return;
    //                     case 2:
    //                         GpsManager.this.extrapolationOn = intent.getBooleanExtra(GpsConstants.EXTRAPOLATION_FLAG, false);
    //                         if (GpsManager.this.extrapolationOn) {
    //                             GpsManager.this.extrapolationStartTime = SystemClock.elapsedRealtime();
    //                         } else {
    //                             GpsManager.this.extrapolationStartTime = 0;
    //                         }
    //                         GpsManager.sLogger.v("extrapolation is:" + GpsManager.this.extrapolationOn + " time:" + GpsManager.this.extrapolationStartTime);
    //                         return;
    //                     default:
    //                         return;
    //                 }
    //             }
    //         } catch (Throwable t) {
    //             GpsManager.sLogger.e(t);
    //         }
    //     }
    // }
    //
    // @DexIgnore
    // class Anon2 implements LocationListener {
    //     @DexIgnore
    //     Anon2() {
    //     }
    //
    //     @DexIgnore
    //     public void onLocationChanged(Location location) {
    //         float f;
    //         Object valueOf;
    //         try {
    //             if (GpsManager.sLogger.isLoggable(2)) {
    //                 GpsManager.sLogger.d(GpsManager.TAG_GPS_LOC + (location.isFromMockProvider() ? "[Mock] " : "") + location);
    //             }
    //             if (GpsManager.this.startUbloxCalled && !location.isFromMockProvider() && GpsManager.this.activeLocationSource != GpsManager.LocationSource.UBLOX) {
    //                 GpsManager.LocationSource last = GpsManager.this.activeLocationSource;
    //                 GpsManager.this.activeLocationSource = GpsManager.LocationSource.UBLOX;
    //                 GpsManager.this.handler.removeCallbacks(GpsManager.this.noPhoneLocationFixRunnable);
    //                 GpsManager.this.handler.removeCallbacks(GpsManager.this.locationFixRunnable);
    //                 GpsManager.this.handler.postDelayed(GpsManager.this.locationFixRunnable, 1000);
    //                 GpsManager.this.stopSendingLocation();
    //                 int second = ((int) (SystemClock.elapsedRealtime() - GpsManager.this.gpsLastEventTime)) / 1000;
    //                 float phoneAccuracy = -1.0f;
    //                 long elapsedTime = SystemClock.elapsedRealtime() - GpsManager.this.lastPhoneCoordinateTime;
    //                 if (GpsManager.this.lastPhoneCoordinate != null && elapsedTime < GpsManager.MINIMUM_DRIVE_TIME) {
    //                     phoneAccuracy = GpsManager.this.lastPhoneCoordinate.accuracy;
    //                 }
    //                 if (GpsManager.this.lastUbloxCoordinate != null) {
    //                     f = GpsManager.this.lastUbloxCoordinate.getAccuracy();
    //                 } else {
    //                     f = -1.0f;
    //                 }
    //                 int accuracy = (int) f;
    //                 ConnectionServiceAnalyticsSupport.recordGpsAcquireLocation(String.valueOf(second), String.valueOf(accuracy));
    //                 GpsManager.this.gpsLastEventTime = SystemClock.elapsedRealtime();
    //                 GpsManager.this.markLocationFix(GpsConstants.DEBUG_TTS_UBLOX_SWITCH, "Phone =" + (phoneAccuracy == -1.0f ? "n/a" : Float.valueOf(phoneAccuracy)) + " ublox =" + accuracy, false, true);
    //                 Logger access$Anon000 = GpsManager.sLogger;
    //                 StringBuilder append = new StringBuilder().append("[Gps-loc] [LocationProvider] switched from [").append(last).append("] to [").append(GpsManager.LocationSource.UBLOX).append("] ").append("Phone =");
    //                 if (phoneAccuracy == -1.0f) {
    //                     valueOf = "n/a";
    //                 } else {
    //                     valueOf = Float.valueOf(phoneAccuracy);
    //                 }
    //                 access$Anon000.v(append.append(valueOf).append(" ublox =").append(accuracy).append(" time=").append(elapsedTime).toString());
    //             }
    //         } catch (Throwable t) {
    //             GpsManager.sLogger.e(t);
    //         }
    //     }
    //
    //     @DexIgnore
    //     public void onStatusChanged(String provider, int status, Bundle extras) {
    //         if (PowerManager.isAwake()) {
    //             GpsManager.sLogger.i(GpsManager.TAG_GPS_STATUS + provider + HereManeuverDisplayBuilder.COMMA + status);
    //         }
    //     }
    //
    //     @DexIgnore
    //     public void onProviderEnabled(String provider) {
    //         GpsManager.sLogger.i(GpsManager.TAG_GPS_STATUS + provider + "[enabled]");
    //     }
    //
    //     @DexIgnore
    //     public void onProviderDisabled(String provider) {
    //         GpsManager.sLogger.i(GpsManager.TAG_GPS_STATUS + provider + "[disabled]");
    //     }
    // }
    //
    // @DexIgnore
    // class Anon3 implements LocationListener {
    //     private float accuracyMax = 0.0f;
    //     private float accuracyMin = 0.0f;
    //     private int accuracySampleCount = 0;
    //     private long accuracySampleStartTime = SystemClock.elapsedRealtime();
    //     private float accuracySum = 0.0f;
    //
    //     @DexIgnore
    //     Anon3() {
    //     }
    //
    //     @DexIgnore
    //     private void collectAccuracyStats(float accuracy) {
    //         if (accuracy != 0.0f) {
    //             this.accuracySum += accuracy;
    //             this.accuracySampleCount++;
    //             if (this.accuracyMin == 0.0f || accuracy < this.accuracyMin) {
    //                 this.accuracyMin = accuracy;
    //             }
    //             if (this.accuracyMax < accuracy) {
    //                 this.accuracyMax = accuracy;
    //             }
    //         }
    //         if (GpsManager.this.lastUbloxCoordinateTime - this.accuracySampleStartTime > 300000) {
    //             if (this.accuracySampleCount > 0) {
    //                 ConnectionServiceAnalyticsSupport.recordGpsAccuracy(Integer.toString(Math.round(this.accuracyMin)), Integer.toString(Math.round(this.accuracyMax)), Integer.toString(Math.round(this.accuracySum / ((float) this.accuracySampleCount))));
    //                 this.accuracySum = 0.0f;
    //                 this.accuracyMax = 0.0f;
    //                 this.accuracyMin = 0.0f;
    //                 this.accuracySampleCount = 0;
    //             }
    //             this.accuracySampleStartTime = GpsManager.this.lastUbloxCoordinateTime;
    //         }
    //     }
    //
    //     @DexIgnore
    //     public void onLocationChanged(Location location) {
    //         try {
    //             if (!GpsManager.this.warmResetCancelled) {
    //                 GpsManager.this.cancelUbloxResetRunnable();
    //             }
    //             if (!RouteRecorder.getInstance().isPlaying()) {
    //                 GpsManager.this.updateDrivingState(location);
    //                 GpsManager.this.lastUbloxCoordinate = location;
    //                 GpsManager.this.lastUbloxCoordinateTime = SystemClock.elapsedRealtime();
    //                 collectAccuracyStats(location.getAccuracy());
    //                 if (GpsManager.this.connectionService != null && GpsManager.this.connectionService.isConnected()) {
    //                     if (GpsManager.this.activeLocationSource != GpsManager.LocationSource.UBLOX) {
    //                         long time = SystemClock.elapsedRealtime() - GpsManager.this.lastPhoneCoordinateTime;
    //                         if (time > GpsManager.MINIMUM_DRIVE_TIME) {
    //                             GpsManager.sLogger.v("phone threshold exceeded= " + time + ", start uBloxReporting");
    //                             GpsManager.this.startUbloxReporting();
    //                             return;
    //                         }
    //                     }
    //                     if (!GpsManager.this.firstSwitchToUbloxFromPhone || GpsManager.this.switchBetweenPhoneUbloxAllowed) {
    //                         GpsManager.this.checkGpsToPhoneAccuracy(location);
    //                     }
    //                 } else if (GpsManager.this.activeLocationSource != GpsManager.LocationSource.UBLOX) {
    //                     GpsManager.sLogger.v("not connected with phone, start uBloxReporting");
    //                     GpsManager.this.startUbloxReporting();
    //                 }
    //             }
    //         } catch (Throwable t) {
    //             GpsManager.sLogger.e(t);
    //         }
    //     }
    //
    //     @DexIgnore
    //     public void onStatusChanged(String provider, int status, Bundle extras) {
    //     }
    //
    //     @DexIgnore
    //     public void onProviderEnabled(String provider) {
    //     }
    //
    //     @DexIgnore
    //     public void onProviderDisabled(String provider) {
    //     }
    // }
    //
    // @DexIgnore
    // class Anon4 implements GpsStatus.Listener {
    //     @DexIgnore
    //     Anon4() {
    //     }
    //
    //     @DexIgnore
    //     public void onGpsStatusChanged(int event) {
    //         switch (event) {
    //             case 1:
    //                 GpsManager.sLogger.i("[Gps-stat] gps searching...");
    //                 return;
    //             case 2:
    //                 GpsManager.sLogger.w("[Gps-stat] gps stopped searching");
    //                 return;
    //             case 3:
    //                 GpsManager.sLogger.i("[Gps-stat] gps got first fix");
    //                 return;
    //             default:
    //                 return;
    //         }
    //     }
    // }
    //
    // @DexIgnore
    // class Anon5 implements Runnable {
    //     @DexIgnore
    //     Anon5() {
    //     }
    //
    //     @DexIgnore
    //     public void run() {
    //         boolean resetRunnable = true;
    //         try {
    //             if (GpsManager.this.activeLocationSource == GpsManager.LocationSource.UBLOX) {
    //                 GpsManager.sLogger.i("[Gps-loc] noPhoneLocationFixRunnable: has location fix now");
    //                 resetRunnable = false;
    //             } else {
    //                 GpsManager.this.updateDrivingState(null);
    //                 GpsManager.this.startSendingLocation();
    //             }
    //             if (resetRunnable) {
    //                 GpsManager.this.handler.postDelayed(GpsManager.this.noPhoneLocationFixRunnable, 5000);
    //             }
    //         } catch (Throwable th) {
    //             if (1 != 0) {
    //                 GpsManager.this.handler.postDelayed(GpsManager.this.noPhoneLocationFixRunnable, 5000);
    //             }
    //             throw th;
    //         }
    //     }
    // }
    //
    // @DexIgnore
    // class Anon6 implements Runnable {
    //     @DexIgnore
    //     Anon6() {
    //     }
    //
    //     @DexIgnore
    //     public void run() {
    //         GpsManager.this.updateDrivingState(null);
    //     }
    // }
    //
    // @DexIgnore
    // class Anon7 implements Runnable {
    //     @DexIgnore
    //     Anon7() {
    //     }
    //
    //     @DexIgnore
    //     public void run() {
    //         try {
    //             GpsManager.sLogger.v("issuing warm reset to ublox, no location report in last " + GpsManager.WARM_RESET_INTERVAL);
    //             GpsManager.this.sendGpsEventBroadcast(GpsConstants.GPS_EVENT_WARM_RESET_UBLOX, null);
    //         } catch (Throwable t) {
    //             GpsManager.sLogger.e(t);
    //         }
    //     }
    // }
    //
    // @DexIgnore
    // class Anon8 implements Runnable {
    //     @DexIgnore
    //     Anon8() {
    //     }
    //
    //     @DexIgnore
    //     public void run() {
    //         boolean resetRunnable = true;
    //         try {
    //             long time = SystemClock.elapsedRealtime() - GpsManager.this.lastUbloxCoordinateTime;
    //             if (time >= GpsManager.MINIMUM_DRIVE_TIME) {
    //                 GpsManager.sLogger.i("[Gps-loc] locationFixRunnable: lost location fix[" + time + "]");
    //                 if (GpsManager.this.extrapolationOn) {
    //                     long diff = SystemClock.elapsedRealtime() - GpsManager.this.extrapolationStartTime;
    //                     GpsManager.sLogger.i("[Gps-loc] locationFixRunnable: extrapolation on time:" + diff);
    //                     if (diff < 5000) {
    //                         GpsManager.sLogger.i("[Gps-loc] locationFixRunnable: reset");
    //                         if (1 != 0) {
    //                             GpsManager.this.handler.postDelayed(GpsManager.this.locationFixRunnable, 1000);
    //                             return;
    //                         }
    //                         return;
    //                     }
    //                     GpsManager.this.extrapolationOn = false;
    //                     GpsManager.this.extrapolationStartTime = 0;
    //                     GpsManager.sLogger.i("[Gps-loc] locationFixRunnable: expired");
    //                 }
    //                 resetRunnable = false;
    //                 GpsManager.this.updateDrivingState(null);
    //                 GpsManager.sLogger.e("[Gps-loc] lost gps fix");
    //                 GpsManager.this.handler.removeCallbacks(GpsManager.this.locationFixRunnable);
    //                 GpsManager.this.handler.removeCallbacks(GpsManager.this.noPhoneLocationFixRunnable);
    //                 GpsManager.this.handler.postDelayed(GpsManager.this.noPhoneLocationFixRunnable, 5000);
    //                 GpsManager.this.startSendingLocation();
    //                 ConnectionServiceAnalyticsSupport.recordGpsLostLocation(String.valueOf((SystemClock.elapsedRealtime() - GpsManager.this.gpsLastEventTime) / 1000));
    //                 GpsManager.this.gpsLastEventTime = SystemClock.elapsedRealtime();
    //             }
    //             if (resetRunnable) {
    //                 GpsManager.this.handler.postDelayed(GpsManager.this.locationFixRunnable, 1000);
    //             }
    //         } catch (Throwable th) {
    //             if (1 != 0) {
    //                 GpsManager.this.handler.postDelayed(GpsManager.this.locationFixRunnable, 1000);
    //             }
    //             throw th;
    //         }
    //     }
    // }
    //
    // @DexIgnore
    // class Anon9 implements Handler.Callback {
    //     @DexIgnore
    //     Anon9() {
    //     }
    //
    //     @DexIgnore
    //     public boolean handleMessage(Message msg) {
    //         try {
    //             switch (msg.what) {
    //                 case 1:
    //                     GpsManager.this.nmeaParser.parseNmeaMessage((String) msg.obj);
    //                     break;
    //                 case 2:
    //                     if (SystemClock.elapsedRealtime() - GpsManager.this.gpsManagerStartTime > StaticConfig.PERMISSION_CHECK_INTERVAL) {
    //                         Bundle dataBundle = (Bundle) msg.obj;
    //                         Bundle bundle = new Bundle();
    //                         bundle.putBundle(GpsConstants.GPS_EVENT_SATELLITE_DATA, dataBundle);
    //                         GpsManager.this.sendGpsEventBroadcast(GpsConstants.GPS_SATELLITE_STATUS, bundle);
    //                         break;
    //                     }
    //                     break;
    //             }
    //         } catch (Throwable t) {
    //             GpsManager.sLogger.e(t);
    //         }
    //         return true;
    //     }
    // }

    @DexIgnore
    enum LocationSource {
        UBLOX,
        PHONE
    }

    @DexIgnore
    public static GpsManager getInstance() {
        return singleton;
    }

    @DexAdd
    private GpsManager() {
        this((Void)null);
        if (SettingsManager.global.getBoolean(GPS_PHONE_SWITCH_PROPERTY, Boolean.FALSE)) {
            this.keepPhoneGpsOn = true;
            this.switchBetweenPhoneUbloxAllowed = true;
            sLogger.v("switch between phone and ublox allowed");
        }
    }

    @DexEdit
    private GpsManager(@DexIgnore Void tag) {
        // Context context = HudApplication.getAppContext();
        // try {
        //     int n = SystemProperties.getInt(GPS_SYSTEM_PROPERTY, 0);
        //     if (n == -1) {
        //         this.keepPhoneGpsOn = true;
        //         this.switchBetweenPhoneUbloxAllowed = true;
        //         sLogger.v("switch between phone and ublox allowed");
        //     }
        //     sLogger.v("keepPhoneGpsOn[" + this.keepPhoneGpsOn + "] val[" + n + "]");
        //     this.isDebugTTSEnabled = new File("/sdcard/debug_tts").exists();
        //     setUbloxAccuracyThreshold();
        //     setUbloxTimeDiscardThreshold();
        // } catch (Throwable t) {
        //     sLogger.e(t);
        // }
        // LocationProvider gpsProvider = null;
        // this.locationManager = (LocationManager) context.getSystemService("location");
        // if (this.locationManager.isProviderEnabled("gps")) {
        //     gpsProvider = this.locationManager.getProvider("gps");
        //     if (gpsProvider != null) {
        //         this.nmeaHandlerThread = new HandlerThread("navdynmea");
        //         this.nmeaHandlerThread.start();
        //         this.nmeaHandler = new Handler(this.nmeaHandlerThread.getLooper(), this.nmeaCallback);
        //         this.nmeaParser = new GpsNmeaParser(sLogger, this.nmeaHandler);
        //         this.locationManager.addGpsStatusListener(this.ubloxGpsStatusListener);
        //         this.locationManager.addNmeaListener(this.ubloxGpsNmeaListener);
        //     }
        // }
        // if (DeviceUtil.isNavdyDevice()) {
        //     sLogger.i("[Gps-i] setting up ublox gps");
        //     if (gpsProvider != null) {
        //         this.locationManager.requestLocationUpdates("gps", 0, 0.0f, this.ubloxGpsLocationListener);
        //         try {
        //             this.locationManager.requestLocationUpdates(GpsConstants.NAVDY_GPS_PROVIDER, 0, 0.0f, this.navdyGpsLocationListener);
        //             sLogger.v("requestLocationUpdates successful for NAVDY_GPS_PROVIDER");
        //         } catch (Throwable t2) {
        //             sLogger.e("requestLocationUpdates", t2);
        //         }
        //         sLogger.i("[Gps-i] ublox gps listeners installed");
        //     } else {
        //         sLogger.e("[Gps-i] gps provider not found");
        //     }
        // } else {
        //     sLogger.i("[Gps-i] not a Hud device,not setting up ublox gps");
        // }
        // this.locationManager.addTestProvider("network", false, true, false, false, true, true, true, 0, 3);
        // this.locationManager.setTestProviderEnabled("network", true);
        // this.locationManager.setTestProviderStatus("network", 2, null, System.currentTimeMillis());
        // sLogger.i("[Gps-i] added mock network provider");
        // this.gpsManagerStartTime = SystemClock.elapsedRealtime();
        // this.gpsLastEventTime = this.gpsManagerStartTime;
        // ConnectionServiceAnalyticsSupport.recordGpsAttemptAcquireLocation();
        // if (DeviceUtil.isNavdyDevice()) {
        //     setUbloxResetRunnable();
        // }
        // IntentFilter filter = new IntentFilter();
        // filter.addAction(GpsConstants.GPS_EVENT_DEAD_RECKONING_STARTED);
        // filter.addAction(GpsConstants.GPS_EVENT_DEAD_RECKONING_STOPPED);
        // filter.addAction(GpsConstants.EXTRAPOLATION);
        // context.registerReceiver(this.eventReceiver, filter);
    }

    @DexIgnore
    public void setConnectionService(HudConnectionService connectionService2) {
        this.connectionService = connectionService2;
    }

    @DexIgnore
    public void feedLocation(Coordinate coordinate) {
        boolean hasAtleastOneLocation = this.lastPhoneCoordinateTime > 0 || this.lastUbloxCoordinateTime > 0;
        long now = System.currentTimeMillis();
        if (coordinate.accuracy < 30.0f || !hasAtleastOneLocation) {
            long timeDiff = now - coordinate.timestamp;
            if (timeDiff >= 2000 && hasAtleastOneLocation) {
                sLogger.e("OLD location from phone(discard) time:" + timeDiff + ", timestamp:" + coordinate.timestamp + " now:" + now + " lat:" + coordinate.latitude + " lng:" + coordinate.longitude);
            } else if (this.lastPhoneCoordinate != null && this.connectionService.getDevicePlatform() == DeviceInfo.Platform.PLATFORM_iOS && this.lastPhoneCoordinate.latitude == coordinate.latitude && this.lastPhoneCoordinate.longitude == coordinate.longitude && this.lastPhoneCoordinate.accuracy == coordinate.accuracy && this.lastPhoneCoordinate.altitude == coordinate.altitude && this.lastPhoneCoordinate.bearing == coordinate.bearing && this.lastPhoneCoordinate.speed == coordinate.speed) {
                sLogger.e("same location(discard) diff=" + (coordinate.timestamp - this.lastPhoneCoordinate.timestamp));
            } else {
                this.lastPhoneCoordinate = coordinate;
                this.lastPhoneCoordinateTime = SystemClock.elapsedRealtime();
                this.handler.removeCallbacks(this.noPhoneLocationFixRunnable);
                Location androidLocation = null;
                RouteRecorder routeRecorder = RouteRecorder.getInstance();
                if (routeRecorder.isRecording()) {
                    androidLocation = androidLocationFromCoordinate(coordinate);
                    routeRecorder.injectLocation(androidLocation, true);
                }
                if (this.activeLocationSource == null) {
                    sLogger.v("[Gps-loc] [LocationProvider] switched from [" + this.activeLocationSource + "] to [" + GpsManager.LocationSource.PHONE + "]");
                    stopUbloxReporting();
                    this.activeLocationSource = GpsManager.LocationSource.PHONE;
                    feedLocationToProvider(androidLocation, coordinate);
                    markLocationFix(GpsConstants.DEBUG_TTS_PHONE_SWITCH, "Phone =" + this.lastPhoneCoordinate.accuracy + " ublox=n/a", true, false);
                } else if (this.activeLocationSource == GpsManager.LocationSource.PHONE) {
                    feedLocationToProvider(androidLocation, coordinate);
                } else if (SystemClock.elapsedRealtime() - this.lastUbloxCoordinateTime > MINIMUM_DRIVE_TIME) {
                    sLogger.v("[Gps-loc] [LocationProvider] switched from [" + this.activeLocationSource + "] to [" + GpsManager.LocationSource.PHONE + "]");
                    this.activeLocationSource = GpsManager.LocationSource.PHONE;
                    stopUbloxReporting();
                    feedLocationToProvider(androidLocation, coordinate);
                    markLocationFix(GpsConstants.DEBUG_TTS_PHONE_SWITCH, "Phone =" + this.lastPhoneCoordinate.accuracy + " ublox =lost", true, false);
                }
            }
        } else {
            this.inaccurateGpsCount++;
            this.gpsAccuracy += coordinate.accuracy;
            if (this.lastInaccurateGpsTime == 0 || now - this.lastInaccurateGpsTime >= INACCURATE_GPS_REPORT_INTERVAL) {
                sLogger.e("BAD gps accuracy (discarding) avg:" + (this.gpsAccuracy / ((float) this.inaccurateGpsCount)) + ", count=" + this.inaccurateGpsCount + ", last coord: " + coordinate);
                this.inaccurateGpsCount = 0;
                this.gpsAccuracy = 0.0f;
                this.lastInaccurateGpsTime = now;
            }
        }
    }

    @DexIgnore
    private void feedLocationToProvider(Location androidLocation, Coordinate coordinate) {
        try {
            if (sLogger.isLoggable(3)) {
                sLogger.d(TAG_PHONE_LOC + coordinate);
            }
            if (androidLocation == null) {
                androidLocation = androidLocationFromCoordinate(coordinate);
            }
            updateDrivingState(androidLocation);
            this.locationManager.setTestProviderLocation(androidLocation.getProvider(), androidLocation);
        } catch (Throwable t) {
            sLogger.e("feedLocation", t);
        }
    }

    @DexIgnore
    private static Location androidLocationFromCoordinate(Coordinate c) {
        Location location = new Location("network");
        location.setLatitude(c.latitude);
        location.setLongitude(c.longitude);
        location.setAccuracy(c.accuracy);
        location.setAltitude(c.altitude);
        location.setBearing(c.bearing);
        location.setSpeed(c.speed);
        location.setTime(System.currentTimeMillis());
        location.setElapsedRealtimeNanos(SystemClock.elapsedRealtimeNanos());
        return location;
    }

    @DexIgnore
    private void startSendingLocation() {
        if (this.connectionService != null && this.connectionService.isConnected()) {
            try {
                sLogger.i("[Gps-loc] phone start sending location");
                this.connectionService.sendMessage(START_SENDING_LOCATION);
            } catch (Throwable t) {
                sLogger.e(t);
            }
        }
    }

    @DexIgnore
    private void setDriving(boolean driving2) {
        if (this.driving != driving2) {
            this.driving = driving2;
            sendGpsEventBroadcast(driving2 ? GpsConstants.GPS_EVENT_DRIVING_STARTED : GpsConstants.GPS_EVENT_DRIVING_STOPPED, null);
        }
    }

    @DexIgnore
    private void updateDrivingState(Location location) {
        if (this.driving) {
            if (location != null && ((double) location.getSpeed()) >= 0.22353333333333333d) {
                this.transitioning = false;
            } else if (!this.transitioning) {
                this.transitioning = true;
                this.transitionStartTime = SystemClock.elapsedRealtime();
            } else if (SystemClock.elapsedRealtime() - this.transitionStartTime > MINIMUM_DRIVE_TIME) {
                setDriving(false);
                this.transitioning = false;
            }
        } else if (location == null || ((double) location.getSpeed()) <= 2.2353333333333336d) {
            this.transitioning = false;
        } else if (!this.transitioning) {
            this.transitioning = true;
            this.transitionStartTime = SystemClock.elapsedRealtime();
        } else if (SystemClock.elapsedRealtime() - this.transitionStartTime > MINIMUM_DRIVE_TIME) {
            setDriving(true);
            this.transitioning = false;
        }
        this.handler.removeCallbacks(this.noLocationUpdatesRunnable);
        if (this.driving) {
            this.handler.postDelayed(this.noLocationUpdatesRunnable, MINIMUM_DRIVE_TIME);
        }
    }

    @DexIgnore
    private void stopSendingLocation() {
        if (this.keepPhoneGpsOn) {
            sLogger.v("stopSendingLocation: keeping phone gps on");
        } else if (this.isDebugTTSEnabled) {
            sLogger.v("stopSendingLocation: not stopping, debug_tts enabled");
        } else if (this.connectionService != null && this.connectionService.isConnected()) {
            try {
                sLogger.i("[Gps-loc] phone stop sending location");
                this.connectionService.sendMessage(STOP_SENDING_LOCATION);
            } catch (Throwable t) {
                sLogger.e(t);
            }
        }
    }

    @DexIgnore
    private void sendSpeechRequest(SpeechRequest speechRequest) {
        if (this.connectionService != null && this.connectionService.isConnected()) {
            try {
                sLogger.i("[Gps-loc] send speech request");
                this.connectionService.sendMessage(speechRequest);
            } catch (Throwable t) {
                sLogger.e(t);
            }
        }
    }

    @DexIgnore
    private void processNmea(String nmea) {
        Message msg = this.nmeaHandler.obtainMessage(1);
        msg.obj = nmea;
        this.nmeaHandler.sendMessage(msg);
    }

    @DexIgnore
    private void sendGpsEventBroadcast(String eventName, Bundle extras) {
        try {
            Intent intent = new Intent(eventName);
            if (extras != null) {
                intent.putExtras(extras);
            }
            if (SystemClock.elapsedRealtime() - this.gpsManagerStartTime <= StaticConfig.PERMISSION_CHECK_INTERVAL) {
                this.queue.add(intent);
                this.handler.removeCallbacks(this.queueCallback);
                this.handler.postDelayed(this.queueCallback, StaticConfig.PERMISSION_CHECK_INTERVAL);
                return;
            }
            Context context = HudApplication.getAppContext();
            UserHandle handle = android.os.Process.myUserHandle();
            if (this.queue.size() > 0) {
                this.queueCallback.run();
            }
            context.sendBroadcastAsUser(intent, handle);
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }

    @DexIgnore
    public void startUbloxReporting() {
        try {
            HudApplication.getAppContext().sendBroadcastAsUser(new Intent(START_UBLOX), android.os.Process.myUserHandle());
            this.startUbloxCalled = true;
            sLogger.v("started ublox reporting");
        } catch (Throwable t) {
            sLogger.e("startUbloxReporting", t);
        }
    }

    @DexIgnore
    public void stopUbloxReporting() {
        try {
            this.startUbloxCalled = false;
            HudApplication.getAppContext().sendBroadcastAsUser(new Intent(STOP_UBLOX), android.os.Process.myUserHandle());
            sLogger.v("stopped ublox reporting");
        } catch (Throwable t) {
            sLogger.e("stopUbloxReporting", t);
        }
    }

    @DexIgnore
    private void setUbloxAccuracyThreshold() {
        try {
            Context context = HudApplication.getAppContext();
            UserHandle handle = android.os.Process.myUserHandle();
            Intent intent = new Intent(SET_UBLOX_ACCURACY);
            intent.putExtra(UBLOX_ACCURACY, 30);
            context.sendBroadcastAsUser(intent, handle);
            sLogger.v("setUbloxAccuracyThreshold:30");
        } catch (Throwable t) {
            sLogger.e("setUbloxAccuracyThreshold", t);
        }
    }

    @DexIgnore
    private void setUbloxTimeDiscardThreshold() {
        try {
            Context context = HudApplication.getAppContext();
            UserHandle handle = android.os.Process.myUserHandle();
            Intent intent = new Intent(SET_LOCATION_DISCARD_TIME_THRESHOLD);
            intent.putExtra(DISCARD_TIME, 2000);
            context.sendBroadcastAsUser(intent, handle);
            sLogger.v("setUbloxTimeDiscardThreshold:2000");
        } catch (Throwable t) {
            sLogger.e("setUbloxTimeDiscardThreshold", t);
        }
    }

    @DexIgnore
    private void checkGpsToPhoneAccuracy(Location location) {
        float accuracy = location.getAccuracy();
        long time = SystemClock.elapsedRealtime() - this.lastPhoneCoordinateTime;
        if (sLogger.isLoggable(3)) {
            sLogger.v("[Gps-loc-navdy] checkGpsToPhoneAccuracy: ublox[" + location.getAccuracy() + "] phone[" + (this.lastPhoneCoordinate != null ? this.lastPhoneCoordinate.accuracy : "n/a") + "] lastPhoneTime [" + (this.lastPhoneCoordinate != null ? Long.valueOf(time) : "n/a") + "]");
        }
        if (accuracy != 0.0f) {
            if (this.activeLocationSource == GpsManager.LocationSource.UBLOX) {
                if (this.lastPhoneCoordinate != null && SystemClock.elapsedRealtime() - this.lastPhoneCoordinateTime <= MINIMUM_DRIVE_TIME) {
                    if (this.lastPhoneCoordinate.accuracy + 2.0f < accuracy) {
                        if (sLogger.isLoggable(3)) {
                            sLogger.v("[Gps-loc-navdy]  ublox accuracy [" + accuracy + "] > phone accuracy[" + this.lastPhoneCoordinate.accuracy + "]");
                        }
                        if (!this.startUbloxCalled) {
                            return;
                        }
                        if (accuracy <= UBLOX_MIN_ACCEPTABLE_THRESHOLD) {
                            sLogger.v("ublox accuracy [" + accuracy + "] > phone accuracy[" + this.lastPhoneCoordinate.accuracy + "], but not above threshold");
                            stopSendingLocation();
                            return;
                        }
                        sLogger.v("ublox accuracy [" + accuracy + "] > phone accuracy[" + this.lastPhoneCoordinate.accuracy + "], stop uBloxReporting");
                        this.handler.removeCallbacks(this.locationFixRunnable);
                        this.handler.removeCallbacks(this.noPhoneLocationFixRunnable);
                        stopUbloxReporting();
                        sLogger.v("[Gps-loc] [LocationProvider] switched from [" + this.activeLocationSource + "] to [" + GpsManager.LocationSource.PHONE + "]");
                        this.activeLocationSource = GpsManager.LocationSource.PHONE;
                        markLocationFix(GpsConstants.DEBUG_TTS_PHONE_SWITCH, "Phone =" + this.lastPhoneCoordinate.accuracy + " ublox =" + accuracy, true, false);
                    } else if (sLogger.isLoggable(3)) {
                        sLogger.v("ublox accuracy [" + accuracy + "] < phone accuracy[" + this.lastPhoneCoordinate.accuracy + "] no action");
                    }
                }
            } else if (accuracy + 2.0f < this.lastPhoneCoordinate.accuracy) {
                sLogger.v("ublox accuracy [" + accuracy + "] < phone accuracy[" + this.lastPhoneCoordinate.accuracy + "], start uBloxReporting, switch complete");
                this.firstSwitchToUbloxFromPhone = true;
                markLocationFix(GpsConstants.DEBUG_TTS_UBLOX_SWITCH, "Phone =" + this.lastPhoneCoordinate.accuracy + " ublox =" + accuracy, false, false);
                startUbloxReporting();
            } else if (sLogger.isLoggable(3)) {
                sLogger.v("ublox accuracy [" + accuracy + "] > phone accuracy[" + this.lastPhoneCoordinate.accuracy + "] no action");
            }
        }
    }

    @DexIgnore
    private void markLocationFix(String title, String info, boolean usingPhone, boolean usingUblox) {
        Bundle bundle = new Bundle();
        bundle.putString("title", title);
        bundle.putString("info", info);
        bundle.putBoolean(GpsConstants.USING_PHONE_LOCATION, usingPhone);
        bundle.putBoolean(GpsConstants.USING_UBLOX_LOCATION, usingUblox);
        sendGpsEventBroadcast(GpsConstants.GPS_EVENT_SWITCH, bundle);
    }

    @DexIgnore
    private void setUbloxResetRunnable() {
        sLogger.v("setUbloxResetRunnable");
        this.handler.postDelayed(this.warmResetRunnable, WARM_RESET_INTERVAL);
    }

    @DexIgnore
    private void cancelUbloxResetRunnable() {
        this.handler.removeCallbacks(this.warmResetRunnable);
        this.warmResetCancelled = true;
    }

    @DexIgnore
    public void shutdown() {
        if (this.locationManager != null) {
            this.locationManager.removeUpdates(this.ubloxGpsLocationListener);
            this.locationManager.removeUpdates(this.navdyGpsLocationListener);
            this.locationManager.removeGpsStatusListener(this.ubloxGpsStatusListener);
            this.locationManager.removeNmeaListener(this.ubloxGpsNmeaListener);
        }
    }
}
