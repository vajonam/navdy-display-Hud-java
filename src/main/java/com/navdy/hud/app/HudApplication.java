package com.navdy.hud.app;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.support.multidex.MultiDexApplication;

import com.navdy.hud.app.config.SettingsManager;
import com.navdy.hud.app.device.gps.GpsManager;
import com.navdy.hud.app.event.Shutdown;
import com.navdy.hud.app.event.Shutdown.Reason;
import com.navdy.hud.app.event.Shutdown.State;
import com.navdy.hud.app.manager.UpdateReminderManager;
import com.navdy.hud.app.maps.here.HereMapsManager;
import com.navdy.hud.app.profile.HudLocale;
import com.navdy.hud.app.receiver.LogLevelReceiver;
import com.navdy.hud.app.service.StickyService;
import com.navdy.hud.app.util.CrashReporter;
import com.navdy.service.library.log.LogAppender;
import com.navdy.service.library.log.LogcatAppender;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.squareup.otto.Bus;

import lanchon.dexpatcher.annotation.DexAdd;
import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexReplace;
import mortar.Mortar;
import mortar.MortarScope;

@DexEdit
public class HudApplication extends MultiDexApplication {
    @DexIgnore
    public static /* final */ String NOT_A_CRASH; // = "NAVDY_NOT_A_CRASH";
    @DexIgnore
    private static /* final */ String TAG; // = "HudApplication";
    @DexIgnore
    private static Context sAppContext;
    @DexIgnore
    private static HudApplication sApplication;
    @DexIgnore
    private static /* final */ Logger sLogger; // = new com.navdy.service.library.log.Logger(com.navdy.hud.app.HudApplication.class);
    // @javax.inject.Inject
    @DexIgnore
    public Bus bus;
    @DexIgnore
    private Handler handler; // = new android.os.Handler(android.os.Looper.getMainLooper());
    @DexIgnore
    private boolean initialized;
    @DexIgnore
    private Reason lastSeenReason; // = com.navdy.hud.app.event.Shutdown.Reason.UNKNOWN;
    @DexIgnore
    private MortarScope rootScope;
    @DexIgnore
    private UpdateReminderManager updateReminderManager;

    // @DexIgnore
    // class Anon1 implements Runnable {
    //     @DexIgnore
    //     final /* synthetic */ DriveRecorder val$dataRecorder;
    //
    //     @DexIgnore
    //     Anon1(DriveRecorder driveRecorder) {
    //         this.val$dataRecorder = driveRecorder;
    //     }
    //
    //     @DexIgnore
    //     public void run() {
    //         this.val$dataRecorder.load();
    //     }
    // }
    //
    // @DexIgnore
    // class Anon2 implements Runnable {
    //
    //     @DexIgnore
    //     class Anon1 implements Runnable {
    //         @DexIgnore
    //         Anon1() {
    //         }
    //
    //         @DexIgnore
    //         public void run() {
    //             CrashReporter.getInstance().checkForKernelCrashes(PathManager.getInstance().getKernelCrashFiles());
    //         }
    //     }
    //
    //     @DexIgnore
    //     Anon2() {
    //     }
    //
    //     @DexIgnore
    //     public void run() {
    //         TaskManager.getInstance().execute(new Anon2.Anon1(), 1);
    //     }
    // }
    //
    // @DexIgnore
    // class Anon3 implements Runnable {
    //
    //     @DexIgnore
    //     class Anon1 implements Runnable {
    //         @DexIgnore
    //         Anon1() {
    //         }
    //
    //         @DexIgnore
    //         public void run() {
    //             CrashReporter crashReporter = CrashReporter.getInstance();
    //             crashReporter.checkForAnr();
    //             crashReporter.checkForTombstones();
    //             crashReporter.checkForOTAFailure();
    //         }
    //     }
    //
    //     @DexIgnore
    //     Anon3() {
    //     }
    //
    //     @DexIgnore
    //     public void run() {
    //         TaskManager.getInstance().execute(new Anon3.Anon1(), 1);
    //     }
    // }
    //
    // @DexIgnore
    // class Anon4 implements Runnable {
    //     @DexIgnore
    //     Anon4() {
    //     }
    //
    //     @DexIgnore
    //     public void run() {
    //         long initialTime = SystemClock.elapsedRealtime();
    //         GlympseManager.getInstance();
    //         HudApplication.sLogger.v("time taken by Glympse init: " + (SystemClock.elapsedRealtime() - initialTime) + " ms");
    //     }
    // }

    @DexIgnore
    public static void setContext(Context context) {
        sAppContext = context;
    }

    /* access modifiers changed from: protected */
    @DexReplace
    public void attachBaseContext(Context base) {
        sAppContext = base;
        super.attachBaseContext(HudLocale.onAttach(base));
    }

    @SuppressLint("MissingSuperCall")
    @DexIgnore
    public void onCreate() {
        throw null;
        // if (BuildConfig.DEBUG) {
        //     StrictMode.setThreadPolicy(new ThreadPolicy.Builder().detectAll().penaltyLog().penaltyFlashScreen().penaltyDeathOnNetwork().build());
        //     StrictMode.setVmPolicy(new VmPolicy.Builder().detectAll().penaltyLog().build());
        //     Log.v("", "enabling StrictMode");
        // }
        // Log.e("", "::onCreate locale:" + getResources().getConfiguration().locale);
        // sApplication = this;
        // sAppContext = sApplication;
        // super.onCreate();
        // String processName = SystemUtils.getProcessName(sAppContext, android.os.Process.myPid());
        // String packageName = getPackageName();
        // initLogger(false);
        // sLogger.d(processName + " starting  on " + Build.BRAND + HereManeuverDisplayBuilder.COMMA + Build.HARDWARE + HereManeuverDisplayBuilder.COMMA + Build.MODEL);
        // if (processName.equalsIgnoreCase(packageName + ":connectionService")) {
        //     sLogger.v("startup:" + processName + " :connection service");
        //     initConnectionService(processName);
        // } else if (processName.equalsIgnoreCase(packageName)) {
        //     sLogger.v("startup:" + processName + " :hud app locale=" + Locale.getDefault().toString());
        //     sLogger.v(processName + ":initializing Mortar");
        //     long l1 = SystemClock.elapsedRealtime();
        //     ProdModule module = new ProdModule(this);
        //     this.rootScope = Mortar.createRootScope(BuildConfig.DEBUG, ObjectGraph.create(module));
        //     this.rootScope.getObjectGraph().inject(this);
        //     sLogger.v(processName + ":mortar init took:" + (SystemClock.elapsedRealtime() - l1));
        //     initTaskManager(processName);
        //     sLogger.i("init network state manager");
        //     NetworkStateManager.getInstance();
        //     sLogger.i("*** starting hud sticky-service ***");
        //     Intent intent = new Intent();
        //     intent.setClass(this, StickyService.class);
        //     startService(intent);
        //     sLogger.i("*** started hud sticky-service ***");
        //     IntentFilter filter = new IntentFilter();
        //     filter.addAction(GpsConstants.GPS_EVENT_SWITCH);
        //     filter.addAction(GpsConstants.GPS_EVENT_WARM_RESET_UBLOX);
        //     filter.addAction(GpsConstants.GPS_SATELLITE_STATUS);
        //     filter.addAction(GpsConstants.GPS_COLLECT_LOGS);
        //     filter.addAction(GpsConstants.GPS_EVENT_DRIVING_STARTED);
        //     filter.addAction(GpsConstants.GPS_EVENT_DRIVING_STOPPED);
        //     filter.addAction(GpsConstants.GPS_EVENT_ENABLE_ESF_RAW);
        //     registerReceiver(new GpsEventsReceiver(), filter);
        //     sLogger.v("registered GpsEventsReceiver");
        //     IntentFilter analyticsFilter = new IntentFilter();
        //     analyticsFilter.addAction(AnalyticsSupport.ANALYTICS_INTENT);
        //     registerReceiver(new AnalyticsSupport.AnalyticsIntentsReceiver(), analyticsFilter);
        //     sLogger.v("registered AnalyticsIntentsReceiver");
        //     Intent myIntent = new Intent(this, MainActivity.class);
        //     myIntent.addFlags(268435456);
        //     startActivity(myIntent);
        // } else {
        //     sLogger.v("startup:" + processName + " :no-op");
        // }
        // sLogger.v("startup:" + processName + " :initialization done");
    }

    @DexIgnore
    public MortarScope getRootScope() {
        return this.rootScope;
    }

    @DexIgnore
    public Object getSystemService(String name) {
        if (Mortar.isScopeSystemService(name)) {
            return this.rootScope;
        }
        return super.getSystemService(name);
    }

    @DexIgnore
    public static Context getAppContext() {
        return sAppContext;
    }

    @DexIgnore
    public static HudApplication getApplication() {
        return sApplication;
    }

    @DexIgnore
    private static void initLogger(boolean initializingApp) {
        Logger.init(new LogAppender[]{new LogcatAppender()});
    }

    @DexIgnore
    private void initTaskManager(String processName) {
        sLogger.v(processName + ":initializing taskMgr");
        TaskManager taskManager = TaskManager.getInstance();
        try {
            taskManager.addTaskQueue(1, 3);
            taskManager.addTaskQueue(5, 1);
            taskManager.addTaskQueue(8, 1);
            taskManager.addTaskQueue(6, 1);
            taskManager.addTaskQueue(7, 1);
            taskManager.addTaskQueue(9, 1);
            taskManager.addTaskQueue(10, 1);
            taskManager.addTaskQueue(11, 1);
            taskManager.addTaskQueue(12, 1);
            taskManager.addTaskQueue(13, 1);
            taskManager.addTaskQueue(14, 1);
            taskManager.addTaskQueue(22, 1);
            taskManager.addTaskQueue(15, 1);
            taskManager.addTaskQueue(16, 1);
            taskManager.addTaskQueue(17, 1);
            taskManager.addTaskQueue(18, 1);
            taskManager.addTaskQueue(2, 5);
            taskManager.addTaskQueue(3, 1);
            taskManager.addTaskQueue(4, 1);
            taskManager.addTaskQueue(19, 1);
            taskManager.addTaskQueue(20, 1);
            taskManager.addTaskQueue(21, 1);
            taskManager.addTaskQueue(23, 1);
            taskManager.init();
        } catch (IllegalStateException e) {
            if (!e.getMessage().equals("already initialized")) {
                throw e;
            }
        }
    }

    @DexReplace
    private void initConnectionService(String str) {
        sLogger.v(str + ":initializing taskMgr");
        TaskManager instance = TaskManager.getInstance();
        instance.addTaskQueue(1, 3);
        instance.addTaskQueue(5, 1);
        instance.addTaskQueue(9, 1);
        instance.init();
        this.handler.post(new StartGPS());
        registerReceiver(new LogLevelReceiver(), new IntentFilter(Logger.ACTION_RELOAD));
    }

    @DexAdd
    class StartGPS implements Runnable {
        final Logger ssLogger = new Logger(StartGPS.class);

        @Override
        public void run() {
            ssLogger.v(":initializing gps manager");
            GpsManager.getInstance();
        }
    }

    @DexIgnore
    public void initHudApp() {
        // if (!this.initialized) {
        //     this.initialized = true;
        //     String processName = SystemUtils.getProcessName(sAppContext, android.os.Process.myPid());
        //     NavdyNativeLibWrapper.loadlibrary();
        //     if (CrashReporter.isEnabled()) {
        //         sLogger.v(processName + ":initializing crash reporter");
        //         CrashReporter.getInstance().installCrashHandler(sAppContext);
        //     } else {
        //         sLogger.v(processName + ":crash reporter not installed");
        //     }
        //     if (DeviceUtil.isNavdyDevice()) {
        //         PropsFileUpdater.run();
        //         ProjectorBrightness.init();
        //     }
        //     sLogger.v(processName + ":updating logging setup");
        //     initLogger(true);
        //     IntentFilter otaFilter = new IntentFilter();
        //     otaFilter.addAction(FileTransferHandler.ACTION_OTA_DOWNLOAD);
        //     SharedPreferences prefs = RemoteDeviceManager.getInstance().getSharedPreferences();
        //     if (prefs == null) {
        //         sLogger.e("unable to get SharedPreferences");
        //     } else {
        //         registerReceiver(new OTAUpdateService.OTADownloadIntentsReceiver(prefs), otaFilter);
        //     }
        //     PicassoUtil.initPicasso(sAppContext);
        //     RemoteDeviceManager.getInstance().getSharedPreferences().edit();
        //     try {
        //         sLogger.v("dbase:opening...");
        //         long l1 = SystemClock.elapsedRealtime();
        //         HudDatabase.getInstance().getWritableDatabase();
        //         sLogger.v("dbase: opened[" + (SystemClock.elapsedRealtime() - l1) + "]");
        //     } catch (Throwable th) {
        //         sLogger.e("dbase: error opening", t);
        //     }
        //     AnalyticsSupport.analyticsApplicationInit(this);
        //     TaskManager.getInstance().execute(new Anon1((DriveRecorder) this.rootScope.getObjectGraph().get(DriveRecorder.class)), 1);
        //     sLogger.v(processName + ":initializing here maps engine");
        //     HereMapsManager.getInstance();
        //     sLogger.v(processName + ":called initialized");
        //     String str = HomeScreenResourceValues.speedMph;
        //     ViewRect viewRect = HomeScreenConstants.routeOverviewRect;
        //     int i = SmartDashViewResourceValues.middleGaugeShrinkLeftX;
        //     String s = TTSUtils.TTS_DIAL_BATTERY_EXTREMELY_LOW;
        //     int n = GlanceConstants.colorFacebook;
        //     GlanceHelper.initMessageAttributes();
        //     if (CrashReporter.isEnabled()) {
        //         this.handler.post(new Anon2());
        //         this.handler.postDelayed(new Anon3(), 45000);
        //     }
        //     DialManager.getInstance();
        //     RecentCallManager.getInstance();
        //     PBAPClientManager.getInstance();
        //     ContactImageHelper.getInstance();
        //     FavoriteContactsManager.getInstance();
        //     DestinationsManager.getInstance();
        //     MessageManager.getInstance();
        //     GlanceHandler.getInstance();
        //     ReportIssueService.initialize();
        //     ShutdownMonitor.getInstance();
        //     if (!DeviceUtil.isNavdyDevice()) {
        //         SoundUtils.init();
        //     }
        //     RemoteDeviceManager.getInstance().getFeatureUtil();
        //     if (DeviceUtil.isNavdyDevice()) {
        //         sLogger.v("start dead reckoning mgr");
        //         GpsDeadReckoningManager.getInstance();
        //     }
        //     this.updateReminderManager = new UpdateReminderManager(this);
        //     sLogger.i("init network b/w controller");
        //     NetworkBandwidthController.getInstance();
        //     CpuProfiler.getInstance().start();
        //     this.handler.post(new Anon4());
        //     sLogger.v(processName + ":background init done");
        // }
    }

    @DexIgnore
    public static boolean isDeveloperBuild() {
        return BuildConfig.DEBUG && BuildConfig.VERSION_NAME.endsWith("-dev");
    }

    @DexIgnore
    public Bus getBus() {
        return this.bus;
    }

    @DexIgnore
    public void setShutdownReason(Reason reason) {
        this.lastSeenReason = reason;
    }

    @DexIgnore
    public void shutdown() {
        sLogger.i("shutting down HUD");
        CrashReporter.getInstance().stopCrashReporting(true);
        if (SettingsManager.global.modified) {
            SettingsManager.global.apply_immediate();
        }
        Intent intent = new Intent();
        intent.setClass(this, StickyService.class);
        stopService(intent);
        if (this.bus != null) {
            sLogger.i("shutting down HUD - reason: " + this.lastSeenReason);
            this.bus.post(new Shutdown(this.lastSeenReason, State.SHUTTING_DOWN));
        }
        HereMapsManager.getInstance().getLocationFixManager().shutdown();
    }
}
