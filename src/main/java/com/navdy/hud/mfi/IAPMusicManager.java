package com.navdy.hud.mfi;

import android.text.TextUtils;
import android.util.Log;

import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit
public class IAPMusicManager extends IAPControlMessageProcessor {
    @DexIgnore
    private static /* final */ String TAG; // = com.navdy.hud.mfi.IAPMusicManager.class.getSimpleName();
    @DexIgnore
    public static iAPProcessor.iAPMessage[] mMessages; // = {com.navdy.hud.mfi.iAPProcessor.iAPMessage.NowPlayingUpdate};
    @DexIgnore
    private NowPlayingUpdate aggregatedNowPlayingUpdate; // = new com.navdy.hud.mfi.NowPlayingUpdate();
    @DexIgnore
    private IAPNowPlayingUpdateListener mNowPlayingUpdateListener;

    @DexIgnore
    public enum MediaItemAttributes {
        MediaItemPersistentIdentifier(0),
        MediaItemTitle(1),
        MediaItemPlaybackDurationInMilliSeconds(4),
        MediaItemAlbumTitle(6),
        MediaItemAlbumTrackNumber(7),
        MediaItemAlbumTrackCount(8),
        MediaItemArtist(12),
        MediaItemGenre(16),
        MediaItemArtworkFileTransferIdentifier(26);
        
        int paramIndex;

        @DexIgnore
        private MediaItemAttributes(int index) {
            this.paramIndex = index;
        }

        @DexIgnore
        public int getParam() {
            return this.paramIndex;
        }
    }

    @DexIgnore
    public enum MediaKey {
        Siri,
        PlayPause,
        Next,
        Previous
    }

    @DexIgnore
    public enum NowPlayingUpdatesParameters {
        MediaItemAttributes,
        PlaybackAttributes
    }

    @DexIgnore
    public enum PlaybackAttributes {
        PlaybackStatus(0),
        PlaybackElapsedTimeInMilliseconds(1),
        PlaybackShuffleMode(5),
        PlaybackRepeatMode(6),
        PlaybackAppName(7),
        PlaybackAppBundleId(16);
        
        int paramIndex;

        @DexIgnore
        private PlaybackAttributes(int index) {
            this.paramIndex = index;
        }

        @DexIgnore
        public int getParam() {
            return this.paramIndex;
        }
    }

    @DexIgnore
    public IAPMusicManager(iAPProcessor processor) {
        super(mMessages, processor);
    }

    @DexIgnore
    public void setNowPlayingUpdateListener(IAPNowPlayingUpdateListener nowPlayingUpdateListener) {
        this.mNowPlayingUpdateListener = nowPlayingUpdateListener;
    }

    @DexIgnore
    public void startNowPlayingUpdates() {
        iAPProcessor.IAP2SessionMessage message = new iAPProcessor.IAP2SessionMessage(iAPProcessor.iAPMessage.StartNowPlayingUpdates);
        iAPProcessor.IAP2ParamsCreator mediaItemProperties = new iAPProcessor.IAP2ParamsCreator();
        for (IAPMusicManager.MediaItemAttributes attribute : IAPMusicManager.MediaItemAttributes.values()) {
            mediaItemProperties.addNone(attribute.getParam());
        }
        message.addBlob(NowPlayingUpdatesParameters.MediaItemAttributes, mediaItemProperties.toBytes());
        iAPProcessor.IAP2ParamsCreator playbackProperties = new iAPProcessor.IAP2ParamsCreator();
        for (IAPMusicManager.PlaybackAttributes attribute2 : IAPMusicManager.PlaybackAttributes.values()) {
            playbackProperties.addNone(attribute2.getParam());
        }
        message.addBlob(NowPlayingUpdatesParameters.PlaybackAttributes, playbackProperties.toBytes());
        this.miAPProcessor.sendControlMessage(message);
    }

    @DexIgnore
    public void stopNowPlayingUpdates() {
        this.miAPProcessor.sendControlMessage(new iAPProcessor.IAP2SessionMessage(iAPProcessor.iAPMessage.StopNowPlayingUpdates));
    }

    @DexReplace
    @Override
    public void bProcessControlMessage(iAPProcessor.iAPMessage message, int session, byte[] data) {
        iAPProcessor.IAP2Params params = iAPProcessor.parse(data);
        if (message == iAPProcessor.iAPMessage.NowPlayingUpdate) {
            NowPlayingUpdate update = parseNowPlayingUpdate(params);
            Log.d(TAG, "Parsed " + update);
            mergeNowPlayingUpdate(update);
            if (this.mNowPlayingUpdateListener != null) {
                this.mNowPlayingUpdateListener.onNowPlayingUpdate(this.aggregatedNowPlayingUpdate);
            }
        }
    }

    @DexReplace
    private NowPlayingUpdate parseNowPlayingUpdate(final iAPProcessor.IAP2Params params) {
        final NowPlayingUpdate nowPlayingUpdate = new NowPlayingUpdate();
        if (params.hasParam(NowPlayingUpdatesParameters.MediaItemAttributes)) {
            final iAPProcessor.IAP2Params mediaItemAttributesParams = new iAPProcessor.IAP2Params(params.getBlob(NowPlayingUpdatesParameters.MediaItemAttributes), 0);
            if (mediaItemAttributesParams.hasParam(MediaItemAttributes.MediaItemPersistentIdentifier.getParam())) {
                nowPlayingUpdate.mediaItemPersistentIdentifier = mediaItemAttributesParams.getUInt64(MediaItemAttributes.MediaItemPersistentIdentifier.getParam());
            }
            //Since Apple is not returning "MediaItemPropertyAlbumPersistentIdentifer" (yes, they even have this typo in the documentation), need to rely on string based detection
            if (mediaItemAttributesParams.hasParam(MediaItemAttributes.MediaItemAlbumTitle.getParam()) && mediaItemAttributesParams.hasParam(MediaItemAttributes.MediaItemTitle.getParam())) {
                String newAlbumSong = mediaItemAttributesParams.getUTF8(MediaItemAttributes.MediaItemAlbumTitle.getParam()) + mediaItemAttributesParams.getUTF8(MediaItemAttributes.MediaItemTitle.getParam());
                String originalAlbumSong = this.aggregatedNowPlayingUpdate.mediaItemAlbumTitle + this.aggregatedNowPlayingUpdate.mediaItemTitle;

                //Album change detected, increment the mediaItemArtworkFileTransferIdentifier
                if ( !newAlbumSong.equals(originalAlbumSong) ) {
                    this.aggregatedNowPlayingUpdate.mediaItemArtworkFileTransferIdentifier++;
                    //Once the global FileTransferIdentifier reaches 255 the accessory must immediately reset it to 128 or the lowest inactive FileTransferIdentifier value, whichever is lower, before it initiates the next transfer.
                    if (this.aggregatedNowPlayingUpdate.mediaItemArtworkFileTransferIdentifier > 255) {
                        this.aggregatedNowPlayingUpdate.mediaItemArtworkFileTransferIdentifier = 128;
                    }
                }
            }
            if (mediaItemAttributesParams.hasParam(MediaItemAttributes.MediaItemTitle.getParam())) {
                nowPlayingUpdate.mediaItemTitle = mediaItemAttributesParams.getUTF8(MediaItemAttributes.MediaItemTitle.getParam());
            }
            if (mediaItemAttributesParams.hasParam(MediaItemAttributes.MediaItemAlbumTrackCount.getParam())) {
                nowPlayingUpdate.mediaItemAlbumTrackCount = mediaItemAttributesParams.getUInt16(MediaItemAttributes.MediaItemAlbumTrackCount.getParam());
            }
            if (mediaItemAttributesParams.hasParam(MediaItemAttributes.MediaItemAlbumTrackNumber.getParam())) {
                nowPlayingUpdate.mediaItemAlbumTrackNumber = mediaItemAttributesParams.getUInt16(MediaItemAttributes.MediaItemAlbumTrackNumber.getParam());
            }
            if (mediaItemAttributesParams.hasParam(MediaItemAttributes.MediaItemArtist.getParam())) {
                nowPlayingUpdate.mediaItemArtist = mediaItemAttributesParams.getUTF8(MediaItemAttributes.MediaItemArtist.getParam());
            }
            if (mediaItemAttributesParams.hasParam(MediaItemAttributes.MediaItemAlbumTitle.getParam())) {
                nowPlayingUpdate.mediaItemAlbumTitle = mediaItemAttributesParams.getUTF8(MediaItemAttributes.MediaItemAlbumTitle.getParam());
            }
            if (mediaItemAttributesParams.hasParam(MediaItemAttributes.MediaItemArtworkFileTransferIdentifier.getParam())) {
                nowPlayingUpdate.mediaItemArtworkFileTransferIdentifier = mediaItemAttributesParams.getUInt8(MediaItemAttributes.MediaItemArtworkFileTransferIdentifier.getParam());
            }
            if (mediaItemAttributesParams.hasParam(MediaItemAttributes.MediaItemGenre.getParam())) {
                nowPlayingUpdate.mediaItemGenre = mediaItemAttributesParams.getUTF8(MediaItemAttributes.MediaItemGenre.getParam());
            }
            if (mediaItemAttributesParams.hasParam(MediaItemAttributes.MediaItemPlaybackDurationInMilliSeconds.getParam())) {
                nowPlayingUpdate.mediaItemPlaybackDurationInMilliseconds = mediaItemAttributesParams.getUInt32(MediaItemAttributes.MediaItemPlaybackDurationInMilliSeconds.getParam());
            }
        }
        if (params.hasParam(NowPlayingUpdatesParameters.PlaybackAttributes)) {
            final iAPProcessor.IAP2Params playbackAttributesParams = new iAPProcessor.IAP2Params(params.getBlob(NowPlayingUpdatesParameters.PlaybackAttributes), 0);
            if (playbackAttributesParams.hasParam(PlaybackAttributes.PlaybackStatus.getParam())) {
                nowPlayingUpdate.mPlaybackStatus = playbackAttributesParams.<NowPlayingUpdate.PlaybackStatus>getEnum(NowPlayingUpdate.PlaybackStatus.class, PlaybackAttributes.PlaybackStatus.getParam());
            }
            if (playbackAttributesParams.hasParam(PlaybackAttributes.PlaybackElapsedTimeInMilliseconds.getParam())) {
                nowPlayingUpdate.mPlaybackElapsedTimeMilliseconds = playbackAttributesParams.getUInt32(PlaybackAttributes.PlaybackElapsedTimeInMilliseconds.getParam());
            }
            if (playbackAttributesParams.hasParam(PlaybackAttributes.PlaybackAppName.getParam())) {
                nowPlayingUpdate.mAppName = playbackAttributesParams.getUTF8(PlaybackAttributes.PlaybackAppName.getParam());
            }
            if (playbackAttributesParams.hasParam(PlaybackAttributes.PlaybackAppBundleId.getParam())) {
                nowPlayingUpdate.mAppBundleId = playbackAttributesParams.getUTF8(PlaybackAttributes.PlaybackAppBundleId.getParam());
            }
            if (playbackAttributesParams.hasParam(PlaybackAttributes.PlaybackShuffleMode.getParam())) {
                nowPlayingUpdate.playbackShuffle = playbackAttributesParams.<NowPlayingUpdate.PlaybackShuffle>getEnum(NowPlayingUpdate.PlaybackShuffle.class, PlaybackAttributes.PlaybackShuffleMode.getParam());
            }
            if (playbackAttributesParams.hasParam(PlaybackAttributes.PlaybackRepeatMode.getParam())) {
                nowPlayingUpdate.playbackRepeat = playbackAttributesParams.<NowPlayingUpdate.PlaybackRepeat>getEnum(NowPlayingUpdate.PlaybackRepeat.class, PlaybackAttributes.PlaybackRepeatMode.getParam());
            }
        }
        return nowPlayingUpdate;
    }

    @DexIgnore
    private void mergeNowPlayingUpdate(NowPlayingUpdate incomingUpdate) {
        if (incomingUpdate.mediaItemTitle != null) {
            this.aggregatedNowPlayingUpdate.mediaItemTitle = incomingUpdate.mediaItemTitle;
        }
        if (incomingUpdate.mediaItemAlbumTitle != null) {
            this.aggregatedNowPlayingUpdate.mediaItemAlbumTitle = incomingUpdate.mediaItemAlbumTitle;
        }
        if (incomingUpdate.mediaItemArtist != null) {
            this.aggregatedNowPlayingUpdate.mediaItemArtist = incomingUpdate.mediaItemArtist;
        }
        if (incomingUpdate.mediaItemGenre != null) {
            this.aggregatedNowPlayingUpdate.mediaItemGenre = incomingUpdate.mediaItemGenre;
        }
        if (incomingUpdate.mPlaybackStatus != null) {
            this.aggregatedNowPlayingUpdate.mPlaybackStatus = incomingUpdate.mPlaybackStatus;
        }
        if (incomingUpdate.playbackShuffle != null) {
            this.aggregatedNowPlayingUpdate.playbackShuffle = incomingUpdate.playbackShuffle;
        }
        if (incomingUpdate.playbackRepeat != null) {
            this.aggregatedNowPlayingUpdate.playbackRepeat = incomingUpdate.playbackRepeat;
        }
        if (incomingUpdate.mediaItemPersistentIdentifier != null) {
            this.aggregatedNowPlayingUpdate.mediaItemPersistentIdentifier = incomingUpdate.mediaItemPersistentIdentifier;
        }
        if (incomingUpdate.mediaItemPlaybackDurationInMilliseconds > 0) {
            this.aggregatedNowPlayingUpdate.mediaItemPlaybackDurationInMilliseconds = incomingUpdate.mediaItemPlaybackDurationInMilliseconds;
        }
        if (incomingUpdate.mediaItemAlbumTrackNumber >= 0) {
            this.aggregatedNowPlayingUpdate.mediaItemAlbumTrackNumber = incomingUpdate.mediaItemAlbumTrackNumber;
        }
        if (incomingUpdate.mediaItemAlbumTrackCount >= 0) {
            this.aggregatedNowPlayingUpdate.mediaItemAlbumTrackCount = incomingUpdate.mediaItemAlbumTrackCount;
        }
        if (incomingUpdate.mPlaybackElapsedTimeMilliseconds >= 0) {
            this.aggregatedNowPlayingUpdate.mPlaybackElapsedTimeMilliseconds = incomingUpdate.mPlaybackElapsedTimeMilliseconds;
        }
        if (!TextUtils.isEmpty(incomingUpdate.mAppName)) {
            this.aggregatedNowPlayingUpdate.mAppName = incomingUpdate.mAppName;
        }
        if (!TextUtils.isEmpty(incomingUpdate.mAppBundleId)) {
            this.aggregatedNowPlayingUpdate.mAppBundleId = incomingUpdate.mAppBundleId;
        }
        if (incomingUpdate.mediaItemArtworkFileTransferIdentifier >= 128 && incomingUpdate.mediaItemArtworkFileTransferIdentifier <= 255) {
            this.aggregatedNowPlayingUpdate.mediaItemArtworkFileTransferIdentifier = incomingUpdate.mediaItemArtworkFileTransferIdentifier;
        }
    }

    @DexIgnore
    public void onKeyDown(IAPMusicManager.MediaKey key) {
        this.miAPProcessor.onKeyDown(key);
    }

    @DexIgnore
    public void onKeyUp(IAPMusicManager.MediaKey key) {
        this.miAPProcessor.onKeyUp(key);
    }

    @DexIgnore
    public void onKeyDown(int key) {
        this.miAPProcessor.onKeyDown(key);
    }

    @DexIgnore
    public void onKeyUp(int key) {
        this.miAPProcessor.onKeyUp(key);
    }
}
