package com.navdy.obd;

public class VoltageSettings implements android.os.Parcelable {
    public static final android.os.Parcelable.Creator<com.navdy.obd.VoltageSettings> CREATOR = new com.navdy.obd.VoltageSettings.Anon1();
    public static final float DEFAULT_CHARGING_VOLTAGE = 13.1f;
    public static final float DEFAULT_ENGINE_OFF_VOLTAGE = 12.9f;
    public static final float DEFAULT_LOW_BATTERY_VOLTAGE = 12.2f;
    public final float chargingVoltage;
    public final float engineOffVoltage;
    public final float lowBatteryVoltage;

    static class Anon1 implements android.os.Parcelable.Creator<com.navdy.obd.VoltageSettings> {
        Anon1() {
        }

        public com.navdy.obd.VoltageSettings createFromParcel(android.os.Parcel source) {
            return new com.navdy.obd.VoltageSettings(source);
        }

        public com.navdy.obd.VoltageSettings[] newArray(int size) {
            return new com.navdy.obd.VoltageSettings[size];
        }
    }

    public VoltageSettings() {
        this.lowBatteryVoltage = 12.2f;
        this.engineOffVoltage = 12.9f;
        this.chargingVoltage = 13.1f;
    }

    public VoltageSettings(float lowerBatteryVoltage, float engineOffVoltage2, float chargingVoltage2) {
        this.lowBatteryVoltage = lowerBatteryVoltage;
        this.engineOffVoltage = engineOffVoltage2;
        this.chargingVoltage = chargingVoltage2;
    }

    public VoltageSettings(android.os.Parcel in) {
        this.lowBatteryVoltage = in.readFloat();
        this.engineOffVoltage = in.readFloat();
        this.chargingVoltage = in.readFloat();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeFloat(this.lowBatteryVoltage);
        dest.writeFloat(this.engineOffVoltage);
        dest.writeFloat(this.chargingVoltage);
    }

    public java.lang.String toString() {
        java.lang.StringBuffer sb = new java.lang.StringBuffer("VoltageSettings{");
        sb.append("lowBatteryVoltage=").append(this.lowBatteryVoltage);
        sb.append(", engineOffVoltage=").append(this.engineOffVoltage);
        sb.append(", chargingVoltage=").append(this.chargingVoltage);
        sb.append('}');
        return sb.toString();
    }
}
