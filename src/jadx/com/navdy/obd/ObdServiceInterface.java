package com.navdy.obd;

public class ObdServiceInterface {
    public static final java.lang.String ACTION_RESCAN = "com.navdy.obd.action.RESCAN";
    public static final java.lang.String ACTION_START_AUTO_CONNECT = "com.navdy.obd.action.START_AUTO_CONNECT";
    public static final java.lang.String ACTION_STOP_AUTO_CONNECT = "com.navdy.obd.action.STOP_AUTO_CONNECT";
    public static final java.lang.String CAN_BUS_MONITOR_ERROR_MESSAGE_BAD_STATE = "BAD_STATE";
    public static final java.lang.String GENERIC_CAN_BUS_MONITOR_ERROR_MESSAGE = "NO_DATA";
    public static final int MODE_J1939 = 1;
    public static final int MODE_OBD2 = 0;
    public static final java.lang.String OBD_CLASS_NAME = "com.navdy.obd.ObdService";
    public static final java.lang.String OBD_PACKAGE_NAME = "com.navdy.obd.app.ObdTestApp";

    public static android.content.ComponentName getObdServiceComponent() {
        return new android.content.ComponentName(OBD_PACKAGE_NAME, OBD_CLASS_NAME);
    }

    public static void startObdService(android.content.Context context) {
        android.content.Intent intent = new android.content.Intent();
        intent.setComponent(getObdServiceComponent());
        intent.setAction(ACTION_START_AUTO_CONNECT);
        context.startService(intent);
    }

    public static void stopObdService(android.content.Context context) {
        android.content.Intent intent = new android.content.Intent();
        intent.setComponent(getObdServiceComponent());
        intent.setAction(ACTION_STOP_AUTO_CONNECT);
        context.startService(intent);
    }
}
