package com.navdy.obd;

public class ECU implements android.os.Parcelable {
    public static final android.os.Parcelable.Creator<com.navdy.obd.ECU> CREATOR = new com.navdy.obd.ECU.Anon1();
    public final int address;
    public final com.navdy.obd.PidSet supportedPids;

    static class Anon1 implements android.os.Parcelable.Creator<com.navdy.obd.ECU> {
        Anon1() {
        }

        public com.navdy.obd.ECU createFromParcel(android.os.Parcel source) {
            return new com.navdy.obd.ECU(source);
        }

        public com.navdy.obd.ECU[] newArray(int size) {
            return new com.navdy.obd.ECU[size];
        }
    }

    public ECU(int address2, com.navdy.obd.PidSet supportedPids2) {
        this.address = address2;
        this.supportedPids = supportedPids2;
    }

    public int describeContents() {
        return 0;
    }

    public ECU(android.os.Parcel in) {
        this.address = in.readInt();
        this.supportedPids = (com.navdy.obd.PidSet) in.readParcelable(com.navdy.obd.PidSet.class.getClassLoader());
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeInt(this.address);
        dest.writeParcelable(this.supportedPids, 0);
    }
}
