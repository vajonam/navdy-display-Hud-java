package com.navdy.obd;

public interface ICanBusMonitoringListener extends android.os.IInterface {

    public static abstract class Stub extends android.os.Binder implements com.navdy.obd.ICanBusMonitoringListener {
        private static final java.lang.String DESCRIPTOR = "com.navdy.obd.ICanBusMonitoringListener";
        static final int TRANSACTION_getGpsSpeed = 4;
        static final int TRANSACTION_getLatitude = 5;
        static final int TRANSACTION_getLongitude = 6;
        static final int TRANSACTION_getMake = 8;
        static final int TRANSACTION_getModel = 9;
        static final int TRANSACTION_getYear = 10;
        static final int TRANSACTION_isMonitoringLimitReached = 7;
        static final int TRANSACTION_onCanBusMonitorSuccess = 3;
        static final int TRANSACTION_onCanBusMonitoringError = 2;
        static final int TRANSACTION_onNewDataAvailable = 1;

        private static class Proxy implements com.navdy.obd.ICanBusMonitoringListener {
            private android.os.IBinder mRemote;

            Proxy(android.os.IBinder remote) {
                this.mRemote = remote;
            }

            public android.os.IBinder asBinder() {
                return this.mRemote;
            }

            public java.lang.String getInterfaceDescriptor() {
                return com.navdy.obd.ICanBusMonitoringListener.Stub.DESCRIPTOR;
            }

            public void onNewDataAvailable(java.lang.String fileName) throws android.os.RemoteException {
                android.os.Parcel _data = android.os.Parcel.obtain();
                android.os.Parcel _reply = android.os.Parcel.obtain();
                try {
                    _data.writeInterfaceToken(com.navdy.obd.ICanBusMonitoringListener.Stub.DESCRIPTOR);
                    _data.writeString(fileName);
                    this.mRemote.transact(1, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void onCanBusMonitoringError(java.lang.String errorMessage) throws android.os.RemoteException {
                android.os.Parcel _data = android.os.Parcel.obtain();
                android.os.Parcel _reply = android.os.Parcel.obtain();
                try {
                    _data.writeInterfaceToken(com.navdy.obd.ICanBusMonitoringListener.Stub.DESCRIPTOR);
                    _data.writeString(errorMessage);
                    this.mRemote.transact(2, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void onCanBusMonitorSuccess(int averageAmountOfData) throws android.os.RemoteException {
                android.os.Parcel _data = android.os.Parcel.obtain();
                android.os.Parcel _reply = android.os.Parcel.obtain();
                try {
                    _data.writeInterfaceToken(com.navdy.obd.ICanBusMonitoringListener.Stub.DESCRIPTOR);
                    _data.writeInt(averageAmountOfData);
                    this.mRemote.transact(3, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public int getGpsSpeed() throws android.os.RemoteException {
                android.os.Parcel _data = android.os.Parcel.obtain();
                android.os.Parcel _reply = android.os.Parcel.obtain();
                try {
                    _data.writeInterfaceToken(com.navdy.obd.ICanBusMonitoringListener.Stub.DESCRIPTOR);
                    this.mRemote.transact(4, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readInt();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public double getLatitude() throws android.os.RemoteException {
                android.os.Parcel _data = android.os.Parcel.obtain();
                android.os.Parcel _reply = android.os.Parcel.obtain();
                try {
                    _data.writeInterfaceToken(com.navdy.obd.ICanBusMonitoringListener.Stub.DESCRIPTOR);
                    this.mRemote.transact(5, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readDouble();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public double getLongitude() throws android.os.RemoteException {
                android.os.Parcel _data = android.os.Parcel.obtain();
                android.os.Parcel _reply = android.os.Parcel.obtain();
                try {
                    _data.writeInterfaceToken(com.navdy.obd.ICanBusMonitoringListener.Stub.DESCRIPTOR);
                    this.mRemote.transact(6, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readDouble();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean isMonitoringLimitReached() throws android.os.RemoteException {
                boolean _result = false;
                android.os.Parcel _data = android.os.Parcel.obtain();
                android.os.Parcel _reply = android.os.Parcel.obtain();
                try {
                    _data.writeInterfaceToken(com.navdy.obd.ICanBusMonitoringListener.Stub.DESCRIPTOR);
                    this.mRemote.transact(7, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public java.lang.String getMake() throws android.os.RemoteException {
                android.os.Parcel _data = android.os.Parcel.obtain();
                android.os.Parcel _reply = android.os.Parcel.obtain();
                try {
                    _data.writeInterfaceToken(com.navdy.obd.ICanBusMonitoringListener.Stub.DESCRIPTOR);
                    this.mRemote.transact(8, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readString();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public java.lang.String getModel() throws android.os.RemoteException {
                android.os.Parcel _data = android.os.Parcel.obtain();
                android.os.Parcel _reply = android.os.Parcel.obtain();
                try {
                    _data.writeInterfaceToken(com.navdy.obd.ICanBusMonitoringListener.Stub.DESCRIPTOR);
                    this.mRemote.transact(9, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readString();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public java.lang.String getYear() throws android.os.RemoteException {
                android.os.Parcel _data = android.os.Parcel.obtain();
                android.os.Parcel _reply = android.os.Parcel.obtain();
                try {
                    _data.writeInterfaceToken(com.navdy.obd.ICanBusMonitoringListener.Stub.DESCRIPTOR);
                    this.mRemote.transact(10, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readString();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }
        }

        public Stub() {
            attachInterface(this, DESCRIPTOR);
        }

        public static com.navdy.obd.ICanBusMonitoringListener asInterface(android.os.IBinder obj) {
            if (obj == null) {
                return null;
            }
            android.os.IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
            if (iin == null || !(iin instanceof com.navdy.obd.ICanBusMonitoringListener)) {
                return new com.navdy.obd.ICanBusMonitoringListener.Stub.Proxy(obj);
            }
            return (com.navdy.obd.ICanBusMonitoringListener) iin;
        }

        public android.os.IBinder asBinder() {
            return this;
        }

        public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException {
            switch (code) {
                case 1:
                    data.enforceInterface(DESCRIPTOR);
                    onNewDataAvailable(data.readString());
                    reply.writeNoException();
                    return true;
                case 2:
                    data.enforceInterface(DESCRIPTOR);
                    onCanBusMonitoringError(data.readString());
                    reply.writeNoException();
                    return true;
                case 3:
                    data.enforceInterface(DESCRIPTOR);
                    onCanBusMonitorSuccess(data.readInt());
                    reply.writeNoException();
                    return true;
                case 4:
                    data.enforceInterface(DESCRIPTOR);
                    int _result = getGpsSpeed();
                    reply.writeNoException();
                    reply.writeInt(_result);
                    return true;
                case 5:
                    data.enforceInterface(DESCRIPTOR);
                    double _result2 = getLatitude();
                    reply.writeNoException();
                    reply.writeDouble(_result2);
                    return true;
                case 6:
                    data.enforceInterface(DESCRIPTOR);
                    double _result3 = getLongitude();
                    reply.writeNoException();
                    reply.writeDouble(_result3);
                    return true;
                case 7:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result4 = isMonitoringLimitReached();
                    reply.writeNoException();
                    reply.writeInt(_result4 ? 1 : 0);
                    return true;
                case 8:
                    data.enforceInterface(DESCRIPTOR);
                    java.lang.String _result5 = getMake();
                    reply.writeNoException();
                    reply.writeString(_result5);
                    return true;
                case 9:
                    data.enforceInterface(DESCRIPTOR);
                    java.lang.String _result6 = getModel();
                    reply.writeNoException();
                    reply.writeString(_result6);
                    return true;
                case 10:
                    data.enforceInterface(DESCRIPTOR);
                    java.lang.String _result7 = getYear();
                    reply.writeNoException();
                    reply.writeString(_result7);
                    return true;
                case 1598968902:
                    reply.writeString(DESCRIPTOR);
                    return true;
                default:
                    return super.onTransact(code, data, reply, flags);
            }
        }
    }

    int getGpsSpeed() throws android.os.RemoteException;

    double getLatitude() throws android.os.RemoteException;

    double getLongitude() throws android.os.RemoteException;

    java.lang.String getMake() throws android.os.RemoteException;

    java.lang.String getModel() throws android.os.RemoteException;

    java.lang.String getYear() throws android.os.RemoteException;

    boolean isMonitoringLimitReached() throws android.os.RemoteException;

    void onCanBusMonitorSuccess(int i) throws android.os.RemoteException;

    void onCanBusMonitoringError(java.lang.String str) throws android.os.RemoteException;

    void onNewDataAvailable(java.lang.String str) throws android.os.RemoteException;
}
