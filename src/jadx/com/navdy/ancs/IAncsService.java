package com.navdy.ancs;

public interface IAncsService extends android.os.IInterface {

    public static abstract class Stub extends android.os.Binder implements com.navdy.ancs.IAncsService {
        private static final java.lang.String DESCRIPTOR = "com.navdy.ancs.IAncsService";
        static final int TRANSACTION_addListener = 1;
        static final int TRANSACTION_connect = 3;
        static final int TRANSACTION_connectToDevice = 9;
        static final int TRANSACTION_connectToService = 4;
        static final int TRANSACTION_disconnect = 5;
        static final int TRANSACTION_getState = 7;
        static final int TRANSACTION_performNotificationAction = 6;
        static final int TRANSACTION_removeListener = 2;
        static final int TRANSACTION_setNotificationFilter = 8;

        private static class Proxy implements com.navdy.ancs.IAncsService {
            private android.os.IBinder mRemote;

            Proxy(android.os.IBinder remote) {
                this.mRemote = remote;
            }

            public android.os.IBinder asBinder() {
                return this.mRemote;
            }

            public java.lang.String getInterfaceDescriptor() {
                return com.navdy.ancs.IAncsService.Stub.DESCRIPTOR;
            }

            public void addListener(com.navdy.ancs.IAncsServiceListener listener) throws android.os.RemoteException {
                android.os.Parcel _data = android.os.Parcel.obtain();
                android.os.Parcel _reply = android.os.Parcel.obtain();
                try {
                    _data.writeInterfaceToken(com.navdy.ancs.IAncsService.Stub.DESCRIPTOR);
                    _data.writeStrongBinder(listener != null ? listener.asBinder() : null);
                    this.mRemote.transact(1, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void removeListener(com.navdy.ancs.IAncsServiceListener listener) throws android.os.RemoteException {
                android.os.Parcel _data = android.os.Parcel.obtain();
                android.os.Parcel _reply = android.os.Parcel.obtain();
                try {
                    _data.writeInterfaceToken(com.navdy.ancs.IAncsService.Stub.DESCRIPTOR);
                    _data.writeStrongBinder(listener != null ? listener.asBinder() : null);
                    this.mRemote.transact(2, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void connect(java.lang.String address) throws android.os.RemoteException {
                android.os.Parcel _data = android.os.Parcel.obtain();
                android.os.Parcel _reply = android.os.Parcel.obtain();
                try {
                    _data.writeInterfaceToken(com.navdy.ancs.IAncsService.Stub.DESCRIPTOR);
                    _data.writeString(address);
                    this.mRemote.transact(3, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void connectToService(android.os.ParcelUuid uuid) throws android.os.RemoteException {
                android.os.Parcel _data = android.os.Parcel.obtain();
                android.os.Parcel _reply = android.os.Parcel.obtain();
                try {
                    _data.writeInterfaceToken(com.navdy.ancs.IAncsService.Stub.DESCRIPTOR);
                    if (uuid != null) {
                        _data.writeInt(1);
                        uuid.writeToParcel(_data, 0);
                    } else {
                        _data.writeInt(0);
                    }
                    this.mRemote.transact(4, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void disconnect() throws android.os.RemoteException {
                android.os.Parcel _data = android.os.Parcel.obtain();
                android.os.Parcel _reply = android.os.Parcel.obtain();
                try {
                    _data.writeInterfaceToken(com.navdy.ancs.IAncsService.Stub.DESCRIPTOR);
                    this.mRemote.transact(5, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void performNotificationAction(int notificationUid, int actionId) throws android.os.RemoteException {
                android.os.Parcel _data = android.os.Parcel.obtain();
                android.os.Parcel _reply = android.os.Parcel.obtain();
                try {
                    _data.writeInterfaceToken(com.navdy.ancs.IAncsService.Stub.DESCRIPTOR);
                    _data.writeInt(notificationUid);
                    _data.writeInt(actionId);
                    this.mRemote.transact(6, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public int getState() throws android.os.RemoteException {
                android.os.Parcel _data = android.os.Parcel.obtain();
                android.os.Parcel _reply = android.os.Parcel.obtain();
                try {
                    _data.writeInterfaceToken(com.navdy.ancs.IAncsService.Stub.DESCRIPTOR);
                    this.mRemote.transact(7, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readInt();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void setNotificationFilter(java.util.List<java.lang.String> appWhiteList, long newerThanTimestamp) throws android.os.RemoteException {
                android.os.Parcel _data = android.os.Parcel.obtain();
                android.os.Parcel _reply = android.os.Parcel.obtain();
                try {
                    _data.writeInterfaceToken(com.navdy.ancs.IAncsService.Stub.DESCRIPTOR);
                    _data.writeStringList(appWhiteList);
                    _data.writeLong(newerThanTimestamp);
                    this.mRemote.transact(8, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void connectToDevice(android.os.ParcelUuid uuid, java.lang.String address) throws android.os.RemoteException {
                android.os.Parcel _data = android.os.Parcel.obtain();
                android.os.Parcel _reply = android.os.Parcel.obtain();
                try {
                    _data.writeInterfaceToken(com.navdy.ancs.IAncsService.Stub.DESCRIPTOR);
                    if (uuid != null) {
                        _data.writeInt(1);
                        uuid.writeToParcel(_data, 0);
                    } else {
                        _data.writeInt(0);
                    }
                    _data.writeString(address);
                    this.mRemote.transact(9, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }
        }

        public Stub() {
            attachInterface(this, DESCRIPTOR);
        }

        public static com.navdy.ancs.IAncsService asInterface(android.os.IBinder obj) {
            if (obj == null) {
                return null;
            }
            android.os.IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
            if (iin == null || !(iin instanceof com.navdy.ancs.IAncsService)) {
                return new com.navdy.ancs.IAncsService.Stub.Proxy(obj);
            }
            return (com.navdy.ancs.IAncsService) iin;
        }

        public android.os.IBinder asBinder() {
            return this;
        }

        public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException {
            android.os.ParcelUuid _arg0;
            android.os.ParcelUuid _arg02;
            switch (code) {
                case 1:
                    data.enforceInterface(DESCRIPTOR);
                    addListener(com.navdy.ancs.IAncsServiceListener.Stub.asInterface(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case 2:
                    data.enforceInterface(DESCRIPTOR);
                    removeListener(com.navdy.ancs.IAncsServiceListener.Stub.asInterface(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case 3:
                    data.enforceInterface(DESCRIPTOR);
                    connect(data.readString());
                    reply.writeNoException();
                    return true;
                case 4:
                    data.enforceInterface(DESCRIPTOR);
                    if (data.readInt() != 0) {
                        _arg02 = (android.os.ParcelUuid) android.os.ParcelUuid.CREATOR.createFromParcel(data);
                    } else {
                        _arg02 = null;
                    }
                    connectToService(_arg02);
                    reply.writeNoException();
                    return true;
                case 5:
                    data.enforceInterface(DESCRIPTOR);
                    disconnect();
                    reply.writeNoException();
                    return true;
                case 6:
                    data.enforceInterface(DESCRIPTOR);
                    performNotificationAction(data.readInt(), data.readInt());
                    reply.writeNoException();
                    return true;
                case 7:
                    data.enforceInterface(DESCRIPTOR);
                    int _result = getState();
                    reply.writeNoException();
                    reply.writeInt(_result);
                    return true;
                case 8:
                    data.enforceInterface(DESCRIPTOR);
                    setNotificationFilter(data.createStringArrayList(), data.readLong());
                    reply.writeNoException();
                    return true;
                case 9:
                    data.enforceInterface(DESCRIPTOR);
                    if (data.readInt() != 0) {
                        _arg0 = (android.os.ParcelUuid) android.os.ParcelUuid.CREATOR.createFromParcel(data);
                    } else {
                        _arg0 = null;
                    }
                    connectToDevice(_arg0, data.readString());
                    reply.writeNoException();
                    return true;
                case 1598968902:
                    reply.writeString(DESCRIPTOR);
                    return true;
                default:
                    return super.onTransact(code, data, reply, flags);
            }
        }
    }

    void addListener(com.navdy.ancs.IAncsServiceListener iAncsServiceListener) throws android.os.RemoteException;

    void connect(java.lang.String str) throws android.os.RemoteException;

    void connectToDevice(android.os.ParcelUuid parcelUuid, java.lang.String str) throws android.os.RemoteException;

    void connectToService(android.os.ParcelUuid parcelUuid) throws android.os.RemoteException;

    void disconnect() throws android.os.RemoteException;

    int getState() throws android.os.RemoteException;

    void performNotificationAction(int i, int i2) throws android.os.RemoteException;

    void removeListener(com.navdy.ancs.IAncsServiceListener iAncsServiceListener) throws android.os.RemoteException;

    void setNotificationFilter(java.util.List<java.lang.String> list, long j) throws android.os.RemoteException;
}
