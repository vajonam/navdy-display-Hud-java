package com.navdy.ancs;

public interface IAncsServiceListener extends android.os.IInterface {

    public static abstract class Stub extends android.os.Binder implements com.navdy.ancs.IAncsServiceListener {
        private static final java.lang.String DESCRIPTOR = "com.navdy.ancs.IAncsServiceListener";
        static final int TRANSACTION_onConnectionStateChange = 1;
        static final int TRANSACTION_onNotification = 2;

        private static class Proxy implements com.navdy.ancs.IAncsServiceListener {
            private android.os.IBinder mRemote;

            Proxy(android.os.IBinder remote) {
                this.mRemote = remote;
            }

            public android.os.IBinder asBinder() {
                return this.mRemote;
            }

            public java.lang.String getInterfaceDescriptor() {
                return com.navdy.ancs.IAncsServiceListener.Stub.DESCRIPTOR;
            }

            public void onConnectionStateChange(int newState) throws android.os.RemoteException {
                android.os.Parcel _data = android.os.Parcel.obtain();
                android.os.Parcel _reply = android.os.Parcel.obtain();
                try {
                    _data.writeInterfaceToken(com.navdy.ancs.IAncsServiceListener.Stub.DESCRIPTOR);
                    _data.writeInt(newState);
                    this.mRemote.transact(1, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void onNotification(com.navdy.ancs.AppleNotification notification) throws android.os.RemoteException {
                android.os.Parcel _data = android.os.Parcel.obtain();
                android.os.Parcel _reply = android.os.Parcel.obtain();
                try {
                    _data.writeInterfaceToken(com.navdy.ancs.IAncsServiceListener.Stub.DESCRIPTOR);
                    if (notification != null) {
                        _data.writeInt(1);
                        notification.writeToParcel(_data, 0);
                    } else {
                        _data.writeInt(0);
                    }
                    this.mRemote.transact(2, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }
        }

        public Stub() {
            attachInterface(this, DESCRIPTOR);
        }

        public static com.navdy.ancs.IAncsServiceListener asInterface(android.os.IBinder obj) {
            if (obj == null) {
                return null;
            }
            android.os.IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
            if (iin == null || !(iin instanceof com.navdy.ancs.IAncsServiceListener)) {
                return new com.navdy.ancs.IAncsServiceListener.Stub.Proxy(obj);
            }
            return (com.navdy.ancs.IAncsServiceListener) iin;
        }

        public android.os.IBinder asBinder() {
            return this;
        }

        public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException {
            com.navdy.ancs.AppleNotification _arg0;
            switch (code) {
                case 1:
                    data.enforceInterface(DESCRIPTOR);
                    onConnectionStateChange(data.readInt());
                    reply.writeNoException();
                    return true;
                case 2:
                    data.enforceInterface(DESCRIPTOR);
                    if (data.readInt() != 0) {
                        _arg0 = (com.navdy.ancs.AppleNotification) com.navdy.ancs.AppleNotification.CREATOR.createFromParcel(data);
                    } else {
                        _arg0 = null;
                    }
                    onNotification(_arg0);
                    reply.writeNoException();
                    return true;
                case 1598968902:
                    reply.writeString(DESCRIPTOR);
                    return true;
                default:
                    return super.onTransact(code, data, reply, flags);
            }
        }
    }

    void onConnectionStateChange(int i) throws android.os.RemoteException;

    void onNotification(com.navdy.ancs.AppleNotification appleNotification) throws android.os.RemoteException;
}
