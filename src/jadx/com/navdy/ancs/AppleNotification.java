package com.navdy.ancs;

public class AppleNotification implements android.os.Parcelable {
    public static final int ACTION_ID_NEGATIVE = 1;
    public static final int ACTION_ID_POSITIVE = 0;
    public static final int ATTRIBUTE_APP_IDENTIFIER = 0;
    public static final int ATTRIBUTE_DATE = 5;
    public static final int ATTRIBUTE_MESSAGE = 3;
    public static final int ATTRIBUTE_MESSAGE_SIZE = 4;
    public static final int ATTRIBUTE_NEGATIVE_ACTION_LABEL = 7;
    public static final int ATTRIBUTE_POSITIVE_ACTION_LABEL = 6;
    public static final int ATTRIBUTE_SUBTITLE = 2;
    public static final int ATTRIBUTE_TITLE = 1;
    public static final int CATEGORY_BUSINESS_AND_FINANCE = 9;
    public static final int CATEGORY_EMAIL = 6;
    public static final int CATEGORY_ENTERTAINMENT = 11;
    public static final int CATEGORY_HEALTH_AND_FITNESS = 8;
    public static final int CATEGORY_INCOMING_CALL = 1;
    public static final int CATEGORY_LOCATION = 10;
    public static final int CATEGORY_MISSED_CALL = 2;
    public static final int CATEGORY_NEWS = 7;
    public static final int CATEGORY_OTHER = 0;
    public static final int CATEGORY_SCHEDULE = 5;
    public static final int CATEGORY_SOCIAL = 4;
    public static final int CATEGORY_VOICE_MAIL = 3;
    public static final android.os.Parcelable.Creator<com.navdy.ancs.AppleNotification> CREATOR = new com.navdy.ancs.AppleNotification.Anon1();
    public static final int EVENT_ADDED = 0;
    static final int EVENT_FLAG_IMPORTANT = 2;
    static final int EVENT_FLAG_NEGATIVE_ACTION = 16;
    static final int EVENT_FLAG_POSITIVE_ACTION = 8;
    static final int EVENT_FLAG_PRE_EXISTING = 4;
    static final int EVENT_FLAG_SILENT = 1;
    public static final int EVENT_MODIFIED = 1;
    public static final int EVENT_REMOVED = 2;
    private static final java.lang.String TAG = com.navdy.ancs.AppleNotification.class.getSimpleName();
    private java.lang.String appId;
    private java.lang.String appName;
    private int categoryCount;
    private int categoryId;
    private java.util.Date date;
    private int eventFlags;
    private int eventId;
    private java.lang.String message;
    private java.lang.String negativeActionLabel;
    private int notificationUid;
    private java.lang.String positiveActionLabel;
    private java.lang.String subTitle;
    private java.lang.String title;

    static class Anon1 implements android.os.Parcelable.Creator<com.navdy.ancs.AppleNotification> {
        Anon1() {
        }

        public com.navdy.ancs.AppleNotification createFromParcel(android.os.Parcel source) {
            return new com.navdy.ancs.AppleNotification(source);
        }

        public com.navdy.ancs.AppleNotification[] newArray(int size) {
            return new com.navdy.ancs.AppleNotification[size];
        }
    }

    public static com.navdy.ancs.AppleNotification parse(byte[] bytes) {
        return new com.navdy.ancs.AppleNotification(bytes[0] & 255, bytes[1] & 255, bytes[2] & 255, bytes[3] & 255, bytesToInt(bytes, 4));
    }

    public AppleNotification(int eventId2, int eventFlags2, int categoryId2, int categoryCount2, int notificationUid2) {
        this.eventId = eventId2;
        this.categoryId = categoryId2;
        this.categoryCount = categoryCount2;
        this.eventFlags = eventFlags2;
        this.notificationUid = notificationUid2;
    }

    public boolean isSilent() {
        return (this.eventFlags & 1) != 0;
    }

    public boolean isImportant() {
        return (this.eventFlags & 2) != 0;
    }

    public boolean isPreExisting() {
        return (this.eventFlags & 4) != 0;
    }

    public boolean hasPositiveAction() {
        return (this.eventFlags & 8) != 0;
    }

    public boolean hasNegativeAction() {
        return (this.eventFlags & 16) != 0;
    }

    public int getEventId() {
        return this.eventId;
    }

    public int getCategoryId() {
        return this.categoryId;
    }

    public int getCategoryCount() {
        return this.categoryCount;
    }

    public int getEventFlags() {
        return this.eventFlags;
    }

    public int getNotificationUid() {
        return this.notificationUid;
    }

    public java.lang.String getTitle() {
        return this.title;
    }

    public void setTitle(java.lang.String title2) {
        this.title = title2;
    }

    public java.lang.String getSubTitle() {
        return this.subTitle;
    }

    public void setSubTitle(java.lang.String subTitle2) {
        this.subTitle = subTitle2;
    }

    public java.lang.String getMessage() {
        return this.message;
    }

    public void setMessage(java.lang.String message2) {
        this.message = message2;
    }

    public java.util.Date getDate() {
        return this.date;
    }

    public void setDate(java.util.Date date2) {
        this.date = date2;
    }

    public java.lang.String getAppId() {
        return this.appId;
    }

    public void setAppId(java.lang.String appId2) {
        this.appId = appId2;
    }

    public java.lang.String getPositiveActionLabel() {
        return this.positiveActionLabel;
    }

    public void setPositiveActionLabel(java.lang.String positiveActionLabel2) {
        this.positiveActionLabel = positiveActionLabel2;
    }

    public java.lang.String getNegativeActionLabel() {
        return this.negativeActionLabel;
    }

    public void setNegativeActionLabel(java.lang.String negativeActionLabel2) {
        this.negativeActionLabel = negativeActionLabel2;
    }

    public java.lang.String getAppName() {
        return this.appName;
    }

    public void setAppName(java.lang.String appName2) {
        this.appName = appName2;
    }

    public void setAttribute(int attributeId, java.lang.String attributeValue) {
        switch (attributeId) {
            case 0:
                setAppId(attributeValue);
                return;
            case 1:
                setTitle(attributeValue);
                return;
            case 2:
                setSubTitle(attributeValue);
                return;
            case 3:
                setMessage(attributeValue);
                return;
            case 5:
                try {
                    setDate(new java.text.SimpleDateFormat("yyyyMMdd'T'HHmmSS", java.util.Locale.ENGLISH).parse(attributeValue));
                    return;
                } catch (java.text.ParseException e) {
                    android.util.Log.e(TAG, "Unable to parse date - " + attributeValue);
                    return;
                }
            case 6:
                setPositiveActionLabel(attributeValue);
                return;
            case 7:
                setNegativeActionLabel(attributeValue);
                return;
            default:
                android.util.Log.d(TAG, "Unhandled attribute - id:" + attributeId + " val:" + attributeValue);
                return;
        }
    }

    public java.lang.String toString() {
        java.lang.StringBuilder builder = new java.lang.StringBuilder("Notification{eventId=" + this.eventId + ", categoryId=" + this.categoryId + ", categoryCount=" + this.categoryCount + ", eventFlags=" + this.eventFlags + ", notificationUid=" + this.notificationUid);
        if (this.appId != null) {
            builder.append(", appId='");
            builder.append(this.appId);
            builder.append('\'');
        }
        if (this.appName != null) {
            builder.append(", appName='");
            builder.append(this.appName);
            builder.append('\'');
        }
        if (this.title != null) {
            builder.append(", title='");
            builder.append(this.title);
            builder.append('\'');
        }
        if (this.subTitle != null) {
            builder.append(", subTitle='");
            builder.append(this.subTitle);
            builder.append('\'');
        }
        if (this.message != null) {
            builder.append(", message='");
            builder.append(this.message);
            builder.append('\'');
        }
        if (this.positiveActionLabel != null) {
            builder.append(", positiveAction='");
            builder.append(this.positiveActionLabel);
            builder.append('\'');
        }
        if (this.negativeActionLabel != null) {
            builder.append(", negativeAction='");
            builder.append(this.negativeActionLabel);
            builder.append('\'');
        }
        if (this.date != null) {
            builder.append(", date='");
            builder.append(this.date);
            builder.append('\'');
        }
        builder.append("}");
        return builder.toString();
    }

    public static int byteToInt(byte b) {
        return b & com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient.ORDER_BY_DEFAULT;
    }

    public static int bytesToShort(byte b0, byte b1) {
        return byteToInt(b0) + (byteToInt(b1) << 8);
    }

    public static int bytesToShort(byte[] bytes, int offset) {
        return bytesToShort(bytes[offset], bytes[offset + 1]);
    }

    public static int bytesToInt(byte b0, byte b1, byte b2, byte b3) {
        return byteToInt(b0) + (byteToInt(b1) << 8) + (byteToInt(b2) << 16) + (byteToInt(b3) << 24);
    }

    public static int bytesToInt(byte[] bytes, int offset) {
        return bytesToInt(bytes[offset], bytes[offset + 1], bytes[offset + 2], bytes[offset + 3]);
    }

    public int describeContents() {
        return 0;
    }

    public AppleNotification(android.os.Parcel in) {
        this(in.readInt(), in.readInt(), in.readInt(), in.readInt(), in.readInt());
        setTitle(in.readString());
        setSubTitle(in.readString());
        setMessage(in.readString());
        long dateMs = in.readLong();
        if (dateMs != 0) {
            setDate(new java.util.Date(dateMs));
        }
        setAppId(in.readString());
        setPositiveActionLabel(in.readString());
        setNegativeActionLabel(in.readString());
        setAppName(in.readString());
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeInt(this.eventId);
        dest.writeInt(this.eventFlags);
        dest.writeInt(this.categoryId);
        dest.writeInt(this.categoryCount);
        dest.writeInt(this.notificationUid);
        dest.writeString(this.title);
        dest.writeString(this.subTitle);
        dest.writeString(this.message);
        dest.writeLong(this.date != null ? this.date.getTime() : 0);
        dest.writeString(this.appId);
        dest.writeString(this.positiveActionLabel);
        dest.writeString(this.negativeActionLabel);
        dest.writeString(this.appName);
    }
}
