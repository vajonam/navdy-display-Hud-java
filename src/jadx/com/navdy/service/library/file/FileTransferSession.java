package com.navdy.service.library.file;

public class FileTransferSession {
    public static final int CHUNK_SIZE = 131072;
    public static final int PULL_CHUNK_SIZE = 16384;
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.service.library.file.FileTransferSession.class);
    private long mAlreadyTransferred;
    java.security.MessageDigest mDataCheckSum;
    java.lang.String mDestinationFolder;
    int mExpectedChunk = 0;
    java.io.File mFile;
    java.lang.String mFileName;
    private java.io.FileInputStream mFileReader;
    long mFileSize;
    private com.navdy.service.library.events.file.FileType mFileType;
    private boolean mIsTestTransfer = false;
    private long mLastActivity = 0;
    private boolean mPullRequest = false;
    private byte[] mReadBuffer;
    long mTotalBytesTransferred;
    int mTransferId = -1;
    private boolean mWaitForAcknowledgements;
    long negotiatedOffset = -1;

    public FileTransferSession(int mTransferId2) {
        this.mTransferId = mTransferId2;
    }

    public synchronized long getLastActivity() {
        return this.mLastActivity;
    }

    public synchronized com.navdy.service.library.events.file.FileTransferResponse initPull(android.content.Context context, com.navdy.service.library.events.file.FileType type, java.lang.String destinationFolder, java.lang.String sourceFileName, boolean supportsFlowControl) {
        com.navdy.service.library.events.file.FileTransferResponse build;
        com.navdy.service.library.events.file.FileTransferResponse.Builder builder = new com.navdy.service.library.events.file.FileTransferResponse.Builder();
        this.mWaitForAcknowledgements = supportsFlowControl;
        this.mFileType = type;
        builder.supportsAcks(java.lang.Boolean.valueOf(supportsFlowControl));
        builder.destinationFileName(sourceFileName).fileType(type);
        try {
            synchronized (this) {
                this.mLastActivity = java.lang.System.currentTimeMillis();
                this.mDestinationFolder = destinationFolder;
                this.mFileName = sourceFileName;
                this.mFile = new java.io.File(destinationFolder, this.mFileName);
                this.mPullRequest = true;
                if (!this.mFile.exists() || !this.mFile.canRead() || this.mFile.length() <= 0) {
                    build = builder.success(java.lang.Boolean.valueOf(false)).error(com.navdy.service.library.events.file.FileTransferError.FILE_TRANSFER_IO_ERROR).build();
                } else {
                    this.mFileSize = this.mFile.length();
                    this.mFileReader = new java.io.FileInputStream(this.mFile);
                    build = builder.success(java.lang.Boolean.valueOf(true)).maxChunkSize(java.lang.Integer.valueOf(16384)).transferId(java.lang.Integer.valueOf(this.mTransferId)).offset(java.lang.Long.valueOf(this.mFileSize)).build();
                }
            }
        } catch (Throwable th) {
            build = builder.success(java.lang.Boolean.valueOf(false)).error(com.navdy.service.library.events.file.FileTransferError.FILE_TRANSFER_IO_ERROR).build();
        }
        return build;
    }

    public synchronized com.navdy.service.library.events.file.FileTransferResponse initFileTransfer(android.content.Context context, com.navdy.service.library.events.file.FileType fileType, java.lang.String destinationFolder, java.lang.String destinationFileName, long size, long offsetRequested, java.lang.String checkSum, boolean override) {
        com.navdy.service.library.events.file.FileTransferResponse build;
        long finalOffset;
        synchronized (this) {
            this.mLastActivity = java.lang.System.currentTimeMillis();
        }
        this.mDestinationFolder = destinationFolder;
        this.mFileName = destinationFileName;
        this.mFileSize = size;
        this.mFileType = fileType;
        synchronized (this) {
            this.mExpectedChunk = 0;
        }
        java.io.File destinationDirectory = new java.io.File(destinationFolder);
        java.lang.String responseCheckSum = null;
        com.navdy.service.library.events.file.FileTransferResponse.Builder builder = new com.navdy.service.library.events.file.FileTransferResponse.Builder();
        builder.fileType(fileType).destinationFileName(destinationFileName);
        builder.transferId(java.lang.Integer.valueOf(this.mTransferId));
        try {
            java.io.File file = new java.io.File(destinationDirectory, destinationFileName);
            if (file.exists()) {
                if (!file.canWrite()) {
                    build = builder.error(com.navdy.service.library.events.file.FileTransferError.FILE_TRANSFER_PERMISSION_DENIED).success(java.lang.Boolean.valueOf(false)).build();
                } else {
                    long localOffset = file.length();
                    sLogger.d("Requested offset :" + offsetRequested + " , Already existing file size:" + localOffset + ", override :" + override);
                    if (offsetRequested <= localOffset) {
                        if (override) {
                            finalOffset = offsetRequested;
                        } else {
                            finalOffset = localOffset;
                        }
                        if (finalOffset > 0 && !com.navdy.service.library.util.IOUtils.hashForFile(file, offsetRequested).equals(checkSum)) {
                            sLogger.e("Checksum mismatch, local file does not match remote file");
                            finalOffset = 0;
                        }
                    } else {
                        finalOffset = localOffset;
                    }
                    sLogger.d("New proposed offset :" + finalOffset);
                    if (localOffset - finalOffset > 0) {
                        java.io.FileOutputStream fos = null;
                        java.nio.channels.FileChannel channel = null;
                        try {
                            java.io.FileOutputStream fos2 = new java.io.FileOutputStream(file, true);
                            try {
                                channel = fos2.getChannel();
                                channel.truncate(finalOffset);
                                com.navdy.service.library.util.IOUtils.closeStream(fos2);
                                com.navdy.service.library.util.IOUtils.closeStream(channel);
                                sLogger.e("Truncated the file , previous size :" + localOffset + ", new size :" + file.length());
                            } catch (Throwable th) {
                                th = th;
                                fos = fos2;
                                com.navdy.service.library.util.IOUtils.closeStream(fos);
                                com.navdy.service.library.util.IOUtils.closeStream(channel);
                                throw th;
                            }
                        } catch (Throwable th2) {
                            t = th2;
                            sLogger.e("Exception truncating the file to offset " + finalOffset, t);
                            build = builder.error(com.navdy.service.library.events.file.FileTransferError.FILE_TRANSFER_IO_ERROR).success(java.lang.Boolean.valueOf(false)).build();
                            com.navdy.service.library.util.IOUtils.closeStream(fos);
                            com.navdy.service.library.util.IOUtils.closeStream(channel);
                            return build;
                        }
                    }
                }
                return build;
            }
            sLogger.d("File does not exists, creating new file");
            finalOffset = 0;
            if (!file.createNewFile()) {
                sLogger.d("Permission denied");
                build = builder.error(com.navdy.service.library.events.file.FileTransferError.FILE_TRANSFER_PERMISSION_DENIED).success(java.lang.Boolean.valueOf(false)).build();
                return build;
            }
            this.negotiatedOffset = finalOffset;
            sLogger.d("Final offset proposed " + this.negotiatedOffset);
            long totalSize = size - this.negotiatedOffset;
            if (com.navdy.service.library.util.IOUtils.getFreeSpace(destinationFolder) <= totalSize) {
                sLogger.d("Not enough space to write the file, space required :" + totalSize);
                build = builder.error(com.navdy.service.library.events.file.FileTransferError.FILE_TRANSFER_IO_ERROR).success(java.lang.Boolean.valueOf(false)).build();
            } else {
                if (this.negotiatedOffset == offsetRequested) {
                    responseCheckSum = checkSum;
                } else if (this.negotiatedOffset > 0) {
                    responseCheckSum = com.navdy.service.library.util.IOUtils.hashForFile(file, this.negotiatedOffset);
                }
                builder.maxChunkSize(java.lang.Integer.valueOf(131072)).offset(java.lang.Long.valueOf(this.negotiatedOffset));
                sLogger.d("FileTransferRequest successful");
                builder.success(java.lang.Boolean.valueOf(true));
                if (responseCheckSum != null) {
                    builder.checksum(responseCheckSum);
                }
                build = builder.build();
            }
        } catch (java.lang.Exception e) {
            sLogger.e("Error processing FileTransferRequest ", e);
            build = builder.error(com.navdy.service.library.events.file.FileTransferError.FILE_TRANSFER_IO_ERROR).success(java.lang.Boolean.valueOf(false)).build();
        }
        return build;
    }

    public synchronized com.navdy.service.library.events.file.FileTransferResponse initTestData(android.content.Context context, com.navdy.service.library.events.file.FileType fileType, java.lang.String fileName, long size) {
        com.navdy.service.library.events.file.FileTransferResponse build;
        this.mLastActivity = java.lang.System.currentTimeMillis();
        this.mDestinationFolder = "";
        this.mFileName = fileName;
        this.mFileSize = size;
        this.mFileType = fileType;
        this.mExpectedChunk = 0;
        this.mTotalBytesTransferred = 0;
        this.negotiatedOffset = 0;
        this.mIsTestTransfer = true;
        com.navdy.service.library.events.file.FileTransferResponse.Builder builder = new com.navdy.service.library.events.file.FileTransferResponse.Builder();
        builder.fileType(fileType).destinationFileName(fileName);
        builder.maxChunkSize(java.lang.Integer.valueOf(131072)).offset(java.lang.Long.valueOf(this.negotiatedOffset));
        builder.transferId(java.lang.Integer.valueOf(this.mTransferId));
        try {
            this.mDataCheckSum = java.security.MessageDigest.getInstance("MD5");
            sLogger.d("FileTransferRequest successful");
            builder.success(java.lang.Boolean.valueOf(true));
            build = builder.build();
        } catch (java.security.NoSuchAlgorithmException e) {
            sLogger.e("unable to create MD5 message digest");
            build = builder.error(com.navdy.service.library.events.file.FileTransferError.FILE_TRANSFER_CHECKSUM_ERROR).success(java.lang.Boolean.valueOf(false)).build();
        }
        return build;
    }

    private synchronized com.navdy.service.library.events.file.FileTransferStatus verifyTestDataChunk(android.content.Context context, com.navdy.service.library.events.file.FileTransferData data, com.navdy.service.library.events.file.FileTransferStatus.Builder responseBuilder) {
        com.navdy.service.library.events.file.FileTransferStatus build;
        if (data.chunkIndex.intValue() != this.mExpectedChunk) {
            sLogger.e("Illegal chunk,  Expected :" + this.mExpectedChunk + ", received:" + data.chunkIndex);
            build = responseBuilder.error(com.navdy.service.library.events.file.FileTransferError.FILE_TRANSFER_ILLEGAL_CHUNK).build();
        } else {
            if (data.dataBytes != null && data.dataBytes.size() > 0) {
                this.mTotalBytesTransferred += (long) data.dataBytes.size();
                this.mDataCheckSum.update(data.dataBytes.toByteArray());
            }
            responseBuilder.totalBytesTransferred(java.lang.Long.valueOf(this.mTotalBytesTransferred));
            this.mExpectedChunk++;
            if (data.lastChunk.booleanValue()) {
                if (this.mTotalBytesTransferred != this.mFileSize) {
                    sLogger.e("Not the last chunk, data missing. Did not receive full file");
                    build = responseBuilder.error(com.navdy.service.library.events.file.FileTransferError.FILE_TRANSFER_ILLEGAL_CHUNK).build();
                } else {
                    java.lang.String finalChecksum = com.navdy.service.library.util.IOUtils.bytesToHexString(this.mDataCheckSum.digest());
                    if (finalChecksum.equals(data.fileCheckSum)) {
                        sLogger.d("Received last chunk, Transfer complete, final data size " + this.mFileSize);
                        responseBuilder.transferComplete(java.lang.Boolean.valueOf(true));
                    } else {
                        sLogger.e(java.lang.String.format("Received last chunk but the checksum did not match. expected = \"%s\", actual = \"%s\"", new java.lang.Object[]{data.fileCheckSum, finalChecksum}));
                        build = responseBuilder.error(com.navdy.service.library.events.file.FileTransferError.FILE_TRANSFER_CHECKSUM_ERROR).build();
                    }
                }
            }
            build = responseBuilder.success(java.lang.Boolean.valueOf(true)).build();
        }
        return build;
    }

    public synchronized boolean handleFileTransferStatus(com.navdy.service.library.events.file.FileTransferStatus fileTransferStatus) {
        boolean z;
        if (fileTransferStatus != null) {
            if (fileTransferStatus.success.booleanValue() && fileTransferStatus.chunkIndex.intValue() == this.mExpectedChunk - 1 && fileTransferStatus.totalBytesTransferred.longValue() == this.mAlreadyTransferred) {
                z = true;
            }
        }
        z = false;
        return z;
    }

    /* JADX WARNING: Unknown top exception splitter block from list: {B:46:0x018a=Splitter:B:46:0x018a, B:39:0x014c=Splitter:B:39:0x014c} */
    public synchronized com.navdy.service.library.events.file.FileTransferStatus appendChunk(android.content.Context context, com.navdy.service.library.events.file.FileTransferData data) {
        com.navdy.service.library.events.file.FileTransferStatus build;
        this.mLastActivity = java.lang.System.currentTimeMillis();
        java.io.FileOutputStream outputStream = null;
        com.navdy.service.library.events.file.FileTransferStatus.Builder responseBuilder = new com.navdy.service.library.events.file.FileTransferStatus.Builder().transferId(java.lang.Integer.valueOf(this.mTransferId)).totalBytesTransferred(java.lang.Long.valueOf(0)).transferComplete(java.lang.Boolean.valueOf(false)).success(java.lang.Boolean.valueOf(false)).chunkIndex(data.chunkIndex);
        if (this.mIsTestTransfer) {
            build = verifyTestDataChunk(context, data, responseBuilder);
        } else {
            java.io.File file = new java.io.File(this.mDestinationFolder, this.mFileName);
            if (!file.exists()) {
                sLogger.e("Session not initiated ");
                build = responseBuilder.error(com.navdy.service.library.events.file.FileTransferError.FILE_TRANSFER_NOT_INITIATED).build();
            } else if (data.chunkIndex.intValue() != this.mExpectedChunk) {
                sLogger.e("Illegal chunk,  Expected :" + this.mExpectedChunk + ", received:" + data.chunkIndex);
                build = responseBuilder.error(com.navdy.service.library.events.file.FileTransferError.FILE_TRANSFER_ILLEGAL_CHUNK).build();
            } else {
                try {
                    sLogger.v(java.lang.String.format("Starting to write chunk %d into file %s from folder %s", new java.lang.Object[]{data.chunkIndex, file.getName(), file.getAbsolutePath()}));
                    if (data.dataBytes != null && data.dataBytes.size() > 0) {
                        java.io.FileOutputStream outputStream2 = new java.io.FileOutputStream(file, true);
                        try {
                            outputStream2.write(data.dataBytes.toByteArray());
                            outputStream = outputStream2;
                        } catch (java.io.IOException e) {
                            e = e;
                            outputStream = outputStream2;
                            try {
                                sLogger.e("Error writing the chunk to the file", e);
                                build = responseBuilder.error(com.navdy.service.library.events.file.FileTransferError.FILE_TRANSFER_IO_ERROR).build();
                                com.navdy.service.library.util.IOUtils.fileSync(outputStream);
                                com.navdy.service.library.util.IOUtils.closeStream(outputStream);
                                return build;
                            } catch (Throwable th) {
                                th = th;
                            }
                        } catch (Throwable th2) {
                            th = th2;
                            outputStream = outputStream2;
                            com.navdy.service.library.util.IOUtils.fileSync(outputStream);
                            com.navdy.service.library.util.IOUtils.closeStream(outputStream);
                            throw th;
                        }
                    }
                    responseBuilder.totalBytesTransferred(java.lang.Long.valueOf(file.length()));
                    this.mExpectedChunk++;
                    if (data.lastChunk.booleanValue()) {
                        if (file.length() != this.mFileSize) {
                            sLogger.e("Not the last chunk, data missing. Did not receive full file");
                            build = responseBuilder.error(com.navdy.service.library.events.file.FileTransferError.FILE_TRANSFER_ILLEGAL_CHUNK).build();
                            com.navdy.service.library.util.IOUtils.fileSync(outputStream);
                            com.navdy.service.library.util.IOUtils.closeStream(outputStream);
                        } else {
                            java.lang.String finalChecksumForTheFile = com.navdy.service.library.util.IOUtils.hashForFile(file);
                            if (finalChecksumForTheFile.equals(data.fileCheckSum)) {
                                sLogger.d("Written last chunk to the file , Transfer complete, final file size " + file.length());
                                responseBuilder.transferComplete(java.lang.Boolean.valueOf(true));
                            } else {
                                sLogger.e(java.lang.String.format("Received last chunk but the checksum did not match. expected = \"%s\", actual = \"%s\"", new java.lang.Object[]{data.fileCheckSum, finalChecksumForTheFile}));
                                build = responseBuilder.error(com.navdy.service.library.events.file.FileTransferError.FILE_TRANSFER_CHECKSUM_ERROR).build();
                                com.navdy.service.library.util.IOUtils.fileSync(outputStream);
                                com.navdy.service.library.util.IOUtils.closeStream(outputStream);
                            }
                        }
                    }
                    com.navdy.service.library.util.IOUtils.fileSync(outputStream);
                    com.navdy.service.library.util.IOUtils.closeStream(outputStream);
                    build = responseBuilder.success(java.lang.Boolean.valueOf(true)).build();
                } catch (java.io.IOException e2) {
                    e = e2;
                }
            }
        }
        return build;
    }

    public synchronized com.navdy.service.library.events.file.FileTransferData getNextChunk() throws java.lang.Throwable {
        com.navdy.service.library.events.file.FileTransferData fileTransferData;
        try {
            int chunkId = this.mExpectedChunk;
            this.mExpectedChunk = chunkId + 1;
            long remainingBytes = this.mFileSize - this.mAlreadyTransferred;
            if (remainingBytes < 0) {
                throw new java.io.EOFException("End of file, no more chunk");
            }
            int nextChunkSize = 16384;
            boolean lastChunk = false;
            if (remainingBytes < ((long) 16384)) {
                nextChunkSize = (int) remainingBytes;
                lastChunk = true;
            }
            if (this.mReadBuffer == null || this.mReadBuffer.length != nextChunkSize) {
                this.mReadBuffer = new byte[nextChunkSize];
            }
            this.mAlreadyTransferred += (long) this.mFileReader.read(this.mReadBuffer);
            fileTransferData = new com.navdy.service.library.events.file.FileTransferData.Builder().transferId(java.lang.Integer.valueOf(this.mTransferId)).chunkIndex(java.lang.Integer.valueOf(chunkId)).dataBytes(okio.ByteString.of(this.mReadBuffer)).lastChunk(java.lang.Boolean.valueOf(lastChunk)).build();
        } catch (Throwable t) {
            sLogger.e("Exception while reading the next chunk ", t);
            com.navdy.service.library.util.IOUtils.closeStream(this.mFileReader);
            fileTransferData = null;
        }
        return fileTransferData;
    }

    public synchronized void endSession(android.content.Context context, boolean deleteFile) {
        com.navdy.service.library.util.IOUtils.closeStream(this.mFileReader);
        if (this.mPullRequest && deleteFile) {
            com.navdy.service.library.util.IOUtils.deleteFile(context, this.mFile.getAbsolutePath());
        }
        this.mReadBuffer = null;
    }

    public com.navdy.service.library.events.file.FileType getFileType() {
        return this.mFileType;
    }

    public boolean isFlowControlEnabled() {
        return this.mWaitForAcknowledgements;
    }
}
