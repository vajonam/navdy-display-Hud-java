package com.navdy.service.library.file;

public interface IFileTransferManager {
    com.navdy.service.library.events.file.FileType getFileType(int i);

    com.navdy.service.library.events.file.FileTransferData getNextChunk(int i) throws java.lang.Throwable;

    com.navdy.service.library.events.file.FileTransferStatus handleFileTransferData(com.navdy.service.library.events.file.FileTransferData fileTransferData);

    com.navdy.service.library.events.file.FileTransferResponse handleFileTransferRequest(com.navdy.service.library.events.file.FileTransferRequest fileTransferRequest);

    boolean handleFileTransferStatus(com.navdy.service.library.events.file.FileTransferStatus fileTransferStatus);

    void stop();
}
