package com.navdy.service.library.file;

public interface IFileTransferAuthority {
    java.lang.String getDirectoryForFileType(com.navdy.service.library.events.file.FileType fileType);

    java.lang.String getFileToSend(com.navdy.service.library.events.file.FileType fileType);

    boolean isFileTypeAllowed(com.navdy.service.library.events.file.FileType fileType);

    void onFileSent(com.navdy.service.library.events.file.FileType fileType);
}
