package com.navdy.service.library.util;

public class ScalingUtilities {

    public enum ScalingLogic {
        CROP,
        FIT
    }

    public static android.graphics.Bitmap decodeResource(android.content.res.Resources res, int resId, int dstWidth, int dstHeight, com.navdy.service.library.util.ScalingUtilities.ScalingLogic scalingLogic) {
        android.graphics.BitmapFactory.Options options = new android.graphics.BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        android.graphics.BitmapFactory.decodeResource(res, resId, options);
        options.inJustDecodeBounds = false;
        options.inSampleSize = calculateSampleSize(options.outWidth, options.outHeight, dstWidth, dstHeight, scalingLogic);
        return android.graphics.BitmapFactory.decodeResource(res, resId, options);
    }

    @java.lang.Deprecated
    public static android.graphics.Bitmap createScaledBitmap(android.graphics.Bitmap unscaledBitmap, int dstWidth, int dstHeight, com.navdy.service.library.util.ScalingUtilities.ScalingLogic scalingLogic) {
        if (unscaledBitmap.getHeight() == dstHeight && unscaledBitmap.getWidth() == dstWidth) {
            return unscaledBitmap;
        }
        android.graphics.Rect srcRect = calculateSrcRect(unscaledBitmap.getWidth(), unscaledBitmap.getHeight(), dstWidth, dstHeight, scalingLogic);
        android.graphics.Rect dstRect = calculateDstRect(unscaledBitmap.getWidth(), unscaledBitmap.getHeight(), dstWidth, dstHeight, scalingLogic);
        android.graphics.Bitmap scaledBitmap = android.graphics.Bitmap.createBitmap(dstRect.width(), dstRect.height(), android.graphics.Bitmap.Config.ARGB_8888);
        new android.graphics.Canvas(scaledBitmap).drawBitmap(unscaledBitmap, srcRect, dstRect, new android.graphics.Paint(2));
        return scaledBitmap;
    }

    public static android.graphics.Bitmap createScaledBitmapAndRecycleOriginalIfScaled(android.graphics.Bitmap originalBitmap, int dstWidth, int dstHeight, com.navdy.service.library.util.ScalingUtilities.ScalingLogic scalingLogic) {
        if (originalBitmap.getHeight() == dstHeight && originalBitmap.getWidth() == dstWidth) {
            return originalBitmap;
        }
        android.graphics.Rect srcRect = calculateSrcRect(originalBitmap.getWidth(), originalBitmap.getHeight(), dstWidth, dstHeight, scalingLogic);
        android.graphics.Rect dstRect = calculateDstRect(originalBitmap.getWidth(), originalBitmap.getHeight(), dstWidth, dstHeight, scalingLogic);
        android.graphics.Bitmap scaledBitmap = android.graphics.Bitmap.createBitmap(dstRect.width(), dstRect.height(), android.graphics.Bitmap.Config.ARGB_8888);
        new android.graphics.Canvas(scaledBitmap).drawBitmap(originalBitmap, srcRect, dstRect, new android.graphics.Paint(2));
        originalBitmap.recycle();
        return scaledBitmap;
    }

    public static android.graphics.Bitmap decodeByteArray(byte[] data, int dstWidth, int dstHeight, com.navdy.service.library.util.ScalingUtilities.ScalingLogic scalingLogic) {
        android.graphics.BitmapFactory.Options options = new android.graphics.BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        android.graphics.BitmapFactory.decodeByteArray(data, 0, data.length, options);
        if (options.outWidth == dstWidth && options.outHeight == dstHeight) {
            options = null;
        } else {
            options.inJustDecodeBounds = false;
            options.inSampleSize = calculateSampleSize(options.outWidth, options.outHeight, dstWidth, dstHeight, scalingLogic);
        }
        return android.graphics.BitmapFactory.decodeByteArray(data, 0, data.length, options);
    }

    public static byte[] encodeByteArray(android.graphics.Bitmap bitmap) {
        java.io.ByteArrayOutputStream stream = new java.io.ByteArrayOutputStream();
        bitmap.compress(android.graphics.Bitmap.CompressFormat.PNG, 100, stream);
        return stream.toByteArray();
    }

    public static int calculateSampleSize(int srcWidth, int srcHeight, int dstWidth, int dstHeight, com.navdy.service.library.util.ScalingUtilities.ScalingLogic scalingLogic) {
        if (scalingLogic == com.navdy.service.library.util.ScalingUtilities.ScalingLogic.FIT) {
            if (((float) srcWidth) / ((float) srcHeight) > ((float) dstWidth) / ((float) dstHeight)) {
                return srcWidth / dstWidth;
            }
            return srcHeight / dstHeight;
        } else if (((float) srcWidth) / ((float) srcHeight) > ((float) dstWidth) / ((float) dstHeight)) {
            return srcHeight / dstHeight;
        } else {
            return srcWidth / dstWidth;
        }
    }

    public static android.graphics.Rect calculateSrcRect(int srcWidth, int srcHeight, int dstWidth, int dstHeight, com.navdy.service.library.util.ScalingUtilities.ScalingLogic scalingLogic) {
        if (scalingLogic != com.navdy.service.library.util.ScalingUtilities.ScalingLogic.CROP) {
            return new android.graphics.Rect(0, 0, srcWidth, srcHeight);
        }
        float dstAspect = ((float) dstWidth) / ((float) dstHeight);
        if (((float) srcWidth) / ((float) srcHeight) > dstAspect) {
            int srcRectWidth = (int) (((float) srcHeight) * dstAspect);
            int srcRectLeft = (srcWidth - srcRectWidth) / 2;
            return new android.graphics.Rect(srcRectLeft, 0, srcRectLeft + srcRectWidth, srcHeight);
        }
        int srcRectHeight = (int) (((float) srcWidth) / dstAspect);
        int scrRectTop = (srcHeight - srcRectHeight) / 2;
        return new android.graphics.Rect(0, scrRectTop, srcWidth, scrRectTop + srcRectHeight);
    }

    public static android.graphics.Rect calculateDstRect(int srcWidth, int srcHeight, int dstWidth, int dstHeight, com.navdy.service.library.util.ScalingUtilities.ScalingLogic scalingLogic) {
        if (scalingLogic != com.navdy.service.library.util.ScalingUtilities.ScalingLogic.FIT) {
            return new android.graphics.Rect(0, 0, dstWidth, dstHeight);
        }
        float srcAspect = ((float) srcWidth) / ((float) srcHeight);
        if (srcAspect > ((float) dstWidth) / ((float) dstHeight)) {
            return new android.graphics.Rect(0, 0, dstWidth, (int) (((float) dstWidth) / srcAspect));
        }
        return new android.graphics.Rect(0, 0, (int) (((float) dstHeight) * srcAspect), dstHeight);
    }
}
