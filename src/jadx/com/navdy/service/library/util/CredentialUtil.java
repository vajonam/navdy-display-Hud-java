package com.navdy.service.library.util;

public class CredentialUtil {
    public static final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.service.library.util.CredentialUtil.class);

    public static java.lang.String getCredentials(android.content.Context context, java.lang.String metaName) {
        java.lang.String credentials = "";
        try {
            return context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData.getString(metaName);
        } catch (android.content.pm.PackageManager.NameNotFoundException e) {
            logger.e("Failed to load meta-data, NameNotFound: " + e.getMessage());
            return credentials;
        } catch (java.lang.NullPointerException e2) {
            logger.e("Failed to load meta-data, NullPointer: " + e2.getMessage());
            return credentials;
        }
    }
}
