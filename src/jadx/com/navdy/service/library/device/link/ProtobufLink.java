package com.navdy.service.library.device.link;

public class ProtobufLink implements com.navdy.service.library.device.link.Link {
    private int bandwidthLevel = 1;
    private com.navdy.service.library.device.connection.ConnectionInfo connectionInfo;
    private com.navdy.service.library.device.link.ReaderThread readerThread;
    private com.navdy.service.library.device.link.WriterThread writerThread;

    public ProtobufLink(com.navdy.service.library.device.connection.ConnectionInfo connectionInfo2) {
        this.connectionInfo = connectionInfo2;
    }

    public boolean start(com.navdy.service.library.network.SocketAdapter socket, java.util.concurrent.LinkedBlockingDeque<com.navdy.service.library.device.link.EventRequest> outboundEventQueue, com.navdy.service.library.device.link.LinkListener listener) {
        if ((this.writerThread == null || this.writerThread.isClosing()) && (this.readerThread == null || this.readerThread.isClosing())) {
            java.io.InputStream inputStream = null;
            try {
                inputStream = socket.getInputStream();
                java.io.OutputStream outputStream = socket.getOutputStream();
                this.readerThread = new com.navdy.service.library.device.link.ReaderThread(this.connectionInfo.getType(), inputStream, listener, true);
                this.writerThread = new com.navdy.service.library.device.link.WriterThread(outboundEventQueue, outputStream, null);
                this.readerThread.start();
                this.writerThread.start();
                return true;
            } catch (java.io.IOException e) {
                com.navdy.service.library.util.IOUtils.closeStream(inputStream);
                this.readerThread = null;
                this.writerThread = null;
                return false;
            }
        } else {
            throw new java.lang.IllegalStateException("Must stop threads before calling startLink");
        }
    }

    public void setBandwidthLevel(int level) {
        this.bandwidthLevel = level;
    }

    public int getBandWidthLevel() {
        return this.bandwidthLevel;
    }

    public void close() {
        if (this.writerThread != null) {
            this.writerThread.cancel();
            this.writerThread = null;
        }
        if (this.readerThread != null) {
            this.readerThread.cancel();
            this.readerThread = null;
        }
    }
}
