package com.navdy.service.library.device.link;

public interface LinkListener {
    void linkEstablished(com.navdy.service.library.device.connection.ConnectionType connectionType);

    void linkLost(com.navdy.service.library.device.connection.ConnectionType connectionType, com.navdy.service.library.device.connection.Connection.DisconnectCause disconnectCause);

    void onNavdyEventReceived(byte[] bArr);

    void onNetworkLinkReady();
}
