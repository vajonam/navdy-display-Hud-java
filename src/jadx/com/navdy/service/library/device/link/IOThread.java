package com.navdy.service.library.device.link;

public class IOThread extends java.lang.Thread {
    private static final int SHUTDOWN_TIMEOUT = 1000;
    protected volatile boolean closing = false;
    protected final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(getClass());

    public void cancel() {
        this.closing = true;
        interrupt();
        try {
            join(1000);
        } catch (java.lang.InterruptedException e) {
            this.logger.e("Interrupted");
        }
        if (isAlive()) {
            this.logger.w("Thread still alive after join");
        }
    }

    public boolean isClosing() {
        return this.closing;
    }
}
