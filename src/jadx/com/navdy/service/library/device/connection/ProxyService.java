package com.navdy.service.library.device.connection;

public abstract class ProxyService extends java.lang.Thread {
    public abstract void cancel();
}
