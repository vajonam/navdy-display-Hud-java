package com.navdy.service.library.device.connection;

public class BTConnectionInfo extends com.navdy.service.library.device.connection.ConnectionInfo {
    @com.google.gson.annotations.SerializedName("address")
    com.navdy.service.library.device.connection.ServiceAddress mAddress;

    public BTConnectionInfo(com.navdy.service.library.device.NavdyDeviceId id, com.navdy.service.library.device.connection.ServiceAddress address, com.navdy.service.library.device.connection.ConnectionType connectionType) {
        super(id, connectionType);
        this.mAddress = address;
    }

    public BTConnectionInfo(com.navdy.service.library.device.NavdyDeviceId id, java.util.UUID serviceUUID, com.navdy.service.library.device.connection.ConnectionType connectionType) {
        super(id, connectionType);
        if (id.getBluetoothAddress() != null) {
            this.mAddress = new com.navdy.service.library.device.connection.ServiceAddress(id.getBluetoothAddress(), serviceUUID.toString());
            return;
        }
        throw new java.lang.IllegalArgumentException("BT address missing");
    }

    public com.navdy.service.library.device.connection.ServiceAddress getAddress() {
        return this.mAddress;
    }

    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass() || !super.equals(o)) {
            return false;
        }
        return this.mAddress.equals(((com.navdy.service.library.device.connection.BTConnectionInfo) o).mAddress);
    }

    public int hashCode() {
        return (super.hashCode() * 31) + this.mAddress.hashCode();
    }
}
