package com.navdy.service.library.device.connection;

public enum ConnectionType {
    HTTP("_http._tcp."),
    TCP_PROTOBUF(com.navdy.service.library.device.connection.TCPConnectionInfo.SERVICE_TYPE),
    BT_PROTOBUF,
    EA_PROTOBUF,
    BT_TUNNEL,
    EA_TUNNEL,
    BT_IAP2_LINK;
    
    private final java.lang.String mServiceType;

    private ConnectionType(java.lang.String serviceType) {
        this.mServiceType = serviceType;
    }

    public java.lang.String getServiceType() {
        return this.mServiceType;
    }

    public static com.navdy.service.library.device.connection.ConnectionType fromServiceType(java.lang.String serviceType) {
        com.navdy.service.library.device.connection.ConnectionType[] values;
        for (com.navdy.service.library.device.connection.ConnectionType type : values()) {
            if (serviceType.equals(type.getServiceType())) {
                return type;
            }
        }
        return null;
    }

    public static java.util.Set<java.lang.String> getServiceTypes() {
        com.navdy.service.library.device.connection.ConnectionType[] values;
        java.util.HashSet<java.lang.String> names = new java.util.HashSet<>(values().length);
        for (com.navdy.service.library.device.connection.ConnectionType type : values()) {
            if (type.getServiceType() != null) {
                names.add(type.getServiceType());
            }
        }
        return names;
    }
}
