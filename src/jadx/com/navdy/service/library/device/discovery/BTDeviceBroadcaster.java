package com.navdy.service.library.device.discovery;

public class BTDeviceBroadcaster implements com.navdy.service.library.device.discovery.RemoteDeviceBroadcaster {
    public static final int DISCOVERY_TIMEOUT = 300;
    public static final java.lang.String DISPLAY_NAME = " Navdy Display";
    public static final java.util.regex.Pattern DISPLAY_PATTERN = java.util.regex.Pattern.compile("navdy|hud");
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.service.library.device.discovery.BTDeviceBroadcaster.class);
    private android.bluetooth.BluetoothAdapter adapter = android.bluetooth.BluetoothAdapter.getDefaultAdapter();
    private boolean started;

    public static boolean isDisplay(java.lang.String bluetoothName) {
        return DISPLAY_PATTERN.matcher(bluetoothName.toLowerCase(java.util.Locale.US)).find();
    }

    public boolean start() {
        if (!this.started) {
            this.started = true;
            setScanMode(23, 300);
        }
        return this.started;
    }

    public boolean stop() {
        if (this.started) {
            this.started = false;
            setScanMode(20, 300);
        }
        return true;
    }

    private void setScanMode(int mode, int timeout) {
        try {
            java.lang.Class.forName(this.adapter.getClass().getName()).getDeclaredMethod("setScanMode", new java.lang.Class[]{java.lang.Integer.TYPE, java.lang.Integer.TYPE}).invoke(this.adapter, new java.lang.Object[]{java.lang.Integer.valueOf(mode), java.lang.Integer.valueOf(timeout)});
        } catch (java.lang.Exception ex) {
            sLogger.d("enableDiscovery failed - " + ex.toString(), ex);
        }
    }

    private void ensureDisplayLike() {
        java.lang.String name = this.adapter.getName();
        if (!isDisplay(name)) {
            this.adapter.setName(name + DISPLAY_NAME);
        }
    }
}
