package com.navdy.service.library.network;

public interface SocketAdapter extends java.io.Closeable {
    void close() throws java.io.IOException;

    void connect() throws java.io.IOException;

    java.io.InputStream getInputStream() throws java.io.IOException;

    java.io.OutputStream getOutputStream() throws java.io.IOException;

    com.navdy.service.library.device.NavdyDeviceId getRemoteDevice();

    boolean isConnected();
}
