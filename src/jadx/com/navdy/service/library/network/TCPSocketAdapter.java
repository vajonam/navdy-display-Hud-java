package com.navdy.service.library.network;

public class TCPSocketAdapter implements com.navdy.service.library.network.SocketAdapter {
    private final java.lang.String host;
    private final int port;
    private java.net.Socket socket;

    public TCPSocketAdapter(java.lang.String host2, int port2) {
        if (host2 == null) {
            throw new java.lang.IllegalArgumentException("Host must not be null");
        }
        this.host = host2;
        this.port = port2;
    }

    public TCPSocketAdapter(java.net.Socket socket2) {
        this.host = null;
        this.port = -1;
        this.socket = socket2;
    }

    public void connect() throws java.io.IOException {
        if (this.host == null) {
            throw new java.lang.IllegalStateException("Can't connect with accepted socket");
        }
        this.socket = new java.net.Socket(this.host, this.port);
    }

    public void close() throws java.io.IOException {
        if (this.socket != null) {
            this.socket.close();
        }
        this.socket = null;
    }

    public java.io.InputStream getInputStream() throws java.io.IOException {
        return this.socket.getInputStream();
    }

    public java.io.OutputStream getOutputStream() throws java.io.IOException {
        return this.socket.getOutputStream();
    }

    public boolean isConnected() {
        if (this.socket != null) {
            return this.socket.isConnected();
        }
        return false;
    }

    public com.navdy.service.library.device.NavdyDeviceId getRemoteDevice() {
        if (isConnected()) {
            return com.navdy.service.library.device.NavdyDeviceId.UNKNOWN_ID;
        }
        return null;
    }

    public java.lang.String getRemoteAddress() {
        if (isConnected()) {
            return this.socket.getInetAddress().getHostAddress();
        }
        return null;
    }
}
