package com.navdy.service.library.network.http;

public class HttpUtils {
    public static final okhttp3.MediaType JSON = okhttp3.MediaType.parse("application/json; charset=utf-8");
    public static final java.lang.String MIME_TYPE_IMAGE_PNG = "image/png";
    public static final java.lang.String MIME_TYPE_TEXT = "text/plain";
    public static final java.lang.String MIME_TYPE_ZIP = "application/zip";

    public static okhttp3.RequestBody getJsonRequestBody(java.lang.String jsonData) {
        return okhttp3.RequestBody.create(JSON, jsonData);
    }
}
