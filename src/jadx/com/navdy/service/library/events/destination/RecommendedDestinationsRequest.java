package com.navdy.service.library.events.destination;

public final class RecommendedDestinationsRequest extends com.squareup.wire.Message {
    public static final java.lang.Long DEFAULT_SERIAL_NUMBER = java.lang.Long.valueOf(0);
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(tag = 1, type = com.squareup.wire.Message.Datatype.INT64)
    public final java.lang.Long serial_number;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.destination.RecommendedDestinationsRequest> {
        public java.lang.Long serial_number;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.destination.RecommendedDestinationsRequest message) {
            super(message);
            if (message != null) {
                this.serial_number = message.serial_number;
            }
        }

        public com.navdy.service.library.events.destination.RecommendedDestinationsRequest.Builder serial_number(java.lang.Long serial_number2) {
            this.serial_number = serial_number2;
            return this;
        }

        public com.navdy.service.library.events.destination.RecommendedDestinationsRequest build() {
            return new com.navdy.service.library.events.destination.RecommendedDestinationsRequest(this);
        }
    }

    public RecommendedDestinationsRequest(java.lang.Long serial_number2) {
        this.serial_number = serial_number2;
    }

    private RecommendedDestinationsRequest(com.navdy.service.library.events.destination.RecommendedDestinationsRequest.Builder builder) {
        this(builder.serial_number);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.destination.RecommendedDestinationsRequest)) {
            return false;
        }
        return equals((java.lang.Object) this.serial_number, (java.lang.Object) ((com.navdy.service.library.events.destination.RecommendedDestinationsRequest) other).serial_number);
    }

    public int hashCode() {
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int i = this.serial_number != null ? this.serial_number.hashCode() : 0;
        this.hashCode = i;
        return i;
    }
}
