package com.navdy.service.library.events.dial;

public final class DialBondResponse extends com.squareup.wire.Message {
    public static final java.lang.String DEFAULT_MACADDRESS = "";
    public static final java.lang.String DEFAULT_NAME = "";
    public static final com.navdy.service.library.events.dial.DialError DEFAULT_STATUS = com.navdy.service.library.events.dial.DialError.DIAL_ERROR;
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(tag = 3, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String macAddress;
    @com.squareup.wire.ProtoField(tag = 2, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String name;
    @com.squareup.wire.ProtoField(tag = 1, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.dial.DialError status;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.dial.DialBondResponse> {
        public java.lang.String macAddress;
        public java.lang.String name;
        public com.navdy.service.library.events.dial.DialError status;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.dial.DialBondResponse message) {
            super(message);
            if (message != null) {
                this.status = message.status;
                this.name = message.name;
                this.macAddress = message.macAddress;
            }
        }

        public com.navdy.service.library.events.dial.DialBondResponse.Builder status(com.navdy.service.library.events.dial.DialError status2) {
            this.status = status2;
            return this;
        }

        public com.navdy.service.library.events.dial.DialBondResponse.Builder name(java.lang.String name2) {
            this.name = name2;
            return this;
        }

        public com.navdy.service.library.events.dial.DialBondResponse.Builder macAddress(java.lang.String macAddress2) {
            this.macAddress = macAddress2;
            return this;
        }

        public com.navdy.service.library.events.dial.DialBondResponse build() {
            return new com.navdy.service.library.events.dial.DialBondResponse(this);
        }
    }

    public DialBondResponse(com.navdy.service.library.events.dial.DialError status2, java.lang.String name2, java.lang.String macAddress2) {
        this.status = status2;
        this.name = name2;
        this.macAddress = macAddress2;
    }

    private DialBondResponse(com.navdy.service.library.events.dial.DialBondResponse.Builder builder) {
        this(builder.status, builder.name, builder.macAddress);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.dial.DialBondResponse)) {
            return false;
        }
        com.navdy.service.library.events.dial.DialBondResponse o = (com.navdy.service.library.events.dial.DialBondResponse) other;
        if (!equals((java.lang.Object) this.status, (java.lang.Object) o.status) || !equals((java.lang.Object) this.name, (java.lang.Object) o.name) || !equals((java.lang.Object) this.macAddress, (java.lang.Object) o.macAddress)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2 = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = (this.status != null ? this.status.hashCode() : 0) * 37;
        if (this.name != null) {
            i = this.name.hashCode();
        } else {
            i = 0;
        }
        int i3 = (hashCode + i) * 37;
        if (this.macAddress != null) {
            i2 = this.macAddress.hashCode();
        }
        int result2 = i3 + i2;
        this.hashCode = result2;
        return result2;
    }
}
