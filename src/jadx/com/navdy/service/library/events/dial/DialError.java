package com.navdy.service.library.events.dial;

public enum DialError implements com.squareup.wire.ProtoEnum {
    DIAL_ERROR(1),
    DIAL_ALREADY_PAIRED(2),
    DIAL_OPERATION_IN_PROGRESS(3),
    DIAL_PAIRED(4),
    DIAL_NOT_FOUND(5),
    DIAL_NOT_PAIRED(6);
    
    private final int value;

    private DialError(int value2) {
        this.value = value2;
    }

    public int getValue() {
        return this.value;
    }
}
