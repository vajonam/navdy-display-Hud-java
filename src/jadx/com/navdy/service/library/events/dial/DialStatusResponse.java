package com.navdy.service.library.events.dial;

public final class DialStatusResponse extends com.squareup.wire.Message {
    public static final java.lang.Integer DEFAULT_BATTERYLEVEL = java.lang.Integer.valueOf(0);
    public static final java.lang.Boolean DEFAULT_ISCONNECTED = java.lang.Boolean.valueOf(false);
    public static final java.lang.Boolean DEFAULT_ISPAIRED = java.lang.Boolean.valueOf(false);
    public static final java.lang.String DEFAULT_MACADDRESS = "";
    public static final java.lang.String DEFAULT_NAME = "";
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(tag = 5, type = com.squareup.wire.Message.Datatype.INT32)
    public final java.lang.Integer batteryLevel;
    @com.squareup.wire.ProtoField(tag = 2, type = com.squareup.wire.Message.Datatype.BOOL)
    public final java.lang.Boolean isConnected;
    @com.squareup.wire.ProtoField(tag = 1, type = com.squareup.wire.Message.Datatype.BOOL)
    public final java.lang.Boolean isPaired;
    @com.squareup.wire.ProtoField(tag = 4, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String macAddress;
    @com.squareup.wire.ProtoField(tag = 3, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String name;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.dial.DialStatusResponse> {
        public java.lang.Integer batteryLevel;
        public java.lang.Boolean isConnected;
        public java.lang.Boolean isPaired;
        public java.lang.String macAddress;
        public java.lang.String name;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.dial.DialStatusResponse message) {
            super(message);
            if (message != null) {
                this.isPaired = message.isPaired;
                this.isConnected = message.isConnected;
                this.name = message.name;
                this.macAddress = message.macAddress;
                this.batteryLevel = message.batteryLevel;
            }
        }

        public com.navdy.service.library.events.dial.DialStatusResponse.Builder isPaired(java.lang.Boolean isPaired2) {
            this.isPaired = isPaired2;
            return this;
        }

        public com.navdy.service.library.events.dial.DialStatusResponse.Builder isConnected(java.lang.Boolean isConnected2) {
            this.isConnected = isConnected2;
            return this;
        }

        public com.navdy.service.library.events.dial.DialStatusResponse.Builder name(java.lang.String name2) {
            this.name = name2;
            return this;
        }

        public com.navdy.service.library.events.dial.DialStatusResponse.Builder macAddress(java.lang.String macAddress2) {
            this.macAddress = macAddress2;
            return this;
        }

        public com.navdy.service.library.events.dial.DialStatusResponse.Builder batteryLevel(java.lang.Integer batteryLevel2) {
            this.batteryLevel = batteryLevel2;
            return this;
        }

        public com.navdy.service.library.events.dial.DialStatusResponse build() {
            return new com.navdy.service.library.events.dial.DialStatusResponse(this);
        }
    }

    public DialStatusResponse(java.lang.Boolean isPaired2, java.lang.Boolean isConnected2, java.lang.String name2, java.lang.String macAddress2, java.lang.Integer batteryLevel2) {
        this.isPaired = isPaired2;
        this.isConnected = isConnected2;
        this.name = name2;
        this.macAddress = macAddress2;
        this.batteryLevel = batteryLevel2;
    }

    private DialStatusResponse(com.navdy.service.library.events.dial.DialStatusResponse.Builder builder) {
        this(builder.isPaired, builder.isConnected, builder.name, builder.macAddress, builder.batteryLevel);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.dial.DialStatusResponse)) {
            return false;
        }
        com.navdy.service.library.events.dial.DialStatusResponse o = (com.navdy.service.library.events.dial.DialStatusResponse) other;
        if (!equals((java.lang.Object) this.isPaired, (java.lang.Object) o.isPaired) || !equals((java.lang.Object) this.isConnected, (java.lang.Object) o.isConnected) || !equals((java.lang.Object) this.name, (java.lang.Object) o.name) || !equals((java.lang.Object) this.macAddress, (java.lang.Object) o.macAddress) || !equals((java.lang.Object) this.batteryLevel, (java.lang.Object) o.batteryLevel)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2;
        int i3;
        int i4 = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = (this.isPaired != null ? this.isPaired.hashCode() : 0) * 37;
        if (this.isConnected != null) {
            i = this.isConnected.hashCode();
        } else {
            i = 0;
        }
        int i5 = (hashCode + i) * 37;
        if (this.name != null) {
            i2 = this.name.hashCode();
        } else {
            i2 = 0;
        }
        int i6 = (i5 + i2) * 37;
        if (this.macAddress != null) {
            i3 = this.macAddress.hashCode();
        } else {
            i3 = 0;
        }
        int i7 = (i6 + i3) * 37;
        if (this.batteryLevel != null) {
            i4 = this.batteryLevel.hashCode();
        }
        int result2 = i7 + i4;
        this.hashCode = result2;
        return result2;
    }
}
