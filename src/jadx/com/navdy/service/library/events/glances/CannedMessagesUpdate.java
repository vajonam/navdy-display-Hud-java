package com.navdy.service.library.events.glances;

public final class CannedMessagesUpdate extends com.squareup.wire.Message {
    public static final java.util.List<java.lang.String> DEFAULT_CANNEDMESSAGE = java.util.Collections.emptyList();
    public static final java.lang.Long DEFAULT_SERIAL_NUMBER = java.lang.Long.valueOf(0);
    public static final com.navdy.service.library.events.RequestStatus DEFAULT_STATUS = com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS;
    public static final java.lang.String DEFAULT_STATUSDETAIL = "";
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REPEATED, tag = 4, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.util.List<java.lang.String> cannedMessage;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 3, type = com.squareup.wire.Message.Datatype.INT64)
    public final java.lang.Long serial_number;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 1, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.RequestStatus status;
    @com.squareup.wire.ProtoField(tag = 2, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String statusDetail;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.glances.CannedMessagesUpdate> {
        public java.util.List<java.lang.String> cannedMessage;
        public java.lang.Long serial_number;
        public com.navdy.service.library.events.RequestStatus status;
        public java.lang.String statusDetail;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.glances.CannedMessagesUpdate message) {
            super(message);
            if (message != null) {
                this.status = message.status;
                this.statusDetail = message.statusDetail;
                this.serial_number = message.serial_number;
                this.cannedMessage = com.navdy.service.library.events.glances.CannedMessagesUpdate.copyOf(message.cannedMessage);
            }
        }

        public com.navdy.service.library.events.glances.CannedMessagesUpdate.Builder status(com.navdy.service.library.events.RequestStatus status2) {
            this.status = status2;
            return this;
        }

        public com.navdy.service.library.events.glances.CannedMessagesUpdate.Builder statusDetail(java.lang.String statusDetail2) {
            this.statusDetail = statusDetail2;
            return this;
        }

        public com.navdy.service.library.events.glances.CannedMessagesUpdate.Builder serial_number(java.lang.Long serial_number2) {
            this.serial_number = serial_number2;
            return this;
        }

        public com.navdy.service.library.events.glances.CannedMessagesUpdate.Builder cannedMessage(java.util.List<java.lang.String> cannedMessage2) {
            this.cannedMessage = checkForNulls(cannedMessage2);
            return this;
        }

        public com.navdy.service.library.events.glances.CannedMessagesUpdate build() {
            checkRequiredFields();
            return new com.navdy.service.library.events.glances.CannedMessagesUpdate(this);
        }
    }

    public CannedMessagesUpdate(com.navdy.service.library.events.RequestStatus status2, java.lang.String statusDetail2, java.lang.Long serial_number2, java.util.List<java.lang.String> cannedMessage2) {
        this.status = status2;
        this.statusDetail = statusDetail2;
        this.serial_number = serial_number2;
        this.cannedMessage = immutableCopyOf(cannedMessage2);
    }

    private CannedMessagesUpdate(com.navdy.service.library.events.glances.CannedMessagesUpdate.Builder builder) {
        this(builder.status, builder.statusDetail, builder.serial_number, builder.cannedMessage);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.glances.CannedMessagesUpdate)) {
            return false;
        }
        com.navdy.service.library.events.glances.CannedMessagesUpdate o = (com.navdy.service.library.events.glances.CannedMessagesUpdate) other;
        if (!equals((java.lang.Object) this.status, (java.lang.Object) o.status) || !equals((java.lang.Object) this.statusDetail, (java.lang.Object) o.statusDetail) || !equals((java.lang.Object) this.serial_number, (java.lang.Object) o.serial_number) || !equals(this.cannedMessage, o.cannedMessage)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2 = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = (this.status != null ? this.status.hashCode() : 0) * 37;
        if (this.statusDetail != null) {
            i = this.statusDetail.hashCode();
        } else {
            i = 0;
        }
        int i3 = (hashCode + i) * 37;
        if (this.serial_number != null) {
            i2 = this.serial_number.hashCode();
        }
        int result2 = ((i3 + i2) * 37) + (this.cannedMessage != null ? this.cannedMessage.hashCode() : 1);
        this.hashCode = result2;
        return result2;
    }
}
