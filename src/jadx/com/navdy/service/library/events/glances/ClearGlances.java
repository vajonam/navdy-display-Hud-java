package com.navdy.service.library.events.glances;

public final class ClearGlances extends com.squareup.wire.Message {
    private static final long serialVersionUID = 0;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.glances.ClearGlances> {
        public Builder() {
        }

        public Builder(com.navdy.service.library.events.glances.ClearGlances message) {
            super(message);
        }

        public com.navdy.service.library.events.glances.ClearGlances build() {
            return new com.navdy.service.library.events.glances.ClearGlances(this);
        }
    }

    public ClearGlances() {
    }

    private ClearGlances(com.navdy.service.library.events.glances.ClearGlances.Builder builder) {
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        return other instanceof com.navdy.service.library.events.glances.ClearGlances;
    }

    public int hashCode() {
        return 0;
    }
}
