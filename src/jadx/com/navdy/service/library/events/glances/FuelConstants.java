package com.navdy.service.library.events.glances;

public enum FuelConstants implements com.squareup.wire.ProtoEnum {
    FUEL_LEVEL(0),
    NO_ROUTE(1),
    GAS_STATION_NAME(2),
    GAS_STATION_ADDRESS(3),
    GAS_STATION_DISTANCE(4);
    
    private final int value;

    private FuelConstants(int value2) {
        this.value = value2;
    }

    public int getValue() {
        return this.value;
    }
}
