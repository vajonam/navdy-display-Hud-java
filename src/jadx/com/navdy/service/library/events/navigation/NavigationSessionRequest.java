package com.navdy.service.library.events.navigation;

public final class NavigationSessionRequest extends com.squareup.wire.Message {
    public static final java.lang.String DEFAULT_LABEL = "";
    public static final com.navdy.service.library.events.navigation.NavigationSessionState DEFAULT_NEWSTATE = com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_ENGINE_NOT_READY;
    public static final java.lang.Boolean DEFAULT_ORIGINDISPLAY = java.lang.Boolean.valueOf(false);
    public static final java.lang.String DEFAULT_ROUTEID = "";
    public static final java.lang.Integer DEFAULT_SIMULATIONSPEED = java.lang.Integer.valueOf(0);
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(tag = 2, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String label;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 1, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.navigation.NavigationSessionState newState;
    @com.squareup.wire.ProtoField(tag = 5, type = com.squareup.wire.Message.Datatype.BOOL)
    public final java.lang.Boolean originDisplay;
    @com.squareup.wire.ProtoField(tag = 3, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String routeId;
    @com.squareup.wire.ProtoField(tag = 4, type = com.squareup.wire.Message.Datatype.UINT32)
    public final java.lang.Integer simulationSpeed;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.navigation.NavigationSessionRequest> {
        public java.lang.String label;
        public com.navdy.service.library.events.navigation.NavigationSessionState newState;
        public java.lang.Boolean originDisplay;
        public java.lang.String routeId;
        public java.lang.Integer simulationSpeed;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.navigation.NavigationSessionRequest message) {
            super(message);
            if (message != null) {
                this.newState = message.newState;
                this.label = message.label;
                this.routeId = message.routeId;
                this.simulationSpeed = message.simulationSpeed;
                this.originDisplay = message.originDisplay;
            }
        }

        public com.navdy.service.library.events.navigation.NavigationSessionRequest.Builder newState(com.navdy.service.library.events.navigation.NavigationSessionState newState2) {
            this.newState = newState2;
            return this;
        }

        public com.navdy.service.library.events.navigation.NavigationSessionRequest.Builder label(java.lang.String label2) {
            this.label = label2;
            return this;
        }

        public com.navdy.service.library.events.navigation.NavigationSessionRequest.Builder routeId(java.lang.String routeId2) {
            this.routeId = routeId2;
            return this;
        }

        public com.navdy.service.library.events.navigation.NavigationSessionRequest.Builder simulationSpeed(java.lang.Integer simulationSpeed2) {
            this.simulationSpeed = simulationSpeed2;
            return this;
        }

        public com.navdy.service.library.events.navigation.NavigationSessionRequest.Builder originDisplay(java.lang.Boolean originDisplay2) {
            this.originDisplay = originDisplay2;
            return this;
        }

        public com.navdy.service.library.events.navigation.NavigationSessionRequest build() {
            checkRequiredFields();
            return new com.navdy.service.library.events.navigation.NavigationSessionRequest(this);
        }
    }

    public NavigationSessionRequest(com.navdy.service.library.events.navigation.NavigationSessionState newState2, java.lang.String label2, java.lang.String routeId2, java.lang.Integer simulationSpeed2, java.lang.Boolean originDisplay2) {
        this.newState = newState2;
        this.label = label2;
        this.routeId = routeId2;
        this.simulationSpeed = simulationSpeed2;
        this.originDisplay = originDisplay2;
    }

    private NavigationSessionRequest(com.navdy.service.library.events.navigation.NavigationSessionRequest.Builder builder) {
        this(builder.newState, builder.label, builder.routeId, builder.simulationSpeed, builder.originDisplay);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.navigation.NavigationSessionRequest)) {
            return false;
        }
        com.navdy.service.library.events.navigation.NavigationSessionRequest o = (com.navdy.service.library.events.navigation.NavigationSessionRequest) other;
        if (!equals((java.lang.Object) this.newState, (java.lang.Object) o.newState) || !equals((java.lang.Object) this.label, (java.lang.Object) o.label) || !equals((java.lang.Object) this.routeId, (java.lang.Object) o.routeId) || !equals((java.lang.Object) this.simulationSpeed, (java.lang.Object) o.simulationSpeed) || !equals((java.lang.Object) this.originDisplay, (java.lang.Object) o.originDisplay)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2;
        int i3;
        int i4 = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = (this.newState != null ? this.newState.hashCode() : 0) * 37;
        if (this.label != null) {
            i = this.label.hashCode();
        } else {
            i = 0;
        }
        int i5 = (hashCode + i) * 37;
        if (this.routeId != null) {
            i2 = this.routeId.hashCode();
        } else {
            i2 = 0;
        }
        int i6 = (i5 + i2) * 37;
        if (this.simulationSpeed != null) {
            i3 = this.simulationSpeed.hashCode();
        } else {
            i3 = 0;
        }
        int i7 = (i6 + i3) * 37;
        if (this.originDisplay != null) {
            i4 = this.originDisplay.hashCode();
        }
        int result2 = i7 + i4;
        this.hashCode = result2;
        return result2;
    }
}
