package com.navdy.service.library.events.navigation;

public final class RouteManeuverResponse extends com.squareup.wire.Message {
    public static final java.util.List<com.navdy.service.library.events.navigation.RouteManeuver> DEFAULT_MANEUVERS = java.util.Collections.emptyList();
    public static final com.navdy.service.library.events.RequestStatus DEFAULT_STATUS = com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS;
    public static final java.lang.String DEFAULT_STATUSDETAIL = "";
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REPEATED, messageType = com.navdy.service.library.events.navigation.RouteManeuver.class, tag = 3)
    public final java.util.List<com.navdy.service.library.events.navigation.RouteManeuver> maneuvers;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 1, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.RequestStatus status;
    @com.squareup.wire.ProtoField(tag = 2, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String statusDetail;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.navigation.RouteManeuverResponse> {
        public java.util.List<com.navdy.service.library.events.navigation.RouteManeuver> maneuvers;
        public com.navdy.service.library.events.RequestStatus status;
        public java.lang.String statusDetail;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.navigation.RouteManeuverResponse message) {
            super(message);
            if (message != null) {
                this.status = message.status;
                this.statusDetail = message.statusDetail;
                this.maneuvers = com.navdy.service.library.events.navigation.RouteManeuverResponse.copyOf(message.maneuvers);
            }
        }

        public com.navdy.service.library.events.navigation.RouteManeuverResponse.Builder status(com.navdy.service.library.events.RequestStatus status2) {
            this.status = status2;
            return this;
        }

        public com.navdy.service.library.events.navigation.RouteManeuverResponse.Builder statusDetail(java.lang.String statusDetail2) {
            this.statusDetail = statusDetail2;
            return this;
        }

        public com.navdy.service.library.events.navigation.RouteManeuverResponse.Builder maneuvers(java.util.List<com.navdy.service.library.events.navigation.RouteManeuver> maneuvers2) {
            this.maneuvers = checkForNulls(maneuvers2);
            return this;
        }

        public com.navdy.service.library.events.navigation.RouteManeuverResponse build() {
            checkRequiredFields();
            return new com.navdy.service.library.events.navigation.RouteManeuverResponse(this);
        }
    }

    public RouteManeuverResponse(com.navdy.service.library.events.RequestStatus status2, java.lang.String statusDetail2, java.util.List<com.navdy.service.library.events.navigation.RouteManeuver> maneuvers2) {
        this.status = status2;
        this.statusDetail = statusDetail2;
        this.maneuvers = immutableCopyOf(maneuvers2);
    }

    private RouteManeuverResponse(com.navdy.service.library.events.navigation.RouteManeuverResponse.Builder builder) {
        this(builder.status, builder.statusDetail, builder.maneuvers);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.navigation.RouteManeuverResponse)) {
            return false;
        }
        com.navdy.service.library.events.navigation.RouteManeuverResponse o = (com.navdy.service.library.events.navigation.RouteManeuverResponse) other;
        if (!equals((java.lang.Object) this.status, (java.lang.Object) o.status) || !equals((java.lang.Object) this.statusDetail, (java.lang.Object) o.statusDetail) || !equals(this.maneuvers, o.maneuvers)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = (this.status != null ? this.status.hashCode() : 0) * 37;
        if (this.statusDetail != null) {
            i = this.statusDetail.hashCode();
        }
        int result2 = ((hashCode + i) * 37) + (this.maneuvers != null ? this.maneuvers.hashCode() : 1);
        this.hashCode = result2;
        return result2;
    }
}
