package com.navdy.service.library.events.navigation;

public enum DistanceUnit implements com.squareup.wire.ProtoEnum {
    DISTANCE_MILES(1),
    DISTANCE_KMS(2),
    DISTANCE_FEET(3),
    DISTANCE_METERS(4);
    
    private final int value;

    private DistanceUnit(int value2) {
        this.value = value2;
    }

    public int getValue() {
        return this.value;
    }
}
