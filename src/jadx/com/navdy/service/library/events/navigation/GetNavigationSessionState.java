package com.navdy.service.library.events.navigation;

public final class GetNavigationSessionState extends com.squareup.wire.Message {
    private static final long serialVersionUID = 0;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.navigation.GetNavigationSessionState> {
        public Builder() {
        }

        public Builder(com.navdy.service.library.events.navigation.GetNavigationSessionState message) {
            super(message);
        }

        public com.navdy.service.library.events.navigation.GetNavigationSessionState build() {
            return new com.navdy.service.library.events.navigation.GetNavigationSessionState(this);
        }
    }

    public GetNavigationSessionState() {
    }

    private GetNavigationSessionState(com.navdy.service.library.events.navigation.GetNavigationSessionState.Builder builder) {
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        return other instanceof com.navdy.service.library.events.navigation.GetNavigationSessionState;
    }

    public int hashCode() {
        return 0;
    }
}
