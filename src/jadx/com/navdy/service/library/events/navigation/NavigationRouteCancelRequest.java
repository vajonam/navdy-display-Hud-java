package com.navdy.service.library.events.navigation;

public final class NavigationRouteCancelRequest extends com.squareup.wire.Message {
    public static final java.lang.String DEFAULT_HANDLE = "";
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(tag = 1, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String handle;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.navigation.NavigationRouteCancelRequest> {
        public java.lang.String handle;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.navigation.NavigationRouteCancelRequest message) {
            super(message);
            if (message != null) {
                this.handle = message.handle;
            }
        }

        public com.navdy.service.library.events.navigation.NavigationRouteCancelRequest.Builder handle(java.lang.String handle2) {
            this.handle = handle2;
            return this;
        }

        public com.navdy.service.library.events.navigation.NavigationRouteCancelRequest build() {
            return new com.navdy.service.library.events.navigation.NavigationRouteCancelRequest(this);
        }
    }

    public NavigationRouteCancelRequest(java.lang.String handle2) {
        this.handle = handle2;
    }

    private NavigationRouteCancelRequest(com.navdy.service.library.events.navigation.NavigationRouteCancelRequest.Builder builder) {
        this(builder.handle);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.navigation.NavigationRouteCancelRequest)) {
            return false;
        }
        return equals((java.lang.Object) this.handle, (java.lang.Object) ((com.navdy.service.library.events.navigation.NavigationRouteCancelRequest) other).handle);
    }

    public int hashCode() {
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int i = this.handle != null ? this.handle.hashCode() : 0;
        this.hashCode = i;
        return i;
    }
}
