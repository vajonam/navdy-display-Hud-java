package com.navdy.service.library.events.photo;

public final class PhotoRequest extends com.squareup.wire.Message {
    public static final java.lang.String DEFAULT_IDENTIFIER = "";
    public static final java.lang.String DEFAULT_NAME = "";
    public static final java.lang.String DEFAULT_PHOTOCHECKSUM = "";
    public static final com.navdy.service.library.events.photo.PhotoType DEFAULT_PHOTOTYPE = com.navdy.service.library.events.photo.PhotoType.PHOTO_CONTACT;
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 1, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String identifier;
    @com.squareup.wire.ProtoField(tag = 4, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String name;
    @com.squareup.wire.ProtoField(tag = 2, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String photoChecksum;
    @com.squareup.wire.ProtoField(tag = 3, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.photo.PhotoType photoType;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.photo.PhotoRequest> {
        public java.lang.String identifier;
        public java.lang.String name;
        public java.lang.String photoChecksum;
        public com.navdy.service.library.events.photo.PhotoType photoType;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.photo.PhotoRequest message) {
            super(message);
            if (message != null) {
                this.identifier = message.identifier;
                this.photoChecksum = message.photoChecksum;
                this.photoType = message.photoType;
                this.name = message.name;
            }
        }

        public com.navdy.service.library.events.photo.PhotoRequest.Builder identifier(java.lang.String identifier2) {
            this.identifier = identifier2;
            return this;
        }

        public com.navdy.service.library.events.photo.PhotoRequest.Builder photoChecksum(java.lang.String photoChecksum2) {
            this.photoChecksum = photoChecksum2;
            return this;
        }

        public com.navdy.service.library.events.photo.PhotoRequest.Builder photoType(com.navdy.service.library.events.photo.PhotoType photoType2) {
            this.photoType = photoType2;
            return this;
        }

        public com.navdy.service.library.events.photo.PhotoRequest.Builder name(java.lang.String name2) {
            this.name = name2;
            return this;
        }

        public com.navdy.service.library.events.photo.PhotoRequest build() {
            checkRequiredFields();
            return new com.navdy.service.library.events.photo.PhotoRequest(this);
        }
    }

    public PhotoRequest(java.lang.String identifier2, java.lang.String photoChecksum2, com.navdy.service.library.events.photo.PhotoType photoType2, java.lang.String name2) {
        this.identifier = identifier2;
        this.photoChecksum = photoChecksum2;
        this.photoType = photoType2;
        this.name = name2;
    }

    private PhotoRequest(com.navdy.service.library.events.photo.PhotoRequest.Builder builder) {
        this(builder.identifier, builder.photoChecksum, builder.photoType, builder.name);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.photo.PhotoRequest)) {
            return false;
        }
        com.navdy.service.library.events.photo.PhotoRequest o = (com.navdy.service.library.events.photo.PhotoRequest) other;
        if (!equals((java.lang.Object) this.identifier, (java.lang.Object) o.identifier) || !equals((java.lang.Object) this.photoChecksum, (java.lang.Object) o.photoChecksum) || !equals((java.lang.Object) this.photoType, (java.lang.Object) o.photoType) || !equals((java.lang.Object) this.name, (java.lang.Object) o.name)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2;
        int i3 = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = (this.identifier != null ? this.identifier.hashCode() : 0) * 37;
        if (this.photoChecksum != null) {
            i = this.photoChecksum.hashCode();
        } else {
            i = 0;
        }
        int i4 = (hashCode + i) * 37;
        if (this.photoType != null) {
            i2 = this.photoType.hashCode();
        } else {
            i2 = 0;
        }
        int i5 = (i4 + i2) * 37;
        if (this.name != null) {
            i3 = this.name.hashCode();
        }
        int result2 = i5 + i3;
        this.hashCode = result2;
        return result2;
    }
}
