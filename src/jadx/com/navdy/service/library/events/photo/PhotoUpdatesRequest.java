package com.navdy.service.library.events.photo;

public final class PhotoUpdatesRequest extends com.squareup.wire.Message {
    public static final com.navdy.service.library.events.photo.PhotoType DEFAULT_PHOTOTYPE = com.navdy.service.library.events.photo.PhotoType.PHOTO_ALBUM_ART;
    public static final java.lang.Boolean DEFAULT_START = java.lang.Boolean.valueOf(false);
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(tag = 2, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.photo.PhotoType photoType;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 1, type = com.squareup.wire.Message.Datatype.BOOL)
    public final java.lang.Boolean start;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.photo.PhotoUpdatesRequest> {
        public com.navdy.service.library.events.photo.PhotoType photoType;
        public java.lang.Boolean start;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.photo.PhotoUpdatesRequest message) {
            super(message);
            if (message != null) {
                this.start = message.start;
                this.photoType = message.photoType;
            }
        }

        public com.navdy.service.library.events.photo.PhotoUpdatesRequest.Builder start(java.lang.Boolean start2) {
            this.start = start2;
            return this;
        }

        public com.navdy.service.library.events.photo.PhotoUpdatesRequest.Builder photoType(com.navdy.service.library.events.photo.PhotoType photoType2) {
            this.photoType = photoType2;
            return this;
        }

        public com.navdy.service.library.events.photo.PhotoUpdatesRequest build() {
            checkRequiredFields();
            return new com.navdy.service.library.events.photo.PhotoUpdatesRequest(this);
        }
    }

    public PhotoUpdatesRequest(java.lang.Boolean start2, com.navdy.service.library.events.photo.PhotoType photoType2) {
        this.start = start2;
        this.photoType = photoType2;
    }

    private PhotoUpdatesRequest(com.navdy.service.library.events.photo.PhotoUpdatesRequest.Builder builder) {
        this(builder.start, builder.photoType);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.photo.PhotoUpdatesRequest)) {
            return false;
        }
        com.navdy.service.library.events.photo.PhotoUpdatesRequest o = (com.navdy.service.library.events.photo.PhotoUpdatesRequest) other;
        if (!equals((java.lang.Object) this.start, (java.lang.Object) o.start) || !equals((java.lang.Object) this.photoType, (java.lang.Object) o.photoType)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int result;
        int i = 0;
        int result2 = this.hashCode;
        if (result2 != 0) {
            return result2;
        }
        if (this.start != null) {
            result = this.start.hashCode();
        } else {
            result = 0;
        }
        int i2 = result * 37;
        if (this.photoType != null) {
            i = this.photoType.hashCode();
        }
        int result3 = i2 + i;
        this.hashCode = result3;
        return result3;
    }
}
