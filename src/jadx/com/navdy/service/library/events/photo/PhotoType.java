package com.navdy.service.library.events.photo;

public enum PhotoType implements com.squareup.wire.ProtoEnum {
    PHOTO_CONTACT(0),
    PHOTO_ALBUM_ART(1),
    PHOTO_DRIVER_PROFILE(2);
    
    private final int value;

    private PhotoType(int value2) {
        this.value = value2;
    }

    public int getValue() {
        return this.value;
    }
}
