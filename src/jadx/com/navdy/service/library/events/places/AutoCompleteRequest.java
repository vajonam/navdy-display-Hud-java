package com.navdy.service.library.events.places;

public final class AutoCompleteRequest extends com.squareup.wire.Message {
    public static final java.lang.Integer DEFAULT_MAXRESULTS = java.lang.Integer.valueOf(0);
    public static final java.lang.String DEFAULT_PARTIALSEARCH = "";
    public static final java.lang.Integer DEFAULT_SEARCHAREA = java.lang.Integer.valueOf(0);
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(tag = 3, type = com.squareup.wire.Message.Datatype.INT32)
    public final java.lang.Integer maxResults;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 1, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String partialSearch;
    @com.squareup.wire.ProtoField(tag = 2, type = com.squareup.wire.Message.Datatype.INT32)
    public final java.lang.Integer searchArea;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.places.AutoCompleteRequest> {
        public java.lang.Integer maxResults;
        public java.lang.String partialSearch;
        public java.lang.Integer searchArea;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.places.AutoCompleteRequest message) {
            super(message);
            if (message != null) {
                this.partialSearch = message.partialSearch;
                this.searchArea = message.searchArea;
                this.maxResults = message.maxResults;
            }
        }

        public com.navdy.service.library.events.places.AutoCompleteRequest.Builder partialSearch(java.lang.String partialSearch2) {
            this.partialSearch = partialSearch2;
            return this;
        }

        public com.navdy.service.library.events.places.AutoCompleteRequest.Builder searchArea(java.lang.Integer searchArea2) {
            this.searchArea = searchArea2;
            return this;
        }

        public com.navdy.service.library.events.places.AutoCompleteRequest.Builder maxResults(java.lang.Integer maxResults2) {
            this.maxResults = maxResults2;
            return this;
        }

        public com.navdy.service.library.events.places.AutoCompleteRequest build() {
            checkRequiredFields();
            return new com.navdy.service.library.events.places.AutoCompleteRequest(this);
        }
    }

    public AutoCompleteRequest(java.lang.String partialSearch2, java.lang.Integer searchArea2, java.lang.Integer maxResults2) {
        this.partialSearch = partialSearch2;
        this.searchArea = searchArea2;
        this.maxResults = maxResults2;
    }

    private AutoCompleteRequest(com.navdy.service.library.events.places.AutoCompleteRequest.Builder builder) {
        this(builder.partialSearch, builder.searchArea, builder.maxResults);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.places.AutoCompleteRequest)) {
            return false;
        }
        com.navdy.service.library.events.places.AutoCompleteRequest o = (com.navdy.service.library.events.places.AutoCompleteRequest) other;
        if (!equals((java.lang.Object) this.partialSearch, (java.lang.Object) o.partialSearch) || !equals((java.lang.Object) this.searchArea, (java.lang.Object) o.searchArea) || !equals((java.lang.Object) this.maxResults, (java.lang.Object) o.maxResults)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2 = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = (this.partialSearch != null ? this.partialSearch.hashCode() : 0) * 37;
        if (this.searchArea != null) {
            i = this.searchArea.hashCode();
        } else {
            i = 0;
        }
        int i3 = (hashCode + i) * 37;
        if (this.maxResults != null) {
            i2 = this.maxResults.hashCode();
        }
        int result2 = i3 + i2;
        this.hashCode = result2;
        return result2;
    }
}
