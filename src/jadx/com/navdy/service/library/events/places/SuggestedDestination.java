package com.navdy.service.library.events.places;

public final class SuggestedDestination extends com.squareup.wire.Message {
    public static final java.lang.Integer DEFAULT_DURATION_TRAFFIC = java.lang.Integer.valueOf(0);
    public static final com.navdy.service.library.events.places.SuggestedDestination.SuggestionType DEFAULT_TYPE = com.navdy.service.library.events.places.SuggestedDestination.SuggestionType.SUGGESTION_RECOMMENDATION;
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 1)
    public final com.navdy.service.library.events.destination.Destination destination;
    @com.squareup.wire.ProtoField(tag = 2, type = com.squareup.wire.Message.Datatype.UINT32)
    public final java.lang.Integer duration_traffic;
    @com.squareup.wire.ProtoField(tag = 3, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.places.SuggestedDestination.SuggestionType type;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.places.SuggestedDestination> {
        public com.navdy.service.library.events.destination.Destination destination;
        public java.lang.Integer duration_traffic;
        public com.navdy.service.library.events.places.SuggestedDestination.SuggestionType type;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.places.SuggestedDestination message) {
            super(message);
            if (message != null) {
                this.destination = message.destination;
                this.duration_traffic = message.duration_traffic;
                this.type = message.type;
            }
        }

        public com.navdy.service.library.events.places.SuggestedDestination.Builder destination(com.navdy.service.library.events.destination.Destination destination2) {
            this.destination = destination2;
            return this;
        }

        public com.navdy.service.library.events.places.SuggestedDestination.Builder duration_traffic(java.lang.Integer duration_traffic2) {
            this.duration_traffic = duration_traffic2;
            return this;
        }

        public com.navdy.service.library.events.places.SuggestedDestination.Builder type(com.navdy.service.library.events.places.SuggestedDestination.SuggestionType type2) {
            this.type = type2;
            return this;
        }

        public com.navdy.service.library.events.places.SuggestedDestination build() {
            checkRequiredFields();
            return new com.navdy.service.library.events.places.SuggestedDestination(this);
        }
    }

    public enum SuggestionType implements com.squareup.wire.ProtoEnum {
        SUGGESTION_RECOMMENDATION(0),
        SUGGESTION_CALENDAR(1);
        
        private final int value;

        private SuggestionType(int value2) {
            this.value = value2;
        }

        public int getValue() {
            return this.value;
        }
    }

    public SuggestedDestination(com.navdy.service.library.events.destination.Destination destination2, java.lang.Integer duration_traffic2, com.navdy.service.library.events.places.SuggestedDestination.SuggestionType type2) {
        this.destination = destination2;
        this.duration_traffic = duration_traffic2;
        this.type = type2;
    }

    private SuggestedDestination(com.navdy.service.library.events.places.SuggestedDestination.Builder builder) {
        this(builder.destination, builder.duration_traffic, builder.type);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.places.SuggestedDestination)) {
            return false;
        }
        com.navdy.service.library.events.places.SuggestedDestination o = (com.navdy.service.library.events.places.SuggestedDestination) other;
        if (!equals((java.lang.Object) this.destination, (java.lang.Object) o.destination) || !equals((java.lang.Object) this.duration_traffic, (java.lang.Object) o.duration_traffic) || !equals((java.lang.Object) this.type, (java.lang.Object) o.type)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2 = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = (this.destination != null ? this.destination.hashCode() : 0) * 37;
        if (this.duration_traffic != null) {
            i = this.duration_traffic.hashCode();
        } else {
            i = 0;
        }
        int i3 = (hashCode + i) * 37;
        if (this.type != null) {
            i2 = this.type.hashCode();
        }
        int result2 = i3 + i2;
        this.hashCode = result2;
        return result2;
    }
}
