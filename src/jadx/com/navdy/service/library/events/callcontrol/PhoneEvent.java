package com.navdy.service.library.events.callcontrol;

public final class PhoneEvent extends com.squareup.wire.Message {
    public static final java.lang.String DEFAULT_CALLUUID = "";
    public static final java.lang.String DEFAULT_CONTACT_NAME = "";
    public static final java.lang.String DEFAULT_LABEL = "";
    public static final java.lang.String DEFAULT_NUMBER = "";
    public static final com.navdy.service.library.events.callcontrol.PhoneStatus DEFAULT_STATUS = com.navdy.service.library.events.callcontrol.PhoneStatus.PHONE_IDLE;
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(tag = 5, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String callUUID;
    @com.squareup.wire.ProtoField(tag = 3, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String contact_name;
    @com.squareup.wire.ProtoField(tag = 4, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String label;
    @com.squareup.wire.ProtoField(tag = 2, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String number;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 1, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.callcontrol.PhoneStatus status;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.callcontrol.PhoneEvent> {
        public java.lang.String callUUID;
        public java.lang.String contact_name;
        public java.lang.String label;
        public java.lang.String number;
        public com.navdy.service.library.events.callcontrol.PhoneStatus status;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.callcontrol.PhoneEvent message) {
            super(message);
            if (message != null) {
                this.status = message.status;
                this.number = message.number;
                this.contact_name = message.contact_name;
                this.label = message.label;
                this.callUUID = message.callUUID;
            }
        }

        public com.navdy.service.library.events.callcontrol.PhoneEvent.Builder status(com.navdy.service.library.events.callcontrol.PhoneStatus status2) {
            this.status = status2;
            return this;
        }

        public com.navdy.service.library.events.callcontrol.PhoneEvent.Builder number(java.lang.String number2) {
            this.number = number2;
            return this;
        }

        public com.navdy.service.library.events.callcontrol.PhoneEvent.Builder contact_name(java.lang.String contact_name2) {
            this.contact_name = contact_name2;
            return this;
        }

        public com.navdy.service.library.events.callcontrol.PhoneEvent.Builder label(java.lang.String label2) {
            this.label = label2;
            return this;
        }

        public com.navdy.service.library.events.callcontrol.PhoneEvent.Builder callUUID(java.lang.String callUUID2) {
            this.callUUID = callUUID2;
            return this;
        }

        public com.navdy.service.library.events.callcontrol.PhoneEvent build() {
            checkRequiredFields();
            return new com.navdy.service.library.events.callcontrol.PhoneEvent(this);
        }
    }

    public PhoneEvent(com.navdy.service.library.events.callcontrol.PhoneStatus status2, java.lang.String number2, java.lang.String contact_name2, java.lang.String label2, java.lang.String callUUID2) {
        this.status = status2;
        this.number = number2;
        this.contact_name = contact_name2;
        this.label = label2;
        this.callUUID = callUUID2;
    }

    private PhoneEvent(com.navdy.service.library.events.callcontrol.PhoneEvent.Builder builder) {
        this(builder.status, builder.number, builder.contact_name, builder.label, builder.callUUID);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.callcontrol.PhoneEvent)) {
            return false;
        }
        com.navdy.service.library.events.callcontrol.PhoneEvent o = (com.navdy.service.library.events.callcontrol.PhoneEvent) other;
        if (!equals((java.lang.Object) this.status, (java.lang.Object) o.status) || !equals((java.lang.Object) this.number, (java.lang.Object) o.number) || !equals((java.lang.Object) this.contact_name, (java.lang.Object) o.contact_name) || !equals((java.lang.Object) this.label, (java.lang.Object) o.label) || !equals((java.lang.Object) this.callUUID, (java.lang.Object) o.callUUID)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2;
        int i3;
        int i4 = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = (this.status != null ? this.status.hashCode() : 0) * 37;
        if (this.number != null) {
            i = this.number.hashCode();
        } else {
            i = 0;
        }
        int i5 = (hashCode + i) * 37;
        if (this.contact_name != null) {
            i2 = this.contact_name.hashCode();
        } else {
            i2 = 0;
        }
        int i6 = (i5 + i2) * 37;
        if (this.label != null) {
            i3 = this.label.hashCode();
        } else {
            i3 = 0;
        }
        int i7 = (i6 + i3) * 37;
        if (this.callUUID != null) {
            i4 = this.callUUID.hashCode();
        }
        int result2 = i7 + i4;
        this.hashCode = result2;
        return result2;
    }
}
