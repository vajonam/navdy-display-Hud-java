package com.navdy.service.library.events.settings;

public final class ReadSettingsRequest extends com.squareup.wire.Message {
    public static final java.util.List<java.lang.String> DEFAULT_SETTINGS = java.util.Collections.emptyList();
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REPEATED, tag = 1, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.util.List<java.lang.String> settings;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.settings.ReadSettingsRequest> {
        public java.util.List<java.lang.String> settings;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.settings.ReadSettingsRequest message) {
            super(message);
            if (message != null) {
                this.settings = com.navdy.service.library.events.settings.ReadSettingsRequest.copyOf(message.settings);
            }
        }

        public com.navdy.service.library.events.settings.ReadSettingsRequest.Builder settings(java.util.List<java.lang.String> settings2) {
            this.settings = checkForNulls(settings2);
            return this;
        }

        public com.navdy.service.library.events.settings.ReadSettingsRequest build() {
            return new com.navdy.service.library.events.settings.ReadSettingsRequest(this);
        }
    }

    public ReadSettingsRequest(java.util.List<java.lang.String> settings2) {
        this.settings = immutableCopyOf(settings2);
    }

    private ReadSettingsRequest(com.navdy.service.library.events.settings.ReadSettingsRequest.Builder builder) {
        this(builder.settings);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.settings.ReadSettingsRequest)) {
            return false;
        }
        return equals(this.settings, ((com.navdy.service.library.events.settings.ReadSettingsRequest) other).settings);
    }

    public int hashCode() {
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int i = this.settings != null ? this.settings.hashCode() : 1;
        this.hashCode = i;
        return i;
    }
}
