package com.navdy.service.library.events.settings;

public final class NetworkStateChange extends com.squareup.wire.Message {
    public static final java.lang.Boolean DEFAULT_CELLNETWORK = java.lang.Boolean.valueOf(false);
    public static final java.lang.Boolean DEFAULT_NETWORKAVAILABLE = java.lang.Boolean.valueOf(false);
    public static final java.lang.Boolean DEFAULT_REACHABILITY = java.lang.Boolean.valueOf(false);
    public static final java.lang.Boolean DEFAULT_WIFINETWORK = java.lang.Boolean.valueOf(false);
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(tag = 2, type = com.squareup.wire.Message.Datatype.BOOL)
    public final java.lang.Boolean cellNetwork;
    @com.squareup.wire.ProtoField(tag = 1, type = com.squareup.wire.Message.Datatype.BOOL)
    public final java.lang.Boolean networkAvailable;
    @com.squareup.wire.ProtoField(tag = 4, type = com.squareup.wire.Message.Datatype.BOOL)
    public final java.lang.Boolean reachability;
    @com.squareup.wire.ProtoField(tag = 3, type = com.squareup.wire.Message.Datatype.BOOL)
    public final java.lang.Boolean wifiNetwork;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.settings.NetworkStateChange> {
        public java.lang.Boolean cellNetwork;
        public java.lang.Boolean networkAvailable;
        public java.lang.Boolean reachability;
        public java.lang.Boolean wifiNetwork;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.settings.NetworkStateChange message) {
            super(message);
            if (message != null) {
                this.networkAvailable = message.networkAvailable;
                this.cellNetwork = message.cellNetwork;
                this.wifiNetwork = message.wifiNetwork;
                this.reachability = message.reachability;
            }
        }

        public com.navdy.service.library.events.settings.NetworkStateChange.Builder networkAvailable(java.lang.Boolean networkAvailable2) {
            this.networkAvailable = networkAvailable2;
            return this;
        }

        public com.navdy.service.library.events.settings.NetworkStateChange.Builder cellNetwork(java.lang.Boolean cellNetwork2) {
            this.cellNetwork = cellNetwork2;
            return this;
        }

        public com.navdy.service.library.events.settings.NetworkStateChange.Builder wifiNetwork(java.lang.Boolean wifiNetwork2) {
            this.wifiNetwork = wifiNetwork2;
            return this;
        }

        public com.navdy.service.library.events.settings.NetworkStateChange.Builder reachability(java.lang.Boolean reachability2) {
            this.reachability = reachability2;
            return this;
        }

        public com.navdy.service.library.events.settings.NetworkStateChange build() {
            return new com.navdy.service.library.events.settings.NetworkStateChange(this);
        }
    }

    public NetworkStateChange(java.lang.Boolean networkAvailable2, java.lang.Boolean cellNetwork2, java.lang.Boolean wifiNetwork2, java.lang.Boolean reachability2) {
        this.networkAvailable = networkAvailable2;
        this.cellNetwork = cellNetwork2;
        this.wifiNetwork = wifiNetwork2;
        this.reachability = reachability2;
    }

    private NetworkStateChange(com.navdy.service.library.events.settings.NetworkStateChange.Builder builder) {
        this(builder.networkAvailable, builder.cellNetwork, builder.wifiNetwork, builder.reachability);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.settings.NetworkStateChange)) {
            return false;
        }
        com.navdy.service.library.events.settings.NetworkStateChange o = (com.navdy.service.library.events.settings.NetworkStateChange) other;
        if (!equals((java.lang.Object) this.networkAvailable, (java.lang.Object) o.networkAvailable) || !equals((java.lang.Object) this.cellNetwork, (java.lang.Object) o.cellNetwork) || !equals((java.lang.Object) this.wifiNetwork, (java.lang.Object) o.wifiNetwork) || !equals((java.lang.Object) this.reachability, (java.lang.Object) o.reachability)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2;
        int i3 = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = (this.networkAvailable != null ? this.networkAvailable.hashCode() : 0) * 37;
        if (this.cellNetwork != null) {
            i = this.cellNetwork.hashCode();
        } else {
            i = 0;
        }
        int i4 = (hashCode + i) * 37;
        if (this.wifiNetwork != null) {
            i2 = this.wifiNetwork.hashCode();
        } else {
            i2 = 0;
        }
        int i5 = (i4 + i2) * 37;
        if (this.reachability != null) {
            i3 = this.reachability.hashCode();
        }
        int result2 = i5 + i3;
        this.hashCode = result2;
        return result2;
    }
}
