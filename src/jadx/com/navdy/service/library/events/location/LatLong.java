package com.navdy.service.library.events.location;

public final class LatLong extends com.squareup.wire.Message {
    public static final java.lang.Double DEFAULT_LATITUDE = java.lang.Double.valueOf(0.0d);
    public static final java.lang.Double DEFAULT_LONGITUDE = java.lang.Double.valueOf(0.0d);
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 1, type = com.squareup.wire.Message.Datatype.DOUBLE)
    public final java.lang.Double latitude;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 2, type = com.squareup.wire.Message.Datatype.DOUBLE)
    public final java.lang.Double longitude;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.location.LatLong> {
        public java.lang.Double latitude;
        public java.lang.Double longitude;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.location.LatLong message) {
            super(message);
            if (message != null) {
                this.latitude = message.latitude;
                this.longitude = message.longitude;
            }
        }

        public com.navdy.service.library.events.location.LatLong.Builder latitude(java.lang.Double latitude2) {
            this.latitude = latitude2;
            return this;
        }

        public com.navdy.service.library.events.location.LatLong.Builder longitude(java.lang.Double longitude2) {
            this.longitude = longitude2;
            return this;
        }

        public com.navdy.service.library.events.location.LatLong build() {
            checkRequiredFields();
            return new com.navdy.service.library.events.location.LatLong(this);
        }
    }

    public LatLong(java.lang.Double latitude2, java.lang.Double longitude2) {
        this.latitude = latitude2;
        this.longitude = longitude2;
    }

    private LatLong(com.navdy.service.library.events.location.LatLong.Builder builder) {
        this(builder.latitude, builder.longitude);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.location.LatLong)) {
            return false;
        }
        com.navdy.service.library.events.location.LatLong o = (com.navdy.service.library.events.location.LatLong) other;
        if (!equals((java.lang.Object) this.latitude, (java.lang.Object) o.latitude) || !equals((java.lang.Object) this.longitude, (java.lang.Object) o.longitude)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int result;
        int i = 0;
        int result2 = this.hashCode;
        if (result2 != 0) {
            return result2;
        }
        if (this.latitude != null) {
            result = this.latitude.hashCode();
        } else {
            result = 0;
        }
        int i2 = result * 37;
        if (this.longitude != null) {
            i = this.longitude.hashCode();
        }
        int result3 = i2 + i;
        this.hashCode = result3;
        return result3;
    }
}
