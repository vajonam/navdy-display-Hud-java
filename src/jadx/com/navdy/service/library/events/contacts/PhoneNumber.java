package com.navdy.service.library.events.contacts;

public final class PhoneNumber extends com.squareup.wire.Message {
    public static final java.lang.String DEFAULT_CUSTOMTYPE = "";
    public static final java.lang.String DEFAULT_NUMBER = "";
    public static final com.navdy.service.library.events.contacts.PhoneNumberType DEFAULT_NUMBERTYPE = com.navdy.service.library.events.contacts.PhoneNumberType.PHONE_NUMBER_HOME;
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(tag = 3, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String customType;
    @com.squareup.wire.ProtoField(tag = 1, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String number;
    @com.squareup.wire.ProtoField(tag = 2, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.contacts.PhoneNumberType numberType;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.contacts.PhoneNumber> {
        public java.lang.String customType;
        public java.lang.String number;
        public com.navdy.service.library.events.contacts.PhoneNumberType numberType;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.contacts.PhoneNumber message) {
            super(message);
            if (message != null) {
                this.number = message.number;
                this.numberType = message.numberType;
                this.customType = message.customType;
            }
        }

        public com.navdy.service.library.events.contacts.PhoneNumber.Builder number(java.lang.String number2) {
            this.number = number2;
            return this;
        }

        public com.navdy.service.library.events.contacts.PhoneNumber.Builder numberType(com.navdy.service.library.events.contacts.PhoneNumberType numberType2) {
            this.numberType = numberType2;
            return this;
        }

        public com.navdy.service.library.events.contacts.PhoneNumber.Builder customType(java.lang.String customType2) {
            this.customType = customType2;
            return this;
        }

        public com.navdy.service.library.events.contacts.PhoneNumber build() {
            return new com.navdy.service.library.events.contacts.PhoneNumber(this);
        }
    }

    public PhoneNumber(java.lang.String number2, com.navdy.service.library.events.contacts.PhoneNumberType numberType2, java.lang.String customType2) {
        this.number = number2;
        this.numberType = numberType2;
        this.customType = customType2;
    }

    private PhoneNumber(com.navdy.service.library.events.contacts.PhoneNumber.Builder builder) {
        this(builder.number, builder.numberType, builder.customType);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.contacts.PhoneNumber)) {
            return false;
        }
        com.navdy.service.library.events.contacts.PhoneNumber o = (com.navdy.service.library.events.contacts.PhoneNumber) other;
        if (!equals((java.lang.Object) this.number, (java.lang.Object) o.number) || !equals((java.lang.Object) this.numberType, (java.lang.Object) o.numberType) || !equals((java.lang.Object) this.customType, (java.lang.Object) o.customType)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2 = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = (this.number != null ? this.number.hashCode() : 0) * 37;
        if (this.numberType != null) {
            i = this.numberType.hashCode();
        } else {
            i = 0;
        }
        int i3 = (hashCode + i) * 37;
        if (this.customType != null) {
            i2 = this.customType.hashCode();
        }
        int result2 = i3 + i2;
        this.hashCode = result2;
        return result2;
    }
}
