package com.navdy.service.library.events.contacts;

public final class Contact extends com.squareup.wire.Message {
    public static final java.lang.String DEFAULT_LABEL = "";
    public static final java.lang.String DEFAULT_NAME = "";
    public static final java.lang.String DEFAULT_NUMBER = "";
    public static final com.navdy.service.library.events.contacts.PhoneNumberType DEFAULT_NUMBERTYPE = com.navdy.service.library.events.contacts.PhoneNumberType.PHONE_NUMBER_HOME;
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(tag = 4, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String label;
    @com.squareup.wire.ProtoField(tag = 1, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String name;
    @com.squareup.wire.ProtoField(tag = 2, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String number;
    @com.squareup.wire.ProtoField(tag = 3, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.contacts.PhoneNumberType numberType;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.contacts.Contact> {
        public java.lang.String label;
        public java.lang.String name;
        public java.lang.String number;
        public com.navdy.service.library.events.contacts.PhoneNumberType numberType;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.contacts.Contact message) {
            super(message);
            if (message != null) {
                this.name = message.name;
                this.number = message.number;
                this.numberType = message.numberType;
                this.label = message.label;
            }
        }

        public com.navdy.service.library.events.contacts.Contact.Builder name(java.lang.String name2) {
            this.name = name2;
            return this;
        }

        public com.navdy.service.library.events.contacts.Contact.Builder number(java.lang.String number2) {
            this.number = number2;
            return this;
        }

        public com.navdy.service.library.events.contacts.Contact.Builder numberType(com.navdy.service.library.events.contacts.PhoneNumberType numberType2) {
            this.numberType = numberType2;
            return this;
        }

        public com.navdy.service.library.events.contacts.Contact.Builder label(java.lang.String label2) {
            this.label = label2;
            return this;
        }

        public com.navdy.service.library.events.contacts.Contact build() {
            return new com.navdy.service.library.events.contacts.Contact(this);
        }
    }

    public Contact(java.lang.String name2, java.lang.String number2, com.navdy.service.library.events.contacts.PhoneNumberType numberType2, java.lang.String label2) {
        this.name = name2;
        this.number = number2;
        this.numberType = numberType2;
        this.label = label2;
    }

    private Contact(com.navdy.service.library.events.contacts.Contact.Builder builder) {
        this(builder.name, builder.number, builder.numberType, builder.label);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.contacts.Contact)) {
            return false;
        }
        com.navdy.service.library.events.contacts.Contact o = (com.navdy.service.library.events.contacts.Contact) other;
        if (!equals((java.lang.Object) this.name, (java.lang.Object) o.name) || !equals((java.lang.Object) this.number, (java.lang.Object) o.number) || !equals((java.lang.Object) this.numberType, (java.lang.Object) o.numberType) || !equals((java.lang.Object) this.label, (java.lang.Object) o.label)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2;
        int i3 = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = (this.name != null ? this.name.hashCode() : 0) * 37;
        if (this.number != null) {
            i = this.number.hashCode();
        } else {
            i = 0;
        }
        int i4 = (hashCode + i) * 37;
        if (this.numberType != null) {
            i2 = this.numberType.hashCode();
        } else {
            i2 = 0;
        }
        int i5 = (i4 + i2) * 37;
        if (this.label != null) {
            i3 = this.label.hashCode();
        }
        int result2 = i5 + i3;
        this.hashCode = result2;
        return result2;
    }
}
