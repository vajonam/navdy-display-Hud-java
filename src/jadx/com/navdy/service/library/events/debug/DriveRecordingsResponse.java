package com.navdy.service.library.events.debug;

public final class DriveRecordingsResponse extends com.squareup.wire.Message {
    public static final java.util.List<java.lang.String> DEFAULT_RECORDINGS = java.util.Collections.emptyList();
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REPEATED, tag = 1, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.util.List<java.lang.String> recordings;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.debug.DriveRecordingsResponse> {
        public java.util.List<java.lang.String> recordings;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.debug.DriveRecordingsResponse message) {
            super(message);
            if (message != null) {
                this.recordings = com.navdy.service.library.events.debug.DriveRecordingsResponse.copyOf(message.recordings);
            }
        }

        public com.navdy.service.library.events.debug.DriveRecordingsResponse.Builder recordings(java.util.List<java.lang.String> recordings2) {
            this.recordings = checkForNulls(recordings2);
            return this;
        }

        public com.navdy.service.library.events.debug.DriveRecordingsResponse build() {
            return new com.navdy.service.library.events.debug.DriveRecordingsResponse(this);
        }
    }

    public DriveRecordingsResponse(java.util.List<java.lang.String> recordings2) {
        this.recordings = immutableCopyOf(recordings2);
    }

    private DriveRecordingsResponse(com.navdy.service.library.events.debug.DriveRecordingsResponse.Builder builder) {
        this(builder.recordings);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.debug.DriveRecordingsResponse)) {
            return false;
        }
        return equals(this.recordings, ((com.navdy.service.library.events.debug.DriveRecordingsResponse) other).recordings);
    }

    public int hashCode() {
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int i = this.recordings != null ? this.recordings.hashCode() : 1;
        this.hashCode = i;
        return i;
    }
}
