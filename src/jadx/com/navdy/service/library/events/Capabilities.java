package com.navdy.service.library.events;

public final class Capabilities extends com.squareup.wire.Message {
    public static final java.lang.Boolean DEFAULT_CANNEDRESPONSETOSMS = java.lang.Boolean.valueOf(false);
    public static final java.lang.Boolean DEFAULT_COMPACTUI = java.lang.Boolean.valueOf(false);
    public static final java.lang.Boolean DEFAULT_CUSTOMDIALLONGPRESS = java.lang.Boolean.valueOf(false);
    public static final java.lang.Boolean DEFAULT_LOCALMUSICBROWSER = java.lang.Boolean.valueOf(false);
    public static final java.lang.Boolean DEFAULT_MUSICARTWORKCACHE = java.lang.Boolean.valueOf(false);
    public static final java.lang.Boolean DEFAULT_NAVCOORDSLOOKUP = java.lang.Boolean.valueOf(false);
    public static final java.lang.Boolean DEFAULT_PLACETYPESEARCH = java.lang.Boolean.valueOf(false);
    public static final java.lang.Boolean DEFAULT_SEARCHRESULTLIST = java.lang.Boolean.valueOf(false);
    public static final java.lang.Boolean DEFAULT_SUPPORTSPHONETICTTS = java.lang.Boolean.valueOf(false);
    public static final java.lang.Boolean DEFAULT_SUPPORTSPLAYAUDIOREQUEST = java.lang.Boolean.valueOf(false);
    public static final java.lang.Boolean DEFAULT_VOICESEARCH = java.lang.Boolean.valueOf(false);
    public static final java.lang.Boolean DEFAULT_VOICESEARCHNEWIOSPAUSEBEHAVIORS = java.lang.Boolean.valueOf(false);
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(tag = 9, type = com.squareup.wire.Message.Datatype.BOOL)
    public final java.lang.Boolean cannedResponseToSms;
    @com.squareup.wire.ProtoField(tag = 1, type = com.squareup.wire.Message.Datatype.BOOL)
    public final java.lang.Boolean compactUi;
    @com.squareup.wire.ProtoField(tag = 10, type = com.squareup.wire.Message.Datatype.BOOL)
    public final java.lang.Boolean customDialLongPress;
    @com.squareup.wire.ProtoField(tag = 4, type = com.squareup.wire.Message.Datatype.BOOL)
    public final java.lang.Boolean localMusicBrowser;
    @com.squareup.wire.ProtoField(tag = 7, type = com.squareup.wire.Message.Datatype.BOOL)
    public final java.lang.Boolean musicArtworkCache;
    @com.squareup.wire.ProtoField(tag = 5, type = com.squareup.wire.Message.Datatype.BOOL)
    public final java.lang.Boolean navCoordsLookup;
    @com.squareup.wire.ProtoField(tag = 2, type = com.squareup.wire.Message.Datatype.BOOL)
    public final java.lang.Boolean placeTypeSearch;
    @com.squareup.wire.ProtoField(tag = 6, type = com.squareup.wire.Message.Datatype.BOOL)
    public final java.lang.Boolean searchResultList;
    @com.squareup.wire.ProtoField(tag = 11, type = com.squareup.wire.Message.Datatype.BOOL)
    public final java.lang.Boolean supportsPhoneticTts;
    @com.squareup.wire.ProtoField(tag = 12, type = com.squareup.wire.Message.Datatype.BOOL)
    public final java.lang.Boolean supportsPlayAudioRequest;
    @com.squareup.wire.ProtoField(tag = 3, type = com.squareup.wire.Message.Datatype.BOOL)
    public final java.lang.Boolean voiceSearch;
    @com.squareup.wire.ProtoField(tag = 8, type = com.squareup.wire.Message.Datatype.BOOL)
    public final java.lang.Boolean voiceSearchNewIOSPauseBehaviors;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.Capabilities> {
        public java.lang.Boolean cannedResponseToSms;
        public java.lang.Boolean compactUi;
        public java.lang.Boolean customDialLongPress;
        public java.lang.Boolean localMusicBrowser;
        public java.lang.Boolean musicArtworkCache;
        public java.lang.Boolean navCoordsLookup;
        public java.lang.Boolean placeTypeSearch;
        public java.lang.Boolean searchResultList;
        public java.lang.Boolean supportsPhoneticTts;
        public java.lang.Boolean supportsPlayAudioRequest;
        public java.lang.Boolean voiceSearch;
        public java.lang.Boolean voiceSearchNewIOSPauseBehaviors;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.Capabilities message) {
            super(message);
            if (message != null) {
                this.compactUi = message.compactUi;
                this.placeTypeSearch = message.placeTypeSearch;
                this.voiceSearch = message.voiceSearch;
                this.localMusicBrowser = message.localMusicBrowser;
                this.navCoordsLookup = message.navCoordsLookup;
                this.searchResultList = message.searchResultList;
                this.musicArtworkCache = message.musicArtworkCache;
                this.voiceSearchNewIOSPauseBehaviors = message.voiceSearchNewIOSPauseBehaviors;
                this.cannedResponseToSms = message.cannedResponseToSms;
                this.customDialLongPress = message.customDialLongPress;
                this.supportsPhoneticTts = message.supportsPhoneticTts;
                this.supportsPlayAudioRequest = message.supportsPlayAudioRequest;
            }
        }

        public com.navdy.service.library.events.Capabilities.Builder compactUi(java.lang.Boolean compactUi2) {
            this.compactUi = compactUi2;
            return this;
        }

        public com.navdy.service.library.events.Capabilities.Builder placeTypeSearch(java.lang.Boolean placeTypeSearch2) {
            this.placeTypeSearch = placeTypeSearch2;
            return this;
        }

        public com.navdy.service.library.events.Capabilities.Builder voiceSearch(java.lang.Boolean voiceSearch2) {
            this.voiceSearch = voiceSearch2;
            return this;
        }

        public com.navdy.service.library.events.Capabilities.Builder localMusicBrowser(java.lang.Boolean localMusicBrowser2) {
            this.localMusicBrowser = localMusicBrowser2;
            return this;
        }

        public com.navdy.service.library.events.Capabilities.Builder navCoordsLookup(java.lang.Boolean navCoordsLookup2) {
            this.navCoordsLookup = navCoordsLookup2;
            return this;
        }

        public com.navdy.service.library.events.Capabilities.Builder searchResultList(java.lang.Boolean searchResultList2) {
            this.searchResultList = searchResultList2;
            return this;
        }

        public com.navdy.service.library.events.Capabilities.Builder musicArtworkCache(java.lang.Boolean musicArtworkCache2) {
            this.musicArtworkCache = musicArtworkCache2;
            return this;
        }

        public com.navdy.service.library.events.Capabilities.Builder voiceSearchNewIOSPauseBehaviors(java.lang.Boolean voiceSearchNewIOSPauseBehaviors2) {
            this.voiceSearchNewIOSPauseBehaviors = voiceSearchNewIOSPauseBehaviors2;
            return this;
        }

        public com.navdy.service.library.events.Capabilities.Builder cannedResponseToSms(java.lang.Boolean cannedResponseToSms2) {
            this.cannedResponseToSms = cannedResponseToSms2;
            return this;
        }

        public com.navdy.service.library.events.Capabilities.Builder customDialLongPress(java.lang.Boolean customDialLongPress2) {
            this.customDialLongPress = customDialLongPress2;
            return this;
        }

        public com.navdy.service.library.events.Capabilities.Builder supportsPhoneticTts(java.lang.Boolean supportsPhoneticTts2) {
            this.supportsPhoneticTts = supportsPhoneticTts2;
            return this;
        }

        public com.navdy.service.library.events.Capabilities.Builder supportsPlayAudioRequest(java.lang.Boolean supportsPlayAudioRequest2) {
            this.supportsPlayAudioRequest = supportsPlayAudioRequest2;
            return this;
        }

        public com.navdy.service.library.events.Capabilities build() {
            return new com.navdy.service.library.events.Capabilities(this);
        }
    }

    public Capabilities(java.lang.Boolean compactUi2, java.lang.Boolean placeTypeSearch2, java.lang.Boolean voiceSearch2, java.lang.Boolean localMusicBrowser2, java.lang.Boolean navCoordsLookup2, java.lang.Boolean searchResultList2, java.lang.Boolean musicArtworkCache2, java.lang.Boolean voiceSearchNewIOSPauseBehaviors2, java.lang.Boolean cannedResponseToSms2, java.lang.Boolean customDialLongPress2, java.lang.Boolean supportsPhoneticTts2, java.lang.Boolean supportsPlayAudioRequest2) {
        this.compactUi = compactUi2;
        this.placeTypeSearch = placeTypeSearch2;
        this.voiceSearch = voiceSearch2;
        this.localMusicBrowser = localMusicBrowser2;
        this.navCoordsLookup = navCoordsLookup2;
        this.searchResultList = searchResultList2;
        this.musicArtworkCache = musicArtworkCache2;
        this.voiceSearchNewIOSPauseBehaviors = voiceSearchNewIOSPauseBehaviors2;
        this.cannedResponseToSms = cannedResponseToSms2;
        this.customDialLongPress = customDialLongPress2;
        this.supportsPhoneticTts = supportsPhoneticTts2;
        this.supportsPlayAudioRequest = supportsPlayAudioRequest2;
    }

    private Capabilities(com.navdy.service.library.events.Capabilities.Builder builder) {
        this(builder.compactUi, builder.placeTypeSearch, builder.voiceSearch, builder.localMusicBrowser, builder.navCoordsLookup, builder.searchResultList, builder.musicArtworkCache, builder.voiceSearchNewIOSPauseBehaviors, builder.cannedResponseToSms, builder.customDialLongPress, builder.supportsPhoneticTts, builder.supportsPlayAudioRequest);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.Capabilities)) {
            return false;
        }
        com.navdy.service.library.events.Capabilities o = (com.navdy.service.library.events.Capabilities) other;
        if (!equals((java.lang.Object) this.compactUi, (java.lang.Object) o.compactUi) || !equals((java.lang.Object) this.placeTypeSearch, (java.lang.Object) o.placeTypeSearch) || !equals((java.lang.Object) this.voiceSearch, (java.lang.Object) o.voiceSearch) || !equals((java.lang.Object) this.localMusicBrowser, (java.lang.Object) o.localMusicBrowser) || !equals((java.lang.Object) this.navCoordsLookup, (java.lang.Object) o.navCoordsLookup) || !equals((java.lang.Object) this.searchResultList, (java.lang.Object) o.searchResultList) || !equals((java.lang.Object) this.musicArtworkCache, (java.lang.Object) o.musicArtworkCache) || !equals((java.lang.Object) this.voiceSearchNewIOSPauseBehaviors, (java.lang.Object) o.voiceSearchNewIOSPauseBehaviors) || !equals((java.lang.Object) this.cannedResponseToSms, (java.lang.Object) o.cannedResponseToSms) || !equals((java.lang.Object) this.customDialLongPress, (java.lang.Object) o.customDialLongPress) || !equals((java.lang.Object) this.supportsPhoneticTts, (java.lang.Object) o.supportsPhoneticTts) || !equals((java.lang.Object) this.supportsPlayAudioRequest, (java.lang.Object) o.supportsPlayAudioRequest)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        int i8;
        int i9;
        int i10;
        int i11 = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = (this.compactUi != null ? this.compactUi.hashCode() : 0) * 37;
        if (this.placeTypeSearch != null) {
            i = this.placeTypeSearch.hashCode();
        } else {
            i = 0;
        }
        int i12 = (hashCode + i) * 37;
        if (this.voiceSearch != null) {
            i2 = this.voiceSearch.hashCode();
        } else {
            i2 = 0;
        }
        int i13 = (i12 + i2) * 37;
        if (this.localMusicBrowser != null) {
            i3 = this.localMusicBrowser.hashCode();
        } else {
            i3 = 0;
        }
        int i14 = (i13 + i3) * 37;
        if (this.navCoordsLookup != null) {
            i4 = this.navCoordsLookup.hashCode();
        } else {
            i4 = 0;
        }
        int i15 = (i14 + i4) * 37;
        if (this.searchResultList != null) {
            i5 = this.searchResultList.hashCode();
        } else {
            i5 = 0;
        }
        int i16 = (i15 + i5) * 37;
        if (this.musicArtworkCache != null) {
            i6 = this.musicArtworkCache.hashCode();
        } else {
            i6 = 0;
        }
        int i17 = (i16 + i6) * 37;
        if (this.voiceSearchNewIOSPauseBehaviors != null) {
            i7 = this.voiceSearchNewIOSPauseBehaviors.hashCode();
        } else {
            i7 = 0;
        }
        int i18 = (i17 + i7) * 37;
        if (this.cannedResponseToSms != null) {
            i8 = this.cannedResponseToSms.hashCode();
        } else {
            i8 = 0;
        }
        int i19 = (i18 + i8) * 37;
        if (this.customDialLongPress != null) {
            i9 = this.customDialLongPress.hashCode();
        } else {
            i9 = 0;
        }
        int i20 = (i19 + i9) * 37;
        if (this.supportsPhoneticTts != null) {
            i10 = this.supportsPhoneticTts.hashCode();
        } else {
            i10 = 0;
        }
        int i21 = (i20 + i10) * 37;
        if (this.supportsPlayAudioRequest != null) {
            i11 = this.supportsPlayAudioRequest.hashCode();
        }
        int result2 = i21 + i11;
        this.hashCode = result2;
        return result2;
    }
}
