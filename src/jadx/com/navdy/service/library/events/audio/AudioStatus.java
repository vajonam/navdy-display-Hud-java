package com.navdy.service.library.events.audio;

public final class AudioStatus extends com.squareup.wire.Message {
    public static final com.navdy.service.library.events.audio.AudioStatus.ConnectionType DEFAULT_CONNECTIONTYPE = com.navdy.service.library.events.audio.AudioStatus.ConnectionType.AUDIO_CONNECTION_NONE;
    public static final java.lang.Boolean DEFAULT_HASSCOCONNECTION = java.lang.Boolean.valueOf(false);
    public static final java.lang.Boolean DEFAULT_ISCONNECTED = java.lang.Boolean.valueOf(false);
    public static final java.lang.Boolean DEFAULT_ISPLAYING = java.lang.Boolean.valueOf(false);
    public static final com.navdy.service.library.events.audio.AudioStatus.ProfileType DEFAULT_PROFILETYPE = com.navdy.service.library.events.audio.AudioStatus.ProfileType.AUDIO_PROFILE_NONE;
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(tag = 2, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.audio.AudioStatus.ConnectionType connectionType;
    @com.squareup.wire.ProtoField(tag = 5, type = com.squareup.wire.Message.Datatype.BOOL)
    public final java.lang.Boolean hasSCOConnection;
    @com.squareup.wire.ProtoField(tag = 1, type = com.squareup.wire.Message.Datatype.BOOL)
    public final java.lang.Boolean isConnected;
    @com.squareup.wire.ProtoField(tag = 4, type = com.squareup.wire.Message.Datatype.BOOL)
    public final java.lang.Boolean isPlaying;
    @com.squareup.wire.ProtoField(tag = 3, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.audio.AudioStatus.ProfileType profileType;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.audio.AudioStatus> {
        public com.navdy.service.library.events.audio.AudioStatus.ConnectionType connectionType;
        public java.lang.Boolean hasSCOConnection;
        public java.lang.Boolean isConnected;
        public java.lang.Boolean isPlaying;
        public com.navdy.service.library.events.audio.AudioStatus.ProfileType profileType;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.audio.AudioStatus message) {
            super(message);
            if (message != null) {
                this.isConnected = message.isConnected;
                this.connectionType = message.connectionType;
                this.profileType = message.profileType;
                this.isPlaying = message.isPlaying;
                this.hasSCOConnection = message.hasSCOConnection;
            }
        }

        public com.navdy.service.library.events.audio.AudioStatus.Builder isConnected(java.lang.Boolean isConnected2) {
            this.isConnected = isConnected2;
            return this;
        }

        public com.navdy.service.library.events.audio.AudioStatus.Builder connectionType(com.navdy.service.library.events.audio.AudioStatus.ConnectionType connectionType2) {
            this.connectionType = connectionType2;
            return this;
        }

        public com.navdy.service.library.events.audio.AudioStatus.Builder profileType(com.navdy.service.library.events.audio.AudioStatus.ProfileType profileType2) {
            this.profileType = profileType2;
            return this;
        }

        public com.navdy.service.library.events.audio.AudioStatus.Builder isPlaying(java.lang.Boolean isPlaying2) {
            this.isPlaying = isPlaying2;
            return this;
        }

        public com.navdy.service.library.events.audio.AudioStatus.Builder hasSCOConnection(java.lang.Boolean hasSCOConnection2) {
            this.hasSCOConnection = hasSCOConnection2;
            return this;
        }

        public com.navdy.service.library.events.audio.AudioStatus build() {
            return new com.navdy.service.library.events.audio.AudioStatus(this);
        }
    }

    public enum ConnectionType implements com.squareup.wire.ProtoEnum {
        AUDIO_CONNECTION_NONE(1),
        AUDIO_CONNECTION_BLUETOOTH(2),
        AUDIO_CONNECTION_SPEAKER(3),
        AUDIO_CONNECTION_AUX(4);
        
        private final int value;

        private ConnectionType(int value2) {
            this.value = value2;
        }

        public int getValue() {
            return this.value;
        }
    }

    public enum ProfileType implements com.squareup.wire.ProtoEnum {
        AUDIO_PROFILE_NONE(1),
        AUDIO_PROFILE_A2DP(2),
        AUDIO_PROFILE_HFP(3);
        
        private final int value;

        private ProfileType(int value2) {
            this.value = value2;
        }

        public int getValue() {
            return this.value;
        }
    }

    public AudioStatus(java.lang.Boolean isConnected2, com.navdy.service.library.events.audio.AudioStatus.ConnectionType connectionType2, com.navdy.service.library.events.audio.AudioStatus.ProfileType profileType2, java.lang.Boolean isPlaying2, java.lang.Boolean hasSCOConnection2) {
        this.isConnected = isConnected2;
        this.connectionType = connectionType2;
        this.profileType = profileType2;
        this.isPlaying = isPlaying2;
        this.hasSCOConnection = hasSCOConnection2;
    }

    private AudioStatus(com.navdy.service.library.events.audio.AudioStatus.Builder builder) {
        this(builder.isConnected, builder.connectionType, builder.profileType, builder.isPlaying, builder.hasSCOConnection);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.audio.AudioStatus)) {
            return false;
        }
        com.navdy.service.library.events.audio.AudioStatus o = (com.navdy.service.library.events.audio.AudioStatus) other;
        if (!equals((java.lang.Object) this.isConnected, (java.lang.Object) o.isConnected) || !equals((java.lang.Object) this.connectionType, (java.lang.Object) o.connectionType) || !equals((java.lang.Object) this.profileType, (java.lang.Object) o.profileType) || !equals((java.lang.Object) this.isPlaying, (java.lang.Object) o.isPlaying) || !equals((java.lang.Object) this.hasSCOConnection, (java.lang.Object) o.hasSCOConnection)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2;
        int i3;
        int i4 = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = (this.isConnected != null ? this.isConnected.hashCode() : 0) * 37;
        if (this.connectionType != null) {
            i = this.connectionType.hashCode();
        } else {
            i = 0;
        }
        int i5 = (hashCode + i) * 37;
        if (this.profileType != null) {
            i2 = this.profileType.hashCode();
        } else {
            i2 = 0;
        }
        int i6 = (i5 + i2) * 37;
        if (this.isPlaying != null) {
            i3 = this.isPlaying.hashCode();
        } else {
            i3 = 0;
        }
        int i7 = (i6 + i3) * 37;
        if (this.hasSCOConnection != null) {
            i4 = this.hasSCOConnection.hashCode();
        }
        int result2 = i7 + i4;
        this.hashCode = result2;
        return result2;
    }
}
