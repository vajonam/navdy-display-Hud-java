package com.navdy.service.library.events.audio;

public enum Category implements com.squareup.wire.ProtoEnum {
    SPEECH_TURN_BY_TURN(1),
    SPEECH_REROUTE(2),
    SPEECH_SPEED_WARNING(3),
    SPEECH_GPS_CONNECTIVITY(4),
    SPEECH_MESSAGE_READ_OUT(5),
    SPEECH_WELCOME_MESSAGE(6),
    SPEECH_NOTIFICATION(7),
    SPEECH_CAMERA_WARNING(8),
    AUDIO_DRIVE_BEHAVIOR(9);
    
    private final int value;

    private Category(int value2) {
        this.value = value2;
    }

    public int getValue() {
        return this.value;
    }
}
