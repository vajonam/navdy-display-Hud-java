package com.navdy.service.library.events.audio;

public final class MusicTrackInfoRequest extends com.squareup.wire.Message {
    private static final long serialVersionUID = 0;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.audio.MusicTrackInfoRequest> {
        public Builder() {
        }

        public Builder(com.navdy.service.library.events.audio.MusicTrackInfoRequest message) {
            super(message);
        }

        public com.navdy.service.library.events.audio.MusicTrackInfoRequest build() {
            return new com.navdy.service.library.events.audio.MusicTrackInfoRequest(this);
        }
    }

    public MusicTrackInfoRequest() {
    }

    private MusicTrackInfoRequest(com.navdy.service.library.events.audio.MusicTrackInfoRequest.Builder builder) {
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        return other instanceof com.navdy.service.library.events.audio.MusicTrackInfoRequest;
    }

    public int hashCode() {
        return 0;
    }
}
