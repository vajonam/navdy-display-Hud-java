package com.navdy.service.library.events.audio;

public final class MusicEvent extends com.squareup.wire.Message {
    public static final com.navdy.service.library.events.audio.MusicEvent.Action DEFAULT_ACTION = com.navdy.service.library.events.audio.MusicEvent.Action.MUSIC_ACTION_PLAY;
    public static final java.lang.String DEFAULT_COLLECTIONID = "";
    public static final com.navdy.service.library.events.audio.MusicCollectionSource DEFAULT_COLLECTIONSOURCE = com.navdy.service.library.events.audio.MusicCollectionSource.COLLECTION_SOURCE_UNKNOWN;
    public static final com.navdy.service.library.events.audio.MusicCollectionType DEFAULT_COLLECTIONTYPE = com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_UNKNOWN;
    public static final com.navdy.service.library.events.audio.MusicDataSource DEFAULT_DATASOURCE = com.navdy.service.library.events.audio.MusicDataSource.MUSIC_SOURCE_NONE;
    public static final java.lang.Integer DEFAULT_INDEX = java.lang.Integer.valueOf(0);
    public static final com.navdy.service.library.events.audio.MusicRepeatMode DEFAULT_REPEATMODE = com.navdy.service.library.events.audio.MusicRepeatMode.MUSIC_REPEAT_MODE_UNKNOWN;
    public static final com.navdy.service.library.events.audio.MusicShuffleMode DEFAULT_SHUFFLEMODE = com.navdy.service.library.events.audio.MusicShuffleMode.MUSIC_SHUFFLE_MODE_UNKNOWN;
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 1, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.audio.MusicEvent.Action action;
    @com.squareup.wire.ProtoField(tag = 13, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String collectionId;
    @com.squareup.wire.ProtoField(tag = 14, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.audio.MusicCollectionSource collectionSource;
    @com.squareup.wire.ProtoField(tag = 15, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.audio.MusicCollectionType collectionType;
    @com.squareup.wire.ProtoField(tag = 2, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.audio.MusicDataSource dataSource;
    @com.squareup.wire.ProtoField(tag = 16, type = com.squareup.wire.Message.Datatype.INT32)
    public final java.lang.Integer index;
    @com.squareup.wire.ProtoField(tag = 18, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.audio.MusicRepeatMode repeatMode;
    @com.squareup.wire.ProtoField(tag = 17, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.audio.MusicShuffleMode shuffleMode;

    public enum Action implements com.squareup.wire.ProtoEnum {
        MUSIC_ACTION_PLAY(1),
        MUSIC_ACTION_PAUSE(2),
        MUSIC_ACTION_NEXT(3),
        MUSIC_ACTION_PREVIOUS(4),
        MUSIC_ACTION_REWIND_START(5),
        MUSIC_ACTION_FAST_FORWARD_START(6),
        MUSIC_ACTION_REWIND_STOP(7),
        MUSIC_ACTION_FAST_FORWARD_STOP(8),
        MUSIC_ACTION_MODE_CHANGE(9);
        
        private final int value;

        private Action(int value2) {
            this.value = value2;
        }

        public int getValue() {
            return this.value;
        }
    }

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.audio.MusicEvent> {
        public com.navdy.service.library.events.audio.MusicEvent.Action action;
        public java.lang.String collectionId;
        public com.navdy.service.library.events.audio.MusicCollectionSource collectionSource;
        public com.navdy.service.library.events.audio.MusicCollectionType collectionType;
        public com.navdy.service.library.events.audio.MusicDataSource dataSource;
        public java.lang.Integer index;
        public com.navdy.service.library.events.audio.MusicRepeatMode repeatMode;
        public com.navdy.service.library.events.audio.MusicShuffleMode shuffleMode;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.audio.MusicEvent message) {
            super(message);
            if (message != null) {
                this.action = message.action;
                this.dataSource = message.dataSource;
                this.collectionSource = message.collectionSource;
                this.collectionType = message.collectionType;
                this.collectionId = message.collectionId;
                this.index = message.index;
                this.shuffleMode = message.shuffleMode;
                this.repeatMode = message.repeatMode;
            }
        }

        public com.navdy.service.library.events.audio.MusicEvent.Builder action(com.navdy.service.library.events.audio.MusicEvent.Action action2) {
            this.action = action2;
            return this;
        }

        public com.navdy.service.library.events.audio.MusicEvent.Builder dataSource(com.navdy.service.library.events.audio.MusicDataSource dataSource2) {
            this.dataSource = dataSource2;
            return this;
        }

        public com.navdy.service.library.events.audio.MusicEvent.Builder collectionSource(com.navdy.service.library.events.audio.MusicCollectionSource collectionSource2) {
            this.collectionSource = collectionSource2;
            return this;
        }

        public com.navdy.service.library.events.audio.MusicEvent.Builder collectionType(com.navdy.service.library.events.audio.MusicCollectionType collectionType2) {
            this.collectionType = collectionType2;
            return this;
        }

        public com.navdy.service.library.events.audio.MusicEvent.Builder collectionId(java.lang.String collectionId2) {
            this.collectionId = collectionId2;
            return this;
        }

        public com.navdy.service.library.events.audio.MusicEvent.Builder index(java.lang.Integer index2) {
            this.index = index2;
            return this;
        }

        public com.navdy.service.library.events.audio.MusicEvent.Builder shuffleMode(com.navdy.service.library.events.audio.MusicShuffleMode shuffleMode2) {
            this.shuffleMode = shuffleMode2;
            return this;
        }

        public com.navdy.service.library.events.audio.MusicEvent.Builder repeatMode(com.navdy.service.library.events.audio.MusicRepeatMode repeatMode2) {
            this.repeatMode = repeatMode2;
            return this;
        }

        public com.navdy.service.library.events.audio.MusicEvent build() {
            checkRequiredFields();
            return new com.navdy.service.library.events.audio.MusicEvent(this);
        }
    }

    public MusicEvent(com.navdy.service.library.events.audio.MusicEvent.Action action2, com.navdy.service.library.events.audio.MusicDataSource dataSource2, com.navdy.service.library.events.audio.MusicCollectionSource collectionSource2, com.navdy.service.library.events.audio.MusicCollectionType collectionType2, java.lang.String collectionId2, java.lang.Integer index2, com.navdy.service.library.events.audio.MusicShuffleMode shuffleMode2, com.navdy.service.library.events.audio.MusicRepeatMode repeatMode2) {
        this.action = action2;
        this.dataSource = dataSource2;
        this.collectionSource = collectionSource2;
        this.collectionType = collectionType2;
        this.collectionId = collectionId2;
        this.index = index2;
        this.shuffleMode = shuffleMode2;
        this.repeatMode = repeatMode2;
    }

    private MusicEvent(com.navdy.service.library.events.audio.MusicEvent.Builder builder) {
        this(builder.action, builder.dataSource, builder.collectionSource, builder.collectionType, builder.collectionId, builder.index, builder.shuffleMode, builder.repeatMode);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.audio.MusicEvent)) {
            return false;
        }
        com.navdy.service.library.events.audio.MusicEvent o = (com.navdy.service.library.events.audio.MusicEvent) other;
        if (!equals((java.lang.Object) this.action, (java.lang.Object) o.action) || !equals((java.lang.Object) this.dataSource, (java.lang.Object) o.dataSource) || !equals((java.lang.Object) this.collectionSource, (java.lang.Object) o.collectionSource) || !equals((java.lang.Object) this.collectionType, (java.lang.Object) o.collectionType) || !equals((java.lang.Object) this.collectionId, (java.lang.Object) o.collectionId) || !equals((java.lang.Object) this.index, (java.lang.Object) o.index) || !equals((java.lang.Object) this.shuffleMode, (java.lang.Object) o.shuffleMode) || !equals((java.lang.Object) this.repeatMode, (java.lang.Object) o.repeatMode)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7 = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = (this.action != null ? this.action.hashCode() : 0) * 37;
        if (this.dataSource != null) {
            i = this.dataSource.hashCode();
        } else {
            i = 0;
        }
        int i8 = (hashCode + i) * 37;
        if (this.collectionSource != null) {
            i2 = this.collectionSource.hashCode();
        } else {
            i2 = 0;
        }
        int i9 = (i8 + i2) * 37;
        if (this.collectionType != null) {
            i3 = this.collectionType.hashCode();
        } else {
            i3 = 0;
        }
        int i10 = (i9 + i3) * 37;
        if (this.collectionId != null) {
            i4 = this.collectionId.hashCode();
        } else {
            i4 = 0;
        }
        int i11 = (i10 + i4) * 37;
        if (this.index != null) {
            i5 = this.index.hashCode();
        } else {
            i5 = 0;
        }
        int i12 = (i11 + i5) * 37;
        if (this.shuffleMode != null) {
            i6 = this.shuffleMode.hashCode();
        } else {
            i6 = 0;
        }
        int i13 = (i12 + i6) * 37;
        if (this.repeatMode != null) {
            i7 = this.repeatMode.hashCode();
        }
        int result2 = i13 + i7;
        this.hashCode = result2;
        return result2;
    }
}
