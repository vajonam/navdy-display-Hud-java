package com.navdy.service.library.events.audio;

public enum MusicPlaybackState implements com.squareup.wire.ProtoEnum {
    PLAYBACK_NONE(1),
    PLAYBACK_BUFFERING(2),
    PLAYBACK_CONNECTING(3),
    PLAYBACK_ERROR(4),
    PLAYBACK_PLAYING(5),
    PLAYBACK_PAUSED(6),
    PLAYBACK_STOPPED(7),
    PLAYBACK_FAST_FORWARDING(8),
    PLAYBACK_REWINDING(9),
    PLAYBACK_SKIPPING_TO_NEXT(10),
    PLAYBACK_SKIPPING_TO_PREVIOUS(11);
    
    private final int value;

    private MusicPlaybackState(int value2) {
        this.value = value2;
    }

    public int getValue() {
        return this.value;
    }
}
