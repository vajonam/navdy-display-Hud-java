package com.navdy.service.library.events.audio;

public final class VoiceSearchRequest extends com.squareup.wire.Message {
    public static final java.lang.Boolean DEFAULT_END = java.lang.Boolean.valueOf(false);
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(tag = 1, type = com.squareup.wire.Message.Datatype.BOOL)
    public final java.lang.Boolean end;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.audio.VoiceSearchRequest> {
        public java.lang.Boolean end;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.audio.VoiceSearchRequest message) {
            super(message);
            if (message != null) {
                this.end = message.end;
            }
        }

        public com.navdy.service.library.events.audio.VoiceSearchRequest.Builder end(java.lang.Boolean end2) {
            this.end = end2;
            return this;
        }

        public com.navdy.service.library.events.audio.VoiceSearchRequest build() {
            return new com.navdy.service.library.events.audio.VoiceSearchRequest(this);
        }
    }

    public VoiceSearchRequest(java.lang.Boolean end2) {
        this.end = end2;
    }

    private VoiceSearchRequest(com.navdy.service.library.events.audio.VoiceSearchRequest.Builder builder) {
        this(builder.end);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.audio.VoiceSearchRequest)) {
            return false;
        }
        return equals((java.lang.Object) this.end, (java.lang.Object) ((com.navdy.service.library.events.audio.VoiceSearchRequest) other).end);
    }

    public int hashCode() {
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int i = this.end != null ? this.end.hashCode() : 0;
        this.hashCode = i;
        return i;
    }
}
