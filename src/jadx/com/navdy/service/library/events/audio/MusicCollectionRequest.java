package com.navdy.service.library.events.audio;

public final class MusicCollectionRequest extends com.squareup.wire.Message {
    public static final java.lang.String DEFAULT_COLLECTIONID = "";
    public static final com.navdy.service.library.events.audio.MusicCollectionSource DEFAULT_COLLECTIONSOURCE = com.navdy.service.library.events.audio.MusicCollectionSource.COLLECTION_SOURCE_UNKNOWN;
    public static final com.navdy.service.library.events.audio.MusicCollectionType DEFAULT_COLLECTIONTYPE = com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_UNKNOWN;
    public static final java.util.List<com.navdy.service.library.events.audio.MusicCollectionFilter> DEFAULT_FILTERS = java.util.Collections.emptyList();
    public static final com.navdy.service.library.events.audio.MusicCollectionType DEFAULT_GROUPBY = com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_UNKNOWN;
    public static final java.lang.Boolean DEFAULT_INCLUDECHARACTERMAP = java.lang.Boolean.valueOf(false);
    public static final java.lang.Integer DEFAULT_LIMIT = java.lang.Integer.valueOf(0);
    public static final java.lang.Integer DEFAULT_OFFSET = java.lang.Integer.valueOf(0);
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(tag = 4, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String collectionId;
    @com.squareup.wire.ProtoField(tag = 1, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.audio.MusicCollectionSource collectionSource;
    @com.squareup.wire.ProtoField(tag = 2, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.audio.MusicCollectionType collectionType;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REPEATED, messageType = com.navdy.service.library.events.audio.MusicCollectionFilter.class, tag = 3)
    public final java.util.List<com.navdy.service.library.events.audio.MusicCollectionFilter> filters;
    @com.squareup.wire.ProtoField(tag = 8, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.audio.MusicCollectionType groupBy;
    @com.squareup.wire.ProtoField(tag = 7, type = com.squareup.wire.Message.Datatype.BOOL)
    public final java.lang.Boolean includeCharacterMap;
    @com.squareup.wire.ProtoField(tag = 5, type = com.squareup.wire.Message.Datatype.INT32)
    public final java.lang.Integer limit;
    @com.squareup.wire.ProtoField(tag = 6, type = com.squareup.wire.Message.Datatype.INT32)
    public final java.lang.Integer offset;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.audio.MusicCollectionRequest> {
        public java.lang.String collectionId;
        public com.navdy.service.library.events.audio.MusicCollectionSource collectionSource;
        public com.navdy.service.library.events.audio.MusicCollectionType collectionType;
        public java.util.List<com.navdy.service.library.events.audio.MusicCollectionFilter> filters;
        public com.navdy.service.library.events.audio.MusicCollectionType groupBy;
        public java.lang.Boolean includeCharacterMap;
        public java.lang.Integer limit;
        public java.lang.Integer offset;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.audio.MusicCollectionRequest message) {
            super(message);
            if (message != null) {
                this.collectionSource = message.collectionSource;
                this.collectionType = message.collectionType;
                this.filters = com.navdy.service.library.events.audio.MusicCollectionRequest.copyOf(message.filters);
                this.collectionId = message.collectionId;
                this.limit = message.limit;
                this.offset = message.offset;
                this.includeCharacterMap = message.includeCharacterMap;
                this.groupBy = message.groupBy;
            }
        }

        public com.navdy.service.library.events.audio.MusicCollectionRequest.Builder collectionSource(com.navdy.service.library.events.audio.MusicCollectionSource collectionSource2) {
            this.collectionSource = collectionSource2;
            return this;
        }

        public com.navdy.service.library.events.audio.MusicCollectionRequest.Builder collectionType(com.navdy.service.library.events.audio.MusicCollectionType collectionType2) {
            this.collectionType = collectionType2;
            return this;
        }

        public com.navdy.service.library.events.audio.MusicCollectionRequest.Builder filters(java.util.List<com.navdy.service.library.events.audio.MusicCollectionFilter> filters2) {
            this.filters = checkForNulls(filters2);
            return this;
        }

        public com.navdy.service.library.events.audio.MusicCollectionRequest.Builder collectionId(java.lang.String collectionId2) {
            this.collectionId = collectionId2;
            return this;
        }

        public com.navdy.service.library.events.audio.MusicCollectionRequest.Builder limit(java.lang.Integer limit2) {
            this.limit = limit2;
            return this;
        }

        public com.navdy.service.library.events.audio.MusicCollectionRequest.Builder offset(java.lang.Integer offset2) {
            this.offset = offset2;
            return this;
        }

        public com.navdy.service.library.events.audio.MusicCollectionRequest.Builder includeCharacterMap(java.lang.Boolean includeCharacterMap2) {
            this.includeCharacterMap = includeCharacterMap2;
            return this;
        }

        public com.navdy.service.library.events.audio.MusicCollectionRequest.Builder groupBy(com.navdy.service.library.events.audio.MusicCollectionType groupBy2) {
            this.groupBy = groupBy2;
            return this;
        }

        public com.navdy.service.library.events.audio.MusicCollectionRequest build() {
            return new com.navdy.service.library.events.audio.MusicCollectionRequest(this);
        }
    }

    public MusicCollectionRequest(com.navdy.service.library.events.audio.MusicCollectionSource collectionSource2, com.navdy.service.library.events.audio.MusicCollectionType collectionType2, java.util.List<com.navdy.service.library.events.audio.MusicCollectionFilter> filters2, java.lang.String collectionId2, java.lang.Integer limit2, java.lang.Integer offset2, java.lang.Boolean includeCharacterMap2, com.navdy.service.library.events.audio.MusicCollectionType groupBy2) {
        this.collectionSource = collectionSource2;
        this.collectionType = collectionType2;
        this.filters = immutableCopyOf(filters2);
        this.collectionId = collectionId2;
        this.limit = limit2;
        this.offset = offset2;
        this.includeCharacterMap = includeCharacterMap2;
        this.groupBy = groupBy2;
    }

    private MusicCollectionRequest(com.navdy.service.library.events.audio.MusicCollectionRequest.Builder builder) {
        this(builder.collectionSource, builder.collectionType, builder.filters, builder.collectionId, builder.limit, builder.offset, builder.includeCharacterMap, builder.groupBy);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.audio.MusicCollectionRequest)) {
            return false;
        }
        com.navdy.service.library.events.audio.MusicCollectionRequest o = (com.navdy.service.library.events.audio.MusicCollectionRequest) other;
        if (!equals((java.lang.Object) this.collectionSource, (java.lang.Object) o.collectionSource) || !equals((java.lang.Object) this.collectionType, (java.lang.Object) o.collectionType) || !equals(this.filters, o.filters) || !equals((java.lang.Object) this.collectionId, (java.lang.Object) o.collectionId) || !equals((java.lang.Object) this.limit, (java.lang.Object) o.limit) || !equals((java.lang.Object) this.offset, (java.lang.Object) o.offset) || !equals((java.lang.Object) this.includeCharacterMap, (java.lang.Object) o.includeCharacterMap) || !equals((java.lang.Object) this.groupBy, (java.lang.Object) o.groupBy)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6 = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = (this.collectionSource != null ? this.collectionSource.hashCode() : 0) * 37;
        if (this.collectionType != null) {
            i = this.collectionType.hashCode();
        } else {
            i = 0;
        }
        int hashCode2 = (((hashCode + i) * 37) + (this.filters != null ? this.filters.hashCode() : 1)) * 37;
        if (this.collectionId != null) {
            i2 = this.collectionId.hashCode();
        } else {
            i2 = 0;
        }
        int i7 = (hashCode2 + i2) * 37;
        if (this.limit != null) {
            i3 = this.limit.hashCode();
        } else {
            i3 = 0;
        }
        int i8 = (i7 + i3) * 37;
        if (this.offset != null) {
            i4 = this.offset.hashCode();
        } else {
            i4 = 0;
        }
        int i9 = (i8 + i4) * 37;
        if (this.includeCharacterMap != null) {
            i5 = this.includeCharacterMap.hashCode();
        } else {
            i5 = 0;
        }
        int i10 = (i9 + i5) * 37;
        if (this.groupBy != null) {
            i6 = this.groupBy.hashCode();
        }
        int result2 = i10 + i6;
        this.hashCode = result2;
        return result2;
    }
}
