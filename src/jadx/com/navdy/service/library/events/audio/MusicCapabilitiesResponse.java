package com.navdy.service.library.events.audio;

public final class MusicCapabilitiesResponse extends com.squareup.wire.Message {
    public static final java.util.List<com.navdy.service.library.events.audio.MusicCapability> DEFAULT_CAPABILITIES = java.util.Collections.emptyList();
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REPEATED, messageType = com.navdy.service.library.events.audio.MusicCapability.class, tag = 1)
    public final java.util.List<com.navdy.service.library.events.audio.MusicCapability> capabilities;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.audio.MusicCapabilitiesResponse> {
        public java.util.List<com.navdy.service.library.events.audio.MusicCapability> capabilities;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.audio.MusicCapabilitiesResponse message) {
            super(message);
            if (message != null) {
                this.capabilities = com.navdy.service.library.events.audio.MusicCapabilitiesResponse.copyOf(message.capabilities);
            }
        }

        public com.navdy.service.library.events.audio.MusicCapabilitiesResponse.Builder capabilities(java.util.List<com.navdy.service.library.events.audio.MusicCapability> capabilities2) {
            this.capabilities = checkForNulls(capabilities2);
            return this;
        }

        public com.navdy.service.library.events.audio.MusicCapabilitiesResponse build() {
            return new com.navdy.service.library.events.audio.MusicCapabilitiesResponse(this);
        }
    }

    public MusicCapabilitiesResponse(java.util.List<com.navdy.service.library.events.audio.MusicCapability> capabilities2) {
        this.capabilities = immutableCopyOf(capabilities2);
    }

    private MusicCapabilitiesResponse(com.navdy.service.library.events.audio.MusicCapabilitiesResponse.Builder builder) {
        this(builder.capabilities);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.audio.MusicCapabilitiesResponse)) {
            return false;
        }
        return equals(this.capabilities, ((com.navdy.service.library.events.audio.MusicCapabilitiesResponse) other).capabilities);
    }

    public int hashCode() {
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int i = this.capabilities != null ? this.capabilities.hashCode() : 1;
        this.hashCode = i;
        return i;
    }
}
