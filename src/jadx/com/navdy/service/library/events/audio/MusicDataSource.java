package com.navdy.service.library.events.audio;

public enum MusicDataSource implements com.squareup.wire.ProtoEnum {
    MUSIC_SOURCE_NONE(1),
    MUSIC_SOURCE_ANDROID_INTENT(2),
    MUSIC_SOURCE_MEDIA_CONTROLLER(3),
    MUSIC_SOURCE_OS_NOTIFICATION(4),
    MUSIC_SOURCE_SPOTIFY_INTENT(5),
    MUSIC_SOURCE_PANDORA_API(6);
    
    private final int value;

    private MusicDataSource(int value2) {
        this.value = value2;
    }

    public int getValue() {
        return this.value;
    }
}
