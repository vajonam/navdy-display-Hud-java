package com.navdy.service.library.events.audio;

public final class MusicCollectionSourceUpdate extends com.squareup.wire.Message {
    public static final com.navdy.service.library.events.audio.MusicCollectionSource DEFAULT_COLLECTIONSOURCE = com.navdy.service.library.events.audio.MusicCollectionSource.COLLECTION_SOURCE_UNKNOWN;
    public static final java.lang.Long DEFAULT_SERIAL_NUMBER = java.lang.Long.valueOf(0);
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(tag = 1, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.audio.MusicCollectionSource collectionSource;
    @com.squareup.wire.ProtoField(tag = 2, type = com.squareup.wire.Message.Datatype.INT64)
    public final java.lang.Long serial_number;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.audio.MusicCollectionSourceUpdate> {
        public com.navdy.service.library.events.audio.MusicCollectionSource collectionSource;
        public java.lang.Long serial_number;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.audio.MusicCollectionSourceUpdate message) {
            super(message);
            if (message != null) {
                this.collectionSource = message.collectionSource;
                this.serial_number = message.serial_number;
            }
        }

        public com.navdy.service.library.events.audio.MusicCollectionSourceUpdate.Builder collectionSource(com.navdy.service.library.events.audio.MusicCollectionSource collectionSource2) {
            this.collectionSource = collectionSource2;
            return this;
        }

        public com.navdy.service.library.events.audio.MusicCollectionSourceUpdate.Builder serial_number(java.lang.Long serial_number2) {
            this.serial_number = serial_number2;
            return this;
        }

        public com.navdy.service.library.events.audio.MusicCollectionSourceUpdate build() {
            return new com.navdy.service.library.events.audio.MusicCollectionSourceUpdate(this);
        }
    }

    public MusicCollectionSourceUpdate(com.navdy.service.library.events.audio.MusicCollectionSource collectionSource2, java.lang.Long serial_number2) {
        this.collectionSource = collectionSource2;
        this.serial_number = serial_number2;
    }

    private MusicCollectionSourceUpdate(com.navdy.service.library.events.audio.MusicCollectionSourceUpdate.Builder builder) {
        this(builder.collectionSource, builder.serial_number);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.audio.MusicCollectionSourceUpdate)) {
            return false;
        }
        com.navdy.service.library.events.audio.MusicCollectionSourceUpdate o = (com.navdy.service.library.events.audio.MusicCollectionSourceUpdate) other;
        if (!equals((java.lang.Object) this.collectionSource, (java.lang.Object) o.collectionSource) || !equals((java.lang.Object) this.serial_number, (java.lang.Object) o.serial_number)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int result;
        int i = 0;
        int result2 = this.hashCode;
        if (result2 != 0) {
            return result2;
        }
        if (this.collectionSource != null) {
            result = this.collectionSource.hashCode();
        } else {
            result = 0;
        }
        int i2 = result * 37;
        if (this.serial_number != null) {
            i = this.serial_number.hashCode();
        }
        int result3 = i2 + i;
        this.hashCode = result3;
        return result3;
    }
}
