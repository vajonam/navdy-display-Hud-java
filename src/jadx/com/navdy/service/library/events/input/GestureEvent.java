package com.navdy.service.library.events.input;

public final class GestureEvent extends com.squareup.wire.Message {
    public static final com.navdy.service.library.events.input.Gesture DEFAULT_GESTURE = com.navdy.service.library.events.input.Gesture.GESTURE_SWIPE_LEFT;
    public static final java.lang.Integer DEFAULT_X = java.lang.Integer.valueOf(0);
    public static final java.lang.Integer DEFAULT_Y = java.lang.Integer.valueOf(0);
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 1, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.input.Gesture gesture;
    @com.squareup.wire.ProtoField(tag = 2, type = com.squareup.wire.Message.Datatype.INT32)
    public final java.lang.Integer x;
    @com.squareup.wire.ProtoField(tag = 3, type = com.squareup.wire.Message.Datatype.INT32)
    public final java.lang.Integer y;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.input.GestureEvent> {
        public com.navdy.service.library.events.input.Gesture gesture;
        public java.lang.Integer x;
        public java.lang.Integer y;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.input.GestureEvent message) {
            super(message);
            if (message != null) {
                this.gesture = message.gesture;
                this.x = message.x;
                this.y = message.y;
            }
        }

        public com.navdy.service.library.events.input.GestureEvent.Builder gesture(com.navdy.service.library.events.input.Gesture gesture2) {
            this.gesture = gesture2;
            return this;
        }

        public com.navdy.service.library.events.input.GestureEvent.Builder x(java.lang.Integer x2) {
            this.x = x2;
            return this;
        }

        public com.navdy.service.library.events.input.GestureEvent.Builder y(java.lang.Integer y2) {
            this.y = y2;
            return this;
        }

        public com.navdy.service.library.events.input.GestureEvent build() {
            checkRequiredFields();
            return new com.navdy.service.library.events.input.GestureEvent(this);
        }
    }

    public GestureEvent(com.navdy.service.library.events.input.Gesture gesture2, java.lang.Integer x2, java.lang.Integer y2) {
        this.gesture = gesture2;
        this.x = x2;
        this.y = y2;
    }

    private GestureEvent(com.navdy.service.library.events.input.GestureEvent.Builder builder) {
        this(builder.gesture, builder.x, builder.y);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.input.GestureEvent)) {
            return false;
        }
        com.navdy.service.library.events.input.GestureEvent o = (com.navdy.service.library.events.input.GestureEvent) other;
        if (!equals((java.lang.Object) this.gesture, (java.lang.Object) o.gesture) || !equals((java.lang.Object) this.x, (java.lang.Object) o.x) || !equals((java.lang.Object) this.y, (java.lang.Object) o.y)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2 = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = (this.gesture != null ? this.gesture.hashCode() : 0) * 37;
        if (this.x != null) {
            i = this.x.hashCode();
        } else {
            i = 0;
        }
        int i3 = (hashCode + i) * 37;
        if (this.y != null) {
            i2 = this.y.hashCode();
        }
        int result2 = i3 + i2;
        this.hashCode = result2;
        return result2;
    }
}
