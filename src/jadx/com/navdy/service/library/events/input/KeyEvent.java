package com.navdy.service.library.events.input;

public enum KeyEvent implements com.squareup.wire.ProtoEnum {
    KEY_DOWN(1),
    KEY_UP(2);
    
    private final int value;

    private KeyEvent(int value2) {
        this.value = value2;
    }

    public int getValue() {
        return this.value;
    }
}
