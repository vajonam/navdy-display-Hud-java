package com.navdy.service.library.events.input;

public enum MediaRemoteKey implements com.squareup.wire.ProtoEnum {
    MEDIA_REMOTE_KEY_SIRI(1),
    MEDIA_REMOTE_KEY_PLAY(2),
    MEDIA_REMOTE_KEY_NEXT(3),
    MEDIA_REMOTE_KEY_PREV(4);
    
    private final int value;

    private MediaRemoteKey(int value2) {
        this.value = value2;
    }

    public int getValue() {
        return this.value;
    }
}
