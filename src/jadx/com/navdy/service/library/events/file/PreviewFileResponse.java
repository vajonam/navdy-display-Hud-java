package com.navdy.service.library.events.file;

public final class PreviewFileResponse extends com.squareup.wire.Message {
    public static final java.lang.String DEFAULT_FILENAME = "";
    public static final com.navdy.service.library.events.RequestStatus DEFAULT_STATUS = com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS;
    public static final java.lang.String DEFAULT_STATUS_DETAIL = "";
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(tag = 3, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String filename;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 1, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.RequestStatus status;
    @com.squareup.wire.ProtoField(tag = 2, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String status_detail;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.file.PreviewFileResponse> {
        public java.lang.String filename;
        public com.navdy.service.library.events.RequestStatus status;
        public java.lang.String status_detail;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.file.PreviewFileResponse message) {
            super(message);
            if (message != null) {
                this.status = message.status;
                this.status_detail = message.status_detail;
                this.filename = message.filename;
            }
        }

        public com.navdy.service.library.events.file.PreviewFileResponse.Builder status(com.navdy.service.library.events.RequestStatus status2) {
            this.status = status2;
            return this;
        }

        public com.navdy.service.library.events.file.PreviewFileResponse.Builder status_detail(java.lang.String status_detail2) {
            this.status_detail = status_detail2;
            return this;
        }

        public com.navdy.service.library.events.file.PreviewFileResponse.Builder filename(java.lang.String filename2) {
            this.filename = filename2;
            return this;
        }

        public com.navdy.service.library.events.file.PreviewFileResponse build() {
            checkRequiredFields();
            return new com.navdy.service.library.events.file.PreviewFileResponse(this);
        }
    }

    public PreviewFileResponse(com.navdy.service.library.events.RequestStatus status2, java.lang.String status_detail2, java.lang.String filename2) {
        this.status = status2;
        this.status_detail = status_detail2;
        this.filename = filename2;
    }

    private PreviewFileResponse(com.navdy.service.library.events.file.PreviewFileResponse.Builder builder) {
        this(builder.status, builder.status_detail, builder.filename);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.file.PreviewFileResponse)) {
            return false;
        }
        com.navdy.service.library.events.file.PreviewFileResponse o = (com.navdy.service.library.events.file.PreviewFileResponse) other;
        if (!equals((java.lang.Object) this.status, (java.lang.Object) o.status) || !equals((java.lang.Object) this.status_detail, (java.lang.Object) o.status_detail) || !equals((java.lang.Object) this.filename, (java.lang.Object) o.filename)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2 = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = (this.status != null ? this.status.hashCode() : 0) * 37;
        if (this.status_detail != null) {
            i = this.status_detail.hashCode();
        } else {
            i = 0;
        }
        int i3 = (hashCode + i) * 37;
        if (this.filename != null) {
            i2 = this.filename.hashCode();
        }
        int result2 = i3 + i2;
        this.hashCode = result2;
        return result2;
    }
}
