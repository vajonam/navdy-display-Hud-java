package com.navdy.service.library.events.file;

public final class FileListRequest extends com.squareup.wire.Message {
    public static final com.navdy.service.library.events.file.FileType DEFAULT_FILE_TYPE = com.navdy.service.library.events.file.FileType.FILE_TYPE_OTA;
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 1, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.file.FileType file_type;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.file.FileListRequest> {
        public com.navdy.service.library.events.file.FileType file_type;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.file.FileListRequest message) {
            super(message);
            if (message != null) {
                this.file_type = message.file_type;
            }
        }

        public com.navdy.service.library.events.file.FileListRequest.Builder file_type(com.navdy.service.library.events.file.FileType file_type2) {
            this.file_type = file_type2;
            return this;
        }

        public com.navdy.service.library.events.file.FileListRequest build() {
            checkRequiredFields();
            return new com.navdy.service.library.events.file.FileListRequest(this);
        }
    }

    public FileListRequest(com.navdy.service.library.events.file.FileType file_type2) {
        this.file_type = file_type2;
    }

    private FileListRequest(com.navdy.service.library.events.file.FileListRequest.Builder builder) {
        this(builder.file_type);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.file.FileListRequest)) {
            return false;
        }
        return equals((java.lang.Object) this.file_type, (java.lang.Object) ((com.navdy.service.library.events.file.FileListRequest) other).file_type);
    }

    public int hashCode() {
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int i = this.file_type != null ? this.file_type.hashCode() : 0;
        this.hashCode = i;
        return i;
    }
}
