package com.navdy.service.library.events.obd;

public final class ObdStatusResponse extends com.squareup.wire.Message {
    public static final java.lang.Boolean DEFAULT_ISCONNECTED = java.lang.Boolean.valueOf(false);
    public static final java.lang.Integer DEFAULT_SPEED = java.lang.Integer.valueOf(0);
    public static final com.navdy.service.library.events.RequestStatus DEFAULT_STATUS = com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS;
    public static final java.lang.String DEFAULT_VIN = "";
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 2, type = com.squareup.wire.Message.Datatype.BOOL)
    public final java.lang.Boolean isConnected;
    @com.squareup.wire.ProtoField(tag = 4, type = com.squareup.wire.Message.Datatype.INT32)
    public final java.lang.Integer speed;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 1, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.RequestStatus status;
    @com.squareup.wire.ProtoField(tag = 3, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String vin;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.obd.ObdStatusResponse> {
        public java.lang.Boolean isConnected;
        public java.lang.Integer speed;
        public com.navdy.service.library.events.RequestStatus status;
        public java.lang.String vin;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.obd.ObdStatusResponse message) {
            super(message);
            if (message != null) {
                this.status = message.status;
                this.isConnected = message.isConnected;
                this.vin = message.vin;
                this.speed = message.speed;
            }
        }

        public com.navdy.service.library.events.obd.ObdStatusResponse.Builder status(com.navdy.service.library.events.RequestStatus status2) {
            this.status = status2;
            return this;
        }

        public com.navdy.service.library.events.obd.ObdStatusResponse.Builder isConnected(java.lang.Boolean isConnected2) {
            this.isConnected = isConnected2;
            return this;
        }

        public com.navdy.service.library.events.obd.ObdStatusResponse.Builder vin(java.lang.String vin2) {
            this.vin = vin2;
            return this;
        }

        public com.navdy.service.library.events.obd.ObdStatusResponse.Builder speed(java.lang.Integer speed2) {
            this.speed = speed2;
            return this;
        }

        public com.navdy.service.library.events.obd.ObdStatusResponse build() {
            checkRequiredFields();
            return new com.navdy.service.library.events.obd.ObdStatusResponse(this);
        }
    }

    public ObdStatusResponse(com.navdy.service.library.events.RequestStatus status2, java.lang.Boolean isConnected2, java.lang.String vin2, java.lang.Integer speed2) {
        this.status = status2;
        this.isConnected = isConnected2;
        this.vin = vin2;
        this.speed = speed2;
    }

    private ObdStatusResponse(com.navdy.service.library.events.obd.ObdStatusResponse.Builder builder) {
        this(builder.status, builder.isConnected, builder.vin, builder.speed);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.obd.ObdStatusResponse)) {
            return false;
        }
        com.navdy.service.library.events.obd.ObdStatusResponse o = (com.navdy.service.library.events.obd.ObdStatusResponse) other;
        if (!equals((java.lang.Object) this.status, (java.lang.Object) o.status) || !equals((java.lang.Object) this.isConnected, (java.lang.Object) o.isConnected) || !equals((java.lang.Object) this.vin, (java.lang.Object) o.vin) || !equals((java.lang.Object) this.speed, (java.lang.Object) o.speed)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2;
        int i3 = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = (this.status != null ? this.status.hashCode() : 0) * 37;
        if (this.isConnected != null) {
            i = this.isConnected.hashCode();
        } else {
            i = 0;
        }
        int i4 = (hashCode + i) * 37;
        if (this.vin != null) {
            i2 = this.vin.hashCode();
        } else {
            i2 = 0;
        }
        int i5 = (i4 + i2) * 37;
        if (this.speed != null) {
            i3 = this.speed.hashCode();
        }
        int result2 = i5 + i3;
        this.hashCode = result2;
        return result2;
    }
}
