package com.navdy.service.library.events.obd;

public final class ObdStatusRequest extends com.squareup.wire.Message {
    private static final long serialVersionUID = 0;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.obd.ObdStatusRequest> {
        public Builder() {
        }

        public Builder(com.navdy.service.library.events.obd.ObdStatusRequest message) {
            super(message);
        }

        public com.navdy.service.library.events.obd.ObdStatusRequest build() {
            return new com.navdy.service.library.events.obd.ObdStatusRequest(this);
        }
    }

    public ObdStatusRequest() {
    }

    private ObdStatusRequest(com.navdy.service.library.events.obd.ObdStatusRequest.Builder builder) {
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        return other instanceof com.navdy.service.library.events.obd.ObdStatusRequest;
    }

    public int hashCode() {
        return 0;
    }
}
