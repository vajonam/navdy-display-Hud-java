package com.navdy.service.library.events.preferences;

public final class InputPreferences extends com.squareup.wire.Message {
    public static final com.navdy.service.library.events.preferences.InputPreferences.DialSensitivity DEFAULT_DIAL_SENSITIVITY = com.navdy.service.library.events.preferences.InputPreferences.DialSensitivity.DIAL_SENSITIVITY_STANDARD;
    public static final java.lang.Long DEFAULT_SERIAL_NUMBER = java.lang.Long.valueOf(0);
    public static final java.lang.Boolean DEFAULT_USE_GESTURES = java.lang.Boolean.valueOf(true);
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(tag = 3, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.preferences.InputPreferences.DialSensitivity dial_sensitivity;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 1, type = com.squareup.wire.Message.Datatype.INT64)
    public final java.lang.Long serial_number;
    @com.squareup.wire.ProtoField(tag = 2, type = com.squareup.wire.Message.Datatype.BOOL)
    public final java.lang.Boolean use_gestures;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.preferences.InputPreferences> {
        public com.navdy.service.library.events.preferences.InputPreferences.DialSensitivity dial_sensitivity;
        public java.lang.Long serial_number;
        public java.lang.Boolean use_gestures;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.preferences.InputPreferences message) {
            super(message);
            if (message != null) {
                this.serial_number = message.serial_number;
                this.use_gestures = message.use_gestures;
                this.dial_sensitivity = message.dial_sensitivity;
            }
        }

        public com.navdy.service.library.events.preferences.InputPreferences.Builder serial_number(java.lang.Long serial_number2) {
            this.serial_number = serial_number2;
            return this;
        }

        public com.navdy.service.library.events.preferences.InputPreferences.Builder use_gestures(java.lang.Boolean use_gestures2) {
            this.use_gestures = use_gestures2;
            return this;
        }

        public com.navdy.service.library.events.preferences.InputPreferences.Builder dial_sensitivity(com.navdy.service.library.events.preferences.InputPreferences.DialSensitivity dial_sensitivity2) {
            this.dial_sensitivity = dial_sensitivity2;
            return this;
        }

        public com.navdy.service.library.events.preferences.InputPreferences build() {
            checkRequiredFields();
            return new com.navdy.service.library.events.preferences.InputPreferences(this);
        }
    }

    public enum DialSensitivity implements com.squareup.wire.ProtoEnum {
        DIAL_SENSITIVITY_STANDARD(0),
        DIAL_SENSITIVITY_LOW(1);
        
        private final int value;

        private DialSensitivity(int value2) {
            this.value = value2;
        }

        public int getValue() {
            return this.value;
        }
    }

    public InputPreferences(java.lang.Long serial_number2, java.lang.Boolean use_gestures2, com.navdy.service.library.events.preferences.InputPreferences.DialSensitivity dial_sensitivity2) {
        this.serial_number = serial_number2;
        this.use_gestures = use_gestures2;
        this.dial_sensitivity = dial_sensitivity2;
    }

    private InputPreferences(com.navdy.service.library.events.preferences.InputPreferences.Builder builder) {
        this(builder.serial_number, builder.use_gestures, builder.dial_sensitivity);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.preferences.InputPreferences)) {
            return false;
        }
        com.navdy.service.library.events.preferences.InputPreferences o = (com.navdy.service.library.events.preferences.InputPreferences) other;
        if (!equals((java.lang.Object) this.serial_number, (java.lang.Object) o.serial_number) || !equals((java.lang.Object) this.use_gestures, (java.lang.Object) o.use_gestures) || !equals((java.lang.Object) this.dial_sensitivity, (java.lang.Object) o.dial_sensitivity)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2 = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = (this.serial_number != null ? this.serial_number.hashCode() : 0) * 37;
        if (this.use_gestures != null) {
            i = this.use_gestures.hashCode();
        } else {
            i = 0;
        }
        int i3 = (hashCode + i) * 37;
        if (this.dial_sensitivity != null) {
            i2 = this.dial_sensitivity.hashCode();
        }
        int result2 = i3 + i2;
        this.hashCode = result2;
        return result2;
    }
}
