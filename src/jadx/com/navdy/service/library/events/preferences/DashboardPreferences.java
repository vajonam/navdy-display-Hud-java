package com.navdy.service.library.events.preferences;

public final class DashboardPreferences extends com.squareup.wire.Message {
    public static final java.lang.String DEFAULT_LEFTGAUGEID = "";
    public static final com.navdy.service.library.events.preferences.MiddleGauge DEFAULT_MIDDLEGAUGE = com.navdy.service.library.events.preferences.MiddleGauge.SPEEDOMETER;
    public static final java.lang.String DEFAULT_RIGHTGAUGEID = "";
    public static final com.navdy.service.library.events.preferences.ScrollableSide DEFAULT_SCROLLABLESIDE = com.navdy.service.library.events.preferences.ScrollableSide.LEFT;
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(tag = 4, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String leftGaugeId;
    @com.squareup.wire.ProtoField(tag = 1, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.preferences.MiddleGauge middleGauge;
    @com.squareup.wire.ProtoField(tag = 3, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String rightGaugeId;
    @com.squareup.wire.ProtoField(tag = 2, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.preferences.ScrollableSide scrollableSide;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.preferences.DashboardPreferences> {
        public java.lang.String leftGaugeId;
        public com.navdy.service.library.events.preferences.MiddleGauge middleGauge;
        public java.lang.String rightGaugeId;
        public com.navdy.service.library.events.preferences.ScrollableSide scrollableSide;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.preferences.DashboardPreferences message) {
            super(message);
            if (message != null) {
                this.middleGauge = message.middleGauge;
                this.scrollableSide = message.scrollableSide;
                this.rightGaugeId = message.rightGaugeId;
                this.leftGaugeId = message.leftGaugeId;
            }
        }

        public com.navdy.service.library.events.preferences.DashboardPreferences.Builder middleGauge(com.navdy.service.library.events.preferences.MiddleGauge middleGauge2) {
            this.middleGauge = middleGauge2;
            return this;
        }

        public com.navdy.service.library.events.preferences.DashboardPreferences.Builder scrollableSide(com.navdy.service.library.events.preferences.ScrollableSide scrollableSide2) {
            this.scrollableSide = scrollableSide2;
            return this;
        }

        public com.navdy.service.library.events.preferences.DashboardPreferences.Builder rightGaugeId(java.lang.String rightGaugeId2) {
            this.rightGaugeId = rightGaugeId2;
            return this;
        }

        public com.navdy.service.library.events.preferences.DashboardPreferences.Builder leftGaugeId(java.lang.String leftGaugeId2) {
            this.leftGaugeId = leftGaugeId2;
            return this;
        }

        public com.navdy.service.library.events.preferences.DashboardPreferences build() {
            return new com.navdy.service.library.events.preferences.DashboardPreferences(this);
        }
    }

    public DashboardPreferences(com.navdy.service.library.events.preferences.MiddleGauge middleGauge2, com.navdy.service.library.events.preferences.ScrollableSide scrollableSide2, java.lang.String rightGaugeId2, java.lang.String leftGaugeId2) {
        this.middleGauge = middleGauge2;
        this.scrollableSide = scrollableSide2;
        this.rightGaugeId = rightGaugeId2;
        this.leftGaugeId = leftGaugeId2;
    }

    private DashboardPreferences(com.navdy.service.library.events.preferences.DashboardPreferences.Builder builder) {
        this(builder.middleGauge, builder.scrollableSide, builder.rightGaugeId, builder.leftGaugeId);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.preferences.DashboardPreferences)) {
            return false;
        }
        com.navdy.service.library.events.preferences.DashboardPreferences o = (com.navdy.service.library.events.preferences.DashboardPreferences) other;
        if (!equals((java.lang.Object) this.middleGauge, (java.lang.Object) o.middleGauge) || !equals((java.lang.Object) this.scrollableSide, (java.lang.Object) o.scrollableSide) || !equals((java.lang.Object) this.rightGaugeId, (java.lang.Object) o.rightGaugeId) || !equals((java.lang.Object) this.leftGaugeId, (java.lang.Object) o.leftGaugeId)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2;
        int i3 = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = (this.middleGauge != null ? this.middleGauge.hashCode() : 0) * 37;
        if (this.scrollableSide != null) {
            i = this.scrollableSide.hashCode();
        } else {
            i = 0;
        }
        int i4 = (hashCode + i) * 37;
        if (this.rightGaugeId != null) {
            i2 = this.rightGaugeId.hashCode();
        } else {
            i2 = 0;
        }
        int i5 = (i4 + i2) * 37;
        if (this.leftGaugeId != null) {
            i3 = this.leftGaugeId.hashCode();
        }
        int result2 = i5 + i3;
        this.hashCode = result2;
        return result2;
    }
}
