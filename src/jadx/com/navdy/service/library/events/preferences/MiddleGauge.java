package com.navdy.service.library.events.preferences;

public enum MiddleGauge implements com.squareup.wire.ProtoEnum {
    SPEEDOMETER(1),
    TACHOMETER(2);
    
    private final int value;

    private MiddleGauge(int value2) {
        this.value = value2;
    }

    public int getValue() {
        return this.value;
    }
}
