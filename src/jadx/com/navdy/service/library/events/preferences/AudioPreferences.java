package com.navdy.service.library.events.preferences;

public final class AudioPreferences extends com.squareup.wire.Message {
    public static final java.lang.Boolean DEFAULT_DRIVINGBEHAVIORALERTS = java.lang.Boolean.valueOf(true);
    public static final java.lang.Long DEFAULT_SERIAL_NUMBER = java.lang.Long.valueOf(0);
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(tag = 2, type = com.squareup.wire.Message.Datatype.BOOL)
    public final java.lang.Boolean drivingBehaviorAlerts;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 1, type = com.squareup.wire.Message.Datatype.INT64)
    public final java.lang.Long serial_number;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.preferences.AudioPreferences> {
        public java.lang.Boolean drivingBehaviorAlerts;
        public java.lang.Long serial_number;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.preferences.AudioPreferences message) {
            super(message);
            if (message != null) {
                this.serial_number = message.serial_number;
                this.drivingBehaviorAlerts = message.drivingBehaviorAlerts;
            }
        }

        public com.navdy.service.library.events.preferences.AudioPreferences.Builder serial_number(java.lang.Long serial_number2) {
            this.serial_number = serial_number2;
            return this;
        }

        public com.navdy.service.library.events.preferences.AudioPreferences.Builder drivingBehaviorAlerts(java.lang.Boolean drivingBehaviorAlerts2) {
            this.drivingBehaviorAlerts = drivingBehaviorAlerts2;
            return this;
        }

        public com.navdy.service.library.events.preferences.AudioPreferences build() {
            checkRequiredFields();
            return new com.navdy.service.library.events.preferences.AudioPreferences(this);
        }
    }

    public AudioPreferences(java.lang.Long serial_number2, java.lang.Boolean drivingBehaviorAlerts2) {
        this.serial_number = serial_number2;
        this.drivingBehaviorAlerts = drivingBehaviorAlerts2;
    }

    private AudioPreferences(com.navdy.service.library.events.preferences.AudioPreferences.Builder builder) {
        this(builder.serial_number, builder.drivingBehaviorAlerts);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.preferences.AudioPreferences)) {
            return false;
        }
        com.navdy.service.library.events.preferences.AudioPreferences o = (com.navdy.service.library.events.preferences.AudioPreferences) other;
        if (!equals((java.lang.Object) this.serial_number, (java.lang.Object) o.serial_number) || !equals((java.lang.Object) this.drivingBehaviorAlerts, (java.lang.Object) o.drivingBehaviorAlerts)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int result;
        int i = 0;
        int result2 = this.hashCode;
        if (result2 != 0) {
            return result2;
        }
        if (this.serial_number != null) {
            result = this.serial_number.hashCode();
        } else {
            result = 0;
        }
        int i2 = result * 37;
        if (this.drivingBehaviorAlerts != null) {
            i = this.drivingBehaviorAlerts.hashCode();
        }
        int result3 = i2 + i;
        this.hashCode = result3;
        return result3;
    }
}
