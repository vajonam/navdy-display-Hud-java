package com.navdy.service.library.events.preferences;

public final class DriverProfilePreferences extends com.squareup.wire.Message {
    public static final java.util.List<java.lang.String> DEFAULT_ADDITIONALLOCALES = java.util.Collections.emptyList();
    public static final java.lang.Boolean DEFAULT_AUTO_ON_ENABLED = java.lang.Boolean.valueOf(true);
    public static final java.lang.String DEFAULT_CAR_MAKE = "";
    public static final java.lang.String DEFAULT_CAR_MODEL = "";
    public static final java.lang.String DEFAULT_CAR_YEAR = "";
    public static final java.lang.String DEFAULT_DEVICE_NAME = "";
    public static final com.navdy.service.library.events.preferences.DriverProfilePreferences.DialLongPressAction DEFAULT_DIAL_LONG_PRESS_ACTION = com.navdy.service.library.events.preferences.DriverProfilePreferences.DialLongPressAction.DIAL_LONG_PRESS_VOICE_ASSISTANT;
    public static final com.navdy.service.library.events.preferences.DriverProfilePreferences.DisplayFormat DEFAULT_DISPLAY_FORMAT = com.navdy.service.library.events.preferences.DriverProfilePreferences.DisplayFormat.DISPLAY_FORMAT_NORMAL;
    public static final java.lang.String DEFAULT_DRIVER_EMAIL = "";
    public static final java.lang.String DEFAULT_DRIVER_NAME = "";
    public static final com.navdy.service.library.events.preferences.DriverProfilePreferences.FeatureMode DEFAULT_FEATURE_MODE = com.navdy.service.library.events.preferences.DriverProfilePreferences.FeatureMode.FEATURE_MODE_RELEASE;
    public static final java.lang.Boolean DEFAULT_LIMIT_BANDWIDTH = java.lang.Boolean.valueOf(false);
    public static final java.lang.String DEFAULT_LOCALE = "en_US";
    public static final java.lang.Long DEFAULT_OBDBLACKLISTLASTMODIFIED = java.lang.Long.valueOf(0);
    public static final com.navdy.service.library.events.preferences.DriverProfilePreferences.ObdScanSetting DEFAULT_OBDSCANSETTING = com.navdy.service.library.events.preferences.DriverProfilePreferences.ObdScanSetting.SCAN_DEFAULT_ON;
    public static final java.lang.String DEFAULT_PHOTO_CHECKSUM = "";
    public static final java.lang.Boolean DEFAULT_PROFILE_IS_PUBLIC = java.lang.Boolean.valueOf(false);
    public static final java.lang.Long DEFAULT_SERIAL_NUMBER = java.lang.Long.valueOf(0);
    public static final com.navdy.service.library.events.preferences.DriverProfilePreferences.UnitSystem DEFAULT_UNIT_SYSTEM = com.navdy.service.library.events.preferences.DriverProfilePreferences.UnitSystem.UNIT_SYSTEM_IMPERIAL;
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REPEATED, tag = 19, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.util.List<java.lang.String> additionalLocales;
    @com.squareup.wire.ProtoField(tag = 10, type = com.squareup.wire.Message.Datatype.BOOL)
    public final java.lang.Boolean auto_on_enabled;
    @com.squareup.wire.ProtoField(tag = 7, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String car_make;
    @com.squareup.wire.ProtoField(tag = 8, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String car_model;
    @com.squareup.wire.ProtoField(tag = 9, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String car_year;
    @com.squareup.wire.ProtoField(tag = 3, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String device_name;
    @com.squareup.wire.ProtoField(tag = 18, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.preferences.DriverProfilePreferences.DialLongPressAction dial_long_press_action;
    @com.squareup.wire.ProtoField(tag = 11, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.preferences.DriverProfilePreferences.DisplayFormat display_format;
    @com.squareup.wire.ProtoField(tag = 6, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String driver_email;
    @com.squareup.wire.ProtoField(tag = 2, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String driver_name;
    @com.squareup.wire.ProtoField(tag = 14, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.preferences.DriverProfilePreferences.FeatureMode feature_mode;
    @com.squareup.wire.ProtoField(tag = 16, type = com.squareup.wire.Message.Datatype.BOOL)
    public final java.lang.Boolean limit_bandwidth;
    @com.squareup.wire.ProtoField(tag = 12, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String locale;
    @com.squareup.wire.ProtoField(tag = 17, type = com.squareup.wire.Message.Datatype.INT64)
    public final java.lang.Long obdBlacklistLastModified;
    @com.squareup.wire.ProtoField(tag = 15, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.preferences.DriverProfilePreferences.ObdScanSetting obdScanSetting;
    @com.squareup.wire.ProtoField(tag = 5, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String photo_checksum;
    @com.squareup.wire.ProtoField(tag = 4, type = com.squareup.wire.Message.Datatype.BOOL)
    public final java.lang.Boolean profile_is_public;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 1, type = com.squareup.wire.Message.Datatype.INT64)
    public final java.lang.Long serial_number;
    @com.squareup.wire.ProtoField(tag = 13, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.preferences.DriverProfilePreferences.UnitSystem unit_system;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.preferences.DriverProfilePreferences> {
        public java.util.List<java.lang.String> additionalLocales;
        public java.lang.Boolean auto_on_enabled;
        public java.lang.String car_make;
        public java.lang.String car_model;
        public java.lang.String car_year;
        public java.lang.String device_name;
        public com.navdy.service.library.events.preferences.DriverProfilePreferences.DialLongPressAction dial_long_press_action;
        public com.navdy.service.library.events.preferences.DriverProfilePreferences.DisplayFormat display_format;
        public java.lang.String driver_email;
        public java.lang.String driver_name;
        public com.navdy.service.library.events.preferences.DriverProfilePreferences.FeatureMode feature_mode;
        public java.lang.Boolean limit_bandwidth;
        public java.lang.String locale;
        public java.lang.Long obdBlacklistLastModified;
        public com.navdy.service.library.events.preferences.DriverProfilePreferences.ObdScanSetting obdScanSetting;
        public java.lang.String photo_checksum;
        public java.lang.Boolean profile_is_public;
        public java.lang.Long serial_number;
        public com.navdy.service.library.events.preferences.DriverProfilePreferences.UnitSystem unit_system;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.preferences.DriverProfilePreferences message) {
            super(message);
            if (message != null) {
                this.serial_number = message.serial_number;
                this.driver_name = message.driver_name;
                this.device_name = message.device_name;
                this.profile_is_public = message.profile_is_public;
                this.photo_checksum = message.photo_checksum;
                this.driver_email = message.driver_email;
                this.car_make = message.car_make;
                this.car_model = message.car_model;
                this.car_year = message.car_year;
                this.auto_on_enabled = message.auto_on_enabled;
                this.display_format = message.display_format;
                this.locale = message.locale;
                this.unit_system = message.unit_system;
                this.feature_mode = message.feature_mode;
                this.obdScanSetting = message.obdScanSetting;
                this.limit_bandwidth = message.limit_bandwidth;
                this.obdBlacklistLastModified = message.obdBlacklistLastModified;
                this.dial_long_press_action = message.dial_long_press_action;
                this.additionalLocales = com.navdy.service.library.events.preferences.DriverProfilePreferences.copyOf(message.additionalLocales);
            }
        }

        public com.navdy.service.library.events.preferences.DriverProfilePreferences.Builder serial_number(java.lang.Long serial_number2) {
            this.serial_number = serial_number2;
            return this;
        }

        public com.navdy.service.library.events.preferences.DriverProfilePreferences.Builder driver_name(java.lang.String driver_name2) {
            this.driver_name = driver_name2;
            return this;
        }

        public com.navdy.service.library.events.preferences.DriverProfilePreferences.Builder device_name(java.lang.String device_name2) {
            this.device_name = device_name2;
            return this;
        }

        public com.navdy.service.library.events.preferences.DriverProfilePreferences.Builder profile_is_public(java.lang.Boolean profile_is_public2) {
            this.profile_is_public = profile_is_public2;
            return this;
        }

        public com.navdy.service.library.events.preferences.DriverProfilePreferences.Builder photo_checksum(java.lang.String photo_checksum2) {
            this.photo_checksum = photo_checksum2;
            return this;
        }

        public com.navdy.service.library.events.preferences.DriverProfilePreferences.Builder driver_email(java.lang.String driver_email2) {
            this.driver_email = driver_email2;
            return this;
        }

        public com.navdy.service.library.events.preferences.DriverProfilePreferences.Builder car_make(java.lang.String car_make2) {
            this.car_make = car_make2;
            return this;
        }

        public com.navdy.service.library.events.preferences.DriverProfilePreferences.Builder car_model(java.lang.String car_model2) {
            this.car_model = car_model2;
            return this;
        }

        public com.navdy.service.library.events.preferences.DriverProfilePreferences.Builder car_year(java.lang.String car_year2) {
            this.car_year = car_year2;
            return this;
        }

        public com.navdy.service.library.events.preferences.DriverProfilePreferences.Builder auto_on_enabled(java.lang.Boolean auto_on_enabled2) {
            this.auto_on_enabled = auto_on_enabled2;
            return this;
        }

        public com.navdy.service.library.events.preferences.DriverProfilePreferences.Builder display_format(com.navdy.service.library.events.preferences.DriverProfilePreferences.DisplayFormat display_format2) {
            this.display_format = display_format2;
            return this;
        }

        public com.navdy.service.library.events.preferences.DriverProfilePreferences.Builder locale(java.lang.String locale2) {
            this.locale = locale2;
            return this;
        }

        public com.navdy.service.library.events.preferences.DriverProfilePreferences.Builder unit_system(com.navdy.service.library.events.preferences.DriverProfilePreferences.UnitSystem unit_system2) {
            this.unit_system = unit_system2;
            return this;
        }

        public com.navdy.service.library.events.preferences.DriverProfilePreferences.Builder feature_mode(com.navdy.service.library.events.preferences.DriverProfilePreferences.FeatureMode feature_mode2) {
            this.feature_mode = feature_mode2;
            return this;
        }

        public com.navdy.service.library.events.preferences.DriverProfilePreferences.Builder obdScanSetting(com.navdy.service.library.events.preferences.DriverProfilePreferences.ObdScanSetting obdScanSetting2) {
            this.obdScanSetting = obdScanSetting2;
            return this;
        }

        public com.navdy.service.library.events.preferences.DriverProfilePreferences.Builder limit_bandwidth(java.lang.Boolean limit_bandwidth2) {
            this.limit_bandwidth = limit_bandwidth2;
            return this;
        }

        public com.navdy.service.library.events.preferences.DriverProfilePreferences.Builder obdBlacklistLastModified(java.lang.Long obdBlacklistLastModified2) {
            this.obdBlacklistLastModified = obdBlacklistLastModified2;
            return this;
        }

        public com.navdy.service.library.events.preferences.DriverProfilePreferences.Builder dial_long_press_action(com.navdy.service.library.events.preferences.DriverProfilePreferences.DialLongPressAction dial_long_press_action2) {
            this.dial_long_press_action = dial_long_press_action2;
            return this;
        }

        public com.navdy.service.library.events.preferences.DriverProfilePreferences.Builder additionalLocales(java.util.List<java.lang.String> additionalLocales2) {
            this.additionalLocales = checkForNulls(additionalLocales2);
            return this;
        }

        public com.navdy.service.library.events.preferences.DriverProfilePreferences build() {
            checkRequiredFields();
            return new com.navdy.service.library.events.preferences.DriverProfilePreferences(this);
        }
    }

    public enum DialLongPressAction implements com.squareup.wire.ProtoEnum {
        DIAL_LONG_PRESS_VOICE_ASSISTANT(0),
        DIAL_LONG_PRESS_PLACE_SEARCH(1);
        
        private final int value;

        private DialLongPressAction(int value2) {
            this.value = value2;
        }

        public int getValue() {
            return this.value;
        }
    }

    public enum DisplayFormat implements com.squareup.wire.ProtoEnum {
        DISPLAY_FORMAT_NORMAL(0),
        DISPLAY_FORMAT_COMPACT(1);
        
        private final int value;

        private DisplayFormat(int value2) {
            this.value = value2;
        }

        public int getValue() {
            return this.value;
        }
    }

    public enum FeatureMode implements com.squareup.wire.ProtoEnum {
        FEATURE_MODE_RELEASE(1),
        FEATURE_MODE_BETA(2),
        FEATURE_MODE_EXPERIMENTAL(3);
        
        private final int value;

        private FeatureMode(int value2) {
            this.value = value2;
        }

        public int getValue() {
            return this.value;
        }
    }

    public enum ObdScanSetting implements com.squareup.wire.ProtoEnum {
        SCAN_DEFAULT_ON(1),
        SCAN_ON(2),
        SCAN_OFF(3),
        SCAN_DEFAULT_OFF(4);
        
        private final int value;

        private ObdScanSetting(int value2) {
            this.value = value2;
        }

        public int getValue() {
            return this.value;
        }
    }

    public enum UnitSystem implements com.squareup.wire.ProtoEnum {
        UNIT_SYSTEM_METRIC(0),
        UNIT_SYSTEM_IMPERIAL(1);
        
        private final int value;

        private UnitSystem(int value2) {
            this.value = value2;
        }

        public int getValue() {
            return this.value;
        }
    }

    public DriverProfilePreferences(java.lang.Long serial_number2, java.lang.String driver_name2, java.lang.String device_name2, java.lang.Boolean profile_is_public2, java.lang.String photo_checksum2, java.lang.String driver_email2, java.lang.String car_make2, java.lang.String car_model2, java.lang.String car_year2, java.lang.Boolean auto_on_enabled2, com.navdy.service.library.events.preferences.DriverProfilePreferences.DisplayFormat display_format2, java.lang.String locale2, com.navdy.service.library.events.preferences.DriverProfilePreferences.UnitSystem unit_system2, com.navdy.service.library.events.preferences.DriverProfilePreferences.FeatureMode feature_mode2, com.navdy.service.library.events.preferences.DriverProfilePreferences.ObdScanSetting obdScanSetting2, java.lang.Boolean limit_bandwidth2, java.lang.Long obdBlacklistLastModified2, com.navdy.service.library.events.preferences.DriverProfilePreferences.DialLongPressAction dial_long_press_action2, java.util.List<java.lang.String> additionalLocales2) {
        this.serial_number = serial_number2;
        this.driver_name = driver_name2;
        this.device_name = device_name2;
        this.profile_is_public = profile_is_public2;
        this.photo_checksum = photo_checksum2;
        this.driver_email = driver_email2;
        this.car_make = car_make2;
        this.car_model = car_model2;
        this.car_year = car_year2;
        this.auto_on_enabled = auto_on_enabled2;
        this.display_format = display_format2;
        this.locale = locale2;
        this.unit_system = unit_system2;
        this.feature_mode = feature_mode2;
        this.obdScanSetting = obdScanSetting2;
        this.limit_bandwidth = limit_bandwidth2;
        this.obdBlacklistLastModified = obdBlacklistLastModified2;
        this.dial_long_press_action = dial_long_press_action2;
        this.additionalLocales = immutableCopyOf(additionalLocales2);
    }

    private DriverProfilePreferences(com.navdy.service.library.events.preferences.DriverProfilePreferences.Builder builder) {
        this(builder.serial_number, builder.driver_name, builder.device_name, builder.profile_is_public, builder.photo_checksum, builder.driver_email, builder.car_make, builder.car_model, builder.car_year, builder.auto_on_enabled, builder.display_format, builder.locale, builder.unit_system, builder.feature_mode, builder.obdScanSetting, builder.limit_bandwidth, builder.obdBlacklistLastModified, builder.dial_long_press_action, builder.additionalLocales);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.preferences.DriverProfilePreferences)) {
            return false;
        }
        com.navdy.service.library.events.preferences.DriverProfilePreferences o = (com.navdy.service.library.events.preferences.DriverProfilePreferences) other;
        if (!equals((java.lang.Object) this.serial_number, (java.lang.Object) o.serial_number) || !equals((java.lang.Object) this.driver_name, (java.lang.Object) o.driver_name) || !equals((java.lang.Object) this.device_name, (java.lang.Object) o.device_name) || !equals((java.lang.Object) this.profile_is_public, (java.lang.Object) o.profile_is_public) || !equals((java.lang.Object) this.photo_checksum, (java.lang.Object) o.photo_checksum) || !equals((java.lang.Object) this.driver_email, (java.lang.Object) o.driver_email) || !equals((java.lang.Object) this.car_make, (java.lang.Object) o.car_make) || !equals((java.lang.Object) this.car_model, (java.lang.Object) o.car_model) || !equals((java.lang.Object) this.car_year, (java.lang.Object) o.car_year) || !equals((java.lang.Object) this.auto_on_enabled, (java.lang.Object) o.auto_on_enabled) || !equals((java.lang.Object) this.display_format, (java.lang.Object) o.display_format) || !equals((java.lang.Object) this.locale, (java.lang.Object) o.locale) || !equals((java.lang.Object) this.unit_system, (java.lang.Object) o.unit_system) || !equals((java.lang.Object) this.feature_mode, (java.lang.Object) o.feature_mode) || !equals((java.lang.Object) this.obdScanSetting, (java.lang.Object) o.obdScanSetting) || !equals((java.lang.Object) this.limit_bandwidth, (java.lang.Object) o.limit_bandwidth) || !equals((java.lang.Object) this.obdBlacklistLastModified, (java.lang.Object) o.obdBlacklistLastModified) || !equals((java.lang.Object) this.dial_long_press_action, (java.lang.Object) o.dial_long_press_action) || !equals(this.additionalLocales, o.additionalLocales)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        int i8;
        int i9;
        int i10;
        int i11;
        int i12;
        int i13;
        int i14;
        int i15;
        int i16;
        int i17 = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = (this.serial_number != null ? this.serial_number.hashCode() : 0) * 37;
        if (this.driver_name != null) {
            i = this.driver_name.hashCode();
        } else {
            i = 0;
        }
        int i18 = (hashCode + i) * 37;
        if (this.device_name != null) {
            i2 = this.device_name.hashCode();
        } else {
            i2 = 0;
        }
        int i19 = (i18 + i2) * 37;
        if (this.profile_is_public != null) {
            i3 = this.profile_is_public.hashCode();
        } else {
            i3 = 0;
        }
        int i20 = (i19 + i3) * 37;
        if (this.photo_checksum != null) {
            i4 = this.photo_checksum.hashCode();
        } else {
            i4 = 0;
        }
        int i21 = (i20 + i4) * 37;
        if (this.driver_email != null) {
            i5 = this.driver_email.hashCode();
        } else {
            i5 = 0;
        }
        int i22 = (i21 + i5) * 37;
        if (this.car_make != null) {
            i6 = this.car_make.hashCode();
        } else {
            i6 = 0;
        }
        int i23 = (i22 + i6) * 37;
        if (this.car_model != null) {
            i7 = this.car_model.hashCode();
        } else {
            i7 = 0;
        }
        int i24 = (i23 + i7) * 37;
        if (this.car_year != null) {
            i8 = this.car_year.hashCode();
        } else {
            i8 = 0;
        }
        int i25 = (i24 + i8) * 37;
        if (this.auto_on_enabled != null) {
            i9 = this.auto_on_enabled.hashCode();
        } else {
            i9 = 0;
        }
        int i26 = (i25 + i9) * 37;
        if (this.display_format != null) {
            i10 = this.display_format.hashCode();
        } else {
            i10 = 0;
        }
        int i27 = (i26 + i10) * 37;
        if (this.locale != null) {
            i11 = this.locale.hashCode();
        } else {
            i11 = 0;
        }
        int i28 = (i27 + i11) * 37;
        if (this.unit_system != null) {
            i12 = this.unit_system.hashCode();
        } else {
            i12 = 0;
        }
        int i29 = (i28 + i12) * 37;
        if (this.feature_mode != null) {
            i13 = this.feature_mode.hashCode();
        } else {
            i13 = 0;
        }
        int i30 = (i29 + i13) * 37;
        if (this.obdScanSetting != null) {
            i14 = this.obdScanSetting.hashCode();
        } else {
            i14 = 0;
        }
        int i31 = (i30 + i14) * 37;
        if (this.limit_bandwidth != null) {
            i15 = this.limit_bandwidth.hashCode();
        } else {
            i15 = 0;
        }
        int i32 = (i31 + i15) * 37;
        if (this.obdBlacklistLastModified != null) {
            i16 = this.obdBlacklistLastModified.hashCode();
        } else {
            i16 = 0;
        }
        int i33 = (i32 + i16) * 37;
        if (this.dial_long_press_action != null) {
            i17 = this.dial_long_press_action.hashCode();
        }
        int result2 = ((i33 + i17) * 37) + (this.additionalLocales != null ? this.additionalLocales.hashCode() : 1);
        this.hashCode = result2;
        return result2;
    }
}
