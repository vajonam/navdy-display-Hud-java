package com.navdy.service.library.events.notification;

public final class NotificationsStatusUpdate extends com.squareup.wire.Message {
    public static final com.navdy.service.library.events.notification.NotificationsError DEFAULT_ERRORDETAILS = com.navdy.service.library.events.notification.NotificationsError.NOTIFICATIONS_ERROR_UNKNOWN;
    public static final com.navdy.service.library.events.notification.ServiceType DEFAULT_SERVICE = com.navdy.service.library.events.notification.ServiceType.SERVICE_ANCS;
    public static final com.navdy.service.library.events.notification.NotificationsState DEFAULT_STATE = com.navdy.service.library.events.notification.NotificationsState.NOTIFICATIONS_ENABLED;
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(tag = 3, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.notification.NotificationsError errorDetails;
    @com.squareup.wire.ProtoField(tag = 2, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.notification.ServiceType service;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 1, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.notification.NotificationsState state;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.notification.NotificationsStatusUpdate> {
        public com.navdy.service.library.events.notification.NotificationsError errorDetails;
        public com.navdy.service.library.events.notification.ServiceType service;
        public com.navdy.service.library.events.notification.NotificationsState state;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.notification.NotificationsStatusUpdate message) {
            super(message);
            if (message != null) {
                this.state = message.state;
                this.service = message.service;
                this.errorDetails = message.errorDetails;
            }
        }

        public com.navdy.service.library.events.notification.NotificationsStatusUpdate.Builder state(com.navdy.service.library.events.notification.NotificationsState state2) {
            this.state = state2;
            return this;
        }

        public com.navdy.service.library.events.notification.NotificationsStatusUpdate.Builder service(com.navdy.service.library.events.notification.ServiceType service2) {
            this.service = service2;
            return this;
        }

        public com.navdy.service.library.events.notification.NotificationsStatusUpdate.Builder errorDetails(com.navdy.service.library.events.notification.NotificationsError errorDetails2) {
            this.errorDetails = errorDetails2;
            return this;
        }

        public com.navdy.service.library.events.notification.NotificationsStatusUpdate build() {
            checkRequiredFields();
            return new com.navdy.service.library.events.notification.NotificationsStatusUpdate(this);
        }
    }

    public NotificationsStatusUpdate(com.navdy.service.library.events.notification.NotificationsState state2, com.navdy.service.library.events.notification.ServiceType service2, com.navdy.service.library.events.notification.NotificationsError errorDetails2) {
        this.state = state2;
        this.service = service2;
        this.errorDetails = errorDetails2;
    }

    private NotificationsStatusUpdate(com.navdy.service.library.events.notification.NotificationsStatusUpdate.Builder builder) {
        this(builder.state, builder.service, builder.errorDetails);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.notification.NotificationsStatusUpdate)) {
            return false;
        }
        com.navdy.service.library.events.notification.NotificationsStatusUpdate o = (com.navdy.service.library.events.notification.NotificationsStatusUpdate) other;
        if (!equals((java.lang.Object) this.state, (java.lang.Object) o.state) || !equals((java.lang.Object) this.service, (java.lang.Object) o.service) || !equals((java.lang.Object) this.errorDetails, (java.lang.Object) o.errorDetails)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2 = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = (this.state != null ? this.state.hashCode() : 0) * 37;
        if (this.service != null) {
            i = this.service.hashCode();
        } else {
            i = 0;
        }
        int i3 = (hashCode + i) * 37;
        if (this.errorDetails != null) {
            i2 = this.errorDetails.hashCode();
        }
        int result2 = i3 + i2;
        this.hashCode = result2;
        return result2;
    }
}
