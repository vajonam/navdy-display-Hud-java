package com.navdy.service.library.log;

public class LogcatAppender implements com.navdy.service.library.log.LogAppender {
    public void v(java.lang.String tag, java.lang.String msg) {
        android.util.Log.v(tag, msg);
    }

    public void v(java.lang.String tag, java.lang.String msg, java.lang.Throwable tr) {
        android.util.Log.v(tag, msg, tr);
    }

    public void d(java.lang.String tag, java.lang.String msg) {
        android.util.Log.d(tag, msg);
    }

    public void d(java.lang.String tag, java.lang.String msg, java.lang.Throwable tr) {
        android.util.Log.d(tag, msg, tr);
    }

    public void i(java.lang.String tag, java.lang.String msg) {
        android.util.Log.i(tag, msg);
    }

    public void i(java.lang.String tag, java.lang.String msg, java.lang.Throwable tr) {
        android.util.Log.i(tag, msg, tr);
    }

    public void w(java.lang.String tag, java.lang.String msg) {
        android.util.Log.w(tag, msg);
    }

    public void w(java.lang.String tag, java.lang.String msg, java.lang.Throwable tr) {
        android.util.Log.w(tag, msg, tr);
    }

    public void e(java.lang.String tag, java.lang.String msg) {
        android.util.Log.e(tag, msg);
    }

    public void e(java.lang.String tag, java.lang.String msg, java.lang.Throwable tr) {
        android.util.Log.e(tag, msg, tr);
    }

    public void flush() {
    }

    public void close() {
    }
}
