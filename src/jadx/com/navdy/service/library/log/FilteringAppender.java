package com.navdy.service.library.log;

public class FilteringAppender implements com.navdy.service.library.log.LogAppender {
    private com.navdy.service.library.log.LogAppender baseAppender;
    private com.navdy.service.library.log.Filter filter;

    public FilteringAppender(com.navdy.service.library.log.LogAppender baseAppender2, com.navdy.service.library.log.Filter filter2) {
        this.baseAppender = baseAppender2;
        this.filter = filter2;
    }

    public void v(java.lang.String tag, java.lang.String msg) {
        if (this.filter.matches(2, tag, msg)) {
            this.baseAppender.v(tag, msg);
        }
    }

    public void v(java.lang.String tag, java.lang.String msg, java.lang.Throwable tr) {
        if (this.filter.matches(2, tag, msg)) {
            this.baseAppender.v(tag, msg, tr);
        }
    }

    public void d(java.lang.String tag, java.lang.String msg) {
        if (this.filter.matches(3, tag, msg)) {
            this.baseAppender.d(tag, msg);
        }
    }

    public void d(java.lang.String tag, java.lang.String msg, java.lang.Throwable tr) {
        if (this.filter.matches(3, tag, msg)) {
            this.baseAppender.d(tag, msg, tr);
        }
    }

    public void i(java.lang.String tag, java.lang.String msg) {
        if (this.filter.matches(4, tag, msg)) {
            this.baseAppender.i(tag, msg);
        }
    }

    public void i(java.lang.String tag, java.lang.String msg, java.lang.Throwable tr) {
        if (this.filter.matches(4, tag, msg)) {
            this.baseAppender.i(tag, msg, tr);
        }
    }

    public void w(java.lang.String tag, java.lang.String msg) {
        if (this.filter.matches(5, tag, msg)) {
            this.baseAppender.w(tag, msg);
        }
    }

    public void w(java.lang.String tag, java.lang.String msg, java.lang.Throwable tr) {
        if (this.filter.matches(5, tag, msg)) {
            this.baseAppender.w(tag, msg, tr);
        }
    }

    public void e(java.lang.String tag, java.lang.String msg) {
        if (this.filter.matches(6, tag, msg)) {
            this.baseAppender.e(tag, msg);
        }
    }

    public void e(java.lang.String tag, java.lang.String msg, java.lang.Throwable tr) {
        if (this.filter.matches(6, tag, msg)) {
            this.baseAppender.e(tag, msg, tr);
        }
    }

    public void flush() {
        this.baseAppender.flush();
    }

    public void close() {
        this.baseAppender.close();
    }
}
