package com.navdy.hud.device.connection;

public class iAP2Link implements com.navdy.service.library.device.link.Link, com.navdy.hud.mfi.LinkLayer.PhysicalLayer, com.navdy.hud.mfi.iAPProcessor.StateListener, com.navdy.hud.mfi.IAPCommunicationsUpdateListener {
    public static com.navdy.hud.mfi.EASession proxyEASession;
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.device.connection.iAP2Link.class);
    private int bandwidthLevel = 1;
    private com.navdy.hud.device.connection.iAP2Link.ConnectedThread connectedThread;
    /* access modifiers changed from: private */
    public com.navdy.service.library.events.callcontrol.PhoneEvent currentPhoneEvent;
    private com.navdy.service.library.device.link.WriterThread.EventProcessor eventProcessor = new com.navdy.hud.device.connection.iAP2Link.Anon1();
    /* access modifiers changed from: private */
    public java.util.Map<com.navdy.service.library.events.NavdyEvent.MessageType, com.navdy.hud.device.connection.iAP2Link.NavdyEventProcessor> eventProcessors = new java.util.HashMap();
    /* access modifiers changed from: private */
    public com.navdy.hud.mfi.IAPCommunicationsManager iAPCommunicationsManager;
    /* access modifiers changed from: private */
    public com.navdy.hud.mfi.iAPProcessor iAPProcessor;
    /* access modifiers changed from: private */
    public com.navdy.service.library.events.callcontrol.CallAction lastCallActionRequested;
    /* access modifiers changed from: private */
    public com.navdy.hud.mfi.LinkLayer linkLayer;
    /* access modifiers changed from: private */
    public com.navdy.service.library.device.link.LinkListener linkListener;
    private final android.bluetooth.BluetoothAdapter mAdapter = android.bluetooth.BluetoothAdapter.getDefaultAdapter();
    private com.navdy.hud.device.connection.iAP2MusicHelper musicHelper;
    private com.navdy.hud.device.connection.iAP2Link.ProxyStream proxyStream = new com.navdy.hud.device.connection.iAP2Link.ProxyStream(this, null);
    private com.navdy.service.library.device.link.ReaderThread readerThread;
    /* access modifiers changed from: private */
    public com.squareup.wire.Wire wire;
    private com.navdy.service.library.device.link.WriterThread writerThread;

    class Anon1 implements com.navdy.service.library.device.link.WriterThread.EventProcessor {
        Anon1() {
        }

        public byte[] processEvent(byte[] eventData) {
            com.navdy.service.library.events.NavdyEvent.MessageType type = com.navdy.service.library.events.WireUtil.getEventType(eventData);
            if (type == null || !com.navdy.hud.device.connection.iAP2Link.this.eventProcessors.containsKey(type)) {
                return eventData;
            }
            boolean handled = false;
            try {
                handled = ((com.navdy.hud.device.connection.iAP2Link.NavdyEventProcessor) com.navdy.hud.device.connection.iAP2Link.this.eventProcessors.get(type)).processNavdyEvent((com.navdy.service.library.events.NavdyEvent) com.navdy.hud.device.connection.iAP2Link.this.wire.parseFrom(eventData, com.navdy.service.library.events.NavdyEvent.class));
            } catch (java.io.IOException e) {
                com.navdy.hud.device.connection.iAP2Link.sLogger.e("Error while parsing request", e);
            }
            if (handled) {
                return null;
            }
            return eventData;
        }
    }

    class Anon2 implements com.navdy.hud.device.connection.iAP2Link.NavdyEventProcessor {
        Anon2() {
        }

        public boolean processNavdyEvent(com.navdy.service.library.events.NavdyEvent event) {
            com.navdy.service.library.events.callcontrol.TelephonyRequest request = (com.navdy.service.library.events.callcontrol.TelephonyRequest) event.getExtension(com.navdy.service.library.events.Ext_NavdyEvent.telephonyRequest);
            com.navdy.hud.device.connection.iAP2Link.this.lastCallActionRequested = request.action;
            com.navdy.hud.device.connection.iAP2Link.this.sendMessageAsNavdyEvent(new com.navdy.service.library.events.callcontrol.TelephonyResponse(request.action, com.navdy.hud.device.connection.IAPMessageUtility.performActionForTelephonyRequest(request, com.navdy.hud.device.connection.iAP2Link.this.iAPCommunicationsManager) ? com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS : com.navdy.service.library.events.RequestStatus.REQUEST_NOT_AVAILABLE, null));
            return true;
        }
    }

    class Anon3 implements com.navdy.hud.device.connection.iAP2Link.NavdyEventProcessor {
        Anon3() {
        }

        public boolean processNavdyEvent(com.navdy.service.library.events.NavdyEvent event) {
            com.navdy.hud.device.connection.iAP2Link.this.sendMessageAsNavdyEvent(new com.navdy.service.library.events.callcontrol.PhoneStatusResponse(com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS, com.navdy.hud.device.connection.iAP2Link.this.currentPhoneEvent));
            return true;
        }
    }

    class Anon4 implements com.navdy.hud.device.connection.iAP2Link.NavdyEventProcessor {
        Anon4() {
        }

        public boolean processNavdyEvent(com.navdy.service.library.events.NavdyEvent event) {
            com.navdy.service.library.events.input.LaunchAppEvent launchAppRequest = (com.navdy.service.library.events.input.LaunchAppEvent) event.getExtension(com.navdy.service.library.events.Ext_NavdyEvent.launchAppEvent);
            if (!android.text.TextUtils.isEmpty(launchAppRequest.appBundleID)) {
                com.navdy.hud.device.connection.iAP2Link.this.iAPProcessor.launchApp(launchAppRequest.appBundleID, false);
            } else {
                com.navdy.hud.device.connection.iAP2Link.this.iAPProcessor.launchApp(false);
            }
            return true;
        }
    }

    class Anon5 implements com.navdy.hud.device.connection.iAP2Link.NavdyEventProcessor {
        Anon5() {
        }

        public boolean processNavdyEvent(com.navdy.service.library.events.NavdyEvent event) {
            return true;
        }
    }

    private class ConnectedThread extends com.navdy.service.library.device.link.IOThread {
        private final java.io.InputStream mmInStream;
        private final java.io.OutputStream mmOutStream;
        private final com.navdy.service.library.network.SocketAdapter mmSocket;

        public ConnectedThread(com.navdy.service.library.network.SocketAdapter socket) {
            com.navdy.hud.device.connection.iAP2Link.sLogger.d("create ConnectedThread");
            this.mmSocket = socket;
            java.io.InputStream tmpIn = null;
            java.io.OutputStream tmpOut = null;
            try {
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();
            } catch (java.io.IOException e) {
                com.navdy.hud.device.connection.iAP2Link.sLogger.e("temp sockets not created", e);
            }
            this.mmInStream = tmpIn;
            this.mmOutStream = tmpOut;
        }

        public void run() {
            android.bluetooth.BluetoothDevice device = com.navdy.hud.device.connection.iAP2Link.this.getRemoteDevice(this.mmSocket);
            com.navdy.hud.device.connection.iAP2Link.sLogger.i("BEGIN mConnectedThread - " + device.getAddress() + " name:" + device.getName());
            com.navdy.service.library.device.connection.Connection.DisconnectCause cause = com.navdy.service.library.device.connection.Connection.DisconnectCause.NORMAL;
            com.navdy.hud.device.connection.iAP2Link.this.linkListener.linkEstablished(com.navdy.service.library.device.connection.ConnectionType.BT_IAP2_LINK);
            com.navdy.hud.device.connection.iAP2Link.this.linkLayer.connectionStarted(device.getAddress(), device.getName());
            byte[] buffer = new byte[1024];
            while (!this.closing) {
                try {
                    com.navdy.hud.device.connection.iAP2Link.this.linkLayer.queue(new com.navdy.hud.mfi.LinkPacket(java.util.Arrays.copyOf(buffer, this.mmInStream.read(buffer))));
                } catch (java.io.IOException e) {
                    if (!this.closing) {
                        com.navdy.hud.device.connection.iAP2Link.sLogger.e("disconnected: " + e.getMessage());
                        cause = com.navdy.service.library.device.connection.Connection.DisconnectCause.ABORTED;
                    }
                }
            }
            com.navdy.service.library.util.IOUtils.closeStream(this.mmInStream);
            com.navdy.hud.device.connection.iAP2Link.this.linkLayer.connectionEnded();
            com.navdy.hud.device.connection.iAP2Link.this.linkListener.linkLost(com.navdy.service.library.device.connection.ConnectionType.BT_IAP2_LINK, cause);
        }

        public void write(byte[] buffer) {
            try {
                this.mmOutStream.write(buffer);
            } catch (java.io.IOException e) {
                com.navdy.hud.device.connection.iAP2Link.sLogger.e("Exception during write", e);
            }
        }

        public void cancel() {
            super.cancel();
            com.navdy.service.library.util.IOUtils.closeStream(this.mmOutStream);
            com.navdy.service.library.util.IOUtils.closeStream(this.mmSocket);
        }
    }

    interface NavdyEventProcessor {
        boolean processNavdyEvent(com.navdy.service.library.events.NavdyEvent navdyEvent);
    }

    private class ProxyStream extends java.io.OutputStream {
        private java.io.OutputStream outputStream;

        private ProxyStream() {
        }

        /* synthetic */ ProxyStream(com.navdy.hud.device.connection.iAP2Link x0, com.navdy.hud.device.connection.iAP2Link.Anon1 x1) {
            this();
        }

        public void setOutputStream(java.io.OutputStream stream) {
            this.outputStream = stream;
        }

        public void write(@android.support.annotation.NonNull byte[] buffer) throws java.io.IOException {
            if (this.outputStream != null) {
                this.outputStream.write(buffer);
            }
        }

        public void write(int oneByte) throws java.io.IOException {
            throw new java.io.IOException("Shouldn't be calling this method!");
        }
    }

    public iAP2Link(com.navdy.hud.mfi.IAPListener listener, android.content.Context context) {
        this.iAPProcessor = new com.navdy.hud.mfi.iAPProcessor(listener, context);
        this.iAPProcessor.setAccessoryName(getName());
        this.iAPCommunicationsManager = new com.navdy.hud.mfi.IAPCommunicationsManager(this.iAPProcessor);
        this.musicHelper = new com.navdy.hud.device.connection.iAP2MusicHelper(this, this.iAPProcessor);
        this.linkLayer = new com.navdy.hud.mfi.LinkLayer();
        this.iAPProcessor.connect(this.linkLayer);
        this.iAPProcessor.connect((com.navdy.hud.mfi.iAPProcessor.StateListener) this);
        this.linkLayer.connect(this.iAPProcessor);
        this.linkLayer.connect((com.navdy.hud.mfi.LinkLayer.PhysicalLayer) this);
        this.wire = new com.squareup.wire.Wire((java.lang.Class<?>[]) new java.lang.Class[]{com.navdy.service.library.events.Ext_NavdyEvent.class});
        addEventProcessors();
    }

    public java.lang.String getName() {
        return this.mAdapter.getName();
    }

    /* access modifiers changed from: 0000 */
    public void sendMessageAsNavdyEvent(com.squareup.wire.Message message) {
        if (message != null && this.linkListener != null) {
            this.linkListener.onNavdyEventReceived(com.navdy.service.library.events.NavdyEventUtil.eventFromMessage(message).toByteArray());
        }
    }

    /* access modifiers changed from: 0000 */
    public void addEventProcessor(com.navdy.service.library.events.NavdyEvent.MessageType type, com.navdy.hud.device.connection.iAP2Link.NavdyEventProcessor processor) {
        if (this.eventProcessors.containsKey(type)) {
            sLogger.w("Already have a processor for that type!");
        }
        this.eventProcessors.put(type, processor);
    }

    private void addEventProcessors() {
        this.eventProcessors.put(com.navdy.service.library.events.NavdyEvent.MessageType.TelephonyRequest, new com.navdy.hud.device.connection.iAP2Link.Anon2());
        this.eventProcessors.put(com.navdy.service.library.events.NavdyEvent.MessageType.PhoneStatusRequest, new com.navdy.hud.device.connection.iAP2Link.Anon3());
        this.eventProcessors.put(com.navdy.service.library.events.NavdyEvent.MessageType.LaunchAppEvent, new com.navdy.hud.device.connection.iAP2Link.Anon4());
        this.eventProcessors.put(com.navdy.service.library.events.NavdyEvent.MessageType.CallStateUpdateRequest, new com.navdy.hud.device.connection.iAP2Link.Anon5());
    }

    public void onCallStateUpdate(com.navdy.hud.mfi.CallStateUpdate callStateUpdate) {
        com.navdy.service.library.events.callcontrol.PhoneEvent event = com.navdy.hud.device.connection.IAPMessageUtility.getPhoneEventForCallStateUpdate(callStateUpdate, this.lastCallActionRequested);
        if (event != null) {
            this.currentPhoneEvent = event;
            this.lastCallActionRequested = null;
            sendMessageAsNavdyEvent(this.currentPhoneEvent);
        }
    }

    public void onCommunicationUpdate(com.navdy.hud.mfi.CommunicationUpdate communicationUpdate) {
        sLogger.e("CommunicationUpdate : " + communicationUpdate);
    }

    public boolean start(com.navdy.service.library.network.SocketAdapter socket, java.util.concurrent.LinkedBlockingDeque<com.navdy.service.library.device.link.EventRequest> outboundEventQueue, com.navdy.service.library.device.link.LinkListener listener) {
        if ((this.writerThread == null || this.writerThread.isClosing()) && (this.readerThread == null || this.readerThread.isClosing())) {
            this.linkListener = listener;
            this.connectedThread = new com.navdy.hud.device.connection.iAP2Link.ConnectedThread(socket);
            this.writerThread = new com.navdy.service.library.device.link.WriterThread(outboundEventQueue, this.proxyStream, this.eventProcessor);
            this.connectedThread.start();
            this.writerThread.start();
            return true;
        }
        throw new java.lang.IllegalStateException("Must stop threads before calling startLink");
    }

    public void setBandwidthLevel(int level) {
        this.bandwidthLevel = level;
        this.musicHelper.setBandwidthLevel(level);
    }

    public int getBandWidthLevel() {
        return this.bandwidthLevel;
    }

    public void close() {
        if (this.musicHelper != null) {
            this.musicHelper.close();
        }
        if (this.connectedThread != null) {
            this.connectedThread.cancel();
            this.connectedThread = null;
        }
        if (this.writerThread != null) {
            this.writerThread.cancel();
            this.writerThread = null;
        }
        if (this.readerThread != null) {
            this.readerThread.cancel();
            this.readerThread = null;
        }
    }

    private boolean startProtobufLink(com.navdy.service.library.network.SocketAdapter socket) {
        java.io.InputStream inputStream = null;
        try {
            inputStream = socket.getInputStream();
            java.io.OutputStream outputStream = socket.getOutputStream();
            this.readerThread = new com.navdy.service.library.device.link.ReaderThread(com.navdy.service.library.device.connection.ConnectionType.EA_PROTOBUF, inputStream, this.linkListener, false);
            this.proxyStream.setOutputStream(outputStream);
            this.readerThread.start();
            return true;
        } catch (java.io.IOException e) {
            com.navdy.service.library.util.IOUtils.closeStream(inputStream);
            this.readerThread = null;
            return false;
        }
    }

    private void stopProtobufLink() {
        if (this.readerThread != null) {
            this.readerThread.cancel();
            this.readerThread = null;
        }
        this.proxyStream.setOutputStream(null);
    }

    /* access modifiers changed from: private */
    public android.bluetooth.BluetoothDevice getRemoteDevice(com.navdy.service.library.network.SocketAdapter socket) {
        return this.mAdapter.getRemoteDevice(socket.getRemoteDevice().getBluetoothAddress());
    }

    public void onSessionStart(com.navdy.hud.mfi.EASession session) {
        java.lang.String proto = session.getProtocol();
        if (proto.equals(com.navdy.hud.mfi.iAPProcessor.PROTOCOL_V1)) {
            startProtobufLink(new com.navdy.hud.device.connection.EASessionSocketAdapter(session));
        } else if (proto.equals(com.navdy.hud.mfi.iAPProcessor.PROXY_V1)) {
            proxyEASession = session;
            if (this.linkListener != null) {
                this.linkListener.onNetworkLinkReady();
            }
        }
    }

    public void onSessionStop(com.navdy.hud.mfi.EASession session) {
        java.lang.String proto = session.getProtocol();
        if (proto.equals(com.navdy.hud.mfi.iAPProcessor.PROTOCOL_V1)) {
            stopProtobufLink();
        } else if (proto.equals(com.navdy.hud.mfi.iAPProcessor.PROXY_V1)) {
            proxyEASession = null;
        }
        session.close();
    }

    public void onError() {
        sLogger.e("Error in IAP session");
    }

    public void onReady() {
        try {
            this.iAPCommunicationsManager.setUpdatesListener(this);
            this.iAPCommunicationsManager.startUpdates();
            this.iAPCommunicationsManager.startCommunicationUpdates();
            this.musicHelper.onReady();
            this.iAPProcessor.startHIDSession();
        } catch (Throwable t) {
            sLogger.e("Error starting the call state updates ", t);
        }
    }

    public byte[] getLocalAddress() {
        return com.navdy.hud.mfi.Utils.parseMACAddress(this.mAdapter.getAddress());
    }

    public void queue(com.navdy.hud.mfi.LinkPacket pkt) {
        if (this.connectedThread != null) {
            this.connectedThread.write(pkt.data);
        }
    }
}
