package com.navdy.hud.device.connection;

public class EASessionSocketAdapter implements com.navdy.service.library.network.SocketAdapter {
    com.navdy.hud.mfi.EASession eaSession;
    private com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.device.connection.EASessionSocketAdapter.class);

    public EASessionSocketAdapter(com.navdy.hud.mfi.EASession eaSession2) {
        this.eaSession = eaSession2;
    }

    public void connect() throws java.io.IOException {
        if (!isConnected()) {
            throw new java.io.IOException("Can't create outbound EASession connections");
        }
        this.sLogger.d("Connecting to existing eaSession");
    }

    public void close() throws java.io.IOException {
        this.sLogger.d("closing connection");
        if (this.eaSession != null) {
            this.eaSession.close();
            this.eaSession = null;
        }
    }

    public java.io.InputStream getInputStream() throws java.io.IOException {
        if (this.eaSession != null) {
            return this.eaSession.getInputStream();
        }
        return null;
    }

    public java.io.OutputStream getOutputStream() throws java.io.IOException {
        if (this.eaSession != null) {
            return this.eaSession.getOutputStream();
        }
        return null;
    }

    public boolean isConnected() {
        return this.eaSession != null;
    }

    public com.navdy.service.library.device.NavdyDeviceId getRemoteDevice() {
        if (isConnected()) {
            return new com.navdy.service.library.device.NavdyDeviceId(com.navdy.service.library.device.NavdyDeviceId.Type.EA, this.eaSession.getMacAddress(), this.eaSession.getName());
        }
        return null;
    }
}
