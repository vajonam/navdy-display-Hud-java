package com.navdy.hud.device.connection;

class iAP2MusicHelper implements com.navdy.hud.mfi.IAPNowPlayingUpdateListener, com.navdy.hud.mfi.IAPFileTransferListener {
    private static final int LIMITED_BANDWIDTH_FILE_TRANSFER_LIMIT = 10000;
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.device.connection.iAP2MusicHelper.class);
    private boolean fileTransferCanceled = false;
    private final java.lang.Object helperStateLock = new java.lang.Object();
    private com.navdy.hud.mfi.IAPMusicManager iAPMusicManager;
    private com.navdy.hud.mfi.IAPFileTransferManager iapFileTransferManager;
    private long keyDownTime = 0;
    /* access modifiers changed from: private */
    public com.navdy.service.library.events.audio.MusicArtworkResponse lastMusicArtworkResponse;
    /* access modifiers changed from: private */
    public com.navdy.hud.device.connection.iAP2Link link;
    private int nowPlayingFileTransferIdentifier = -1;
    private android.util.LruCache<java.lang.Integer, com.navdy.service.library.events.audio.MusicTrackInfo> transferIdTrackInfoMap = new android.util.LruCache<>(5);
    private com.navdy.service.library.events.audio.MusicArtworkRequest waitingArtworkRequest = null;
    private com.navdy.hud.device.connection.iAP2MusicHelper.FileHolder waitingFile;
    private int waitingFileTransferIdentifier = -1;

    class Anon1 implements java.lang.Runnable {
        final /* synthetic */ byte[] val$data;
        final /* synthetic */ int val$fileTransferIdentifier;
        final /* synthetic */ com.navdy.service.library.events.audio.MusicTrackInfo val$musicTrackInfo;

        Anon1(int i, byte[] bArr, com.navdy.service.library.events.audio.MusicTrackInfo musicTrackInfo) {
            this.val$fileTransferIdentifier = i;
            this.val$data = bArr;
            this.val$musicTrackInfo = musicTrackInfo;
        }

        public void run() {
            com.navdy.hud.device.connection.iAP2MusicHelper.sLogger.d("onFileReceived task " + this.val$fileTransferIdentifier);
            okio.ByteString photo = null;
            if (this.val$data != null && this.val$data.length > 0) {
                try {
                    android.graphics.Bitmap bitmap = com.navdy.service.library.util.ScalingUtilities.decodeByteArray(this.val$data, 200, 200, com.navdy.service.library.util.ScalingUtilities.ScalingLogic.FIT);
                    if (bitmap != null) {
                        photo = okio.ByteString.of(com.navdy.service.library.util.ScalingUtilities.encodeByteArray(com.navdy.service.library.util.ScalingUtilities.createScaledBitmapAndRecycleOriginalIfScaled(bitmap, 200, 200, com.navdy.service.library.util.ScalingUtilities.ScalingLogic.FIT)));
                    } else {
                        com.navdy.hud.device.connection.iAP2MusicHelper.sLogger.e("Couldn't decode byte array to bitmap");
                    }
                } catch (java.lang.Exception e) {
                    com.navdy.hud.device.connection.iAP2MusicHelper.sLogger.e("Error updating the art work received ", e);
                }
            }
            com.navdy.hud.device.connection.iAP2MusicHelper.this.lastMusicArtworkResponse = new com.navdy.service.library.events.audio.MusicArtworkResponse.Builder().name(this.val$musicTrackInfo.name).album(this.val$musicTrackInfo.album).author(this.val$musicTrackInfo.author).photo(photo).build();
            com.navdy.hud.device.connection.iAP2MusicHelper.sLogger.d("sending artworkResponse " + com.navdy.hud.device.connection.iAP2MusicHelper.this.lastMusicArtworkResponse);
            com.navdy.hud.device.connection.iAP2MusicHelper.this.link.sendMessageAsNavdyEvent(com.navdy.hud.device.connection.iAP2MusicHelper.this.lastMusicArtworkResponse);
        }
    }

    class Anon2 implements com.navdy.hud.device.connection.iAP2Link.NavdyEventProcessor {
        Anon2() {
        }

        public boolean processNavdyEvent(com.navdy.service.library.events.NavdyEvent event) {
            com.navdy.hud.device.connection.iAP2MusicHelper.this.onMediaRemoteKeyEvent((com.navdy.service.library.events.input.MediaRemoteKeyEvent) event.getExtension(com.navdy.service.library.events.Ext_NavdyEvent.mediaRemoteKeyEvent));
            return true;
        }
    }

    class Anon3 implements com.navdy.hud.device.connection.iAP2Link.NavdyEventProcessor {
        Anon3() {
        }

        public boolean processNavdyEvent(com.navdy.service.library.events.NavdyEvent event) {
            return true;
        }
    }

    class Anon4 implements com.navdy.hud.device.connection.iAP2Link.NavdyEventProcessor {
        Anon4() {
        }

        public boolean processNavdyEvent(com.navdy.service.library.events.NavdyEvent event) {
            com.navdy.hud.device.connection.iAP2MusicHelper.sLogger.d("got PhotoUpdatesRequest");
            com.navdy.hud.device.connection.iAP2MusicHelper.this.onPhotoUpdatesRequest((com.navdy.service.library.events.photo.PhotoUpdatesRequest) event.getExtension(com.navdy.service.library.events.Ext_NavdyEvent.photoUpdateRequest));
            return true;
        }
    }

    class Anon5 implements com.navdy.hud.device.connection.iAP2Link.NavdyEventProcessor {
        Anon5() {
        }

        public boolean processNavdyEvent(com.navdy.service.library.events.NavdyEvent event) {
            return com.navdy.hud.device.connection.iAP2MusicHelper.this.onMusicArtworkRequest((com.navdy.service.library.events.audio.MusicArtworkRequest) event.getExtension(com.navdy.service.library.events.Ext_NavdyEvent.musicArtworkRequest));
        }
    }

    private class FileHolder {
        byte[] data = null;

        /* access modifiers changed from: 0000 */
        public int size() {
            if (this.data != null) {
                return this.data.length;
            }
            return 0;
        }

        FileHolder(byte[] data2) {
            this.data = data2;
        }
    }

    iAP2MusicHelper(com.navdy.hud.device.connection.iAP2Link link2, com.navdy.hud.mfi.iAPProcessor iAPProcessor) {
        this.link = link2;
        this.iapFileTransferManager = new com.navdy.hud.mfi.IAPFileTransferManager(iAPProcessor);
        iAPProcessor.connect((com.navdy.hud.mfi.IIAPFileTransferManager) this.iapFileTransferManager);
        this.iAPMusicManager = new com.navdy.hud.mfi.IAPMusicManager(iAPProcessor);
        this.iAPMusicManager.setNowPlayingUpdateListener(this);
        addEventProcessors();
    }

    private com.navdy.service.library.events.audio.MusicTrackInfo getLastMusicTrackInfo() {
        com.navdy.service.library.events.audio.MusicTrackInfo musicTrackInfo;
        synchronized (this.helperStateLock) {
            musicTrackInfo = (com.navdy.service.library.events.audio.MusicTrackInfo) this.transferIdTrackInfoMap.get(java.lang.Integer.valueOf(this.nowPlayingFileTransferIdentifier));
        }
        return musicTrackInfo;
    }

    public synchronized void onNowPlayingUpdate(com.navdy.hud.mfi.NowPlayingUpdate nowPlayingUpdate) {
        com.navdy.service.library.events.audio.MusicTrackInfo musicTrackInfo = com.navdy.hud.device.connection.IAPMessageUtility.getMusicTrackInfoForNowPlayingUpdate(nowPlayingUpdate);
        synchronized (this.helperStateLock) {
            this.nowPlayingFileTransferIdentifier = nowPlayingUpdate.mediaItemArtworkFileTransferIdentifier;
            sLogger.d("onNowPlayingUpdate nowPlayingFileTransferIdentifier: " + this.nowPlayingFileTransferIdentifier + ", musicTrackInfo: " + musicTrackInfo);
            if (musicTrackInfo != null && (!android.text.TextUtils.isEmpty(musicTrackInfo.name) || !android.text.TextUtils.isEmpty(musicTrackInfo.album) || !android.text.TextUtils.isEmpty(musicTrackInfo.author))) {
                this.transferIdTrackInfoMap.put(java.lang.Integer.valueOf(this.nowPlayingFileTransferIdentifier), musicTrackInfo);
            }
        }
        this.link.sendMessageAsNavdyEvent(musicTrackInfo);
    }

    public void onFileTransferSetup(int fileTransferIdentifier, long size) {
        synchronized (this.helperStateLock) {
            sLogger.d("onFileTransferSetup, fileTransferIdentifier: " + fileTransferIdentifier + ", size: " + size + ", waitingArtworkRequest: " + this.waitingArtworkRequest + ", nowPlayingFileTransferIdentifier: " + this.nowPlayingFileTransferIdentifier);
            com.navdy.service.library.events.audio.MusicTrackInfo musicTrackInfo = (com.navdy.service.library.events.audio.MusicTrackInfo) this.transferIdTrackInfoMap.get(java.lang.Integer.valueOf(fileTransferIdentifier));
            this.fileTransferCanceled = false;
            if (this.waitingArtworkRequest == null || !sameTrack(this.waitingArtworkRequest, musicTrackInfo) || this.nowPlayingFileTransferIdentifier != fileTransferIdentifier) {
                sLogger.d("onFileTransferSetup waiting for artwork request, waitingFileTransferIdentifier: " + fileTransferIdentifier);
                this.waitingFileTransferIdentifier = fileTransferIdentifier;
                this.waitingFile = null;
            } else {
                sLogger.d("onFileTransferSetup continuing file transfer");
                this.iapFileTransferManager.onFileTransferSetupResponse(this.nowPlayingFileTransferIdentifier, true);
            }
        }
    }

    public synchronized void onFileReceived(int fileTransferIdentifier, byte[] data) {
        synchronized (this.helperStateLock) {
            sLogger.d("onFileReceived: " + fileTransferIdentifier + " (" + (data != null ? data.length : 0) + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.CLOSE_BRACKET);
            if (this.transferIdTrackInfoMap.get(java.lang.Integer.valueOf(fileTransferIdentifier)) == null) {
                sLogger.e("onFileReceived: no music info for this file (yet?)");
                this.waitingFile = new com.navdy.hud.device.connection.iAP2MusicHelper.FileHolder(data);
            } else {
                com.navdy.service.library.events.audio.MusicTrackInfo musicTrackInfo = (com.navdy.service.library.events.audio.MusicTrackInfo) this.transferIdTrackInfoMap.remove(java.lang.Integer.valueOf(fileTransferIdentifier));
                sendArtworkResponse(fileTransferIdentifier, data, musicTrackInfo);
            }
        }
    }

    private void sendArtworkResponse(int fileTransferIdentifier, byte[] data, com.navdy.service.library.events.audio.MusicTrackInfo musicTrackInfo) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.device.connection.iAP2MusicHelper.Anon1(fileTransferIdentifier, data, musicTrackInfo), 1);
    }

    public synchronized void onFileTransferCancel(int fileTransferIdentifier) {
        synchronized (this.helperStateLock) {
            sLogger.d("onFileTransferCancel: " + fileTransferIdentifier);
            com.navdy.service.library.events.audio.MusicTrackInfo musicTrackInfo = (com.navdy.service.library.events.audio.MusicTrackInfo) this.transferIdTrackInfoMap.remove(java.lang.Integer.valueOf(fileTransferIdentifier));
            if (this.waitingArtworkRequest == null || !sameTrack(this.waitingArtworkRequest, musicTrackInfo) || this.nowPlayingFileTransferIdentifier != fileTransferIdentifier) {
                sLogger.d("File transferred canceled, waiting for the meta data to send empty response");
                this.fileTransferCanceled = true;
                this.waitingFileTransferIdentifier = fileTransferIdentifier;
                this.waitingFile = null;
            } else {
                sLogger.d("Sending response with null data");
                sendArtworkResponse(fileTransferIdentifier, null, musicTrackInfo);
            }
        }
    }

    private void addEventProcessors() {
        this.link.addEventProcessor(com.navdy.service.library.events.NavdyEvent.MessageType.MediaRemoteKeyEvent, new com.navdy.hud.device.connection.iAP2MusicHelper.Anon2());
        this.link.addEventProcessor(com.navdy.service.library.events.NavdyEvent.MessageType.NowPlayingUpdateRequest, new com.navdy.hud.device.connection.iAP2MusicHelper.Anon3());
        this.link.addEventProcessor(com.navdy.service.library.events.NavdyEvent.MessageType.PhotoUpdatesRequest, new com.navdy.hud.device.connection.iAP2MusicHelper.Anon4());
        this.link.addEventProcessor(com.navdy.service.library.events.NavdyEvent.MessageType.MusicArtworkRequest, new com.navdy.hud.device.connection.iAP2MusicHelper.Anon5());
    }

    /* access modifiers changed from: private */
    public synchronized boolean onMusicArtworkRequest(com.navdy.service.library.events.audio.MusicArtworkRequest musicArtworkRequest) {
        boolean z = true;
        synchronized (this) {
            com.navdy.service.library.events.audio.MusicTrackInfo musicTrackInfo = getLastMusicTrackInfo();
            sLogger.d("onMusicArtworkRequest: " + musicArtworkRequest + ", musicTrackInfo: " + musicTrackInfo);
            if (musicArtworkRequest == null || (android.text.TextUtils.isEmpty(musicArtworkRequest.name) && android.text.TextUtils.isEmpty(musicArtworkRequest.album) && android.text.TextUtils.isEmpty(musicArtworkRequest.author))) {
                z = false;
            } else {
                synchronized (this.helperStateLock) {
                    if (musicTrackInfo != null) {
                        if (sameTrack(musicArtworkRequest, musicTrackInfo) && this.nowPlayingFileTransferIdentifier == this.waitingFileTransferIdentifier) {
                            sLogger.d("onMusicArtworkRequest, waiting file: " + (this.waitingFile != null ? java.lang.Integer.valueOf(this.waitingFile.size()) : com.amazonaws.services.s3.internal.Constants.NULL_VERSION_ID));
                            if (this.waitingFile != null) {
                                sLogger.d("onMusicArtworkRequest using waiting file " + this.waitingFileTransferIdentifier);
                                sendArtworkResponse(this.waitingFileTransferIdentifier, this.waitingFile.data, musicTrackInfo);
                            } else if (this.fileTransferCanceled) {
                                sLogger.d("onMusicArtworkRequest, The file transfer has been canceled, send an empty artwork response");
                                sendArtworkResponse(this.waitingFileTransferIdentifier, null, musicTrackInfo);
                            } else {
                                sLogger.d("onMusicArtworkRequest continuing file transfer");
                                this.iapFileTransferManager.onFileTransferSetupResponse(this.nowPlayingFileTransferIdentifier, true);
                            }
                        }
                    }
                    this.waitingArtworkRequest = musicArtworkRequest;
                    sLogger.d("onMusicArtworkRequest waiting for file transfer, waitingArtworkRequest: " + this.waitingArtworkRequest);
                }
            }
        }
        return z;
    }

    /* access modifiers changed from: private */
    public void onPhotoUpdatesRequest(com.navdy.service.library.events.photo.PhotoUpdatesRequest request) {
        boolean postPhotoUpdates = (request.photoType != com.navdy.service.library.events.photo.PhotoType.PHOTO_ALBUM_ART || request.start == null) ? false : request.start.booleanValue();
        sLogger.d("onPhotoUpdatesRequest: " + postPhotoUpdates + ", " + request);
        if (postPhotoUpdates) {
            this.link.sendMessageAsNavdyEvent(this.lastMusicArtworkResponse);
        }
    }

    /* access modifiers changed from: private */
    public void onMediaRemoteKeyEvent(com.navdy.service.library.events.input.MediaRemoteKeyEvent request) {
        sLogger.d("(MFi) Media Key : " + request.key + " , " + request.action);
        switch (request.action) {
            case KEY_DOWN:
                this.keyDownTime = android.os.SystemClock.elapsedRealtime();
                this.iAPMusicManager.onKeyDown(request.key.ordinal());
                return;
            case KEY_UP:
                if (android.os.SystemClock.elapsedRealtime() - this.keyDownTime <= 500) {
                    this.iapFileTransferManager.cancel();
                }
                this.keyDownTime = 0;
                this.iAPMusicManager.onKeyUp(request.key.ordinal());
                return;
            default:
                return;
        }
    }

    private boolean sameTrack(com.navdy.service.library.events.audio.MusicArtworkRequest artworkRequest, com.navdy.service.library.events.audio.MusicTrackInfo musicTrackInfo) {
        return artworkRequest != null && musicTrackInfo != null && android.text.TextUtils.equals(artworkRequest.name, musicTrackInfo.name) && android.text.TextUtils.equals(artworkRequest.album, musicTrackInfo.album) && android.text.TextUtils.equals(artworkRequest.author, musicTrackInfo.author);
    }

    /* access modifiers changed from: 0000 */
    public void setBandwidthLevel(int level) {
        sLogger.d("Bandwidth level changing : " + (level <= 0 ? "LOW" : "NORMAL"));
        if (level <= 0) {
            this.iapFileTransferManager.setFileTransferLimit(10000);
        } else {
            this.iapFileTransferManager.setFileTransferLimit(1048576);
        }
    }

    /* access modifiers changed from: 0000 */
    public void close() {
        this.keyDownTime = 0;
    }

    /* access modifiers changed from: 0000 */
    public void onReady() {
        this.iapFileTransferManager.setFileTransferListener(this);
        this.iAPMusicManager.startNowPlayingUpdates();
    }
}
