package com.navdy.hud.device.connection;

public class IAPMessageUtility {
    private static final java.util.Map<com.navdy.hud.mfi.NowPlayingUpdate.PlaybackStatus, com.navdy.service.library.events.audio.MusicPlaybackState> PLAYBACK_STATES = new com.navdy.hud.device.connection.IAPMessageUtility.Anon1();
    private static final java.util.Map<com.navdy.hud.mfi.NowPlayingUpdate.PlaybackRepeat, com.navdy.service.library.events.audio.MusicRepeatMode> REPEAT_MODES = new com.navdy.hud.device.connection.IAPMessageUtility.Anon3();
    private static final java.util.Map<com.navdy.hud.mfi.NowPlayingUpdate.PlaybackShuffle, com.navdy.service.library.events.audio.MusicShuffleMode> SHUFFLE_MODES = new com.navdy.hud.device.connection.IAPMessageUtility.Anon2();

    static class Anon1 extends java.util.HashMap<com.navdy.hud.mfi.NowPlayingUpdate.PlaybackStatus, com.navdy.service.library.events.audio.MusicPlaybackState> {
        Anon1() {
            put(com.navdy.hud.mfi.NowPlayingUpdate.PlaybackStatus.Stopped, com.navdy.service.library.events.audio.MusicPlaybackState.PLAYBACK_STOPPED);
            put(com.navdy.hud.mfi.NowPlayingUpdate.PlaybackStatus.Playing, com.navdy.service.library.events.audio.MusicPlaybackState.PLAYBACK_PLAYING);
            put(com.navdy.hud.mfi.NowPlayingUpdate.PlaybackStatus.Paused, com.navdy.service.library.events.audio.MusicPlaybackState.PLAYBACK_PAUSED);
            put(com.navdy.hud.mfi.NowPlayingUpdate.PlaybackStatus.SeekForward, com.navdy.service.library.events.audio.MusicPlaybackState.PLAYBACK_FAST_FORWARDING);
            put(com.navdy.hud.mfi.NowPlayingUpdate.PlaybackStatus.SeekBackward, com.navdy.service.library.events.audio.MusicPlaybackState.PLAYBACK_REWINDING);
        }
    }

    static class Anon2 extends java.util.HashMap<com.navdy.hud.mfi.NowPlayingUpdate.PlaybackShuffle, com.navdy.service.library.events.audio.MusicShuffleMode> {
        Anon2() {
            put(com.navdy.hud.mfi.NowPlayingUpdate.PlaybackShuffle.Off, com.navdy.service.library.events.audio.MusicShuffleMode.MUSIC_SHUFFLE_MODE_OFF);
            put(com.navdy.hud.mfi.NowPlayingUpdate.PlaybackShuffle.Songs, com.navdy.service.library.events.audio.MusicShuffleMode.MUSIC_SHUFFLE_MODE_SONGS);
            put(com.navdy.hud.mfi.NowPlayingUpdate.PlaybackShuffle.Albums, com.navdy.service.library.events.audio.MusicShuffleMode.MUSIC_SHUFFLE_MODE_ALBUMS);
        }
    }

    static class Anon3 extends java.util.HashMap<com.navdy.hud.mfi.NowPlayingUpdate.PlaybackRepeat, com.navdy.service.library.events.audio.MusicRepeatMode> {
        Anon3() {
            put(com.navdy.hud.mfi.NowPlayingUpdate.PlaybackRepeat.Off, com.navdy.service.library.events.audio.MusicRepeatMode.MUSIC_REPEAT_MODE_OFF);
            put(com.navdy.hud.mfi.NowPlayingUpdate.PlaybackRepeat.One, com.navdy.service.library.events.audio.MusicRepeatMode.MUSIC_REPEAT_MODE_ONE);
            put(com.navdy.hud.mfi.NowPlayingUpdate.PlaybackRepeat.All, com.navdy.service.library.events.audio.MusicRepeatMode.MUSIC_REPEAT_MODE_ALL);
        }
    }

    public static com.navdy.service.library.events.callcontrol.PhoneEvent getPhoneEventForCallStateUpdate(com.navdy.hud.mfi.CallStateUpdate callstateUpdate, com.navdy.service.library.events.callcontrol.CallAction lastCallActionRequested) {
        if (callstateUpdate == null) {
            return null;
        }
        com.navdy.service.library.events.callcontrol.PhoneEvent.Builder builder = new com.navdy.service.library.events.callcontrol.PhoneEvent.Builder();
        switch (callstateUpdate.status) {
            case Disconnected:
                if (lastCallActionRequested != null && lastCallActionRequested == com.navdy.service.library.events.callcontrol.CallAction.CALL_DIAL) {
                    builder.status(com.navdy.service.library.events.callcontrol.PhoneStatus.PHONE_DISCONNECTING);
                    break;
                } else {
                    builder.status(com.navdy.service.library.events.callcontrol.PhoneStatus.PHONE_IDLE);
                    break;
                }
            case Sending:
                builder.status(com.navdy.service.library.events.callcontrol.PhoneStatus.PHONE_DIALING);
                break;
            case Ringing:
                builder.status(com.navdy.service.library.events.callcontrol.PhoneStatus.PHONE_RINGING);
                break;
            case Connecting:
                builder.status(com.navdy.service.library.events.callcontrol.PhoneStatus.PHONE_OFFHOOK);
                break;
            case Active:
                builder.status(com.navdy.service.library.events.callcontrol.PhoneStatus.PHONE_OFFHOOK);
                break;
            case Held:
                builder.status(com.navdy.service.library.events.callcontrol.PhoneStatus.PHONE_HELD);
                break;
            case Disconnecting:
                return null;
        }
        if (!android.text.TextUtils.isEmpty(callstateUpdate.displayName)) {
            builder.contact_name(callstateUpdate.displayName);
        }
        if (!android.text.TextUtils.isEmpty(callstateUpdate.remoteID)) {
            builder.number(callstateUpdate.remoteID);
        }
        if (!android.text.TextUtils.isEmpty(callstateUpdate.label)) {
            builder.label(callstateUpdate.label);
        }
        if (!android.text.TextUtils.isEmpty(callstateUpdate.callUUID)) {
            builder.callUUID(callstateUpdate.callUUID);
        }
        return builder.build();
    }

    public static boolean performActionForTelephonyRequest(com.navdy.service.library.events.callcontrol.TelephonyRequest request, com.navdy.hud.mfi.IAPCommunicationsManager manager) {
        if (request == null) {
            return false;
        }
        switch (request.action) {
            case CALL_ACCEPT:
                if (android.text.TextUtils.isEmpty(request.callUUID)) {
                    manager.acceptCall();
                    break;
                } else {
                    manager.acceptCall(request.callUUID);
                    break;
                }
            case CALL_END:
            case CALL_REJECT:
                if (android.text.TextUtils.isEmpty(request.callUUID)) {
                    manager.endCall();
                    break;
                } else {
                    manager.endCall(request.callUUID);
                    break;
                }
            case CALL_DIAL:
                if (!android.text.TextUtils.isEmpty(request.number)) {
                    manager.initiateDestinationCall(request.number);
                    break;
                }
                break;
            case CALL_MUTE:
                manager.mute();
                break;
            case CALL_UNMUTE:
                manager.unMute();
                break;
            default:
                return false;
        }
        return true;
    }

    public static com.navdy.service.library.events.audio.MusicTrackInfo getMusicTrackInfoForNowPlayingUpdate(com.navdy.hud.mfi.NowPlayingUpdate nowPlayingUpdate) {
        if (nowPlayingUpdate == null) {
            return null;
        }
        com.navdy.service.library.events.audio.MusicTrackInfo.Builder resultBuilder = new com.navdy.service.library.events.audio.MusicTrackInfo.Builder().playbackState((com.navdy.service.library.events.audio.MusicPlaybackState) PLAYBACK_STATES.get(nowPlayingUpdate.mPlaybackStatus)).name(nowPlayingUpdate.mediaItemTitle).album(nowPlayingUpdate.mediaItemAlbumTitle).author(nowPlayingUpdate.mediaItemArtist).duration(java.lang.Integer.valueOf((int) nowPlayingUpdate.mediaItemPlaybackDurationInMilliseconds)).currentPosition(java.lang.Integer.valueOf((int) nowPlayingUpdate.mPlaybackElapsedTimeMilliseconds)).shuffleMode((com.navdy.service.library.events.audio.MusicShuffleMode) SHUFFLE_MODES.get(nowPlayingUpdate.playbackShuffle)).repeatMode((com.navdy.service.library.events.audio.MusicRepeatMode) REPEAT_MODES.get(nowPlayingUpdate.playbackRepeat));
        if (nowPlayingUpdate.mediaItemPersistentIdentifier != null) {
            resultBuilder.trackId(nowPlayingUpdate.mediaItemPersistentIdentifier.toString());
        }
        resultBuilder.isPreviousAllowed(java.lang.Boolean.valueOf(true)).isNextAllowed(java.lang.Boolean.valueOf(true));
        return resultBuilder.build();
    }
}
