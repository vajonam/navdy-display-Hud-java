package com.navdy.hud.mfi;

public interface IIAPFileTransferManager {
    public static final int FILE_TRANSFER_SESSION_ID = 2;

    void cancel();

    void clear();

    long getFileTransferLimit();

    void onCanceled(int i);

    void onError(int i, java.lang.Throwable th);

    void onFileTransferSetupRequest(int i, long j);

    void onFileTransferSetupResponse(int i, boolean z);

    void onPaused(int i);

    void onSuccess(int i, byte[] bArr);

    void queue(byte[] bArr);

    void sendMessage(int i, byte[] bArr);

    void setFileTransferLimit(int i);
}
