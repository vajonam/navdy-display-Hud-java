package com.navdy.hud.mfi;

public class IAPFileTransferManager implements com.navdy.hud.mfi.IIAPFileTransferManager {
    public static final int DEFAULT_FILE_TRANSFER_LIMIT = 1048576;
    private static final int FILE_TRANSFER_IDENTIFIER_INDEX = 0;
    private static final int MAX_SESSIONS = 3;
    /* access modifiers changed from: private */
    public static final java.lang.String TAG = com.navdy.hud.mfi.IAPFileTransferManager.class.getSimpleName();
    private volatile int expectedFileTransferIdentifier = -1;
    private long mFileTransferLimit = com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient.VCARD_ATTR_URL;
    /* access modifiers changed from: private */
    public com.navdy.hud.mfi.IAPFileTransferListener mFileTransferListener;
    private com.navdy.hud.mfi.iAPProcessor mIapProcessor;
    private java.util.concurrent.ExecutorService mSerialExecutor = java.util.concurrent.Executors.newSingleThreadExecutor();
    /* access modifiers changed from: private */
    public java.util.HashMap<java.lang.Integer, com.navdy.hud.mfi.FileTransferSession> mSessionCache;

    class Anon1 implements java.lang.Runnable {
        final /* synthetic */ byte[] val$data;

        Anon1(byte[] bArr) {
            this.val$data = bArr;
        }

        public void run() {
            if (this.val$data != null) {
                int fileTransferIdentifier = this.val$data[0] & 255;
                com.navdy.hud.mfi.FileTransferSession transferSession = (com.navdy.hud.mfi.FileTransferSession) com.navdy.hud.mfi.IAPFileTransferManager.this.mSessionCache.get(java.lang.Integer.valueOf(fileTransferIdentifier));
                if (transferSession == null) {
                    android.util.Log.d(com.navdy.hud.mfi.IAPFileTransferManager.TAG, "Creating a new file transfer session " + fileTransferIdentifier);
                    transferSession = new com.navdy.hud.mfi.FileTransferSession(fileTransferIdentifier, com.navdy.hud.mfi.IAPFileTransferManager.this);
                    com.navdy.hud.mfi.IAPFileTransferManager.this.mSessionCache.put(java.lang.Integer.valueOf(fileTransferIdentifier), transferSession);
                }
                transferSession.bProcessFileTransferMessage(this.val$data);
            }
        }
    }

    class Anon2 implements java.lang.Runnable {
        Anon2() {
        }

        public void run() {
            for (com.navdy.hud.mfi.FileTransferSession session : com.navdy.hud.mfi.IAPFileTransferManager.this.mSessionCache.values()) {
                if (session != null) {
                    if (session.isActive()) {
                        android.util.Log.d(com.navdy.hud.mfi.IAPFileTransferManager.TAG, "Canceling, File transfer session , ID : " + session.mFileTransferIdentifier);
                        session.cancel();
                        if (com.navdy.hud.mfi.IAPFileTransferManager.this.mFileTransferListener != null) {
                            com.navdy.hud.mfi.IAPFileTransferManager.this.mFileTransferListener.onFileTransferCancel(session.mFileTransferIdentifier);
                        }
                    } else {
                        android.util.Log.d(com.navdy.hud.mfi.IAPFileTransferManager.TAG, "File transfer session not canceled , ID : " + session.mFileTransferIdentifier + ", Not active");
                    }
                }
            }
        }
    }

    public IAPFileTransferManager(com.navdy.hud.mfi.iAPProcessor processor) {
        this.mIapProcessor = processor;
        this.mSessionCache = new java.util.HashMap<>();
    }

    public void setFileTransferListener(com.navdy.hud.mfi.IAPFileTransferListener mFileTransferListener2) {
        this.mFileTransferListener = mFileTransferListener2;
    }

    public void queue(byte[] data) {
        this.mSerialExecutor.submit(new com.navdy.hud.mfi.IAPFileTransferManager.Anon1(data));
    }

    public void onSuccess(int fileTransferIdentifier, byte[] data) {
        if (this.mFileTransferListener != null) {
            android.util.Log.d(TAG, "File successfully transferred " + fileTransferIdentifier);
            this.mFileTransferListener.onFileReceived(fileTransferIdentifier, data);
        }
        this.mSessionCache.remove(java.lang.Integer.valueOf(fileTransferIdentifier));
    }

    public void onPaused(int fileTransferIdentifier) {
        android.util.Log.d(TAG, "File transfer paused");
    }

    public void onCanceled(int fileTransferIdentifier) {
        android.util.Log.d(TAG, "File transfer was cancelled " + fileTransferIdentifier);
        this.mSessionCache.remove(java.lang.Integer.valueOf(fileTransferIdentifier));
        if (this.mFileTransferListener != null) {
            this.mFileTransferListener.onFileTransferCancel(fileTransferIdentifier);
        }
    }

    public void sendMessage(int fileTransferIdentifier, byte[] message) {
        this.mIapProcessor.sendToDevice(2, message);
    }

    public void onError(int fileTransferIdentifier, java.lang.Throwable t) {
        android.util.Log.d(TAG, "Error in file transfer " + t);
        this.mSessionCache.remove(java.lang.Integer.valueOf(fileTransferIdentifier));
    }

    public void clear() {
        this.mSessionCache.clear();
    }

    public void setFileTransferLimit(int limit) {
        this.mFileTransferLimit = (long) limit;
    }

    public long getFileTransferLimit() {
        return this.mFileTransferLimit;
    }

    public void onFileTransferSetupRequest(int identifier, long size) {
        if (this.mFileTransferListener != null) {
            this.mFileTransferListener.onFileTransferSetup(identifier, size);
        }
    }

    public void onFileTransferSetupResponse(int identifier, boolean proceed) {
        com.navdy.hud.mfi.FileTransferSession transferSession = (com.navdy.hud.mfi.FileTransferSession) this.mSessionCache.get(java.lang.Integer.valueOf(identifier));
        if (transferSession == null) {
            return;
        }
        if (proceed) {
            transferSession.proceed();
        } else {
            transferSession.cancel();
        }
    }

    public void cancel() {
        this.mSerialExecutor.submit(new com.navdy.hud.mfi.IAPFileTransferManager.Anon2());
    }
}
