package com.navdy.hud.mfi;

public class IAPCommunicationsManager extends com.navdy.hud.mfi.IAPControlMessageProcessor {
    private static java.lang.String TAG = com.navdy.hud.mfi.IAPCommunicationsManager.class.getSimpleName();
    public static com.navdy.hud.mfi.iAPProcessor.iAPMessage[] mMessages = {com.navdy.hud.mfi.iAPProcessor.iAPMessage.CallStateUpdate, com.navdy.hud.mfi.iAPProcessor.iAPMessage.CommunicationUpdate};
    private com.navdy.hud.mfi.CallStateUpdate.Status callStatus;
    private java.lang.String callUUid;
    private com.navdy.hud.mfi.IAPCommunicationsUpdateListener mUpdatesListener;

    enum AcceptAction {
        HoldAndAccept,
        EndAndAccept
    }

    enum AcceptCall {
        AcceptAction,
        CallUUID
    }

    enum EndCall {
        EndAction,
        CallUUID
    }

    enum EndCallAction {
        EndDecline,
        EndAll
    }

    enum InitiateCall {
        Type,
        DestinationID,
        Service,
        AddressBookId
    }

    enum InitiateCallType {
        Destination,
        VoiceMail,
        Redial
    }

    enum MuteStatusUpdate {
        MuteStatus
    }

    public enum Service {
        Unknown,
        Telephony,
        FaceTimeAudio,
        FaceTimeVideo
    }

    enum StartCallStateUpdates {
        RemoteID,
        DisplayName,
        Status,
        Direction,
        CallUUID,
        Skip,
        AddressBookID,
        Label,
        Service,
        IsConferenced,
        ConferenceGroup,
        DisconnectReason
    }

    enum StartCommunicationUpdates {
        MuteStatus(9);
        
        int id;

        private StartCommunicationUpdates(int id2) {
            this.id = id2;
        }
    }

    public IAPCommunicationsManager(com.navdy.hud.mfi.iAPProcessor processor) {
        super(mMessages, processor);
    }

    public void setUpdatesListener(com.navdy.hud.mfi.IAPCommunicationsUpdateListener listener) {
        this.mUpdatesListener = listener;
    }

    public void bProcessControlMessage(com.navdy.hud.mfi.iAPProcessor.iAPMessage message, int session, byte[] data) {
        com.navdy.hud.mfi.iAPProcessor.IAP2Params params = com.navdy.hud.mfi.iAPProcessor.parse(data);
        if (message.id == com.navdy.hud.mfi.iAPProcessor.iAPMessage.CallStateUpdate.id) {
            com.navdy.hud.mfi.CallStateUpdate update = parseCallStateUpdate(params);
            this.callStatus = update.status;
            this.callUUid = update.callUUID;
            try {
                this.mUpdatesListener.onCallStateUpdate(update);
            } catch (Throwable t) {
                android.util.Log.d(TAG, "Bad call state update listener " + t);
            }
        } else if (message.id == com.navdy.hud.mfi.iAPProcessor.iAPMessage.CommunicationUpdate.id) {
            try {
                this.mUpdatesListener.onCommunicationUpdate(parseCommunicationUpdate(params));
            } catch (Throwable t2) {
                android.util.Log.d(TAG, "Bad call state update listener " + t2);
            }
        }
    }

    public void startUpdates() {
        com.navdy.hud.mfi.iAPProcessor.IAP2SessionMessage message = new com.navdy.hud.mfi.iAPProcessor.IAP2SessionMessage(com.navdy.hud.mfi.iAPProcessor.iAPMessage.StartCallStateUpdates);
        message.addNone((java.lang.Enum) com.navdy.hud.mfi.IAPCommunicationsManager.StartCallStateUpdates.RemoteID).addNone((java.lang.Enum) com.navdy.hud.mfi.IAPCommunicationsManager.StartCallStateUpdates.DisplayName).addNone((java.lang.Enum) com.navdy.hud.mfi.IAPCommunicationsManager.StartCallStateUpdates.Status).addNone((java.lang.Enum) com.navdy.hud.mfi.IAPCommunicationsManager.StartCallStateUpdates.Direction).addNone((java.lang.Enum) com.navdy.hud.mfi.IAPCommunicationsManager.StartCallStateUpdates.CallUUID).addNone((java.lang.Enum) com.navdy.hud.mfi.IAPCommunicationsManager.StartCallStateUpdates.AddressBookID).addNone((java.lang.Enum) com.navdy.hud.mfi.IAPCommunicationsManager.StartCallStateUpdates.Label).addNone((java.lang.Enum) com.navdy.hud.mfi.IAPCommunicationsManager.StartCallStateUpdates.Service).addNone((java.lang.Enum) com.navdy.hud.mfi.IAPCommunicationsManager.StartCallStateUpdates.IsConferenced).addNone((java.lang.Enum) com.navdy.hud.mfi.IAPCommunicationsManager.StartCallStateUpdates.ConferenceGroup).addNone((java.lang.Enum) com.navdy.hud.mfi.IAPCommunicationsManager.StartCallStateUpdates.DisconnectReason);
        this.miAPProcessor.sendControlMessage(message);
    }

    public void stopUpdates() {
        this.miAPProcessor.sendControlMessage(new com.navdy.hud.mfi.iAPProcessor.IAP2SessionMessage(com.navdy.hud.mfi.iAPProcessor.iAPMessage.StopCallStateUpdates));
    }

    public void startCommunicationUpdates() {
    }

    public void stopCommunicationUpdates() {
    }

    public static com.navdy.hud.mfi.CallStateUpdate parseCallStateUpdate(com.navdy.hud.mfi.iAPProcessor.IAP2Params updateParams) {
        com.navdy.hud.mfi.CallStateUpdate update = new com.navdy.hud.mfi.CallStateUpdate();
        if (updateParams.hasParam((java.lang.Enum) com.navdy.hud.mfi.IAPCommunicationsManager.StartCallStateUpdates.RemoteID)) {
            update.remoteID = updateParams.getUTF8((java.lang.Enum) com.navdy.hud.mfi.IAPCommunicationsManager.StartCallStateUpdates.RemoteID);
        }
        if (updateParams.hasParam((java.lang.Enum) com.navdy.hud.mfi.IAPCommunicationsManager.StartCallStateUpdates.DisplayName)) {
            update.displayName = updateParams.getUTF8((java.lang.Enum) com.navdy.hud.mfi.IAPCommunicationsManager.StartCallStateUpdates.DisplayName);
        }
        if (updateParams.hasParam((java.lang.Enum) com.navdy.hud.mfi.IAPCommunicationsManager.StartCallStateUpdates.Status)) {
            update.status = (com.navdy.hud.mfi.CallStateUpdate.Status) updateParams.getEnum(com.navdy.hud.mfi.CallStateUpdate.Status.class, (java.lang.Enum) com.navdy.hud.mfi.IAPCommunicationsManager.StartCallStateUpdates.Status);
        }
        if (updateParams.hasParam((java.lang.Enum) com.navdy.hud.mfi.IAPCommunicationsManager.StartCallStateUpdates.Direction)) {
            update.direction = (com.navdy.hud.mfi.CallStateUpdate.Direction) updateParams.getEnum(com.navdy.hud.mfi.CallStateUpdate.Direction.class, (java.lang.Enum) com.navdy.hud.mfi.IAPCommunicationsManager.StartCallStateUpdates.Direction);
        }
        if (updateParams.hasParam((java.lang.Enum) com.navdy.hud.mfi.IAPCommunicationsManager.StartCallStateUpdates.CallUUID)) {
            update.callUUID = updateParams.getUTF8((java.lang.Enum) com.navdy.hud.mfi.IAPCommunicationsManager.StartCallStateUpdates.CallUUID);
        }
        if (updateParams.hasParam((java.lang.Enum) com.navdy.hud.mfi.IAPCommunicationsManager.StartCallStateUpdates.AddressBookID)) {
            update.addressBookID = updateParams.getUTF8((java.lang.Enum) com.navdy.hud.mfi.IAPCommunicationsManager.StartCallStateUpdates.AddressBookID);
        }
        if (updateParams.hasParam((java.lang.Enum) com.navdy.hud.mfi.IAPCommunicationsManager.StartCallStateUpdates.Label)) {
            update.label = updateParams.getUTF8((java.lang.Enum) com.navdy.hud.mfi.IAPCommunicationsManager.StartCallStateUpdates.Label);
        }
        if (updateParams.hasParam((java.lang.Enum) com.navdy.hud.mfi.IAPCommunicationsManager.StartCallStateUpdates.Service)) {
            update.service = (com.navdy.hud.mfi.IAPCommunicationsManager.Service) updateParams.getEnum(com.navdy.hud.mfi.IAPCommunicationsManager.Service.class, (java.lang.Enum) com.navdy.hud.mfi.IAPCommunicationsManager.StartCallStateUpdates.Service);
        }
        if (updateParams.hasParam((java.lang.Enum) com.navdy.hud.mfi.IAPCommunicationsManager.StartCallStateUpdates.IsConferenced)) {
            update.isConferenced = updateParams.getBoolean((java.lang.Enum) com.navdy.hud.mfi.IAPCommunicationsManager.StartCallStateUpdates.IsConferenced);
        }
        if (updateParams.hasParam((java.lang.Enum) com.navdy.hud.mfi.IAPCommunicationsManager.StartCallStateUpdates.ConferenceGroup)) {
            update.conferenceGroup = updateParams.getUInt8((java.lang.Enum) com.navdy.hud.mfi.IAPCommunicationsManager.StartCallStateUpdates.ConferenceGroup);
        }
        if (updateParams.hasParam((java.lang.Enum) com.navdy.hud.mfi.IAPCommunicationsManager.StartCallStateUpdates.DisconnectReason)) {
            update.disconnectReason = (com.navdy.hud.mfi.CallStateUpdate.DisconnectReason) updateParams.getEnum(com.navdy.hud.mfi.CallStateUpdate.DisconnectReason.class, (java.lang.Enum) com.navdy.hud.mfi.IAPCommunicationsManager.StartCallStateUpdates.DisconnectReason);
        }
        return update;
    }

    public static com.navdy.hud.mfi.CommunicationUpdate parseCommunicationUpdate(com.navdy.hud.mfi.iAPProcessor.IAP2Params updateParams) {
        com.navdy.hud.mfi.CommunicationUpdate communicationUpdate = new com.navdy.hud.mfi.CommunicationUpdate();
        if (updateParams.hasParam(com.navdy.hud.mfi.IAPCommunicationsManager.StartCommunicationUpdates.MuteStatus.id)) {
            communicationUpdate.muteStatus = updateParams.getBoolean(com.navdy.hud.mfi.IAPCommunicationsManager.StartCommunicationUpdates.MuteStatus.id);
        }
        return communicationUpdate;
    }

    public void initiateDestinationCall(java.lang.String number) {
        com.navdy.hud.mfi.iAPProcessor.IAP2SessionMessage message = new com.navdy.hud.mfi.iAPProcessor.IAP2SessionMessage(com.navdy.hud.mfi.iAPProcessor.iAPMessage.InitiateCall);
        message.addEnum((java.lang.Enum) com.navdy.hud.mfi.IAPCommunicationsManager.InitiateCall.Type, (java.lang.Enum) com.navdy.hud.mfi.IAPCommunicationsManager.InitiateCallType.Destination);
        message.addString((java.lang.Enum) com.navdy.hud.mfi.IAPCommunicationsManager.InitiateCall.DestinationID, number);
        message.addEnum((java.lang.Enum) com.navdy.hud.mfi.IAPCommunicationsManager.InitiateCall.Service, (java.lang.Enum) com.navdy.hud.mfi.IAPCommunicationsManager.Service.Telephony);
        this.miAPProcessor.sendControlMessage(message);
    }

    public void redial() {
        com.navdy.hud.mfi.iAPProcessor.IAP2SessionMessage message = new com.navdy.hud.mfi.iAPProcessor.IAP2SessionMessage(com.navdy.hud.mfi.iAPProcessor.iAPMessage.InitiateCall);
        message.addEnum((java.lang.Enum) com.navdy.hud.mfi.IAPCommunicationsManager.InitiateCall.Type, (java.lang.Enum) com.navdy.hud.mfi.IAPCommunicationsManager.InitiateCallType.Redial);
        this.miAPProcessor.sendControlMessage(message);
    }

    public void callVoiceMail() {
        com.navdy.hud.mfi.iAPProcessor.IAP2SessionMessage message = new com.navdy.hud.mfi.iAPProcessor.IAP2SessionMessage(com.navdy.hud.mfi.iAPProcessor.iAPMessage.InitiateCall);
        message.addEnum((java.lang.Enum) com.navdy.hud.mfi.IAPCommunicationsManager.InitiateCall.Type, (java.lang.Enum) com.navdy.hud.mfi.IAPCommunicationsManager.InitiateCallType.VoiceMail);
        this.miAPProcessor.sendControlMessage(message);
    }

    public void acceptCall(java.lang.String callUUid2) {
        if (!android.text.TextUtils.isEmpty(callUUid2)) {
            com.navdy.hud.mfi.iAPProcessor.IAP2SessionMessage message = new com.navdy.hud.mfi.iAPProcessor.IAP2SessionMessage(com.navdy.hud.mfi.iAPProcessor.iAPMessage.AcceptCall);
            message.addEnum((java.lang.Enum) com.navdy.hud.mfi.IAPCommunicationsManager.AcceptCall.AcceptAction, (java.lang.Enum) com.navdy.hud.mfi.IAPCommunicationsManager.AcceptAction.HoldAndAccept);
            message.addString((java.lang.Enum) com.navdy.hud.mfi.IAPCommunicationsManager.AcceptCall.CallUUID, callUUid2);
            this.miAPProcessor.sendControlMessage(message);
        }
    }

    public void acceptCall() {
        if (!android.text.TextUtils.isEmpty(this.callUUid)) {
            com.navdy.hud.mfi.iAPProcessor.IAP2SessionMessage message = new com.navdy.hud.mfi.iAPProcessor.IAP2SessionMessage(com.navdy.hud.mfi.iAPProcessor.iAPMessage.AcceptCall);
            message.addEnum((java.lang.Enum) com.navdy.hud.mfi.IAPCommunicationsManager.AcceptCall.AcceptAction, (java.lang.Enum) com.navdy.hud.mfi.IAPCommunicationsManager.AcceptAction.HoldAndAccept);
            message.addString((java.lang.Enum) com.navdy.hud.mfi.IAPCommunicationsManager.AcceptCall.CallUUID, this.callUUid);
            this.miAPProcessor.sendControlMessage(message);
        }
    }

    public void endCall() {
        if (!android.text.TextUtils.isEmpty(this.callUUid)) {
            com.navdy.hud.mfi.iAPProcessor.IAP2SessionMessage message = new com.navdy.hud.mfi.iAPProcessor.IAP2SessionMessage(com.navdy.hud.mfi.iAPProcessor.iAPMessage.EndCall);
            message.addEnum((java.lang.Enum) com.navdy.hud.mfi.IAPCommunicationsManager.EndCall.EndAction, (java.lang.Enum) com.navdy.hud.mfi.IAPCommunicationsManager.EndCallAction.EndDecline);
            message.addString((java.lang.Enum) com.navdy.hud.mfi.IAPCommunicationsManager.EndCall.CallUUID, this.callUUid);
            this.miAPProcessor.sendControlMessage(message);
        }
    }

    public void endCall(java.lang.String callUUid2) {
        if (!android.text.TextUtils.isEmpty(callUUid2)) {
            com.navdy.hud.mfi.iAPProcessor.IAP2SessionMessage message = new com.navdy.hud.mfi.iAPProcessor.IAP2SessionMessage(com.navdy.hud.mfi.iAPProcessor.iAPMessage.EndCall);
            message.addEnum((java.lang.Enum) com.navdy.hud.mfi.IAPCommunicationsManager.EndCall.EndAction, (java.lang.Enum) com.navdy.hud.mfi.IAPCommunicationsManager.EndCallAction.EndDecline);
            message.addString((java.lang.Enum) com.navdy.hud.mfi.IAPCommunicationsManager.EndCall.CallUUID, callUUid2);
            this.miAPProcessor.sendControlMessage(message);
        }
    }

    public void mute() {
        com.navdy.hud.mfi.iAPProcessor.IAP2SessionMessage message = new com.navdy.hud.mfi.iAPProcessor.IAP2SessionMessage(com.navdy.hud.mfi.iAPProcessor.iAPMessage.MuteStatusUpdate);
        message.addBoolean((java.lang.Enum) com.navdy.hud.mfi.IAPCommunicationsManager.MuteStatusUpdate.MuteStatus, true);
        this.miAPProcessor.sendControlMessage(message);
    }

    public void unMute() {
        com.navdy.hud.mfi.iAPProcessor.IAP2SessionMessage message = new com.navdy.hud.mfi.iAPProcessor.IAP2SessionMessage(com.navdy.hud.mfi.iAPProcessor.iAPMessage.MuteStatusUpdate);
        message.addBoolean((java.lang.Enum) com.navdy.hud.mfi.IAPCommunicationsManager.MuteStatusUpdate.MuteStatus, false);
        this.miAPProcessor.sendControlMessage(message);
    }
}
