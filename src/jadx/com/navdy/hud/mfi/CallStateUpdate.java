package com.navdy.hud.mfi;

public class CallStateUpdate {
    public java.lang.String addressBookID;
    public java.lang.String callUUID;
    public int conferenceGroup;
    public com.navdy.hud.mfi.CallStateUpdate.Direction direction = com.navdy.hud.mfi.CallStateUpdate.Direction.Unknown;
    public com.navdy.hud.mfi.CallStateUpdate.DisconnectReason disconnectReason = com.navdy.hud.mfi.CallStateUpdate.DisconnectReason.Ended;
    public java.lang.String displayName;
    public boolean isConferenced;
    public java.lang.String label;
    public java.lang.String remoteID;
    public com.navdy.hud.mfi.IAPCommunicationsManager.Service service = com.navdy.hud.mfi.IAPCommunicationsManager.Service.Unknown;
    public com.navdy.hud.mfi.CallStateUpdate.Status status = com.navdy.hud.mfi.CallStateUpdate.Status.Disconnected;

    public enum Direction {
        Unknown,
        Incoming,
        Outgoing
    }

    public enum DisconnectReason {
        Ended,
        Declined,
        Failed
    }

    public enum Status {
        Disconnected,
        Sending,
        Ringing,
        Connecting,
        Active,
        Held,
        Disconnecting
    }
}
