package com.navdy.hud.mfi;

public interface IAPListener {
    void onCoprocessorStatusCheckFailed(java.lang.String str, java.lang.String str2, java.lang.String str3);

    void onDeviceAuthenticationSuccess(int i);
}
