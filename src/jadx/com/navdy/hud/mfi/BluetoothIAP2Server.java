package com.navdy.hud.mfi;

public class BluetoothIAP2Server implements com.navdy.hud.mfi.LinkLayer.PhysicalLayer {
    public static final java.util.UUID ACCESSORY_IAP2 = java.util.UUID.fromString("00000000-deca-fade-deca-deafdecacaff");
    public static final java.util.UUID DEVICE_IAP2 = java.util.UUID.fromString("00000000-deca-fade-deca-deafdecacafe");
    private static final java.lang.String SDP_NAME = "Wireless iAP";
    public static final int STATE_CONNECTED = 3;
    public static final int STATE_CONNECTING = 2;
    public static final int STATE_LISTEN = 1;
    public static final int STATE_NONE = 0;
    private static final java.lang.String TAG = "BluetoothIAP2Server";
    /* access modifiers changed from: private */
    public com.navdy.hud.mfi.LinkLayer linkLayer;
    private com.navdy.hud.mfi.BluetoothIAP2Server.AcceptThread mAcceptThread;
    /* access modifiers changed from: private */
    public final android.bluetooth.BluetoothAdapter mAdapter = android.bluetooth.BluetoothAdapter.getDefaultAdapter();
    /* access modifiers changed from: private */
    public com.navdy.hud.mfi.BluetoothIAP2Server.ConnectThread mConnectThread;
    private com.navdy.hud.mfi.BluetoothIAP2Server.ConnectedThread mConnectedThread;
    private final android.os.Handler mHandler;
    /* access modifiers changed from: private */
    public int mState = 0;

    private class AcceptThread extends java.lang.Thread {
        private final android.bluetooth.BluetoothServerSocket mmServerSocket;

        public AcceptThread() {
            android.bluetooth.BluetoothServerSocket tmp = null;
            try {
                tmp = com.navdy.hud.mfi.BluetoothIAP2Server.this.mAdapter.listenUsingRfcommWithServiceRecord(com.navdy.hud.mfi.BluetoothIAP2Server.SDP_NAME, com.navdy.hud.mfi.BluetoothIAP2Server.ACCESSORY_IAP2);
            } catch (java.io.IOException e) {
                android.util.Log.e(com.navdy.hud.mfi.BluetoothIAP2Server.TAG, "Socket listen() failed", e);
            }
            this.mmServerSocket = tmp;
        }

        public void run() {
            android.util.Log.d(com.navdy.hud.mfi.BluetoothIAP2Server.TAG, "Socket BEGIN mAcceptThread" + this);
            setName("AcceptThread");
            if (this.mmServerSocket == null) {
                android.util.Log.e(com.navdy.hud.mfi.BluetoothIAP2Server.TAG, "No socket (maybe BT not enabled?) - END mAcceptThread");
                return;
            }
            while (com.navdy.hud.mfi.BluetoothIAP2Server.this.mState != 3) {
                try {
                    android.bluetooth.BluetoothSocket socket = this.mmServerSocket.accept();
                    android.util.Log.d(com.navdy.hud.mfi.BluetoothIAP2Server.TAG, "Socket accepted");
                    if (socket != null) {
                        synchronized (com.navdy.hud.mfi.BluetoothIAP2Server.this) {
                            switch (com.navdy.hud.mfi.BluetoothIAP2Server.this.mState) {
                                case 0:
                                case 3:
                                    try {
                                        socket.close();
                                        break;
                                    } catch (java.io.IOException e) {
                                        android.util.Log.e(com.navdy.hud.mfi.BluetoothIAP2Server.TAG, "Could not close unwanted socket", e);
                                        break;
                                    }
                                case 1:
                                case 2:
                                    com.navdy.hud.mfi.BluetoothIAP2Server.this.connected(socket, socket.getRemoteDevice());
                                    break;
                            }
                        }
                    }
                } catch (java.io.IOException e2) {
                    android.util.Log.e(com.navdy.hud.mfi.BluetoothIAP2Server.TAG, "Socket accept() failed", e2);
                }
            }
            android.util.Log.i(com.navdy.hud.mfi.BluetoothIAP2Server.TAG, "END mAcceptThread");
            return;
        }

        public void cancel() {
            android.util.Log.d(com.navdy.hud.mfi.BluetoothIAP2Server.TAG, "Socket cancel " + this);
            try {
                if (this.mmServerSocket != null) {
                    this.mmServerSocket.close();
                }
            } catch (java.io.IOException e) {
                android.util.Log.e(com.navdy.hud.mfi.BluetoothIAP2Server.TAG, "Socket close() of bluetoothServer failed", e);
            }
        }
    }

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            com.navdy.hud.mfi.BluetoothIAP2Server.this.start();
        }
    }

    private class ConnectThread extends java.lang.Thread {
        private final android.bluetooth.BluetoothDevice mmDevice;
        private final android.bluetooth.BluetoothSocket mmSocket;

        public ConnectThread(android.bluetooth.BluetoothDevice device) {
            this.mmDevice = device;
            android.bluetooth.BluetoothSocket tmp = null;
            try {
                tmp = device.createRfcommSocketToServiceRecord(com.navdy.hud.mfi.BluetoothIAP2Server.DEVICE_IAP2);
            } catch (java.io.IOException e) {
                android.util.Log.e(com.navdy.hud.mfi.BluetoothIAP2Server.TAG, "Socket create() failed", e);
            }
            this.mmSocket = tmp;
        }

        public void run() {
            android.util.Log.i(com.navdy.hud.mfi.BluetoothIAP2Server.TAG, "BEGIN mConnectThread");
            setName("ConnectThread");
            com.navdy.hud.mfi.BluetoothIAP2Server.this.mAdapter.cancelDiscovery();
            try {
                this.mmSocket.connect();
                synchronized (com.navdy.hud.mfi.BluetoothIAP2Server.this) {
                    com.navdy.hud.mfi.BluetoothIAP2Server.this.mConnectThread = null;
                }
                com.navdy.hud.mfi.BluetoothIAP2Server.this.connected(this.mmSocket, this.mmDevice);
            } catch (java.io.IOException e) {
                try {
                    this.mmSocket.close();
                } catch (java.io.IOException e2) {
                    android.util.Log.e(com.navdy.hud.mfi.BluetoothIAP2Server.TAG, "unable to close() socket during connection failure", e2);
                }
                com.navdy.hud.mfi.BluetoothIAP2Server.this.connectionFailed();
            }
        }

        public void cancel() {
            try {
                this.mmSocket.close();
            } catch (java.io.IOException e) {
                android.util.Log.e(com.navdy.hud.mfi.BluetoothIAP2Server.TAG, "close() of connect socket failed", e);
            }
        }
    }

    private class ConnectedThread extends java.lang.Thread {
        private final java.io.InputStream mmInStream;
        private final java.io.OutputStream mmOutStream;
        private final android.bluetooth.BluetoothSocket mmSocket;

        public ConnectedThread(android.bluetooth.BluetoothSocket socket) {
            android.util.Log.d(com.navdy.hud.mfi.BluetoothIAP2Server.TAG, "create ConnectedThread");
            this.mmSocket = socket;
            java.io.InputStream tmpIn = null;
            java.io.OutputStream tmpOut = null;
            try {
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();
            } catch (java.io.IOException e) {
                android.util.Log.e(com.navdy.hud.mfi.BluetoothIAP2Server.TAG, "temp sockets not created", e);
            }
            this.mmInStream = tmpIn;
            this.mmOutStream = tmpOut;
        }

        public void run() {
            android.bluetooth.BluetoothDevice device = this.mmSocket.getRemoteDevice();
            android.util.Log.i(com.navdy.hud.mfi.BluetoothIAP2Server.TAG, "BEGIN mConnectedThread - " + device.getAddress() + " name:" + device.getName());
            com.navdy.hud.mfi.BluetoothIAP2Server.this.linkLayer.connectionStarted(device.getAddress(), device.getName());
            byte[] buffer = new byte[1024];
            while (true) {
                try {
                    com.navdy.hud.mfi.BluetoothIAP2Server.this.linkLayer.queue(new com.navdy.hud.mfi.LinkPacket(java.util.Arrays.copyOf(buffer, this.mmInStream.read(buffer))));
                } catch (java.io.IOException e) {
                    android.util.Log.e(com.navdy.hud.mfi.BluetoothIAP2Server.TAG, "disconnected", e);
                    com.navdy.hud.mfi.BluetoothIAP2Server.this.connectionLost();
                    return;
                }
            }
        }

        public void write(byte[] buffer) {
            try {
                this.mmOutStream.write(buffer);
            } catch (java.io.IOException e) {
                android.util.Log.e(com.navdy.hud.mfi.BluetoothIAP2Server.TAG, "Exception during write", e);
            }
        }

        public void cancel() {
            try {
                this.mmSocket.close();
            } catch (java.io.IOException e) {
                android.util.Log.e(com.navdy.hud.mfi.BluetoothIAP2Server.TAG, "close() of connect socket failed", e);
            }
        }
    }

    public BluetoothIAP2Server(android.os.Handler handler) {
        this.mHandler = handler;
    }

    public java.lang.String getName() {
        return this.mAdapter.getName();
    }

    public byte[] getLocalAddress() {
        return com.navdy.hud.mfi.Utils.parseMACAddress(this.mAdapter.getAddress());
    }

    public void connect(com.navdy.hud.mfi.LinkLayer linkLayer2) {
        this.linkLayer = linkLayer2;
    }

    public synchronized void start() {
        android.util.Log.d(TAG, "start");
        setState(1);
        if (this.mAcceptThread == null) {
            this.mAcceptThread = new com.navdy.hud.mfi.BluetoothIAP2Server.AcceptThread();
            this.mAcceptThread.start();
        }
    }

    public synchronized void stop() {
        android.util.Log.d(TAG, "stop");
        if (this.mConnectThread != null) {
            this.mConnectThread.cancel();
            this.mConnectThread = null;
        }
        if (this.mConnectedThread != null) {
            this.mConnectedThread.cancel();
            this.mConnectedThread = null;
        }
        if (this.mAcceptThread != null) {
            this.mAcceptThread.cancel();
            this.mAcceptThread = null;
        }
        setState(0);
    }

    public synchronized void connect(android.bluetooth.BluetoothDevice device) {
        android.util.Log.d(TAG, "connect to: " + device);
        if (this.mConnectThread != null) {
            this.mConnectThread.cancel();
            this.mConnectThread = null;
        }
        if (this.mConnectedThread != null) {
            this.mConnectedThread.cancel();
            this.mConnectedThread = null;
        }
        this.mConnectThread = new com.navdy.hud.mfi.BluetoothIAP2Server.ConnectThread(device);
        this.mConnectThread.start();
        setState(2);
    }

    public synchronized void connected(android.bluetooth.BluetoothSocket socket, android.bluetooth.BluetoothDevice device) {
        android.util.Log.d(TAG, "connected");
        if (this.mConnectThread != null) {
            this.mConnectThread.cancel();
            this.mConnectThread = null;
        }
        if (this.mConnectedThread != null) {
            this.mConnectedThread.cancel();
            this.mConnectedThread = null;
        }
        if (this.mAcceptThread != null) {
            this.mAcceptThread.cancel();
            this.mAcceptThread = null;
        }
        this.mConnectedThread = new com.navdy.hud.mfi.BluetoothIAP2Server.ConnectedThread(socket);
        this.mConnectedThread.start();
        android.os.Message msg = this.mHandler.obtainMessage(4);
        android.os.Bundle bundle = new android.os.Bundle();
        bundle.putString(com.navdy.hud.mfi.Constants.DEVICE_NAME, device.getName());
        msg.setData(bundle);
        this.mHandler.sendMessage(msg);
        setState(3);
    }

    private synchronized void setState(int state) {
        android.util.Log.d(TAG, "setState() " + this.mState + " -> " + state);
        this.mState = state;
        this.mHandler.obtainMessage(1, state, -1).sendToTarget();
    }

    public void write(byte[] out) {
        synchronized (this) {
            if (this.mState == 3) {
                com.navdy.hud.mfi.BluetoothIAP2Server.ConnectedThread r = this.mConnectedThread;
                r.write(out);
            }
        }
    }

    /* access modifiers changed from: private */
    public void connectionFailed() {
        android.os.Message msg = this.mHandler.obtainMessage(5);
        android.os.Bundle bundle = new android.os.Bundle();
        bundle.putString(com.navdy.hud.mfi.Constants.TOAST, "Unable to connect device");
        msg.setData(bundle);
        this.mHandler.sendMessage(msg);
        start();
    }

    /* access modifiers changed from: private */
    public void connectionLost() {
        this.linkLayer.connectionEnded();
        android.os.Message msg = this.mHandler.obtainMessage(5);
        android.os.Bundle bundle = new android.os.Bundle();
        bundle.putString(com.navdy.hud.mfi.Constants.TOAST, "Device connection was lost");
        msg.setData(bundle);
        this.mHandler.sendMessage(msg);
        this.mHandler.postDelayed(new com.navdy.hud.mfi.BluetoothIAP2Server.Anon1(), 500);
    }

    public synchronized void reconfirmDiscoverableState(android.app.Activity intentContext) {
        if (intentContext != null) {
            try {
                android.content.Intent discoverableIntent = new android.content.Intent("android.bluetooth.adapter.action.REQUEST_DISCOVERABLE");
                discoverableIntent.putExtra("android.bluetooth.adapter.extra.DISCOVERABLE_DURATION", 300);
                intentContext.startActivity(discoverableIntent);
                android.util.Log.d(TAG, "Now Discoverable");
            } catch (java.lang.Exception e) {
                android.util.Log.e(TAG, "Failed to make discoverable", e);
            }
        }
        return;
    }

    public void queue(com.navdy.hud.mfi.LinkPacket pkt) {
        write(pkt.data);
    }
}
