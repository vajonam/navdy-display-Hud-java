package com.navdy.hud.mfi;

public class Utils {
    public static final java.lang.String MFI_TAG = "MFi";
    protected static final java.lang.String TAG = "navdy-mfi-Utils";
    protected static final char[] hexArray = "0123456789ABCDEF".toCharArray();

    public static java.lang.String bytesToHex(byte[] bytes) {
        return bytesToHex(bytes, false);
    }

    public static java.lang.String bytesToHex(byte[] bytes, boolean spaces) {
        int charsPerByte = spaces ? 3 : 2;
        char[] hexChars = new char[(bytes.length * charsPerByte)];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 255;
            hexChars[j * charsPerByte] = hexArray[v >>> 4];
            hexChars[(j * charsPerByte) + 1] = hexArray[v & 15];
            if (spaces) {
                hexChars[(j * charsPerByte) + 2] = com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.SPACE_CHAR;
            }
        }
        return new java.lang.String(hexChars);
    }

    public static java.lang.String bytesToMacAddress(byte[] address) {
        return java.lang.String.format(java.util.Locale.US, "%02X:%02X:%02X:%02X:%02X:%02X", new java.lang.Object[]{java.lang.Byte.valueOf(address[0]), java.lang.Byte.valueOf(address[1]), java.lang.Byte.valueOf(address[2]), java.lang.Byte.valueOf(address[3]), java.lang.Byte.valueOf(address[4]), java.lang.Byte.valueOf(address[5])});
    }

    public static int nibbleToByte(byte n) {
        int b = n & 255;
        if (b <= 57) {
            return b - 48;
        }
        return (b - 65) + 10;
    }

    public static byte[] parseMACAddress(java.lang.String addr) {
        byte[] d = addr.toUpperCase().getBytes();
        byte[] mac = new byte[6];
        for (int i = 0; i < 6; i++) {
            mac[i] = (byte) ((nibbleToByte(d[i * 3]) << 4) | nibbleToByte(d[(i * 3) + 1]));
        }
        return mac;
    }

    public static android.util.SparseArray<java.lang.String> getConstantsMap(java.lang.Class c) {
        return getConstantsMap(c, null);
    }

    public static android.util.SparseArray<java.lang.String> getConstantsMap(java.lang.Class c, java.lang.String prefix) {
        java.lang.reflect.Field[] fields;
        android.util.SparseArray<java.lang.String> constants = new android.util.SparseArray<>();
        for (java.lang.reflect.Field field : c.getDeclaredFields()) {
            try {
                if (field.getType().equals(java.lang.Integer.TYPE) && java.lang.reflect.Modifier.isStatic(field.getModifiers()) && (prefix == null || field.getName().startsWith(prefix))) {
                    constants.put(field.getInt(null), field.getName());
                }
            } catch (java.lang.IllegalAccessException e) {
                android.util.Log.e(TAG, "", e);
            }
        }
        return constants;
    }

    public static void logTransfer(java.lang.String tag, java.lang.String fmt, java.lang.Object... args) {
        java.lang.String format;
        if (android.util.Log.isLoggable(tag, 3)) {
            boolean logPacketData = android.util.Log.isLoggable(tag, 2);
            for (int i = 0; i < args.length; i++) {
                if (args[i] instanceof byte[]) {
                    byte[] data = args[i];
                    if (logPacketData) {
                        format = bytesToHex(data, true);
                    } else {
                        format = java.lang.String.format("%d bytes", new java.lang.Object[]{java.lang.Integer.valueOf(data.length)});
                    }
                    args[i] = format;
                }
            }
            android.util.Log.d(tag, java.lang.String.format(fmt, args));
        }
    }

    public static byte[] intsToBytes(int[] ints) {
        byte[] bytes = new byte[ints.length];
        for (int i = 0; i < ints.length; i++) {
            bytes[i] = (byte) ints[i];
        }
        return bytes;
    }

    public static java.lang.String toASCII(byte[] bytes) {
        java.lang.String s = "";
        for (byte b : bytes) {
            int v = b & 255;
            s = s + ((v < 32 || v >= 128) ? '.' : (char) v);
        }
        return s;
    }

    public static long unpackInt64(byte[] d, int offset) {
        return java.nio.ByteBuffer.wrap(d, offset, d.length - offset).getLong();
    }

    public static long unpackUInt32(byte[] d, int offset) {
        return (((long) (d[offset] & com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient.ORDER_BY_DEFAULT)) << 24) | (((long) (d[offset + 1] & com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient.ORDER_BY_DEFAULT)) << 16) | (((long) (d[offset + 2] & com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient.ORDER_BY_DEFAULT)) << 8) | ((long) (d[offset + 3] & com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient.ORDER_BY_DEFAULT));
    }

    public static int unpackUInt16(byte[] d, int offset) {
        return ((d[offset] & com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient.ORDER_BY_DEFAULT) << 8) | (d[offset + 1] & com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient.ORDER_BY_DEFAULT);
    }

    public static int unpackUInt8(byte[] d, int offset) {
        return d[offset] & com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient.ORDER_BY_DEFAULT;
    }

    public static byte[] packUInt16(int i) {
        return new byte[]{(byte) ((i >> 8) & 255), (byte) (i & 255)};
    }

    public static byte[] packUInt8(int b) {
        return new byte[]{(byte) (b & 255)};
    }

    public static java.math.BigInteger unpackUInt64(byte[] d, int offset) {
        return new java.math.BigInteger(new byte[]{(byte) (d[offset] & com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient.ORDER_BY_DEFAULT), (byte) (d[offset + 1] & com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient.ORDER_BY_DEFAULT), (byte) (d[offset + 2] & com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient.ORDER_BY_DEFAULT), (byte) (d[offset + 3] & com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient.ORDER_BY_DEFAULT), (byte) (d[offset + 4] & com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient.ORDER_BY_DEFAULT), (byte) (d[offset + 5] & com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient.ORDER_BY_DEFAULT), (byte) (d[offset + 6] & com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient.ORDER_BY_DEFAULT), (byte) (d[offset + 7] & com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient.ORDER_BY_DEFAULT)});
    }
}
