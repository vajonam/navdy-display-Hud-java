package com.navdy.hud.mfi;

public class iAPProcessor implements com.navdy.hud.mfi.SessionPacketReceiver, com.navdy.hud.mfi.EASessionPacketReceiver {
    public static final java.lang.String CHARSET_NAME = "UTF-8";
    public static final int DEVICE_AUTHENTICATION_CERTIFICATE_WAIT_TIME = 3000;
    public static final int DEVICE_AUTHENTICATION_RETRY_INTERVAL = 5000;
    private static final int EASESSION_SESSION = 7;
    public static final int HEADER_BUFFER = 100;
    public static final java.lang.String HID_COMPONENT_NAME = "navdy-media-remote";
    public static final int MAX_DEVICE_AUTHENTICATION_RETRIES = 5;
    private static final int MESSAGE_LINK_LOST = 2;
    private static final int MESSAGE_RETRY_DEVICE_AUTHENTICATION = 3;
    private static final int MESSAGE_SESSION_MESSAGE = 1;
    private static final int MESSAGE_SKIP_DEVICE_AUTHENTICATION = 4;
    public static final java.lang.String NAVDY_CLIENT_BUNDLE_ID = "com.navdy.NavdyClient";
    public static final int PRODUCT_IDENTIFIER = 1;
    public static final java.lang.String PROTOCOL_V1 = "com.navdy.hud.api.v1";
    public static final java.lang.String PROXY_V1 = "com.navdy.hud.proxy.v1";
    public static final int STARTING_OFFSET = 6;
    /* access modifiers changed from: private */
    public static final java.lang.String TAG = com.navdy.hud.mfi.iAPProcessor.class.getSimpleName();
    public static final int VENDOR_IDENTIFIER = 39046;
    static final android.util.SparseArray<com.navdy.hud.mfi.iAPProcessor.iAPMessage> iAPMsgById = new android.util.SparseArray<>();
    static final boolean isCommunicationUpdateSupported = false;
    static final boolean isCommunicationsSupported = true;
    static final boolean isHIDKeyboardSupported = false;
    static final boolean isHIDMediaRemoteSupported = true;
    private static final boolean isHIDSupported = true;
    static final boolean isMusicSupported = true;
    public static final com.navdy.hud.mfi.iAPProcessor.MediaRemoteComponent mediaRemoteComponent = com.navdy.hud.mfi.iAPProcessor.MediaRemoteComponent.HIDMediaRemote;
    /* access modifiers changed from: private */
    public static final byte[] msgStart = {64, 64};
    private static java.lang.String[] protocols = {PROTOCOL_V1, PROXY_V1};
    com.navdy.hud.mfi.iAPProcessor.iAPMessage __someiAPMessageToForceEnumLoading__;
    java.lang.String accessoryName;
    private com.navdy.hud.mfi.AuthCoprocessor authCoprocessor;
    private java.util.HashMap<com.navdy.hud.mfi.iAPProcessor.iAPMessage, com.navdy.hud.mfi.IAPControlMessageProcessor> controlProcessors;
    private int controlSession;
    private int deviceAuthenticationRetries;
    private android.util.SparseArray<com.navdy.hud.mfi.EASession> eaSessionById;
    int[] hidDescriptorKeyboard;
    int[] hidDescriptorMediaRemote;
    private com.navdy.hud.mfi.IAPListener iapListener;
    private boolean isDeviceAuthenticationSupported;
    private boolean linkEstablished;
    private com.navdy.hud.mfi.LinkLayer linkLayer;
    private com.navdy.hud.mfi.IIAPFileTransferManager mFileTransferManager;
    private final android.os.Handler myHandler;
    private android.util.SparseArray<com.navdy.hud.mfi.iAPProcessor.SessionMessage> reassambleMsgs;
    /* access modifiers changed from: private */
    public com.navdy.hud.mfi.iAPProcessor.StateListener stateListener;

    enum AccessoryHIDReport {
        HIDComponentIdentifier,
        HIDReport
    }

    class Anon1 extends android.os.Handler {
        Anon1(android.os.Looper x0) {
            super(x0);
        }

        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {
                case 1:
                    com.navdy.hud.mfi.iAPProcessor.this.handleSessionPacket((com.navdy.hud.mfi.SessionPacket) msg.obj);
                    return;
                case 2:
                    com.navdy.hud.mfi.iAPProcessor.this.handleLinkLoss();
                    return;
                case 3:
                    com.navdy.hud.mfi.iAPProcessor.this.retryDeviceAuthentication();
                    return;
                case 4:
                    if (com.navdy.hud.mfi.iAPProcessor.this.stateListener != null) {
                        android.util.Log.d(com.navdy.hud.mfi.Utils.MFI_TAG, "Skipping Device Authentication as we did not get the certificate");
                        com.navdy.hud.mfi.iAPProcessor.this.stateListener.onReady();
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    enum AppLaunchMethod {
        LaunchWithUserAlert,
        LaunchWithoutAlert
    }

    enum BluetoothTransportComponent {
        TransportComponentIdentifier,
        TransportComponentName,
        TransportSupportsiAP2Connection,
        BluetoothTransportMediaAccessControlAddress
    }

    enum ExternalAccessoryProtocol {
        ExternalAccessoryProtocolIdentifier,
        ExternalAccessoryProtocolName,
        ExternalAccessoryProtocolMatchAction,
        NativeTransportComponentIdentifier
    }

    enum HIDComponentFunction {
        Keyboard,
        MediaPlaybackRemote,
        AssistiveTouchPointer,
        StandardGamepadFormFitting,
        ExtendedGamepadFormFitting,
        ExtendedGamepadNonFormFitting,
        AssistiveSwitchControl,
        Headphone
    }

    static class IAP2Param {
        int id;
        int length;
        int offset;

        IAP2Param() {
        }
    }

    static class IAP2Params {
        public static final int DATA_OFFSET = 4;
        public static final int ID_OFFSET = 2;
        byte[] data;
        android.util.SparseArray<com.navdy.hud.mfi.iAPProcessor.IAP2Param> params = new android.util.SparseArray<>();

        IAP2Params(byte[] data2, int offset) {
            this.data = data2;
            int i = offset;
            while (i < data2.length) {
                com.navdy.hud.mfi.iAPProcessor.IAP2Param param = new com.navdy.hud.mfi.iAPProcessor.IAP2Param();
                param.offset = i;
                param.length = com.navdy.hud.mfi.Utils.unpackUInt16(data2, i);
                param.id = com.navdy.hud.mfi.Utils.unpackUInt16(data2, i + 2);
                this.params.put(param.id, param);
                i += param.length;
            }
        }

        /* access modifiers changed from: 0000 */
        public boolean hasParam(int id) {
            return this.params.get(id) != null;
        }

        /* access modifiers changed from: 0000 */
        public boolean hasParam(java.lang.Enum e) {
            return hasParam(e.ordinal());
        }

        /* access modifiers changed from: 0000 */
        public int getUInt8(java.lang.Enum param) {
            return getUInt8(param.ordinal());
        }

        /* access modifiers changed from: 0000 */
        public int getUInt8(int paramId) {
            return com.navdy.hud.mfi.Utils.unpackUInt8(this.data, ((com.navdy.hud.mfi.iAPProcessor.IAP2Param) this.params.get(paramId)).offset + 4);
        }

        /* access modifiers changed from: 0000 */
        public int getUInt16(int paramId) {
            return com.navdy.hud.mfi.Utils.unpackUInt16(this.data, ((com.navdy.hud.mfi.iAPProcessor.IAP2Param) this.params.get(paramId)).offset + 4);
        }

        /* access modifiers changed from: 0000 */
        public int getUInt16(java.lang.Enum param) {
            return getUInt16(param.ordinal());
        }

        /* access modifiers changed from: 0000 */
        public long getUInt32(java.lang.Enum param) {
            return getUInt32(param.ordinal());
        }

        /* access modifiers changed from: 0000 */
        public long getUInt32(int paramId) {
            return com.navdy.hud.mfi.Utils.unpackUInt32(this.data, ((com.navdy.hud.mfi.iAPProcessor.IAP2Param) this.params.get(paramId)).offset + 4);
        }

        /* access modifiers changed from: 0000 */
        public java.math.BigInteger getUInt64(int paramId) {
            return com.navdy.hud.mfi.Utils.unpackUInt64(this.data, ((com.navdy.hud.mfi.iAPProcessor.IAP2Param) this.params.get(paramId)).offset + 4);
        }

        /* access modifiers changed from: 0000 */
        public byte[] getBlob(int paramId) {
            com.navdy.hud.mfi.iAPProcessor.IAP2Param p = (com.navdy.hud.mfi.iAPProcessor.IAP2Param) this.params.get(paramId);
            return java.util.Arrays.copyOfRange(this.data, p.offset + 4, p.offset + p.length);
        }

        /* access modifiers changed from: 0000 */
        public byte[] getBlob(java.lang.Enum param) {
            return getBlob(param.ordinal());
        }

        /* access modifiers changed from: 0000 */
        public java.lang.String getUTF8(int paramId) {
            byte[] blob = getBlob(paramId);
            try {
                return new java.lang.String(blob, 0, blob.length - 1, "UTF-8");
            } catch (java.io.UnsupportedEncodingException e) {
                return null;
            }
        }

        /* access modifiers changed from: 0000 */
        public java.lang.String getUTF8(java.lang.Enum param) {
            return getUTF8(param.ordinal());
        }

        /* access modifiers changed from: 0000 */
        public int getEnum(int paramId) {
            return getUInt8(paramId);
        }

        /* access modifiers changed from: 0000 */
        public <E> E getEnum(java.lang.Class<E> type, java.lang.Enum param) {
            return getEnum(type, param.ordinal());
        }

        /* access modifiers changed from: 0000 */
        public <E> E getEnum(java.lang.Class<E> type, int param) {
            int value = getEnum(param);
            E[] values = type.getEnumConstants();
            if (value < 0 || value >= values.length) {
                return null;
            }
            return values[value];
        }

        /* access modifiers changed from: 0000 */
        public int getEnum(java.lang.Enum param) {
            return getEnum(param.ordinal());
        }

        /* access modifiers changed from: 0000 */
        public boolean getBoolean(int paramId) {
            return getUInt8(paramId) != 0;
        }

        /* access modifiers changed from: 0000 */
        public boolean getBoolean(java.lang.Enum param) {
            return getBoolean(param.ordinal());
        }
    }

    static class IAP2ParamsCreator {
        private static final int META_DATA_LENGTH = 4;
        public static final int SIZE_OF_LENGTH = 2;
        public static final int SIZE_OF_PARAM_ID = 2;
        java.io.DataOutputStream dos = new java.io.DataOutputStream(this.params);
        java.io.ByteArrayOutputStream params = new java.io.ByteArrayOutputStream();

        /* access modifiers changed from: 0000 */
        public byte[] toBytes() {
            return this.params.toByteArray();
        }

        /* access modifiers changed from: 0000 */
        public com.navdy.hud.mfi.iAPProcessor.IAP2ParamsCreator addBoolean(int paramId, boolean val) {
            return addUInt8(paramId, val ? 1 : 0);
        }

        /* access modifiers changed from: 0000 */
        public com.navdy.hud.mfi.iAPProcessor.IAP2ParamsCreator addBoolean(java.lang.Enum param, boolean val) {
            return addUInt8(param.ordinal(), val ? 1 : 0);
        }

        /* access modifiers changed from: 0000 */
        public com.navdy.hud.mfi.iAPProcessor.IAP2ParamsCreator addUInt8(int paramId, int b) {
            try {
                this.dos.writeShort(5);
                this.dos.writeShort(paramId);
                this.dos.writeByte(b);
            } catch (java.io.IOException e) {
                android.util.Log.e(com.navdy.hud.mfi.iAPProcessor.TAG, "Exception while creating message ", e);
            }
            return this;
        }

        /* access modifiers changed from: 0000 */
        public com.navdy.hud.mfi.iAPProcessor.IAP2ParamsCreator addUInt16(int paramId, int i) {
            try {
                this.dos.writeShort(6);
                this.dos.writeShort(paramId);
                this.dos.writeShort(i);
            } catch (java.io.IOException e) {
                android.util.Log.e(com.navdy.hud.mfi.iAPProcessor.TAG, "Exception while creating message ", e);
            }
            return this;
        }

        /* access modifiers changed from: 0000 */
        public com.navdy.hud.mfi.iAPProcessor.IAP2ParamsCreator addEnum(int paramId, int b) {
            return addUInt8(paramId, b);
        }

        /* access modifiers changed from: 0000 */
        public com.navdy.hud.mfi.iAPProcessor.IAP2ParamsCreator addBlob(int paramId, byte[] blob) {
            try {
                this.dos.writeShort(blob.length + 4);
                this.dos.writeShort(paramId);
                this.dos.write(blob);
            } catch (java.io.IOException e) {
                android.util.Log.e(com.navdy.hud.mfi.iAPProcessor.TAG, "Exception while creating message ", e);
            }
            return this;
        }

        /* access modifiers changed from: 0000 */
        public com.navdy.hud.mfi.iAPProcessor.IAP2ParamsCreator addString(int paramId, java.lang.String s) {
            try {
                byte[] data = s.getBytes("UTF-8");
                this.dos.writeShort(data.length + 4 + 1);
                this.dos.writeShort(paramId);
                this.dos.write(data);
                this.dos.write(new byte[]{0});
            } catch (java.io.IOException e) {
                android.util.Log.e(com.navdy.hud.mfi.iAPProcessor.TAG, "Exception while creating message ", e);
            }
            return this;
        }

        /* access modifiers changed from: 0000 */
        public com.navdy.hud.mfi.iAPProcessor.IAP2ParamsCreator addNone(int paramId) {
            try {
                this.dos.writeShort(4);
                this.dos.writeShort(paramId);
            } catch (java.io.IOException e) {
                android.util.Log.e(com.navdy.hud.mfi.iAPProcessor.TAG, "Exception while creating message ", e);
            }
            return this;
        }

        /* access modifiers changed from: 0000 */
        public com.navdy.hud.mfi.iAPProcessor.IAP2ParamsCreator addUInt8(java.lang.Enum paramId, int b) {
            return addUInt8(paramId.ordinal(), b);
        }

        /* access modifiers changed from: 0000 */
        public com.navdy.hud.mfi.iAPProcessor.IAP2ParamsCreator addUInt16(java.lang.Enum paramId, int i) {
            return addUInt16(paramId.ordinal(), i);
        }

        /* access modifiers changed from: 0000 */
        public com.navdy.hud.mfi.iAPProcessor.IAP2ParamsCreator addEnum(java.lang.Enum paramId, int b) {
            return addEnum(paramId.ordinal(), b);
        }

        /* access modifiers changed from: 0000 */
        public com.navdy.hud.mfi.iAPProcessor.IAP2ParamsCreator addEnum(java.lang.Enum paramId, java.lang.Enum b) {
            return addEnum(paramId.ordinal(), b.ordinal());
        }

        /* access modifiers changed from: 0000 */
        public com.navdy.hud.mfi.iAPProcessor.IAP2ParamsCreator addBlob(java.lang.Enum paramId, byte[] blob) {
            return addBlob(paramId.ordinal(), blob);
        }

        /* access modifiers changed from: 0000 */
        public com.navdy.hud.mfi.iAPProcessor.IAP2ParamsCreator addString(java.lang.Enum paramId, java.lang.String s) {
            return addString(paramId.ordinal(), s);
        }

        /* access modifiers changed from: 0000 */
        public com.navdy.hud.mfi.iAPProcessor.IAP2ParamsCreator addNone(java.lang.Enum paramId) {
            return addNone(paramId.ordinal());
        }
    }

    static class IAP2SessionMessage extends com.navdy.hud.mfi.iAPProcessor.IAP2ParamsCreator {
        int msgId;

        IAP2SessionMessage(int msgId2) {
            this.msgId = msgId2;
            try {
                this.dos.write(com.navdy.hud.mfi.iAPProcessor.msgStart);
                this.dos.writeShort(0);
                this.dos.writeShort(msgId2);
            } catch (java.io.IOException ie) {
                android.util.Log.e(com.navdy.hud.mfi.iAPProcessor.TAG, "Exception while creating message ", ie);
            }
        }

        IAP2SessionMessage(com.navdy.hud.mfi.iAPProcessor.iAPMessage msgId2) {
            this(msgId2.id);
        }

        /* access modifiers changed from: 0000 */
        public byte[] toBytes() {
            byte[] bytes = super.toBytes();
            com.navdy.hud.mfi.iAPProcessor.copy(bytes, 2, com.navdy.hud.mfi.Utils.packUInt16(bytes.length));
            return bytes;
        }
    }

    enum IdentificationInformation {
        Name,
        ModelIdentifier,
        Manufacturer,
        SerialNumber,
        FirmwareVersion,
        HardwareVersion,
        MessagesSentByAccessory,
        MessagesReceivedFromDevice,
        PowerProvidingCapability,
        MaximumCurrentDrawnFromDevice,
        SupportedExternalAccessoryProtocol,
        AppMatchTeamID,
        CurrentLanguage,
        SupportedLanguage,
        SerialTransportComponent,
        USBDeviceTransportComponent,
        USBHostTransportComponent,
        BluetoothTransportComponent,
        iAP2HIDComponent,
        VehicleInformationComponent,
        VehicleStatusComponent,
        LocationInformationComponent,
        USBHostHIDComponent
    }

    public enum MediaRemoteComponent {
        HIDKeyboard,
        HIDMediaRemote
    }

    enum RequestAppLaunch {
        AppBundleID,
        LaunchAlert
    }

    class SessionMessage {
        com.navdy.hud.mfi.ByteArrayBuilder builder;
        int msgId;
        int msgLen;

        SessionMessage(int msgLen2, int msgId2) {
            this.msgLen = msgLen2;
            this.msgId = msgId2;
            this.builder = new com.navdy.hud.mfi.ByteArrayBuilder(msgLen2);
        }

        /* access modifiers changed from: 0000 */
        public void append(byte[] bytes) {
            try {
                this.builder.addBlob(bytes);
            } catch (java.io.IOException e) {
                android.util.Log.e(com.navdy.hud.mfi.iAPProcessor.TAG, "Error appending messages ");
            }
        }
    }

    enum StartHID {
        HIDComponentIdentifier,
        VendorIdentifier,
        ProductIdentifier,
        LocalizedKeyboardCountryCode,
        HIDReportDescriptor
    }

    public interface StateListener {
        void onError();

        void onReady();

        void onSessionStart(com.navdy.hud.mfi.EASession eASession);

        void onSessionStop(com.navdy.hud.mfi.EASession eASession);
    }

    enum StopHID {
        HIDComponentIdentifier
    }

    enum iAP2HIDComponent {
        HIDComponentIdentifier,
        HIDComponentName,
        HIDComponentFunction
    }

    enum iAPMessage {
        RequestAuthenticationRequest(43520),
        AuthenticationCertificate(43521),
        RequestAuthenticationChallengeResponse(43522),
        AuthenticationResponse(43523),
        AuthenticationFailed(43524),
        AuthenticationSucceeded(43525),
        StartIdentification(7424),
        IdentificationInformation(7425),
        IdentificationAccepted(7426),
        IdentificationRejected(7427),
        CancelIdentification(7428),
        IdentificationInformationUpdate(7429),
        StartExternalAccessoryProtocolSession(59904),
        StopExternalAccessoryProtocolSession(59905),
        StatusExternalAccessoryProtocolSession(59906),
        RequestDeviceAuthenticationCertificate(43536),
        DeviceAuthenticationCertificate(43537),
        RequestDeviceAuthenticationChallengeResponse(43538),
        DeviceAuthenticationResponse(43539),
        DeviceAuthenticationFailed(43540),
        DeviceAuthenticationSucceeded(43541),
        StartHID(26624),
        DeviceHIDReport(26625),
        AccessoryHIDReport(26626),
        StopHID(26627),
        StartCallStateUpdates(16724),
        CallStateUpdate(16725),
        StopCallStateUpdates(16726),
        InitiateCall(16730),
        AcceptCall(16731),
        EndCall(16732),
        HoldStatusUpdate(16735),
        MuteStatusUpdate(16736),
        StartCommunicationUpdates(16727),
        CommunicationUpdate(16728),
        StopCommunicationUpdates(16729),
        StartListUpdates(16752),
        ListUpdate(16753),
        StopListUpdate(16754),
        StartNowPlayingUpdates(20480),
        NowPlayingUpdate(20481),
        StopNowPlayingUpdates(20482),
        RequestAppLaunch(59906);
        
        int id;

        private iAPMessage(int id2) {
            this.id = id2;
            com.navdy.hud.mfi.iAPProcessor.iAPMsgById.put(id2, this);
        }
    }

    public void setAccessoryName(java.lang.String accessoryName2) {
        this.accessoryName = accessoryName2;
    }

    public void connect(com.navdy.hud.mfi.IIAPFileTransferManager fileTransferManager) {
        this.mFileTransferManager = fileTransferManager;
    }

    public void connect(com.navdy.hud.mfi.iAPProcessor.StateListener eaSessionListener) {
        this.stateListener = eaSessionListener;
    }

    public void connect(com.navdy.hud.mfi.LinkLayer linkLayer2) {
        this.linkLayer = linkLayer2;
    }

    public void registerControlMessageProcessor(com.navdy.hud.mfi.iAPProcessor.iAPMessage message, com.navdy.hud.mfi.IAPControlMessageProcessor processor) {
        if (processor != null) {
            this.controlProcessors.put(message, processor);
        }
    }

    public iAPProcessor(com.navdy.hud.mfi.IAPListener listener, android.content.Context context) {
        this.eaSessionById = new android.util.SparseArray<>();
        this.deviceAuthenticationRetries = 0;
        this.controlSession = -1;
        this.isDeviceAuthenticationSupported = true;
        this.hidDescriptorMediaRemote = new int[]{5, 12, 9, 1, com.navdy.hud.app.bluetooth.obex.ResponseCodes.OBEX_HTTP_CREATED, 1, 21, 0, 37, 1, 9, com.navdy.hud.app.bluetooth.obex.ResponseCodes.OBEX_HTTP_UNSUPPORTED_TYPE, 9, com.navdy.hud.app.bluetooth.obex.ResponseCodes.OBEX_HTTP_ENTITY_TOO_LARGE, 9, com.navdy.hud.app.bluetooth.obex.ResponseCodes.OBEX_HTTP_USE_PROXY, 9, 182, 117, 1, 149, 4, com.navdy.hud.app.bluetooth.obex.ObexHelper.OBEX_OPCODE_DISCONNECT, 2, 117, 4, 149, 1, com.navdy.hud.app.bluetooth.obex.ObexHelper.OBEX_OPCODE_DISCONNECT, 3, 192};
        this.hidDescriptorKeyboard = new int[]{5, 12, 9, 1, com.navdy.hud.app.bluetooth.obex.ResponseCodes.OBEX_HTTP_CREATED, 1, 5, 7, 21, 0, 37, 1, 117, 8, 149, 1, com.navdy.hud.app.bluetooth.obex.ObexHelper.OBEX_OPCODE_DISCONNECT, 3, 5, 12, 21, 0, 37, 1, 9, 64, 9, com.navdy.hud.app.bluetooth.obex.ResponseCodes.OBEX_HTTP_ENTITY_TOO_LARGE, 9, com.navdy.hud.app.bluetooth.obex.ResponseCodes.OBEX_HTTP_USE_PROXY, 9, 182, 117, 1, 149, 4, com.navdy.hud.app.bluetooth.obex.ObexHelper.OBEX_OPCODE_DISCONNECT, 2, 117, 4, 149, 1, com.navdy.hud.app.bluetooth.obex.ObexHelper.OBEX_OPCODE_DISCONNECT, 3, 192};
        this.accessoryName = "Navdy HUD";
        this.myHandler = new com.navdy.hud.mfi.iAPProcessor.Anon1(android.os.Looper.getMainLooper());
        this.__someiAPMessageToForceEnumLoading__ = com.navdy.hud.mfi.iAPProcessor.iAPMessage.RequestAuthenticationRequest;
        this.reassambleMsgs = new android.util.SparseArray<>();
        this.iapListener = listener;
        this.authCoprocessor = new com.navdy.hud.mfi.AuthCoprocessor(listener, context);
        this.controlProcessors = new java.util.HashMap<>();
    }

    public iAPProcessor(com.navdy.hud.mfi.IAPListener listener) {
        this(listener, null);
    }

    private boolean startsWith(byte[] d, byte[] prefix) {
        for (int i = 0; i < d.length; i++) {
            if (i >= prefix.length) {
                return true;
            }
            if (d[i] != prefix[i]) {
                return false;
            }
        }
        return false;
    }

    static void copy(byte[] dst, int dstPos, byte[] src) {
        java.lang.System.arraycopy(src, 0, dst, dstPos, src.length);
    }

    /* access modifiers changed from: private */
    public void retryDeviceAuthentication() {
        if (this.linkEstablished) {
            this.deviceAuthenticationRetries++;
            sendToDevice(this.controlSession, (com.navdy.hud.mfi.iAPProcessor.IAP2ParamsCreator) new com.navdy.hud.mfi.iAPProcessor.IAP2SessionMessage(com.navdy.hud.mfi.iAPProcessor.iAPMessage.RequestDeviceAuthenticationCertificate));
        }
    }

    /* access modifiers changed from: private */
    public void handleLinkLoss() {
        this.myHandler.removeMessages(3);
        this.myHandler.removeMessages(4);
        this.linkEstablished = false;
        this.deviceAuthenticationRetries = 0;
        if (this.mFileTransferManager != null) {
            this.mFileTransferManager.clear();
        }
        for (int index = 0; index < this.eaSessionById.size(); index++) {
            stopEASession((com.navdy.hud.mfi.EASession) this.eaSessionById.valueAt(index));
        }
        this.eaSessionById.clear();
    }

    /* access modifiers changed from: private */
    public void handleSessionPacket(com.navdy.hud.mfi.SessionPacket smsg) {
        try {
            this.linkEstablished = true;
            if (smsg.session != 2) {
                com.navdy.hud.mfi.iAPProcessor.SessionMessage rcvMsg = (com.navdy.hud.mfi.iAPProcessor.SessionMessage) this.reassambleMsgs.get(smsg.session);
                if (rcvMsg == null) {
                    if (startsWith(smsg.data, msgStart)) {
                        rcvMsg = new com.navdy.hud.mfi.iAPProcessor.SessionMessage(com.navdy.hud.mfi.Utils.unpackUInt16(smsg.data, 2), com.navdy.hud.mfi.Utils.unpackUInt16(smsg.data, 4));
                        this.reassambleMsgs.put(smsg.session, rcvMsg);
                    } else {
                        int eaSessionId = com.navdy.hud.mfi.Utils.unpackUInt16(smsg.data, 0);
                        android.util.Log.d(com.navdy.hud.mfi.Utils.MFI_TAG, "Apple Device(E):" + eaSessionId + " Size:" + smsg.data.length);
                        if (this.eaSessionById.get(eaSessionId) != null) {
                            ((com.navdy.hud.mfi.EASession) this.eaSessionById.get(eaSessionId)).queue(java.util.Arrays.copyOfRange(smsg.data, 2, smsg.data.length));
                        }
                    }
                }
                if (rcvMsg != null) {
                    rcvMsg.append(smsg.data);
                    if (rcvMsg.builder.size() < rcvMsg.msgLen) {
                        return;
                    }
                    if (rcvMsg.builder.size() != rcvMsg.msgLen) {
                        this.reassambleMsgs.remove(smsg.session);
                        throw new java.lang.RuntimeException("bad message size");
                    }
                    handleSessionMessage(smsg.session, rcvMsg.msgId, rcvMsg.builder.build());
                    this.reassambleMsgs.remove(smsg.session);
                }
            } else if (this.mFileTransferManager != null) {
                this.mFileTransferManager.queue(smsg.data);
            }
        } catch (java.lang.Exception e) {
            android.util.Log.e(TAG, "", e);
            this.reassambleMsgs.remove(smsg.session);
            if (this.stateListener != null) {
                this.stateListener.onError();
            }
        }
    }

    private void handleSessionMessage(int session, int msgid, byte[] data) throws java.io.IOException {
        boolean ok;
        com.navdy.hud.mfi.iAPProcessor.iAPMessage msgid_ = (com.navdy.hud.mfi.iAPProcessor.iAPMessage) iAPMsgById.get(msgid);
        if (msgid_ == null) {
            android.util.Log.e(TAG, java.lang.String.format("received unknown message (%d bytes)", new java.lang.Object[]{java.lang.Integer.valueOf(data.length)}));
            return;
        }
        android.util.Log.d(com.navdy.hud.mfi.Utils.MFI_TAG, "received message " + msgid_.toString() + " (" + data.length + " bytes) ");
        switch (msgid_) {
            case RequestAuthenticationRequest:
                this.controlSession = session;
                this.deviceAuthenticationRetries = 0;
                this.authCoprocessor.open();
                try {
                    sendToDevice(session, new com.navdy.hud.mfi.iAPProcessor.IAP2SessionMessage(com.navdy.hud.mfi.iAPProcessor.iAPMessage.AuthenticationCertificate).addBlob(0, this.authCoprocessor.readAccessoryCertificate()));
                    return;
                } finally {
                    this.authCoprocessor.close();
                }
            case RequestAuthenticationChallengeResponse:
                byte[] chg = parse(data).getBlob(0);
                this.authCoprocessor.open();
                try {
                    sendToDevice(session, new com.navdy.hud.mfi.iAPProcessor.IAP2SessionMessage(com.navdy.hud.mfi.iAPProcessor.iAPMessage.AuthenticationResponse).addBlob(0, this.authCoprocessor.createAccessorySignature(chg)));
                    return;
                } finally {
                    this.authCoprocessor.close();
                }
            case AuthenticationSucceeded:
                return;
            case StartIdentification:
                this.isDeviceAuthenticationSupported = true;
                sendIdentification(session);
                return;
            case IdentificationAccepted:
                this.deviceAuthenticationRetries = 0;
                if (this.isDeviceAuthenticationSupported) {
                    android.util.Log.d(com.navdy.hud.mfi.Utils.MFI_TAG, "DeviceAuthentication supported, attempting");
                    sendToDevice(session, (com.navdy.hud.mfi.iAPProcessor.IAP2ParamsCreator) new com.navdy.hud.mfi.iAPProcessor.IAP2SessionMessage(com.navdy.hud.mfi.iAPProcessor.iAPMessage.RequestDeviceAuthenticationCertificate));
                    this.myHandler.sendEmptyMessageDelayed(4, com.navdy.hud.app.device.gps.GpsManager.MINIMUM_DRIVE_TIME);
                    return;
                } else if (this.stateListener != null) {
                    this.stateListener.onReady();
                    return;
                } else {
                    return;
                }
            case IdentificationRejected:
                android.util.Log.d(TAG, java.lang.String.format("msg data: %s", new java.lang.Object[]{com.navdy.hud.mfi.Utils.bytesToHex(data, true)}));
                if (this.isDeviceAuthenticationSupported) {
                    android.util.Log.d(com.navdy.hud.mfi.Utils.MFI_TAG, "Identification rejected, trying without DeviceAuthentication");
                    this.isDeviceAuthenticationSupported = false;
                    sendIdentification(session);
                    return;
                }
                android.util.Log.d(com.navdy.hud.mfi.Utils.MFI_TAG, "Identification rejected, even with DeviceAuthentication turned off");
                if (this.stateListener != null) {
                    this.stateListener.onError();
                    return;
                }
                return;
            case DeviceAuthenticationCertificate:
                this.myHandler.removeMessages(4);
                byte[] cert = parse(data).getBlob(0);
                this.authCoprocessor.open();
                try {
                    sendToDevice(session, new com.navdy.hud.mfi.iAPProcessor.IAP2SessionMessage(com.navdy.hud.mfi.iAPProcessor.iAPMessage.RequestDeviceAuthenticationChallengeResponse).addBlob(0, this.authCoprocessor.validateDeviceCertificateAndGenerateDeviceChallenge(cert)));
                    return;
                } finally {
                    this.authCoprocessor.close();
                }
            case DeviceAuthenticationResponse:
                byte[] respChg = parse(data).getBlob(0);
                this.authCoprocessor.open();
                try {
                    if (respChg.length > 128) {
                        android.util.Log.w(com.navdy.hud.mfi.Utils.MFI_TAG, "DeviceAuthenticationResponse response too big - working around iOS 10 bug");
                        ok = true;
                    } else {
                        ok = this.authCoprocessor.validateDeviceSignature(respChg);
                    }
                    if (!ok) {
                        android.util.Log.e(com.navdy.hud.mfi.Utils.MFI_TAG, "DeviceAuthenticationResponse validation failed");
                    }
                    if (this.deviceAuthenticationRetries >= 5) {
                        android.util.Log.d(com.navdy.hud.mfi.Utils.MFI_TAG, "DeviceAuthenticationRetries reached the limit so sending success");
                        ok = true;
                    }
                    sendToDevice(session, (com.navdy.hud.mfi.iAPProcessor.IAP2ParamsCreator) new com.navdy.hud.mfi.iAPProcessor.IAP2SessionMessage(ok ? com.navdy.hud.mfi.iAPProcessor.iAPMessage.DeviceAuthenticationSucceeded : com.navdy.hud.mfi.iAPProcessor.iAPMessage.DeviceAuthenticationFailed));
                    if (!ok) {
                        this.myHandler.sendEmptyMessageDelayed(3, 5000);
                    } else if (this.iapListener != null) {
                        this.iapListener.onDeviceAuthenticationSuccess(this.deviceAuthenticationRetries);
                    }
                    if (this.stateListener != null) {
                        if (ok) {
                            this.stateListener.onReady();
                        } else {
                            this.stateListener.onError();
                        }
                    }
                    return;
                } finally {
                    this.authCoprocessor.close();
                }
            case StartExternalAccessoryProtocolSession:
                com.navdy.hud.mfi.iAPProcessor.IAP2Params params = parse(data);
                int protoId = params.getUInt8(0);
                int eaSessionId = params.getUInt16(1);
                android.util.Log.d(com.navdy.hud.mfi.Utils.MFI_TAG, "StartExternalAccessoryProtocolSession: " + eaSessionId + " (protoId: " + protoId + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.CLOSE_BRACKET);
                com.navdy.hud.mfi.EASession eaSession = new com.navdy.hud.mfi.EASession(this, this.linkLayer.getRemoteAddress(), this.linkLayer.getRemoteName(), eaSessionId, protocols[protoId]);
                this.eaSessionById.append(eaSessionId, eaSession);
                startEASession(eaSession);
                return;
            case StopExternalAccessoryProtocolSession:
                int sessionId = parse(data).getUInt16(0);
                stopEASession((com.navdy.hud.mfi.EASession) this.eaSessionById.get(sessionId));
                this.eaSessionById.remove(sessionId);
                return;
            default:
                com.navdy.hud.mfi.IAPControlMessageProcessor processor = (com.navdy.hud.mfi.IAPControlMessageProcessor) this.controlProcessors.get(msgid_);
                if (processor != null) {
                    processor.processControlMessage(msgid_, session, data);
                    return;
                }
                return;
        }
    }

    private void sendIdentification(int session) throws java.io.IOException {
        com.navdy.hud.mfi.iAPProcessor.IAP2SessionMessage message = new com.navdy.hud.mfi.iAPProcessor.IAP2SessionMessage(com.navdy.hud.mfi.iAPProcessor.iAPMessage.IdentificationInformation);
        message.addString((java.lang.Enum) com.navdy.hud.mfi.iAPProcessor.IdentificationInformation.Name, this.accessoryName).addString((java.lang.Enum) com.navdy.hud.mfi.iAPProcessor.IdentificationInformation.ModelIdentifier, android.os.Build.MODEL).addString((java.lang.Enum) com.navdy.hud.mfi.iAPProcessor.IdentificationInformation.Manufacturer, android.os.Build.MANUFACTURER).addString((java.lang.Enum) com.navdy.hud.mfi.iAPProcessor.IdentificationInformation.SerialNumber, android.os.Build.SERIAL).addString((java.lang.Enum) com.navdy.hud.mfi.iAPProcessor.IdentificationInformation.FirmwareVersion, android.os.Build.VERSION.RELEASE).addString((java.lang.Enum) com.navdy.hud.mfi.iAPProcessor.IdentificationInformation.HardwareVersion, android.os.Build.HARDWARE);
        com.navdy.hud.mfi.ByteArrayBuilder messageSentByAccessory = new com.navdy.hud.mfi.ByteArrayBuilder();
        if (this.isDeviceAuthenticationSupported) {
            messageSentByAccessory.addInt16(com.navdy.hud.mfi.iAPProcessor.iAPMessage.RequestDeviceAuthenticationCertificate.id).addInt16(com.navdy.hud.mfi.iAPProcessor.iAPMessage.RequestDeviceAuthenticationChallengeResponse.id).addInt16(com.navdy.hud.mfi.iAPProcessor.iAPMessage.DeviceAuthenticationSucceeded.id).addInt16(com.navdy.hud.mfi.iAPProcessor.iAPMessage.DeviceAuthenticationFailed.id).build();
        }
        messageSentByAccessory.addInt16(com.navdy.hud.mfi.iAPProcessor.iAPMessage.StartHID.id).addInt16(com.navdy.hud.mfi.iAPProcessor.iAPMessage.StopHID.id).addInt16(com.navdy.hud.mfi.iAPProcessor.iAPMessage.AccessoryHIDReport.id);
        messageSentByAccessory.addInt16(com.navdy.hud.mfi.iAPProcessor.iAPMessage.StartCallStateUpdates.id).addInt16(com.navdy.hud.mfi.iAPProcessor.iAPMessage.StopCallStateUpdates.id).addInt16(com.navdy.hud.mfi.iAPProcessor.iAPMessage.InitiateCall.id).addInt16(com.navdy.hud.mfi.iAPProcessor.iAPMessage.AcceptCall.id).addInt16(com.navdy.hud.mfi.iAPProcessor.iAPMessage.EndCall.id).addInt16(com.navdy.hud.mfi.iAPProcessor.iAPMessage.MuteStatusUpdate.id);
        messageSentByAccessory.addInt16(com.navdy.hud.mfi.iAPProcessor.iAPMessage.StartNowPlayingUpdates.id).addInt16(com.navdy.hud.mfi.iAPProcessor.iAPMessage.StopNowPlayingUpdates.id);
        messageSentByAccessory.addInt16(com.navdy.hud.mfi.iAPProcessor.iAPMessage.RequestAppLaunch.id);
        com.navdy.hud.mfi.ByteArrayBuilder messageReceivedByAccessory = new com.navdy.hud.mfi.ByteArrayBuilder();
        messageReceivedByAccessory.addInt16(com.navdy.hud.mfi.iAPProcessor.iAPMessage.StartExternalAccessoryProtocolSession.id).addInt16(com.navdy.hud.mfi.iAPProcessor.iAPMessage.StopExternalAccessoryProtocolSession.id);
        messageReceivedByAccessory.addInt16(com.navdy.hud.mfi.iAPProcessor.iAPMessage.DeviceHIDReport.id);
        messageReceivedByAccessory.addInt16(com.navdy.hud.mfi.iAPProcessor.iAPMessage.CallStateUpdate.id);
        messageReceivedByAccessory.addInt16(com.navdy.hud.mfi.iAPProcessor.iAPMessage.NowPlayingUpdate.id);
        if (this.isDeviceAuthenticationSupported) {
            messageReceivedByAccessory.addInt16(com.navdy.hud.mfi.iAPProcessor.iAPMessage.DeviceAuthenticationCertificate.id).addInt16(com.navdy.hud.mfi.iAPProcessor.iAPMessage.DeviceAuthenticationResponse.id);
        }
        message.addBlob((java.lang.Enum) com.navdy.hud.mfi.iAPProcessor.IdentificationInformation.MessagesSentByAccessory, messageSentByAccessory.build()).addBlob((java.lang.Enum) com.navdy.hud.mfi.iAPProcessor.IdentificationInformation.MessagesReceivedFromDevice, messageReceivedByAccessory.build()).addEnum((java.lang.Enum) com.navdy.hud.mfi.iAPProcessor.IdentificationInformation.PowerProvidingCapability, 0).addUInt16((java.lang.Enum) com.navdy.hud.mfi.iAPProcessor.IdentificationInformation.MaximumCurrentDrawnFromDevice, 0).addBlob((java.lang.Enum) com.navdy.hud.mfi.iAPProcessor.IdentificationInformation.SupportedExternalAccessoryProtocol, new com.navdy.hud.mfi.iAPProcessor.IAP2ParamsCreator().addUInt8((java.lang.Enum) com.navdy.hud.mfi.iAPProcessor.ExternalAccessoryProtocol.ExternalAccessoryProtocolIdentifier, 0).addString((java.lang.Enum) com.navdy.hud.mfi.iAPProcessor.ExternalAccessoryProtocol.ExternalAccessoryProtocolName, protocols[0]).addEnum((java.lang.Enum) com.navdy.hud.mfi.iAPProcessor.ExternalAccessoryProtocol.ExternalAccessoryProtocolMatchAction, 1).toBytes()).addBlob((java.lang.Enum) com.navdy.hud.mfi.iAPProcessor.IdentificationInformation.SupportedExternalAccessoryProtocol, new com.navdy.hud.mfi.iAPProcessor.IAP2ParamsCreator().addUInt8((java.lang.Enum) com.navdy.hud.mfi.iAPProcessor.ExternalAccessoryProtocol.ExternalAccessoryProtocolIdentifier, 1).addString((java.lang.Enum) com.navdy.hud.mfi.iAPProcessor.ExternalAccessoryProtocol.ExternalAccessoryProtocolName, protocols[1]).addEnum((java.lang.Enum) com.navdy.hud.mfi.iAPProcessor.ExternalAccessoryProtocol.ExternalAccessoryProtocolMatchAction, 1).toBytes()).addString((java.lang.Enum) com.navdy.hud.mfi.iAPProcessor.IdentificationInformation.CurrentLanguage, com.navdy.hud.app.profile.HudLocale.DEFAULT_LANGUAGE).addString((java.lang.Enum) com.navdy.hud.mfi.iAPProcessor.IdentificationInformation.SupportedLanguage, com.navdy.hud.app.profile.HudLocale.DEFAULT_LANGUAGE).addBlob((java.lang.Enum) com.navdy.hud.mfi.iAPProcessor.IdentificationInformation.BluetoothTransportComponent, new com.navdy.hud.mfi.iAPProcessor.IAP2ParamsCreator().addUInt16((java.lang.Enum) com.navdy.hud.mfi.iAPProcessor.BluetoothTransportComponent.TransportComponentIdentifier, 1234).addString((java.lang.Enum) com.navdy.hud.mfi.iAPProcessor.BluetoothTransportComponent.TransportComponentName, "navdy-transport").addNone((java.lang.Enum) com.navdy.hud.mfi.iAPProcessor.BluetoothTransportComponent.TransportSupportsiAP2Connection).addBlob((java.lang.Enum) com.navdy.hud.mfi.iAPProcessor.BluetoothTransportComponent.BluetoothTransportMediaAccessControlAddress, this.linkLayer.getLocalAddress()).toBytes());
        message.addBlob((java.lang.Enum) com.navdy.hud.mfi.iAPProcessor.IdentificationInformation.iAP2HIDComponent, new com.navdy.hud.mfi.iAPProcessor.IAP2ParamsCreator().addUInt16((java.lang.Enum) com.navdy.hud.mfi.iAPProcessor.iAP2HIDComponent.HIDComponentIdentifier, com.navdy.hud.mfi.iAPProcessor.MediaRemoteComponent.HIDMediaRemote.ordinal()).addString((java.lang.Enum) com.navdy.hud.mfi.iAPProcessor.iAP2HIDComponent.HIDComponentName, HID_COMPONENT_NAME).addEnum((java.lang.Enum) com.navdy.hud.mfi.iAPProcessor.iAP2HIDComponent.HIDComponentFunction, (java.lang.Enum) com.navdy.hud.mfi.iAPProcessor.HIDComponentFunction.MediaPlaybackRemote).toBytes());
        sendToDevice(session, (com.navdy.hud.mfi.iAPProcessor.IAP2ParamsCreator) message);
    }

    public void sendControlMessage(com.navdy.hud.mfi.iAPProcessor.IAP2ParamsCreator msg) {
        sendToDevice(this.controlSession, msg);
    }

    public void sendToDevice(int sessionId, com.navdy.hud.mfi.iAPProcessor.IAP2ParamsCreator msg) {
        byte[] data = msg.toBytes();
        if (msg instanceof com.navdy.hud.mfi.iAPProcessor.IAP2SessionMessage) {
            com.navdy.hud.mfi.iAPProcessor.iAPMessage msgId_ = (com.navdy.hud.mfi.iAPProcessor.iAPMessage) iAPMsgById.get(((com.navdy.hud.mfi.iAPProcessor.IAP2SessionMessage) msg).msgId);
            android.util.Log.d(com.navdy.hud.mfi.Utils.MFI_TAG, "sending message " + msgId_.toString());
            com.navdy.hud.mfi.Utils.logTransfer(TAG, "sending message %s (%d bytes)", msgId_.toString(), java.lang.Integer.valueOf(data.length));
        }
        sendToDevice(sessionId, data);
    }

    public void sendToDevice(int sessionId, byte[] bytes) {
        this.linkLayer.queue(new com.navdy.hud.mfi.SessionPacket(sessionId, bytes));
    }

    private void startEASession(com.navdy.hud.mfi.EASession eaSession) {
        android.util.Log.d(TAG, "startEASession: session: " + eaSession.getSessionIdentifier());
        if (this.stateListener != null) {
            this.stateListener.onSessionStart(eaSession);
        }
    }

    private void stopEASession(com.navdy.hud.mfi.EASession eaSession) {
        android.util.Log.d(TAG, "stopEASession: session: " + eaSession.getSessionIdentifier());
        if (this.stateListener != null) {
            this.stateListener.onSessionStop(eaSession);
        }
    }

    public void linkLost() {
        this.myHandler.sendEmptyMessage(2);
    }

    public void queue(com.navdy.hud.mfi.SessionPacket pkt) {
        this.myHandler.sendMessage(this.myHandler.obtainMessage(1, pkt));
    }

    public void queue(com.navdy.hud.mfi.EASessionPacket pkt) {
        int eaSessionIdentifier = ((com.navdy.hud.mfi.EASession) this.eaSessionById.get(pkt.session)).getSessionIdentifier();
        int maximumLinkPayloadSize = this.linkLayer.native_get_maximum_payload_size() - 100;
        if (maximumLinkPayloadSize > 0) {
            com.navdy.hud.mfi.Utils.logTransfer(TAG, "EA->iAP[" + eaSessionIdentifier + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.SLASH + 7 + "]", new java.lang.Object[0]);
            int remainingDataLength = pkt.data.length;
            int offset = 0;
            do {
                int packetLength = java.lang.Math.min(remainingDataLength, maximumLinkPayloadSize);
                remainingDataLength -= packetLength;
                int bufferSize = packetLength + 2;
                try {
                    android.util.Log.d(com.navdy.hud.mfi.Utils.MFI_TAG, "Accessory(E)   :" + eaSessionIdentifier + " Size:" + packetLength);
                    sendToDevice(7, new com.navdy.hud.mfi.ByteArrayBuilder(bufferSize).addInt16(eaSessionIdentifier).addBlob(pkt.data, offset, packetLength).build());
                    offset += packetLength;
                    continue;
                } catch (java.io.IOException e) {
                    android.util.Log.e(TAG, "Error while sending to the device" + e);
                    continue;
                }
            } while (remainingDataLength > 0);
        }
    }

    public void startHIDSession() {
        int[] descriptor = null;
        int component = mediaRemoteComponent.ordinal();
        switch (mediaRemoteComponent) {
            case HIDKeyboard:
                descriptor = this.hidDescriptorKeyboard;
                break;
            case HIDMediaRemote:
                descriptor = this.hidDescriptorMediaRemote;
                break;
        }
        sendToDevice(this.controlSession, new com.navdy.hud.mfi.iAPProcessor.IAP2SessionMessage(com.navdy.hud.mfi.iAPProcessor.iAPMessage.StartHID).addUInt16((java.lang.Enum) com.navdy.hud.mfi.iAPProcessor.StartHID.HIDComponentIdentifier, component).addUInt16((java.lang.Enum) com.navdy.hud.mfi.iAPProcessor.StartHID.VendorIdentifier, (int) VENDOR_IDENTIFIER).addUInt16((java.lang.Enum) com.navdy.hud.mfi.iAPProcessor.StartHID.ProductIdentifier, 1).addBlob((java.lang.Enum) com.navdy.hud.mfi.iAPProcessor.StartHID.HIDReportDescriptor, com.navdy.hud.mfi.Utils.intsToBytes(descriptor)));
    }

    public void stopHIDSession() {
        sendToDevice(this.controlSession, new com.navdy.hud.mfi.iAPProcessor.IAP2SessionMessage(com.navdy.hud.mfi.iAPProcessor.iAPMessage.StopHID).addUInt16((java.lang.Enum) com.navdy.hud.mfi.iAPProcessor.AccessoryHIDReport.HIDComponentIdentifier, mediaRemoteComponent.ordinal()));
    }

    public void onKeyDown(java.lang.Enum key) {
        onKeyDown(key.ordinal());
    }

    public void onKeyDown(int key) {
        sendHIDReport(1 << key);
    }

    public void onKeyUp(java.lang.Enum key) {
        sendHIDReport(0);
    }

    public void onKeyUp(int key) {
        sendHIDReport(0);
    }

    private void sendHIDReport(int keyMask) {
        int component = mediaRemoteComponent.ordinal();
        int[] report = null;
        switch (mediaRemoteComponent) {
            case HIDKeyboard:
                report = new int[]{0, keyMask};
                break;
            case HIDMediaRemote:
                report = new int[]{keyMask};
                break;
        }
        sendToDevice(this.controlSession, new com.navdy.hud.mfi.iAPProcessor.IAP2SessionMessage(com.navdy.hud.mfi.iAPProcessor.iAPMessage.AccessoryHIDReport).addUInt16((java.lang.Enum) com.navdy.hud.mfi.iAPProcessor.AccessoryHIDReport.HIDComponentIdentifier, component).addBlob((java.lang.Enum) com.navdy.hud.mfi.iAPProcessor.AccessoryHIDReport.HIDReport, com.navdy.hud.mfi.Utils.intsToBytes(report)));
    }

    public int getControlSessionId() {
        return this.controlSession;
    }

    public void launchApp(java.lang.String appBundleId, boolean launchAlert) {
        com.navdy.hud.mfi.iAPProcessor.IAP2SessionMessage requestAppLaunchMessage = new com.navdy.hud.mfi.iAPProcessor.IAP2SessionMessage(com.navdy.hud.mfi.iAPProcessor.iAPMessage.RequestAppLaunch.id);
        requestAppLaunchMessage.addString((java.lang.Enum) com.navdy.hud.mfi.iAPProcessor.RequestAppLaunch.AppBundleID, appBundleId);
        requestAppLaunchMessage.addEnum((java.lang.Enum) com.navdy.hud.mfi.iAPProcessor.RequestAppLaunch.LaunchAlert, (java.lang.Enum) launchAlert ? com.navdy.hud.mfi.iAPProcessor.AppLaunchMethod.LaunchWithUserAlert : com.navdy.hud.mfi.iAPProcessor.AppLaunchMethod.LaunchWithoutAlert);
        sendControlMessage(requestAppLaunchMessage);
    }

    public void launchApp(boolean launchAlert) {
        launchApp(NAVDY_CLIENT_BUNDLE_ID, launchAlert);
    }

    public static com.navdy.hud.mfi.iAPProcessor.IAP2Params parse(byte[] data) {
        if (data != null) {
            return new com.navdy.hud.mfi.iAPProcessor.IAP2Params(data, 6);
        }
        throw new java.lang.IllegalArgumentException();
    }
}
