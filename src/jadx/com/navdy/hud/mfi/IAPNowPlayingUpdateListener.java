package com.navdy.hud.mfi;

public interface IAPNowPlayingUpdateListener {
    void onNowPlayingUpdate(com.navdy.hud.mfi.NowPlayingUpdate nowPlayingUpdate);
}
