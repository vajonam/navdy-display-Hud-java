package com.navdy.hud.mfi;

public abstract class IAPControlMessageProcessor {
    protected java.util.concurrent.ExecutorService mSerialExecutor = java.util.concurrent.Executors.newSingleThreadExecutor();
    protected com.navdy.hud.mfi.iAPProcessor miAPProcessor;

    class Anon1 implements java.lang.Runnable {
        final /* synthetic */ byte[] val$data;
        final /* synthetic */ com.navdy.hud.mfi.iAPProcessor.iAPMessage val$message;
        final /* synthetic */ int val$session;

        Anon1(com.navdy.hud.mfi.iAPProcessor.iAPMessage iapmessage, int i, byte[] bArr) {
            this.val$message = iapmessage;
            this.val$session = i;
            this.val$data = bArr;
        }

        public void run() {
            com.navdy.hud.mfi.IAPControlMessageProcessor.this.bProcessControlMessage(this.val$message, this.val$session, this.val$data);
        }
    }

    public abstract void bProcessControlMessage(com.navdy.hud.mfi.iAPProcessor.iAPMessage iapmessage, int i, byte[] bArr);

    public IAPControlMessageProcessor(com.navdy.hud.mfi.iAPProcessor.iAPMessage[] messages, com.navdy.hud.mfi.iAPProcessor processor) {
        this.miAPProcessor = processor;
        if (messages != null) {
            for (com.navdy.hud.mfi.iAPProcessor.iAPMessage message : messages) {
                this.miAPProcessor.registerControlMessageProcessor(message, this);
            }
        }
    }

    public void processControlMessage(com.navdy.hud.mfi.iAPProcessor.iAPMessage message, int session, byte[] data) {
        this.mSerialExecutor.submit(new com.navdy.hud.mfi.IAPControlMessageProcessor.Anon1(message, session, data));
    }
}
