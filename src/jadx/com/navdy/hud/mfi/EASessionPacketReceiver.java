package com.navdy.hud.mfi;

public interface EASessionPacketReceiver {
    void queue(com.navdy.hud.mfi.EASessionPacket eASessionPacket);
}
