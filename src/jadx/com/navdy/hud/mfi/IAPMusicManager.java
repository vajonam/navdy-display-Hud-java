package com.navdy.hud.mfi;

public class IAPMusicManager extends com.navdy.hud.mfi.IAPControlMessageProcessor {
    private static final java.lang.String TAG = com.navdy.hud.mfi.IAPMusicManager.class.getSimpleName();
    public static com.navdy.hud.mfi.iAPProcessor.iAPMessage[] mMessages = {com.navdy.hud.mfi.iAPProcessor.iAPMessage.NowPlayingUpdate};
    private com.navdy.hud.mfi.NowPlayingUpdate aggregatedNowPlayingUpdate = new com.navdy.hud.mfi.NowPlayingUpdate();
    private com.navdy.hud.mfi.IAPNowPlayingUpdateListener mNowPlayingUpdateListener;

    public enum MediaItemAttributes {
        MediaItemPersistentIdentifier(0),
        MediaItemTitle(1),
        MediaItemPlaybackDurationInMilliSeconds(4),
        MediaItemAlbumTitle(6),
        MediaItemAlbumTrackNumber(7),
        MediaItemAlbumTrackCount(8),
        MediaItemArtist(12),
        MediaItemGenre(16),
        MediaItemArtworkFileTransferIdentifier(26);
        
        int paramIndex;

        private MediaItemAttributes(int index) {
            this.paramIndex = index;
        }

        public int getParam() {
            return this.paramIndex;
        }
    }

    public enum MediaKey {
        Siri,
        PlayPause,
        Next,
        Previous
    }

    public enum NowPlayingUpdatesParameters {
        MediaItemAttributes,
        PlaybackAttributes
    }

    public enum PlaybackAttributes {
        PlaybackStatus(0),
        PlaybackElapsedTimeInMilliseconds(1),
        PlaybackShuffleMode(5),
        PlaybackRepeatMode(6),
        PlaybackAppName(7),
        PlaybackAppBundleId(16);
        
        int paramIndex;

        private PlaybackAttributes(int index) {
            this.paramIndex = index;
        }

        public int getParam() {
            return this.paramIndex;
        }
    }

    public IAPMusicManager(com.navdy.hud.mfi.iAPProcessor processor) {
        super(mMessages, processor);
    }

    public void setNowPlayingUpdateListener(com.navdy.hud.mfi.IAPNowPlayingUpdateListener nowPlayingUpdateListener) {
        this.mNowPlayingUpdateListener = nowPlayingUpdateListener;
    }

    public void startNowPlayingUpdates() {
        com.navdy.hud.mfi.iAPProcessor.IAP2SessionMessage message = new com.navdy.hud.mfi.iAPProcessor.IAP2SessionMessage(com.navdy.hud.mfi.iAPProcessor.iAPMessage.StartNowPlayingUpdates);
        com.navdy.hud.mfi.iAPProcessor.IAP2ParamsCreator mediaItemProperties = new com.navdy.hud.mfi.iAPProcessor.IAP2ParamsCreator();
        for (com.navdy.hud.mfi.IAPMusicManager.MediaItemAttributes attribute : com.navdy.hud.mfi.IAPMusicManager.MediaItemAttributes.values()) {
            mediaItemProperties.addNone(attribute.getParam());
        }
        message.addBlob((java.lang.Enum) com.navdy.hud.mfi.IAPMusicManager.NowPlayingUpdatesParameters.MediaItemAttributes, mediaItemProperties.toBytes());
        com.navdy.hud.mfi.iAPProcessor.IAP2ParamsCreator playbackProperties = new com.navdy.hud.mfi.iAPProcessor.IAP2ParamsCreator();
        for (com.navdy.hud.mfi.IAPMusicManager.PlaybackAttributes attribute2 : com.navdy.hud.mfi.IAPMusicManager.PlaybackAttributes.values()) {
            playbackProperties.addNone(attribute2.getParam());
        }
        message.addBlob((java.lang.Enum) com.navdy.hud.mfi.IAPMusicManager.NowPlayingUpdatesParameters.PlaybackAttributes, playbackProperties.toBytes());
        this.miAPProcessor.sendControlMessage(message);
    }

    public void stopNowPlayingUpdates() {
        this.miAPProcessor.sendControlMessage(new com.navdy.hud.mfi.iAPProcessor.IAP2SessionMessage(com.navdy.hud.mfi.iAPProcessor.iAPMessage.StopNowPlayingUpdates));
    }

    public void bProcessControlMessage(com.navdy.hud.mfi.iAPProcessor.iAPMessage message, int session, byte[] data) {
        com.navdy.hud.mfi.iAPProcessor.IAP2Params params = com.navdy.hud.mfi.iAPProcessor.parse(data);
        switch (message) {
            case NowPlayingUpdate:
                com.navdy.hud.mfi.NowPlayingUpdate update = parseNowPlayingUpdate(params);
                android.util.Log.d(TAG, "Parsed " + update);
                mergeNowPlayingUpdate(update);
                if (this.mNowPlayingUpdateListener != null) {
                    this.mNowPlayingUpdateListener.onNowPlayingUpdate(this.aggregatedNowPlayingUpdate);
                    return;
                }
                return;
            default:
                return;
        }
    }

    private static com.navdy.hud.mfi.NowPlayingUpdate parseNowPlayingUpdate(com.navdy.hud.mfi.iAPProcessor.IAP2Params params) {
        com.navdy.hud.mfi.NowPlayingUpdate nowPlayingUpdate = new com.navdy.hud.mfi.NowPlayingUpdate();
        if (params.hasParam((java.lang.Enum) com.navdy.hud.mfi.IAPMusicManager.NowPlayingUpdatesParameters.MediaItemAttributes)) {
            com.navdy.hud.mfi.iAPProcessor.IAP2Params mediaItemAttributesParams = new com.navdy.hud.mfi.iAPProcessor.IAP2Params(params.getBlob((java.lang.Enum) com.navdy.hud.mfi.IAPMusicManager.NowPlayingUpdatesParameters.MediaItemAttributes), 0);
            if (mediaItemAttributesParams.hasParam(com.navdy.hud.mfi.IAPMusicManager.MediaItemAttributes.MediaItemPersistentIdentifier.getParam())) {
                nowPlayingUpdate.mediaItemPersistentIdentifier = mediaItemAttributesParams.getUInt64(com.navdy.hud.mfi.IAPMusicManager.MediaItemAttributes.MediaItemPersistentIdentifier.getParam());
            }
            if (mediaItemAttributesParams.hasParam(com.navdy.hud.mfi.IAPMusicManager.MediaItemAttributes.MediaItemTitle.getParam())) {
                nowPlayingUpdate.mediaItemTitle = mediaItemAttributesParams.getUTF8(com.navdy.hud.mfi.IAPMusicManager.MediaItemAttributes.MediaItemTitle.getParam());
            }
            if (mediaItemAttributesParams.hasParam(com.navdy.hud.mfi.IAPMusicManager.MediaItemAttributes.MediaItemAlbumTrackCount.getParam())) {
                nowPlayingUpdate.mediaItemAlbumTrackCount = mediaItemAttributesParams.getUInt16(com.navdy.hud.mfi.IAPMusicManager.MediaItemAttributes.MediaItemAlbumTrackCount.getParam());
            }
            if (mediaItemAttributesParams.hasParam(com.navdy.hud.mfi.IAPMusicManager.MediaItemAttributes.MediaItemAlbumTrackNumber.getParam())) {
                nowPlayingUpdate.mediaItemAlbumTrackNumber = mediaItemAttributesParams.getUInt16(com.navdy.hud.mfi.IAPMusicManager.MediaItemAttributes.MediaItemAlbumTrackNumber.getParam());
            }
            if (mediaItemAttributesParams.hasParam(com.navdy.hud.mfi.IAPMusicManager.MediaItemAttributes.MediaItemArtist.getParam())) {
                nowPlayingUpdate.mediaItemArtist = mediaItemAttributesParams.getUTF8(com.navdy.hud.mfi.IAPMusicManager.MediaItemAttributes.MediaItemArtist.getParam());
            }
            if (mediaItemAttributesParams.hasParam(com.navdy.hud.mfi.IAPMusicManager.MediaItemAttributes.MediaItemAlbumTitle.getParam())) {
                nowPlayingUpdate.mediaItemAlbumTitle = mediaItemAttributesParams.getUTF8(com.navdy.hud.mfi.IAPMusicManager.MediaItemAttributes.MediaItemAlbumTitle.getParam());
            }
            if (mediaItemAttributesParams.hasParam(com.navdy.hud.mfi.IAPMusicManager.MediaItemAttributes.MediaItemArtworkFileTransferIdentifier.getParam())) {
                nowPlayingUpdate.mediaItemArtworkFileTransferIdentifier = mediaItemAttributesParams.getUInt8(com.navdy.hud.mfi.IAPMusicManager.MediaItemAttributes.MediaItemArtworkFileTransferIdentifier.getParam());
            }
            if (mediaItemAttributesParams.hasParam(com.navdy.hud.mfi.IAPMusicManager.MediaItemAttributes.MediaItemGenre.getParam())) {
                nowPlayingUpdate.mediaItemGenre = mediaItemAttributesParams.getUTF8(com.navdy.hud.mfi.IAPMusicManager.MediaItemAttributes.MediaItemGenre.getParam());
            }
            if (mediaItemAttributesParams.hasParam(com.navdy.hud.mfi.IAPMusicManager.MediaItemAttributes.MediaItemPlaybackDurationInMilliSeconds.getParam())) {
                nowPlayingUpdate.mediaItemPlaybackDurationInMilliseconds = mediaItemAttributesParams.getUInt32(com.navdy.hud.mfi.IAPMusicManager.MediaItemAttributes.MediaItemPlaybackDurationInMilliSeconds.getParam());
            }
        }
        if (params.hasParam((java.lang.Enum) com.navdy.hud.mfi.IAPMusicManager.NowPlayingUpdatesParameters.PlaybackAttributes)) {
            com.navdy.hud.mfi.iAPProcessor.IAP2Params playbackAttributesParams = new com.navdy.hud.mfi.iAPProcessor.IAP2Params(params.getBlob((java.lang.Enum) com.navdy.hud.mfi.IAPMusicManager.NowPlayingUpdatesParameters.PlaybackAttributes), 0);
            if (playbackAttributesParams.hasParam(com.navdy.hud.mfi.IAPMusicManager.PlaybackAttributes.PlaybackStatus.getParam())) {
                nowPlayingUpdate.mPlaybackStatus = (com.navdy.hud.mfi.NowPlayingUpdate.PlaybackStatus) playbackAttributesParams.getEnum(com.navdy.hud.mfi.NowPlayingUpdate.PlaybackStatus.class, com.navdy.hud.mfi.IAPMusicManager.PlaybackAttributes.PlaybackStatus.getParam());
            }
            if (playbackAttributesParams.hasParam(com.navdy.hud.mfi.IAPMusicManager.PlaybackAttributes.PlaybackElapsedTimeInMilliseconds.getParam())) {
                nowPlayingUpdate.mPlaybackElapsedTimeMilliseconds = playbackAttributesParams.getUInt32(com.navdy.hud.mfi.IAPMusicManager.PlaybackAttributes.PlaybackElapsedTimeInMilliseconds.getParam());
            }
            if (playbackAttributesParams.hasParam(com.navdy.hud.mfi.IAPMusicManager.PlaybackAttributes.PlaybackAppName.getParam())) {
                nowPlayingUpdate.mAppName = playbackAttributesParams.getUTF8(com.navdy.hud.mfi.IAPMusicManager.PlaybackAttributes.PlaybackAppName.getParam());
            }
            if (playbackAttributesParams.hasParam(com.navdy.hud.mfi.IAPMusicManager.PlaybackAttributes.PlaybackAppBundleId.getParam())) {
                nowPlayingUpdate.mAppBundleId = playbackAttributesParams.getUTF8(com.navdy.hud.mfi.IAPMusicManager.PlaybackAttributes.PlaybackAppBundleId.getParam());
            }
            if (playbackAttributesParams.hasParam(com.navdy.hud.mfi.IAPMusicManager.PlaybackAttributes.PlaybackShuffleMode.getParam())) {
                nowPlayingUpdate.playbackShuffle = (com.navdy.hud.mfi.NowPlayingUpdate.PlaybackShuffle) playbackAttributesParams.getEnum(com.navdy.hud.mfi.NowPlayingUpdate.PlaybackShuffle.class, com.navdy.hud.mfi.IAPMusicManager.PlaybackAttributes.PlaybackShuffleMode.getParam());
            }
            if (playbackAttributesParams.hasParam(com.navdy.hud.mfi.IAPMusicManager.PlaybackAttributes.PlaybackRepeatMode.getParam())) {
                nowPlayingUpdate.playbackRepeat = (com.navdy.hud.mfi.NowPlayingUpdate.PlaybackRepeat) playbackAttributesParams.getEnum(com.navdy.hud.mfi.NowPlayingUpdate.PlaybackRepeat.class, com.navdy.hud.mfi.IAPMusicManager.PlaybackAttributes.PlaybackRepeatMode.getParam());
            }
        }
        return nowPlayingUpdate;
    }

    private void mergeNowPlayingUpdate(com.navdy.hud.mfi.NowPlayingUpdate incomingUpdate) {
        if (incomingUpdate.mediaItemTitle != null) {
            this.aggregatedNowPlayingUpdate.mediaItemTitle = incomingUpdate.mediaItemTitle;
        }
        if (incomingUpdate.mediaItemAlbumTitle != null) {
            this.aggregatedNowPlayingUpdate.mediaItemAlbumTitle = incomingUpdate.mediaItemAlbumTitle;
        }
        if (incomingUpdate.mediaItemArtist != null) {
            this.aggregatedNowPlayingUpdate.mediaItemArtist = incomingUpdate.mediaItemArtist;
        }
        if (incomingUpdate.mediaItemGenre != null) {
            this.aggregatedNowPlayingUpdate.mediaItemGenre = incomingUpdate.mediaItemGenre;
        }
        if (incomingUpdate.mPlaybackStatus != null) {
            this.aggregatedNowPlayingUpdate.mPlaybackStatus = incomingUpdate.mPlaybackStatus;
        }
        if (incomingUpdate.playbackShuffle != null) {
            this.aggregatedNowPlayingUpdate.playbackShuffle = incomingUpdate.playbackShuffle;
        }
        if (incomingUpdate.playbackRepeat != null) {
            this.aggregatedNowPlayingUpdate.playbackRepeat = incomingUpdate.playbackRepeat;
        }
        if (incomingUpdate.mediaItemPersistentIdentifier != null) {
            this.aggregatedNowPlayingUpdate.mediaItemPersistentIdentifier = incomingUpdate.mediaItemPersistentIdentifier;
        }
        if (incomingUpdate.mediaItemPlaybackDurationInMilliseconds > 0) {
            this.aggregatedNowPlayingUpdate.mediaItemPlaybackDurationInMilliseconds = incomingUpdate.mediaItemPlaybackDurationInMilliseconds;
        }
        if (incomingUpdate.mediaItemAlbumTrackNumber >= 0) {
            this.aggregatedNowPlayingUpdate.mediaItemAlbumTrackNumber = incomingUpdate.mediaItemAlbumTrackNumber;
        }
        if (incomingUpdate.mediaItemAlbumTrackCount >= 0) {
            this.aggregatedNowPlayingUpdate.mediaItemAlbumTrackCount = incomingUpdate.mediaItemAlbumTrackCount;
        }
        if (incomingUpdate.mPlaybackElapsedTimeMilliseconds >= 0) {
            this.aggregatedNowPlayingUpdate.mPlaybackElapsedTimeMilliseconds = incomingUpdate.mPlaybackElapsedTimeMilliseconds;
        }
        if (!android.text.TextUtils.isEmpty(incomingUpdate.mAppName)) {
            this.aggregatedNowPlayingUpdate.mAppName = incomingUpdate.mAppName;
        }
        if (!android.text.TextUtils.isEmpty(incomingUpdate.mAppBundleId)) {
            this.aggregatedNowPlayingUpdate.mAppBundleId = incomingUpdate.mAppBundleId;
        }
        if (incomingUpdate.mediaItemArtworkFileTransferIdentifier >= 128 && incomingUpdate.mediaItemArtworkFileTransferIdentifier <= 255) {
            this.aggregatedNowPlayingUpdate.mediaItemArtworkFileTransferIdentifier = incomingUpdate.mediaItemArtworkFileTransferIdentifier;
        }
    }

    public void onKeyDown(com.navdy.hud.mfi.IAPMusicManager.MediaKey key) {
        this.miAPProcessor.onKeyDown((java.lang.Enum) key);
    }

    public void onKeyUp(com.navdy.hud.mfi.IAPMusicManager.MediaKey key) {
        this.miAPProcessor.onKeyUp((java.lang.Enum) key);
    }

    public void onKeyDown(int key) {
        this.miAPProcessor.onKeyDown(key);
    }

    public void onKeyUp(int key) {
        this.miAPProcessor.onKeyUp(key);
    }
}
