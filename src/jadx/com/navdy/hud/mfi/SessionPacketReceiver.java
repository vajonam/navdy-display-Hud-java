package com.navdy.hud.mfi;

public interface SessionPacketReceiver {
    void queue(com.navdy.hud.mfi.SessionPacket sessionPacket);
}
