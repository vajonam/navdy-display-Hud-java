package com.navdy.hud.mfi;

public class EASession {
    /* access modifiers changed from: private */
    public static final java.lang.String TAG = com.navdy.hud.mfi.EASession.class.getSimpleName();
    private okio.Buffer buffer;
    /* access modifiers changed from: private */
    public boolean closed;
    /* access modifiers changed from: private */
    public final java.lang.Object dataAvailable = new java.lang.Object();
    /* access modifiers changed from: private */
    public com.navdy.hud.mfi.iAPProcessor iAPProcessor;
    private java.lang.String macAddress;
    /* access modifiers changed from: private */
    public com.navdy.hud.mfi.EASession mySelf = this;
    private java.lang.String name;
    private java.lang.String protocol;
    private int sessionIdentifier;

    class BlockingInputStream extends java.io.InputStream {
        private java.io.InputStream source;

        public BlockingInputStream(java.io.InputStream source2) {
            this.source = source2;
        }

        public int read() throws java.io.IOException {
            int read;
            synchronized (com.navdy.hud.mfi.EASession.this.dataAvailable) {
                waitForAvailable();
                read = this.source.read();
            }
            return read;
        }

        public int read(byte[] buffer) throws java.io.IOException {
            int read;
            synchronized (com.navdy.hud.mfi.EASession.this.dataAvailable) {
                waitForAvailable();
                read = this.source.read(buffer);
            }
            return read;
        }

        public int read(byte[] buffer, int byteOffset, int byteCount) throws java.io.IOException {
            int read;
            synchronized (com.navdy.hud.mfi.EASession.this.dataAvailable) {
                waitForAvailable();
                read = this.source.read(buffer, byteOffset, byteCount);
            }
            return read;
        }

        private void waitForAvailable() throws java.io.IOException {
            if (this.source.available() == 0) {
                try {
                    com.navdy.hud.mfi.EASession.this.dataAvailable.wait();
                } catch (java.lang.InterruptedException e) {
                }
            }
        }
    }

    class OutStream extends java.io.OutputStream {
        private int FLUSH_DELAY = 250;
        private java.lang.Runnable autoFlush = new com.navdy.hud.mfi.EASession.OutStream.Anon1();
        private java.util.List<byte[]> frames = new java.util.ArrayList();
        private android.os.Handler handler = new android.os.Handler(android.os.Looper.getMainLooper());

        class Anon1 implements java.lang.Runnable {
            Anon1() {
            }

            public void run() {
                try {
                    com.navdy.hud.mfi.EASession.OutStream.this.flush();
                } catch (java.io.IOException e) {
                }
            }
        }

        OutStream() {
        }

        public void close() throws java.io.IOException {
            flush();
        }

        public synchronized void flush() throws java.io.IOException {
            this.handler.removeCallbacks(this.autoFlush);
            byte[] packet = assembleFrames();
            if (!com.navdy.hud.mfi.EASession.this.closed && packet != null) {
                com.navdy.hud.mfi.EASession.this.iAPProcessor.queue(new com.navdy.hud.mfi.EASessionPacket(com.navdy.hud.mfi.EASession.this.getSessionIdentifier(), packet));
            }
        }

        private byte[] assembleFrames() {
            byte[] buffer = null;
            int totalLength = 0;
            for (int i = 0; i < this.frames.size(); i++) {
                totalLength += ((byte[]) this.frames.get(i)).length;
            }
            if (totalLength > 0) {
                if (this.frames.size() == 1) {
                    buffer = (byte[]) this.frames.get(0);
                } else {
                    int offset = 0;
                    buffer = new byte[totalLength];
                    for (int i2 = 0; i2 < this.frames.size(); i2++) {
                        byte[] src = (byte[]) this.frames.get(i2);
                        java.lang.System.arraycopy(src, 0, buffer, offset, src.length);
                        offset += src.length;
                    }
                }
                this.frames.clear();
            }
            return buffer;
        }

        public synchronized void write(byte[] buffer) throws java.io.IOException {
            if (android.util.Log.isLoggable(com.navdy.hud.mfi.EASession.TAG, 3)) {
                android.util.Log.d(com.navdy.hud.mfi.EASession.TAG, java.lang.String.format("%s: OutStream.write: %d bytes: %s, '%s'", new java.lang.Object[]{com.navdy.hud.mfi.EASession.this.mySelf, java.lang.Integer.valueOf(buffer.length), com.navdy.hud.mfi.Utils.bytesToHex(buffer, true), com.navdy.hud.mfi.Utils.toASCII(buffer)}));
            }
            this.frames.add(buffer);
            this.handler.removeCallbacks(this.autoFlush);
            this.handler.postDelayed(this.autoFlush, (long) this.FLUSH_DELAY);
        }

        public void write(byte[] buffer, int offset, int count) throws java.io.IOException {
            write(java.util.Arrays.copyOfRange(buffer, offset, count));
        }

        public void write(int oneByte) throws java.io.IOException {
            write(new byte[]{(byte) oneByte});
        }
    }

    public java.lang.String toString() {
        return java.lang.String.format("%s{%s/%d}", new java.lang.Object[]{super.toString(), this.protocol, java.lang.Integer.valueOf(this.sessionIdentifier)});
    }

    public EASession(com.navdy.hud.mfi.iAPProcessor iAPProcessor2, java.lang.String macAddress2, java.lang.String name2, int sessionIdentifier2, java.lang.String protocol2) {
        this.iAPProcessor = iAPProcessor2;
        this.macAddress = macAddress2;
        this.name = name2;
        this.sessionIdentifier = sessionIdentifier2;
        this.protocol = protocol2;
        if (android.util.Log.isLoggable(TAG, 3)) {
            android.util.Log.d(TAG, java.lang.String.format("%s: created", new java.lang.Object[]{this.mySelf}));
        }
        this.buffer = new okio.Buffer();
    }

    public java.lang.String getMacAddress() {
        return this.macAddress;
    }

    public java.lang.String getName() {
        return this.name;
    }

    public int getSessionIdentifier() {
        return this.sessionIdentifier;
    }

    public java.lang.String getProtocol() {
        return this.protocol;
    }

    public void close() {
        android.util.Log.d(TAG, "Closing EASession streams");
        synchronized (this.dataAvailable) {
            this.closed = true;
            this.buffer.close();
            this.dataAvailable.notify();
        }
    }

    public void queue(byte[] data) {
        if (android.util.Log.isLoggable(TAG, 3)) {
            android.util.Log.d(TAG, java.lang.String.format("%s: buffer.write: %d bytes: %s, '%s'", new java.lang.Object[]{this.mySelf, java.lang.Integer.valueOf(data.length), com.navdy.hud.mfi.Utils.bytesToHex(data, true), com.navdy.hud.mfi.Utils.toASCII(data)}));
        }
        synchronized (this.dataAvailable) {
            this.buffer.write(data);
            this.dataAvailable.notify();
        }
    }

    public java.io.InputStream getInputStream() {
        return new com.navdy.hud.mfi.EASession.BlockingInputStream(this.buffer.inputStream());
    }

    public java.io.OutputStream getOutputStream() {
        return new com.navdy.hud.mfi.EASession.OutStream();
    }
}
