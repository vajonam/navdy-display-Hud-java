package com.navdy.hud.mfi;

public class FileTransferSession {
    public static final int DATA_OFFSET = 2;
    private static final int INDEX_OF_COMMAND = 1;
    private static final int INDEX_OF_ID = 0;
    public static final int MESSAGE_SIZE = 2;
    private static final java.lang.String TAG = com.navdy.hud.mfi.FileTransferSession.class.getSimpleName();
    private boolean mCanceled = false;
    private byte[] mFileData;
    private int mFileReceivedSoFar;
    private long mFileSize;
    public int mFileTransferIdentifier;
    com.navdy.hud.mfi.IIAPFileTransferManager mFileTransferManager;
    private boolean mFinished = false;
    private boolean mStarted = false;

    enum FileTransferCommand {
        COMMAND_DATA(0),
        COMMAND_START(1),
        COMMAND_CANCEL(2),
        COMMAND_PAUSE(3),
        COMMAND_SETUP(4),
        COMMAND_SUCCESS(5),
        COMMAND_FAILURE(6),
        COMMAND_LAST_DATA(64),
        COMMAND_FIRST_DATA(128),
        COMMAND_FIRST_AND_ONLY_DATA(192);
        
        public final int value;

        private FileTransferCommand(int value2) {
            this.value = value2;
        }

        public static com.navdy.hud.mfi.FileTransferSession.FileTransferCommand fromInt(int val) {
            if (val >= COMMAND_DATA.value && val <= COMMAND_FAILURE.value) {
                return values()[val];
            }
            if (val == COMMAND_LAST_DATA.value) {
                return COMMAND_LAST_DATA;
            }
            if (val == COMMAND_FIRST_DATA.value) {
                return COMMAND_FIRST_DATA;
            }
            if (val == COMMAND_FIRST_AND_ONLY_DATA.value) {
                return COMMAND_FIRST_AND_ONLY_DATA;
            }
            return null;
        }
    }

    public FileTransferSession(int mFileTransferIdentifier2, com.navdy.hud.mfi.IIAPFileTransferManager fileTransferManager) {
        this.mFileTransferManager = fileTransferManager;
        this.mFileTransferIdentifier = mFileTransferIdentifier2;
    }

    public void bProcessFileTransferMessage(byte[] data) {
        com.navdy.hud.mfi.FileTransferSession.FileTransferCommand command = com.navdy.hud.mfi.FileTransferSession.FileTransferCommand.fromInt(data[1] & 255);
        if (command == com.navdy.hud.mfi.FileTransferSession.FileTransferCommand.COMMAND_SETUP || this.mStarted) {
            switch (command) {
                case COMMAND_SETUP:
                    this.mStarted = true;
                    this.mFileSize = com.navdy.hud.mfi.Utils.unpackInt64(data, 2);
                    android.util.Log.d(com.navdy.hud.mfi.Utils.MFI_TAG, "Apple Device(F): " + this.mFileTransferIdentifier + " Command : " + command.name() + ", File_Size : " + this.mFileSize);
                    if (this.mFileSize > this.mFileTransferManager.getFileTransferLimit()) {
                        android.util.Log.e(TAG, "File too large, not starting the transfer");
                        sendCommand(com.navdy.hud.mfi.FileTransferSession.FileTransferCommand.COMMAND_CANCEL);
                        return;
                    }
                    this.mFileTransferManager.onFileTransferSetupRequest(this.mFileTransferIdentifier, this.mFileSize);
                    if (this.mFileSize == 0) {
                        sendCommand(com.navdy.hud.mfi.FileTransferSession.FileTransferCommand.COMMAND_CANCEL);
                        this.mFileTransferManager.onSuccess(this.mFileTransferIdentifier, null);
                        return;
                    }
                    return;
                case COMMAND_FIRST_DATA:
                    try {
                        java.lang.System.arraycopy(data, 2, this.mFileData, this.mFileReceivedSoFar, data.length - 2);
                        this.mFileReceivedSoFar += data.length - 2;
                        android.util.Log.d(com.navdy.hud.mfi.Utils.MFI_TAG, "Apple Device(F): " + this.mFileTransferIdentifier + " Command : " + command.name() + ", Received : " + this.mFileReceivedSoFar);
                        return;
                    } catch (Throwable t) {
                        error(t);
                        return;
                    }
                case COMMAND_FIRST_AND_ONLY_DATA:
                    try {
                        java.lang.System.arraycopy(data, 2, this.mFileData, this.mFileReceivedSoFar, data.length - 2);
                        this.mFileReceivedSoFar += data.length - 2;
                        android.util.Log.d(com.navdy.hud.mfi.Utils.MFI_TAG, "Apple Device(F): " + this.mFileTransferIdentifier + " Command : " + command.name() + ", Received : " + this.mFileReceivedSoFar);
                        sendCommand(com.navdy.hud.mfi.FileTransferSession.FileTransferCommand.COMMAND_SUCCESS);
                        this.mFinished = true;
                        this.mFileTransferManager.onSuccess(this.mFileTransferIdentifier, this.mFileData);
                        return;
                    } catch (Throwable t2) {
                        error(t2);
                        return;
                    }
                case COMMAND_DATA:
                    try {
                        java.lang.System.arraycopy(data, 2, this.mFileData, this.mFileReceivedSoFar, data.length - 2);
                        this.mFileReceivedSoFar += data.length - 2;
                        android.util.Log.d(com.navdy.hud.mfi.Utils.MFI_TAG, "Apple Device(F): " + this.mFileTransferIdentifier + " Command : " + command.name() + ", Received : " + this.mFileReceivedSoFar);
                        return;
                    } catch (Throwable t3) {
                        error(t3);
                        return;
                    }
                case COMMAND_LAST_DATA:
                    try {
                        java.lang.System.arraycopy(data, 2, this.mFileData, this.mFileReceivedSoFar, data.length - 2);
                        this.mFileReceivedSoFar += data.length - 2;
                        android.util.Log.d(com.navdy.hud.mfi.Utils.MFI_TAG, "Apple Device(F): " + this.mFileTransferIdentifier + " Command : " + command.name() + ", Received : " + this.mFileReceivedSoFar);
                        if (((long) this.mFileReceivedSoFar) != this.mFileSize) {
                            sendCommand(com.navdy.hud.mfi.FileTransferSession.FileTransferCommand.COMMAND_FAILURE);
                            this.mFinished = true;
                            this.mFileTransferManager.onError(this.mFileTransferIdentifier, new java.io.IOException("Missed packets"));
                            return;
                        }
                        sendCommand(com.navdy.hud.mfi.FileTransferSession.FileTransferCommand.COMMAND_SUCCESS);
                        this.mFinished = true;
                        this.mFileTransferManager.onSuccess(this.mFileTransferIdentifier, this.mFileData);
                        return;
                    } catch (Throwable t4) {
                        error(t4);
                        return;
                    }
                case COMMAND_CANCEL:
                    this.mFinished = true;
                    android.util.Log.d(com.navdy.hud.mfi.Utils.MFI_TAG, "Apple Device(F): " + this.mFileTransferIdentifier + " Command : " + command.name());
                    this.mFileTransferManager.onCanceled(this.mFileTransferIdentifier);
                    return;
                case COMMAND_PAUSE:
                    android.util.Log.d(com.navdy.hud.mfi.Utils.MFI_TAG, "Apple Device(F): " + this.mFileTransferIdentifier + " Command : " + command.name());
                    this.mFileTransferManager.onPaused(this.mFileTransferIdentifier);
                    return;
                default:
                    return;
            }
        } else {
            android.util.Log.e(TAG, "File transfer is not initiated, bad state " + command);
            this.mFileTransferManager.onError(this.mFileTransferIdentifier, new java.lang.IllegalStateException("File transfer not started"));
        }
    }

    private void error(java.lang.Throwable t) {
        this.mFinished = true;
        sendCommand(com.navdy.hud.mfi.FileTransferSession.FileTransferCommand.COMMAND_FAILURE);
        this.mFileTransferManager.onError(this.mFileTransferIdentifier, t);
    }

    private void sendCommand(com.navdy.hud.mfi.FileTransferSession.FileTransferCommand command) {
        this.mFileTransferManager.sendMessage(this.mFileTransferIdentifier, new byte[]{(byte) (this.mFileTransferIdentifier & 255), (byte) command.value});
        android.util.Log.d(com.navdy.hud.mfi.Utils.MFI_TAG, "Accessory(F): " + this.mFileTransferIdentifier + " Command : " + command.name());
    }

    public void release() {
    }

    public synchronized void cancel() {
        this.mCanceled = true;
        if (this.mFileTransferManager != null) {
            sendCommand(com.navdy.hud.mfi.FileTransferSession.FileTransferCommand.COMMAND_CANCEL);
            this.mFinished = true;
        }
    }

    public synchronized void proceed() {
        if (!this.mCanceled) {
            this.mFileData = new byte[((int) this.mFileSize)];
            if (this.mFileTransferManager != null) {
                sendCommand(com.navdy.hud.mfi.FileTransferSession.FileTransferCommand.COMMAND_START);
            }
        }
    }

    public boolean isActive() {
        return this.mStarted && !this.mFinished;
    }
}
