package com.navdy.hud.mfi;

public class LinkLayer implements com.navdy.hud.mfi.SessionPacketReceiver, com.navdy.hud.mfi.LinkPacketReceiver {
    public static final int CONTROL_BYTE_ACK = 64;
    public static final int CONTROL_BYTE_EAK = 32;
    public static final int CONTROL_BYTE_RST = 16;
    public static final int CONTROL_BYTE_SLP = 8;
    public static final int CONTROL_BYTE_SYN = 128;
    public static final int DELAY_MILLIS = 20;
    public static final int MESSAGE_CONNECTION_ENDED = 4;
    public static final int MESSAGE_CONNECTION_STARTED = 3;
    public static final int MESSAGE_LINK_PACKET = 1;
    public static final int MESSAGE_PERIODIC = 0;
    public static final int MESSAGE_SESSION_PACKET = 2;
    public static final java.lang.String TAG = "LinkLayer";
    private static final boolean sLogLinkPacket = true;
    /* access modifiers changed from: private */
    public java.util.concurrent.ConcurrentLinkedQueue<com.navdy.hud.mfi.SessionPacket> controlMessages;
    /* access modifiers changed from: private */
    public com.navdy.hud.mfi.iAPProcessor iAPProcessor;
    private android.os.Handler myHandler;
    /* access modifiers changed from: private */
    public java.util.concurrent.ConcurrentLinkedQueue<com.navdy.hud.mfi.SessionPacket> nonControlMessages;
    /* access modifiers changed from: private */
    public com.navdy.hud.mfi.LinkLayer.PhysicalLayer physicalLayer;
    private java.lang.String remoteAddress;
    private java.lang.String remoteName;

    class Anon1 extends android.os.Handler {
        Anon1(android.os.Looper x0) {
            super(x0);
        }

        public void handleMessage(android.os.Message msg) {
            byte[] data = null;
            int session = 0;
            int messageType = msg.what;
            switch (msg.what) {
                case 0:
                case 2:
                    com.navdy.hud.mfi.SessionPacket sessionPacket = null;
                    if (com.navdy.hud.mfi.LinkLayer.this.controlMessages.peek() != null) {
                        sessionPacket = (com.navdy.hud.mfi.SessionPacket) com.navdy.hud.mfi.LinkLayer.this.controlMessages.poll();
                    } else if (com.navdy.hud.mfi.LinkLayer.this.nonControlMessages.peek() != null) {
                        sessionPacket = (com.navdy.hud.mfi.SessionPacket) com.navdy.hud.mfi.LinkLayer.this.nonControlMessages.poll();
                    }
                    if (sessionPacket != null) {
                        messageType = 2;
                        data = sessionPacket.data;
                        session = sessionPacket.session;
                        break;
                    }
                    break;
                case 1:
                    data = ((com.navdy.hud.mfi.Packet) msg.obj).data;
                    break;
                case 4:
                    com.navdy.hud.mfi.LinkLayer.this.iAPProcessor.linkLost();
                    break;
            }
            if (msg.what == 0) {
                sendEmptyMessageDelayed(0, 20);
            }
            doNativeRunLoop(messageType, data, session);
        }

        private void doNativeRunLoop(int messageType, byte[] data, int session) {
            if (com.navdy.hud.mfi.LinkLayer.this.native_runloop(messageType, data, session) < 0) {
                android.util.Log.i(com.navdy.hud.mfi.LinkLayer.TAG, "runloop detected a disconnect");
                com.navdy.hud.mfi.LinkLayer.this.connectionEnded();
                return;
            }
            transferPackets(1);
            transferPackets(2);
        }

        private void transferPackets(int messageType) {
            while (true) {
                int session = com.navdy.hud.mfi.LinkLayer.this.native_get_message_session();
                byte[] data = com.navdy.hud.mfi.LinkLayer.this.native_pop_message_data(messageType);
                if (data != null) {
                    switch (messageType) {
                        case 1:
                            com.navdy.hud.mfi.LinkLayer.this.logOutGoingLinkPacket(data);
                            com.navdy.hud.mfi.LinkLayer.this.physicalLayer.queue(new com.navdy.hud.mfi.LinkPacket(data));
                            break;
                        case 2:
                            com.navdy.hud.mfi.SessionPacket pkt = new com.navdy.hud.mfi.SessionPacket(session, data);
                            com.navdy.hud.mfi.LinkLayer.this.log("LL->iAP[" + pkt.session + "]", pkt);
                            com.navdy.hud.mfi.LinkLayer.this.iAPProcessor.queue(pkt);
                            break;
                        default:
                            android.util.Log.e(com.navdy.hud.mfi.LinkLayer.TAG, "transferPackets: unsupported message type (" + messageType + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.CLOSE_BRACKET);
                            break;
                    }
                } else {
                    return;
                }
            }
        }
    }

    public interface PhysicalLayer extends com.navdy.hud.mfi.LinkPacketReceiver {
        byte[] getLocalAddress();
    }

    /* access modifiers changed from: private */
    public native int native_get_message_session();

    /* access modifiers changed from: private */
    public native byte[] native_pop_message_data(int i);

    /* access modifiers changed from: private */
    public native int native_runloop(int i, byte[] bArr, int i2);

    public native int native_get_maximum_payload_size();

    public void connect(com.navdy.hud.mfi.LinkLayer.PhysicalLayer physicalLayer2) {
        this.physicalLayer = physicalLayer2;
    }

    public void connect(com.navdy.hud.mfi.iAPProcessor iAPProcessor2) {
        this.iAPProcessor = iAPProcessor2;
    }

    static {
        java.lang.System.loadLibrary("iap2");
    }

    public LinkLayer() {
        start();
    }

    public void start() {
        this.controlMessages = new java.util.concurrent.ConcurrentLinkedQueue<>();
        this.nonControlMessages = new java.util.concurrent.ConcurrentLinkedQueue<>();
        this.myHandler = new com.navdy.hud.mfi.LinkLayer.Anon1(android.os.Looper.getMainLooper());
        this.myHandler.sendEmptyMessageDelayed(0, 20);
    }

    /* access modifiers changed from: private */
    public void logOutGoingLinkPacket(byte[] data) {
        if (data != null && data.length > 7) {
            int packetSequenceNumber = data[5] & 255;
            int ackNumber = data[6] & 255;
            int controlByte = data[4] & 255;
            java.lang.StringBuilder controlBytes = new java.lang.StringBuilder();
            if ((controlByte & 64) != 0) {
                controlBytes.append(" ACK ");
            }
            if ((controlByte & 128) != 0) {
                controlBytes.append(" SYN ");
            }
            if ((controlByte & 32) != 0) {
                controlBytes.append(" EAK ");
            }
            if ((controlByte & 16) != 0) {
                controlBytes.append(" RST ");
            }
            if ((controlByte & 8) != 0) {
                controlBytes.append(" SLP ");
            }
            android.util.Log.d(com.navdy.hud.mfi.Utils.MFI_TAG, "Accessory(L)   :" + packetSequenceNumber + " " + ackNumber + " Size:" + data.length + controlBytes.toString());
        }
    }

    public void stop() {
    }

    private void queuePacket(int messageType, java.lang.Object pkt) {
        this.myHandler.sendMessage(this.myHandler.obtainMessage(messageType, pkt));
    }

    /* access modifiers changed from: private */
    public void log(java.lang.String desc, com.navdy.hud.mfi.Packet pkt) {
        com.navdy.hud.mfi.Utils.logTransfer(TAG, "%s: %s", desc, pkt.data);
    }

    public void queue(com.navdy.hud.mfi.LinkPacket pkt) {
        queuePacket(1, pkt);
    }

    public void queue(com.navdy.hud.mfi.SessionPacket pkt) {
        log("iAP->LL[" + pkt.session + "]", pkt);
        if (pkt.session == this.iAPProcessor.getControlSessionId()) {
            this.controlMessages.add(pkt);
        } else {
            this.nonControlMessages.add(pkt);
        }
        this.myHandler.sendEmptyMessage(2);
    }

    public byte[] getLocalAddress() {
        return this.physicalLayer.getLocalAddress();
    }

    public java.lang.String getRemoteAddress() {
        return this.remoteAddress;
    }

    public java.lang.String getRemoteName() {
        return this.remoteName;
    }

    public void connectionStarted(java.lang.String remoteAddress2, java.lang.String remoteName2) {
        this.remoteAddress = remoteAddress2;
        this.remoteName = remoteName2;
        queuePacket(3, null);
    }

    public void connectionEnded() {
        if (this.myHandler != null) {
            this.myHandler.removeMessages(0);
            this.myHandler.removeMessages(1);
            this.myHandler.removeMessages(2);
        }
        this.controlMessages.clear();
        this.nonControlMessages.clear();
        queuePacket(4, null);
    }
}
