package com.navdy.hud.mfi;

public interface IAPCommunicationsUpdateListener {
    void onCallStateUpdate(com.navdy.hud.mfi.CallStateUpdate callStateUpdate);

    void onCommunicationUpdate(com.navdy.hud.mfi.CommunicationUpdate communicationUpdate);
}
