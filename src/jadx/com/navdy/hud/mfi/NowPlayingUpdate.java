package com.navdy.hud.mfi;

public class NowPlayingUpdate {
    public java.lang.String mAppBundleId;
    public java.lang.String mAppName;
    public long mPlaybackElapsedTimeMilliseconds;
    public com.navdy.hud.mfi.NowPlayingUpdate.PlaybackStatus mPlaybackStatus;
    public java.lang.String mediaItemAlbumTitle;
    public int mediaItemAlbumTrackCount;
    public int mediaItemAlbumTrackNumber;
    public java.lang.String mediaItemArtist;
    public int mediaItemArtworkFileTransferIdentifier;
    public java.lang.String mediaItemGenre;
    public java.math.BigInteger mediaItemPersistentIdentifier;
    public long mediaItemPlaybackDurationInMilliseconds;
    public java.lang.String mediaItemTitle;
    public com.navdy.hud.mfi.NowPlayingUpdate.PlaybackRepeat playbackRepeat;
    public com.navdy.hud.mfi.NowPlayingUpdate.PlaybackShuffle playbackShuffle;

    public enum PlaybackRepeat {
        Off,
        One,
        All
    }

    public enum PlaybackShuffle {
        Off,
        Songs,
        Albums
    }

    public enum PlaybackStatus {
        Stopped,
        Playing,
        Paused,
        SeekForward,
        SeekBackward
    }

    public java.lang.String toString() {
        java.lang.StringBuilder sb = new java.lang.StringBuilder("NowPlayingUpdate{");
        if (this.mediaItemTitle != null) {
            sb.append("title='").append(this.mediaItemTitle).append('\'');
        }
        if (this.mediaItemPlaybackDurationInMilliseconds != 0) {
            sb.append(", duration=").append(this.mediaItemPlaybackDurationInMilliseconds);
        }
        if (this.mediaItemAlbumTitle != null) {
            sb.append(", albumTitle='").append(this.mediaItemAlbumTitle).append('\'');
        }
        if (this.mediaItemAlbumTrackNumber != 0) {
            sb.append(", albumTrackNumber=").append(this.mediaItemAlbumTrackNumber);
        }
        if (this.mediaItemAlbumTrackCount != 0) {
            sb.append(", albumTrackCount=").append(this.mediaItemAlbumTrackCount);
        }
        if (this.mediaItemArtist != null) {
            sb.append(", artist='").append(this.mediaItemArtist).append('\'');
        }
        if (this.mediaItemGenre != null) {
            sb.append(", itemGenre='").append(this.mediaItemGenre).append('\'');
        }
        if (this.mediaItemArtworkFileTransferIdentifier != 0) {
            sb.append(", fileTransferIdentifier=").append(this.mediaItemArtworkFileTransferIdentifier);
        }
        if (this.mPlaybackStatus != null) {
            sb.append(", playbackStatus=").append(this.mPlaybackStatus);
        }
        sb.append(", playbackElapsedTimeMilliseconds=").append(this.mPlaybackElapsedTimeMilliseconds);
        sb.append('}');
        return sb.toString();
    }
}
