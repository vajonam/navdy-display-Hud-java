package com.navdy.hud.mfi;

public class SessionPacket extends com.navdy.hud.mfi.Packet {
    int session;

    public SessionPacket(int session2, byte[] data) {
        super(data);
        this.session = session2;
    }
}
