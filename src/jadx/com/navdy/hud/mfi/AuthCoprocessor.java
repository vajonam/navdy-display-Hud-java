package com.navdy.hud.mfi;

public class AuthCoprocessor {
    private static final int ACCESSORY_CERTIFICATE_DATA_REGISTER = 49;
    private static final int ACCESSORY_CERTIFICATE_LENGTH_REGISTER = 48;
    private static final int ACCESSORY_SIGNATURE_GENERATION = 1;
    private static final int AUTH_ERROR_FLAG = 128;
    private static final int BUS = 0;
    private static final int CHALLENGE_DATA_REGISTER = 33;
    private static final int CHALLENGE_LENGTH_REGISTER = 32;
    private static final int DEVICE_ADDRESS = 17;
    private static final int DEVICE_CERTIFICATE_DATA_REGISTER = 81;
    private static final int DEVICE_CERTIFICATE_LENGTH_REGISTER = 80;
    private static final int DEVICE_CERTIFICATE_VALIDATION = 4;
    private static final int DEVICE_CHALLENGE_GENERATION = 2;
    private static final int DEVICE_SIGNATURE_VALIDATION = 3;
    private static final int ERROR_CODE_REGISTER = 5;
    private static final int PAGE_SIZE = 128;
    private static final int SIGNATURE_DATA_REGISTER = 18;
    private static final int SIGNATURE_LENGTH_REGISTER = 17;
    private static final int STATUS_CONTROL_REGISTER = 16;
    private static final java.lang.String TAG = "AuthCoprocessor";
    private android.content.Context context;
    private boolean crashReportSent = false;
    private com.navdy.hud.mfi.I2C i2c = new com.navdy.hud.mfi.I2C(0);
    private com.navdy.hud.mfi.IAPListener iapListener;

    class Anon1 implements com.navdy.hud.mfi.IAPListener {
        Anon1() {
        }

        public void onCoprocessorStatusCheckFailed(java.lang.String desc, java.lang.String status, java.lang.String errorCode) {
        }

        public void onDeviceAuthenticationSuccess(int retries) {
        }
    }

    public AuthCoprocessor(com.navdy.hud.mfi.IAPListener listener, android.content.Context context2) {
        if (listener == null) {
            this.iapListener = new com.navdy.hud.mfi.AuthCoprocessor.Anon1();
        } else {
            this.iapListener = listener;
        }
        this.context = context2;
    }

    public void open() throws java.io.IOException {
        this.i2c.open();
    }

    public void close() {
        this.i2c.close();
    }

    private void sendCrashReport() {
        if (this.crashReportSent) {
            android.util.Log.e(TAG, "skipping duplicate crash report");
        } else if (this.context == null) {
            android.util.Log.e(TAG, "cannot sent crash report, null context");
        } else {
            try {
                android.content.Intent intent = new android.content.Intent(this.context, java.lang.Class.forName("com.navdy.hud.app.util.CrashReportService"));
                intent.setAction("DumpCrashReport");
                intent.putExtra(com.navdy.hud.app.util.CrashReportService.EXTRA_CRASH_TYPE, "MFI_ERROR");
                this.context.startService(intent);
                this.crashReportSent = true;
            } catch (java.lang.Exception e) {
                android.util.Log.e(TAG, "error in lookup of CrashReportService class", e);
            }
        }
    }

    private void checkStatus(java.lang.String desc) throws java.io.IOException {
        int status = this.i2c.readByte(17, 16);
        int err = this.i2c.readByte(17, 5);
        if ((status & 128) != 0) {
            java.lang.String statusHex = com.navdy.hud.mfi.Utils.bytesToHex(new byte[]{(byte) status});
            java.lang.String errorHex = com.navdy.hud.mfi.Utils.bytesToHex(new byte[]{(byte) err});
            android.util.Log.e(TAG, "auth CP status: " + statusHex);
            android.util.Log.e(TAG, "auth CP error code: " + errorHex);
            sendCrashReport();
            try {
                this.iapListener.onCoprocessorStatusCheckFailed(desc, statusHex, errorHex);
            } catch (Throwable e) {
                android.util.Log.e(TAG, "logging to listener failed", e);
            }
            throw new java.lang.RuntimeException("failed to " + desc);
        }
    }

    private void writePages(int lengthRegister, int dataRegister, byte[] data, java.lang.String desc) throws java.io.IOException {
        this.i2c.writeShort(17, lengthRegister, data.length);
        checkStatus("write " + desc + " length");
        int bytesToWrite = data.length;
        int offset = 0;
        byte[] buf = new byte[128];
        while (bytesToWrite > 0) {
            int len = java.lang.Math.min(bytesToWrite, 128);
            java.lang.System.arraycopy(data, offset, buf, 0, len);
            this.i2c.write(17, dataRegister, buf, 0, buf.length);
            checkStatus("write " + desc);
            dataRegister++;
            offset += len;
            bytesToWrite -= len;
        }
    }

    public byte[] readAccessoryCertificate() throws java.io.IOException {
        byte[] cert = new byte[this.i2c.readShort(17, 48)];
        this.i2c.read(17, 49, cert);
        checkStatus("read accessory certificate");
        return cert;
    }

    public byte[] createAccessorySignature(byte[] challenge) {
        try {
            if (challenge.length != 20) {
                throw new java.lang.RuntimeException("wrong challenge size");
            }
            writePages(32, 33, challenge, "accessory signature");
            this.i2c.writeShort(17, 17, 128);
            this.i2c.writeByte(17, 16, 1);
            checkStatus("generate accessory signature");
            byte[] signature = new byte[this.i2c.readShort(17, 17)];
            this.i2c.read(17, 18, signature);
            checkStatus("read accessory signature");
            return signature;
        } catch (java.lang.Exception e) {
            android.util.Log.e(TAG, "", e);
            return null;
        }
    }

    public byte[] validateDeviceCertificateAndGenerateDeviceChallenge(byte[] certificate) {
        try {
            writePages(80, DEVICE_CERTIFICATE_DATA_REGISTER, certificate, "device certificate");
            this.i2c.writeByte(17, 16, 4);
            checkStatus("validate device certificate");
            this.i2c.writeByte(17, 16, 2);
            checkStatus("generate device challenge");
            byte[] challenge = new byte[this.i2c.readShort(17, 32)];
            this.i2c.read(17, 33, challenge);
            checkStatus("read device challenge");
            return challenge;
        } catch (java.lang.Exception e) {
            android.util.Log.e(TAG, "", e);
            return null;
        }
    }

    public boolean validateDeviceSignature(byte[] signature) {
        try {
            writePages(17, 18, signature, "device signature");
            this.i2c.writeByte(17, 16, 3);
            checkStatus("validate device signature");
            return true;
        } catch (java.lang.Exception e) {
            android.util.Log.e(TAG, "", e);
            return false;
        }
    }
}
