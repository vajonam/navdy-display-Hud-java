package com.navdy.hud.mfi;

public class ByteArrayBuilder {
    java.io.ByteArrayOutputStream bos;
    java.io.DataOutputStream dos;

    public ByteArrayBuilder() {
        this.bos = new java.io.ByteArrayOutputStream();
        this.dos = new java.io.DataOutputStream(this.bos);
    }

    public ByteArrayBuilder(int initialSize) {
        this.bos = new java.io.ByteArrayOutputStream(initialSize);
        this.dos = new java.io.DataOutputStream(this.bos);
    }

    public com.navdy.hud.mfi.ByteArrayBuilder addInt16(int val) throws java.io.IOException {
        this.dos.writeShort(val);
        return this;
    }

    public com.navdy.hud.mfi.ByteArrayBuilder addInt8(int val) throws java.io.IOException {
        this.dos.writeByte(val);
        return this;
    }

    public com.navdy.hud.mfi.ByteArrayBuilder addInt32(int val) throws java.io.IOException {
        this.dos.writeInt(val);
        return this;
    }

    public com.navdy.hud.mfi.ByteArrayBuilder addInt64(long val) throws java.io.IOException {
        this.dos.writeLong(val);
        return this;
    }

    public com.navdy.hud.mfi.ByteArrayBuilder addUTF8(java.lang.String s) throws java.io.IOException {
        this.dos.write(s.getBytes("UTF-8"));
        return this;
    }

    public com.navdy.hud.mfi.ByteArrayBuilder addBlob(byte[] b) throws java.io.IOException {
        this.dos.write(b);
        return this;
    }

    public com.navdy.hud.mfi.ByteArrayBuilder addBlob(byte[] b, int startOffset, int length) throws java.io.IOException {
        this.dos.write(b, startOffset, length);
        return this;
    }

    public byte[] build() {
        return this.bos.toByteArray();
    }

    public int size() {
        return this.bos.size();
    }
}
