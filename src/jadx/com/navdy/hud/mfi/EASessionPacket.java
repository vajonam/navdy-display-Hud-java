package com.navdy.hud.mfi;

public class EASessionPacket extends com.navdy.hud.mfi.Packet {
    int session;

    public EASessionPacket(int session2, byte[] data) {
        super(data);
        this.session = session2;
    }
}
