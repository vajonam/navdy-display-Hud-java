package com.navdy.hud.mfi;

public interface LinkPacketReceiver {
    void queue(com.navdy.hud.mfi.LinkPacket linkPacket);
}
