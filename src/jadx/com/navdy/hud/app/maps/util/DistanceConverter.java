package com.navdy.hud.app.maps.util;

public class DistanceConverter {

    public static class Distance {
        public com.navdy.service.library.events.navigation.DistanceUnit unit;
        public float value;

        /* access modifiers changed from: 0000 */
        public void clear() {
            this.value = 0.0f;
            this.unit = null;
        }

        public java.lang.String getFormattedExtendedDistanceUnit() {
            android.content.res.Resources resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
            switch (this.unit) {
                case DISTANCE_METERS:
                    if (this.value == 1.0f) {
                        return resources.getString(com.navdy.hud.app.R.string.unit_meters_ext_singular);
                    }
                    return resources.getString(com.navdy.hud.app.R.string.unit_meters_ext);
                case DISTANCE_FEET:
                    if (this.value == 1.0f) {
                        return resources.getString(com.navdy.hud.app.R.string.unit_feet_ext_singular);
                    }
                    return resources.getString(com.navdy.hud.app.R.string.unit_feet_ext);
                case DISTANCE_KMS:
                    if (this.value == 1.0f) {
                        return resources.getString(com.navdy.hud.app.R.string.unit_kilometers_ext_singular);
                    }
                    return resources.getString(com.navdy.hud.app.R.string.unit_kilometers_ext);
                case DISTANCE_MILES:
                    if (this.value == 1.0f) {
                        return resources.getString(com.navdy.hud.app.R.string.unit_miles_ext_singular);
                    }
                    return resources.getString(com.navdy.hud.app.R.string.unit_miles_ext);
                default:
                    return "";
            }
        }
    }

    public static void convertToDistance(com.navdy.hud.app.manager.SpeedManager.SpeedUnit unit, float value, com.navdy.hud.app.maps.util.DistanceConverter.Distance distance) {
        distance.clear();
        switch (unit) {
            case KILOMETERS_PER_HOUR:
                if (value >= 400.0f) {
                    distance.value = value / 1000.0f;
                    distance.value = (float) com.navdy.hud.app.maps.here.HereMapUtil.roundToN((double) distance.value, 10);
                    distance.unit = com.navdy.service.library.events.navigation.DistanceUnit.DISTANCE_KMS;
                    return;
                }
                distance.value = value;
                distance.unit = com.navdy.service.library.events.navigation.DistanceUnit.DISTANCE_METERS;
                return;
            default:
                if (value >= 160.934f) {
                    distance.value = value / 1609.34f;
                    distance.value = (float) com.navdy.hud.app.maps.here.HereMapUtil.roundToN((double) distance.value, 10);
                    distance.unit = com.navdy.service.library.events.navigation.DistanceUnit.DISTANCE_MILES;
                    return;
                }
                distance.value = (float) ((int) (3.28084f * value));
                distance.unit = com.navdy.service.library.events.navigation.DistanceUnit.DISTANCE_FEET;
                return;
        }
    }

    public static float convertToMeters(float value, com.navdy.service.library.events.navigation.DistanceUnit unit) {
        switch (unit) {
            case DISTANCE_FEET:
                return value / 3.28084f;
            case DISTANCE_KMS:
                return value * 1000.0f;
            case DISTANCE_MILES:
                return value * 1609.34f;
            default:
                return value;
        }
    }
}
