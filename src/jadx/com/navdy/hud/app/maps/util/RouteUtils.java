package com.navdy.hud.app.maps.util;

public class RouteUtils {
    private static final double SCALE_FACTOR = 1048576.0d;
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.maps.util.RouteUtils.class);

    public static final class Bounds {
        public double height;
        public double width;

        Bounds(double width2, double height2) {
            this.width = width2;
            this.height = height2;
        }
    }

    public static java.util.List<java.lang.Float> simplify(java.util.List<com.here.android.mpa.common.GeoCoordinate> points) {
        com.navdy.hud.app.maps.util.RouteUtils.Bounds bounds = getLatLngBounds(points);
        return simplify(points, 2000.0d * java.lang.Math.max(bounds.width, bounds.height));
    }

    public static java.util.List<java.lang.Float> simplify(java.util.List<com.here.android.mpa.common.GeoCoordinate> points, double tolerance) {
        return simplifyRadialDistance(points, tolerance * tolerance);
    }

    static java.util.List<java.lang.Float> simplifyRadialDistance(java.util.List<com.here.android.mpa.common.GeoCoordinate> points, double sqTolerance) {
        if (points == null || points.size() == 0) {
            return null;
        }
        float prevLatitude = (float) ((com.here.android.mpa.common.GeoCoordinate) points.get(0)).getLatitude();
        float prevLongitude = (float) ((com.here.android.mpa.common.GeoCoordinate) points.get(0)).getLongitude();
        java.util.ArrayList<java.lang.Float> newLatLongs = new java.util.ArrayList<>();
        newLatLongs.add(java.lang.Float.valueOf(prevLatitude));
        newLatLongs.add(java.lang.Float.valueOf(prevLongitude));
        int len = points.size();
        com.here.android.mpa.common.GeoCoordinate geo = (com.here.android.mpa.common.GeoCoordinate) points.get(0);
        float pointLongitude = (float) geo.getLongitude();
        newLatLongs.add(java.lang.Float.valueOf((float) geo.getLatitude()));
        newLatLongs.add(java.lang.Float.valueOf(pointLongitude));
        for (int i = 1; i < len - 1; i++) {
            com.here.android.mpa.common.GeoCoordinate geo2 = (com.here.android.mpa.common.GeoCoordinate) points.get(i);
            float pointLatitude = (float) geo2.getLatitude();
            float pointLongitude2 = (float) geo2.getLongitude();
            double dx = ((double) (pointLatitude - prevLatitude)) * SCALE_FACTOR;
            double dy = ((double) (pointLongitude2 - prevLongitude)) * SCALE_FACTOR;
            if ((dx * dx) + (dy * dy) > sqTolerance) {
                newLatLongs.add(java.lang.Float.valueOf(pointLatitude));
                newLatLongs.add(java.lang.Float.valueOf(pointLongitude2));
                prevLatitude = pointLatitude;
                prevLongitude = pointLongitude2;
            }
        }
        com.here.android.mpa.common.GeoCoordinate geo3 = (com.here.android.mpa.common.GeoCoordinate) points.get(len - 1);
        float pointLongitude3 = (float) geo3.getLongitude();
        newLatLongs.add(java.lang.Float.valueOf((float) geo3.getLatitude()));
        newLatLongs.add(java.lang.Float.valueOf(pointLongitude3));
        return newLatLongs;
    }

    static com.navdy.hud.app.maps.util.RouteUtils.Bounds getLatLngBounds(java.util.List<com.here.android.mpa.common.GeoCoordinate> points) {
        double lowestLat = ((com.here.android.mpa.common.GeoCoordinate) points.get(0)).getLatitude();
        double highestLat = lowestLat;
        double lowestLong = ((com.here.android.mpa.common.GeoCoordinate) points.get(0)).getLongitude();
        double highestLong = lowestLong;
        for (com.here.android.mpa.common.GeoCoordinate geo : points) {
            double latitude = geo.getLatitude();
            double longitude = geo.getLongitude();
            lowestLat = java.lang.Math.min(latitude, lowestLat);
            highestLat = java.lang.Math.max(latitude, highestLat);
            lowestLong = java.lang.Math.min(longitude, lowestLong);
            highestLong = java.lang.Math.max(longitude, highestLong);
        }
        return new com.navdy.hud.app.maps.util.RouteUtils.Bounds(highestLong - lowestLong, highestLat - lowestLat);
    }

    public static java.lang.String formatEtaMinutes(android.content.res.Resources resources, int minutes) {
        if (minutes < 60) {
            return resources.getString(com.navdy.hud.app.R.string.eta_time_min, new java.lang.Object[]{java.lang.Integer.valueOf(minutes)});
        } else if (minutes < 1440) {
            int hours = minutes / 60;
            int minutes2 = minutes - (hours * 60);
            if (minutes2 > 0) {
                return resources.getString(com.navdy.hud.app.R.string.eta_time_hour, new java.lang.Object[]{java.lang.Integer.valueOf(hours), java.lang.Integer.valueOf(minutes2)});
            }
            return resources.getString(com.navdy.hud.app.R.string.eta_time_hour_no_min, new java.lang.Object[]{java.lang.Integer.valueOf(hours)});
        } else {
            int days = minutes / com.navdy.hud.app.ui.component.homescreen.HomeScreenConstants.MINUTES_DAY;
            int hours2 = (minutes - (days * com.navdy.hud.app.ui.component.homescreen.HomeScreenConstants.MINUTES_DAY)) / 60;
            if (hours2 > 0) {
                return resources.getString(com.navdy.hud.app.R.string.eta_time_day, new java.lang.Object[]{java.lang.Integer.valueOf(days), java.lang.Integer.valueOf(hours2)});
            }
            return resources.getString(com.navdy.hud.app.R.string.eta_time_day_no_hour, new java.lang.Object[]{java.lang.Integer.valueOf(days)});
        }
    }
}
