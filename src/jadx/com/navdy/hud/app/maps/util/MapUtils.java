package com.navdy.hud.app.maps.util;

public final class MapUtils {
    public static final float INVALID_DISTANCE = -1.0f;
    private static final double MAX_ALTITUDE = 10000.0d;
    private static final double MAX_LATITUDE = 90.0d;
    private static final double MAX_LONGITUDE = 180.0d;
    private static final double MIN_ALTITUDE = -10000.0d;
    private static final double MIN_LATITUDE = -90.0d;
    private static final double MIN_LONGITUDE = -180.0d;
    public static final int SECONDS_PER_HOUR = 3600;
    public static final int SECONDS_PER_MINUTE = 60;

    private MapUtils() {
    }

    public static java.lang.String formatTime(int etaDifference) {
        if (etaDifference >= 3600) {
            return java.lang.String.format("%d:%02d", new java.lang.Object[]{java.lang.Integer.valueOf(etaDifference / 3600), java.lang.Integer.valueOf(etaDifference / 60)});
        }
        return java.lang.String.format("%d", new java.lang.Object[]{java.lang.Integer.valueOf(etaDifference / 60)});
    }

    public static boolean sanitizeCoords(double latitude, double longitude) {
        return latitude <= MAX_LATITUDE && latitude >= MIN_LATITUDE && longitude <= MAX_LONGITUDE && longitude >= MIN_LONGITUDE;
    }

    public static com.here.android.mpa.common.GeoCoordinate sanitizeCoords(com.here.android.mpa.common.GeoCoordinate geoCoordinate) {
        double latitude = geoCoordinate.getLatitude();
        double longitude = geoCoordinate.getLongitude();
        double altitude = geoCoordinate.getAltitude();
        if (!sanitizeCoords(latitude, longitude)) {
            return null;
        }
        if (altitude >= MIN_ALTITUDE && altitude <= MAX_ALTITUDE) {
            return geoCoordinate;
        }
        geoCoordinate.setAltitude(0.0d);
        return geoCoordinate;
    }

    public static int getMinuteCeiling(int time) {
        return time % 60 == 0 ? time : (time + 60) - (time % 60);
    }

    public static int getIconForDestination(com.navdy.service.library.events.destination.Destination.FavoriteType type) {
        return com.navdy.hud.app.R.drawable.icon_pin_route_circle;
    }

    public static float distanceBetween(com.navdy.service.library.events.location.LatLong latLong, com.navdy.service.library.events.location.LatLong other) {
        if (latLong == null || other == null) {
            return -1.0f;
        }
        float[] results = new float[1];
        android.location.Location.distanceBetween(latLong.latitude.doubleValue(), latLong.longitude.doubleValue(), other.latitude.doubleValue(), other.longitude.doubleValue(), results);
        return results[0];
    }
}
