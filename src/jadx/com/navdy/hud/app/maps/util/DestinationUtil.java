package com.navdy.hud.app.maps.util;

public class DestinationUtil {
    public static java.util.List<com.navdy.hud.app.ui.component.destination.DestinationParcelable> convert(android.content.Context context, java.util.List<com.navdy.service.library.events.destination.Destination> destinations, int defaultColor, int deselectedColor, boolean extendedUnits) {
        int iconRes;
        int color;
        com.navdy.hud.app.maps.here.HereLocationFixManager hereLocationFixManager = com.navdy.hud.app.maps.here.HereMapsManager.getInstance().getLocationFixManager();
        com.here.android.mpa.common.GeoCoordinate coordinate = null;
        com.navdy.service.library.events.location.LatLong currentPosition = null;
        if (hereLocationFixManager != null) {
            coordinate = hereLocationFixManager.getLastGeoCoordinate();
        }
        if (coordinate != null) {
            currentPosition = new com.navdy.service.library.events.location.LatLong(java.lang.Double.valueOf(coordinate.getLatitude()), java.lang.Double.valueOf(coordinate.getLongitude()));
        }
        java.util.ArrayList arrayList = new java.util.ArrayList(destinations.size());
        for (int i = 0; i < destinations.size(); i++) {
            com.navdy.service.library.events.destination.Destination destination = (com.navdy.service.library.events.destination.Destination) destinations.get(i);
            float distance = -1.0f;
            double displayLat = 0.0d;
            double displayLng = 0.0d;
            double navLat = 0.0d;
            double navLng = 0.0d;
            java.lang.String distanceSubTitle = null;
            if (destination.navigation_position != null) {
                navLat = destination.navigation_position.latitude.doubleValue();
                navLng = destination.navigation_position.longitude.doubleValue();
                if (destination.navigation_position.latitude.doubleValue() != 0.0d && destination.navigation_position.longitude.doubleValue() != 0.0d) {
                    distance = com.navdy.hud.app.maps.util.MapUtils.distanceBetween(destination.navigation_position, currentPosition);
                } else if (!(destination.display_position == null || destination.display_position.latitude.doubleValue() == 0.0d || destination.display_position.longitude.doubleValue() == 0.0d)) {
                    distance = com.navdy.hud.app.maps.util.MapUtils.distanceBetween(destination.display_position, currentPosition);
                }
            }
            java.lang.String distanceStr = null;
            if (distance != -1.0f) {
                com.navdy.hud.app.maps.util.DistanceConverter.Distance formattedDistance = new com.navdy.hud.app.maps.util.DistanceConverter.Distance();
                com.navdy.hud.app.maps.util.DistanceConverter.convertToDistance(com.navdy.hud.app.manager.SpeedManager.getInstance().getSpeedUnit(), distance, formattedDistance);
                distanceStr = com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.getFormattedDistance(formattedDistance.value, formattedDistance.unit, extendedUnits);
                int index = distanceStr.indexOf(" ");
                if (index != -1) {
                    distanceSubTitle = "<b>" + distanceStr.substring(0, index) + "</b>" + distanceStr.substring(index);
                }
            }
            if (destination.display_position != null) {
                displayLat = destination.display_position.latitude.doubleValue();
                displayLng = destination.display_position.longitude.doubleValue();
            }
            com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.PlaceTypeResourceHolder resourceHolder = com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.getPlaceTypeHolder(destination.place_type);
            if (resourceHolder != null) {
                iconRes = resourceHolder.iconRes;
                color = android.support.v4.content.ContextCompat.getColor(context, resourceHolder.colorRes);
            } else {
                iconRes = com.navdy.hud.app.R.drawable.icon_mm_places_2;
                color = defaultColor;
            }
            com.navdy.hud.app.ui.component.destination.DestinationParcelable parcelable = new com.navdy.hud.app.ui.component.destination.DestinationParcelable(0, destination.destination_title, destination.destination_subtitle, false, distanceSubTitle, true, destination.full_address, navLat, navLng, displayLat, displayLng, iconRes, 0, color, deselectedColor, com.navdy.hud.app.ui.component.destination.DestinationParcelable.DestinationType.DESTINATION, destination.place_type);
            parcelable.setIdentifier(destination.identifier);
            parcelable.setPlaceId(destination.place_id);
            parcelable.distanceStr = distanceStr;
            parcelable.setContacts(com.navdy.hud.app.framework.contacts.ContactUtil.fromContacts(destination.contacts));
            parcelable.setPhoneNumbers(com.navdy.hud.app.framework.contacts.ContactUtil.fromPhoneNumbers(destination.phoneNumbers));
            arrayList.add(parcelable);
        }
        return arrayList;
    }
}
