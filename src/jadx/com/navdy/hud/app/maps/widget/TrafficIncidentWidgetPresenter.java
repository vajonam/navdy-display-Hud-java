package com.navdy.hud.app.maps.widget;

public class TrafficIncidentWidgetPresenter extends com.navdy.hud.app.view.DashboardWidgetPresenter {
    /* access modifiers changed from: private */
    public static android.os.Handler handler = new android.os.Handler(android.os.Looper.getMainLooper());
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger("TrafficIncidentWidget");
    private com.squareup.otto.Bus bus = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getBus();
    private boolean registered;
    /* access modifiers changed from: private */
    public android.widget.TextView textView;

    class Anon1 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.hud.app.maps.MapEvents.DisplayTrafficIncident val$event;

        /* renamed from: com.navdy.hud.app.maps.widget.TrafficIncidentWidgetPresenter$Anon1$Anon1 reason: collision with other inner class name */
        class C0028Anon1 implements java.lang.Runnable {
            final /* synthetic */ android.text.SpannableStringBuilder val$builder;

            C0028Anon1(android.text.SpannableStringBuilder spannableStringBuilder) {
                this.val$builder = spannableStringBuilder;
            }

            public void run() {
                if (com.navdy.hud.app.maps.widget.TrafficIncidentWidgetPresenter.this.textView != null) {
                    com.navdy.hud.app.maps.widget.TrafficIncidentWidgetPresenter.this.textView.setTextAppearance(com.navdy.hud.app.HudApplication.getAppContext(), com.navdy.hud.app.R.style.incident_2);
                    com.navdy.hud.app.maps.widget.TrafficIncidentWidgetPresenter.this.textView.setText(this.val$builder);
                }
            }
        }

        Anon1(com.navdy.hud.app.maps.MapEvents.DisplayTrafficIncident displayTrafficIncident) {
            this.val$event = displayTrafficIncident;
        }

        public void run() {
            android.text.SpannableStringBuilder builder = new android.text.SpannableStringBuilder();
            android.graphics.Bitmap bitmap = null;
            if (this.val$event.icon != null) {
                bitmap = this.val$event.icon.getBitmap(40, 40);
            }
            builder.append(this.val$event.type.name());
            builder.setSpan(new android.text.style.StyleSpan(1), 0, builder.length(), 33);
            builder.append(" ");
            int start = builder.length();
            if (bitmap != null) {
                android.text.style.ImageSpan imageSpan = new android.text.style.ImageSpan(com.navdy.hud.app.HudApplication.getAppContext(), bitmap);
                builder.append("IMG");
                builder.setSpan(imageSpan, start, builder.length(), 33);
                builder.append(" ");
            }
            if (!android.text.TextUtils.isEmpty(this.val$event.title)) {
                builder.append(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.OPEN_BRACKET);
                builder.append(this.val$event.title);
                builder.append(") ,");
            }
            if (!android.text.TextUtils.isEmpty(this.val$event.description)) {
                builder.append(this.val$event.description);
                builder.append(" , ");
            }
            if (!android.text.TextUtils.isEmpty(this.val$event.affectedStreet)) {
                builder.append("street=");
                builder.append(this.val$event.affectedStreet);
                builder.append(" , ");
            }
            if (this.val$event.distanceToIncident != -1) {
                builder.append("dist=");
                builder.append(java.lang.String.valueOf(this.val$event.distanceToIncident));
                builder.append(" , ");
            }
            if (this.val$event.updated != null) {
                builder.append(java.lang.String.valueOf(java.util.concurrent.TimeUnit.MILLISECONDS.toMinutes(new java.util.Date().getTime() - this.val$event.updated.getTime())) + " minutes ago");
            }
            com.navdy.hud.app.maps.widget.TrafficIncidentWidgetPresenter.handler.post(new com.navdy.hud.app.maps.widget.TrafficIncidentWidgetPresenter.Anon1.C0028Anon1(builder));
        }
    }

    public android.graphics.drawable.Drawable getDrawable() {
        return null;
    }

    /* access modifiers changed from: protected */
    public void updateGauge() {
    }

    public java.lang.String getWidgetIdentifier() {
        return com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.TRAFFIC_INCIDENT_GAUGE_ID;
    }

    public java.lang.String getWidgetName() {
        return null;
    }

    public void setView(com.navdy.hud.app.view.DashboardWidgetView dashboardWidgetView, android.os.Bundle arguments) {
        if (dashboardWidgetView != null) {
            boolean isActive = false;
            if (arguments != null) {
                isActive = arguments.getBoolean(com.navdy.hud.app.view.DashboardWidgetPresenter.EXTRA_IS_ACTIVE, false);
            }
            dashboardWidgetView.setContentView((int) com.navdy.hud.app.R.layout.maps_traffic_incident_widget);
            this.textView = (android.widget.TextView) dashboardWidgetView.findViewById(com.navdy.hud.app.R.id.incidentInfo);
            setText();
            super.setView(dashboardWidgetView, arguments);
            if (!isActive) {
                sLogger.v("not active");
                unregister();
            } else if (!this.registered) {
                this.bus.register(this);
                this.registered = true;
                sLogger.v("register");
            } else {
                sLogger.v("already register");
            }
        } else {
            unregister();
            this.textView = null;
            super.setView(dashboardWidgetView, arguments);
        }
    }

    @com.squareup.otto.Subscribe
    public void onDisplayTrafficIncident(com.navdy.hud.app.maps.MapEvents.DisplayTrafficIncident event) {
        try {
            if (this.textView != null) {
                sLogger.v("DisplayTrafficIncident type:" + event.type + ", category:" + event.category + ", title:" + event.title + ", description:" + event.description + ", affectedStreet:" + event.affectedStreet + ", distanceToIncident:" + event.distanceToIncident + ", reported:" + event.reported + ", updated:" + event.updated + ", icon:" + event.icon);
                switch (event.type) {
                    case NORMAL:
                        this.textView.setTextAppearance(com.navdy.hud.app.HudApplication.getAppContext(), com.navdy.hud.app.R.style.incident_1);
                        this.textView.setText("NORMAL");
                        return;
                    case INACTIVE:
                        this.textView.setTextAppearance(com.navdy.hud.app.HudApplication.getAppContext(), com.navdy.hud.app.R.style.incident_1);
                        this.textView.setText("INACTIVE");
                        return;
                    case FAILED:
                        this.textView.setTextAppearance(com.navdy.hud.app.HudApplication.getAppContext(), com.navdy.hud.app.R.style.incident_1);
                        this.textView.setText("FAILED");
                        return;
                    default:
                        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.maps.widget.TrafficIncidentWidgetPresenter.Anon1(event), 1);
                        return;
                }
                sLogger.e(t);
            }
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }

    private void setText() {
        this.textView.setTextAppearance(com.navdy.hud.app.HudApplication.getAppContext(), com.navdy.hud.app.R.style.incident_1);
        if (!com.navdy.hud.app.maps.here.HereMapsManager.getInstance().isInitialized() || !com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().isTrafficUpdaterRunning()) {
            this.textView.setText("INACTIVE");
        } else {
            this.textView.setText("NORMAL");
        }
    }

    private void unregister() {
        if (this.registered) {
            sLogger.v("unregister");
            this.bus.unregister(this);
            this.registered = false;
        }
    }
}
