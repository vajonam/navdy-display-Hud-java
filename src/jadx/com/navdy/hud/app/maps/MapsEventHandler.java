package com.navdy.hud.app.maps;

public final class MapsEventHandler {
    private static final boolean VERBOSE = com.navdy.hud.app.BuildConfig.DEBUG;
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.maps.MapsEventHandler.class);
    private static final com.navdy.hud.app.maps.MapsEventHandler sSingleton = new com.navdy.hud.app.maps.MapsEventHandler();
    @javax.inject.Inject
    com.squareup.otto.Bus bus;
    @javax.inject.Inject
    com.navdy.hud.app.service.ConnectionHandler connectionHandler;
    @javax.inject.Inject
    com.navdy.hud.app.profile.DriverProfileManager mDriverProfileManager;
    @javax.inject.Inject
    protected android.content.SharedPreferences sharedPreferences;

    public static final com.navdy.hud.app.maps.MapsEventHandler getInstance() {
        return sSingleton;
    }

    private MapsEventHandler() {
        mortar.Mortar.inject(com.navdy.hud.app.HudApplication.getAppContext(), this);
        this.bus.register(this);
    }

    @com.squareup.otto.Subscribe
    public void onConnectionStatusChange(com.navdy.service.library.events.connection.ConnectionStateChange event) {
        handleConnectionStateChange(event);
    }

    @com.squareup.otto.Subscribe
    public void onPlaceSearchRequest(com.navdy.service.library.events.places.PlacesSearchRequest event) {
        com.navdy.hud.app.maps.here.HerePlacesManager.handlePlacesSearchRequest(event);
    }

    @com.squareup.otto.Subscribe
    public void onAutoCompleteRequest(com.navdy.service.library.events.places.AutoCompleteRequest event) {
        com.navdy.hud.app.maps.here.HerePlacesManager.handleAutoCompleteRequest(event);
    }

    @com.squareup.otto.Subscribe
    public void onRouteSearchRequest(com.navdy.service.library.events.navigation.NavigationRouteRequest event) {
        com.navdy.hud.app.maps.here.HereRouteManager.handleRouteRequest(event);
    }

    @com.squareup.otto.Subscribe
    public void onRouteCancelRequest(com.navdy.service.library.events.navigation.NavigationRouteCancelRequest event) {
        com.navdy.hud.app.maps.here.HereRouteManager.handleRouteCancelRequest(event, false);
    }

    @com.squareup.otto.Subscribe
    public void onNavigationRequest(com.navdy.service.library.events.navigation.NavigationSessionRequest event) {
        handleNavigationRequest(event);
    }

    @com.squareup.otto.Subscribe
    public void onGetNavigationSessionState(com.navdy.service.library.events.navigation.GetNavigationSessionState event) {
        if (!com.navdy.hud.app.maps.here.HereMapsManager.getInstance().isInitialized()) {
            sLogger.i("onGetNavigationSessionState:engine not ready");
            this.bus.post(new com.navdy.hud.app.event.RemoteEvent(new com.navdy.service.library.events.navigation.NavigationSessionStatusEvent(com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_ENGINE_NOT_READY, null, null, null, null)));
            return;
        }
        com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().postNavigationSessionStatusEvent(true);
    }

    @com.squareup.otto.Subscribe
    public void onGetRouteManeuverRequest(com.navdy.service.library.events.navigation.RouteManeuverRequest routeManeuverRequest) {
        try {
            com.navdy.hud.app.maps.here.HereRouteManager.handleRouteManeuverRequest(routeManeuverRequest);
        } catch (Throwable t) {
            sendEventToClient(new com.navdy.service.library.events.navigation.RouteManeuverResponse(com.navdy.service.library.events.RequestStatus.REQUEST_SERVICE_ERROR, t.toString(), null));
            sLogger.e(t);
        }
    }

    public void sendEventToClient(com.squareup.wire.Message message) {
        try {
            com.navdy.service.library.device.RemoteDevice remoteDevice = this.connectionHandler.getRemoteDevice();
            if (remoteDevice != null) {
                remoteDevice.postEvent(message);
            } else if (VERBOSE) {
                sLogger.w("event not sent to client:");
            }
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }

    public com.squareup.otto.Bus getBus() {
        return this.bus;
    }

    public android.content.SharedPreferences getSharedPreferences() {
        return this.sharedPreferences;
    }

    public com.navdy.service.library.events.preferences.NavigationPreferences getNavigationPreferences() {
        return this.mDriverProfileManager.getCurrentProfile().getNavigationPreferences();
    }

    private void handleConnectionStateChange(com.navdy.service.library.events.connection.ConnectionStateChange event) {
        if (!com.navdy.hud.app.maps.here.HereMapsManager.getInstance().isInitialized()) {
            sLogger.v("handleConnectionStateChange:engine not initiazed");
            return;
        }
        boolean connected = false;
        if (event.state == com.navdy.service.library.events.connection.ConnectionStateChange.ConnectionState.CONNECTION_VERIFIED) {
            connected = true;
            this.bus.post(new com.navdy.hud.app.event.RemoteEvent(new com.navdy.service.library.events.navigation.NavigationSessionStatusEvent(com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_ENGINE_READY, null, null, null, null)));
        }
        com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().handleConnectionState(connected);
    }

    private void handleNavigationRequest(com.navdy.service.library.events.navigation.NavigationSessionRequest request) {
        if (!com.navdy.hud.app.maps.here.HereMapsManager.getInstance().isInitialized()) {
            sendEventToClient(new com.navdy.service.library.events.navigation.NavigationSessionResponse(com.navdy.service.library.events.RequestStatus.REQUEST_NOT_READY, com.navdy.hud.app.HudApplication.getAppContext().getString(com.navdy.hud.app.R.string.map_engine_not_ready), request.newState, request.routeId));
        } else {
            com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().handleNavigationSessionRequest(request);
        }
    }
}
