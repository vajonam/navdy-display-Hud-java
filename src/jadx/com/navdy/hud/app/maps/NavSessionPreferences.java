package com.navdy.hud.app.maps;

public class NavSessionPreferences {
    public boolean spokenTurnByTurn;

    public NavSessionPreferences(com.navdy.service.library.events.preferences.NavigationPreferences prefs) {
        setDefault(prefs);
    }

    public void setDefault(com.navdy.service.library.events.preferences.NavigationPreferences prefs) {
        this.spokenTurnByTurn = java.lang.Boolean.TRUE.equals(prefs.spokenTurnByTurn);
    }
}
