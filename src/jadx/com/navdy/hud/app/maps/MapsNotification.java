package com.navdy.hud.app.maps;

public class MapsNotification {
    private static final java.lang.String MAP_ENGINE_NOT_INIT_ID = "maps-no-init";
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.maps.MapsNotification.class);

    public static void showMapsEngineNotInitializedToast() {
        java.lang.String str;
        sLogger.d("maps engine not init toast");
        android.content.res.Resources resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        android.os.Bundle bundle = new android.os.Bundle();
        bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_TIMEOUT, 2000);
        bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_IMAGE, com.navdy.hud.app.R.drawable.icon_mm_map);
        com.here.android.mpa.common.OnEngineInitListener.Error error = com.navdy.hud.app.maps.here.HereMapsManager.getInstance().getError();
        if (error == null) {
            str = "";
        } else {
            str = error.name();
        }
        bundle.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_2, resources.getString(com.navdy.hud.app.R.string.map_engine_not_ready));
        bundle.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_3, str);
        com.navdy.hud.app.framework.toast.ToastManager.getInstance().addToast(new com.navdy.hud.app.framework.toast.ToastManager.ToastParams(MAP_ENGINE_NOT_INIT_ID, bundle, null, true, false));
    }
}
