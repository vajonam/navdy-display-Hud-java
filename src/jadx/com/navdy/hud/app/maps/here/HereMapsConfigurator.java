package com.navdy.hud.app.maps.here;

class HereMapsConfigurator {
    private static final com.navdy.hud.app.maps.here.HereMapsConfigurator sInstance = new com.navdy.hud.app.maps.here.HereMapsConfigurator();
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.maps.here.HereMapsConfigurator.class);
    private volatile boolean isAlreadyConfigured;

    HereMapsConfigurator() {
    }

    public static com.navdy.hud.app.maps.here.HereMapsConfigurator getInstance() {
        return sInstance;
    }

    /* access modifiers changed from: 0000 */
    public synchronized void updateMapsConfig() {
        if (this.isAlreadyConfigured) {
            sLogger.w("MWConfig is already configured");
        } else {
            com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
            android.content.Context context = com.navdy.hud.app.HudApplication.getAppContext();
            java.lang.String latestHereMapConfigFolder = com.navdy.hud.app.storage.PathManager.getInstance().getLatestHereMapsConfigPath();
            sLogger.i("MWConfig: starting");
            java.util.List<java.lang.String> hereMapsConfigFolders = com.navdy.hud.app.storage.PathManager.getInstance().getHereMapsConfigDirs();
            com.navdy.hud.app.util.PackagedResource mwConfigLatest = new com.navdy.hud.app.util.PackagedResource("mwconfig_client", latestHereMapConfigFolder);
            try {
                long l1 = android.os.SystemClock.elapsedRealtime();
                mwConfigLatest.updateFromResources(context, com.navdy.hud.app.R.raw.mwconfig_hud, com.navdy.hud.app.R.raw.mwconfig_hud_md5);
                removeOldHereMapsFolders(hereMapsConfigFolders, latestHereMapConfigFolder);
                sLogger.i("MWConfig: time took " + (android.os.SystemClock.elapsedRealtime() - l1));
                this.isAlreadyConfigured = true;
            } catch (Throwable throwable) {
                sLogger.e("MWConfig error", throwable);
            }
            com.navdy.service.library.util.IOUtils.checkIntegrity(context, latestHereMapConfigFolder, com.navdy.hud.app.R.raw.here_mwconfig_integrity);
        }
        return;
    }

    /* access modifiers changed from: 0000 */
    public void removeMWConfigFolder() {
        com.navdy.service.library.util.IOUtils.deleteDirectory(com.navdy.hud.app.HudApplication.getAppContext(), new java.io.File(com.navdy.hud.app.storage.PathManager.getInstance().getLatestHereMapsConfigPath()));
    }

    private void removeOldHereMapsFolders(java.util.List<java.lang.String> hereMapsConfigFolders, java.lang.String latestHereMapConfigFolder) {
        for (java.lang.String hereMapsConfigFolder : hereMapsConfigFolders) {
            if (!android.text.TextUtils.equals(hereMapsConfigFolder, latestHereMapConfigFolder)) {
                com.navdy.service.library.util.IOUtils.deleteDirectory(com.navdy.hud.app.HudApplication.getAppContext(), new java.io.File(hereMapsConfigFolder));
            }
        }
    }
}
