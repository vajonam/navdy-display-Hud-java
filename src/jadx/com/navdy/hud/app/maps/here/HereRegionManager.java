package com.navdy.hud.app.maps.here;

public class HereRegionManager {
    private static final long REGION_CHECK_INTERVAL = 900000;
    private static final com.navdy.hud.app.maps.here.HereRegionManager sInstance = new com.navdy.hud.app.maps.here.HereRegionManager();
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.maps.here.HereRegionManager.class);
    @javax.inject.Inject
    com.squareup.otto.Bus bus;
    private final java.lang.Runnable checkRegionRunnable = new com.navdy.hud.app.maps.here.HereRegionManager.Anon1();
    /* access modifiers changed from: private */
    public com.navdy.hud.app.maps.MapEvents.RegionEvent currentRegionEvent;
    private final android.os.Handler handler;
    private final com.navdy.hud.app.maps.here.HereMapsManager hereMapsManager;

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            com.navdy.hud.app.maps.here.HereRegionManager.this.checkRegion();
        }
    }

    class Anon2 implements com.here.android.mpa.search.ResultListener<com.here.android.mpa.search.Location> {
        Anon2() {
        }

        public void onCompleted(com.here.android.mpa.search.Location location, com.here.android.mpa.search.ErrorCode errorCode) {
            try {
                if (errorCode != com.here.android.mpa.search.ErrorCode.NONE) {
                    com.navdy.hud.app.maps.here.HereRegionManager.sLogger.e("reverseGeoCode:onComplete: Error[" + errorCode.name() + "]");
                } else if (location == null) {
                    com.navdy.hud.app.maps.here.HereRegionManager.sLogger.e("reverseGeoCode:onComplete: address is null");
                } else {
                    com.here.android.mpa.search.Address address = location.getAddress();
                    if (address != null) {
                        com.navdy.hud.app.maps.MapEvents.RegionEvent regionEvent = new com.navdy.hud.app.maps.MapEvents.RegionEvent(address.getState(), address.getCountryName());
                        if (!regionEvent.equals(com.navdy.hud.app.maps.here.HereRegionManager.this.currentRegionEvent)) {
                            com.navdy.hud.app.maps.here.HereRegionManager.sLogger.v("" + regionEvent);
                            com.navdy.hud.app.maps.here.HereRegionManager.this.currentRegionEvent = regionEvent;
                            com.navdy.hud.app.maps.here.HereRegionManager.this.bus.post(com.navdy.hud.app.maps.here.HereRegionManager.this.currentRegionEvent);
                        }
                    }
                }
            } catch (Throwable t) {
                com.navdy.hud.app.maps.here.HereRegionManager.sLogger.e(t);
                com.navdy.hud.app.util.CrashReporter.getInstance().reportNonFatalException(t);
            }
        }
    }

    public static com.navdy.hud.app.maps.here.HereRegionManager getInstance() {
        return sInstance;
    }

    private HereRegionManager() {
        mortar.Mortar.inject(com.navdy.hud.app.HudApplication.getAppContext(), this);
        this.hereMapsManager = com.navdy.hud.app.maps.here.HereMapsManager.getInstance();
        this.handler = new android.os.Handler(android.os.Looper.getMainLooper());
        this.handler.postDelayed(this.checkRegionRunnable, REGION_CHECK_INTERVAL);
    }

    /* access modifiers changed from: private */
    public void checkRegion() {
        try {
            com.here.android.mpa.common.GeoPosition geoPosition = this.hereMapsManager.getLastGeoPosition();
            if (geoPosition == null) {
                sLogger.v("invalid checkRegion call with unknown geoPosition");
                return;
            }
            com.here.android.mpa.search.ErrorCode error = new com.here.android.mpa.search.ReverseGeocodeRequest2(geoPosition.getCoordinate()).execute(new com.navdy.hud.app.maps.here.HereRegionManager.Anon2());
            if (error != com.here.android.mpa.search.ErrorCode.NONE) {
                sLogger.e("reverseGeoCode:execute: Error[" + error.name() + "]");
            }
            this.handler.postDelayed(this.checkRegionRunnable, REGION_CHECK_INTERVAL);
        } catch (Throwable t) {
            sLogger.e(t);
            com.navdy.hud.app.util.CrashReporter.getInstance().reportNonFatalException(t);
        } finally {
            this.handler.postDelayed(this.checkRegionRunnable, REGION_CHECK_INTERVAL);
        }
    }
}
