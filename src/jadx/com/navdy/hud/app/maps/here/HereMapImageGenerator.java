package com.navdy.hud.app.maps.here;

public class HereMapImageGenerator {
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.maps.here.HereMapImageGenerator.class);
    private static final com.navdy.hud.app.maps.here.HereMapImageGenerator singleton = new com.navdy.hud.app.maps.here.HereMapImageGenerator();
    /* access modifiers changed from: private */
    public android.os.Handler handler = new android.os.Handler(android.os.Looper.getMainLooper());
    private final com.here.android.mpa.mapping.Map map = new com.here.android.mpa.mapping.Map();
    private final com.here.android.mpa.mapping.MapOffScreenRenderer mapOffScreenRenderer = new com.here.android.mpa.mapping.MapOffScreenRenderer(com.navdy.hud.app.HudApplication.getAppContext());
    private boolean renderRunning;
    private java.lang.Object waitForRender = new java.lang.Object();

    class Anon1 implements com.here.android.mpa.common.OnScreenCaptureListener {
        final /* synthetic */ com.navdy.hud.app.maps.here.HereMapImageGenerator.MapGeneratorParams val$mapGeneratorParams;

        Anon1(com.navdy.hud.app.maps.here.HereMapImageGenerator.MapGeneratorParams mapGeneratorParams) {
            this.val$mapGeneratorParams = mapGeneratorParams;
        }

        public void onScreenCaptured(android.graphics.Bitmap bitmap) {
            try {
                com.navdy.hud.app.maps.here.HereMapImageGenerator.this.stopOffscreenRenderer();
                com.navdy.hud.app.maps.here.HereMapImageGenerator.sLogger.e("onScreenCaptured bitmap:" + bitmap);
                com.navdy.hud.app.maps.here.HereMapImageGenerator.this.saveFileToDisk(this.val$mapGeneratorParams, bitmap);
            } catch (Throwable t) {
                com.navdy.hud.app.maps.here.HereMapImageGenerator.sLogger.e("onScreenCaptured", t);
            }
        }
    }

    class Anon2 implements java.lang.Runnable {
        final /* synthetic */ android.graphics.Bitmap val$bitmap;
        final /* synthetic */ com.navdy.hud.app.maps.here.HereMapImageGenerator.MapGeneratorParams val$mapGeneratorParams;

        class Anon1 implements java.lang.Runnable {
            Anon1() {
            }

            public void run() {
                com.navdy.hud.app.maps.here.HereMapImageGenerator.Anon2.this.val$mapGeneratorParams.callback.result(com.navdy.hud.app.maps.here.HereMapImageGenerator.Anon2.this.val$bitmap);
            }
        }

        Anon2(android.graphics.Bitmap bitmap, com.navdy.hud.app.maps.here.HereMapImageGenerator.MapGeneratorParams mapGeneratorParams) {
            this.val$bitmap = bitmap;
            this.val$mapGeneratorParams = mapGeneratorParams;
        }

        public void run() {
            java.io.FileOutputStream fout = null;
            try {
                if (this.val$bitmap != null) {
                    java.lang.String path = com.navdy.hud.app.maps.here.HereMapImageGenerator.this.getMapImageFile(this.val$mapGeneratorParams.id);
                    java.io.FileOutputStream fout2 = new java.io.FileOutputStream(path);
                    try {
                        this.val$bitmap.compress(android.graphics.Bitmap.CompressFormat.JPEG, 100, fout2);
                        com.navdy.service.library.util.IOUtils.fileSync(fout2);
                        com.navdy.service.library.util.IOUtils.closeStream(fout2);
                        fout = null;
                        com.navdy.hud.app.maps.here.HereMapImageGenerator.sLogger.v("bitmap saved:" + path);
                    } catch (Throwable th) {
                        th = th;
                        fout = fout2;
                        com.navdy.service.library.util.IOUtils.fileSync(fout);
                        com.navdy.service.library.util.IOUtils.closeStream(fout);
                        throw th;
                    }
                } else {
                    com.navdy.hud.app.maps.here.HereMapImageGenerator.sLogger.e("bitmap not saved:");
                }
                if (this.val$mapGeneratorParams.callback != null) {
                    com.navdy.hud.app.maps.here.HereMapImageGenerator.this.handler.post(new com.navdy.hud.app.maps.here.HereMapImageGenerator.Anon2.Anon1());
                }
                com.navdy.service.library.util.IOUtils.fileSync(fout);
                com.navdy.service.library.util.IOUtils.closeStream(fout);
            } catch (Throwable th2) {
                t = th2;
            }
        }
    }

    public interface ICallback {
        void result(android.graphics.Bitmap bitmap);
    }

    public static class MapGeneratorParams {
        public com.navdy.hud.app.maps.here.HereMapImageGenerator.ICallback callback;
        public int height;
        public java.lang.String id;
        public double latitude;
        public double longitude;
        public int width;
        public double zoomLevel;
    }

    public static com.navdy.hud.app.maps.here.HereMapImageGenerator getInstance() {
        return singleton;
    }

    private HereMapImageGenerator() {
        this.map.setMapScheme("terrain.day");
        this.map.setExtrudedBuildingsVisible(false);
        this.map.setFadingAnimations(false);
        this.map.getPositionIndicator().setVisible(false);
        this.mapOffScreenRenderer.setMap(this.map);
        this.mapOffScreenRenderer.setBlockingRendering(true);
    }

    public java.lang.String getMapImageFile(java.lang.String id) {
        return com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile().getPlacesImageDir().getAbsolutePath() + java.io.File.separator + id + ".jpg";
    }

    public void generateMapSnapshot(com.navdy.hud.app.maps.here.HereMapImageGenerator.MapGeneratorParams mapGeneratorParams) {
        if (mapGeneratorParams == null) {
            throw new java.lang.IllegalArgumentException();
        } else if (!com.navdy.hud.app.maps.here.HereMapsManager.getInstance().isInitialized()) {
            sLogger.e("map engine not initialized");
            if (mapGeneratorParams.callback != null) {
                mapGeneratorParams.callback.result(null);
            }
        }
    }

    private void generateMapSnapshotInternal(com.navdy.hud.app.maps.here.HereMapImageGenerator.MapGeneratorParams mapGeneratorParams) {
        try {
            sLogger.e("generateMapSnapshotInternal lat:" + mapGeneratorParams.latitude + " lon:" + mapGeneratorParams.longitude);
            this.map.setCenter(new com.here.android.mpa.common.GeoCoordinate(mapGeneratorParams.latitude, mapGeneratorParams.longitude), com.here.android.mpa.mapping.Map.Animation.NONE);
            this.mapOffScreenRenderer.setSize(mapGeneratorParams.width, mapGeneratorParams.height);
            if (0 == 0) {
                this.mapOffScreenRenderer.start();
            }
            this.mapOffScreenRenderer.getScreenCapture(new com.navdy.hud.app.maps.here.HereMapImageGenerator.Anon1(mapGeneratorParams));
        } catch (Throwable t) {
            sLogger.e("renderMapSnapshot", t);
            if (0 != 0) {
                stopOffscreenRenderer();
            }
        }
    }

    /* access modifiers changed from: private */
    public void stopOffscreenRenderer() {
        try {
            this.mapOffScreenRenderer.stop();
        } catch (Throwable t) {
            sLogger.e(t);
        } finally {
            notifyWaitingRenders();
        }
    }

    private void notifyWaitingRenders() {
        synchronized (this.waitForRender) {
            this.renderRunning = false;
            this.waitForRender.notifyAll();
        }
    }

    /* access modifiers changed from: private */
    public void saveFileToDisk(com.navdy.hud.app.maps.here.HereMapImageGenerator.MapGeneratorParams mapGeneratorParams, android.graphics.Bitmap bitmap) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.maps.here.HereMapImageGenerator.Anon2(bitmap, mapGeneratorParams), 1);
    }
}
