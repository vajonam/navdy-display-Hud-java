package com.navdy.hud.app.maps.here;

public class HereOfflineMapsVersion {
    static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.maps.here.HereOfflineMapsVersion.class);
    private java.util.Date date;
    private java.util.List<java.lang.String> packages;
    private java.lang.String rawDate;
    private java.lang.String version;

    public HereOfflineMapsVersion(java.lang.String offlineMapsData) {
        try {
            org.json.JSONObject jsonObject = new org.json.JSONObject(offlineMapsData);
            this.rawDate = jsonObject.getString("version");
            this.date = parseMapDate(this.rawDate);
            this.version = jsonObject.getString("here_map_version");
            org.json.JSONArray offlineMaps = jsonObject.getJSONArray("map_packages");
            int len = offlineMaps.length();
            this.packages = new java.util.ArrayList(len);
            for (int i = 0; i < len; i++) {
                this.packages.add(offlineMaps.getString(i));
            }
        } catch (java.lang.Exception e) {
            sLogger.e("Failed to parse off-line maps version info", e);
        }
    }

    public java.lang.String getRawDate() {
        return this.rawDate;
    }

    public java.util.Date getDate() {
        return this.date;
    }

    public java.util.List<java.lang.String> getPackages() {
        return this.packages;
    }

    public java.lang.String getVersion() {
        return this.version;
    }

    private java.util.Date parseMapDate(java.lang.String str) throws java.lang.Exception {
        int y = -1;
        int m = -1;
        int d = -1;
        java.util.StringTokenizer tokenizer = new java.util.StringTokenizer(str, com.navdy.hud.app.framework.glance.GlanceConstants.PERIOD);
        while (tokenizer.hasMoreElements()) {
            java.lang.String token = tokenizer.nextToken();
            if (y == -1) {
                y = java.lang.Integer.parseInt(token);
            } else if (m == -1) {
                m = java.lang.Integer.parseInt(token);
            } else {
                d = java.lang.Integer.parseInt(token);
            }
        }
        if (y == -1 || m == -1 || d == -1) {
            return null;
        }
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        calendar.set(1, y);
        calendar.set(2, m - 1);
        calendar.set(5, d);
        return calendar.getTime();
    }
}
