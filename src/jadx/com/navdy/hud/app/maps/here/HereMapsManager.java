package com.navdy.hud.app.maps.here;

public class HereMapsManager {
    public static final double DEFAULT_LATITUDE = 37.802086d;
    public static final double DEFAULT_LONGITUDE = -122.419015d;
    public static final float DEFAULT_TILT = 60.0f;
    public static final float DEFAULT_ZOOM_LEVEL = 16.5f;
    private static final java.lang.String DISABLE_MAPS_PROPERTY = "persist.sys.map.disable";
    private static final boolean ENABLE_MAPS = (!com.navdy.hud.app.util.os.SystemProperties.getBoolean(DISABLE_MAPS_PROPERTY, false));
    private static final java.lang.String ENROUTE_MAP_SCHEME = "guidance.hud";
    private static final int GPS_SPEED_LAST_LOCATION_THRESHOLD = 2000;
    static final int MIN_SAME_POSITION_UPDATE_THRESHOLD = 500;
    private static final java.lang.String MW_CONFIG_EXCEPTION_MSG = "Invalid configuration file. Check MWConfig!";
    private static final java.lang.String NAVDY_HERE_MAP_SERVICE_NAME = "com.navdy.HereMapService";
    private static final java.lang.String NEW_DISKCACHE = "diskcache-v5";
    private static final java.lang.String OLD_DISKCACHE = "diskcache-v4";
    public static final float ROUTE_CALC_START_ZOOM_LEVEL = 15.5f;
    private static final java.lang.String TRACKING_MAP_SCHEME = "assistance.hud";
    private static final java.lang.String TRAFFIC_REROUTE_PROPERTY = "persist.sys.hud_traffic_reroute";
    private static boolean recalcCurrentRouteForTraffic = false;
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.maps.here.HereMapsManager.class);
    private static final com.navdy.hud.app.maps.here.HereMapsManager singleton = new com.navdy.hud.app.maps.here.HereMapsManager();
    private final android.os.HandlerThread bkLocationReceiverHandlerThread;
    @javax.inject.Inject
    com.squareup.otto.Bus bus;
    private android.content.Context context = com.navdy.hud.app.HudApplication.getAppContext();
    private com.here.android.mpa.common.OnEngineInitListener engineInitListener = new com.navdy.hud.app.maps.here.HereMapsManager.Anon1();
    /* access modifiers changed from: private */
    public volatile boolean extrapolationOn;
    /* access modifiers changed from: private */
    public android.os.Handler handler = new android.os.Handler(android.os.Looper.getMainLooper());
    /* access modifiers changed from: private */
    public com.navdy.hud.app.maps.here.HereLocationFixManager hereLocationFixManager;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.maps.here.HereMapAnimator hereMapAnimator;
    /* access modifiers changed from: private */
    public java.lang.Object initLock = new java.lang.Object();
    /* access modifiers changed from: private */
    public boolean initialMapRendering = true;
    /* access modifiers changed from: private */
    public com.here.android.mpa.common.GeoPosition lastGeoPosition;
    /* access modifiers changed from: private */
    public long lastGeoPositionTime;
    private android.content.SharedPreferences.OnSharedPreferenceChangeListener listener = new com.navdy.hud.app.maps.here.HereMapsManager.Anon4();
    private volatile boolean lowBandwidthDetected = false;
    @javax.inject.Inject
    com.navdy.hud.app.profile.DriverProfileManager mDriverProfileManager;
    /* access modifiers changed from: private */
    public com.here.android.mpa.mapping.Map map;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.maps.here.HereMapController mapController;
    /* access modifiers changed from: private */
    public boolean mapDataVerified;
    /* access modifiers changed from: private */
    public com.here.android.mpa.common.MapEngine mapEngine;
    /* access modifiers changed from: private */
    public long mapEngineStartTime;
    /* access modifiers changed from: private */
    public com.here.android.mpa.common.OnEngineInitListener.Error mapError;
    private boolean mapInitLoaderComplete;
    /* access modifiers changed from: private */
    public boolean mapInitialized;
    /* access modifiers changed from: private */
    public boolean mapInitializing = true;
    /* access modifiers changed from: private */
    public com.here.android.mpa.odml.MapLoader mapLoader;
    /* access modifiers changed from: private */
    public int mapPackageCount;
    private com.here.android.mpa.mapping.MapView mapView;
    private com.navdy.hud.app.ui.component.homescreen.NavigationView navigationView;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.maps.here.HereOfflineMapsVersion offlineMapsVersion;
    private com.here.android.mpa.common.PositioningManager.OnPositionChangedListener positionChangedListener = new com.navdy.hud.app.maps.here.HereMapsManager.Anon3();
    /* access modifiers changed from: private */
    public com.here.android.mpa.common.PositioningManager positioningManager;
    private boolean positioningManagerInstalled;
    @javax.inject.Inject
    com.navdy.hud.app.device.PowerManager powerManager;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.maps.notification.RouteCalculationEventHandler routeCalcEventHandler;
    private com.here.android.mpa.common.GeoCoordinate routeStartPoint;
    @javax.inject.Inject
    android.content.SharedPreferences sharedPreferences;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.manager.SpeedManager speedManager;
    private volatile boolean turnEngineOn = false;
    private boolean voiceSkinsLoaded;

    class Anon1 implements com.here.android.mpa.common.OnEngineInitListener {

        /* renamed from: com.navdy.hud.app.maps.here.HereMapsManager$Anon1$Anon1 reason: collision with other inner class name */
        class C0016Anon1 implements java.lang.Runnable {
            final /* synthetic */ long val$t1;

            /* renamed from: com.navdy.hud.app.maps.here.HereMapsManager$Anon1$Anon1$Anon1 reason: collision with other inner class name */
            class C0017Anon1 implements java.lang.Runnable {
                final /* synthetic */ java.lang.Throwable val$t;

                C0017Anon1(java.lang.Throwable th) {
                    this.val$t = th;
                }

                public void run() {
                    throw new com.navdy.hud.app.maps.here.HereMapsManager.MWConfigCorruptException(this.val$t);
                }
            }

            /* renamed from: com.navdy.hud.app.maps.here.HereMapsManager$Anon1$Anon1$Anon2 */
            class Anon2 implements java.lang.Runnable {
                final /* synthetic */ long val$t2;

                /* renamed from: com.navdy.hud.app.maps.here.HereMapsManager$Anon1$Anon1$Anon2$Anon1 reason: collision with other inner class name */
                class C0018Anon1 implements java.lang.Runnable {
                    final /* synthetic */ long val$t3;

                    C0018Anon1(long j) {
                        this.val$t3 = j;
                    }

                    public void run() {
                        com.navdy.hud.app.maps.here.HereMapAnimator.AnimationMode animationMode;
                        com.navdy.hud.app.maps.here.HereNavigationManager hereNavigationManager = com.navdy.hud.app.maps.here.HereNavigationManager.getInstance();
                        com.navdy.hud.app.maps.here.HereMapsManager.this.registerConnectivityReceiver();
                        com.navdy.hud.app.maps.here.HereMapsManager.sLogger.v("map created");
                        com.navdy.hud.app.maps.notification.TrafficNotificationManager.getInstance();
                        if (com.navdy.hud.app.maps.MapSettings.isCustomAnimationEnabled()) {
                            animationMode = com.navdy.hud.app.maps.here.HereMapAnimator.AnimationMode.ANIMATION;
                        } else if (com.navdy.hud.app.maps.MapSettings.isFullCustomAnimatonEnabled()) {
                            animationMode = com.navdy.hud.app.maps.here.HereMapAnimator.AnimationMode.ZOOM_TILT_ANIMATION;
                        } else {
                            animationMode = com.navdy.hud.app.maps.here.HereMapAnimator.AnimationMode.NONE;
                        }
                        com.navdy.hud.app.maps.here.HereMapsManager.sLogger.v("HereMapAnimator mode is [" + animationMode + "]");
                        com.navdy.hud.app.maps.here.HereMapsManager.this.hereMapAnimator = new com.navdy.hud.app.maps.here.HereMapAnimator(animationMode, com.navdy.hud.app.maps.here.HereMapsManager.this.mapController, hereNavigationManager.getNavController());
                        if (!com.navdy.hud.app.maps.here.HereMapsManager.this.initialMapRendering) {
                            com.navdy.hud.app.maps.here.HereMapsManager.sLogger.v("HereMapAnimator MapRendering initial disabled");
                            com.navdy.hud.app.maps.here.HereMapsManager.this.hereMapAnimator.stopMapRendering();
                        }
                        com.navdy.hud.app.maps.here.HereMapCameraManager.getInstance().initialize(com.navdy.hud.app.maps.here.HereMapsManager.this.mapController, com.navdy.hud.app.maps.here.HereMapsManager.this.bus, com.navdy.hud.app.maps.here.HereMapsManager.this.hereMapAnimator);
                        com.navdy.hud.app.maps.here.HereMapsManager.this.hereLocationFixManager = new com.navdy.hud.app.maps.here.HereLocationFixManager(com.navdy.hud.app.maps.here.HereMapsManager.this.bus);
                        com.navdy.hud.app.maps.here.HereMapsManager.sLogger.v("MAP-ENGINE-INIT-4 took [" + (android.os.SystemClock.elapsedRealtime() - this.val$t3) + "]");
                        com.navdy.hud.app.maps.here.HereMapsManager.this.routeCalcEventHandler = new com.navdy.hud.app.maps.notification.RouteCalculationEventHandler(com.navdy.hud.app.maps.here.HereMapsManager.this.bus);
                        com.navdy.hud.app.maps.here.HereMapsManager.this.initMapLoader();
                        com.navdy.hud.app.maps.here.HereMapsManager.this.offlineMapsVersion = new com.navdy.hud.app.maps.here.HereOfflineMapsVersion(com.navdy.hud.app.maps.here.HereMapsManager.this.getOfflineMapsData());
                        synchronized (com.navdy.hud.app.maps.here.HereMapsManager.this.initLock) {
                            com.navdy.hud.app.maps.here.HereMapsManager.this.mapInitializing = false;
                            com.navdy.hud.app.maps.here.HereMapsManager.this.mapInitialized = true;
                            com.navdy.hud.app.maps.here.HereMapsManager.this.bus.post(new com.navdy.hud.app.maps.MapEvents.MapEngineInitialize(com.navdy.hud.app.maps.here.HereMapsManager.this.mapInitialized));
                            com.navdy.hud.app.maps.here.HereMapsManager.sLogger.v("MAP-ENGINE-INIT event sent");
                        }
                        com.navdy.hud.app.maps.here.HereMapsManager.sLogger.v("setEngineOnlineState initial");
                        com.navdy.hud.app.maps.here.HereMapsManager.this.setEngineOnlineState();
                        if (!com.navdy.hud.app.maps.here.HereMapsManager.this.powerManager.inQuietMode()) {
                            com.navdy.hud.app.maps.here.HereMapsManager.this.startTrafficUpdater();
                        }
                    }
                }

                Anon2(long j) {
                    this.val$t2 = j;
                }

                public void run() {
                    com.navdy.hud.app.maps.here.HereMapsManager.this.positioningManager.start(com.here.android.mpa.common.PositioningManager.LocationMethod.GPS_NETWORK);
                    com.navdy.hud.app.maps.here.HereMapsManager.sLogger.v("position manager started");
                    com.navdy.hud.app.maps.here.HereMapsManager.sLogger.v("MAP-ENGINE-INIT-3 took [" + (android.os.SystemClock.elapsedRealtime() - this.val$t2) + "]");
                    com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.maps.here.HereMapsManager.Anon1.C0016Anon1.Anon2.C0018Anon1(android.os.SystemClock.elapsedRealtime()), 3);
                }
            }

            C0016Anon1(long j) {
                this.val$t1 = j;
            }

            public void run() {
                com.navdy.hud.app.maps.here.HereMapsManager.this.mapEngine.onResume();
                com.navdy.hud.app.maps.here.HereMapsManager.sLogger.v(":initializing maps config");
                com.navdy.hud.app.maps.here.HereMapsConfigurator.getInstance().updateMapsConfig();
                com.navdy.hud.app.maps.here.HereMapsManager.sLogger.v("creating geopos");
                new com.here.android.mpa.common.GeoPosition(new com.here.android.mpa.common.GeoCoordinate(37.802086d, -122.419015d));
                com.navdy.hud.app.maps.here.HereMapsManager.sLogger.v("created geopos");
                if (!com.navdy.hud.app.util.DeviceUtil.isUserBuild()) {
                    long l1 = android.os.SystemClock.elapsedRealtime();
                    com.navdy.hud.app.maps.here.HereLaneInfoBuilder.init();
                    com.navdy.hud.app.maps.here.HereMapsManager.sLogger.v("time to init HereLaneInfoBuilder [" + (android.os.SystemClock.elapsedRealtime() - l1) + "]");
                }
                com.navdy.hud.app.maps.here.HereMapsManager.sLogger.v("engine initialized refcount:" + com.navdy.hud.app.maps.here.HereMapsManager.this.mapEngine.getResourceReferenceCount());
                try {
                    com.navdy.hud.app.maps.here.HereMapsManager.this.map = new com.here.android.mpa.mapping.Map();
                } catch (Throwable t) {
                    if (t.getMessage().contains(com.navdy.hud.app.maps.here.HereMapsManager.MW_CONFIG_EXCEPTION_MSG)) {
                        com.navdy.hud.app.maps.here.HereMapsManager.sLogger.e("MWConfig is corrupted! Cleaning up and restarting HUD app.");
                        com.navdy.hud.app.maps.here.HereMapsConfigurator.getInstance().removeMWConfigFolder();
                    }
                    com.navdy.hud.app.maps.here.HereMapsManager.this.handler.post(new com.navdy.hud.app.maps.here.HereMapsManager.Anon1.C0016Anon1.C0017Anon1(t));
                }
                com.navdy.hud.app.maps.here.HereMapsManager.this.map.setProjectionMode(com.here.android.mpa.mapping.Map.Projection.MERCATOR);
                com.navdy.hud.app.maps.here.HereMapsManager.this.map.setTrafficInfoVisible(true);
                com.navdy.hud.app.maps.here.HereMapsManager.this.mapController = new com.navdy.hud.app.maps.here.HereMapController(com.navdy.hud.app.maps.here.HereMapsManager.this.map, com.navdy.hud.app.maps.here.HereMapController.State.NONE);
                com.navdy.hud.app.maps.here.HereMapsManager.this.setMapAttributes(new com.here.android.mpa.common.GeoCoordinate(37.802086d, -122.419015d));
                com.navdy.hud.app.maps.here.HereMapsManager.sLogger.v("setting default map attributes");
                com.navdy.hud.app.maps.here.HereMapsManager.this.setMapTraffic();
                com.navdy.hud.app.maps.here.HereMapsManager.this.setMapPoiLayer(com.navdy.hud.app.maps.here.HereMapsManager.this.map);
                com.navdy.hud.app.maps.here.HereMapsManager.this.positioningManager = com.here.android.mpa.common.PositioningManager.getInstance();
                com.navdy.hud.app.maps.here.HereMapsManager.sLogger.v("MAP-ENGINE-INIT-2 took [" + (android.os.SystemClock.elapsedRealtime() - this.val$t1) + "]");
                com.navdy.hud.app.maps.here.HereMapsManager.this.handler.post(new com.navdy.hud.app.maps.here.HereMapsManager.Anon1.C0016Anon1.Anon2(android.os.SystemClock.elapsedRealtime()));
            }
        }

        Anon1() {
        }

        public void onEngineInitializationCompleted(com.here.android.mpa.common.OnEngineInitListener.Error error) {
            long t1 = android.os.SystemClock.elapsedRealtime();
            com.navdy.hud.app.maps.here.HereMapsManager.sLogger.v("MAP-ENGINE-INIT took [" + (t1 - com.navdy.hud.app.maps.here.HereMapsManager.this.mapEngineStartTime) + "]");
            com.navdy.hud.app.maps.here.HereMapsManager.sLogger.v("MAP-ENGINE-INIT HERE-SDK version:" + com.here.android.mpa.common.Version.getSdkVersion());
            if (error == com.here.android.mpa.common.OnEngineInitListener.Error.NONE) {
                com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.maps.here.HereMapsManager.Anon1.C0016Anon1(t1), 3);
                return;
            }
            java.lang.String errString = error.toString();
            com.navdy.hud.app.analytics.AnalyticsSupport.recordKeyFailure("HereMaps", errString);
            com.navdy.hud.app.maps.here.HereMapsManager.sLogger.e("MAP-ENGINE-INIT engine NOT initialized:" + error);
            com.navdy.hud.app.maps.here.HereMapsManager.this.mapError = error;
            synchronized (com.navdy.hud.app.maps.here.HereMapsManager.this.initLock) {
                com.navdy.hud.app.maps.here.HereMapsManager.this.mapInitializing = false;
                com.navdy.hud.app.maps.here.HereMapsManager.this.bus.post(new com.navdy.hud.app.maps.MapEvents.MapEngineInitialize(com.navdy.hud.app.maps.here.HereMapsManager.this.mapInitialized));
            }
            if (error == com.here.android.mpa.common.OnEngineInitListener.Error.USAGE_EXPIRED || error == com.here.android.mpa.common.OnEngineInitListener.Error.MISSING_APP_CREDENTIAL) {
                com.navdy.hud.app.ui.activity.Main.handleLibraryInitializationError("Here Maps initialization failure: " + errString);
            }
        }
    }

    class Anon10 implements java.lang.Runnable {
        Anon10() {
        }

        public void run() {
            com.navdy.hud.app.maps.here.HereMapsManager.this.map.setTrafficInfoVisible(false);
            com.here.android.mpa.mapping.MapTrafficLayer trafficLayer = com.navdy.hud.app.maps.here.HereMapsManager.this.map.getMapTrafficLayer();
            trafficLayer.setDisplayFilter(com.here.android.mpa.mapping.TrafficEvent.Severity.NORMAL);
            trafficLayer.setEnabled(com.here.android.mpa.mapping.MapTrafficLayer.RenderLayer.ONROUTE, false);
            trafficLayer.setEnabled(com.here.android.mpa.mapping.MapTrafficLayer.RenderLayer.FLOW, false);
            trafficLayer.setEnabled(com.here.android.mpa.mapping.MapTrafficLayer.RenderLayer.INCIDENT, false);
        }
    }

    class Anon11 extends android.content.BroadcastReceiver {
        Anon11() {
        }

        public void onReceive(android.content.Context context, android.content.Intent intent) {
            if ("android.net.conn.CONNECTIVITY_CHANGE".equals(intent.getAction())) {
                com.navdy.hud.app.maps.here.HereMapsManager.sLogger.v("setEngineOnlineState n/w state change");
                com.navdy.hud.app.maps.here.HereMapsManager.this.setEngineOnlineState();
            }
        }
    }

    class Anon12 implements java.lang.Runnable {
        Anon12() {
        }

        public void run() {
            com.navdy.hud.app.maps.here.HereMapsManager.this.mapController.setTrafficInfoVisible(false);
            com.navdy.hud.app.maps.here.HereMapsManager.sLogger.v("hidetraffic: map off");
            com.here.android.mpa.mapping.MapRoute mapRoute = com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().getCurrentMapRoute();
            if (mapRoute != null) {
                mapRoute.setTrafficEnabled(false);
                com.navdy.hud.app.maps.here.HereMapsManager.sLogger.v("hidetraffic: route off");
            }
            com.navdy.hud.app.maps.here.HereMapsManager.sLogger.v("hidetraffic: traffic is disabled");
        }
    }

    class Anon13 implements java.lang.Runnable {
        Anon13() {
        }

        public void run() {
            com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().setTrafficToMode();
            com.navdy.hud.app.maps.here.HereMapsManager.sLogger.v("showTraffic: traffic is enabled");
        }
    }

    class Anon2 implements java.lang.Runnable {

        class Anon1 implements com.here.android.mpa.odml.MapLoader.Listener {

            /* renamed from: com.navdy.hud.app.maps.here.HereMapsManager$Anon2$Anon1$Anon1 reason: collision with other inner class name */
            class C0019Anon1 implements java.lang.Runnable {
                final /* synthetic */ com.here.android.mpa.odml.MapPackage val$mapPackage;

                C0019Anon1(com.here.android.mpa.odml.MapPackage mapPackage) {
                    this.val$mapPackage = mapPackage;
                }

                public void run() {
                    com.navdy.hud.app.maps.here.HereMapsManager.this.printMapPackages(this.val$mapPackage, java.util.EnumSet.of(com.here.android.mpa.odml.MapPackage.InstallationState.INSTALLED, com.here.android.mpa.odml.MapPackage.InstallationState.PARTIALLY_INSTALLED));
                    com.navdy.hud.app.maps.here.HereMapsManager.sLogger.v("initMapLoader: map package count:" + com.navdy.hud.app.maps.here.HereMapsManager.this.mapPackageCount);
                    com.navdy.hud.app.maps.here.HereMapsManager.this.mapDataVerified = true;
                }
            }

            Anon1() {
            }

            public void onProgress(int progressPercentage) {
                com.navdy.hud.app.maps.here.HereMapsManager.sLogger.v("MapLoader: progress[" + progressPercentage + "]");
            }

            public void onInstallationSize(long diskSize, long networkSize) {
                com.navdy.hud.app.maps.here.HereMapsManager.sLogger.v("MapLoader: onInstallationSize disk[" + diskSize + "] network[" + networkSize + "]");
            }

            public void onGetMapPackagesComplete(com.here.android.mpa.odml.MapPackage mapPackage, com.here.android.mpa.odml.MapLoader.ResultCode resultCode) {
                if (resultCode != com.here.android.mpa.odml.MapLoader.ResultCode.OPERATION_SUCCESSFUL) {
                    com.navdy.hud.app.maps.here.HereMapsManager.sLogger.e("initMapLoader: get map package failed:" + resultCode.name());
                    return;
                }
                com.navdy.hud.app.maps.here.HereMapsManager.sLogger.v("initMapLoader: get map package success");
                com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.maps.here.HereMapsManager.Anon2.Anon1.C0019Anon1(mapPackage), 2);
            }

            public void onCheckForUpdateComplete(boolean updateAvailable, java.lang.String currentMapVersion, java.lang.String newestMapVersion, com.here.android.mpa.odml.MapLoader.ResultCode mapLoaderResultCode) {
                com.navdy.hud.app.maps.here.HereMapsManager.sLogger.v("MapLoader: updateAvailable[" + updateAvailable + "] currentMapVersion[" + currentMapVersion + "] newestMapVersion[" + newestMapVersion + "] resultCode[" + mapLoaderResultCode + "]");
            }

            public void onPerformMapDataUpdateComplete(com.here.android.mpa.odml.MapPackage mapPackage, com.here.android.mpa.odml.MapLoader.ResultCode resultCode) {
                com.navdy.hud.app.maps.here.HereMapsManager.sLogger.v("MapLoader: onPerformMapDataUpdateComplete result[" + resultCode.name() + "] package[" + (mapPackage != null ? mapPackage.getTitle() : com.amazonaws.services.s3.internal.Constants.NULL_VERSION_ID) + "]");
            }

            public void onInstallMapPackagesComplete(com.here.android.mpa.odml.MapPackage mapPackage, com.here.android.mpa.odml.MapLoader.ResultCode resultCode) {
                com.navdy.hud.app.maps.here.HereMapsManager.sLogger.v("MapLoader: onInstallMapPackagesComplete result[" + resultCode.name() + "] package[" + (mapPackage != null ? mapPackage.getTitle() : com.amazonaws.services.s3.internal.Constants.NULL_VERSION_ID) + "]");
            }

            public void onUninstallMapPackagesComplete(com.here.android.mpa.odml.MapPackage mapPackage, com.here.android.mpa.odml.MapLoader.ResultCode resultCode) {
            }
        }

        Anon2() {
        }

        public void run() {
            long l1 = android.os.SystemClock.elapsedRealtime();
            com.navdy.hud.app.maps.here.HereMapsManager.this.mapLoader = com.here.android.mpa.odml.MapLoader.getInstance();
            com.navdy.hud.app.maps.here.HereMapsManager.sLogger.v("initMapLoader took [" + (android.os.SystemClock.elapsedRealtime() - l1) + "]");
            com.navdy.hud.app.maps.here.HereMapsManager.this.mapLoader.addListener(new com.navdy.hud.app.maps.here.HereMapsManager.Anon2.Anon1());
            com.navdy.hud.app.maps.here.HereMapsManager.this.mapPackageCount = 0;
            com.navdy.hud.app.maps.here.HereMapsManager.this.invokeMapLoader();
        }
    }

    class Anon3 implements com.here.android.mpa.common.PositioningManager.OnPositionChangedListener {

        class Anon1 implements java.lang.Runnable {
            final /* synthetic */ com.here.android.mpa.common.GeoPosition val$geoPosition;
            final /* synthetic */ boolean val$isMapMatched;
            final /* synthetic */ com.here.android.mpa.common.PositioningManager.LocationMethod val$locationMethod;

            Anon1(com.here.android.mpa.common.GeoPosition geoPosition, com.here.android.mpa.common.PositioningManager.LocationMethod locationMethod, boolean z) {
                this.val$geoPosition = geoPosition;
                this.val$locationMethod = locationMethod;
                this.val$isMapMatched = z;
            }

            public void run() {
                try {
                    if (!(this.val$geoPosition instanceof com.here.android.mpa.common.MatchedGeoPosition)) {
                        com.here.android.mpa.common.GeoCoordinate coordinate = this.val$geoPosition.getCoordinate();
                        com.navdy.hud.app.maps.here.HereMapsManager.sLogger.v("position not map-matched lat:" + coordinate.getLatitude() + " lng:" + coordinate.getLongitude() + " speed:" + this.val$geoPosition.getSpeed());
                        return;
                    }
                    if (com.navdy.hud.app.maps.here.HereMapsManager.this.extrapolationOn) {
                        if (!com.navdy.hud.app.maps.here.HereMapUtil.isInTunnel(com.navdy.hud.app.maps.here.HereMapsManager.this.positioningManager.getRoadElement())) {
                            com.navdy.hud.app.maps.here.HereMapsManager.sLogger.i("TUNNEL extrapolation off");
                            com.navdy.hud.app.maps.here.HereMapsManager.this.extrapolationOn = false;
                            com.navdy.hud.app.maps.here.HereMapsManager.this.sendExtrapolationEvent();
                        } else {
                            com.navdy.hud.app.maps.here.HereMapsManager.this.sendExtrapolationEvent();
                        }
                    }
                    if (com.navdy.hud.app.maps.here.HereMapsManager.this.lastGeoPosition != null) {
                        long t = android.os.SystemClock.elapsedRealtime() - com.navdy.hud.app.maps.here.HereMapsManager.this.lastGeoPositionTime;
                        if (t < 500 && com.navdy.hud.app.maps.here.HereMapUtil.isCoordinateEqual(com.navdy.hud.app.maps.here.HereMapsManager.this.lastGeoPosition.getCoordinate(), this.val$geoPosition.getCoordinate())) {
                            if (com.navdy.hud.app.maps.here.HereMapsManager.sLogger.isLoggable(2)) {
                                com.navdy.hud.app.maps.here.HereMapsManager.sLogger.v("GEO-Here same pos as last:" + t);
                                return;
                            }
                            return;
                        }
                    }
                    com.navdy.hud.app.maps.here.HereMapsManager.this.lastGeoPosition = this.val$geoPosition;
                    com.navdy.hud.app.maps.here.HereMapsManager.this.lastGeoPositionTime = android.os.SystemClock.elapsedRealtime();
                    com.navdy.hud.app.maps.here.HereMapsManager.this.hereMapAnimator.setGeoPosition(this.val$geoPosition);
                    com.navdy.hud.app.maps.here.HereMapsManager.this.hereLocationFixManager.onHerePositionUpdated(this.val$locationMethod, this.val$geoPosition, this.val$isMapMatched);
                    com.navdy.hud.app.maps.here.HereMapCameraManager.getInstance().onGeoPositionChange(this.val$geoPosition);
                    if (com.navdy.hud.app.maps.here.HereMapsManager.this.speedManager.getObdSpeed() == -1 && android.os.SystemClock.elapsedRealtime() - com.navdy.hud.app.maps.here.HereMapsManager.this.hereLocationFixManager.getLastLocationTime() >= 2000 && (this.val$geoPosition instanceof com.here.android.mpa.common.MatchedGeoPosition) && this.val$geoPosition.isExtrapolated() && com.navdy.hud.app.maps.here.HereMapsManager.this.speedManager.setGpsSpeed((float) this.val$geoPosition.getSpeed(), android.os.SystemClock.elapsedRealtimeNanos() / 1000000)) {
                        com.navdy.hud.app.maps.here.HereMapsManager.this.bus.post(new com.navdy.hud.app.maps.MapEvents.GPSSpeedEvent());
                    }
                    com.navdy.hud.app.maps.here.HereMapsManager.this.bus.post(this.val$geoPosition);
                    if (com.navdy.hud.app.maps.here.HereMapsManager.sLogger.isLoggable(2)) {
                        com.here.android.mpa.common.GeoCoordinate geoCoordinate = this.val$geoPosition.getCoordinate();
                        com.navdy.hud.app.maps.here.HereMapsManager.sLogger.v("GEO-Here speed-mps[" + this.val$geoPosition.getSpeed() + "] " + "] lat=[" + geoCoordinate.getLatitude() + "] lon=[" + geoCoordinate.getLongitude() + "] provider=[" + this.val$locationMethod.name() + "] timestamp:[" + this.val$geoPosition.getTimestamp().getTime() + "]");
                    }
                } catch (Throwable t2) {
                    com.navdy.hud.app.maps.here.HereMapsManager.sLogger.e(t2);
                }
            }
        }

        class Anon2 implements java.lang.Runnable {
            final /* synthetic */ com.here.android.mpa.common.PositioningManager.LocationMethod val$locationMethod;
            final /* synthetic */ com.here.android.mpa.common.PositioningManager.LocationStatus val$locationStatus;

            Anon2(com.here.android.mpa.common.PositioningManager.LocationMethod locationMethod, com.here.android.mpa.common.PositioningManager.LocationStatus locationStatus) {
                this.val$locationMethod = locationMethod;
                this.val$locationStatus = locationStatus;
            }

            public void run() {
                try {
                    if (com.navdy.hud.app.maps.here.HereMapsManager.sLogger.isLoggable(2)) {
                        com.navdy.hud.app.maps.here.HereMapsManager.sLogger.d("onPositionFixChanged: method:" + this.val$locationMethod + " status: " + this.val$locationStatus);
                    }
                    if (this.val$locationMethod == com.here.android.mpa.common.PositioningManager.LocationMethod.GPS && com.navdy.hud.app.maps.here.HereMapUtil.isInTunnel(com.navdy.hud.app.maps.here.HereMapsManager.this.positioningManager.getRoadElement()) && !com.navdy.hud.app.maps.here.HereMapsManager.this.extrapolationOn) {
                        com.navdy.hud.app.maps.here.HereMapsManager.sLogger.d("TUNNEL extrapolation on");
                        com.navdy.hud.app.maps.here.HereMapsManager.this.extrapolationOn = true;
                        com.navdy.hud.app.maps.here.HereMapsManager.this.sendExtrapolationEvent();
                    }
                } catch (Throwable t) {
                    com.navdy.hud.app.maps.here.HereMapsManager.sLogger.e(t);
                }
            }
        }

        Anon3() {
        }

        public void onPositionUpdated(com.here.android.mpa.common.PositioningManager.LocationMethod locationMethod, com.here.android.mpa.common.GeoPosition geoPosition, boolean isMapMatched) {
            if (geoPosition != null) {
                com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.maps.here.HereMapsManager.Anon3.Anon1(geoPosition, locationMethod, isMapMatched), 18);
            }
        }

        public void onPositionFixChanged(com.here.android.mpa.common.PositioningManager.LocationMethod locationMethod, com.here.android.mpa.common.PositioningManager.LocationStatus locationStatus) {
            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.maps.here.HereMapsManager.Anon3.Anon2(locationMethod, locationStatus), 18);
        }
    }

    class Anon4 implements android.content.SharedPreferences.OnSharedPreferenceChangeListener {

        class Anon1 implements java.lang.Runnable {
            Anon1() {
            }

            public void run() {
                if (!com.navdy.hud.app.maps.here.HereMapsManager.this.mapController.getMapScheme().equals(com.navdy.hud.app.maps.here.HereMapsManager.ENROUTE_MAP_SCHEME)) {
                    com.navdy.hud.app.maps.here.HereMapsManager.this.mapController.setMapScheme(com.navdy.hud.app.maps.here.HereMapsManager.this.getTrackingMapScheme());
                }
            }
        }

        Anon4() {
        }

        public void onSharedPreferenceChanged(android.content.SharedPreferences sharedPreferences, java.lang.String key) {
            com.navdy.hud.app.maps.here.HereMapsManager.sLogger.d("onSharedPreferenceChanged: " + key);
            if (!com.navdy.hud.app.maps.here.HereMapsManager.this.isInitialized()) {
                com.navdy.hud.app.maps.here.HereMapsManager.sLogger.w("onSharedPreferenceChanged: map engine not intialized:" + key);
                return;
            }
            char c = 65535;
            switch (key.hashCode()) {
                case -1094616929:
                    if (key.equals(com.navdy.hud.app.settings.HUDSettings.MAP_ANIMATION_MODE)) {
                        c = 3;
                        break;
                    }
                    break;
                case -285821321:
                    if (key.equals(com.navdy.hud.app.settings.HUDSettings.MAP_SCHEME)) {
                        c = 2;
                        break;
                    }
                    break;
                case 133816335:
                    if (key.equals(com.navdy.hud.app.settings.HUDSettings.MAP_TILT)) {
                        c = 0;
                        break;
                    }
                    break;
                case 134000933:
                    if (key.equals(com.navdy.hud.app.settings.HUDSettings.MAP_ZOOM)) {
                        c = 1;
                        break;
                    }
                    break;
            }
            switch (c) {
                case 0:
                    java.lang.String val = sharedPreferences.getString(key, null);
                    if (val == null) {
                        com.navdy.hud.app.maps.here.HereMapsManager.sLogger.v("onSharedPreferenceChanged:no tilt");
                        return;
                    }
                    try {
                        float tilt = java.lang.Float.parseFloat(val);
                        com.navdy.hud.app.maps.here.HereMapsManager.sLogger.w("onSharedPreferenceChanged:tilt:" + tilt);
                        com.navdy.hud.app.maps.here.HereMapsManager.this.mapController.setTilt(tilt);
                        return;
                    } catch (Throwable t) {
                        com.navdy.hud.app.maps.here.HereMapsManager.sLogger.e("onSharedPreferenceChanged:tilt", t);
                        return;
                    }
                case 1:
                    java.lang.String val2 = sharedPreferences.getString(key, null);
                    if (val2 == null) {
                        com.navdy.hud.app.maps.here.HereMapsManager.sLogger.v("onSharedPreferenceChanged:no zoom");
                        return;
                    }
                    try {
                        float zoom = java.lang.Float.parseFloat(val2);
                        com.navdy.hud.app.maps.here.HereMapsManager.sLogger.w("onSharedPreferenceChanged:zoom:" + zoom);
                        com.navdy.hud.app.maps.here.HereMapsManager.this.mapController.setZoomLevel((double) zoom);
                        return;
                    } catch (Throwable t2) {
                        com.navdy.hud.app.maps.here.HereMapsManager.sLogger.e("onSharedPreferenceChanged:zoom", t2);
                        return;
                    }
                case 2:
                    com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.maps.here.HereMapsManager.Anon4.Anon1(), 2);
                    return;
                default:
                    return;
            }
        }
    }

    class Anon5 implements java.lang.Runnable {

        class Anon1 implements java.lang.Runnable {
            Anon1() {
            }

            public void run() {
                com.navdy.hud.app.maps.here.HereMapsManager.this.initialize();
            }
        }

        Anon5() {
        }

        public void run() {
            boolean checkAgain = false;
            try {
                if (((android.location.LocationManager) com.navdy.hud.app.HudApplication.getAppContext().getSystemService("location")).getProvider("network") == null) {
                    com.navdy.hud.app.maps.here.HereMapsManager.sLogger.v("n/w provider not found yet, check again");
                    checkAgain = true;
                } else {
                    com.navdy.hud.app.maps.here.HereMapsManager.sLogger.v("n/w provider found, initialize here");
                    com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.maps.here.HereMapsManager.Anon5.Anon1(), 2);
                }
                if (checkAgain) {
                    com.navdy.hud.app.maps.here.HereMapsManager.this.handler.postDelayed(this, 1000);
                }
            } catch (Throwable th) {
                if (0 != 0) {
                    com.navdy.hud.app.maps.here.HereMapsManager.this.handler.postDelayed(this, 1000);
                }
                throw th;
            }
        }
    }

    class Anon6 implements java.lang.Runnable {
        Anon6() {
        }

        public void run() {
            com.navdy.hud.app.maps.here.HereMapsManager.this.turnOnline();
        }
    }

    class Anon7 implements java.lang.Runnable {
        Anon7() {
        }

        public void run() {
            com.navdy.hud.app.maps.here.HereMapsManager.this.turnOffline();
        }
    }

    class Anon8 implements java.lang.Runnable {
        Anon8() {
        }

        public void run() {
            com.here.android.mpa.mapping.MapTrafficLayer trafficLayer = com.navdy.hud.app.maps.here.HereMapsManager.this.map.getMapTrafficLayer();
            trafficLayer.setDisplayFilter(com.here.android.mpa.mapping.TrafficEvent.Severity.NORMAL);
            trafficLayer.setEnabled(com.here.android.mpa.mapping.MapTrafficLayer.RenderLayer.FLOW, true);
            trafficLayer.setEnabled(com.here.android.mpa.mapping.MapTrafficLayer.RenderLayer.INCIDENT, true);
            trafficLayer.setEnabled(com.here.android.mpa.mapping.MapTrafficLayer.RenderLayer.ONROUTE, true);
            com.navdy.hud.app.maps.here.HereMapsManager.this.map.setTrafficInfoVisible(true);
        }
    }

    class Anon9 implements java.lang.Runnable {
        Anon9() {
        }

        public void run() {
            com.here.android.mpa.mapping.MapTrafficLayer trafficLayer = com.navdy.hud.app.maps.here.HereMapsManager.this.map.getMapTrafficLayer();
            trafficLayer.setDisplayFilter(com.here.android.mpa.mapping.TrafficEvent.Severity.NORMAL);
            trafficLayer.setEnabled(com.here.android.mpa.mapping.MapTrafficLayer.RenderLayer.ONROUTE, true);
            trafficLayer.setEnabled(com.here.android.mpa.mapping.MapTrafficLayer.RenderLayer.FLOW, true);
            trafficLayer.setEnabled(com.here.android.mpa.mapping.MapTrafficLayer.RenderLayer.INCIDENT, true);
            com.navdy.hud.app.maps.here.HereMapsManager.this.map.setTrafficInfoVisible(true);
        }
    }

    public static class MWConfigCorruptException extends java.lang.RuntimeException {
        public MWConfigCorruptException(java.lang.Throwable t) {
            super(t);
        }
    }

    public static com.navdy.hud.app.maps.here.HereMapsManager getInstance() {
        return singleton;
    }

    /* access modifiers changed from: private */
    public void startTrafficUpdater() {
        try {
            com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().startTrafficUpdater();
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }

    public com.navdy.hud.app.maps.here.HereOfflineMapsVersion getOfflineMapsVersion() {
        return this.offlineMapsVersion;
    }

    /* access modifiers changed from: private */
    public void initMapLoader() {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.maps.here.HereMapsManager.Anon2(), 2);
    }

    /* access modifiers changed from: private */
    public void invokeMapLoader() {
        if (this.mapLoader == null) {
            sLogger.v("invokeMapLoader: no maploader");
        } else if (!com.navdy.service.library.util.SystemUtils.isConnectedToNetwork(com.navdy.hud.app.HudApplication.getAppContext())) {
            sLogger.v("invokeMapLoader: not connected to n/w");
        } else if (!this.mapInitLoaderComplete) {
            this.mapInitLoaderComplete = true;
            sLogger.v("initMapLoader called getMapPackages:" + this.mapLoader.getMapPackages());
        } else {
            sLogger.v("invokeMapLoader: already complete");
        }
    }

    public synchronized void installPositionListener() {
        if (!this.positioningManagerInstalled) {
            this.positioningManager.addListener(new java.lang.ref.WeakReference(this.positionChangedListener));
            sLogger.v("position manager listener installed");
            this.positioningManagerInstalled = true;
        }
    }

    private HereMapsManager() {
        mortar.Mortar.inject(this.context, this);
        this.speedManager = com.navdy.hud.app.manager.SpeedManager.getInstance();
        if (ENABLE_MAPS) {
            checkforNetworkProvider();
        }
        this.bkLocationReceiverHandlerThread = new android.os.HandlerThread("here_bk_location");
        this.bkLocationReceiverHandlerThread.start();
        this.bus.register(this);
    }

    private void checkforNetworkProvider() {
        this.handler.post(new com.navdy.hud.app.maps.here.HereMapsManager.Anon5());
    }

    private void migrateDiskCache(java.lang.String diskCacheRootPath) {
        java.io.File oldDiskCache = new java.io.File(diskCacheRootPath, OLD_DISKCACHE);
        java.io.File newDiskCache = new java.io.File(diskCacheRootPath, NEW_DISKCACHE);
        if (!oldDiskCache.exists()) {
            sLogger.i("No old cache found");
        } else if (!newDiskCache.exists()) {
            sLogger.i("Migrating v4 diskcache to v5");
            if (!oldDiskCache.renameTo(newDiskCache)) {
                sLogger.i("Failed to rename diskcache");
            }
        } else {
            sLogger.i("Not migrating diskcache - v5 already exists");
        }
    }

    /* access modifiers changed from: private */
    public void initialize() {
        sLogger.v(":initializing voice skins");
        com.navdy.hud.app.maps.here.VoiceSkinsConfigurator.updateVoiceSkins();
        sLogger.v(":initializing event handlers");
        com.navdy.hud.app.maps.MapsEventHandler.getInstance();
        if (com.navdy.hud.app.maps.MapSettings.isDebugHereLocation()) {
            com.navdy.hud.app.debug.DebugReceiver.setHereDebugLocation(true);
        }
        com.here.android.mpa.mapping.Map.setMaximumFps(com.navdy.hud.app.maps.MapSettings.getMapFps());
        com.here.android.mpa.mapping.Map.enableMaximumFpsLimit(true);
        sLogger.v("map-fps [" + com.here.android.mpa.mapping.Map.getMaximumFps() + "] enabled[" + com.here.android.mpa.mapping.Map.isMaximumFpsLimited() + "]");
        try {
            java.lang.reflect.Field f = com.nokia.maps.MapSettings.class.getDeclaredField("h");
            f.setAccessible(true);
            sLogger.e("enable worker thread before is " + f.get(null));
            f.set(null, com.nokia.maps.MapSettings.b.a);
            sLogger.e("enable worker thread after is " + f.get(null));
        } catch (Throwable t) {
            sLogger.e("enable worker thread", t);
        }
        this.mapEngine = com.here.android.mpa.common.MapEngine.getInstance();
        sLogger.v("calling maps engine init");
        this.sharedPreferences.registerOnSharedPreferenceChangeListener(this.listener);
        recalcCurrentRouteForTraffic = com.navdy.hud.app.util.os.SystemProperties.getBoolean(TRAFFIC_REROUTE_PROPERTY, false);
        sLogger.v("persist.sys.hud_traffic_reroute=" + recalcCurrentRouteForTraffic);
        java.lang.String mapsPartition = com.navdy.hud.app.storage.PathManager.getInstance().getMapsPartitionPath();
        if (!mapsPartition.isEmpty()) {
            java.lang.String diskCacheRootPath = mapsPartition + java.io.File.separator + ".here-maps";
            migrateDiskCache(diskCacheRootPath);
            try {
                if (!com.here.android.mpa.common.MapSettings.setIsolatedDiskCacheRootPath(diskCacheRootPath, NAVDY_HERE_MAP_SERVICE_NAME)) {
                    sLogger.e("setIsolatedDiskCacheRootPath() failed");
                }
            } catch (java.lang.Exception e) {
                sLogger.e("exception in setIsolatedDiskCacheRootPath()", e);
            }
        }
        this.mapEngineStartTime = android.os.SystemClock.elapsedRealtime();
        this.mapEngine.init(this.context, this.engineInitListener);
    }

    public boolean isInitialized() {
        boolean z;
        synchronized (this.initLock) {
            z = this.mapInitialized;
        }
        return z;
    }

    public boolean isInitializing() {
        boolean z;
        synchronized (this.initLock) {
            z = this.mapInitializing;
        }
        return z;
    }

    public com.here.android.mpa.common.OnEngineInitListener.Error getError() {
        return this.mapError;
    }

    public boolean isRenderingAllowed() {
        return !this.powerManager.inQuietMode();
    }

    public com.navdy.hud.app.maps.here.HereMapController getMapController() {
        return this.mapController;
    }

    public com.here.android.mpa.mapping.MapView getMapView() {
        return this.mapView;
    }

    public void setMapView(com.here.android.mpa.mapping.MapView mapView2, com.navdy.hud.app.ui.component.homescreen.NavigationView navigationView2) {
        this.mapView = mapView2;
        this.navigationView = navigationView2;
        com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().setMapView(mapView2, navigationView2);
        com.navdy.hud.app.maps.here.HereMapAnimator.AnimationMode mode = this.hereMapAnimator.getAnimationMode();
        if (mode != com.navdy.hud.app.maps.here.HereMapAnimator.AnimationMode.NONE) {
            sLogger.v("installing map render listener:" + mode);
            this.mapView.addOnMapRenderListener(this.hereMapAnimator.getMapRenderListener());
            return;
        }
        sLogger.v("not installing map render listener:" + mode);
    }

    public com.here.android.mpa.routing.RouteOptions getRouteOptions() {
        com.here.android.mpa.routing.RouteOptions routeOptions = new com.here.android.mpa.routing.RouteOptions();
        routeOptions.setTransportMode(com.here.android.mpa.routing.RouteOptions.TransportMode.CAR);
        com.navdy.service.library.events.preferences.NavigationPreferences preferences = this.mDriverProfileManager.getCurrentProfile().getNavigationPreferences();
        switch (preferences.routingType) {
            case ROUTING_FASTEST:
                routeOptions.setRouteType(com.here.android.mpa.routing.RouteOptions.Type.FASTEST);
                break;
            case ROUTING_SHORTEST:
                routeOptions.setRouteType(com.here.android.mpa.routing.RouteOptions.Type.SHORTEST);
                break;
        }
        routeOptions.setHighwaysAllowed(java.lang.Boolean.TRUE.equals(preferences.allowHighways));
        routeOptions.setTollRoadsAllowed(java.lang.Boolean.TRUE.equals(preferences.allowTollRoads));
        routeOptions.setFerriesAllowed(false);
        routeOptions.setTunnelsAllowed(java.lang.Boolean.TRUE.equals(preferences.allowTunnels));
        routeOptions.setDirtRoadsAllowed(java.lang.Boolean.TRUE.equals(preferences.allowUnpavedRoads));
        routeOptions.setCarShuttleTrainsAllowed(java.lang.Boolean.TRUE.equals(preferences.allowAutoTrains));
        routeOptions.setCarpoolAllowed(false);
        return routeOptions;
    }

    public com.here.android.mpa.common.GeoPosition getLastGeoPosition() {
        return this.lastGeoPosition;
    }

    /* access modifiers changed from: 0000 */
    public java.lang.String getTrackingMapScheme() {
        return TRACKING_MAP_SCHEME;
    }

    /* access modifiers changed from: 0000 */
    public java.lang.String getEnrouteMapScheme() {
        return ENROUTE_MAP_SCHEME;
    }

    private double getDefaultZoomLevel() {
        return 16.5d;
    }

    private float getDefaultTiltLevel() {
        return 60.0f;
    }

    /* access modifiers changed from: private */
    public void setMapAttributes(com.here.android.mpa.common.GeoCoordinate geoCoordinate) {
        this.map.setExtrudedBuildingsVisible(false);
        this.map.setLandmarksVisible(false);
        this.map.setStreetLevelCoverageVisible(false);
        this.map.setMapScheme(getTrackingMapScheme());
        if (geoCoordinate != null) {
            this.map.setCenter(geoCoordinate, com.here.android.mpa.mapping.Map.Animation.NONE, getDefaultZoomLevel(), -1.0f, getDefaultTiltLevel());
            sLogger.v("setcenterDefault:" + geoCoordinate);
            return;
        }
        sLogger.v("geoCoordinate not available");
    }

    public void setTrafficOverlay(com.navdy.hud.app.maps.NavigationMode navigationMode) {
        if (navigationMode == com.navdy.hud.app.maps.NavigationMode.MAP) {
            setMapTraffic();
        } else if (navigationMode == com.navdy.hud.app.maps.NavigationMode.MAP_ON_ROUTE) {
            setMapOnRouteTraffic();
        }
    }

    public void clearTrafficOverlay() {
        clearMapTraffic();
    }

    public com.here.android.mpa.common.PositioningManager getPositioningManager() {
        return this.positioningManager;
    }

    /* access modifiers changed from: private */
    public void setEngineOnlineState() {
        boolean online = com.navdy.hud.app.framework.network.NetworkStateManager.isConnectedToNetwork(this.context);
        this.lowBandwidthDetected = false;
        sLogger.i("setEngineOnlineState:setting maps engine online state:" + online);
        if (online) {
            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.maps.here.HereMapsManager.Anon6(), 1);
        } else {
            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.maps.here.HereMapsManager.Anon7(), 1);
        }
    }

    /* access modifiers changed from: private */
    public void setMapPoiLayer(com.here.android.mpa.mapping.Map map2) {
        map2.setCartoMarkersVisible(false);
        map2.getMapTransitLayer().setMode(com.here.android.mpa.mapping.MapTransitLayer.Mode.NOTHING);
        map2.setVisibleLayers(java.util.EnumSet.of(com.here.android.mpa.mapping.Map.LayerCategory.ICON_PUBLIC_TRANSIT_STATION, new com.here.android.mpa.mapping.Map.LayerCategory[]{com.here.android.mpa.mapping.Map.LayerCategory.PUBLIC_TRANSIT_LINE, com.here.android.mpa.mapping.Map.LayerCategory.LABEL_PUBLIC_TRANSIT_STATION, com.here.android.mpa.mapping.Map.LayerCategory.LABEL_PUBLIC_TRANSIT_LINE, com.here.android.mpa.mapping.Map.LayerCategory.BEACH, com.here.android.mpa.mapping.Map.LayerCategory.WOODLAND, com.here.android.mpa.mapping.Map.LayerCategory.DESERT, com.here.android.mpa.mapping.Map.LayerCategory.GLACIER, com.here.android.mpa.mapping.Map.LayerCategory.AMUSEMENT_PARK, com.here.android.mpa.mapping.Map.LayerCategory.ANIMAL_PARK, com.here.android.mpa.mapping.Map.LayerCategory.BUILTUP, com.here.android.mpa.mapping.Map.LayerCategory.CEMETERY, com.here.android.mpa.mapping.Map.LayerCategory.BUILDING, com.here.android.mpa.mapping.Map.LayerCategory.NEIGHBORHOOD_AREA, com.here.android.mpa.mapping.Map.LayerCategory.NATIONAL_PARK, com.here.android.mpa.mapping.Map.LayerCategory.NATIVE_RESERVATION}), false);
        java.util.EnumSet<com.here.android.mpa.mapping.Map.LayerCategory> visibleLayers = map2.getVisibleLayers();
        sLogger.v("==== visible layers == [" + visibleLayers.size() + "]");
        java.lang.StringBuilder layers = new java.lang.StringBuilder();
        boolean first = true;
        java.util.Iterator it = visibleLayers.iterator();
        while (it.hasNext()) {
            com.here.android.mpa.mapping.Map.LayerCategory layerCategory = (com.here.android.mpa.mapping.Map.LayerCategory) it.next();
            if (!first) {
                layers.append(", ");
            }
            layers.append(layerCategory.name());
            first = false;
        }
        sLogger.v(layers.toString());
    }

    /* access modifiers changed from: private */
    public void setMapTraffic() {
        this.mapController.execute(new com.navdy.hud.app.maps.here.HereMapsManager.Anon8());
    }

    private void setMapOnRouteTraffic() {
        this.mapController.execute(new com.navdy.hud.app.maps.here.HereMapsManager.Anon9());
    }

    /* access modifiers changed from: 0000 */
    public void clearMapTraffic() {
        this.mapController.execute(new com.navdy.hud.app.maps.here.HereMapsManager.Anon10());
    }

    private boolean isTrafficOverlayVisible() {
        return isTrafficOverlayEnabled(com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().getCurrentNavigationMode());
    }

    private boolean isTrafficOverlayEnabled(com.navdy.hud.app.maps.NavigationMode navigationMode) {
        com.here.android.mpa.mapping.MapTrafficLayer trafficLayer = this.map.getMapTrafficLayer();
        if (navigationMode == com.navdy.hud.app.maps.NavigationMode.MAP) {
            if (!trafficLayer.isEnabled(com.here.android.mpa.mapping.MapTrafficLayer.RenderLayer.FLOW) || !trafficLayer.isEnabled(com.here.android.mpa.mapping.MapTrafficLayer.RenderLayer.INCIDENT)) {
                return false;
            }
            return true;
        } else if (navigationMode == com.navdy.hud.app.maps.NavigationMode.MAP_ON_ROUTE) {
            return trafficLayer.isEnabled(com.here.android.mpa.mapping.MapTrafficLayer.RenderLayer.ONROUTE);
        } else {
            return false;
        }
    }

    public boolean isEngineOnline() {
        return isInitialized() && this.mapEngine != null && com.here.android.mpa.common.MapEngine.isOnlineEnabled();
    }

    /* access modifiers changed from: private */
    public void registerConnectivityReceiver() {
        android.content.IntentFilter filter = new android.content.IntentFilter("android.net.conn.CONNECTIVITY_CHANGE");
        this.context.registerReceiver(new com.navdy.hud.app.maps.here.HereMapsManager.Anon11(), filter);
    }

    public com.navdy.hud.app.maps.here.HereLocationFixManager getLocationFixManager() {
        return this.hereLocationFixManager;
    }

    public com.navdy.hud.app.maps.notification.RouteCalculationEventHandler getRouteCalcEventHandler() {
        return this.routeCalcEventHandler;
    }

    public synchronized void turnOnline() {
        if (!isInitialized()) {
            sLogger.i("turnOnline: engine not yet initialized");
        } else {
            sLogger.i("turnOnline: enabling traffic info");
            requestMapsEngineOnlineStateChange(true);
            if (com.navdy.hud.app.framework.glance.GlanceHelper.isTrafficNotificationEnabled()) {
                sLogger.i("turnOnline: enabling traffic notifications");
                com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().startTrafficRerouteListener();
            } else {
                sLogger.i("turnOnline: not enabling traffic notifications, glance off");
            }
            sLogger.v("turnOnline invokeMapLoader");
            invokeMapLoader();
            sLogger.v("isOnline:" + com.here.android.mpa.common.MapEngine.isOnlineEnabled());
        }
    }

    public synchronized void turnOffline() {
        if (!isInitialized()) {
            sLogger.i("turnOffline: engine not yet initialized");
        } else {
            requestMapsEngineOnlineStateChange(false);
            com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().stopTrafficRerouteListener();
            sLogger.v("isOnline:" + com.here.android.mpa.common.MapEngine.isOnlineEnabled());
        }
    }

    public void requestMapsEngineOnlineStateChange(boolean turnOn) {
        sLogger.d("Request to set online state " + turnOn);
        this.turnEngineOn = turnOn;
        updateEngineOnlineState();
    }

    public synchronized void updateEngineOnlineState() {
        boolean z;
        boolean z2 = true;
        synchronized (this) {
            sLogger.d("Updating the engine online state LowBandwidthDetected :" + this.lowBandwidthDetected + ", TurnEngineOn :" + this.turnEngineOn);
            if (isInitialized()) {
                com.navdy.service.library.log.Logger logger = sLogger;
                java.lang.StringBuilder append = new java.lang.StringBuilder().append("Turning engine on :");
                if (this.lowBandwidthDetected || !this.turnEngineOn) {
                    z = false;
                } else {
                    z = true;
                }
                logger.d(append.append(z).toString());
                if (this.lowBandwidthDetected || !this.turnEngineOn) {
                    z2 = false;
                }
                com.here.android.mpa.common.MapEngine.setOnline(z2);
            }
        }
    }

    @com.squareup.otto.Subscribe
    public void onWakeup(com.navdy.hud.app.event.Wakeup event) {
        if (isInitialized()) {
            startTrafficUpdater();
        }
    }

    @com.squareup.otto.Subscribe
    public void onLinkPropertiesChanged(com.navdy.service.library.events.connection.LinkPropertiesChanged propertiesChanged) {
        if (propertiesChanged != null && propertiesChanged.bandwidthLevel != null) {
            sLogger.d("Link Properties changed");
            if (propertiesChanged.bandwidthLevel.intValue() <= 0) {
                sLogger.d("Switching to low bandwidth mode");
                this.lowBandwidthDetected = true;
                updateEngineOnlineState();
                return;
            }
            sLogger.d("Switching back to normal bandwidth mode");
            this.lowBandwidthDetected = false;
            updateEngineOnlineState();
        }
    }

    public com.here.android.mpa.common.GeoCoordinate getRouteStartPoint() {
        return this.routeStartPoint;
    }

    public void setRouteStartPoint(com.here.android.mpa.common.GeoCoordinate geoCoordinate) {
        this.routeStartPoint = geoCoordinate;
    }

    public boolean isRecalcCurrentRouteForTrafficEnabled() {
        return recalcCurrentRouteForTraffic;
    }

    /* access modifiers changed from: private */
    public void sendExtrapolationEvent() {
        android.os.Bundle bundle = new android.os.Bundle();
        bundle.putBoolean(com.navdy.hud.app.device.gps.GpsConstants.EXTRAPOLATION_FLAG, this.extrapolationOn);
        com.navdy.hud.app.device.gps.GpsUtils.sendEventBroadcast(com.navdy.hud.app.device.gps.GpsConstants.EXTRAPOLATION, bundle);
    }

    public void postManeuverDisplay() {
        if (isInitialized()) {
            com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().postManeuverDisplay();
        }
    }

    public android.os.Looper getBkLocationReceiverLooper() {
        return this.bkLocationReceiverHandlerThread.getLooper();
    }

    public java.lang.String getOfflineMapsData() {
        try {
            java.io.File dir = new java.io.File(com.navdy.hud.app.storage.PathManager.getInstance().getHereMapsDataDirectory());
            if (!dir.exists()) {
                return null;
            }
            java.io.File metaData = new java.io.File(dir, com.navdy.hud.app.storage.PathManager.HERE_MAP_META_JSON_FILE);
            if (metaData.exists()) {
                return com.navdy.service.library.util.IOUtils.convertFileToString(metaData.getAbsolutePath());
            }
            return null;
        } catch (Throwable t) {
            sLogger.e(t);
            return null;
        }
    }

    public void startMapRendering() {
        if (this.hereMapAnimator != null) {
            this.hereMapAnimator.startMapRendering();
        } else {
            this.initialMapRendering = true;
        }
    }

    public void stopMapRendering() {
        if (this.hereMapAnimator != null) {
            this.hereMapAnimator.stopMapRendering();
        } else {
            this.initialMapRendering = false;
        }
    }

    public void hideTraffic() {
        if (!isInitialized()) {
            return;
        }
        if (com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getDriverProfileManager().isTrafficEnabled()) {
            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.maps.here.HereMapsManager.Anon12(), 3);
        } else {
            sLogger.v("hidetraffic: traffic is not enabled");
        }
    }

    public void showTraffic() {
        if (!isInitialized()) {
            return;
        }
        if (com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getDriverProfileManager().isTrafficEnabled()) {
            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.maps.here.HereMapsManager.Anon13(), 3);
        } else {
            sLogger.v("showTraffic: traffic is not enabled");
        }
    }

    public com.navdy.hud.app.maps.here.HereMapAnimator getMapAnimator() {
        return this.hereMapAnimator;
    }

    public void initNavigation() {
        if (isInitialized()) {
            com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().getNavController().initialize();
        }
    }

    public void handleBandwidthPreferenceChange() {
        if (isInitialized()) {
            com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().setBandwidthPreferences();
        }
    }

    public boolean isNavigationModeOn() {
        if (isInitialized()) {
            return com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().isNavigationModeOn();
        }
        return false;
    }

    public boolean isNavigationPossible() {
        if (!isInitialized() || this.hereLocationFixManager.getLastGeoCoordinate() == null) {
            return false;
        }
        return true;
    }

    public java.lang.String getVersion() {
        if (isInitialized()) {
            return com.here.android.mpa.common.Version.getSdkVersion();
        }
        return null;
    }

    public boolean isVoiceSkinsLoaded() {
        return this.voiceSkinsLoaded;
    }

    public void setVoiceSkinsLoaded() {
        this.voiceSkinsLoaded = true;
    }

    public boolean isMapDataVerified() {
        return this.mapDataVerified;
    }

    public int getMapPackageCount() {
        return this.mapPackageCount;
    }

    /* access modifiers changed from: private */
    public void printMapPackages(com.here.android.mpa.odml.MapPackage mapPackage, java.util.EnumSet<com.here.android.mpa.odml.MapPackage.InstallationState> state) {
        if (mapPackage != null) {
            if (state.contains(mapPackage.getInstallationState())) {
                this.mapPackageCount++;
            }
            java.util.List<com.here.android.mpa.odml.MapPackage> children = mapPackage.getChildren();
            if (children != null) {
                for (com.here.android.mpa.odml.MapPackage child : children) {
                    printMapPackages(child, state);
                }
            }
        }
    }

    public void checkForMapDataUpdate() {
        try {
            if (!isInitialized()) {
                sLogger.v("checkForMapDataUpdate MapLoader: engine not initialized");
            } else if (this.mapLoader == null) {
                sLogger.v("checkForMapDataUpdate MapLoader: not available");
            } else if (!this.mapInitLoaderComplete) {
                sLogger.v("checkForMapDataUpdate MapLoader: loader not initialized");
            } else {
                sLogger.v("checkForMapDataUpdate MapLoader: called checkForMapDataUpdate:" + this.mapLoader.checkForMapDataUpdate());
            }
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }
}
