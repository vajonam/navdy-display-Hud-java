package com.navdy.hud.app.maps.here;

class HereNewManeuverListener extends com.here.android.mpa.guidance.NavigationManager.NewInstructionEventListener {
    /* access modifiers changed from: private */
    public final com.squareup.otto.Bus bus;
    /* access modifiers changed from: private */
    public boolean hasNewRoute;
    /* access modifiers changed from: private */
    public final com.navdy.hud.app.maps.here.HereNavigationManager hereNavigationManager;
    /* access modifiers changed from: private */
    public final com.navdy.service.library.log.Logger logger;
    /* access modifiers changed from: private */
    public final com.navdy.hud.app.maps.here.HereNavController navController;
    /* access modifiers changed from: private */
    public com.here.android.mpa.routing.Maneuver navManeuver;
    /* access modifiers changed from: private */
    public final java.lang.String tag;
    /* access modifiers changed from: private */
    public final boolean verbose;

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            try {
                com.here.android.mpa.routing.Maneuver nextManeuver = com.navdy.hud.app.maps.here.HereNewManeuverListener.this.navController.getNextManeuver();
                com.navdy.hud.app.maps.here.HereNewManeuverListener.this.navManeuver = nextManeuver;
                if (nextManeuver == null) {
                    com.navdy.hud.app.maps.here.HereNewManeuverListener.this.logger.w(com.navdy.hud.app.maps.here.HereNewManeuverListener.this.tag + "There is no next maneuver");
                    return;
                }
                com.here.android.mpa.routing.Maneuver maneuverAfterNext = com.navdy.hud.app.maps.here.HereNewManeuverListener.this.navController.getAfterNextManeuver();
                if (com.navdy.hud.app.maps.here.HereNewManeuverListener.this.verbose) {
                    com.navdy.hud.app.maps.here.HereNewManeuverListener.this.logger.v(com.navdy.hud.app.maps.here.HereNewManeuverListener.this.tag + " " + com.navdy.hud.app.maps.here.HereNewManeuverListener.this.navController.getState().name() + " current road[" + com.navdy.hud.app.maps.here.HereMapUtil.getCurrentRoadName() + "] maneuver road name[" + nextManeuver.getRoadName() + "] maneuver next roadname[" + nextManeuver.getNextRoadName() + "] turn[" + nextManeuver.getTurn().name() + "] action[" + nextManeuver.getAction().name() + "] icon[" + nextManeuver.getIcon().name() + "]");
                }
                if (nextManeuver.getAction() == com.here.android.mpa.routing.Maneuver.Action.END || nextManeuver.getIcon() == com.here.android.mpa.routing.Maneuver.Icon.END) {
                    com.navdy.hud.app.maps.here.HereNewManeuverListener.this.logger.v(com.navdy.hud.app.maps.here.HereNewManeuverListener.this.tag + " " + com.navdy.hud.app.maps.here.HereNewManeuverListener.this.navController.getState() + " LAST MANEUVER RECEIVED");
                    maneuverAfterNext = null;
                }
                boolean clearPreviousRoute = com.navdy.hud.app.maps.here.HereNewManeuverListener.this.hasNewRoute;
                com.navdy.hud.app.maps.here.HereNewManeuverListener.this.hasNewRoute = false;
                com.navdy.hud.app.maps.here.HereNewManeuverListener.this.hereNavigationManager.updateNavigationInfo(nextManeuver, maneuverAfterNext, null, clearPreviousRoute);
                if (!com.navdy.hud.app.maps.here.HereNewManeuverListener.this.hereNavigationManager.isShownFirstManeuver()) {
                    com.navdy.hud.app.maps.here.HereNewManeuverListener.this.hereNavigationManager.setShownFirstManeuver(true);
                    if (com.navdy.hud.app.maps.here.HereNewManeuverListener.this.hereNavigationManager.getNavigationSessionPreference().spokenTurnByTurn) {
                        java.lang.String tts = com.navdy.hud.app.maps.here.HereRouteManager.buildStartRouteTTS();
                        com.navdy.hud.app.maps.here.HereNewManeuverListener.this.logger.v("tts[" + tts + "]");
                        com.navdy.hud.app.maps.here.HereNewManeuverListener.this.bus.post(com.navdy.hud.app.maps.notification.RouteCalculationNotification.CANCEL_CALC_TTS);
                        com.navdy.hud.app.framework.voice.TTSUtils.sendSpeechRequest(tts, com.navdy.service.library.events.audio.Category.SPEECH_TURN_BY_TURN, com.navdy.hud.app.maps.notification.RouteCalculationNotification.ROUTE_CALC_TTS_ID);
                    }
                    com.navdy.hud.app.maps.here.HereNewManeuverListener.this.bus.post(new com.navdy.hud.app.maps.MapEvents.ManeuverEvent(com.navdy.hud.app.maps.MapEvents.ManeuverEvent.Type.FIRST, nextManeuver));
                    return;
                }
                com.navdy.hud.app.maps.here.HereNewManeuverListener.this.bus.post(new com.navdy.hud.app.maps.MapEvents.ManeuverEvent(com.navdy.hud.app.maps.MapEvents.ManeuverEvent.Type.INTERMEDIATE, nextManeuver));
            } catch (Throwable t) {
                com.navdy.hud.app.maps.here.HereNewManeuverListener.this.logger.e(t);
            }
        }
    }

    HereNewManeuverListener(com.navdy.service.library.log.Logger logger2, java.lang.String tag2, boolean verbose2, com.navdy.hud.app.maps.here.HereNavController navController2, com.navdy.hud.app.maps.here.HereNavigationManager hereNavigationManager2, com.squareup.otto.Bus bus2) {
        this.logger = logger2;
        this.tag = tag2;
        this.verbose = verbose2;
        this.navController = navController2;
        this.hereNavigationManager = hereNavigationManager2;
        this.bus = bus2;
    }

    public void onNewInstructionEvent() {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.maps.here.HereNewManeuverListener.Anon1(), 20);
    }

    /* access modifiers changed from: 0000 */
    public void setNewRoute() {
        this.logger.v("setNewRoute");
        this.hasNewRoute = true;
        if (this.logger.isLoggable(2)) {
            this.logger.v("setNewRoute: send empty maneuver");
        }
        this.bus.post(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.EMPTY_MANEUVER_DISPLAY);
    }

    /* access modifiers changed from: 0000 */
    public com.here.android.mpa.routing.Maneuver getNavManeuver() {
        return this.navManeuver;
    }

    /* access modifiers changed from: 0000 */
    public void clearNavManeuver() {
        this.navManeuver = null;
    }
}
