package com.navdy.hud.app.maps.here;

public class HereRouteViaGenerator {
    private static final java.lang.String COMMA = ", ";
    private static final java.lang.String EMPTY = "";
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.maps.here.HereRouteViaGenerator.class);
    private static final java.lang.StringBuilder viaBuilder = new java.lang.StringBuilder();

    private static class DistanceContainer implements java.lang.Comparable<com.navdy.hud.app.maps.here.HereRouteViaGenerator.DistanceContainer> {
        int index;
        long order;
        double val;
        java.lang.String via;

        DistanceContainer(double val2, int index2, long order2, java.lang.String via2) {
            this.val = val2;
            this.index = index2;
            this.order = order2;
            this.via = via2;
        }

        public int compareTo(com.navdy.hud.app.maps.here.HereRouteViaGenerator.DistanceContainer another) {
            int diff = this.index - another.index;
            if (diff != 0) {
                return diff;
            }
            int diff2 = ((int) another.val) - ((int) this.val);
            if (diff2 != 0) {
                return diff2;
            }
            return (int) (this.order - another.order);
        }
    }

    public static java.lang.String getViaString(com.here.android.mpa.routing.Route route) {
        return getViaString(route, 1);
    }

    public static java.lang.String getViaString(com.here.android.mpa.routing.Route route, int elements) {
        java.lang.String viaStr;
        java.util.concurrent.atomic.AtomicInteger orderId = new java.util.concurrent.atomic.AtomicInteger(0);
        synchronized (viaBuilder) {
            viaBuilder.setLength(0);
            if (route != null) {
                java.util.List<com.navdy.hud.app.maps.here.HereRouteViaGenerator.DistanceContainer> list = buildDistanceListDesc(null, route.getManeuvers(), orderId, null);
                if (list != null && list.size() > 0) {
                    int len = list.size();
                    for (int i = 0; i < len; i++) {
                        if (viaBuilder.length() > 0) {
                            viaBuilder.append(COMMA);
                        }
                        viaBuilder.append(((com.navdy.hud.app.maps.here.HereRouteViaGenerator.DistanceContainer) list.get(i)).via);
                        elements--;
                        if (elements == 0) {
                            break;
                        }
                    }
                }
            }
            viaStr = viaBuilder.toString();
            viaBuilder.setLength(0);
            sLogger.v("getViaString[" + viaStr + "]");
        }
        return viaStr;
    }

    public static java.lang.String[] generateVia(java.util.List<com.here.android.mpa.routing.RouteResult> results, java.lang.String requestId) {
        sLogger.v("generating via for " + results.size() + " routes");
        java.util.concurrent.atomic.AtomicInteger orderId = new java.util.concurrent.atomic.AtomicInteger(0);
        long l1 = android.os.SystemClock.elapsedRealtime();
        int len = results.size();
        int viaIndex = 0;
        java.lang.String[] viaList = new java.lang.String[len];
        double[] viaDistance = new double[len];
        try {
            java.util.List<com.navdy.hud.app.maps.here.HereRouteViaGenerator.DistanceContainer> list = buildDistanceListDesc(results, null, orderId, requestId);
            if (sLogger.isLoggable(2)) {
                for (com.navdy.hud.app.maps.here.HereRouteViaGenerator.DistanceContainer container : list) {
                    sLogger.v("[" + container.index + "] road[" + container.via + "] distance[" + container.val + "]");
                }
            }
            java.util.HashSet<java.lang.String> seenVias = new java.util.HashSet<>();
            while (list.size() > 0 && viaIndex != viaList.length) {
                if (isRouteCancelled(requestId)) {
                    break;
                }
                int tmpIndex = 0;
                int currentIndex = -1;
                while (true) {
                    com.navdy.hud.app.maps.here.HereRouteViaGenerator.DistanceContainer container2 = (com.navdy.hud.app.maps.here.HereRouteViaGenerator.DistanceContainer) list.get(tmpIndex);
                    if (currentIndex != -1) {
                        if (currentIndex != container2.index) {
                            sLogger.i("no more roadelements,go back");
                            com.navdy.hud.app.maps.here.HereRouteViaGenerator.DistanceContainer c = (com.navdy.hud.app.maps.here.HereRouteViaGenerator.DistanceContainer) list.get(tmpIndex - 1);
                            viaList[viaIndex] = c.via;
                            viaDistance[viaIndex] = c.val;
                            break;
                        }
                    } else {
                        currentIndex = container2.index;
                    }
                    if (!seenVias.contains(container2.via)) {
                        viaList[container2.index] = container2.via;
                        viaDistance[container2.index] = container2.val;
                        seenVias.add(container2.via);
                        break;
                    }
                    sLogger.v("already seen[" + container2.via + "]");
                    if (tmpIndex + 1 == list.size()) {
                        sLogger.i("no more roadelements,use current");
                        break;
                    }
                    tmpIndex++;
                }
                java.util.Iterator<com.navdy.hud.app.maps.here.HereRouteViaGenerator.DistanceContainer> iterator = list.iterator();
                while (iterator.hasNext()) {
                    if (((com.navdy.hud.app.maps.here.HereRouteViaGenerator.DistanceContainer) iterator.next()).index == viaIndex) {
                        iterator.remove();
                    }
                }
                viaIndex++;
            }
            sLogger.v("via algo took[" + (android.os.SystemClock.elapsedRealtime() - l1) + "] len[" + viaList.length + "]");
            for (int i = 0; i < viaList.length; i++) {
                sLogger.v("via[" + (i + 1) + "] [" + viaList[i] + "][" + viaDistance[i] + "]");
            }
        } catch (Throwable th) {
            sLogger.v("via algo took[" + (android.os.SystemClock.elapsedRealtime() - l1) + "] len[" + viaList.length + "]");
            for (int i2 = 0; i2 < viaList.length; i2++) {
                sLogger.v("via[" + (i2 + 1) + "] [" + viaList[i2] + "][" + viaDistance[i2] + "]");
            }
        }
        return viaList;
    }

    private static java.util.List<com.navdy.hud.app.maps.here.HereRouteViaGenerator.DistanceContainer> buildDistanceListDesc(java.util.List<com.here.android.mpa.routing.RouteResult> results, java.util.List<com.here.android.mpa.routing.Maneuver> maneuvers, java.util.concurrent.atomic.AtomicInteger orderId, java.lang.String requestId) {
        java.util.HashMap<java.lang.String, com.navdy.hud.app.maps.here.HereRouteViaGenerator.DistanceContainer> map = new java.util.HashMap<>();
        java.util.ArrayList<com.navdy.hud.app.maps.here.HereRouteViaGenerator.DistanceContainer> list = new java.util.ArrayList<>();
        int index = 0;
        if (results != null) {
            java.util.Iterator it = results.iterator();
            loop0:
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                int counter = 1;
                for (com.here.android.mpa.routing.RouteElement routeElement : ((com.here.android.mpa.routing.RouteResult) it.next()).getRoute().getRouteElements().getElements()) {
                    processRoadElement(routeElement.getRoadElement(), map, index, orderId);
                    if (counter % 5 == 0 && requestId != null && isRouteCancelled(requestId)) {
                        break loop0;
                    }
                    counter++;
                }
                list.addAll(map.values());
                map.clear();
                index++;
            }
            java.util.Collections.sort(list);
        } else {
            for (com.here.android.mpa.routing.Maneuver maneuver : maneuvers) {
                for (com.here.android.mpa.common.RoadElement roadElement : maneuver.getRoadElements()) {
                    processRoadElement(roadElement, map, 0, orderId);
                }
            }
            list.addAll(map.values());
            java.util.Collections.sort(list);
        }
        return list;
    }

    private static void processRoadElement(com.here.android.mpa.common.RoadElement roadElement, java.util.HashMap<java.lang.String, com.navdy.hud.app.maps.here.HereRouteViaGenerator.DistanceContainer> map, int index, java.util.concurrent.atomic.AtomicInteger orderId) {
        java.lang.String name = getDisplayString(roadElement);
        if (!android.text.TextUtils.isEmpty(name)) {
            com.navdy.hud.app.maps.here.HereRouteViaGenerator.DistanceContainer distance = (com.navdy.hud.app.maps.here.HereRouteViaGenerator.DistanceContainer) map.get(name);
            if (distance == null) {
                map.put(name, new com.navdy.hud.app.maps.here.HereRouteViaGenerator.DistanceContainer(roadElement.getGeometryLength(), index, (long) orderId.getAndIncrement(), name));
                return;
            }
            distance.val += roadElement.getGeometryLength();
        }
    }

    private static java.lang.String getDisplayString(com.here.android.mpa.common.RoadElement roadElement) {
        if (roadElement == null) {
            return null;
        }
        java.lang.String str = roadElement.getRouteName();
        if (android.text.TextUtils.isEmpty(str)) {
            return roadElement.getRoadName();
        }
        return str;
    }

    private static boolean isRouteCancelled(java.lang.String requestId) {
        if (requestId == null) {
            return false;
        }
        java.lang.String activeRouteCalcId = com.navdy.hud.app.maps.here.HereRouteManager.getActiveRouteCalcId();
        if (android.text.TextUtils.equals(requestId, activeRouteCalcId)) {
            return false;
        }
        sLogger.v("route request [" + requestId + "] is not active anymore, current [" + activeRouteCalcId + "]");
        return true;
    }
}
