package com.navdy.hud.app.maps.here;

public class HereRouteCalculator {
    private static java.util.concurrent.atomic.AtomicLong ID = new java.util.concurrent.atomic.AtomicLong(1);
    private static final int PROGRESS_INTERVAL_MS = 200;
    private com.here.android.mpa.routing.CoreRouter coreRouter;
    /* access modifiers changed from: private */
    public com.navdy.service.library.log.Logger logger;
    private final java.util.Comparator<com.navdy.service.library.events.navigation.NavigationRouteResult> routeDurationComparator = new com.navdy.hud.app.maps.here.HereRouteCalculator.Anon2();
    private final java.util.Comparator<com.navdy.service.library.events.navigation.NavigationRouteResult> routeLengthComparator = new com.navdy.hud.app.maps.here.HereRouteCalculator.Anon3();
    /* access modifiers changed from: private */
    public java.lang.String tag;
    private boolean verbose;

    class Anon1 implements com.here.android.mpa.routing.CoreRouter.Listener {
        private int lastPercentage = -1;
        private long lastProgress = 0;
        final /* synthetic */ boolean val$allowCancellation;
        final /* synthetic */ boolean val$cache;
        final /* synthetic */ boolean val$calculatePolyline;
        final /* synthetic */ boolean val$factoringInTraffic;
        final /* synthetic */ com.navdy.hud.app.maps.here.HereRouteCalculator.RouteCalculatorListener val$listener;
        final /* synthetic */ com.navdy.service.library.events.navigation.NavigationRouteRequest val$request;
        final /* synthetic */ com.here.android.mpa.routing.RouteOptions val$routeOptions;
        final /* synthetic */ com.here.android.mpa.common.GeoCoordinate val$startPoint;
        final /* synthetic */ long val$startTime;

        /* renamed from: com.navdy.hud.app.maps.here.HereRouteCalculator$Anon1$Anon1 reason: collision with other inner class name */
        class C0023Anon1 implements java.lang.Runnable {
            final /* synthetic */ java.util.List val$results;
            final /* synthetic */ com.here.android.mpa.routing.RoutingError val$routingError;

            C0023Anon1(com.here.android.mpa.routing.RoutingError routingError, java.util.List list) {
                this.val$routingError = routingError;
                this.val$results = list;
            }

            public void run() {
                com.navdy.service.library.events.navigation.NavigationRouteResult outgoingRouteResult;
                try {
                    long l2 = android.os.SystemClock.elapsedRealtime();
                    if ((this.val$routingError == com.here.android.mpa.routing.RoutingError.NONE || this.val$routingError == com.here.android.mpa.routing.RoutingError.VIOLATES_OPTIONS) && this.val$results.size() > 0) {
                        com.navdy.hud.app.maps.here.HereRouteCalculator.Anon1.this.val$listener.preSuccess();
                        java.util.ArrayList arrayList = new java.util.ArrayList(this.val$results.size());
                        java.lang.String requestId = null;
                        if (com.navdy.hud.app.maps.here.HereRouteCalculator.Anon1.this.val$allowCancellation && com.navdy.hud.app.maps.here.HereRouteCalculator.Anon1.this.val$request != null) {
                            requestId = com.navdy.hud.app.maps.here.HereRouteCalculator.Anon1.this.val$request.requestId;
                        }
                        java.lang.String[] viaList = com.navdy.hud.app.maps.here.HereRouteViaGenerator.generateVia(this.val$results, requestId);
                        int index = 0;
                        for (com.here.android.mpa.routing.RouteResult routeResult : this.val$results) {
                            if (!com.navdy.hud.app.maps.here.HereRouteCalculator.Anon1.this.val$allowCancellation || !com.navdy.hud.app.maps.here.HereRouteCalculator.this.isRouteCancelled(com.navdy.hud.app.maps.here.HereRouteCalculator.Anon1.this.val$request)) {
                                com.here.android.mpa.routing.Route route = routeResult.getRoute();
                                java.lang.String id = java.util.UUID.randomUUID().toString();
                                int index2 = index + 1;
                                java.lang.String via = viaList[index];
                                if (android.text.TextUtils.isEmpty(via)) {
                                    via = com.navdy.hud.app.HudApplication.getAppContext().getResources().getString(com.navdy.hud.app.R.string.route_num, new java.lang.Object[]{java.lang.Integer.valueOf(index2)});
                                }
                                java.util.EnumSet<com.here.android.mpa.routing.RouteResult.ViolatedOption> violatedOptions = routeResult.getViolatedOptions();
                                if (violatedOptions != null && violatedOptions.size() > 0) {
                                    java.util.Iterator it = violatedOptions.iterator();
                                    while (it.hasNext()) {
                                        com.navdy.hud.app.maps.here.HereRouteCalculator.this.logger.v(com.navdy.hud.app.maps.here.HereRouteCalculator.this.tag + " violated options [" + ((com.here.android.mpa.routing.RouteResult.ViolatedOption) it.next()).name() + "]");
                                    }
                                }
                                if (com.navdy.hud.app.maps.here.HereRouteCalculator.Anon1.this.val$request != null) {
                                    outgoingRouteResult = com.navdy.hud.app.maps.here.HereRouteCalculator.this.getRouteResultFromRoute(id, route, via, com.navdy.hud.app.maps.here.HereRouteCalculator.Anon1.this.val$request.label, com.navdy.hud.app.maps.here.HereRouteCalculator.Anon1.this.val$request.streetAddress, com.navdy.hud.app.maps.here.HereRouteCalculator.Anon1.this.val$calculatePolyline);
                                } else {
                                    outgoingRouteResult = com.navdy.hud.app.maps.here.HereRouteCalculator.this.getRouteResultFromRoute(id, route, via, null, null, com.navdy.hud.app.maps.here.HereRouteCalculator.Anon1.this.val$calculatePolyline);
                                }
                                arrayList.add(outgoingRouteResult);
                                if (com.navdy.hud.app.maps.here.HereRouteCalculator.Anon1.this.val$cache) {
                                    com.navdy.hud.app.maps.here.HereRouteCache.getInstance().addRoute(id, new com.navdy.hud.app.maps.here.HereRouteCache.RouteInfo(route, com.navdy.hud.app.maps.here.HereRouteCalculator.Anon1.this.val$request, outgoingRouteResult, com.navdy.hud.app.maps.here.HereRouteCalculator.Anon1.this.val$factoringInTraffic, com.navdy.hud.app.maps.here.HereRouteCalculator.Anon1.this.val$startPoint));
                                }
                                if (com.navdy.hud.app.maps.here.HereRouteCalculator.this.logger.isLoggable(2)) {
                                    com.navdy.hud.app.maps.here.HereMapUtil.printRouteDetails(route, com.navdy.hud.app.maps.here.HereRouteCalculator.Anon1.this.val$request == null ? "" : com.navdy.hud.app.maps.here.HereRouteCalculator.Anon1.this.val$request.streetAddress, com.navdy.hud.app.maps.here.HereRouteCalculator.Anon1.this.val$request, null);
                                }
                                if (com.navdy.hud.app.maps.MapSettings.isGenerateRouteIcons()) {
                                    com.navdy.hud.app.maps.here.HereMapUtil.generateRouteIcons(id, com.navdy.hud.app.maps.here.HereRouteCalculator.Anon1.this.val$request.label, route);
                                }
                                index = index2;
                            } else {
                                return;
                            }
                        }
                        if (!com.navdy.hud.app.maps.here.HereRouteCalculator.Anon1.this.val$allowCancellation || !com.navdy.hud.app.maps.here.HereRouteCalculator.this.isRouteCancelled(com.navdy.hud.app.maps.here.HereRouteCalculator.Anon1.this.val$request)) {
                            java.util.Collections.sort(arrayList, com.navdy.hud.app.maps.here.HereRouteCalculator.this.getComparator(com.navdy.hud.app.maps.here.HereRouteCalculator.Anon1.this.val$routeOptions.getRouteType()));
                            com.navdy.hud.app.maps.here.HereRouteCalculator.Anon1.this.val$listener.postSuccess(arrayList);
                        } else {
                            return;
                        }
                    } else {
                        int resultsSize = -1;
                        if (this.val$results != null) {
                            resultsSize = this.val$results.size();
                        }
                        com.navdy.hud.app.maps.here.HereRouteCalculator.this.logger.e(com.navdy.hud.app.maps.here.HereRouteCalculator.this.tag + "got error:" + this.val$routingError + " results=" + resultsSize);
                        com.navdy.hud.app.maps.here.HereRouteCalculator.Anon1.this.val$listener.error(this.val$routingError, null);
                    }
                    com.navdy.hud.app.maps.here.HereRouteCalculator.this.logger.v(com.navdy.hud.app.maps.here.HereRouteCalculator.this.tag + "handleSearchRequest- time-2=" + (android.os.SystemClock.elapsedRealtime() - l2));
                } catch (Throwable t) {
                    com.navdy.hud.app.maps.here.HereRouteCalculator.this.logger.e(com.navdy.hud.app.maps.here.HereRouteCalculator.this.tag, t);
                    com.navdy.hud.app.maps.here.HereRouteCalculator.Anon1.this.val$listener.error(null, t);
                }
            }
        }

        Anon1(com.navdy.hud.app.maps.here.HereRouteCalculator.RouteCalculatorListener routeCalculatorListener, boolean z, com.navdy.service.library.events.navigation.NavigationRouteRequest navigationRouteRequest, boolean z2, boolean z3, boolean z4, com.here.android.mpa.common.GeoCoordinate geoCoordinate, com.here.android.mpa.routing.RouteOptions routeOptions, long j) {
            this.val$listener = routeCalculatorListener;
            this.val$allowCancellation = z;
            this.val$request = navigationRouteRequest;
            this.val$calculatePolyline = z2;
            this.val$cache = z3;
            this.val$factoringInTraffic = z4;
            this.val$startPoint = geoCoordinate;
            this.val$routeOptions = routeOptions;
            this.val$startTime = j;
        }

        public void onCalculateRouteFinished(java.util.List<com.here.android.mpa.routing.RouteResult> results, com.here.android.mpa.routing.RoutingError routingError) {
            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.maps.here.HereRouteCalculator.Anon1.C0023Anon1(routingError, results), 2);
        }

        public void onProgress(int percentage) {
            long now = android.os.SystemClock.elapsedRealtime();
            if (percentage == this.lastPercentage) {
                return;
            }
            if (now > this.lastProgress + 200 || percentage == 100) {
                this.lastPercentage = percentage;
                this.lastProgress = now;
                com.navdy.hud.app.maps.here.HereRouteCalculator.this.logger.i(com.navdy.hud.app.maps.here.HereRouteCalculator.this.tag + "route calculation: [" + percentage + "%] done timeElapsed:" + (now - this.val$startTime));
                this.val$listener.progress(percentage);
            }
        }
    }

    class Anon2 implements java.util.Comparator<com.navdy.service.library.events.navigation.NavigationRouteResult> {
        Anon2() {
        }

        public int compare(com.navdy.service.library.events.navigation.NavigationRouteResult o1, com.navdy.service.library.events.navigation.NavigationRouteResult o2) {
            return java.lang.Integer.compare(o1.duration_traffic.intValue(), o2.duration_traffic.intValue());
        }
    }

    class Anon3 implements java.util.Comparator<com.navdy.service.library.events.navigation.NavigationRouteResult> {
        Anon3() {
        }

        public int compare(com.navdy.service.library.events.navigation.NavigationRouteResult o1, com.navdy.service.library.events.navigation.NavigationRouteResult o2) {
            return java.lang.Integer.compare(o1.length.intValue(), o2.length.intValue());
        }
    }

    static /* synthetic */ class Anon4 {
        static final /* synthetic */ int[] $SwitchMap$com$here$android$mpa$routing$RouteOptions$Type = new int[com.here.android.mpa.routing.RouteOptions.Type.values().length];

        static {
            try {
                $SwitchMap$com$here$android$mpa$routing$RouteOptions$Type[com.here.android.mpa.routing.RouteOptions.Type.SHORTEST.ordinal()] = 1;
            } catch (java.lang.NoSuchFieldError e) {
            }
            try {
                $SwitchMap$com$here$android$mpa$routing$RouteOptions$Type[com.here.android.mpa.routing.RouteOptions.Type.FASTEST.ordinal()] = 2;
            } catch (java.lang.NoSuchFieldError e2) {
            }
        }
    }

    public interface RouteCalculatorListener {
        void error(com.here.android.mpa.routing.RoutingError routingError, java.lang.Throwable th);

        void postSuccess(java.util.ArrayList<com.navdy.service.library.events.navigation.NavigationRouteResult> arrayList);

        void preSuccess();

        void progress(int i);
    }

    public HereRouteCalculator(com.navdy.service.library.log.Logger logger2, boolean verbose2) {
        this.logger = logger2;
        this.verbose = verbose2;
        this.tag = "[" + ID.getAndIncrement() + "]";
    }

    public void calculateRoute(com.navdy.service.library.events.navigation.NavigationRouteRequest request, com.here.android.mpa.common.GeoCoordinate startPoint, java.util.List<com.here.android.mpa.common.GeoCoordinate> waypoints, com.here.android.mpa.common.GeoCoordinate endPoint, boolean factoringInTraffic, com.navdy.hud.app.maps.here.HereRouteCalculator.RouteCalculatorListener listener, int maxRoutes, com.here.android.mpa.routing.RouteOptions routeOptions, boolean allowCancellation) {
        calculateRoute(request, startPoint, waypoints, endPoint, factoringInTraffic, listener, maxRoutes, routeOptions, true, true, allowCancellation);
    }

    public void calculateRoute(com.navdy.service.library.events.navigation.NavigationRouteRequest request, com.here.android.mpa.common.GeoCoordinate startPoint, java.util.List<com.here.android.mpa.common.GeoCoordinate> waypoints, com.here.android.mpa.common.GeoCoordinate endPoint, boolean factoringInTraffic, com.navdy.hud.app.maps.here.HereRouteCalculator.RouteCalculatorListener listener, int maxRoutes, com.here.android.mpa.routing.RouteOptions routeOptions, boolean cache, boolean calculatePolyline, boolean allowCancellation) {
        this.coreRouter = new com.here.android.mpa.routing.CoreRouter();
        com.here.android.mpa.routing.RoutePlan routePlan = new com.here.android.mpa.routing.RoutePlan();
        routeOptions.setRouteCount(maxRoutes);
        routePlan.setRouteOptions(routeOptions);
        routePlan.addWaypoint(new com.here.android.mpa.routing.RouteWaypoint(startPoint));
        if (waypoints != null) {
            for (com.here.android.mpa.common.GeoCoordinate waypoint : waypoints) {
                routePlan.addWaypoint(new com.here.android.mpa.routing.RouteWaypoint(waypoint));
            }
        }
        routePlan.addWaypoint(new com.here.android.mpa.routing.RouteWaypoint(endPoint));
        com.here.android.mpa.routing.DynamicPenalty dynamicPenalty = new com.here.android.mpa.routing.DynamicPenalty();
        if (factoringInTraffic) {
            this.logger.v(this.tag + "Factoring traffic in route calc[ONLINE]");
            dynamicPenalty.setTrafficPenaltyMode(com.here.android.mpa.routing.Route.TrafficPenaltyMode.OPTIMAL);
            this.coreRouter.setConnectivity(com.here.android.mpa.routing.CoreRouter.Connectivity.ONLINE);
        } else {
            this.logger.v(this.tag + "Not factoring traffic in route calc[OFFLINE]");
            dynamicPenalty.setTrafficPenaltyMode(com.here.android.mpa.routing.Route.TrafficPenaltyMode.DISABLED);
            this.coreRouter.setConnectivity(com.here.android.mpa.routing.CoreRouter.Connectivity.OFFLINE);
        }
        this.coreRouter.setDynamicPenalty(dynamicPenalty);
        this.logger.i(this.tag + "Generating route with " + routePlan.getWaypointCount() + " waypoints");
        long startTime = android.os.SystemClock.elapsedRealtime();
        this.coreRouter.calculateRoute(routePlan, new com.navdy.hud.app.maps.here.HereRouteCalculator.Anon1(listener, allowCancellation, request, calculatePolyline, cache, factoringInTraffic, startPoint, routeOptions, startTime));
    }

    /* access modifiers changed from: private */
    public java.util.Comparator<com.navdy.service.library.events.navigation.NavigationRouteResult> getComparator(com.here.android.mpa.routing.RouteOptions.Type routeType) {
        switch (com.navdy.hud.app.maps.here.HereRouteCalculator.Anon4.$SwitchMap$com$here$android$mpa$routing$RouteOptions$Type[routeType.ordinal()]) {
            case 1:
                return this.routeLengthComparator;
            default:
                return this.routeDurationComparator;
        }
    }

    public com.navdy.service.library.events.navigation.NavigationRouteResult getRouteResultFromRoute(java.lang.String routeId, com.here.android.mpa.routing.Route route, java.lang.String via, java.lang.String label, java.lang.String address, boolean calculatePolyline) {
        java.util.List<java.lang.Float> simplified = null;
        java.util.List<com.navdy.service.library.events.location.Coordinate> oldList = new java.util.ArrayList<>();
        if (calculatePolyline) {
            long t1 = android.os.SystemClock.elapsedRealtime();
            java.util.List<com.here.android.mpa.common.GeoCoordinate> routeGeometry = route.getRouteGeometry();
            this.logger.v(this.tag + "getting geometry took " + (android.os.SystemClock.elapsedRealtime() - t1) + " ms");
            this.logger.v(this.tag + "points before simplification: " + routeGeometry.size());
            java.util.List<java.lang.Float> simplified2 = com.navdy.hud.app.maps.util.RouteUtils.simplify(routeGeometry);
            this.logger.v(this.tag + "points after simplification: " + (simplified2.size() / 2));
            com.navdy.service.library.events.location.Coordinate first = new com.navdy.service.library.events.location.Coordinate(java.lang.Double.valueOf(((com.here.android.mpa.common.GeoCoordinate) routeGeometry.get(0)).getLatitude()), java.lang.Double.valueOf(((com.here.android.mpa.common.GeoCoordinate) routeGeometry.get(0)).getLongitude()), null, null, null, null, null, null);
            oldList = new java.util.ArrayList<>();
            oldList.add(first);
            simplified = simplified2;
        } else {
            this.logger.v(this.tag + " poly line calc off");
        }
        int trafficDuration = route.getTta(com.here.android.mpa.routing.Route.TrafficPenaltyMode.OPTIMAL, com.glympse.android.lib.e.gA).getDuration();
        int length = route.getLength();
        int freeFlowDuration = route.getTta(com.here.android.mpa.routing.Route.TrafficPenaltyMode.DISABLED, com.glympse.android.lib.e.gA).getDuration();
        com.navdy.service.library.events.navigation.NavigationRouteResult outgoingRouteResult = new com.navdy.service.library.events.navigation.NavigationRouteResult(routeId, label, oldList, java.lang.Integer.valueOf(length), java.lang.Integer.valueOf(freeFlowDuration), java.lang.Integer.valueOf(trafficDuration), simplified, via, address);
        this.logger.v(this.tag + "route id[" + routeId + "] via[" + via + "] length[" + length + "] duration[" + trafficDuration + "] freeFlowDuration[" + freeFlowDuration + "]");
        return outgoingRouteResult;
    }

    public void cancel() {
        try {
            if (this.coreRouter != null) {
                this.coreRouter.cancel();
            }
        } catch (Throwable t) {
            this.logger.e(this.tag, t);
        } finally {
            this.coreRouter = null;
        }
    }

    /* access modifiers changed from: private */
    public boolean isRouteCancelled(com.navdy.service.library.events.navigation.NavigationRouteRequest request) {
        if (request == null) {
            return false;
        }
        java.lang.String activeRouteCalcId = com.navdy.hud.app.maps.here.HereRouteManager.getActiveRouteCalcId();
        if (android.text.TextUtils.equals(request.requestId, activeRouteCalcId)) {
            return false;
        }
        this.logger.v("route request [" + request.requestId + "] is not active anymore, current [" + activeRouteCalcId + "]");
        return true;
    }
}
