package com.navdy.hud.app.maps.here;

class HereTrafficRerouteListener extends com.here.android.mpa.guidance.NavigationManager.TrafficRerouteListener {
    static final int MIN_INTERVAL_REROUTE_FIRST = 90;
    static final long NOTIF_FROM_HERE_THRESHOLD = java.util.concurrent.TimeUnit.MINUTES.toMillis(1);
    static final long REROUTE_POINT_THRESHOLD = java.util.concurrent.TimeUnit.MINUTES.toSeconds(20);
    static final int ROUTE_CHECK_INITIAL_INTERVAL = ((int) java.util.concurrent.TimeUnit.MINUTES.toMillis(1));
    static final int ROUTE_CHECK_INTERVAL = ((int) java.util.concurrent.TimeUnit.MINUTES.toMillis(5));
    static final int ROUTE_CHECK_INTERVAL_LIMIT_BANDWIDTH = ((int) java.util.concurrent.TimeUnit.MINUTES.toMillis(30));
    static final int WAIT_INTERVAL_AFTER_FIRST_MANEUVER_SHOWN = ((int) java.util.concurrent.TimeUnit.SECONDS.toMillis(60));
    private final com.squareup.otto.Bus bus;
    /* access modifiers changed from: private */
    public com.here.android.mpa.routing.Route currentProposedRoute;
    private long fasterBy;
    /* access modifiers changed from: private */
    public final com.navdy.hud.app.maps.here.HereNavigationManager hereNavigationManager;
    private long lastTrafficNotifFromHere;
    /* access modifiers changed from: private */
    public final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger("HereTrafficReroute");
    /* access modifiers changed from: private */
    public final com.navdy.hud.app.maps.here.HereNavController navController;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager navdyTrafficRerouteManager;
    private boolean notifFromHere;
    private com.here.android.mpa.common.GeoCoordinate placeCalculated;
    private java.lang.Runnable startRunnable = new com.navdy.hud.app.maps.here.HereTrafficRerouteListener.Anon1();
    private java.lang.Runnable stopRunnable = new com.navdy.hud.app.maps.here.HereTrafficRerouteListener.Anon2();
    /* access modifiers changed from: private */
    public final java.lang.String tag;
    private long timeCalculated;
    private boolean trafficRerouteListenerRunning;
    private final boolean verbose;
    private java.lang.String viaString;

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            com.navdy.hud.app.maps.here.HereTrafficRerouteListener.this.navController.addTrafficRerouteListener(new java.lang.ref.WeakReference(com.navdy.hud.app.maps.here.HereTrafficRerouteListener.this));
            com.navdy.hud.app.maps.here.HereTrafficRerouteListener.this.logger.v("addTrafficRerouteListener");
        }
    }

    class Anon2 implements java.lang.Runnable {
        Anon2() {
        }

        public void run() {
            com.navdy.hud.app.maps.here.HereTrafficRerouteListener.this.navController.removeTrafficRerouteListener(com.navdy.hud.app.maps.here.HereTrafficRerouteListener.this);
            com.navdy.hud.app.maps.here.HereTrafficRerouteListener.this.logger.v("removeTrafficRerouteListener");
        }
    }

    class Anon3 implements java.lang.Runnable {
        Anon3() {
        }

        public void run() {
            if (com.navdy.hud.app.maps.here.HereTrafficRerouteListener.this.currentProposedRoute != null) {
                com.navdy.hud.app.maps.here.HereTrafficRerouteListener.this.hereNavigationManager.clearNavManeuver();
                com.here.android.mpa.guidance.NavigationManager.Error error = com.navdy.hud.app.maps.here.HereTrafficRerouteListener.this.navController.setRoute(com.navdy.hud.app.maps.here.HereTrafficRerouteListener.this.currentProposedRoute);
                if (error == com.here.android.mpa.guidance.NavigationManager.Error.NONE) {
                    com.navdy.hud.app.maps.here.HereTrafficRerouteListener.this.hereNavigationManager.updateMapRoute(com.navdy.hud.app.maps.here.HereTrafficRerouteListener.this.currentProposedRoute, com.navdy.service.library.events.navigation.NavigationSessionRouteChange.RerouteReason.NAV_SESSION_TRAFFIC_REROUTE, null, null, false, true);
                    com.navdy.hud.app.maps.here.HereTrafficRerouteListener.this.logger.e("confirmReroute: set");
                } else {
                    com.navdy.hud.app.maps.here.HereTrafficRerouteListener.this.logger.e("confirmReroute: traffic route could not be set:" + error);
                }
                com.navdy.hud.app.maps.here.HereTrafficRerouteListener.this.clearProposedRoute();
                return;
            }
            com.navdy.hud.app.maps.here.HereTrafficRerouteListener.this.logger.e("confirmReroute: no route available");
        }
    }

    class Anon4 implements java.lang.Runnable {
        final /* synthetic */ com.here.android.mpa.routing.Route val$fasterRoute;
        final /* synthetic */ boolean val$notifFromHereTraffic;

        Anon4(boolean z, com.here.android.mpa.routing.Route route) {
            this.val$notifFromHereTraffic = z;
            this.val$fasterRoute = route;
        }

        public void run() {
            try {
                java.lang.String fromTag = this.val$notifFromHereTraffic ? "[Here Traffic]" : "[Navdy Calc]";
                com.navdy.hud.app.maps.here.HereTrafficRerouteListener.this.logger.v(com.navdy.hud.app.maps.here.HereTrafficRerouteListener.this.tag + fromTag + " onTrafficRerouted");
                com.navdy.hud.app.maps.here.HereTrafficRerouteListener.this.dismissReroute();
                if (!com.navdy.hud.app.maps.here.HereTrafficRerouteListener.this.hereNavigationManager.isNavigationModeOn()) {
                    com.navdy.hud.app.maps.here.HereTrafficRerouteListener.this.logger.v(com.navdy.hud.app.maps.here.HereTrafficRerouteListener.this.tag + " onTrafficRerouted, not currently navigating");
                } else if (com.navdy.hud.app.maps.here.HereTrafficRerouteListener.this.hereNavigationManager.hasArrived()) {
                    com.navdy.hud.app.maps.here.HereTrafficRerouteListener.this.logger.v("onTrafficRerouted, arrival mode on");
                } else if (com.navdy.hud.app.maps.here.HereTrafficRerouteListener.this.hereNavigationManager.isOnGasRoute()) {
                    com.navdy.hud.app.maps.here.HereTrafficRerouteListener.this.logger.v("onTrafficRerouted, on fas route");
                } else {
                    com.here.android.mpa.routing.Route currentRoute = com.navdy.hud.app.maps.here.HereTrafficRerouteListener.this.hereNavigationManager.getCurrentRoute();
                    if (currentRoute == null) {
                        com.navdy.hud.app.maps.here.HereTrafficRerouteListener.this.logger.v(com.navdy.hud.app.maps.here.HereTrafficRerouteListener.this.tag + " onTrafficRerouted, not current route");
                        return;
                    }
                    java.util.List<com.here.android.mpa.routing.Maneuver> currentManeuvers = currentRoute.getManeuvers();
                    if (this.val$notifFromHereTraffic) {
                        java.lang.String currentRoadNames = com.navdy.hud.app.maps.here.HereMapUtil.getAllRoadNames(this.val$fasterRoute.getManeuvers(), 0);
                        java.lang.String newRoadNames = com.navdy.hud.app.maps.here.HereMapUtil.getAllRoadNames(currentManeuvers, 0);
                        com.navdy.hud.app.maps.here.HereTrafficRerouteListener.this.logger.v("[HereNotif-RoadNames-current]");
                        com.navdy.hud.app.maps.here.HereTrafficRerouteListener.this.logger.v(currentRoadNames);
                        com.navdy.hud.app.maps.here.HereTrafficRerouteListener.this.logger.v("[HereNotif-RoadNames-faster]");
                        com.navdy.hud.app.maps.here.HereTrafficRerouteListener.this.logger.v(newRoadNames);
                    }
                    com.here.android.mpa.routing.RouteTta fasterRouteTta = this.val$fasterRoute.getTta(com.here.android.mpa.routing.Route.TrafficPenaltyMode.OPTIMAL, com.glympse.android.lib.e.gA);
                    com.here.android.mpa.routing.RouteTta fasterRouteTtaNoTraffic = this.val$fasterRoute.getTta(com.here.android.mpa.routing.Route.TrafficPenaltyMode.DISABLED, com.glympse.android.lib.e.gA);
                    if (fasterRouteTta == null || fasterRouteTtaNoTraffic == null) {
                        com.navdy.hud.app.maps.here.HereTrafficRerouteListener.this.logger.v(com.navdy.hud.app.maps.here.HereTrafficRerouteListener.this.tag + "error retrieving navigation and TTA");
                        return;
                    }
                    if (!this.val$notifFromHereTraffic) {
                        long d1 = (long) fasterRouteTta.getDuration();
                        if (d1 == ((long) fasterRouteTtaNoTraffic.getDuration())) {
                            com.navdy.hud.app.maps.here.HereTrafficRerouteListener.this.logger.v(com.navdy.hud.app.maps.here.HereTrafficRerouteListener.this.tag + "faster route durations is same=" + d1);
                            return;
                        }
                    }
                    long oldTtaDuration = 0;
                    com.here.android.mpa.routing.RouteTta currentRouteTta = currentRoute.getTta(com.here.android.mpa.routing.Route.TrafficPenaltyMode.OPTIMAL, com.glympse.android.lib.e.gA);
                    if (currentRouteTta != null) {
                        oldTtaDuration = (long) currentRouteTta.getDuration();
                    }
                    java.util.Date etaDate = com.navdy.hud.app.maps.here.HereTrafficRerouteListener.this.navController.getEta(true, com.here.android.mpa.routing.Route.TrafficPenaltyMode.OPTIMAL);
                    if (!com.navdy.hud.app.maps.here.HereMapUtil.isValidEtaDate(etaDate)) {
                        com.navdy.hud.app.maps.here.HereTrafficRerouteListener.this.logger.v(com.navdy.hud.app.maps.here.HereTrafficRerouteListener.this.tag + "error retrieving eta for current");
                        return;
                    }
                    long etaUtc = etaDate.getTime();
                    long now = java.lang.System.currentTimeMillis();
                    long oldTta = (etaUtc - now) / 1000;
                    long newTta = (long) fasterRouteTta.getDuration();
                    long diff = oldTta - newTta;
                    com.navdy.hud.app.maps.here.HereTrafficRerouteListener.this.logger.i("etaDate[" + etaDate + "] etaUtc[" + etaUtc + "] currentTta[" + oldTta + "] now[" + now + "] newTta[" + newTta + "]" + "] diff[" + diff + "]" + "] oldTtaDuration[" + oldTtaDuration + "]");
                    java.util.HashSet<java.lang.String> seenVia = new java.util.HashSet<>();
                    java.util.ArrayList arrayList = new java.util.ArrayList(1);
                    java.util.List<com.here.android.mpa.routing.Maneuver> newRouteDifferentManeuver = com.navdy.hud.app.maps.here.HereMapUtil.getDifferentManeuver(this.val$fasterRoute, currentRoute, 2, arrayList);
                    if (newRouteDifferentManeuver.size() == 0) {
                        com.navdy.hud.app.maps.here.HereTrafficRerouteListener.this.logger.i("could not find a difference in maneuvers");
                        return;
                    }
                    java.lang.String via = null;
                    java.lang.StringBuilder viaBuilder = new java.lang.StringBuilder();
                    for (com.here.android.mpa.routing.Maneuver m : newRouteDifferentManeuver) {
                        java.lang.String str = com.navdy.hud.app.maps.here.HereMapUtil.getNextRoadName(m);
                        if (via == null) {
                            via = str;
                            viaBuilder.append(via);
                            seenVia.add(via);
                        } else if (!seenVia.contains(str)) {
                            viaBuilder.append(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA);
                            viaBuilder.append(" ");
                            viaBuilder.append(str);
                            seenVia.add(str);
                        }
                    }
                    java.lang.String str2 = com.navdy.hud.app.maps.here.HereRouteViaGenerator.getViaString(this.val$fasterRoute);
                    if (!seenVia.contains(str2)) {
                        viaBuilder.append(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA);
                        viaBuilder.append(" ");
                        viaBuilder.append(str2);
                    }
                    java.lang.String currentVia = null;
                    com.navdy.hud.app.maps.here.HereRouteCache.RouteInfo routeInfo = com.navdy.hud.app.maps.here.HereRouteCache.getInstance().getRoute(com.navdy.hud.app.maps.here.HereTrafficRerouteListener.this.hereNavigationManager.getCurrentRouteId());
                    if (routeInfo != null) {
                        currentVia = routeInfo.routeResult.via;
                    }
                    java.lang.String additionalVia = viaBuilder.toString();
                    long distanceDiff = ((long) this.val$fasterRoute.getLength()) - com.navdy.hud.app.maps.here.HereTrafficRerouteListener.this.navController.getDestinationDistance();
                    int minDuration = com.navdy.hud.app.maps.here.HereTrafficRerouteListener.this.getRerouteMinDuration();
                    com.navdy.hud.app.maps.here.HereTrafficRerouteListener.this.logger.v(com.navdy.hud.app.maps.here.HereTrafficRerouteListener.this.tag + fromTag + " new route via[" + via + "] additionalVia[" + additionalVia + "] is faster by[" + diff + "] seconds than current route via[" + currentVia + "] distanceDiff [" + distanceDiff + "] minDur[" + minDuration + "]");
                    if (diff >= ((long) minDuration)) {
                        com.navdy.hud.app.maps.here.HereTrafficRerouteListener.this.logger.v("faster route is above threshold:" + diff);
                        if (!com.navdy.hud.app.maps.here.HereTrafficRerouteListener.this.isFasterRouteDivergePointCloseBy(currentManeuvers, arrayList, newRouteDifferentManeuver)) {
                            com.navdy.hud.app.maps.here.HereTrafficRerouteListener.this.logger.v("faster route not near by, threshold not met");
                        } else {
                            com.navdy.hud.app.maps.here.HereTrafficRerouteListener.this.setFasterRoute(this.val$fasterRoute, this.val$notifFromHereTraffic, via, diff, additionalVia, etaUtc, distanceDiff, 0);
                        }
                    } else {
                        com.navdy.hud.app.maps.here.HereTrafficRerouteListener.this.logger.v("faster route is below threshold:" + diff);
                        if (!com.navdy.hud.app.maps.here.HereMapsManager.getInstance().isRecalcCurrentRouteForTrafficEnabled()) {
                            com.navdy.hud.app.maps.here.HereTrafficRerouteListener.this.fasterRouteNotFound(this.val$notifFromHereTraffic, diff, via, currentVia);
                        } else if (!com.navdy.hud.app.maps.here.HereTrafficRerouteListener.this.isFasterRouteDivergePointCloseBy(currentManeuvers, arrayList, newRouteDifferentManeuver)) {
                            com.navdy.hud.app.maps.here.HereTrafficRerouteListener.this.logger.v("faster route not near by, threshold not met");
                        } else {
                            com.navdy.hud.app.maps.here.HereTrafficRerouteListener.this.navdyTrafficRerouteManager.reCalculateCurrentRoute(currentRoute, this.val$fasterRoute, this.val$notifFromHereTraffic, via, diff, additionalVia, etaUtc, distanceDiff, currentVia, fasterRouteTta);
                        }
                    }
                }
            } catch (Throwable t) {
                com.navdy.hud.app.maps.here.HereTrafficRerouteListener.this.logger.e(t);
                com.navdy.hud.app.util.CrashReporter.getInstance().reportNonFatalException(t);
            }
        }
    }

    HereTrafficRerouteListener(java.lang.String tag2, boolean verbose2, com.navdy.hud.app.maps.here.HereNavController navController2, com.navdy.hud.app.maps.here.HereNavigationManager hereNavigationManager2, com.squareup.otto.Bus bus2) {
        this.logger.v("HereTrafficRerouteListener:ctor");
        this.tag = tag2;
        this.verbose = verbose2;
        this.navController = navController2;
        this.hereNavigationManager = hereNavigationManager2;
        this.bus = bus2;
    }

    public void confirmReroute() {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.maps.here.HereTrafficRerouteListener.Anon3(), 20);
    }

    public void dismissReroute() {
        clearProposedRoute();
    }

    /* access modifiers changed from: private */
    public void clearProposedRoute() {
        this.logger.v("clearProposedRoute");
        if (this.currentProposedRoute != null) {
            this.logger.v("clearProposedRoute:cleared");
            com.navdy.hud.app.maps.notification.TrafficNotificationManager.getInstance().removeFasterRouteNotifiation();
            this.currentProposedRoute = null;
            this.timeCalculated = 0;
            this.placeCalculated = null;
            this.notifFromHere = false;
            this.viaString = null;
            this.fasterBy = 0;
        }
    }

    public void onTrafficRerouteBegin(com.here.android.mpa.guidance.TrafficNotification trafficNotification) {
        this.logger.v(this.tag + " reroute-begin:");
        if (this.verbose) {
            com.navdy.hud.app.maps.here.HereMapUtil.printTrafficNotification(trafficNotification, this.tag);
        }
    }

    public void onTrafficRerouted(com.here.android.mpa.routing.Route fasterRoute) {
        long now = android.os.SystemClock.elapsedRealtime();
        long diff = now - this.lastTrafficNotifFromHere;
        if (diff < NOTIF_FROM_HERE_THRESHOLD) {
            this.logger.i(this.tag + "here traffic notif came in " + diff);
        } else if (!isMoreRouteNotificationComplete()) {
            this.logger.v("onTrafficRerouted: more route notif in progress");
        } else {
            this.lastTrafficNotifFromHere = now;
            handleFasterRoute(fasterRoute, true);
        }
    }

    public void onTrafficRerouteFailed(com.here.android.mpa.guidance.TrafficNotification trafficNotification) {
        this.logger.e(this.tag + " reroute-failed");
        com.navdy.hud.app.maps.here.HereMapUtil.printTrafficNotification(trafficNotification, this.tag);
    }

    public void onTrafficRerouteState(com.here.android.mpa.guidance.NavigationManager.TrafficRerouteListener.TrafficEnabledRoutingState trafficEnabledRoutingState) {
    }

    public void start() {
        if (!this.trafficRerouteListenerRunning) {
            this.trafficRerouteListenerRunning = true;
            com.navdy.service.library.task.TaskManager.getInstance().execute(this.startRunnable, 15);
            this.logger.v("added traffic reroute listener");
        }
    }

    public void stop() {
        if (this.trafficRerouteListenerRunning) {
            this.trafficRerouteListenerRunning = false;
            com.navdy.service.library.task.TaskManager.getInstance().execute(this.stopRunnable, 15);
            this.logger.v("removed traffic reroute listener");
        }
    }

    public boolean shouldCalculateFasterRoute() {
        long elapsed = android.os.SystemClock.elapsedRealtime() - this.timeCalculated;
        this.logger.v("elapsed=" + elapsed);
        if (elapsed < ((long) (this.hereNavigationManager.getRerouteInterval() - 15000))) {
            return false;
        }
        if (isMoreRouteNotificationComplete()) {
            return true;
        }
        this.logger.v("shouldCalculateFasterRoute: more route notif in progress");
        return false;
    }

    public boolean isMoreRouteNotificationComplete() {
        if (!this.hereNavigationManager.isShownFirstManeuver()) {
            this.logger.v("isMoreRouteNotificationComplete:not shown first maneuver");
            return false;
        } else if (com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().isCurrentNotificationId(com.navdy.hud.app.framework.notifications.NotificationId.ROUTE_CALC_NOTIFICATION_ID)) {
            this.logger.v("isMoreRouteNotificationComplete:route calc notif on");
            return false;
        } else {
            com.navdy.hud.app.screen.BaseScreen screen = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getUiStateManager().getCurrentScreen();
            if (screen != null && screen.getScreen() == com.navdy.service.library.events.ui.Screen.SCREEN_DESTINATION_PICKER) {
                android.os.Bundle bundle = screen.getArguments();
                if (bundle != null && bundle.getBoolean(com.navdy.hud.app.maps.notification.RouteCalculationNotification.ROUTE_PICKER)) {
                    this.logger.v("isMoreRouteNotificationComplete:user looking at route picker");
                    return false;
                }
            }
            if (com.navdy.hud.app.maps.here.HereMapsManager.getInstance().getRouteCalcEventHandler().hasMoreRoutes()) {
                this.logger.v("isMoreRouteNotificationComplete:user has not seen more routes");
                return false;
            }
            long diff = android.os.SystemClock.elapsedRealtime() - this.hereNavigationManager.getFirstManeuverShowntime();
            if (diff >= ((long) WAIT_INTERVAL_AFTER_FIRST_MANEUVER_SHOWN)) {
                return true;
            }
            this.logger.v("isMoreRouteNotificationComplete:first maneuver just shown:" + diff);
            return false;
        }
    }

    public com.here.android.mpa.routing.Route getCurrentProposedRoute() {
        return this.currentProposedRoute;
    }

    public long getCurrentProposedRouteTime() {
        return this.timeCalculated;
    }

    public java.lang.String getViaString() {
        return this.viaString;
    }

    public long getFasterBy() {
        return this.fasterBy;
    }

    public void handleFasterRoute(com.here.android.mpa.routing.Route fasterRoute, boolean notifFromHereTraffic) {
        if (fasterRoute == null) {
            this.logger.e("null faster route:" + notifFromHereTraffic);
        } else {
            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.maps.here.HereTrafficRerouteListener.Anon4(notifFromHereTraffic, fasterRoute), 2);
        }
    }

    /* access modifiers changed from: private */
    public boolean isFasterRouteDivergePointCloseBy(java.util.List<com.here.android.mpa.routing.Maneuver> currentManeuvers, java.util.List<com.here.android.mpa.routing.Maneuver> oldRouteDifferentManeuver, java.util.List<com.here.android.mpa.routing.Maneuver> newRouteDifferentManeuver) {
        com.here.android.mpa.routing.Maneuver fasterChange;
        com.here.android.mpa.routing.Maneuver nextSlowChange;
        com.here.android.mpa.routing.Maneuver currentManeuver = this.navController.getNextManeuver();
        com.here.android.mpa.routing.Maneuver slowChange = (com.here.android.mpa.routing.Maneuver) oldRouteDifferentManeuver.get(0);
        if (newRouteDifferentManeuver.size() > 1) {
            fasterChange = (com.here.android.mpa.routing.Maneuver) newRouteDifferentManeuver.get(1);
        } else {
            fasterChange = (com.here.android.mpa.routing.Maneuver) newRouteDifferentManeuver.get(0);
        }
        if (oldRouteDifferentManeuver.size() > 1) {
            nextSlowChange = (com.here.android.mpa.routing.Maneuver) oldRouteDifferentManeuver.get(1);
        } else {
            nextSlowChange = slowChange;
        }
        this.logger.v(this.tag + " diverge point road faster [" + com.navdy.hud.app.maps.here.HereMapUtil.getRoadName(fasterChange) + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA + com.navdy.hud.app.maps.here.HereMapUtil.getNextRoadName(fasterChange) + "]  slower [" + com.navdy.hud.app.maps.here.HereMapUtil.getRoadName(nextSlowChange) + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA + com.navdy.hud.app.maps.here.HereMapUtil.getNextRoadName(nextSlowChange) + "]");
        if (!com.navdy.hud.app.maps.here.HereMapUtil.areManeuverEqual(currentManeuver, slowChange)) {
            int offset = -1;
            if (currentManeuver == null) {
                this.logger.i(this.tag + "no current maneuver");
            } else {
                int len = currentManeuvers.size();
                int i = 0;
                while (true) {
                    if (i >= len) {
                        break;
                    } else if (com.navdy.hud.app.maps.here.HereMapUtil.areManeuverEqual((com.here.android.mpa.routing.Maneuver) currentManeuvers.get(i), currentManeuver)) {
                        offset = i;
                        break;
                    } else {
                        i++;
                    }
                }
            }
            if (offset != -1) {
                this.logger.v(this.tag + "reroute offset=" + offset);
                long timeToManeuver = 0;
                long distToManeuver = 0;
                com.navdy.hud.app.maps.here.HereMapUtil.LongContainer longContainer = new com.navdy.hud.app.maps.here.HereMapUtil.LongContainer();
                int len2 = currentManeuvers.size();
                boolean slowManeuverFound = false;
                int i2 = offset;
                while (true) {
                    if (i2 >= len2) {
                        break;
                    }
                    com.here.android.mpa.routing.Maneuver m = (com.here.android.mpa.routing.Maneuver) currentManeuvers.get(i2);
                    if (com.navdy.hud.app.maps.here.HereMapUtil.areManeuverEqual(m, slowChange)) {
                        this.logger.v(this.tag + "reroute diverge:" + i2 + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA + len2);
                        slowManeuverFound = true;
                        break;
                    }
                    timeToManeuver += com.navdy.hud.app.maps.here.HereMapUtil.getManeuverDriveTime(m, longContainer);
                    distToManeuver += longContainer.val;
                    i2++;
                }
                if (!slowManeuverFound) {
                    this.logger.i(this.tag + "reroute threshold slow maneuver not found");
                    return true;
                } else if (timeToManeuver >= REROUTE_POINT_THRESHOLD) {
                    this.logger.i(this.tag + "reroute threshold [" + timeToManeuver + "] is > " + REROUTE_POINT_THRESHOLD + " dist=" + distToManeuver);
                    dismissReroute();
                    return false;
                } else {
                    this.logger.v(this.tag + "reroute point threshold [" + timeToManeuver + "] distance[" + distToManeuver + "]");
                }
            } else {
                this.logger.i(this.tag + "offset not found");
            }
        } else {
            this.logger.i(this.tag + "first change and current are same");
        }
        return true;
    }

    /* access modifiers changed from: 0000 */
    public void setFasterRoute(com.here.android.mpa.routing.Route fasterRoute, boolean notifFromHereTraffic, java.lang.String via, long diff, java.lang.String additionalVia, long etaUtc, long distanceDiff, long newEta) {
        this.hereNavigationManager.incrCurrentTrafficRerouteCount();
        this.hereNavigationManager.setLastTrafficRerouteTime(android.os.SystemClock.elapsedRealtime());
        this.currentProposedRoute = fasterRoute;
        this.timeCalculated = android.os.SystemClock.elapsedRealtime();
        this.notifFromHere = notifFromHereTraffic;
        this.viaString = via;
        this.fasterBy = diff;
        this.placeCalculated = com.navdy.hud.app.maps.here.HereMapsManager.getInstance().getLocationFixManager().getLastGeoCoordinate();
        this.bus.post(new com.navdy.hud.app.maps.MapEvents.TrafficRerouteEvent(additionalVia, additionalVia, diff, etaUtc, distanceDiff, newEta));
        this.logger.v("faster reroute event sent");
    }

    /* access modifiers changed from: 0000 */
    public void fasterRouteNotFound(boolean notifFromHereTraffic, long diff, java.lang.String via, java.lang.String currentVia) {
        java.lang.String fromTag = notifFromHereTraffic ? "[Here Traffic]" : "[Navdy Calc]";
        this.logger.v(this.tag + fromTag + " new route threshold not met:" + getRerouteMinDuration());
        if (diff > 0 && com.navdy.hud.app.framework.voice.TTSUtils.isDebugTTSEnabled()) {
            com.navdy.hud.app.framework.voice.TTSUtils.debugShowFasterRouteToast(fromTag + " Faster route below threshold", "via[" + via + "] faster by[" + diff + "] seconds current via[" + currentVia + "]");
        }
    }

    public void setNavdyTrafficRerouteManager(com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager navdyTrafficRerouteManager2) {
        this.navdyTrafficRerouteManager = navdyTrafficRerouteManager2;
    }

    public int getRerouteMinDuration() {
        if (this.hereNavigationManager.getCurrentTrafficRerouteCount() < 1) {
            this.logger.v("getRerouteMinDuration:90");
            return MIN_INTERVAL_REROUTE_FIRST;
        }
        int secondsTillLastDuration = ((int) (android.os.SystemClock.elapsedRealtime() - this.hereNavigationManager.getLastTrafficRerouteTime())) / 1000;
        int ret = (int) (90.0d + (1.5d * ((double) java.lang.Math.max(360 - secondsTillLastDuration, 0))));
        this.logger.v("getRerouteMinDuration:" + ret + " secslast:" + secondsTillLastDuration);
        return ret;
    }
}
