package com.navdy.hud.app.maps.here;

public class HereRouteCache extends android.util.LruCache<java.lang.String, com.navdy.hud.app.maps.here.HereRouteCache.RouteInfo> {
    public static final int MAX_CACHE_ROUTE_COUNT = 20;
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.maps.here.HereRouteCache.class);
    private static final com.navdy.hud.app.maps.here.HereRouteCache sSingleton = new com.navdy.hud.app.maps.here.HereRouteCache();

    public static class RouteInfo {
        public com.here.android.mpa.routing.Route route;
        public com.navdy.service.library.events.navigation.NavigationRouteRequest routeRequest;
        public com.navdy.service.library.events.navigation.NavigationRouteResult routeResult;
        public com.here.android.mpa.common.GeoCoordinate routeStartPoint;
        public boolean traffic;

        public RouteInfo(com.here.android.mpa.routing.Route route2, com.navdy.service.library.events.navigation.NavigationRouteRequest routeRequest2, com.navdy.service.library.events.navigation.NavigationRouteResult routeResult2, boolean traffic2, com.here.android.mpa.common.GeoCoordinate routeStartPoint2) {
            this.route = route2;
            this.routeRequest = routeRequest2;
            this.routeResult = routeResult2;
            this.traffic = traffic2;
            this.routeStartPoint = routeStartPoint2;
        }
    }

    public static com.navdy.hud.app.maps.here.HereRouteCache getInstance() {
        return sSingleton;
    }

    private HereRouteCache() {
        super(20);
    }

    /* access modifiers changed from: protected */
    public int sizeOf(java.lang.String key, com.here.android.mpa.routing.Route value) {
        return 1;
    }

    public void addRoute(java.lang.String id, com.navdy.hud.app.maps.here.HereRouteCache.RouteInfo routeInfo) {
        super.put(id, routeInfo);
    }

    public com.navdy.hud.app.maps.here.HereRouteCache.RouteInfo removeRoute(java.lang.String id) {
        return (com.navdy.hud.app.maps.here.HereRouteCache.RouteInfo) super.remove(id);
    }

    public com.navdy.hud.app.maps.here.HereRouteCache.RouteInfo getRoute(java.lang.String id) {
        if (id != null) {
            return (com.navdy.hud.app.maps.here.HereRouteCache.RouteInfo) super.get(id);
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public void entryRemoved(boolean evicted, java.lang.String key, com.here.android.mpa.routing.Route oldValue, com.here.android.mpa.routing.Route newValue) {
        if (evicted && oldValue != null) {
            sLogger.v("route removed from cache:" + key);
        }
    }
}
