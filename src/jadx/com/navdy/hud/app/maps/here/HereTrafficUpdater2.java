package com.navdy.hud.app.maps.here;

public class HereTrafficUpdater2 {
    /* access modifiers changed from: private */
    public static final com.navdy.hud.app.maps.MapEvents.DisplayTrafficIncident FAILED_EVENT = new com.navdy.hud.app.maps.MapEvents.DisplayTrafficIncident(com.navdy.hud.app.maps.MapEvents.DisplayTrafficIncident.Type.FAILED, com.navdy.hud.app.maps.MapEvents.DisplayTrafficIncident.Category.UNDEFINED);
    private static final com.navdy.hud.app.maps.MapEvents.DisplayTrafficIncident INACTIVE_EVENT = new com.navdy.hud.app.maps.MapEvents.DisplayTrafficIncident(com.navdy.hud.app.maps.MapEvents.DisplayTrafficIncident.Type.INACTIVE, com.navdy.hud.app.maps.MapEvents.DisplayTrafficIncident.Category.UNDEFINED);
    private static final com.navdy.hud.app.maps.MapEvents.DisplayTrafficIncident NORMAL_EVENT = new com.navdy.hud.app.maps.MapEvents.DisplayTrafficIncident(com.navdy.hud.app.maps.MapEvents.DisplayTrafficIncident.Type.NORMAL, com.navdy.hud.app.maps.MapEvents.DisplayTrafficIncident.Category.UNDEFINED);
    /* access modifiers changed from: private */
    public static final int STALE_REQUEST_TIME = ((int) java.util.concurrent.TimeUnit.MINUTES.toMillis(2));
    private static final java.lang.String TAG = "HereTrafficUpdater2";
    /* access modifiers changed from: private */
    public static final int TRAFFIC_DATA_CHECK_TIME_INTERVAL = ((int) java.util.concurrent.TimeUnit.SECONDS.toMillis(30));
    private static final int TRAFFIC_DATA_REFRESH_INTERVAL = ((int) java.util.concurrent.TimeUnit.MINUTES.toMillis(5));
    private static final int TRAFFIC_DATA_REFRESH_INTERVAL_LIMITED_BANDWIDTH = ((int) java.util.concurrent.TimeUnit.MINUTES.toMillis(15));
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(TAG);
    /* access modifiers changed from: private */
    public com.squareup.otto.Bus bus;
    private java.lang.Runnable checkRunnable = new com.navdy.hud.app.maps.here.HereTrafficUpdater2.Anon4();
    /* access modifiers changed from: private */
    public com.here.android.mpa.guidance.TrafficUpdater.RequestInfo currentRequestInfo;
    /* access modifiers changed from: private */
    public android.os.Handler handler = new android.os.Handler(android.os.Looper.getMainLooper());
    /* access modifiers changed from: private */
    public long lastRequestTime;
    /* access modifiers changed from: private */
    public com.here.android.mpa.guidance.TrafficUpdater.GetEventsListener onGetEventsListener = new com.navdy.hud.app.maps.here.HereTrafficUpdater2.Anon2();
    private com.here.android.mpa.guidance.TrafficUpdater.Listener onRequestListener = new com.navdy.hud.app.maps.here.HereTrafficUpdater2.Anon1();
    private java.lang.Runnable refreshRunnable = new com.navdy.hud.app.maps.here.HereTrafficUpdater2.Anon3();
    /* access modifiers changed from: private */
    public com.here.android.mpa.routing.Route route;
    /* access modifiers changed from: private */
    public com.here.android.mpa.guidance.TrafficUpdater trafficUpdater;

    class Anon1 implements com.here.android.mpa.guidance.TrafficUpdater.Listener {

        /* renamed from: com.navdy.hud.app.maps.here.HereTrafficUpdater2$Anon1$Anon1 reason: collision with other inner class name */
        class C0024Anon1 implements java.lang.Runnable {
            C0024Anon1() {
            }

            public void run() {
                long l1 = android.os.SystemClock.elapsedRealtime();
                try {
                    com.navdy.hud.app.maps.here.HereNavigationManager hereNavigationManager = com.navdy.hud.app.maps.here.HereNavigationManager.getInstance();
                    if (!hereNavigationManager.isNavigationModeOn()) {
                        com.navdy.hud.app.maps.here.HereTrafficUpdater2.sLogger.v("nav is off");
                        return;
                    }
                    com.here.android.mpa.routing.Maneuver next = hereNavigationManager.getNextManeuver();
                    com.here.android.mpa.routing.Maneuver prev = hereNavigationManager.getPrevManeuver();
                    java.util.List<com.here.android.mpa.routing.RouteElement> aheadRouteElements = com.navdy.hud.app.maps.here.HereMapUtil.getAheadRouteElements(com.navdy.hud.app.maps.here.HereTrafficUpdater2.this.route, com.navdy.hud.app.maps.here.HereMapUtil.getCurrentRoadElement(), prev, next);
                    if (aheadRouteElements == null || aheadRouteElements.size() == 0) {
                        com.navdy.hud.app.maps.here.HereTrafficUpdater2.sLogger.i("onRequestListener: no ahead road elements");
                        com.navdy.hud.app.maps.here.HereTrafficUpdater2.this.currentRequestInfo = null;
                        com.navdy.hud.app.maps.here.HereTrafficUpdater2.this.lastRequestTime = 0;
                        com.navdy.hud.app.maps.here.HereTrafficUpdater2.this.reset(com.navdy.hud.app.maps.here.HereTrafficUpdater2.this.getRefereshInterval());
                        com.navdy.hud.app.maps.here.HereTrafficUpdater2.sLogger.v("ahead route build time [" + (android.os.SystemClock.elapsedRealtime() - l1) + "]");
                    }
                    com.navdy.hud.app.maps.here.HereTrafficUpdater2.sLogger.i("onRequestListener: calling getevent size:" + aheadRouteElements.size());
                    com.navdy.hud.app.maps.here.HereTrafficUpdater2.this.trafficUpdater.getEvents(aheadRouteElements, com.navdy.hud.app.maps.here.HereTrafficUpdater2.this.onGetEventsListener);
                    com.navdy.hud.app.maps.here.HereTrafficUpdater2.sLogger.v("ahead route build time [" + (android.os.SystemClock.elapsedRealtime() - l1) + "]");
                } catch (Throwable t) {
                    com.navdy.hud.app.maps.here.HereTrafficUpdater2.this.currentRequestInfo = null;
                    com.navdy.hud.app.maps.here.HereTrafficUpdater2.this.lastRequestTime = 0;
                    com.navdy.hud.app.maps.here.HereTrafficUpdater2.this.reset(com.navdy.hud.app.maps.here.HereTrafficUpdater2.this.getRefereshInterval() / 2);
                    com.navdy.hud.app.maps.here.HereTrafficUpdater2.sLogger.e("onRequestListener", t);
                }
            }
        }

        Anon1() {
        }

        public void onStatusChanged(com.here.android.mpa.guidance.TrafficUpdater.RequestState requestState) {
            boolean reset = false;
            int resetTime = com.navdy.hud.app.maps.here.HereTrafficUpdater2.this.getRefereshInterval();
            try {
                com.navdy.hud.app.maps.here.HereTrafficUpdater2.sLogger.i("onRequestListener::" + requestState);
                if (requestState != com.here.android.mpa.guidance.TrafficUpdater.RequestState.DONE) {
                    com.navdy.hud.app.maps.here.HereTrafficUpdater2.sLogger.i("onRequestListener:: traffic data download failed");
                    resetTime = com.navdy.hud.app.maps.here.HereTrafficUpdater2.this.getRefereshInterval() / 2;
                    com.navdy.hud.app.maps.here.HereTrafficUpdater2.this.bus.post(com.navdy.hud.app.maps.here.HereTrafficUpdater2.FAILED_EVENT);
                    reset = true;
                } else if (com.navdy.hud.app.maps.here.HereTrafficUpdater2.this.route == null) {
                    com.navdy.hud.app.maps.here.HereTrafficUpdater2.sLogger.i("onRequestListener:traffic downloaded for open map");
                    if (!com.navdy.hud.app.maps.MapSettings.isTrafficDashWidgetsEnabled()) {
                        com.navdy.hud.app.maps.here.HereTrafficUpdater2.this.currentRequestInfo = null;
                        com.navdy.hud.app.maps.here.HereTrafficUpdater2.this.lastRequestTime = 0;
                    }
                    if (1 != 0) {
                        com.navdy.hud.app.maps.here.HereTrafficUpdater2.this.reset(resetTime);
                        return;
                    }
                    return;
                } else {
                    com.navdy.hud.app.maps.here.HereTrafficUpdater2.sLogger.i("onRequestListener:traffic downloaded for route");
                    if (!com.navdy.hud.app.maps.MapSettings.isTrafficDashWidgetsEnabled()) {
                        if (!com.navdy.hud.app.maps.MapSettings.isTrafficDashWidgetsEnabled()) {
                            com.navdy.hud.app.maps.here.HereTrafficUpdater2.this.currentRequestInfo = null;
                            com.navdy.hud.app.maps.here.HereTrafficUpdater2.this.lastRequestTime = 0;
                        }
                        if (1 != 0) {
                            com.navdy.hud.app.maps.here.HereTrafficUpdater2.this.reset(resetTime);
                            return;
                        }
                        return;
                    }
                    com.navdy.hud.app.maps.here.HereTrafficUpdater2.sLogger.i("onRequestListener:call getEvents");
                    com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.maps.here.HereTrafficUpdater2.Anon1.C0024Anon1(), 2);
                }
                if (!com.navdy.hud.app.maps.MapSettings.isTrafficDashWidgetsEnabled()) {
                    com.navdy.hud.app.maps.here.HereTrafficUpdater2.this.currentRequestInfo = null;
                    com.navdy.hud.app.maps.here.HereTrafficUpdater2.this.lastRequestTime = 0;
                }
                if (reset) {
                    com.navdy.hud.app.maps.here.HereTrafficUpdater2.this.reset(resetTime);
                }
            } catch (Throwable th) {
                if (!com.navdy.hud.app.maps.MapSettings.isTrafficDashWidgetsEnabled()) {
                    com.navdy.hud.app.maps.here.HereTrafficUpdater2.this.currentRequestInfo = null;
                    com.navdy.hud.app.maps.here.HereTrafficUpdater2.this.lastRequestTime = 0;
                }
                if (0 != 0) {
                    com.navdy.hud.app.maps.here.HereTrafficUpdater2.this.reset(resetTime);
                }
                throw th;
            }
        }
    }

    class Anon2 implements com.here.android.mpa.guidance.TrafficUpdater.GetEventsListener {

        class Anon1 implements java.lang.Runnable {
            final /* synthetic */ java.util.List val$list;
            final /* synthetic */ com.here.android.mpa.routing.Route val$r;

            Anon1(com.here.android.mpa.routing.Route route, java.util.List list) {
                this.val$r = route;
                this.val$list = list;
            }

            public void run() {
                if (com.navdy.hud.app.maps.here.HereTrafficUpdater2.this.route == null) {
                    com.navdy.hud.app.maps.here.HereTrafficUpdater2.sLogger.w("no current route, cannot process traffic events");
                    return;
                }
                com.navdy.hud.app.maps.here.HereTrafficUpdater2.this.processEvents(this.val$r, this.val$list, com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().getNavController().getDestinationDistance());
            }
        }

        Anon2() {
        }

        public void onComplete(java.util.List<com.here.android.mpa.mapping.TrafficEvent> list, com.here.android.mpa.guidance.TrafficUpdater.Error error) {
            boolean reset = false;
            try {
                com.navdy.hud.app.maps.here.HereTrafficUpdater2.sLogger.i("onGetEventsListener::" + error);
                com.here.android.mpa.routing.Route r = com.navdy.hud.app.maps.here.HereTrafficUpdater2.this.route;
                if (r == null) {
                    com.navdy.hud.app.maps.here.HereTrafficUpdater2.sLogger.i("onGetEventsListener: route is null");
                    if (!reset) {
                        return;
                    }
                    return;
                }
                reset = true;
                com.navdy.hud.app.maps.here.HereTrafficUpdater2.this.currentRequestInfo = null;
                com.navdy.hud.app.maps.here.HereTrafficUpdater2.this.lastRequestTime = 0;
                if (error != com.here.android.mpa.guidance.TrafficUpdater.Error.NONE) {
                    com.navdy.hud.app.maps.here.HereTrafficUpdater2.sLogger.i("onGetEventsListener: error " + error);
                    com.navdy.hud.app.maps.here.HereTrafficUpdater2.this.bus.post(com.navdy.hud.app.maps.here.HereTrafficUpdater2.FAILED_EVENT);
                } else if (list == null || list.size() == 0) {
                    com.navdy.hud.app.maps.here.HereTrafficUpdater2.sLogger.i("onGetEventsListener: no events");
                    com.navdy.hud.app.maps.here.HereTrafficUpdater2.this.bus.post(com.navdy.hud.app.maps.here.HereTrafficUpdater2.FAILED_EVENT);
                } else {
                    com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.maps.here.HereTrafficUpdater2.Anon2.Anon1(r, list), 2);
                }
                if (reset) {
                    com.navdy.hud.app.maps.here.HereTrafficUpdater2.this.reset(com.navdy.hud.app.maps.here.HereTrafficUpdater2.this.getRefereshInterval());
                }
            } finally {
                if (reset) {
                    com.navdy.hud.app.maps.here.HereTrafficUpdater2.this.reset(com.navdy.hud.app.maps.here.HereTrafficUpdater2.this.getRefereshInterval());
                }
            }
        }
    }

    class Anon3 implements java.lang.Runnable {
        Anon3() {
        }

        public void run() {
            com.navdy.hud.app.maps.here.HereTrafficUpdater2.this.startRequest();
        }
    }

    class Anon4 implements java.lang.Runnable {
        Anon4() {
        }

        /* JADX INFO: finally extract failed */
        public void run() {
            boolean check = false;
            try {
                com.navdy.hud.app.maps.here.HereTrafficUpdater2.sLogger.v("checkRunnable");
                if (com.navdy.hud.app.maps.here.HereTrafficUpdater2.this.currentRequestInfo != null && com.navdy.hud.app.maps.here.HereTrafficUpdater2.this.lastRequestTime > 0) {
                    long diff = android.os.SystemClock.elapsedRealtime() - com.navdy.hud.app.maps.here.HereTrafficUpdater2.this.lastRequestTime;
                    com.navdy.hud.app.maps.here.HereTrafficUpdater2.sLogger.v("checkRunnable:" + diff);
                    if (diff >= ((long) com.navdy.hud.app.maps.here.HereTrafficUpdater2.STALE_REQUEST_TIME)) {
                        com.navdy.hud.app.maps.here.HereTrafficUpdater2.sLogger.v("checkRunnable: request stale");
                        com.navdy.hud.app.maps.here.HereTrafficUpdater2.this.reset(5000);
                    } else {
                        check = true;
                        com.navdy.hud.app.maps.here.HereTrafficUpdater2.sLogger.v("checkRunnable: request not stale");
                    }
                }
                if (check) {
                    com.navdy.hud.app.maps.here.HereTrafficUpdater2.this.handler.postDelayed(this, (long) com.navdy.hud.app.maps.here.HereTrafficUpdater2.TRAFFIC_DATA_CHECK_TIME_INTERVAL);
                }
            } catch (Throwable th) {
                if (check) {
                    com.navdy.hud.app.maps.here.HereTrafficUpdater2.this.handler.postDelayed(this, (long) com.navdy.hud.app.maps.here.HereTrafficUpdater2.TRAFFIC_DATA_CHECK_TIME_INTERVAL);
                }
                throw th;
            }
        }
    }

    static /* synthetic */ class Anon5 {
        static final /* synthetic */ int[] $SwitchMap$com$here$android$mpa$guidance$TrafficUpdater$Error = new int[com.here.android.mpa.guidance.TrafficUpdater.Error.values().length];
        static final /* synthetic */ int[] $SwitchMap$com$here$android$mpa$mapping$TrafficEvent$Severity = new int[com.here.android.mpa.mapping.TrafficEvent.Severity.values().length];

        static {
            try {
                $SwitchMap$com$here$android$mpa$mapping$TrafficEvent$Severity[com.here.android.mpa.mapping.TrafficEvent.Severity.BLOCKING.ordinal()] = 1;
            } catch (java.lang.NoSuchFieldError e) {
            }
            try {
                $SwitchMap$com$here$android$mpa$mapping$TrafficEvent$Severity[com.here.android.mpa.mapping.TrafficEvent.Severity.VERY_HIGH.ordinal()] = 2;
            } catch (java.lang.NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$com$here$android$mpa$mapping$TrafficEvent$Severity[com.here.android.mpa.mapping.TrafficEvent.Severity.HIGH.ordinal()] = 3;
            } catch (java.lang.NoSuchFieldError e3) {
            }
            try {
                $SwitchMap$com$here$android$mpa$guidance$TrafficUpdater$Error[com.here.android.mpa.guidance.TrafficUpdater.Error.NONE.ordinal()] = 1;
            } catch (java.lang.NoSuchFieldError e4) {
            }
        }
    }

    private static class HereTrafficEvent {
        public long distance;
        public com.here.android.mpa.mapping.TrafficEvent trafficEvent;

        public HereTrafficEvent(com.here.android.mpa.mapping.TrafficEvent trafficEvent2, long distance2) {
            this.trafficEvent = trafficEvent2;
            this.distance = distance2;
        }
    }

    public HereTrafficUpdater2(com.squareup.otto.Bus bus2) {
        sLogger.i("ctor");
        this.bus = bus2;
        this.trafficUpdater = com.here.android.mpa.guidance.TrafficUpdater.getInstance();
    }

    public void start() {
        sLogger.i("start");
        startRequest();
    }

    public void setRoute(com.here.android.mpa.routing.Route route2) {
        sLogger.i("setRoute:" + route2 + " id=" + java.lang.System.identityHashCode(route2));
        cancelRequest();
        this.route = route2;
        startRequest();
    }

    /* access modifiers changed from: private */
    public void startRequest() {
        try {
            if (!com.navdy.service.library.util.SystemUtils.isConnectedToNetwork(com.navdy.hud.app.HudApplication.getAppContext())) {
                sLogger.i("startRequest: not connected to n/w, retry");
                reset(getRefereshInterval() / 2);
                this.bus.post(FAILED_EVENT);
                this.lastRequestTime = 0;
            } else if (com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().hasArrived()) {
                sLogger.i("startRequest: arrived, not making request");
                this.bus.post(INACTIVE_EVENT);
                this.lastRequestTime = 0;
            } else {
                if (this.route != null) {
                    sLogger.i("startRequest: getting route traffic");
                    this.currentRequestInfo = this.trafficUpdater.request(this.route, this.onRequestListener);
                } else {
                    com.here.android.mpa.common.GeoCoordinate coordinate = com.navdy.hud.app.maps.here.HereMapsManager.getInstance().getLocationFixManager().getLastGeoCoordinate();
                    if (coordinate == null) {
                        sLogger.i("startRequest: getting area traffic, no current coordinate");
                        this.lastRequestTime = 0;
                        this.bus.post(FAILED_EVENT);
                        reset(getRefereshInterval() / 2);
                        return;
                    }
                    sLogger.i("startRequest: getting area traffic");
                    this.currentRequestInfo = this.trafficUpdater.request(coordinate, this.onRequestListener);
                }
                com.here.android.mpa.guidance.TrafficUpdater.Error error = this.currentRequestInfo.getError();
                sLogger.i("startRequest: returned:" + error);
                switch (com.navdy.hud.app.maps.here.HereTrafficUpdater2.Anon5.$SwitchMap$com$here$android$mpa$guidance$TrafficUpdater$Error[error.ordinal()]) {
                    case 1:
                        this.handler.removeCallbacks(this.checkRunnable);
                        this.handler.postDelayed(this.checkRunnable, (long) TRAFFIC_DATA_CHECK_TIME_INTERVAL);
                        this.lastRequestTime = android.os.SystemClock.elapsedRealtime();
                        return;
                    default:
                        this.lastRequestTime = 0;
                        this.bus.post(FAILED_EVENT);
                        reset(getRefereshInterval() / 2);
                        return;
                }
            }
        } catch (Throwable t) {
            sLogger.e(t);
            reset(getRefereshInterval() / 2);
        }
    }

    private void cancelRequest() {
        try {
            if (this.currentRequestInfo != null) {
                if (this.currentRequestInfo.getError() == com.here.android.mpa.guidance.TrafficUpdater.Error.NONE) {
                    this.trafficUpdater.cancelRequest(this.currentRequestInfo.getRequestId());
                    this.lastRequestTime = 0;
                    sLogger.v("cancelRequest: called cancel");
                }
                this.currentRequestInfo = null;
            }
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }

    /* access modifiers changed from: private */
    public void reset(int delay) {
        sLogger.v("reset");
        cancelRequest();
        this.handler.removeCallbacks(this.refreshRunnable);
        this.handler.postDelayed(this.refreshRunnable, (long) delay);
        this.handler.removeCallbacks(this.checkRunnable);
    }

    /* access modifiers changed from: private */
    public void processEvents(com.here.android.mpa.routing.Route route2, java.util.List<com.here.android.mpa.mapping.TrafficEvent> events, long distanceRemaining) {
        long l1 = android.os.SystemClock.elapsedRealtime();
        sLogger.v("processEvents size:" + events.size());
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        com.here.android.mpa.common.GeoCoordinate destination = route2.getDestination();
        int blocking = 0;
        int veryHighCount = 0;
        int highCount = 0;
        java.util.List<com.navdy.hud.app.maps.here.HereTrafficUpdater2.HereTrafficEvent> blockingList = new java.util.ArrayList<>();
        java.util.ArrayList arrayList = new java.util.ArrayList();
        java.util.ArrayList arrayList2 = new java.util.ArrayList();
        com.here.android.mpa.common.RoadElement currentRoadElement = com.navdy.hud.app.maps.here.HereMapUtil.getCurrentRoadElement();
        if (currentRoadElement == null) {
            sLogger.w("no current road element");
            return;
        }
        com.here.android.mpa.common.GeoCoordinate coordinate = com.navdy.hud.app.maps.here.HereMapsManager.getInstance().getLocationFixManager().getLastGeoCoordinate();
        if (coordinate == null) {
            sLogger.w("no current coordinate");
            return;
        }
        java.util.List<com.here.android.mpa.routing.Maneuver> maneuvers = route2.getManeuvers();
        com.here.android.mpa.common.RoadElement lastRoadElement = null;
        com.here.android.mpa.routing.Maneuver lastManeuver = com.navdy.hud.app.maps.here.HereMapUtil.getLastManeuver(maneuvers);
        if (lastManeuver != null) {
            java.util.List<com.here.android.mpa.common.RoadElement> lastManeuverRoadElements = lastManeuver.getRoadElements();
            if (lastManeuverRoadElements != null && lastManeuverRoadElements.size() > 0) {
                lastRoadElement = (com.here.android.mpa.common.RoadElement) lastManeuverRoadElements.get(lastManeuverRoadElements.size() - 1);
            }
        }
        for (com.here.android.mpa.mapping.TrafficEvent event : events) {
            if (event.isOnRoute(route2)) {
                java.util.List<com.here.android.mpa.common.RoadElement> affectedRoadElements = event.getAffectedRoadElements();
                if (affectedRoadElements != null && affectedRoadElements.size() != 0) {
                    boolean calcDistance = false;
                    com.here.android.mpa.mapping.TrafficEvent.Severity severity = event.getSeverity();
                    switch (com.navdy.hud.app.maps.here.HereTrafficUpdater2.Anon5.$SwitchMap$com$here$android$mpa$mapping$TrafficEvent$Severity[severity.ordinal()]) {
                        case 1:
                        case 2:
                        case 3:
                            calcDistance = true;
                            break;
                    }
                    long distanceToEvent = -1;
                    if (calcDistance) {
                        distanceToEvent = com.navdy.hud.app.maps.here.HereMapUtil.getDistanceToRoadElement(currentRoadElement, (com.here.android.mpa.common.RoadElement) affectedRoadElements.get(0), maneuvers);
                        long eventToDestinationDistance = com.navdy.hud.app.maps.here.HereMapUtil.getDistanceToRoadElement((com.here.android.mpa.common.RoadElement) affectedRoadElements.get(0), lastRoadElement, maneuvers);
                        if (eventToDestinationDistance == -1) {
                            sLogger.v("could not get distance");
                        } else {
                            long distanceToEventHaversine = (long) event.getDistanceTo(coordinate);
                            long total = distanceToEvent + eventToDestinationDistance;
                            sLogger.v("[EVENT] distanceToEvent:" + distanceToEvent + " eventToDestinationDistance:" + eventToDestinationDistance + " distanceToDest:" + distanceRemaining + " total:" + total + " distanceToEventHaversine:" + distanceToEventHaversine);
                        }
                    } else {
                        sLogger.v("[EVENT] n/a severity = " + severity);
                    }
                    switch (com.navdy.hud.app.maps.here.HereTrafficUpdater2.Anon5.$SwitchMap$com$here$android$mpa$mapping$TrafficEvent$Severity[severity.ordinal()]) {
                        case 1:
                            com.navdy.hud.app.maps.here.HereTrafficUpdater2.HereTrafficEvent hereTrafficEvent = new com.navdy.hud.app.maps.here.HereTrafficUpdater2.HereTrafficEvent(event, distanceToEvent);
                            blockingList.add(hereTrafficEvent);
                            blocking++;
                            break;
                        case 2:
                            com.navdy.hud.app.maps.here.HereTrafficUpdater2.HereTrafficEvent hereTrafficEvent2 = new com.navdy.hud.app.maps.here.HereTrafficUpdater2.HereTrafficEvent(event, distanceToEvent);
                            arrayList.add(hereTrafficEvent2);
                            veryHighCount++;
                            break;
                        case 3:
                            com.navdy.hud.app.maps.here.HereTrafficUpdater2.HereTrafficEvent hereTrafficEvent3 = new com.navdy.hud.app.maps.here.HereTrafficUpdater2.HereTrafficEvent(event, distanceToEvent);
                            arrayList2.add(hereTrafficEvent3);
                            highCount++;
                            break;
                    }
                } else {
                    sLogger.v("no road elements for event");
                }
            } else {
                sLogger.v("[NOT_ON_ROUTE] event is not on route");
            }
        }
        sLogger.v("blocking:" + blocking + " very high:" + veryHighCount + " high:" + highCount);
        if (blocking > 0) {
            this.bus.post(buildEvent(com.navdy.hud.app.maps.MapEvents.DisplayTrafficIncident.Type.BLOCKING, blockingList));
        } else if (veryHighCount > 0) {
            this.bus.post(buildEvent(com.navdy.hud.app.maps.MapEvents.DisplayTrafficIncident.Type.VERY_HIGH, arrayList));
        } else if (highCount > 0) {
            this.bus.post(buildEvent(com.navdy.hud.app.maps.MapEvents.DisplayTrafficIncident.Type.HIGH, arrayList2));
        } else {
            this.bus.post(NORMAL_EVENT);
        }
        sLogger.v("processEvents took [" + (android.os.SystemClock.elapsedRealtime() - l1) + "]");
    }

    private com.navdy.hud.app.maps.MapEvents.DisplayTrafficIncident buildEvent(com.navdy.hud.app.maps.MapEvents.DisplayTrafficIncident.Type type, java.util.List<com.navdy.hud.app.maps.here.HereTrafficUpdater2.HereTrafficEvent> list) {
        java.lang.String title;
        java.lang.String description;
        com.navdy.hud.app.maps.MapEvents.DisplayTrafficIncident.Category category;
        com.navdy.hud.app.maps.here.HereTrafficUpdater2.HereTrafficEvent accidentEvent = null;
        com.navdy.hud.app.maps.here.HereTrafficUpdater2.HereTrafficEvent closureEvent = null;
        com.navdy.hud.app.maps.here.HereTrafficUpdater2.HereTrafficEvent roadworkEvent = null;
        com.navdy.hud.app.maps.here.HereTrafficUpdater2.HereTrafficEvent congestionEvent = null;
        com.navdy.hud.app.maps.here.HereTrafficUpdater2.HereTrafficEvent flowEvent = null;
        com.navdy.hud.app.maps.here.HereTrafficUpdater2.HereTrafficEvent otherEvent = null;
        com.navdy.hud.app.maps.here.HereTrafficUpdater2.HereTrafficEvent undefinedEvent = null;
        for (com.navdy.hud.app.maps.here.HereTrafficUpdater2.HereTrafficEvent event : list) {
            java.lang.String shortText = event.trafficEvent.getShortText();
            char c = 65535;
            switch (shortText.hashCode()) {
                case -1360575985:
                    if (shortText.equals(com.navdy.hud.app.maps.MapEvents.DisplayTrafficIncident.ACCIDENT)) {
                        c = 0;
                        break;
                    }
                    break;
                case 2160942:
                    if (shortText.equals(com.navdy.hud.app.maps.MapEvents.DisplayTrafficIncident.FLOW)) {
                        c = 4;
                        break;
                    }
                    break;
                case 75532016:
                    if (shortText.equals("OTHER")) {
                        c = 5;
                        break;
                    }
                    break;
                case 1584535067:
                    if (shortText.equals(com.navdy.hud.app.maps.MapEvents.DisplayTrafficIncident.CLOSURE)) {
                        c = 1;
                        break;
                    }
                    break;
                case 1748463920:
                    if (shortText.equals(com.navdy.hud.app.maps.MapEvents.DisplayTrafficIncident.UNDEFINED)) {
                        c = 6;
                        break;
                    }
                    break;
                case 1943412802:
                    if (shortText.equals(com.navdy.hud.app.maps.MapEvents.DisplayTrafficIncident.ROADWORKS)) {
                        c = 2;
                        break;
                    }
                    break;
                case 2101625895:
                    if (shortText.equals(com.navdy.hud.app.maps.MapEvents.DisplayTrafficIncident.CONGESTION)) {
                        c = 3;
                        break;
                    }
                    break;
            }
            switch (c) {
                case 0:
                    if (accidentEvent != null) {
                        if (accidentEvent.distance <= event.distance) {
                            break;
                        } else {
                            accidentEvent = event;
                            break;
                        }
                    } else {
                        accidentEvent = event;
                        break;
                    }
                case 1:
                    if (closureEvent != null) {
                        if (closureEvent.distance <= event.distance) {
                            break;
                        } else {
                            closureEvent = event;
                            break;
                        }
                    } else {
                        closureEvent = event;
                        break;
                    }
                case 2:
                    if (roadworkEvent != null) {
                        if (roadworkEvent.distance <= event.distance) {
                            break;
                        } else {
                            roadworkEvent = event;
                            break;
                        }
                    } else {
                        roadworkEvent = event;
                        break;
                    }
                case 3:
                    if (congestionEvent != null) {
                        if (congestionEvent.distance <= event.distance) {
                            break;
                        } else {
                            congestionEvent = event;
                            break;
                        }
                    } else {
                        congestionEvent = event;
                        break;
                    }
                case 4:
                    if (flowEvent != null) {
                        if (flowEvent.distance <= event.distance) {
                            break;
                        } else {
                            flowEvent = event;
                            break;
                        }
                    } else {
                        flowEvent = event;
                        break;
                    }
                case 5:
                    if (otherEvent != null) {
                        if (otherEvent.distance <= event.distance) {
                            break;
                        } else {
                            otherEvent = event;
                            break;
                        }
                    } else {
                        otherEvent = event;
                        break;
                    }
                default:
                    if (undefinedEvent != null) {
                        if (undefinedEvent.distance <= event.distance) {
                            break;
                        } else {
                            undefinedEvent = event;
                            break;
                        }
                    } else {
                        undefinedEvent = event;
                        break;
                    }
            }
        }
        com.navdy.hud.app.maps.here.HereTrafficUpdater2.HereTrafficEvent userEvent = null;
        if (accidentEvent != null) {
            userEvent = accidentEvent;
        } else if (closureEvent != null) {
            userEvent = closureEvent;
        } else if (roadworkEvent != null) {
            userEvent = roadworkEvent;
        } else if (congestionEvent != null) {
            userEvent = congestionEvent;
        } else if (flowEvent != null) {
            userEvent = flowEvent;
        } else if (otherEvent != null) {
            userEvent = otherEvent;
        } else if (undefinedEvent != null) {
            userEvent = undefinedEvent;
        }
        java.lang.String affectedStreet = userEvent.trafficEvent.getFirstAffectedStreet();
        java.lang.String shortText2 = userEvent.trafficEvent.getShortText();
        char c2 = 65535;
        switch (shortText2.hashCode()) {
            case -1360575985:
                if (shortText2.equals(com.navdy.hud.app.maps.MapEvents.DisplayTrafficIncident.ACCIDENT)) {
                    c2 = 0;
                    break;
                }
                break;
            case 2160942:
                if (shortText2.equals(com.navdy.hud.app.maps.MapEvents.DisplayTrafficIncident.FLOW)) {
                    c2 = 4;
                    break;
                }
                break;
            case 75532016:
                if (shortText2.equals("OTHER")) {
                    c2 = 5;
                    break;
                }
                break;
            case 1584535067:
                if (shortText2.equals(com.navdy.hud.app.maps.MapEvents.DisplayTrafficIncident.CLOSURE)) {
                    c2 = 1;
                    break;
                }
                break;
            case 1748463920:
                if (shortText2.equals(com.navdy.hud.app.maps.MapEvents.DisplayTrafficIncident.UNDEFINED)) {
                    c2 = 6;
                    break;
                }
                break;
            case 1943412802:
                if (shortText2.equals(com.navdy.hud.app.maps.MapEvents.DisplayTrafficIncident.ROADWORKS)) {
                    c2 = 2;
                    break;
                }
                break;
            case 2101625895:
                if (shortText2.equals(com.navdy.hud.app.maps.MapEvents.DisplayTrafficIncident.CONGESTION)) {
                    c2 = 3;
                    break;
                }
                break;
        }
        switch (c2) {
            case 0:
                title = "Accident";
                description = userEvent.trafficEvent.getEventText();
                category = com.navdy.hud.app.maps.MapEvents.DisplayTrafficIncident.Category.ACCIDENT;
                break;
            case 1:
                title = "Closure";
                description = userEvent.trafficEvent.getEventText();
                category = com.navdy.hud.app.maps.MapEvents.DisplayTrafficIncident.Category.CLOSURE;
                break;
            case 2:
                title = "RoadWork";
                description = userEvent.trafficEvent.getEventText();
                category = com.navdy.hud.app.maps.MapEvents.DisplayTrafficIncident.Category.ROADWORKS;
                break;
            case 3:
                title = "Congestion";
                description = userEvent.trafficEvent.getEventText();
                category = com.navdy.hud.app.maps.MapEvents.DisplayTrafficIncident.Category.CONGESTION;
                break;
            case 4:
                title = "Congestion";
                description = "";
                category = com.navdy.hud.app.maps.MapEvents.DisplayTrafficIncident.Category.FLOW;
                break;
            case 5:
                title = "Other";
                description = userEvent.trafficEvent.getEventText();
                category = com.navdy.hud.app.maps.MapEvents.DisplayTrafficIncident.Category.OTHER;
                break;
            default:
                title = "Undefined:" + userEvent.trafficEvent.getShortText();
                description = userEvent.trafficEvent.getEventText();
                category = com.navdy.hud.app.maps.MapEvents.DisplayTrafficIncident.Category.UNDEFINED;
                break;
        }
        return new com.navdy.hud.app.maps.MapEvents.DisplayTrafficIncident(type, category, title, description, affectedStreet, userEvent.distance, userEvent.trafficEvent.getActivationDate(), userEvent.trafficEvent.getActivationDate(), userEvent.trafficEvent.getIconOnRoute());
    }

    /* access modifiers changed from: 0000 */
    public boolean isRunning() {
        return this.route != null;
    }

    /* access modifiers changed from: 0000 */
    public int getRefereshInterval() {
        if (com.navdy.hud.app.framework.network.NetworkBandwidthController.getInstance().isLimitBandwidthModeOn()) {
            return TRAFFIC_DATA_REFRESH_INTERVAL_LIMITED_BANDWIDTH;
        }
        return TRAFFIC_DATA_REFRESH_INTERVAL;
    }
}
