package com.navdy.hud.app.maps.here;

public class HereSafetySpotListener extends com.here.android.mpa.guidance.NavigationManager.SafetySpotListener {
    /* access modifiers changed from: private */
    public static final int CHECK_INTERVAL = ((int) java.util.concurrent.TimeUnit.SECONDS.toMillis(30));
    private static final int SAFETY_SPOT_HIDE_DISTANCE = 1000;
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger("SafetySpotListener");
    private final com.squareup.otto.Bus bus;
    /* access modifiers changed from: private */
    public boolean canDrawOnTheMap = true;
    /* access modifiers changed from: private */
    public final java.lang.Runnable checkExpiredSpots = new com.navdy.hud.app.maps.here.HereSafetySpotListener.Anon1();
    /* access modifiers changed from: private */
    public final android.os.Handler handler = new android.os.Handler(android.os.Looper.getMainLooper());
    /* access modifiers changed from: private */
    public final com.navdy.hud.app.maps.here.HereMapController mapController;
    /* access modifiers changed from: private */
    public java.util.List<com.navdy.hud.app.maps.here.HereSafetySpotListener.SafetySpotContainer> safetySpotNotifications = new java.util.ArrayList();

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            try {
                java.util.List<com.navdy.hud.app.maps.here.HereSafetySpotListener.SafetySpotContainer> invalid = com.navdy.hud.app.maps.here.HereSafetySpotListener.this.removeExpiredSpots();
                if (com.navdy.hud.app.maps.here.HereSafetySpotListener.this.canDrawOnTheMap) {
                    com.navdy.hud.app.maps.here.HereSafetySpotListener.this.removeMapMarkers(invalid);
                }
            } catch (Throwable t) {
                com.navdy.hud.app.maps.here.HereSafetySpotListener.sLogger.e("check", t);
            } finally {
                com.navdy.hud.app.maps.here.HereSafetySpotListener.this.handler.postDelayed(this, (long) com.navdy.hud.app.maps.here.HereSafetySpotListener.CHECK_INTERVAL);
            }
        }
    }

    class Anon2 implements java.lang.Runnable {
        final /* synthetic */ com.here.android.mpa.guidance.SafetySpotNotification val$safetySpotNotification;

        Anon2(com.here.android.mpa.guidance.SafetySpotNotification safetySpotNotification) {
            this.val$safetySpotNotification = safetySpotNotification;
        }

        public void run() {
            java.util.List<com.here.android.mpa.guidance.SafetySpotNotificationInfo> safetySpotNotificationList = this.val$safetySpotNotification.getSafetySpotNotificationInfos();
            if (safetySpotNotificationList == null || safetySpotNotificationList.size() == 0) {
                com.navdy.hud.app.maps.here.HereSafetySpotListener.sLogger.i("no safety spots");
                return;
            }
            com.here.android.mpa.common.GeoCoordinate currentLocation = com.navdy.hud.app.maps.here.HereMapsManager.getInstance().getLocationFixManager().getLastGeoCoordinate();
            if (currentLocation == null) {
                com.navdy.hud.app.maps.here.HereSafetySpotListener.sLogger.e("no current location");
                return;
            }
            java.util.List<com.here.android.mpa.guidance.SafetySpotNotificationInfo> tmpList = new java.util.ArrayList<>();
            for (com.here.android.mpa.guidance.SafetySpotNotificationInfo info : safetySpotNotificationList) {
                com.here.android.mpa.mapping.SafetySpotInfo safetySpotInfo = info.getSafetySpot();
                if (!(safetySpotInfo == null || safetySpotInfo.getType() == com.here.android.mpa.mapping.SafetySpotInfo.Type.UNDEFINED)) {
                    tmpList.add(info);
                }
            }
            if (tmpList.size() == 0) {
                com.navdy.hud.app.maps.here.HereSafetySpotListener.sLogger.i("no valid safety spots");
                return;
            }
            com.navdy.hud.app.maps.here.HereSafetySpotListener.this.handler.removeCallbacks(com.navdy.hud.app.maps.here.HereSafetySpotListener.this.checkExpiredSpots);
            com.navdy.hud.app.maps.here.HereSafetySpotListener.this.addSafetySpotMarkers(tmpList, currentLocation);
            com.navdy.hud.app.maps.here.HereSafetySpotListener.this.handler.postDelayed(com.navdy.hud.app.maps.here.HereSafetySpotListener.this.checkExpiredSpots, (long) com.navdy.hud.app.maps.here.HereSafetySpotListener.CHECK_INTERVAL);
        }
    }

    class Anon3 implements java.lang.Runnable {
        final /* synthetic */ boolean val$enabled;

        Anon3(boolean z) {
            this.val$enabled = z;
        }

        public void run() {
            if (com.navdy.hud.app.maps.here.HereSafetySpotListener.this.canDrawOnTheMap != this.val$enabled) {
                com.navdy.hud.app.maps.here.HereSafetySpotListener.this.canDrawOnTheMap = this.val$enabled;
                if (com.navdy.hud.app.maps.here.HereSafetySpotListener.this.canDrawOnTheMap) {
                    com.navdy.hud.app.maps.here.HereSafetySpotListener.sLogger.d("Enabling the safety spots on the Map, Total spots Valid : " + com.navdy.hud.app.maps.here.HereSafetySpotListener.this.safetySpotNotifications.size() + ", Invalid : " + com.navdy.hud.app.maps.here.HereSafetySpotListener.this.removeExpiredSpots());
                    java.util.List<com.here.android.mpa.mapping.MapObject> mapMarkers = new java.util.ArrayList<>();
                    for (com.navdy.hud.app.maps.here.HereSafetySpotListener.SafetySpotContainer container : com.navdy.hud.app.maps.here.HereSafetySpotListener.this.safetySpotNotifications) {
                        mapMarkers.add(container.mapMarker);
                    }
                    com.navdy.hud.app.maps.here.HereSafetySpotListener.this.mapController.addMapObjects(mapMarkers);
                    return;
                }
                com.navdy.hud.app.maps.here.HereSafetySpotListener.sLogger.d("Remove all the safety spots from the map");
                com.navdy.hud.app.maps.here.HereSafetySpotListener.this.removeMapMarkers(com.navdy.hud.app.maps.here.HereSafetySpotListener.this.safetySpotNotifications);
            }
        }
    }

    static /* synthetic */ class Anon4 {
        static final /* synthetic */ int[] $SwitchMap$com$here$android$mpa$mapping$SafetySpotInfo$Type = new int[com.here.android.mpa.mapping.SafetySpotInfo.Type.values().length];

        static {
            try {
                $SwitchMap$com$here$android$mpa$mapping$SafetySpotInfo$Type[com.here.android.mpa.mapping.SafetySpotInfo.Type.SPEED_CAMERA.ordinal()] = 1;
            } catch (java.lang.NoSuchFieldError e) {
            }
            try {
                $SwitchMap$com$here$android$mpa$mapping$SafetySpotInfo$Type[com.here.android.mpa.mapping.SafetySpotInfo.Type.SPEED_REDLIGHT_CAMERA.ordinal()] = 2;
            } catch (java.lang.NoSuchFieldError e2) {
            }
        }
    }

    private static class SafetySpotContainer {
        /* access modifiers changed from: private */
        public com.here.android.mpa.mapping.MapMarker mapMarker;
        /* access modifiers changed from: private */
        public com.here.android.mpa.guidance.SafetySpotNotificationInfo safetySpotNotificationInfo;

        private SafetySpotContainer() {
        }

        /* synthetic */ SafetySpotContainer(com.navdy.hud.app.maps.here.HereSafetySpotListener.Anon1 x0) {
            this();
        }
    }

    HereSafetySpotListener(com.squareup.otto.Bus bus2, com.navdy.hud.app.maps.here.HereMapController mapController2) {
        sLogger.v("ctor");
        this.bus = bus2;
        this.mapController = mapController2;
    }

    /* access modifiers changed from: private */
    public java.util.List<com.navdy.hud.app.maps.here.HereSafetySpotListener.SafetySpotContainer> removeExpiredSpots() {
        java.util.List<com.navdy.hud.app.maps.here.HereSafetySpotListener.SafetySpotContainer> invalid = null;
        com.here.android.mpa.common.GeoCoordinate currentLocation = com.navdy.hud.app.maps.here.HereMapsManager.getInstance().getLocationFixManager().getLastGeoCoordinate();
        if (currentLocation == null) {
            sLogger.e("check:no current location");
        } else if (this.safetySpotNotifications.size() != 0) {
            java.util.List<com.navdy.hud.app.maps.here.HereSafetySpotListener.SafetySpotContainer> valid = new java.util.ArrayList<>();
            invalid = new java.util.ArrayList<>();
            for (com.navdy.hud.app.maps.here.HereSafetySpotListener.SafetySpotContainer info : this.safetySpotNotifications) {
                int id = java.lang.System.identityHashCode(info.safetySpotNotificationInfo);
                if (isSafetySpotRelevant(info.safetySpotNotificationInfo, currentLocation)) {
                    invalid.add(info);
                    sLogger.i("check remove:" + id);
                } else {
                    valid.add(info);
                    sLogger.i("check valid:" + id);
                }
            }
            this.safetySpotNotifications.clear();
            this.safetySpotNotifications.addAll(valid);
        }
        return invalid;
    }

    private boolean isSafetySpotRelevant(com.here.android.mpa.guidance.SafetySpotNotificationInfo safetySpotNotificationInfo, com.here.android.mpa.common.GeoCoordinate currentLocation) {
        int distanceCalc = (int) currentLocation.distanceTo(safetySpotNotificationInfo.getSafetySpot().getCoordinate());
        sLogger.i("check safety spot id[" + java.lang.System.identityHashCode(safetySpotNotificationInfo) + "] type[" + safetySpotNotificationInfo.getSafetySpot().getType() + "] distance calc[" + distanceCalc + "] distance[" + safetySpotNotificationInfo.getDistance() + "]");
        return distanceCalc >= 1000;
    }

    public void onSafetySpot(com.here.android.mpa.guidance.SafetySpotNotification safetySpotNotification) {
        this.handler.post(new com.navdy.hud.app.maps.here.HereSafetySpotListener.Anon2(safetySpotNotification));
    }

    /* access modifiers changed from: private */
    public void addSafetySpotMarkers(java.util.List<com.here.android.mpa.guidance.SafetySpotNotificationInfo> list, com.here.android.mpa.common.GeoCoordinate currentLocation) {
        com.here.android.mpa.mapping.SafetySpotInfo.Type nearestType = null;
        int nearestDistance = 0;
        java.util.List<com.navdy.hud.app.maps.here.HereSafetySpotListener.SafetySpotContainer> addList = new java.util.ArrayList<>();
        java.util.List<com.here.android.mpa.mapping.MapObject> mapMarkers = new java.util.ArrayList<>();
        for (com.here.android.mpa.guidance.SafetySpotNotificationInfo info : list) {
            try {
                com.navdy.hud.app.maps.here.HereSafetySpotListener.SafetySpotContainer container = new com.navdy.hud.app.maps.here.HereSafetySpotListener.SafetySpotContainer(null);
                container.safetySpotNotificationInfo = info;
                com.here.android.mpa.mapping.SafetySpotInfo safetySpot = info.getSafetySpot();
                com.here.android.mpa.mapping.SafetySpotInfo.Type type = safetySpot.getType();
                com.here.android.mpa.common.GeoCoordinate safetySpotLocation = safetySpot.getCoordinate();
                com.here.android.mpa.common.Image image = new com.here.android.mpa.common.Image();
                switch (com.navdy.hud.app.maps.here.HereSafetySpotListener.Anon4.$SwitchMap$com$here$android$mpa$mapping$SafetySpotInfo$Type[type.ordinal()]) {
                    case 1:
                        image.setImageResource(com.navdy.hud.app.R.drawable.icon_speed_camera);
                        break;
                    default:
                        image.setImageResource(com.navdy.hud.app.R.drawable.icon_red_light_camera);
                        break;
                }
                com.here.android.mpa.mapping.MapMarker mapMarker = new com.here.android.mpa.mapping.MapMarker(safetySpotLocation, image);
                container.mapMarker = mapMarker;
                if (this.canDrawOnTheMap) {
                    mapMarkers.add(container.mapMarker);
                }
                addList.add(container);
                int distance = (int) info.getDistance();
                sLogger.i("add safety spot id[" + java.lang.System.identityHashCode(info) + "] type[" + type + "] distance calc[" + ((int) currentLocation.distanceTo(safetySpotLocation)) + "] distance[" + distance + "]");
                if (nearestType == null || nearestDistance > distance) {
                    nearestType = type;
                    nearestDistance = distance;
                }
            } catch (Throwable t) {
                sLogger.e("map marker error", t);
            }
        }
        if (this.canDrawOnTheMap) {
            this.mapController.addMapObjects(mapMarkers);
        }
        this.safetySpotNotifications.addAll(addList);
        if (nearestType != null && nearestDistance > 0) {
            sendTts(nearestType, nearestDistance);
        }
    }

    private void sendTts(com.here.android.mpa.mapping.SafetySpotInfo.Type type, int distance) {
        int resId;
        if (java.lang.Boolean.TRUE.equals(com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile().getNavigationPreferences().spokenCameraWarnings)) {
            com.navdy.hud.app.maps.util.DistanceConverter.Distance distanceConverter = new com.navdy.hud.app.maps.util.DistanceConverter.Distance();
            com.navdy.hud.app.maps.util.DistanceConverter.convertToDistance(com.navdy.hud.app.manager.SpeedManager.getInstance().getSpeedUnit(), (float) distance, distanceConverter);
            java.lang.String formattedDistance = com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.getFormattedDistance(distanceConverter.value, distanceConverter.unit, true);
            switch (com.navdy.hud.app.maps.here.HereSafetySpotListener.Anon4.$SwitchMap$com$here$android$mpa$mapping$SafetySpotInfo$Type[type.ordinal()]) {
                case 1:
                    resId = com.navdy.hud.app.R.string.camera_warning_speed;
                    break;
                case 2:
                    resId = com.navdy.hud.app.R.string.camera_warning_both;
                    break;
                default:
                    resId = com.navdy.hud.app.R.string.camera_warning_red_light;
                    break;
            }
            com.navdy.hud.app.framework.voice.TTSUtils.sendSpeechRequest(com.navdy.hud.app.HudApplication.getAppContext().getResources().getString(resId, new java.lang.Object[]{formattedDistance}), com.navdy.service.library.events.audio.Category.SPEECH_CAMERA_WARNING, null);
        }
    }

    public void setSafetySpotsEnabledOnMap(boolean enabled) {
        this.handler.post(new com.navdy.hud.app.maps.here.HereSafetySpotListener.Anon3(enabled));
    }

    /* access modifiers changed from: private */
    public void removeMapMarkers(java.util.List<com.navdy.hud.app.maps.here.HereSafetySpotListener.SafetySpotContainer> list) {
        if (list != null) {
            java.util.List<com.here.android.mpa.mapping.MapObject> mapMarkers = new java.util.ArrayList<>();
            for (com.navdy.hud.app.maps.here.HereSafetySpotListener.SafetySpotContainer info : list) {
                mapMarkers.add(info.mapMarker);
            }
            this.mapController.removeMapObjects(mapMarkers);
        }
    }
}
