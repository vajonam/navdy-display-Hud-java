package com.navdy.hud.app.maps.here;

public class HereRerouteListener extends com.here.android.mpa.guidance.NavigationManager.RerouteListener {
    /* access modifiers changed from: private */
    public static final long MIN_REROUTE_TTS_INTERVAL = java.util.concurrent.TimeUnit.SECONDS.toMillis(15);
    public static final com.navdy.hud.app.maps.MapEvents.RerouteEvent REROUTE_FAILED = new com.navdy.hud.app.maps.MapEvents.RerouteEvent(com.navdy.hud.app.maps.MapEvents.RouteEventType.FAILED);
    public static final com.navdy.hud.app.maps.MapEvents.RerouteEvent REROUTE_FINISHED = new com.navdy.hud.app.maps.MapEvents.RerouteEvent(com.navdy.hud.app.maps.MapEvents.RouteEventType.FINISHED);
    public static final com.navdy.hud.app.maps.MapEvents.RerouteEvent REROUTE_STARTED = new com.navdy.hud.app.maps.MapEvents.RerouteEvent(com.navdy.hud.app.maps.MapEvents.RouteEventType.STARTED);
    /* access modifiers changed from: private */
    public com.squareup.otto.Bus bus;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.maps.here.HereNavigationManager hereNavigationManager;
    /* access modifiers changed from: private */
    public long lastRerouteTTS;
    /* access modifiers changed from: private */
    public com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.maps.here.HereRerouteListener.class);
    /* access modifiers changed from: private */
    public final com.navdy.hud.app.analytics.NavigationQualityTracker navigationQualityTracker;
    /* access modifiers changed from: private */
    public java.lang.String routeCalcId;
    /* access modifiers changed from: private */
    public volatile boolean routeCalculationOn;
    /* access modifiers changed from: private */
    public java.lang.String tag;

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            com.navdy.hud.app.maps.here.HereRerouteListener.this.routeCalcId = com.navdy.hud.app.maps.here.HereRerouteListener.this.hereNavigationManager.getCurrentRouteId();
            com.navdy.hud.app.maps.here.HereRerouteListener.this.logger.i(com.navdy.hud.app.maps.here.HereRerouteListener.this.tag + " reroute-begin: id=" + com.navdy.hud.app.maps.here.HereRerouteListener.this.routeCalcId);
            com.navdy.hud.app.maps.here.HereRerouteListener.this.routeCalculationOn = true;
            long now = android.os.SystemClock.elapsedRealtime();
            if (com.navdy.hud.app.maps.here.HereRerouteListener.this.hereNavigationManager.getNavigationSessionPreference().spokenTurnByTurn && !com.navdy.hud.app.maps.here.HereRerouteListener.this.hereNavigationManager.hasArrived() && now - com.navdy.hud.app.maps.here.HereRerouteListener.this.lastRerouteTTS > com.navdy.hud.app.maps.here.HereRerouteListener.MIN_REROUTE_TTS_INTERVAL && !com.navdy.hud.app.maps.MapSettings.isTtsRecalculationDisabled()) {
                com.navdy.hud.app.maps.here.HereRerouteListener.this.bus.post(com.navdy.hud.app.maps.here.HereRerouteListener.this.hereNavigationManager.TTS_REROUTING);
                com.navdy.hud.app.maps.here.HereRerouteListener.this.lastRerouteTTS = now;
            }
            com.navdy.hud.app.maps.here.HereRerouteListener.this.bus.post(com.navdy.hud.app.maps.here.HereRerouteListener.REROUTE_STARTED);
            com.navdy.hud.app.maps.here.HereRerouteListener.this.bus.post(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.EMPTY_MANEUVER_DISPLAY);
        }
    }

    class Anon2 implements java.lang.Runnable {
        final /* synthetic */ com.here.android.mpa.routing.Route val$route;

        Anon2(com.here.android.mpa.routing.Route route) {
            this.val$route = route;
        }

        public void run() {
            java.lang.String currentRouteId = com.navdy.hud.app.maps.here.HereRerouteListener.this.hereNavigationManager.getCurrentRouteId();
            java.lang.String id = com.navdy.hud.app.maps.here.HereRerouteListener.this.routeCalcId;
            com.navdy.hud.app.maps.here.HereRerouteListener.this.logger.i(com.navdy.hud.app.maps.here.HereRerouteListener.this.tag + " reroute-end: " + this.val$route + " id=" + com.navdy.hud.app.maps.here.HereRerouteListener.this.routeCalcId + " current=" + currentRouteId);
            com.navdy.hud.app.maps.here.HereRerouteListener.this.routeCalcId = null;
            com.navdy.hud.app.maps.here.HereRerouteListener.this.routeCalculationOn = false;
            com.navdy.hud.app.maps.here.HereRerouteListener.this.bus.post(com.navdy.hud.app.maps.here.HereRerouteListener.REROUTE_FINISHED);
            if (this.val$route == null) {
                return;
            }
            if (!com.navdy.hud.app.maps.here.HereRerouteListener.this.hereNavigationManager.isNavigationModeOn()) {
                com.navdy.hud.app.maps.here.HereRerouteListener.this.logger.i(com.navdy.hud.app.maps.here.HereRerouteListener.this.tag + "reroute-end: navigation already ended, not adding route");
            } else if (!android.text.TextUtils.equals(id, currentRouteId)) {
                com.navdy.hud.app.maps.here.HereRerouteListener.this.logger.i("route change before recalc completed: cannot use");
            } else {
                com.navdy.hud.app.maps.here.HereRerouteListener.this.hereNavigationManager.setReroute(this.val$route, com.navdy.service.library.events.navigation.NavigationSessionRouteChange.RerouteReason.NAV_SESSION_OFF_REROUTE, null, null, false, com.navdy.hud.app.maps.here.HereRerouteListener.this.hereNavigationManager.isTrafficConsidered(currentRouteId));
                com.navdy.hud.app.maps.here.HereRerouteListener.this.navigationQualityTracker.trackTripRecalculated();
            }
        }
    }

    class Anon3 implements java.lang.Runnable {
        Anon3() {
        }

        public void run() {
            com.navdy.hud.app.maps.here.HereRerouteListener.this.routeCalculationOn = false;
            com.navdy.hud.app.maps.here.HereRerouteListener.this.routeCalcId = null;
            com.navdy.hud.app.maps.here.HereRerouteListener.this.logger.w(com.navdy.hud.app.maps.here.HereRerouteListener.this.tag + " reroute-failed");
            if (com.navdy.hud.app.maps.here.HereRerouteListener.this.hereNavigationManager.getNavigationSessionPreference().spokenTurnByTurn && !com.navdy.hud.app.maps.here.HereRerouteListener.this.hereNavigationManager.hasArrived() && !com.navdy.hud.app.maps.MapSettings.isTtsRecalculationDisabled()) {
                com.navdy.hud.app.maps.here.HereRerouteListener.this.bus.post(com.navdy.hud.app.maps.here.HereRerouteListener.this.hereNavigationManager.TTS_REROUTING_FAILED);
            }
            com.navdy.hud.app.maps.here.HereRerouteListener.this.bus.post(com.navdy.hud.app.maps.here.HereRerouteListener.REROUTE_FAILED);
        }
    }

    HereRerouteListener(com.navdy.service.library.log.Logger logger2, java.lang.String tag2, com.navdy.hud.app.maps.here.HereNavigationManager hereNavigationManager2, com.squareup.otto.Bus bus2) {
        this.tag = tag2;
        this.hereNavigationManager = hereNavigationManager2;
        this.bus = bus2;
        this.navigationQualityTracker = com.navdy.hud.app.analytics.NavigationQualityTracker.getInstance();
    }

    public void onRerouteBegin() {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.maps.here.HereRerouteListener.Anon1(), 20);
    }

    public void onRerouteEnd(com.here.android.mpa.routing.Route route) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.maps.here.HereRerouteListener.Anon2(route), 20);
    }

    public void onRerouteFailed() {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.maps.here.HereRerouteListener.Anon3(), 20);
    }

    public boolean isRecalculating() {
        return this.routeCalculationOn;
    }
}
