package com.navdy.hud.app.maps.here;

public class HereNavController {
    private static final int ANIMATE_STATE_CLEAR_DELAY = 2000;
    private static final com.navdy.hud.app.framework.trips.TripManager.FinishedTripRouteEvent TRIP_END = new com.navdy.hud.app.framework.trips.TripManager.FinishedTripRouteEvent();
    private static final com.navdy.hud.app.framework.trips.TripManager.NewTripEvent TRIP_START = new com.navdy.hud.app.framework.trips.TripManager.NewTripEvent();
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.maps.here.HereNavController.class);
    private final com.squareup.otto.Bus bus;
    private java.lang.Runnable clearAnimatorState = new com.navdy.hud.app.maps.here.HereNavController.Anon1();
    /* access modifiers changed from: private */
    public java.lang.Runnable clearAnimatorStateBk = new com.navdy.hud.app.maps.here.HereNavController.Anon2();
    private boolean firstTrip = true;
    private android.os.Handler handler = new android.os.Handler(android.os.Looper.getMainLooper());
    private boolean initialized;
    private final com.here.android.mpa.guidance.NavigationManager navigationManager;
    private final com.navdy.hud.app.analytics.NavigationQualityTracker navigationQualityTracker;
    private com.here.android.mpa.routing.Route route;
    private volatile com.navdy.hud.app.maps.here.HereNavController.State state;

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            com.navdy.service.library.task.TaskManager.getInstance().execute(com.navdy.hud.app.maps.here.HereNavController.this.clearAnimatorStateBk, 17);
        }
    }

    class Anon2 implements java.lang.Runnable {
        Anon2() {
        }

        public void run() {
            com.navdy.hud.app.maps.here.HereMapsManager.getInstance().getMapAnimator().clearState();
        }
    }

    class Anon3 implements java.lang.Runnable {
        Anon3() {
        }

        public void run() {
            com.navdy.hud.app.framework.glympse.GlympseManager.getInstance().expireActiveTickets();
        }
    }

    public enum State {
        NAVIGATING,
        TRACKING
    }

    HereNavController(com.here.android.mpa.guidance.NavigationManager navigationManager2, com.squareup.otto.Bus bus2) {
        this.navigationManager = navigationManager2;
        this.bus = bus2;
        sLogger.v(":ctor: state:" + this.state);
        this.navigationQualityTracker = com.navdy.hud.app.analytics.NavigationQualityTracker.getInstance();
    }

    public com.navdy.hud.app.maps.here.HereNavController.State getState() {
        return this.state;
    }

    public synchronized void initialize() {
        if (!this.initialized) {
            this.initialized = true;
            this.state = com.navdy.hud.app.maps.here.HereNavController.State.TRACKING;
            sLogger.v("initialize state:" + this.state + " error = " + this.navigationManager.startTracking());
            startTrip();
        }
    }

    public synchronized boolean isInitialized() {
        return this.initialized;
    }

    public synchronized com.here.android.mpa.guidance.NavigationManager.Error startNavigation(com.here.android.mpa.routing.Route route2, int simulationSpeed) {
        com.here.android.mpa.guidance.NavigationManager.Error error;
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        sLogger.v("startNavigation state[" + this.state + "]");
        sLogger.v("startNavigation simulationSpeed:" + simulationSpeed + " route-id=" + java.lang.System.identityHashCode(route2));
        if (this.state == com.navdy.hud.app.maps.here.HereNavController.State.NAVIGATING) {
            sLogger.v("startNavigation stop existing nav");
            stopNavigation(false);
        }
        long l1 = android.os.SystemClock.elapsedRealtime();
        if (simulationSpeed == 0) {
            simulationSpeed = com.navdy.hud.app.maps.MapSettings.getSimulationSpeed();
        }
        if (simulationSpeed > 0) {
            error = this.navigationManager.simulate(route2, (long) simulationSpeed);
        } else {
            error = this.navigationManager.startNavigation(route2);
        }
        sLogger.v("startNavigation took [" + (android.os.SystemClock.elapsedRealtime() - l1) + "]");
        if (error == com.here.android.mpa.guidance.NavigationManager.Error.NONE) {
            this.state = com.navdy.hud.app.maps.here.HereNavController.State.NAVIGATING;
            this.route = route2;
            sLogger.v("startNavigation: success [" + this.state + "] route-id=" + java.lang.System.identityHashCode(route2));
            endTrip();
            startTrip();
            this.navigationQualityTracker.trackTripStarted((long) route2.getTta(com.here.android.mpa.routing.Route.TrafficPenaltyMode.OPTIMAL, com.glympse.android.lib.e.gA).getDuration(), (long) route2.getTta(com.here.android.mpa.routing.Route.TrafficPenaltyMode.DISABLED, com.glympse.android.lib.e.gA).getDuration(), (long) route2.getLength());
            com.navdy.hud.app.analytics.AnalyticsSupport.recordNavigation(true);
        } else {
            sLogger.e("startNavigation error [" + this.state + "] " + error.name());
        }
        return error;
    }

    public synchronized void stopNavigation(boolean sendTripEvent) {
        boolean resetMapAnimator = false;
        synchronized (this) {
            com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
            sLogger.v("stopNavigation state[" + this.state + "]");
            if (this.state == com.navdy.hud.app.maps.here.HereNavController.State.NAVIGATING) {
                long l1 = android.os.SystemClock.elapsedRealtime();
                sLogger.v("stopNavigation:stopping nav state[" + this.state + " ] route-id=" + java.lang.System.identityHashCode(this.route));
                if (this.navigationManager.getNavigationMode() == com.here.android.mpa.guidance.NavigationManager.NavigationMode.SIMULATION) {
                    resetMapAnimator = true;
                }
                expireGlympseTickets();
                this.navigationManager.stop();
                if (resetMapAnimator) {
                    this.handler.postDelayed(this.clearAnimatorState, 2000);
                }
                sLogger.v("stopNavigation:stopped nav state[" + this.state + " ] took [" + (android.os.SystemClock.elapsedRealtime() - l1) + "]");
                sLogger.v("stopNavigation: tracking error state =" + this.navigationManager.startTracking());
                this.state = com.navdy.hud.app.maps.here.HereNavController.State.TRACKING;
                com.navdy.hud.app.analytics.AnalyticsSupport.recordNavigation(false);
                if (sendTripEvent) {
                    endTrip();
                    startTrip();
                }
                this.route = null;
                this.navigationQualityTracker.cancelTrip();
            } else {
                sLogger.v("stopNavigation: already " + this.state);
            }
        }
    }

    public synchronized void arrivedAtDestination() {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        sLogger.v("arrivedAtDestination state[" + this.state + "]");
        if (this.state == com.navdy.hud.app.maps.here.HereNavController.State.NAVIGATING) {
            this.navigationQualityTracker.trackTripEnded(getElapsedDistance());
            expireGlympseTickets();
            this.navigationManager.stop();
            sLogger.v("arrivedAtDestination: tracking error state =" + this.navigationManager.startTracking());
        } else {
            sLogger.v("arrivedAtDestination: not navigating:" + this.state);
        }
    }

    public synchronized void pause() {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        this.navigationManager.pause();
    }

    public synchronized com.here.android.mpa.guidance.NavigationManager.Error resume() {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        return this.navigationManager.resume();
    }

    public com.here.android.mpa.routing.RouteTta getTta(com.here.android.mpa.routing.Route.TrafficPenaltyMode trafficPenaltyMode, boolean wholeRoute) {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        return this.navigationManager.getTta(trafficPenaltyMode, wholeRoute);
    }

    public long getDestinationDistance() {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        if (this.state == com.navdy.hud.app.maps.here.HereNavController.State.TRACKING) {
            return 0;
        }
        return this.navigationManager.getDestinationDistance();
    }

    public java.util.Date getEta(boolean wholeRoute, com.here.android.mpa.routing.Route.TrafficPenaltyMode trafficPenaltyMode) {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        return this.navigationManager.getEta(wholeRoute, trafficPenaltyMode);
    }

    public java.util.Date getTtaDate(boolean wholeRoute, com.here.android.mpa.routing.Route.TrafficPenaltyMode trafficPenaltyMode) {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        com.here.android.mpa.routing.RouteTta routeTta = this.navigationManager.getTta(trafficPenaltyMode, wholeRoute);
        if (routeTta == null) {
            return null;
        }
        int trafficDuration = routeTta.getDuration();
        if (trafficDuration > 0) {
            return new java.util.Date(java.lang.System.currentTimeMillis() + java.util.concurrent.TimeUnit.SECONDS.toMillis((long) trafficDuration));
        }
        return null;
    }

    public com.here.android.mpa.routing.Maneuver getNextManeuver() {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        return this.navigationManager.getNextManeuver();
    }

    public com.here.android.mpa.routing.Maneuver getAfterNextManeuver() {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        return this.navigationManager.getAfterNextManeuver();
    }

    public long getElapsedDistance() {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        return this.navigationManager.getElapsedDistance();
    }

    public void addLaneInfoListener(java.lang.ref.WeakReference<com.here.android.mpa.guidance.NavigationManager.LaneInformationListener> listener) {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        this.navigationManager.addLaneInformationListener(listener);
    }

    public void removeLaneInfoListener(com.here.android.mpa.guidance.NavigationManager.LaneInformationListener listener) {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        this.navigationManager.removeLaneInformationListener(listener);
    }

    public void setRealisticViewMode(com.here.android.mpa.guidance.NavigationManager.RealisticViewMode mode) {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        this.navigationManager.setRealisticViewMode(mode);
    }

    public void addRealisticViewAspectRatio(com.here.android.mpa.guidance.NavigationManager.AspectRatio aspectRatio) {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        this.navigationManager.addRealisticViewAspectRatio(aspectRatio);
    }

    public void addRealisticViewListener(java.lang.ref.WeakReference<com.here.android.mpa.guidance.NavigationManager.RealisticViewListener> listener) {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        this.navigationManager.addRealisticViewListener(listener);
    }

    public void removeRealisticViewListener(com.here.android.mpa.guidance.NavigationManager.RealisticViewListener listener) {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        this.navigationManager.removeRealisticViewListener(listener);
    }

    public com.here.android.mpa.guidance.NavigationManager.Error setRoute(com.here.android.mpa.routing.Route route2) {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        return this.navigationManager.setRoute(route2);
    }

    public void addTrafficRerouteListener(java.lang.ref.WeakReference<com.here.android.mpa.guidance.NavigationManager.TrafficRerouteListener> listener) {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        this.navigationManager.addTrafficRerouteListener(listener);
    }

    public void removeTrafficRerouteListener(com.here.android.mpa.guidance.NavigationManager.TrafficRerouteListener listener) {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        this.navigationManager.removeTrafficRerouteListener(listener);
    }

    public void setDistanceUnit(com.here.android.mpa.guidance.NavigationManager.UnitSystem unitSystem) {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        this.navigationManager.setDistanceUnit(unitSystem);
    }

    public long getNextManeuverDistance() {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        return this.navigationManager.getNextManeuverDistance();
    }

    private void startTrip() {
        if (this.firstTrip) {
            this.firstTrip = false;
            sLogger.v("first trip started");
        } else {
            sLogger.v("trip started");
        }
        this.bus.post(TRIP_START);
    }

    public void endTrip() {
        sLogger.v("trip ended");
        this.bus.post(TRIP_END);
    }

    public void setTrafficAvoidanceMode(com.here.android.mpa.guidance.NavigationManager.TrafficAvoidanceMode mode) {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        this.navigationManager.setTrafficAvoidanceMode(mode);
    }

    /* access modifiers changed from: 0000 */
    public void expireGlympseTickets() {
        this.handler.post(new com.navdy.hud.app.maps.here.HereNavController.Anon3());
    }
}
