package com.navdy.hud.app.maps.here;

public final class HereMapCameraManager {
    public static final float DEFAULT_TILT = 60.0f;
    public static final double DEFAULT_ZOOM_LEVEL = 16.5d;
    private static final int EASE_OUT_DYNAMIC_ZOOM_TILT_INTERVAL = 10000;
    private static final double HIGH_SPEED_THRESHOLD = 29.0576d;
    private static final float HIGH_SPEED_TILT = 70.0f;
    private static final double HIGH_SPEED_ZOOM = 15.25d;
    private static final double LOW_SPEED_THRESHOLD = 8.9408d;
    private static final float LOW_SPEED_TILT = 60.0f;
    private static final double LOW_SPEED_ZOOM = 16.5d;
    private static final double MAX_ZOOM_LEVEL_DIAL = 16.5d;
    private static final double MEDIUM_SPEED_THRESHOLD = 20.1168d;
    private static final double MEDIUM_SPEED_ZOOM = 16.0d;
    private static final double MIN_SPEED_DIFFERENCE = 1.5d;
    private static final float MIN_TILT_DIFFERENCE_TRIGGER = 5.0f;
    private static final double MIN_ZOOM_DIFFERENCE_TRIGGER = 0.25d;
    public static final double MIN_ZOOM_LEVEL_DIAL = 12.0d;
    private static final double MPH_TO_MS = 0.44704d;
    private static final int N_ZOOM_TILT_ANIMATION_STEPS = 5;
    private static final float OVERVIEW_MAP_ZOOM_LEVEL = 10000.0f;
    private static final double VERY_HIGH_SPEED_THRESHOLD = 33.528d;
    private static final double VERY_HIGH_SPEED_ZOOM = 14.75d;
    private static final long ZOOM_LOCK_DURATION = 10000;
    private static final double ZOOM_STEP = 0.25d;
    private static final int ZOOM_TILT_ANIMATION_INTERVAL = 200;
    private static com.navdy.hud.app.maps.here.HereMapCameraManager sInstance = new com.navdy.hud.app.maps.here.HereMapCameraManager();
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.maps.here.HereMapCameraManager.class);
    /* access modifiers changed from: private */
    public volatile boolean animationOn;
    /* access modifiers changed from: private */
    public float currentDynamicTilt;
    /* access modifiers changed from: private */
    public double currentDynamicZoom;
    private com.here.android.mpa.common.GeoCoordinate currentGeoCoordinate;
    /* access modifiers changed from: private */
    public com.here.android.mpa.common.GeoPosition currentGeoPosition;
    /* access modifiers changed from: private */
    public java.lang.Runnable easeOutDynamicZoomTilt = new com.navdy.hud.app.maps.here.HereMapCameraManager.Anon2();
    /* access modifiers changed from: private */
    public java.lang.Runnable easeOutDynamicZoomTiltBk = new com.navdy.hud.app.maps.here.HereMapCameraManager.Anon3();
    /* access modifiers changed from: private */
    public volatile boolean goBackfromOverviewOn;
    /* access modifiers changed from: private */
    public volatile boolean goToOverviewOn;
    /* access modifiers changed from: private */
    public final android.os.Handler handler = new android.os.Handler(android.os.Looper.getMainLooper());
    /* access modifiers changed from: private */
    public com.navdy.hud.app.maps.here.HereMapAnimator hereMapAnimator;
    private final com.navdy.hud.app.maps.here.HereNavigationManager hereNavigationManager = com.navdy.hud.app.maps.here.HereNavigationManager.getInstance();
    private com.navdy.hud.app.ui.component.homescreen.HomeScreenView homeScreenView;
    private boolean init;
    private boolean initialZoom;
    /* access modifiers changed from: private */
    public boolean isFullAnimationSettingEnabled;
    /* access modifiers changed from: private */
    public long lastDynamicZoomTime;
    private double lastRecordedSpeed;
    /* access modifiers changed from: private */
    public com.navdy.service.library.events.preferences.LocalPreferences localPreferences;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.maps.here.HereMapController mapController;
    private boolean mapUIReady;
    /* access modifiers changed from: private */
    public volatile int nTimesAnimated;
    private com.navdy.hud.app.ui.component.homescreen.NavigationView navigationView;
    private com.navdy.hud.app.manager.SpeedManager speedManager = com.navdy.hud.app.manager.SpeedManager.getInstance();
    /* access modifiers changed from: private */
    public java.lang.Runnable unlockZoom = new com.navdy.hud.app.maps.here.HereMapCameraManager.Anon1();
    /* access modifiers changed from: private */
    public volatile boolean zoomActionOn;

    class Anon1 implements java.lang.Runnable {

        /* renamed from: com.navdy.hud.app.maps.here.HereMapCameraManager$Anon1$Anon1 reason: collision with other inner class name */
        class C0014Anon1 implements java.lang.Runnable {
            C0014Anon1() {
            }

            public void run() {
                float zoomLevel;
                if (!com.navdy.hud.app.maps.here.HereMapCameraManager.this.isManualZoom()) {
                    com.navdy.hud.app.maps.here.HereMapCameraManager.sLogger.v("unlocking dial zoom:auto");
                    com.navdy.hud.app.maps.here.HereMapCameraManager.this.zoomToDefault();
                    return;
                }
                com.navdy.hud.app.maps.here.HereMapCameraManager.sLogger.v("unlocking dial zoom:manual");
                if (com.navdy.hud.app.maps.here.HereMapCameraManager.this.isOverViewMapVisible()) {
                    zoomLevel = com.navdy.hud.app.maps.here.HereMapCameraManager.OVERVIEW_MAP_ZOOM_LEVEL;
                } else {
                    zoomLevel = (float) com.navdy.hud.app.maps.here.HereMapCameraManager.this.mapController.getZoomLevel();
                }
                com.navdy.hud.app.maps.here.HereMapCameraManager.this.localPreferences = new com.navdy.service.library.events.preferences.LocalPreferences.Builder(com.navdy.hud.app.maps.here.HereMapCameraManager.this.localPreferences).manualZoomLevel(java.lang.Float.valueOf(zoomLevel)).build();
                com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getDriverProfileManager().updateLocalPreferences(com.navdy.hud.app.maps.here.HereMapCameraManager.this.localPreferences);
                com.navdy.hud.app.maps.here.HereMapCameraManager.sLogger.v("unlocking dial zoom:manual pref saved");
            }
        }

        Anon1() {
        }

        public void run() {
            com.navdy.hud.app.maps.here.HereMapCameraManager.this.handler.removeCallbacks(com.navdy.hud.app.maps.here.HereMapCameraManager.this.easeOutDynamicZoomTilt);
            com.navdy.hud.app.maps.here.HereMapCameraManager.this.zoomActionOn = false;
            if (com.navdy.hud.app.maps.here.HereMapCameraManager.this.mapController.getState() == com.navdy.hud.app.maps.here.HereMapController.State.ROUTE_PICKER) {
                com.navdy.hud.app.maps.here.HereMapCameraManager.sLogger.v("unlocking: in route search, no-op");
            } else {
                com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.maps.here.HereMapCameraManager.Anon1.C0014Anon1(), 2);
            }
        }
    }

    class Anon2 implements java.lang.Runnable {
        Anon2() {
        }

        public void run() {
            com.navdy.service.library.task.TaskManager.getInstance().execute(com.navdy.hud.app.maps.here.HereMapCameraManager.this.easeOutDynamicZoomTiltBk, 16);
        }
    }

    class Anon3 implements java.lang.Runnable {
        Anon3() {
        }

        public void run() {
            boolean isSignificantZoomDiff;
            boolean isSignificantTiltDiff;
            if (com.navdy.hud.app.maps.here.HereMapCameraManager.this.zoomActionOn) {
                com.navdy.hud.app.maps.here.HereMapCameraManager.sLogger.v("easeout user zoom action is on");
                return;
            }
            if (java.lang.Math.abs(com.navdy.hud.app.maps.here.HereMapCameraManager.this.mapController.getZoomLevel() - com.navdy.hud.app.maps.here.HereMapCameraManager.this.currentDynamicZoom) >= 0.25d) {
                isSignificantZoomDiff = true;
            } else {
                isSignificantZoomDiff = false;
            }
            if (java.lang.Math.abs(com.navdy.hud.app.maps.here.HereMapCameraManager.this.mapController.getTilt() - com.navdy.hud.app.maps.here.HereMapCameraManager.this.currentDynamicTilt) > com.navdy.hud.app.maps.here.HereMapCameraManager.MIN_TILT_DIFFERENCE_TRIGGER) {
                isSignificantTiltDiff = true;
            } else {
                isSignificantTiltDiff = false;
            }
            if (isSignificantZoomDiff || isSignificantTiltDiff) {
                com.navdy.hud.app.maps.here.HereMapCameraManager.this.lastDynamicZoomTime = android.os.SystemClock.elapsedRealtime();
                com.navdy.hud.app.maps.here.HereMapCameraManager.this.animateCamera();
                com.navdy.hud.app.maps.here.HereMapCameraManager.sLogger.v("easeout zoom=" + isSignificantZoomDiff + " tilt=" + isSignificantTiltDiff + " current-zoom=" + com.navdy.hud.app.maps.here.HereMapCameraManager.this.currentDynamicZoom + " current-tilt=" + com.navdy.hud.app.maps.here.HereMapCameraManager.this.currentDynamicTilt);
                return;
            }
            com.navdy.hud.app.maps.here.HereMapCameraManager.sLogger.v("easeout not significant current-zoom=" + com.navdy.hud.app.maps.here.HereMapCameraManager.this.currentDynamicZoom + " current-tilt=" + com.navdy.hud.app.maps.here.HereMapCameraManager.this.currentDynamicTilt);
        }
    }

    class Anon4 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.hud.app.maps.MapEvents.DialMapZoom val$event;

        class Anon1 implements java.lang.Runnable {

            /* renamed from: com.navdy.hud.app.maps.here.HereMapCameraManager$Anon4$Anon1$Anon1 reason: collision with other inner class name */
            class C0015Anon1 implements java.lang.Runnable {
                C0015Anon1() {
                }

                public void run() {
                    com.navdy.hud.app.maps.here.HereMapCameraManager.sLogger.v("goBackfromOverview:complete");
                    com.navdy.hud.app.maps.here.HereMapCameraManager.this.goBackfromOverviewOn = false;
                }
            }

            Anon1() {
            }

            public void run() {
                com.navdy.hud.app.maps.here.HereMapCameraManager.this.goBackFromOverview(new com.navdy.hud.app.maps.here.HereMapCameraManager.Anon4.Anon1.C0015Anon1());
                com.navdy.hud.app.maps.here.HereMapCameraManager.this.handler.postDelayed(com.navdy.hud.app.maps.here.HereMapCameraManager.this.unlockZoom, com.navdy.hud.app.maps.here.HereMapCameraManager.ZOOM_LOCK_DURATION);
            }
        }

        class Anon2 implements java.lang.Runnable {
            final /* synthetic */ com.here.android.mpa.common.GeoCoordinate val$coordinate;

            class Anon1 implements java.lang.Runnable {
                Anon1() {
                }

                public void run() {
                    com.navdy.hud.app.maps.here.HereMapCameraManager.this.goToOverviewOn = false;
                    com.navdy.hud.app.maps.here.HereMapCameraManager.sLogger.v("goToOverviewOn:complete");
                }
            }

            Anon2(com.here.android.mpa.common.GeoCoordinate geoCoordinate) {
                this.val$coordinate = geoCoordinate;
            }

            public void run() {
                com.navdy.hud.app.maps.here.HereMapCameraManager.this.goToOverview(this.val$coordinate, new com.navdy.hud.app.maps.here.HereMapCameraManager.Anon4.Anon2.Anon1());
            }
        }

        Anon4(com.navdy.hud.app.maps.MapEvents.DialMapZoom dialMapZoom) {
            this.val$event = dialMapZoom;
        }

        public void run() {
            com.here.android.mpa.common.GeoCoordinate c;
            try {
                switch (com.navdy.hud.app.maps.here.HereMapCameraManager.Anon9.$SwitchMap$com$navdy$hud$app$maps$here$HereMapController$State[com.navdy.hud.app.maps.here.HereMapCameraManager.this.mapController.getState().ordinal()]) {
                    case 1:
                    case 2:
                        if (com.navdy.hud.app.maps.here.HereMapCameraManager.this.currentGeoPosition == null) {
                            c = com.navdy.hud.app.maps.here.HereMapsManager.getInstance().getLocationFixManager().getLastGeoCoordinate();
                        } else {
                            c = com.navdy.hud.app.maps.here.HereMapCameraManager.this.currentGeoPosition.getCoordinate();
                        }
                        if (c == null) {
                            com.navdy.hud.app.maps.here.HereMapCameraManager.sLogger.i("no location");
                            return;
                        }
                        com.here.android.mpa.common.GeoCoordinate coordinate = c;
                        if (!com.navdy.hud.app.maps.here.HereMapCameraManager.this.animationOn && !com.navdy.hud.app.maps.here.HereMapCameraManager.this.goBackfromOverviewOn && !com.navdy.hud.app.maps.here.HereMapCameraManager.this.goToOverviewOn && (!com.navdy.hud.app.maps.here.HereMapCameraManager.this.isOverViewMapVisible() || this.val$event.type != com.navdy.hud.app.maps.MapEvents.DialMapZoom.Type.OUT)) {
                            double currentMapZoom = com.navdy.hud.app.maps.here.HereMapCameraManager.this.mapController.getZoomLevel();
                            double zoomStep = 0.0d;
                            com.navdy.hud.app.maps.here.HereMapCameraManager.this.zoomActionOn = true;
                            com.navdy.hud.app.maps.here.HereMapCameraManager.this.handler.removeCallbacks(com.navdy.hud.app.maps.here.HereMapCameraManager.this.unlockZoom);
                            switch (com.navdy.hud.app.maps.here.HereMapCameraManager.Anon9.$SwitchMap$com$navdy$hud$app$maps$MapEvents$DialMapZoom$Type[this.val$event.type.ordinal()]) {
                                case 1:
                                    if (com.navdy.hud.app.maps.here.HereMapCameraManager.this.isOverViewMapVisible()) {
                                        com.navdy.hud.app.maps.here.HereMapCameraManager.this.goToOverviewOn = false;
                                        if (!com.navdy.hud.app.maps.here.HereMapCameraManager.this.goBackfromOverviewOn) {
                                            com.navdy.hud.app.maps.here.HereMapCameraManager.sLogger.v("goBackfromOverview:on");
                                            com.navdy.hud.app.maps.here.HereMapCameraManager.this.goBackfromOverviewOn = true;
                                            com.navdy.hud.app.maps.here.HereMapCameraManager.this.handler.post(new com.navdy.hud.app.maps.here.HereMapCameraManager.Anon4.Anon1());
                                            return;
                                        }
                                        return;
                                    } else if (!com.navdy.hud.app.maps.here.HereMapCameraManager.this.goBackfromOverviewOn) {
                                        zoomStep = 0.25d;
                                        break;
                                    } else {
                                        return;
                                    }
                                case 2:
                                    zoomStep = -0.25d;
                                    break;
                            }
                            double newZoomLevel = currentMapZoom + zoomStep;
                            if (currentMapZoom < 16.5d && newZoomLevel >= 16.5d) {
                                newZoomLevel = 16.5d;
                            }
                            boolean isInZoomRange = newZoomLevel <= 16.5d && newZoomLevel >= 12.0d;
                            boolean enteredOverviewRange = currentMapZoom + zoomStep < 12.0d;
                            if (isInZoomRange) {
                                long l1 = android.os.SystemClock.elapsedRealtime();
                                if (com.navdy.hud.app.maps.here.HereMapCameraManager.this.isFullAnimationSettingEnabled) {
                                    com.navdy.hud.app.maps.here.HereMapCameraManager.this.hereMapAnimator.setZoom(newZoomLevel);
                                } else {
                                    com.navdy.hud.app.maps.here.HereMapCameraManager.this.mapController.setCenter(coordinate, com.here.android.mpa.mapping.Map.Animation.NONE, newZoomLevel, -1.0f, -1.0f);
                                }
                                com.navdy.hud.app.maps.here.HereMapCameraManager.sLogger.v("zoomLevel:" + newZoomLevel + " time:" + (android.os.SystemClock.elapsedRealtime() - l1));
                            } else if (enteredOverviewRange) {
                                com.navdy.hud.app.maps.here.HereMapCameraManager.this.goBackfromOverviewOn = false;
                                com.navdy.hud.app.maps.here.HereMapCameraManager.this.goToOverviewOn = true;
                                com.navdy.hud.app.maps.here.HereMapCameraManager.sLogger.v("goToOverviewOn:on");
                                com.navdy.hud.app.maps.here.HereMapCameraManager.this.handler.post(new com.navdy.hud.app.maps.here.HereMapCameraManager.Anon4.Anon2(coordinate));
                                com.navdy.hud.app.maps.here.HereMapCameraManager.sLogger.v("zoomLevel: overview");
                            }
                            com.navdy.hud.app.maps.here.HereMapCameraManager.this.handler.postDelayed(com.navdy.hud.app.maps.here.HereMapCameraManager.this.unlockZoom, com.navdy.hud.app.maps.here.HereMapCameraManager.ZOOM_LOCK_DURATION);
                            return;
                        } else if (com.navdy.hud.app.maps.here.HereMapCameraManager.sLogger.isLoggable(2)) {
                            com.navdy.hud.app.maps.here.HereMapCameraManager.sLogger.v("onDialZoom no-op, animationOn: " + com.navdy.hud.app.maps.here.HereMapCameraManager.this.animationOn + " goBackfromOverviewOn:" + com.navdy.hud.app.maps.here.HereMapCameraManager.this.goBackfromOverviewOn + " goToOverviewOn:" + com.navdy.hud.app.maps.here.HereMapCameraManager.this.goToOverviewOn + " overviewMap on-out:" + (com.navdy.hud.app.maps.here.HereMapCameraManager.this.isOverViewMapVisible() && this.val$event.type == com.navdy.hud.app.maps.MapEvents.DialMapZoom.Type.OUT));
                            return;
                        } else {
                            return;
                        }
                    default:
                        return;
                }
            } catch (Throwable t) {
                com.navdy.hud.app.maps.here.HereMapCameraManager.sLogger.e("onDialZoom", t);
            }
            com.navdy.hud.app.maps.here.HereMapCameraManager.sLogger.e("onDialZoom", t);
        }
    }

    class Anon5 implements java.lang.Runnable {
        Anon5() {
        }

        public void run() {
            if (com.navdy.hud.app.maps.here.HereMapCameraManager.this.isFullAnimationSettingEnabled) {
                com.navdy.hud.app.maps.here.HereMapCameraManager.this.hereMapAnimator.setZoom(com.navdy.hud.app.maps.here.HereMapCameraManager.this.currentDynamicZoom);
                com.navdy.hud.app.maps.here.HereMapCameraManager.this.hereMapAnimator.setTilt(com.navdy.hud.app.maps.here.HereMapCameraManager.this.currentDynamicTilt);
                return;
            }
            com.navdy.hud.app.maps.here.HereMapCameraManager.this.nTimesAnimated = 0;
            com.navdy.hud.app.maps.here.HereMapCameraManager.this.animationOn = true;
            com.navdy.hud.app.maps.here.HereMapCameraManager.this.handler.post(com.navdy.hud.app.maps.here.HereMapCameraManager.this.getAnimateCameraRunnable((com.navdy.hud.app.maps.here.HereMapCameraManager.this.currentDynamicZoom - com.navdy.hud.app.maps.here.HereMapCameraManager.this.mapController.getZoomLevel()) / 5.0d, (com.navdy.hud.app.maps.here.HereMapCameraManager.this.currentDynamicTilt - com.navdy.hud.app.maps.here.HereMapCameraManager.this.mapController.getTilt()) / com.navdy.hud.app.maps.here.HereMapCameraManager.MIN_TILT_DIFFERENCE_TRIGGER));
        }
    }

    class Anon6 implements java.lang.Runnable {
        final /* synthetic */ java.lang.Runnable val$endAction;

        Anon6(java.lang.Runnable runnable) {
            this.val$endAction = runnable;
        }

        public void run() {
            if (this.val$endAction != null) {
                com.navdy.hud.app.maps.here.HereMapCameraManager.this.handler.post(this.val$endAction);
            }
        }
    }

    class Anon7 implements java.lang.Runnable {
        Anon7() {
        }

        public void run() {
            com.navdy.hud.app.maps.here.HereMapCameraManager.this.animateCamera();
        }
    }

    class Anon8 implements java.lang.Runnable {
        final /* synthetic */ float val$tiltStep;
        final /* synthetic */ double val$zoomStep;

        class Anon1 implements java.lang.Runnable {
            Anon1() {
            }

            public void run() {
                com.here.android.mpa.common.GeoCoordinate coordinate;
                if (com.navdy.hud.app.maps.here.HereMapCameraManager.this.currentGeoPosition == null) {
                    coordinate = com.navdy.hud.app.maps.here.HereMapCameraManager.this.mapController.getCenter();
                } else {
                    coordinate = com.navdy.hud.app.maps.here.HereMapCameraManager.this.currentGeoPosition.getCoordinate();
                }
                double newZoom = com.navdy.hud.app.maps.here.HereMapCameraManager.this.mapController.getZoomLevel() + com.navdy.hud.app.maps.here.HereMapCameraManager.Anon8.this.val$zoomStep;
                float newTilt = com.navdy.hud.app.maps.here.HereMapCameraManager.this.mapController.getTilt() + com.navdy.hud.app.maps.here.HereMapCameraManager.Anon8.this.val$tiltStep;
                if (newZoom > 16.5d) {
                    newZoom = 16.5d;
                }
                if (newTilt > com.navdy.hud.app.maps.here.HereMapCameraManager.HIGH_SPEED_TILT) {
                    newTilt = com.navdy.hud.app.maps.here.HereMapCameraManager.HIGH_SPEED_TILT;
                }
                com.navdy.hud.app.maps.here.HereMapCameraManager.this.mapController.setCenter(coordinate, com.here.android.mpa.mapping.Map.Animation.NONE, newZoom, -1.0f, newTilt);
                com.navdy.hud.app.maps.here.HereMapCameraManager.this.nTimesAnimated = com.navdy.hud.app.maps.here.HereMapCameraManager.this.nTimesAnimated + 1;
                if (com.navdy.hud.app.maps.here.HereMapCameraManager.this.nTimesAnimated == 5) {
                    com.navdy.hud.app.maps.here.HereMapCameraManager.this.animationOn = false;
                    com.navdy.hud.app.maps.here.HereMapCameraManager.this.nTimesAnimated = 0;
                    return;
                }
                com.navdy.hud.app.maps.here.HereMapCameraManager.this.handler.postDelayed(com.navdy.hud.app.maps.here.HereMapCameraManager.this.getAnimateCameraRunnable(com.navdy.hud.app.maps.here.HereMapCameraManager.Anon8.this.val$zoomStep, com.navdy.hud.app.maps.here.HereMapCameraManager.Anon8.this.val$tiltStep), 200);
            }
        }

        Anon8(double d, float f) {
            this.val$zoomStep = d;
            this.val$tiltStep = f;
        }

        public void run() {
            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.maps.here.HereMapCameraManager.Anon8.Anon1(), 17);
        }
    }

    static /* synthetic */ class Anon9 {
        static final /* synthetic */ int[] $SwitchMap$com$navdy$hud$app$maps$MapEvents$DialMapZoom$Type = new int[com.navdy.hud.app.maps.MapEvents.DialMapZoom.Type.values().length];
        static final /* synthetic */ int[] $SwitchMap$com$navdy$hud$app$maps$here$HereMapController$State = new int[com.navdy.hud.app.maps.here.HereMapController.State.values().length];

        static {
            try {
                $SwitchMap$com$navdy$hud$app$maps$MapEvents$DialMapZoom$Type[com.navdy.hud.app.maps.MapEvents.DialMapZoom.Type.IN.ordinal()] = 1;
            } catch (java.lang.NoSuchFieldError e) {
            }
            try {
                $SwitchMap$com$navdy$hud$app$maps$MapEvents$DialMapZoom$Type[com.navdy.hud.app.maps.MapEvents.DialMapZoom.Type.OUT.ordinal()] = 2;
            } catch (java.lang.NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$com$navdy$hud$app$maps$here$HereMapController$State[com.navdy.hud.app.maps.here.HereMapController.State.OVERVIEW.ordinal()] = 1;
            } catch (java.lang.NoSuchFieldError e3) {
            }
            try {
                $SwitchMap$com$navdy$hud$app$maps$here$HereMapController$State[com.navdy.hud.app.maps.here.HereMapController.State.AR_MODE.ordinal()] = 2;
            } catch (java.lang.NoSuchFieldError e4) {
            }
        }
    }

    public static com.navdy.hud.app.maps.here.HereMapCameraManager getInstance() {
        return sInstance;
    }

    private HereMapCameraManager() {
    }

    public synchronized void initialize(com.navdy.hud.app.maps.here.HereMapController mapController2, com.squareup.otto.Bus bus, com.navdy.hud.app.maps.here.HereMapAnimator hereMapAnimator2) {
        boolean z = true;
        synchronized (this) {
            if (!this.init) {
                this.mapController = mapController2;
                this.hereMapAnimator = hereMapAnimator2;
                if (hereMapAnimator2.getAnimationMode() != com.navdy.hud.app.maps.here.HereMapAnimator.AnimationMode.ZOOM_TILT_ANIMATION) {
                    z = false;
                }
                this.isFullAnimationSettingEnabled = z;
                com.navdy.hud.app.profile.DriverProfile profile = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile();
                this.localPreferences = profile.getLocalPreferences();
                sLogger.v("localPreferences [" + profile.getProfileName() + "] " + this.localPreferences);
                setZoom();
                this.currentDynamicZoom = 16.5d;
                this.currentDynamicTilt = 60.0f;
                bus.register(this);
                this.init = true;
            }
        }
    }

    public boolean setZoom() {
        if (!this.init) {
            return false;
        }
        if (this.currentGeoPosition == null || this.localPreferences == null) {
            return false;
        }
        if (!getNavigationView()) {
            return false;
        }
        com.navdy.hud.app.maps.here.HereMapController.State state = this.mapController.getState();
        if (isManualZoom()) {
            sLogger.v("setZoom: manual: " + state);
            double level = (double) this.localPreferences.manualZoomLevel.floatValue();
            if (level != 10000.0d) {
                sLogger.v("setZoom: normal zoom level: " + level);
                if (level < 12.0d || level > 16.5d) {
                    double prevLevel = level;
                    level = 16.5d;
                    sLogger.w("setZoom: normal zoom level changed from [" + prevLevel + "] to [" + 16.5d + "]");
                }
                if (this.mapController.getState() == com.navdy.hud.app.maps.here.HereMapController.State.OVERVIEW || this.mapController.getState() == com.navdy.hud.app.maps.here.HereMapController.State.TRANSITION) {
                    sLogger.v("setZoom: normal zoom level, showRouteMap");
                    this.navigationView.showRouteMap(this.currentGeoPosition, level, this.currentDynamicTilt);
                } else if (this.mapController.getState() == com.navdy.hud.app.maps.here.HereMapController.State.AR_MODE) {
                    this.mapController.setCenter(this.currentGeoPosition.getCoordinate(), com.here.android.mpa.mapping.Map.Animation.NONE, level, (float) this.currentGeoPosition.getHeading(), this.currentDynamicTilt);
                    sLogger.v("setZoom: normal zoom level, already in route map, setcenter");
                } else {
                    sLogger.v("setZoom: normal zoom level, incorrect state:" + state);
                }
            } else if (state == com.navdy.hud.app.maps.here.HereMapController.State.AR_MODE) {
                sLogger.v("setZoom: overview map zoom level, showOverviewMap");
                this.navigationView.showOverviewMap(this.hereNavigationManager.getCurrentRoute(), this.hereNavigationManager.getCurrentMapRoute(), this.currentGeoPosition.getCoordinate(), this.hereNavigationManager.hasArrived());
            } else {
                sLogger.v("setZoom: overview map zoom level, not in ar mode:" + state + ", set to OVERVIEW");
                this.mapController.setState(com.navdy.hud.app.maps.here.HereMapController.State.OVERVIEW);
                this.navigationView.showStartMarker();
            }
        } else {
            sLogger.v("setZoom: automatic: " + state);
            if (state == com.navdy.hud.app.maps.here.HereMapController.State.OVERVIEW || state == com.navdy.hud.app.maps.here.HereMapController.State.TRANSITION) {
                sLogger.v("setZoom:automatic showRouteMap");
                this.navigationView.showRouteMap(this.currentGeoPosition, this.currentDynamicZoom, this.currentDynamicTilt);
            } else if (this.mapController.getState() == com.navdy.hud.app.maps.here.HereMapController.State.AR_MODE) {
                sLogger.v("setZoom:automatic already in route map, setCenter");
                this.mapController.setCenter(this.currentGeoPosition.getCoordinate(), com.here.android.mpa.mapping.Map.Animation.NONE, this.currentDynamicZoom, (float) this.currentGeoPosition.getHeading(), this.currentDynamicTilt);
            } else {
                sLogger.v("setZoom:automatic incorrect state:" + state);
            }
        }
        return true;
    }

    public void onGeoPositionChange(com.here.android.mpa.common.GeoPosition geoPosition) {
        double speed;
        this.currentGeoPosition = geoPosition;
        this.currentGeoCoordinate = geoPosition.getCoordinate();
        if (this.localPreferences.manualZoom == null || !this.localPreferences.manualZoom.booleanValue()) {
            double speed2 = (double) this.speedManager.getObdSpeed();
            if (speed2 != -1.0d) {
                speed = (double) com.navdy.hud.app.manager.SpeedManager.convert(speed2, this.speedManager.getSpeedUnit(), com.navdy.hud.app.manager.SpeedManager.SpeedUnit.METERS_PER_SECOND);
            } else {
                speed = this.currentGeoPosition.getSpeed();
            }
            if (speed != this.lastRecordedSpeed) {
                double lastSpeed = this.lastRecordedSpeed;
                boolean isSignificantSpeedDiff = java.lang.Math.abs(speed - this.lastRecordedSpeed) >= MIN_SPEED_DIFFERENCE;
                this.lastRecordedSpeed = speed;
                if (isSignificantSpeedDiff) {
                    this.handler.removeCallbacks(this.easeOutDynamicZoomTilt);
                    this.currentDynamicZoom = getDynamicZoom(speed);
                    this.currentDynamicTilt = getDynamicTilt(speed);
                    if (!this.zoomActionOn) {
                        boolean isSignificantZoomDiff = java.lang.Math.abs(this.mapController.getZoomLevel() - this.currentDynamicZoom) >= 0.25d;
                        boolean isSignificantTiltDiff = java.lang.Math.abs(this.mapController.getTilt() - this.currentDynamicTilt) > MIN_TILT_DIFFERENCE_TRIGGER;
                        if (isSignificantZoomDiff || isSignificantTiltDiff) {
                            long lastZoomInterval = android.os.SystemClock.elapsedRealtime() - this.lastDynamicZoomTime;
                            if (lastZoomInterval < ZOOM_LOCK_DURATION) {
                                long timeLeft = ZOOM_LOCK_DURATION - lastZoomInterval;
                                this.handler.postDelayed(this.easeOutDynamicZoomTilt, timeLeft);
                                if (sLogger.isLoggable(2)) {
                                    sLogger.v("significant too early =" + lastZoomInterval + " scheduled =" + timeLeft);
                                    return;
                                }
                                return;
                            }
                            sLogger.v("significant zoom=" + isSignificantZoomDiff + " tilt=" + isSignificantTiltDiff + " current-zoom=" + this.currentDynamicZoom + " current-tilt=" + this.currentDynamicTilt + " speed = " + speed + " lastSpeed=" + lastSpeed);
                            this.lastDynamicZoomTime = android.os.SystemClock.elapsedRealtime();
                            animateCamera();
                        }
                    }
                }
            } else if (sLogger.isLoggable(2)) {
                sLogger.v("speed not changed :" + speed);
            }
        }
    }

    @com.squareup.otto.Subscribe
    public void onLocalPreferencesChanged(com.navdy.service.library.events.preferences.LocalPreferences preferences) {
        if (preferences.equals(this.localPreferences)) {
            sLogger.v("localPref not changed:" + preferences);
            return;
        }
        sLogger.v("localPref changed:" + preferences);
        this.localPreferences = preferences;
        setZoom();
    }

    @com.squareup.otto.Subscribe
    public void onDriverProfileChanged(com.navdy.hud.app.event.DriverProfileChanged event) {
        com.navdy.hud.app.profile.DriverProfile profile = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile();
        this.localPreferences = profile.getLocalPreferences();
        sLogger.v("driver changed[" + profile.getProfileName() + "] " + this.localPreferences);
        setZoom();
    }

    @com.squareup.otto.Subscribe
    public void onLocationFixChangeEvent(com.navdy.hud.app.maps.MapEvents.LocationFix event) {
        if (this.init && this.mapUIReady) {
            setInitialZoom();
        }
    }

    @com.squareup.otto.Subscribe
    public void onMapUIReady(com.navdy.hud.app.maps.MapEvents.MapUIReady event) {
        this.mapUIReady = true;
        setInitialZoom();
    }

    @com.squareup.otto.Subscribe
    public void onDialZoom(com.navdy.hud.app.maps.MapEvents.DialMapZoom event) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.maps.here.HereMapCameraManager.Anon4(event), 16);
    }

    private void setInitialZoom() {
        if (!this.initialZoom) {
            sLogger.v("setInitialZoom:" + this.currentGeoPosition);
            if (this.currentGeoPosition == null) {
                com.here.android.mpa.common.GeoCoordinate coordinate = com.navdy.hud.app.maps.here.HereMapsManager.getInstance().getLocationFixManager().getLastGeoCoordinate();
                if (coordinate != null) {
                    this.currentGeoPosition = new com.here.android.mpa.common.GeoPosition(coordinate);
                    this.currentGeoCoordinate = coordinate;
                } else {
                    return;
                }
            }
            this.initialZoom = setZoom();
            sLogger.v("setInitialZoom:" + this.initialZoom);
        }
    }

    /* access modifiers changed from: private */
    public void animateCamera() {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.maps.here.HereMapCameraManager.Anon5(), 2);
    }

    /* access modifiers changed from: private */
    public void goToOverview(com.here.android.mpa.common.GeoCoordinate coordinate, java.lang.Runnable endAction) {
        if (getNavigationView()) {
            if (this.hereNavigationManager.getCurrentRoute() != null) {
                this.navigationView.animateToOverview(coordinate, this.hereNavigationManager.getCurrentRoute(), this.hereNavigationManager.getCurrentMapRoute(), endAction, this.hereNavigationManager.hasArrived());
                return;
            }
            this.navigationView.animateToOverview(coordinate, null, null, endAction, false);
        }
    }

    /* access modifiers changed from: private */
    public void goBackFromOverview(java.lang.Runnable endAction) {
        if (getNavigationView()) {
            this.navigationView.animateBackfromOverview(new com.navdy.hud.app.maps.here.HereMapCameraManager.Anon6(endAction));
        }
    }

    private double getDynamicZoom(double speed) {
        if (speed <= LOW_SPEED_THRESHOLD) {
            return 16.5d;
        }
        if (speed > LOW_SPEED_THRESHOLD && speed <= MEDIUM_SPEED_THRESHOLD) {
            return getLinearMapSection(speed, LOW_SPEED_THRESHOLD, MEDIUM_SPEED_THRESHOLD, 16.5d, MEDIUM_SPEED_ZOOM);
        }
        if (speed > MEDIUM_SPEED_THRESHOLD && speed <= HIGH_SPEED_THRESHOLD) {
            return getLinearMapSection(speed, MEDIUM_SPEED_THRESHOLD, HIGH_SPEED_THRESHOLD, MEDIUM_SPEED_ZOOM, HIGH_SPEED_ZOOM);
        }
        if (speed <= HIGH_SPEED_THRESHOLD || speed > VERY_HIGH_SPEED_THRESHOLD) {
            return VERY_HIGH_SPEED_ZOOM;
        }
        return getLinearMapSection(speed, HIGH_SPEED_THRESHOLD, VERY_HIGH_SPEED_THRESHOLD, HIGH_SPEED_ZOOM, VERY_HIGH_SPEED_ZOOM);
    }

    private float getDynamicTilt(double speed) {
        if (speed <= LOW_SPEED_THRESHOLD) {
            return 60.0f;
        }
        if (speed <= LOW_SPEED_THRESHOLD || speed > HIGH_SPEED_THRESHOLD) {
            return HIGH_SPEED_TILT;
        }
        return (float) getLinearMapSection(speed, LOW_SPEED_THRESHOLD, HIGH_SPEED_THRESHOLD, 60.0d, 70.0d);
    }

    private double getLinearMapSection(double x, double xStart, double xEnd, double yStart, double yEnd) {
        return (((x - xStart) * (yEnd - yStart)) / (xEnd - xStart)) + yStart;
    }

    /* access modifiers changed from: private */
    public void zoomToDefault() {
        if (this.init) {
            if (isManualZoom()) {
                sLogger.v("zoomToDefault: manual zoom enabled");
            } else if (!getNavigationView()) {
                sLogger.v("zoomToDefault: no view");
            } else if (this.navigationView.isOverviewMapMode()) {
                goBackFromOverview(new com.navdy.hud.app.maps.here.HereMapCameraManager.Anon7());
            } else {
                animateCamera();
            }
        }
    }

    public boolean isManualZoom() {
        return this.localPreferences != null && java.lang.Boolean.TRUE.equals(this.localPreferences.manualZoom);
    }

    public boolean isOverviewZoomLevel() {
        return isManualZoom() && this.localPreferences.manualZoomLevel.floatValue() == OVERVIEW_MAP_ZOOM_LEVEL;
    }

    private boolean getNavigationView() {
        if (this.navigationView != null) {
            return true;
        }
        com.navdy.hud.app.ui.framework.UIStateManager uiStateManager = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getUiStateManager();
        this.navigationView = uiStateManager.getNavigationView();
        this.homeScreenView = uiStateManager.getHomescreenView();
        if (this.navigationView == null) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: private */
    public boolean isOverViewMapVisible() {
        return getNavigationView() && this.navigationView.isOverviewMapMode();
    }

    /* access modifiers changed from: private */
    public java.lang.Runnable getAnimateCameraRunnable(double zoomStep, float tiltStep) {
        return new com.navdy.hud.app.maps.here.HereMapCameraManager.Anon8(zoomStep, tiltStep);
    }

    public com.here.android.mpa.common.GeoPosition getLastGeoPosition() {
        return this.currentGeoPosition;
    }

    public com.here.android.mpa.common.GeoCoordinate getLastGeoCoordinate() {
        return this.currentGeoCoordinate;
    }

    public float getLastTilt() {
        return this.currentDynamicTilt;
    }

    public double getLastZoom() {
        if (!isManualZoom()) {
            return this.currentDynamicZoom;
        }
        if (this.localPreferences.manualZoomLevel.floatValue() == OVERVIEW_MAP_ZOOM_LEVEL) {
            return 12.0d;
        }
        return (double) this.localPreferences.manualZoomLevel.floatValue();
    }
}
