package com.navdy.hud.app.maps.here;

class VoiceSkinsConfigurator {
    private static final com.navdy.hud.app.util.PackagedResource resource = new com.navdy.hud.app.util.PackagedResource("voiceskins", com.navdy.hud.app.storage.PathManager.getInstance().getHereVoiceSkinsPath());
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.maps.here.VoiceSkinsConfigurator.class);

    VoiceSkinsConfigurator() {
    }

    static void updateVoiceSkins() {
        try {
            android.content.Context context = com.navdy.hud.app.HudApplication.getAppContext();
            com.navdy.service.library.util.IOUtils.checkIntegrity(context, com.navdy.hud.app.storage.PathManager.getInstance().getHereVoiceSkinsPath(), com.navdy.hud.app.R.raw.here_voices_integrity);
            sLogger.i("Voice skins update: starting");
            long l1 = android.os.SystemClock.elapsedRealtime();
            resource.updateFromResources(context, com.navdy.hud.app.R.raw.here_voices, com.navdy.hud.app.R.raw.here_voices_md5, true);
            sLogger.i("Voice skins update: time took " + (android.os.SystemClock.elapsedRealtime() - l1) + " ms");
        } catch (Throwable throwable) {
            sLogger.e("Voice skins error", throwable);
        }
    }
}
