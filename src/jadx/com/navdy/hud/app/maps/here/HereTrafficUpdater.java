package com.navdy.hud.app.maps.here;

public class HereTrafficUpdater {
    private static final int MAX_DISTANCE_NOTIFICATION_METERS = 5000;
    private static final int MIN_DISTANCE_NOTIFICATION_METERS = 200;
    private static final int MIN_JAM_DURATION_SECS = 180;
    private static final int ONE_MINUTE_SECS = 60;
    private static final long REFRESH_INTERVAL_MILLIS = 30000;
    public static final boolean TRAFFIC_DEBUG_ENABLED = false;
    /* access modifiers changed from: private */
    public final com.squareup.otto.Bus bus;
    private com.here.android.mpa.mapping.TrafficEvent currentEvent;
    private int currentRemainingTimeInJam;
    private com.here.android.mpa.guidance.TrafficUpdater.RequestInfo currentRequestInfo;
    /* access modifiers changed from: private */
    public com.here.android.mpa.routing.Route currentRoute;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.maps.here.HereTrafficUpdater.State currentState;
    private java.lang.Runnable dismissEtaUpdate = new com.navdy.hud.app.maps.here.HereTrafficUpdater.Anon2();
    private final android.os.Handler handler;
    /* access modifiers changed from: private */
    public final com.navdy.service.library.log.Logger logger;
    private final com.here.android.mpa.guidance.NavigationManager navigationManager;
    /* access modifiers changed from: private */
    public com.here.android.mpa.guidance.TrafficUpdater.GetEventsListener onGetEventsListener = new com.navdy.hud.app.maps.here.HereTrafficUpdater.Anon4();
    private com.here.android.mpa.guidance.TrafficUpdater.Listener onRequestListener = new com.navdy.hud.app.maps.here.HereTrafficUpdater.Anon3();
    private java.lang.Runnable refreshRunnable = new com.navdy.hud.app.maps.here.HereTrafficUpdater.Anon1();
    /* access modifiers changed from: private */
    public final java.lang.String tag;
    /* access modifiers changed from: private */
    public final com.here.android.mpa.guidance.TrafficUpdater trafficUpdater;
    private final boolean verbose;

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            com.navdy.hud.app.maps.here.HereTrafficUpdater.this.startRequest();
        }
    }

    class Anon2 implements java.lang.Runnable {
        Anon2() {
        }

        public void run() {
            com.navdy.hud.app.maps.here.HereTrafficUpdater.this.bus.post(new com.navdy.hud.app.maps.MapEvents.TrafficDelayDismissEvent());
        }
    }

    class Anon3 implements com.here.android.mpa.guidance.TrafficUpdater.Listener {
        Anon3() {
        }

        public void onStatusChanged(com.here.android.mpa.guidance.TrafficUpdater.RequestState requestState) {
            if (com.navdy.hud.app.maps.here.HereTrafficUpdater.this.currentRoute == null) {
                com.navdy.hud.app.maps.here.HereTrafficUpdater.this.logger.w(com.navdy.hud.app.maps.here.HereTrafficUpdater.this.tag + "onRequestListener triggered and currentRoute is null");
            } else if (requestState == com.here.android.mpa.guidance.TrafficUpdater.RequestState.DONE) {
                com.navdy.hud.app.maps.here.HereTrafficUpdater.this.trafficUpdater.getEvents(com.navdy.hud.app.maps.here.HereTrafficUpdater.this.currentRoute, com.navdy.hud.app.maps.here.HereTrafficUpdater.this.onGetEventsListener);
            } else {
                com.navdy.hud.app.maps.here.HereTrafficUpdater.this.logger.i(com.navdy.hud.app.maps.here.HereTrafficUpdater.this.tag + " error on completion of TrafficUpdater.request");
                com.navdy.hud.app.maps.here.HereTrafficUpdater.this.refresh();
            }
        }
    }

    class Anon4 implements com.here.android.mpa.guidance.TrafficUpdater.GetEventsListener {

        class Anon1 implements java.lang.Runnable {
            final /* synthetic */ java.util.List val$list;

            Anon1(java.util.List list) {
                this.val$list = list;
            }

            public void run() {
                com.navdy.hud.app.maps.here.HereTrafficUpdater.this.processEvents(com.navdy.hud.app.maps.here.HereTrafficUpdater.this.currentRoute, this.val$list);
            }
        }

        Anon4() {
        }

        public void onComplete(java.util.List<com.here.android.mpa.mapping.TrafficEvent> list, com.here.android.mpa.guidance.TrafficUpdater.Error error) {
            if (com.navdy.hud.app.maps.here.HereTrafficUpdater.this.currentRoute == null) {
                com.navdy.hud.app.maps.here.HereTrafficUpdater.this.logger.w(com.navdy.hud.app.maps.here.HereTrafficUpdater.this.tag + "onGetEventsListener triggered and currentRoute is null");
                return;
            }
            if (error != com.here.android.mpa.guidance.TrafficUpdater.Error.NONE) {
                com.navdy.hud.app.maps.here.HereTrafficUpdater.this.logger.i(com.navdy.hud.app.maps.here.HereTrafficUpdater.this.tag + " error " + error.name() + " while getting TrafficEvents.");
            } else if (com.navdy.hud.app.maps.here.HereTrafficUpdater.this.currentState == com.navdy.hud.app.maps.here.HereTrafficUpdater.State.ACTIVE || com.navdy.hud.app.maps.here.HereTrafficUpdater.this.currentState == com.navdy.hud.app.maps.here.HereTrafficUpdater.State.TRACK_DISTANCE) {
                com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.maps.here.HereTrafficUpdater.Anon4.Anon1(list), 2);
            }
            com.navdy.hud.app.maps.here.HereTrafficUpdater.this.refresh();
        }
    }

    static /* synthetic */ class Anon5 {
        static final /* synthetic */ int[] $SwitchMap$com$here$android$mpa$guidance$TrafficUpdater$Error = new int[com.here.android.mpa.guidance.TrafficUpdater.Error.values().length];

        static {
            try {
                $SwitchMap$com$here$android$mpa$guidance$TrafficUpdater$Error[com.here.android.mpa.guidance.TrafficUpdater.Error.NONE.ordinal()] = 1;
            } catch (java.lang.NoSuchFieldError e) {
            }
            try {
                $SwitchMap$com$here$android$mpa$guidance$TrafficUpdater$Error[com.here.android.mpa.guidance.TrafficUpdater.Error.UNKNOWN.ordinal()] = 2;
            } catch (java.lang.NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$com$here$android$mpa$guidance$TrafficUpdater$Error[com.here.android.mpa.guidance.TrafficUpdater.Error.OUT_OF_MEMORY.ordinal()] = 3;
            } catch (java.lang.NoSuchFieldError e3) {
            }
            try {
                $SwitchMap$com$here$android$mpa$guidance$TrafficUpdater$Error[com.here.android.mpa.guidance.TrafficUpdater.Error.REQUEST_FAILED.ordinal()] = 4;
            } catch (java.lang.NoSuchFieldError e4) {
            }
        }
    }

    private class DistanceTrackEvent {
        public final com.here.android.mpa.routing.Route route;
        public final com.here.android.mpa.mapping.TrafficEvent trafficEvent;

        public DistanceTrackEvent(com.here.android.mpa.routing.Route route2, com.here.android.mpa.mapping.TrafficEvent trafficEvent2) {
            this.route = route2;
            this.trafficEvent = trafficEvent2;
        }
    }

    private class JamTrackEvent {
        public final int remainingTimeInJam;
        public final com.here.android.mpa.routing.Route route;
        public final com.here.android.mpa.mapping.TrafficEvent trafficEvent;

        public JamTrackEvent(com.here.android.mpa.routing.Route route2, com.here.android.mpa.mapping.TrafficEvent trafficEvent2, int remainingTimeInJam2) {
            this.route = route2;
            this.trafficEvent = trafficEvent2;
            this.remainingTimeInJam = remainingTimeInJam2;
        }
    }

    public enum State {
        STOPPED,
        ACTIVE,
        TRACK_DISTANCE,
        TRACK_JAM
    }

    private class TrackDismissEvent {
        public final com.here.android.mpa.routing.Route route;

        public TrackDismissEvent(com.here.android.mpa.routing.Route route2) {
            this.route = route2;
        }
    }

    @com.squareup.otto.Subscribe
    public void onTrackingUpdate(com.navdy.hud.app.maps.here.HerePositionUpdateListener.MapMatchEvent mapMatchEvent) {
        try {
            if (this.currentState == com.navdy.hud.app.maps.here.HereTrafficUpdater.State.TRACK_DISTANCE) {
                onDistanceTrackingUpdate(mapMatchEvent);
            } else if (this.currentState == com.navdy.hud.app.maps.here.HereTrafficUpdater.State.TRACK_JAM) {
                onJamTrackingUpdate();
            }
        } catch (Throwable t) {
            this.logger.e(t);
            com.navdy.hud.app.util.CrashReporter.getInstance().reportNonFatalException(t);
        }
    }

    @com.squareup.otto.Subscribe
    public void onDistanceTrackEvent(com.navdy.hud.app.maps.here.HereTrafficUpdater.DistanceTrackEvent event) {
        if (isCurrentRoute(event.route)) {
            trackEvent(event.trafficEvent);
        }
    }

    @com.squareup.otto.Subscribe
    public void onJamTrackEvent(com.navdy.hud.app.maps.here.HereTrafficUpdater.JamTrackEvent event) {
        if (isCurrentRoute(event.route)) {
            this.currentEvent = event.trafficEvent;
            trackJam(event.remainingTimeInJam);
        }
    }

    @com.squareup.otto.Subscribe
    public void onTrackDismissEvent(com.navdy.hud.app.maps.here.HereTrafficUpdater.TrackDismissEvent event) {
        if (isCurrentRoute(event.route)) {
            changeState(com.navdy.hud.app.maps.here.HereTrafficUpdater.State.ACTIVE);
        }
    }

    public HereTrafficUpdater(com.navdy.service.library.log.Logger logger2, java.lang.String tag2, boolean verbose2, com.here.android.mpa.guidance.NavigationManager navigationManager2, com.squareup.otto.Bus bus2) {
        this.logger = logger2;
        this.tag = tag2;
        this.verbose = verbose2;
        this.navigationManager = navigationManager2;
        this.bus = bus2;
        this.trafficUpdater = com.here.android.mpa.guidance.TrafficUpdater.getInstance();
        this.trafficUpdater.enableUpdate(true);
        this.handler = new android.os.Handler();
        this.currentState = com.navdy.hud.app.maps.here.HereTrafficUpdater.State.STOPPED;
        this.bus.register(this);
    }

    public void activate(com.here.android.mpa.routing.Route newRoute) {
        this.currentRoute = newRoute;
        changeState(com.navdy.hud.app.maps.here.HereTrafficUpdater.State.ACTIVE);
    }

    public void stop() {
        changeState(com.navdy.hud.app.maps.here.HereTrafficUpdater.State.STOPPED);
    }

    private void trackEvent(com.here.android.mpa.mapping.TrafficEvent newTrafficEvent) {
        this.currentEvent = newTrafficEvent;
        changeState(com.navdy.hud.app.maps.here.HereTrafficUpdater.State.TRACK_DISTANCE);
    }

    private void trackJam(int newRemainingTimeInJam) {
        this.currentRemainingTimeInJam = newRemainingTimeInJam;
        changeState(com.navdy.hud.app.maps.here.HereTrafficUpdater.State.TRACK_JAM);
    }

    private void changeState(com.navdy.hud.app.maps.here.HereTrafficUpdater.State newState) {
        if (newState != this.currentState) {
            if (newState == com.navdy.hud.app.maps.here.HereTrafficUpdater.State.STOPPED) {
                if (this.currentState == com.navdy.hud.app.maps.here.HereTrafficUpdater.State.TRACK_DISTANCE) {
                    stopTrackingDistance();
                } else if (this.currentState == com.navdy.hud.app.maps.here.HereTrafficUpdater.State.TRACK_JAM) {
                    stopTrackingJamTime();
                }
                stopUpdates();
            } else if (newState == com.navdy.hud.app.maps.here.HereTrafficUpdater.State.ACTIVE) {
                if (this.currentState == com.navdy.hud.app.maps.here.HereTrafficUpdater.State.STOPPED) {
                    startUpdates();
                } else if (this.currentState == com.navdy.hud.app.maps.here.HereTrafficUpdater.State.TRACK_DISTANCE) {
                    stopTrackingDistance();
                } else if (this.currentState == com.navdy.hud.app.maps.here.HereTrafficUpdater.State.TRACK_JAM) {
                    stopTrackingJamTime();
                }
            } else if (newState == com.navdy.hud.app.maps.here.HereTrafficUpdater.State.TRACK_DISTANCE) {
                if (this.currentState == com.navdy.hud.app.maps.here.HereTrafficUpdater.State.ACTIVE) {
                    startTrackingDistance();
                } else {
                    this.logger.w(this.tag + " invalid state change from " + this.currentState.name() + " to " + newState.name());
                    return;
                }
            } else if (newState == com.navdy.hud.app.maps.here.HereTrafficUpdater.State.TRACK_JAM) {
                if (this.currentState == com.navdy.hud.app.maps.here.HereTrafficUpdater.State.ACTIVE) {
                    startTrackingJamTime();
                } else if (this.currentState == com.navdy.hud.app.maps.here.HereTrafficUpdater.State.TRACK_DISTANCE) {
                    stopTrackingDistance();
                    startTrackingJamTime();
                } else {
                    this.logger.w(this.tag + " invalid state change from " + this.currentState.name() + " to " + newState.name());
                    return;
                }
            }
            this.currentState = newState;
        }
    }

    /* access modifiers changed from: private */
    public void processEvents(com.here.android.mpa.routing.Route route, java.util.List<com.here.android.mpa.mapping.TrafficEvent> events) {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        com.here.android.mpa.mapping.TrafficEvent firstEvent = null;
        com.here.android.mpa.mapping.TrafficEvent backupEvent = null;
        java.util.Map<java.lang.String, com.here.android.mpa.mapping.TrafficEvent> roadElementIdToTrafficEventMap = new java.util.HashMap<>();
        for (com.here.android.mpa.mapping.TrafficEvent event : events) {
            java.util.List<com.here.android.mpa.common.RoadElement> affectedRoadElements = event.getAffectedRoadElements();
            if (affectedRoadElements.size() > 0) {
                roadElementIdToTrafficEventMap.put(((com.here.android.mpa.common.RoadElement) affectedRoadElements.get(0)).getIdentifier().toString(), event);
            }
        }
        int distanceToLocation = 0;
        java.util.Iterator it = route.getRouteElementsFromLength((int) this.navigationManager.getElapsedDistance(), route.getLength()).getElements().iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            com.here.android.mpa.common.RoadElement roadElement = ((com.here.android.mpa.routing.RouteElement) it.next()).getRoadElement();
            java.lang.String roadId = roadElement.getIdentifier().toString();
            this.logger.v(this.tag + "route element id=" + roadId);
            if (roadElementIdToTrafficEventMap.containsKey(roadId)) {
                com.here.android.mpa.mapping.TrafficEvent event2 = (com.here.android.mpa.mapping.TrafficEvent) roadElementIdToTrafficEventMap.get(roadId);
                boolean isVerySevereEvent = event2.getSeverity().getValue() > com.here.android.mpa.mapping.TrafficEvent.Severity.HIGH.getValue();
                boolean isSevereEvent = event2.getSeverity().getValue() > com.here.android.mpa.mapping.TrafficEvent.Severity.NORMAL.getValue();
                this.logger.v(this.tag + "event inside route: isVerySevereEvent=" + isVerySevereEvent + "; isSevereEvent=" + isSevereEvent + "; distanceToLocation=" + distanceToLocation);
                if (!isVerySevereEvent || distanceToLocation >= 200) {
                    if (isVerySevereEvent && distanceToLocation > 200) {
                        firstEvent = event2;
                        break;
                    } else if (isSevereEvent && backupEvent == null && distanceToLocation > 200) {
                        backupEvent = event2;
                    }
                } else {
                    int remainingTimeInJam = getRemainingTimeInJam(route, event2);
                    this.logger.v(this.tag + "triggering jam, remaining time=" + remainingTimeInJam + " sec");
                    if (remainingTimeInJam >= 180) {
                        com.squareup.otto.Bus bus2 = this.bus;
                        com.navdy.hud.app.maps.here.HereTrafficUpdater.JamTrackEvent jamTrackEvent = new com.navdy.hud.app.maps.here.HereTrafficUpdater.JamTrackEvent(route, event2, remainingTimeInJam);
                        bus2.post(jamTrackEvent);
                        return;
                    }
                }
            }
            distanceToLocation = (int) (((double) distanceToLocation) + roadElement.getGeometryLength());
            if (distanceToLocation > 5000) {
                break;
            }
        }
        if (firstEvent != null) {
            com.squareup.otto.Bus bus3 = this.bus;
            com.navdy.hud.app.maps.here.HereTrafficUpdater.DistanceTrackEvent distanceTrackEvent = new com.navdy.hud.app.maps.here.HereTrafficUpdater.DistanceTrackEvent(route, firstEvent);
            bus3.post(distanceTrackEvent);
        } else if (backupEvent != null) {
            com.squareup.otto.Bus bus4 = this.bus;
            com.navdy.hud.app.maps.here.HereTrafficUpdater.DistanceTrackEvent distanceTrackEvent2 = new com.navdy.hud.app.maps.here.HereTrafficUpdater.DistanceTrackEvent(route, backupEvent);
            bus4.post(distanceTrackEvent2);
        } else {
            com.squareup.otto.Bus bus5 = this.bus;
            com.navdy.hud.app.maps.here.HereTrafficUpdater.TrackDismissEvent trackDismissEvent = new com.navdy.hud.app.maps.here.HereTrafficUpdater.TrackDismissEvent(route);
            bus5.post(trackDismissEvent);
        }
    }

    private boolean isCurrentRoute(com.here.android.mpa.routing.Route route) {
        return route.equals(this.currentRoute);
    }

    private int getRemainingTimeInJam(com.here.android.mpa.routing.Route route, com.here.android.mpa.mapping.TrafficEvent event) {
        double remainingTime = 0.0d;
        java.util.List<com.here.android.mpa.common.RoadElement> roadElements = event.getAffectedRoadElements();
        com.here.android.mpa.common.RoadElement lastRoadElementInEvent = (com.here.android.mpa.common.RoadElement) roadElements.get(roadElements.size() - 1);
        com.here.android.mpa.routing.RouteElements remainingRouteElementsInRoute = route.getRouteElementsFromLength((int) this.navigationManager.getElapsedDistance(), route.getLength());
        java.lang.String lastRoadElementInEventId = lastRoadElementInEvent.getIdentifier().toString();
        boolean foundRoadElement = false;
        java.util.Iterator it = remainingRouteElementsInRoute.getElements().iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            com.here.android.mpa.common.RoadElement roadElement = ((com.here.android.mpa.routing.RouteElement) it.next()).getRoadElement();
            if (roadElement.getIdentifier().toString().equals(lastRoadElementInEventId)) {
                foundRoadElement = true;
                break;
            }
            remainingTime += roadElement.getGeometryLength() / ((double) roadElement.getDefaultSpeed());
        }
        if (foundRoadElement) {
            return (int) remainingTime;
        }
        return 0;
    }

    private com.navdy.hud.app.maps.MapEvents.LiveTrafficEvent transformToLiveTrafficEvent(com.here.android.mpa.mapping.TrafficEvent event) {
        com.navdy.hud.app.maps.MapEvents.LiveTrafficEvent.Severity severity;
        com.navdy.hud.app.maps.MapEvents.LiveTrafficEvent.Type type = event.isFlow() ? com.navdy.hud.app.maps.MapEvents.LiveTrafficEvent.Type.CONGESTION : com.navdy.hud.app.maps.MapEvents.LiveTrafficEvent.Type.INCIDENT;
        if (event.getSeverity().getValue() > com.here.android.mpa.mapping.TrafficEvent.Severity.HIGH.getValue()) {
            severity = com.navdy.hud.app.maps.MapEvents.LiveTrafficEvent.Severity.VERY_HIGH;
        } else {
            severity = com.navdy.hud.app.maps.MapEvents.LiveTrafficEvent.Severity.HIGH;
        }
        return new com.navdy.hud.app.maps.MapEvents.LiveTrafficEvent(type, severity);
    }

    /* access modifiers changed from: private */
    public void startRequest() {
        if (this.currentRoute == null) {
            this.logger.w(this.tag + "startRequest: currentRoute must not be null.");
            return;
        }
        this.currentRequestInfo = this.trafficUpdater.request(this.currentRoute, this.onRequestListener);
        switch (com.navdy.hud.app.maps.here.HereTrafficUpdater.Anon5.$SwitchMap$com$here$android$mpa$guidance$TrafficUpdater$Error[this.currentRequestInfo.getError().ordinal()]) {
            case 1:
                return;
            case 2:
            case 3:
            case 4:
                this.logger.i(this.tag + " recovering from error before TrafficUpdater.request");
                refresh();
                return;
            default:
                this.logger.i(this.tag + " got error before TrafficUpdater.request. Invalid/malformed request.");
                changeState(com.navdy.hud.app.maps.here.HereTrafficUpdater.State.STOPPED);
                return;
        }
    }

    private void cancelRequest() {
        if (this.currentRequestInfo == null) {
            this.logger.w(this.tag + "cancelRequest: currentRequestInfo must not be null.");
            return;
        }
        if (this.currentRequestInfo.getError() == com.here.android.mpa.guidance.TrafficUpdater.Error.NONE) {
            this.trafficUpdater.cancelRequest(this.currentRequestInfo.getRequestId());
        }
        this.currentRequestInfo = null;
    }

    /* access modifiers changed from: private */
    public void refresh() {
        this.logger.v(this.tag + "posting refresh in 30 sec...");
        cancelRequest();
        this.handler.removeCallbacks(this.refreshRunnable);
        this.handler.postDelayed(this.refreshRunnable, 30000);
    }

    private void onDistanceTrackingUpdate(com.navdy.hud.app.maps.here.HerePositionUpdateListener.MapMatchEvent mapMatchEvent) {
        boolean isVerySevereEvent;
        if (this.currentEvent == null) {
            this.logger.w(this.tag + "onDistanceTrackingUpdate triggered while currentEvent is null.");
            return;
        }
        if (this.currentEvent.getSeverity().getValue() >= com.here.android.mpa.mapping.TrafficEvent.Severity.VERY_HIGH.getValue()) {
            isVerySevereEvent = true;
        } else {
            isVerySevereEvent = false;
        }
        java.lang.String currentRoadId = null;
        com.here.android.mpa.common.RoadElement roadElement = mapMatchEvent.mapMatch.getRoadElement();
        if (roadElement != null) {
            currentRoadId = roadElement.getIdentifier().toString();
        }
        java.lang.String eventFirstRoadId = ((com.here.android.mpa.common.RoadElement) this.currentEvent.getAffectedRoadElements().get(0)).getIdentifier().toString();
        if (this.verbose) {
            this.logger.v(this.tag + "Position update: currentRoadId=" + currentRoadId + "; eventFirstRoadId=" + eventFirstRoadId);
        }
        if (android.text.TextUtils.equals(eventFirstRoadId, currentRoadId)) {
            if (isVerySevereEvent) {
                int remainingTimeInJam = getRemainingTimeInJam(this.currentRoute, this.currentEvent);
                this.logger.v(this.tag + "triggering jam, remaining time=" + remainingTimeInJam + " sec");
                if (remainingTimeInJam >= 180) {
                    this.bus.post(new com.navdy.hud.app.maps.here.HereTrafficUpdater.JamTrackEvent(this.currentRoute, this.currentEvent, remainingTimeInJam));
                    return;
                }
            }
            this.currentEvent = null;
            changeState(com.navdy.hud.app.maps.here.HereTrafficUpdater.State.ACTIVE);
        }
    }

    private void onJamTrackingUpdate() {
        if (this.currentRoute == null) {
            this.logger.w(this.tag + "onJamTrackingUpdate triggered while currentRoute is null.");
        } else if (this.currentEvent == null) {
            this.logger.w(this.tag + "onJamTrackingUpdate triggered while currentEvent is null.");
        } else if (this.currentRemainingTimeInJam == 0) {
            this.logger.w(this.tag + "onJamTrackingUpdate triggered while currentRemainingTimeInJam is zero.");
        } else {
            int remainingTimeInJam = getRemainingTimeInJam(this.currentRoute, this.currentEvent);
            if (this.verbose) {
                this.logger.v(this.tag + "Jam time update: remainingTimeInJam=" + remainingTimeInJam);
            }
            if (remainingTimeInJam > 0) {
                int currentMinuteCeiling = com.navdy.hud.app.maps.util.MapUtils.getMinuteCeiling(this.currentRemainingTimeInJam);
                int minuteCeiling = com.navdy.hud.app.maps.util.MapUtils.getMinuteCeiling(remainingTimeInJam);
                if (java.lang.Math.abs(currentMinuteCeiling - minuteCeiling) >= 60) {
                    this.currentRemainingTimeInJam = remainingTimeInJam;
                    this.bus.post(new com.navdy.hud.app.maps.MapEvents.TrafficJamProgressEvent(minuteCeiling));
                    return;
                }
                return;
            }
            this.currentRemainingTimeInJam = 0;
            changeState(com.navdy.hud.app.maps.here.HereTrafficUpdater.State.ACTIVE);
        }
    }

    private void startUpdates() {
        if (this.currentRoute == null) {
            this.logger.e(this.tag + " route must not be null.");
        }
    }

    private void stopUpdates() {
        cancelRequest();
        this.currentEvent = null;
        this.handler.removeCallbacks(this.refreshRunnable);
        this.handler.removeCallbacks(this.dismissEtaUpdate);
        this.currentRoute = null;
        this.bus.post(new com.navdy.hud.app.maps.MapEvents.TrafficJamDismissEvent());
        this.bus.post(new com.navdy.hud.app.maps.MapEvents.TrafficDelayDismissEvent());
    }

    private void startTrackingDistance() {
        this.bus.post(transformToLiveTrafficEvent(this.currentEvent));
    }

    private void stopTrackingDistance() {
        this.bus.post(new com.navdy.hud.app.maps.MapEvents.LiveTrafficDismissEvent());
    }

    private void startTrackingJamTime() {
        this.bus.post(new com.navdy.hud.app.maps.MapEvents.TrafficJamProgressEvent(com.navdy.hud.app.maps.util.MapUtils.getMinuteCeiling(this.currentRemainingTimeInJam)));
    }

    private void stopTrackingJamTime() {
        this.bus.post(new com.navdy.hud.app.maps.MapEvents.TrafficJamDismissEvent());
    }
}
