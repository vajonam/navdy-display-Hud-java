package com.navdy.hud.app.maps.here;

public class NavdyTrafficRerouteManager {
    private static final int KILL_ROUTE_CALC = 45000;
    private static final int STALE_PERIOD = 300000;
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger("NavdyTrafficRerout");
    private final com.squareup.otto.Bus bus;
    /* access modifiers changed from: private */
    public java.lang.Runnable fasterRouteCheck = new com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.Anon4();
    /* access modifiers changed from: private */
    public java.lang.Runnable fasterRouteTrigger = new com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.Anon3();
    /* access modifiers changed from: private */
    public android.os.Handler handler = new android.os.Handler(android.os.Looper.getMainLooper());
    /* access modifiers changed from: private */
    public final com.navdy.hud.app.maps.here.HereNavigationManager hereNavigationManager;
    /* access modifiers changed from: private */
    public final com.navdy.hud.app.maps.here.HereTrafficRerouteListener hereTrafficRerouteListener;
    /* access modifiers changed from: private */
    public java.lang.Runnable killRouteCalc = new com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.Anon2();
    /* access modifiers changed from: private */
    public com.navdy.hud.app.maps.here.HereRouteCalculator routeCalculator = new com.navdy.hud.app.maps.here.HereRouteCalculator(sLogger, false);
    /* access modifiers changed from: private */
    public volatile boolean running;
    private java.lang.Runnable staleRoute = new com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.Anon1();

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            try {
                com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.hereTrafficRerouteListener.dismissReroute();
            } catch (Throwable t) {
                com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.sLogger.e(t);
            }
        }
    }

    class Anon2 implements java.lang.Runnable {
        Anon2() {
        }

        public void run() {
            try {
                com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.sLogger.v("stopping route calc");
                com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.routeCalculator.cancel();
            } catch (Throwable t) {
                com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.sLogger.e(t);
            }
        }
    }

    class Anon3 implements java.lang.Runnable {
        Anon3() {
        }

        public void run() {
            com.navdy.service.library.task.TaskManager.getInstance().execute(com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.fasterRouteCheck, 2);
        }
    }

    class Anon4 implements java.lang.Runnable {

        class Anon1 implements com.navdy.hud.app.maps.here.HereRouteCalculator.RouteCalculatorListener {

            /* renamed from: com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager$Anon4$Anon1$Anon1 reason: collision with other inner class name */
            class C0025Anon1 implements java.lang.Runnable {
                final /* synthetic */ java.util.ArrayList val$outgoingResults;

                C0025Anon1(java.util.ArrayList arrayList) {
                    this.val$outgoingResults = arrayList;
                }

                public void run() {
                    try {
                        if (!com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.running) {
                            com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.sLogger.v("success,route manager stopped");
                        } else if (this.val$outgoingResults == null || this.val$outgoingResults.size() == 0) {
                            com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.sLogger.v("success, but no result");
                            com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.cleanupRouteIfStale();
                        } else {
                            com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.sLogger.v(net.hockeyapp.android.tasks.LoginTask.BUNDLE_SUCCESS);
                            com.navdy.service.library.events.navigation.NavigationRouteResult result = (com.navdy.service.library.events.navigation.NavigationRouteResult) this.val$outgoingResults.get(0);
                            com.navdy.hud.app.maps.here.HereRouteCache hereRouteCache = com.navdy.hud.app.maps.here.HereRouteCache.getInstance();
                            com.navdy.hud.app.maps.here.HereRouteCache.RouteInfo routeInfo = hereRouteCache.getRoute(result.routeId);
                            if (routeInfo == null) {
                                com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.sLogger.v("did not get routeinfo");
                                com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.cleanupRouteIfStale();
                                return;
                            }
                            hereRouteCache.removeRoute(result.routeId);
                            if (result.duration_traffic.intValue() == 0) {
                                com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.sLogger.v("did not get traffic duration");
                                com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.cleanupRouteIfStale();
                                return;
                            }
                            com.here.android.mpa.routing.Route currentRoute = com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.hereNavigationManager.getCurrentRoute();
                            if (currentRoute == null) {
                                com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.sLogger.v("no current route");
                                com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.cleanupRouteIfStale();
                                return;
                            }
                            com.here.android.mpa.routing.RouteTta trafficTta = routeInfo.route.getTta(com.here.android.mpa.routing.Route.TrafficPenaltyMode.OPTIMAL, com.glympse.android.lib.e.gA);
                            com.here.android.mpa.routing.RouteTta noTrafficTta = routeInfo.route.getTta(com.here.android.mpa.routing.Route.TrafficPenaltyMode.DISABLED, com.glympse.android.lib.e.gA);
                            if (!(trafficTta == null || noTrafficTta == null)) {
                                com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.sLogger.v("route duration traffic[" + trafficTta.getDuration() + "] no-traffic[" + noTrafficTta.getDuration() + "]");
                            }
                            java.util.List<com.here.android.mpa.routing.Maneuver> fasterManeuvers = routeInfo.route.getManeuvers();
                            java.util.List<com.here.android.mpa.routing.Maneuver> currentManeuvers = currentRoute.getManeuvers();
                            com.here.android.mpa.routing.Maneuver currentManeuver = com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.hereNavigationManager.getNavController().getNextManeuver();
                            int firstOffSet = -1;
                            int secondOffset = 0;
                            if (currentManeuver == null) {
                                com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.sLogger.i("no current maneuver");
                                firstOffSet = 0;
                            } else {
                                int len = currentManeuvers.size();
                                int i = 0;
                                while (true) {
                                    if (i >= len) {
                                        break;
                                    } else if (com.navdy.hud.app.maps.here.HereMapUtil.areManeuverEqual((com.here.android.mpa.routing.Maneuver) currentManeuvers.get(i), currentManeuver)) {
                                        firstOffSet = i;
                                        break;
                                    } else {
                                        i++;
                                    }
                                }
                                if (firstOffSet == -1) {
                                    com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.sLogger.i("cannot find current maneuver in route");
                                    com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.cleanupRouteIfStale();
                                    return;
                                }
                                int len2 = fasterManeuvers.size();
                                int i2 = 0;
                                while (true) {
                                    if (i2 >= len2) {
                                        break;
                                    } else if (com.navdy.hud.app.maps.here.HereMapUtil.areManeuverEqual((com.here.android.mpa.routing.Maneuver) fasterManeuvers.get(i2), currentManeuver)) {
                                        secondOffset = i2;
                                        break;
                                    } else {
                                        i2++;
                                    }
                                }
                            }
                            com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.sLogger.i("firstOff = " + firstOffSet + " secondOff = " + secondOffset);
                            java.util.ArrayList arrayList = new java.util.ArrayList(2);
                            boolean areRoutesEqual = com.navdy.hud.app.maps.here.HereMapUtil.areManeuversEqual(currentManeuvers, firstOffSet, fasterManeuvers, secondOffset, arrayList);
                            java.lang.String via = com.navdy.hud.app.maps.here.HereRouteViaGenerator.getViaString(routeInfo.route);
                            java.lang.String currentVia = null;
                            if (com.navdy.hud.app.maps.here.HereRouteCache.getInstance().getRoute(com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.hereNavigationManager.getCurrentRouteId()) != null) {
                                currentVia = routeInfo.routeResult.via;
                            }
                            if (areRoutesEqual) {
                                com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.sLogger.v("fast route same as current route faster-via[" + via + "] current-via[" + currentVia + "]");
                                if (com.navdy.hud.app.framework.voice.TTSUtils.isDebugTTSEnabled()) {
                                    com.navdy.hud.app.framework.voice.TTSUtils.debugShowFasterRouteToast("Faster route not found", " current route via[" + currentVia + "] is still fastest");
                                }
                                com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.cleanupRouteIfStale();
                                return;
                            }
                            com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.sLogger.i("fast route is different than current route");
                            java.lang.String currentRoadNames = com.navdy.hud.app.maps.here.HereMapUtil.getAllRoadNames(currentManeuvers, 0);
                            java.lang.String newRoadNames = com.navdy.hud.app.maps.here.HereMapUtil.getAllRoadNames(fasterManeuvers, 0);
                            com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.sLogger.v("[RoadNames-current] [offset=" + firstOffSet + "] maneuverCount=" + currentManeuvers.size());
                            com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.sLogger.v(currentRoadNames);
                            com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.sLogger.v("[RoadNames-faster] [offset=" + secondOffset + "] maneuverCount=" + fasterManeuvers.size());
                            com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.sLogger.v(newRoadNames);
                            com.here.android.mpa.routing.Route currentProposedRoute = com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.hereTrafficRerouteListener.getCurrentProposedRoute();
                            if (currentProposedRoute != null) {
                                com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.sLogger.i("check if current proposed route is same as new one");
                                java.util.List<com.here.android.mpa.routing.Maneuver> currentProposedRouteManeuvers = currentProposedRoute.getManeuvers();
                                if (com.navdy.hud.app.maps.here.HereMapUtil.areManeuversEqual(currentProposedRouteManeuvers, 0, fasterManeuvers, 0, null)) {
                                    java.util.Date oldEta = com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.hereNavigationManager.getNavController().getEta(true, com.here.android.mpa.routing.Route.TrafficPenaltyMode.OPTIMAL);
                                    if (!com.navdy.hud.app.maps.here.HereMapUtil.isValidEtaDate(oldEta)) {
                                        com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.sLogger.v("current eta is invalid:" + oldEta);
                                        com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.cleanupRouteIfStale();
                                        return;
                                    }
                                    long diff = ((oldEta.getTime() - java.lang.System.currentTimeMillis()) / 1000) - ((long) trafficTta.getDuration());
                                    int minDuration = com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.hereTrafficRerouteListener.getRerouteMinDuration();
                                    com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.sLogger.i("current proposed route[" + currentVia + "] is same as new one[" + via + "] new-diff[" + diff + "] old-diff[" + com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.hereTrafficRerouteListener.getFasterBy() + "] minDur[" + minDuration + "]");
                                    if (diff < ((long) minDuration)) {
                                        com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.sLogger.i("route has dropped below threshold:" + diff);
                                        com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.hereTrafficRerouteListener.dismissReroute();
                                        return;
                                    }
                                    com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.sLogger.i("route still above threshold, update");
                                } else {
                                    com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.sLogger.i("current proposed route[" + currentVia + "] is different from new one[" + via + "]");
                                    java.lang.String currentRoadNames2 = com.navdy.hud.app.maps.here.HereMapUtil.getAllRoadNames(currentProposedRouteManeuvers, 0);
                                    java.lang.String newRoadNames2 = com.navdy.hud.app.maps.here.HereMapUtil.getAllRoadNames(fasterManeuvers, 0);
                                    com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.sLogger.v("[RoadNames-proposed-current] maneuverCount=" + currentProposedRouteManeuvers.size());
                                    com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.sLogger.v(currentRoadNames2);
                                    com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.sLogger.v("[RoadNames-proposed-faster] maneuverCount=" + fasterManeuvers.size());
                                    com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.sLogger.v(newRoadNames2);
                                }
                            }
                            com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.sLogger.i("calling here traffic reroute listener");
                            com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.hereTrafficRerouteListener.handleFasterRoute(routeInfo.route, false);
                        }
                    } catch (Throwable t) {
                        com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.sLogger.e(t);
                    }
                }
            }

            Anon1() {
            }

            public void preSuccess() {
                com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.handler.removeCallbacks(com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.killRouteCalc);
            }

            public void postSuccess(java.util.ArrayList<com.navdy.service.library.events.navigation.NavigationRouteResult> outgoingResults) {
                com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.Anon4.Anon1.C0025Anon1(outgoingResults), 2);
            }

            public void error(com.here.android.mpa.routing.RoutingError error, java.lang.Throwable t) {
                com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.handler.removeCallbacks(com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.killRouteCalc);
                com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.cleanupRouteIfStale();
                if (com.navdy.hud.app.framework.voice.TTSUtils.isDebugTTSEnabled()) {
                    java.lang.String title = "Faster route not found [Error]";
                    java.lang.String info = "";
                    if (t != null) {
                        info = t.toString();
                    } else if (error != null) {
                        info = error.name();
                    }
                    com.navdy.hud.app.framework.voice.TTSUtils.debugShowFasterRouteToast(title, info);
                }
                com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.sLogger.e("error occured:" + error, t);
            }

            public void progress(int progress) {
            }
        }

        Anon4() {
        }

        public void run() {
            try {
                com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.handler.removeCallbacks(com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.killRouteCalc);
                if (!com.navdy.hud.app.framework.network.NetworkStateManager.isConnectedToNetwork(com.navdy.hud.app.HudApplication.getAppContext())) {
                    com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.cleanupRouteIfStale();
                    com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.sLogger.v("skip traffic check, no n/w");
                    if (com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.running) {
                        com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.handler.postDelayed(com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.fasterRouteTrigger, (long) (com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.hereNavigationManager.getRerouteInterval() / 2));
                    }
                } else if (!com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.hereNavigationManager.isNavigationModeOn()) {
                    com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.hereTrafficRerouteListener.dismissReroute();
                    com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.sLogger.v("skip traffic check, not navigating");
                    if (com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.running) {
                        com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.handler.postDelayed(com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.fasterRouteTrigger, (long) (com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.hereNavigationManager.getRerouteInterval() / 2));
                    }
                } else if (com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.hereNavigationManager.isRerouting()) {
                    com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.sLogger.v("skip traffic check, rerouting");
                    com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.hereTrafficRerouteListener.dismissReroute();
                    if (com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.running) {
                        com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.handler.postDelayed(com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.fasterRouteTrigger, (long) (com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.hereNavigationManager.getRerouteInterval() / 2));
                    }
                } else if (com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.hereNavigationManager.hasArrived()) {
                    com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.sLogger.v("skip traffic check, arrival mode on");
                    com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.hereTrafficRerouteListener.dismissReroute();
                    if (com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.running) {
                        com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.handler.postDelayed(com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.fasterRouteTrigger, (long) (com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.hereNavigationManager.getRerouteInterval() / 2));
                    }
                } else if (com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.hereNavigationManager.isOnGasRoute()) {
                    com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.sLogger.v("skip traffic check, on gas route");
                    com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.hereTrafficRerouteListener.dismissReroute();
                    if (com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.running) {
                        com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.handler.postDelayed(com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.fasterRouteTrigger, (long) (com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.hereNavigationManager.getRerouteInterval() / 2));
                    }
                } else if (!com.navdy.hud.app.framework.network.NetworkBandwidthController.getInstance().isLimitBandwidthModeOn() || !com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.hereNavigationManager.hasTrafficRerouteOnce()) {
                    com.here.android.mpa.routing.Route currentRoute = com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.hereNavigationManager.getCurrentRoute();
                    if (currentRoute == null) {
                        com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.hereTrafficRerouteListener.dismissReroute();
                        com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.sLogger.v("skip traffic check, no current route");
                        if (com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.running) {
                            com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.handler.postDelayed(com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.fasterRouteTrigger, (long) (com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.hereNavigationManager.getRerouteInterval() / 2));
                            return;
                        }
                        return;
                    }
                    com.navdy.hud.app.maps.here.HereMapsManager hereMapsManager = com.navdy.hud.app.maps.here.HereMapsManager.getInstance();
                    com.here.android.mpa.common.GeoCoordinate startPoint = hereMapsManager.getLocationFixManager().getLastGeoCoordinate();
                    if (startPoint == null) {
                        com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.cleanupRouteIfStale();
                        com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.sLogger.e("skip traffic check, no start point");
                        if (com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.running) {
                            com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.handler.postDelayed(com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.fasterRouteTrigger, (long) (com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.hereNavigationManager.getRerouteInterval() / 2));
                            return;
                        }
                        return;
                    }
                    com.here.android.mpa.common.GeoCoordinate endPoint = currentRoute.getDestination();
                    if (endPoint == null) {
                        com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.cleanupRouteIfStale();
                        com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.sLogger.e("skip traffic check, no end point");
                        if (com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.running) {
                            com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.handler.postDelayed(com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.fasterRouteTrigger, (long) (com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.hereNavigationManager.getRerouteInterval() / 2));
                        }
                    } else if (!com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.hereTrafficRerouteListener.shouldCalculateFasterRoute()) {
                        com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.sLogger.e("skip traffic check, time not right");
                        if (com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.running) {
                            com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.handler.postDelayed(com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.fasterRouteTrigger, (long) (com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.hereNavigationManager.getRerouteInterval() / 2));
                        }
                    } else {
                        com.here.android.mpa.routing.RouteOptions routeOptions = hereMapsManager.getRouteOptions();
                        routeOptions.setRouteType(com.here.android.mpa.routing.RouteOptions.Type.FASTEST);
                        com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.routeCalculator.cancel();
                        com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.handler.postDelayed(com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.killRouteCalc, 45000);
                        com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.routeCalculator.calculateRoute(null, startPoint, null, endPoint, true, new com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.Anon4.Anon1(), 1, routeOptions, true, false, false);
                        if (com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.running) {
                            com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.handler.postDelayed(com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.fasterRouteTrigger, (long) (com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.hereNavigationManager.getRerouteInterval() / 2));
                        }
                    }
                } else {
                    com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.sLogger.v("user has already selected faster route, no-op in limited b/w condition");
                    if (com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.running) {
                        com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.handler.postDelayed(com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.fasterRouteTrigger, (long) (com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.hereNavigationManager.getRerouteInterval() / 2));
                    }
                }
            } catch (Throwable th) {
                if (com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.running) {
                    com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.handler.postDelayed(com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.fasterRouteTrigger, (long) (com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.hereNavigationManager.getRerouteInterval() / 2));
                }
                throw th;
            }
        }
    }

    class Anon5 implements com.navdy.hud.app.maps.here.HereRouteCalculator.RouteCalculatorListener {
        final /* synthetic */ java.lang.String val$additionalVia;
        final /* synthetic */ java.lang.String val$currentVia;
        final /* synthetic */ long val$diff;
        final /* synthetic */ long val$distanceDiff;
        final /* synthetic */ long val$etaUtc;
        final /* synthetic */ com.here.android.mpa.routing.Route val$fasterRoute;
        final /* synthetic */ com.here.android.mpa.routing.RouteTta val$fasterRouteTta;
        final /* synthetic */ boolean val$notifFromHereTraffic;
        final /* synthetic */ java.lang.String val$via;

        class Anon1 implements java.lang.Runnable {
            final /* synthetic */ java.util.ArrayList val$outgoingResults;

            Anon1(java.util.ArrayList arrayList) {
                this.val$outgoingResults = arrayList;
            }

            public void run() {
                try {
                    if (!com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.running) {
                        com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.sLogger.v("reCalculateCurrentRoute: success,route manager stopped");
                        com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.cleanupRouteIfStale();
                        com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.hereTrafficRerouteListener.fasterRouteNotFound(com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.Anon5.this.val$notifFromHereTraffic, com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.Anon5.this.val$diff, com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.Anon5.this.val$via, com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.Anon5.this.val$currentVia);
                    } else if (this.val$outgoingResults == null || this.val$outgoingResults.size() == 0) {
                        com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.sLogger.v("reCalculateCurrentRoute: success, but no result");
                        com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.cleanupRouteIfStale();
                        com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.hereTrafficRerouteListener.fasterRouteNotFound(com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.Anon5.this.val$notifFromHereTraffic, com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.Anon5.this.val$diff, com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.Anon5.this.val$via, com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.Anon5.this.val$currentVia);
                    } else {
                        com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.sLogger.v("reCalculateCurrentRoute: success");
                        com.navdy.service.library.events.navigation.NavigationRouteResult result = (com.navdy.service.library.events.navigation.NavigationRouteResult) this.val$outgoingResults.get(0);
                        com.navdy.hud.app.maps.here.HereRouteCache hereRouteCache = com.navdy.hud.app.maps.here.HereRouteCache.getInstance();
                        com.navdy.hud.app.maps.here.HereRouteCache.RouteInfo routeInfo = hereRouteCache.getRoute(result.routeId);
                        if (routeInfo == null) {
                            com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.sLogger.v("reCalculateCurrentRoute: did not get routeinfo");
                            com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.cleanupRouteIfStale();
                            com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.hereTrafficRerouteListener.fasterRouteNotFound(com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.Anon5.this.val$notifFromHereTraffic, com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.Anon5.this.val$diff, com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.Anon5.this.val$via, com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.Anon5.this.val$currentVia);
                            return;
                        }
                        hereRouteCache.removeRoute(result.routeId);
                        if (result.duration_traffic.intValue() == 0) {
                            com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.sLogger.v("reCalculateCurrentRoute: did not get traffic duration");
                            com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.cleanupRouteIfStale();
                            com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.hereTrafficRerouteListener.fasterRouteNotFound(com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.Anon5.this.val$notifFromHereTraffic, com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.Anon5.this.val$diff, com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.Anon5.this.val$via, com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.Anon5.this.val$currentVia);
                            return;
                        }
                        com.here.android.mpa.routing.RouteTta currentTrafficTta = routeInfo.route.getTta(com.here.android.mpa.routing.Route.TrafficPenaltyMode.OPTIMAL, com.glympse.android.lib.e.gA);
                        if (currentTrafficTta == null) {
                            com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.sLogger.v("reCalculateCurrentRoute: could not get traffic route tta");
                            com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.cleanupRouteIfStale();
                            com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.hereTrafficRerouteListener.fasterRouteNotFound(com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.Anon5.this.val$notifFromHereTraffic, com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.Anon5.this.val$diff, com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.Anon5.this.val$via, com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.Anon5.this.val$currentVia);
                            return;
                        }
                        int currentDuration = currentTrafficTta.getDuration();
                        int fasterDuration = com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.Anon5.this.val$fasterRouteTta.getDuration();
                        com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.sLogger.v("reCalculateCurrentRoute: currentDuration=" + currentDuration + " fasterDuration=" + fasterDuration);
                        long newDiff = (long) (currentDuration - fasterDuration);
                        int minDuration = com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.hereTrafficRerouteListener.getRerouteMinDuration();
                        if (newDiff >= ((long) minDuration)) {
                            com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.sLogger.v("reCalculateCurrentRoute: set faster route, duration:" + newDiff + " >= threshold:" + minDuration);
                            long now = java.lang.System.currentTimeMillis();
                            long newEtaUtc = now + ((long) (fasterDuration * 1000));
                            java.util.Date date = new java.util.Date(newEtaUtc);
                            java.util.Date date2 = new java.util.Date(now);
                            java.util.Date date3 = new java.util.Date(com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.Anon5.this.val$etaUtc);
                            com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.sLogger.v("reCalculateCurrentRoute: newEta:" + date + " now=" + date2 + " currentEta=" + date3);
                            com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.hereTrafficRerouteListener.setFasterRoute(com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.Anon5.this.val$fasterRoute, com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.Anon5.this.val$notifFromHereTraffic, com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.Anon5.this.val$via, 0, com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.Anon5.this.val$additionalVia, 0, com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.Anon5.this.val$distanceDiff, newEtaUtc);
                            return;
                        }
                        com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.sLogger.v("reCalculateCurrentRoute: duration:" + newDiff + " < threshold:" + minDuration);
                        com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.cleanupRouteIfStale();
                        com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.hereTrafficRerouteListener.fasterRouteNotFound(com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.Anon5.this.val$notifFromHereTraffic, newDiff, com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.Anon5.this.val$via, com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.Anon5.this.val$currentVia);
                    }
                } catch (Throwable t) {
                    com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.sLogger.e(t);
                }
            }
        }

        Anon5(boolean z, long j, java.lang.String str, java.lang.String str2, com.here.android.mpa.routing.RouteTta routeTta, long j2, com.here.android.mpa.routing.Route route, java.lang.String str3, long j3) {
            this.val$notifFromHereTraffic = z;
            this.val$diff = j;
            this.val$via = str;
            this.val$currentVia = str2;
            this.val$fasterRouteTta = routeTta;
            this.val$etaUtc = j2;
            this.val$fasterRoute = route;
            this.val$additionalVia = str3;
            this.val$distanceDiff = j3;
        }

        public void preSuccess() {
            com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.handler.removeCallbacks(com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.killRouteCalc);
        }

        public void postSuccess(java.util.ArrayList<com.navdy.service.library.events.navigation.NavigationRouteResult> outgoingResults) {
            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.Anon5.Anon1(outgoingResults), 2);
        }

        public void error(com.here.android.mpa.routing.RoutingError error, java.lang.Throwable t) {
            com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.handler.removeCallbacks(com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.killRouteCalc);
            com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.cleanupRouteIfStale();
            com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.this.hereTrafficRerouteListener.fasterRouteNotFound(this.val$notifFromHereTraffic, this.val$diff, this.val$via, this.val$currentVia);
            com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.sLogger.e("error occured:" + error, t);
        }

        public void progress(int progress) {
        }
    }

    NavdyTrafficRerouteManager(com.navdy.hud.app.maps.here.HereNavigationManager hereNavigationManager2, com.navdy.hud.app.maps.here.HereTrafficRerouteListener hereTrafficRerouteListener2, com.squareup.otto.Bus bus2) {
        sLogger.v("NavdyTrafficRerouteManager:ctor");
        this.hereNavigationManager = hereNavigationManager2;
        this.hereTrafficRerouteListener = hereTrafficRerouteListener2;
        this.bus = bus2;
    }

    public void start() {
        if (!this.running) {
            this.running = true;
            this.handler.removeCallbacks(this.staleRoute);
            this.handler.removeCallbacks(this.fasterRouteTrigger);
            this.handler.postDelayed(this.fasterRouteTrigger, (long) com.navdy.hud.app.maps.here.HereTrafficRerouteListener.ROUTE_CHECK_INITIAL_INTERVAL);
            this.bus.register(this);
            sLogger.v("running");
        }
    }

    public void stop() {
        if (this.running) {
            this.running = false;
            this.handler.removeCallbacks(this.fasterRouteTrigger);
            this.handler.postDelayed(this.staleRoute, 300000);
            this.bus.unregister(this);
            sLogger.v("stopped");
        }
    }

    @com.squareup.otto.Subscribe
    public void onBandwidthSettingChanged(com.navdy.hud.app.framework.network.NetworkBandwidthController.UserBandwidthSettingChanged event) {
        this.handler.removeCallbacks(this.fasterRouteTrigger);
        if (this.running) {
            long interval = (long) (this.hereNavigationManager.getRerouteInterval() / 2);
            sLogger.v("faster route time changed to " + interval);
            this.handler.postDelayed(this.fasterRouteTrigger, interval);
        }
    }

    /* access modifiers changed from: private */
    public void cleanupRouteIfStale() {
        if (this.hereTrafficRerouteListener.getCurrentProposedRoute() != null) {
            long time = android.os.SystemClock.elapsedRealtime() - this.hereTrafficRerouteListener.getCurrentProposedRouteTime();
            if (time > 300000) {
                sLogger.v("clear stale route:" + time);
                this.hereTrafficRerouteListener.dismissReroute();
                return;
            }
            sLogger.v("route not stale:" + time);
            return;
        }
        sLogger.v("no proposed route");
    }

    public void reset() {
        try {
            sLogger.v("reset::");
            this.routeCalculator.cancel();
            this.hereTrafficRerouteListener.dismissReroute();
            this.handler.removeCallbacks(this.fasterRouteTrigger);
            if (this.running) {
                sLogger.v("reset::timer reset");
                this.handler.postDelayed(this.fasterRouteTrigger, (long) com.navdy.hud.app.maps.here.HereTrafficRerouteListener.ROUTE_CHECK_INITIAL_INTERVAL);
            }
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }

    /* access modifiers changed from: 0000 */
    public void reCalculateCurrentRoute(com.here.android.mpa.routing.Route currentRoute, com.here.android.mpa.routing.Route fasterRoute, boolean notifFromHereTraffic, java.lang.String via, long diff, java.lang.String additionalVia, long etaUtc, long distanceDiff, java.lang.String currentVia, com.here.android.mpa.routing.RouteTta fasterRouteTta) {
        try {
            sLogger.v("reCalculateCurrentRoute:");
            this.handler.removeCallbacks(this.killRouteCalc);
            if (!com.navdy.hud.app.framework.network.NetworkStateManager.isConnectedToNetwork(com.navdy.hud.app.HudApplication.getAppContext())) {
                cleanupRouteIfStale();
                this.hereTrafficRerouteListener.fasterRouteNotFound(notifFromHereTraffic, diff, via, currentVia);
                sLogger.v("reCalculateCurrentRoute:skip no n/w");
            } else if (!this.hereNavigationManager.isNavigationModeOn()) {
                this.hereTrafficRerouteListener.dismissReroute();
                this.hereTrafficRerouteListener.fasterRouteNotFound(notifFromHereTraffic, diff, via, currentVia);
                sLogger.v("reCalculateCurrentRoute: not navigating");
            } else if (this.hereNavigationManager.isRerouting()) {
                sLogger.v("reCalculateCurrentRoute: rerouting");
                this.hereTrafficRerouteListener.dismissReroute();
                this.hereTrafficRerouteListener.fasterRouteNotFound(notifFromHereTraffic, diff, via, currentVia);
            } else if (this.hereNavigationManager.hasArrived()) {
                sLogger.v("reCalculateCurrentRoute: arrival mode on");
                this.hereTrafficRerouteListener.dismissReroute();
                this.hereTrafficRerouteListener.fasterRouteNotFound(notifFromHereTraffic, diff, via, currentVia);
            } else if (this.hereNavigationManager.isOnGasRoute()) {
                sLogger.v("reCalculateCurrentRoute: on gas route");
                this.hereTrafficRerouteListener.dismissReroute();
                this.hereTrafficRerouteListener.fasterRouteNotFound(notifFromHereTraffic, diff, via, currentVia);
            } else {
                com.navdy.hud.app.maps.here.HereMapsManager hereMapsManager = com.navdy.hud.app.maps.here.HereMapsManager.getInstance();
                com.here.android.mpa.common.GeoCoordinate startPoint = hereMapsManager.getLocationFixManager().getLastGeoCoordinate();
                if (startPoint == null) {
                    cleanupRouteIfStale();
                    this.hereTrafficRerouteListener.fasterRouteNotFound(notifFromHereTraffic, diff, via, currentVia);
                    sLogger.e("reCalculateCurrentRoute: no start point");
                    return;
                }
                com.here.android.mpa.common.GeoCoordinate endPoint = currentRoute.getDestination();
                if (endPoint == null) {
                    cleanupRouteIfStale();
                    this.hereTrafficRerouteListener.fasterRouteNotFound(notifFromHereTraffic, diff, via, currentVia);
                    sLogger.e("reCalculateCurrentRoute: no end point");
                    return;
                }
                com.here.android.mpa.routing.RouteOptions routeOptions = hereMapsManager.getRouteOptions();
                routeOptions.setRouteType(com.here.android.mpa.routing.RouteOptions.Type.FASTEST);
                com.here.android.mpa.routing.Maneuver currentManeuver = this.hereNavigationManager.getNavController().getNextManeuver();
                if (currentManeuver == null) {
                    cleanupRouteIfStale();
                    this.hereTrafficRerouteListener.fasterRouteNotFound(notifFromHereTraffic, diff, via, currentVia);
                    sLogger.i("reCalculateCurrentRoute: no current maneuver");
                    return;
                }
                int offset = -1;
                java.util.List<com.here.android.mpa.routing.Maneuver> maneuvers = currentRoute.getManeuvers();
                int len = 0;
                if (maneuvers != null) {
                    len = maneuvers.size();
                    int i = 0;
                    while (true) {
                        if (i >= len) {
                            break;
                        } else if (com.navdy.hud.app.maps.here.HereMapUtil.areManeuverEqual((com.here.android.mpa.routing.Maneuver) maneuvers.get(i), currentManeuver)) {
                            offset = i;
                            break;
                        } else {
                            i++;
                        }
                    }
                }
                if (offset == -1) {
                    sLogger.i("reCalculateCurrentRoute: cannot find current maneuver in route");
                    cleanupRouteIfStale();
                    this.hereTrafficRerouteListener.fasterRouteNotFound(notifFromHereTraffic, diff, via, currentVia);
                    return;
                }
                java.util.ArrayList arrayList = new java.util.ArrayList();
                for (int i2 = offset; i2 < len; i2++) {
                    com.here.android.mpa.common.GeoCoordinate geoCoordinate = ((com.here.android.mpa.routing.Maneuver) maneuvers.get(i2)).getCoordinate();
                    if (geoCoordinate != null) {
                        sLogger.i("reCalculateCurrentRoute: added waypoint:" + geoCoordinate);
                        arrayList.add(geoCoordinate);
                    }
                }
                sLogger.i("reCalculateCurrentRoute: waypoints:" + arrayList.size());
                this.routeCalculator.cancel();
                this.handler.postDelayed(this.killRouteCalc, 45000);
                this.routeCalculator.calculateRoute(null, startPoint, arrayList, endPoint, true, new com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.Anon5(notifFromHereTraffic, diff, via, currentVia, fasterRouteTta, etaUtc, fasterRoute, additionalVia, distanceDiff), 1, routeOptions, true, false, false);
            }
        } catch (Throwable t) {
            sLogger.e(t);
            cleanupRouteIfStale();
            this.hereTrafficRerouteListener.fasterRouteNotFound(notifFromHereTraffic, diff, via, currentVia);
            com.navdy.hud.app.util.CrashReporter.getInstance().reportNonFatalException(t);
        }
    }
}
