package com.navdy.hud.app.maps.here;

public class HereRealisticViewListener extends com.here.android.mpa.guidance.NavigationManager.RealisticViewListener {
    private static final com.navdy.hud.app.maps.MapEvents.HideSignPostJunction HIDE_SIGN_POST_JUNCTION = new com.navdy.hud.app.maps.MapEvents.HideSignPostJunction();
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.maps.here.HereRealisticViewListener.class);
    private final com.squareup.otto.Bus bus;
    /* access modifiers changed from: private */
    public final com.navdy.hud.app.maps.here.HereNavController navController;
    private boolean running;
    private java.lang.Runnable start = new com.navdy.hud.app.maps.here.HereRealisticViewListener.Anon1();
    private java.lang.Runnable stop = new com.navdy.hud.app.maps.here.HereRealisticViewListener.Anon2();

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            com.navdy.hud.app.maps.here.HereRealisticViewListener.this.navController.setRealisticViewMode(com.here.android.mpa.guidance.NavigationManager.RealisticViewMode.NIGHT);
            com.navdy.hud.app.maps.here.HereRealisticViewListener.this.navController.addRealisticViewAspectRatio(com.here.android.mpa.guidance.NavigationManager.AspectRatio.AR_3x5);
            com.navdy.hud.app.maps.here.HereRealisticViewListener.this.navController.addRealisticViewListener(new java.lang.ref.WeakReference(com.navdy.hud.app.maps.here.HereRealisticViewListener.this));
            com.navdy.hud.app.maps.here.HereRealisticViewListener.sLogger.v("added realistic view listener");
        }
    }

    class Anon2 implements java.lang.Runnable {
        Anon2() {
        }

        public void run() {
            com.navdy.hud.app.maps.here.HereRealisticViewListener.this.navController.setRealisticViewMode(com.here.android.mpa.guidance.NavigationManager.RealisticViewMode.OFF);
            com.navdy.hud.app.maps.here.HereRealisticViewListener.this.navController.removeRealisticViewListener(com.navdy.hud.app.maps.here.HereRealisticViewListener.this);
            com.navdy.hud.app.maps.here.HereRealisticViewListener.sLogger.v("removed realistic view listener");
        }
    }

    HereRealisticViewListener(com.squareup.otto.Bus bus2, com.navdy.hud.app.maps.here.HereNavController navController2) {
        this.navController = navController2;
        this.bus = bus2;
    }

    /* access modifiers changed from: 0000 */
    public synchronized void start() {
        if (!this.running) {
            com.navdy.service.library.task.TaskManager.getInstance().execute(this.start, 15);
            this.running = true;
            sLogger.v("start:added realistic view listener");
        }
    }

    /* access modifiers changed from: 0000 */
    public synchronized void stop() {
        if (this.running) {
            com.navdy.service.library.task.TaskManager.getInstance().execute(this.stop, 15);
            this.running = false;
            clearDisplayEvent();
            sLogger.v("stop:remove realistic view listener");
        }
    }

    public void onRealisticViewNextManeuver(com.here.android.mpa.guidance.NavigationManager.AspectRatio ratio, com.here.android.mpa.common.Image img1, com.here.android.mpa.common.Image img2) {
    }

    public void onRealisticViewShow(com.here.android.mpa.guidance.NavigationManager.AspectRatio ratio, com.here.android.mpa.common.Image img1, com.here.android.mpa.common.Image img2) {
        this.bus.post(new com.navdy.hud.app.maps.MapEvents.DisplayJunction(img1, img2));
    }

    public void onRealisticViewHide() {
        clearDisplayEvent();
    }

    private void clearDisplayEvent() {
        this.bus.post(HIDE_SIGN_POST_JUNCTION);
    }
}
