package com.navdy.hud.app.maps.here;

public class HereLaneInfoBuilder {
    private static boolean initialized;
    private static java.util.HashMap<java.lang.Integer, android.graphics.drawable.Drawable> sLaneDrawableCache;
    private static java.util.HashMap<com.here.android.mpa.guidance.LaneInformation.Direction, java.lang.Integer> sLeftOnlyNotRecommendedDirection;
    private static java.util.HashMap<com.here.android.mpa.guidance.LaneInformation.Direction, java.lang.Integer> sLeftOnlyRecommendedDirection;
    private static java.util.HashMap<com.here.android.mpa.guidance.LaneInformation.Direction, java.lang.Integer> sLeftRightNotRecommendedDirection;
    private static java.util.HashMap<com.here.android.mpa.guidance.LaneInformation.Direction, java.lang.Integer> sLeftRightRecommendedDirection;
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.maps.here.HereLaneInfoBuilder.class);
    private static java.util.HashMap<com.here.android.mpa.guidance.LaneInformation.Direction, java.lang.Integer> sRightOnlyNotRecommendedDirection;
    private static java.util.HashMap<com.here.android.mpa.guidance.LaneInformation.Direction, java.lang.Integer> sRightOnlyRecommendedDirection;
    private static java.util.HashMap<com.here.android.mpa.guidance.LaneInformation.Direction, java.lang.Integer> sSingleNotRecommendedDirection;
    private static java.util.HashMap<com.here.android.mpa.guidance.LaneInformation.Direction, java.lang.Integer> sSingleRecommendedDirection;

    static /* synthetic */ class Anon1 {
        static final /* synthetic */ int[] $SwitchMap$com$here$android$mpa$guidance$LaneInformation$Direction = new int[com.here.android.mpa.guidance.LaneInformation.Direction.values().length];

        static {
            try {
                $SwitchMap$com$here$android$mpa$guidance$LaneInformation$Direction[com.here.android.mpa.guidance.LaneInformation.Direction.STRAIGHT.ordinal()] = 1;
            } catch (java.lang.NoSuchFieldError e) {
            }
            try {
                $SwitchMap$com$here$android$mpa$guidance$LaneInformation$Direction[com.here.android.mpa.guidance.LaneInformation.Direction.SLIGHTLY_RIGHT.ordinal()] = 2;
            } catch (java.lang.NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$com$here$android$mpa$guidance$LaneInformation$Direction[com.here.android.mpa.guidance.LaneInformation.Direction.RIGHT.ordinal()] = 3;
            } catch (java.lang.NoSuchFieldError e3) {
            }
            try {
                $SwitchMap$com$here$android$mpa$guidance$LaneInformation$Direction[com.here.android.mpa.guidance.LaneInformation.Direction.SHARP_RIGHT.ordinal()] = 4;
            } catch (java.lang.NoSuchFieldError e4) {
            }
            try {
                $SwitchMap$com$here$android$mpa$guidance$LaneInformation$Direction[com.here.android.mpa.guidance.LaneInformation.Direction.U_TURN_LEFT.ordinal()] = 5;
            } catch (java.lang.NoSuchFieldError e5) {
            }
            try {
                $SwitchMap$com$here$android$mpa$guidance$LaneInformation$Direction[com.here.android.mpa.guidance.LaneInformation.Direction.SHARP_LEFT.ordinal()] = 6;
            } catch (java.lang.NoSuchFieldError e6) {
            }
            try {
                $SwitchMap$com$here$android$mpa$guidance$LaneInformation$Direction[com.here.android.mpa.guidance.LaneInformation.Direction.LEFT.ordinal()] = 7;
            } catch (java.lang.NoSuchFieldError e7) {
            }
            try {
                $SwitchMap$com$here$android$mpa$guidance$LaneInformation$Direction[com.here.android.mpa.guidance.LaneInformation.Direction.SLIGHTLY_LEFT.ordinal()] = 8;
            } catch (java.lang.NoSuchFieldError e8) {
            }
            try {
                $SwitchMap$com$here$android$mpa$guidance$LaneInformation$Direction[com.here.android.mpa.guidance.LaneInformation.Direction.U_TURN_RIGHT.ordinal()] = 9;
            } catch (java.lang.NoSuchFieldError e9) {
            }
            $SwitchMap$com$navdy$hud$app$maps$here$HereLaneInfoBuilder$DirectionType = new int[com.navdy.hud.app.maps.here.HereLaneInfoBuilder.DirectionType.values().length];
            try {
                $SwitchMap$com$navdy$hud$app$maps$here$HereLaneInfoBuilder$DirectionType[com.navdy.hud.app.maps.here.HereLaneInfoBuilder.DirectionType.LEFT.ordinal()] = 1;
            } catch (java.lang.NoSuchFieldError e10) {
            }
            try {
                $SwitchMap$com$navdy$hud$app$maps$here$HereLaneInfoBuilder$DirectionType[com.navdy.hud.app.maps.here.HereLaneInfoBuilder.DirectionType.RIGHT.ordinal()] = 2;
            } catch (java.lang.NoSuchFieldError e11) {
            }
            try {
                $SwitchMap$com$navdy$hud$app$maps$here$HereLaneInfoBuilder$DirectionType[com.navdy.hud.app.maps.here.HereLaneInfoBuilder.DirectionType.LEFT_RIGHT.ordinal()] = 3;
            } catch (java.lang.NoSuchFieldError e12) {
            }
        }
    }

    public enum DirectionType {
        LEFT,
        RIGHT,
        LEFT_RIGHT
    }

    public static synchronized void init() {
        synchronized (com.navdy.hud.app.maps.here.HereLaneInfoBuilder.class) {
            if (!initialized) {
                com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
                initialized = true;
                android.content.res.Resources resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
                sLaneDrawableCache = new java.util.HashMap<>();
                sSingleRecommendedDirection = new java.util.HashMap<>();
                sSingleRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation.Direction.STRAIGHT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_rec_straight_only));
                sSingleRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation.Direction.SLIGHTLY_RIGHT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_rec_slight_right_only));
                sSingleRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation.Direction.RIGHT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_rec_right_only));
                sSingleRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation.Direction.SHARP_RIGHT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_rec_heavy_right_only));
                sSingleRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation.Direction.U_TURN_RIGHT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_rec_uturn_right_only));
                sSingleRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation.Direction.SECOND_RIGHT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_glance_whatsapp));
                sSingleRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation.Direction.MERGE_RIGHT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_glance_whatsapp));
                sSingleRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation.Direction.U_TURN_LEFT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_rec_uturn_left_only));
                sSingleRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation.Direction.SHARP_LEFT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_rec_heavy_left_only));
                sSingleRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation.Direction.LEFT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_rec_left_only));
                sSingleRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation.Direction.SLIGHTLY_LEFT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_rec_slight_left_only));
                sSingleRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation.Direction.SECOND_LEFT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_glance_whatsapp));
                sSingleRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation.Direction.MERGE_LEFT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_glance_whatsapp));
                sSingleRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation.Direction.MERGE_LANES, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_glance_whatsapp));
                sLaneDrawableCache.put(java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_rec_straight_only), resources.getDrawable(com.navdy.hud.app.R.drawable.icon_lg_rec_straight_only));
                sLaneDrawableCache.put(java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_rec_slight_right_only), resources.getDrawable(com.navdy.hud.app.R.drawable.icon_lg_rec_slight_right_only));
                sLaneDrawableCache.put(java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_rec_right_only), resources.getDrawable(com.navdy.hud.app.R.drawable.icon_lg_rec_right_only));
                sLaneDrawableCache.put(java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_rec_heavy_right_only), resources.getDrawable(com.navdy.hud.app.R.drawable.icon_lg_rec_heavy_right_only));
                sLaneDrawableCache.put(java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_rec_uturn_right_only), resources.getDrawable(com.navdy.hud.app.R.drawable.icon_lg_rec_uturn_right_only));
                sLaneDrawableCache.put(java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_rec_uturn_left_only), resources.getDrawable(com.navdy.hud.app.R.drawable.icon_lg_rec_uturn_left_only));
                sLaneDrawableCache.put(java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_rec_heavy_left_only), resources.getDrawable(com.navdy.hud.app.R.drawable.icon_lg_rec_heavy_left_only));
                sLaneDrawableCache.put(java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_rec_left_only), resources.getDrawable(com.navdy.hud.app.R.drawable.icon_lg_rec_left_only));
                sLaneDrawableCache.put(java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_rec_slight_left_only), resources.getDrawable(com.navdy.hud.app.R.drawable.icon_lg_rec_slight_left_only));
                sLaneDrawableCache.put(java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_glance_whatsapp), resources.getDrawable(com.navdy.hud.app.R.drawable.icon_glance_whatsapp));
                sSingleNotRecommendedDirection = new java.util.HashMap<>();
                sSingleNotRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation.Direction.STRAIGHT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_straight_only));
                sSingleNotRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation.Direction.SLIGHTLY_RIGHT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_slight_right_only));
                sSingleNotRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation.Direction.RIGHT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_right_only));
                sSingleNotRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation.Direction.SHARP_RIGHT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_heavy_right_only));
                sSingleNotRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation.Direction.U_TURN_RIGHT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_uturn_right_only));
                sSingleNotRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation.Direction.SECOND_RIGHT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_glance_whatsapp));
                sSingleNotRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation.Direction.MERGE_RIGHT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_glance_whatsapp));
                sSingleNotRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation.Direction.U_TURN_LEFT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_uturn_left_only));
                sSingleNotRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation.Direction.SHARP_LEFT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_heavy_left_only));
                sSingleNotRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation.Direction.LEFT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_left_only));
                sSingleNotRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation.Direction.SLIGHTLY_LEFT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_slight_left_only));
                sSingleNotRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation.Direction.SECOND_LEFT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_glance_whatsapp));
                sSingleNotRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation.Direction.MERGE_LEFT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_glance_whatsapp));
                sSingleNotRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation.Direction.MERGE_LANES, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_glance_whatsapp));
                sLaneDrawableCache.put(java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_straight_only), resources.getDrawable(com.navdy.hud.app.R.drawable.icon_lg_straight_only));
                sLaneDrawableCache.put(java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_slight_right_only), resources.getDrawable(com.navdy.hud.app.R.drawable.icon_lg_slight_right_only));
                sLaneDrawableCache.put(java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_right_only), resources.getDrawable(com.navdy.hud.app.R.drawable.icon_lg_right_only));
                sLaneDrawableCache.put(java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_heavy_right_only), resources.getDrawable(com.navdy.hud.app.R.drawable.icon_lg_heavy_right_only));
                sLaneDrawableCache.put(java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_uturn_right_only), resources.getDrawable(com.navdy.hud.app.R.drawable.icon_lg_uturn_right_only));
                sLaneDrawableCache.put(java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_uturn_left_only), resources.getDrawable(com.navdy.hud.app.R.drawable.icon_lg_uturn_left_only));
                sLaneDrawableCache.put(java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_heavy_left_only), resources.getDrawable(com.navdy.hud.app.R.drawable.icon_lg_heavy_left_only));
                sLaneDrawableCache.put(java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_left_only), resources.getDrawable(com.navdy.hud.app.R.drawable.icon_lg_left_only));
                sLaneDrawableCache.put(java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_slight_left_only), resources.getDrawable(com.navdy.hud.app.R.drawable.icon_lg_slight_left_only));
                sLaneDrawableCache.put(java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_glance_whatsapp), resources.getDrawable(com.navdy.hud.app.R.drawable.icon_glance_whatsapp));
                sRightOnlyRecommendedDirection = new java.util.HashMap<>();
                sRightOnlyRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation.Direction.STRAIGHT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_compright_rec_straight));
                sRightOnlyRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation.Direction.SLIGHTLY_RIGHT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_compright_rec_slight_right));
                sRightOnlyRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation.Direction.RIGHT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_compright_rec_right));
                sRightOnlyRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation.Direction.SHARP_RIGHT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_compright_rec_heavy_right));
                sRightOnlyRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation.Direction.U_TURN_RIGHT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_compright_rec_uturn_right));
                sRightOnlyRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation.Direction.SECOND_RIGHT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_glance_whatsapp));
                sRightOnlyRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation.Direction.MERGE_RIGHT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_glance_whatsapp));
                sLaneDrawableCache.put(java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_compright_rec_straight), resources.getDrawable(com.navdy.hud.app.R.drawable.icon_lg_compright_rec_straight));
                sLaneDrawableCache.put(java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_compright_rec_slight_right), resources.getDrawable(com.navdy.hud.app.R.drawable.icon_lg_compright_rec_slight_right));
                sLaneDrawableCache.put(java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_compright_rec_right), resources.getDrawable(com.navdy.hud.app.R.drawable.icon_lg_compright_rec_right));
                sLaneDrawableCache.put(java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_compright_rec_heavy_right), resources.getDrawable(com.navdy.hud.app.R.drawable.icon_lg_compright_rec_heavy_right));
                sLaneDrawableCache.put(java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_compright_rec_uturn_right), resources.getDrawable(com.navdy.hud.app.R.drawable.icon_lg_compright_rec_uturn_right));
                sLaneDrawableCache.put(java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_glance_whatsapp), resources.getDrawable(com.navdy.hud.app.R.drawable.icon_glance_whatsapp));
                sRightOnlyNotRecommendedDirection = new java.util.HashMap<>();
                sRightOnlyNotRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation.Direction.STRAIGHT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_compright_straight));
                sRightOnlyNotRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation.Direction.SLIGHTLY_RIGHT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_compright_slight_right));
                sRightOnlyNotRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation.Direction.RIGHT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_compright_right));
                sRightOnlyNotRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation.Direction.SHARP_RIGHT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_compright_heavy_right));
                sRightOnlyNotRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation.Direction.U_TURN_RIGHT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_compright_uturn_right));
                sRightOnlyNotRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation.Direction.SECOND_RIGHT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_glance_whatsapp));
                sRightOnlyNotRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation.Direction.MERGE_RIGHT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_glance_whatsapp));
                sLaneDrawableCache.put(java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_compright_straight), resources.getDrawable(com.navdy.hud.app.R.drawable.icon_lg_compright_straight));
                sLaneDrawableCache.put(java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_compright_slight_right), resources.getDrawable(com.navdy.hud.app.R.drawable.icon_lg_compright_slight_right));
                sLaneDrawableCache.put(java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_compright_right), resources.getDrawable(com.navdy.hud.app.R.drawable.icon_lg_compright_right));
                sLaneDrawableCache.put(java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_compright_heavy_right), resources.getDrawable(com.navdy.hud.app.R.drawable.icon_lg_compright_heavy_right));
                sLaneDrawableCache.put(java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_compright_uturn_right), resources.getDrawable(com.navdy.hud.app.R.drawable.icon_lg_compright_uturn_right));
                sLaneDrawableCache.put(java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_glance_whatsapp), resources.getDrawable(com.navdy.hud.app.R.drawable.icon_glance_whatsapp));
                sLeftOnlyRecommendedDirection = new java.util.HashMap<>();
                sLeftOnlyRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation.Direction.STRAIGHT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_compleft_rec_straight));
                sLeftOnlyRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation.Direction.SLIGHTLY_LEFT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_compleft_rec_slight_left));
                sLeftOnlyRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation.Direction.LEFT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_compleft_rec_left));
                sLeftOnlyRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation.Direction.SHARP_LEFT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_compleft_rec_heavy_left));
                sLeftOnlyRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation.Direction.U_TURN_LEFT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_compleft_rec_uturn_left));
                sLeftOnlyRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation.Direction.SECOND_LEFT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_glance_whatsapp));
                sLeftOnlyRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation.Direction.MERGE_LEFT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_glance_whatsapp));
                sLaneDrawableCache.put(java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_compleft_rec_straight), resources.getDrawable(com.navdy.hud.app.R.drawable.icon_lg_compleft_rec_straight));
                sLaneDrawableCache.put(java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_compleft_rec_slight_left), resources.getDrawable(com.navdy.hud.app.R.drawable.icon_lg_compleft_rec_slight_left));
                sLaneDrawableCache.put(java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_compleft_rec_left), resources.getDrawable(com.navdy.hud.app.R.drawable.icon_lg_compleft_rec_left));
                sLaneDrawableCache.put(java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_compleft_rec_heavy_left), resources.getDrawable(com.navdy.hud.app.R.drawable.icon_lg_compleft_rec_heavy_left));
                sLaneDrawableCache.put(java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_compleft_rec_uturn_left), resources.getDrawable(com.navdy.hud.app.R.drawable.icon_lg_compleft_rec_uturn_left));
                sLaneDrawableCache.put(java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_glance_whatsapp), resources.getDrawable(com.navdy.hud.app.R.drawable.icon_glance_whatsapp));
                sLeftOnlyNotRecommendedDirection = new java.util.HashMap<>();
                sLeftOnlyNotRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation.Direction.STRAIGHT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_compleft_straight));
                sLeftOnlyNotRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation.Direction.SLIGHTLY_LEFT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_compleft_slight_left));
                sLeftOnlyNotRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation.Direction.LEFT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_compleft_left));
                sLeftOnlyNotRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation.Direction.SHARP_LEFT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_compleft_heavy_left));
                sLeftOnlyNotRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation.Direction.U_TURN_LEFT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_compleft_uturn_left));
                sLeftOnlyNotRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation.Direction.SECOND_LEFT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_glance_whatsapp));
                sLeftOnlyNotRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation.Direction.MERGE_LEFT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_glance_whatsapp));
                sLaneDrawableCache.put(java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_compleft_straight), resources.getDrawable(com.navdy.hud.app.R.drawable.icon_lg_compleft_straight));
                sLaneDrawableCache.put(java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_compleft_slight_left), resources.getDrawable(com.navdy.hud.app.R.drawable.icon_lg_compleft_slight_left));
                sLaneDrawableCache.put(java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_compleft_left), resources.getDrawable(com.navdy.hud.app.R.drawable.icon_lg_compleft_left));
                sLaneDrawableCache.put(java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_compleft_heavy_left), resources.getDrawable(com.navdy.hud.app.R.drawable.icon_lg_compleft_heavy_left));
                sLaneDrawableCache.put(java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_compleft_uturn_left), resources.getDrawable(com.navdy.hud.app.R.drawable.icon_lg_compleft_uturn_left));
                sLaneDrawableCache.put(java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_glance_whatsapp), resources.getDrawable(com.navdy.hud.app.R.drawable.icon_glance_whatsapp));
                sLeftRightRecommendedDirection = new java.util.HashMap<>();
                sLeftRightRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation.Direction.STRAIGHT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_compboth_rec_straight));
                sLeftRightRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation.Direction.SLIGHTLY_RIGHT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_compboth_rec_slight_right));
                sLeftRightRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation.Direction.RIGHT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_compboth_rec_right));
                sLeftRightRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation.Direction.SHARP_RIGHT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_compboth_rec_heavy_right));
                sLeftRightRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation.Direction.SECOND_RIGHT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_glance_whatsapp));
                sLeftRightRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation.Direction.MERGE_RIGHT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_glance_whatsapp));
                sLeftRightRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation.Direction.SLIGHTLY_LEFT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_compboth_rec_slight_left));
                sLeftRightRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation.Direction.LEFT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_compboth_rec_left));
                sLeftRightRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation.Direction.SHARP_LEFT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_compboth_rec_heavy_left));
                sLeftRightRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation.Direction.MERGE_LEFT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_glance_whatsapp));
                sLaneDrawableCache.put(java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_compboth_rec_straight), resources.getDrawable(com.navdy.hud.app.R.drawable.icon_lg_compboth_rec_straight));
                sLaneDrawableCache.put(java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_compboth_rec_slight_right), resources.getDrawable(com.navdy.hud.app.R.drawable.icon_lg_compboth_rec_slight_right));
                sLaneDrawableCache.put(java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_compboth_rec_right), resources.getDrawable(com.navdy.hud.app.R.drawable.icon_lg_compboth_rec_right));
                sLaneDrawableCache.put(java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_compboth_rec_heavy_right), resources.getDrawable(com.navdy.hud.app.R.drawable.icon_lg_compboth_rec_heavy_right));
                sLaneDrawableCache.put(java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_compboth_rec_slight_left), resources.getDrawable(com.navdy.hud.app.R.drawable.icon_lg_compboth_rec_slight_left));
                sLaneDrawableCache.put(java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_compboth_rec_left), resources.getDrawable(com.navdy.hud.app.R.drawable.icon_lg_compboth_rec_left));
                sLaneDrawableCache.put(java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_compboth_rec_heavy_left), resources.getDrawable(com.navdy.hud.app.R.drawable.icon_lg_compboth_rec_heavy_left));
                sLaneDrawableCache.put(java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_glance_whatsapp), resources.getDrawable(com.navdy.hud.app.R.drawable.icon_glance_whatsapp));
                sLeftRightNotRecommendedDirection = new java.util.HashMap<>();
                sLeftRightNotRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation.Direction.STRAIGHT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_compboth_straight));
                sLeftRightNotRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation.Direction.SLIGHTLY_RIGHT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_compboth_slight_right));
                sLeftRightNotRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation.Direction.RIGHT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_compboth_right));
                sLeftRightNotRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation.Direction.SHARP_RIGHT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_compboth_heavy_right));
                sLeftRightNotRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation.Direction.MERGE_RIGHT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_glance_whatsapp));
                sLeftRightNotRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation.Direction.SLIGHTLY_LEFT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_compboth_slight_left));
                sLeftRightNotRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation.Direction.LEFT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_compboth_left));
                sLeftRightNotRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation.Direction.SHARP_LEFT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_compboth_heavy_left));
                sLeftRightNotRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation.Direction.MERGE_LEFT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_glance_whatsapp));
                sLaneDrawableCache.put(java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_compboth_straight), resources.getDrawable(com.navdy.hud.app.R.drawable.icon_lg_compboth_straight));
                sLaneDrawableCache.put(java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_compboth_slight_right), resources.getDrawable(com.navdy.hud.app.R.drawable.icon_lg_compboth_slight_right));
                sLaneDrawableCache.put(java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_compboth_right), resources.getDrawable(com.navdy.hud.app.R.drawable.icon_lg_compboth_right));
                sLaneDrawableCache.put(java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_compboth_heavy_right), resources.getDrawable(com.navdy.hud.app.R.drawable.icon_lg_compboth_heavy_right));
                sLaneDrawableCache.put(java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_compboth_slight_left), resources.getDrawable(com.navdy.hud.app.R.drawable.icon_lg_compboth_slight_left));
                sLaneDrawableCache.put(java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_compboth_left), resources.getDrawable(com.navdy.hud.app.R.drawable.icon_lg_compboth_left));
                sLaneDrawableCache.put(java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_lg_compboth_heavy_left), resources.getDrawable(com.navdy.hud.app.R.drawable.icon_lg_compboth_heavy_left));
                sLaneDrawableCache.put(java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_glance_whatsapp), resources.getDrawable(com.navdy.hud.app.R.drawable.icon_glance_whatsapp));
            }
        }
    }

    public static android.graphics.drawable.Drawable[] getDrawable(java.util.List<com.here.android.mpa.guidance.LaneInformation.Direction> directions, com.navdy.hud.app.maps.MapEvents.LaneData.Position position, com.here.android.mpa.guidance.LaneInformation.Direction onRouteDirection) {
        android.graphics.drawable.Drawable[] ret;
        if (directions == null) {
            return null;
        }
        if (directions.size() == 1) {
            ret = new android.graphics.drawable.Drawable[]{getDrawableForDirection((com.here.android.mpa.guidance.LaneInformation.Direction) directions.get(0), position, sSingleRecommendedDirection, sSingleNotRecommendedDirection)};
        } else {
            com.navdy.hud.app.maps.here.HereLaneInfoBuilder.DirectionType directionType = getDirectionType(directions);
            android.graphics.drawable.Drawable[] drawables = new android.graphics.drawable.Drawable[directions.size()];
            int counter = 0;
            for (com.here.android.mpa.guidance.LaneInformation.Direction d : directions) {
                com.navdy.hud.app.maps.MapEvents.LaneData.Position tempPosition = position;
                if (position == com.navdy.hud.app.maps.MapEvents.LaneData.Position.ON_ROUTE && onRouteDirection != null) {
                    tempPosition = d == onRouteDirection ? com.navdy.hud.app.maps.MapEvents.LaneData.Position.ON_ROUTE : com.navdy.hud.app.maps.MapEvents.LaneData.Position.OFF_ROUTE;
                }
                switch (directionType) {
                    case LEFT:
                        int counter2 = counter + 1;
                        drawables[counter] = getDrawableForDirection(d, tempPosition, sLeftOnlyRecommendedDirection, sLeftOnlyNotRecommendedDirection);
                        counter = counter2;
                        break;
                    case RIGHT:
                        int counter3 = counter + 1;
                        drawables[counter] = getDrawableForDirection(d, tempPosition, sRightOnlyRecommendedDirection, sRightOnlyNotRecommendedDirection);
                        counter = counter3;
                        break;
                    case LEFT_RIGHT:
                        int counter4 = counter + 1;
                        drawables[counter] = getDrawableForDirection(d, tempPosition, sLeftRightRecommendedDirection, sLeftRightNotRecommendedDirection);
                        counter = counter4;
                        break;
                }
            }
            ret = drawables;
        }
        if (ret == null) {
            return ret;
        }
        int nonNulls = 0;
        int nulls = 0;
        for (android.graphics.drawable.Drawable drawable : ret) {
            if (drawable == null) {
                nulls++;
            } else {
                nonNulls++;
            }
        }
        if (nonNulls == 0) {
            return null;
        }
        if (nulls <= 0) {
            return ret;
        }
        android.graphics.drawable.Drawable[] drawables2 = new android.graphics.drawable.Drawable[nonNulls];
        int counter5 = 0;
        for (int i = 0; i < ret.length; i++) {
            if (ret[i] != null) {
                int counter6 = counter5 + 1;
                drawables2[counter5] = ret[i];
                counter5 = counter6;
            }
        }
        return drawables2;
    }

    private static android.graphics.drawable.Drawable getDrawableForDirection(com.here.android.mpa.guidance.LaneInformation.Direction direction, com.navdy.hud.app.maps.MapEvents.LaneData.Position position, java.util.HashMap<com.here.android.mpa.guidance.LaneInformation.Direction, java.lang.Integer> recommendedMap, java.util.HashMap<com.here.android.mpa.guidance.LaneInformation.Direction, java.lang.Integer> notRecommendedMap) {
        java.lang.Integer icon;
        if (position == com.navdy.hud.app.maps.MapEvents.LaneData.Position.ON_ROUTE) {
            icon = (java.lang.Integer) recommendedMap.get(direction);
        } else {
            icon = (java.lang.Integer) notRecommendedMap.get(direction);
        }
        if (icon == null) {
            return null;
        }
        return (android.graphics.drawable.Drawable) sLaneDrawableCache.get(icon);
    }

    private static com.navdy.hud.app.maps.here.HereLaneInfoBuilder.DirectionType getDirectionType(java.util.List<com.here.android.mpa.guidance.LaneInformation.Direction> directions) {
        int left = 0;
        int right = 0;
        for (com.here.android.mpa.guidance.LaneInformation.Direction d : directions) {
            switch (com.navdy.hud.app.maps.here.HereLaneInfoBuilder.Anon1.$SwitchMap$com$here$android$mpa$guidance$LaneInformation$Direction[d.ordinal()]) {
                case 2:
                    right++;
                    break;
                case 3:
                    right++;
                    break;
                case 4:
                    right++;
                    break;
                case 5:
                    left++;
                    break;
                case 6:
                    left++;
                    break;
                case 7:
                    left++;
                    break;
                case 8:
                    left++;
                    break;
                case 9:
                    right++;
                    break;
            }
        }
        if (left > 0 && right > 0) {
            return com.navdy.hud.app.maps.here.HereLaneInfoBuilder.DirectionType.LEFT_RIGHT;
        }
        if (left > 0) {
            return com.navdy.hud.app.maps.here.HereLaneInfoBuilder.DirectionType.LEFT;
        }
        return com.navdy.hud.app.maps.here.HereLaneInfoBuilder.DirectionType.RIGHT;
    }

    public static boolean compareLaneData(java.util.ArrayList<com.navdy.hud.app.maps.MapEvents.LaneData> l1, java.util.ArrayList<com.navdy.hud.app.maps.MapEvents.LaneData> l2) {
        if (l1 == null || l2 == null) {
            return false;
        }
        int size1 = l1.size();
        if (size1 != l2.size()) {
            return false;
        }
        for (int i = 0; i < size1; i++) {
            com.navdy.hud.app.maps.MapEvents.LaneData laneData1 = (com.navdy.hud.app.maps.MapEvents.LaneData) l1.get(i);
            com.navdy.hud.app.maps.MapEvents.LaneData laneData2 = (com.navdy.hud.app.maps.MapEvents.LaneData) l2.get(i);
            if (laneData1 == null || laneData2 == null || laneData1.position != laneData2.position || laneData1.icons == null || laneData2.icons == null || laneData1.icons.length != laneData2.icons.length) {
                return false;
            }
            for (int j = 0; j < laneData1.icons.length; j++) {
                if (laneData1.icons[j] != laneData2.icons[j]) {
                    return false;
                }
            }
        }
        return true;
    }

    public static int getNumDirections(java.util.EnumSet<com.here.android.mpa.guidance.LaneInformation.Direction> directions, com.navdy.hud.app.maps.here.HereLaneInfoBuilder.DirectionType type) {
        int n = 0;
        java.util.Iterator it = directions.iterator();
        while (it.hasNext()) {
            switch (com.navdy.hud.app.maps.here.HereLaneInfoBuilder.Anon1.$SwitchMap$com$here$android$mpa$guidance$LaneInformation$Direction[((com.here.android.mpa.guidance.LaneInformation.Direction) it.next()).ordinal()]) {
                case 2:
                case 3:
                case 4:
                    if (type != com.navdy.hud.app.maps.here.HereLaneInfoBuilder.DirectionType.RIGHT) {
                        break;
                    } else {
                        n++;
                        break;
                    }
                case 6:
                case 7:
                case 8:
                    if (type != com.navdy.hud.app.maps.here.HereLaneInfoBuilder.DirectionType.LEFT) {
                        break;
                    } else {
                        n++;
                        break;
                    }
            }
        }
        return n;
    }

    public static com.here.android.mpa.guidance.LaneInformation.Direction getDirection(java.util.EnumSet<com.here.android.mpa.guidance.LaneInformation.Direction> directions, com.navdy.hud.app.maps.here.HereLaneInfoBuilder.DirectionType type) {
        java.util.Iterator it = directions.iterator();
        while (it.hasNext()) {
            com.here.android.mpa.guidance.LaneInformation.Direction d = (com.here.android.mpa.guidance.LaneInformation.Direction) it.next();
            switch (com.navdy.hud.app.maps.here.HereLaneInfoBuilder.Anon1.$SwitchMap$com$here$android$mpa$guidance$LaneInformation$Direction[d.ordinal()]) {
                case 2:
                case 3:
                case 4:
                    if (type != com.navdy.hud.app.maps.here.HereLaneInfoBuilder.DirectionType.RIGHT) {
                        break;
                    } else {
                        return d;
                    }
                case 6:
                case 7:
                case 8:
                    if (type != com.navdy.hud.app.maps.here.HereLaneInfoBuilder.DirectionType.LEFT) {
                        break;
                    } else {
                        return d;
                    }
            }
        }
        return null;
    }
}
