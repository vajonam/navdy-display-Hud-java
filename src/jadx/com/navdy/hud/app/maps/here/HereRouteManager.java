package com.navdy.hud.app.maps.here;

public class HereRouteManager {
    private static final java.lang.String EMPTY_STR = "";
    public static final long MAX_ALLOWED_NAV_DISPLAY_POSITION_DIFF = 160934;
    private static final double MAX_DISTANCE = 1.5E7d;
    /* access modifiers changed from: private */
    public static final int MAX_ONLINE_ROUTE_CALCULATION_TIME = ((int) java.util.concurrent.TimeUnit.SECONDS.toMillis(20));
    private static final int MAX_ROUTES = com.navdy.hud.app.util.os.SystemProperties.getInt(MAX_ROUTES_PROP, 3);
    private static final java.lang.String MAX_ROUTES_PROP = "persist.sys.routes.max";
    private static final boolean VERBOSE = com.navdy.hud.app.BuildConfig.DEBUG;
    /* access modifiers changed from: private */
    public static volatile java.lang.String activeGeoCalcId;
    /* access modifiers changed from: private */
    public static com.navdy.service.library.events.navigation.NavigationRouteRequest activeGeoRouteRequest;
    private static volatile java.lang.String activeRouteCalcId;
    /* access modifiers changed from: private */
    public static com.navdy.hud.app.maps.here.HereRouteCalculator activeRouteCalculator;
    /* access modifiers changed from: private */
    public static boolean activeRouteCancelledByUser;
    /* access modifiers changed from: private */
    public static com.here.android.mpa.search.GeocodeRequest activeRouteGeocodeRequest;
    private static int activeRouteProgress;
    /* access modifiers changed from: private */
    public static com.navdy.service.library.events.navigation.NavigationRouteRequest activeRouteRequest;
    private static final com.squareup.otto.Bus bus = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getBus();
    /* access modifiers changed from: private */
    public static final android.content.Context context = com.navdy.hud.app.HudApplication.getAppContext();
    /* access modifiers changed from: private */
    public static android.os.Handler handler = new android.os.Handler(android.os.Looper.getMainLooper());
    /* access modifiers changed from: private */
    public static final com.navdy.hud.app.maps.here.HereMapsManager hereMapsManager = com.navdy.hud.app.maps.here.HereMapsManager.getInstance();
    /* access modifiers changed from: private */
    public static final java.lang.Object lockObj = new java.lang.Object();
    /* access modifiers changed from: private */
    public static final com.navdy.hud.app.maps.MapsEventHandler mapsEventHandler = com.navdy.hud.app.maps.MapsEventHandler.getInstance();
    /* access modifiers changed from: private */
    public static final java.util.HashSet<java.lang.String> pendingRouteRequestIds = new java.util.HashSet<>();
    private static final java.util.Queue<com.navdy.service.library.events.navigation.NavigationRouteRequest> pendingRouteRequestQueue = new java.util.LinkedList();
    private static com.navdy.hud.app.maps.MapEvents.RouteCalculationEvent routeCalculationEvent;
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.maps.here.HereRouteManager.class);

    static class Anon1 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.service.library.events.navigation.NavigationRouteRequest val$routeRequest;

        Anon1(com.navdy.service.library.events.navigation.NavigationRouteRequest navigationRouteRequest) {
            this.val$routeRequest = navigationRouteRequest;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:52:?, code lost:
            r3 = com.navdy.hud.app.maps.here.HereNavigationManager.getInstance();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:53:0x018b, code lost:
            if (r3.hasArrived() == false) goto L_0x019d;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:54:0x018d, code lost:
            com.navdy.hud.app.maps.here.HereRouteManager.access$Anon100().v("stop arrival mode");
            r3.setIgnoreArrived(true);
            r3.stopNavigation();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:55:0x019d, code lost:
            com.navdy.hud.app.maps.here.HereRouteManager.access$Anon700(r9.val$routeRequest, r1);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:64:?, code lost:
            return;
         */
        /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
        public void run() {
            try {
                com.navdy.hud.app.maps.here.HereRouteManager.printRouteRequest(this.val$routeRequest);
                if (!com.navdy.hud.app.maps.here.HereRouteManager.hereMapsManager.isInitialized()) {
                    com.navdy.hud.app.maps.here.HereRouteManager.sLogger.i("engine not ready");
                    com.navdy.hud.app.maps.here.HereRouteManager.returnErrorResponse(com.navdy.service.library.events.RequestStatus.REQUEST_NOT_READY, this.val$routeRequest, com.navdy.hud.app.maps.here.HereRouteManager.context.getString(com.navdy.hud.app.R.string.map_engine_not_ready));
                    return;
                }
                com.navdy.hud.app.maps.here.HereLocationFixManager hereLocationFixManager = com.navdy.hud.app.maps.here.HereRouteManager.hereMapsManager.getLocationFixManager();
                if (hereLocationFixManager == null) {
                    com.navdy.hud.app.maps.here.HereRouteManager.sLogger.i("engine not ready, location fix manager n/a");
                    com.navdy.hud.app.maps.here.HereRouteManager.returnErrorResponse(com.navdy.service.library.events.RequestStatus.REQUEST_NOT_READY, this.val$routeRequest, com.navdy.hud.app.maps.here.HereRouteManager.context.getString(com.navdy.hud.app.R.string.map_engine_not_ready));
                } else if (!com.navdy.hud.app.framework.network.NetworkStateManager.getInstance().isNetworkStateInitialized()) {
                    com.navdy.hud.app.maps.here.HereRouteManager.sLogger.i("network state is not initialized yet");
                    com.navdy.hud.app.maps.here.HereRouteManager.returnErrorResponse(com.navdy.service.library.events.RequestStatus.REQUEST_NOT_READY, this.val$routeRequest, com.navdy.hud.app.maps.here.HereRouteManager.context.getString(com.navdy.hud.app.R.string.network_not_initialized));
                } else {
                    com.here.android.mpa.common.GeoCoordinate geoPosition = com.navdy.hud.app.maps.here.HereRouteManager.hereMapsManager.getRouteStartPoint();
                    if (geoPosition == null) {
                        geoPosition = hereLocationFixManager.getLastGeoCoordinate();
                    } else {
                        com.navdy.hud.app.maps.here.HereRouteManager.sLogger.v("using debug start point:" + geoPosition);
                    }
                    if (geoPosition == null) {
                        com.navdy.hud.app.maps.here.HereRouteManager.sLogger.i("start location n/a");
                        com.navdy.hud.app.maps.here.HereRouteManager.returnErrorResponse(com.navdy.service.library.events.RequestStatus.REQUEST_NO_LOCATION_SERVICE, this.val$routeRequest, com.navdy.hud.app.maps.here.HereRouteManager.context.getString(com.navdy.hud.app.R.string.no_current_position));
                    } else if (this.val$routeRequest.requestId == null) {
                        com.navdy.hud.app.maps.here.HereRouteManager.sLogger.i("no request id specified");
                        com.navdy.hud.app.maps.here.HereRouteManager.returnErrorResponse(com.navdy.service.library.events.RequestStatus.REQUEST_INVALID_REQUEST, this.val$routeRequest, "no route id");
                    } else {
                        com.navdy.hud.app.maps.here.HereRouteManager.sLogger.i("reset traffic reroute listener");
                        com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().resetTrafficRerouteListener();
                        synchronized (com.navdy.hud.app.maps.here.HereRouteManager.lockObj) {
                            java.lang.String activeRouteId = null;
                            if (com.navdy.hud.app.maps.here.HereRouteManager.activeRouteRequest != null || com.navdy.hud.app.maps.here.HereRouteManager.isUIShowingRouteCalculation()) {
                                if (com.navdy.hud.app.maps.here.HereMapUtil.isSavedRoute(this.val$routeRequest)) {
                                    com.navdy.hud.app.maps.here.HereRouteManager.sLogger.i("saved route cannot override an existing route calculation");
                                    com.navdy.hud.app.maps.here.HereRouteManager.returnErrorResponse(com.navdy.service.library.events.RequestStatus.REQUEST_ALREADY_IN_PROGRESS, this.val$routeRequest, null);
                                    return;
                                } else if (com.navdy.hud.app.maps.here.HereRouteManager.activeRouteRequest != null) {
                                    activeRouteId = com.navdy.hud.app.maps.here.HereRouteManager.activeRouteRequest.requestId;
                                }
                            }
                            if (com.navdy.hud.app.maps.here.HereRouteManager.pendingRouteRequestIds.contains(this.val$routeRequest.requestId) || android.text.TextUtils.equals(this.val$routeRequest.requestId, activeRouteId)) {
                                com.navdy.hud.app.maps.here.HereRouteManager.sLogger.v("route request[" + this.val$routeRequest.requestId + "] already received");
                            }
                        }
                    }
                }
            } catch (Throwable t) {
                com.navdy.hud.app.maps.here.HereRouteManager.sLogger.e("", t);
                com.navdy.hud.app.maps.here.HereRouteManager.returnErrorResponse(com.navdy.service.library.events.RequestStatus.REQUEST_UNKNOWN_ERROR, this.val$routeRequest, com.navdy.hud.app.maps.here.HereRouteManager.context.getString(com.navdy.hud.app.R.string.unknown_error));
            }
        }
    }

    static class Anon2 implements com.navdy.hud.app.maps.here.HerePlacesManager.GeoCodeCallback {
        final /* synthetic */ boolean val$factoringInTraffic;
        final /* synthetic */ com.navdy.service.library.events.navigation.NavigationRouteRequest val$request;
        final /* synthetic */ com.here.android.mpa.common.GeoCoordinate val$startPoint;
        final /* synthetic */ java.util.List val$waypoints;

        Anon2(com.navdy.service.library.events.navigation.NavigationRouteRequest navigationRouteRequest, com.here.android.mpa.common.GeoCoordinate geoCoordinate, java.util.List list, boolean z) {
            this.val$request = navigationRouteRequest;
            this.val$startPoint = geoCoordinate;
            this.val$waypoints = list;
            this.val$factoringInTraffic = z;
        }

        /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
        public void result(com.here.android.mpa.search.ErrorCode errorCode, java.util.List<com.here.android.mpa.search.Location> locationList) {
            boolean matchesCurrentRequestId;
            try {
                if (errorCode == com.here.android.mpa.search.ErrorCode.CANCELLED) {
                    com.navdy.hud.app.maps.here.HereRouteManager.sLogger.v("geocode request has been cancelled [" + this.val$request.requestId + "]");
                    return;
                }
                synchronized (com.navdy.hud.app.maps.here.HereRouteManager.lockObj) {
                    matchesCurrentRequestId = android.text.TextUtils.equals(this.val$request.requestId, com.navdy.hud.app.maps.here.HereRouteManager.activeGeoCalcId);
                }
                if (!matchesCurrentRequestId) {
                    com.navdy.hud.app.maps.here.HereRouteManager.sLogger.v("geocode request is not valid active:" + com.navdy.hud.app.maps.here.HereRouteManager.activeGeoCalcId + " request:" + this.val$request.requestId);
                    com.navdy.hud.app.maps.MapsEventHandler.getInstance().sendEventToClient(new com.navdy.service.library.events.navigation.NavigationRouteResponse(com.navdy.service.library.events.RequestStatus.REQUEST_CANCELLED, null, this.val$request.destination, null, null, java.lang.Boolean.valueOf(false), this.val$request.requestId));
                    return;
                }
                synchronized (com.navdy.hud.app.maps.here.HereRouteManager.lockObj) {
                    com.navdy.hud.app.maps.here.HereRouteManager.activeRouteGeocodeRequest = null;
                    com.navdy.hud.app.maps.here.HereRouteManager.activeGeoCalcId = null;
                    com.navdy.hud.app.maps.here.HereRouteManager.activeGeoRouteRequest = null;
                }
                if (errorCode != com.here.android.mpa.search.ErrorCode.NONE || locationList == null || locationList.size() <= 0) {
                    com.navdy.hud.app.maps.here.HereRouteManager.sLogger.v("could not geocode, Street Address placesSearch error:" + errorCode.name());
                    com.navdy.hud.app.maps.here.HereRouteManager.returnErrorResponse(com.navdy.service.library.events.RequestStatus.REQUEST_SERVICE_ERROR, this.val$request, com.navdy.hud.app.maps.here.HereRouteManager.context.getString(com.navdy.hud.app.R.string.geocoding_failed));
                    return;
                }
                com.navdy.hud.app.maps.here.HereRouteManager.sLogger.v("Street Address results size=" + locationList.size());
                com.here.android.mpa.common.GeoCoordinate newEndPoint = ((com.here.android.mpa.search.Location) locationList.get(0)).getCoordinate();
                com.navdy.hud.app.maps.here.HereRouteManager.sLogger.v("Street Address placesSearch success: new-end-geo:" + newEndPoint);
                com.navdy.hud.app.maps.here.HereRouteManager.routeSearch(this.val$request, this.val$startPoint, this.val$waypoints, newEndPoint, this.val$factoringInTraffic);
            } catch (Throwable t) {
                com.navdy.hud.app.maps.here.HereRouteManager.sLogger.v("Street Address placesSearch exception", t);
                com.navdy.hud.app.maps.here.HereRouteManager.returnErrorResponse(com.navdy.service.library.events.RequestStatus.REQUEST_UNKNOWN_ERROR, this.val$request, com.navdy.hud.app.maps.here.HereRouteManager.context.getString(com.navdy.hud.app.R.string.geocoding_failed));
            }
        }
    }

    static class Anon3 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.hud.app.maps.here.HereRouteCalculator val$traffic;

        Anon3(com.navdy.hud.app.maps.here.HereRouteCalculator hereRouteCalculator) {
            this.val$traffic = hereRouteCalculator;
        }

        public void run() {
            com.navdy.hud.app.maps.here.HereRouteManager.sLogger.w("cancel traffic based route,max time reached:" + com.navdy.hud.app.maps.here.HereRouteManager.MAX_ONLINE_ROUTE_CALCULATION_TIME);
            this.val$traffic.cancel();
        }
    }

    static class Anon4 implements com.navdy.hud.app.maps.here.HereRouteCalculator.RouteCalculatorListener {
        final /* synthetic */ com.here.android.mpa.common.GeoCoordinate val$endPoint;
        final /* synthetic */ com.navdy.service.library.events.navigation.NavigationRouteRequest val$request;
        final /* synthetic */ java.lang.Runnable val$runnable;
        final /* synthetic */ com.here.android.mpa.common.GeoCoordinate val$startPoint;
        final /* synthetic */ java.util.List val$waypoints;

        Anon4(com.navdy.service.library.events.navigation.NavigationRouteRequest navigationRouteRequest, java.lang.Runnable runnable, com.here.android.mpa.common.GeoCoordinate geoCoordinate, java.util.List list, com.here.android.mpa.common.GeoCoordinate geoCoordinate2) {
            this.val$request = navigationRouteRequest;
            this.val$runnable = runnable;
            this.val$startPoint = geoCoordinate;
            this.val$waypoints = list;
            this.val$endPoint = geoCoordinate2;
        }

        public void postSuccess(java.util.ArrayList<com.navdy.service.library.events.navigation.NavigationRouteResult> outgoingResults) {
            com.navdy.hud.app.maps.here.HereRouteManager.sLogger.v("got result for traffic");
            com.navdy.hud.app.maps.here.HereRouteManager.returnSuccessResponse(this.val$request, outgoingResults, true);
        }

        public void error(com.here.android.mpa.routing.RoutingError error, java.lang.Throwable t) {
            com.navdy.hud.app.maps.here.HereRouteManager.handler.removeCallbacks(this.val$runnable);
            if (error != null) {
                com.navdy.hud.app.maps.here.HereRouteManager.sLogger.v("got error for traffic [" + error.name() + "]");
            } else {
                com.navdy.hud.app.maps.here.HereRouteManager.sLogger.v("got error for traffic", t);
            }
            synchronized (com.navdy.hud.app.maps.here.HereRouteManager.lockObj) {
                if (com.navdy.hud.app.maps.here.HereRouteManager.activeRouteCancelledByUser) {
                    com.navdy.hud.app.maps.here.HereRouteManager.sLogger.v("user cancelled routing");
                    com.navdy.hud.app.maps.here.HereRouteManager.returnErrorResponse(com.navdy.service.library.events.RequestStatus.REQUEST_CANCELLED, this.val$request, null);
                    return;
                }
                com.navdy.hud.app.maps.here.HereRouteManager.sLogger.v("launching no traffic");
                com.navdy.hud.app.maps.here.HereRouteManager.routeSearchNoTraffic(this.val$request, this.val$startPoint, this.val$waypoints, this.val$endPoint, false);
            }
        }

        public void progress(int progress) {
        }

        public void preSuccess() {
            synchronized (com.navdy.hud.app.maps.here.HereRouteManager.lockObj) {
                com.navdy.hud.app.maps.here.HereRouteManager.activeRouteCalculator = null;
            }
            com.navdy.hud.app.maps.here.HereRouteManager.handler.removeCallbacks(this.val$runnable);
            com.navdy.hud.app.maps.here.HereRouteManager.sLogger.v("route successfully calculated, post calc in progress");
        }
    }

    static class Anon5 implements com.navdy.hud.app.maps.here.HereRouteCalculator.RouteCalculatorListener {
        final /* synthetic */ com.navdy.service.library.events.navigation.NavigationRouteRequest val$request;

        Anon5(com.navdy.service.library.events.navigation.NavigationRouteRequest navigationRouteRequest) {
            this.val$request = navigationRouteRequest;
        }

        public void postSuccess(java.util.ArrayList<com.navdy.service.library.events.navigation.NavigationRouteResult> outgoingResults) {
            com.navdy.hud.app.maps.here.HereRouteManager.returnSuccessResponse(this.val$request, outgoingResults, false);
        }

        public void error(com.here.android.mpa.routing.RoutingError error, java.lang.Throwable t) {
            java.lang.String errorStr;
            if (error != null) {
                errorStr = error.name();
            } else {
                errorStr = com.navdy.hud.app.maps.here.HereRouteManager.context.getString(com.navdy.hud.app.R.string.unknown_error);
            }
            synchronized (com.navdy.hud.app.maps.here.HereRouteManager.lockObj) {
                if (com.navdy.hud.app.maps.here.HereRouteManager.activeRouteCancelledByUser) {
                    com.navdy.hud.app.maps.here.HereRouteManager.sLogger.v("user cancelled routing");
                    com.navdy.hud.app.maps.here.HereRouteManager.returnErrorResponse(com.navdy.service.library.events.RequestStatus.REQUEST_CANCELLED, this.val$request, null);
                } else {
                    com.navdy.hud.app.maps.here.HereRouteManager.returnErrorResponse(com.navdy.service.library.events.RequestStatus.REQUEST_SERVICE_ERROR, this.val$request, errorStr);
                }
            }
        }

        public void progress(int progress) {
        }

        public void preSuccess() {
            synchronized (com.navdy.hud.app.maps.here.HereRouteManager.lockObj) {
                com.navdy.hud.app.maps.here.HereRouteManager.activeRouteCalculator = null;
            }
        }
    }

    static class Anon6 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.service.library.events.navigation.RouteManeuverRequest val$routeManeuverRequest;

        Anon6(com.navdy.service.library.events.navigation.RouteManeuverRequest routeManeuverRequest) {
            this.val$routeManeuverRequest = routeManeuverRequest;
        }

        public void run() {
            com.navdy.hud.app.maps.MapEvents.ManeuverDisplay display;
            try {
                if (this.val$routeManeuverRequest.routeId != null) {
                    long l1 = android.os.SystemClock.elapsedRealtime();
                    com.navdy.hud.app.maps.here.HereRouteCache.RouteInfo routeInfo = com.navdy.hud.app.maps.here.HereRouteCache.getInstance().getRoute(this.val$routeManeuverRequest.routeId);
                    if (routeInfo != null) {
                        java.lang.String streetAddress = null;
                        if (routeInfo.routeRequest != null) {
                            streetAddress = routeInfo.routeRequest.streetAddress;
                        }
                        java.util.List<com.here.android.mpa.routing.Maneuver> list = routeInfo.route.getManeuvers();
                        java.util.ArrayList arrayList = new java.util.ArrayList(20);
                        int len = list.size();
                        com.here.android.mpa.routing.Maneuver previous = null;
                        com.here.android.mpa.routing.Maneuver current = null;
                        for (int i = 0; i < len; i++) {
                            if (current != null) {
                                previous = current;
                            }
                            current = (com.here.android.mpa.routing.Maneuver) list.get(i);
                            com.here.android.mpa.routing.Maneuver after = null;
                            if (i < len - 1) {
                                after = (com.here.android.mpa.routing.Maneuver) list.get(i + 1);
                            }
                            int maneuverTime = 0;
                            if (i == 0) {
                                display = com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.getStartManeuverDisplay(current, after);
                            } else {
                                display = com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.getManeuverDisplay(current, after == null, (long) current.getDistanceToNextManeuver(), streetAddress, previous, routeInfo.routeRequest, after, true, false, null);
                                if (after != null) {
                                    maneuverTime = (int) ((after.getStartTime().getTime() - current.getStartTime().getTime()) / 1000);
                                }
                            }
                            arrayList.add(new com.navdy.service.library.events.navigation.RouteManeuver(display.currentRoad, display.navigationTurn, display.pendingRoad, java.lang.Float.valueOf(display.distance), display.distanceUnit, java.lang.Integer.valueOf(maneuverTime)));
                        }
                        com.navdy.hud.app.maps.here.HereRouteManager.sLogger.v("Time to build maneuver list:" + (android.os.SystemClock.elapsedRealtime() - l1));
                        com.navdy.hud.app.maps.here.HereRouteManager.mapsEventHandler.sendEventToClient(new com.navdy.service.library.events.navigation.RouteManeuverResponse(com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS, null, arrayList));
                        return;
                    }
                }
                com.navdy.hud.app.maps.here.HereRouteManager.mapsEventHandler.sendEventToClient(new com.navdy.service.library.events.navigation.RouteManeuverResponse(com.navdy.service.library.events.RequestStatus.REQUEST_INVALID_REQUEST, null, null));
            } catch (Throwable t) {
                com.navdy.hud.app.maps.here.HereRouteManager.mapsEventHandler.sendEventToClient(new com.navdy.service.library.events.navigation.RouteManeuverResponse(com.navdy.service.library.events.RequestStatus.REQUEST_SERVICE_ERROR, t.toString(), null));
                com.navdy.hud.app.maps.here.HereRouteManager.sLogger.e(t);
            }
        }
    }

    static class Anon7 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.service.library.events.navigation.NavigationRouteRequest val$pendingRequest;

        Anon7(com.navdy.service.library.events.navigation.NavigationRouteRequest navigationRouteRequest) {
            this.val$pendingRequest = navigationRouteRequest;
        }

        public void run() {
            com.navdy.hud.app.maps.here.HereRouteManager.sLogger.v("launched pending request");
            com.navdy.hud.app.maps.here.HereRouteManager.handleRouteRequest(this.val$pendingRequest);
        }
    }

    static /* synthetic */ class Anon8 {
        static final /* synthetic */ int[] $SwitchMap$com$here$android$mpa$routing$RouteOptions$Type = new int[com.here.android.mpa.routing.RouteOptions.Type.values().length];

        static {
            try {
                $SwitchMap$com$here$android$mpa$routing$RouteOptions$Type[com.here.android.mpa.routing.RouteOptions.Type.SHORTEST.ordinal()] = 1;
            } catch (java.lang.NoSuchFieldError e) {
            }
            try {
                $SwitchMap$com$here$android$mpa$routing$RouteOptions$Type[com.here.android.mpa.routing.RouteOptions.Type.FASTEST.ordinal()] = 2;
            } catch (java.lang.NoSuchFieldError e2) {
            }
        }
    }

    public static void handleRouteRequest(com.navdy.service.library.events.navigation.NavigationRouteRequest routeRequest) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.maps.here.HereRouteManager.Anon1(routeRequest), 19);
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:60:0x0412=Splitter:B:60:0x0412, B:67:0x0425=Splitter:B:67:0x0425} */
    public static void handleRouteRequest(com.navdy.service.library.events.navigation.NavigationRouteRequest request, com.here.android.mpa.common.GeoCoordinate startPoint) {
        com.here.android.mpa.common.GeoCoordinate geoCheck;
        boolean b;
        long l1 = android.os.SystemClock.elapsedRealtime();
        java.util.ArrayList arrayList = new java.util.ArrayList();
        if (request.waypoints != null) {
            for (com.navdy.service.library.events.location.Coordinate waypoint : request.waypoints) {
                com.here.android.mpa.common.GeoCoordinate geoCoordinate = new com.here.android.mpa.common.GeoCoordinate(waypoint.latitude.doubleValue(), waypoint.longitude.doubleValue());
                arrayList.add(geoCoordinate);
            }
        }
        if (request.destinationDisplay == null || request.destinationDisplay.latitude.doubleValue() == 0.0d || request.destinationDisplay.longitude.doubleValue() == 0.0d || request.destination.latitude == request.destinationDisplay.latitude || request.destination.longitude == request.destinationDisplay.longitude) {
            sLogger.v("display/nav distance check pass: no display pos");
            geoCheck = new com.here.android.mpa.common.GeoCoordinate(request.destination.latitude.doubleValue(), request.destination.longitude.doubleValue());
        } else {
            long d = (long) new com.here.android.mpa.common.GeoCoordinate(request.destinationDisplay.latitude.doubleValue(), request.destinationDisplay.longitude.doubleValue()).distanceTo(new com.here.android.mpa.common.GeoCoordinate(request.destination.latitude.doubleValue(), request.destination.longitude.doubleValue()));
            if (d >= MAX_ALLOWED_NAV_DISPLAY_POSITION_DIFF) {
                geoCheck = new com.here.android.mpa.common.GeoCoordinate(request.destinationDisplay.latitude.doubleValue(), request.destinationDisplay.longitude.doubleValue());
                sLogger.e("display/nav distance check failed:" + d);
                com.navdy.hud.app.analytics.AnalyticsSupport.recordBadRoutePosition(request.destinationDisplay.latitude + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA + request.destinationDisplay.longitude, request.destination.latitude + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA + request.destination.longitude, request.streetAddress);
            } else {
                sLogger.v("display/nav distance check pass:" + d);
                geoCheck = new com.here.android.mpa.common.GeoCoordinate(request.destination.latitude.doubleValue(), request.destination.longitude.doubleValue());
            }
        }
        com.here.android.mpa.common.GeoCoordinate endPoint = geoCheck;
        sLogger.v("handleRouteRequest start=" + startPoint + ", end=" + endPoint);
        boolean geoCode = false;
        if (!java.lang.Boolean.TRUE.equals(request.geoCodeStreetAddress)) {
            double distance = startPoint.distanceTo(endPoint);
            if (distance > MAX_DISTANCE) {
                sLogger.e("distance between start and endpoint:" + distance + " max:" + MAX_DISTANCE);
                returnErrorResponse(com.navdy.service.library.events.RequestStatus.REQUEST_INVALID_REQUEST, request, context.getString(com.navdy.hud.app.R.string.long_drive_error));
                return;
            }
        } else if (android.text.TextUtils.isEmpty(request.streetAddress)) {
            returnErrorResponse(com.navdy.service.library.events.RequestStatus.REQUEST_INVALID_REQUEST, request, context.getString(com.navdy.hud.app.R.string.unknown_address));
            return;
        } else {
            geoCode = true;
        }
        if (hereMapsManager.isEngineOnline()) {
            b = true;
        } else {
            b = false;
        }
        boolean factoringInTraffic = b;
        sLogger.v("maps engine online:" + factoringInTraffic);
        if (geoCode) {
            sLogger.v("Street Address:" + request.streetAddress);
            synchronized (lockObj) {
                if (isRouteCalcInProgress()) {
                    if (request.cancelCurrent == null || !request.cancelCurrent.booleanValue()) {
                        returnErrorResponse(com.navdy.service.library.events.RequestStatus.REQUEST_ALREADY_IN_PROGRESS, request, activeRouteCalcId);
                        return;
                    } else if (activeRouteCalculator != null) {
                        sLogger.v("geo:route cancelling current request :" + activeRouteCalcId);
                        activeRouteCalculator.cancel();
                        activeRouteCalculator = null;
                        returnErrorResponse(com.navdy.service.library.events.RequestStatus.REQUEST_CANCELLED, activeRouteRequest, activeRouteCalcId);
                    }
                }
                if (isRouteCalcGeocodeRequestInProgress()) {
                    if (request.cancelCurrent == null || !request.cancelCurrent.booleanValue()) {
                        returnErrorResponse(com.navdy.service.library.events.RequestStatus.REQUEST_ALREADY_IN_PROGRESS, request, activeGeoCalcId);
                        return;
                    }
                    cancelCurrentGeocodeRequest();
                }
                activeGeoCalcId = request.requestId;
                activeGeoRouteRequest = request;
                java.lang.String str = request.streetAddress;
                com.navdy.hud.app.maps.here.HereRouteManager.Anon2 anon2 = new com.navdy.hud.app.maps.here.HereRouteManager.Anon2(request, startPoint, arrayList, factoringInTraffic);
                activeRouteGeocodeRequest = com.navdy.hud.app.maps.here.HerePlacesManager.geoCodeStreetAddress(startPoint, str, 100000, anon2);
                if (activeRouteGeocodeRequest == null) {
                    activeRouteGeocodeRequest = null;
                    activeGeoCalcId = null;
                    sLogger.v("geocode request failed");
                } else {
                    sLogger.v("geocode request triggered");
                }
            }
        } else {
            sLogger.v("geocoding not set");
            routeSearch(request, startPoint, arrayList, endPoint, factoringInTraffic);
        }
        sLogger.v("handleRouteRequest- time-1 =" + (android.os.SystemClock.elapsedRealtime() - l1));
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0084, code lost:
        if (r8 == false) goto L_0x008a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0086, code lost:
        routeSearchTraffic(r4, r5, r6, r7, true);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x008a, code lost:
        routeSearchNoTraffic(r4, r5, r6, r7, true);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:?, code lost:
        return;
     */
    public static void routeSearch(com.navdy.service.library.events.navigation.NavigationRouteRequest request, com.here.android.mpa.common.GeoCoordinate startPoint, java.util.List<com.here.android.mpa.common.GeoCoordinate> waypoints, com.here.android.mpa.common.GeoCoordinate endPoint, boolean factoringInTraffic) {
        synchronized (lockObj) {
            if (isRouteCalcGeocodeRequestInProgress()) {
                if (request.cancelCurrent == null || !request.cancelCurrent.booleanValue()) {
                    returnErrorResponse(com.navdy.service.library.events.RequestStatus.REQUEST_ALREADY_IN_PROGRESS, request, activeGeoCalcId);
                    return;
                }
                cancelCurrentGeocodeRequest();
            }
            if (isRouteCalcInProgress()) {
                if (request.cancelCurrent == null || !request.cancelCurrent.booleanValue()) {
                    returnErrorResponse(com.navdy.service.library.events.RequestStatus.REQUEST_ALREADY_IN_PROGRESS, request, activeRouteCalcId);
                } else {
                    pendingRouteRequestQueue.add(request);
                    pendingRouteRequestIds.add(request.requestId);
                    sLogger.v("add to pending Queue");
                    if (activeRouteCancelledByUser) {
                        sLogger.v("cancellation in progress");
                        return;
                    }
                    sLogger.v("cancelling current request :" + activeRouteCalcId);
                    activeRouteCancelledByUser = true;
                    activeRouteCalculator.cancel();
                }
            }
        }
    }

    private static void routeSearchTraffic(com.navdy.service.library.events.navigation.NavigationRouteRequest request, com.here.android.mpa.common.GeoCoordinate startPoint, java.util.List<com.here.android.mpa.common.GeoCoordinate> waypoints, com.here.android.mpa.common.GeoCoordinate endPoint, boolean inProgressRouteCheck) {
        com.navdy.hud.app.maps.here.HereRouteCalculator traffic = new com.navdy.hud.app.maps.here.HereRouteCalculator(sLogger, VERBOSE);
        if (inProgressRouteCheck) {
            synchronized (lockObj) {
                activeRouteCalcId = request.requestId;
                activeRouteRequest = request;
                activeRouteProgress = 0;
                activeRouteCalculator = traffic;
                returnStatusResponse(activeRouteCalcId, activeRouteProgress, activeRouteRequest.requestId);
            }
        }
        java.lang.Runnable runnable = new com.navdy.hud.app.maps.here.HereRouteManager.Anon3(traffic);
        sLogger.v("traffic route calc started");
        handler.postDelayed(runnable, (long) MAX_ONLINE_ROUTE_CALCULATION_TIME);
        com.here.android.mpa.routing.RouteOptions routeOptions = hereMapsManager.getRouteOptions();
        synchronized (lockObj) {
            routeCalculationEvent = new com.navdy.hud.app.maps.MapEvents.RouteCalculationEvent();
            routeCalculationEvent.state = com.navdy.hud.app.maps.MapEvents.RouteCalculationState.STARTED;
            routeCalculationEvent.start = startPoint;
            routeCalculationEvent.waypoints = waypoints;
            routeCalculationEvent.end = endPoint;
            routeCalculationEvent.request = request;
            routeCalculationEvent.routeOptions = routeOptions;
            bus.post(routeCalculationEvent);
        }
        traffic.calculateRoute(request, startPoint, waypoints, endPoint, true, new com.navdy.hud.app.maps.here.HereRouteManager.Anon4(request, runnable, startPoint, waypoints, endPoint), MAX_ROUTES, routeOptions, true);
    }

    /* access modifiers changed from: private */
    public static void routeSearchNoTraffic(com.navdy.service.library.events.navigation.NavigationRouteRequest request, com.here.android.mpa.common.GeoCoordinate startPoint, java.util.List<com.here.android.mpa.common.GeoCoordinate> waypoints, com.here.android.mpa.common.GeoCoordinate endPoint, boolean newRequest) {
        com.navdy.hud.app.maps.here.HereRouteCalculator noTraffic = new com.navdy.hud.app.maps.here.HereRouteCalculator(sLogger, VERBOSE);
        com.here.android.mpa.routing.RouteOptions routeOptions = hereMapsManager.getRouteOptions();
        if (newRequest) {
            activeRouteCalcId = request.requestId;
            activeRouteRequest = request;
            activeRouteProgress = 0;
            activeRouteCalculator = noTraffic;
            returnStatusResponse(activeRouteCalcId, activeRouteProgress, activeRouteRequest.requestId);
            synchronized (lockObj) {
                routeCalculationEvent = new com.navdy.hud.app.maps.MapEvents.RouteCalculationEvent();
                routeCalculationEvent.state = com.navdy.hud.app.maps.MapEvents.RouteCalculationState.STARTED;
                routeCalculationEvent.start = startPoint;
                routeCalculationEvent.waypoints = waypoints;
                routeCalculationEvent.end = endPoint;
                routeCalculationEvent.request = request;
                routeCalculationEvent.routeOptions = routeOptions;
                bus.post(routeCalculationEvent);
            }
        } else {
            synchronized (lockObj) {
                if (activeRouteCalculator != null) {
                    sLogger.v("replaced active route calc with no traffic");
                    activeRouteCalculator = noTraffic;
                }
            }
        }
        sLogger.v("no-traffic route calc started");
        noTraffic.calculateRoute(request, startPoint, waypoints, endPoint, false, new com.navdy.hud.app.maps.here.HereRouteManager.Anon5(request), MAX_ROUTES, routeOptions, true);
    }

    private static void cancelCurrentGeocodeRequest() {
        synchronized (lockObj) {
            if (activeRouteGeocodeRequest != null) {
                try {
                    activeRouteGeocodeRequest.cancel();
                    sLogger.v("geo: geocode request cancelled:" + activeGeoCalcId);
                    if (activeGeoRouteRequest != null) {
                        returnErrorResponse(com.navdy.service.library.events.RequestStatus.REQUEST_CANCELLED, activeGeoRouteRequest, null);
                    }
                } catch (Throwable t) {
                    sLogger.e(t);
                }
                activeRouteGeocodeRequest = null;
                activeGeoCalcId = null;
                activeGeoRouteRequest = null;
            }
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0041, code lost:
        clearActiveRouteCalc();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x005b, code lost:
        if (1 == 0) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x005d, code lost:
        clearActiveRouteCalc();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x003f, code lost:
        if (0 == 0) goto L_?;
     */
    public static void returnErrorResponse(com.navdy.service.library.events.RequestStatus status, com.navdy.service.library.events.navigation.NavigationRouteRequest request, java.lang.String errorText) {
        try {
            sLogger.e(status + ": " + errorText);
            com.navdy.service.library.events.navigation.NavigationRouteResponse response = new com.navdy.service.library.events.navigation.NavigationRouteResponse(status, errorText, request.destination, null, null, java.lang.Boolean.valueOf(false), request.requestId);
            com.navdy.hud.app.maps.MapsEventHandler.getInstance().sendEventToClient(response);
            synchronized (lockObj) {
                if (status != com.navdy.service.library.events.RequestStatus.REQUEST_ALREADY_IN_PROGRESS) {
                    if (routeCalculationEvent != null) {
                        routeCalculationEvent.response = response;
                        routeCalculationEvent.state = com.navdy.hud.app.maps.MapEvents.RouteCalculationState.STOPPED;
                        bus.post(routeCalculationEvent);
                    }
                }
            }
        } catch (Throwable t) {
            try {
                sLogger.e(t);
            } finally {
                if (1 != 0) {
                    clearActiveRouteCalc();
                }
            }
        }
    }

    private static void returnStatusResponse(java.lang.String handle, int progress, java.lang.String requestId) {
        try {
            sLogger.v("handle[" + handle + "] progress [" + progress + "]");
            com.navdy.hud.app.maps.MapsEventHandler.getInstance().sendEventToClient(new com.navdy.service.library.events.navigation.NavigationRouteStatus(handle, java.lang.Integer.valueOf(progress), requestId));
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }

    /* access modifiers changed from: private */
    public static void returnSuccessResponse(com.navdy.service.library.events.navigation.NavigationRouteRequest request, java.util.ArrayList<com.navdy.service.library.events.navigation.NavigationRouteResult> outgoingResults, boolean factoringInTraffic) {
        boolean booleanValue;
        try {
            com.navdy.service.library.events.navigation.NavigationRouteResponse navigationRouteResponse = new com.navdy.service.library.events.navigation.NavigationRouteResponse(com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS, "", request.destination, request.label, outgoingResults, java.lang.Boolean.valueOf(factoringInTraffic), request.requestId);
            if (!factoringInTraffic) {
                com.navdy.hud.app.framework.voice.TTSUtils.debugShowNoTrafficToast();
            }
            com.navdy.hud.app.maps.MapsEventHandler.getInstance().sendEventToClient(navigationRouteResponse);
            synchronized (lockObj) {
                if (routeCalculationEvent != null) {
                    routeCalculationEvent.response = navigationRouteResponse;
                    routeCalculationEvent.state = com.navdy.hud.app.maps.MapEvents.RouteCalculationState.STOPPED;
                    bus.post(routeCalculationEvent);
                }
            }
            sLogger.v("sent route response");
            if (request.autoNavigate != null && request.autoNavigate.booleanValue()) {
                sLogger.i("start auto-navigation to route " + request.label);
                com.navdy.service.library.events.navigation.NavigationSessionRequest.Builder simulationSpeed = new com.navdy.service.library.events.navigation.NavigationSessionRequest.Builder().newState(com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_STARTED).label(request.label).routeId(((com.navdy.service.library.events.navigation.NavigationRouteResult) outgoingResults.get(0)).routeId).simulationSpeed(java.lang.Integer.valueOf(0));
                if (request.originDisplay == null) {
                    booleanValue = false;
                } else {
                    booleanValue = request.originDisplay.booleanValue();
                }
                com.navdy.service.library.events.navigation.NavigationSessionRequest navigationSessionRequest = simulationSpeed.originDisplay(java.lang.Boolean.valueOf(booleanValue)).build();
                bus.post(navigationSessionRequest);
                bus.post(new com.navdy.hud.app.event.RemoteEvent(navigationSessionRequest));
            }
            com.navdy.hud.app.analytics.NavigationQualityTracker.getInstance().trackCalculationWithTraffic(factoringInTraffic);
            clearActiveRouteCalc();
        } catch (Throwable t) {
            try {
                sLogger.e(t);
            } finally {
                clearActiveRouteCalc();
            }
        }
    }

    public static void handleRouteManeuverRequest(com.navdy.service.library.events.navigation.RouteManeuverRequest routeManeuverRequest) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.maps.here.HereRouteManager.Anon6(routeManeuverRequest), 2);
    }

    private static boolean isRouteCalcInProgress() {
        boolean z;
        synchronized (lockObj) {
            if (activeRouteCalculator != null) {
                sLogger.i("route calculation already in progress:" + activeRouteCalcId);
                z = true;
            } else {
                z = false;
            }
        }
        return z;
    }

    private static boolean isRouteCalcGeocodeRequestInProgress() {
        boolean z;
        synchronized (lockObj) {
            if (activeRouteGeocodeRequest != null) {
                sLogger.i("route calculation geocode already in progress:" + activeRouteCalcId);
                z = true;
            } else {
                z = false;
            }
        }
        return z;
    }

    public static void clearActiveRouteCalc(java.lang.String id) {
        synchronized (lockObj) {
            if (android.text.TextUtils.equals(id, activeRouteCalcId)) {
                clearActiveRouteCalc();
            }
        }
    }

    public static void clearActiveRouteCalc() {
        synchronized (lockObj) {
            sLogger.v("clearActiveRouteCalc");
            activeRouteCalculator = null;
            activeRouteCalcId = null;
            activeRouteRequest = null;
            activeRouteProgress = 0;
            activeRouteCancelledByUser = false;
            routeCalculationEvent = null;
            int len = pendingRouteRequestQueue.size();
            if (len > 0) {
                sLogger.v("pending route queue size:" + len);
                com.navdy.service.library.events.navigation.NavigationRouteRequest pendingRequest = (com.navdy.service.library.events.navigation.NavigationRouteRequest) pendingRouteRequestQueue.remove();
                pendingRouteRequestIds.remove(pendingRequest.requestId);
                handler.post(new com.navdy.hud.app.maps.here.HereRouteManager.Anon7(pendingRequest));
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:64:?, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:?, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:?, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:?, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:?, code lost:
        return false;
     */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    public static boolean handleRouteCancelRequest(com.navdy.service.library.events.navigation.NavigationRouteCancelRequest routeCancelRequest, boolean originDisplay) {
        try {
            sLogger.v("routeCancelRequest:" + routeCancelRequest.handle);
            if (routeCancelRequest.handle == null) {
                sLogger.v("routeCancelRequest:null handle");
                return false;
            }
            synchronized (lockObj) {
                if (activeRouteGeocodeRequest != null && android.text.TextUtils.equals(routeCancelRequest.handle, activeGeoCalcId)) {
                    cancelCurrentGeocodeRequest();
                    sLogger.v("routeCancelRequest: sent navrouteresponse-geocode cancel [" + routeCancelRequest.handle + "]");
                    bus.post(new com.navdy.hud.app.event.RemoteEvent(new com.navdy.service.library.events.navigation.NavigationRouteResponse.Builder().requestId(routeCancelRequest.handle).status(com.navdy.service.library.events.RequestStatus.REQUEST_CANCELLED).destination(new com.navdy.service.library.events.location.Coordinate.Builder().latitude(java.lang.Double.valueOf(0.0d)).longitude(java.lang.Double.valueOf(0.0d)).build()).build()));
                    if (0 == 0) {
                        sLogger.v("may be pending route exists: " + routeCancelRequest.handle);
                        com.navdy.hud.app.maps.MapEvents.RouteCalculationEvent event = new com.navdy.hud.app.maps.MapEvents.RouteCalculationEvent();
                        event.pendingNavigationRequestId = routeCancelRequest.handle;
                        event.abortOriginDisplay = originDisplay;
                        event.state = com.navdy.hud.app.maps.MapEvents.RouteCalculationState.ABORT_NAVIGATION;
                        bus.post(event);
                    }
                } else if (activeRouteCalculator == null) {
                    sLogger.v("no active route calculation:" + routeCancelRequest.handle);
                    if (0 == 0) {
                        sLogger.v("may be pending route exists: " + routeCancelRequest.handle);
                        com.navdy.hud.app.maps.MapEvents.RouteCalculationEvent event2 = new com.navdy.hud.app.maps.MapEvents.RouteCalculationEvent();
                        event2.pendingNavigationRequestId = routeCancelRequest.handle;
                        event2.abortOriginDisplay = originDisplay;
                        event2.state = com.navdy.hud.app.maps.MapEvents.RouteCalculationState.ABORT_NAVIGATION;
                        bus.post(event2);
                    }
                } else if (!android.text.TextUtils.equals(routeCancelRequest.handle, activeRouteCalcId)) {
                    sLogger.v("invalid active route id:" + routeCancelRequest.handle);
                    if (0 == 0) {
                        sLogger.v("may be pending route exists: " + routeCancelRequest.handle);
                        com.navdy.hud.app.maps.MapEvents.RouteCalculationEvent event3 = new com.navdy.hud.app.maps.MapEvents.RouteCalculationEvent();
                        event3.pendingNavigationRequestId = routeCancelRequest.handle;
                        event3.abortOriginDisplay = originDisplay;
                        event3.state = com.navdy.hud.app.maps.MapEvents.RouteCalculationState.ABORT_NAVIGATION;
                        bus.post(event3);
                    }
                } else if (activeRouteCancelledByUser) {
                    sLogger.v("request already cancelled:" + routeCancelRequest.handle);
                    if (0 == 0) {
                        sLogger.v("may be pending route exists: " + routeCancelRequest.handle);
                        com.navdy.hud.app.maps.MapEvents.RouteCalculationEvent event4 = new com.navdy.hud.app.maps.MapEvents.RouteCalculationEvent();
                        event4.pendingNavigationRequestId = routeCancelRequest.handle;
                        event4.abortOriginDisplay = originDisplay;
                        event4.state = com.navdy.hud.app.maps.MapEvents.RouteCalculationState.ABORT_NAVIGATION;
                        bus.post(event4);
                    }
                } else {
                    if (originDisplay) {
                        sLogger.v("send cancel to client");
                        bus.post(new com.navdy.hud.app.event.RemoteEvent(routeCancelRequest));
                    }
                    activeRouteCancelledByUser = true;
                    activeRouteCalculator.cancel();
                    sLogger.v("called cancel");
                    sLogger.v("routeCancelRequest: sent navrouteresponse cancel [" + routeCancelRequest.handle + "]");
                    bus.post(new com.navdy.hud.app.event.RemoteEvent(new com.navdy.service.library.events.navigation.NavigationRouteResponse.Builder().requestId(routeCancelRequest.handle).status(com.navdy.service.library.events.RequestStatus.REQUEST_CANCELLED).destination(new com.navdy.service.library.events.location.Coordinate.Builder().latitude(java.lang.Double.valueOf(0.0d)).longitude(java.lang.Double.valueOf(0.0d)).build()).build()));
                    if (1 == 0) {
                        sLogger.v("may be pending route exists: " + routeCancelRequest.handle);
                        com.navdy.hud.app.maps.MapEvents.RouteCalculationEvent event5 = new com.navdy.hud.app.maps.MapEvents.RouteCalculationEvent();
                        event5.pendingNavigationRequestId = routeCancelRequest.handle;
                        event5.abortOriginDisplay = originDisplay;
                        event5.state = com.navdy.hud.app.maps.MapEvents.RouteCalculationState.ABORT_NAVIGATION;
                        bus.post(event5);
                    }
                }
            }
        } catch (Throwable t) {
            sLogger.e(t);
            return false;
        }
    }

    public static boolean cancelActiveRouteRequest() {
        if (!android.text.TextUtils.isEmpty(activeRouteCalcId)) {
            return handleRouteCancelRequest(new com.navdy.service.library.events.navigation.NavigationRouteCancelRequest(activeRouteCalcId), true);
        }
        sLogger.v("no active route id");
        return false;
    }

    public static void printRouteRequest(com.navdy.service.library.events.navigation.NavigationRouteRequest request) {
        java.lang.String routeAttributesStr = null;
        try {
            java.util.List<com.navdy.service.library.events.navigation.NavigationRouteRequest.RouteAttribute> routeAttributes = request.routeAttributes;
            if (routeAttributes != null && routeAttributes.size() > 0) {
                java.lang.StringBuilder builder = new java.lang.StringBuilder();
                for (com.navdy.service.library.events.navigation.NavigationRouteRequest.RouteAttribute attribute : routeAttributes) {
                    builder.append(attribute.name());
                    builder.append(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA);
                }
                routeAttributesStr = builder.toString();
            }
            sLogger.v("NavigationRouteRequest label[" + request.label + "] local[" + request.originDisplay + "] streetAddress[" + request.streetAddress + "] destination_id[" + request.destination_identifier + "] autonav[" + request.autoNavigate + "] geoCode[" + request.geoCodeStreetAddress + "] dest_coordinate[" + request.destination + "] favType[" + request.destinationType + "] requestId[" + request.requestId + "]" + "] display_coordinate[" + request.destinationDisplay + "] route attrs[" + routeAttributesStr + "]");
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }

    public static boolean isUIShowingRouteCalculation() {
        if (com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().getNotification(com.navdy.hud.app.framework.notifications.NotificationId.ROUTE_CALC_NOTIFICATION_ID) != null) {
            return true;
        }
        return false;
    }

    public static java.lang.String getActiveRouteCalcId() {
        return activeRouteCalcId;
    }

    public static void startNavigationLookup(com.navdy.hud.app.framework.destinations.Destination lookupDestination) {
        if (lookupDestination == null) {
            sLogger.e("startNavigationLookup null destination");
            return;
        }
        synchronized (lockObj) {
            routeCalculationEvent = new com.navdy.hud.app.maps.MapEvents.RouteCalculationEvent();
            routeCalculationEvent.state = com.navdy.hud.app.maps.MapEvents.RouteCalculationState.FINDING_NAV_COORDINATES;
            routeCalculationEvent.lookupDestination = lookupDestination;
            routeCalculationEvent.routeOptions = com.navdy.hud.app.maps.here.HereMapsManager.getInstance().getRouteOptions();
            sLogger.v("startNavigationLookup sent nav_coordinate_lookup ");
            bus.post(routeCalculationEvent);
        }
    }

    public static java.lang.String buildStartRouteTTS() {
        return buildRouteTTS(com.navdy.hud.app.R.string.route_start_msg, null);
    }

    public static java.lang.String buildChangeRouteTTS(java.lang.String routeId) {
        return buildRouteTTS(com.navdy.hud.app.R.string.route_change_msg, routeId);
    }

    public static java.lang.String buildRouteTTS(int id, java.lang.String routeId) {
        if (!com.navdy.hud.app.maps.here.HereMapsManager.getInstance().isInitialized()) {
            return "";
        }
        com.navdy.hud.app.maps.here.HereNavigationManager hereNavigationManager = com.navdy.hud.app.maps.here.HereNavigationManager.getInstance();
        android.content.res.Resources resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        java.lang.String routeType = "";
        java.lang.String eta = "";
        java.lang.String via = "";
        com.navdy.hud.app.maps.here.HereRouteCache instance = com.navdy.hud.app.maps.here.HereRouteCache.getInstance();
        if (routeId == null) {
            routeId = hereNavigationManager.getCurrentRouteId();
        }
        com.navdy.hud.app.maps.here.HereRouteCache.RouteInfo info = instance.getRoute(routeId);
        if (info != null) {
            via = info.routeResult.via;
            if (java.lang.Boolean.TRUE.equals(hereNavigationManager.getGeneratePhoneticTTS())) {
                via = "\u001b\\tn=address\\" + via + "\u001b\\tn=normal\\";
            }
            java.util.Date ttaDate = com.navdy.hud.app.maps.here.HereMapUtil.getRouteTtaDate(info.route);
            java.lang.StringBuilder builder = new java.lang.StringBuilder();
            eta = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getTimeHelper().formatTime12Hour(ttaDate, builder, false) + " " + builder.toString();
        }
        switch (id) {
            case com.navdy.hud.app.R.string.route_change_msg /*2131296837*/:
                return resources.getString(com.navdy.hud.app.R.string.route_change_msg, new java.lang.Object[]{via, eta});
            case com.navdy.hud.app.R.string.route_start_msg /*2131296844*/:
                com.here.android.mpa.routing.RouteOptions routeOptions = hereNavigationManager.getRouteOptions();
                if (routeOptions != null) {
                    switch (com.navdy.hud.app.maps.here.HereRouteManager.Anon8.$SwitchMap$com$here$android$mpa$routing$RouteOptions$Type[routeOptions.getRouteType().ordinal()]) {
                        case 1:
                            routeType = com.navdy.hud.app.maps.notification.RouteCalculationNotification.shortestRoute;
                            break;
                        case 2:
                            routeType = com.navdy.hud.app.maps.notification.RouteCalculationNotification.fastestRoute;
                            break;
                    }
                }
                return resources.getString(com.navdy.hud.app.R.string.route_start_msg, new java.lang.Object[]{routeType, via, eta});
            default:
                return "";
        }
    }
}
