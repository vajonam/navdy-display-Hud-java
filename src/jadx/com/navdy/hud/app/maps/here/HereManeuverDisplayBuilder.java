package com.navdy.hud.app.maps.here;

public class HereManeuverDisplayBuilder {
    public static final java.lang.String ARRIVED = resources.getString(com.navdy.hud.app.R.string.arrived);
    public static final java.lang.String CLOSE_BRACKET = ")";
    public static final boolean COMBINE_HIGHWAY_MANEUVER = true;
    public static final java.lang.String COMMA = ",";
    static final java.lang.String DESTINATION_LEFT = resources.getString(com.navdy.hud.app.R.string.left);
    private static final int DESTINATION_POINT_A_MIN_DISTANCE = 30;
    static final java.lang.String DESTINATION_RIGHT = resources.getString(com.navdy.hud.app.R.string.right);
    private static final com.navdy.hud.app.maps.util.DistanceConverter.Distance DISTANCE = new com.navdy.hud.app.maps.util.DistanceConverter.Distance();
    public static final java.lang.String EMPTY = "";
    public static final com.navdy.hud.app.maps.MapEvents.ManeuverDisplay EMPTY_MANEUVER_DISPLAY = new com.navdy.hud.app.maps.MapEvents.ManeuverDisplay();
    private static final java.lang.String END_TURN = resources.getString(com.navdy.hud.app.R.string.end_maneuver_turn);
    private static final java.lang.String ENTER_HIGHWAY = resources.getString(com.navdy.hud.app.R.string.enter_highway);
    private static final java.lang.String EXIT = resources.getString(com.navdy.hud.app.R.string.exit);
    public static final java.lang.String EXIT_NUMBER = resources.getString(com.navdy.hud.app.R.string.exit_number);
    private static final java.lang.String FEET_METER_FORMAT = "%.0f %s";
    public static final float FT_IN_METER = 3.28084f;
    private static final java.lang.String HYPHEN = resources.getString(com.navdy.hud.app.R.string.hyphen);
    public static final float KM_DISPLAY_THRESHOLD = 400.0f;
    private static final java.lang.String KM_FORMAT = "%.1f %s";
    private static final java.lang.String KM_FORMAT_SHORT = "%.0f %s";
    public static final float METERS_IN_KM = 1000.0f;
    public static final float METERS_IN_MI = 1609.34f;
    public static final float MILES_DISPLAY_THRESHOLD = 160.934f;
    private static final java.lang.String MILES_FORMAT = "%.1f %s";
    private static final java.lang.String MILES_FORMAT_SHORT = "%.0f %s";
    private static final int MIN_STEP_FEET = 10;
    private static final int MIN_STEP_METERS = 5;
    private static final long NEXT_DISTANCE_THRESHOLD = 6437;
    private static final long NEXT_DISTANCE_THRESHOLD_HIGHWAY = 16093;
    private static final long NOW_DISTANCE_THRESHOLD = 30;
    private static final long NOW_DISTANCE_THRESHOLD_HIGHWAY = 100;
    private static final java.lang.String ONTO = resources.getString(com.navdy.hud.app.R.string.onto);
    public static final java.lang.String OPEN_BRACKET = "(";
    public static final int SHORT_DISTANCE_DISPLAY_THRESHOLD = 10;
    public static final boolean SHOW_DESTINATION_DIRECTION = true;
    public static final java.lang.String SLASH = "/";
    public static final char SLASH_CHAR = '/';
    private static final float SMALL_UNITS_THRESHOLD = 0.1f;
    public static final boolean SMART_MANEUVER_CALCULATION = true;
    private static final long SOON_DISTANCE_THRESHOLD = 1287;
    private static final long SOON_DISTANCE_THRESHOLD_HIGHWAY = 3218;
    public static final java.lang.String SPACE = " ";
    public static final char SPACE_CHAR = ' ';
    public static final java.lang.String START_TURN = resources.getString(com.navdy.hud.app.R.string.start_maneuver_turn);
    private static final java.lang.String STAY_ON = resources.getString(com.navdy.hud.app.R.string.stay_on);
    private static final long THEN_MANEUVER_THRESHOLD = 250;
    private static final long THEN_MANEUVER_THRESHOLD_HIGHWAY = 1000;
    public static final java.lang.String TOWARDS = resources.getString(com.navdy.hud.app.R.string.toward);
    private static final java.lang.String UNIT_FEET = resources.getString(com.navdy.hud.app.R.string.unit_feet);
    private static final java.lang.String UNIT_FEET_EXTENDED = resources.getString(com.navdy.hud.app.R.string.unit_feet_ext);
    private static final java.lang.String UNIT_FEET_EXTENDED_SINGULAR = resources.getString(com.navdy.hud.app.R.string.unit_feet_ext_singular);
    private static final java.lang.String UNIT_KILOMETERS = resources.getString(com.navdy.hud.app.R.string.unit_kilometers);
    private static final java.lang.String UNIT_KILOMETERS_EXTENDED = resources.getString(com.navdy.hud.app.R.string.unit_kilometers_ext);
    private static final java.lang.String UNIT_KILOMETERS_EXTENDED_SINGULAR = resources.getString(com.navdy.hud.app.R.string.unit_kilometers_ext_singular);
    private static final java.lang.String UNIT_METERS = resources.getString(com.navdy.hud.app.R.string.unit_meters);
    private static final java.lang.String UNIT_METERS_EXTENDED = resources.getString(com.navdy.hud.app.R.string.unit_meters_ext);
    private static final java.lang.String UNIT_METERS_EXTENDED_SINGULAR = resources.getString(com.navdy.hud.app.R.string.unit_meters_ext_singular);
    private static final java.lang.String UNIT_MILES = resources.getString(com.navdy.hud.app.R.string.unit_miles);
    private static final java.lang.String UNIT_MILES_EXTENDED = resources.getString(com.navdy.hud.app.R.string.unit_miles_ext);
    private static final java.lang.String UNIT_MILES_EXTENDED_SINGULAR = resources.getString(com.navdy.hud.app.R.string.unit_miles_ext_singular);
    private static final java.lang.String UTURN = resources.getString(com.navdy.hud.app.R.string.uturn);
    private static final java.util.HashSet<java.lang.String> allowedTurnTextMap = new java.util.HashSet<>();
    private static com.here.android.mpa.routing.Maneuver lastStayManeuver;
    private static java.lang.String lastStayManeuverRoadName;
    private static final java.lang.StringBuilder mainSignPostBuilder = new java.lang.StringBuilder();
    private static final android.content.res.Resources resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
    private static final java.util.HashMap<com.here.android.mpa.routing.Maneuver.Icon, com.navdy.service.library.events.navigation.NavigationTurn> sHereIconToNavigationIconMap = new java.util.HashMap<>();
    private static final java.util.HashMap<com.here.android.mpa.routing.Maneuver.Turn, java.lang.String> sHereTurnToNavigationTurnMap = new java.util.HashMap<>();
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.class);
    private static final java.util.Map<com.navdy.service.library.events.navigation.NavigationTurn, java.lang.Integer> sTurnIconImageMap = new java.util.HashMap();
    private static final java.util.Map<com.navdy.service.library.events.navigation.NavigationTurn, java.lang.Integer> sTurnIconSoonImageMap = new java.util.HashMap();
    private static final java.util.Map<com.navdy.service.library.events.navigation.NavigationTurn, java.lang.Integer> sTurnIconSoonImageMapGrey = new java.util.HashMap();
    private static final com.navdy.hud.app.manager.SpeedManager speedManager = com.navdy.hud.app.manager.SpeedManager.getInstance();

    static /* synthetic */ class Anon1 {
        static final /* synthetic */ int[] $SwitchMap$com$here$android$mpa$routing$Maneuver$Turn = new int[com.here.android.mpa.routing.Maneuver.Turn.values().length];

        static {
            $SwitchMap$com$navdy$service$library$events$navigation$DistanceUnit = new int[com.navdy.service.library.events.navigation.DistanceUnit.values().length];
            try {
                $SwitchMap$com$navdy$service$library$events$navigation$DistanceUnit[com.navdy.service.library.events.navigation.DistanceUnit.DISTANCE_MILES.ordinal()] = 1;
            } catch (java.lang.NoSuchFieldError e) {
            }
            try {
                $SwitchMap$com$navdy$service$library$events$navigation$DistanceUnit[com.navdy.service.library.events.navigation.DistanceUnit.DISTANCE_FEET.ordinal()] = 2;
            } catch (java.lang.NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$com$navdy$service$library$events$navigation$DistanceUnit[com.navdy.service.library.events.navigation.DistanceUnit.DISTANCE_KMS.ordinal()] = 3;
            } catch (java.lang.NoSuchFieldError e3) {
            }
            try {
                $SwitchMap$com$navdy$service$library$events$navigation$DistanceUnit[com.navdy.service.library.events.navigation.DistanceUnit.DISTANCE_METERS.ordinal()] = 4;
            } catch (java.lang.NoSuchFieldError e4) {
            }
            $SwitchMap$com$navdy$hud$app$manager$SpeedManager$SpeedUnit = new int[com.navdy.hud.app.manager.SpeedManager.SpeedUnit.values().length];
            try {
                $SwitchMap$com$navdy$hud$app$manager$SpeedManager$SpeedUnit[com.navdy.hud.app.manager.SpeedManager.SpeedUnit.MILES_PER_HOUR.ordinal()] = 1;
            } catch (java.lang.NoSuchFieldError e5) {
            }
            try {
                $SwitchMap$com$navdy$hud$app$manager$SpeedManager$SpeedUnit[com.navdy.hud.app.manager.SpeedManager.SpeedUnit.KILOMETERS_PER_HOUR.ordinal()] = 2;
            } catch (java.lang.NoSuchFieldError e6) {
            }
            try {
                $SwitchMap$com$navdy$hud$app$manager$SpeedManager$SpeedUnit[com.navdy.hud.app.manager.SpeedManager.SpeedUnit.METERS_PER_SECOND.ordinal()] = 3;
            } catch (java.lang.NoSuchFieldError e7) {
            }
            $SwitchMap$com$navdy$hud$app$maps$here$HereManeuverDisplayBuilder$ManeuverState = new int[com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.ManeuverState.values().length];
            try {
                $SwitchMap$com$navdy$hud$app$maps$here$HereManeuverDisplayBuilder$ManeuverState[com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.ManeuverState.STAY.ordinal()] = 1;
            } catch (java.lang.NoSuchFieldError e8) {
            }
            try {
                $SwitchMap$com$navdy$hud$app$maps$here$HereManeuverDisplayBuilder$ManeuverState[com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.ManeuverState.NEXT.ordinal()] = 2;
            } catch (java.lang.NoSuchFieldError e9) {
            }
            try {
                $SwitchMap$com$navdy$hud$app$maps$here$HereManeuverDisplayBuilder$ManeuverState[com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.ManeuverState.SOON.ordinal()] = 3;
            } catch (java.lang.NoSuchFieldError e10) {
            }
            try {
                $SwitchMap$com$navdy$hud$app$maps$here$HereManeuverDisplayBuilder$ManeuverState[com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.ManeuverState.NOW.ordinal()] = 4;
            } catch (java.lang.NoSuchFieldError e11) {
            }
            $SwitchMap$com$navdy$hud$app$maps$MapEvents$DestinationDirection = new int[com.navdy.hud.app.maps.MapEvents.DestinationDirection.values().length];
            try {
                $SwitchMap$com$navdy$hud$app$maps$MapEvents$DestinationDirection[com.navdy.hud.app.maps.MapEvents.DestinationDirection.LEFT.ordinal()] = 1;
            } catch (java.lang.NoSuchFieldError e12) {
            }
            try {
                $SwitchMap$com$navdy$hud$app$maps$MapEvents$DestinationDirection[com.navdy.hud.app.maps.MapEvents.DestinationDirection.RIGHT.ordinal()] = 2;
            } catch (java.lang.NoSuchFieldError e13) {
            }
            try {
                $SwitchMap$com$here$android$mpa$routing$Maneuver$Turn[com.here.android.mpa.routing.Maneuver.Turn.QUITE_LEFT.ordinal()] = 1;
            } catch (java.lang.NoSuchFieldError e14) {
            }
            try {
                $SwitchMap$com$here$android$mpa$routing$Maneuver$Turn[com.here.android.mpa.routing.Maneuver.Turn.QUITE_RIGHT.ordinal()] = 2;
            } catch (java.lang.NoSuchFieldError e15) {
            }
            try {
                $SwitchMap$com$here$android$mpa$routing$Maneuver$Turn[com.here.android.mpa.routing.Maneuver.Turn.HEAVY_LEFT.ordinal()] = 3;
            } catch (java.lang.NoSuchFieldError e16) {
            }
            try {
                $SwitchMap$com$here$android$mpa$routing$Maneuver$Turn[com.here.android.mpa.routing.Maneuver.Turn.HEAVY_RIGHT.ordinal()] = 4;
            } catch (java.lang.NoSuchFieldError e17) {
            }
            try {
                $SwitchMap$com$here$android$mpa$routing$Maneuver$Turn[com.here.android.mpa.routing.Maneuver.Turn.KEEP_RIGHT.ordinal()] = 5;
            } catch (java.lang.NoSuchFieldError e18) {
            }
            try {
                $SwitchMap$com$here$android$mpa$routing$Maneuver$Turn[com.here.android.mpa.routing.Maneuver.Turn.KEEP_MIDDLE.ordinal()] = 6;
            } catch (java.lang.NoSuchFieldError e19) {
            }
            try {
                $SwitchMap$com$here$android$mpa$routing$Maneuver$Turn[com.here.android.mpa.routing.Maneuver.Turn.KEEP_LEFT.ordinal()] = 7;
            } catch (java.lang.NoSuchFieldError e20) {
            }
        }
    }

    public enum ManeuverState {
        STAY,
        NEXT,
        SOON,
        NOW
    }

    public static class PendingRoadInfo {
        public boolean dontUseSignpostText;
        public boolean enterHighway;
        public boolean hasTo;
        public boolean usePrevManeuverInfo;
    }

    static {
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver.Icon.UNDEFINED, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_STRAIGHT);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver.Icon.GO_STRAIGHT, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_STRAIGHT);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver.Icon.KEEP_MIDDLE, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_STRAIGHT);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver.Icon.KEEP_RIGHT, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_KEEP_RIGHT);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver.Icon.LIGHT_RIGHT, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_EASY_RIGHT);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver.Icon.QUITE_RIGHT, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_RIGHT);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver.Icon.HEAVY_RIGHT, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_SHARP_RIGHT);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver.Icon.KEEP_LEFT, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_KEEP_LEFT);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver.Icon.LIGHT_LEFT, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_EASY_LEFT);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver.Icon.QUITE_LEFT, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_LEFT);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver.Icon.HEAVY_LEFT, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_SHARP_LEFT);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver.Icon.UTURN_RIGHT, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_UTURN_RIGHT);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver.Icon.UTURN_LEFT, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_UTURN_LEFT);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver.Icon.ROUNDABOUT_1, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_ROUNDABOUT_S);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver.Icon.ROUNDABOUT_2, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_ROUNDABOUT_S);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver.Icon.ROUNDABOUT_3, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_ROUNDABOUT_S);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver.Icon.ROUNDABOUT_4, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_ROUNDABOUT_S);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver.Icon.ROUNDABOUT_5, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_ROUNDABOUT_S);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver.Icon.ROUNDABOUT_6, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_ROUNDABOUT_S);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver.Icon.ROUNDABOUT_7, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_ROUNDABOUT_S);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver.Icon.ROUNDABOUT_8, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_ROUNDABOUT_S);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver.Icon.ROUNDABOUT_9, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_ROUNDABOUT_S);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver.Icon.ROUNDABOUT_10, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_ROUNDABOUT_S);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver.Icon.ROUNDABOUT_11, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_ROUNDABOUT_S);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver.Icon.ROUNDABOUT_12, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_ROUNDABOUT_S);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver.Icon.ENTER_HIGHWAY_RIGHT_LANE, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_MERGE_LEFT);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver.Icon.ENTER_HIGHWAY_LEFT_LANE, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_MERGE_RIGHT);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver.Icon.LEAVE_HIGHWAY_RIGHT_LANE, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_EXIT_RIGHT);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver.Icon.LEAVE_HIGHWAY_LEFT_LANE, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_EXIT_LEFT);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver.Icon.HIGHWAY_KEEP_RIGHT, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_KEEP_RIGHT);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver.Icon.HIGHWAY_KEEP_LEFT, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_KEEP_LEFT);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver.Icon.ROUNDABOUT_1_LH, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_ROUNDABOUT_N);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver.Icon.ROUNDABOUT_2_LH, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_ROUNDABOUT_N);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver.Icon.ROUNDABOUT_3_LH, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_ROUNDABOUT_N);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver.Icon.ROUNDABOUT_4_LH, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_ROUNDABOUT_N);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver.Icon.ROUNDABOUT_5_LH, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_ROUNDABOUT_N);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver.Icon.ROUNDABOUT_6_LH, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_ROUNDABOUT_N);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver.Icon.ROUNDABOUT_7_LH, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_ROUNDABOUT_N);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver.Icon.ROUNDABOUT_8_LH, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_ROUNDABOUT_N);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver.Icon.ROUNDABOUT_9_LH, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_ROUNDABOUT_N);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver.Icon.ROUNDABOUT_10_LH, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_ROUNDABOUT_N);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver.Icon.ROUNDABOUT_11_LH, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_ROUNDABOUT_N);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver.Icon.ROUNDABOUT_12_LH, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_ROUNDABOUT_N);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver.Icon.START, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_START);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver.Icon.END, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_END);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver.Icon.FERRY, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_FERRY);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver.Icon.HEAD_TO, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_STRAIGHT);
        sHereTurnToNavigationTurnMap.put(com.here.android.mpa.routing.Maneuver.Turn.UNDEFINED, resources.getString(com.navdy.hud.app.R.string.undefined_turn));
        sHereTurnToNavigationTurnMap.put(com.here.android.mpa.routing.Maneuver.Turn.NO_TURN, resources.getString(com.navdy.hud.app.R.string.no_turn));
        sHereTurnToNavigationTurnMap.put(com.here.android.mpa.routing.Maneuver.Turn.KEEP_MIDDLE, resources.getString(com.navdy.hud.app.R.string.keep_middle));
        sHereTurnToNavigationTurnMap.put(com.here.android.mpa.routing.Maneuver.Turn.KEEP_RIGHT, resources.getString(com.navdy.hud.app.R.string.keep_right));
        sHereTurnToNavigationTurnMap.put(com.here.android.mpa.routing.Maneuver.Turn.LIGHT_RIGHT, resources.getString(com.navdy.hud.app.R.string.slight_right));
        sHereTurnToNavigationTurnMap.put(com.here.android.mpa.routing.Maneuver.Turn.QUITE_RIGHT, resources.getString(com.navdy.hud.app.R.string.quite_right));
        sHereTurnToNavigationTurnMap.put(com.here.android.mpa.routing.Maneuver.Turn.HEAVY_RIGHT, resources.getString(com.navdy.hud.app.R.string.quite_right));
        sHereTurnToNavigationTurnMap.put(com.here.android.mpa.routing.Maneuver.Turn.KEEP_LEFT, resources.getString(com.navdy.hud.app.R.string.keep_left));
        sHereTurnToNavigationTurnMap.put(com.here.android.mpa.routing.Maneuver.Turn.LIGHT_LEFT, resources.getString(com.navdy.hud.app.R.string.slight_left));
        sHereTurnToNavigationTurnMap.put(com.here.android.mpa.routing.Maneuver.Turn.QUITE_LEFT, resources.getString(com.navdy.hud.app.R.string.quite_left));
        sHereTurnToNavigationTurnMap.put(com.here.android.mpa.routing.Maneuver.Turn.HEAVY_LEFT, resources.getString(com.navdy.hud.app.R.string.quite_left));
        sHereTurnToNavigationTurnMap.put(com.here.android.mpa.routing.Maneuver.Turn.RETURN, UTURN);
        sHereTurnToNavigationTurnMap.put(com.here.android.mpa.routing.Maneuver.Turn.ROUNDABOUT_1, resources.getString(com.navdy.hud.app.R.string.roundabout_1));
        sHereTurnToNavigationTurnMap.put(com.here.android.mpa.routing.Maneuver.Turn.ROUNDABOUT_2, resources.getString(com.navdy.hud.app.R.string.roundabout_2));
        sHereTurnToNavigationTurnMap.put(com.here.android.mpa.routing.Maneuver.Turn.ROUNDABOUT_3, resources.getString(com.navdy.hud.app.R.string.roundabout_3));
        sHereTurnToNavigationTurnMap.put(com.here.android.mpa.routing.Maneuver.Turn.ROUNDABOUT_4, resources.getString(com.navdy.hud.app.R.string.roundabout_4));
        sHereTurnToNavigationTurnMap.put(com.here.android.mpa.routing.Maneuver.Turn.ROUNDABOUT_5, resources.getString(com.navdy.hud.app.R.string.roundabout_5));
        sHereTurnToNavigationTurnMap.put(com.here.android.mpa.routing.Maneuver.Turn.ROUNDABOUT_6, resources.getString(com.navdy.hud.app.R.string.roundabout_6));
        sHereTurnToNavigationTurnMap.put(com.here.android.mpa.routing.Maneuver.Turn.ROUNDABOUT_7, resources.getString(com.navdy.hud.app.R.string.roundabout_7));
        sHereTurnToNavigationTurnMap.put(com.here.android.mpa.routing.Maneuver.Turn.ROUNDABOUT_8, resources.getString(com.navdy.hud.app.R.string.roundabout_8));
        sHereTurnToNavigationTurnMap.put(com.here.android.mpa.routing.Maneuver.Turn.ROUNDABOUT_9, resources.getString(com.navdy.hud.app.R.string.roundabout_9));
        sHereTurnToNavigationTurnMap.put(com.here.android.mpa.routing.Maneuver.Turn.ROUNDABOUT_10, resources.getString(com.navdy.hud.app.R.string.roundabout_10));
        sHereTurnToNavigationTurnMap.put(com.here.android.mpa.routing.Maneuver.Turn.ROUNDABOUT_11, resources.getString(com.navdy.hud.app.R.string.roundabout_11));
        sHereTurnToNavigationTurnMap.put(com.here.android.mpa.routing.Maneuver.Turn.ROUNDABOUT_12, resources.getString(com.navdy.hud.app.R.string.roundabout_12));
        sTurnIconImageMap.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_START, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.tbt_straight));
        sTurnIconImageMap.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_LEFT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.tbt_turn_left));
        sTurnIconImageMap.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_RIGHT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.tbt_turn_right));
        sTurnIconImageMap.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_EASY_RIGHT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.tbt_easy_right));
        sTurnIconImageMap.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_EASY_LEFT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.tbt_easy_left));
        sTurnIconImageMap.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_SHARP_LEFT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.tbt_sharp_left));
        sTurnIconImageMap.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_SHARP_RIGHT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.tbt_sharp_right));
        sTurnIconImageMap.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_EXIT_RIGHT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.tbt_exit_right));
        sTurnIconImageMap.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_EXIT_LEFT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.tbt_exit_left));
        sTurnIconImageMap.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_KEEP_LEFT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.tbt_stay_left));
        sTurnIconImageMap.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_KEEP_RIGHT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.tbt_stay_right));
        sTurnIconImageMap.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_END, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.tbt_end));
        sTurnIconImageMap.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_STRAIGHT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.tbt_go_straight));
        sTurnIconImageMap.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_UTURN_LEFT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.tbt_u_turn_left));
        sTurnIconImageMap.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_UTURN_RIGHT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.tbt_u_turn_right));
        sTurnIconImageMap.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_ROUNDABOUT_S, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.tbt_left_roundabout));
        sTurnIconImageMap.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_ROUNDABOUT_N, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.tbt_right_roundabout));
        sTurnIconImageMap.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_MERGE_LEFT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.tbt_merge_left));
        sTurnIconImageMap.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_MERGE_RIGHT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.tbt_merge_right));
        sTurnIconSoonImageMap.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_START, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.tbt_straight_soon));
        sTurnIconSoonImageMap.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_LEFT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.tbt_turn_left_soon));
        sTurnIconSoonImageMap.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_RIGHT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.tbt_turn_right_soon));
        sTurnIconSoonImageMap.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_EASY_RIGHT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.tbt_easy_right_soon));
        sTurnIconSoonImageMap.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_EASY_LEFT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.tbt_easy_left_soon));
        sTurnIconSoonImageMap.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_SHARP_LEFT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.tbt_sharp_left_soon));
        sTurnIconSoonImageMap.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_SHARP_RIGHT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.tbt_sharp_right_soon));
        sTurnIconSoonImageMap.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_EXIT_RIGHT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.tbt_exit_right_soon));
        sTurnIconSoonImageMap.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_EXIT_LEFT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.tbt_exit_left_soon));
        sTurnIconSoonImageMap.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_KEEP_LEFT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.tbt_stay_left_soon));
        sTurnIconSoonImageMap.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_KEEP_RIGHT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.tbt_stay_right_soon));
        sTurnIconSoonImageMap.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_END, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.tbt_end_soon));
        sTurnIconSoonImageMap.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_STRAIGHT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.tbt_go_straight_soon));
        sTurnIconSoonImageMap.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_UTURN_LEFT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.tbt_u_turn_left_soon));
        sTurnIconSoonImageMap.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_UTURN_RIGHT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.tbt_u_turn_right_soon));
        sTurnIconSoonImageMap.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_ROUNDABOUT_S, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.tbt_left_roundabout_soon));
        sTurnIconSoonImageMap.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_ROUNDABOUT_N, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.tbt_right_roundabout_soon));
        sTurnIconSoonImageMap.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_MERGE_LEFT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.tbt_merge_left_soon));
        sTurnIconSoonImageMap.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_MERGE_RIGHT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.tbt_merge_right_soon));
        sTurnIconSoonImageMapGrey.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_START, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_tbt_start_grey));
        sTurnIconSoonImageMapGrey.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_LEFT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_tbt_quite_left_grey));
        sTurnIconSoonImageMapGrey.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_RIGHT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_tbt_quite_right_grey));
        sTurnIconSoonImageMapGrey.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_EASY_RIGHT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_tbt_light_right_grey));
        sTurnIconSoonImageMapGrey.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_EASY_LEFT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_tbt_light_left_grey));
        sTurnIconSoonImageMapGrey.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_SHARP_LEFT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_tbt_heavy_left_grey));
        sTurnIconSoonImageMapGrey.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_SHARP_RIGHT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_tbt_heavy_right_grey));
        sTurnIconSoonImageMapGrey.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_EXIT_RIGHT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_tbt_leave_highway_grey));
        sTurnIconSoonImageMapGrey.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_EXIT_LEFT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_tbt_leave_highway_left_lane_grey));
        sTurnIconSoonImageMapGrey.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_KEEP_LEFT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_tbt_keep_left_grey));
        sTurnIconSoonImageMapGrey.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_KEEP_RIGHT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_tbt_keep_right_grey));
        sTurnIconSoonImageMapGrey.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_END, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_tbt_arrive_grey));
        sTurnIconSoonImageMapGrey.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_STRAIGHT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_tbt_go_straight_grey));
        sTurnIconSoonImageMapGrey.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_UTURN_LEFT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.tbt_icon_return_grey));
        sTurnIconSoonImageMapGrey.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_UTURN_RIGHT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_tbt_uturn_right_grey));
        sTurnIconSoonImageMapGrey.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_ROUNDABOUT_S, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.tbt_left_roundabout_soon));
        sTurnIconSoonImageMapGrey.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_ROUNDABOUT_N, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.tbt_right_roundabout_soon));
        sTurnIconSoonImageMapGrey.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_MERGE_LEFT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_tbt_merge_left_grey));
        sTurnIconSoonImageMapGrey.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_MERGE_RIGHT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_tbt_merge_right_grey));
        EMPTY_MANEUVER_DISPLAY.empty = true;
        allowedTurnTextMap.add(EXIT);
        allowedTurnTextMap.add(UTURN);
        allowedTurnTextMap.add(START_TURN);
        allowedTurnTextMap.add(END_TURN);
        allowedTurnTextMap.add(ARRIVED);
        allowedTurnTextMap.add(sHereTurnToNavigationTurnMap.get(com.here.android.mpa.routing.Maneuver.Turn.ROUNDABOUT_1));
        allowedTurnTextMap.add(sHereTurnToNavigationTurnMap.get(com.here.android.mpa.routing.Maneuver.Turn.ROUNDABOUT_2));
        allowedTurnTextMap.add(sHereTurnToNavigationTurnMap.get(com.here.android.mpa.routing.Maneuver.Turn.ROUNDABOUT_3));
        allowedTurnTextMap.add(sHereTurnToNavigationTurnMap.get(com.here.android.mpa.routing.Maneuver.Turn.ROUNDABOUT_4));
        allowedTurnTextMap.add(sHereTurnToNavigationTurnMap.get(com.here.android.mpa.routing.Maneuver.Turn.ROUNDABOUT_5));
        allowedTurnTextMap.add(sHereTurnToNavigationTurnMap.get(com.here.android.mpa.routing.Maneuver.Turn.ROUNDABOUT_6));
        allowedTurnTextMap.add(sHereTurnToNavigationTurnMap.get(com.here.android.mpa.routing.Maneuver.Turn.ROUNDABOUT_7));
        allowedTurnTextMap.add(sHereTurnToNavigationTurnMap.get(com.here.android.mpa.routing.Maneuver.Turn.ROUNDABOUT_8));
        allowedTurnTextMap.add(sHereTurnToNavigationTurnMap.get(com.here.android.mpa.routing.Maneuver.Turn.ROUNDABOUT_9));
        allowedTurnTextMap.add(sHereTurnToNavigationTurnMap.get(com.here.android.mpa.routing.Maneuver.Turn.ROUNDABOUT_10));
        allowedTurnTextMap.add(sHereTurnToNavigationTurnMap.get(com.here.android.mpa.routing.Maneuver.Turn.ROUNDABOUT_11));
        allowedTurnTextMap.add(sHereTurnToNavigationTurnMap.get(com.here.android.mpa.routing.Maneuver.Turn.ROUNDABOUT_12));
    }

    public static com.navdy.hud.app.maps.MapEvents.ManeuverDisplay getManeuverDisplay(com.here.android.mpa.routing.Maneuver current, boolean last, long distance, java.lang.String streetAddress, com.here.android.mpa.routing.Maneuver previousManeuver, com.navdy.service.library.events.navigation.NavigationRouteRequest request, com.here.android.mpa.routing.Maneuver nextManeuver, boolean calculateEarlyManeuver, boolean calcManeuverState, com.navdy.hud.app.maps.MapEvents.DestinationDirection destinationDirection) {
        com.navdy.hud.app.maps.MapEvents.DestinationDirection direction2;
        com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.PendingRoadInfo info = new com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.PendingRoadInfo();
        com.navdy.hud.app.maps.MapEvents.ManeuverDisplay display = new com.navdy.hud.app.maps.MapEvents.ManeuverDisplay();
        display.maneuverId = current != null ? java.lang.String.valueOf(java.lang.System.identityHashCode(current)) : "unknown";
        com.here.android.mpa.common.RoadElement currentRoadElement = com.navdy.hud.app.maps.here.HereMapUtil.getCurrentRoadElement();
        boolean isHighway = com.navdy.hud.app.maps.here.HereMapUtil.isCurrentRoadHighway(current);
        if (!isHighway) {
            isHighway = com.navdy.hud.app.maps.here.HereMapUtil.isCurrentRoadHighway(currentRoadElement);
        }
        if (calcManeuverState) {
            if (distance >= 2147483647L) {
                distance = 0;
            }
            display.maneuverState = getManeuverState(distance, isHighway);
        } else {
            display.maneuverState = com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.ManeuverState.NOW;
        }
        setNavigationTurnIconId(current, distance, display.maneuverState, display, calculateEarlyManeuver ? nextManeuver : null, isHighway, destinationDirection);
        setNavigationDistance(distance, display, false, !calcManeuverState);
        if (display.maneuverState == com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.ManeuverState.STAY) {
            if (currentRoadElement == null) {
                display.currentRoad = null;
            } else {
                display.currentRoad = com.navdy.hud.app.maps.here.HereMapUtil.concatText(currentRoadElement.getRouteName(), currentRoadElement.getRoadName(), true, isHighway);
                if (!android.text.TextUtils.isEmpty(display.currentRoad)) {
                    lastStayManeuverRoadName = display.currentRoad;
                    lastStayManeuver = current;
                }
            }
            if (android.text.TextUtils.isEmpty(display.currentRoad)) {
                if (lastStayManeuver != current) {
                    lastStayManeuver = null;
                    lastStayManeuverRoadName = null;
                } else if (lastStayManeuverRoadName != null) {
                    display.currentRoad = lastStayManeuverRoadName;
                }
            }
            if (android.text.TextUtils.isEmpty(display.currentRoad)) {
                if (previousManeuver != null) {
                    display.currentRoad = com.navdy.hud.app.maps.here.HereMapUtil.concatText(previousManeuver.getNextRoadNumber(), previousManeuver.getNextRoadName(), true, isHighway);
                }
                if (display.currentRoad == null) {
                    display.currentRoad = "";
                }
            }
        } else {
            lastStayManeuver = null;
            lastStayManeuverRoadName = null;
            display.currentRoad = com.navdy.hud.app.maps.here.HereMapUtil.concatText(current.getRoadNumber(), current.getRoadName(), true, isHighway);
            if (android.text.TextUtils.isEmpty(display.currentRoad)) {
                display.currentRoad = getPendingRoadText(current, previousManeuver, nextManeuver, info);
            }
        }
        display.currentSpeedLimit = com.navdy.hud.app.maps.here.HereMapUtil.getCurrentSpeedLimit();
        if (last) {
            display.pendingTurn = END_TURN;
            java.lang.String currentRoad = com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().getDestinationLabel();
            if (android.text.TextUtils.isEmpty(currentRoad)) {
                if (streetAddress != null) {
                    currentRoad = com.navdy.hud.app.maps.here.HereMapUtil.parseStreetAddress(streetAddress);
                } else {
                    currentRoad = com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().getDestinationStreetAddress();
                }
                if (android.text.TextUtils.isEmpty(currentRoad)) {
                    currentRoad = current.getRoadName();
                }
            }
            com.navdy.hud.app.maps.MapEvents.DestinationDirection direction = null;
            if (request != null) {
                com.navdy.service.library.events.location.Coordinate c1 = request.destination;
                com.navdy.service.library.events.location.Coordinate c2 = request.destinationDisplay;
                if (c1 == null || c2 == null) {
                    display.direction = com.navdy.hud.app.maps.MapEvents.DestinationDirection.UNKNOWN;
                    sLogger.v("dest(1) cannot calculate direction pos not available");
                } else {
                    com.here.android.mpa.common.GeoCoordinate M = new com.here.android.mpa.common.GeoCoordinate(request.destinationDisplay.latitude.doubleValue(), request.destinationDisplay.longitude.doubleValue());
                    com.here.android.mpa.common.GeoCoordinate B = new com.here.android.mpa.common.GeoCoordinate(request.destination.latitude.doubleValue(), request.destination.longitude.doubleValue());
                    if (previousManeuver == null) {
                        com.here.android.mpa.routing.Route route = com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().getCurrentRoute();
                        if (route != null) {
                            java.util.List<com.here.android.mpa.routing.Maneuver> maneuvers = route.getManeuvers();
                            if (maneuvers != null && maneuvers.size() == 2) {
                                previousManeuver = (com.here.android.mpa.routing.Maneuver) maneuvers.get(0);
                            }
                        }
                    }
                    com.here.android.mpa.common.GeoCoordinate A = getEstimatedPointAfromManeuver(previousManeuver, B);
                    if (A != null) {
                        double position = java.lang.Math.sin(((B.getLatitude() - A.getLatitude()) * (M.getLongitude() - A.getLongitude())) - ((B.getLongitude() - A.getLongitude()) * (M.getLatitude() - A.getLatitude())));
                        if (position >= 0.0d) {
                            direction = com.navdy.hud.app.maps.MapEvents.DestinationDirection.RIGHT;
                        } else if (position < 0.0d) {
                            direction = com.navdy.hud.app.maps.MapEvents.DestinationDirection.LEFT;
                        } else {
                            direction = com.navdy.hud.app.maps.MapEvents.DestinationDirection.UNKNOWN;
                        }
                        sLogger.v("dest(1) position=" + position + " DIRECTION = " + direction);
                        display.direction = direction;
                        if (direction != com.navdy.hud.app.maps.MapEvents.DestinationDirection.UNKNOWN) {
                            display.destinationIconId = getDestinationIcon(direction);
                        }
                        double navToDisplayAngle = B.getHeading(M);
                        double estimatedToNavAngle = A.getHeading(B);
                        int diffAngle = (int) (navToDisplayAngle - estimatedToNavAngle);
                        sLogger.v("dest(2): navToDisplayAngle=" + ((int) navToDisplayAngle) + " currentHereAngle=" + ((int) estimatedToNavAngle) + " diffAngle =" + diffAngle);
                        if (diffAngle < 0) {
                            diffAngle += 360;
                        }
                        if (diffAngle >= 1 && diffAngle <= 179) {
                            direction2 = com.navdy.hud.app.maps.MapEvents.DestinationDirection.RIGHT;
                        } else if (diffAngle < 181 || diffAngle > 359) {
                            direction2 = com.navdy.hud.app.maps.MapEvents.DestinationDirection.UNKNOWN;
                        } else {
                            direction2 = com.navdy.hud.app.maps.MapEvents.DestinationDirection.LEFT;
                        }
                        sLogger.v("dest(2): diffAngle=" + diffAngle + " direction=" + direction2);
                    } else {
                        display.direction = com.navdy.hud.app.maps.MapEvents.DestinationDirection.UNKNOWN;
                        sLogger.v("dest(1) cannot calculate direction, A pos not available");
                    }
                }
            }
            if (direction != null) {
                android.content.res.Resources resources2 = resources;
                java.lang.Object[] objArr = new java.lang.Object[2];
                if (android.text.TextUtils.isEmpty(currentRoad)) {
                    currentRoad = resources.getString(com.navdy.hud.app.R.string.end_maneuver_direction_no_road);
                }
                objArr[0] = currentRoad;
                objArr[1] = getDestinationDirection(direction);
                display.pendingRoad = resources2.getString(com.navdy.hud.app.R.string.end_maneuver_direction, objArr);
            } else {
                android.content.res.Resources resources3 = resources;
                java.lang.Object[] objArr2 = new java.lang.Object[1];
                if (android.text.TextUtils.isEmpty(currentRoad)) {
                    currentRoad = resources.getString(com.navdy.hud.app.R.string.end_maneuver_direction_no_road);
                }
                objArr2[0] = currentRoad;
                display.pendingRoad = resources3.getString(com.navdy.hud.app.R.string.end_maneuver, objArr2);
            }
        } else {
            display.pendingTurn = getTurnText(current, info, display.maneuverState, previousManeuver, nextManeuver);
            if (display.maneuverState == com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.ManeuverState.STAY) {
                display.pendingRoad = display.currentRoad;
            } else {
                java.lang.String pendingRoadText = getPendingRoadText(current, previousManeuver, nextManeuver, info);
                if (android.text.TextUtils.isEmpty(pendingRoadText)) {
                    pendingRoadText = com.navdy.hud.app.maps.here.HereMapUtil.getRoadName(current.getRoadElements());
                }
                if (!android.text.TextUtils.isEmpty(pendingRoadText)) {
                    display.pendingRoad = pendingRoadText;
                } else {
                    display.pendingRoad = "";
                }
            }
        }
        return display;
    }

    public static com.navdy.hud.app.maps.MapEvents.ManeuverDisplay getStartManeuverDisplay(com.here.android.mpa.routing.Maneuver current, com.here.android.mpa.routing.Maneuver next) {
        com.navdy.hud.app.maps.MapEvents.ManeuverDisplay display = new com.navdy.hud.app.maps.MapEvents.ManeuverDisplay();
        boolean isCurrentHighway = com.navdy.hud.app.maps.here.HereMapUtil.isCurrentRoadHighway(current);
        boolean isNextHighway = com.navdy.hud.app.maps.here.HereMapUtil.isCurrentRoadHighway(next);
        java.lang.String currentRoad = com.navdy.hud.app.maps.here.HereMapUtil.concatText(current.getRoadNumber(), current.getRoadName(), true, isCurrentHighway);
        java.lang.String nextRoad = com.navdy.hud.app.maps.here.HereMapUtil.concatText(next.getRoadNumber(), next.getRoadName(), true, isNextHighway);
        if (android.text.TextUtils.isEmpty(currentRoad)) {
            currentRoad = nextRoad;
            java.lang.String str = com.navdy.hud.app.maps.here.HereMapUtil.concatText(next.getNextRoadNumber(), next.getNextRoadName(), isNextHighway, isNextHighway);
            if (!android.text.TextUtils.isEmpty(str)) {
                nextRoad = str;
            }
        }
        if (android.text.TextUtils.isEmpty(currentRoad)) {
            currentRoad = getPendingRoadText(current, null, null, null);
        }
        display.turnIconId = ((java.lang.Integer) sTurnIconImageMap.get(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_START)).intValue();
        display.pendingTurn = START_TURN;
        display.currentRoad = com.navdy.hud.app.maps.here.HereMapUtil.concatText(current.getRoadNumber(), current.getRoadName(), true, isCurrentHighway);
        display.currentSpeedLimit = com.navdy.hud.app.maps.here.HereMapUtil.getCurrentSpeedLimit();
        if (!android.text.TextUtils.equals(currentRoad, nextRoad)) {
            display.pendingRoad = resources.getString(com.navdy.hud.app.R.string.start_maneuver, new java.lang.Object[]{currentRoad, nextRoad});
        } else if (android.text.TextUtils.isEmpty(currentRoad)) {
            display.pendingRoad = "";
        } else {
            display.pendingRoad = resources.getString(com.navdy.hud.app.R.string.start_maneuver_no_next_road, new java.lang.Object[]{currentRoad});
        }
        setNavigationDistance(0, display, false, false);
        display.totalDistanceRemaining = display.totalDistance;
        display.totalDistanceRemainingUnit = display.totalDistanceUnit;
        display.distanceToPendingRoadText = "";
        display.navigationTurn = com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_START;
        display.maneuverId = current != null ? java.lang.String.valueOf(java.lang.System.identityHashCode(current)) : "start";
        return display;
    }

    public static com.navdy.hud.app.maps.MapEvents.ManeuverDisplay getArrivedManeuverDisplay() {
        com.navdy.hud.app.maps.MapEvents.ManeuverDisplay display = new com.navdy.hud.app.maps.MapEvents.ManeuverDisplay();
        display.maneuverId = "arrived";
        display.turnIconId = com.navdy.hud.app.R.drawable.icon_tbt_arrive;
        display.pendingTurn = ARRIVED;
        com.navdy.hud.app.maps.here.HereNavigationManager hereNavigationManager = com.navdy.hud.app.maps.here.HereNavigationManager.getInstance();
        java.lang.String currentRoad = hereNavigationManager.getDestinationLabel();
        if (android.text.TextUtils.isEmpty(currentRoad)) {
            currentRoad = hereNavigationManager.getDestinationStreetAddress();
            if (android.text.TextUtils.isEmpty(currentRoad)) {
                currentRoad = "";
            }
        }
        display.pendingRoad = currentRoad;
        return display;
    }

    private static java.lang.String getTurnText(com.here.android.mpa.routing.Maneuver current, com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.PendingRoadInfo roadInfo, com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.ManeuverState maneuverState, com.here.android.mpa.routing.Maneuver prevManeuver, com.here.android.mpa.routing.Maneuver nextManeuver) {
        com.here.android.mpa.routing.Maneuver.Action action = current.getAction();
        com.here.android.mpa.routing.Maneuver.Turn turn = current.getTurn();
        if (maneuverState == com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.ManeuverState.STAY) {
            return STAY_ON;
        }
        if (com.navdy.hud.app.maps.here.HereMapUtil.isHighwayAction(current)) {
            if (action == com.here.android.mpa.routing.Maneuver.Action.ENTER_HIGHWAY_FROM_LEFT || action == com.here.android.mpa.routing.Maneuver.Action.ENTER_HIGHWAY_FROM_RIGHT) {
                return ENTER_HIGHWAY;
            }
            if (!(prevManeuver == null || nextManeuver == null)) {
                if (com.navdy.hud.app.maps.here.HereMapUtil.isHighwayAction(prevManeuver)) {
                    roadInfo.enterHighway = true;
                    roadInfo.usePrevManeuverInfo = true;
                    turn = prevManeuver.getTurn();
                } else if (com.navdy.hud.app.maps.here.HereMapUtil.isHighwayAction(nextManeuver)) {
                    roadInfo.enterHighway = true;
                }
            }
        } else if (action == com.here.android.mpa.routing.Maneuver.Action.LEAVE_HIGHWAY) {
            java.lang.String from = com.navdy.hud.app.maps.here.HereMapUtil.getRoadName(current);
            java.lang.String to = com.navdy.hud.app.maps.here.HereMapUtil.getNextRoadName(current);
            if (!android.text.TextUtils.equals(from, to)) {
                return EXIT;
            }
            sLogger.v("ignoring action[" + action + "] using[" + turn + "] from[" + from + "] to[" + to + "]");
        } else if (action == com.here.android.mpa.routing.Maneuver.Action.UTURN) {
            return UTURN;
        }
        switch (com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.Anon1.$SwitchMap$com$here$android$mpa$routing$Maneuver$Turn[turn.ordinal()]) {
            case 1:
            case 2:
            case 3:
            case 4:
                roadInfo.dontUseSignpostText = true;
                break;
            case 5:
            case 6:
            case 7:
                roadInfo.hasTo = true;
                break;
        }
        return (java.lang.String) sHereTurnToNavigationTurnMap.get(turn);
    }

    private static java.lang.String getPendingRoadText(com.here.android.mpa.routing.Maneuver maneuver, com.here.android.mpa.routing.Maneuver before, com.here.android.mpa.routing.Maneuver after, com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.PendingRoadInfo pendingRoadInfo) {
        java.lang.String sb;
        if (!(pendingRoadInfo == null || !pendingRoadInfo.usePrevManeuverInfo || before == null)) {
            maneuver = before;
            before = null;
            after = null;
        }
        java.lang.String number = maneuver.getNextRoadNumber();
        java.lang.String name = maneuver.getNextRoadName();
        com.here.android.mpa.routing.Signpost signpost = maneuver.getSignpost();
        com.navdy.hud.app.maps.here.HereMapUtil.SignPostInfo info = new com.navdy.hud.app.maps.here.HereMapUtil.SignPostInfo();
        java.lang.String signPostText = com.navdy.hud.app.maps.here.HereMapUtil.getSignpostText(signpost, maneuver, info, null);
        if (pendingRoadInfo != null && pendingRoadInfo.dontUseSignpostText && (!android.text.TextUtils.isEmpty(number) || !android.text.TextUtils.isEmpty(name))) {
            signPostText = null;
            info.hasNameInSignPost = false;
            info.hasNumberInSignPost = false;
        }
        boolean useToRoad = false;
        boolean useSignpost = false;
        boolean toInstructionAdded = false;
        boolean ontoInstructionAdded = false;
        boolean maneuverMatch = com.navdy.hud.app.maps.here.HereMapUtil.hasSameSignpostAndToRoad(maneuver, before);
        if (maneuverMatch) {
            useToRoad = true;
        } else {
            maneuverMatch = com.navdy.hud.app.maps.here.HereMapUtil.hasSameSignpostAndToRoad(maneuver, after);
            if (maneuverMatch) {
                useSignpost = true;
            }
        }
        synchronized (mainSignPostBuilder) {
            mainSignPostBuilder.setLength(0);
            if (!android.text.TextUtils.isEmpty(number) && !info.hasNumberInSignPost) {
                mainSignPostBuilder.append(number);
            }
            boolean hasSignPost = false;
            if (!android.text.TextUtils.isEmpty(signPostText)) {
                boolean addSignpost = true;
                if (maneuverMatch && !useSignpost) {
                    addSignpost = false;
                    signPostText = "";
                }
                hasSignPost = true;
                if (addSignpost) {
                    if (com.navdy.hud.app.maps.here.HereMapUtil.needSeparator(mainSignPostBuilder)) {
                        mainSignPostBuilder.append(SLASH);
                    }
                    mainSignPostBuilder.append(signPostText);
                }
            }
            java.lang.String toInstruction = null;
            if (!android.text.TextUtils.isEmpty(name)) {
                if (hasSignPost && !signPostText.contains(name)) {
                    boolean addOnto = true;
                    if (maneuverMatch && !useToRoad) {
                        addOnto = false;
                    }
                    if (addOnto) {
                        if (pendingRoadInfo != null && pendingRoadInfo.hasTo) {
                            toInstruction = mainSignPostBuilder.toString();
                            mainSignPostBuilder.setLength(0);
                        } else if (com.navdy.hud.app.maps.here.HereMapUtil.needSeparator(mainSignPostBuilder)) {
                            mainSignPostBuilder.append(" ");
                        }
                        if (!useToRoad) {
                            if (!com.navdy.hud.app.maps.MapSettings.isTbtOntoDisabled()) {
                                mainSignPostBuilder.append(ONTO);
                                mainSignPostBuilder.append(" ");
                                ontoInstructionAdded = true;
                            }
                        } else if (com.navdy.hud.app.maps.here.HereMapUtil.needSeparator(mainSignPostBuilder)) {
                            mainSignPostBuilder.append(" ");
                        }
                        if (android.text.TextUtils.isEmpty(toInstruction) && pendingRoadInfo != null && pendingRoadInfo.hasTo) {
                            pendingRoadInfo.hasTo = false;
                            if (!com.navdy.hud.app.maps.MapSettings.doNotShowTurnTextInTBT()) {
                                mainSignPostBuilder.append(TOWARDS);
                                mainSignPostBuilder.append(" ");
                            }
                            toInstructionAdded = true;
                        }
                        if (!com.navdy.hud.app.maps.MapSettings.isTbtOntoDisabled()) {
                            mainSignPostBuilder.append(com.navdy.hud.app.maps.here.HereMapUtil.concatText(number, name, true, com.navdy.hud.app.maps.here.HereMapUtil.isCurrentRoadHighway(maneuver)));
                        } else if ((pendingRoadInfo == null || !pendingRoadInfo.hasTo) && android.text.TextUtils.isEmpty(signPostText)) {
                            mainSignPostBuilder.append(com.navdy.hud.app.maps.here.HereMapUtil.concatText(number, name, true, com.navdy.hud.app.maps.here.HereMapUtil.isCurrentRoadHighway(maneuver)));
                        }
                    }
                    info.hasNameInSignPost = true;
                }
                if (!info.hasNameInSignPost) {
                    boolean needName = true;
                    if (mainSignPostBuilder.length() > 0 && com.navdy.hud.app.maps.here.HereMapUtil.isCurrentRoadHighway(maneuver)) {
                        needName = false;
                    }
                    if (needName) {
                        boolean bracket = false;
                        if (com.navdy.hud.app.maps.here.HereMapUtil.needSeparator(mainSignPostBuilder)) {
                            mainSignPostBuilder.append(" ");
                        }
                        if (!android.text.TextUtils.isEmpty(number)) {
                            bracket = true;
                            mainSignPostBuilder.append(OPEN_BRACKET);
                        }
                        mainSignPostBuilder.append(name);
                        if (bracket) {
                            mainSignPostBuilder.append(CLOSE_BRACKET);
                        }
                    }
                }
                if (!android.text.TextUtils.isEmpty(toInstruction)) {
                    java.lang.String onto = mainSignPostBuilder.toString();
                    int index = onto.indexOf(ONTO + " ");
                    if (index != -1) {
                        onto = onto.substring(ONTO.length() + index + " ".length()).trim();
                    }
                    if (!android.text.TextUtils.isEmpty(onto) && toInstruction.contains(onto)) {
                        mainSignPostBuilder.setLength(0);
                    }
                    if (com.navdy.hud.app.maps.here.HereMapUtil.needSeparator(mainSignPostBuilder)) {
                        mainSignPostBuilder.append(" ");
                    }
                    if (!com.navdy.hud.app.maps.MapSettings.doNotShowTurnTextInTBT()) {
                        mainSignPostBuilder.append(TOWARDS);
                        mainSignPostBuilder.append(" ");
                    }
                    mainSignPostBuilder.append(toInstruction);
                    toInstructionAdded = true;
                    pendingRoadInfo.hasTo = false;
                } else if (pendingRoadInfo != null && pendingRoadInfo.hasTo) {
                    java.lang.String toInstruction2 = mainSignPostBuilder.toString();
                    mainSignPostBuilder.setLength(0);
                    if (!com.navdy.hud.app.maps.MapSettings.doNotShowTurnTextInTBT()) {
                        mainSignPostBuilder.append(TOWARDS);
                        mainSignPostBuilder.append(" ");
                    }
                    mainSignPostBuilder.append(toInstruction2);
                    toInstructionAdded = true;
                    pendingRoadInfo.hasTo = false;
                }
            }
            if (pendingRoadInfo != null) {
                if (!com.navdy.hud.app.maps.MapSettings.isTbtOntoDisabled() && pendingRoadInfo.enterHighway && !ontoInstructionAdded && !toInstructionAdded && mainSignPostBuilder.length() > 0) {
                    java.lang.String str = mainSignPostBuilder.toString();
                    mainSignPostBuilder.setLength(0);
                    mainSignPostBuilder.append(ONTO);
                    mainSignPostBuilder.append(" ");
                    mainSignPostBuilder.append(str);
                } else if (pendingRoadInfo.hasTo && mainSignPostBuilder.length() > 0) {
                    java.lang.String str2 = mainSignPostBuilder.toString();
                    mainSignPostBuilder.setLength(0);
                    if (!com.navdy.hud.app.maps.MapSettings.doNotShowTurnTextInTBT()) {
                        mainSignPostBuilder.append(TOWARDS);
                        mainSignPostBuilder.append(" ");
                    }
                    mainSignPostBuilder.append(str2);
                }
            }
            sb = mainSignPostBuilder.toString();
        }
        return sb;
    }

    private static com.navdy.service.library.events.navigation.NavigationTurn getNavigationTurn(com.here.android.mpa.routing.Maneuver maneuver) {
        com.here.android.mpa.routing.Maneuver.Icon icon = maneuver.getIcon();
        if (icon == null) {
            return null;
        }
        return (com.navdy.service.library.events.navigation.NavigationTurn) sHereIconToNavigationIconMap.get(icon);
    }

    private static void setNavigationTurnIconId(com.here.android.mpa.routing.Maneuver maneuver, long distance, com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.ManeuverState maneuverState, com.navdy.hud.app.maps.MapEvents.ManeuverDisplay display, com.here.android.mpa.routing.Maneuver nextManeuver, boolean isHighway, com.navdy.hud.app.maps.MapEvents.DestinationDirection destinationDirection) {
        long threshold;
        long threshold2;
        java.lang.Integer resourceId = null;
        java.lang.Integer resourceSoonId = null;
        java.lang.Integer resourceNowId = null;
        switch (maneuverState) {
            case STAY:
                resourceId = java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.tbt_go_straight_soon);
                display.navigationTurn = com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_STRAIGHT;
                break;
            case NEXT:
            case SOON:
            case NOW:
                com.navdy.service.library.events.navigation.NavigationTurn navigationTurn = getNavigationTurn(maneuver);
                display.navigationTurn = navigationTurn;
                if (maneuverState == com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.ManeuverState.NOW || maneuverState == com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.ManeuverState.SOON) {
                    resourceId = (java.lang.Integer) sTurnIconImageMap.get(navigationTurn);
                } else {
                    resourceId = (java.lang.Integer) sTurnIconSoonImageMap.get(navigationTurn);
                }
                resourceNowId = (java.lang.Integer) sTurnIconImageMap.get(navigationTurn);
                resourceSoonId = (java.lang.Integer) sTurnIconSoonImageMapGrey.get(navigationTurn);
                if (!(display.navigationTurn != com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_END || destinationDirection == null || destinationDirection == com.navdy.hud.app.maps.MapEvents.DestinationDirection.UNKNOWN)) {
                    if (maneuverState != com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.ManeuverState.NOW) {
                        switch (destinationDirection) {
                            case LEFT:
                                resourceId = java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.tbt_arrive_left_soon);
                                break;
                            case RIGHT:
                                resourceId = java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.tbt_arrive_right_soon);
                                break;
                        }
                    } else {
                        switch (destinationDirection) {
                            case LEFT:
                                resourceId = java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.tbt_arrive_left);
                                break;
                            case RIGHT:
                                resourceId = java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.tbt_arrive_right);
                                break;
                        }
                    }
                }
                if (nextManeuver == null) {
                    display.turnIconId = -1;
                    display.turnIconNowId = 0;
                    display.turnIconSoonId = 0;
                    break;
                } else {
                    if (isHighway) {
                        threshold = SOON_DISTANCE_THRESHOLD_HIGHWAY;
                    } else {
                        threshold = SOON_DISTANCE_THRESHOLD;
                    }
                    if (distance <= threshold) {
                        long nextDistance = (long) nextManeuver.getDistanceFromPreviousManeuver();
                        if (isHighway) {
                            threshold2 = 1000;
                        } else {
                            threshold2 = 250;
                        }
                        if (nextDistance <= threshold2) {
                            java.lang.Integer nextResourceId = (java.lang.Integer) sTurnIconSoonImageMap.get(getNavigationTurn(nextManeuver));
                            if (resourceId == null) {
                                display.nextTurnIconId = -1;
                            } else {
                                display.nextTurnIconId = nextResourceId.intValue();
                            }
                            sLogger.v("distance is less than threshold " + nextDistance);
                            break;
                        }
                    } else {
                        display.nextTurnIconId = -1;
                        break;
                    }
                }
                break;
        }
        if (resourceId == null) {
            display.turnIconId = -1;
            display.turnIconNowId = 0;
            display.turnIconSoonId = 0;
            return;
        }
        display.turnIconId = resourceId.intValue();
        display.turnIconSoonId = resourceSoonId == null ? 0 : resourceSoonId.intValue();
        display.turnIconNowId = resourceNowId == null ? 0 : resourceNowId.intValue();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:26:?, code lost:
        return;
     */
    public static void setNavigationDistance(long meters, com.navdy.hud.app.maps.MapEvents.ManeuverDisplay maneuverDisplay, boolean extendedUnit, boolean calculateDistanceUnitOnly) {
        synchronized (DISTANCE) {
            com.navdy.hud.app.manager.SpeedManager.SpeedUnit speedUnit = speedManager.getSpeedUnit();
            if (meters >= 2147483647L) {
                meters = 0;
            }
            maneuverDisplay.distanceInMeters = meters;
            com.navdy.hud.app.maps.util.DistanceConverter.convertToDistance(speedUnit, (float) meters, DISTANCE);
            maneuverDisplay.distance = DISTANCE.value;
            maneuverDisplay.distanceUnit = DISTANCE.unit;
            maneuverDisplay.distanceToPendingRoadText = getFormattedDistance(maneuverDisplay.distance, maneuverDisplay.distanceUnit, extendedUnit);
            if (!calculateDistanceUnitOnly) {
                float totalDistance = -1.0f;
                float remainingDistance = -1.0f;
                com.navdy.hud.app.maps.here.HereNavigationManager hereNavigationManager = com.navdy.hud.app.maps.here.HereNavigationManager.getInstance();
                if (hereNavigationManager.isNavigationModeOn()) {
                    com.here.android.mpa.routing.Route route = hereNavigationManager.getCurrentRoute();
                    if (route != null) {
                        remainingDistance = (float) hereNavigationManager.getNavController().getDestinationDistance();
                        if (remainingDistance < 0.0f) {
                            remainingDistance = 0.0f;
                        }
                        totalDistance = (float) route.getLength();
                    }
                }
                if (totalDistance != -1.0f) {
                    com.navdy.hud.app.maps.util.DistanceConverter.convertToDistance(speedUnit, totalDistance, DISTANCE);
                    maneuverDisplay.totalDistance = DISTANCE.value;
                    maneuverDisplay.totalDistanceUnit = DISTANCE.unit;
                    com.navdy.hud.app.maps.util.DistanceConverter.convertToDistance(speedUnit, remainingDistance, DISTANCE);
                    maneuverDisplay.totalDistanceRemaining = DISTANCE.value;
                    maneuverDisplay.totalDistanceRemainingUnit = DISTANCE.unit;
                }
            }
        }
    }

    static com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.ManeuverState getManeuverState(long distance, boolean isHighway) {
        double nextThreshold;
        double soonThreshold;
        long nowThreshold;
        com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.ManeuverState maneuverState;
        if (isHighway) {
            nextThreshold = 16093.0d;
            soonThreshold = 3218.0d;
            nowThreshold = NOW_DISTANCE_THRESHOLD_HIGHWAY;
        } else {
            nextThreshold = 6437.0d;
            soonThreshold = 1287.0d;
            nowThreshold = 30;
        }
        if (distance <= nowThreshold) {
            maneuverState = com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.ManeuverState.NOW;
        } else if (((double) distance) <= soonThreshold) {
            maneuverState = com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.ManeuverState.SOON;
        } else if (((double) distance) <= nextThreshold) {
            maneuverState = com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.ManeuverState.NEXT;
        } else {
            maneuverState = com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.ManeuverState.STAY;
        }
        if (sLogger.isLoggable(2)) {
            sLogger.v("maneuverState=" + maneuverState + " distance=" + distance + " isHighway=" + isHighway);
        }
        return maneuverState;
    }

    public static java.lang.String getUnitDisplayStr(com.navdy.hud.app.manager.SpeedManager.SpeedUnit unit) {
        switch (unit) {
            case MILES_PER_HOUR:
                return UNIT_MILES;
            case KILOMETERS_PER_HOUR:
                return UNIT_KILOMETERS;
            case METERS_PER_SECOND:
                return UNIT_METERS;
            default:
                return "";
        }
    }

    public static java.lang.String getDestinationDirection(com.navdy.hud.app.maps.MapEvents.DestinationDirection direction) {
        switch (direction) {
            case LEFT:
                return DESTINATION_LEFT;
            case RIGHT:
                return DESTINATION_RIGHT;
            default:
                return null;
        }
    }

    public static int getDestinationIcon(com.navdy.hud.app.maps.MapEvents.DestinationDirection direction) {
        switch (direction) {
            case LEFT:
                return com.navdy.hud.app.R.drawable.icon_pin_dot_destination_left;
            case RIGHT:
                return com.navdy.hud.app.R.drawable.icon_pin_dot_destination_right;
            default:
                return com.navdy.hud.app.R.drawable.icon_pin_dot_destination;
        }
    }

    private static com.here.android.mpa.common.GeoCoordinate getEstimatedPointAfromManeuver(com.here.android.mpa.routing.Maneuver maneuver, com.here.android.mpa.common.GeoCoordinate destination) {
        if (maneuver == null || destination == null) {
            return null;
        }
        com.here.android.mpa.common.GeoCoordinate last = null;
        java.util.List<com.here.android.mpa.common.GeoCoordinate> geoCoordinates = maneuver.getManeuverGeometry();
        for (int i = geoCoordinates.size() - 1; i >= 0; i--) {
            com.here.android.mpa.common.GeoCoordinate geo = (com.here.android.mpa.common.GeoCoordinate) geoCoordinates.get(i);
            int distance = (int) geo.distanceTo(destination);
            if (distance >= 30) {
                sLogger.v("remaining=" + distance);
                return geo;
            }
            last = geo;
        }
        if (last != null) {
            sLogger.v("remaining=" + ((int) last.distanceTo(destination)));
        }
        return last;
    }

    public static java.lang.String getFormattedDistance(float distance, com.navdy.service.library.events.navigation.DistanceUnit unit, boolean extendedUnit) {
        java.lang.String distanceFormat;
        java.lang.String distanceFormat2;
        switch (unit) {
            case DISTANCE_MILES:
                if (distance < 10.0f) {
                    distanceFormat2 = "%.1f %s";
                } else {
                    distanceFormat2 = "%.0f %s";
                }
                java.lang.Object[] objArr = new java.lang.Object[2];
                objArr[0] = java.lang.Float.valueOf(distance);
                java.lang.String str = !extendedUnit ? UNIT_MILES : distance <= 1.0f ? UNIT_MILES_EXTENDED_SINGULAR : UNIT_MILES_EXTENDED;
                objArr[1] = str;
                return java.lang.String.format(distanceFormat2, objArr);
            case DISTANCE_FEET:
                java.lang.String str2 = "%.0f %s";
                java.lang.Object[] objArr2 = new java.lang.Object[2];
                objArr2[0] = java.lang.Float.valueOf(com.navdy.hud.app.maps.here.HereMapUtil.roundToIntegerStep(10, distance));
                java.lang.String str3 = !extendedUnit ? UNIT_FEET : distance <= 1.0f ? UNIT_FEET_EXTENDED_SINGULAR : UNIT_FEET_EXTENDED;
                objArr2[1] = str3;
                return java.lang.String.format(str2, objArr2);
            case DISTANCE_KMS:
                if (distance < 10.0f) {
                    distanceFormat = "%.1f %s";
                } else {
                    distanceFormat = "%.0f %s";
                }
                java.lang.Object[] objArr3 = new java.lang.Object[2];
                objArr3[0] = java.lang.Float.valueOf(distance);
                java.lang.String str4 = !extendedUnit ? UNIT_KILOMETERS : distance <= 1.0f ? UNIT_KILOMETERS_EXTENDED_SINGULAR : UNIT_KILOMETERS_EXTENDED;
                objArr3[1] = str4;
                return java.lang.String.format(distanceFormat, objArr3);
            case DISTANCE_METERS:
                java.lang.String str5 = "%.0f %s";
                java.lang.Object[] objArr4 = new java.lang.Object[2];
                objArr4[0] = java.lang.Float.valueOf(com.navdy.hud.app.maps.here.HereMapUtil.roundToIntegerStep(5, distance));
                java.lang.String str6 = !extendedUnit ? UNIT_METERS : distance <= 1.0f ? UNIT_METERS_EXTENDED_SINGULAR : UNIT_METERS_EXTENDED;
                objArr4[1] = str6;
                return java.lang.String.format(str5, objArr4);
            default:
                return java.lang.String.valueOf((int) distance);
        }
    }

    public static boolean canShowTurnText(java.lang.String turnText) {
        if (!com.navdy.hud.app.maps.MapSettings.doNotShowTurnTextInTBT() || allowedTurnTextMap.contains(turnText)) {
            return true;
        }
        return false;
    }

    public static boolean shouldShowTurnText(java.lang.String turnText) {
        return allowedTurnTextMap.contains(turnText);
    }
}
