package com.navdy.hud.app.maps.here;

public class HereMapAnimator {
    private static final int DRASTIC_HEADING_CHANGE = 45;
    private static final int INVALID_STEP = -1;
    private static final int INVALID_TILT = -1;
    private static final int INVALID_ZOOM = -1;
    private static final int MAX_DIFFERENCE_HEADING_FILTER = 120;
    private static final int MAX_HEADING = 360;
    private static final long MAX_INTERVAL_HEADING_UPDATE = 3000;
    private static final int MIN_HEADING = 0;
    private static final double MIN_SPEED_FILTER = 0.44704d;
    private static final double MPH_TO_MS = 0.44704d;
    private static final long REFRESH_TIME_THROTTLE = ((long) (1000 / com.navdy.hud.app.maps.MapSettings.getMapFps()));
    /* access modifiers changed from: private */
    public static final java.lang.Object geoPositionLock = new java.lang.Object();
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.maps.here.HereMapAnimator.class);
    /* access modifiers changed from: private */
    public static final java.lang.Object tiltLock = new java.lang.Object();
    /* access modifiers changed from: private */
    public static final java.lang.Object zoomLock = new java.lang.Object();
    /* access modifiers changed from: private */
    public com.here.android.mpa.common.GeoPosition currentGeoPosition;
    /* access modifiers changed from: private */
    public float currentTilt;
    /* access modifiers changed from: private */
    public double currentZoom;
    /* access modifiers changed from: private */
    public long geoPositionUpdateInterval;
    /* access modifiers changed from: private */
    public long lastGeoPositionUpdateTime;
    private volatile float lastHeading = -1.0f;
    private long lastPreDrawTime;
    /* access modifiers changed from: private */
    public long lastTiltUpdateTime;
    /* access modifiers changed from: private */
    public long lastZoomUpdateTime;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.maps.here.HereMapController mapController;
    private com.here.android.mpa.mapping.OnMapRenderListener mapRenderListener;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.maps.here.HereMapAnimator.AnimationMode mode = com.navdy.hud.app.maps.here.HereMapAnimator.AnimationMode.NONE;
    private com.navdy.hud.app.maps.here.HereNavController navController;
    private long preDrawFinishTime;
    /* access modifiers changed from: private */
    public volatile boolean renderingEnabled = true;
    /* access modifiers changed from: private */
    public long tiltUpdateInterval;
    /* access modifiers changed from: private */
    public long zoomUpdateInterval;

    enum AnimationMode {
        NONE,
        ANIMATION,
        ZOOM_TILT_ANIMATION
    }

    class Anon1 implements com.here.android.mpa.mapping.OnMapRenderListener {
        Anon1() {
        }

        public void onPreDraw() {
            com.navdy.hud.app.maps.here.HereMapAnimator.this.onPreDraw();
        }

        public void onPostDraw(boolean invalidated, long renderTime) {
            com.navdy.hud.app.maps.here.HereMapAnimator.this.onPostDraw(invalidated, renderTime);
        }

        public void onSizeChanged(int width, int height) {
        }

        public void onGraphicsDetached() {
        }

        public void onRenderBufferCreated() {
        }
    }

    class Anon2 implements java.lang.Runnable {
        final /* synthetic */ com.here.android.mpa.common.GeoPosition val$geoPosition;

        Anon2(com.here.android.mpa.common.GeoPosition geoPosition) {
            this.val$geoPosition = geoPosition;
        }

        public void run() {
            com.here.android.mpa.common.GeoPosition oldPosition;
            long interval = 0;
            if (com.navdy.hud.app.maps.here.HereMapAnimator.this.lastGeoPositionUpdateTime > 0) {
                interval = android.os.SystemClock.elapsedRealtime() - com.navdy.hud.app.maps.here.HereMapAnimator.this.lastGeoPositionUpdateTime;
            }
            if (!com.navdy.hud.app.maps.here.HereMapAnimator.this.isValidGeoPosition(this.val$geoPosition)) {
                com.navdy.hud.app.maps.here.HereMapAnimator.logger.v("filtering out spurious heading: " + this.val$geoPosition.getHeading() + " speed:" + this.val$geoPosition.getSpeed());
                return;
            }
            com.navdy.hud.app.maps.here.HereMapAnimator.this.lastGeoPositionUpdateTime = android.os.SystemClock.elapsedRealtime();
            synchronized (com.navdy.hud.app.maps.here.HereMapAnimator.geoPositionLock) {
                com.navdy.hud.app.maps.here.HereMapAnimator.this.geoPositionUpdateInterval = interval;
                oldPosition = com.navdy.hud.app.maps.here.HereMapAnimator.this.currentGeoPosition;
                com.navdy.hud.app.maps.here.HereMapAnimator.this.currentGeoPosition = this.val$geoPosition;
            }
            if (oldPosition == null && com.navdy.hud.app.maps.here.HereMapAnimator.this.renderingEnabled) {
                com.navdy.hud.app.maps.here.HereMapAnimator.logger.v("initial setCenter");
                com.navdy.hud.app.maps.here.HereMapAnimator.this.mapController.setCenter(com.navdy.hud.app.maps.here.HereMapAnimator.this.currentGeoPosition.getCoordinate(), com.here.android.mpa.mapping.Map.Animation.NONE, -1.0d, (float) com.navdy.hud.app.maps.here.HereMapAnimator.this.currentGeoPosition.getHeading(), -1.0f);
            }
        }
    }

    class Anon3 implements java.lang.Runnable {
        final /* synthetic */ double val$zoom;

        Anon3(double d) {
            this.val$zoom = d;
        }

        public void run() {
            long interval = 0;
            if (com.navdy.hud.app.maps.here.HereMapAnimator.this.lastZoomUpdateTime > 0) {
                interval = android.os.SystemClock.elapsedRealtime() - com.navdy.hud.app.maps.here.HereMapAnimator.this.lastZoomUpdateTime;
            }
            if (!com.navdy.hud.app.maps.here.HereMapAnimator.this.isValidZoom(this.val$zoom)) {
                com.navdy.hud.app.maps.here.HereMapAnimator.logger.v("filtering out spurious zoom: " + this.val$zoom);
                return;
            }
            com.navdy.hud.app.maps.here.HereMapAnimator.this.lastZoomUpdateTime = android.os.SystemClock.elapsedRealtime();
            synchronized (com.navdy.hud.app.maps.here.HereMapAnimator.zoomLock) {
                com.navdy.hud.app.maps.here.HereMapAnimator.this.zoomUpdateInterval = interval;
                com.navdy.hud.app.maps.here.HereMapAnimator.this.currentZoom = this.val$zoom;
            }
        }
    }

    class Anon4 implements java.lang.Runnable {
        final /* synthetic */ float val$tilt;

        Anon4(float f) {
            this.val$tilt = f;
        }

        public void run() {
            long interval = 0;
            if (com.navdy.hud.app.maps.here.HereMapAnimator.this.lastTiltUpdateTime > 0) {
                interval = android.os.SystemClock.elapsedRealtime() - com.navdy.hud.app.maps.here.HereMapAnimator.this.lastTiltUpdateTime;
            }
            if (!com.navdy.hud.app.maps.here.HereMapAnimator.this.isValidTilt(this.val$tilt)) {
                com.navdy.hud.app.maps.here.HereMapAnimator.logger.v("filtering out spurious tilt: " + this.val$tilt);
                return;
            }
            com.navdy.hud.app.maps.here.HereMapAnimator.this.lastTiltUpdateTime = android.os.SystemClock.elapsedRealtime();
            synchronized (com.navdy.hud.app.maps.here.HereMapAnimator.tiltLock) {
                com.navdy.hud.app.maps.here.HereMapAnimator.this.tiltUpdateInterval = interval;
                com.navdy.hud.app.maps.here.HereMapAnimator.this.currentTilt = this.val$tilt;
            }
        }
    }

    class Anon5 implements java.lang.Runnable {
        Anon5() {
        }

        public void run() {
            com.here.android.mpa.common.GeoPosition pos = com.navdy.hud.app.maps.here.HereMapAnimator.this.currentGeoPosition;
            if (pos == null) {
                return;
            }
            if (com.navdy.hud.app.maps.here.HereMapAnimator.this.mode == com.navdy.hud.app.maps.here.HereMapAnimator.AnimationMode.NONE) {
                com.navdy.hud.app.maps.here.HereMapAnimator.this.setGeoPosition(pos);
                com.navdy.hud.app.maps.here.HereMapAnimator.logger.v("rendering: set last pos");
                return;
            }
            com.navdy.hud.app.maps.here.HereMapAnimator.logger.v("rendering: set center");
            com.navdy.hud.app.maps.here.HereMapAnimator.this.mapController.setCenter(pos.getCoordinate(), com.here.android.mpa.mapping.Map.Animation.NONE, -1.0d, (float) pos.getHeading(), -1.0f);
        }
    }

    HereMapAnimator(com.navdy.hud.app.maps.here.HereMapAnimator.AnimationMode mode2, com.navdy.hud.app.maps.here.HereMapController mapController2, com.navdy.hud.app.maps.here.HereNavController navController2) {
        this.mode = mode2;
        if (mode2 != com.navdy.hud.app.maps.here.HereMapAnimator.AnimationMode.NONE) {
            logger.v("animation throttle = " + REFRESH_TIME_THROTTLE);
            this.mapRenderListener = new com.navdy.hud.app.maps.here.HereMapAnimator.Anon1();
        }
        this.mapController = mapController2;
        this.navController = navController2;
        this.currentGeoPosition = null;
        this.currentZoom = mapController2.getZoomLevel();
        this.currentTilt = mapController2.getTilt();
    }

    public com.here.android.mpa.mapping.OnMapRenderListener getMapRenderListener() {
        return this.mapRenderListener;
    }

    public com.navdy.hud.app.maps.here.HereMapAnimator.AnimationMode getAnimationMode() {
        return this.mode;
    }

    public void setGeoPosition(@android.support.annotation.NonNull com.here.android.mpa.common.GeoPosition geoPosition) {
        switch (this.mode) {
            case NONE:
                double heading = geoPosition.getHeading();
                boolean animate = false;
                if (this.lastHeading != -1.0f) {
                    double orientationDiff = getOrientationDiff(heading, (double) this.lastHeading);
                    double speed = geoPosition.getSpeed();
                    if (speed <= 0.44704d && java.lang.Math.abs(orientationDiff) >= 120.0d) {
                        logger.v("filtering out spurious heading last: " + this.lastHeading + " new:" + heading + " speed:" + speed);
                        return;
                    } else if (java.lang.Math.abs(orientationDiff) >= 45.0d) {
                        animate = true;
                    }
                }
                this.currentGeoPosition = geoPosition;
                this.lastHeading = (float) heading;
                if (this.renderingEnabled && this.mapController.getState() == com.navdy.hud.app.maps.here.HereMapController.State.AR_MODE) {
                    this.mapController.setCenter(geoPosition.getCoordinate(), animate ? com.here.android.mpa.mapping.Map.Animation.BOW : com.here.android.mpa.mapping.Map.Animation.NONE, -1.0d, this.lastHeading, -1.0f);
                    return;
                }
                return;
            default:
                com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.maps.here.HereMapAnimator.Anon2(geoPosition), 17);
                return;
        }
    }

    /* access modifiers changed from: 0000 */
    public void setZoom(double zoom) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.maps.here.HereMapAnimator.Anon3(zoom), 17);
    }

    /* access modifiers changed from: 0000 */
    public void setTilt(float tilt) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.maps.here.HereMapAnimator.Anon4(tilt), 17);
    }

    /* access modifiers changed from: private */
    public void onPreDraw() {
        long geoPositionUpdateIntervalCopy;
        com.here.android.mpa.common.GeoPosition currentGeoPositionCopy;
        long zoomUpdateIntervalCopy;
        double currentZoomCopy;
        long tiltUpdateIntervalCopy;
        float currentTiltCopy;
        if (this.currentGeoPosition != null) {
            long preDrawStartTime = android.os.SystemClock.elapsedRealtime();
            if (this.lastPreDrawTime > 0) {
                long preDrawInterval = preDrawStartTime - this.lastPreDrawTime;
                this.lastPreDrawTime = preDrawStartTime;
                com.here.android.mpa.common.GeoPosition geoPosition = null;
                double zoom = -1.0d;
                float tilt = -1.0f;
                double geoPositionStep = -1.0d;
                double zoomStep = -1.0d;
                double tiltStep = -1.0d;
                synchronized (geoPositionLock) {
                    geoPositionUpdateIntervalCopy = this.geoPositionUpdateInterval;
                    currentGeoPositionCopy = this.currentGeoPosition;
                }
                synchronized (zoomLock) {
                    zoomUpdateIntervalCopy = this.zoomUpdateInterval;
                    currentZoomCopy = this.currentZoom;
                }
                synchronized (tiltLock) {
                    tiltUpdateIntervalCopy = this.tiltUpdateInterval;
                    currentTiltCopy = this.currentTilt;
                }
                boolean hasGeoPositionUpdate = currentGeoPositionCopy != null && geoPositionUpdateIntervalCopy > 0;
                boolean hasZoomUpdate = currentZoomCopy >= 0.0d && zoomUpdateIntervalCopy > 0;
                boolean hasTiltUpdate = currentTiltCopy >= 0.0f && tiltUpdateIntervalCopy > 0;
                if (hasGeoPositionUpdate) {
                    geoPosition = currentGeoPositionCopy;
                    geoPositionStep = ((double) preDrawInterval) / ((double) geoPositionUpdateIntervalCopy);
                }
                if (hasZoomUpdate) {
                    zoom = currentZoomCopy;
                    zoomStep = ((double) preDrawInterval) / ((double) zoomUpdateIntervalCopy);
                }
                if (hasTiltUpdate) {
                    tilt = currentTiltCopy;
                    tiltStep = ((double) preDrawInterval) / ((double) tiltUpdateIntervalCopy);
                }
                if (hasGeoPositionUpdate || hasZoomUpdate || hasTiltUpdate) {
                    double step = java.lang.Math.max(geoPositionStep, java.lang.Math.max(zoomStep, tiltStep));
                    if (step > 1.0d) {
                        step = 1.0d;
                    }
                    com.here.android.mpa.common.GeoCoordinate newCenter = getNewCenter(geoPosition, step);
                    float newHeading = getNewHeading(geoPosition, step);
                    double newZoomLevel = getNewZoomLevel(zoom, step);
                    float newTilt = getNewTilt(tilt, step);
                    if (this.renderingEnabled && this.mapController.getState() == com.navdy.hud.app.maps.here.HereMapController.State.AR_MODE) {
                        this.mapController.setCenter(newCenter, com.here.android.mpa.mapping.Map.Animation.NONE, newZoomLevel, newHeading, newTilt);
                    }
                    this.preDrawFinishTime = android.os.SystemClock.elapsedRealtime();
                    if (logger.isLoggable(2)) {
                        logger.v("predraw logic took " + (this.preDrawFinishTime - preDrawStartTime) + " ms");
                        return;
                    }
                    return;
                }
                this.preDrawFinishTime = android.os.SystemClock.elapsedRealtime();
                return;
            }
            this.lastPreDrawTime = preDrawStartTime;
        }
    }

    /* access modifiers changed from: private */
    public void onPostDraw(boolean invalidated, long renderTime) {
        long lastRenderTime = android.os.SystemClock.elapsedRealtime() - this.preDrawFinishTime;
        long sleepTime = REFRESH_TIME_THROTTLE - lastRenderTime;
        if (logger.isLoggable(2)) {
            logger.v("last render took " + lastRenderTime + " ms");
        }
        if (sleepTime > 0) {
            if (logger.isLoggable(2)) {
                logger.v("sleeping render thread for " + sleepTime + " ms");
            }
            try {
                java.lang.Thread.sleep(sleepTime);
            } catch (java.lang.InterruptedException e) {
                logger.e((java.lang.Throwable) e);
            }
        } else if (logger.isLoggable(2)) {
            logger.v("render thread could not sleep");
        }
    }

    /* access modifiers changed from: private */
    public boolean isValidGeoPosition(@android.support.annotation.NonNull com.here.android.mpa.common.GeoPosition geoPosition) {
        com.here.android.mpa.common.GeoPosition currentGeoPositionCopy;
        long geoPositionUpdateIntervalCopy;
        synchronized (geoPositionLock) {
            currentGeoPositionCopy = this.currentGeoPosition;
            geoPositionUpdateIntervalCopy = this.geoPositionUpdateInterval;
        }
        if (currentGeoPositionCopy == null) {
            return true;
        }
        double orientationDiff = getOrientationDiff(geoPosition.getHeading(), currentGeoPositionCopy.getHeading());
        if (geoPosition.getSpeed() >= 0.44704d || java.lang.Math.abs(orientationDiff) <= 120.0d || geoPositionUpdateIntervalCopy >= 3000) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: private */
    public boolean isValidZoom(double zoom) {
        return zoom >= 0.0d && zoom <= 20.0d;
    }

    /* access modifiers changed from: private */
    public boolean isValidTilt(float tilt) {
        return tilt >= 0.0f && tilt < 90.0f;
    }

    private double getOrientationDiff(double firstOrientation, double secondOrientation) {
        double orientationDiff = firstOrientation - secondOrientation;
        if (orientationDiff > 180.0d) {
            return orientationDiff - 360.0d;
        }
        if (orientationDiff < -180.0d) {
            return orientationDiff + 360.0d;
        }
        return orientationDiff;
    }

    private com.here.android.mpa.common.GeoCoordinate getNewCenter(@android.support.annotation.Nullable com.here.android.mpa.common.GeoPosition geoPosition, double step) {
        com.here.android.mpa.common.GeoCoordinate currentCenter = this.mapController.getCenter();
        if (geoPosition == null || step == -1.0d) {
            return currentCenter;
        }
        com.here.android.mpa.common.GeoCoordinate newCoords = geoPosition.getCoordinate();
        return new com.here.android.mpa.common.GeoCoordinate(currentCenter.getLatitude() + ((newCoords.getLatitude() - currentCenter.getLatitude()) * step), currentCenter.getLongitude() + ((newCoords.getLongitude() - currentCenter.getLongitude()) * step));
    }

    private float getNewHeading(@android.support.annotation.Nullable com.here.android.mpa.common.GeoPosition geoPosition, double step) {
        float currentOrientation = this.mapController.getOrientation();
        if (geoPosition == null || step == -1.0d) {
            return currentOrientation;
        }
        double newHeading = ((double) currentOrientation) + (step * getOrientationDiff((double) ((float) geoPosition.getHeading()), (double) currentOrientation));
        if (newHeading > 360.0d) {
            newHeading -= 360.0d;
        } else if (newHeading < 0.0d) {
            newHeading += 360.0d;
        }
        return (float) newHeading;
    }

    private double getNewZoomLevel(double zoom, double step) {
        if (this.mode != com.navdy.hud.app.maps.here.HereMapAnimator.AnimationMode.ZOOM_TILT_ANIMATION || zoom == -1.0d || step == -1.0d) {
            return -1.0d;
        }
        double currentZoomLevel = this.mapController.getZoomLevel();
        double animatorZoom = currentZoomLevel + ((zoom - currentZoomLevel) * step);
        logger.v("animatorZoom: " + animatorZoom);
        return animatorZoom;
    }

    private float getNewTilt(float tilt, double step) {
        if (this.mode != com.navdy.hud.app.maps.here.HereMapAnimator.AnimationMode.ZOOM_TILT_ANIMATION || tilt == -1.0f || step == -1.0d) {
            return -1.0f;
        }
        float currentTilt2 = this.mapController.getTilt();
        float animatorTilt = (float) (((double) currentTilt2) + (((double) (tilt - currentTilt2)) * step));
        logger.v("animatorTilt: " + animatorTilt);
        return animatorTilt;
    }

    public void startMapRendering() {
        logger.v("startMapRendering: rendering enabled");
        this.renderingEnabled = true;
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.maps.here.HereMapAnimator.Anon5(), 17);
    }

    public void stopMapRendering() {
        logger.v("stopMapRendering: rendering disabled");
        this.renderingEnabled = false;
    }

    public void clearState() {
        logger.v("clearState");
        this.lastHeading = -1.0f;
    }
}
