package com.navdy.hud.app.maps.here;

public final class HereMapsManager$$InjectAdapter extends dagger.internal.Binding<com.navdy.hud.app.maps.here.HereMapsManager> implements dagger.MembersInjector<com.navdy.hud.app.maps.here.HereMapsManager> {
    private dagger.internal.Binding<com.squareup.otto.Bus> bus;
    private dagger.internal.Binding<com.navdy.hud.app.profile.DriverProfileManager> mDriverProfileManager;
    private dagger.internal.Binding<com.navdy.hud.app.device.PowerManager> powerManager;
    private dagger.internal.Binding<android.content.SharedPreferences> sharedPreferences;

    public HereMapsManager$$InjectAdapter() {
        super(null, "members/com.navdy.hud.app.maps.here.HereMapsManager", false, com.navdy.hud.app.maps.here.HereMapsManager.class);
    }

    public void attach(dagger.internal.Linker linker) {
        this.bus = linker.requestBinding("com.squareup.otto.Bus", com.navdy.hud.app.maps.here.HereMapsManager.class, getClass().getClassLoader());
        this.powerManager = linker.requestBinding("com.navdy.hud.app.device.PowerManager", com.navdy.hud.app.maps.here.HereMapsManager.class, getClass().getClassLoader());
        this.sharedPreferences = linker.requestBinding("android.content.SharedPreferences", com.navdy.hud.app.maps.here.HereMapsManager.class, getClass().getClassLoader());
        this.mDriverProfileManager = linker.requestBinding("com.navdy.hud.app.profile.DriverProfileManager", com.navdy.hud.app.maps.here.HereMapsManager.class, getClass().getClassLoader());
    }

    public void getDependencies(java.util.Set<dagger.internal.Binding<?>> set, java.util.Set<dagger.internal.Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.bus);
        injectMembersBindings.add(this.powerManager);
        injectMembersBindings.add(this.sharedPreferences);
        injectMembersBindings.add(this.mDriverProfileManager);
    }

    public void injectMembers(com.navdy.hud.app.maps.here.HereMapsManager object) {
        object.bus = (com.squareup.otto.Bus) this.bus.get();
        object.powerManager = (com.navdy.hud.app.device.PowerManager) this.powerManager.get();
        object.sharedPreferences = (android.content.SharedPreferences) this.sharedPreferences.get();
        object.mDriverProfileManager = (com.navdy.hud.app.profile.DriverProfileManager) this.mDriverProfileManager.get();
    }
}
