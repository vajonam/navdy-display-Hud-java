package com.navdy.hud.app.maps.here;

public class HerePositionUpdateListener extends com.here.android.mpa.guidance.NavigationManager.PositionListener {
    private final com.squareup.otto.Bus bus;
    private com.navdy.hud.app.maps.here.HereNavigationManager hereNavigationManager;
    private com.here.android.mpa.common.GeoPosition lastGeoPosition;
    private long lastGeoPositionTime;
    private com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger("HerePositionListener");
    private com.navdy.hud.app.maps.here.HereNavController navController;
    private java.lang.String tag;

    class Anon1 implements java.lang.Runnable {
        final /* synthetic */ com.here.android.mpa.common.GeoPosition val$geoPosition;

        Anon1(com.here.android.mpa.common.GeoPosition geoPosition) {
            this.val$geoPosition = geoPosition;
        }

        public void run() {
            com.navdy.hud.app.maps.here.HerePositionUpdateListener.this.handlePositionUpdate(this.val$geoPosition);
        }
    }

    public static class MapMatchEvent {
        public final com.here.android.mpa.common.MatchedGeoPosition mapMatch;

        public MapMatchEvent(com.here.android.mpa.common.MatchedGeoPosition mapMatch2) {
            this.mapMatch = mapMatch2;
        }
    }

    HerePositionUpdateListener(java.lang.String tag2, com.navdy.hud.app.maps.here.HereNavController navController2, com.navdy.hud.app.maps.here.HereNavigationManager hereNavigationManager2, com.squareup.otto.Bus bus2) {
        this.tag = tag2;
        this.navController = navController2;
        this.hereNavigationManager = hereNavigationManager2;
        this.bus = bus2;
        bus2.register(this);
    }

    public void onPositionUpdated(com.here.android.mpa.common.GeoPosition geoPosition) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.maps.here.HerePositionUpdateListener.Anon1(geoPosition), 4);
    }

    /* access modifiers changed from: private */
    public void handlePositionUpdate(com.here.android.mpa.common.GeoPosition geoPosition) {
        try {
            if (this.navController.isInitialized() && geoPosition != null) {
                if (this.lastGeoPosition != null) {
                    long t = android.os.SystemClock.elapsedRealtime() - this.lastGeoPositionTime;
                    if (t < 500 && com.navdy.hud.app.maps.here.HereMapUtil.isCoordinateEqual(this.lastGeoPosition.getCoordinate(), geoPosition.getCoordinate())) {
                        if (this.logger.isLoggable(2)) {
                            this.logger.v(this.tag + "GEO-Here-Nav same pos as last:" + t);
                            return;
                        }
                        return;
                    }
                }
                this.lastGeoPosition = geoPosition;
                this.lastGeoPositionTime = android.os.SystemClock.elapsedRealtime();
                if (this.logger.isLoggable(2)) {
                    this.logger.v(this.tag + " onPosUpdated: road name =" + com.navdy.hud.app.maps.here.HereMapUtil.getCurrentRoadName());
                }
                postTrackingEvent(geoPosition);
                this.hereNavigationManager.refreshNavigationInfo();
            }
        } catch (Throwable t2) {
            this.logger.e(t2);
            com.navdy.hud.app.util.CrashReporter.getInstance().reportNonFatalException(t2);
        }
    }

    private void postTrackingEvent(com.here.android.mpa.common.GeoPosition geoPosition) {
        if (this.hereNavigationManager.isNavigationModeOn()) {
            com.here.android.mpa.routing.RouteTta routeTta = this.navController.getTta(com.here.android.mpa.routing.Route.TrafficPenaltyMode.OPTIMAL, true);
            if (routeTta != null) {
                this.bus.post(new com.navdy.hud.app.framework.trips.TripManager.TrackingEvent(geoPosition, this.hereNavigationManager.getCurrentDestinationIdentifier(), routeTta.getDuration(), (int) this.navController.getDestinationDistance()));
            } else {
                this.bus.post(new com.navdy.hud.app.framework.trips.TripManager.TrackingEvent(geoPosition));
            }
        } else {
            this.bus.post(new com.navdy.hud.app.framework.trips.TripManager.TrackingEvent(geoPosition));
        }
    }
}
