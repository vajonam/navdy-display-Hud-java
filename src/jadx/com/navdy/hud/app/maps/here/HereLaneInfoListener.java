package com.navdy.hud.app.maps.here;

public class HereLaneInfoListener extends com.here.android.mpa.guidance.NavigationManager.LaneInformationListener {
    /* access modifiers changed from: private */
    public static final com.navdy.hud.app.maps.MapEvents.LaneData EMPTY_LANE_DATA = new com.navdy.hud.app.maps.MapEvents.LaneData(com.navdy.hud.app.maps.MapEvents.LaneData.Position.OFF_ROUTE, null);
    /* access modifiers changed from: private */
    public static final com.navdy.hud.app.maps.MapEvents.HideTrafficLaneInfo HIDE_LANE_INFO = new com.navdy.hud.app.maps.MapEvents.HideTrafficLaneInfo();
    /* access modifiers changed from: private */
    public static com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.maps.here.HereLaneInfoListener.class);
    /* access modifiers changed from: private */
    public com.squareup.otto.Bus bus;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.maps.MapEvents.DisplayTrafficLaneInfo lastLanesData;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.maps.here.HereNavController navController;
    /* access modifiers changed from: private */
    public volatile boolean running;
    private java.lang.Runnable startRunnable = new com.navdy.hud.app.maps.here.HereLaneInfoListener.Anon1();
    private java.lang.Runnable stopRunnable = new com.navdy.hud.app.maps.here.HereLaneInfoListener.Anon2();
    private com.navdy.service.library.task.TaskManager taskManager = com.navdy.service.library.task.TaskManager.getInstance();
    /* access modifiers changed from: private */
    public java.util.ArrayList<com.here.android.mpa.guidance.LaneInformation.Direction> tempDirections = new java.util.ArrayList<>();
    /* access modifiers changed from: private */
    public java.util.ArrayList<com.here.android.mpa.guidance.LaneInformation.Direction> tempDirections2 = new java.util.ArrayList<>();
    /* access modifiers changed from: private */
    public java.util.ArrayList<com.navdy.hud.app.maps.MapEvents.LaneData> tempLanes = new java.util.ArrayList<>();

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            com.navdy.hud.app.maps.here.HereLaneInfoListener.this.navController.addLaneInfoListener(new java.lang.ref.WeakReference(com.navdy.hud.app.maps.here.HereLaneInfoListener.this));
            com.navdy.hud.app.maps.here.HereLaneInfoListener.sLogger.v("added lane info listener");
        }
    }

    class Anon2 implements java.lang.Runnable {
        Anon2() {
        }

        public void run() {
            com.navdy.hud.app.maps.here.HereLaneInfoListener.this.navController.removeLaneInfoListener(com.navdy.hud.app.maps.here.HereLaneInfoListener.this);
            com.navdy.hud.app.maps.here.HereLaneInfoListener.sLogger.v("removed lane info listener");
        }
    }

    class Anon3 implements java.lang.Runnable {
        final /* synthetic */ java.util.List val$laneInfoList;

        Anon3(java.util.List list) {
            this.val$laneInfoList = list;
        }

        public void run() {
            if (this.val$laneInfoList != null && this.val$laneInfoList.size() != 0) {
                try {
                    int numLanes = this.val$laneInfoList.size();
                    if (com.navdy.hud.app.maps.here.HereLaneInfoListener.sLogger.isLoggable(2)) {
                        com.navdy.hud.app.maps.here.HereLaneInfoListener.sLogger.v("onShowLaneInfo: size=" + numLanes);
                        for (com.here.android.mpa.guidance.LaneInformation laneInfo : this.val$laneInfoList) {
                            com.navdy.hud.app.maps.here.HereLaneInfoListener.sLogger.v("onShowLaneInfo:recommended state:" + laneInfo.getRecommendationState().name());
                            java.util.EnumSet<com.here.android.mpa.guidance.LaneInformation.Direction> directions = laneInfo.getDirections();
                            if (directions != null) {
                                java.util.Iterator it = directions.iterator();
                                while (it.hasNext()) {
                                    com.navdy.hud.app.maps.here.HereLaneInfoListener.sLogger.v("onShowLaneInfo:direction:" + ((com.here.android.mpa.guidance.LaneInformation.Direction) it.next()).name());
                                }
                            }
                        }
                    }
                    int hasHighlyRecommendedLanes = 0;
                    int hasRecommendedLanes = 0;
                    int hasNonRecommendedLanes = 0;
                    for (int i = 0; i < numLanes; i++) {
                        switch (com.navdy.hud.app.maps.here.HereLaneInfoListener.Anon4.$SwitchMap$com$here$android$mpa$guidance$LaneInformation$RecommendationState[((com.here.android.mpa.guidance.LaneInformation) this.val$laneInfoList.get(i)).getRecommendationState().ordinal()]) {
                            case 1:
                                hasNonRecommendedLanes++;
                                break;
                            case 2:
                                hasHighlyRecommendedLanes++;
                                break;
                            case 3:
                                hasRecommendedLanes++;
                                break;
                        }
                    }
                    if (hasHighlyRecommendedLanes == 0 && hasRecommendedLanes == 0) {
                        if (com.navdy.hud.app.maps.here.HereLaneInfoListener.this.lastLanesData != null) {
                            com.navdy.hud.app.maps.here.HereLaneInfoListener.this.lastLanesData = null;
                            com.navdy.hud.app.maps.here.HereLaneInfoListener.this.bus.post(com.navdy.hud.app.maps.here.HereLaneInfoListener.HIDE_LANE_INFO);
                        }
                        if (com.navdy.hud.app.maps.here.HereLaneInfoListener.sLogger.isLoggable(2)) {
                            com.navdy.hud.app.maps.here.HereLaneInfoListener.sLogger.v("Invalid lane data");
                            return;
                        }
                        return;
                    }
                    com.here.android.mpa.guidance.LaneInformation.Direction onRouteDirection = null;
                    boolean needOnRouteDirection = false;
                    java.util.EnumSet enumSet = null;
                    com.here.android.mpa.guidance.LaneInformation.Direction onRouteHighlyRecommendedDirection = null;
                    java.util.EnumSet<com.here.android.mpa.guidance.LaneInformation.Direction> needOnRouteHighlyRecommendedDirectionList = null;
                    com.here.android.mpa.guidance.LaneInformation.Direction onRouteRecommendedDirection = null;
                    java.util.EnumSet<com.here.android.mpa.guidance.LaneInformation.Direction> needOnRouteRecommendedDirectionList = null;
                    for (int i2 = 0; i2 < numLanes; i2++) {
                        com.here.android.mpa.guidance.LaneInformation laneInfo2 = (com.here.android.mpa.guidance.LaneInformation) this.val$laneInfoList.get(i2);
                        switch (com.navdy.hud.app.maps.here.HereLaneInfoListener.Anon4.$SwitchMap$com$here$android$mpa$guidance$LaneInformation$RecommendationState[laneInfo2.getRecommendationState().ordinal()]) {
                            case 2:
                                if (onRouteHighlyRecommendedDirection != null) {
                                    break;
                                } else {
                                    java.util.EnumSet<com.here.android.mpa.guidance.LaneInformation.Direction> directions2 = laneInfo2.getDirections();
                                    if (directions2 != null && directions2.size() > 0) {
                                        if (directions2.size() != 1) {
                                            needOnRouteDirection = true;
                                            needOnRouteHighlyRecommendedDirectionList = directions2;
                                            break;
                                        } else {
                                            onRouteHighlyRecommendedDirection = (com.here.android.mpa.guidance.LaneInformation.Direction) directions2.iterator().next();
                                            if (!com.navdy.hud.app.maps.here.HereLaneInfoListener.sLogger.isLoggable(2)) {
                                                break;
                                            } else {
                                                com.navdy.hud.app.maps.here.HereLaneInfoListener.sLogger.v("onRouteDirection:" + null);
                                                break;
                                            }
                                        }
                                    }
                                }
                            case 3:
                                if (onRouteRecommendedDirection != null) {
                                    break;
                                } else {
                                    java.util.EnumSet<com.here.android.mpa.guidance.LaneInformation.Direction> directions3 = laneInfo2.getDirections();
                                    if (directions3 != null && directions3.size() > 0) {
                                        if (directions3.size() != 1) {
                                            needOnRouteDirection = true;
                                            needOnRouteRecommendedDirectionList = directions3;
                                            break;
                                        } else {
                                            onRouteRecommendedDirection = (com.here.android.mpa.guidance.LaneInformation.Direction) directions3.iterator().next();
                                            if (!com.navdy.hud.app.maps.here.HereLaneInfoListener.sLogger.isLoggable(2)) {
                                                break;
                                            } else {
                                                com.navdy.hud.app.maps.here.HereLaneInfoListener.sLogger.v("onRouteDirection:" + null);
                                                break;
                                            }
                                        }
                                    }
                                }
                        }
                    }
                    if (hasHighlyRecommendedLanes > 0) {
                        if (onRouteHighlyRecommendedDirection != null) {
                            onRouteDirection = onRouteHighlyRecommendedDirection;
                        } else if (onRouteRecommendedDirection == null || !needOnRouteHighlyRecommendedDirectionList.contains(onRouteRecommendedDirection)) {
                            enumSet = needOnRouteHighlyRecommendedDirectionList;
                        } else {
                            onRouteDirection = onRouteRecommendedDirection;
                        }
                    } else if (onRouteRecommendedDirection != null) {
                        onRouteDirection = onRouteRecommendedDirection;
                    } else {
                        enumSet = needOnRouteRecommendedDirectionList;
                    }
                    if (onRouteDirection == null && needOnRouteDirection) {
                        if (com.navdy.hud.app.maps.here.HereLaneInfoListener.sLogger.isLoggable(2)) {
                            com.navdy.hud.app.maps.here.HereLaneInfoListener.sLogger.v("onRouteDirection is needed, but not available");
                        }
                        onRouteDirection = com.navdy.hud.app.maps.here.HereLaneInfoListener.this.getOnRouteDirectionFromManeuver(enumSet);
                        if (onRouteDirection == null) {
                            if (com.navdy.hud.app.maps.here.HereLaneInfoListener.sLogger.isLoggable(2)) {
                                com.navdy.hud.app.maps.here.HereLaneInfoListener.sLogger.v("onRouteDirection:could not get onroute direction from maneuver");
                            }
                            if (com.navdy.hud.app.maps.here.HereLaneInfoListener.this.lastLanesData != null) {
                                com.navdy.hud.app.maps.here.HereLaneInfoListener.this.lastLanesData = null;
                                com.navdy.hud.app.maps.here.HereLaneInfoListener.this.bus.post(com.navdy.hud.app.maps.here.HereLaneInfoListener.HIDE_LANE_INFO);
                                return;
                            }
                            return;
                        } else if (com.navdy.hud.app.maps.here.HereLaneInfoListener.sLogger.isLoggable(2)) {
                            com.navdy.hud.app.maps.here.HereLaneInfoListener.sLogger.v("onRouteDirection:got on route direction:" + onRouteDirection);
                        }
                    }
                    com.navdy.hud.app.maps.here.HereLaneInfoListener.this.tempLanes.clear();
                    for (int i3 = 0; i3 < numLanes; i3++) {
                        com.here.android.mpa.guidance.LaneInformation laneInfo3 = (com.here.android.mpa.guidance.LaneInformation) this.val$laneInfoList.get(i3);
                        java.util.EnumSet<com.here.android.mpa.guidance.LaneInformation.Direction> directions4 = laneInfo3.getDirections();
                        if (directions4 != null && directions4.size() != 0) {
                            com.navdy.hud.app.maps.here.HereLaneInfoListener.this.tempDirections.clear();
                            java.util.Iterator it2 = directions4.iterator();
                            while (it2.hasNext()) {
                                com.here.android.mpa.guidance.LaneInformation.Direction direction = (com.here.android.mpa.guidance.LaneInformation.Direction) it2.next();
                                switch (com.navdy.hud.app.maps.here.HereLaneInfoListener.Anon4.$SwitchMap$com$here$android$mpa$guidance$LaneInformation$Direction[direction.ordinal()]) {
                                    case 1:
                                        break;
                                    default:
                                        com.navdy.hud.app.maps.here.HereLaneInfoListener.this.tempDirections.add(direction);
                                        break;
                                }
                            }
                            if (com.navdy.hud.app.maps.here.HereLaneInfoListener.this.tempDirections.size() != 0) {
                                switch (com.navdy.hud.app.maps.here.HereLaneInfoListener.Anon4.$SwitchMap$com$here$android$mpa$guidance$LaneInformation$RecommendationState[laneInfo3.getRecommendationState().ordinal()]) {
                                    case 1:
                                        com.navdy.hud.app.maps.here.HereLaneInfoListener.this.tempLanes.add(new com.navdy.hud.app.maps.MapEvents.LaneData(com.navdy.hud.app.maps.MapEvents.LaneData.Position.OFF_ROUTE, com.navdy.hud.app.maps.here.HereLaneInfoBuilder.getDrawable(com.navdy.hud.app.maps.here.HereLaneInfoListener.this.tempDirections, com.navdy.hud.app.maps.MapEvents.LaneData.Position.OFF_ROUTE, null)));
                                        break;
                                    case 2:
                                        if (onRouteDirection != null && com.navdy.hud.app.maps.here.HereLaneInfoListener.this.tempDirections.size() > 1) {
                                            com.navdy.hud.app.maps.here.HereLaneInfoListener.this.tempDirections2.clear();
                                            java.util.Iterator<com.here.android.mpa.guidance.LaneInformation.Direction> iterator = com.navdy.hud.app.maps.here.HereLaneInfoListener.this.tempDirections.iterator();
                                            while (iterator.hasNext()) {
                                                com.here.android.mpa.guidance.LaneInformation.Direction d = (com.here.android.mpa.guidance.LaneInformation.Direction) iterator.next();
                                                if (d == onRouteDirection) {
                                                    com.navdy.hud.app.maps.here.HereLaneInfoListener.this.tempDirections2.add(d);
                                                    iterator.remove();
                                                }
                                            }
                                            com.navdy.hud.app.maps.here.HereLaneInfoListener.this.tempDirections.addAll(com.navdy.hud.app.maps.here.HereLaneInfoListener.this.tempDirections2);
                                        }
                                        com.navdy.hud.app.maps.here.HereLaneInfoListener.this.tempLanes.add(new com.navdy.hud.app.maps.MapEvents.LaneData(com.navdy.hud.app.maps.MapEvents.LaneData.Position.ON_ROUTE, com.navdy.hud.app.maps.here.HereLaneInfoBuilder.getDrawable(com.navdy.hud.app.maps.here.HereLaneInfoListener.this.tempDirections, com.navdy.hud.app.maps.MapEvents.LaneData.Position.ON_ROUTE, onRouteDirection)));
                                        break;
                                    case 3:
                                        if (onRouteDirection != null && com.navdy.hud.app.maps.here.HereLaneInfoListener.this.tempDirections.size() > 1) {
                                            com.navdy.hud.app.maps.here.HereLaneInfoListener.this.tempDirections2.clear();
                                            java.util.Iterator<com.here.android.mpa.guidance.LaneInformation.Direction> iterator2 = com.navdy.hud.app.maps.here.HereLaneInfoListener.this.tempDirections.iterator();
                                            while (iterator2.hasNext()) {
                                                com.here.android.mpa.guidance.LaneInformation.Direction d2 = (com.here.android.mpa.guidance.LaneInformation.Direction) iterator2.next();
                                                if (d2 == onRouteDirection) {
                                                    com.navdy.hud.app.maps.here.HereLaneInfoListener.this.tempDirections2.add(d2);
                                                    iterator2.remove();
                                                }
                                            }
                                            com.navdy.hud.app.maps.here.HereLaneInfoListener.this.tempDirections.addAll(com.navdy.hud.app.maps.here.HereLaneInfoListener.this.tempDirections2);
                                        }
                                        if (hasHighlyRecommendedLanes != 0) {
                                            com.navdy.hud.app.maps.here.HereLaneInfoListener.this.tempLanes.add(new com.navdy.hud.app.maps.MapEvents.LaneData(com.navdy.hud.app.maps.MapEvents.LaneData.Position.OFF_ROUTE, com.navdy.hud.app.maps.here.HereLaneInfoBuilder.getDrawable(com.navdy.hud.app.maps.here.HereLaneInfoListener.this.tempDirections, com.navdy.hud.app.maps.MapEvents.LaneData.Position.OFF_ROUTE, null)));
                                            break;
                                        } else {
                                            com.navdy.hud.app.maps.here.HereLaneInfoListener.this.tempLanes.add(new com.navdy.hud.app.maps.MapEvents.LaneData(com.navdy.hud.app.maps.MapEvents.LaneData.Position.ON_ROUTE, com.navdy.hud.app.maps.here.HereLaneInfoBuilder.getDrawable(com.navdy.hud.app.maps.here.HereLaneInfoListener.this.tempDirections, com.navdy.hud.app.maps.MapEvents.LaneData.Position.ON_ROUTE, onRouteDirection)));
                                            break;
                                        }
                                    case 4:
                                        com.navdy.hud.app.maps.here.HereLaneInfoListener.this.tempLanes.add(new com.navdy.hud.app.maps.MapEvents.LaneData(com.navdy.hud.app.maps.MapEvents.LaneData.Position.OFF_ROUTE, com.navdy.hud.app.maps.here.HereLaneInfoBuilder.getDrawable(com.navdy.hud.app.maps.here.HereLaneInfoListener.this.tempDirections, com.navdy.hud.app.maps.MapEvents.LaneData.Position.OFF_ROUTE, null)));
                                        break;
                                }
                            } else {
                                com.navdy.hud.app.maps.here.HereLaneInfoListener.this.tempLanes.add(com.navdy.hud.app.maps.here.HereLaneInfoListener.EMPTY_LANE_DATA);
                            }
                        } else {
                            com.navdy.hud.app.maps.here.HereLaneInfoListener.this.tempLanes.add(com.navdy.hud.app.maps.here.HereLaneInfoListener.EMPTY_LANE_DATA);
                        }
                    }
                    if (com.navdy.hud.app.maps.here.HereLaneInfoListener.sLogger.isLoggable(2)) {
                        com.navdy.hud.app.maps.here.HereLaneInfoListener.sLogger.v("LaneData: size=" + com.navdy.hud.app.maps.here.HereLaneInfoListener.this.tempLanes.size());
                        java.util.Iterator it3 = com.navdy.hud.app.maps.here.HereLaneInfoListener.this.tempLanes.iterator();
                        while (it3.hasNext()) {
                            com.navdy.hud.app.maps.MapEvents.LaneData laneData = (com.navdy.hud.app.maps.MapEvents.LaneData) it3.next();
                            com.navdy.hud.app.maps.here.HereLaneInfoListener.sLogger.v("LaneData:position: " + laneData.position);
                            if (laneData.icons != null) {
                                for (int i4 = 0; i4 < laneData.icons.length; i4++) {
                                    com.navdy.hud.app.maps.here.HereLaneInfoListener.sLogger.v("LaneData:icons" + (i4 + 1) + " = " + laneData.icons[i4]);
                                }
                            } else {
                                com.navdy.hud.app.maps.here.HereLaneInfoListener.sLogger.v("LaneData:icons: null");
                            }
                        }
                    }
                    if (com.navdy.hud.app.maps.here.HereLaneInfoListener.this.lastLanesData == null || !com.navdy.hud.app.maps.here.HereLaneInfoBuilder.compareLaneData(com.navdy.hud.app.maps.here.HereLaneInfoListener.this.lastLanesData.laneData, com.navdy.hud.app.maps.here.HereLaneInfoListener.this.tempLanes)) {
                        java.util.ArrayList<com.navdy.hud.app.maps.MapEvents.LaneData> lanes = new java.util.ArrayList<>(com.navdy.hud.app.maps.here.HereLaneInfoListener.this.tempLanes);
                        com.navdy.hud.app.maps.here.HereLaneInfoListener hereLaneInfoListener = com.navdy.hud.app.maps.here.HereLaneInfoListener.this;
                        com.navdy.hud.app.maps.MapEvents.DisplayTrafficLaneInfo displayTrafficLaneInfo = new com.navdy.hud.app.maps.MapEvents.DisplayTrafficLaneInfo(lanes);
                        hereLaneInfoListener.lastLanesData = displayTrafficLaneInfo;
                        com.navdy.hud.app.maps.here.HereLaneInfoListener.this.tempLanes.clear();
                        com.navdy.hud.app.maps.here.HereLaneInfoListener.sLogger.v("onShowLaneInfo::" + lanes.size());
                        if (com.navdy.hud.app.maps.here.HereLaneInfoListener.this.running) {
                            com.navdy.hud.app.maps.here.HereLaneInfoListener.this.bus.post(com.navdy.hud.app.maps.here.HereLaneInfoListener.this.lastLanesData);
                            return;
                        }
                        return;
                    }
                    if (com.navdy.hud.app.maps.here.HereLaneInfoListener.sLogger.isLoggable(2)) {
                        com.navdy.hud.app.maps.here.HereLaneInfoListener.sLogger.v("LaneData: lanedata is same as last");
                    }
                    com.navdy.hud.app.maps.here.HereLaneInfoListener.this.tempLanes.clear();
                } catch (Throwable t) {
                    com.navdy.hud.app.maps.here.HereLaneInfoListener.sLogger.e(t);
                }
            } else if (com.navdy.hud.app.maps.here.HereLaneInfoListener.this.lastLanesData != null) {
                com.navdy.hud.app.maps.here.HereLaneInfoListener.this.lastLanesData = null;
                com.navdy.hud.app.maps.here.HereLaneInfoListener.this.hideLaneInfo();
            }
        }
    }

    static /* synthetic */ class Anon4 {
        static final /* synthetic */ int[] $SwitchMap$com$here$android$mpa$guidance$LaneInformation$Direction = new int[com.here.android.mpa.guidance.LaneInformation.Direction.values().length];
        static final /* synthetic */ int[] $SwitchMap$com$here$android$mpa$guidance$LaneInformation$RecommendationState = new int[com.here.android.mpa.guidance.LaneInformation.RecommendationState.values().length];
        static final /* synthetic */ int[] $SwitchMap$com$here$android$mpa$routing$Maneuver$Turn = new int[com.here.android.mpa.routing.Maneuver.Turn.values().length];

        static {
            try {
                $SwitchMap$com$here$android$mpa$routing$Maneuver$Turn[com.here.android.mpa.routing.Maneuver.Turn.KEEP_RIGHT.ordinal()] = 1;
            } catch (java.lang.NoSuchFieldError e) {
            }
            try {
                $SwitchMap$com$here$android$mpa$routing$Maneuver$Turn[com.here.android.mpa.routing.Maneuver.Turn.LIGHT_RIGHT.ordinal()] = 2;
            } catch (java.lang.NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$com$here$android$mpa$routing$Maneuver$Turn[com.here.android.mpa.routing.Maneuver.Turn.QUITE_RIGHT.ordinal()] = 3;
            } catch (java.lang.NoSuchFieldError e3) {
            }
            try {
                $SwitchMap$com$here$android$mpa$routing$Maneuver$Turn[com.here.android.mpa.routing.Maneuver.Turn.HEAVY_RIGHT.ordinal()] = 4;
            } catch (java.lang.NoSuchFieldError e4) {
            }
            try {
                $SwitchMap$com$here$android$mpa$routing$Maneuver$Turn[com.here.android.mpa.routing.Maneuver.Turn.KEEP_LEFT.ordinal()] = 5;
            } catch (java.lang.NoSuchFieldError e5) {
            }
            try {
                $SwitchMap$com$here$android$mpa$routing$Maneuver$Turn[com.here.android.mpa.routing.Maneuver.Turn.LIGHT_LEFT.ordinal()] = 6;
            } catch (java.lang.NoSuchFieldError e6) {
            }
            try {
                $SwitchMap$com$here$android$mpa$routing$Maneuver$Turn[com.here.android.mpa.routing.Maneuver.Turn.QUITE_LEFT.ordinal()] = 7;
            } catch (java.lang.NoSuchFieldError e7) {
            }
            try {
                $SwitchMap$com$here$android$mpa$routing$Maneuver$Turn[com.here.android.mpa.routing.Maneuver.Turn.HEAVY_LEFT.ordinal()] = 8;
            } catch (java.lang.NoSuchFieldError e8) {
            }
            try {
                $SwitchMap$com$here$android$mpa$guidance$LaneInformation$Direction[com.here.android.mpa.guidance.LaneInformation.Direction.UNDEFINED.ordinal()] = 1;
            } catch (java.lang.NoSuchFieldError e9) {
            }
            try {
                $SwitchMap$com$here$android$mpa$guidance$LaneInformation$RecommendationState[com.here.android.mpa.guidance.LaneInformation.RecommendationState.NOT_RECOMMENDED.ordinal()] = 1;
            } catch (java.lang.NoSuchFieldError e10) {
            }
            try {
                $SwitchMap$com$here$android$mpa$guidance$LaneInformation$RecommendationState[com.here.android.mpa.guidance.LaneInformation.RecommendationState.HIGHLY_RECOMMENDED.ordinal()] = 2;
            } catch (java.lang.NoSuchFieldError e11) {
            }
            try {
                $SwitchMap$com$here$android$mpa$guidance$LaneInformation$RecommendationState[com.here.android.mpa.guidance.LaneInformation.RecommendationState.RECOMMENDED.ordinal()] = 3;
            } catch (java.lang.NoSuchFieldError e12) {
            }
            try {
                $SwitchMap$com$here$android$mpa$guidance$LaneInformation$RecommendationState[com.here.android.mpa.guidance.LaneInformation.RecommendationState.NOT_AVAILABLE.ordinal()] = 4;
            } catch (java.lang.NoSuchFieldError e13) {
            }
        }
    }

    HereLaneInfoListener(com.squareup.otto.Bus bus2, com.navdy.hud.app.maps.here.HereNavController navController2) {
        sLogger.v("ctor");
        this.bus = bus2;
        this.navController = navController2;
    }

    public void onLaneInformation(java.util.List<com.here.android.mpa.guidance.LaneInformation> laneInfoList, com.here.android.mpa.common.RoadElement roadElement) {
        if (this.running) {
            this.taskManager.execute(new com.navdy.hud.app.maps.here.HereLaneInfoListener.Anon3(laneInfoList), 15);
        }
    }

    public void hideLaneInfo() {
        sLogger.v("hideLaneInfo::");
        this.bus.post(HIDE_LANE_INFO);
    }

    /* access modifiers changed from: 0000 */
    public synchronized void start() {
        if (this.running) {
            sLogger.v("already running");
        } else {
            sLogger.v("start");
            this.taskManager.execute(this.startRunnable, 15);
            this.running = true;
        }
    }

    /* access modifiers changed from: 0000 */
    public synchronized void stop() {
        if (!this.running) {
            sLogger.v("not running");
        } else {
            sLogger.v("stop");
            this.taskManager.execute(this.stopRunnable, 15);
            this.running = false;
            this.bus.post(HIDE_LANE_INFO);
            this.lastLanesData = null;
        }
    }

    /* access modifiers changed from: private */
    public com.here.android.mpa.guidance.LaneInformation.Direction getOnRouteDirectionFromManeuver(java.util.EnumSet<com.here.android.mpa.guidance.LaneInformation.Direction> directions) {
        try {
            com.here.android.mpa.routing.Maneuver maneuver = com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().getNextManeuver();
            if (maneuver == null) {
                return null;
            }
            com.here.android.mpa.routing.Maneuver.Turn turn = maneuver.getTurn();
            if (turn == null) {
                return null;
            }
            switch (com.navdy.hud.app.maps.here.HereLaneInfoListener.Anon4.$SwitchMap$com$here$android$mpa$routing$Maneuver$Turn[turn.ordinal()]) {
                case 1:
                    if (directions.contains(com.here.android.mpa.guidance.LaneInformation.Direction.RIGHT)) {
                        return com.here.android.mpa.guidance.LaneInformation.Direction.RIGHT;
                    }
                    if (com.navdy.hud.app.maps.here.HereLaneInfoBuilder.getNumDirections(directions, com.navdy.hud.app.maps.here.HereLaneInfoBuilder.DirectionType.RIGHT) == 1) {
                        return com.navdy.hud.app.maps.here.HereLaneInfoBuilder.getDirection(directions, com.navdy.hud.app.maps.here.HereLaneInfoBuilder.DirectionType.RIGHT);
                    }
                    return null;
                case 2:
                    if (directions.contains(com.here.android.mpa.guidance.LaneInformation.Direction.SLIGHTLY_RIGHT)) {
                        return com.here.android.mpa.guidance.LaneInformation.Direction.SLIGHTLY_RIGHT;
                    }
                    if (com.navdy.hud.app.maps.here.HereLaneInfoBuilder.getNumDirections(directions, com.navdy.hud.app.maps.here.HereLaneInfoBuilder.DirectionType.RIGHT) == 1) {
                        return com.navdy.hud.app.maps.here.HereLaneInfoBuilder.getDirection(directions, com.navdy.hud.app.maps.here.HereLaneInfoBuilder.DirectionType.RIGHT);
                    }
                    return null;
                case 3:
                case 4:
                    if (directions.contains(com.here.android.mpa.guidance.LaneInformation.Direction.SHARP_RIGHT)) {
                        return com.here.android.mpa.guidance.LaneInformation.Direction.SHARP_RIGHT;
                    }
                    if (com.navdy.hud.app.maps.here.HereLaneInfoBuilder.getNumDirections(directions, com.navdy.hud.app.maps.here.HereLaneInfoBuilder.DirectionType.RIGHT) == 1) {
                        return com.navdy.hud.app.maps.here.HereLaneInfoBuilder.getDirection(directions, com.navdy.hud.app.maps.here.HereLaneInfoBuilder.DirectionType.RIGHT);
                    }
                    return null;
                case 5:
                    if (directions.contains(com.here.android.mpa.guidance.LaneInformation.Direction.LEFT)) {
                        return com.here.android.mpa.guidance.LaneInformation.Direction.LEFT;
                    }
                    if (com.navdy.hud.app.maps.here.HereLaneInfoBuilder.getNumDirections(directions, com.navdy.hud.app.maps.here.HereLaneInfoBuilder.DirectionType.LEFT) == 1) {
                        return com.navdy.hud.app.maps.here.HereLaneInfoBuilder.getDirection(directions, com.navdy.hud.app.maps.here.HereLaneInfoBuilder.DirectionType.LEFT);
                    }
                    return null;
                case 6:
                    if (directions.contains(com.here.android.mpa.guidance.LaneInformation.Direction.SLIGHTLY_LEFT)) {
                        return com.here.android.mpa.guidance.LaneInformation.Direction.SLIGHTLY_LEFT;
                    }
                    if (com.navdy.hud.app.maps.here.HereLaneInfoBuilder.getNumDirections(directions, com.navdy.hud.app.maps.here.HereLaneInfoBuilder.DirectionType.LEFT) == 1) {
                        return com.navdy.hud.app.maps.here.HereLaneInfoBuilder.getDirection(directions, com.navdy.hud.app.maps.here.HereLaneInfoBuilder.DirectionType.LEFT);
                    }
                    return null;
                case 7:
                case 8:
                    if (directions.contains(com.here.android.mpa.guidance.LaneInformation.Direction.SHARP_LEFT)) {
                        return com.here.android.mpa.guidance.LaneInformation.Direction.SHARP_LEFT;
                    }
                    if (com.navdy.hud.app.maps.here.HereLaneInfoBuilder.getNumDirections(directions, com.navdy.hud.app.maps.here.HereLaneInfoBuilder.DirectionType.LEFT) == 1) {
                        return com.navdy.hud.app.maps.here.HereLaneInfoBuilder.getDirection(directions, com.navdy.hud.app.maps.here.HereLaneInfoBuilder.DirectionType.LEFT);
                    }
                    return null;
                default:
                    return null;
            }
        } catch (Throwable t) {
            sLogger.e(t);
            return null;
        }
    }
}
