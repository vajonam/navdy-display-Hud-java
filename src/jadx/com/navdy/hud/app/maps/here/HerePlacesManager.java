package com.navdy.hud.app.maps.here;

public class HerePlacesManager {
    /* access modifiers changed from: private */
    public static final java.util.ArrayList<java.lang.String> EMPTY_AUTO_COMPLETE_RESULT = new java.util.ArrayList<>(0);
    /* access modifiers changed from: private */
    public static final java.util.ArrayList<com.here.android.mpa.search.PlaceLink> EMPTY_RESULT = new java.util.ArrayList<>(0);
    public static final java.lang.String HERE_PLACE_TYPE_ATM = "atm-bank-exchange";
    public static final java.lang.String HERE_PLACE_TYPE_COFFEE = "coffee-tea";
    public static final java.lang.String HERE_PLACE_TYPE_GAS = "petrol-station";
    public static final java.lang.String HERE_PLACE_TYPE_HOSPITAL = "hospital-health-care-facility";
    public static final java.lang.String HERE_PLACE_TYPE_PARKING = "parking-facility";
    public static final java.lang.String[] HERE_PLACE_TYPE_RESTAURANT = {"restaurant", "snacks-fast-food"};
    public static final java.lang.String HERE_PLACE_TYPE_STORE = "shopping";
    private static final int MAX_RESULTS_AUTOCOMPLETE = 10;
    private static final int MAX_RESULTS_SEARCH = 15;
    /* access modifiers changed from: private */
    public static final android.content.Context context = com.navdy.hud.app.HudApplication.getAppContext();
    /* access modifiers changed from: private */
    public static final com.navdy.hud.app.maps.here.HereMapsManager hereMapsManager = com.navdy.hud.app.maps.here.HereMapsManager.getInstance();
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.maps.here.HerePlacesManager.class);
    private static volatile boolean willRestablishOnline;

    static class Anon1 implements com.here.android.mpa.search.ResultListener<com.here.android.mpa.search.DiscoveryResultPage> {
        final /* synthetic */ com.navdy.hud.app.maps.here.HerePlacesManager.PlacesSearchCallback val$callBack;

        Anon1(com.navdy.hud.app.maps.here.HerePlacesManager.PlacesSearchCallback placesSearchCallback) {
            this.val$callBack = placesSearchCallback;
        }

        public void onCompleted(com.here.android.mpa.search.DiscoveryResultPage discoveryResultPage, com.here.android.mpa.search.ErrorCode errorCode) {
            try {
                this.val$callBack.result(errorCode, discoveryResultPage);
            } catch (Throwable t) {
                com.navdy.hud.app.maps.here.HerePlacesManager.sLogger.e(t);
            }
        }
    }

    static class Anon10 implements java.lang.Runnable {
        final /* synthetic */ java.lang.String val$autoCompleteSearch;
        final /* synthetic */ int val$maxResults;
        final /* synthetic */ java.util.List val$results;

        Anon10(java.util.List list, int i, java.lang.String str) {
            this.val$results = list;
            this.val$maxResults = i;
            this.val$autoCompleteSearch = str;
        }

        public void run() {
            try {
                java.util.List<java.lang.String> ret = this.val$results;
                if (this.val$results.size() > this.val$maxResults) {
                    ret = this.val$results.subList(0, this.val$maxResults);
                }
                com.navdy.hud.app.maps.MapsEventHandler.getInstance().sendEventToClient(new com.navdy.service.library.events.places.AutoCompleteResponse(this.val$autoCompleteSearch, com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS, null, ret));
                com.navdy.hud.app.maps.here.HerePlacesManager.sLogger.v("[autocomplete] returned:" + ret.size());
            } catch (Throwable t) {
                com.navdy.hud.app.maps.here.HerePlacesManager.sLogger.e(t);
                com.navdy.hud.app.maps.here.HerePlacesManager.returnSearchErrorResponse(this.val$autoCompleteSearch, com.navdy.service.library.events.RequestStatus.REQUEST_UNKNOWN_ERROR, com.navdy.hud.app.maps.here.HerePlacesManager.context.getString(com.navdy.hud.app.R.string.unknown_error));
            }
        }
    }

    static class Anon2 implements com.here.android.mpa.search.ResultListener<java.util.List<com.here.android.mpa.search.Location>> {
        final /* synthetic */ com.navdy.hud.app.maps.here.HerePlacesManager.GeoCodeCallback val$callBack;

        Anon2(com.navdy.hud.app.maps.here.HerePlacesManager.GeoCodeCallback geoCodeCallback) {
            this.val$callBack = geoCodeCallback;
        }

        public void onCompleted(java.util.List<com.here.android.mpa.search.Location> locations, com.here.android.mpa.search.ErrorCode errorCode) {
            try {
                this.val$callBack.result(errorCode, locations);
            } catch (Throwable t) {
                com.navdy.hud.app.maps.here.HerePlacesManager.sLogger.e(t);
            }
        }
    }

    static class Anon3 implements com.here.android.mpa.search.ResultListener<java.util.List<java.lang.String>> {
        final /* synthetic */ com.navdy.hud.app.maps.here.HerePlacesManager.AutoCompleteCallback val$callBack;

        Anon3(com.navdy.hud.app.maps.here.HerePlacesManager.AutoCompleteCallback autoCompleteCallback) {
            this.val$callBack = autoCompleteCallback;
        }

        public void onCompleted(java.util.List<java.lang.String> results, com.here.android.mpa.search.ErrorCode errorCode) {
            try {
                this.val$callBack.result(errorCode, results);
            } catch (Throwable t) {
                com.navdy.hud.app.maps.here.HerePlacesManager.sLogger.e(t);
            }
        }
    }

    static class Anon4 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.service.library.events.places.PlacesSearchRequest val$request;

        class Anon1 implements com.navdy.hud.app.maps.here.HerePlacesManager.PlacesSearchCallback {
            final /* synthetic */ com.here.android.mpa.common.GeoCoordinate val$geoPosition;
            final /* synthetic */ long val$l1;
            final /* synthetic */ int val$maxResults;
            final /* synthetic */ int val$searchArea;
            final /* synthetic */ java.lang.String val$searchQuery;

            /* renamed from: com.navdy.hud.app.maps.here.HerePlacesManager$Anon4$Anon1$Anon1 reason: collision with other inner class name */
            class C0020Anon1 implements java.lang.Runnable {
                final /* synthetic */ com.here.android.mpa.search.ErrorCode val$errorCode;
                final /* synthetic */ com.here.android.mpa.search.DiscoveryResultPage val$result;

                C0020Anon1(com.here.android.mpa.search.ErrorCode errorCode, com.here.android.mpa.search.DiscoveryResultPage discoveryResultPage) {
                    this.val$errorCode = errorCode;
                    this.val$result = discoveryResultPage;
                }

                public void run() {
                    try {
                        if (this.val$errorCode != com.here.android.mpa.search.ErrorCode.NONE) {
                            com.navdy.hud.app.maps.here.HerePlacesManager.returnSearchErrorResponse(com.navdy.hud.app.maps.here.HerePlacesManager.Anon4.Anon1.this.val$searchQuery, com.navdy.service.library.events.RequestStatus.REQUEST_SERVICE_ERROR, com.navdy.hud.app.maps.here.HerePlacesManager.context.getString(com.navdy.hud.app.R.string.search_error, new java.lang.Object[]{this.val$errorCode.toString()}));
                            if (com.navdy.hud.app.maps.here.HerePlacesManager.sLogger.isLoggable(2)) {
                                com.navdy.hud.app.maps.here.HerePlacesManager.sLogger.v("[search] handleSearchRequest- time-2 =" + (android.os.SystemClock.elapsedRealtime() - com.navdy.hud.app.maps.here.HerePlacesManager.Anon4.Anon1.this.val$l1));
                                return;
                            }
                            return;
                        }
                        if (this.val$result != null) {
                            java.util.List<com.here.android.mpa.search.PlaceLink> links = this.val$result.getPlaceLinks();
                            if (links == null || links.size() == 0) {
                                com.navdy.hud.app.maps.here.HerePlacesManager.sLogger.d("[search] no results");
                                com.navdy.hud.app.maps.here.HerePlacesManager.returnSearchResults(com.navdy.hud.app.maps.here.HerePlacesManager.Anon4.Anon1.this.val$geoPosition, com.navdy.hud.app.maps.here.HerePlacesManager.Anon4.Anon1.this.val$searchQuery, com.navdy.hud.app.maps.here.HerePlacesManager.EMPTY_RESULT, com.navdy.hud.app.maps.here.HerePlacesManager.Anon4.Anon1.this.val$maxResults, com.navdy.hud.app.maps.here.HerePlacesManager.Anon4.Anon1.this.val$searchArea);
                            } else {
                                com.navdy.hud.app.maps.here.HerePlacesManager.sLogger.d("[search] returned results:" + links.size());
                                com.navdy.hud.app.maps.here.HerePlacesManager.returnSearchResults(com.navdy.hud.app.maps.here.HerePlacesManager.Anon4.Anon1.this.val$geoPosition, com.navdy.hud.app.maps.here.HerePlacesManager.Anon4.Anon1.this.val$searchQuery, links, com.navdy.hud.app.maps.here.HerePlacesManager.Anon4.Anon1.this.val$maxResults, com.navdy.hud.app.maps.here.HerePlacesManager.Anon4.Anon1.this.val$searchArea);
                            }
                        } else {
                            com.navdy.hud.app.maps.here.HerePlacesManager.sLogger.d("[search] no results");
                            com.navdy.hud.app.maps.here.HerePlacesManager.returnSearchResults(com.navdy.hud.app.maps.here.HerePlacesManager.Anon4.Anon1.this.val$geoPosition, com.navdy.hud.app.maps.here.HerePlacesManager.Anon4.Anon1.this.val$searchQuery, com.navdy.hud.app.maps.here.HerePlacesManager.EMPTY_RESULT, com.navdy.hud.app.maps.here.HerePlacesManager.Anon4.Anon1.this.val$maxResults, com.navdy.hud.app.maps.here.HerePlacesManager.Anon4.Anon1.this.val$searchArea);
                        }
                        if (com.navdy.hud.app.maps.here.HerePlacesManager.sLogger.isLoggable(2)) {
                            com.navdy.hud.app.maps.here.HerePlacesManager.sLogger.v("[search] handleSearchRequest- time-2 =" + (android.os.SystemClock.elapsedRealtime() - com.navdy.hud.app.maps.here.HerePlacesManager.Anon4.Anon1.this.val$l1));
                        }
                    } catch (Throwable th) {
                        if (com.navdy.hud.app.maps.here.HerePlacesManager.sLogger.isLoggable(2)) {
                            com.navdy.hud.app.maps.here.HerePlacesManager.sLogger.v("[search] handleSearchRequest- time-2 =" + (android.os.SystemClock.elapsedRealtime() - com.navdy.hud.app.maps.here.HerePlacesManager.Anon4.Anon1.this.val$l1));
                        }
                        throw th;
                    }
                }
            }

            Anon1(java.lang.String str, com.here.android.mpa.common.GeoCoordinate geoCoordinate, int i, int i2, long j) {
                this.val$searchQuery = str;
                this.val$geoPosition = geoCoordinate;
                this.val$maxResults = i;
                this.val$searchArea = i2;
                this.val$l1 = j;
            }

            public void result(com.here.android.mpa.search.ErrorCode errorCode, com.here.android.mpa.search.DiscoveryResultPage result) {
                com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.maps.here.HerePlacesManager.Anon4.Anon1.C0020Anon1(errorCode, result), 19);
            }
        }

        Anon4(com.navdy.service.library.events.places.PlacesSearchRequest placesSearchRequest) {
            this.val$request = placesSearchRequest;
        }

        public void run() {
            long l1 = android.os.SystemClock.elapsedRealtime();
            java.lang.String searchQuery = this.val$request.searchQuery;
            int searchArea = this.val$request.searchArea != null ? this.val$request.searchArea.intValue() : -1;
            int n = this.val$request.maxResults != null ? this.val$request.maxResults.intValue() : 15;
            if (n <= 0 || n > 15) {
                n = 15;
            }
            int maxResults = n;
            try {
                com.navdy.hud.app.maps.here.HerePlacesManager.printSearchRequest(this.val$request);
                if (!com.navdy.hud.app.maps.here.HerePlacesManager.hereMapsManager.isInitialized()) {
                    com.navdy.hud.app.maps.here.HerePlacesManager.sLogger.i("[search] engine not ready");
                    com.navdy.hud.app.maps.here.HerePlacesManager.returnSearchErrorResponse(searchQuery, com.navdy.service.library.events.RequestStatus.REQUEST_NOT_READY, com.navdy.hud.app.maps.here.HerePlacesManager.context.getString(com.navdy.hud.app.R.string.map_engine_not_ready));
                    if (com.navdy.hud.app.maps.here.HerePlacesManager.sLogger.isLoggable(2)) {
                        com.navdy.hud.app.maps.here.HerePlacesManager.sLogger.v("[search] handleSearchRequest- time-1 =" + (android.os.SystemClock.elapsedRealtime() - l1));
                    }
                } else if (android.text.TextUtils.isEmpty(searchQuery)) {
                    com.navdy.hud.app.maps.here.HerePlacesManager.sLogger.i("[search] invalid search query");
                    com.navdy.hud.app.maps.here.HerePlacesManager.returnSearchErrorResponse(searchQuery, com.navdy.service.library.events.RequestStatus.REQUEST_INVALID_REQUEST, com.navdy.hud.app.maps.here.HerePlacesManager.context.getString(com.navdy.hud.app.R.string.empty_search_query));
                    if (com.navdy.hud.app.maps.here.HerePlacesManager.sLogger.isLoggable(2)) {
                        com.navdy.hud.app.maps.here.HerePlacesManager.sLogger.v("[search] handleSearchRequest- time-1 =" + (android.os.SystemClock.elapsedRealtime() - l1));
                    }
                } else {
                    com.navdy.hud.app.maps.here.HereLocationFixManager hereLocationFixManager = com.navdy.hud.app.maps.here.HerePlacesManager.hereMapsManager.getLocationFixManager();
                    if (hereLocationFixManager == null) {
                        com.navdy.hud.app.maps.here.HerePlacesManager.sLogger.i("[search] engine not ready, location fix manager n/a");
                        com.navdy.hud.app.maps.here.HerePlacesManager.returnSearchErrorResponse(searchQuery, com.navdy.service.library.events.RequestStatus.REQUEST_NOT_READY, com.navdy.hud.app.maps.here.HerePlacesManager.context.getString(com.navdy.hud.app.R.string.map_engine_not_ready));
                        if (com.navdy.hud.app.maps.here.HerePlacesManager.sLogger.isLoggable(2)) {
                            com.navdy.hud.app.maps.here.HerePlacesManager.sLogger.v("[search] handleSearchRequest- time-1 =" + (android.os.SystemClock.elapsedRealtime() - l1));
                            return;
                        }
                        return;
                    }
                    com.here.android.mpa.common.GeoCoordinate geoPosition = hereLocationFixManager.getLastGeoCoordinate();
                    if (geoPosition == null) {
                        com.navdy.hud.app.maps.here.HerePlacesManager.sLogger.i("start location n/a");
                        com.navdy.hud.app.maps.here.HerePlacesManager.returnSearchErrorResponse(searchQuery, com.navdy.service.library.events.RequestStatus.REQUEST_NO_LOCATION_SERVICE, com.navdy.hud.app.maps.here.HerePlacesManager.context.getString(com.navdy.hud.app.R.string.no_current_position));
                        if (com.navdy.hud.app.maps.here.HerePlacesManager.sLogger.isLoggable(2)) {
                            com.navdy.hud.app.maps.here.HerePlacesManager.sLogger.v("[search] handleSearchRequest- time-1 =" + (android.os.SystemClock.elapsedRealtime() - l1));
                            return;
                        }
                        return;
                    }
                    com.navdy.hud.app.maps.here.HerePlacesManager.searchPlaces(geoPosition, searchQuery, searchArea, maxResults, new com.navdy.hud.app.maps.here.HerePlacesManager.Anon4.Anon1(searchQuery, geoPosition, maxResults, searchArea, l1));
                    if (com.navdy.hud.app.maps.here.HerePlacesManager.sLogger.isLoggable(2)) {
                        com.navdy.hud.app.maps.here.HerePlacesManager.sLogger.v("[search] handleSearchRequest- time-1 =" + (android.os.SystemClock.elapsedRealtime() - l1));
                    }
                }
            } catch (Throwable th) {
                if (com.navdy.hud.app.maps.here.HerePlacesManager.sLogger.isLoggable(2)) {
                    com.navdy.hud.app.maps.here.HerePlacesManager.sLogger.v("[search] handleSearchRequest- time-1 =" + (android.os.SystemClock.elapsedRealtime() - l1));
                }
                throw th;
            }
        }
    }

    static class Anon5 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.service.library.events.places.AutoCompleteRequest val$request;

        class Anon1 implements com.navdy.hud.app.maps.here.HerePlacesManager.AutoCompleteCallback {
            final /* synthetic */ java.lang.String val$autoCompleteSearch;
            final /* synthetic */ long val$l1;
            final /* synthetic */ int val$maxResults;

            /* renamed from: com.navdy.hud.app.maps.here.HerePlacesManager$Anon5$Anon1$Anon1 reason: collision with other inner class name */
            class C0021Anon1 implements java.lang.Runnable {
                final /* synthetic */ com.here.android.mpa.search.ErrorCode val$errorCode;
                final /* synthetic */ java.util.List val$result;

                C0021Anon1(com.here.android.mpa.search.ErrorCode errorCode, java.util.List list) {
                    this.val$errorCode = errorCode;
                    this.val$result = list;
                }

                public void run() {
                    try {
                        if (this.val$errorCode != com.here.android.mpa.search.ErrorCode.NONE) {
                            com.navdy.hud.app.maps.here.HerePlacesManager.returnAutoCompleteErrorResponse(com.navdy.hud.app.maps.here.HerePlacesManager.Anon5.Anon1.this.val$autoCompleteSearch, com.navdy.service.library.events.RequestStatus.REQUEST_SERVICE_ERROR, com.navdy.hud.app.maps.here.HerePlacesManager.context.getString(com.navdy.hud.app.R.string.search_error, new java.lang.Object[]{this.val$errorCode.toString()}));
                            if (com.navdy.hud.app.maps.here.HerePlacesManager.sLogger.isLoggable(2)) {
                                com.navdy.hud.app.maps.here.HerePlacesManager.sLogger.v("handleAutoCompleteRequest- time-2 =" + (android.os.SystemClock.elapsedRealtime() - com.navdy.hud.app.maps.here.HerePlacesManager.Anon5.Anon1.this.val$l1));
                                return;
                            }
                            return;
                        }
                        if (this.val$result == null || this.val$result.size() == 0) {
                            com.navdy.hud.app.maps.here.HerePlacesManager.sLogger.d("no results");
                            com.navdy.hud.app.maps.here.HerePlacesManager.returnAutoCompleteResults(com.navdy.hud.app.maps.here.HerePlacesManager.Anon5.Anon1.this.val$autoCompleteSearch, com.navdy.hud.app.maps.here.HerePlacesManager.Anon5.Anon1.this.val$maxResults, com.navdy.hud.app.maps.here.HerePlacesManager.EMPTY_AUTO_COMPLETE_RESULT);
                        } else {
                            com.navdy.hud.app.maps.here.HerePlacesManager.sLogger.d("returned results:" + this.val$result.size());
                            com.navdy.hud.app.maps.here.HerePlacesManager.returnAutoCompleteResults(com.navdy.hud.app.maps.here.HerePlacesManager.Anon5.Anon1.this.val$autoCompleteSearch, com.navdy.hud.app.maps.here.HerePlacesManager.Anon5.Anon1.this.val$maxResults, this.val$result);
                        }
                        if (com.navdy.hud.app.maps.here.HerePlacesManager.sLogger.isLoggable(2)) {
                            com.navdy.hud.app.maps.here.HerePlacesManager.sLogger.v("handleAutoCompleteRequest- time-2 =" + (android.os.SystemClock.elapsedRealtime() - com.navdy.hud.app.maps.here.HerePlacesManager.Anon5.Anon1.this.val$l1));
                        }
                    } catch (Throwable th) {
                        if (com.navdy.hud.app.maps.here.HerePlacesManager.sLogger.isLoggable(2)) {
                            com.navdy.hud.app.maps.here.HerePlacesManager.sLogger.v("handleAutoCompleteRequest- time-2 =" + (android.os.SystemClock.elapsedRealtime() - com.navdy.hud.app.maps.here.HerePlacesManager.Anon5.Anon1.this.val$l1));
                        }
                        throw th;
                    }
                }
            }

            Anon1(java.lang.String str, int i, long j) {
                this.val$autoCompleteSearch = str;
                this.val$maxResults = i;
                this.val$l1 = j;
            }

            public void result(com.here.android.mpa.search.ErrorCode errorCode, java.util.List<java.lang.String> result) {
                com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.maps.here.HerePlacesManager.Anon5.Anon1.C0021Anon1(errorCode, result), 19);
            }
        }

        Anon5(com.navdy.service.library.events.places.AutoCompleteRequest autoCompleteRequest) {
            this.val$request = autoCompleteRequest;
        }

        public void run() {
            long l1 = android.os.SystemClock.elapsedRealtime();
            java.lang.String autoCompleteSearch = this.val$request.partialSearch;
            int searchArea = this.val$request.searchArea != null ? this.val$request.searchArea.intValue() : -1;
            int n = this.val$request.maxResults != null ? this.val$request.maxResults.intValue() : 10;
            if (n <= 0 || n > 10) {
                n = 10;
            }
            int maxResults = n;
            try {
                com.navdy.hud.app.maps.here.HerePlacesManager.printAutoCompleteRequest(this.val$request);
                if (!com.navdy.hud.app.maps.here.HerePlacesManager.hereMapsManager.isInitialized()) {
                    com.navdy.hud.app.maps.here.HerePlacesManager.sLogger.i("engine not ready");
                    com.navdy.hud.app.maps.here.HerePlacesManager.returnAutoCompleteErrorResponse(autoCompleteSearch, com.navdy.service.library.events.RequestStatus.REQUEST_NOT_READY, com.navdy.hud.app.maps.here.HerePlacesManager.context.getString(com.navdy.hud.app.R.string.map_engine_not_ready));
                    if (com.navdy.hud.app.maps.here.HerePlacesManager.sLogger.isLoggable(2)) {
                        com.navdy.hud.app.maps.here.HerePlacesManager.sLogger.v("handleAutoCompleteRequest- time-1 =" + (android.os.SystemClock.elapsedRealtime() - l1));
                    }
                } else if (android.text.TextUtils.isEmpty(autoCompleteSearch)) {
                    com.navdy.hud.app.maps.here.HerePlacesManager.sLogger.i("invalid autocomplete query");
                    com.navdy.hud.app.maps.here.HerePlacesManager.returnAutoCompleteErrorResponse(autoCompleteSearch, com.navdy.service.library.events.RequestStatus.REQUEST_INVALID_REQUEST, com.navdy.hud.app.maps.here.HerePlacesManager.context.getString(com.navdy.hud.app.R.string.empty_search_query));
                    if (com.navdy.hud.app.maps.here.HerePlacesManager.sLogger.isLoggable(2)) {
                        com.navdy.hud.app.maps.here.HerePlacesManager.sLogger.v("handleAutoCompleteRequest- time-1 =" + (android.os.SystemClock.elapsedRealtime() - l1));
                    }
                } else {
                    com.navdy.hud.app.maps.here.HereLocationFixManager hereLocationFixManager = com.navdy.hud.app.maps.here.HerePlacesManager.hereMapsManager.getLocationFixManager();
                    if (hereLocationFixManager == null) {
                        com.navdy.hud.app.maps.here.HerePlacesManager.sLogger.i("engine not ready, location fix manager n/a");
                        com.navdy.hud.app.maps.here.HerePlacesManager.returnAutoCompleteErrorResponse(autoCompleteSearch, com.navdy.service.library.events.RequestStatus.REQUEST_NOT_READY, com.navdy.hud.app.maps.here.HerePlacesManager.context.getString(com.navdy.hud.app.R.string.map_engine_not_ready));
                        if (com.navdy.hud.app.maps.here.HerePlacesManager.sLogger.isLoggable(2)) {
                            com.navdy.hud.app.maps.here.HerePlacesManager.sLogger.v("handleAutoCompleteRequest- time-1 =" + (android.os.SystemClock.elapsedRealtime() - l1));
                            return;
                        }
                        return;
                    }
                    com.here.android.mpa.common.GeoCoordinate geoPosition = hereLocationFixManager.getLastGeoCoordinate();
                    if (geoPosition == null) {
                        com.navdy.hud.app.maps.here.HerePlacesManager.sLogger.i("start location n/a");
                        com.navdy.hud.app.maps.here.HerePlacesManager.returnAutoCompleteErrorResponse(autoCompleteSearch, com.navdy.service.library.events.RequestStatus.REQUEST_NO_LOCATION_SERVICE, com.navdy.hud.app.maps.here.HerePlacesManager.context.getString(com.navdy.hud.app.R.string.no_current_position));
                        if (com.navdy.hud.app.maps.here.HerePlacesManager.sLogger.isLoggable(2)) {
                            com.navdy.hud.app.maps.here.HerePlacesManager.sLogger.v("handleAutoCompleteRequest- time-1 =" + (android.os.SystemClock.elapsedRealtime() - l1));
                            return;
                        }
                        return;
                    }
                    com.navdy.hud.app.maps.here.HerePlacesManager.autoComplete(geoPosition, autoCompleteSearch, searchArea, maxResults, new com.navdy.hud.app.maps.here.HerePlacesManager.Anon5.Anon1(autoCompleteSearch, maxResults, l1));
                    if (com.navdy.hud.app.maps.here.HerePlacesManager.sLogger.isLoggable(2)) {
                        com.navdy.hud.app.maps.here.HerePlacesManager.sLogger.v("handleAutoCompleteRequest- time-1 =" + (android.os.SystemClock.elapsedRealtime() - l1));
                    }
                }
            } catch (Throwable th) {
                if (com.navdy.hud.app.maps.here.HerePlacesManager.sLogger.isLoggable(2)) {
                    com.navdy.hud.app.maps.here.HerePlacesManager.sLogger.v("handleAutoCompleteRequest- time-1 =" + (android.os.SystemClock.elapsedRealtime() - l1));
                }
                throw th;
            }
        }
    }

    static class Anon6 implements com.navdy.hud.app.maps.here.HerePlacesManager.OnCategoriesSearchListener {
        final /* synthetic */ com.here.android.mpa.search.CategoryFilter val$filter;
        final /* synthetic */ com.navdy.hud.app.maps.here.HerePlacesManager.OnCategoriesSearchListener val$listener;
        final /* synthetic */ int val$nResults;

        class Anon1 implements com.navdy.hud.app.maps.here.HerePlacesManager.OnCategoriesSearchListener {
            Anon1() {
            }

            public void onCompleted(java.util.List<com.here.android.mpa.search.Place> places) {
                com.navdy.hud.app.maps.here.HerePlacesManager.Anon6.this.val$listener.onCompleted(places);
            }

            public void onError(com.navdy.hud.app.maps.here.HerePlacesManager.Error error) {
                com.navdy.hud.app.maps.here.HerePlacesManager.Anon6.this.val$listener.onError(error);
            }
        }

        Anon6(com.navdy.hud.app.maps.here.HerePlacesManager.OnCategoriesSearchListener onCategoriesSearchListener, com.here.android.mpa.search.CategoryFilter categoryFilter, int i) {
            this.val$listener = onCategoriesSearchListener;
            this.val$filter = categoryFilter;
            this.val$nResults = i;
        }

        public void onCompleted(java.util.List<com.here.android.mpa.search.Place> places) {
            this.val$listener.onCompleted(places);
        }

        public void onError(com.navdy.hud.app.maps.here.HerePlacesManager.Error error) {
            com.navdy.hud.app.maps.here.HerePlacesManager.handleCategoriesRequest(this.val$filter, this.val$nResults, new com.navdy.hud.app.maps.here.HerePlacesManager.Anon6.Anon1(), true);
        }
    }

    static class Anon7 implements java.lang.Runnable {
        final /* synthetic */ com.here.android.mpa.search.CategoryFilter val$filter;
        final /* synthetic */ com.navdy.hud.app.maps.here.HerePlacesManager.OnCategoriesSearchListener val$listener;
        final /* synthetic */ int val$nResults;
        final /* synthetic */ boolean val$offline;

        class Anon1 implements com.here.android.mpa.search.ResultListener<com.here.android.mpa.search.DiscoveryResultPage> {
            final /* synthetic */ long val$l1;

            /* renamed from: com.navdy.hud.app.maps.here.HerePlacesManager$Anon7$Anon1$Anon1 reason: collision with other inner class name */
            class C0022Anon1 implements java.lang.Runnable {
                final /* synthetic */ com.here.android.mpa.search.ErrorCode val$error;
                final /* synthetic */ com.here.android.mpa.search.DiscoveryResultPage val$results;

                C0022Anon1(com.here.android.mpa.search.ErrorCode errorCode, com.here.android.mpa.search.DiscoveryResultPage discoveryResultPage) {
                    this.val$error = errorCode;
                    this.val$results = discoveryResultPage;
                }

                public void run() {
                    try {
                        if (this.val$error != com.here.android.mpa.search.ErrorCode.NONE) {
                            com.navdy.hud.app.maps.here.HerePlacesManager.sLogger.e("Error in nearby categories response: " + this.val$error.name());
                            com.navdy.hud.app.maps.here.HerePlacesManager.tryRestablishOnline();
                            com.navdy.hud.app.maps.here.HerePlacesManager.Anon7.this.val$listener.onError(com.navdy.hud.app.maps.here.HerePlacesManager.Error.RESPONSE_ERROR);
                            return;
                        }
                        java.util.List<com.here.android.mpa.search.DiscoveryResult> discoveryResults = this.val$results.getItems();
                        java.util.List<com.here.android.mpa.search.PlaceLink> placeLinks = new java.util.ArrayList<>();
                        for (com.here.android.mpa.search.DiscoveryResult discoveryResult : discoveryResults) {
                            if (com.navdy.hud.app.maps.here.HerePlacesManager.sLogger.isLoggable(2)) {
                                com.navdy.hud.app.maps.here.HerePlacesManager.sLogger.v("Found a nearby category item: " + discoveryResult.getTitle());
                            }
                            if (discoveryResult.getResultType() == com.here.android.mpa.search.DiscoveryResult.ResultType.PLACE) {
                                com.navdy.hud.app.maps.here.HerePlacesManager.sLogger.v("Discovered a place: " + discoveryResult.getTitle());
                                placeLinks.add((com.here.android.mpa.search.PlaceLink) discoveryResult);
                            }
                        }
                        if (placeLinks.size() == 0) {
                            com.navdy.hud.app.maps.here.HerePlacesManager.sLogger.e("no places links found");
                            com.navdy.hud.app.maps.here.HerePlacesManager.tryRestablishOnline();
                            com.navdy.hud.app.maps.here.HerePlacesManager.Anon7.this.val$listener.onError(com.navdy.hud.app.maps.here.HerePlacesManager.Error.RESPONSE_ERROR);
                            com.navdy.hud.app.maps.here.HerePlacesManager.sLogger.v("handleCategoriesRequest- time-2 =" + (android.os.SystemClock.elapsedRealtime() - com.navdy.hud.app.maps.here.HerePlacesManager.Anon7.Anon1.this.val$l1));
                            return;
                        }
                        com.navdy.hud.app.maps.here.HerePlacesManager.handlePlaceLinksRequest(placeLinks, com.navdy.hud.app.maps.here.HerePlacesManager.Anon7.this.val$listener);
                        com.navdy.hud.app.maps.here.HerePlacesManager.sLogger.v("handleCategoriesRequest- time-2 =" + (android.os.SystemClock.elapsedRealtime() - com.navdy.hud.app.maps.here.HerePlacesManager.Anon7.Anon1.this.val$l1));
                    } catch (Throwable t) {
                        com.navdy.hud.app.maps.here.HerePlacesManager.sLogger.e("HERE internal DiscoveryRequest.execute callback exception: ", t);
                        com.navdy.hud.app.maps.here.HerePlacesManager.tryRestablishOnline();
                        com.navdy.hud.app.maps.here.HerePlacesManager.Anon7.this.val$listener.onError(com.navdy.hud.app.maps.here.HerePlacesManager.Error.UNKNOWN_ERROR);
                    } finally {
                        com.navdy.hud.app.maps.here.HerePlacesManager.sLogger.v("handleCategoriesRequest- time-2 =" + (android.os.SystemClock.elapsedRealtime() - com.navdy.hud.app.maps.here.HerePlacesManager.Anon7.Anon1.this.val$l1));
                    }
                }
            }

            Anon1(long j) {
                this.val$l1 = j;
            }

            public void onCompleted(com.here.android.mpa.search.DiscoveryResultPage results, com.here.android.mpa.search.ErrorCode error) {
                com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.maps.here.HerePlacesManager.Anon7.Anon1.C0022Anon1(error, results), 2);
            }
        }

        Anon7(com.here.android.mpa.search.CategoryFilter categoryFilter, int i, com.navdy.hud.app.maps.here.HerePlacesManager.OnCategoriesSearchListener onCategoriesSearchListener, boolean z) {
            this.val$filter = categoryFilter;
            this.val$nResults = i;
            this.val$listener = onCategoriesSearchListener;
            this.val$offline = z;
        }

        public void run() {
            long l1 = android.os.SystemClock.elapsedRealtime();
            if (this.val$filter == null || this.val$nResults <= 0 || this.val$listener == null) {
                throw new java.lang.IllegalArgumentException();
            }
            try {
                if (!com.navdy.hud.app.maps.here.HerePlacesManager.hereMapsManager.isInitialized()) {
                    com.navdy.hud.app.maps.here.HerePlacesManager.sLogger.i("engine not ready");
                    this.val$listener.onError(com.navdy.hud.app.maps.here.HerePlacesManager.Error.NO_MAP_ENGINE);
                    return;
                }
                com.navdy.hud.app.maps.here.HereLocationFixManager hereLocationFixManager = com.navdy.hud.app.maps.here.HerePlacesManager.hereMapsManager.getLocationFixManager();
                if (hereLocationFixManager == null) {
                    com.navdy.hud.app.maps.here.HerePlacesManager.sLogger.i("engine not ready, location fix manager n/a");
                    this.val$listener.onError(com.navdy.hud.app.maps.here.HerePlacesManager.Error.NO_USER_LOCATION);
                    com.navdy.hud.app.maps.here.HerePlacesManager.sLogger.v("handleCategoriesRequest time-1=" + (android.os.SystemClock.elapsedRealtime() - l1));
                    return;
                }
                com.here.android.mpa.common.GeoCoordinate geoPosition = com.navdy.hud.app.maps.here.HerePlacesManager.hereMapsManager.getRouteStartPoint();
                if (geoPosition == null) {
                    geoPosition = hereLocationFixManager.getLastGeoCoordinate();
                } else {
                    com.navdy.hud.app.maps.here.HerePlacesManager.sLogger.v("using debug start point:" + geoPosition);
                }
                if (geoPosition == null) {
                    com.navdy.hud.app.maps.here.HerePlacesManager.sLogger.i("start location n/a");
                    this.val$listener.onError(com.navdy.hud.app.maps.here.HerePlacesManager.Error.NO_USER_LOCATION);
                    com.navdy.hud.app.maps.here.HerePlacesManager.sLogger.v("handleCategoriesRequest time-1=" + (android.os.SystemClock.elapsedRealtime() - l1));
                    return;
                }
                if (this.val$offline) {
                    com.navdy.hud.app.maps.here.HerePlacesManager.establishOffline();
                } else if (!com.navdy.hud.app.maps.here.HereMapsManager.getInstance().isEngineOnline()) {
                    this.val$listener.onError(com.navdy.hud.app.maps.here.HerePlacesManager.Error.BAD_REQUEST);
                    com.navdy.hud.app.maps.here.HerePlacesManager.sLogger.v("handleCategoriesRequest time-1=" + (android.os.SystemClock.elapsedRealtime() - l1));
                    return;
                }
                com.here.android.mpa.search.ErrorCode error = new com.here.android.mpa.search.ExploreRequest().setCategoryFilter(this.val$filter).setSearchCenter(geoPosition).setCollectionSize(this.val$nResults).execute(new com.navdy.hud.app.maps.here.HerePlacesManager.Anon7.Anon1(l1));
                if (error != com.here.android.mpa.search.ErrorCode.NONE) {
                    com.navdy.hud.app.maps.here.HerePlacesManager.sLogger.e("Error while requesting nearby gas stations: " + error.name());
                    com.navdy.hud.app.maps.here.HerePlacesManager.tryRestablishOnline();
                    this.val$listener.onError(com.navdy.hud.app.maps.here.HerePlacesManager.Error.BAD_REQUEST);
                }
                com.navdy.hud.app.maps.here.HerePlacesManager.sLogger.v("handleCategoriesRequest time-1=" + (android.os.SystemClock.elapsedRealtime() - l1));
            } catch (Throwable t) {
                com.navdy.hud.app.maps.here.HerePlacesManager.sLogger.e("HERE internal DiscoveryRequest.execute exception: ", t);
                com.navdy.hud.app.maps.here.HerePlacesManager.tryRestablishOnline();
                this.val$listener.onError(com.navdy.hud.app.maps.here.HerePlacesManager.Error.UNKNOWN_ERROR);
            } finally {
                com.navdy.hud.app.maps.here.HerePlacesManager.sLogger.v("handleCategoriesRequest time-1=" + (android.os.SystemClock.elapsedRealtime() - l1));
            }
        }
    }

    static class Anon8 implements com.here.android.mpa.search.ResultListener<com.here.android.mpa.search.Place> {
        final /* synthetic */ java.util.concurrent.atomic.AtomicInteger val$counter;
        final /* synthetic */ com.navdy.hud.app.maps.here.HerePlacesManager.OnCategoriesSearchListener val$listener;
        final /* synthetic */ java.util.List val$places;

        class Anon1 implements java.lang.Runnable {
            final /* synthetic */ com.here.android.mpa.search.ErrorCode val$error;
            final /* synthetic */ com.here.android.mpa.search.Place val$place;

            Anon1(com.here.android.mpa.search.ErrorCode errorCode, com.here.android.mpa.search.Place place) {
                this.val$error = errorCode;
                this.val$place = place;
            }

            public void run() {
                try {
                    int left = com.navdy.hud.app.maps.here.HerePlacesManager.Anon8.this.val$counter.decrementAndGet();
                    if (this.val$error != com.here.android.mpa.search.ErrorCode.NONE || this.val$place == null) {
                        com.navdy.hud.app.maps.here.HerePlacesManager.sLogger.e("Error in place detail response: " + this.val$error.name());
                    } else {
                        com.navdy.hud.app.maps.here.HerePlacesManager.sLogger.v("Found a Place: " + this.val$place.getName());
                        for (com.here.android.mpa.search.Category category : this.val$place.getCategories()) {
                            com.navdy.hud.app.maps.here.HerePlacesManager.sLogger.v("place category: " + category.getName());
                        }
                        com.navdy.hud.app.maps.here.HerePlacesManager.Anon8.this.val$places.add(this.val$place);
                    }
                    if (left == 0) {
                        com.navdy.hud.app.maps.here.HerePlacesManager.invokeCategorySearchListener(com.navdy.hud.app.maps.here.HerePlacesManager.Anon8.this.val$places, com.navdy.hud.app.maps.here.HerePlacesManager.Anon8.this.val$listener);
                    }
                } catch (Throwable t) {
                    com.navdy.hud.app.maps.here.HerePlacesManager.sLogger.e("HERE internal PlaceLink.getDetailsRequest callback exception: ", t);
                    if (com.navdy.hud.app.maps.here.HerePlacesManager.Anon8.this.val$counter.get() == 0) {
                        com.navdy.hud.app.maps.here.HerePlacesManager.invokeCategorySearchListener(com.navdy.hud.app.maps.here.HerePlacesManager.Anon8.this.val$places, com.navdy.hud.app.maps.here.HerePlacesManager.Anon8.this.val$listener);
                    }
                }
            }
        }

        Anon8(java.util.concurrent.atomic.AtomicInteger atomicInteger, java.util.List list, com.navdy.hud.app.maps.here.HerePlacesManager.OnCategoriesSearchListener onCategoriesSearchListener) {
            this.val$counter = atomicInteger;
            this.val$places = list;
            this.val$listener = onCategoriesSearchListener;
        }

        public void onCompleted(com.here.android.mpa.search.Place place, com.here.android.mpa.search.ErrorCode error) {
            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.maps.here.HerePlacesManager.Anon8.Anon1(error, place), 2);
        }
    }

    static class Anon9 implements java.lang.Runnable {
        final /* synthetic */ com.here.android.mpa.common.GeoCoordinate val$currentLocation;
        final /* synthetic */ java.util.List val$destinationLinks;
        final /* synthetic */ int val$maxResults;
        final /* synthetic */ java.lang.String val$queryText;
        final /* synthetic */ int val$searchArea;

        Anon9(int i, java.util.List list, com.here.android.mpa.common.GeoCoordinate geoCoordinate, int i2, java.lang.String str) {
            this.val$maxResults = i;
            this.val$destinationLinks = list;
            this.val$currentLocation = geoCoordinate;
            this.val$searchArea = i2;
            this.val$queryText = str;
        }

        public void run() {
            int count = 0;
            try {
                java.util.ArrayList arrayList = new java.util.ArrayList(this.val$maxResults);
                java.util.Iterator it = this.val$destinationLinks.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        break;
                    }
                    com.here.android.mpa.search.PlaceLink placeLink = (com.here.android.mpa.search.PlaceLink) it.next();
                    com.here.android.mpa.common.GeoCoordinate placesLocationCoordinate = placeLink.getPosition();
                    if (placesLocationCoordinate != null && (this.val$currentLocation == null || this.val$searchArea <= 0 || this.val$currentLocation.distanceTo(placesLocationCoordinate) <= ((double) this.val$searchArea))) {
                        com.navdy.service.library.events.location.Coordinate coordinate = new com.navdy.service.library.events.location.Coordinate.Builder().latitude(java.lang.Double.valueOf(placesLocationCoordinate.getLatitude())).longitude(java.lang.Double.valueOf(placesLocationCoordinate.getLongitude())).build();
                        java.lang.String address = placeLink.getVicinity();
                        java.lang.String title = placeLink.getTitle();
                        java.lang.String categoryStr = null;
                        double distance = placeLink.getDistance();
                        if (address != null) {
                            address = address.replace("<br/>", ", ");
                        }
                        com.here.android.mpa.search.Category category = placeLink.getCategory();
                        if (category != null) {
                            categoryStr = category.getName();
                        }
                        arrayList.add(new com.navdy.service.library.events.places.PlacesSearchResult(title, null, coordinate, address, categoryStr, java.lang.Double.valueOf(distance)));
                        count++;
                        if (count == this.val$maxResults) {
                            com.navdy.hud.app.maps.here.HerePlacesManager.sLogger.v("[search] max results reached:" + this.val$maxResults);
                            break;
                        }
                    }
                }
                com.navdy.hud.app.maps.MapsEventHandler.getInstance().sendEventToClient(new com.navdy.service.library.events.places.PlacesSearchResponse.Builder().searchQuery(this.val$queryText).status(com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS).statusDetail(null).results(arrayList).build());
                com.navdy.hud.app.maps.here.HerePlacesManager.sLogger.v("[search] returned:" + arrayList.size());
            } catch (Throwable t) {
                com.navdy.hud.app.maps.here.HerePlacesManager.sLogger.e(t);
                com.navdy.hud.app.maps.here.HerePlacesManager.returnSearchErrorResponse(this.val$queryText, com.navdy.service.library.events.RequestStatus.REQUEST_UNKNOWN_ERROR, com.navdy.hud.app.maps.here.HerePlacesManager.context.getString(com.navdy.hud.app.R.string.unknown_error));
            }
        }
    }

    public interface AutoCompleteCallback {
        void result(com.here.android.mpa.search.ErrorCode errorCode, java.util.List<java.lang.String> list);
    }

    public enum Error {
        BAD_REQUEST,
        RESPONSE_ERROR,
        NO_USER_LOCATION,
        NO_MAP_ENGINE,
        NO_ROUTES,
        UNKNOWN_ERROR
    }

    public interface GeoCodeCallback {
        void result(com.here.android.mpa.search.ErrorCode errorCode, java.util.List<com.here.android.mpa.search.Location> list);
    }

    public interface OnCategoriesSearchListener {
        void onCompleted(java.util.List<com.here.android.mpa.search.Place> list);

        void onError(com.navdy.hud.app.maps.here.HerePlacesManager.Error error);
    }

    public interface PlacesSearchCallback {
        void result(com.here.android.mpa.search.ErrorCode errorCode, com.here.android.mpa.search.DiscoveryResultPage discoveryResultPage);
    }

    public static void searchPlaces(com.here.android.mpa.common.GeoCoordinate currentPosition, java.lang.String queryText, int searchArea, int maxResults, com.navdy.hud.app.maps.here.HerePlacesManager.PlacesSearchCallback callBack) {
        com.here.android.mpa.search.SearchRequest request = new com.here.android.mpa.search.SearchRequest(queryText);
        request.setSearchCenter(currentPosition);
        request.setCollectionSize(maxResults);
        com.here.android.mpa.search.ErrorCode errorCode = request.execute(new com.navdy.hud.app.maps.here.HerePlacesManager.Anon1(callBack));
        if (errorCode != com.here.android.mpa.search.ErrorCode.NONE) {
            try {
                callBack.result(errorCode, null);
            } catch (Throwable t) {
                sLogger.e(t);
            }
        }
    }

    public static com.here.android.mpa.search.GeocodeRequest geoCodeStreetAddress(com.here.android.mpa.common.GeoCoordinate currentPosition, java.lang.String queryText, int searchArea, com.navdy.hud.app.maps.here.HerePlacesManager.GeoCodeCallback callBack) {
        com.here.android.mpa.search.GeocodeRequest geocodeRequest = new com.here.android.mpa.search.GeocodeRequest(queryText);
        if (currentPosition != null) {
            geocodeRequest.setSearchArea(currentPosition, searchArea);
        }
        com.here.android.mpa.search.ErrorCode errorCode = geocodeRequest.execute(new com.navdy.hud.app.maps.here.HerePlacesManager.Anon2(callBack));
        if (errorCode == com.here.android.mpa.search.ErrorCode.NONE) {
            return geocodeRequest;
        }
        try {
            callBack.result(errorCode, null);
        } catch (Throwable t) {
            sLogger.e(t);
        }
        return null;
    }

    public static void autoComplete(com.here.android.mpa.common.GeoCoordinate currentPosition, java.lang.String queryText, int searchArea, int maxResults, com.navdy.hud.app.maps.here.HerePlacesManager.AutoCompleteCallback callBack) {
        com.here.android.mpa.search.TextSuggestionRequest request = new com.here.android.mpa.search.TextSuggestionRequest(queryText);
        request.setCollectionSize(maxResults);
        request.setSearchCenter(currentPosition);
        com.here.android.mpa.search.ErrorCode errorCode = request.execute(new com.navdy.hud.app.maps.here.HerePlacesManager.Anon3(callBack));
        if (errorCode != com.here.android.mpa.search.ErrorCode.NONE) {
            try {
                callBack.result(errorCode, null);
            } catch (Throwable t) {
                sLogger.e(t);
            }
        }
    }

    public static void handlePlacesSearchRequest(com.navdy.service.library.events.places.PlacesSearchRequest request) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.maps.here.HerePlacesManager.Anon4(request), 19);
    }

    public static void handleAutoCompleteRequest(com.navdy.service.library.events.places.AutoCompleteRequest request) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.maps.here.HerePlacesManager.Anon5(request), 19);
    }

    public static void handleCategoriesRequest(com.here.android.mpa.search.CategoryFilter filter, int nResults, com.navdy.hud.app.maps.here.HerePlacesManager.OnCategoriesSearchListener listener) {
        handleCategoriesRequest(filter, nResults, new com.navdy.hud.app.maps.here.HerePlacesManager.Anon6(listener, filter, nResults), false);
    }

    public static void handleCategoriesRequest(com.here.android.mpa.search.CategoryFilter filter, int nResults, com.navdy.hud.app.maps.here.HerePlacesManager.OnCategoriesSearchListener listener, boolean offline) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.maps.here.HerePlacesManager.Anon7(filter, nResults, listener, offline), 2);
    }

    /* access modifiers changed from: private */
    public static void handlePlaceLinksRequest(java.util.List<com.here.android.mpa.search.PlaceLink> placeLinks, com.navdy.hud.app.maps.here.HerePlacesManager.OnCategoriesSearchListener listener) {
        java.util.List<com.here.android.mpa.search.Place> places = new java.util.ArrayList<>();
        java.util.concurrent.atomic.AtomicInteger counter = new java.util.concurrent.atomic.AtomicInteger(placeLinks.size());
        for (com.here.android.mpa.search.PlaceLink placeLink : placeLinks) {
            com.here.android.mpa.search.ErrorCode error = placeLink.getDetailsRequest().execute(new com.navdy.hud.app.maps.here.HerePlacesManager.Anon8(counter, places, listener));
            if (error != com.here.android.mpa.search.ErrorCode.NONE) {
                sLogger.e("Error in place detail request: " + error.name());
                if (counter.decrementAndGet() == 0 && places.size() == 0) {
                    tryRestablishOnline();
                    listener.onError(com.navdy.hud.app.maps.here.HerePlacesManager.Error.BAD_REQUEST);
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public static void invokeCategorySearchListener(java.util.List<com.here.android.mpa.search.Place> places, com.navdy.hud.app.maps.here.HerePlacesManager.OnCategoriesSearchListener listener) {
        int size = places.size();
        if (size == 0) {
            sLogger.v("places query complete, no result");
            tryRestablishOnline();
            listener.onError(com.navdy.hud.app.maps.here.HerePlacesManager.Error.RESPONSE_ERROR);
            return;
        }
        sLogger.v("places query complete:" + size);
        tryRestablishOnline();
        listener.onCompleted(places);
    }

    /* access modifiers changed from: private */
    public static void establishOffline() {
        willRestablishOnline = com.navdy.hud.app.maps.here.HereMapsManager.getInstance().isEngineOnline();
        if (willRestablishOnline) {
            sLogger.v("turn engine offline");
            com.navdy.hud.app.maps.here.HereMapsManager.getInstance().turnOffline();
        }
    }

    /* access modifiers changed from: private */
    public static void tryRestablishOnline() {
        if (willRestablishOnline && !com.navdy.hud.app.maps.here.HereMapsManager.getInstance().isEngineOnline() && com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().isRemoteDeviceConnected() && com.navdy.hud.app.framework.network.NetworkStateManager.isConnectedToNetwork(com.navdy.hud.app.HudApplication.getAppContext())) {
            sLogger.v("turn engine online");
            com.navdy.hud.app.maps.here.HereMapsManager.getInstance().turnOnline();
        }
        willRestablishOnline = false;
    }

    /* access modifiers changed from: private */
    public static void returnSearchErrorResponse(java.lang.String searchQuery, com.navdy.service.library.events.RequestStatus status, java.lang.String errorText) {
        try {
            sLogger.e("[search]" + status + ": " + errorText);
            com.navdy.hud.app.maps.MapsEventHandler.getInstance().sendEventToClient(new com.navdy.service.library.events.places.PlacesSearchResponse.Builder().searchQuery(searchQuery).status(status).statusDetail(errorText).results(null).build());
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }

    /* access modifiers changed from: private */
    public static void returnSearchResults(com.here.android.mpa.common.GeoCoordinate currentLocation, java.lang.String queryText, java.util.List<com.here.android.mpa.search.PlaceLink> destinationLinks, int maxResults, int searchArea) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.maps.here.HerePlacesManager.Anon9(maxResults, destinationLinks, currentLocation, searchArea, queryText), 2);
    }

    /* access modifiers changed from: private */
    public static void returnAutoCompleteErrorResponse(java.lang.String partialSearch, com.navdy.service.library.events.RequestStatus status, java.lang.String errorText) {
        try {
            sLogger.e("[autocomplete]" + status + ": " + errorText);
            com.navdy.hud.app.maps.MapsEventHandler.getInstance().sendEventToClient(new com.navdy.service.library.events.places.AutoCompleteResponse(partialSearch, status, errorText, null));
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }

    /* access modifiers changed from: private */
    public static void returnAutoCompleteResults(java.lang.String autoCompleteSearch, int maxResults, java.util.List<java.lang.String> results) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.maps.here.HerePlacesManager.Anon10(results, maxResults, autoCompleteSearch), 2);
    }

    public static void printSearchRequest(com.navdy.service.library.events.places.PlacesSearchRequest request) {
        try {
            sLogger.v("PlacesSearchRequest query[" + request.searchQuery + "] area[" + request.searchArea + "] maxResults[" + request.maxResults + "]");
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }

    public static void printAutoCompleteRequest(com.navdy.service.library.events.places.AutoCompleteRequest request) {
        try {
            sLogger.v("AutoCompleteRequest query[" + request.partialSearch + "] maxResults[" + request.maxResults + "]");
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }

    public static com.here.android.mpa.common.GeoCoordinate getPlaceEntry(com.here.android.mpa.search.Place place) {
        if (place.getLocation().getAccessPoints().size() > 0) {
            return ((com.here.android.mpa.search.NavigationPosition) place.getLocation().getAccessPoints().get(0)).getCoordinate();
        }
        return place.getLocation().getCoordinate();
    }
}
