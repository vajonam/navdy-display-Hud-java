package com.navdy.hud.app.maps.here;

public class HereSpeedWarningListener extends com.here.android.mpa.guidance.NavigationManager.SpeedWarningListener {
    private static final int BUFFER_SPEED = 7;
    private com.squareup.otto.Bus bus;
    private android.content.Context context = com.navdy.hud.app.HudApplication.getAppContext();
    private com.navdy.hud.app.maps.here.HereNavigationManager hereNavigationManager;
    private com.navdy.service.library.log.Logger logger;
    private com.navdy.hud.app.maps.MapsEventHandler mapsEventHandler;
    private com.navdy.hud.app.manager.SpeedManager speedManager = com.navdy.hud.app.manager.SpeedManager.getInstance();
    private boolean speedWarningOn;
    private java.lang.String tag;
    private boolean verbose;
    private int warningSpeed;

    HereSpeedWarningListener(com.navdy.service.library.log.Logger logger2, java.lang.String tag2, boolean verbose2, com.squareup.otto.Bus bus2, com.navdy.hud.app.maps.MapsEventHandler mapsEventHandler2, com.navdy.hud.app.maps.here.HereNavigationManager hereNavigationManager2) {
        this.logger = logger2;
        this.tag = tag2;
        this.verbose = verbose2;
        this.bus = bus2;
        this.mapsEventHandler = mapsEventHandler2;
        this.hereNavigationManager = hereNavigationManager2;
    }

    public void onSpeedExceeded(java.lang.String roadName, float speedLimit) {
        int speedToConsider;
        this.bus.post(com.navdy.hud.app.maps.here.HereNavigationManager.SPEED_EXCEEDED);
        this.logger.w(this.tag + " speed exceeded:" + roadName + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA + speedLimit);
        if (!this.speedWarningOn) {
            this.warningSpeed = -1;
        }
        this.speedWarningOn = true;
        if (java.lang.Boolean.TRUE.equals(com.navdy.hud.app.maps.MapsEventHandler.getInstance().getNavigationPreferences().spokenSpeedLimitWarnings) && !com.navdy.hud.app.framework.phonecall.CallUtils.isPhoneCallInProgress()) {
            int currentSpeed = this.speedManager.getCurrentSpeed();
            if (currentSpeed <= 0) {
                this.logger.w(this.tag + "no obd or gps speed available");
                return;
            }
            com.navdy.hud.app.manager.SpeedManager.SpeedUnit speedUnit = this.speedManager.getSpeedUnit();
            int speedAllowed = com.navdy.hud.app.manager.SpeedManager.convert((double) speedLimit, com.navdy.hud.app.manager.SpeedManager.SpeedUnit.METERS_PER_SECOND, speedUnit);
            if (this.warningSpeed == -1) {
                speedToConsider = speedAllowed + 7;
            } else {
                speedToConsider = this.warningSpeed + (this.warningSpeed / 10);
            }
            if (currentSpeed >= speedToConsider) {
                this.logger.w(this.tag + " speed exceeded current[" + currentSpeed + "] threshold[" + speedToConsider + "] allowed[" + speedAllowed + "] " + speedUnit.name());
                this.warningSpeed = currentSpeed;
                switch (speedUnit) {
                    case MILES_PER_HOUR:
                        java.lang.String str = this.hereNavigationManager.TTS_MILES;
                        break;
                    case KILOMETERS_PER_HOUR:
                        java.lang.String str2 = this.hereNavigationManager.TTS_KMS;
                        break;
                    case METERS_PER_SECOND:
                        java.lang.String str3 = this.hereNavigationManager.TTS_METERS;
                        break;
                    default:
                        java.lang.String str4 = "";
                        break;
                }
                com.navdy.hud.app.framework.voice.TTSUtils.sendSpeechRequest(this.context.getString(com.navdy.hud.app.R.string.tts_speed_warning, new java.lang.Object[]{java.lang.Integer.valueOf(speedAllowed)}), com.navdy.service.library.events.audio.Category.SPEECH_SPEED_WARNING, null);
                return;
            }
            this.logger.w(this.tag + " speed exceeded current[" + currentSpeed + "] threshold[" + speedToConsider + "] allowed[" + speedAllowed + "] " + speedUnit.name());
        }
    }

    public void onSpeedExceededEnd(java.lang.String roadName, float speedLimit) {
        this.speedWarningOn = false;
        this.warningSpeed = -1;
        this.bus.post(com.navdy.hud.app.maps.here.HereNavigationManager.SPEED_NORMAL);
        this.logger.w(this.tag + "speed normal:" + roadName + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA + speedLimit);
    }
}
