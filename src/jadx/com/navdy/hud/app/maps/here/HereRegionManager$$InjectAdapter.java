package com.navdy.hud.app.maps.here;

public final class HereRegionManager$$InjectAdapter extends dagger.internal.Binding<com.navdy.hud.app.maps.here.HereRegionManager> implements dagger.MembersInjector<com.navdy.hud.app.maps.here.HereRegionManager> {
    private dagger.internal.Binding<com.squareup.otto.Bus> bus;

    public HereRegionManager$$InjectAdapter() {
        super(null, "members/com.navdy.hud.app.maps.here.HereRegionManager", false, com.navdy.hud.app.maps.here.HereRegionManager.class);
    }

    public void attach(dagger.internal.Linker linker) {
        this.bus = linker.requestBinding("com.squareup.otto.Bus", com.navdy.hud.app.maps.here.HereRegionManager.class, getClass().getClassLoader());
    }

    public void getDependencies(java.util.Set<dagger.internal.Binding<?>> set, java.util.Set<dagger.internal.Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.bus);
    }

    public void injectMembers(com.navdy.hud.app.maps.here.HereRegionManager object) {
        object.bus = (com.squareup.otto.Bus) this.bus.get();
    }
}
