package com.navdy.hud.app.maps.here;

class HereNavigationEventListener extends com.here.android.mpa.guidance.NavigationManager.NavigationManagerEventListener {
    /* access modifiers changed from: private */
    public final com.navdy.hud.app.maps.here.HereNavigationManager hereNavigationManager;
    /* access modifiers changed from: private */
    public final com.navdy.service.library.log.Logger logger;
    /* access modifiers changed from: private */
    public final java.lang.String tag;

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            com.here.android.mpa.guidance.NavigationManager.NavigationMode mode = com.navdy.hud.app.maps.here.HereNavigationEventListener.this.hereNavigationManager.getHereNavigationState();
            com.navdy.hud.app.maps.here.HereNavigationEventListener.this.logger.i(com.navdy.hud.app.maps.here.HereNavigationEventListener.this.tag + " onNavigationModeChanged HERE mode=" + mode + " Navdy=" + com.navdy.hud.app.maps.here.HereNavigationEventListener.this.hereNavigationManager.getNavigationState());
        }
    }

    HereNavigationEventListener(com.navdy.service.library.log.Logger logger2, java.lang.String tag2, com.navdy.hud.app.maps.here.HereNavigationManager hereNavigationManager2) {
        this.logger = logger2;
        this.tag = tag2;
        this.hereNavigationManager = hereNavigationManager2;
    }

    public void onNavigationModeChanged() {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.maps.here.HereNavigationEventListener.Anon1(), 2);
    }

    public void onRouteUpdated(com.here.android.mpa.routing.Route updatedRoute) {
        this.logger.i(this.tag + " onRouteUpdated:" + updatedRoute.toString());
    }

    public void onEnded(com.here.android.mpa.guidance.NavigationManager.NavigationMode mode) {
        this.logger.i(this.tag + " onEnded:" + mode);
        if (this.hereNavigationManager.isLastManeuver()) {
            this.logger.i(this.tag + " onEnded: last maneuver on, marking arrived");
            this.hereNavigationManager.arrived();
        }
    }
}
