package com.navdy.hud.app.maps.here;

public class HereNavigationManager {
    /* access modifiers changed from: private */
    public static final com.navdy.hud.app.maps.MapEvents.ArrivalEvent ARRIVAL_EVENT = new com.navdy.hud.app.maps.MapEvents.ArrivalEvent();
    private static final int ARRIVAL_GEO_FENCE_THRESHOLD = 804;
    static final java.lang.String EMPTY_STR = "";
    private static final int ETA_CALCULATION_INTERVAL = 30000;
    private static final int ETA_UPDATE_THRESHOLD = 60000;
    private static java.util.Map<java.lang.String, java.lang.String> LANGUAGE_HIERARCHY = new java.util.HashMap();
    static final com.navdy.hud.app.maps.MapEvents.SpeedWarning SPEED_EXCEEDED = new com.navdy.hud.app.maps.MapEvents.SpeedWarning(true);
    static final com.navdy.hud.app.maps.MapEvents.SpeedWarning SPEED_NORMAL = new com.navdy.hud.app.maps.MapEvents.SpeedWarning(false);
    private static final java.lang.String TAG_AUDIO_CALLBACK = "[CB-AUDIO]";
    private static final java.lang.String TAG_GPS_CALLBACK = "[CB-GPS]";
    private static final java.lang.String TAG_NAV_MANAGER_CALLBACK = "[CB-NAVM]";
    private static final java.lang.String TAG_NEW_MANEUVER_CALLBACK = "[CB-NEWM]";
    private static final java.lang.String TAG_POS_UPDATE_CALLBACK = "[CB-POS-UPDATE]";
    private static final java.lang.String TAG_REROUTE_CALLBACK = "[CB-REROUTE]";
    private static final java.lang.String TAG_SAFETY_CALLBACK = "[CB-SAFE]";
    private static final java.lang.String TAG_SHOWLANE_INFO_CALLBACK = "[CB-LANEINFO]";
    private static final java.lang.String TAG_SPEED_CALLBACK = "[CB-SPD]";
    private static final java.lang.String TAG_TRAFFIC_ETA_TRACKER = "[CB-TRAFFIC-ETA]";
    private static final java.lang.String TAG_TRAFFIC_REROUTE_CALLBACK = "[CB-TRAFFIC-REROUTE]";
    private static final java.lang.String TAG_TRAFFIC_UPDATE = "[CB-TRAFFIC-UPDATE]";
    private static final java.lang.String TAG_TRAFFIC_WARN = "[CB-TRAFFIC-WARN]";
    private static final java.lang.String TAG_TTS = "[CB-TTS]";
    private static final java.lang.String TBT_LANGUAGE_CODE = "en-US";
    /* access modifiers changed from: private */
    public static final boolean VERBOSE = com.navdy.hud.app.BuildConfig.DEBUG;
    /* access modifiers changed from: private */
    public static android.content.Context context = com.navdy.hud.app.HudApplication.getAppContext();
    /* access modifiers changed from: private */
    public static final com.navdy.hud.app.maps.here.HereMapsManager hereMapsManager = com.navdy.hud.app.maps.here.HereMapsManager.getInstance();
    /* access modifiers changed from: private */
    public static final com.navdy.hud.app.maps.MapsEventHandler mapsEventHandler = com.navdy.hud.app.maps.MapsEventHandler.getInstance();
    static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.maps.here.HereNavigationManager.class);
    private static final com.navdy.hud.app.maps.here.HereNavigationManager sSingleton = new com.navdy.hud.app.maps.here.HereNavigationManager();
    private static final com.navdy.hud.app.manager.SpeedManager speedManager = com.navdy.hud.app.manager.SpeedManager.getInstance();
    private static final java.lang.Object trafficRerouteLock = new java.lang.Object();
    final com.navdy.hud.app.maps.MapEvents.GpsStatusChange BUS_GPS_SIGNAL_LOST = new com.navdy.hud.app.maps.MapEvents.GpsStatusChange(false);
    final com.navdy.hud.app.maps.MapEvents.GpsStatusChange BUS_GPS_SIGNAL_RESTORED = new com.navdy.hud.app.maps.MapEvents.GpsStatusChange(true);
    /* access modifiers changed from: private */
    public final java.lang.String HERE_TBT_AUDIO_DESTINATION_REACHED_PATTERN;
    private java.lang.String INVALID_ROUTE_ID;
    private java.lang.String INVALID_STATE;
    private java.lang.String NO_CURRENT_POSITION;
    java.lang.String TTS_KMS;
    java.lang.String TTS_METERS;
    java.lang.String TTS_MILES;
    com.navdy.hud.app.event.LocalSpeechRequest TTS_REROUTING;
    com.navdy.hud.app.event.LocalSpeechRequest TTS_REROUTING_FAILED;
    private com.here.android.mpa.guidance.AudioPlayerDelegate audioPlayerDelegate;
    private com.navdy.hud.app.framework.network.NetworkBandwidthController bandwidthController = com.navdy.hud.app.framework.network.NetworkBandwidthController.getInstance();
    /* access modifiers changed from: private */
    public com.squareup.otto.Bus bus = com.navdy.hud.app.maps.MapsEventHandler.getInstance().getBus();
    private com.here.android.mpa.routing.Maneuver currentManeuver;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.maps.here.HereNavigationInfo currentNavigationInfo = new com.navdy.hud.app.maps.here.HereNavigationInfo();
    /* access modifiers changed from: private */
    public com.navdy.hud.app.maps.NavigationMode currentNavigationMode;
    /* access modifiers changed from: private */
    public com.navdy.service.library.events.navigation.NavigationSessionState currentSessionState;
    private java.lang.String debugManeuverDistance;
    private int debugManeuverIcon;
    private java.lang.String debugManeuverInstruction;
    private com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.ManeuverState debugManeuverState;
    private int debugNextManeuverIcon;
    private com.navdy.hud.app.profile.DriverSessionPreferences driverSessionPreferences;
    private java.lang.Runnable etaCalcRunnable = new com.navdy.hud.app.maps.here.HereNavigationManager.Anon1();
    private java.lang.Boolean generatePhoneticTTS;
    private com.navdy.hud.app.maps.here.HereGpsSignalListener gpsSignalListener;
    /* access modifiers changed from: private */
    public android.os.Handler handler = new android.os.Handler(android.os.Looper.getMainLooper());
    /* access modifiers changed from: private */
    public com.navdy.hud.app.maps.here.HereNavController hereNavController;
    private com.navdy.hud.app.maps.here.HereRealisticViewListener hereRealisticViewListener;
    private com.navdy.hud.app.maps.here.HereLaneInfoListener laneInfoListener;
    private com.here.android.mpa.routing.Maneuver maneuverAfterCurrent;
    private com.navdy.hud.app.maps.here.HereMapController mapController;
    private com.here.android.mpa.mapping.MapView mapView;
    private com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager navdyTrafficRerouteManager;
    private com.here.android.mpa.guidance.NavigationManager navigationManager;
    private com.navdy.hud.app.maps.here.HereNavigationEventListener navigationManagerEventListener;
    private com.navdy.hud.app.ui.component.homescreen.NavigationView navigationView;
    private com.navdy.hud.app.maps.here.HereNewManeuverListener newManeuverEventListener;
    private com.navdy.hud.app.maps.here.HerePositionUpdateListener positionUpdateListener;
    private com.here.android.mpa.routing.Maneuver prevManeuver;
    private com.navdy.hud.app.maps.here.HereRerouteListener reRouteListener;
    private com.navdy.hud.app.maps.here.HereSafetySpotListener safetySpotListener;
    private android.content.SharedPreferences sharedPreferences = com.navdy.hud.app.maps.MapsEventHandler.getInstance().getSharedPreferences();
    private com.navdy.hud.app.maps.here.HereSpeedWarningManager speedWarningManager;
    private java.lang.StringBuilder stringBuilder = new java.lang.StringBuilder();
    private boolean switchingToNewRoute;
    private com.navdy.hud.app.common.TimeHelper timeHelper;
    private com.navdy.hud.app.maps.here.HereTrafficETATracker trafficEtaTracker;
    private com.navdy.hud.app.maps.here.HereTrafficRerouteListener trafficRerouteListener;
    private com.navdy.hud.app.maps.here.HereTrafficUpdater2 trafficUpdater;
    private boolean trafficUpdaterStarted;

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            if (com.navdy.hud.app.maps.here.HereNavigationManager.this.currentNavigationMode != com.navdy.hud.app.maps.NavigationMode.MAP) {
                long time = java.lang.System.currentTimeMillis() - com.navdy.hud.app.maps.here.HereNavigationManager.this.currentNavigationInfo.lastManeuverPostTime;
                if (time < 0) {
                    com.navdy.hud.app.maps.here.HereNavigationManager.this.handler.postDelayed(this, 30000);
                } else if (time < 60000) {
                    if (com.navdy.hud.app.maps.here.HereNavigationManager.sLogger.isLoggable(2)) {
                        com.navdy.hud.app.maps.here.HereNavigationManager.sLogger.v("etaCalc threshold not met:" + time);
                    }
                    com.navdy.hud.app.maps.here.HereNavigationManager.this.handler.postDelayed(this, 30000);
                } else {
                    com.navdy.hud.app.maps.here.HereNavigationManager.this.refreshNavigationInfo();
                    if (com.navdy.hud.app.maps.here.HereNavigationManager.sLogger.isLoggable(2)) {
                        com.navdy.hud.app.maps.here.HereNavigationManager.sLogger.v("etaCalc updated maneuver:" + time);
                    }
                    com.navdy.hud.app.maps.here.HereNavigationManager.this.handler.postDelayed(this, 30000);
                }
            }
        }
    }

    class Anon10 implements java.lang.Runnable {
        Anon10() {
        }

        public void run() {
            com.navdy.hud.app.maps.here.HereNavigationManager.sLogger.v("limitbandwidth: turn off HERE traffic avoidance mode");
            com.navdy.hud.app.maps.here.HereNavigationManager.this.hereNavController.setTrafficAvoidanceMode(com.here.android.mpa.guidance.NavigationManager.TrafficAvoidanceMode.DISABLE);
        }
    }

    class Anon11 implements java.lang.Runnable {
        Anon11() {
        }

        public void run() {
            com.navdy.hud.app.maps.here.HereNavigationManager.sLogger.v("limitbandwidth: turn on HERE traffic avoidance mode");
            com.navdy.hud.app.maps.here.HereNavigationManager.this.hereNavController.setTrafficAvoidanceMode(com.here.android.mpa.guidance.NavigationManager.TrafficAvoidanceMode.MANUAL);
        }
    }

    class Anon2 implements com.here.android.mpa.guidance.AudioPlayerDelegate {
        Anon2() {
        }

        public boolean playText(java.lang.String s) {
            com.navdy.hud.app.maps.here.HereNavigationManager.sLogger.v("[CB-TTS] [" + s + "]");
            if (com.navdy.hud.app.maps.here.HereNavigationManager.this.currentNavigationMode == com.navdy.hud.app.maps.NavigationMode.MAP) {
                com.navdy.hud.app.maps.here.HereNavigationManager.sLogger.i("[CB-TTS] not navigating");
            } else {
                boolean hasArrivedPattern = false;
                if (s != null && s.toLowerCase().contains(com.navdy.hud.app.maps.here.HereNavigationManager.this.HERE_TBT_AUDIO_DESTINATION_REACHED_PATTERN)) {
                    hasArrivedPattern = true;
                }
                if (hasArrivedPattern && !com.navdy.hud.app.maps.here.HereNavigationManager.this.currentNavigationInfo.hasArrived) {
                    com.navdy.hud.app.maps.here.HereNavigationManager.sLogger.v("last maneuver marked arrived tts:" + com.navdy.hud.app.maps.here.HereNavigationManager.this.currentNavigationInfo.lastManeuver);
                    if (!com.navdy.hud.app.maps.here.HereNavigationManager.this.currentNavigationInfo.lastManeuver) {
                        com.navdy.hud.app.maps.here.HereNavigationManager.this.setLastManeuver();
                    }
                }
                if (com.navdy.hud.app.maps.here.HereNavigationManager.this.getNavigationSessionPreference().spokenTurnByTurn && !com.navdy.hud.app.maps.here.HereNavigationManager.this.currentNavigationInfo.hasArrived) {
                    if (com.navdy.hud.app.maps.here.HereNavigationManager.this.currentNavigationInfo.lastManeuver && com.navdy.hud.app.maps.here.HereNavigationManager.this.currentNavigationInfo.destinationDirection != null && com.navdy.hud.app.maps.here.HereNavigationManager.this.currentNavigationInfo.destinationDirection != com.navdy.hud.app.maps.MapEvents.DestinationDirection.UNKNOWN && hasArrivedPattern) {
                        com.navdy.hud.app.maps.here.HereNavigationManager.sLogger.v("Found destination reached tts[" + s + "]");
                        android.content.Context access$Anon400 = com.navdy.hud.app.maps.here.HereNavigationManager.context;
                        java.lang.Object[] objArr = new java.lang.Object[1];
                        objArr[0] = com.navdy.hud.app.maps.here.HereNavigationManager.this.currentNavigationInfo.destinationDirection == com.navdy.hud.app.maps.MapEvents.DestinationDirection.LEFT ? com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.DESTINATION_LEFT : com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.DESTINATION_RIGHT;
                        s = access$Anon400.getString(com.navdy.hud.app.R.string.tbt_audio_destination_reached, objArr);
                        com.navdy.hud.app.maps.here.HereNavigationManager.sLogger.v("Converted destination reached tts[" + s + "]");
                    }
                    com.navdy.hud.app.maps.here.HereNavigationManager.this.bus.post(com.navdy.hud.app.maps.notification.RouteCalculationNotification.CANCEL_TBT_TTS);
                    com.navdy.hud.app.framework.voice.TTSUtils.sendSpeechRequest(s, com.navdy.service.library.events.audio.Category.SPEECH_TURN_BY_TURN, com.navdy.hud.app.maps.notification.RouteCalculationNotification.ROUTE_TBT_TTS_ID);
                }
            }
            return true;
        }

        public boolean playFiles(java.lang.String[] strings) {
            return true;
        }
    }

    class Anon3 implements java.lang.Runnable {
        final /* synthetic */ java.lang.String val$id;
        final /* synthetic */ boolean val$includedTraffic;
        final /* synthetic */ com.here.android.mpa.routing.Route val$newRoute;
        final /* synthetic */ java.lang.String val$oldRouteId;
        final /* synthetic */ com.navdy.service.library.events.navigation.NavigationSessionRouteChange.RerouteReason val$reason;
        final /* synthetic */ com.navdy.service.library.events.navigation.NavigationRouteRequest val$routeRequest;
        final /* synthetic */ java.lang.String val$viaStr;

        Anon3(java.lang.String str, com.here.android.mpa.routing.Route route, java.lang.String str2, boolean z, java.lang.String str3, com.navdy.service.library.events.navigation.NavigationSessionRouteChange.RerouteReason rerouteReason, com.navdy.service.library.events.navigation.NavigationRouteRequest navigationRouteRequest) {
            this.val$viaStr = str;
            this.val$newRoute = route;
            this.val$id = str2;
            this.val$includedTraffic = z;
            this.val$oldRouteId = str3;
            this.val$reason = rerouteReason;
            this.val$routeRequest = navigationRouteRequest;
        }

        public void run() {
            com.navdy.hud.app.maps.here.HereRouteCalculator routeCalculator = new com.navdy.hud.app.maps.here.HereRouteCalculator(com.navdy.hud.app.maps.here.HereNavigationManager.sLogger, com.navdy.hud.app.maps.here.HereNavigationManager.VERBOSE);
            java.lang.String via = this.val$viaStr;
            if (android.text.TextUtils.isEmpty(via)) {
                via = com.navdy.hud.app.maps.here.HereRouteViaGenerator.getViaString(this.val$newRoute);
                if (android.text.TextUtils.isEmpty(via)) {
                    via = "";
                }
            }
            java.lang.String label = null;
            java.lang.String streetAddress = null;
            java.util.List<com.navdy.service.library.events.navigation.NavigationRouteRequest.RouteAttribute> routeAttributes = null;
            com.navdy.service.library.events.navigation.NavigationRouteRequest navRequest = com.navdy.hud.app.maps.here.HereNavigationManager.this.currentNavigationInfo.navigationRouteRequest;
            if (navRequest != null) {
                label = navRequest.label;
                streetAddress = navRequest.streetAddress;
                routeAttributes = navRequest.routeAttributes;
            }
            com.navdy.service.library.events.navigation.NavigationRouteResult result = routeCalculator.getRouteResultFromRoute(this.val$id, this.val$newRoute, via, label, streetAddress, true);
            com.navdy.hud.app.maps.here.HereNavigationManager.this.currentNavigationInfo.routeId = this.val$id;
            com.navdy.hud.app.maps.here.HereNavigationManager.this.currentNavigationInfo.route = this.val$newRoute;
            com.navdy.hud.app.maps.here.HereRouteCache.getInstance().addRoute(this.val$id, new com.navdy.hud.app.maps.here.HereRouteCache.RouteInfo(this.val$newRoute, navRequest, result, this.val$includedTraffic, com.navdy.hud.app.maps.here.HereNavigationManager.hereMapsManager.getLocationFixManager().getLastGeoCoordinate()));
            com.navdy.hud.app.maps.here.HereNavigationManager.sLogger.v("new route added to cache");
            com.navdy.hud.app.maps.here.HereNavigationManager.this.postNavigationSessionStatusEvent(com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_REROUTED, false);
            com.navdy.service.library.events.navigation.NavigationSessionRouteChange routeChange = new com.navdy.service.library.events.navigation.NavigationSessionRouteChange(this.val$oldRouteId, result, this.val$reason);
            if (routeAttributes != null && routeAttributes.contains(com.navdy.service.library.events.navigation.NavigationRouteRequest.RouteAttribute.ROUTE_ATTRIBUTE_GAS)) {
                com.navdy.hud.app.maps.here.HereMapUtil.saveGasRouteInfo(navRequest, com.navdy.hud.app.maps.here.HereNavigationManager.this.currentNavigationInfo.deviceId, com.navdy.hud.app.maps.here.HereNavigationManager.sLogger);
            }
            com.navdy.hud.app.maps.here.HereNavigationManager.this.bus.post(routeChange);
            com.navdy.hud.app.maps.here.HereNavigationManager.this.bus.post(new com.navdy.hud.app.event.RemoteEvent(routeChange));
            com.navdy.hud.app.maps.here.HereNavigationManager.sLogger.v("NavigationSessionRouteChange sent to client oldRoute[" + this.val$oldRouteId + "] newRoute[" + this.val$id + "] reason[" + this.val$reason + "] via[" + via + "]");
            if (com.navdy.hud.app.maps.here.HereNavigationManager.sLogger.isLoggable(2)) {
                com.navdy.hud.app.maps.here.HereMapUtil.printRouteDetails(this.val$newRoute, this.val$routeRequest.streetAddress, this.val$routeRequest, null);
            }
        }
    }

    class Anon4 implements java.lang.Runnable {
        final /* synthetic */ boolean val$sendRouteResult;
        final /* synthetic */ com.navdy.service.library.events.navigation.NavigationSessionState val$sessionState;

        Anon4(boolean z, com.navdy.service.library.events.navigation.NavigationSessionState navigationSessionState) {
            this.val$sendRouteResult = z;
            this.val$sessionState = navigationSessionState;
        }

        public void run() {
            java.lang.String label = null;
            com.navdy.service.library.events.navigation.NavigationRouteResult result = null;
            java.lang.String routeId = null;
            com.navdy.service.library.events.navigation.NavigationRouteRequest navRequest = com.navdy.hud.app.maps.here.HereNavigationManager.this.currentNavigationInfo.navigationRouteRequest;
            if (navRequest != null) {
                label = navRequest.label;
                routeId = com.navdy.hud.app.maps.here.HereNavigationManager.this.currentNavigationInfo.routeId;
                if (this.val$sendRouteResult) {
                    com.navdy.hud.app.maps.here.HereRouteCache.RouteInfo routeInfo = com.navdy.hud.app.maps.here.HereRouteCache.getInstance().getRoute(routeId);
                    if (routeInfo != null) {
                        com.navdy.service.library.events.navigation.NavigationRouteResult result2 = routeInfo.routeResult;
                        int freeFlowDuration = result2.duration.intValue();
                        int trafficDuration = result2.duration_traffic.intValue();
                        java.util.Date noTraffic = com.navdy.hud.app.maps.here.HereNavigationManager.this.hereNavController.getEta(true, com.here.android.mpa.routing.Route.TrafficPenaltyMode.DISABLED);
                        java.util.Date traffic = com.navdy.hud.app.maps.here.HereNavigationManager.this.hereNavController.getEta(true, com.here.android.mpa.routing.Route.TrafficPenaltyMode.OPTIMAL);
                        long now = java.lang.System.currentTimeMillis();
                        if (com.navdy.hud.app.maps.here.HereMapUtil.isValidEtaDate(noTraffic)) {
                            freeFlowDuration = ((int) (noTraffic.getTime() - now)) / 1000;
                        }
                        if (com.navdy.hud.app.maps.here.HereMapUtil.isValidEtaDate(traffic)) {
                            trafficDuration = ((int) (traffic.getTime() - now)) / 1000;
                        }
                        int length = (int) com.navdy.hud.app.maps.here.HereNavigationManager.this.hereNavController.getDestinationDistance();
                        result = new com.navdy.service.library.events.navigation.NavigationRouteResult.Builder(result2).duration(java.lang.Integer.valueOf(freeFlowDuration)).duration_traffic(java.lang.Integer.valueOf(trafficDuration)).length(java.lang.Integer.valueOf(length)).build();
                        com.navdy.hud.app.maps.here.HereNavigationManager.sLogger.v("NavigationSessionStatusEvent new data duration[" + freeFlowDuration + "] traffic[" + trafficDuration + "] length[" + length + "]");
                    }
                }
            }
            if (routeId == null && this.val$sessionState == com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_STOPPED) {
                com.navdy.service.library.events.navigation.NavigationRouteRequest lastRequest = com.navdy.hud.app.maps.here.HereNavigationManager.this.currentNavigationInfo.getLastNavigationRequest();
                if (lastRequest != null) {
                    label = lastRequest.label;
                    routeId = com.navdy.hud.app.maps.here.HereNavigationManager.this.currentNavigationInfo.getLastRouteId();
                }
            }
            com.navdy.service.library.events.navigation.NavigationSessionStatusEvent navigationSessionState = new com.navdy.service.library.events.navigation.NavigationSessionStatusEvent(this.val$sessionState, label, routeId, result, com.navdy.hud.app.maps.here.HereNavigationManager.this.currentNavigationInfo.destinationIdentifier);
            com.navdy.hud.app.maps.here.HereNavigationManager.mapsEventHandler.sendEventToClient(navigationSessionState);
            com.navdy.hud.app.maps.here.HereNavigationManager.sLogger.v("posted NavigationSessionStatusEvent state[" + navigationSessionState.sessionState + "] label[" + navigationSessionState.label + "] routeId[" + navigationSessionState.routeId + "]" + "] result[" + (result == null ? com.amazonaws.services.s3.internal.Constants.NULL_VERSION_ID : result.via) + "]");
        }
    }

    class Anon5 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.service.library.events.navigation.NavigationSessionRequest val$event;

        Anon5(com.navdy.service.library.events.navigation.NavigationSessionRequest navigationSessionRequest) {
            this.val$event = navigationSessionRequest;
        }

        public void run() {
            com.navdy.hud.app.maps.here.HereNavigationManager.this.handleNavigationSessionRequestInternal(this.val$event);
        }
    }

    class Anon6 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.service.library.device.NavdyDeviceId val$deviceId;

        Anon6(com.navdy.service.library.device.NavdyDeviceId navdyDeviceId) {
            this.val$deviceId = navdyDeviceId;
        }

        public void run() {
            com.navdy.hud.app.maps.here.HereNavigationManager.sLogger.i("stop navigation, device id has changed from[" + com.navdy.hud.app.maps.here.HereNavigationManager.this.currentNavigationInfo.deviceId + "] to [" + this.val$deviceId + "]");
            com.navdy.hud.app.maps.here.HereNavigationManager.this.setNavigationMode(com.navdy.hud.app.maps.NavigationMode.MAP, null, null);
        }
    }

    class Anon7 implements java.lang.Runnable {
        final /* synthetic */ boolean val$clearPrevious;
        final /* synthetic */ com.here.android.mpa.routing.Maneuver val$maneuverAfterNext;
        final /* synthetic */ com.here.android.mpa.routing.Maneuver val$nextManeuver;
        final /* synthetic */ com.here.android.mpa.routing.Maneuver val$previous;

        Anon7(com.here.android.mpa.routing.Maneuver maneuver, com.here.android.mpa.routing.Maneuver maneuver2, com.here.android.mpa.routing.Maneuver maneuver3, boolean z) {
            this.val$nextManeuver = maneuver;
            this.val$maneuverAfterNext = maneuver2;
            this.val$previous = maneuver3;
            this.val$clearPrevious = z;
        }

        public void run() {
            try {
                com.navdy.hud.app.maps.here.HereNavigationManager.this.updateNavigationInfoInternal(this.val$nextManeuver, this.val$maneuverAfterNext, this.val$previous, this.val$clearPrevious);
            } catch (Throwable t) {
                com.navdy.hud.app.maps.here.HereNavigationManager.sLogger.e("updateNavigationInfoInternal", t);
            }
        }
    }

    class Anon8 implements java.lang.Runnable {
        Anon8() {
        }

        public void run() {
            com.navdy.hud.app.maps.here.HereNavigationManager.this.setNavigationManagerUnit();
        }
    }

    class Anon9 implements java.lang.Runnable {

        class Anon1 implements java.lang.Runnable {
            final /* synthetic */ com.navdy.service.library.events.navigation.NavigationRouteRequest val$nextRoute;

            Anon1(com.navdy.service.library.events.navigation.NavigationRouteRequest navigationRouteRequest) {
                this.val$nextRoute = navigationRouteRequest;
            }

            public void run() {
                com.navdy.hud.app.maps.here.HereNavigationManager.this.bus.post(this.val$nextRoute);
                com.navdy.hud.app.maps.here.HereNavigationManager.this.bus.post(new com.navdy.hud.app.event.RemoteEvent(this.val$nextRoute));
            }
        }

        Anon9() {
        }

        public void run() {
            synchronized (this) {
                if (com.navdy.hud.app.maps.here.HereNavigationManager.this.isNavigationModeOn()) {
                    com.navdy.hud.app.maps.here.HereNavigationManager.sLogger.v("arrived = true");
                    com.navdy.hud.app.maps.here.HereNavigationManager.this.hereNavController.arrivedAtDestination();
                    com.navdy.hud.app.maps.here.HereMapUtil.removeRouteInfo(com.navdy.hud.app.maps.here.HereNavigationManager.sLogger, false);
                    if (!com.navdy.hud.app.maps.here.HereNavigationManager.this.currentNavigationInfo.navigationRouteRequest.routeAttributes.contains(com.navdy.service.library.events.navigation.NavigationRouteRequest.RouteAttribute.ROUTE_ATTRIBUTE_GAS)) {
                        com.navdy.hud.app.maps.here.HereNavigationManager.sLogger.v("arrived : regular");
                        if (!com.navdy.hud.app.maps.here.HereNavigationManager.this.currentNavigationInfo.hasArrived) {
                            com.navdy.hud.app.maps.here.HereNavigationManager.this.bus.post(new com.navdy.hud.app.maps.MapEvents.ManeuverEvent(com.navdy.hud.app.maps.MapEvents.ManeuverEvent.Type.LAST, null));
                        }
                        com.navdy.hud.app.maps.here.HereNavigationManager.this.currentNavigationInfo.hasArrived = true;
                        com.navdy.hud.app.maps.here.HereNavigationManager.this.currentNavigationInfo.ignoreArrived = false;
                        com.navdy.hud.app.maps.here.HereNavigationManager.this.bus.post(com.navdy.hud.app.maps.here.HereNavigationManager.ARRIVAL_EVENT);
                        com.navdy.hud.app.maps.here.HereNavigationManager.this.postArrivedManeuver();
                        com.navdy.hud.app.maps.here.HereNavigationManager.this.removeCurrentRoute();
                        com.navdy.hud.app.maps.here.HereNavigationManager.this.currentSessionState = com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_STOPPED;
                        com.navdy.hud.app.maps.here.HereNavigationManager.sLogger.v("arrived: session stopped");
                        com.navdy.hud.app.maps.here.HereNavigationManager.this.postNavigationSessionStatusEvent(com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_ARRIVED, false);
                        com.navdy.hud.app.maps.here.HereNavigationManager.this.postNavigationSessionStatusEvent(false);
                        com.navdy.hud.app.maps.here.HereNavigationManager.sLogger.v("arrived: posted arrival maneuver");
                    } else {
                        com.navdy.hud.app.maps.here.HereNavigationManager.sLogger.v("arrived : gas");
                        com.navdy.service.library.events.navigation.NavigationRouteRequest nextRoute = com.navdy.hud.app.maps.here.HereMapUtil.getSavedRouteData(com.navdy.hud.app.maps.here.HereNavigationManager.sLogger).navigationRouteRequest;
                        com.navdy.hud.app.maps.here.HereNavigationManager.this.setNavigationMode(com.navdy.hud.app.maps.NavigationMode.MAP, null, null);
                        if (nextRoute != null) {
                            com.navdy.hud.app.maps.here.HereNavigationManager.sLogger.v("arrived : gas, has next route");
                            com.navdy.hud.app.maps.here.HereNavigationManager.this.handler.post(new com.navdy.hud.app.maps.here.HereNavigationManager.Anon9.Anon1(nextRoute));
                        } else {
                            com.navdy.hud.app.maps.here.HereNavigationManager.sLogger.v("arrived : gas, no next route");
                        }
                    }
                }
            }
        }
    }

    static {
        LANGUAGE_HIERARCHY.put("en-AU", "en-GB");
        LANGUAGE_HIERARCHY.put("en-GB", com.navdy.hud.app.profile.HudLocale.DEFAULT_LANGUAGE);
        LANGUAGE_HIERARCHY.put(TBT_LANGUAGE_CODE, com.navdy.hud.app.profile.HudLocale.DEFAULT_LANGUAGE);
    }

    public static com.navdy.hud.app.maps.here.HereNavigationManager getInstance() {
        return sSingleton;
    }

    private HereNavigationManager() {
        java.lang.String language = java.util.Locale.getDefault().getLanguage();
        android.content.res.Resources resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        this.TTS_REROUTING = new com.navdy.hud.app.event.LocalSpeechRequest(new com.navdy.service.library.events.audio.SpeechRequest.Builder().words(resources.getString(com.navdy.hud.app.R.string.tts_recalculate_route)).category(com.navdy.service.library.events.audio.Category.SPEECH_REROUTE).language(language).build());
        this.TTS_REROUTING_FAILED = new com.navdy.hud.app.event.LocalSpeechRequest(new com.navdy.service.library.events.audio.SpeechRequest.Builder().words(resources.getString(com.navdy.hud.app.R.string.tts_recalculate_route_failed)).category(com.navdy.service.library.events.audio.Category.SPEECH_REROUTE).language(language).build());
        this.TTS_MILES = resources.getString(com.navdy.hud.app.R.string.tts_miles_per_hour);
        this.TTS_KMS = resources.getString(com.navdy.hud.app.R.string.tts_kms_per_hour);
        this.TTS_METERS = resources.getString(com.navdy.hud.app.R.string.tts_meters_per_second);
        this.INVALID_STATE = resources.getString(com.navdy.hud.app.R.string.invalid_state);
        this.NO_CURRENT_POSITION = resources.getString(com.navdy.hud.app.R.string.no_current_position);
        this.INVALID_ROUTE_ID = resources.getString(com.navdy.hud.app.R.string.invalid_routeid);
        this.HERE_TBT_AUDIO_DESTINATION_REACHED_PATTERN = resources.getString(com.navdy.hud.app.R.string.tbt_audio_destination_reached_pattern).toLowerCase();
        this.currentSessionState = com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_ENGINE_NOT_READY;
        this.timeHelper = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getTimeHelper();
        initNavigationManager();
    }

    private void initNavigationManager() {
        this.navigationManager = com.here.android.mpa.guidance.NavigationManager.getInstance();
        this.hereNavController = new com.navdy.hud.app.maps.here.HereNavController(this.navigationManager, this.bus);
        this.mapController = hereMapsManager.getMapController();
        this.navigationManagerEventListener = new com.navdy.hud.app.maps.here.HereNavigationEventListener(sLogger, TAG_NAV_MANAGER_CALLBACK, this);
        this.navigationManager.addNavigationManagerEventListener(new java.lang.ref.WeakReference(this.navigationManagerEventListener));
        this.newManeuverEventListener = new com.navdy.hud.app.maps.here.HereNewManeuverListener(sLogger, TAG_NEW_MANEUVER_CALLBACK, VERBOSE, this.hereNavController, this, this.bus);
        this.navigationManager.addNewInstructionEventListener(new java.lang.ref.WeakReference(this.newManeuverEventListener));
        this.gpsSignalListener = new com.navdy.hud.app.maps.here.HereGpsSignalListener(sLogger, TAG_GPS_CALLBACK, this.bus, mapsEventHandler, this);
        this.navigationManager.addGpsSignalListener(new java.lang.ref.WeakReference(this.gpsSignalListener));
        this.speedWarningManager = new com.navdy.hud.app.maps.here.HereSpeedWarningManager(TAG_SPEED_CALLBACK, this.bus, mapsEventHandler, this);
        this.safetySpotListener = new com.navdy.hud.app.maps.here.HereSafetySpotListener(this.bus, this.mapController);
        this.navigationManager.addSafetySpotListener(new java.lang.ref.WeakReference(this.safetySpotListener));
        this.reRouteListener = new com.navdy.hud.app.maps.here.HereRerouteListener(sLogger, TAG_REROUTE_CALLBACK, this, this.bus);
        this.navigationManager.addRerouteListener(new java.lang.ref.WeakReference(this.reRouteListener));
        if (!com.navdy.hud.app.util.DeviceUtil.isUserBuild() && com.navdy.hud.app.maps.MapSettings.isLaneGuidanceEnabled()) {
            this.laneInfoListener = new com.navdy.hud.app.maps.here.HereLaneInfoListener(this.bus, this.hereNavController);
        }
        this.hereRealisticViewListener = new com.navdy.hud.app.maps.here.HereRealisticViewListener(this.bus, this.hereNavController);
        this.positionUpdateListener = new com.navdy.hud.app.maps.here.HerePositionUpdateListener(TAG_POS_UPDATE_CALLBACK, this.hereNavController, this, this.bus);
        this.navigationManager.addPositionListener(new java.lang.ref.WeakReference(this.positionUpdateListener));
        this.trafficRerouteListener = new com.navdy.hud.app.maps.here.HereTrafficRerouteListener(TAG_TRAFFIC_REROUTE_CALLBACK, true, this.hereNavController, this, this.bus);
        this.navdyTrafficRerouteManager = new com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager(this, this.trafficRerouteListener, this.bus);
        this.trafficRerouteListener.setNavdyTrafficRerouteManager(this.navdyTrafficRerouteManager);
        this.trafficUpdater = new com.navdy.hud.app.maps.here.HereTrafficUpdater2(this.bus);
        this.trafficEtaTracker = new com.navdy.hud.app.maps.here.HereTrafficETATracker(this.hereNavController, this.bus);
        this.navigationManager.addNavigationManagerEventListener(new java.lang.ref.WeakReference(this.trafficEtaTracker));
        setMapUpdateMode();
        setupNavigationTTS();
        setNavigationManagerUnit();
        this.driverSessionPreferences = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getDriverProfileManager().getSessionPreferences();
        if (this.bandwidthController.isLimitBandwidthModeOn()) {
            sLogger.v("limitbandwidth: initial on");
            setBandwidthPreferences();
        } else {
            setTrafficRerouteMode(mapsEventHandler.getNavigationPreferences().rerouteForTraffic);
        }
        setGeneratePhoneticTTS(java.lang.Boolean.TRUE.equals(mapsEventHandler.getNavigationPreferences().phoneticTurnByTurn));
        this.bus.register(this);
        sLogger.v("start speed warning manager");
        this.speedWarningManager.start();
        if (!setNavigationMode(com.navdy.hud.app.maps.NavigationMode.MAP, null, null)) {
            sLogger.e("navigation mode cannot be switched to map");
        }
    }

    public void setMapView(com.here.android.mpa.mapping.MapView mapView2, com.navdy.hud.app.ui.component.homescreen.NavigationView navigationView2) {
        this.mapView = mapView2;
        this.navigationView = navigationView2;
    }

    @com.squareup.otto.Subscribe
    public void onNavigationPreferences(com.navdy.service.library.events.preferences.NavigationPreferences preferences) {
        setGeneratePhoneticTTS(java.lang.Boolean.TRUE.equals(preferences.phoneticTurnByTurn));
    }

    @com.squareup.otto.Subscribe
    public void onNotificationPreference(com.navdy.service.library.events.preferences.NotificationPreferences preferences) {
        if (!preferences.enabled.booleanValue()) {
            sLogger.v("glances, disabled, turn traffic notif off");
            resetTrafficRerouteListener();
            stopTrafficRerouteListener();
        } else if (com.navdy.hud.app.framework.glance.GlanceHelper.isTrafficNotificationEnabled()) {
            sLogger.v("glances, enabled, turn traffic notif on if not on");
            startTrafficRerouteListener();
        } else {
            sLogger.v("traffic glances disabled, turn traffic notif off");
            resetTrafficRerouteListener();
            stopTrafficRerouteListener();
        }
    }

    @com.squareup.otto.Subscribe
    public void onTrafficRerouteConfirm(com.navdy.hud.app.maps.MapEvents.TrafficRerouteAction action) {
        if (action == com.navdy.hud.app.maps.MapEvents.TrafficRerouteAction.REROUTE) {
            this.trafficRerouteListener.confirmReroute();
        } else {
            this.trafficRerouteListener.dismissReroute();
        }
    }

    private void tearDownCurrentRoute() {
        removeCurrentRoute();
        removeDestinationMarker();
        this.currentNavigationInfo.mapDestinationMarker = null;
        this.trafficUpdater.setRoute(null);
        this.trafficRerouteListener.dismissReroute();
        this.bus.post(new com.navdy.hud.app.maps.MapEvents.TrafficRerouteDismissEvent());
        this.bus.post(new com.navdy.hud.app.maps.MapEvents.LiveTrafficDismissEvent());
    }

    public synchronized boolean removeCurrentRoute() {
        boolean z;
        try {
            com.here.android.mpa.mapping.MapRoute current = this.currentNavigationInfo.mapRoute;
            if (current != null) {
                sLogger.v("removed map route:" + current);
                this.currentNavigationInfo.mapRoute = null;
                this.mapController.removeMapObject(current);
                z = true;
            }
        } catch (Throwable t) {
            sLogger.e(t);
        }
        z = false;
        return z;
    }

    public synchronized void addCurrentRoute(com.here.android.mpa.mapping.MapRoute mapRoute) {
        try {
            if (this.currentNavigationInfo.mapRoute != null) {
                removeCurrentRoute();
            }
            if (mapRoute != null) {
                this.currentNavigationInfo.mapRoute = mapRoute;
                this.mapController.addMapObject(mapRoute);
                sLogger.v("addCurrentRoute:" + mapRoute);
            }
        } catch (Throwable t) {
            sLogger.e(t);
        }
        return;
    }

    private com.here.android.mpa.guidance.VoiceSkin findSkinSupporting(java.util.List<com.here.android.mpa.guidance.VoiceSkin> skins, java.lang.String languageCode) {
        if (skins == null) {
            return null;
        }
        if (languageCode == null) {
            return null;
        }
        java.lang.String desiredLanguage = languageCode.toLowerCase(java.util.Locale.US);
        for (com.here.android.mpa.guidance.VoiceSkin skin : skins) {
            if (skin.getLanguageCode().toLowerCase(java.util.Locale.US).startsWith(desiredLanguage)) {
                return skin;
            }
        }
        return null;
    }

    private void dumpVoiceSkinInfo(java.util.List<com.here.android.mpa.guidance.VoiceSkin> skins) {
        java.lang.StringBuffer buffer = new java.lang.StringBuffer("Supported voice skins: ");
        for (com.here.android.mpa.guidance.VoiceSkin skin : skins) {
            buffer.append("[language:");
            buffer.append(skin.getLanguage());
            buffer.append(",code:");
            buffer.append(skin.getLanguageCode());
            buffer.append("],");
        }
        sLogger.i(buffer.toString());
    }

    private void setupNavigationTTS() {
        java.util.Locale currentLocale = com.navdy.hud.app.profile.HudLocale.getCurrentLocale(com.navdy.hud.app.HudApplication.getAppContext());
        java.lang.String regionalLanguage = currentLocale.toLanguageTag();
        java.lang.String baseLanguage = com.navdy.hud.app.profile.HudLocale.getBaseLanguage(regionalLanguage);
        java.util.List<com.here.android.mpa.guidance.VoiceSkin> localSkins = com.here.android.mpa.guidance.VoiceCatalog.getInstance().getLocalVoiceSkins();
        com.here.android.mpa.guidance.VoiceSkin selectedSkin = null;
        if (localSkins != null) {
            while (regionalLanguage != null && selectedSkin == null) {
                selectedSkin = findSkinSupporting(localSkins, regionalLanguage);
                regionalLanguage = (java.lang.String) LANGUAGE_HIERARCHY.get(regionalLanguage);
            }
            if (selectedSkin == null) {
                selectedSkin = findSkinSupporting(localSkins, baseLanguage);
            }
            if (selectedSkin == null) {
                selectedSkin = findSkinSupporting(localSkins, TBT_LANGUAGE_CODE);
            }
        }
        if (selectedSkin == null) {
            sLogger.e("No voice skin found for default locale:" + com.navdy.hud.app.maps.here.HereMapUtil.TBT_ISO3_LANG_CODE);
            return;
        }
        sLogger.d("Found voice skin lang[" + selectedSkin.getLanguage() + " code[" + selectedSkin.getLanguageCode() + "] for locale:" + currentLocale);
        this.navigationManager.setVoiceSkin(selectedSkin);
        hereMapsManager.setVoiceSkinsLoaded();
        this.audioPlayerDelegate = new com.navdy.hud.app.maps.here.HereNavigationManager.Anon2();
        this.navigationManager.getAudioPlayer().setDelegate(this.audioPlayerDelegate);
    }

    public void updateMapRoute(com.here.android.mpa.routing.Route newRoute, com.navdy.service.library.events.navigation.NavigationSessionRouteChange.RerouteReason reason, java.lang.String routeId, java.lang.String via, boolean sendStartManeuver, boolean includedTraffic) {
        updateMapRoute(newRoute, reason, this.currentNavigationInfo.navigationRouteRequest, routeId, via, sendStartManeuver, includedTraffic);
    }

    public synchronized void updateMapRoute(com.here.android.mpa.routing.Route newRoute, com.navdy.service.library.events.navigation.NavigationSessionRouteChange.RerouteReason reason, com.navdy.service.library.events.navigation.NavigationRouteRequest navigationRouteRequest, java.lang.String routeId, java.lang.String viaStr, boolean sendStartManeuver, boolean includedTraffic) {
        boolean updateDisplay = false;
        if (!(this.currentNavigationInfo.mapRoute == null && this.currentNavigationInfo.mapDestinationMarker == null)) {
            tearDownCurrentRoute();
            this.currentNavigationInfo.destinationDirection = null;
            this.currentNavigationInfo.destinationDirectionStr = null;
            this.currentNavigationInfo.destinationIconId = -1;
            if (newRoute == null || reason == null) {
                updateDisplay = true;
                this.currentNavigationInfo.hasArrived = false;
                this.currentNavigationInfo.ignoreArrived = false;
                this.currentNavigationInfo.lastManeuver = false;
                this.currentNavigationInfo.arrivedManeuverDisplay = null;
            }
        }
        if (newRoute != null) {
            com.here.android.mpa.mapping.MapRoute mapRoute = new com.here.android.mpa.mapping.MapRoute(newRoute);
            mapRoute.setTrafficEnabled(true);
            addCurrentRoute(mapRoute);
            this.currentNavigationInfo.mapRoute.setTrafficEnabled(true);
            this.bus.post(new com.navdy.hud.app.maps.MapEvents.NewRouteAdded(reason));
            this.newManeuverEventListener.setNewRoute();
            if (sendStartManeuver) {
                sendStartManeuver(newRoute);
                sLogger.v("updateMapRoute:sending start maneuver");
            }
            sLogger.v("map route added:" + this.currentNavigationInfo.mapRoute);
            removeDestinationMarker();
            this.currentNavigationInfo.mapDestinationMarker = null;
            this.currentNavigationInfo.currentRerouteReason = reason;
            addDestinationMarker(newRoute.getDestination(), com.navdy.hud.app.R.drawable.icon_pin_dot_destination);
            this.trafficUpdater.setRoute(this.currentNavigationInfo.mapRoute.getRoute());
            if (reason != null) {
                java.lang.String id = routeId != null ? routeId : java.util.UUID.randomUUID().toString();
                java.lang.String oldRouteId = this.currentNavigationInfo.routeId;
                com.navdy.service.library.events.navigation.NavigationRouteRequest routeRequest = this.currentNavigationInfo.navigationRouteRequest;
                if (reason == com.navdy.service.library.events.navigation.NavigationSessionRouteChange.RerouteReason.NAV_SESSION_FUEL_REROUTE) {
                    this.currentNavigationInfo.navigationRouteRequest = navigationRouteRequest;
                    this.currentNavigationInfo.routeOptions = hereMapsManager.getRouteOptions();
                    this.currentNavigationInfo.streetAddress = com.navdy.hud.app.maps.here.HereMapUtil.parseStreetAddress(navigationRouteRequest.streetAddress);
                    this.currentNavigationInfo.destinationLabel = null;
                } else if (reason == com.navdy.service.library.events.navigation.NavigationSessionRouteChange.RerouteReason.NAV_SESSION_TRAFFIC_REROUTE) {
                    this.currentNavigationInfo.trafficRerouteOnce = true;
                }
                com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.maps.here.HereNavigationManager.Anon3(viaStr, newRoute, id, includedTraffic, oldRouteId, reason, routeRequest), 2);
            }
        } else {
            hereMapsManager.getLocationFixManager().addMarkers(this.mapController);
        }
        if (updateDisplay) {
            refreshNavigationInfo();
        }
    }

    public synchronized void setReroute(com.here.android.mpa.routing.Route route, com.navdy.service.library.events.navigation.NavigationSessionRouteChange.RerouteReason rerouteReason, java.lang.String routeId, java.lang.String via, boolean sendStartManeuver, boolean trafficConsidered) {
        sLogger.v("setReroute:" + sendStartManeuver);
        if (!isNavigationModeOn()) {
            sLogger.v("setReroute: nav mode ended");
        } else {
            clearNavManeuver();
            com.here.android.mpa.guidance.NavigationManager.Error error = this.hereNavController.setRoute(route);
            if (error == com.here.android.mpa.guidance.NavigationManager.Error.NONE) {
                updateMapRoute(route, rerouteReason, routeId, via, sendStartManeuver, trafficConsidered);
                sLogger.i("setReroute: done");
            } else {
                sLogger.e("setReroute: route could not be set:" + error);
            }
        }
    }

    public void postNavigationSessionStatusEvent(boolean sendRouteResult) {
        postNavigationSessionStatusEvent(this.currentSessionState, sendRouteResult);
    }

    public void postNavigationSessionStatusEvent(com.navdy.service.library.events.navigation.NavigationSessionState sessionState, boolean sendRouteResult) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.maps.here.HereNavigationManager.Anon4(sendRouteResult, sessionState), 2);
    }

    /* access modifiers changed from: protected */
    public void returnResponse(com.navdy.service.library.events.RequestStatus status, java.lang.String errorText, com.navdy.service.library.events.navigation.NavigationSessionState pendingState, java.lang.String routeId) {
        if (status != com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS) {
            sLogger.e(status + ": " + errorText);
        } else {
            sLogger.i(com.navdy.hud.app.device.dial.DialConstants.DIAL_PAIRING_SUCCESS);
        }
        mapsEventHandler.sendEventToClient(new com.navdy.service.library.events.navigation.NavigationSessionResponse(status, errorText, pendingState, routeId));
    }

    public void handleNavigationSessionRequest(com.navdy.service.library.events.navigation.NavigationSessionRequest event) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.maps.here.HereNavigationManager.Anon5(event), 20);
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x02ad, code lost:
        r11 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:?, code lost:
        r17.switchingToNewRoute = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x02b3, code lost:
        throw r11;
     */
    public synchronized void handleNavigationSessionRequestInternal(com.navdy.service.library.events.navigation.NavigationSessionRequest event) {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        try {
            sLogger.v("NavigationSessionRequest id[" + event.routeId + "] newState[" + event.newState + "] label[" + event.label + "] sim-speed[" + event.simulationSpeed + "] currentState[" + this.currentSessionState + "]");
            if (com.navdy.hud.app.maps.here.HereMapUtil.isValidNavigationState(event.newState)) {
                if (hereMapsManager.getLocationFixManager().getLastGeoCoordinate() != null) {
                    com.navdy.hud.app.maps.here.HereRouteCache.RouteInfo routeInfo = null;
                    boolean routeIdProvided = false;
                    if (event.routeId != null) {
                        routeIdProvided = true;
                        routeInfo = com.navdy.hud.app.maps.here.HereRouteCache.getInstance().getRoute(event.routeId);
                    }
                    if (!isNavigationModeOn()) {
                        sLogger.v("navigation is not active");
                        switch (event.newState) {
                            case NAV_SESSION_PAUSED:
                                sLogger.e("cannot pause navigation current state:" + this.currentSessionState);
                                returnResponse(com.navdy.service.library.events.RequestStatus.REQUEST_INVALID_STATE, context.getString(com.navdy.hud.app.R.string.cannot_move_state, new java.lang.Object[]{this.currentSessionState.name(), event.newState.name()}), null, event.routeId);
                                break;
                            case NAV_SESSION_STOPPED:
                                sLogger.v("already stopped");
                                returnResponse(com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS, "", com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_STOPPED, null);
                                break;
                            case NAV_SESSION_STARTED:
                                if (routeInfo != null) {
                                    long l1 = android.os.SystemClock.elapsedRealtime();
                                    startNavigation(event, routeInfo);
                                    sLogger.v("navigation took[" + (android.os.SystemClock.elapsedRealtime() - l1) + "]");
                                    break;
                                } else {
                                    sLogger.e("valid route id required");
                                    returnResponse(com.navdy.service.library.events.RequestStatus.REQUEST_INVALID_REQUEST, this.INVALID_ROUTE_ID, null, event.routeId);
                                    break;
                                }
                        }
                    } else {
                        sLogger.v("navigation is active");
                        switch (event.newState) {
                            case NAV_SESSION_PAUSED:
                                if (this.currentSessionState != com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_STARTED) {
                                    sLogger.v("already paused");
                                    returnResponse(com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS, "", com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_PAUSED, null);
                                    break;
                                } else {
                                    sLogger.v("pausing navigation");
                                    this.hereNavController.pause();
                                    this.currentSessionState = com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_PAUSED;
                                    returnResponse(com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS, "", com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_PAUSED, event.routeId);
                                    postNavigationSessionStatusEvent(false);
                                    break;
                                }
                            case NAV_SESSION_STOPPED:
                                sLogger.v("stopping navigation");
                                java.lang.StringBuilder error = new java.lang.StringBuilder();
                                if (!setNavigationMode(com.navdy.hud.app.maps.NavigationMode.MAP, null, error)) {
                                    returnResponse(com.navdy.service.library.events.RequestStatus.REQUEST_SERVICE_ERROR, error.toString(), null, event.routeId);
                                    break;
                                } else {
                                    this.navigationView.resetMapOverview();
                                    returnResponse(com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS, "", this.currentSessionState, event.routeId);
                                    break;
                                }
                            case NAV_SESSION_STARTED:
                                if (routeInfo == null && routeIdProvided) {
                                    returnResponse(com.navdy.service.library.events.RequestStatus.REQUEST_INVALID_REQUEST, this.INVALID_ROUTE_ID, null, event.routeId);
                                    break;
                                } else {
                                    if (!(event.routeId == null || android.text.TextUtils.equals(event.routeId, this.currentNavigationInfo.routeId))) {
                                        sLogger.v("different route: switching to new route");
                                        java.lang.StringBuilder error2 = new java.lang.StringBuilder();
                                        sLogger.v("stop navigation");
                                        long l12 = android.os.SystemClock.elapsedRealtime();
                                        this.switchingToNewRoute = true;
                                        if (setNavigationMode(com.navdy.hud.app.maps.NavigationMode.MAP, null, error2)) {
                                            sLogger.v("start navigation");
                                            startNavigation(event, routeInfo);
                                            sLogger.v("navigation took[" + (android.os.SystemClock.elapsedRealtime() - l12) + "]");
                                        } else {
                                            returnResponse(com.navdy.service.library.events.RequestStatus.REQUEST_SERVICE_ERROR, error2.toString(), null, event.routeId);
                                        }
                                        this.switchingToNewRoute = false;
                                        break;
                                    } else {
                                        sLogger.v("same route");
                                        if (this.currentSessionState != com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_STARTED) {
                                            sLogger.v("resume navigation");
                                            com.here.android.mpa.guidance.NavigationManager.Error error3 = this.hereNavController.resume();
                                            if (error3 != com.here.android.mpa.guidance.NavigationManager.Error.NONE) {
                                                sLogger.e("cannot resume navigation:" + error3);
                                                returnResponse(com.navdy.service.library.events.RequestStatus.REQUEST_SERVICE_ERROR, context.getString(com.navdy.hud.app.R.string.unable_to_resume, new java.lang.Object[]{error3}), com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_STARTED, event.routeId);
                                                if (!setNavigationMode(com.navdy.hud.app.maps.NavigationMode.MAP, null, null)) {
                                                    sLogger.w("cannot even go to tracking mode");
                                                    break;
                                                }
                                            } else {
                                                this.currentSessionState = com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_STARTED;
                                                returnResponse(com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS, "", com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_STARTED, event.routeId);
                                                postNavigationSessionStatusEvent(false);
                                                break;
                                            }
                                        } else {
                                            returnResponse(com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS, "", com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_STARTED, event.routeId);
                                            break;
                                        }
                                    }
                                }
                                break;
                        }
                    }
                } else {
                    returnResponse(com.navdy.service.library.events.RequestStatus.REQUEST_NO_LOCATION_SERVICE, this.NO_CURRENT_POSITION, null, event.routeId);
                }
            } else {
                returnResponse(com.navdy.service.library.events.RequestStatus.REQUEST_INVALID_REQUEST, this.INVALID_STATE, null, event.routeId);
            }
        } catch (Throwable t) {
            sLogger.e("handleNavigationSessionRequest", t);
            returnResponse(com.navdy.service.library.events.RequestStatus.REQUEST_SERVICE_ERROR, t.toString(), null, event.routeId);
        }
        return;
    }

    private void startNavigation(com.navdy.service.library.events.navigation.NavigationSessionRequest event, com.navdy.hud.app.maps.here.HereRouteCache.RouteInfo routeInfo) {
        com.navdy.hud.app.maps.here.HereNavigationInfo navigationInfo = new com.navdy.hud.app.maps.here.HereNavigationInfo();
        navigationInfo.navigationRouteRequest = routeInfo.routeRequest;
        navigationInfo.startLocation = routeInfo.routeStartPoint;
        navigationInfo.routeOptions = hereMapsManager.getRouteOptions();
        navigationInfo.waypoints = new java.util.ArrayList();
        for (com.navdy.service.library.events.location.Coordinate waypoint : routeInfo.routeRequest.waypoints) {
            navigationInfo.waypoints.add(new com.here.android.mpa.common.GeoCoordinate(waypoint.latitude.doubleValue(), waypoint.longitude.doubleValue()));
        }
        navigationInfo.destination = new com.here.android.mpa.common.GeoCoordinate(routeInfo.routeRequest.destination.latitude.doubleValue(), routeInfo.routeRequest.destination.longitude.doubleValue());
        navigationInfo.route = routeInfo.route;
        navigationInfo.simulationSpeed = event.simulationSpeed.intValue();
        navigationInfo.routeId = event.routeId;
        navigationInfo.deviceId = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getDeviceId();
        com.navdy.service.library.device.NavdyDeviceId lastKnownDeviceId = com.navdy.hud.app.maps.here.HereMapUtil.getLastKnownDeviceId();
        if (navigationInfo.deviceId == null) {
            navigationInfo.deviceId = lastKnownDeviceId;
            sLogger.v("lastknown device id = " + navigationInfo.deviceId);
        }
        if (navigationInfo.deviceId == null) {
            com.navdy.service.library.events.DeviceInfo deviceInfo = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getConnectionHandler().getLastConnectedDeviceInfo();
            if (deviceInfo != null) {
                navigationInfo.deviceId = new com.navdy.service.library.device.NavdyDeviceId(deviceInfo.deviceId);
                sLogger.v("device id is null , adding last device:" + deviceInfo.deviceId);
            }
        }
        if (routeInfo.routeRequest.destination_identifier != null) {
            navigationInfo.destinationIdentifier = routeInfo.routeRequest.destination_identifier;
        }
        java.lang.String streetAddress = null;
        com.navdy.service.library.events.navigation.NavigationRouteRequest navRequest = navigationInfo.navigationRouteRequest;
        if (navRequest != null) {
            streetAddress = navRequest.streetAddress;
            navigationInfo.destinationLabel = navRequest.label;
        }
        navigationInfo.streetAddress = com.navdy.hud.app.maps.here.HereMapUtil.parseStreetAddress(streetAddress);
        java.lang.StringBuilder error = new java.lang.StringBuilder();
        if (setNavigationMode(com.navdy.hud.app.maps.NavigationMode.MAP_ON_ROUTE, navigationInfo, error)) {
            returnResponse(com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS, null, this.currentSessionState, event.routeId);
            return;
        }
        returnResponse(com.navdy.service.library.events.RequestStatus.REQUEST_SERVICE_ERROR, error.toString(), this.currentSessionState, event.routeId);
    }

    public void handleConnectionState(boolean connected) {
        if (!connected) {
            sLogger.i("client not connected");
            return;
        }
        sLogger.i("client connected");
        if (isNavigationModeOn()) {
            com.navdy.service.library.device.NavdyDeviceId deviceId = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getDeviceId();
            if (deviceId == null || deviceId.equals(this.currentNavigationInfo.deviceId)) {
                sLogger.i("navigation still on, device id has not changed");
            } else {
                com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.maps.here.HereNavigationManager.Anon6(deviceId), 20);
            }
        }
    }

    private static com.here.android.mpa.guidance.NavigationManager.MapUpdateMode getHereMapUpdateMode() {
        return com.here.android.mpa.guidance.NavigationManager.MapUpdateMode.NONE;
    }

    public com.navdy.hud.app.maps.NavigationMode getCurrentNavigationMode() {
        return this.currentNavigationMode;
    }

    public com.navdy.service.library.events.navigation.NavigationSessionState getCurrentNavigationState() {
        return this.currentSessionState;
    }

    public boolean isNavigationModeOn() {
        if (this.hereNavController.getState() == com.navdy.hud.app.maps.here.HereNavController.State.NAVIGATING) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: 0000 */
    public synchronized boolean setNavigationMode(com.navdy.hud.app.maps.NavigationMode navigationMode, com.navdy.hud.app.maps.here.HereNavigationInfo navigationInfo, java.lang.StringBuilder error) {
        boolean z;
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        sLogger.i("setNavigationMode:" + navigationMode);
        if (this.currentNavigationMode == navigationMode) {
            sLogger.i("setNavigationMode: already in " + navigationMode);
            z = true;
        } else {
            if (navigationInfo == null) {
                if (navigationMode != com.navdy.hud.app.maps.NavigationMode.MAP) {
                    sLogger.e("setNavigationMode: navigation info cannot be null");
                    if (error != null) {
                        error.append(context.getString(com.navdy.hud.app.R.string.invalid_param));
                    }
                    z = false;
                }
            }
            com.navdy.service.library.events.navigation.NavigationSessionState prevState = this.currentSessionState;
            boolean changeNavigation = true;
            boolean sendStartManeuver = false;
            if (this.currentNavigationMode != null) {
                if ((this.currentNavigationMode == com.navdy.hud.app.maps.NavigationMode.MAP_ON_ROUTE && navigationMode == com.navdy.hud.app.maps.NavigationMode.TBT_ON_ROUTE) || (this.currentNavigationMode == com.navdy.hud.app.maps.NavigationMode.TBT_ON_ROUTE && navigationMode == com.navdy.hud.app.maps.NavigationMode.MAP_ON_ROUTE)) {
                    sLogger.v("navigation switch not reqd from= " + this.currentNavigationMode + " to=" + navigationMode);
                    this.currentNavigationMode = navigationMode;
                    changeNavigation = false;
                } else {
                    boolean isNavOn = isNavigationModeOn();
                    sLogger.v("remove route before calling nav:stop navon=" + isNavOn);
                    if (isNavOn) {
                        sLogger.v("navigation stopped from:" + this.currentSessionState);
                        updateMapRoute(null, null, null, null, false, false);
                        com.navdy.hud.app.maps.here.HereMapUtil.removeRouteInfo(sLogger, false);
                    }
                    this.hereNavController.stopNavigation(true);
                    this.currentSessionState = com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_STOPPED;
                    if (prevState != com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_STOPPED) {
                        postNavigationSessionStatusEvent(false);
                    }
                    prevState = this.currentSessionState;
                }
            }
            this.handler.removeCallbacks(this.etaCalcRunnable);
            this.currentNavigationInfo.clear();
            this.debugManeuverState = null;
            if (changeNavigation) {
                switch (navigationMode) {
                    case MAP:
                        this.hereNavController.stopNavigation(false);
                        this.bus.post(new com.navdy.hud.app.maps.MapEvents.GPSSpeedEvent());
                        if (!com.navdy.hud.app.util.DeviceUtil.isUserBuild() && com.navdy.hud.app.maps.MapSettings.isLaneGuidanceEnabled()) {
                            this.laneInfoListener.stop();
                        }
                        this.hereRealisticViewListener.stop();
                        this.currentNavigationInfo.copy(navigationInfo);
                        this.currentNavigationMode = com.navdy.hud.app.maps.NavigationMode.MAP;
                        this.currentSessionState = com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_STOPPED;
                        this.mapController.setMapScheme(hereMapsManager.getTrackingMapScheme());
                        break;
                    case MAP_ON_ROUTE:
                    case TBT_ON_ROUTE:
                        updateMapRoute(navigationInfo.route, null, null, null, false, isTrafficConsidered(navigationInfo.routeId));
                        com.here.android.mpa.guidance.NavigationManager.Error navError = this.hereNavController.startNavigation(navigationInfo.route, navigationInfo.simulationSpeed);
                        if (navError != com.here.android.mpa.guidance.NavigationManager.Error.NONE) {
                            updateMapRoute(null, null, null, null, false, false);
                            this.currentSessionState = com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_ERROR;
                            sLogger.e("cannot switch navigation mode from:" + this.currentNavigationMode + " to=" + navigationMode + " error=" + navError + " simulating=" + (navigationInfo.simulationSpeed > 0 ? "true" : "false"));
                            postNavigationSessionStatusEvent(false);
                            if (error != null) {
                                error.append(navError.name());
                            }
                            z = false;
                            break;
                        } else {
                            sLogger.v("switched navigation mode from:" + this.currentNavigationMode + " to=" + navigationMode + " simulating=" + (navigationInfo.simulationSpeed > 0 ? "true" : "false"));
                            this.currentNavigationInfo.copy(navigationInfo);
                            this.currentNavigationMode = navigationMode;
                            this.currentSessionState = com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_STARTED;
                            if (!com.navdy.hud.app.util.DeviceUtil.isUserBuild() && com.navdy.hud.app.maps.MapSettings.isLaneGuidanceEnabled()) {
                                this.laneInfoListener.start();
                            }
                            this.hereRealisticViewListener.start();
                            this.mapController.setMapScheme(hereMapsManager.getEnrouteMapScheme());
                            sendStartManeuver = true;
                            this.handler.postDelayed(this.etaCalcRunnable, 30000);
                            if (!this.currentNavigationInfo.navigationRouteRequest.routeAttributes.contains(com.navdy.service.library.events.navigation.NavigationRouteRequest.RouteAttribute.ROUTE_ATTRIBUTE_GAS)) {
                                com.navdy.hud.app.maps.here.HereMapUtil.saveRouteInfo(navigationInfo.navigationRouteRequest, navigationInfo.deviceId, sLogger);
                                break;
                            } else {
                                com.navdy.hud.app.maps.here.HereMapUtil.saveGasRouteInfo(navigationInfo.navigationRouteRequest, navigationInfo.deviceId, sLogger);
                                break;
                            }
                        }
                }
            }
            setTrafficOverlay();
            this.bus.post(new com.navdy.hud.app.maps.MapEvents.NavigationModeChange(this.currentNavigationMode));
            if (sendStartManeuver) {
                sLogger.v("send start maneuver");
                sendStartManeuver(this.currentNavigationInfo.route);
            }
            if (prevState != this.currentSessionState) {
                postNavigationSessionStatusEvent(false);
            }
            z = true;
        }
        return z;
    }

    /* access modifiers changed from: 0000 */
    public void refreshNavigationInfo() {
        updateNavigationInfo(null, null, null, false);
    }

    /* access modifiers changed from: 0000 */
    public void updateNavigationInfo(com.here.android.mpa.routing.Maneuver nextManeuver, com.here.android.mpa.routing.Maneuver maneuverAfterNext, com.here.android.mpa.routing.Maneuver previous, boolean clearPrevious) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.maps.here.HereNavigationManager.Anon7(nextManeuver, maneuverAfterNext, previous, clearPrevious), 4);
    }

    /* access modifiers changed from: private */
    public synchronized void updateNavigationInfoInternal(com.here.android.mpa.routing.Maneuver nextManeuver, com.here.android.mpa.routing.Maneuver maneuverAfterNext, com.here.android.mpa.routing.Maneuver previous, boolean clearPrevious) {
        com.navdy.service.library.events.navigation.NavigationRouteRequest navigationRouteRequest;
        boolean calculateEarlyManeuver = true;
        if (this.currentNavigationInfo.hasArrived) {
            if (this.currentNavigationInfo.ignoreArrived) {
                sLogger.v("ignore arrived");
            } else {
                try {
                    com.here.android.mpa.common.GeoPosition currentPos = hereMapsManager.getLastGeoPosition();
                    if (currentPos != null) {
                        int distance = (int) currentPos.getCoordinate().distanceTo(this.currentNavigationInfo.destination);
                        if (distance >= ARRIVAL_GEO_FENCE_THRESHOLD) {
                            sLogger.v("we have exceeded geo threshold:" + distance + " , stopping nav");
                            stopNavigation();
                        }
                    }
                } catch (Throwable t) {
                    sLogger.e(t);
                }
                postArrivedManeuver();
            }
        } else if (this.currentSessionState == com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_STARTED || this.currentSessionState == com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_PAUSED) {
            if (nextManeuver == null) {
                nextManeuver = this.currentManeuver;
                com.here.android.mpa.routing.Maneuver maneuverAfterNext2 = this.maneuverAfterCurrent;
                com.here.android.mpa.routing.Maneuver previous2 = this.prevManeuver;
                if (!(this.maneuverAfterCurrent == null || this.currentNavigationInfo.maneuverAfterCurrent != this.maneuverAfterCurrent || this.currentNavigationInfo.maneuverState == null || this.currentNavigationInfo.maneuverState == com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.ManeuverState.STAY)) {
                    calculateEarlyManeuver = false;
                }
            } else {
                if (clearPrevious) {
                    sLogger.v("setNewRoute: cleared");
                    this.prevManeuver = null;
                } else {
                    this.prevManeuver = this.currentManeuver;
                }
                this.currentManeuver = nextManeuver;
                this.maneuverAfterCurrent = maneuverAfterNext;
                this.currentNavigationInfo.maneuverAfterCurrent = maneuverAfterNext;
                this.currentNavigationInfo.maneuverAfterCurrentIconid = -1;
                this.currentNavigationInfo.maneuverState = null;
            }
            if (nextManeuver != null) {
                boolean last = false;
                if (nextManeuver.getAction() == com.here.android.mpa.routing.Maneuver.Action.END || nextManeuver.getIcon() == com.here.android.mpa.routing.Maneuver.Icon.END) {
                    last = true;
                }
                boolean hasDirectionInfo = false;
                boolean calculateDirection = false;
                if (last) {
                    if (this.currentNavigationInfo.destinationDirection != null) {
                        hasDirectionInfo = true;
                    } else {
                        calculateDirection = true;
                    }
                }
                long distance2 = this.hereNavController.getNextManeuverDistance();
                com.here.android.mpa.routing.Maneuver maneuver = this.currentManeuver;
                com.here.android.mpa.routing.Maneuver maneuver2 = this.prevManeuver;
                if (calculateDirection) {
                    navigationRouteRequest = this.currentNavigationInfo.navigationRouteRequest;
                } else {
                    navigationRouteRequest = null;
                }
                com.navdy.hud.app.maps.MapEvents.ManeuverDisplay maneuverDisplay = com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.getManeuverDisplay(maneuver, last, distance2, null, maneuver2, navigationRouteRequest, this.maneuverAfterCurrent, calculateEarlyManeuver, true, this.currentNavigationInfo.destinationDirection);
                if (!last) {
                    if (this.debugManeuverState != null) {
                        maneuverDisplay.maneuverState = this.debugManeuverState;
                    }
                    if (this.debugManeuverInstruction != null) {
                        maneuverDisplay.pendingRoad = this.debugManeuverInstruction;
                    }
                    if (this.debugManeuverIcon != 0) {
                        maneuverDisplay.turnIconId = this.debugManeuverIcon;
                    }
                    if (this.debugManeuverDistance != null) {
                        maneuverDisplay.distanceToPendingRoadText = this.debugManeuverDistance;
                    }
                    if (this.debugNextManeuverIcon != 0) {
                        maneuverDisplay.nextTurnIconId = this.debugNextManeuverIcon;
                    }
                }
                if (last && !this.currentNavigationInfo.lastManeuver) {
                    sLogger.v("last maneuver marked distance:" + distance2);
                    setLastManeuver();
                }
                if (last) {
                    if (!hasDirectionInfo) {
                        this.currentNavigationInfo.destinationDirection = maneuverDisplay.direction;
                        this.currentNavigationInfo.destinationDirectionStr = maneuverDisplay.pendingRoad;
                        this.currentNavigationInfo.destinationIconId = maneuverDisplay.destinationIconId;
                        if (this.currentNavigationInfo.destinationDirection != com.navdy.hud.app.maps.MapEvents.DestinationDirection.UNKNOWN) {
                            sLogger.v("we have destination marker info: " + this.currentNavigationInfo.destinationDirection);
                            removeDestinationMarker();
                            this.currentNavigationInfo.mapDestinationMarker = null;
                            if (this.currentNavigationInfo.route != null) {
                                addDestinationMarker(this.currentNavigationInfo.route.getDestination(), this.currentNavigationInfo.destinationIconId);
                            }
                        }
                    } else if (this.currentNavigationInfo.destinationDirection != com.navdy.hud.app.maps.MapEvents.DestinationDirection.UNKNOWN) {
                        maneuverDisplay.pendingRoad = this.currentNavigationInfo.destinationDirectionStr;
                        maneuverDisplay.destinationIconId = this.currentNavigationInfo.destinationIconId;
                    }
                }
                if (this.currentNavigationInfo.maneuverAfterCurrentIconid == -1) {
                    this.currentNavigationInfo.maneuverAfterCurrentIconid = maneuverDisplay.nextTurnIconId;
                    this.currentNavigationInfo.maneuverState = maneuverDisplay.maneuverState;
                } else {
                    maneuverDisplay.nextTurnIconId = this.currentNavigationInfo.maneuverAfterCurrentIconid;
                }
                java.util.Date etaDate = this.hereNavController.getEta(true, com.here.android.mpa.routing.Route.TrafficPenaltyMode.OPTIMAL);
                if (!com.navdy.hud.app.maps.here.HereMapUtil.isValidEtaDate(etaDate)) {
                    etaDate = this.hereNavController.getTtaDate(true, com.here.android.mpa.routing.Route.TrafficPenaltyMode.OPTIMAL);
                    if (!com.navdy.hud.app.maps.here.HereMapUtil.isValidEtaDate(etaDate)) {
                        sLogger.v("intermediate maneuver eta-tta date invalid = " + etaDate);
                        etaDate = null;
                    } else {
                        sLogger.v("intermediate maneuver eta-tta date = " + etaDate);
                    }
                }
                if (etaDate != null) {
                    maneuverDisplay.eta = this.timeHelper.formatTime(etaDate, this.stringBuilder);
                    maneuverDisplay.etaAmPm = this.stringBuilder.toString();
                    maneuverDisplay.etaDate = etaDate;
                }
                boolean isHighway = false;
                com.here.android.mpa.common.RoadElement element = com.navdy.hud.app.maps.here.HereMapUtil.getCurrentRoadElement();
                if (element != null) {
                    isHighway = com.navdy.hud.app.maps.here.HereMapUtil.isCurrentRoadHighway(element);
                }
                if (com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.getManeuverState(this.hereNavController.getNextManeuverDistance(), isHighway) == com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.ManeuverState.SOON) {
                    this.bus.post(new com.navdy.hud.app.maps.MapEvents.ManeuverSoonEvent(nextManeuver));
                }
                if (sLogger.isLoggable(2)) {
                    sLogger.d("ManeuverDisplay-nav:" + getHereNavigationState() + " :" + maneuverDisplay.toString());
                    sLogger.v("[ManeuverDisplay-nav-short] " + maneuverDisplay.pendingTurn + " " + maneuverDisplay.pendingRoad);
                }
                this.currentNavigationInfo.lastManeuverPostTime = java.lang.System.currentTimeMillis();
                this.bus.post(maneuverDisplay);
            } else {
                sLogger.w("there is no next maneuver");
            }
        } else if (this.currentSessionState == com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_STOPPED) {
            this.prevManeuver = null;
            this.currentManeuver = null;
            this.maneuverAfterCurrent = null;
            com.navdy.hud.app.maps.MapEvents.ManeuverDisplay maneuverDisplay2 = new com.navdy.hud.app.maps.MapEvents.ManeuverDisplay();
            maneuverDisplay2.currentRoad = com.navdy.hud.app.maps.here.HereMapUtil.getCurrentRoadName();
            maneuverDisplay2.currentSpeedLimit = com.navdy.hud.app.maps.here.HereMapUtil.getCurrentSpeedLimit();
            if (sLogger.isLoggable(2)) {
                sLogger.d("ManeuverDisplay:" + getHereNavigationState() + " :" + maneuverDisplay2.toString());
            }
            this.bus.post(maneuverDisplay2);
        } else {
            this.prevManeuver = null;
            this.currentManeuver = null;
            this.maneuverAfterCurrent = null;
            sLogger.e("Invalid state:" + this.currentSessionState + " maneuver=" + nextManeuver);
        }
    }

    public synchronized void setMapUpdateMode() {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        com.here.android.mpa.guidance.NavigationManager.MapUpdateMode mapUpdateMode = getHereMapUpdateMode();
        sLogger.v("setting here map update mode to[" + mapUpdateMode + "]");
        this.navigationManager.setMapUpdateMode(mapUpdateMode);
    }

    public java.lang.String getDestinationStreetAddress() {
        return this.currentNavigationInfo.streetAddress;
    }

    public java.lang.String getDestinationLabel() {
        return this.currentNavigationInfo.destinationLabel;
    }

    public com.here.android.mpa.routing.Route getCurrentRoute() {
        return this.currentNavigationInfo.route;
    }

    public java.lang.String getCurrentRouteId() {
        return this.currentNavigationInfo.routeId;
    }

    public com.here.android.mpa.mapping.MapRoute getCurrentMapRoute() {
        return this.currentNavigationInfo.mapRoute;
    }

    public com.navdy.service.library.events.navigation.NavigationRouteRequest getCurrentNavigationRouteRequest() {
        return this.currentNavigationInfo.navigationRouteRequest;
    }

    public com.here.android.mpa.mapping.MapMarker getDestinationMarker() {
        return this.currentNavigationInfo.mapDestinationMarker;
    }

    public com.navdy.service.library.device.NavdyDeviceId getCurrentNavigationDeviceId() {
        return this.currentNavigationInfo.deviceId;
    }

    public void sendStartManeuver(com.here.android.mpa.routing.Route route) {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        if (route != null) {
            java.util.List<com.here.android.mpa.routing.Maneuver> maneuvers = route.getManeuvers();
            if (maneuvers.size() >= 2) {
                com.navdy.hud.app.maps.MapEvents.ManeuverDisplay display = com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.getStartManeuverDisplay((com.here.android.mpa.routing.Maneuver) maneuvers.get(0), (com.here.android.mpa.routing.Maneuver) maneuvers.get(1));
                java.util.Date date = this.hereNavController.getEta(true, com.here.android.mpa.routing.Route.TrafficPenaltyMode.OPTIMAL);
                if (!com.navdy.hud.app.maps.here.HereMapUtil.isValidEtaDate(date)) {
                    date = this.hereNavController.getTtaDate(true, com.here.android.mpa.routing.Route.TrafficPenaltyMode.OPTIMAL);
                    if (com.navdy.hud.app.maps.here.HereMapUtil.isValidEtaDate(date)) {
                        sLogger.v("start maneuver eta-tta date = " + date);
                    } else {
                        date = com.navdy.hud.app.maps.here.HereMapUtil.getRouteTtaDate(route);
                        sLogger.v("start maneuver tta date = " + date);
                    }
                } else {
                    sLogger.v("start maneuver eta date = " + date);
                }
                if (date != null) {
                    display.etaDate = date;
                    display.eta = this.timeHelper.formatTime(date, this.stringBuilder);
                    display.etaAmPm = this.stringBuilder.toString();
                }
                if (sLogger.isLoggable(2)) {
                    sLogger.d("ManeuverDisplay:START:" + getHereNavigationState() + " :" + display.toString());
                }
                this.currentNavigationInfo.lastManeuverPostTime = java.lang.System.currentTimeMillis();
                this.bus.post(display);
                return;
            }
            sLogger.i("start maneuver not sent");
        }
    }

    private void setTrafficRerouteMode(com.navdy.service.library.events.preferences.NavigationPreferences.RerouteForTraffic mode) {
        this.navigationManager.setTrafficAvoidanceMode(com.here.android.mpa.guidance.NavigationManager.TrafficAvoidanceMode.MANUAL);
    }

    private void setTrafficOverlay() {
        sLogger.v("enableTraffic on map:" + true);
        if (1 != 0) {
            hereMapsManager.setTrafficOverlay(this.currentNavigationMode);
        } else {
            hereMapsManager.clearTrafficOverlay();
        }
    }

    public com.navdy.hud.app.maps.NavSessionPreferences getNavigationSessionPreference() {
        return this.driverSessionPreferences.getNavigationSessionPreference();
    }

    public void stopAndRemoveAllRoutes() {
        sLogger.v("stopAndRemoveAllRoutes");
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        com.navdy.hud.app.maps.here.HereMapUtil.removeRouteInfo(sLogger, true);
        stopNavigation();
        com.navdy.hud.app.analytics.AnalyticsSupport.recordNavigationCancelled();
    }

    public void stopNavigation() {
        if (isNavigationModeOn()) {
            sLogger.v("stopNavigation: nav mode is on");
            java.lang.String label = null;
            com.navdy.service.library.events.navigation.NavigationRouteRequest navRequest = this.currentNavigationInfo.navigationRouteRequest;
            if (navRequest != null) {
                label = navRequest.label;
            }
            handleNavigationSessionRequest(new com.navdy.service.library.events.navigation.NavigationSessionRequest(com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_STOPPED, label, this.currentNavigationInfo.routeId, java.lang.Integer.valueOf(0), java.lang.Boolean.valueOf(true)));
            return;
        }
        sLogger.v("stopNavigation: nav mode not on");
    }

    public java.lang.String getCurrentDestinationIdentifier() {
        return this.currentNavigationInfo.destinationIdentifier;
    }

    public java.lang.Boolean getGeneratePhoneticTTS() {
        return this.generatePhoneticTTS;
    }

    private void setGeneratePhoneticTTS(boolean phoneticTTS) {
        this.generatePhoneticTTS = java.lang.Boolean.valueOf(phoneticTTS);
        this.navigationManager.setTtsOutputFormat(phoneticTTS ? com.here.android.mpa.guidance.NavigationManager.TtsOutputFormat.NUANCE : com.here.android.mpa.guidance.NavigationManager.TtsOutputFormat.RAW);
    }

    @com.squareup.otto.Subscribe
    public void onDriverProfileChanged(com.navdy.hud.app.event.DriverProfileChanged profileChanged) {
        sLogger.v("driver profile changed");
    }

    @com.squareup.otto.Subscribe
    public void onTimeSettingsChange(com.navdy.hud.app.common.TimeHelper.UpdateClock event) {
        sLogger.v("time setting changed");
        if (isNavigationModeOn()) {
            refreshNavigationInfo();
        }
    }

    @com.squareup.otto.Subscribe
    public void onSpeedUnitChanged(com.navdy.hud.app.manager.SpeedManager.SpeedUnitChanged event) {
        sLogger.i("speed unit setting has changed to " + speedManager.getSpeedUnit().name() + ", refreshing");
        refreshNavigationInfo();
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.maps.here.HereNavigationManager.Anon8(), 3);
    }

    /* access modifiers changed from: private */
    public void setNavigationManagerUnit() {
        switch (speedManager.getSpeedUnit()) {
            case KILOMETERS_PER_HOUR:
                this.hereNavController.setDistanceUnit(com.here.android.mpa.guidance.NavigationManager.UnitSystem.METRIC);
                return;
            default:
                this.hereNavController.setDistanceUnit(com.here.android.mpa.guidance.NavigationManager.UnitSystem.IMPERIAL_US);
                return;
        }
    }

    private void addDestinationMarker(com.here.android.mpa.common.GeoCoordinate geoCoordinate, int iconId) {
        try {
            if (this.currentNavigationInfo.mapDestinationMarker != null) {
                removeDestinationMarker();
                this.currentNavigationInfo.mapDestinationMarker = null;
            }
            com.here.android.mpa.common.Image image = new com.here.android.mpa.common.Image();
            image.setImageResource(iconId);
            this.currentNavigationInfo.mapDestinationMarker = new com.here.android.mpa.mapping.MapMarker(geoCoordinate, image);
            this.mapController.addMapObject(this.currentNavigationInfo.mapDestinationMarker);
            hereMapsManager.getLocationFixManager().addMarkers(this.mapController);
            sLogger.v("added destination marker");
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }

    private void removeDestinationMarker() {
        if (this.currentNavigationInfo.mapDestinationMarker != null) {
            sLogger.v("removed destination marker");
            this.mapController.removeMapObject(this.currentNavigationInfo.mapDestinationMarker);
            hereMapsManager.getLocationFixManager().removeMarkers(this.mapController);
        }
    }

    public com.navdy.hud.app.ui.component.homescreen.NavigationView getNavigationView() {
        return this.navigationView;
    }

    public void arrived() {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.maps.here.HereNavigationManager.Anon9(), 20);
    }

    public boolean hasArrived() {
        if (isNavigationModeOn()) {
            return this.currentNavigationInfo.hasArrived;
        }
        return false;
    }

    public void setIgnoreArrived(boolean b) {
        if (isNavigationModeOn() && this.currentNavigationInfo.hasArrived) {
            sLogger.v("ignore arrived:" + b);
            this.currentNavigationInfo.ignoreArrived = b;
        }
    }

    public boolean isOnGasRoute() {
        if (!isNavigationModeOn()) {
            return false;
        }
        com.navdy.service.library.events.navigation.NavigationRouteRequest request = this.currentNavigationInfo.navigationRouteRequest;
        if (request == null || request.routeAttributes == null) {
            return false;
        }
        return request.routeAttributes.contains(com.navdy.service.library.events.navigation.NavigationRouteRequest.RouteAttribute.ROUTE_ATTRIBUTE_GAS);
    }

    public boolean isRerouting() {
        return this.reRouteListener.isRecalculating();
    }

    public boolean isLastManeuver() {
        return this.currentNavigationInfo.lastManeuver;
    }

    public void setLastManeuver() {
        this.currentNavigationInfo.lastManeuver = true;
    }

    public com.here.android.mpa.guidance.NavigationManager.Error addNewRoute(java.util.ArrayList<com.navdy.service.library.events.navigation.NavigationRouteResult> outgoingResults, com.navdy.service.library.events.navigation.NavigationSessionRouteChange.RerouteReason reason, com.navdy.service.library.events.navigation.NavigationRouteRequest navigationRouteRequest) {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        return addNewRoute(outgoingResults, reason, navigationRouteRequest, -1);
    }

    public com.here.android.mpa.guidance.NavigationManager.Error addNewRoute(java.util.ArrayList<com.navdy.service.library.events.navigation.NavigationRouteResult> outgoingResults, com.navdy.service.library.events.navigation.NavigationSessionRouteChange.RerouteReason reason, com.navdy.service.library.events.navigation.NavigationRouteRequest navigationRouteRequest, long simulationSpeed) {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        if (outgoingResults == null || outgoingResults.size() <= 0) {
            sLogger.e("addNewRoute calc no result, end trip:");
            stopNavigation();
            return com.here.android.mpa.guidance.NavigationManager.Error.NOT_FOUND;
        }
        sLogger.v("addNewRoute calculated=" + reason);
        java.lang.String id = ((com.navdy.service.library.events.navigation.NavigationRouteResult) outgoingResults.get(0)).routeId;
        com.navdy.hud.app.maps.here.HereRouteCache.RouteInfo info = com.navdy.hud.app.maps.here.HereRouteCache.getInstance().getRoute(id);
        if (info != null) {
            updateMapRoute(info.route, reason, navigationRouteRequest, null, null, false, info.traffic);
            sLogger.i("setNavigationMode: set transition mode");
            com.here.android.mpa.guidance.NavigationManager.Error navError = this.hereNavController.startNavigation(info.route, (int) simulationSpeed);
            if (navError == com.here.android.mpa.guidance.NavigationManager.Error.NONE) {
                sLogger.v("addNewRoute navigation started");
                return navError;
            }
            sLogger.e("addNewRoute navigation started failed:" + navError);
            stopNavigation();
            return navError;
        }
        sLogger.e("addNewRoute calc could not found route:" + id);
        stopNavigation();
        return com.here.android.mpa.guidance.NavigationManager.Error.NOT_FOUND;
    }

    public com.here.android.mpa.guidance.NavigationManager.Error addNewArrivalRoute(java.util.ArrayList<com.navdy.service.library.events.navigation.NavigationRouteResult> outgoingResults) {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        return addNewRoute(outgoingResults, com.navdy.service.library.events.navigation.NavigationSessionRouteChange.RerouteReason.NAV_SESSION_ARRIVAL_REROUTE, null);
    }

    /* access modifiers changed from: private */
    public void postArrivedManeuver() {
        com.navdy.hud.app.maps.MapEvents.ManeuverDisplay maneuverDisplay = this.currentNavigationInfo.arrivedManeuverDisplay;
        if (maneuverDisplay == null) {
            maneuverDisplay = com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.getArrivedManeuverDisplay();
            this.currentNavigationInfo.arrivedManeuverDisplay = maneuverDisplay;
            sLogger.v("arrival maneuver created");
        }
        this.currentNavigationInfo.lastManeuverPostTime = java.lang.System.currentTimeMillis();
        this.bus.post(maneuverDisplay);
    }

    public com.here.android.mpa.guidance.NavigationManager.NavigationMode getHereNavigationState() {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        return this.navigationManager.getNavigationMode();
    }

    public com.navdy.hud.app.maps.here.HereNavController.State getNavigationState() {
        return this.hereNavController.getState();
    }

    public int getCurrentTrafficRerouteCount() {
        return this.currentNavigationInfo.getTrafficReRouteCount();
    }

    public void incrCurrentTrafficRerouteCount() {
        this.currentNavigationInfo.incrTrafficRerouteCount();
    }

    public long getLastTrafficRerouteTime() {
        return this.currentNavigationInfo.getLastTrafficRerouteTime();
    }

    public void setLastTrafficRerouteTime(long l) {
        this.currentNavigationInfo.setLastTrafficRerouteTime(l);
    }

    public void startTrafficRerouteListener() {
        synchronized (trafficRerouteLock) {
            this.navdyTrafficRerouteManager.start();
            if (com.navdy.hud.app.framework.network.NetworkBandwidthController.getInstance().isLimitBandwidthModeOn()) {
                sLogger.v("startTrafficRerouteListener: limit b/w not starting here traffic reroute");
            } else {
                this.trafficRerouteListener.start();
            }
        }
    }

    public void stopTrafficRerouteListener() {
        synchronized (trafficRerouteLock) {
            this.navdyTrafficRerouteManager.stop();
            this.trafficRerouteListener.stop();
        }
    }

    public void resetTrafficRerouteListener() {
        synchronized (trafficRerouteLock) {
            this.navdyTrafficRerouteManager.reset();
        }
    }

    public boolean isTrafficUpdaterRunning() {
        return this.trafficUpdater.isRunning();
    }

    public void postManeuverDisplay() {
        refreshNavigationInfo();
    }

    public com.here.android.mpa.routing.Maneuver getNextManeuver() {
        if (isNavigationModeOn()) {
            return this.currentManeuver;
        }
        return null;
    }

    public com.here.android.mpa.routing.Maneuver getPrevManeuver() {
        if (isNavigationModeOn()) {
            return this.prevManeuver;
        }
        return null;
    }

    public void setTrafficToMode() {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        try {
            com.navdy.hud.app.maps.here.HereMapController.State state = this.mapController.getState();
            switch (state) {
                case AR_MODE:
                    sLogger.v("setTrafficToMode: " + state);
                    if (!com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getDriverProfileManager().isTrafficEnabled()) {
                        sLogger.v("setTrafficToMode: traffic is not enabled");
                        return;
                    }
                    this.mapController.setTrafficInfoVisible(true);
                    sLogger.v("setTrafficToMode: map traffic enabled");
                    com.here.android.mpa.mapping.MapRoute mapRoute = getCurrentMapRoute();
                    if (mapRoute != null) {
                        mapRoute.setTrafficEnabled(true);
                        sLogger.v("setTrafficToMode: route traffic enabled");
                        return;
                    }
                    return;
                default:
                    sLogger.v("setTrafficToMode: " + state + "::  no-op");
                    return;
            }
        } catch (Throwable t) {
            sLogger.e(t);
        }
        sLogger.e(t);
    }

    public com.navdy.hud.app.maps.here.HereNavController getNavController() {
        return this.hereNavController;
    }

    /* access modifiers changed from: 0000 */
    public void setBandwidthPreferences() {
        boolean limitBandwidthModeOn = this.bandwidthController.isLimitBandwidthModeOn();
        com.navdy.hud.app.profile.DriverProfileManager profileManager = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getDriverProfileManager();
        if (limitBandwidthModeOn) {
            sLogger.v("limitbandwidth: on");
            sLogger.v("limitbandwidth: turn map traffic off");
            hereMapsManager.clearMapTraffic();
            profileManager.disableTraffic();
            sLogger.v("limitbandwidth: turn HERE traffic listener off");
            synchronized (trafficRerouteLock) {
                this.trafficRerouteListener.stop();
            }
            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.maps.here.HereNavigationManager.Anon10(), 3);
            return;
        }
        sLogger.v("limitbandwidth: off");
        sLogger.v("limitbandwidth: turn map traffic on");
        hereMapsManager.setTrafficOverlay(getCurrentNavigationMode());
        profileManager.enableTraffic();
        synchronized (trafficRerouteLock) {
            if (com.navdy.hud.app.framework.glance.GlanceHelper.isTrafficNotificationEnabled()) {
                sLogger.v("limitbandwidth: turn HERE traffic listener on");
                this.trafficRerouteListener.start();
            } else {
                sLogger.v("limitbandwidth: HERE traffic listener off, not enabled");
            }
        }
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.maps.here.HereNavigationManager.Anon11(), 3);
    }

    public int getRerouteInterval() {
        if (this.bandwidthController.isLimitBandwidthModeOn()) {
            return com.navdy.hud.app.maps.here.HereTrafficRerouteListener.ROUTE_CHECK_INTERVAL_LIMIT_BANDWIDTH;
        }
        return com.navdy.hud.app.maps.here.HereTrafficRerouteListener.ROUTE_CHECK_INTERVAL;
    }

    public com.navdy.service.library.events.navigation.NavigationSessionRouteChange.RerouteReason getCurrentRerouteReason() {
        return this.currentNavigationInfo.currentRerouteReason;
    }

    public boolean hasTrafficRerouteOnce() {
        return this.currentNavigationInfo.trafficRerouteOnce;
    }

    public boolean isShownFirstManeuver() {
        return this.currentNavigationInfo.firstManeuverShown;
    }

    public long getFirstManeuverShowntime() {
        return this.currentNavigationInfo.firstManeuverShownTime;
    }

    public void setShownFirstManeuver(boolean b) {
        this.currentNavigationInfo.firstManeuverShown = b;
        this.currentNavigationInfo.firstManeuverShownTime = android.os.SystemClock.elapsedRealtime();
    }

    public com.here.android.mpa.routing.RouteOptions getRouteOptions() {
        return this.currentNavigationInfo.routeOptions;
    }

    public boolean isSwitchingToNewRoute() {
        return this.switchingToNewRoute;
    }

    public void setDebugManeuverState(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.ManeuverState state) {
        sLogger.v("debugManeuverState:" + state);
        this.debugManeuverState = state;
    }

    public void setDebugManeuverInstruction(java.lang.String instruction) {
        sLogger.v("debugManeuverInstruction:" + instruction);
        this.debugManeuverInstruction = instruction;
    }

    public void setDebugNextManeuverIcon(int icon) {
        sLogger.v("debugNextManeuverIcon:" + icon);
        if (icon == 0) {
            this.currentNavigationInfo.maneuverAfterCurrentIconid = -1;
        }
        this.debugNextManeuverIcon = icon;
    }

    public void setDebugManeuverIcon(int icon) {
        sLogger.v("debugManeuverIcon:" + icon);
        this.debugManeuverIcon = icon;
    }

    public void setDebugManeuverDistance(java.lang.String distance) {
        sLogger.v("debugManeuverDistance:" + distance);
        this.debugManeuverDistance = distance;
    }

    /* access modifiers changed from: 0000 */
    public void startTrafficUpdater() {
        if (this.trafficUpdater != null && !this.trafficUpdaterStarted) {
            this.trafficUpdaterStarted = true;
            this.trafficUpdater.start();
        }
    }

    public java.lang.String getCurrentVia() {
        java.lang.String id = getCurrentRouteId();
        if (id == null) {
            return null;
        }
        com.navdy.hud.app.maps.here.HereRouteCache.RouteInfo info = com.navdy.hud.app.maps.here.HereRouteCache.getInstance().getRoute(id);
        if (info == null || info.routeResult == null) {
            return null;
        }
        return info.routeResult.via;
    }

    public java.lang.String getFullStreetAddress() {
        com.navdy.service.library.events.navigation.NavigationRouteRequest req = this.currentNavigationInfo.navigationRouteRequest;
        if (req != null) {
            return req.streetAddress;
        }
        return null;
    }

    public void clearNavManeuver() {
        this.newManeuverEventListener.clearNavManeuver();
    }

    public com.here.android.mpa.routing.Maneuver getNavManeuver() {
        return this.newManeuverEventListener.getNavManeuver();
    }

    public boolean isTrafficConsidered(java.lang.String routeId) {
        try {
            com.navdy.hud.app.maps.here.HereRouteCache.RouteInfo info = com.navdy.hud.app.maps.here.HereRouteCache.getInstance().getRoute(routeId);
            if (info != null) {
                return info.traffic;
            }
        } catch (Throwable t) {
            sLogger.e(t);
        }
        return false;
    }

    public com.here.android.mpa.common.GeoCoordinate getStartLocation() {
        return this.currentNavigationInfo.startLocation;
    }

    public com.navdy.hud.app.maps.here.HereSafetySpotListener getSafetySpotListener() {
        return this.safetySpotListener;
    }
}
