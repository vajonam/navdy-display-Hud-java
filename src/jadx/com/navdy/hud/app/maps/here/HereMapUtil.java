package com.navdy.hud.app.maps.here;

public class HereMapUtil {
    private static final java.lang.String ACTIVE_GAS_ROUTE_INFO_REQUEST = "gasrouterequest.bin";
    private static final java.lang.String ACTIVE_ROUTE_INFO_DEVICE_ID = "routerequest.device";
    private static final java.lang.String ACTIVE_ROUTE_INFO_REQUEST = "routerequest.bin";
    private static final int EQUALITY_DISTANCE_VARIATION = 500;
    private static final long INVALID_ETA_TIME_DIFF = 360000;
    public static final double MILES_TO_METERS = 1609.34d;
    public static final int SHORT_DISTANCE_IN_METERS = 20;
    private static final long STALE_ROUTE_TIME = java.util.concurrent.TimeUnit.HOURS.toMillis(3);
    public static final java.lang.String TBT_ISO3_LANG_CODE = new java.util.Locale(com.navdy.hud.app.profile.HudLocale.DEFAULT_LANGUAGE, "US").getISO3Language().toUpperCase();
    private static final boolean VERBOSE = com.navdy.hud.app.BuildConfig.DEBUG;
    private static final java.lang.String day;
    private static final java.lang.String hr;
    private static java.lang.String lastKnownDeviceId;
    private static final java.lang.String min;
    private static final java.lang.String min_short;
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.maps.here.HereMapUtil.class);
    private static final java.lang.StringBuilder signPostBuilder = new java.lang.StringBuilder();
    private static final java.lang.StringBuilder signPostBuilder2 = new java.lang.StringBuilder();
    private static final java.util.HashSet<java.lang.String> signPostSet = new java.util.HashSet<>();
    private static final android.text.style.TextAppearanceSpan sp_20_b_1;
    private static final android.text.style.TextAppearanceSpan sp_20_b_2;
    private static final android.text.style.TextAppearanceSpan sp_24_1;
    private static final android.text.style.TextAppearanceSpan sp_24_2;
    private static final android.text.style.TextAppearanceSpan sp_26_1;
    private static final android.text.style.TextAppearanceSpan sp_26_2;
    private static final java.lang.StringBuilder stringBuilder = new java.lang.StringBuilder();
    private static final java.lang.StringBuilder trafficEventBuilder = new java.lang.StringBuilder();
    private static final com.squareup.wire.Wire wire = new com.squareup.wire.Wire((java.lang.Class<?>[]) new java.lang.Class[]{com.navdy.service.library.events.Ext_NavdyEvent.class});

    static class Anon1 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.hud.app.maps.here.HereMapUtil.IImageLoadCallback val$callback;
        final /* synthetic */ int val$height;
        final /* synthetic */ com.here.android.mpa.common.Image val$image;
        final /* synthetic */ int val$width;

        Anon1(com.here.android.mpa.common.Image image, int i, int i2, com.navdy.hud.app.maps.here.HereMapUtil.IImageLoadCallback iImageLoadCallback) {
            this.val$image = image;
            this.val$width = i;
            this.val$height = i2;
            this.val$callback = iImageLoadCallback;
        }

        public void run() {
            try {
                this.val$callback.result(this.val$image, this.val$image.getBitmap(this.val$width, this.val$height));
            } catch (Throwable t) {
                com.navdy.hud.app.maps.here.HereMapUtil.sLogger.e(t);
                this.val$callback.result(this.val$image, null);
            }
        }
    }

    static /* synthetic */ class Anon2 {
        static final /* synthetic */ int[] $SwitchMap$com$here$android$mpa$routing$Maneuver$Action = new int[com.here.android.mpa.routing.Maneuver.Action.values().length];

        static {
            try {
                $SwitchMap$com$here$android$mpa$routing$Maneuver$Action[com.here.android.mpa.routing.Maneuver.Action.CONTINUE_HIGHWAY.ordinal()] = 1;
            } catch (java.lang.NoSuchFieldError e) {
            }
            try {
                $SwitchMap$com$here$android$mpa$routing$Maneuver$Action[com.here.android.mpa.routing.Maneuver.Action.CHANGE_HIGHWAY.ordinal()] = 2;
            } catch (java.lang.NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$com$here$android$mpa$routing$Maneuver$Action[com.here.android.mpa.routing.Maneuver.Action.ENTER_HIGHWAY.ordinal()] = 3;
            } catch (java.lang.NoSuchFieldError e3) {
            }
            try {
                $SwitchMap$com$here$android$mpa$routing$Maneuver$Action[com.here.android.mpa.routing.Maneuver.Action.ENTER_HIGHWAY_FROM_LEFT.ordinal()] = 4;
            } catch (java.lang.NoSuchFieldError e4) {
            }
            try {
                $SwitchMap$com$here$android$mpa$routing$Maneuver$Action[com.here.android.mpa.routing.Maneuver.Action.ENTER_HIGHWAY_FROM_RIGHT.ordinal()] = 5;
            } catch (java.lang.NoSuchFieldError e5) {
            }
        }
    }

    public interface IImageLoadCallback {
        void result(com.here.android.mpa.common.Image image, android.graphics.Bitmap bitmap);
    }

    public static class LongContainer {
        public long val;
    }

    public static class SavedRouteData {
        public final boolean isGasRoute;
        public final com.navdy.service.library.events.navigation.NavigationRouteRequest navigationRouteRequest;

        public SavedRouteData(com.navdy.service.library.events.navigation.NavigationRouteRequest navigationRouteRequest2, boolean isGasRoute2) {
            this.navigationRouteRequest = navigationRouteRequest2;
            this.isGasRoute = isGasRoute2;
        }
    }

    public static class SignPostInfo {
        public boolean hasNameInSignPost;
        public boolean hasNumberInSignPost;
    }

    static {
        android.content.Context context = com.navdy.hud.app.HudApplication.getAppContext();
        android.content.res.Resources resources = context.getResources();
        sp_20_b_1 = new android.text.style.TextAppearanceSpan(context, com.navdy.hud.app.R.style.route_duration_20_b);
        sp_20_b_2 = new android.text.style.TextAppearanceSpan(context, com.navdy.hud.app.R.style.route_duration_20_b);
        sp_24_1 = new android.text.style.TextAppearanceSpan(context, com.navdy.hud.app.R.style.route_duration_24);
        sp_24_2 = new android.text.style.TextAppearanceSpan(context, com.navdy.hud.app.R.style.route_duration_24);
        sp_26_1 = new android.text.style.TextAppearanceSpan(context, com.navdy.hud.app.R.style.route_duration_26);
        sp_26_2 = new android.text.style.TextAppearanceSpan(context, com.navdy.hud.app.R.style.route_duration_26);
        min = resources.getString(com.navdy.hud.app.R.string.route_time_min);
        min_short = resources.getString(com.navdy.hud.app.R.string.route_time_min_short);
        hr = resources.getString(com.navdy.hud.app.R.string.route_time_hr);
        day = resources.getString(com.navdy.hud.app.R.string.route_time_day);
    }

    public static com.here.android.mpa.common.RoadElement getCurrentRoadElement() {
        com.here.android.mpa.common.PositioningManager positioningManager = com.navdy.hud.app.maps.here.HereMapsManager.getInstance().getPositioningManager();
        if (positioningManager == null) {
            return null;
        }
        return positioningManager.getRoadElement();
    }

    public static java.lang.String getCurrentRoadName() {
        com.here.android.mpa.common.PositioningManager positioningManager = com.navdy.hud.app.maps.here.HereMapsManager.getInstance().getPositioningManager();
        if (positioningManager == null) {
            return null;
        }
        com.here.android.mpa.common.RoadElement roadElement = positioningManager.getRoadElement();
        if (roadElement != null) {
            return getRoadName(roadElement);
        }
        return null;
    }

    public static java.lang.String getRoadName(java.util.List<com.here.android.mpa.common.RoadElement> roadElements) {
        if (roadElements == null || roadElements.size() == 0) {
            return "";
        }
        for (com.here.android.mpa.common.RoadElement roadElement : roadElements) {
            java.lang.String name = roadElement.getRoadName();
            if (!android.text.TextUtils.isEmpty(name)) {
                return name;
            }
            java.lang.String name2 = roadElement.getRouteName();
            if (!android.text.TextUtils.isEmpty(name2)) {
                return name2;
            }
        }
        return "";
    }

    public static java.lang.String getRoadName(com.here.android.mpa.routing.Maneuver maneuver) {
        return concatText(maneuver.getRoadNumber(), maneuver.getRoadName(), true, isCurrentRoadHighway(maneuver));
    }

    public static java.lang.String getNextRoadName(com.here.android.mpa.routing.Maneuver maneuver) {
        return concatText(maneuver.getNextRoadNumber(), maneuver.getNextRoadName(), true, isCurrentRoadHighway(maneuver));
    }

    public static java.lang.String getRoadName(com.here.android.mpa.common.RoadElement roadElement) {
        if (roadElement == null) {
            return null;
        }
        return concatText(roadElement.getRouteName(), roadElement.getRoadName(), true, isCurrentRoadHighway(roadElement));
    }

    public static float getCurrentSpeedLimit() {
        com.here.android.mpa.common.RoadElement roadElement = getCurrentRoadElement();
        if (roadElement != null) {
            return roadElement.getSpeedLimit();
        }
        return 0.0f;
    }

    public static boolean isCurrentRoadHighway(com.here.android.mpa.common.RoadElement roadElement) {
        if (roadElement != null && roadElement.getAttributes().contains(com.here.android.mpa.common.RoadElement.Attribute.HIGHWAY)) {
            return true;
        }
        return false;
    }

    public static boolean isCurrentRoadHighway(com.here.android.mpa.routing.Maneuver maneuver) {
        if (maneuver == null) {
            return false;
        }
        if (isManeuverActionHighway(maneuver.getAction())) {
            return true;
        }
        java.util.List<com.here.android.mpa.common.RoadElement> elements = maneuver.getRoadElements();
        if (elements == null || elements.size() == 0) {
            return false;
        }
        return isCurrentRoadHighway((com.here.android.mpa.common.RoadElement) elements.get(0));
    }

    public static boolean isManeuverActionHighway(com.here.android.mpa.routing.Maneuver.Action action) {
        if (action == null) {
            return false;
        }
        switch (com.navdy.hud.app.maps.here.HereMapUtil.Anon2.$SwitchMap$com$here$android$mpa$routing$Maneuver$Action[action.ordinal()]) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
                return true;
            default:
                return false;
        }
    }

    public static boolean isManeuverShort(com.here.android.mpa.routing.Maneuver maneuver) {
        int distance = maneuver.getDistanceToNextManeuver();
        if (distance <= 20) {
            if (VERBOSE) {
                sLogger.v("[Nav] distance is short:" + distance);
            }
            return true;
        }
        if (VERBOSE) {
            sLogger.v("[Nav] distance is NOT short:" + distance);
        }
        return false;
    }

    public static boolean isStartManeuver(com.here.android.mpa.routing.Maneuver maneuver) {
        if (maneuver != null && maneuver.getIcon() == com.here.android.mpa.routing.Maneuver.Icon.START) {
            return true;
        }
        return false;
    }

    public static void printRouteDetails(com.here.android.mpa.routing.Route route, java.lang.String streetAddress, com.navdy.service.library.events.navigation.NavigationRouteRequest request, java.lang.StringBuilder buffer) {
        printRouteDetails(route, streetAddress, request, buffer, null);
    }

    public static void printRouteDetails(com.here.android.mpa.routing.Route route, java.lang.String streetAddress, com.navdy.service.library.events.navigation.NavigationRouteRequest request, java.lang.StringBuilder buffer, @android.support.annotation.Nullable com.here.android.mpa.routing.Maneuver printFromManeuver) {
        com.navdy.hud.app.maps.MapEvents.ManeuverDisplay display;
        if (route != null) {
            long l1 = android.os.SystemClock.elapsedRealtime();
            java.lang.String routeInfo = "[Route] start[" + route.getStart() + "] end[" + route.getDestination() + "] length[" + route.getLength() + " meters] tta[" + route.getTta(com.here.android.mpa.routing.Route.TrafficPenaltyMode.OPTIMAL, com.glympse.android.lib.e.gA).getDuration() + " seconds]";
            if (buffer == null) {
                sLogger.v(routeInfo);
            } else {
                buffer.append(routeInfo + com.navdy.hud.app.framework.glance.GlanceConstants.NEWLINE);
            }
            java.util.List<com.here.android.mpa.routing.Maneuver> maneuvers = route.getManeuvers();
            if (printFromManeuver != null) {
                int fromManeuverIndex = findManeuverIndex(maneuvers, printFromManeuver);
                if (fromManeuverIndex >= 0) {
                    maneuvers = maneuvers.subList(fromManeuverIndex, maneuvers.size());
                } else {
                    return;
                }
            }
            int len = maneuvers.size();
            com.here.android.mpa.routing.Maneuver previous = null;
            com.here.android.mpa.routing.Maneuver current = null;
            for (int i = 0; i < len; i++) {
                if (current != null) {
                    previous = current;
                }
                current = (com.here.android.mpa.routing.Maneuver) maneuvers.get(i);
                com.here.android.mpa.routing.Maneuver after = null;
                if (i < len - 1) {
                    after = (com.here.android.mpa.routing.Maneuver) maneuvers.get(i + 1);
                }
                java.lang.String iconStr = "";
                com.here.android.mpa.routing.Maneuver.Icon icon = current.getIcon();
                if (icon != null) {
                    iconStr = icon.name();
                }
                java.lang.String directionStr = "";
                com.here.android.mpa.routing.Maneuver.TrafficDirection trafficDirection = current.getTrafficDirection();
                if (trafficDirection != null) {
                    directionStr = trafficDirection.name();
                }
                java.lang.String maneuverRaw = "    [Maneuver-Raw] turn[" + current.getTurn().name() + "] to[" + current.getNextRoadNumber() + " " + current.getNextRoadName() + "] distance[" + current.getDistanceToNextManeuver() + " meters] action[" + current.getAction() + "] current[" + current.getRoadNumber() + " " + current.getRoadName() + "] traffic-directon[" + directionStr + "] angle[" + current.getAngle() + "] orientation[" + current.getMapOrientation() + "] distanceFromStart[" + current.getDistanceFromStart() + " meters] Icon[" + iconStr + "]";
                if (buffer != null) {
                    buffer.append(maneuverRaw + com.navdy.hud.app.framework.glance.GlanceConstants.NEWLINE);
                } else {
                    sLogger.v(maneuverRaw);
                }
                com.here.android.mpa.routing.Signpost signpost = current.getSignpost();
                if (signpost != null) {
                    java.lang.String rawSignPost = "    [Maneuver-Raw-SignPost] exitNumber[" + signpost.getExitNumber() + "] exitText[" + signpost.getExitText() + "]";
                    if (buffer != null) {
                        buffer.append(rawSignPost);
                    } else {
                        sLogger.v(rawSignPost);
                    }
                    for (com.here.android.mpa.routing.Signpost.LocalizedLabel label : signpost.getExitDirections()) {
                        java.lang.String signpostLabel = "     [Maneuver-Raw-SignPost-Label]  text[" + label.getText() + "] routeDirection[" + label.getRouteDirection() + "] routeName[" + label.getRouteName() + "]";
                        if (buffer != null) {
                            buffer.append(signpostLabel + com.navdy.hud.app.framework.glance.GlanceConstants.NEWLINE);
                        } else {
                            sLogger.v(signpostLabel);
                        }
                    }
                }
                if (i == 0) {
                    display = com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.getStartManeuverDisplay(current, after);
                } else {
                    display = com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.getManeuverDisplay(current, after == null, (long) current.getDistanceToNextManeuver(), streetAddress, previous, request, after, true, false, null);
                }
                sLogger.v("[Maneuver-Display] " + display.toString());
                java.lang.String shortTbt = "[Maneuver-Display-short] " + display.pendingTurn + " " + display.pendingRoad + " ( " + display.distanceToPendingRoadText + " )";
                if (buffer != null) {
                    buffer.append(shortTbt + com.navdy.hud.app.framework.glance.GlanceConstants.NEWLINE);
                } else {
                    sLogger.v(shortTbt);
                }
            }
            sLogger.v("Time to print route:" + (android.os.SystemClock.elapsedRealtime() - l1));
        }
    }

    public static void printManeuverDetails(com.here.android.mpa.routing.Maneuver maneuver, java.lang.String tag) {
        sLogger.v(tag + " turn[" + maneuver.getTurn().name() + "] to[" + maneuver.getNextRoadNumber() + " " + maneuver.getNextRoadName() + "] distance[" + maneuver.getDistanceToNextManeuver() + " meters] action[" + maneuver.getAction() + "] current[" + maneuver.getRoadNumber() + " " + maneuver.getRoadName() + "] distanceFromStart[" + maneuver.getDistanceFromStart() + " meters]");
    }

    static boolean isValidNavigationState(com.navdy.service.library.events.navigation.NavigationSessionState navigationSessionState) {
        if (navigationSessionState == null || (navigationSessionState != com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_STARTED && navigationSessionState != com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_PAUSED && navigationSessionState != com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_STOPPED)) {
            return false;
        }
        return true;
    }

    public static java.lang.String parseStreetAddress(java.lang.String streetAddress) {
        if (streetAddress == null) {
            return null;
        }
        int index = streetAddress.indexOf(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA);
        if (index >= 0) {
            return streetAddress.substring(0, index);
        }
        return streetAddress;
    }

    public static boolean areManeuverEqual(com.here.android.mpa.routing.Maneuver maneuver, com.here.android.mpa.routing.Maneuver other) {
        if (maneuver == null || other == null) {
            return false;
        }
        boolean ret = maneuver.getRoadElements().equals(other.getRoadElements());
        if (ret) {
            return ret;
        }
        try {
            if (maneuver.getTurn() != other.getTurn() || !android.text.TextUtils.equals(maneuver.getNextRoadNumber(), other.getNextRoadNumber()) || !android.text.TextUtils.equals(maneuver.getNextRoadName(), other.getNextRoadName()) || !android.text.TextUtils.equals(maneuver.getRoadNumber(), other.getRoadNumber()) || !android.text.TextUtils.equals(maneuver.getRoadName(), other.getRoadName()) || maneuver.getAction() != other.getAction() || java.lang.Math.abs(maneuver.getDistanceToNextManeuver() - other.getDistanceToNextManeuver()) > 500) {
                return ret;
            }
            if (!sLogger.isLoggable(2)) {
                return true;
            }
            sLogger.v("areManeuverEqual: distance diff=" + java.lang.Math.abs(maneuver.getDistanceToNextManeuver() - other.getDistanceToNextManeuver()) + " allowance=" + 500);
            return true;
        } catch (Throwable t) {
            sLogger.e(t);
            return ret;
        }
    }

    static java.util.List<com.here.android.mpa.routing.Maneuver> getDifferentManeuver(com.here.android.mpa.routing.Route route, com.here.android.mpa.routing.Route other, int n, java.util.List<com.here.android.mpa.routing.Maneuver> otherManeuverDifference) {
        java.util.List<com.here.android.mpa.routing.Maneuver> result = new java.util.ArrayList<>();
        java.util.List<com.here.android.mpa.routing.Maneuver> routeManeuvers = route.getManeuvers();
        java.util.List<com.here.android.mpa.routing.Maneuver> otherManeuvers = other.getManeuvers();
        int size1 = routeManeuvers.size();
        int size2 = otherManeuvers.size();
        int maxIndex = size1 > size2 ? size2 - 1 : size1 - 1;
        int i = 0;
        while (i <= maxIndex) {
            com.here.android.mpa.routing.Maneuver routeManeuver = (com.here.android.mpa.routing.Maneuver) routeManeuvers.get(i);
            com.here.android.mpa.routing.Maneuver otherManeuver = (com.here.android.mpa.routing.Maneuver) otherManeuvers.get(i);
            if (sLogger.isLoggable(2)) {
                printManeuverDetails(routeManeuver, "getDifferentManeuver-1");
                printManeuverDetails(otherManeuver, "getDifferentManeuver-2");
            }
            if (!areManeuverEqual(routeManeuver, otherManeuver)) {
                java.lang.String s = getNextRoadName(routeManeuver);
                if (!android.text.TextUtils.isEmpty(s)) {
                    if (sLogger.isLoggable(2)) {
                        sLogger.v("getDifferentManeuver:diff:" + s);
                    }
                    result.add(routeManeuver);
                    otherManeuverDifference.add(otherManeuver);
                    n--;
                    if (n == 0) {
                        break;
                    }
                } else {
                    i++;
                }
            }
            i++;
        }
        return result;
    }

    static boolean areManeuversEqual(java.util.List<com.here.android.mpa.routing.Maneuver> first, int firstOffset, java.util.List<com.here.android.mpa.routing.Maneuver> second, int secondOffset, java.util.List<com.here.android.mpa.routing.Maneuver> difference) {
        int len1 = first.size() - firstOffset;
        int len2 = second.size() - secondOffset;
        int secondOffset2 = secondOffset;
        int firstOffset2 = firstOffset;
        while (len1 > 0 && len2 > 0) {
            int firstOffset3 = firstOffset2 + 1;
            com.here.android.mpa.routing.Maneuver m1 = (com.here.android.mpa.routing.Maneuver) first.get(firstOffset2);
            int secondOffset3 = secondOffset2 + 1;
            com.here.android.mpa.routing.Maneuver m2 = (com.here.android.mpa.routing.Maneuver) second.get(secondOffset2);
            if (!areManeuverEqual(m1, m2)) {
                sLogger.v("maneuver are NOT equal");
                printManeuverDetails(m1, "[maneuver-1]");
                printManeuverDetails(m2, "[maneuver-2]");
                if (difference == null) {
                    return false;
                }
                difference.add(m1);
                return false;
            }
            if (sLogger.isLoggable(2)) {
                sLogger.v("maneuver are equal");
                printManeuverDetails(m1, "[maneuver-1]");
                printManeuverDetails(m2, "[maneuver-2]");
            }
            len1--;
            len2--;
            secondOffset2 = secondOffset3;
            firstOffset2 = firstOffset3;
        }
        if (len1 > 0 || len2 > 0) {
            if (len1 > 0 && difference != null) {
                difference.add(first.get(firstOffset2));
            }
            int i = secondOffset2;
            int i2 = firstOffset2;
            return false;
        }
        int i3 = secondOffset2;
        int i4 = firstOffset2;
        return true;
    }

    static void printTrafficNotification(com.here.android.mpa.guidance.TrafficNotification trafficNotification, java.lang.String tag) {
        java.util.List<com.here.android.mpa.guidance.TrafficNotificationInfo> list = trafficNotification.getInfoList();
        if (list != null) {
            for (com.here.android.mpa.guidance.TrafficNotificationInfo trafficNotificationInfo : list) {
                sLogger.v(tag + " " + trafficNotificationInfo);
            }
        }
    }

    public static com.here.android.mpa.routing.Maneuver getLastEqualManeuver(com.here.android.mpa.routing.Route route, com.here.android.mpa.routing.Route other) {
        int maxIndex;
        java.util.List<com.here.android.mpa.routing.Maneuver> routeManeuvers = route.getManeuvers();
        java.util.List<com.here.android.mpa.routing.Maneuver> otherManeuvers = other.getManeuvers();
        if (routeManeuvers.size() < otherManeuvers.size()) {
            maxIndex = routeManeuvers.size() - 1;
        } else {
            maxIndex = otherManeuvers.size() - 1;
        }
        for (int i = 0; i <= maxIndex; i++) {
            if (areManeuverEqual((com.here.android.mpa.routing.Maneuver) routeManeuvers.get(i), (com.here.android.mpa.routing.Maneuver) otherManeuvers.get(i)) && i > 0) {
                return (com.here.android.mpa.routing.Maneuver) routeManeuvers.get(i - 1);
            }
        }
        return null;
    }

    public static boolean isValidEtaDate(java.util.Date etaDate) {
        if (etaDate == null) {
            return false;
        }
        int year = etaDate.getYear();
        if (etaDate == null || year <= 71) {
            sLogger.e("invalid eta date[" + etaDate + "]");
            return false;
        }
        long timeDiff = java.lang.System.currentTimeMillis() - etaDate.getTime();
        if (timeDiff >= INVALID_ETA_TIME_DIFF) {
            sLogger.e("invalid eta timediff[" + timeDiff + "]");
            return false;
        }
        if (timeDiff > 0) {
            sLogger.i("eta is behind timediff[" + timeDiff + "]");
        }
        return true;
    }

    public static java.util.Date getRouteTtaDate(com.here.android.mpa.routing.Route route) {
        if (route == null) {
            return null;
        }
        try {
            com.here.android.mpa.routing.RouteTta tta = route.getTta(com.here.android.mpa.routing.Route.TrafficPenaltyMode.OPTIMAL, com.glympse.android.lib.e.gA);
            if (tta != null && tta.getDuration() == 0) {
                tta = null;
            }
            if (tta == null) {
                tta = route.getTta(com.here.android.mpa.routing.Route.TrafficPenaltyMode.DISABLED, com.glympse.android.lib.e.gA);
            }
            if (tta == null) {
                return null;
            }
            return new java.util.Date(java.lang.System.currentTimeMillis() + java.util.concurrent.TimeUnit.SECONDS.toMillis((long) tta.getDuration()));
        } catch (Throwable t) {
            sLogger.e(t);
            return null;
        }
    }

    public static void saveRouteInfo(com.navdy.service.library.events.navigation.NavigationRouteRequest request, com.navdy.service.library.device.NavdyDeviceId deviceId, com.navdy.service.library.log.Logger logger) {
        saveRouteInfoInternal(request, deviceId, logger, ACTIVE_ROUTE_INFO_REQUEST);
    }

    public static void saveGasRouteInfo(com.navdy.service.library.events.navigation.NavigationRouteRequest request, com.navdy.service.library.device.NavdyDeviceId deviceId, com.navdy.service.library.log.Logger sLogger2) {
        saveRouteInfoInternal(request, deviceId, sLogger2, ACTIVE_GAS_ROUTE_INFO_REQUEST);
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x0066 A[Catch:{ Throwable -> 0x007c }] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x008f A[SYNTHETIC, Splitter:B:27:0x008f] */
    private static synchronized void saveRouteInfoInternal(com.navdy.service.library.events.navigation.NavigationRouteRequest r, com.navdy.service.library.device.NavdyDeviceId deviceId, com.navdy.service.library.log.Logger logger, java.lang.String fileName) {
        synchronized (com.navdy.hud.app.maps.here.HereMapUtil.class) {
            java.io.FileOutputStream file = null;
            lastKnownDeviceId = null;
            if (deviceId == null) {
                sLogger.v("saveRouteInfo: null deviceid");
                try {
                    com.navdy.service.library.device.NavdyDeviceId deviceId2 = new com.navdy.service.library.device.NavdyDeviceId(com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getConnectionHandler().getLastConnectedDeviceInfo().deviceId);
                    try {
                        sLogger.v("saveRouteInfo: last deviceid found:" + deviceId2);
                        deviceId = deviceId2;
                    } catch (Throwable th) {
                        th = th;
                        com.navdy.service.library.device.NavdyDeviceId navdyDeviceId = deviceId2;
                        throw th;
                    }
                } catch (Throwable th2) {
                    t = th2;
                    sLogger.e("saveRouteInfo", t);
                    logger.v("saveRouteInfo request[" + r + "] deviceId[" + deviceId + "]");
                    if (r.destination == null) {
                    }
                }
            }
            logger.v("saveRouteInfo request[" + r + "] deviceId[" + deviceId + "]");
            if (r.destination == null) {
                logger.w("route does not have destination");
                removeRouteInfoFiles(sLogger, true);
            } else {
                java.util.List<com.navdy.service.library.events.navigation.NavigationRouteRequest.RouteAttribute> routeAttributes = new java.util.ArrayList<>(r.routeAttributes);
                if (!routeAttributes.contains(com.navdy.service.library.events.navigation.NavigationRouteRequest.RouteAttribute.ROUTE_ATTRIBUTE_SAVED_ROUTE)) {
                    routeAttributes.add(com.navdy.service.library.events.navigation.NavigationRouteRequest.RouteAttribute.ROUTE_ATTRIBUTE_SAVED_ROUTE);
                }
                com.navdy.service.library.events.navigation.NavigationRouteRequest request = new com.navdy.service.library.events.navigation.NavigationRouteRequest.Builder(r).routeAttributes(routeAttributes).build();
                sLogger.v("saveRouteInfo added saved route attribute");
                java.lang.String path = com.navdy.hud.app.storage.PathManager.getInstance().getActiveRouteInfoDir();
                java.io.FileOutputStream file2 = new java.io.FileOutputStream(path + java.io.File.separator + fileName);
                try {
                    byte[] data = request.toByteArray();
                    file2.write(data);
                    com.navdy.service.library.util.IOUtils.fileSync(file2);
                    com.navdy.service.library.util.IOUtils.closeStream(file2);
                    file = null;
                    logger.v("saveRouteInfo: route info size[" + data.length + "]");
                    if (deviceId != null) {
                        file2 = new java.io.FileOutputStream(path + java.io.File.separator + ACTIVE_ROUTE_INFO_DEVICE_ID);
                        byte[] data2 = deviceId.toString().getBytes();
                        file2.write(data2);
                        com.navdy.service.library.util.IOUtils.fileSync(file2);
                        com.navdy.service.library.util.IOUtils.closeStream(file2);
                        file = null;
                        lastKnownDeviceId = deviceId.toString();
                        logger.v("saveRouteInfo: device id size[" + data2.length + "]");
                    } else {
                        logger.v("saveRouteInfo: device id not written:null");
                    }
                } catch (Throwable th3) {
                    th = th3;
                    java.io.FileOutputStream fileOutputStream = file2;
                    throw th;
                }
            }
        }
    }

    public static synchronized void removeRouteInfo(com.navdy.service.library.log.Logger logger, boolean clearAll) {
        synchronized (com.navdy.hud.app.maps.here.HereMapUtil.class) {
            removeRouteInfoFiles(logger, clearAll);
        }
    }

    private static void removeRouteInfoFiles(com.navdy.service.library.log.Logger logger, boolean clearAll) {
        try {
            android.content.Context context = com.navdy.hud.app.HudApplication.getAppContext();
            java.lang.String path = com.navdy.hud.app.storage.PathManager.getInstance().getActiveRouteInfoDir();
            boolean b1 = com.navdy.service.library.util.IOUtils.deleteFile(context, path + java.io.File.separator + ACTIVE_GAS_ROUTE_INFO_REQUEST);
            logger.v("removeRouteInfo delete gasRoute[" + b1 + "]");
            if (!b1 || clearAll) {
                boolean b2 = com.navdy.service.library.util.IOUtils.deleteFile(context, path + java.io.File.separator + ACTIVE_ROUTE_INFO_REQUEST);
                logger.v("removeRouteInfo delete route[" + b2 + "] delete id[" + com.navdy.service.library.util.IOUtils.deleteFile(context, path + java.io.File.separator + ACTIVE_ROUTE_INFO_DEVICE_ID) + "]");
            }
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }

    public static synchronized com.navdy.hud.app.maps.here.HereMapUtil.SavedRouteData getSavedRouteData(com.navdy.service.library.log.Logger logger) {
        com.navdy.hud.app.maps.here.HereMapUtil.SavedRouteData savedRouteData;
        synchronized (com.navdy.hud.app.maps.here.HereMapUtil.class) {
            com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
            com.navdy.service.library.events.navigation.NavigationRouteRequest request = getSavedRouteRequest(logger);
            savedRouteData = new com.navdy.hud.app.maps.here.HereMapUtil.SavedRouteData(request, request != null && request.routeAttributes.contains(com.navdy.service.library.events.navigation.NavigationRouteRequest.RouteAttribute.ROUTE_ATTRIBUTE_GAS));
        }
        return savedRouteData;
    }

    private static com.navdy.service.library.events.navigation.NavigationRouteRequest getSavedRouteRequest(com.navdy.service.library.log.Logger logger) {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        logger.v("getSavedRouteRequest");
        java.io.FileInputStream file = null;
        try {
            lastKnownDeviceId = null;
            java.lang.String path = com.navdy.hud.app.storage.PathManager.getInstance().getActiveRouteInfoDir();
            java.io.File f = new java.io.File(path + java.io.File.separator + ACTIVE_GAS_ROUTE_INFO_REQUEST);
            boolean gasRouteExists = false;
            if (f.exists()) {
                if (isRouteFileStale(f)) {
                    long modified = f.lastModified();
                    com.navdy.service.library.log.Logger logger2 = logger;
                    logger2.v("getSavedRouteRequest: gas route is stale file-time =" + modified + " now=" + java.lang.System.currentTimeMillis() + " deleted=" + com.navdy.service.library.util.IOUtils.deleteFile(com.navdy.hud.app.HudApplication.getAppContext(), f.getAbsolutePath()));
                } else {
                    gasRouteExists = true;
                    logger.v("getSavedRouteRequest: gas route is good, file-time =" + f.lastModified() + " now=" + java.lang.System.currentTimeMillis());
                }
            }
            if (!gasRouteExists) {
                logger.v("getSavedRouteRequest: no saved gas route");
                f = new java.io.File(path + java.io.File.separator + ACTIVE_ROUTE_INFO_REQUEST);
                if (!f.exists()) {
                    logger.v("getSavedRouteRequest: no saved route");
                    return null;
                } else if (isRouteFileStale(f)) {
                    long modified2 = f.lastModified();
                    com.navdy.service.library.log.Logger logger3 = logger;
                    logger3.v("getSavedRouteRequest: route is stale file-time =" + modified2 + " now=" + java.lang.System.currentTimeMillis() + " deleted=" + com.navdy.service.library.util.IOUtils.deleteFile(com.navdy.hud.app.HudApplication.getAppContext(), f.getAbsolutePath()));
                    return null;
                } else {
                    logger.v("getSavedRouteRequest: route is good file-time =" + f.lastModified() + " now=" + java.lang.System.currentTimeMillis());
                }
            }
            java.io.FileInputStream file2 = new java.io.FileInputStream(f);
            try {
                com.navdy.service.library.events.navigation.NavigationRouteRequest request = new com.navdy.service.library.events.navigation.NavigationRouteRequest.Builder((com.navdy.service.library.events.navigation.NavigationRouteRequest) wire.parseFrom((java.io.InputStream) file2, com.navdy.service.library.events.navigation.NavigationRouteRequest.class)).requestId(java.util.UUID.randomUUID().toString()).build();
                com.navdy.service.library.util.IOUtils.closeStream(file2);
                file = null;
                java.io.File f2 = new java.io.File(path + java.io.File.separator + ACTIVE_ROUTE_INFO_DEVICE_ID);
                if (!f2.exists()) {
                    logger.v("getSavedRouteRequest: no saved device id");
                    return request;
                }
                java.lang.String deviceId = com.navdy.service.library.util.IOUtils.convertFileToString(f2.getAbsolutePath());
                com.navdy.service.library.device.NavdyDeviceId id = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getDeviceId();
                if (id == null || id.equals(new com.navdy.service.library.device.NavdyDeviceId(deviceId))) {
                    lastKnownDeviceId = deviceId;
                    logger.v("getSavedRouteRequest: returning route");
                    return request;
                }
                logger.v("getSavedRouteRequest: device id is different now[" + id + "] stored[" + deviceId + "]");
                removeRouteInfo(logger, true);
                return null;
            } catch (Throwable th) {
                t = th;
                file = file2;
                sLogger.e(t);
                com.navdy.service.library.util.IOUtils.closeStream(file);
                removeRouteInfo(logger, true);
                lastKnownDeviceId = null;
                return null;
            }
        } catch (Throwable th2) {
            t = th2;
            sLogger.e(t);
            com.navdy.service.library.util.IOUtils.closeStream(file);
            removeRouteInfo(logger, true);
            lastKnownDeviceId = null;
            return null;
        }
    }

    public static com.navdy.service.library.device.NavdyDeviceId getLastKnownDeviceId() {
        if (lastKnownDeviceId == null) {
            return null;
        }
        try {
            com.navdy.service.library.device.NavdyDeviceId id = new com.navdy.service.library.device.NavdyDeviceId(lastKnownDeviceId);
            lastKnownDeviceId = null;
            return id;
        } catch (Throwable t) {
            lastKnownDeviceId = null;
            sLogger.e(t);
            return null;
        }
    }

    public static boolean isCoordinateEqual(com.here.android.mpa.common.GeoCoordinate g1, com.here.android.mpa.common.GeoCoordinate g2) {
        double lat1 = g1.getLatitude();
        double lng1 = g1.getLongitude();
        double lat2 = g2.getLatitude();
        double lng2 = g2.getLongitude();
        if (lat1 == lat2 && lng1 == lng2) {
            return true;
        }
        return false;
    }

    public static java.lang.String concatText(java.lang.String number, java.lang.String name, boolean addParenthesis, boolean isHighway) {
        java.lang.String sb;
        synchronized (stringBuilder) {
            stringBuilder.setLength(0);
            boolean hasNumber = false;
            if (!android.text.TextUtils.isEmpty(number)) {
                stringBuilder.append(number);
                hasNumber = true;
            } else {
                isHighway = false;
            }
            if (!android.text.TextUtils.isEmpty(name) && !isHighway) {
                if (hasNumber) {
                    stringBuilder.append(" ");
                    if (addParenthesis) {
                        stringBuilder.append(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.OPEN_BRACKET);
                    }
                } else {
                    addParenthesis = false;
                }
                stringBuilder.append(name);
                if (addParenthesis) {
                    stringBuilder.append(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.CLOSE_BRACKET);
                }
            }
            sb = stringBuilder.toString();
        }
        return sb;
    }

    public static boolean hasSameSignpostAndToRoad(com.here.android.mpa.routing.Maneuver first, com.here.android.mpa.routing.Maneuver second) {
        if (first == null || second == null) {
            return false;
        }
        java.lang.String toRoad1 = concatText(first.getNextRoadNumber(), first.getNextRoadName(), true, false);
        java.lang.String toRoad2 = concatText(second.getNextRoadNumber(), second.getNextRoadName(), true, false);
        if (android.text.TextUtils.isEmpty(toRoad1) || android.text.TextUtils.isEmpty(toRoad2) || !android.text.TextUtils.equals(toRoad1, toRoad2)) {
            return false;
        }
        java.lang.String signPostText1 = getSignpostText(first.getSignpost(), first, null, null);
        java.lang.String signPostText2 = getSignpostText(second.getSignpost(), second, null, null);
        if (android.text.TextUtils.isEmpty(signPostText1) || android.text.TextUtils.isEmpty(signPostText2) || !android.text.TextUtils.equals(signPostText1, signPostText2)) {
            return false;
        }
        return true;
    }

    public static java.lang.String getSignpostText(com.here.android.mpa.routing.Signpost signpost, com.here.android.mpa.routing.Maneuver maneuver, com.navdy.hud.app.maps.here.HereMapUtil.SignPostInfo info, java.util.ArrayList<java.lang.String> parsedSignpost) {
        java.lang.String sb;
        if (signpost == null) {
            return null;
        }
        synchronized (signPostBuilder) {
            signPostBuilder.setLength(0);
            signPostBuilder2.setLength(0);
            signPostSet.clear();
            java.lang.String exitNumber = signpost.getExitNumber();
            java.lang.String exitText = signpost.getExitText();
            boolean hasExitNumber = false;
            boolean leaveHighway = false;
            if (!android.text.TextUtils.isEmpty(exitNumber)) {
                if (maneuver.getAction() != com.here.android.mpa.routing.Maneuver.Action.LEAVE_HIGHWAY) {
                    signPostBuilder.append(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.EXIT_NUMBER);
                    signPostBuilder.append(" ");
                    hasExitNumber = false;
                    leaveHighway = true;
                } else {
                    hasExitNumber = true;
                }
                signPostBuilder.append(exitNumber);
                if (parsedSignpost != null) {
                    parsedSignpost.add(exitNumber);
                }
            }
            if (!android.text.TextUtils.isEmpty(exitText)) {
                if (needSeparator(signPostBuilder)) {
                    signPostBuilder.append(" ");
                }
                signPostBuilder.append(exitText);
                if (parsedSignpost != null) {
                    parsedSignpost.add(exitText);
                }
            }
            if (hasExitNumber) {
                signPostBuilder.append(" ");
            }
            for (com.here.android.mpa.routing.Signpost.LocalizedLabel label : signpost.getExitDirections()) {
                if (label.getLanguage().equalsIgnoreCase(TBT_ISO3_LANG_CODE)) {
                    java.lang.String routeName = label.getRouteName();
                    java.lang.String direction = label.getRouteDirection();
                    java.lang.String text = label.getText();
                    if (!android.text.TextUtils.isEmpty(routeName)) {
                        if (info != null) {
                            info.hasNumberInSignPost = true;
                        }
                        if (leaveHighway) {
                            leaveHighway = false;
                            if (needSeparator(signPostBuilder)) {
                                signPostBuilder.append(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.SLASH);
                            }
                        }
                        if (needSeparator(signPostBuilder)) {
                            signPostBuilder.append(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.SLASH);
                        }
                        signPostBuilder.append(routeName);
                    }
                    if (!android.text.TextUtils.isEmpty(direction)) {
                        if (info != null && info.hasNumberInSignPost) {
                            signPostBuilder.append(" ");
                        }
                        if (parsedSignpost != null && !android.text.TextUtils.isEmpty(routeName)) {
                            parsedSignpost.add(routeName + " " + direction);
                        }
                        signPostBuilder.append(direction);
                    }
                    if (!android.text.TextUtils.isEmpty(text) && !signPostSet.contains(text)) {
                        if (info != null) {
                            info.hasNameInSignPost = true;
                        }
                        if (needSeparator(signPostBuilder2)) {
                            signPostBuilder2.append(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.SLASH);
                        }
                        signPostBuilder2.append(text);
                        signPostSet.add(text);
                    }
                }
            }
            if (signPostBuilder2.length() > 0) {
                if (needSeparator(signPostBuilder)) {
                    signPostBuilder.append(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.SLASH);
                }
                signPostBuilder.append(signPostBuilder2.toString());
            }
            sb = signPostBuilder.toString();
        }
        return sb;
    }

    public static boolean needSeparator(java.lang.StringBuilder builder) {
        if (builder == null) {
            return false;
        }
        int len = builder.length();
        if (len == 0) {
            return false;
        }
        char ch = builder.charAt(len - 1);
        if (ch == '/' || ch == ' ') {
            return false;
        }
        return true;
    }

    public static boolean isHighwayAction(com.here.android.mpa.routing.Maneuver maneuver) {
        if (maneuver == null) {
            return false;
        }
        com.here.android.mpa.routing.Maneuver.Action action = maneuver.getAction();
        if (action == null) {
            return false;
        }
        switch (com.navdy.hud.app.maps.here.HereMapUtil.Anon2.$SwitchMap$com$here$android$mpa$routing$Maneuver$Action[action.ordinal()]) {
            case 3:
            case 4:
            case 5:
                return true;
            default:
                return false;
        }
    }

    public static java.lang.String getAllRoadNames(java.util.List<com.here.android.mpa.routing.Maneuver> maneuvers, int startOffset) {
        if (maneuvers == null || startOffset < 0) {
            return "";
        }
        int len = maneuvers.size();
        if (startOffset >= len) {
            return "";
        }
        java.lang.StringBuilder builder = new java.lang.StringBuilder();
        for (int i = startOffset; i < len; i++) {
            builder.append("[");
            builder.append(getRoadName((com.here.android.mpa.routing.Maneuver) maneuvers.get(i)));
            builder.append("]");
            builder.append(" ");
        }
        return builder.toString();
    }

    public static long getManeuverDriveTime(com.here.android.mpa.routing.Maneuver maneuver, com.navdy.hud.app.maps.here.HereMapUtil.LongContainer distance) {
        if (maneuver == null) {
            return 0;
        }
        java.util.List<com.here.android.mpa.common.RoadElement> roadElements = maneuver.getRoadElements();
        if (roadElements == null || roadElements.size() == 0) {
            return 0;
        }
        long time = 0;
        if (distance != null) {
            distance.val = 0;
        }
        for (com.here.android.mpa.common.RoadElement roadElement : roadElements) {
            if (roadElement != null && roadElement.getDefaultSpeed() > 0.0f) {
                double length = roadElement.getGeometryLength();
                time += java.lang.Math.round(length / ((double) roadElement.getDefaultSpeed()));
                if (distance != null) {
                    distance.val = (long) (((double) distance.val) + length);
                }
            }
        }
        return time;
    }

    public static boolean isInTunnel(com.here.android.mpa.common.RoadElement roadElement) {
        if (roadElement != null) {
            java.util.EnumSet<com.here.android.mpa.common.RoadElement.Attribute> attributes = roadElement.getAttributes();
            if (attributes != null) {
                return attributes.contains(com.here.android.mpa.common.RoadElement.Attribute.TUNNEL);
            }
        }
        return false;
    }

    public static double roundToN(double val, int N) {
        return ((double) ((long) (((double) N) * val))) / ((double) N);
    }

    public static int findManeuverIndex(java.util.List<com.here.android.mpa.routing.Maneuver> list, com.here.android.mpa.routing.Maneuver maneuver) {
        if (list == null || maneuver == null) {
            return -1;
        }
        int len = list.size();
        for (int i = 0; i < len; i++) {
            if (areManeuverEqual((com.here.android.mpa.routing.Maneuver) list.get(i), maneuver)) {
                return i;
            }
        }
        return -1;
    }

    public static java.util.List<com.here.android.mpa.routing.RouteElement> getAheadRouteElements(com.here.android.mpa.routing.Route route, com.here.android.mpa.common.RoadElement currentRoadElement, com.here.android.mpa.routing.Maneuver prev, com.here.android.mpa.routing.Maneuver next) {
        if (route == null || currentRoadElement == null || next == null) {
            sLogger.i("getAheadRouteElements:invalid arg");
            return null;
        }
        java.util.List<com.here.android.mpa.routing.Maneuver> maneuvers = route.getManeuvers();
        int index = findManeuverIndex(maneuvers, next);
        if (index == -1) {
            sLogger.i("getAheadRouteElements:maneuver index not found");
            return null;
        }
        sLogger.i("getAheadRouteElements:found maneuver index:" + index + " total:" + maneuvers.size());
        java.util.List<com.here.android.mpa.routing.RouteElement> aheadRouteElements = new java.util.ArrayList<>();
        if (prev != null) {
            java.util.List<com.here.android.mpa.routing.RouteElement> routeElements = prev.getRouteElements();
            if (routeElements != null && routeElements.size() > 0) {
                int len = routeElements.size();
                boolean found = false;
                for (int i = 0; i < len; i++) {
                    com.here.android.mpa.routing.RouteElement routeElement = (com.here.android.mpa.routing.RouteElement) routeElements.get(i);
                    if (!found) {
                        com.here.android.mpa.common.RoadElement re = routeElement.getRoadElement();
                        if (re != null && re.equals(currentRoadElement)) {
                            found = true;
                        }
                    } else {
                        aheadRouteElements.add(routeElement);
                    }
                }
            }
        }
        int len2 = maneuvers.size();
        for (int i2 = index; i2 < len2; i2++) {
            aheadRouteElements.addAll(((com.here.android.mpa.routing.Maneuver) maneuvers.get(i2)).getRouteElements());
        }
        return aheadRouteElements;
    }

    public static com.here.android.mpa.routing.Maneuver getLastManeuver(java.util.List<com.here.android.mpa.routing.Maneuver> list) {
        if (list == null || list.size() <= 1) {
            return null;
        }
        return (com.here.android.mpa.routing.Maneuver) list.get(list.size() - 2);
    }

    public static long getDistanceToRoadElement(com.here.android.mpa.common.RoadElement from, com.here.android.mpa.common.RoadElement to, java.util.List<com.here.android.mpa.routing.Maneuver> maneuvers) {
        if (from == null || to == null || maneuvers == null) {
            return -1;
        }
        long distance = -1;
        boolean fromFound = false;
        int maneuverLen = maneuvers.size();
        for (int i = 0; i < maneuverLen; i++) {
            java.util.List<com.here.android.mpa.common.RoadElement> roadElements = ((com.here.android.mpa.routing.Maneuver) maneuvers.get(i)).getRoadElements();
            int elementLen = roadElements.size();
            for (int j = 0; j < elementLen; j++) {
                com.here.android.mpa.common.RoadElement re = (com.here.android.mpa.common.RoadElement) roadElements.get(j);
                if (!fromFound) {
                    if (re.equals(from)) {
                        fromFound = true;
                        distance = (long) re.getGeometryLength();
                    }
                } else if (re.equals(to)) {
                    return distance;
                } else {
                    distance += (long) re.getGeometryLength();
                }
            }
        }
        return -1;
    }

    public static void loadImage(com.here.android.mpa.common.Image image, int width, int height, com.navdy.hud.app.maps.here.HereMapUtil.IImageLoadCallback callback) {
        if (image == null || callback == null || width <= 0 || height <= 0) {
            throw new java.lang.IllegalArgumentException();
        }
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.maps.here.HereMapUtil.Anon1(image, width, height, callback), 1);
    }

    public static boolean isSavedRoute(com.navdy.service.library.events.navigation.NavigationRouteRequest request) {
        if (request == null || request.routeAttributes == null || !request.routeAttributes.contains(com.navdy.service.library.events.navigation.NavigationRouteRequest.RouteAttribute.ROUTE_ATTRIBUTE_SAVED_ROUTE)) {
            return false;
        }
        return true;
    }

    public static boolean isGeoCoordinateEquals(com.here.android.mpa.common.GeoCoordinate g1, com.here.android.mpa.common.GeoCoordinate g2) {
        if (g1.getLatitude() == g2.getLatitude() && g1.getLongitude() == g2.getLongitude()) {
            return true;
        }
        return false;
    }

    static float roundToIntegerStep(int minStep, float value) {
        return (float) (minStep * java.lang.Math.round(value / ((float) minStep)));
    }

    static boolean isRouteFileStale(java.io.File f) {
        long l = java.lang.System.currentTimeMillis() - f.lastModified();
        if (l >= STALE_ROUTE_TIME || l < 0) {
            return true;
        }
        return false;
    }

    public static void generateRouteIcons(java.lang.String id, java.lang.String label, com.here.android.mpa.routing.Route route) {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        try {
            android.content.Context context = com.navdy.hud.app.HudApplication.getAppContext();
            java.lang.String path = com.navdy.hud.app.storage.PathManager.getInstance().getMapsPartitionPath() + java.io.File.separator + "routeicons" + java.io.File.separator + id + com.navdy.service.library.util.MusicDataUtils.ALTERNATE_SEPARATOR + com.navdy.hud.app.util.GenericUtil.normalizeToFilename(label);
            sLogger.v("generateRouteIcons: " + path);
            java.io.File f = new java.io.File(path);
            if (f.exists()) {
                com.navdy.service.library.util.IOUtils.deleteDirectory(context, f);
            }
            f.mkdirs();
            int iconCount = 0;
            for (com.here.android.mpa.routing.Maneuver maneuver : route.getManeuvers()) {
                com.here.android.mpa.common.Image icon = maneuver.getNextRoadImage();
                if (icon != null) {
                    int w = (int) icon.getWidth();
                    int h = (int) icon.getHeight();
                    sLogger.v("generateRouteIcons: w=" + w + " h=" + h);
                    android.graphics.Bitmap bitmap = icon.getBitmap(w * 3, h * 3);
                    if (bitmap != null) {
                        java.lang.String filename = "man_" + com.navdy.hud.app.util.GenericUtil.normalizeToFilename(getNextRoadName(maneuver)) + com.navdy.service.library.util.MusicDataUtils.ALTERNATE_SEPARATOR + w + com.navdy.service.library.util.MusicDataUtils.ALTERNATE_SEPARATOR + h + ".png";
                        java.io.ByteArrayOutputStream bout = new java.io.ByteArrayOutputStream();
                        bitmap.compress(android.graphics.Bitmap.CompressFormat.PNG, 100, bout);
                        com.navdy.service.library.util.IOUtils.copyFile(path + java.io.File.separator + filename, bout.toByteArray());
                        iconCount++;
                        sLogger.v("icon generated");
                    }
                }
            }
            sLogger.v("total icons generated:" + iconCount);
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }

    public static android.text.SpannableStringBuilder getEtaString(com.here.android.mpa.routing.Route route, java.lang.String routeId, boolean addSpaceBetweenUnits) {
        android.text.SpannableStringBuilder tripDuration = new android.text.SpannableStringBuilder();
        if (route != null) {
            java.util.Date ttaDate = getRouteTtaDate(route);
            long duration = 0;
            if (ttaDate != null) {
                duration = ttaDate.getTime() - java.lang.System.currentTimeMillis();
            } else {
                sLogger.i("tta date is null for route:" + routeId);
            }
            if (duration > 0) {
                long minutes = java.util.concurrent.TimeUnit.MILLISECONDS.toMinutes(duration);
                if (minutes < 60) {
                    formatString(tripDuration, java.lang.String.valueOf(minutes), sp_26_1, min, sp_20_b_1, null, null, null, null, true);
                } else {
                    long hours = java.util.concurrent.TimeUnit.MILLISECONDS.toHours(duration);
                    long minutes2 = java.util.concurrent.TimeUnit.MILLISECONDS.toMinutes(duration - java.util.concurrent.TimeUnit.HOURS.toMillis(hours));
                    if (hours < 10) {
                        formatString(tripDuration, java.lang.String.valueOf(hours), sp_26_1, hr, sp_20_b_1, java.lang.String.valueOf(minutes2), sp_26_2, min_short, sp_20_b_2, addSpaceBetweenUnits);
                    } else if (hours < 24) {
                        formatString(tripDuration, java.lang.String.valueOf(hours), sp_24_1, hr, sp_20_b_1, java.lang.String.valueOf(minutes2), sp_24_2, min_short, sp_20_b_2, addSpaceBetweenUnits);
                    } else {
                        long days = java.util.concurrent.TimeUnit.MILLISECONDS.toDays(duration);
                        long hours2 = java.util.concurrent.TimeUnit.MILLISECONDS.toHours(duration - java.util.concurrent.TimeUnit.DAYS.toMillis(days));
                        if (days == 1) {
                            formatString(tripDuration, java.lang.String.valueOf(days), sp_26_1, day, sp_20_b_1, java.lang.String.valueOf(hours2), sp_26_2, hr, sp_20_b_2, addSpaceBetweenUnits);
                        } else {
                            formatString(tripDuration, java.lang.String.valueOf(days), sp_24_1, day, sp_20_b_1, java.lang.String.valueOf(hours2), sp_24_2, hr, sp_20_b_2, addSpaceBetweenUnits);
                        }
                    }
                }
            }
        }
        return tripDuration;
    }

    private static void formatString(android.text.SpannableStringBuilder builder, java.lang.String s1, android.text.style.TextAppearanceSpan s1Span, java.lang.String s2, android.text.style.TextAppearanceSpan s2Span, java.lang.String s3, android.text.style.TextAppearanceSpan s3Span, java.lang.String s4, android.text.style.TextAppearanceSpan s4Span, boolean addSpaceBetweenUnits) {
        if (s1 != null) {
            builder.append(s1);
            builder.setSpan(s1Span, 0, builder.length(), 33);
        }
        if (s2 != null) {
            if (addSpaceBetweenUnits) {
                builder.append(" ");
            }
            int offset = builder.length();
            builder.append(s2);
            builder.setSpan(s2Span, offset, builder.length(), 33);
        }
        if (s3 != null) {
            int offset2 = builder.length();
            builder.append(" ");
            builder.append(s3);
            builder.setSpan(s3Span, offset2, builder.length(), 33);
        }
        if (s4 != null) {
            if (addSpaceBetweenUnits) {
                builder.append(" ");
            }
            int offset3 = builder.length();
            builder.append(s4);
            builder.setSpan(s4Span, offset3, builder.length(), 33);
        }
    }

    public static java.lang.String getCurrentTtaStringWithDestination() {
        try {
            if (!com.navdy.hud.app.maps.here.HereMapsManager.getInstance().isInitialized()) {
                return null;
            }
            com.navdy.hud.app.maps.here.HereNavigationManager hereNavigationManager = com.navdy.hud.app.maps.here.HereNavigationManager.getInstance();
            if (!hereNavigationManager.isNavigationModeOn()) {
                return null;
            }
            java.lang.String label = hereNavigationManager.getDestinationLabel();
            if (android.text.TextUtils.isEmpty(label)) {
                label = hereNavigationManager.getDestinationStreetAddress();
                if (android.text.TextUtils.isEmpty(label)) {
                    label = null;
                }
            }
            android.content.res.Resources resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
            if (!com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().hasArrived()) {
                java.lang.String tta = getCurrentTta();
                if (tta == null) {
                    return null;
                }
                if (label == null) {
                    return tta;
                }
                return com.navdy.hud.app.HudApplication.getAppContext().getString(com.navdy.hud.app.R.string.mm_active_trip_eta, new java.lang.Object[]{tta, label});
            } else if (label == null) {
                return resources.getString(com.navdy.hud.app.R.string.mm_active_trip_arrived_no_label);
            } else {
                return resources.getString(com.navdy.hud.app.R.string.mm_active_trip_arrived, new java.lang.Object[]{label});
            }
        } catch (Throwable t) {
            sLogger.e(t);
            return null;
        }
    }

    public static java.lang.String getCurrentTta() {
        try {
            if (!com.navdy.hud.app.maps.here.HereMapsManager.getInstance().isInitialized() || !com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().isNavigationModeOn()) {
                return null;
            }
            com.navdy.hud.app.ui.component.homescreen.HomeScreenView view = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getUiStateManager().getHomescreenView();
            if (view != null) {
                return view.getEtaView().getLastTta();
            }
            return null;
        } catch (Throwable t) {
            sLogger.e(t);
            return null;
        }
    }

    public static java.lang.String convertDateToTta(java.util.Date etaDate) {
        if (!isValidEtaDate(etaDate)) {
            return null;
        }
        long duration = etaDate.getTime() - java.lang.System.currentTimeMillis();
        if (duration <= 0) {
            return null;
        }
        long minutes = java.util.concurrent.TimeUnit.MILLISECONDS.toMinutes(duration);
        if (minutes < 60) {
            return java.lang.String.valueOf(minutes) + " " + min;
        }
        long hours = java.util.concurrent.TimeUnit.MILLISECONDS.toHours(duration);
        long minutes2 = java.util.concurrent.TimeUnit.MILLISECONDS.toMinutes(duration - java.util.concurrent.TimeUnit.HOURS.toMillis(hours));
        if (hours < 24) {
            return java.lang.String.valueOf(hours) + " " + hr + " " + java.lang.String.valueOf(minutes2) + " " + min_short;
        }
        long days = java.util.concurrent.TimeUnit.MILLISECONDS.toDays(duration);
        return java.lang.String.valueOf(days) + " " + day + " " + java.lang.String.valueOf(java.util.concurrent.TimeUnit.MILLISECONDS.toHours(duration - java.util.concurrent.TimeUnit.DAYS.toMillis(days))) + " " + hr;
    }
}
