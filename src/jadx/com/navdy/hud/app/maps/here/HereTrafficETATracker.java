package com.navdy.hud.app.maps.here;

public class HereTrafficETATracker extends com.here.android.mpa.guidance.NavigationManager.NavigationManagerEventListener {
    private static final long ETA_THRESHOLD = java.util.concurrent.TimeUnit.MINUTES.toMillis(10);
    private static final long INVALID = -1;
    private static final long REFRESH_ETA_INTERVAL_MILLIS = java.util.concurrent.TimeUnit.SECONDS.toMillis(60);
    /* access modifiers changed from: private */
    public long baseEta;
    private final com.squareup.otto.Bus bus;
    /* access modifiers changed from: private */
    public java.lang.Runnable dismissEtaRunnable = new com.navdy.hud.app.maps.here.HereTrafficETATracker.Anon3();
    /* access modifiers changed from: private */
    public final android.os.Handler handler;
    /* access modifiers changed from: private */
    public final com.navdy.hud.app.maps.here.HereNavController navController;
    private java.lang.Runnable navigationModeChangeBkRunnable = new com.navdy.hud.app.maps.here.HereTrafficETATracker.Anon4();
    /* access modifiers changed from: private */
    public java.lang.Runnable refreshEtaBkRunnable = new com.navdy.hud.app.maps.here.HereTrafficETATracker.Anon2();
    /* access modifiers changed from: private */
    public java.lang.Runnable refreshEtaRunnable = new com.navdy.hud.app.maps.here.HereTrafficETATracker.Anon1();
    /* access modifiers changed from: private */
    public final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.maps.here.HereTrafficETATracker.class);

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            com.navdy.service.library.task.TaskManager.getInstance().execute(com.navdy.hud.app.maps.here.HereTrafficETATracker.this.refreshEtaBkRunnable, 3);
        }
    }

    class Anon2 implements java.lang.Runnable {
        Anon2() {
        }

        public void run() {
            com.navdy.hud.app.maps.here.HereTrafficETATracker.this.etaUpdate();
        }
    }

    class Anon3 implements java.lang.Runnable {
        Anon3() {
        }

        public void run() {
            com.navdy.hud.app.maps.here.HereTrafficETATracker.this.sendTrafficDelayDismissEvent();
        }
    }

    class Anon4 implements java.lang.Runnable {
        Anon4() {
        }

        public void run() {
            com.navdy.hud.app.maps.here.HereNavController.State state = com.navdy.hud.app.maps.here.HereTrafficETATracker.this.navController.getState();
            if (state == com.navdy.hud.app.maps.here.HereNavController.State.NAVIGATING) {
                java.util.Date etaDate = com.navdy.hud.app.maps.here.HereTrafficETATracker.this.navController.getEta(true, com.here.android.mpa.routing.Route.TrafficPenaltyMode.OPTIMAL);
                if (com.navdy.hud.app.maps.here.HereMapUtil.isValidEtaDate(etaDate)) {
                    com.navdy.hud.app.maps.here.HereTrafficETATracker.this.baseEta = etaDate.getTime();
                    com.navdy.hud.app.maps.here.HereTrafficETATracker.this.sLogger.v("got baseEta from eta:" + com.navdy.hud.app.maps.here.HereTrafficETATracker.this.baseEta);
                } else {
                    java.util.Date etaDate2 = com.navdy.hud.app.maps.here.HereTrafficETATracker.this.navController.getTtaDate(true, com.here.android.mpa.routing.Route.TrafficPenaltyMode.OPTIMAL);
                    if (!com.navdy.hud.app.maps.here.HereMapUtil.isValidEtaDate(etaDate2)) {
                        com.navdy.hud.app.maps.here.HereTrafficETATracker.this.baseEta = -1;
                        com.navdy.hud.app.maps.here.HereTrafficETATracker.this.sendTrafficDelayDismissEvent();
                        com.navdy.hud.app.maps.here.HereTrafficETATracker.this.sLogger.i("no baseEta");
                    } else {
                        com.navdy.hud.app.maps.here.HereTrafficETATracker.this.baseEta = etaDate2.getTime();
                        com.navdy.hud.app.maps.here.HereTrafficETATracker.this.sLogger.v("got baseEta from eta--tta:" + com.navdy.hud.app.maps.here.HereTrafficETATracker.this.baseEta);
                    }
                }
                com.navdy.hud.app.maps.here.HereTrafficETATracker.this.refreshEta();
                return;
            }
            com.navdy.hud.app.maps.here.HereTrafficETATracker.this.sLogger.i("no baseEta, not navigation:" + state);
            com.navdy.hud.app.maps.here.HereTrafficETATracker.this.baseEta = -1;
            com.navdy.hud.app.maps.here.HereTrafficETATracker.this.handler.removeCallbacks(com.navdy.hud.app.maps.here.HereTrafficETATracker.this.refreshEtaRunnable);
            com.navdy.hud.app.maps.here.HereTrafficETATracker.this.handler.removeCallbacks(com.navdy.hud.app.maps.here.HereTrafficETATracker.this.dismissEtaRunnable);
            com.navdy.hud.app.maps.here.HereTrafficETATracker.this.sendTrafficDelayDismissEvent();
        }
    }

    public HereTrafficETATracker(com.navdy.hud.app.maps.here.HereNavController navController2, com.squareup.otto.Bus bus2) {
        this.navController = navController2;
        this.bus = bus2;
        this.handler = new android.os.Handler(android.os.Looper.getMainLooper());
        this.sLogger.v("ctor");
    }

    /* access modifiers changed from: private */
    public void refreshEta() {
        this.sLogger.v("posting refresh ETA in 60 sec...");
        this.handler.removeCallbacks(this.refreshEtaRunnable);
        this.handler.postDelayed(this.refreshEtaRunnable, REFRESH_ETA_INTERVAL_MILLIS);
    }

    /* access modifiers changed from: private */
    public void etaUpdate() {
        this.sLogger.v("etaUpdate");
        if (this.baseEta == -1) {
            this.sLogger.v("base ETA is invalid");
            return;
        }
        java.util.Date etaDate = this.navController.getEta(true, com.here.android.mpa.routing.Route.TrafficPenaltyMode.OPTIMAL);
        if (com.navdy.hud.app.maps.here.HereMapUtil.isValidEtaDate(etaDate)) {
            long newEta = etaDate.getTime();
            long etaDiff = newEta - this.baseEta;
            if (etaDiff > ETA_THRESHOLD) {
                this.sLogger.v("ETA threshold delay, triggering delay:" + etaDiff);
                this.baseEta = newEta;
                this.bus.post(new com.navdy.hud.app.maps.MapEvents.TrafficDelayEvent(etaDiff / 1000));
                this.handler.removeCallbacks(this.dismissEtaRunnable);
                this.handler.postDelayed(this.dismissEtaRunnable, ETA_THRESHOLD);
            } else {
                this.sLogger.v("ETA threshold ok:" + etaDiff);
            }
        }
        refreshEta();
    }

    public void onRouteUpdated(com.here.android.mpa.routing.Route route) {
        sendTrafficDelayDismissEvent();
    }

    public void onNavigationModeChanged() {
        com.navdy.service.library.task.TaskManager.getInstance().execute(this.navigationModeChangeBkRunnable, 3);
    }

    /* access modifiers changed from: private */
    public void sendTrafficDelayDismissEvent() {
        this.bus.post(new com.navdy.hud.app.maps.MapEvents.TrafficDelayDismissEvent());
    }
}
