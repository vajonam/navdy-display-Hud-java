package com.navdy.hud.app.maps.here;

public class HereLocationFixManager {
    private static final java.text.SimpleDateFormat DATE_FORMAT = new java.text.SimpleDateFormat("yyyy-MM-dd'_'HH_mm_ss.SSS", java.util.Locale.US);
    /* access modifiers changed from: private */
    public static final byte[] RECORD_MARKER = "<< Marker >>\n".getBytes();
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.maps.here.HereLocationFixManager.class);
    private android.location.LocationListener androidGpsLocationListener = null;
    /* access modifiers changed from: private */
    public final com.squareup.otto.Bus bus;
    /* access modifiers changed from: private */
    public java.lang.Runnable checkLocationFix = new com.navdy.hud.app.maps.here.HereLocationFixManager.Anon1();
    private long counter;
    private android.location.LocationListener debugTTSLocationListener = null;
    /* access modifiers changed from: private */
    public boolean firstLocationFixCheck = true;
    private com.here.android.mpa.common.GeoPosition geoPosition;
    /* access modifiers changed from: private */
    public android.os.Handler handler = new android.os.Handler(android.os.Looper.getMainLooper());
    private volatile boolean hereGpsSignal;
    /* access modifiers changed from: private */
    public volatile boolean isRecording;
    /* access modifiers changed from: private */
    public android.location.Location lastAndroidLocation;
    /* access modifiers changed from: private */
    public long lastAndroidLocationPostTime;
    /* access modifiers changed from: private */
    public long lastAndroidLocationTime;
    private com.navdy.hud.app.device.gps.GpsUtils.GpsSwitch lastGpsSwitchEvent;
    private long lastHerelocationUpdateTime;
    private volatile long lastLocationTime;
    /* access modifiers changed from: private */
    public volatile boolean locationFix;
    private final android.location.LocationListener locationListener = new com.navdy.hud.app.maps.here.HereLocationFixManager.Anon2();
    private com.here.android.mpa.common.PositioningManager.LocationMethod locationMethod;
    /* access modifiers changed from: private */
    public com.here.android.mpa.mapping.MapMarker rawLocationMarker;
    /* access modifiers changed from: private */
    public boolean rawLocationMarkerAdded;
    /* access modifiers changed from: private */
    public java.io.FileOutputStream recordingFile;
    /* access modifiers changed from: private */
    public volatile java.lang.String recordingLabel;
    private final com.navdy.hud.app.manager.SpeedManager speedManager;

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            try {
                long diff = android.os.SystemClock.elapsedRealtime() - com.navdy.hud.app.maps.here.HereLocationFixManager.this.lastAndroidLocationTime;
                if (com.navdy.hud.app.maps.here.HereLocationFixManager.this.locationFix) {
                    if (diff >= com.navdy.hud.app.device.gps.GpsManager.MINIMUM_DRIVE_TIME) {
                        com.navdy.hud.app.maps.here.HereLocationFixManager.sLogger.i("lost location fix[" + diff + "]");
                        com.navdy.hud.app.maps.here.HereLocationFixManager.this.locationFix = false;
                        com.navdy.hud.app.maps.here.HereLocationFixManager.this.bus.post(new com.navdy.hud.app.maps.MapEvents.LocationFix(false, false, false));
                    } else if (com.navdy.hud.app.maps.here.HereLocationFixManager.sLogger.isLoggable(2)) {
                        com.navdy.hud.app.maps.here.HereLocationFixManager.sLogger.v("still has location fix [" + diff + "]");
                    }
                } else if (com.navdy.hud.app.maps.here.HereLocationFixManager.this.lastAndroidLocation == null || diff > com.navdy.hud.app.device.gps.GpsManager.MINIMUM_DRIVE_TIME) {
                    if (com.navdy.hud.app.maps.here.HereLocationFixManager.sLogger.isLoggable(2)) {
                        com.navdy.hud.app.maps.here.HereLocationFixManager.sLogger.v("still don't have location fix [" + diff + "]");
                    }
                    if (com.navdy.hud.app.maps.here.HereLocationFixManager.this.firstLocationFixCheck) {
                        com.navdy.hud.app.maps.here.HereLocationFixManager.this.bus.post(new com.navdy.hud.app.maps.MapEvents.LocationFix(false, false, false));
                    }
                } else {
                    com.navdy.hud.app.maps.here.HereLocationFixManager.sLogger.v("got location fix [" + diff + "] " + com.navdy.hud.app.maps.here.HereLocationFixManager.this.lastAndroidLocation);
                    com.navdy.hud.app.maps.here.HereLocationFixManager.this.locationFix = true;
                    com.navdy.hud.app.maps.here.HereLocationFixManager.this.bus.post(com.navdy.hud.app.maps.here.HereLocationFixManager.this.getLocationFixEvent());
                }
            } catch (Throwable t) {
                com.navdy.hud.app.maps.here.HereLocationFixManager.sLogger.e(t);
            } finally {
                com.navdy.hud.app.maps.here.HereLocationFixManager.this.handler.postDelayed(com.navdy.hud.app.maps.here.HereLocationFixManager.this.checkLocationFix, 1000);
                com.navdy.hud.app.maps.here.HereLocationFixManager.this.firstLocationFixCheck = false;
            }
        }
    }

    class Anon2 implements android.location.LocationListener {
        Anon2() {
        }

        public void onLocationChanged(android.location.Location location) {
            if (!com.navdy.hud.app.maps.here.HereLocationFixManager.this.locationFix) {
                com.navdy.hud.app.maps.here.HereLocationFixManager.this.setMaptoLocation(location);
            }
            if (com.navdy.hud.app.device.gps.GpsUtils.isDebugRawGpsPosEnabled()) {
                try {
                    if (com.navdy.hud.app.maps.here.HereLocationFixManager.this.rawLocationMarker != null && com.navdy.hud.app.maps.here.HereLocationFixManager.this.rawLocationMarkerAdded) {
                        com.navdy.hud.app.maps.here.HereLocationFixManager.this.rawLocationMarker.setCoordinate(new com.here.android.mpa.common.GeoCoordinate(location.getLatitude(), location.getLongitude()));
                    }
                } catch (Throwable t) {
                    com.navdy.hud.app.maps.here.HereLocationFixManager.sLogger.e(t);
                }
            }
            if (com.navdy.hud.app.maps.here.HereLocationFixManager.this.isRecording) {
                com.navdy.hud.app.maps.here.HereLocationFixManager.this.recordRawLocation(location);
            }
            com.navdy.hud.app.maps.here.HereLocationFixManager.this.lastAndroidLocationTime = android.os.SystemClock.elapsedRealtime();
            com.navdy.hud.app.maps.here.HereLocationFixManager.this.lastAndroidLocation = location;
            if (com.navdy.hud.app.maps.here.HereLocationFixManager.this.lastAndroidLocationTime - com.navdy.hud.app.maps.here.HereLocationFixManager.this.lastAndroidLocationPostTime >= 1000) {
                if (!android.text.TextUtils.equals(location.getProvider(), com.navdy.hud.app.device.gps.GpsConstants.NAVDY_GPS_PROVIDER)) {
                    com.navdy.hud.app.maps.here.HereLocationFixManager.this.sendLocation(location);
                }
                com.navdy.hud.app.maps.here.HereLocationFixManager.this.lastAndroidLocationPostTime = com.navdy.hud.app.maps.here.HereLocationFixManager.this.lastAndroidLocationTime;
            }
        }

        public void onStatusChanged(java.lang.String provider, int status, android.os.Bundle extras) {
        }

        public void onProviderEnabled(java.lang.String provider) {
        }

        public void onProviderDisabled(java.lang.String provider) {
        }
    }

    class Anon3 implements android.location.LocationListener {
        boolean toastSent = false;
        final /* synthetic */ android.location.LocationManager val$locationManager;
        final /* synthetic */ long val$t1;

        class Anon1 implements java.lang.Runnable {
            Anon1() {
            }

            public void run() {
                com.navdy.hud.app.framework.voice.TTSUtils.debugShowGotUbloxFix();
            }
        }

        Anon3(android.location.LocationManager locationManager, long j) {
            this.val$locationManager = locationManager;
            this.val$t1 = j;
        }

        public void onLocationChanged(android.location.Location location) {
            if (this.toastSent) {
                com.navdy.hud.app.maps.here.HereLocationFixManager.sLogger.v("ublox first fix toast already posted");
                return;
            }
            this.val$locationManager.removeUpdates(this);
            this.toastSent = true;
            long t2 = android.os.SystemClock.elapsedRealtime() - this.val$t1;
            com.navdy.hud.app.maps.here.HereLocationFixManager.sLogger.v("got first u-blox fix, unregister (" + t2 + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.CLOSE_BRACKET);
            if (t2 < 10000) {
                com.navdy.hud.app.maps.here.HereLocationFixManager.this.handler.postDelayed(new com.navdy.hud.app.maps.here.HereLocationFixManager.Anon3.Anon1(), 10000 - t2);
            } else {
                com.navdy.hud.app.framework.voice.TTSUtils.debugShowGotUbloxFix();
            }
        }

        public void onStatusChanged(java.lang.String provider, int status, android.os.Bundle extras) {
        }

        public void onProviderEnabled(java.lang.String provider) {
        }

        public void onProviderDisabled(java.lang.String provider) {
        }
    }

    class Anon4 implements android.location.LocationListener {
        Anon4() {
        }

        public void onLocationChanged(android.location.Location location) {
            if (com.navdy.hud.app.maps.here.HereLocationFixManager.this.isRecording && !com.navdy.hud.app.util.DeviceUtil.isNavdyDevice()) {
                com.navdy.hud.app.maps.here.HereLocationFixManager.this.recordRawLocation(location);
            }
            com.navdy.hud.app.maps.here.HereLocationFixManager.this.sendLocation(location);
        }

        public void onStatusChanged(java.lang.String provider, int status, android.os.Bundle extras) {
        }

        public void onProviderEnabled(java.lang.String provider) {
        }

        public void onProviderDisabled(java.lang.String provider) {
        }
    }

    class Anon5 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.service.library.events.debug.StartDriveRecordingEvent val$event;

        Anon5(com.navdy.service.library.events.debug.StartDriveRecordingEvent startDriveRecordingEvent) {
            this.val$event = startDriveRecordingEvent;
        }

        public void run() {
            try {
                if (!com.navdy.hud.app.maps.here.HereLocationFixManager.this.isRecording) {
                    com.navdy.hud.app.maps.here.HereLocationFixManager.this.recordingLabel = this.val$event.label;
                    com.navdy.hud.app.maps.here.HereLocationFixManager.this.isRecording = true;
                    java.io.File driveLogsDir = com.navdy.hud.app.debug.DriveRecorder.getDriveLogsDir(com.navdy.hud.app.maps.here.HereLocationFixManager.this.recordingLabel);
                    if (!driveLogsDir.exists()) {
                        driveLogsDir.mkdirs();
                    }
                    java.io.File driveLogFile = new java.io.File(driveLogsDir, com.navdy.hud.app.maps.here.HereLocationFixManager.this.recordingLabel + ".here");
                    com.navdy.hud.app.maps.here.HereLocationFixManager.this.recordingFile = new java.io.FileOutputStream(driveLogFile);
                    com.navdy.hud.app.maps.here.HereLocationFixManager.sLogger.v("created recording file [" + com.navdy.hud.app.maps.here.HereLocationFixManager.this.recordingLabel + "] path:" + driveLogFile.getAbsolutePath());
                    return;
                }
                com.navdy.hud.app.maps.here.HereLocationFixManager.sLogger.v("already recording");
            } catch (Throwable t) {
                com.navdy.hud.app.maps.here.HereLocationFixManager.sLogger.e(t);
                com.navdy.hud.app.maps.here.HereLocationFixManager.this.isRecording = false;
                com.navdy.hud.app.maps.here.HereLocationFixManager.this.recordingLabel = null;
                com.navdy.service.library.util.IOUtils.closeStream(com.navdy.hud.app.maps.here.HereLocationFixManager.this.recordingFile);
                com.navdy.hud.app.maps.here.HereLocationFixManager.this.recordingFile = null;
            }
        }
    }

    class Anon6 implements java.lang.Runnable {
        Anon6() {
        }

        public void run() {
            try {
                if (com.navdy.hud.app.maps.here.HereLocationFixManager.this.isRecording) {
                    com.navdy.hud.app.maps.here.HereLocationFixManager.this.isRecording = false;
                    com.navdy.service.library.util.IOUtils.fileSync(com.navdy.hud.app.maps.here.HereLocationFixManager.this.recordingFile);
                    com.navdy.service.library.util.IOUtils.closeStream(com.navdy.hud.app.maps.here.HereLocationFixManager.this.recordingFile);
                    com.navdy.hud.app.maps.here.HereLocationFixManager.this.recordingFile = null;
                    com.navdy.hud.app.maps.here.HereLocationFixManager.sLogger.v("stopped recording file [" + com.navdy.hud.app.maps.here.HereLocationFixManager.this.recordingLabel + "]");
                    com.navdy.hud.app.maps.here.HereLocationFixManager.this.recordingLabel = null;
                    return;
                }
                com.navdy.hud.app.maps.here.HereLocationFixManager.sLogger.v("not recording");
            } catch (Throwable t) {
                com.navdy.hud.app.maps.here.HereLocationFixManager.sLogger.e(t);
            }
        }
    }

    class Anon7 implements java.lang.Runnable {
        final /* synthetic */ android.location.Location val$location;

        Anon7(android.location.Location location) {
            this.val$location = location;
        }

        public void run() {
            try {
                java.lang.String provider = this.val$location.getProvider();
                if (com.navdy.hud.app.device.gps.GpsConstants.NAVDY_GPS_PROVIDER.equals(provider)) {
                    provider = "gps";
                }
                com.navdy.hud.app.maps.here.HereLocationFixManager.this.recordingFile.write((this.val$location.getLatitude() + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA + this.val$location.getLongitude() + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA + this.val$location.getBearing() + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA + this.val$location.getSpeed() + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA + this.val$location.getAccuracy() + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA + this.val$location.getAltitude() + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA + this.val$location.getTime() + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA + "raw" + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA + provider + com.navdy.hud.app.framework.glance.GlanceConstants.NEWLINE).getBytes());
            } catch (Throwable t) {
                com.navdy.hud.app.maps.here.HereLocationFixManager.sLogger.e(t);
            }
        }
    }

    class Anon8 implements java.lang.Runnable {
        final /* synthetic */ com.here.android.mpa.common.GeoPosition val$geoPosition;
        final /* synthetic */ boolean val$isMapMatched;
        final /* synthetic */ com.here.android.mpa.common.PositioningManager.LocationMethod val$locationMethod;

        Anon8(com.here.android.mpa.common.GeoPosition geoPosition, com.here.android.mpa.common.PositioningManager.LocationMethod locationMethod, boolean z) {
            this.val$geoPosition = geoPosition;
            this.val$locationMethod = locationMethod;
            this.val$isMapMatched = z;
        }

        public void run() {
            try {
                com.here.android.mpa.common.GeoCoordinate coordinate = this.val$geoPosition.getCoordinate();
                java.lang.String additional = "";
                if (this.val$geoPosition instanceof com.here.android.mpa.common.MatchedGeoPosition) {
                    com.here.android.mpa.common.MatchedGeoPosition matchedGeoPosition = this.val$geoPosition;
                    additional = com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA + matchedGeoPosition.getMatchQuality() + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA + matchedGeoPosition.isExtrapolated() + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA + matchedGeoPosition.isOnStreet();
                }
                com.navdy.hud.app.maps.here.HereLocationFixManager.this.recordingFile.write((coordinate.getLatitude() + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA + coordinate.getLongitude() + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA + this.val$geoPosition.getHeading() + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA + this.val$geoPosition.getSpeed() + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA + this.val$geoPosition.getLatitudeAccuracy() + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA + coordinate.getAltitude() + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA + this.val$geoPosition.getTimestamp().getTime() + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA + "here" + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA + this.val$locationMethod.name() + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA + this.val$isMapMatched + additional + com.navdy.hud.app.framework.glance.GlanceConstants.NEWLINE).getBytes());
            } catch (Throwable t) {
                com.navdy.hud.app.maps.here.HereLocationFixManager.sLogger.e(t);
            }
        }
    }

    class Anon9 implements java.lang.Runnable {
        Anon9() {
        }

        public void run() {
            try {
                com.navdy.hud.app.maps.here.HereLocationFixManager.this.recordingFile.write(com.navdy.hud.app.maps.here.HereLocationFixManager.RECORD_MARKER);
            } catch (Throwable t) {
                com.navdy.hud.app.maps.here.HereLocationFixManager.sLogger.e(t);
            }
        }
    }

    HereLocationFixManager(com.squareup.otto.Bus bus2) {
        this.bus = bus2;
        this.speedManager = com.navdy.hud.app.manager.SpeedManager.getInstance();
        android.location.LocationManager locationManager = (android.location.LocationManager) com.navdy.hud.app.HudApplication.getAppContext().getSystemService("location");
        android.os.Looper locationReceiverLooper = com.navdy.hud.app.maps.here.HereMapsManager.getInstance().getBkLocationReceiverLooper();
        if (com.navdy.hud.app.framework.voice.TTSUtils.isDebugTTSEnabled() && com.navdy.hud.app.util.DeviceUtil.isNavdyDevice() && locationManager.getProvider("gps") != null) {
            this.debugTTSLocationListener = new com.navdy.hud.app.maps.here.HereLocationFixManager.Anon3(locationManager, android.os.SystemClock.elapsedRealtime());
            locationManager.requestLocationUpdates("gps", 0, 0.0f, this.debugTTSLocationListener, locationReceiverLooper);
        }
        android.location.Location last = null;
        if (locationManager.getProvider(com.navdy.hud.app.device.gps.GpsConstants.NAVDY_GPS_PROVIDER) != null) {
            last = locationManager.getLastKnownLocation(com.navdy.hud.app.device.gps.GpsConstants.NAVDY_GPS_PROVIDER);
            locationManager.requestLocationUpdates(com.navdy.hud.app.device.gps.GpsConstants.NAVDY_GPS_PROVIDER, 0, 0.0f, this.locationListener, locationReceiverLooper);
            sLogger.v("installed gps listener");
        }
        if (locationManager.getProvider("gps") != null) {
            this.androidGpsLocationListener = new com.navdy.hud.app.maps.here.HereLocationFixManager.Anon4();
            locationManager.requestLocationUpdates("gps", 0, 0.0f, this.androidGpsLocationListener, locationReceiverLooper);
        }
        sLogger.v("installed gps listener");
        if (locationManager.getProvider("network") != null) {
            if (last == null) {
                last = locationManager.getLastKnownLocation("network");
            }
            locationManager.requestLocationUpdates("network", 0, 0.0f, this.locationListener, locationReceiverLooper);
            sLogger.v("installed n/w listener");
        }
        sLogger.v("got last location from location manager:" + last);
        if (!this.locationFix && last != null) {
            sLogger.v("sending to map");
            setMaptoLocation(last);
        }
        this.bus.register(this);
        this.handler.postDelayed(this.checkLocationFix, 1000);
        com.navdy.service.library.events.debug.StartDriveRecordingEvent event = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getConnectionHandler().getDriverRecordingEvent();
        if (event != null) {
            onStartDriveRecording(event);
        }
        sLogger.v("initialized");
    }

    public void onHerePositionUpdated(com.here.android.mpa.common.PositioningManager.LocationMethod locationMethod2, com.here.android.mpa.common.GeoPosition geoPosition2, boolean isMapMatched) {
        if (sLogger.isLoggable(2)) {
            com.navdy.service.library.log.Logger logger = sLogger;
            java.lang.StringBuilder append = new java.lang.StringBuilder().append("onHerePositionUpdated [");
            long j = this.counter + 1;
            this.counter = j;
            logger.v(append.append(j).append("] geoPosition[").append(geoPosition2.getCoordinate()).append("] method [").append(locationMethod2.name()).append("] valid[").append(geoPosition2.isValid()).append("]").toString());
        }
        if (locationMethod2 != null && geoPosition2 != null) {
            this.locationMethod = locationMethod2;
            this.geoPosition = geoPosition2;
            this.lastHerelocationUpdateTime = android.os.SystemClock.elapsedRealtime();
            if (this.isRecording) {
                recordHereLocation(geoPosition2, locationMethod2, isMapMatched);
            }
        }
    }

    public boolean hasLocationFix() {
        return this.locationFix;
    }

    public com.here.android.mpa.common.GeoCoordinate getLastGeoCoordinate() {
        if (this.geoPosition == null) {
            return null;
        }
        return this.geoPosition.getCoordinate();
    }

    @com.squareup.otto.Subscribe
    public void onHereGpsEvent(com.navdy.hud.app.maps.MapEvents.GpsStatusChange event) {
        this.hereGpsSignal = event.connected;
        sLogger.v("here gps event connected [" + this.hereGpsSignal + "]");
    }

    @com.squareup.otto.Subscribe
    public void onGpsEventSwitch(com.navdy.hud.app.device.gps.GpsUtils.GpsSwitch event) {
        sLogger.v("Gps-Switch phone=" + event.usingPhone + " ublox=" + event.usingUblox);
        this.lastGpsSwitchEvent = event;
    }

    public void setMaptoLocation(android.location.Location location) {
        try {
            if (!this.locationFix && this.geoPosition == null) {
                sLogger.v("marking location fix:" + location);
                this.geoPosition = new com.here.android.mpa.common.GeoPosition(new com.here.android.mpa.common.GeoCoordinate(location.getLatitude(), location.getLongitude(), location.getAltitude()));
                this.locationMethod = com.here.android.mpa.common.PositioningManager.LocationMethod.NETWORK;
                this.lastHerelocationUpdateTime = android.os.SystemClock.elapsedRealtime();
                this.locationFix = true;
                this.bus.post(new com.navdy.hud.app.maps.MapEvents.LocationFix(true, true, false));
            }
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }

    @com.squareup.otto.Subscribe
    public void onStartDriveRecording(com.navdy.service.library.events.debug.StartDriveRecordingEvent event) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.maps.here.HereLocationFixManager.Anon5(event), 9);
    }

    @com.squareup.otto.Subscribe
    public void onStopDriveRecording(com.navdy.service.library.events.debug.StopDriveRecordingEvent event) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.maps.here.HereLocationFixManager.Anon6(), 9);
    }

    /* access modifiers changed from: private */
    public void recordRawLocation(android.location.Location location) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.maps.here.HereLocationFixManager.Anon7(location), 9);
    }

    private void recordHereLocation(com.here.android.mpa.common.GeoPosition geoPosition2, com.here.android.mpa.common.PositioningManager.LocationMethod locationMethod2, boolean isMapMatched) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.maps.here.HereLocationFixManager.Anon8(geoPosition2, locationMethod2, isMapMatched), 9);
    }

    public void recordMarker() {
        if (this.isRecording) {
            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.maps.here.HereLocationFixManager.Anon9(), 9);
        }
    }

    public boolean isRecording() {
        return this.isRecording;
    }

    public void addMarkers(com.navdy.hud.app.maps.here.HereMapController mapController) {
        if (com.navdy.hud.app.device.gps.GpsUtils.isDebugRawGpsPosEnabled()) {
            try {
                if (this.rawLocationMarker == null) {
                    this.rawLocationMarker = new com.here.android.mpa.mapping.MapMarker();
                    com.here.android.mpa.common.Image image = new com.here.android.mpa.common.Image();
                    image.setImageResource(com.navdy.hud.app.R.drawable.icon_position_raw_gps);
                    this.rawLocationMarker.setIcon(image);
                    sLogger.v("marker created");
                }
                if (!this.rawLocationMarkerAdded) {
                    mapController.addMapObject(this.rawLocationMarker);
                    this.rawLocationMarkerAdded = true;
                    sLogger.v("marker added");
                }
            } catch (Throwable t) {
                sLogger.e(t);
            }
        }
    }

    public void removeMarkers(com.navdy.hud.app.maps.here.HereMapController mapController) {
        if (com.navdy.hud.app.device.gps.GpsUtils.isDebugRawGpsPosEnabled() && this.rawLocationMarker != null && this.rawLocationMarkerAdded) {
            mapController.removeMapObject(this.rawLocationMarker);
            this.rawLocationMarkerAdded = false;
            sLogger.v("marker removed");
        }
    }

    public void shutdown() {
        android.location.LocationManager locationManager = (android.location.LocationManager) com.navdy.hud.app.HudApplication.getAppContext().getSystemService("location");
        locationManager.removeUpdates(this.locationListener);
        if (this.androidGpsLocationListener != null) {
            locationManager.removeUpdates(this.androidGpsLocationListener);
        }
        if (this.debugTTSLocationListener != null) {
            locationManager.removeUpdates(this.debugTTSLocationListener);
        }
    }

    public com.navdy.hud.app.maps.MapEvents.LocationFix getLocationFixEvent() {
        if (!this.locationFix) {
            return new com.navdy.hud.app.maps.MapEvents.LocationFix(false, false, false);
        }
        boolean phone = false;
        boolean gps = false;
        if (this.lastAndroidLocation != null) {
            if (android.text.TextUtils.equals(this.lastAndroidLocation.getProvider(), com.navdy.hud.app.device.gps.GpsConstants.NAVDY_GPS_PROVIDER)) {
                gps = true;
            } else {
                phone = true;
            }
        }
        return new com.navdy.hud.app.maps.MapEvents.LocationFix(true, phone, gps);
    }

    /* access modifiers changed from: private */
    public void sendLocation(android.location.Location location) {
        if (this.speedManager.setGpsSpeed(location.getSpeed(), location.getElapsedRealtimeNanos() / 1000000)) {
            this.bus.post(new com.navdy.hud.app.maps.MapEvents.GPSSpeedEvent());
        }
        this.lastLocationTime = android.os.SystemClock.elapsedRealtime();
        this.bus.post(location);
    }

    public long getLastLocationTime() {
        return this.lastLocationTime;
    }
}
