package com.navdy.hud.app.maps.here;

public class HereSpeedWarningManager {
    private static final int BUFFER_SPEED = 7;
    private static final long DISCARD_INTERVAL = java.util.concurrent.TimeUnit.SECONDS.toMillis(10);
    private static final int PERIODIC_CHECK_INTERVAL = 5000;
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.maps.here.HereSpeedWarningManager.class);
    private com.squareup.otto.Bus bus;
    private java.lang.Runnable checkSpeedRunnable = new com.navdy.hud.app.maps.here.HereSpeedWarningManager.Anon3();
    private android.content.Context context = com.navdy.hud.app.HudApplication.getAppContext();
    /* access modifiers changed from: private */
    public android.os.Handler handler;
    private com.navdy.hud.app.maps.here.HereNavigationManager hereNavigationManager;
    private long lastSpeedWarningTime;
    /* access modifiers changed from: private */
    public long lastUpdate;
    private com.navdy.hud.app.maps.MapsEventHandler mapsEventHandler;
    /* access modifiers changed from: private */
    public java.lang.Runnable periodicCheckRunnable = new com.navdy.hud.app.maps.here.HereSpeedWarningManager.Anon2();
    /* access modifiers changed from: private */
    public java.lang.Runnable periodicRunnable = new com.navdy.hud.app.maps.here.HereSpeedWarningManager.Anon1();
    /* access modifiers changed from: private */
    public boolean registered;
    private com.navdy.hud.app.manager.SpeedManager speedManager = com.navdy.hud.app.manager.SpeedManager.getInstance();
    private volatile boolean speedWarningOn;
    private java.lang.String tag;
    private int warningSpeed;

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            com.navdy.service.library.task.TaskManager.getInstance().execute(com.navdy.hud.app.maps.here.HereSpeedWarningManager.this.periodicCheckRunnable, 12);
        }
    }

    class Anon2 implements java.lang.Runnable {
        Anon2() {
        }

        public void run() {
            try {
                if (!com.navdy.hud.app.maps.here.HereSpeedWarningManager.this.registered) {
                    if (com.navdy.hud.app.maps.here.HereSpeedWarningManager.this.registered) {
                        com.navdy.hud.app.maps.here.HereSpeedWarningManager.this.handler.postDelayed(com.navdy.hud.app.maps.here.HereSpeedWarningManager.this.periodicRunnable, 5000);
                    }
                } else if (android.os.SystemClock.elapsedRealtime() - com.navdy.hud.app.maps.here.HereSpeedWarningManager.this.lastUpdate >= 5000) {
                    com.navdy.hud.app.maps.here.HereSpeedWarningManager.this.checkSpeed();
                    if (com.navdy.hud.app.maps.here.HereSpeedWarningManager.this.registered) {
                        com.navdy.hud.app.maps.here.HereSpeedWarningManager.this.handler.postDelayed(com.navdy.hud.app.maps.here.HereSpeedWarningManager.this.periodicRunnable, 5000);
                    }
                } else if (com.navdy.hud.app.maps.here.HereSpeedWarningManager.this.registered) {
                    com.navdy.hud.app.maps.here.HereSpeedWarningManager.this.handler.postDelayed(com.navdy.hud.app.maps.here.HereSpeedWarningManager.this.periodicRunnable, 5000);
                }
            } catch (Throwable th) {
                if (com.navdy.hud.app.maps.here.HereSpeedWarningManager.this.registered) {
                    com.navdy.hud.app.maps.here.HereSpeedWarningManager.this.handler.postDelayed(com.navdy.hud.app.maps.here.HereSpeedWarningManager.this.periodicRunnable, 5000);
                }
                throw th;
            }
        }
    }

    class Anon3 implements java.lang.Runnable {
        Anon3() {
        }

        public void run() {
            try {
                if (android.os.SystemClock.elapsedRealtime() - com.navdy.hud.app.maps.here.HereSpeedWarningManager.this.lastUpdate >= 1000) {
                    com.navdy.hud.app.maps.here.HereSpeedWarningManager.this.checkSpeed();
                }
            } catch (Throwable t) {
                com.navdy.hud.app.maps.here.HereSpeedWarningManager.sLogger.e(t);
            }
        }
    }

    HereSpeedWarningManager(java.lang.String tag2, com.squareup.otto.Bus bus2, com.navdy.hud.app.maps.MapsEventHandler mapsEventHandler2, com.navdy.hud.app.maps.here.HereNavigationManager hereNavigationManager2) {
        this.tag = tag2;
        this.bus = bus2;
        this.mapsEventHandler = mapsEventHandler2;
        this.hereNavigationManager = hereNavigationManager2;
        this.handler = new android.os.Handler(android.os.Looper.getMainLooper());
    }

    public void start() {
        if (!this.registered) {
            this.registered = true;
            this.bus.register(this);
            this.handler.removeCallbacks(this.periodicRunnable);
            this.handler.postDelayed(this.periodicRunnable, 5000);
            sLogger.v("started");
        }
    }

    public void stop() {
        if (this.registered) {
            this.registered = false;
            this.bus.unregister(this);
            this.handler.removeCallbacks(this.periodicRunnable);
            sLogger.v("stopped");
        }
    }

    private void onSpeedExceeded(java.lang.String roadName, int speedLimit, int currentSpeed) {
        int speedToConsider;
        this.bus.post(com.navdy.hud.app.maps.here.HereNavigationManager.SPEED_EXCEEDED);
        if (!this.speedWarningOn) {
            this.warningSpeed = -1;
        }
        boolean alreadyOn = this.speedWarningOn;
        this.speedWarningOn = true;
        if (java.lang.Boolean.TRUE.equals(this.mapsEventHandler.getNavigationPreferences().spokenSpeedLimitWarnings) && !com.navdy.hud.app.framework.phonecall.CallUtils.isPhoneCallInProgress()) {
            com.navdy.hud.app.manager.SpeedManager.SpeedUnit speedUnit = this.speedManager.getSpeedUnit();
            if (this.warningSpeed == -1) {
                speedToConsider = speedLimit + 7;
            } else {
                speedToConsider = this.warningSpeed + (this.warningSpeed / 10);
            }
            if (currentSpeed >= speedToConsider) {
                sLogger.w(this.tag + " speed exceeded-notify current[" + currentSpeed + "] threshold[" + speedToConsider + "] allowed[" + speedLimit + "] " + speedUnit.name());
                this.warningSpeed = currentSpeed;
                switch (speedUnit) {
                    case MILES_PER_HOUR:
                        java.lang.String str = this.hereNavigationManager.TTS_MILES;
                        break;
                    case KILOMETERS_PER_HOUR:
                        java.lang.String str2 = this.hereNavigationManager.TTS_KMS;
                        break;
                    case METERS_PER_SECOND:
                        java.lang.String str3 = this.hereNavigationManager.TTS_METERS;
                        break;
                    default:
                        java.lang.String str4 = "";
                        break;
                }
                long now = android.os.SystemClock.elapsedRealtime();
                long diff = now - this.lastSpeedWarningTime;
                if (diff <= DISCARD_INTERVAL) {
                    sLogger.v("don't post speed tts:" + diff);
                    return;
                }
                java.lang.String speedWarning = this.context.getString(com.navdy.hud.app.R.string.tts_speed_warning, new java.lang.Object[]{java.lang.Integer.valueOf(speedLimit)});
                this.lastSpeedWarningTime = now;
                com.navdy.hud.app.framework.voice.TTSUtils.sendSpeechRequest(speedWarning, com.navdy.service.library.events.audio.Category.SPEECH_SPEED_WARNING, null);
            } else if (!alreadyOn) {
                sLogger.w(this.tag + " speed exceeded current[" + currentSpeed + "] threshold[" + speedToConsider + "] allowed[" + speedLimit + "] " + speedUnit.name());
            }
        } else if (!alreadyOn) {
            sLogger.w(this.tag + " speed exceeded:" + roadName + " limit=" + speedLimit + " current=" + currentSpeed);
        }
    }

    private void onSpeedExceededEnd(java.lang.String roadName, int speedLimit, int currentSpeed) {
        this.speedWarningOn = false;
        this.warningSpeed = -1;
        this.bus.post(com.navdy.hud.app.maps.here.HereNavigationManager.SPEED_NORMAL);
        sLogger.i(this.tag + " speed normal:" + roadName + " limit=" + speedLimit + " current=" + currentSpeed);
    }

    @com.squareup.otto.Subscribe
    public void onPositionUpdated(com.here.android.mpa.common.GeoPosition geoPosition) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(this.checkSpeedRunnable, 12);
    }

    /* access modifiers changed from: private */
    public void checkSpeed() {
        float currentSpeed = (float) this.speedManager.getCurrentSpeed();
        if (currentSpeed != -1.0f) {
            com.here.android.mpa.common.RoadElement roadElement = com.navdy.hud.app.maps.here.HereMapUtil.getCurrentRoadElement();
            if (roadElement != null) {
                this.lastUpdate = android.os.SystemClock.elapsedRealtime();
                int speedAllowed = com.navdy.hud.app.manager.SpeedManager.convert((double) roadElement.getSpeedLimit(), com.navdy.hud.app.manager.SpeedManager.SpeedUnit.METERS_PER_SECOND, this.speedManager.getSpeedUnit());
                if (speedAllowed <= 0) {
                    if (this.speedWarningOn) {
                        sLogger.v("clear speed warning, no speedlimit info avail");
                        onSpeedExceededEnd(roadElement.getRoadName(), speedAllowed, (int) currentSpeed);
                    }
                } else if (currentSpeed > ((float) speedAllowed)) {
                    if (!this.speedWarningOn || this.warningSpeed == -1 || (this.warningSpeed != -1 && currentSpeed > ((float) this.warningSpeed))) {
                        onSpeedExceeded(roadElement.getRoadName(), speedAllowed, (int) currentSpeed);
                    }
                } else if (this.speedWarningOn) {
                    onSpeedExceededEnd(roadElement.getRoadName(), speedAllowed, (int) currentSpeed);
                }
            }
        } else if (this.speedWarningOn) {
            sLogger.i("lost speed info while warning on");
            onSpeedExceededEnd(null, 0, 0);
        }
    }
}
