package com.navdy.hud.app.maps.here;

public class HereGpsSignalListener extends com.here.android.mpa.guidance.NavigationManager.GpsSignalListener {
    private com.squareup.otto.Bus bus;
    private com.navdy.hud.app.maps.here.HereNavigationManager hereNavigationManager;
    private com.navdy.service.library.log.Logger logger;
    private com.navdy.hud.app.maps.MapsEventHandler mapsEventHandler;
    private java.lang.String tag;

    HereGpsSignalListener(com.navdy.service.library.log.Logger logger2, java.lang.String tag2, com.squareup.otto.Bus bus2, com.navdy.hud.app.maps.MapsEventHandler mapsEventHandler2, com.navdy.hud.app.maps.here.HereNavigationManager hereNavigationManager2) {
        this.logger = logger2;
        this.tag = tag2;
        this.bus = bus2;
        this.mapsEventHandler = mapsEventHandler2;
        this.hereNavigationManager = hereNavigationManager2;
    }

    public void onGpsLost() {
        this.logger.w(this.tag + " Gps signal lost");
        this.bus.post(this.hereNavigationManager.BUS_GPS_SIGNAL_LOST);
    }

    public void onGpsRestored() {
        this.logger.i(this.tag + " Gps signal restored");
        this.bus.post(this.hereNavigationManager.BUS_GPS_SIGNAL_RESTORED);
    }
}
