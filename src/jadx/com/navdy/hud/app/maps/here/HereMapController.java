package com.navdy.hud.app.maps.here;

public class HereMapController {
    private static final java.util.concurrent.atomic.AtomicInteger counter = new java.util.concurrent.atomic.AtomicInteger(1);
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.maps.here.HereMapController.class);
    /* access modifiers changed from: private */
    public final com.here.android.mpa.mapping.Map map;
    private final android.os.Handler mapBkHandler;
    private int mapRouteCount;
    private volatile com.navdy.hud.app.maps.here.HereMapController.State state;
    private android.graphics.PointF transformCenter;

    class Anon1 implements java.lang.Runnable {
        final /* synthetic */ float val$tilt;

        Anon1(float f) {
            this.val$tilt = f;
        }

        public void run() {
            com.navdy.hud.app.maps.here.HereMapController.this.map.setTilt(this.val$tilt);
        }
    }

    class Anon10 implements java.lang.Runnable {
        final /* synthetic */ com.here.android.mpa.mapping.Map.OnTransformListener val$listener;

        Anon10(com.here.android.mpa.mapping.Map.OnTransformListener onTransformListener) {
            this.val$listener = onTransformListener;
        }

        public void run() {
            com.navdy.hud.app.maps.here.HereMapController.this.map.addTransformListener(this.val$listener);
        }
    }

    class Anon11 implements java.lang.Runnable {
        final /* synthetic */ com.here.android.mpa.mapping.Map.OnTransformListener val$listener;

        Anon11(com.here.android.mpa.mapping.Map.OnTransformListener onTransformListener) {
            this.val$listener = onTransformListener;
        }

        public void run() {
            com.navdy.hud.app.maps.here.HereMapController.this.map.removeTransformListener(this.val$listener);
        }
    }

    class Anon12 implements java.lang.Runnable {
        final /* synthetic */ com.here.android.mpa.mapping.Map.Animation val$animation;
        final /* synthetic */ com.here.android.mpa.common.GeoBoundingBox val$boundingBox;
        final /* synthetic */ float val$orientation;
        final /* synthetic */ com.here.android.mpa.common.ViewRect val$viewRect;

        Anon12(com.here.android.mpa.common.GeoBoundingBox geoBoundingBox, com.here.android.mpa.common.ViewRect viewRect, com.here.android.mpa.mapping.Map.Animation animation, float f) {
            this.val$boundingBox = geoBoundingBox;
            this.val$viewRect = viewRect;
            this.val$animation = animation;
            this.val$orientation = f;
        }

        public void run() {
            try {
                com.navdy.hud.app.maps.here.HereMapController.this.map.zoomTo(this.val$boundingBox, this.val$viewRect, this.val$animation, this.val$orientation);
            } catch (Throwable t) {
                com.navdy.hud.app.maps.here.HereMapController.sLogger.e(t);
            }
        }
    }

    class Anon13 implements java.lang.Runnable {
        final /* synthetic */ java.lang.String val$mapScheme;

        Anon13(java.lang.String str) {
            this.val$mapScheme = str;
        }

        public void run() {
            com.navdy.hud.app.maps.here.HereMapController.this.map.setMapScheme(this.val$mapScheme);
        }
    }

    class Anon14 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.hud.app.maps.here.HereMapController.Callback val$cb;
        final /* synthetic */ java.lang.Runnable val$runnable;

        Anon14(java.lang.Runnable runnable, com.navdy.hud.app.maps.here.HereMapController.Callback callback) {
            this.val$runnable = runnable;
            this.val$cb = callback;
        }

        public void run() {
            this.val$runnable.run();
            if (this.val$cb != null) {
                this.val$cb.finish();
            }
        }
    }

    class Anon15 implements java.lang.Runnable {
        final /* synthetic */ boolean val$b;

        Anon15(boolean z) {
            this.val$b = z;
        }

        public void run() {
            com.navdy.hud.app.maps.here.HereMapController.this.map.setTrafficInfoVisible(this.val$b);
        }
    }

    class Anon2 implements java.lang.Runnable {
        final /* synthetic */ double val$zoomLevel;

        Anon2(double d) {
            this.val$zoomLevel = d;
        }

        public void run() {
            try {
                com.navdy.hud.app.maps.here.HereMapController.this.map.setZoomLevel(this.val$zoomLevel);
            } catch (Throwable t) {
                com.navdy.hud.app.maps.here.HereMapController.sLogger.e(t);
            }
        }
    }

    class Anon3 implements java.lang.Runnable {
        final /* synthetic */ com.here.android.mpa.mapping.Map.Animation val$animation;
        final /* synthetic */ com.here.android.mpa.common.GeoCoordinate val$center;
        final /* synthetic */ float val$orientation;
        final /* synthetic */ float val$tilt;
        final /* synthetic */ double val$zoom;

        Anon3(com.here.android.mpa.common.GeoCoordinate geoCoordinate, com.here.android.mpa.mapping.Map.Animation animation, double d, float f, float f2) {
            this.val$center = geoCoordinate;
            this.val$animation = animation;
            this.val$zoom = d;
            this.val$orientation = f;
            this.val$tilt = f2;
        }

        public void run() {
            com.navdy.hud.app.maps.here.HereMapController.this.map.setCenter(this.val$center, this.val$animation, this.val$zoom, this.val$orientation, this.val$tilt);
        }
    }

    class Anon4 implements java.lang.Runnable {
        final /* synthetic */ com.here.android.mpa.mapping.Map.Animation val$animation;
        final /* synthetic */ com.here.android.mpa.common.GeoCoordinate val$center;
        final /* synthetic */ float val$orientation;
        final /* synthetic */ float val$tilt;
        final /* synthetic */ double val$zoom;

        Anon4(com.here.android.mpa.common.GeoCoordinate geoCoordinate, com.here.android.mpa.mapping.Map.Animation animation, double d, float f, float f2) {
            this.val$center = geoCoordinate;
            this.val$animation = animation;
            this.val$zoom = d;
            this.val$orientation = f;
            this.val$tilt = f2;
        }

        public void run() {
            com.navdy.hud.app.maps.here.HereMapController.this.map.setCenter(this.val$center, this.val$animation, this.val$zoom, this.val$orientation, this.val$tilt);
        }
    }

    class Anon5 implements java.lang.Runnable {
        final /* synthetic */ com.here.android.mpa.mapping.MapObject val$mapObject;

        Anon5(com.here.android.mpa.mapping.MapObject mapObject) {
            this.val$mapObject = mapObject;
        }

        public void run() {
            com.navdy.hud.app.maps.here.HereMapController.this.addMapObjectInternal(this.val$mapObject);
        }
    }

    class Anon6 implements java.lang.Runnable {
        final /* synthetic */ java.util.List val$mapObjects;

        Anon6(java.util.List list) {
            this.val$mapObjects = list;
        }

        public void run() {
            for (com.here.android.mpa.mapping.MapObject mapObject : this.val$mapObjects) {
                com.navdy.hud.app.maps.here.HereMapController.this.addMapObjectInternal(mapObject);
            }
        }
    }

    class Anon7 implements java.lang.Runnable {
        final /* synthetic */ com.here.android.mpa.mapping.MapObject val$mapObject;

        Anon7(com.here.android.mpa.mapping.MapObject mapObject) {
            this.val$mapObject = mapObject;
        }

        public void run() {
            com.navdy.hud.app.maps.here.HereMapController.this.removeMapObjectInternal(this.val$mapObject);
        }
    }

    class Anon8 implements java.lang.Runnable {
        final /* synthetic */ java.util.List val$list;

        Anon8(java.util.List list) {
            this.val$list = list;
        }

        public void run() {
            for (com.here.android.mpa.mapping.MapObject mapObject : this.val$list) {
                com.navdy.hud.app.maps.here.HereMapController.this.removeMapObjectInternal(mapObject);
            }
        }
    }

    class Anon9 implements java.lang.Runnable {
        final /* synthetic */ android.graphics.PointF val$pointF;

        Anon9(android.graphics.PointF pointF) {
            this.val$pointF = pointF;
        }

        public void run() {
            com.navdy.hud.app.maps.here.HereMapController.this.map.setTransformCenter(this.val$pointF);
        }
    }

    public interface Callback {
        void finish();
    }

    public enum State {
        NONE,
        TRANSITION,
        ROUTE_PICKER,
        OVERVIEW,
        AR_MODE
    }

    public HereMapController(com.here.android.mpa.mapping.Map map2, com.navdy.hud.app.maps.here.HereMapController.State state2) {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        this.map = map2;
        this.state = state2;
        android.os.HandlerThread handlerThread = new android.os.HandlerThread("HereMapController-" + counter.getAndIncrement());
        handlerThread.start();
        this.mapBkHandler = new android.os.Handler(handlerThread.getLooper());
    }

    public com.here.android.mpa.mapping.Map getMap() {
        return this.map;
    }

    public com.navdy.hud.app.maps.here.HereMapController.State getState() {
        return this.state;
    }

    public void setState(com.navdy.hud.app.maps.here.HereMapController.State state2) {
        this.state = state2;
        sLogger.v("state:" + state2);
    }

    public com.here.android.mpa.common.GeoCoordinate getCenter() {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        return this.map.getCenter();
    }

    public double getZoomLevel() {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        return this.map.getZoomLevel();
    }

    public float getTilt() {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        return this.map.getTilt();
    }

    public void setTilt(float tilt) {
        this.mapBkHandler.post(new com.navdy.hud.app.maps.here.HereMapController.Anon1(tilt));
    }

    public float getOrientation() {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        return this.map.getOrientation();
    }

    public void setZoomLevel(double zoomLevel) {
        this.mapBkHandler.post(new com.navdy.hud.app.maps.here.HereMapController.Anon2(zoomLevel));
    }

    public void setCenter(com.here.android.mpa.common.GeoCoordinate center, com.here.android.mpa.mapping.Map.Animation animation, double zoom, float orientation, float tilt) {
        if (this.state == com.navdy.hud.app.maps.here.HereMapController.State.AR_MODE) {
            this.mapBkHandler.post(new com.navdy.hud.app.maps.here.HereMapController.Anon3(center, animation, zoom, orientation, tilt));
        }
    }

    public void setCenterForState(com.navdy.hud.app.maps.here.HereMapController.State state2, com.here.android.mpa.common.GeoCoordinate center, com.here.android.mpa.mapping.Map.Animation animation, double zoom, float orientation, float tilt) {
        if (this.state == state2) {
            this.mapBkHandler.post(new com.navdy.hud.app.maps.here.HereMapController.Anon4(center, animation, zoom, orientation, tilt));
        }
    }

    public void addMapObject(com.here.android.mpa.mapping.MapObject mapObject) {
        this.mapBkHandler.post(new com.navdy.hud.app.maps.here.HereMapController.Anon5(mapObject));
    }

    /* access modifiers changed from: private */
    public void addMapObjectInternal(com.here.android.mpa.mapping.MapObject mapObject) {
        if (mapObject instanceof com.here.android.mpa.mapping.MapRoute) {
            com.here.android.mpa.mapping.MapRoute mapRoute = (com.here.android.mpa.mapping.MapRoute) mapObject;
            com.here.android.mpa.routing.Route route = mapRoute.getRoute();
            this.mapRouteCount++;
            sLogger.v("[map-route-added] maproute=" + java.lang.System.identityHashCode(mapRoute) + " route=" + java.lang.System.identityHashCode(route) + " count=" + this.mapRouteCount);
        }
        this.map.addMapObject(mapObject);
    }

    public void addMapObjects(java.util.List<com.here.android.mpa.mapping.MapObject> mapObjects) {
        this.mapBkHandler.post(new com.navdy.hud.app.maps.here.HereMapController.Anon6(mapObjects));
    }

    public void removeMapObject(com.here.android.mpa.mapping.MapObject mapObject) {
        this.mapBkHandler.post(new com.navdy.hud.app.maps.here.HereMapController.Anon7(mapObject));
    }

    public void removeMapObjects(java.util.List<com.here.android.mpa.mapping.MapObject> list) {
        this.mapBkHandler.post(new com.navdy.hud.app.maps.here.HereMapController.Anon8(list));
    }

    /* access modifiers changed from: private */
    public void removeMapObjectInternal(com.here.android.mpa.mapping.MapObject mapObject) {
        if (mapObject instanceof com.here.android.mpa.mapping.MapRoute) {
            this.mapRouteCount--;
            com.here.android.mpa.mapping.MapRoute mapRoute = (com.here.android.mpa.mapping.MapRoute) mapObject;
            sLogger.v("[map-route-removed] maproute=" + java.lang.System.identityHashCode(mapRoute) + " route=" + java.lang.System.identityHashCode(mapRoute.getRoute()) + " count=" + this.mapRouteCount);
        }
        if (mapObject != null) {
            this.map.removeMapObject(mapObject);
        }
    }

    public void setTransformCenter(android.graphics.PointF pointF) {
        this.transformCenter = pointF;
        this.mapBkHandler.post(new com.navdy.hud.app.maps.here.HereMapController.Anon9(pointF));
    }

    public com.here.android.mpa.mapping.Map.PixelResult projectToPixel(com.here.android.mpa.common.GeoCoordinate latlng) {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        return this.map.projectToPixel(latlng);
    }

    public void addTransformListener(com.here.android.mpa.mapping.Map.OnTransformListener listener) {
        this.mapBkHandler.post(new com.navdy.hud.app.maps.here.HereMapController.Anon10(listener));
    }

    public void removeTransformListener(com.here.android.mpa.mapping.Map.OnTransformListener listener) {
        this.mapBkHandler.post(new com.navdy.hud.app.maps.here.HereMapController.Anon11(listener));
    }

    public void zoomTo(com.here.android.mpa.common.GeoBoundingBox boundingBox, com.here.android.mpa.common.ViewRect viewRect, com.here.android.mpa.mapping.Map.Animation animation, float orientation) {
        this.mapBkHandler.post(new com.navdy.hud.app.maps.here.HereMapController.Anon12(boundingBox, viewRect, animation, orientation));
    }

    public com.here.android.mpa.mapping.PositionIndicator getPositionIndicator() {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        return this.map.getPositionIndicator();
    }

    public java.lang.String getMapScheme() {
        return this.map.getMapScheme();
    }

    public void setMapScheme(java.lang.String mapScheme) {
        this.mapBkHandler.post(new com.navdy.hud.app.maps.here.HereMapController.Anon13(mapScheme));
    }

    public void execute(java.lang.Runnable runnable) {
        this.mapBkHandler.post(runnable);
    }

    public void execute(java.lang.Runnable runnable, com.navdy.hud.app.maps.here.HereMapController.Callback cb) {
        this.mapBkHandler.post(new com.navdy.hud.app.maps.here.HereMapController.Anon14(runnable, cb));
    }

    public void setTrafficInfoVisible(boolean b) {
        this.mapBkHandler.post(new com.navdy.hud.app.maps.here.HereMapController.Anon15(b));
    }
}
