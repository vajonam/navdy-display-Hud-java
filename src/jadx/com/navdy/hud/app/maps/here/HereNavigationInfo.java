package com.navdy.hud.app.maps.here;

public class HereNavigationInfo {
    com.navdy.hud.app.maps.MapEvents.ManeuverDisplay arrivedManeuverDisplay;
    com.navdy.service.library.events.navigation.NavigationSessionRouteChange.RerouteReason currentRerouteReason;
    com.here.android.mpa.common.GeoCoordinate destination;
    com.navdy.hud.app.maps.MapEvents.DestinationDirection destinationDirection;
    java.lang.String destinationDirectionStr;
    int destinationIconId;
    public java.lang.String destinationIdentifier;
    java.lang.String destinationLabel;
    com.navdy.service.library.device.NavdyDeviceId deviceId;
    boolean firstManeuverShown;
    long firstManeuverShownTime;
    boolean hasArrived;
    boolean ignoreArrived;
    boolean lastManeuver;
    long lastManeuverPostTime;
    com.navdy.service.library.events.navigation.NavigationRouteRequest lastRequest;
    java.lang.String lastRouteId;
    long lastTrafficRerouteTime;
    com.here.android.mpa.routing.Maneuver maneuverAfterCurrent;
    int maneuverAfterCurrentIconid = -1;
    com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.ManeuverState maneuverState;
    com.here.android.mpa.mapping.MapMarker mapDestinationMarker;
    com.here.android.mpa.mapping.MapRoute mapRoute;
    com.navdy.service.library.events.navigation.NavigationRouteRequest navigationRouteRequest;
    com.here.android.mpa.routing.Route route;
    java.lang.String routeId;
    com.here.android.mpa.routing.RouteOptions routeOptions;
    int simulationSpeed;
    com.here.android.mpa.common.GeoCoordinate startLocation;
    java.lang.String streetAddress;
    int trafficRerouteCount;
    boolean trafficRerouteOnce;
    java.util.List<com.here.android.mpa.common.GeoCoordinate> waypoints;

    /* access modifiers changed from: 0000 */
    public void clear() {
        if (this.navigationRouteRequest != null) {
            this.lastRequest = this.navigationRouteRequest;
            this.lastRouteId = this.routeId;
        } else {
            this.lastRequest = null;
            this.lastRouteId = null;
        }
        this.navigationRouteRequest = null;
        this.routeId = null;
        this.route = null;
        this.simulationSpeed = -1;
        this.streetAddress = null;
        this.destinationLabel = null;
        this.destinationIdentifier = null;
        this.lastManeuverPostTime = 0;
        this.deviceId = null;
        this.destinationDirection = null;
        this.destinationDirectionStr = null;
        this.destinationIconId = -1;
        this.maneuverAfterCurrent = null;
        this.maneuverAfterCurrentIconid = -1;
        this.hasArrived = false;
        this.ignoreArrived = false;
        this.lastManeuver = false;
        this.arrivedManeuverDisplay = null;
        this.destination = null;
        this.trafficRerouteCount = 0;
        this.lastTrafficRerouteTime = 0;
        this.currentRerouteReason = null;
        this.trafficRerouteOnce = false;
        this.firstManeuverShown = false;
        this.firstManeuverShownTime = 0;
        this.routeOptions = null;
        this.startLocation = null;
    }

    /* access modifiers changed from: 0000 */
    public void copy(com.navdy.hud.app.maps.here.HereNavigationInfo info) {
        clear();
        if (info != null) {
            this.navigationRouteRequest = info.navigationRouteRequest;
            if (this.navigationRouteRequest != null) {
                this.destination = new com.here.android.mpa.common.GeoCoordinate(this.navigationRouteRequest.destination.latitude.doubleValue(), this.navigationRouteRequest.destination.longitude.doubleValue());
            }
            this.routeId = info.routeId;
            this.routeOptions = info.routeOptions;
            this.route = info.route;
            this.simulationSpeed = info.simulationSpeed;
            this.streetAddress = info.streetAddress;
            this.destinationLabel = info.destinationLabel;
            this.destinationIdentifier = info.destinationIdentifier;
            this.deviceId = info.deviceId;
            this.destinationDirection = info.destinationDirection;
            this.destinationDirectionStr = info.destinationDirectionStr;
            this.destinationIconId = info.destinationIconId;
            this.maneuverAfterCurrent = info.maneuverAfterCurrent;
            this.maneuverAfterCurrentIconid = info.maneuverAfterCurrentIconid;
            this.lastRequest = null;
            this.lastRouteId = null;
            this.currentRerouteReason = null;
            this.trafficRerouteOnce = false;
            this.startLocation = info.startLocation;
        }
    }

    /* access modifiers changed from: 0000 */
    public com.navdy.service.library.events.navigation.NavigationRouteRequest getLastNavigationRequest() {
        com.navdy.service.library.events.navigation.NavigationRouteRequest ret = this.lastRequest;
        this.lastRequest = null;
        return ret;
    }

    /* access modifiers changed from: 0000 */
    public java.lang.String getLastRouteId() {
        java.lang.String ret = this.lastRouteId;
        this.lastRouteId = null;
        return ret;
    }

    /* access modifiers changed from: 0000 */
    public int getTrafficReRouteCount() {
        return this.trafficRerouteCount;
    }

    /* access modifiers changed from: 0000 */
    public long getLastTrafficRerouteTime() {
        return this.lastTrafficRerouteTime;
    }

    /* access modifiers changed from: 0000 */
    public void setLastTrafficRerouteTime(long l) {
        this.lastTrafficRerouteTime = l;
    }

    /* access modifiers changed from: 0000 */
    public void incrTrafficRerouteCount() {
        this.trafficRerouteCount++;
    }
}
