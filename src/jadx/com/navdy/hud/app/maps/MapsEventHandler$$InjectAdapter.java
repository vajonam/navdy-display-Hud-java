package com.navdy.hud.app.maps;

public final class MapsEventHandler$$InjectAdapter extends dagger.internal.Binding<com.navdy.hud.app.maps.MapsEventHandler> implements dagger.MembersInjector<com.navdy.hud.app.maps.MapsEventHandler> {
    private dagger.internal.Binding<com.squareup.otto.Bus> bus;
    private dagger.internal.Binding<com.navdy.hud.app.service.ConnectionHandler> connectionHandler;
    private dagger.internal.Binding<com.navdy.hud.app.profile.DriverProfileManager> mDriverProfileManager;
    private dagger.internal.Binding<android.content.SharedPreferences> sharedPreferences;

    public MapsEventHandler$$InjectAdapter() {
        super(null, "members/com.navdy.hud.app.maps.MapsEventHandler", false, com.navdy.hud.app.maps.MapsEventHandler.class);
    }

    public void attach(dagger.internal.Linker linker) {
        this.connectionHandler = linker.requestBinding("com.navdy.hud.app.service.ConnectionHandler", com.navdy.hud.app.maps.MapsEventHandler.class, getClass().getClassLoader());
        this.bus = linker.requestBinding("com.squareup.otto.Bus", com.navdy.hud.app.maps.MapsEventHandler.class, getClass().getClassLoader());
        this.sharedPreferences = linker.requestBinding("android.content.SharedPreferences", com.navdy.hud.app.maps.MapsEventHandler.class, getClass().getClassLoader());
        this.mDriverProfileManager = linker.requestBinding("com.navdy.hud.app.profile.DriverProfileManager", com.navdy.hud.app.maps.MapsEventHandler.class, getClass().getClassLoader());
    }

    public void getDependencies(java.util.Set<dagger.internal.Binding<?>> set, java.util.Set<dagger.internal.Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.connectionHandler);
        injectMembersBindings.add(this.bus);
        injectMembersBindings.add(this.sharedPreferences);
        injectMembersBindings.add(this.mDriverProfileManager);
    }

    public void injectMembers(com.navdy.hud.app.maps.MapsEventHandler object) {
        object.connectionHandler = (com.navdy.hud.app.service.ConnectionHandler) this.connectionHandler.get();
        object.bus = (com.squareup.otto.Bus) this.bus.get();
        object.sharedPreferences = (android.content.SharedPreferences) this.sharedPreferences.get();
        object.mDriverProfileManager = (com.navdy.hud.app.profile.DriverProfileManager) this.mDriverProfileManager.get();
    }
}
