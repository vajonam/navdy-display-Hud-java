package com.navdy.hud.app.maps;

public class MapEvents {
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.maps.MapEvents.class);

    public static class ArrivalEvent {
    }

    public enum DestinationDirection {
        LEFT,
        RIGHT,
        UNKNOWN
    }

    public static class DialMapZoom {
        public final com.navdy.hud.app.maps.MapEvents.DialMapZoom.Type type;

        public enum Type {
            IN,
            OUT
        }

        public DialMapZoom(com.navdy.hud.app.maps.MapEvents.DialMapZoom.Type type2) {
            this.type = type2;
        }
    }

    public static class DisplayJunction {
        public com.here.android.mpa.common.Image junction;
        public com.here.android.mpa.common.Image signpost;

        public DisplayJunction(com.here.android.mpa.common.Image image1, com.here.android.mpa.common.Image image2) {
            this.junction = image1;
            this.signpost = image2;
        }
    }

    public static class DisplayTrafficIncident {
        public static final java.lang.String ACCIDENT = "ACCIDENT";
        public static final java.lang.String CLOSURE = "CLOSURE";
        public static final java.lang.String CONGESTION = "CONGESTION";
        public static final java.lang.String FLOW = "FLOW";
        public static final java.lang.String OTHER = "OTHER";
        public static final java.lang.String ROADWORKS = "ROADWORKS";
        public static final java.lang.String UNDEFINED = "UNDEFINED";
        public java.lang.String affectedStreet;
        public com.navdy.hud.app.maps.MapEvents.DisplayTrafficIncident.Category category;
        public java.lang.String description;
        public long distanceToIncident;
        public com.here.android.mpa.common.Image icon;
        public java.util.Date reported;
        public java.lang.String title;
        public com.navdy.hud.app.maps.MapEvents.DisplayTrafficIncident.Type type;
        public java.util.Date updated;

        public enum Category {
            ACCIDENT,
            CLOSURE,
            ROADWORKS,
            CONGESTION,
            FLOW,
            UNDEFINED,
            OTHER
        }

        public enum Type {
            BLOCKING,
            VERY_HIGH,
            HIGH,
            NORMAL,
            FAILED,
            INACTIVE
        }

        public DisplayTrafficIncident(com.navdy.hud.app.maps.MapEvents.DisplayTrafficIncident.Type type2, com.navdy.hud.app.maps.MapEvents.DisplayTrafficIncident.Category category2) {
            this.type = type2;
            this.category = category2;
        }

        public DisplayTrafficIncident(com.navdy.hud.app.maps.MapEvents.DisplayTrafficIncident.Type type2, com.navdy.hud.app.maps.MapEvents.DisplayTrafficIncident.Category category2, java.lang.String title2, java.lang.String description2, java.lang.String affectedStreet2, long distanceToIncident2, java.util.Date reported2, java.util.Date updated2, com.here.android.mpa.common.Image icon2) {
            this.type = type2;
            this.category = category2;
            this.title = title2;
            this.description = description2;
            this.affectedStreet = affectedStreet2;
            this.distanceToIncident = distanceToIncident2;
            this.reported = reported2;
            this.updated = updated2;
            this.icon = icon2;
        }
    }

    public static class DisplayTrafficLaneInfo {
        public java.util.ArrayList<com.navdy.hud.app.maps.MapEvents.LaneData> laneData;

        public DisplayTrafficLaneInfo(java.util.ArrayList<com.navdy.hud.app.maps.MapEvents.LaneData> laneData2) {
            this.laneData = laneData2;
        }
    }

    public static class GPSSpeedEvent {
    }

    public static class GpsStatusChange {
        public boolean connected;

        public GpsStatusChange(boolean connected2) {
            this.connected = connected2;
        }
    }

    public static class HideSignPostJunction {
    }

    public static class HideTrafficLaneInfo {
    }

    public static class LaneData {
        public android.graphics.drawable.Drawable[] icons;
        public com.navdy.hud.app.maps.MapEvents.LaneData.Position position;

        public enum Position {
            ON_ROUTE,
            OFF_ROUTE
        }

        public LaneData(com.navdy.hud.app.maps.MapEvents.LaneData.Position position2, android.graphics.drawable.Drawable[] icons2) {
            this.position = position2;
            this.icons = icons2;
        }
    }

    public static class LiveTrafficDismissEvent {
    }

    public static class LiveTrafficEvent {
        public final com.navdy.hud.app.maps.MapEvents.LiveTrafficEvent.Severity severity;
        public final com.navdy.hud.app.maps.MapEvents.LiveTrafficEvent.Type type;

        public enum Severity {
            HIGH(0),
            VERY_HIGH(1);
            
            public final int value;

            private Severity(int value2) {
                this.value = value2;
            }
        }

        public enum Type {
            CONGESTION,
            INCIDENT
        }

        public LiveTrafficEvent(com.navdy.hud.app.maps.MapEvents.LiveTrafficEvent.Type type2, com.navdy.hud.app.maps.MapEvents.LiveTrafficEvent.Severity severity2) {
            this.type = type2;
            this.severity = severity2;
        }
    }

    public static class LocationFix {
        public boolean locationAvailable;
        public boolean usingLocalGpsLocation;
        public boolean usingPhoneLocation;

        public LocationFix(boolean locationAvailable2, boolean usingPhoneLocation2, boolean usingLocalGpsLocation2) {
            this.locationAvailable = locationAvailable2;
            this.usingPhoneLocation = usingPhoneLocation2;
            this.usingLocalGpsLocation = usingLocalGpsLocation2;
        }
    }

    public static class ManeuverDisplay {
        static final java.lang.String NOT_AVAILABLE = "N/A";
        public java.lang.String currentRoad;
        public float currentSpeedLimit;
        public int destinationIconId;
        public com.navdy.hud.app.maps.MapEvents.DestinationDirection direction;
        public float distance;
        public long distanceInMeters;
        public java.lang.String distanceToPendingRoadText;
        public com.navdy.service.library.events.navigation.DistanceUnit distanceUnit;
        public boolean empty;
        public java.lang.String eta;
        public java.lang.String etaAmPm;
        public java.util.Date etaDate;
        public java.lang.String maneuverId;
        public com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.ManeuverState maneuverState;
        public com.navdy.service.library.events.navigation.NavigationTurn navigationTurn;
        public int nextTurnIconId = -1;
        public java.lang.String pendingRoad;
        public java.lang.String pendingTurn;
        public float totalDistance;
        public float totalDistanceRemaining;
        public com.navdy.service.library.events.navigation.DistanceUnit totalDistanceRemainingUnit;
        public com.navdy.service.library.events.navigation.DistanceUnit totalDistanceUnit;
        public int turnIconId = -1;
        public int turnIconNowId;
        public int turnIconSoonId;

        public java.lang.String toString() {
            java.lang.String str;
            java.lang.String resourceName = NOT_AVAILABLE;
            if (this.turnIconId != -1) {
                try {
                    resourceName = com.navdy.hud.app.HudApplication.getAppContext().getResources().getResourceName(this.turnIconId);
                } catch (Throwable t) {
                    com.navdy.hud.app.maps.MapEvents.sLogger.e(t);
                }
            }
            java.lang.StringBuilder append = new java.lang.StringBuilder().append("TurnIcon [").append(resourceName).append("] ").append("TurnIcon [").append(this.turnIconId).append("] ").append("PendingTurn[").append(this.pendingTurn).append("] ").append("PendingRoad[").append(this.pendingRoad).append("] ").append("Distanceleft[").append(this.distanceToPendingRoadText).append("] ").append("DistanceleftInMeters[").append(this.distanceInMeters).append("] ").append("CurrentRoad[").append(this.currentRoad).append("] ").append("eta[").append(this.eta).append("] ").append("etaUtc[").append(this.etaDate).append("] ").append("currentSpeedLimit[").append(this.currentSpeedLimit).append("] ").append("earlyManeuver[");
            if (this.nextTurnIconId == -1) {
                str = "not_available] ";
            } else {
                str = "available] ";
            }
            return append.append(str).append("destinationDirection[").append(this.direction).append("] ").append("totalDistance [").append(this.totalDistance).append(" ").append(this.totalDistanceUnit).append("] ").append("totalDistanceRemain [").append(this.totalDistanceRemaining).append(" ").append(this.totalDistanceRemainingUnit).append("] ").append("empty[").append(this.empty).append("] ").append("navigating[").append(isNavigating()).append("] ").append("arrived [").append(isArrived()).append("] ").toString();
        }

        public boolean isEmpty() {
            return this.empty;
        }

        public boolean isArrived() {
            return this.turnIconId == com.navdy.hud.app.R.drawable.icon_tbt_arrive;
        }

        public boolean isNavigating() {
            return this.turnIconId != -1;
        }
    }

    public static class ManeuverEvent {
        public com.here.android.mpa.routing.Maneuver maneuver;
        public com.navdy.hud.app.maps.MapEvents.ManeuverEvent.Type type;

        public enum Type {
            FIRST,
            LAST,
            INTERMEDIATE
        }

        public ManeuverEvent(com.navdy.hud.app.maps.MapEvents.ManeuverEvent.Type type2, com.here.android.mpa.routing.Maneuver maneuver2) {
            this.type = type2;
            this.maneuver = maneuver2;
        }
    }

    public static class ManeuverSoonEvent {
        public final com.here.android.mpa.routing.Maneuver maneuver;

        public ManeuverSoonEvent(com.here.android.mpa.routing.Maneuver maneuver2) {
            this.maneuver = maneuver2;
        }
    }

    public static class MapEngineInitialize {
        public boolean initialized;

        public MapEngineInitialize(boolean initialized2) {
            this.initialized = initialized2;
        }
    }

    public static class MapEngineReady {
    }

    public static class MapUIReady {
    }

    public static class NavigationModeChange {
        public com.navdy.hud.app.maps.NavigationMode navigationMode;

        public NavigationModeChange(com.navdy.hud.app.maps.NavigationMode navigationMode2) {
            this.navigationMode = navigationMode2;
        }
    }

    public static class NewRouteAdded {
        public final com.navdy.service.library.events.navigation.NavigationSessionRouteChange.RerouteReason rerouteReason;

        public NewRouteAdded(com.navdy.service.library.events.navigation.NavigationSessionRouteChange.RerouteReason rerouteReason2) {
            this.rerouteReason = rerouteReason2;
        }
    }

    public static class RegionEvent {
        public final java.lang.String country;
        public final java.lang.String state;

        public RegionEvent(java.lang.String state2, java.lang.String country2) {
            this.state = state2;
            this.country = country2;
        }

        public boolean equals(java.lang.Object o) {
            if (o == this) {
                return true;
            }
            if (!(o instanceof com.navdy.hud.app.maps.MapEvents.RegionEvent)) {
                return false;
            }
            com.navdy.hud.app.maps.MapEvents.RegionEvent r = (com.navdy.hud.app.maps.MapEvents.RegionEvent) o;
            boolean stateEquals = this.state == null ? r.state == null : this.state.equals(r.state);
            if (stateEquals) {
                if (this.country == null) {
                    if (r.country == null) {
                        return true;
                    }
                } else if (this.country.equals(r.country)) {
                    return true;
                }
            }
            return false;
        }

        public int hashCode() {
            return (((this.state == null ? 0 : this.state.hashCode()) + 527) * 31) + (this.country == null ? 0 : this.country.hashCode());
        }

        public java.lang.String toString() {
            return "RegionEvent: " + this.state + ", " + this.country;
        }
    }

    public static class RerouteEvent {
        public com.navdy.hud.app.maps.MapEvents.RouteEventType routeEventType;

        public RerouteEvent(com.navdy.hud.app.maps.MapEvents.RouteEventType routeEventType2) {
            this.routeEventType = routeEventType2;
        }
    }

    public static class RouteCalculationEvent {
        public boolean abortOriginDisplay;
        public java.lang.Object end;
        public com.navdy.hud.app.framework.destinations.Destination lookupDestination;
        public java.lang.String pendingNavigationRequestId;
        public int progress;
        public int progressIncrement;
        public com.navdy.service.library.events.navigation.NavigationRouteRequest request;
        public com.navdy.service.library.events.navigation.NavigationRouteResponse response;
        public com.here.android.mpa.routing.RouteOptions routeOptions;
        public java.lang.Object start;
        public com.navdy.hud.app.maps.MapEvents.RouteCalculationState state;
        public boolean stopped;
        public java.util.List<?> waypoints;
    }

    public enum RouteCalculationState {
        NONE,
        STARTED,
        STOPPED,
        IN_PROGRESS,
        ABORT_NAVIGATION,
        FINDING_NAV_COORDINATES
    }

    public enum RouteEventType {
        STARTED,
        FINISHED,
        FAILED
    }

    public static class SpeedWarning {
        public boolean exceed;

        public SpeedWarning(boolean exceed2) {
            this.exceed = exceed2;
        }
    }

    public static class TrafficDelayDismissEvent {
    }

    public static class TrafficDelayEvent {
        public final long etaDifference;

        public TrafficDelayEvent(long etaDifference2) {
            this.etaDifference = etaDifference2;
        }
    }

    public static class TrafficJamDismissEvent {
    }

    public static class TrafficJamProgressEvent {
        public final int remainingTime;

        public TrafficJamProgressEvent(int remainingTime2) {
            this.remainingTime = remainingTime2;
        }
    }

    public enum TrafficRerouteAction {
        REROUTE,
        DISMISS
    }

    public static class TrafficRerouteDismissEvent {
    }

    public static class TrafficRerouteEvent {
        public java.lang.String additionalVia;
        public long currentEta;
        public long distanceDifference;
        public long etaDifference;
        public long newEta;
        public java.lang.String via;

        public TrafficRerouteEvent(java.lang.String via2, java.lang.String additionalVia2, long etaDifference2, long currentEta2, long distanceDifference2, long newEta2) {
            this.via = via2;
            this.additionalVia = additionalVia2;
            this.etaDifference = etaDifference2;
            this.currentEta = currentEta2;
            this.distanceDifference = distanceDifference2;
            this.newEta = newEta2;
        }
    }
}
