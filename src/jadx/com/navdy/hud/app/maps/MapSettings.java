package com.navdy.hud.app.maps;

public class MapSettings {
    private static final java.lang.String CUSTOM_ANIMATION = "persist.sys.custom_animation";
    private static final java.lang.String DEBUG_HERE_LOC = "persist.sys.dbg_here_loc";
    private static final int DEFAULT_MAP_FPS = 5;
    private static final java.lang.String FULL_CUSTOM_ANIMATION = "persist.sys.full_animation";
    private static final java.lang.String GENERATE_ROUTE_ICONS = "persist.sys.route_icons";
    private static final java.lang.String GLYMPSE_DURATION = "persist.sys.glympse_dur";
    private static final java.lang.String LANE_GUIDANCE_ENABLED = "persist.sys.lane_info_enabled";
    private static final java.lang.String MAP_FPS = "persist.sys.map_fps";
    private static final java.lang.String NO_TURN_TEXT_IN_TBT = "persist.sys.no_turn_text";
    private static final java.lang.String TBT_ONTO_DISABLED = "persist.sys.map.tbt.disableonto";
    private static final java.lang.String TRAFFIC_WIDGETS_ENABLED = "persist.sys.map.traffic.widgets";
    private static final java.lang.String TTS_RECALCULATION_DISABLED = "persist.sys.map.tts.norecalc";
    private static final boolean customAnimationEnabled = com.navdy.hud.app.util.os.SystemProperties.getBoolean(CUSTOM_ANIMATION, false);
    private static final boolean debugHereLocation = com.navdy.hud.app.util.os.SystemProperties.getBoolean(DEBUG_HERE_LOC, false);
    private static final boolean dontShowTurnText = com.navdy.hud.app.util.os.SystemProperties.getBoolean(NO_TURN_TEXT_IN_TBT, false);
    private static final int fps = com.navdy.hud.app.util.os.SystemProperties.getInt(MAP_FPS, 5);
    private static final boolean fullCustomAnimatonEnabled = com.navdy.hud.app.util.os.SystemProperties.getBoolean(FULL_CUSTOM_ANIMATION, false);
    private static final boolean generateRouteIcons = com.navdy.hud.app.util.os.SystemProperties.getBoolean(GENERATE_ROUTE_ICONS, false);
    private static final int glympseDuration;
    private static final boolean laneGuidanceEnabled = com.navdy.hud.app.util.os.SystemProperties.getBoolean(LANE_GUIDANCE_ENABLED, false);
    private static int simulationSpeed;
    private static final boolean tbtOntoDisabled = com.navdy.hud.app.util.os.SystemProperties.getBoolean(TBT_ONTO_DISABLED, false);
    private static final boolean trafficDashWidgetsEnabled = com.navdy.hud.app.util.os.SystemProperties.getBoolean(TRAFFIC_WIDGETS_ENABLED, false);
    private static final boolean ttsDisableRecalculating = com.navdy.hud.app.util.os.SystemProperties.getBoolean(TTS_RECALCULATION_DISABLED, false);

    static {
        int minutes = com.navdy.hud.app.util.os.SystemProperties.getInt(GLYMPSE_DURATION, 0);
        if (minutes <= 0 || minutes > 1440) {
            glympseDuration = 0;
        } else {
            glympseDuration = (int) java.util.concurrent.TimeUnit.MINUTES.toMillis((long) minutes);
        }
    }

    public static boolean isTrafficDashWidgetsEnabled() {
        return trafficDashWidgetsEnabled;
    }

    public static boolean isTbtOntoDisabled() {
        return tbtOntoDisabled || !com.navdy.hud.app.util.DeviceUtil.isUserBuild();
    }

    public static boolean isTtsRecalculationDisabled() {
        return ttsDisableRecalculating;
    }

    public static boolean isCustomAnimationEnabled() {
        return customAnimationEnabled;
    }

    public static boolean isFullCustomAnimatonEnabled() {
        return fullCustomAnimatonEnabled;
    }

    public static int getMapFps() {
        return fps;
    }

    public static boolean isLaneGuidanceEnabled() {
        return laneGuidanceEnabled;
    }

    public static void setSimulationSpeed(int n) {
        simulationSpeed = n;
    }

    public static int getSimulationSpeed() {
        return simulationSpeed;
    }

    public static boolean isGenerateRouteIcons() {
        return generateRouteIcons;
    }

    public static boolean isDebugHereLocation() {
        return debugHereLocation;
    }

    public static boolean doNotShowTurnTextInTBT() {
        return true;
    }

    public static int getGlympseDuration() {
        if (glympseDuration > 0) {
            return glympseDuration;
        }
        return com.navdy.hud.app.framework.glympse.GlympseManager.GLYMPSE_DURATION;
    }
}
