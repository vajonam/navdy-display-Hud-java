package com.navdy.hud.app.maps;

public class GpsEventsReceiver extends android.content.BroadcastReceiver {
    private static final com.navdy.hud.app.event.DrivingStateChange DRIVING_STARTED = new com.navdy.hud.app.event.DrivingStateChange(true);
    private static final com.navdy.hud.app.event.DrivingStateChange DRIVING_STOPPED = new com.navdy.hud.app.event.DrivingStateChange(false);
    private com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.maps.GpsEventsReceiver.class);

    public void onReceive(android.content.Context context, android.content.Intent intent) {
        reportGpsEvent(intent);
    }

    private void reportGpsEvent(android.content.Intent intent) {
        try {
            java.lang.String action = intent.getAction();
            android.os.Bundle b = intent.getExtras();
            char c = 65535;
            switch (action.hashCode()) {
                case -2045233493:
                    if (action.equals(com.navdy.hud.app.device.gps.GpsConstants.GPS_SATELLITE_STATUS)) {
                        c = 7;
                        break;
                    }
                    break;
                case -1801456507:
                    if (action.equals(com.navdy.hud.app.device.gps.GpsConstants.GPS_EVENT_DEAD_RECKONING_STARTED)) {
                        c = 0;
                        break;
                    }
                    break;
                case -1788590639:
                    if (action.equals(com.navdy.hud.app.device.gps.GpsConstants.GPS_EVENT_DEAD_RECKONING_STOPPED)) {
                        c = 1;
                        break;
                    }
                    break;
                case -629625847:
                    if (action.equals(com.navdy.hud.app.device.gps.GpsConstants.GPS_EVENT_DRIVING_STARTED)) {
                        c = 3;
                        break;
                    }
                    break;
                case -616759979:
                    if (action.equals(com.navdy.hud.app.device.gps.GpsConstants.GPS_EVENT_DRIVING_STOPPED)) {
                        c = 4;
                        break;
                    }
                    break;
                case -192082374:
                    if (action.equals(com.navdy.hud.app.device.gps.GpsConstants.GPS_EVENT_ENABLE_ESF_RAW)) {
                        c = 6;
                        break;
                    }
                    break;
                case -107791191:
                    if (action.equals(com.navdy.hud.app.device.gps.GpsConstants.GPS_EVENT_SWITCH)) {
                        c = 2;
                        break;
                    }
                    break;
                case 897686227:
                    if (action.equals(com.navdy.hud.app.device.gps.GpsConstants.GPS_EVENT_WARM_RESET_UBLOX)) {
                        c = 5;
                        break;
                    }
                    break;
                case 1315284313:
                    if (action.equals(com.navdy.hud.app.device.gps.GpsConstants.GPS_COLLECT_LOGS)) {
                        c = 8;
                        break;
                    }
                    break;
            }
            switch (c) {
                case 0:
                    com.navdy.hud.app.framework.voice.TTSUtils.debugShowDRStarted("");
                    return;
                case 1:
                    com.navdy.hud.app.framework.voice.TTSUtils.debugShowDREnded();
                    return;
                case 2:
                    com.navdy.hud.app.framework.voice.TTSUtils.debugShowGpsSwitch(b.getString("title"), b.getString("info"));
                    com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getBus().post(new com.navdy.hud.app.device.gps.GpsUtils.GpsSwitch(b.getBoolean(com.navdy.hud.app.device.gps.GpsConstants.USING_PHONE_LOCATION), b.getBoolean(com.navdy.hud.app.device.gps.GpsConstants.USING_UBLOX_LOCATION)));
                    return;
                case 3:
                    this.sLogger.i("Driving started");
                    com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getBus().post(DRIVING_STARTED);
                    return;
                case 4:
                    this.sLogger.i("Driving stopped");
                    com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getBus().post(DRIVING_STOPPED);
                    return;
                case 5:
                    this.sLogger.v("warm reset ublox");
                    com.navdy.hud.app.device.gps.GpsDeadReckoningManager.getInstance().sendWarmReset();
                    com.navdy.hud.app.framework.voice.TTSUtils.debugShowGpsReset("Warm Reset Ublox");
                    return;
                case 6:
                    this.sLogger.v("esf-raw");
                    com.navdy.hud.app.device.gps.GpsDeadReckoningManager.getInstance().enableEsfRaw();
                    return;
                case 7:
                    android.os.Bundle data = intent.getBundleExtra(com.navdy.hud.app.device.gps.GpsConstants.GPS_EVENT_SATELLITE_DATA);
                    if (data != null) {
                        com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getBus().post(new com.navdy.hud.app.device.gps.GpsUtils.GpsSatelliteData(data));
                        return;
                    }
                    return;
                case 8:
                    java.lang.String path = intent.getStringExtra(com.navdy.hud.app.device.gps.GpsConstants.GPS_EXTRA_LOG_PATH);
                    if (!android.text.TextUtils.isEmpty(path)) {
                        com.navdy.hud.app.device.gps.GpsDeadReckoningManager.getInstance().dumpGpsInfo(path);
                        return;
                    } else {
                        this.sLogger.w("Invalid gps log path:" + path);
                        return;
                    }
                default:
                    return;
            }
        } catch (Throwable t) {
            this.sLogger.e(t);
        }
        this.sLogger.e(t);
    }
}
