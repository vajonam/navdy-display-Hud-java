package com.navdy.hud.app.maps.notification;

public class RouteCalculationEventHandler {
    private static final int MIN_ROUTE_CALC_DISPLAY_TIME = ((int) java.util.concurrent.TimeUnit.SECONDS.toMillis(2));
    private static final int ROUTE_CALC_DELAY_TIME = ((int) java.util.concurrent.TimeUnit.SECONDS.toMillis(1));
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.maps.notification.RouteCalculationEventHandler.class);
    private com.navdy.hud.app.ui.framework.INotificationAnimationListener animationListener = new com.navdy.hud.app.maps.notification.RouteCalculationEventHandler.Anon1();
    private com.squareup.otto.Bus bus;
    private com.navdy.hud.app.maps.MapEvents.RouteCalculationEvent currentRouteCalcEvent;
    private android.os.Handler handler = new android.os.Handler(android.os.Looper.getMainLooper());
    private long routeCalcStartTime;
    private com.navdy.hud.app.maps.MapEvents.RouteCalculationEvent showRoutesCalcEvent;
    private java.lang.Runnable startRouteRunnable = new com.navdy.hud.app.maps.notification.RouteCalculationEventHandler.Anon2();
    private final java.lang.String startTrip;

    class Anon1 implements com.navdy.hud.app.ui.framework.INotificationAnimationListener {
        Anon1() {
        }

        public void onStart(java.lang.String id, com.navdy.hud.app.framework.notifications.NotificationType type, com.navdy.hud.app.ui.framework.UIStateManager.Mode mode) {
            if (mode == com.navdy.hud.app.ui.framework.UIStateManager.Mode.COLLAPSE && com.navdy.hud.app.framework.notifications.NotificationId.ROUTE_CALC_NOTIFICATION_ID.equals(id)) {
                com.navdy.hud.app.maps.notification.RouteCalculationNotification notif = (com.navdy.hud.app.maps.notification.RouteCalculationNotification) com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().getNotification(com.navdy.hud.app.framework.notifications.NotificationId.ROUTE_CALC_NOTIFICATION_ID);
                if (notif != null) {
                    com.navdy.hud.app.maps.notification.RouteCalculationEventHandler.logger.v("hideStartTrip");
                    notif.hideStartTrip();
                }
            }
        }

        public void onStop(java.lang.String id, com.navdy.hud.app.framework.notifications.NotificationType type, com.navdy.hud.app.ui.framework.UIStateManager.Mode mode) {
            if (mode == com.navdy.hud.app.ui.framework.UIStateManager.Mode.EXPAND && com.navdy.hud.app.framework.notifications.NotificationId.ROUTE_CALC_NOTIFICATION_ID.equals(id)) {
                com.navdy.hud.app.maps.notification.RouteCalculationEventHandler.logger.v("showStartTrip: check");
                com.navdy.hud.app.maps.notification.RouteCalculationNotification notif = (com.navdy.hud.app.maps.notification.RouteCalculationNotification) com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().getNotification(com.navdy.hud.app.framework.notifications.NotificationId.ROUTE_CALC_NOTIFICATION_ID);
                if (notif != null && notif.isStarting()) {
                    com.navdy.hud.app.maps.notification.RouteCalculationEventHandler.logger.v("showStartTrip");
                    notif.showStartTrip();
                }
            }
        }
    }

    class Anon2 implements java.lang.Runnable {
        Anon2() {
        }

        public void run() {
            com.navdy.hud.app.maps.notification.RouteCalculationEventHandler.this.startRoute();
        }
    }

    static /* synthetic */ class Anon3 {
        static final /* synthetic */ int[] $SwitchMap$com$here$android$mpa$routing$RouteOptions$Type = new int[com.here.android.mpa.routing.RouteOptions.Type.values().length];

        static {
            $SwitchMap$com$navdy$hud$app$maps$MapEvents$RouteCalculationState = new int[com.navdy.hud.app.maps.MapEvents.RouteCalculationState.values().length];
            try {
                $SwitchMap$com$navdy$hud$app$maps$MapEvents$RouteCalculationState[com.navdy.hud.app.maps.MapEvents.RouteCalculationState.STARTED.ordinal()] = 1;
            } catch (java.lang.NoSuchFieldError e) {
            }
            try {
                $SwitchMap$com$navdy$hud$app$maps$MapEvents$RouteCalculationState[com.navdy.hud.app.maps.MapEvents.RouteCalculationState.FINDING_NAV_COORDINATES.ordinal()] = 2;
            } catch (java.lang.NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$com$navdy$hud$app$maps$MapEvents$RouteCalculationState[com.navdy.hud.app.maps.MapEvents.RouteCalculationState.STOPPED.ordinal()] = 3;
            } catch (java.lang.NoSuchFieldError e3) {
            }
            try {
                $SwitchMap$com$navdy$hud$app$maps$MapEvents$RouteCalculationState[com.navdy.hud.app.maps.MapEvents.RouteCalculationState.IN_PROGRESS.ordinal()] = 4;
            } catch (java.lang.NoSuchFieldError e4) {
            }
            try {
                $SwitchMap$com$navdy$hud$app$maps$MapEvents$RouteCalculationState[com.navdy.hud.app.maps.MapEvents.RouteCalculationState.ABORT_NAVIGATION.ordinal()] = 5;
            } catch (java.lang.NoSuchFieldError e5) {
            }
            try {
                $SwitchMap$com$here$android$mpa$routing$RouteOptions$Type[com.here.android.mpa.routing.RouteOptions.Type.SHORTEST.ordinal()] = 1;
            } catch (java.lang.NoSuchFieldError e6) {
            }
            try {
                $SwitchMap$com$here$android$mpa$routing$RouteOptions$Type[com.here.android.mpa.routing.RouteOptions.Type.FASTEST.ordinal()] = 2;
            } catch (java.lang.NoSuchFieldError e7) {
            }
        }
    }

    public RouteCalculationEventHandler(com.squareup.otto.Bus bus2) {
        logger.v("ctor");
        android.content.res.Resources resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        this.bus = bus2;
        this.startTrip = resources.getString(com.navdy.hud.app.R.string.start_trip);
        com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getUiStateManager().addNotificationAnimationListener(this.animationListener);
        bus2.register(this);
    }

    @com.squareup.otto.Subscribe
    public void onFirstManeuver(com.navdy.hud.app.maps.MapEvents.ManeuverEvent event) {
        logger.v("onFirstManeuver");
    }

    @com.squareup.otto.Subscribe
    public void onNavigationModeChanged(com.navdy.hud.app.maps.MapEvents.NavigationModeChange event) {
        boolean switching = com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().isSwitchingToNewRoute();
        logger.v("onNavigationModeChanged event[" + event.navigationMode + "] switching=" + switching);
        if (event.navigationMode == com.navdy.hud.app.maps.NavigationMode.MAP && !switching) {
            this.showRoutesCalcEvent = null;
            dismissNotification();
        }
    }

    @com.squareup.otto.Subscribe
    public void onRouteCalculation(com.navdy.hud.app.maps.MapEvents.RouteCalculationEvent event) {
        java.lang.String label;
        java.lang.String destinationLabel;
        java.lang.String destinationLabel2;
        java.lang.String label2;
        com.navdy.hud.app.framework.destinations.Destination transformedDestination;
        logger.v("onRouteCalculation event[" + event.state + "]");
        com.navdy.hud.app.framework.notifications.NotificationManager notificationManager = com.navdy.hud.app.framework.notifications.NotificationManager.getInstance();
        switch (event.state) {
            case STARTED:
            case FINDING_NAV_COORDINATES:
                com.navdy.hud.app.screen.BaseScreen screen = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getUiStateManager().getCurrentScreen();
                if (screen != null && screen.getScreen() == com.navdy.service.library.events.ui.Screen.SCREEN_DESTINATION_PICKER) {
                    logger.v("onRouteCalculation: destination picker on");
                    notificationManager.enableNotifications(true);
                }
                this.handler.removeCallbacks(this.startRouteRunnable);
                this.showRoutesCalcEvent = null;
                this.currentRouteCalcEvent = event;
                this.routeCalcStartTime = android.os.SystemClock.elapsedRealtime();
                com.navdy.hud.app.framework.destinations.DestinationsManager.getInstance().clearSuggestedDestination();
                com.navdy.hud.app.ui.component.homescreen.HomeScreenView homeScreenView = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getUiStateManager().getHomescreenView();
                if (homeScreenView != null) {
                    homeScreenView.setShowCollapsedNotification(false);
                }
                com.navdy.hud.app.maps.notification.RouteCalculationNotification notif = (com.navdy.hud.app.maps.notification.RouteCalculationNotification) notificationManager.getNotification(com.navdy.hud.app.framework.notifications.NotificationId.ROUTE_CALC_NOTIFICATION_ID);
                if (notif == null) {
                    notif = new com.navdy.hud.app.maps.notification.RouteCalculationNotification(this, this.bus);
                }
                java.lang.String destinationAddress = null;
                java.lang.String destinationDistance = null;
                if (event.state == com.navdy.hud.app.maps.MapEvents.RouteCalculationState.STARTED) {
                    if (!android.text.TextUtils.isEmpty(event.request.label)) {
                        label2 = event.request.label;
                        destinationLabel2 = label2;
                        destinationAddress = event.request.streetAddress;
                    } else if (!android.text.TextUtils.isEmpty(event.request.streetAddress)) {
                        label2 = event.request.streetAddress;
                        destinationLabel2 = label2;
                    } else {
                        label2 = "";
                        destinationLabel2 = label2;
                    }
                    transformedDestination = com.navdy.hud.app.framework.destinations.DestinationsManager.getInstance().transformToInternalDestination(event.request.requestDestination);
                } else {
                    if (event.lookupDestination != null && !android.text.TextUtils.isEmpty(event.lookupDestination.destinationTitle)) {
                        label = event.lookupDestination.destinationTitle;
                        destinationLabel = label;
                        destinationAddress = event.lookupDestination.fullAddress;
                    } else if (event.lookupDestination == null || android.text.TextUtils.isEmpty(event.lookupDestination.fullAddress)) {
                        label = "";
                        destinationLabel = label;
                    } else {
                        label = event.lookupDestination.fullAddress;
                        destinationLabel = label;
                    }
                    transformedDestination = event.lookupDestination;
                }
                java.lang.String initials = null;
                com.navdy.hud.app.ui.component.vlist.VerticalList.Model model = com.navdy.hud.app.ui.component.mainmenu.PlacesMenu.getPlaceModel(transformedDestination, 0, null, null);
                if (model.type == com.navdy.hud.app.ui.component.vlist.VerticalList.ModelType.TWO_ICONS) {
                    model.iconSelectedColor = 0;
                } else if (transformedDestination == null) {
                    model.iconSelectedColor = com.navdy.hud.app.maps.notification.RouteCalculationNotification.notifBkColor;
                } else if (!(transformedDestination == null || transformedDestination.destinationType == null || transformedDestination.destinationType != com.navdy.hud.app.framework.destinations.Destination.DestinationType.DEFAULT || transformedDestination.favoriteDestinationType == null || transformedDestination.favoriteDestinationType != com.navdy.hud.app.framework.destinations.Destination.FavoriteDestinationType.FAVORITE_NONE)) {
                    model.icon = com.navdy.hud.app.R.drawable.icon_mm_places_2;
                    model.iconSelectedColor = com.navdy.hud.app.maps.notification.RouteCalculationNotification.notifBkColor;
                }
                int icon = model.icon;
                int color = model.iconSelectedColor;
                if (transformedDestination != null) {
                    initials = com.navdy.hud.app.framework.destinations.DestinationsManager.getInitials(transformedDestination.destinationTitle, transformedDestination.favoriteDestinationType);
                    if (!(transformedDestination.destinationIcon == 0 || transformedDestination.destinationIconBkColor == 0)) {
                        icon = transformedDestination.destinationIcon;
                        color = transformedDestination.destinationIconBkColor;
                    }
                    destinationDistance = transformedDestination.distanceStr;
                }
                android.content.res.Resources res = com.navdy.hud.app.HudApplication.getAppContext().getResources();
                java.lang.String tts = null;
                switch (com.navdy.hud.app.maps.notification.RouteCalculationEventHandler.Anon3.$SwitchMap$com$here$android$mpa$routing$RouteOptions$Type[event.routeOptions.getRouteType().ordinal()]) {
                    case 1:
                        tts = res.getString(com.navdy.hud.app.R.string.finding_route, new java.lang.Object[]{com.navdy.hud.app.maps.notification.RouteCalculationNotification.shortestRoute, label2});
                        break;
                    case 2:
                        tts = res.getString(com.navdy.hud.app.R.string.finding_route, new java.lang.Object[]{com.navdy.hud.app.maps.notification.RouteCalculationNotification.fastestRoute, label2});
                        break;
                }
                com.navdy.service.library.events.destination.Destination destination = null;
                if (event.lookupDestination != null) {
                    destination = com.navdy.hud.app.framework.destinations.DestinationsManager.getInstance().transformToProtoDestination(event.lookupDestination);
                }
                java.lang.String requestRouteId = null;
                if (event.request != null) {
                    requestRouteId = event.request.requestId;
                }
                int notifColor = 0;
                if (color == 0 && model.iconFluctuatorColor != -1) {
                    notifColor = model.iconFluctuatorColor;
                }
                notif.showRouteSearch(this.startTrip, label2, icon, color, notifColor, initials, tts, destination, requestRouteId, destinationLabel2, destinationAddress, destinationDistance);
                notificationManager.addNotification(notif);
                return;
            case STOPPED:
                if (!event.stopped && this.currentRouteCalcEvent != null && this.currentRouteCalcEvent.response != null) {
                    event.stopped = true;
                    if (this.currentRouteCalcEvent.response.status == com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS) {
                        com.navdy.hud.app.maps.notification.RouteCalculationNotification routeCNotif = (com.navdy.hud.app.maps.notification.RouteCalculationNotification) com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().getNotification(com.navdy.hud.app.framework.notifications.NotificationId.ROUTE_CALC_NOTIFICATION_ID);
                        if (routeCNotif != null) {
                            logger.v("hideStartTrip");
                            routeCNotif.hideStartTrip();
                        }
                        long diff = android.os.SystemClock.elapsedRealtime() - this.routeCalcStartTime;
                        if (diff < ((long) MIN_ROUTE_CALC_DISPLAY_TIME)) {
                            logger.v("route calc too fast[" + diff + "] wait");
                            this.handler.postDelayed(this.startRouteRunnable, (long) ROUTE_CALC_DELAY_TIME);
                            return;
                        }
                        logger.v("route calc took[" + diff + "]");
                        startRoute();
                        return;
                    }
                    logger.e("routing calculation not successful:" + this.currentRouteCalcEvent.response.status);
                    if (this.currentRouteCalcEvent.response.status == com.navdy.service.library.events.RequestStatus.REQUEST_CANCELLED) {
                        logger.v("request cancelled");
                        this.currentRouteCalcEvent = null;
                        dismissNotification();
                        return;
                    }
                    logger.v("show error");
                    com.navdy.hud.app.maps.notification.RouteCalculationNotification notif2 = (com.navdy.hud.app.maps.notification.RouteCalculationNotification) notificationManager.getNotification(com.navdy.hud.app.framework.notifications.NotificationId.ROUTE_CALC_NOTIFICATION_ID);
                    if (notif2 == null) {
                        notif2 = new com.navdy.hud.app.maps.notification.RouteCalculationNotification(this, this.bus);
                    }
                    notif2.showError();
                    notificationManager.addNotification(notif2);
                    return;
                }
                return;
            case ABORT_NAVIGATION:
                logger.v("abort-nav routeCalc event[ABORT_NAVIGATION] " + event.pendingNavigationRequestId);
                if (this.currentRouteCalcEvent == null || this.currentRouteCalcEvent.request == null) {
                    logger.v("abort-nav:no current route calc event");
                    return;
                } else if (!android.text.TextUtils.equals(this.currentRouteCalcEvent.request.requestId, event.pendingNavigationRequestId)) {
                    logger.v("abort-nav:does not match current request:" + this.currentRouteCalcEvent.request.requestId);
                    return;
                } else {
                    logger.v("abort-nav: found current route id, cancel");
                    this.currentRouteCalcEvent = null;
                    if (event.abortOriginDisplay) {
                        this.bus.post(new com.navdy.hud.app.event.RemoteEvent(new com.navdy.service.library.events.navigation.NavigationRouteCancelRequest(event.pendingNavigationRequestId)));
                        dismissNotification();
                        return;
                    }
                    logger.v("abort-nav: sent navrouteresponse cancel [" + event.pendingNavigationRequestId + "]");
                    com.navdy.service.library.events.navigation.NavigationRouteResponse response = new com.navdy.service.library.events.navigation.NavigationRouteResponse.Builder().requestId(event.pendingNavigationRequestId).status(com.navdy.service.library.events.RequestStatus.REQUEST_CANCELLED).destination(new com.navdy.service.library.events.location.Coordinate.Builder().latitude(java.lang.Double.valueOf(0.0d)).longitude(java.lang.Double.valueOf(0.0d)).build()).build();
                    com.squareup.otto.Bus bus2 = this.bus;
                    com.navdy.hud.app.event.RemoteEvent remoteEvent = new com.navdy.hud.app.event.RemoteEvent(response);
                    bus2.post(remoteEvent);
                    com.navdy.hud.app.maps.here.HereRouteManager.clearActiveRouteCalc(event.pendingNavigationRequestId);
                    dismissNotification();
                    return;
                }
            default:
                return;
        }
    }

    private void navigateToRoute(com.navdy.service.library.events.navigation.NavigationRouteResult result) {
        com.navdy.service.library.events.navigation.NavigationSessionRequest navigationSessionRequest = new com.navdy.service.library.events.navigation.NavigationSessionRequest.Builder().newState(com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_STARTED).label(this.currentRouteCalcEvent.response.label).routeId(result.routeId).simulationSpeed(java.lang.Integer.valueOf(0)).originDisplay(java.lang.Boolean.valueOf(true)).build();
        this.bus.post(navigationSessionRequest);
        this.bus.post(new com.navdy.hud.app.event.RemoteEvent(navigationSessionRequest));
    }

    public com.navdy.hud.app.maps.MapEvents.RouteCalculationEvent getCurrentRouteCalcEvent() {
        return this.currentRouteCalcEvent;
    }

    public void clearCurrentRouteCalcEvent() {
        this.currentRouteCalcEvent = null;
        this.handler.removeCallbacks(this.startRouteRunnable);
    }

    private void dismissNotification() {
        com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().removeNotification(com.navdy.hud.app.framework.notifications.NotificationId.ROUTE_CALC_NOTIFICATION_ID);
    }

    public boolean hasMoreRoutes() {
        return this.showRoutesCalcEvent != null;
    }

    /* access modifiers changed from: private */
    public void startRoute() {
        if (this.currentRouteCalcEvent == null || this.currentRouteCalcEvent.request == null || this.currentRouteCalcEvent.response == null || this.currentRouteCalcEvent.response.results == null) {
            logger.v("invalid state");
            dismissNotification();
            return;
        }
        logger.v("put user on route");
        navigateToRoute((com.navdy.service.library.events.navigation.NavigationRouteResult) this.currentRouteCalcEvent.response.results.get(0));
        int routes = this.currentRouteCalcEvent.response.results.size();
        if (routes > 1) {
            logger.v("more route available:" + routes);
            this.showRoutesCalcEvent = this.currentRouteCalcEvent;
            showMoreRoutes();
            return;
        }
        logger.v("more route NOT available:" + routes);
        this.currentRouteCalcEvent = null;
        dismissNotification();
    }

    private void showMoreRoutes() {
        logger.v("showMoreRoutes");
        if (this.showRoutesCalcEvent != null) {
            com.navdy.hud.app.framework.notifications.NotificationManager notificationManager = com.navdy.hud.app.framework.notifications.NotificationManager.getInstance();
            com.navdy.hud.app.maps.notification.RouteCalculationNotification notif = (com.navdy.hud.app.maps.notification.RouteCalculationNotification) notificationManager.getNotification(com.navdy.hud.app.framework.notifications.NotificationId.ROUTE_CALC_NOTIFICATION_ID);
            if (notif == null) {
                notif = new com.navdy.hud.app.maps.notification.RouteCalculationNotification(this, this.bus);
            }
            notif.showRoutes(this.showRoutesCalcEvent);
            this.showRoutesCalcEvent = null;
            logger.v("showMoreRoutes: posted notif");
            notificationManager.addNotification(notif);
        }
    }
}
