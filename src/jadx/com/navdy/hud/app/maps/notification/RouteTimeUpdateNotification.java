package com.navdy.hud.app.maps.notification;

public class RouteTimeUpdateNotification extends com.navdy.hud.app.maps.notification.BaseTrafficNotification {
    private static final int TAG_DISMISS = 102;
    private static final int TAG_GO = 101;
    private static final int TAG_READ = 103;
    private static java.lang.StringBuilder ampmMarker = new java.lang.StringBuilder();
    private static int badTrafficColor;
    private static java.lang.String delay;
    private static java.util.List<com.navdy.hud.app.ui.component.ChoiceLayout2.Choice> delayChoices = new java.util.ArrayList(1);
    private static java.lang.String done;
    private static java.lang.String fasterRoute;
    private static java.lang.String fasterRouteAvailable;
    private static java.lang.String go;
    private static java.lang.String hour;
    private static java.lang.String hours;
    private static java.lang.String hr;
    private static java.lang.String min;
    private static java.lang.String minute;
    private static java.lang.String minutes;
    private static int normalTrafficColor;
    private static java.util.List<com.navdy.hud.app.ui.component.ChoiceLayout2.Choice> reRouteChoices = new java.util.ArrayList(2);
    private static android.content.res.Resources resources;
    private static java.lang.String save;
    private static int trafficRerouteColor;
    private static java.lang.String trafficUpdate;
    private static java.lang.String via;
    /* access modifiers changed from: private */
    public com.squareup.otto.Bus bus;
    private com.navdy.service.library.events.audio.CancelSpeechRequest cancelSpeechRequest;
    private com.navdy.hud.app.ui.component.ChoiceLayout2.IListener choiceListener = new com.navdy.hud.app.maps.notification.RouteTimeUpdateNotification.Anon1();
    private android.view.ViewGroup container;
    private java.lang.String distanceDiffStr = "";
    private java.lang.String distanceStr = "";
    private java.lang.String etaStr = "";
    private java.lang.String etaUnitStr = "";
    private android.view.ViewGroup extendedContainer;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.maps.MapEvents.TrafficRerouteEvent fasterRouteEvent;
    private java.lang.String id;
    private com.navdy.hud.app.ui.component.image.ColorImageView mainImage;
    private android.widget.TextView mainTitle;
    private android.widget.ImageView sideImage;
    private android.widget.TextView subTitle;
    private android.widget.TextView text1;
    private android.widget.TextView text2;
    private android.widget.TextView text3;
    private java.lang.String timeStr = "";
    private java.lang.String timeStrExpanded = "";
    private java.lang.String timeUnitStr = "";
    /* access modifiers changed from: private */
    public com.navdy.hud.app.maps.MapEvents.TrafficDelayEvent trafficDelayEvent;
    private java.lang.String ttsMessage;
    private com.navdy.hud.app.framework.notifications.NotificationType type;

    class Anon1 implements com.navdy.hud.app.ui.component.ChoiceLayout2.IListener {
        Anon1() {
        }

        public void executeItem(com.navdy.hud.app.ui.component.ChoiceLayout2.Selection selection) {
            if (com.navdy.hud.app.maps.notification.RouteTimeUpdateNotification.this.controller != null) {
                switch (selection.id) {
                    case 5:
                        if (com.navdy.hud.app.maps.notification.RouteTimeUpdateNotification.this.controller != null && com.navdy.hud.app.maps.notification.RouteTimeUpdateNotification.this.controller.isExpandedWithStack()) {
                            com.navdy.hud.app.maps.notification.RouteTimeUpdateNotification.this.controller.collapseNotification(false, false);
                            return;
                        }
                        return;
                    case 101:
                        if (com.navdy.hud.app.maps.notification.RouteTimeUpdateNotification.this.fasterRouteEvent != null) {
                            com.navdy.hud.app.maps.notification.RouteTimeUpdateNotification.this.bus.post(com.navdy.hud.app.maps.MapEvents.TrafficRerouteAction.REROUTE);
                            com.navdy.hud.app.maps.notification.RouteTimeUpdateNotification.this.fasterRouteEvent = null;
                        }
                        com.navdy.hud.app.maps.notification.RouteTimeUpdateNotification.this.dismissNotification();
                        return;
                    case 102:
                        if (com.navdy.hud.app.maps.notification.RouteTimeUpdateNotification.this.fasterRouteEvent != null) {
                            com.navdy.hud.app.maps.notification.RouteTimeUpdateNotification.this.bus.post(com.navdy.hud.app.maps.MapEvents.TrafficRerouteAction.DISMISS);
                            com.navdy.hud.app.maps.notification.RouteTimeUpdateNotification.this.fasterRouteEvent = null;
                        } else if (com.navdy.hud.app.maps.notification.RouteTimeUpdateNotification.this.trafficDelayEvent != null) {
                            com.navdy.hud.app.maps.notification.RouteTimeUpdateNotification.this.trafficDelayEvent = null;
                        }
                        com.navdy.hud.app.maps.notification.RouteTimeUpdateNotification.this.dismissNotification();
                        return;
                    case 103:
                        if (com.navdy.hud.app.maps.notification.RouteTimeUpdateNotification.this.fasterRouteEvent != null) {
                            com.navdy.hud.app.maps.notification.RouteTimeUpdateNotification.this.switchToExpandedMode();
                            return;
                        }
                        return;
                    default:
                        return;
                }
            }
        }

        public void itemSelected(com.navdy.hud.app.ui.component.ChoiceLayout2.Selection selection) {
        }
    }

    public RouteTimeUpdateNotification(java.lang.String id2, com.navdy.hud.app.framework.notifications.NotificationType type2, com.squareup.otto.Bus bus2) {
        this.id = id2;
        this.type = type2;
        this.cancelSpeechRequest = new com.navdy.service.library.events.audio.CancelSpeechRequest(id2);
        if (trafficUpdate == null) {
            resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
            trafficUpdate = resources.getString(com.navdy.hud.app.R.string.traffic_notification_delay_title);
            fasterRoute = resources.getString(com.navdy.hud.app.R.string.traffic_reroute_faster_route);
            delay = resources.getString(com.navdy.hud.app.R.string.traffic_notification_delay);
            done = resources.getString(com.navdy.hud.app.R.string.done);
            min = resources.getString(com.navdy.hud.app.R.string.min);
            hr = resources.getString(com.navdy.hud.app.R.string.hr);
            fasterRouteAvailable = resources.getString(com.navdy.hud.app.R.string.traffic_faster_route_available);
            hour = resources.getString(com.navdy.hud.app.R.string.hour);
            hours = resources.getString(com.navdy.hud.app.R.string.hours);
            minute = resources.getString(com.navdy.hud.app.R.string.minute);
            minutes = resources.getString(com.navdy.hud.app.R.string.minutes);
            save = resources.getString(com.navdy.hud.app.R.string.traffic_notification_save);
            go = resources.getString(com.navdy.hud.app.R.string.traffic_reroute_go);
            via = resources.getString(com.navdy.hud.app.R.string.traffic_reroute_via);
            normalTrafficColor = resources.getColor(com.navdy.hud.app.R.color.traffic_good);
            badTrafficColor = resources.getColor(com.navdy.hud.app.R.color.traffic_bad);
            trafficRerouteColor = resources.getColor(com.navdy.hud.app.R.color.grey_4a);
            int dismissColor = resources.getColor(com.navdy.hud.app.R.color.glance_dismiss);
            int readColor = resources.getColor(com.navdy.hud.app.R.color.glance_ok_blue);
            int goColor = resources.getColor(com.navdy.hud.app.R.color.glance_ok_go);
            java.lang.String dismiss = resources.getString(com.navdy.hud.app.R.string.dismiss);
            delayChoices.add(new com.navdy.hud.app.ui.component.ChoiceLayout2.Choice(102, com.navdy.hud.app.R.drawable.icon_glances_dismiss, dismissColor, com.navdy.hud.app.R.drawable.icon_glances_dismiss, -16777216, dismiss, dismissColor));
            reRouteChoices.add(new com.navdy.hud.app.ui.component.ChoiceLayout2.Choice(103, com.navdy.hud.app.R.drawable.icon_glances_read, readColor, com.navdy.hud.app.R.drawable.icon_glances_read, -16777216, com.navdy.hud.app.framework.glance.GlanceConstants.read, readColor));
            reRouteChoices.add(new com.navdy.hud.app.ui.component.ChoiceLayout2.Choice(101, com.navdy.hud.app.R.drawable.icon_glances_ok_strong, goColor, com.navdy.hud.app.R.drawable.icon_glances_ok_strong, -16777216, go, goColor));
            reRouteChoices.add(new com.navdy.hud.app.ui.component.ChoiceLayout2.Choice(102, com.navdy.hud.app.R.drawable.icon_glances_dismiss, dismissColor, com.navdy.hud.app.R.drawable.icon_glances_dismiss, -16777216, dismiss, dismissColor));
        }
        this.bus = bus2;
        bus2.register(this);
    }

    public com.navdy.hud.app.framework.notifications.NotificationType getType() {
        return this.type;
    }

    public java.lang.String getId() {
        return this.id;
    }

    public android.view.View getView(android.content.Context context) {
        if (this.container == null) {
            this.container = (android.view.ViewGroup) com.navdy.hud.app.framework.glance.GlanceViewCache.getView(com.navdy.hud.app.framework.glance.GlanceViewCache.ViewType.SMALL_SIGN, context);
            this.mainTitle = (android.widget.TextView) this.container.findViewById(com.navdy.hud.app.R.id.mainTitle);
            this.subTitle = (android.widget.TextView) this.container.findViewById(com.navdy.hud.app.R.id.subTitle);
            this.mainImage = (com.navdy.hud.app.ui.component.image.ColorImageView) this.container.findViewById(com.navdy.hud.app.R.id.mainImage);
            this.sideImage = (android.widget.ImageView) this.container.findViewById(com.navdy.hud.app.R.id.sideImage);
            this.text1 = (android.widget.TextView) this.container.findViewById(com.navdy.hud.app.R.id.text1);
            this.text2 = (android.widget.TextView) this.container.findViewById(com.navdy.hud.app.R.id.text2);
            this.text3 = (android.widget.TextView) this.container.findViewById(com.navdy.hud.app.R.id.text3);
            this.choiceLayout = (com.navdy.hud.app.ui.component.ChoiceLayout2) this.container.findViewById(com.navdy.hud.app.R.id.choiceLayout);
        }
        return this.container;
    }

    public android.view.View getExpandedView(android.content.Context context, java.lang.Object data) {
        this.extendedContainer = (android.view.ViewGroup) com.navdy.hud.app.framework.glance.GlanceViewCache.getView(com.navdy.hud.app.framework.glance.GlanceViewCache.ViewType.BIG_MULTI_TEXT, context);
        setExpandedContent(context);
        return this.extendedContainer;
    }

    /* access modifiers changed from: 0000 */
    public void setExpandedContent(android.content.Context context) {
        java.lang.String str;
        android.widget.TextView textView1 = (android.widget.TextView) this.extendedContainer.findViewById(com.navdy.hud.app.R.id.title1);
        textView1.setTextAppearance(context, com.navdy.hud.app.R.style.glance_title_1);
        android.widget.TextView textView2 = (android.widget.TextView) this.extendedContainer.findViewById(com.navdy.hud.app.R.id.title2);
        textView2.setTextAppearance(context, com.navdy.hud.app.R.style.glance_title_2);
        if (this.fasterRouteEvent != null) {
            textView1.setText(fasterRouteAvailable);
            if (this.timeStr == null) {
                buildData();
            }
            if (this.fasterRouteEvent.etaDifference != 0) {
                str = resources.getString(com.navdy.hud.app.R.string.traffic_reroute_msg, new java.lang.Object[]{this.timeStr, this.timeStrExpanded, this.fasterRouteEvent.additionalVia, this.etaStr, this.etaUnitStr, this.distanceStr, this.distanceDiffStr});
            } else {
                str = resources.getString(com.navdy.hud.app.R.string.traffic_reroute_msg_eta_only, new java.lang.Object[]{this.fasterRouteEvent.additionalVia, this.etaStr, this.etaUnitStr, this.distanceStr, this.distanceDiffStr});
            }
            textView2.setText(str);
        }
        textView1.setVisibility(0);
        textView2.setVisibility(0);
    }

    public int getExpandedViewIndicatorColor() {
        return 0;
    }

    public void onStart(com.navdy.hud.app.framework.notifications.INotificationController controller) {
        super.onStart(controller);
        updateState();
        if (!controller.isExpandedWithStack()) {
            this.container.setTranslationX(0.0f);
            this.container.setTranslationY(0.0f);
            this.container.setAlpha(1.0f);
            this.mainTitle.setAlpha(1.0f);
            this.subTitle.setAlpha(1.0f);
            this.choiceLayout.setAlpha(1.0f);
            return;
        }
        this.mainTitle.setAlpha(0.0f);
        this.subTitle.setAlpha(0.0f);
        this.choiceLayout.setAlpha(0.0f);
    }

    public void onUpdate() {
        updateState();
    }

    public void onStop() {
        cancelTts();
        super.onStop();
        if (this.container != null) {
            cleanupView(com.navdy.hud.app.framework.glance.GlanceViewCache.ViewType.SMALL_SIGN, this.container);
            this.container = null;
        }
        if (this.extendedContainer != null) {
            cleanupView(com.navdy.hud.app.framework.glance.GlanceViewCache.ViewType.BIG_MULTI_TEXT, this.extendedContainer);
            this.extendedContainer = null;
        }
    }

    public int getTimeout() {
        return 0;
    }

    public boolean isAlive() {
        return true;
    }

    public boolean isPurgeable() {
        return false;
    }

    public boolean canAddToStackIfCurrentExists() {
        return true;
    }

    public int getColor() {
        if (this.fasterRouteEvent != null) {
            return com.navdy.hud.app.framework.glance.GlanceConstants.colorFasterRoute;
        }
        return com.navdy.hud.app.framework.glance.GlanceConstants.colorTrafficDelay;
    }

    public void onNotificationEvent(com.navdy.hud.app.ui.framework.UIStateManager.Mode mode) {
    }

    public void onExpandedNotificationEvent(com.navdy.hud.app.ui.framework.UIStateManager.Mode mode) {
        if (mode != com.navdy.hud.app.ui.framework.UIStateManager.Mode.COLLAPSE) {
            sendtts();
        } else if (this.controller != null) {
            cancelTts();
            if (com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().isNotificationMarkedForRemoval(this.id)) {
                dismissNotification();
            } else {
                updateState();
            }
        }
    }

    public void onExpandedNotificationSwitched() {
        if (this.controller != null && this.controller.isExpandedWithStack()) {
            sendtts();
        }
    }

    public android.animation.AnimatorSet getViewSwitchAnimation(boolean viewIn) {
        android.animation.AnimatorSet set = new android.animation.AnimatorSet();
        if (!viewIn) {
            set.playTogether(new android.animation.Animator[]{android.animation.ObjectAnimator.ofFloat(this.mainTitle, android.view.View.ALPHA, new float[]{1.0f, 0.0f}), android.animation.ObjectAnimator.ofFloat(this.subTitle, android.view.View.ALPHA, new float[]{1.0f, 0.0f}), android.animation.ObjectAnimator.ofFloat(this.choiceLayout, android.view.View.ALPHA, new float[]{1.0f, 0.0f})});
        } else {
            set.playTogether(new android.animation.Animator[]{android.animation.ObjectAnimator.ofFloat(this.mainTitle, android.view.View.ALPHA, new float[]{0.0f, 1.0f}), android.animation.ObjectAnimator.ofFloat(this.subTitle, android.view.View.ALPHA, new float[]{0.0f, 1.0f}), android.animation.ObjectAnimator.ofFloat(this.choiceLayout, android.view.View.ALPHA, new float[]{0.0f, 1.0f})});
        }
        return set;
    }

    public boolean expandNotification() {
        if (this.controller == null) {
            return false;
        }
        switchToExpandedMode();
        return true;
    }

    private void updateState() {
        if (this.controller != null) {
            this.logger.v("updateState");
            if (this.trafficDelayEvent == null && this.fasterRouteEvent != null) {
                this.mainTitle.setText(fasterRoute);
                if (this.fasterRouteEvent.via != null) {
                    this.subTitle.setText(via + " " + this.fasterRouteEvent.via);
                } else {
                    this.subTitle.setText("");
                }
                this.mainImage.setColor(trafficRerouteColor);
                this.sideImage.setImageResource(com.navdy.hud.app.R.drawable.icon_badge_route);
                buildData();
                if (this.fasterRouteEvent.etaDifference != 0) {
                    this.text1.setText(save);
                    ((android.view.ViewGroup.MarginLayoutParams) this.text2.getLayoutParams()).topMargin = com.navdy.hud.app.framework.glance.GlanceConstants.calendarNormalMargin;
                    this.text2.setTextAppearance(com.navdy.hud.app.HudApplication.getAppContext(), com.navdy.hud.app.R.style.small_glance_sign_text_1);
                    this.text2.setText(this.timeStr);
                    this.text3.setText(this.timeUnitStr);
                }
                if (this.controller.isExpandedWithStack()) {
                    this.choiceLayout.setChoices(com.navdy.hud.app.framework.glance.GlanceConstants.backChoice2, 0, this.choiceListener, 0.5f);
                    this.choiceLayout.setTag(com.navdy.hud.app.framework.glance.GlanceConstants.backChoice);
                } else {
                    this.choiceLayout.setChoices(reRouteChoices, 1, this.choiceListener, 0.5f);
                }
                if (this.extendedContainer != null && this.controller.isExpandedWithStack()) {
                    setExpandedContent(com.navdy.hud.app.HudApplication.getAppContext());
                }
            }
        }
    }

    public void setDelayEvent(com.navdy.hud.app.maps.MapEvents.TrafficDelayEvent event) {
        this.trafficDelayEvent = event;
    }

    public void setFasterRouteEvent(com.navdy.hud.app.maps.MapEvents.TrafficRerouteEvent event) {
        this.fasterRouteEvent = event;
    }

    private void buildData() {
        if (this.fasterRouteEvent != null) {
            if (this.fasterRouteEvent.etaDifference != 0) {
                long val = java.util.concurrent.TimeUnit.SECONDS.toMinutes(this.fasterRouteEvent.etaDifference);
                if (val > 99) {
                    long val2 = java.util.concurrent.TimeUnit.SECONDS.toHours(this.fasterRouteEvent.etaDifference);
                    this.timeStr = java.lang.String.valueOf(val2);
                    this.timeUnitStr = hr;
                    if (val2 > 1) {
                        this.timeStrExpanded = hours;
                    } else {
                        this.timeStrExpanded = hour;
                    }
                } else {
                    this.timeStr = java.lang.String.valueOf(val);
                    this.timeUnitStr = min;
                    if (val > 1) {
                        this.timeStrExpanded = minutes;
                    } else {
                        this.timeStrExpanded = minute;
                    }
                }
            }
            java.util.Date etaDate = null;
            if (this.fasterRouteEvent.newEta != 0) {
                etaDate = new java.util.Date(this.fasterRouteEvent.newEta);
            } else if (this.fasterRouteEvent.currentEta != 0) {
                etaDate = new java.util.Date(this.fasterRouteEvent.currentEta - (this.fasterRouteEvent.etaDifference * 1000));
            }
            if (etaDate != null) {
                ampmMarker.setLength(0);
                this.etaStr = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getTimeHelper().formatTime12Hour(etaDate, ampmMarker, false);
                this.etaUnitStr = ampmMarker.toString();
            }
            long diff = this.fasterRouteEvent.distanceDifference;
            if (diff > 0) {
                this.distanceDiffStr = resources.getString(com.navdy.hud.app.R.string.longer);
            } else {
                this.distanceDiffStr = resources.getString(com.navdy.hud.app.R.string.shorter);
                diff = -diff;
            }
            com.navdy.hud.app.maps.MapEvents.ManeuverDisplay display = new com.navdy.hud.app.maps.MapEvents.ManeuverDisplay();
            com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.setNavigationDistance(diff, display, true, true);
            this.distanceStr = display.distanceToPendingRoadText;
            if (this.fasterRouteEvent.etaDifference != 0) {
                this.ttsMessage = resources.getString(com.navdy.hud.app.R.string.traffic_faster_route_tts, new java.lang.Object[]{this.timeStr, this.timeStrExpanded});
                return;
            }
            this.ttsMessage = resources.getString(com.navdy.hud.app.R.string.traffic_faster_route_tts_eta_only);
        }
    }

    /* access modifiers changed from: private */
    public void switchToExpandedMode() {
        this.choiceLayout.setChoices(com.navdy.hud.app.framework.glance.GlanceConstants.backChoice2, 0, this.choiceListener, 0.5f);
        if (!this.controller.isExpandedWithStack()) {
            this.controller.expandNotification(true);
        }
    }

    private void sendtts() {
        if (this.controller != null && this.controller.isTtsOn() && !android.text.TextUtils.isEmpty(this.ttsMessage)) {
            com.navdy.hud.app.framework.voice.TTSUtils.sendSpeechRequest(this.ttsMessage, com.navdy.service.library.events.audio.Category.SPEECH_REROUTE, this.id);
        }
    }

    private void cancelTts() {
        if (this.controller != null && this.controller.isTtsOn()) {
            this.logger.v("tts-cancelled [" + this.id + "]");
            this.bus.post(new com.navdy.hud.app.event.RemoteEvent(this.cancelSpeechRequest));
        }
    }

    private void cleanupView(com.navdy.hud.app.framework.glance.GlanceViewCache.ViewType viewType, android.view.ViewGroup viewGroup) {
        android.view.ViewGroup parent = (android.view.ViewGroup) viewGroup.getParent();
        if (parent != null) {
            parent.removeView(viewGroup);
        }
        switch (viewType) {
            case SMALL_SIGN:
                this.mainTitle.setAlpha(1.0f);
                this.mainTitle.setVisibility(0);
                this.subTitle.setAlpha(1.0f);
                this.subTitle.setVisibility(0);
                this.choiceLayout.setAlpha(1.0f);
                this.choiceLayout.setVisibility(0);
                this.choiceLayout.setTag(null);
                this.sideImage.setImageResource(0);
                viewGroup.setAlpha(1.0f);
                break;
            case BIG_MULTI_TEXT:
                viewGroup.setAlpha(1.0f);
                break;
        }
        com.navdy.hud.app.framework.glance.GlanceViewCache.putView(viewType, viewGroup);
    }
}
