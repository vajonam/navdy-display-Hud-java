package com.navdy.hud.app.maps.notification;

public abstract class BaseTrafficNotification implements com.navdy.hud.app.framework.notifications.INotification {
    protected static final java.lang.String EMPTY = "";
    public static final float IMAGE_SCALE = 0.5f;
    protected com.navdy.hud.app.ui.component.ChoiceLayout2 choiceLayout;
    protected com.navdy.hud.app.framework.notifications.INotificationController controller;
    protected com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(getClass());

    public void onStart(com.navdy.hud.app.framework.notifications.INotificationController controller2) {
        this.controller = controller2;
    }

    public void onStop() {
        this.controller = null;
        if (this.choiceLayout != null) {
            this.choiceLayout.clear();
        }
    }

    public int getTimeout() {
        return 0;
    }

    public boolean onKey(com.navdy.hud.app.manager.InputManager.CustomKeyEvent event) {
        if (this.choiceLayout == null) {
            return false;
        }
        switch (event) {
            case LEFT:
                this.choiceLayout.moveSelectionLeft();
                return true;
            case RIGHT:
                this.choiceLayout.moveSelectionRight();
                return true;
            case SELECT:
                this.choiceLayout.executeSelectedItem();
                return true;
            default:
                return false;
        }
    }

    /* access modifiers changed from: protected */
    public void dismissNotification() {
        com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().removeNotification(getId());
    }

    public void onClick() {
    }

    public void onTrackHand(float normalized) {
    }

    public boolean onGesture(com.navdy.service.library.events.input.GestureEvent event) {
        return false;
    }

    public com.navdy.hud.app.manager.InputManager.IInputHandler nextHandler() {
        return null;
    }

    public boolean supportScroll() {
        return false;
    }
}
