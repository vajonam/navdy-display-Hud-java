package com.navdy.hud.app.maps.notification;

public class RouteCalculationNotification implements com.navdy.hud.app.framework.notifications.INotification {
    public static final com.navdy.hud.app.event.RemoteEvent CANCEL_CALC_TTS = new com.navdy.hud.app.event.RemoteEvent(new com.navdy.service.library.events.audio.CancelSpeechRequest(ROUTE_CALC_TTS_ID));
    public static final com.navdy.hud.app.event.RemoteEvent CANCEL_TBT_TTS = new com.navdy.hud.app.event.RemoteEvent(new com.navdy.service.library.events.audio.CancelSpeechRequest(ROUTE_TBT_TTS_ID));
    private static final float ICON_SCALE_FACTOR = 1.38f;
    private static final int NAV_LOOK_UP_TIMEOUT = ((int) java.util.concurrent.TimeUnit.SECONDS.toMillis(10));
    public static final java.lang.String ROUTE_CALC_TTS_ID = ("route_calc_" + java.util.UUID.randomUUID().toString());
    public static final java.lang.String ROUTE_PICKER = "ROUTE_PICKER";
    public static final java.lang.String ROUTE_TBT_TTS_ID = ("route_tbt_" + java.util.UUID.randomUUID().toString());
    private static final int TAG_CANCEL = 1;
    private static final int TAG_DISMISS = 3;
    private static final int TAG_RETRY = 4;
    private static final int TAG_ROUTES = 2;
    private static final int TIMEOUT = ((int) java.util.concurrent.TimeUnit.SECONDS.toMillis(30));
    private static java.util.List<com.navdy.hud.app.ui.component.ChoiceLayout2.Choice> cancelRouteCalc = new java.util.ArrayList(1);
    public static final java.lang.String fastestRoute = resources.getString(com.navdy.hud.app.R.string.fastest_route);
    public static final int notifBkColor = resources.getColor(com.navdy.hud.app.R.color.route_sel);
    private static final android.content.res.Resources resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
    private static java.util.List<com.navdy.hud.app.ui.component.ChoiceLayout2.Choice> routeError = new java.util.ArrayList(2);
    private static final java.lang.String routeFailed = resources.getString(com.navdy.hud.app.R.string.route_failed);
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.maps.notification.RouteCalculationNotification.class);
    public static final java.lang.String shortestRoute = resources.getString(com.navdy.hud.app.R.string.shortest_route);
    private static final int showRouteBkColor = resources.getColor(com.navdy.hud.app.R.color.route_unsel);
    private static java.util.List<com.navdy.hud.app.ui.component.ChoiceLayout2.Choice> showRoutes = new java.util.ArrayList(2);
    private static final int startTripImageContainerSize = resources.getDimensionPixelSize(com.navdy.hud.app.R.dimen.start_trip_image_container_size);
    private static final int startTripImageSize = resources.getDimensionPixelSize(com.navdy.hud.app.R.dimen.start_trip_image_size);
    private static final int startTripLeftMargin = resources.getDimensionPixelSize(com.navdy.hud.app.R.dimen.start_trip_left_margin);
    private static final int startTripTextLeftMargin = resources.getDimensionPixelSize(com.navdy.hud.app.R.dimen.start_trip_text_left_margin);
    /* access modifiers changed from: private */
    public com.squareup.otto.Bus bus;
    private com.navdy.hud.app.ui.component.ChoiceLayout2 choiceLayoutView;
    private java.util.List<com.navdy.hud.app.ui.component.ChoiceLayout2.Choice> choices;
    private android.view.ViewGroup containerView;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.framework.notifications.INotificationController controller;
    private java.lang.String destinationAddress;
    private java.lang.String destinationDistance;
    private java.lang.String destinationLabel;
    /* access modifiers changed from: private */
    public boolean dismissedbyUser;
    private com.navdy.hud.app.maps.MapEvents.RouteCalculationEvent event;
    private android.os.Handler handler = new android.os.Handler(android.os.Looper.getMainLooper());
    private int icon;
    private int iconBkColor = 0;
    private com.navdy.hud.app.ui.component.image.IconColorImageView iconColorImageView;
    private com.navdy.hud.app.ui.component.image.InitialsImageView iconInitialsImageView;
    private int iconSide;
    private android.widget.ImageView iconSpinnerView;
    private java.lang.String initials;
    private com.navdy.hud.app.ui.component.ChoiceLayout2.IListener listener = new com.navdy.hud.app.maps.notification.RouteCalculationNotification.Anon3();
    /* access modifiers changed from: private */
    public android.animation.ObjectAnimator loadingAnimator;
    private com.navdy.service.library.events.destination.Destination lookupDestination;
    /* access modifiers changed from: private */
    public com.navdy.service.library.events.places.DestinationSelectedRequest lookupRequest;
    private int notifColor = 0;
    private boolean registered;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.maps.notification.RouteCalculationEventHandler routeCalculationEventHandler;
    /* access modifiers changed from: private */
    public java.lang.String routeId;
    private android.widget.TextView routeTtaView;
    private long startTime;
    private java.lang.String subTitle;
    private android.widget.TextView subTitleView;
    private java.lang.Runnable timeoutRunnable = new com.navdy.hud.app.maps.notification.RouteCalculationNotification.Anon2();
    private java.lang.String title;
    private android.widget.TextView titleView;
    private android.text.SpannableStringBuilder tripDuration;
    private java.lang.String tts;
    private boolean ttsPlayed;
    private com.navdy.hud.app.ui.framework.UIStateManager uiStateManager = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getUiStateManager();
    private java.lang.Runnable waitForNavLookupRunnable = new com.navdy.hud.app.maps.notification.RouteCalculationNotification.Anon1();

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            if (com.navdy.hud.app.maps.notification.RouteCalculationNotification.this.controller != null && com.navdy.hud.app.maps.notification.RouteCalculationNotification.this.lookupRequest != null) {
                com.navdy.hud.app.maps.notification.RouteCalculationNotification.sLogger.v("nav lookup expired");
                com.navdy.hud.app.maps.notification.RouteCalculationNotification.this.lookupRequest = null;
                com.navdy.hud.app.maps.notification.RouteCalculationNotification.this.launchOriginalDestination();
            }
        }
    }

    class Anon2 implements java.lang.Runnable {
        Anon2() {
        }

        public void run() {
            com.navdy.hud.app.maps.notification.RouteCalculationNotification.sLogger.v("timeoutRunnable");
            com.navdy.hud.app.maps.notification.RouteCalculationNotification.this.dismissNotification();
        }
    }

    class Anon3 implements com.navdy.hud.app.ui.component.ChoiceLayout2.IListener {
        Anon3() {
        }

        public void executeItem(com.navdy.hud.app.ui.component.ChoiceLayout2.Selection selection) {
            if (com.navdy.hud.app.maps.notification.RouteCalculationNotification.this.controller != null) {
                switch (selection.id) {
                    case 1:
                        com.navdy.hud.app.maps.notification.RouteCalculationNotification.sLogger.v("cancel");
                        com.navdy.hud.app.maps.notification.RouteCalculationNotification.this.dismissedbyUser = true;
                        com.navdy.hud.app.analytics.AnalyticsSupport.recordRouteSelectionCancelled();
                        if (com.navdy.hud.app.maps.notification.RouteCalculationNotification.this.lookupRequest == null) {
                            com.navdy.hud.app.maps.notification.RouteCalculationNotification.sLogger.v("cancel route");
                            if (!com.navdy.hud.app.maps.here.HereRouteManager.cancelActiveRouteRequest()) {
                                com.navdy.hud.app.maps.notification.RouteCalculationNotification.sLogger.v("route calc was not cancelled, may be it is not running");
                                if (!android.text.TextUtils.isEmpty(com.navdy.hud.app.maps.notification.RouteCalculationNotification.this.routeId)) {
                                    com.navdy.hud.app.maps.notification.RouteCalculationNotification.this.bus.post(new com.navdy.hud.app.event.RemoteEvent(new com.navdy.service.library.events.navigation.NavigationRouteResponse.Builder().requestId(com.navdy.hud.app.maps.notification.RouteCalculationNotification.this.routeId).status(com.navdy.service.library.events.RequestStatus.REQUEST_CANCELLED).destination(new com.navdy.service.library.events.location.Coordinate.Builder().latitude(java.lang.Double.valueOf(0.0d)).longitude(java.lang.Double.valueOf(0.0d)).build()).build()));
                                    com.navdy.hud.app.maps.notification.RouteCalculationNotification.sLogger.v("route calc event sent:" + com.navdy.hud.app.maps.notification.RouteCalculationNotification.this.routeId);
                                } else {
                                    com.navdy.hud.app.maps.notification.RouteCalculationNotification.sLogger.v("route calc was not cancelled,no route id");
                                }
                            } else {
                                com.navdy.hud.app.maps.notification.RouteCalculationNotification.sLogger.v("route calc was cancelled");
                            }
                        }
                        com.navdy.hud.app.maps.notification.RouteCalculationNotification.this.dismissNotification();
                        return;
                    case 2:
                        com.navdy.hud.app.maps.notification.RouteCalculationNotification.sLogger.v("show route picker: choice");
                        com.navdy.hud.app.maps.notification.RouteCalculationNotification.this.dismissedbyUser = true;
                        com.navdy.hud.app.maps.notification.RouteCalculationNotification.this.showRoutePicker();
                        return;
                    case 3:
                        com.navdy.hud.app.maps.notification.RouteCalculationNotification.sLogger.v("dismiss route");
                        com.navdy.hud.app.maps.notification.RouteCalculationNotification.this.dismissedbyUser = true;
                        com.navdy.hud.app.maps.notification.RouteCalculationNotification.this.dismissNotification();
                        return;
                    case 4:
                        com.navdy.hud.app.maps.notification.RouteCalculationNotification.sLogger.v("retry");
                        com.navdy.hud.app.maps.MapEvents.RouteCalculationEvent event = com.navdy.hud.app.maps.notification.RouteCalculationNotification.this.routeCalculationEventHandler.getCurrentRouteCalcEvent();
                        if (event == null || event.request == null) {
                            com.navdy.hud.app.maps.notification.RouteCalculationNotification.sLogger.v("retry: request not found");
                            com.navdy.hud.app.maps.notification.RouteCalculationNotification.this.dismissNotification();
                            return;
                        }
                        com.navdy.hud.app.maps.notification.RouteCalculationNotification.this.bus.post(event.request);
                        com.navdy.hud.app.maps.notification.RouteCalculationNotification.this.bus.post(new com.navdy.hud.app.event.RemoteEvent(event.request));
                        return;
                    default:
                        return;
                }
            }
        }

        public void itemSelected(com.navdy.hud.app.ui.component.ChoiceLayout2.Selection selection) {
        }
    }

    class Anon4 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
        Anon4() {
        }

        public void onAnimationEnd(android.animation.Animator animation) {
            if (com.navdy.hud.app.maps.notification.RouteCalculationNotification.this.loadingAnimator != null) {
                com.navdy.hud.app.maps.notification.RouteCalculationNotification.this.loadingAnimator.setStartDelay(33);
                com.navdy.hud.app.maps.notification.RouteCalculationNotification.this.loadingAnimator.start();
                return;
            }
            com.navdy.hud.app.maps.notification.RouteCalculationNotification.sLogger.v("abandon loading animation");
        }
    }

    static /* synthetic */ class Anon5 {
        static final /* synthetic */ int[] $SwitchMap$com$here$android$mpa$routing$RouteOptions$Type = new int[com.here.android.mpa.routing.RouteOptions.Type.values().length];

        static {
            $SwitchMap$com$navdy$service$library$events$connection$ConnectionStateChange$ConnectionState = new int[com.navdy.service.library.events.connection.ConnectionStateChange.ConnectionState.values().length];
            try {
                $SwitchMap$com$navdy$service$library$events$connection$ConnectionStateChange$ConnectionState[com.navdy.service.library.events.connection.ConnectionStateChange.ConnectionState.CONNECTION_DISCONNECTED.ordinal()] = 1;
            } catch (java.lang.NoSuchFieldError e) {
            }
            try {
                $SwitchMap$com$here$android$mpa$routing$RouteOptions$Type[com.here.android.mpa.routing.RouteOptions.Type.FASTEST.ordinal()] = 1;
            } catch (java.lang.NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$com$here$android$mpa$routing$RouteOptions$Type[com.here.android.mpa.routing.RouteOptions.Type.SHORTEST.ordinal()] = 2;
            } catch (java.lang.NoSuchFieldError e3) {
            }
            $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent = new int[com.navdy.hud.app.manager.InputManager.CustomKeyEvent.values().length];
            try {
                $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[com.navdy.hud.app.manager.InputManager.CustomKeyEvent.LEFT.ordinal()] = 1;
            } catch (java.lang.NoSuchFieldError e4) {
            }
            try {
                $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[com.navdy.hud.app.manager.InputManager.CustomKeyEvent.RIGHT.ordinal()] = 2;
            } catch (java.lang.NoSuchFieldError e5) {
            }
            try {
                $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[com.navdy.hud.app.manager.InputManager.CustomKeyEvent.SELECT.ordinal()] = 3;
            } catch (java.lang.NoSuchFieldError e6) {
            }
            $SwitchMap$com$navdy$service$library$events$input$Gesture = new int[com.navdy.service.library.events.input.Gesture.values().length];
            try {
                $SwitchMap$com$navdy$service$library$events$input$Gesture[com.navdy.service.library.events.input.Gesture.GESTURE_SWIPE_LEFT.ordinal()] = 1;
            } catch (java.lang.NoSuchFieldError e7) {
            }
        }
    }

    static {
        int dismissColor = resources.getColor(com.navdy.hud.app.R.color.glance_dismiss);
        int retryColor = resources.getColor(com.navdy.hud.app.R.color.glance_ok_blue);
        int moreColor = resources.getColor(com.navdy.hud.app.R.color.glance_ok_blue);
        int goColor = resources.getColor(com.navdy.hud.app.R.color.glance_ok_go);
        java.lang.String moreRoutes = resources.getString(com.navdy.hud.app.R.string.more_routes);
        cancelRouteCalc.add(new com.navdy.hud.app.ui.component.ChoiceLayout2.Choice(1, com.navdy.hud.app.R.drawable.icon_glances_dismiss, dismissColor, com.navdy.hud.app.R.drawable.icon_glances_dismiss, -16777216, resources.getString(com.navdy.hud.app.R.string.cancel), dismissColor));
        showRoutes.add(new com.navdy.hud.app.ui.component.ChoiceLayout2.Choice(2, com.navdy.hud.app.R.drawable.icon_glances_read, moreColor, com.navdy.hud.app.R.drawable.icon_glances_read, -16777216, moreRoutes, moreColor));
        showRoutes.add(new com.navdy.hud.app.ui.component.ChoiceLayout2.Choice(3, com.navdy.hud.app.R.drawable.icon_glances_ok_strong, goColor, com.navdy.hud.app.R.drawable.icon_glances_ok_strong, -16777216, resources.getString(com.navdy.hud.app.R.string.route_go), goColor));
        routeError.add(new com.navdy.hud.app.ui.component.ChoiceLayout2.Choice(4, com.navdy.hud.app.R.drawable.icon_glances_retry, retryColor, com.navdy.hud.app.R.drawable.icon_glances_retry, -16777216, resources.getString(com.navdy.hud.app.R.string.retry), retryColor));
        routeError.add(new com.navdy.hud.app.ui.component.ChoiceLayout2.Choice(3, com.navdy.hud.app.R.drawable.icon_glances_dismiss, dismissColor, com.navdy.hud.app.R.drawable.icon_glances_dismiss, -16777216, resources.getString(com.navdy.hud.app.R.string.dismiss), dismissColor));
    }

    public RouteCalculationNotification(com.navdy.hud.app.maps.notification.RouteCalculationEventHandler routeCalculationEventHandler2, com.squareup.otto.Bus bus2) {
        this.routeCalculationEventHandler = routeCalculationEventHandler2;
        this.bus = bus2;
    }

    public com.navdy.hud.app.framework.notifications.NotificationType getType() {
        return com.navdy.hud.app.framework.notifications.NotificationType.ROUTE_CALC;
    }

    public java.lang.String getId() {
        return com.navdy.hud.app.framework.notifications.NotificationId.ROUTE_CALC_NOTIFICATION_ID;
    }

    public android.view.View getView(android.content.Context context) {
        if (this.containerView == null) {
            this.containerView = (android.view.ViewGroup) android.view.LayoutInflater.from(context).inflate(com.navdy.hud.app.R.layout.notification_route_calc, null);
            this.titleView = (android.widget.TextView) this.containerView.findViewById(com.navdy.hud.app.R.id.title);
            this.subTitleView = (android.widget.TextView) this.containerView.findViewById(com.navdy.hud.app.R.id.subTitle);
            this.iconColorImageView = (com.navdy.hud.app.ui.component.image.IconColorImageView) this.containerView.findViewById(com.navdy.hud.app.R.id.iconColorView);
            this.iconInitialsImageView = (com.navdy.hud.app.ui.component.image.InitialsImageView) this.containerView.findViewById(com.navdy.hud.app.R.id.iconInitialsView);
            this.iconSpinnerView = (android.widget.ImageView) this.containerView.findViewById(com.navdy.hud.app.R.id.iconSpinner);
            this.choiceLayoutView = (com.navdy.hud.app.ui.component.ChoiceLayout2) this.containerView.findViewById(com.navdy.hud.app.R.id.choiceLayout);
            this.routeTtaView = (android.widget.TextView) this.containerView.findViewById(com.navdy.hud.app.R.id.routeTtaView);
        }
        return this.containerView;
    }

    public android.view.View getExpandedView(android.content.Context context, java.lang.Object data) {
        return null;
    }

    public int getExpandedViewIndicatorColor() {
        return 0;
    }

    public void onStart(com.navdy.hud.app.framework.notifications.INotificationController controller2) {
        sLogger.v("onStart");
        this.controller = controller2;
        this.handler.removeCallbacks(this.timeoutRunnable);
        if (!this.registered) {
            this.bus.register(this);
            this.registered = true;
        }
        setUI();
    }

    public void onUpdate() {
    }

    public void onStop() {
        sLogger.v("onStop");
        hideStartTrip();
        stopLoadingAnimation();
        this.handler.removeCallbacks(this.waitForNavLookupRunnable);
        this.controller = null;
        this.ttsPlayed = false;
        if (this.choices == cancelRouteCalc) {
            sLogger.v("cancel tts");
            this.bus.post(CANCEL_CALC_TTS);
        }
        if ((this.choices == showRoutes || this.choices == routeError) && isAlive()) {
            this.handler.removeCallbacks(this.timeoutRunnable);
            this.handler.postDelayed(this.timeoutRunnable, (long) TIMEOUT);
            sLogger.v("expire notif in " + TIMEOUT);
        }
        if (this.registered) {
            this.registered = false;
            this.bus.unregister(this);
        }
    }

    public int getTimeout() {
        if (this.choices == cancelRouteCalc) {
            return 0;
        }
        if (this.choices == routeError) {
            return TIMEOUT;
        }
        if (this.choices == showRoutes) {
            return TIMEOUT;
        }
        return 0;
    }

    public boolean isAlive() {
        boolean timedOut;
        boolean alive;
        boolean alive2 = false;
        java.lang.String reason = null;
        if (this.choices == cancelRouteCalc) {
            alive2 = this.dismissedbyUser ? false : this.routeCalculationEventHandler.getCurrentRouteCalcEvent() != null;
            reason = "cancelRouteCalc";
        } else if (this.choices == routeError) {
            alive2 = false;
            reason = "routeError";
        } else if (this.choices == showRoutes) {
            if (this.dismissedbyUser) {
                alive = false;
            } else {
                long diff = android.os.SystemClock.elapsedRealtime() - this.startTime;
                if (diff >= ((long) TIMEOUT)) {
                    timedOut = true;
                } else {
                    timedOut = false;
                }
                if (this.routeCalculationEventHandler.getCurrentRouteCalcEvent() == null || timedOut) {
                    alive = false;
                } else {
                    alive = true;
                }
                sLogger.v("timedOut=" + diff + " = " + timedOut);
            }
            reason = "showRoutes";
        }
        sLogger.v("alive=" + alive2 + " type=" + reason);
        return alive2;
    }

    public boolean isPurgeable() {
        return !isAlive();
    }

    public boolean canAddToStackIfCurrentExists() {
        return true;
    }

    public int getColor() {
        return notifBkColor;
    }

    public void onNotificationEvent(com.navdy.hud.app.ui.framework.UIStateManager.Mode mode) {
    }

    public void onExpandedNotificationEvent(com.navdy.hud.app.ui.framework.UIStateManager.Mode mode) {
    }

    public void onExpandedNotificationSwitched() {
    }

    public android.animation.AnimatorSet getViewSwitchAnimation(boolean viewIn) {
        return null;
    }

    public boolean expandNotification() {
        return false;
    }

    public boolean supportScroll() {
        return false;
    }

    public void onClick() {
    }

    public void onTrackHand(float normalized) {
    }

    public boolean onGesture(com.navdy.service.library.events.input.GestureEvent event2) {
        if (this.controller == null || this.choices != showRoutes) {
            return false;
        }
        switch (event2.gesture) {
            case GESTURE_SWIPE_LEFT:
                sLogger.v("show route picker:gesture");
                this.dismissedbyUser = true;
                showRoutePicker();
                return true;
            default:
                return false;
        }
    }

    public boolean onKey(com.navdy.hud.app.manager.InputManager.CustomKeyEvent event2) {
        if (this.controller == null) {
            return false;
        }
        switch (event2) {
            case LEFT:
                this.controller.resetTimeout();
                this.choiceLayoutView.moveSelectionLeft();
                return true;
            case RIGHT:
                this.controller.resetTimeout();
                this.choiceLayoutView.moveSelectionRight();
                return true;
            case SELECT:
                this.choiceLayoutView.executeSelectedItem();
                return true;
            default:
                return false;
        }
    }

    public com.navdy.hud.app.manager.InputManager.IInputHandler nextHandler() {
        return null;
    }

    /* access modifiers changed from: private */
    public void dismissNotification() {
        this.routeCalculationEventHandler.clearCurrentRouteCalcEvent();
        com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().removeNotification(com.navdy.hud.app.framework.notifications.NotificationId.ROUTE_CALC_NOTIFICATION_ID);
    }

    private void setUI() {
        this.startTime = android.os.SystemClock.elapsedRealtime();
        this.titleView.setText(this.title);
        this.subTitleView.setText(this.subTitle);
        this.iconSpinnerView.setImageResource(this.iconSide);
        int selection = 0;
        if (this.choices == cancelRouteCalc) {
            this.routeTtaView.setVisibility(8);
            setIcon();
            startLoadingAnimation();
            if (this.tts != null && !this.ttsPlayed) {
                playTts(this.tts);
                this.tts = null;
                this.ttsPlayed = true;
            }
            if (this.lookupDestination != null) {
                this.lookupRequest = new com.navdy.service.library.events.places.DestinationSelectedRequest.Builder().request_id(java.util.UUID.randomUUID().toString()).destination(this.lookupDestination).build();
                this.handler.removeCallbacks(this.waitForNavLookupRunnable);
                this.handler.postDelayed(this.waitForNavLookupRunnable, (long) NAV_LOOK_UP_TIMEOUT);
                this.bus.post(new com.navdy.hud.app.event.RemoteEvent(this.lookupRequest));
                sLogger.v("launched nav lookup request:" + this.lookupRequest.request_id + " dest:" + this.lookupDestination + " placeid:" + this.lookupDestination.place_id);
            }
        } else if (this.choices == showRoutes) {
            selection = 1;
            setIcon();
            stopLoadingAnimation();
            this.routeTtaView.setText(this.tripDuration);
            this.routeTtaView.setVisibility(0);
        } else if (this.choices == routeError) {
            this.routeTtaView.setVisibility(8);
            setIcon();
            stopLoadingAnimation();
        }
        this.choiceLayoutView.setChoices(this.choices, selection, this.listener);
        this.controller.startTimeout(getTimeout());
    }

    private void setIcon() {
        if (this.iconBkColor != 0) {
            this.iconColorImageView.setIcon(this.icon, this.iconBkColor, null, ICON_SCALE_FACTOR);
            this.iconColorImageView.setVisibility(0);
            this.iconInitialsImageView.setVisibility(8);
            return;
        }
        this.iconInitialsImageView.setImage(this.icon, this.initials, com.navdy.hud.app.ui.component.image.InitialsImageView.Style.LARGE);
        this.iconInitialsImageView.setVisibility(0);
        this.iconColorImageView.setVisibility(8);
    }

    private void startLoadingAnimation() {
        if (this.loadingAnimator == null) {
            this.loadingAnimator = android.animation.ObjectAnimator.ofFloat(this.iconSpinnerView, android.view.View.ROTATION, new float[]{360.0f});
            this.loadingAnimator.setDuration(500);
            this.loadingAnimator.setInterpolator(new android.view.animation.AccelerateDecelerateInterpolator());
            this.loadingAnimator.addListener(new com.navdy.hud.app.maps.notification.RouteCalculationNotification.Anon4());
        }
        if (!this.loadingAnimator.isRunning()) {
            sLogger.v("started loading animation");
            this.loadingAnimator.start();
        }
    }

    private void stopLoadingAnimation() {
        if (this.loadingAnimator != null) {
            sLogger.v("cancelled loading animation");
            this.loadingAnimator.removeAllListeners();
            this.loadingAnimator.cancel();
            this.iconSpinnerView.setRotation(0.0f);
            this.loadingAnimator = null;
        }
    }

    public void showRouteSearch(java.lang.String title2, java.lang.String subTitle2, int icon2, int iconBkColor2, int notifColor2, java.lang.String initials2, java.lang.String tts2, com.navdy.service.library.events.destination.Destination lookupDestination2, java.lang.String routeId2, java.lang.String destinationLabel2, java.lang.String destinationAddress2, java.lang.String destinationDistance2) {
        sLogger.v("showRouteSearch: navlookup:" + (lookupDestination2 != null));
        boolean navLookupTransition = false;
        if (this.lookupDestination != null && android.text.TextUtils.equals(this.title, title2)) {
            navLookupTransition = true;
            sLogger.v("showRouteSearch: nav lookup transition");
        }
        this.title = title2;
        this.subTitle = subTitle2;
        this.routeId = routeId2;
        if (!navLookupTransition) {
            this.icon = icon2;
            this.iconBkColor = iconBkColor2;
            this.notifColor = notifColor2;
            this.initials = initials2;
        }
        this.choices = cancelRouteCalc;
        this.iconSide = com.navdy.hud.app.R.drawable.loader_circle;
        this.event = null;
        this.tts = tts2;
        this.lookupDestination = lookupDestination2;
        this.destinationLabel = destinationLabel2;
        this.destinationAddress = destinationAddress2;
        this.destinationDistance = destinationDistance2;
        if (this.controller != null) {
            setUI();
        }
    }

    public void showError() {
        this.title = routeFailed;
        this.choices = routeError;
        this.iconSide = com.navdy.hud.app.R.drawable.icon_badge_alert;
        this.event = null;
        this.tts = null;
        if (this.controller != null) {
            setUI();
        }
    }

    public void showRoutes(com.navdy.hud.app.maps.MapEvents.RouteCalculationEvent event2) {
        switch (com.navdy.hud.app.maps.notification.RouteCalculationNotification.Anon5.$SwitchMap$com$here$android$mpa$routing$RouteOptions$Type[event2.routeOptions.getRouteType().ordinal()]) {
            case 1:
                this.title = fastestRoute;
                break;
            case 2:
                this.title = shortestRoute;
                break;
        }
        this.subTitle = resources.getString(com.navdy.hud.app.R.string.via_desc, new java.lang.Object[]{((com.navdy.service.library.events.navigation.NavigationRouteResult) event2.response.results.get(0)).via});
        this.choices = showRoutes;
        this.icon = 0;
        this.iconBkColor = showRouteBkColor;
        this.iconSide = com.navdy.hud.app.R.drawable.icon_route;
        this.event = event2;
        this.tripDuration = null;
        this.tts = null;
        com.navdy.service.library.events.navigation.NavigationRouteResult result = (com.navdy.service.library.events.navigation.NavigationRouteResult) event2.response.results.get(0);
        com.navdy.hud.app.maps.here.HereRouteCache.RouteInfo routeInfo = com.navdy.hud.app.maps.here.HereRouteCache.getInstance().getRoute(result.routeId);
        if (routeInfo != null) {
            this.tripDuration = com.navdy.hud.app.maps.here.HereMapUtil.getEtaString(routeInfo.route, result.routeId, false);
        } else {
            this.tripDuration = new android.text.SpannableStringBuilder();
        }
        if (this.controller != null) {
            setUI();
        }
    }

    /* access modifiers changed from: private */
    public void showRoutePicker() {
        com.navdy.service.library.events.places.PlaceType placeType;
        long duration;
        if (this.event == null || this.event.response == null || this.event.request == null) {
            sLogger.v("showRoutePicker invalid state");
            dismissNotification();
            return;
        }
        android.os.Bundle args = new android.os.Bundle();
        args.putString(com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.PICKER_LEFT_TITLE, resources.getString(com.navdy.hud.app.R.string.routes));
        args.putInt(com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.PICKER_LEFT_ICON, com.navdy.hud.app.R.drawable.icon_route);
        args.putInt(com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.PICKER_LEFT_ICON_BKCOLOR, 0);
        args.putString(com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.PICKER_TITLE, resources.getString(com.navdy.hud.app.R.string.pick_route));
        args.putBoolean(com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.PICKER_HIDE_IF_NAV_STOPS, true);
        args.putBoolean(ROUTE_PICKER, true);
        args.putBoolean(com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.PICKER_SHOW_ROUTE_MAP, true);
        com.here.android.mpa.common.GeoCoordinate geoCoordinate = com.navdy.hud.app.maps.here.HereMapsManager.getInstance().getLocationFixManager().getLastGeoCoordinate();
        args.putDouble(com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.PICKER_MAP_START_LAT, geoCoordinate.getLatitude());
        args.putDouble(com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.PICKER_MAP_START_LNG, geoCoordinate.getLongitude());
        args.putDouble(com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.PICKER_MAP_END_LAT, this.event.request.destination.latitude.doubleValue());
        args.putDouble(com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.PICKER_MAP_END_LNG, this.event.request.destination.longitude.doubleValue());
        int len = this.event.response.results.size();
        com.navdy.hud.app.ui.component.destination.DestinationParcelable[] destinationParcelables = new com.navdy.hud.app.ui.component.destination.DestinationParcelable[len];
        double displayLat = 0.0d;
        double displayLng = 0.0d;
        double navLat = 0.0d;
        double navLng = 0.0d;
        if (this.event.request.destination != null) {
            navLat = this.event.request.destination.latitude.doubleValue();
            navLng = this.event.request.destination.longitude.doubleValue();
        }
        if (this.event.request.destinationDisplay != null) {
            displayLat = this.event.request.destinationDisplay.latitude.doubleValue();
            displayLng = this.event.request.destinationDisplay.longitude.doubleValue();
        }
        java.lang.StringBuilder builder = new java.lang.StringBuilder();
        com.navdy.hud.app.maps.here.HereRouteCache hereRouteCache = com.navdy.hud.app.maps.here.HereRouteCache.getInstance();
        com.navdy.hud.app.common.TimeHelper timeHelper = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getTimeHelper();
        com.navdy.hud.app.maps.MapEvents.ManeuverDisplay maneuverDisplay = new com.navdy.hud.app.maps.MapEvents.ManeuverDisplay();
        com.navdy.service.library.events.destination.Destination requestDestination = this.event.request.requestDestination;
        for (int i = 0; i < len; i++) {
            java.lang.String time = "";
            java.lang.String etaStr = "";
            com.navdy.service.library.events.navigation.NavigationRouteResult result = (com.navdy.service.library.events.navigation.NavigationRouteResult) this.event.response.results.get(i);
            com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.setNavigationDistance((long) result.length.intValue(), maneuverDisplay, false, true);
            com.navdy.hud.app.maps.here.HereRouteCache.RouteInfo routeInfo = hereRouteCache.getRoute(result.routeId);
            if (routeInfo != null) {
                java.util.Date ttaDate = com.navdy.hud.app.maps.here.HereMapUtil.getRouteTtaDate(routeInfo.route);
                if (ttaDate != null) {
                    etaStr = timeHelper.formatTime12Hour(ttaDate, builder, false) + " " + builder.toString();
                    long duration2 = ttaDate.getTime() - java.lang.System.currentTimeMillis();
                    if (duration2 < 0) {
                        duration = 0;
                    } else {
                        duration = java.util.concurrent.TimeUnit.MILLISECONDS.toMinutes(duration2);
                    }
                    time = com.navdy.hud.app.maps.util.RouteUtils.formatEtaMinutes(resources, (int) duration);
                }
            }
            java.lang.String title2 = resources.getString(com.navdy.hud.app.R.string.route_title, new java.lang.Object[]{time, result.via});
            java.lang.String subTitle2 = maneuverDisplay.distanceToPendingRoadText + " " + etaStr;
            java.lang.String str = result.address;
            int i2 = notifBkColor;
            com.navdy.hud.app.ui.component.destination.DestinationParcelable.DestinationType destinationType = com.navdy.hud.app.ui.component.destination.DestinationParcelable.DestinationType.DESTINATION;
            if (requestDestination != null) {
                placeType = requestDestination.place_type;
            } else {
                placeType = null;
            }
            destinationParcelables[i] = new com.navdy.hud.app.ui.component.destination.DestinationParcelable(0, title2, subTitle2, false, null, false, str, navLat, navLng, displayLat, displayLng, com.navdy.hud.app.R.drawable.icon_route, com.navdy.hud.app.R.drawable.icon_route_sm, i2, 0, destinationType, placeType);
            destinationParcelables[i].setRouteId(((com.navdy.service.library.events.navigation.NavigationRouteResult) this.event.response.results.get(i)).routeId);
            if (requestDestination != null) {
                destinationParcelables[i].setIdentifier(requestDestination.identifier);
                destinationParcelables[i].setPlaceId(requestDestination.place_id);
            }
            builder.setLength(0);
        }
        args.putParcelableArray(com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.PICKER_DESTINATIONS, destinationParcelables);
        com.navdy.hud.app.maps.notification.RouteCalcPickerHandler routeCalcPickerHandler = new com.navdy.hud.app.maps.notification.RouteCalcPickerHandler(this.event);
        com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().removeNotification(com.navdy.hud.app.framework.notifications.NotificationId.ROUTE_CALC_NOTIFICATION_ID, true, com.navdy.service.library.events.ui.Screen.SCREEN_DESTINATION_PICKER, args, routeCalcPickerHandler);
        this.event = null;
    }

    private void playTts(java.lang.String tts2) {
        sLogger.v("tts[" + tts2 + "]");
        if (com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().getNavigationSessionPreference().spokenTurnByTurn) {
            this.bus.post(CANCEL_CALC_TTS);
            com.navdy.hud.app.framework.voice.TTSUtils.sendSpeechRequest(tts2, com.navdy.service.library.events.audio.Category.SPEECH_TURN_BY_TURN, ROUTE_CALC_TTS_ID);
        }
    }

    @com.squareup.otto.Subscribe
    public void onDestinationLookup(com.navdy.service.library.events.places.DestinationSelectedResponse response) {
        if (this.controller != null) {
            if (this.lookupRequest == null) {
                sLogger.v("receive destination lookup:" + response.request_id + " , not in lookup mode, ignore");
            } else if (!android.text.TextUtils.equals(response.request_id, this.lookupRequest.request_id)) {
                sLogger.v("receive destination lookup, mismatch id recv[" + response.request_id + "] lookup[" + this.lookupRequest.request_id + "]");
            } else {
                if (response.request_status == null || response.request_status != com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS) {
                    sLogger.v("nav lookup failed:" + response.request_status);
                    launchOriginalDestination();
                } else {
                    sLogger.v("nav lookup suceeded:" + response.destination);
                    com.navdy.hud.app.framework.destinations.DestinationsManager.getInstance().requestNavigation(com.navdy.hud.app.framework.destinations.DestinationsManager.getInstance().transformToInternalDestination(response.destination));
                }
                this.lookupRequest = null;
            }
        }
    }

    @com.squareup.otto.Subscribe
    public void onConnectionStateChange(com.navdy.service.library.events.connection.ConnectionStateChange event2) {
        if (this.controller != null) {
            switch (event2.state) {
                case CONNECTION_DISCONNECTED:
                    if (this.lookupRequest != null) {
                        sLogger.v("nav lookup device disconnected");
                        this.lookupRequest = null;
                        launchOriginalDestination();
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    /* access modifiers changed from: private */
    public void launchOriginalDestination() {
        if (this.lookupDestination != null) {
            com.navdy.hud.app.framework.destinations.Destination d = com.navdy.hud.app.framework.destinations.DestinationsManager.getInstance().transformToInternalDestination(this.lookupDestination);
            com.navdy.hud.app.framework.destinations.DestinationsManager.getInstance().requestNavigation(d);
            this.lookupDestination = null;
            sLogger.v("original destination launched:" + d);
            return;
        }
        sLogger.v("original destination not launched");
    }

    public void showStartTrip() {
        if (this.controller == null) {
            sLogger.v("showStartTrip:null");
            return;
        }
        com.navdy.hud.app.ui.activity.Main main = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getUiStateManager().getRootScreen();
        if (main != null) {
            android.content.Context context = this.controller.getUIContext();
            android.view.ViewGroup lyt = (android.view.ViewGroup) android.view.LayoutInflater.from(context).inflate(com.navdy.hud.app.R.layout.vlist_item, null);
            com.navdy.hud.app.ui.component.HaloView haloView = (com.navdy.hud.app.ui.component.HaloView) lyt.findViewById(com.navdy.hud.app.R.id.halo);
            haloView.setVisibility(8);
            android.view.ViewGroup.MarginLayoutParams lytParams = (android.view.ViewGroup.MarginLayoutParams) ((android.view.ViewGroup) lyt.findViewById(com.navdy.hud.app.R.id.imageContainer)).getLayoutParams();
            lytParams.width = startTripImageContainerSize;
            lytParams.height = startTripImageContainerSize;
            lytParams.leftMargin = startTripLeftMargin;
            android.view.ViewGroup iconContainer = (android.view.ViewGroup) lyt.findViewById(com.navdy.hud.app.R.id.iconContainer);
            android.view.ViewGroup.MarginLayoutParams lytParams2 = (android.view.ViewGroup.MarginLayoutParams) iconContainer.getLayoutParams();
            lytParams2.width = startTripImageSize;
            lytParams2.height = startTripImageSize;
            ((android.view.ViewGroup.MarginLayoutParams) ((android.view.ViewGroup) lyt.findViewById(com.navdy.hud.app.R.id.textContainer)).getLayoutParams()).leftMargin = startTripTextLeftMargin;
            android.widget.TextView startTripTitle = (android.widget.TextView) lyt.findViewById(com.navdy.hud.app.R.id.title);
            android.widget.TextView startTripSubTitle = (android.widget.TextView) lyt.findViewById(com.navdy.hud.app.R.id.subTitle);
            android.widget.TextView startTripSubTitle2 = (android.widget.TextView) lyt.findViewById(com.navdy.hud.app.R.id.subTitle2);
            com.navdy.hud.app.ui.component.vlist.VerticalList.Model model = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(0, 0, 0, 0, 0, this.destinationLabel, this.destinationAddress, this.destinationDistance, null);
            com.navdy.hud.app.ui.component.vlist.VerticalList.setFontSize(model, false);
            ((android.view.ViewGroup.MarginLayoutParams) startTripTitle.getLayoutParams()).topMargin = (int) model.fontInfo.titleFontTopMargin;
            ((android.view.ViewGroup.MarginLayoutParams) startTripSubTitle.getLayoutParams()).topMargin = (int) model.fontInfo.subTitleFontTopMargin;
            ((android.view.ViewGroup.MarginLayoutParams) startTripSubTitle2.getLayoutParams()).topMargin = (int) model.fontInfo.subTitle2FontTopMargin;
            startTripTitle.setTextSize(model.fontInfo.titleFontSize);
            startTripTitle.setSingleLine(true);
            startTripTitle.setText(this.destinationLabel);
            if (!android.text.TextUtils.isEmpty(this.destinationAddress)) {
                startTripSubTitle.setTextSize(model.fontInfo.subTitleFontSize);
                startTripSubTitle.setText(this.destinationAddress);
            } else {
                startTripSubTitle.setVisibility(8);
            }
            if (!android.text.TextUtils.isEmpty(this.destinationDistance)) {
                startTripSubTitle2.setTextSize(model.fontInfo.subTitle2FontSize);
                startTripSubTitle2.setText(this.destinationDistance);
                startTripSubTitle2.setVisibility(0);
            } else {
                startTripSubTitle2.setVisibility(8);
            }
            if (this.iconBkColor != 0) {
                com.navdy.hud.app.ui.component.image.IconColorImageView imageView = new com.navdy.hud.app.ui.component.image.IconColorImageView(context);
                iconContainer.addView(imageView);
                haloView.setStrokeColor(android.graphics.Color.argb(com.navdy.hud.app.ui.component.vlist.viewholder.IconBaseViewHolder.FLUCTUATOR_OPACITY_ALPHA, android.graphics.Color.red(this.iconBkColor), android.graphics.Color.green(this.iconBkColor), android.graphics.Color.blue(this.iconBkColor)));
                imageView.setIcon(this.icon, this.iconBkColor, null, 0.83f);
            } else {
                com.navdy.hud.app.ui.component.image.InitialsImageView imageView2 = new com.navdy.hud.app.ui.component.image.InitialsImageView(context);
                iconContainer.addView(imageView2);
                haloView.setStrokeColor(android.graphics.Color.argb(com.navdy.hud.app.ui.component.vlist.viewholder.IconBaseViewHolder.FLUCTUATOR_OPACITY_ALPHA, android.graphics.Color.red(this.notifColor), android.graphics.Color.green(this.notifColor), android.graphics.Color.blue(this.notifColor)));
                imageView2.setImage(this.icon, this.initials, com.navdy.hud.app.ui.component.image.InitialsImageView.Style.MEDIUM);
            }
            main.injectMainLowerView(lyt);
        }
    }

    public void hideStartTrip() {
        com.navdy.hud.app.ui.activity.Main main = this.uiStateManager.getRootScreen();
        if (main != null) {
            main.ejectMainLowerView();
        }
    }

    public boolean isStarting() {
        return this.choices == cancelRouteCalc;
    }
}
