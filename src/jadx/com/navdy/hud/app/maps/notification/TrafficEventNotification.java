package com.navdy.hud.app.maps.notification;

public class TrafficEventNotification extends com.navdy.hud.app.maps.notification.BaseTrafficNotification {
    private static final int TAG_DISMISS = 1;
    private static java.lang.String ahead;
    private static java.lang.String dismiss;
    private static java.util.List<com.navdy.hud.app.ui.component.ChoiceLayout2.Choice> dismissChoices = new java.util.ArrayList(1);
    private static java.lang.String enableInternet;
    private static java.lang.String noUpdatedTraffic;
    private static java.lang.String slowTraffic;
    private static java.lang.String stoppedTraffic;
    private static java.lang.String trafficIncident;
    private static java.lang.String unknownIncident;
    private com.squareup.otto.Bus bus;
    private com.navdy.hud.app.ui.component.ChoiceLayout2.IListener choiceListener = new com.navdy.hud.app.maps.notification.TrafficEventNotification.Anon1();
    private android.view.ViewGroup container;
    private android.widget.ImageView image;
    private int notifColor;
    private android.widget.TextView subTitle;
    private android.widget.TextView title;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.maps.MapEvents.LiveTrafficEvent trafficEvent;

    class Anon1 implements com.navdy.hud.app.ui.component.ChoiceLayout2.IListener {
        Anon1() {
        }

        public void executeItem(com.navdy.hud.app.ui.component.ChoiceLayout2.Selection selection) {
            com.navdy.hud.app.maps.notification.TrafficEventNotification.this.dismissNotification();
            com.navdy.hud.app.maps.notification.TrafficEventNotification.this.trafficEvent = null;
        }

        public void itemSelected(com.navdy.hud.app.ui.component.ChoiceLayout2.Selection selection) {
        }
    }

    public TrafficEventNotification(com.squareup.otto.Bus bus2) {
        if (stoppedTraffic == null) {
            android.content.res.Resources resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
            stoppedTraffic = resources.getString(com.navdy.hud.app.R.string.traffic_notification_text_stoppedtraffic);
            slowTraffic = resources.getString(com.navdy.hud.app.R.string.traffic_notification_text_slowtraffic);
            trafficIncident = resources.getString(com.navdy.hud.app.R.string.traffic_notification_text_incident);
            noUpdatedTraffic = resources.getString(com.navdy.hud.app.R.string.traffic_no_updated);
            enableInternet = resources.getString(com.navdy.hud.app.R.string.enable_internet);
            ahead = resources.getString(com.navdy.hud.app.R.string.traffic_notification_ahead);
            dismiss = resources.getString(com.navdy.hud.app.R.string.traffic_notification_dismiss);
            unknownIncident = resources.getString(com.navdy.hud.app.R.string.traffic_notification_text_default);
            int dismissColor = resources.getColor(com.navdy.hud.app.R.color.glance_dismiss);
            r4 = com.navdy.hud.app.R.drawable.icon_glances_dismiss;
            dismissChoices.add(new com.navdy.hud.app.ui.component.ChoiceLayout2.Choice(1, com.navdy.hud.app.R.drawable.icon_glances_dismiss, dismissColor, com.navdy.hud.app.R.drawable.icon_glances_dismiss, -16777216, dismiss, dismissColor));
            this.notifColor = resources.getColor(com.navdy.hud.app.R.color.traffic_bad);
        }
        this.bus = bus2;
    }

    public com.navdy.hud.app.framework.notifications.NotificationType getType() {
        return com.navdy.hud.app.framework.notifications.NotificationType.TRAFFIC_EVENT;
    }

    public java.lang.String getId() {
        return com.navdy.hud.app.framework.notifications.NotificationId.TRAFFIC_EVENT_NOTIFICATION_ID;
    }

    public android.view.View getView(android.content.Context context) {
        if (this.container == null) {
            this.container = (android.view.ViewGroup) android.view.LayoutInflater.from(context).inflate(com.navdy.hud.app.R.layout.notification_traffic_event, null);
            this.title = (android.widget.TextView) this.container.findViewById(com.navdy.hud.app.R.id.title);
            this.subTitle = (android.widget.TextView) this.container.findViewById(com.navdy.hud.app.R.id.subTitle);
            this.image = (android.widget.ImageView) this.container.findViewById(com.navdy.hud.app.R.id.image);
            this.choiceLayout = (com.navdy.hud.app.ui.component.ChoiceLayout2) this.container.findViewById(com.navdy.hud.app.R.id.choiceLayout);
        }
        return this.container;
    }

    public android.view.View getExpandedView(android.content.Context context, java.lang.Object data) {
        return null;
    }

    public int getExpandedViewIndicatorColor() {
        return 0;
    }

    public void onStart(com.navdy.hud.app.framework.notifications.INotificationController controller) {
        super.onStart(controller);
        updateState();
    }

    public void onUpdate() {
        updateState();
    }

    public void onStop() {
        super.onStop();
    }

    public int getTimeout() {
        return 0;
    }

    public boolean isAlive() {
        return true;
    }

    public boolean isPurgeable() {
        return false;
    }

    public boolean canAddToStackIfCurrentExists() {
        return true;
    }

    public int getColor() {
        return this.notifColor;
    }

    public void onNotificationEvent(com.navdy.hud.app.ui.framework.UIStateManager.Mode mode) {
    }

    public void onExpandedNotificationEvent(com.navdy.hud.app.ui.framework.UIStateManager.Mode mode) {
    }

    public void onExpandedNotificationSwitched() {
    }

    public android.animation.AnimatorSet getViewSwitchAnimation(boolean viewIn) {
        return null;
    }

    public boolean expandNotification() {
        return false;
    }

    private void updateState() {
        if (this.trafficEvent != null) {
            this.title.setText(getLiveTrafficEventText(this.trafficEvent));
            this.subTitle.setText(ahead);
            this.image.setImageResource(getLiveTrafficEventImage(this.trafficEvent));
            this.choiceLayout.setChoices(dismissChoices, 0, this.choiceListener, 0.5f);
        }
    }

    private int getLiveTrafficEventImage(com.navdy.hud.app.maps.MapEvents.LiveTrafficEvent trafficEvent2) {
        if (trafficEvent2.type == com.navdy.hud.app.maps.MapEvents.LiveTrafficEvent.Type.CONGESTION && trafficEvent2.severity.value > com.navdy.hud.app.maps.MapEvents.LiveTrafficEvent.Severity.VERY_HIGH.value) {
            return com.navdy.hud.app.R.drawable.icon_mm_stopped_traffic;
        }
        if (trafficEvent2.type == com.navdy.hud.app.maps.MapEvents.LiveTrafficEvent.Type.INCIDENT) {
            return com.navdy.hud.app.R.drawable.icon_mm_incident;
        }
        return com.navdy.hud.app.R.drawable.icon_mm_slow_traffic;
    }

    private java.lang.String getLiveTrafficEventText(com.navdy.hud.app.maps.MapEvents.LiveTrafficEvent trafficEvent2) {
        if (trafficEvent2.type == com.navdy.hud.app.maps.MapEvents.LiveTrafficEvent.Type.CONGESTION) {
            if (trafficEvent2.severity.value > com.navdy.hud.app.maps.MapEvents.LiveTrafficEvent.Severity.HIGH.value) {
                return stoppedTraffic;
            }
            return slowTraffic;
        } else if (trafficEvent2.type == com.navdy.hud.app.maps.MapEvents.LiveTrafficEvent.Type.INCIDENT) {
            return trafficIncident;
        } else {
            return unknownIncident;
        }
    }

    public void setTrafficEvent(com.navdy.hud.app.maps.MapEvents.LiveTrafficEvent event) {
        this.trafficEvent = event;
    }
}
