package com.navdy.hud.app.maps.notification;

public class TrafficNotificationManager {
    private static final com.navdy.hud.app.maps.notification.TrafficNotificationManager sInstance = new com.navdy.hud.app.maps.notification.TrafficNotificationManager();
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.maps.notification.TrafficNotificationManager.class);
    private com.squareup.otto.Bus bus = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getBus();
    /* access modifiers changed from: private */
    public android.os.Handler handler = new android.os.Handler(android.os.Looper.getMainLooper());
    /* access modifiers changed from: private */
    public final int junctionViewH;
    /* access modifiers changed from: private */
    public final int junctionViewInflateH;
    /* access modifiers changed from: private */
    public final int junctionViewInflateW;
    /* access modifiers changed from: private */
    public final int junctionViewW;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.maps.MapEvents.DisplayJunction lastJunctionEvent;

    class Anon1 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.hud.app.maps.MapEvents.DisplayJunction val$event;

        /* renamed from: com.navdy.hud.app.maps.notification.TrafficNotificationManager$Anon1$Anon1 reason: collision with other inner class name */
        class C0026Anon1 implements com.navdy.hud.app.maps.here.HereMapUtil.IImageLoadCallback {

            /* renamed from: com.navdy.hud.app.maps.notification.TrafficNotificationManager$Anon1$Anon1$Anon1 reason: collision with other inner class name */
            class C0027Anon1 implements java.lang.Runnable {
                final /* synthetic */ android.graphics.Bitmap val$combined;

                C0027Anon1(android.graphics.Bitmap bitmap) {
                    this.val$combined = bitmap;
                }

                public void run() {
                    try {
                        com.navdy.hud.app.ui.component.homescreen.HomeScreenView homeScreenView = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getUiStateManager().getHomescreenView();
                        android.view.View view = android.view.LayoutInflater.from(homeScreenView.getContext()).inflate(com.navdy.hud.app.R.layout.junction_view_lyt, null);
                        android.widget.ImageView imageView = (android.widget.ImageView) view.findViewById(com.navdy.hud.app.R.id.image);
                        android.widget.FrameLayout.LayoutParams params = new android.widget.FrameLayout.LayoutParams(com.navdy.hud.app.maps.notification.TrafficNotificationManager.this.junctionViewW, com.navdy.hud.app.maps.notification.TrafficNotificationManager.this.junctionViewH);
                        params.gravity = 83;
                        view.setLayoutParams(params);
                        imageView.setImageBitmap(this.val$combined);
                        homeScreenView.injectRightSection(view);
                        com.navdy.hud.app.maps.notification.TrafficNotificationManager.sLogger.i("junction view mode injected");
                    } catch (Throwable t) {
                        com.navdy.hud.app.maps.notification.TrafficNotificationManager.sLogger.e("junction view mode", t);
                    }
                }
            }

            C0026Anon1() {
            }

            public void result(com.here.android.mpa.common.Image image, android.graphics.Bitmap bitmap) {
                if (bitmap == null) {
                    try {
                        com.navdy.hud.app.maps.notification.TrafficNotificationManager.sLogger.w("junction view bitmap not loaded");
                    } catch (Throwable t) {
                        com.navdy.hud.app.maps.notification.TrafficNotificationManager.sLogger.e("junction view mode", t);
                    }
                } else {
                    com.navdy.hud.app.maps.notification.TrafficNotificationManager.sLogger.w("junction view bitmap loaded w=" + bitmap.getWidth() + " h=" + bitmap.getHeight() + " orig w=" + com.navdy.hud.app.maps.notification.TrafficNotificationManager.Anon1.this.val$event.junction.getWidth() + " h=" + com.navdy.hud.app.maps.notification.TrafficNotificationManager.Anon1.this.val$event.junction.getHeight());
                    if (com.navdy.hud.app.maps.notification.TrafficNotificationManager.this.lastJunctionEvent != null) {
                        long l1 = android.os.SystemClock.elapsedRealtime();
                        android.graphics.Bitmap hue = com.navdy.hud.app.util.ImageUtil.hueShift(bitmap, 90.0f);
                        if (hue == null) {
                            com.navdy.hud.app.maps.notification.TrafficNotificationManager.sLogger.w("hue shift not performed");
                            bitmap.recycle();
                            return;
                        }
                        long l2 = android.os.SystemClock.elapsedRealtime();
                        android.graphics.Bitmap saturation = com.navdy.hud.app.util.ImageUtil.applySaturation(bitmap, 0.0f);
                        if (saturation == null) {
                            com.navdy.hud.app.maps.notification.TrafficNotificationManager.sLogger.w("saturation not performed");
                            bitmap.recycle();
                            hue.recycle();
                            return;
                        }
                        long l3 = android.os.SystemClock.elapsedRealtime();
                        bitmap.recycle();
                        android.graphics.Bitmap combined = com.navdy.hud.app.util.ImageUtil.blend(hue, saturation);
                        saturation.recycle();
                        hue.recycle();
                        if (combined == null) {
                            com.navdy.hud.app.maps.notification.TrafficNotificationManager.sLogger.w("combine not performed");
                            return;
                        }
                        long l4 = android.os.SystemClock.elapsedRealtime();
                        com.navdy.hud.app.maps.notification.TrafficNotificationManager.sLogger.i("junction view bitmap took[" + (l4 - l1) + "] hue [" + (l2 - l1) + "] sat[" + (l3 - l2) + "] blend[" + (l4 - l3) + "]");
                        com.navdy.hud.app.maps.notification.TrafficNotificationManager.this.handler.post(new com.navdy.hud.app.maps.notification.TrafficNotificationManager.Anon1.C0026Anon1.C0027Anon1(combined));
                    }
                }
            }
        }

        Anon1(com.navdy.hud.app.maps.MapEvents.DisplayJunction displayJunction) {
            this.val$event = displayJunction;
        }

        public void run() {
            com.navdy.hud.app.maps.here.HereMapUtil.loadImage(this.val$event.junction, com.navdy.hud.app.maps.notification.TrafficNotificationManager.this.junctionViewInflateW, com.navdy.hud.app.maps.notification.TrafficNotificationManager.this.junctionViewInflateH, new com.navdy.hud.app.maps.notification.TrafficNotificationManager.Anon1.C0026Anon1());
        }
    }

    class Anon2 implements java.lang.Runnable {
        Anon2() {
        }

        public void run() {
            try {
                com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getUiStateManager().getHomescreenView().ejectRightSection();
                com.navdy.hud.app.maps.notification.TrafficNotificationManager.sLogger.i("junction view mode Ejected");
            } catch (Throwable t) {
                com.navdy.hud.app.maps.notification.TrafficNotificationManager.sLogger.e("junction view mode", t);
            }
        }
    }

    public static com.navdy.hud.app.maps.notification.TrafficNotificationManager getInstance() {
        return sInstance;
    }

    private TrafficNotificationManager() {
        android.content.res.Resources resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        this.junctionViewW = resources.getDimensionPixelSize(com.navdy.hud.app.R.dimen.traffic_junc_notif_img_w);
        this.junctionViewH = resources.getDimensionPixelSize(com.navdy.hud.app.R.dimen.traffic_junc_notif_img_h);
        this.junctionViewInflateW = resources.getDimensionPixelSize(com.navdy.hud.app.R.dimen.traffic_junc_notif_img_inflate_w);
        this.junctionViewInflateH = resources.getDimensionPixelSize(com.navdy.hud.app.R.dimen.traffic_junc_notif_img_inflate_h);
        this.bus.register(this);
    }

    @com.squareup.otto.Subscribe
    public void onLiveTrafficEvent(com.navdy.hud.app.maps.MapEvents.LiveTrafficEvent event) {
        if (!com.navdy.hud.app.framework.glance.GlanceHelper.isTrafficNotificationEnabled()) {
            sLogger.v("traffic notification disabled:" + event);
            return;
        }
        com.navdy.hud.app.framework.notifications.NotificationManager notificationManager = com.navdy.hud.app.framework.notifications.NotificationManager.getInstance();
        if (notificationManager.isNotificationPresent(com.navdy.hud.app.framework.notifications.NotificationId.TRAFFIC_REROUTE_NOTIFICATION_ID) || notificationManager.isNotificationPresent(com.navdy.hud.app.framework.notifications.NotificationId.TRAFFIC_DELAY_NOTIFICATION_ID) || notificationManager.isNotificationPresent(com.navdy.hud.app.framework.notifications.NotificationId.TRAFFIC_JAM_NOTIFICATION_ID)) {
            sLogger.v("higher priority notification active ignore:" + event.type.name());
            return;
        }
        com.navdy.hud.app.maps.notification.TrafficEventNotification notification = (com.navdy.hud.app.maps.notification.TrafficEventNotification) notificationManager.getNotification(com.navdy.hud.app.framework.notifications.NotificationId.TRAFFIC_EVENT_NOTIFICATION_ID);
        if (notification == null) {
            notification = new com.navdy.hud.app.maps.notification.TrafficEventNotification(this.bus);
        }
        notification.setTrafficEvent(event);
        notificationManager.addNotification(notification);
    }

    @com.squareup.otto.Subscribe
    public void onLiveTrafficDismiss(com.navdy.hud.app.maps.MapEvents.LiveTrafficDismissEvent event) {
        com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().removeNotification(com.navdy.hud.app.framework.notifications.NotificationId.TRAFFIC_EVENT_NOTIFICATION_ID);
    }

    @com.squareup.otto.Subscribe
    public void onTrafficDelayEvent(com.navdy.hud.app.maps.MapEvents.TrafficDelayEvent event) {
    }

    @com.squareup.otto.Subscribe
    public void onTrafficDelayDismiss(com.navdy.hud.app.maps.MapEvents.TrafficDelayDismissEvent event) {
        com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().removeNotification(com.navdy.hud.app.framework.notifications.NotificationId.TRAFFIC_DELAY_NOTIFICATION_ID);
    }

    @com.squareup.otto.Subscribe
    public void onTrafficReroutePrompt(com.navdy.hud.app.maps.MapEvents.TrafficRerouteEvent event) {
        if (!com.navdy.hud.app.framework.glance.GlanceHelper.isTrafficNotificationEnabled()) {
            sLogger.v("traffic notification disabled:" + event);
            return;
        }
        com.navdy.hud.app.framework.notifications.NotificationManager notificationManager = com.navdy.hud.app.framework.notifications.NotificationManager.getInstance();
        notificationManager.removeNotification(com.navdy.hud.app.framework.notifications.NotificationId.TRAFFIC_EVENT_NOTIFICATION_ID);
        notificationManager.removeNotification(com.navdy.hud.app.framework.notifications.NotificationId.TRAFFIC_DELAY_NOTIFICATION_ID);
        notificationManager.removeNotification(com.navdy.hud.app.framework.notifications.NotificationId.TRAFFIC_JAM_NOTIFICATION_ID);
        com.navdy.hud.app.maps.notification.RouteTimeUpdateNotification notification = (com.navdy.hud.app.maps.notification.RouteTimeUpdateNotification) notificationManager.getNotification(com.navdy.hud.app.framework.notifications.NotificationId.TRAFFIC_REROUTE_NOTIFICATION_ID);
        if (notification == null) {
            notification = new com.navdy.hud.app.maps.notification.RouteTimeUpdateNotification(com.navdy.hud.app.framework.notifications.NotificationId.TRAFFIC_REROUTE_NOTIFICATION_ID, com.navdy.hud.app.framework.notifications.NotificationType.FASTER_ROUTE, this.bus);
        }
        notification.setFasterRouteEvent(event);
        notificationManager.addNotification(notification);
    }

    @com.squareup.otto.Subscribe
    public void onTrafficReroutePromptDismiss(com.navdy.hud.app.maps.MapEvents.TrafficRerouteDismissEvent event) {
        removeFasterRouteNotifiation();
    }

    @com.squareup.otto.Subscribe
    public void onTrafficJamProgress(com.navdy.hud.app.maps.MapEvents.TrafficJamProgressEvent event) {
        if (!com.navdy.hud.app.framework.glance.GlanceHelper.isTrafficNotificationEnabled()) {
            sLogger.v("traffic notification disabled:" + event);
            return;
        }
        com.navdy.hud.app.framework.notifications.NotificationManager notificationManager = com.navdy.hud.app.framework.notifications.NotificationManager.getInstance();
        if (notificationManager.isNotificationPresent(com.navdy.hud.app.framework.notifications.NotificationId.TRAFFIC_REROUTE_NOTIFICATION_ID)) {
            sLogger.v("reroute notification active ignore jam event:" + event.remainingTime);
            return;
        }
        notificationManager.removeNotification(com.navdy.hud.app.framework.notifications.NotificationId.TRAFFIC_EVENT_NOTIFICATION_ID);
        notificationManager.removeNotification(com.navdy.hud.app.framework.notifications.NotificationId.TRAFFIC_DELAY_NOTIFICATION_ID);
        com.navdy.hud.app.maps.notification.TrafficJamNotification notification = (com.navdy.hud.app.maps.notification.TrafficJamNotification) notificationManager.getNotification(com.navdy.hud.app.framework.notifications.NotificationId.TRAFFIC_JAM_NOTIFICATION_ID);
        if (notification == null) {
            notification = new com.navdy.hud.app.maps.notification.TrafficJamNotification(this.bus, event);
        }
        notification.setTrafficEvent(event);
        notificationManager.addNotification(notification);
    }

    @com.squareup.otto.Subscribe
    public void onTrafficJamProgressDismiss(com.navdy.hud.app.maps.MapEvents.TrafficJamDismissEvent event) {
        com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().removeNotification(com.navdy.hud.app.framework.notifications.NotificationId.TRAFFIC_JAM_NOTIFICATION_ID);
    }

    public void removeFasterRouteNotifiation() {
        com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().removeNotification(com.navdy.hud.app.framework.notifications.NotificationId.TRAFFIC_REROUTE_NOTIFICATION_ID);
    }

    @com.squareup.otto.Subscribe
    public void onDisplayJunction(com.navdy.hud.app.maps.MapEvents.DisplayJunction event) {
        try {
            sLogger.v("onDisplayJunction");
            if (event.junction == null) {
                sLogger.v("no junction image");
                return;
            }
            removeJunctionView();
            this.lastJunctionEvent = event;
            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.maps.notification.TrafficNotificationManager.Anon1(event), 10);
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }

    @com.squareup.otto.Subscribe
    public void onHideJunctionSignPost(com.navdy.hud.app.maps.MapEvents.HideSignPostJunction event) {
        sLogger.v("onHideJunctionSignPost");
        this.lastJunctionEvent = null;
        removeJunctionView();
    }

    private void removeJunctionView() {
        this.handler.post(new com.navdy.hud.app.maps.notification.TrafficNotificationManager.Anon2());
    }
}
