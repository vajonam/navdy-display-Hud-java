package com.navdy.hud.app.maps.notification;

public class TrafficJamNotification extends com.navdy.hud.app.maps.notification.BaseTrafficNotification {
    public static final float REGULAR_SIZE = 64.0f;
    public static final float SMALL_SIZE = 46.0f;
    private static final int TAG_DISMISS = 1;
    private static java.lang.String delay;
    private static java.lang.String dismiss;
    private static java.util.List<com.navdy.hud.app.ui.component.ChoiceLayout2.Choice> dismissChoices = new java.util.ArrayList(1);
    private static java.lang.String hr;
    private static java.lang.String min;
    private static java.lang.String trafficJam;
    private com.squareup.otto.Bus bus;
    private com.navdy.hud.app.ui.component.ChoiceLayout2.IListener choiceListener = new com.navdy.hud.app.maps.notification.TrafficJamNotification.Anon1();
    private android.view.ViewGroup container;
    private com.navdy.hud.app.view.Gauge gauge;
    private int initialRemainingTime;
    private int notifColor;
    private android.widget.TextView title;
    private android.widget.TextView title1;
    private android.widget.TextView title2;
    private android.widget.TextView title3;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.maps.MapEvents.TrafficJamProgressEvent trafficJamEvent;

    class Anon1 implements com.navdy.hud.app.ui.component.ChoiceLayout2.IListener {
        Anon1() {
        }

        public void executeItem(com.navdy.hud.app.ui.component.ChoiceLayout2.Selection selection) {
            com.navdy.hud.app.maps.notification.TrafficJamNotification.this.dismissNotification();
            com.navdy.hud.app.maps.notification.TrafficJamNotification.this.trafficJamEvent = null;
        }

        public void itemSelected(com.navdy.hud.app.ui.component.ChoiceLayout2.Selection selection) {
        }
    }

    public TrafficJamNotification(com.squareup.otto.Bus bus2, com.navdy.hud.app.maps.MapEvents.TrafficJamProgressEvent initialEvent) {
        if (trafficJam == null) {
            android.content.res.Resources resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
            trafficJam = resources.getString(com.navdy.hud.app.R.string.traffic_jam_title);
            delay = resources.getString(com.navdy.hud.app.R.string.traffic_notification_delay);
            min = resources.getString(com.navdy.hud.app.R.string.min);
            hr = resources.getString(com.navdy.hud.app.R.string.hr);
            dismiss = resources.getString(com.navdy.hud.app.R.string.traffic_notification_dismiss);
            int dismissColor = resources.getColor(com.navdy.hud.app.R.color.glance_dismiss);
            r4 = com.navdy.hud.app.R.drawable.icon_glances_dismiss;
            dismissChoices.add(new com.navdy.hud.app.ui.component.ChoiceLayout2.Choice(1, com.navdy.hud.app.R.drawable.icon_glances_dismiss, dismissColor, com.navdy.hud.app.R.drawable.icon_glances_dismiss, -16777216, dismiss, dismissColor));
            this.notifColor = resources.getColor(com.navdy.hud.app.R.color.traffic_bad);
        }
        this.bus = bus2;
        this.initialRemainingTime = initialEvent.remainingTime;
        this.logger.v("TrafficJamNotification: initialRemainingTime=" + initialEvent.remainingTime);
    }

    public com.navdy.hud.app.framework.notifications.NotificationType getType() {
        return com.navdy.hud.app.framework.notifications.NotificationType.TRAFFIC_JAM;
    }

    public java.lang.String getId() {
        return com.navdy.hud.app.framework.notifications.NotificationId.TRAFFIC_JAM_NOTIFICATION_ID;
    }

    public android.view.View getView(android.content.Context context) {
        if (this.container == null) {
            this.container = (android.view.ViewGroup) android.view.LayoutInflater.from(context).inflate(com.navdy.hud.app.R.layout.notification_traffic_jam, null);
            this.title = (android.widget.TextView) this.container.findViewById(com.navdy.hud.app.R.id.title);
            this.title1 = (android.widget.TextView) this.container.findViewById(com.navdy.hud.app.R.id.title1);
            this.title2 = (android.widget.TextView) this.container.findViewById(com.navdy.hud.app.R.id.title2);
            this.title3 = (android.widget.TextView) this.container.findViewById(com.navdy.hud.app.R.id.title3);
            this.gauge = (com.navdy.hud.app.view.Gauge) this.container.findViewById(com.navdy.hud.app.R.id.traffic_jam_progress);
            this.gauge.setMaxValue(this.initialRemainingTime);
            this.choiceLayout = (com.navdy.hud.app.ui.component.ChoiceLayout2) this.container.findViewById(com.navdy.hud.app.R.id.choiceLayout);
        }
        return this.container;
    }

    public android.view.View getExpandedView(android.content.Context context, java.lang.Object data) {
        return null;
    }

    public int getExpandedViewIndicatorColor() {
        return 0;
    }

    public void onStart(com.navdy.hud.app.framework.notifications.INotificationController controller) {
        super.onStart(controller);
        updateState();
    }

    public void onUpdate() {
        updateState();
    }

    public void onStop() {
        super.onStop();
    }

    public int getTimeout() {
        return 0;
    }

    public boolean isAlive() {
        return true;
    }

    public boolean isPurgeable() {
        return false;
    }

    public boolean canAddToStackIfCurrentExists() {
        return true;
    }

    public int getColor() {
        return this.notifColor;
    }

    public void onNotificationEvent(com.navdy.hud.app.ui.framework.UIStateManager.Mode mode) {
    }

    public void onExpandedNotificationEvent(com.navdy.hud.app.ui.framework.UIStateManager.Mode mode) {
    }

    public void onExpandedNotificationSwitched() {
    }

    public android.animation.AnimatorSet getViewSwitchAnimation(boolean viewIn) {
        return null;
    }

    public boolean expandNotification() {
        return false;
    }

    private void updateState() {
        if (this.trafficJamEvent != null) {
            this.title.setText(trafficJam);
            this.title1.setText(delay);
            if (this.trafficJamEvent.remainingTime >= 3600) {
                this.title2.setTextSize(2, 46.0f);
                this.title3.setText(hr);
            } else {
                this.title2.setTextSize(2, 64.0f);
                this.title3.setText(min);
            }
            this.title2.setText(com.navdy.hud.app.maps.util.MapUtils.formatTime(this.trafficJamEvent.remainingTime));
            this.gauge.setValue(this.trafficJamEvent.remainingTime);
            this.logger.v("TrafficJamNotification: trafficJamEvent.remainingTime=" + this.trafficJamEvent.remainingTime);
            this.choiceLayout.setChoices(dismissChoices, 0, this.choiceListener, 0.5f);
        }
    }

    public void setTrafficEvent(com.navdy.hud.app.maps.MapEvents.TrafficJamProgressEvent event) {
        this.trafficJamEvent = event;
    }
}
