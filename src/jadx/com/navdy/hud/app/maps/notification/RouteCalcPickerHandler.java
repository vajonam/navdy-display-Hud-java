package com.navdy.hud.app.maps.notification;

public class RouteCalcPickerHandler implements com.navdy.hud.app.ui.component.destination.IDestinationPicker {
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.maps.notification.RouteCalcPickerHandler.class);
    private com.navdy.hud.app.maps.MapEvents.RouteCalculationEvent event;

    class Anon1 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.service.library.events.navigation.NavigationRouteResult val$result;
        final /* synthetic */ com.navdy.hud.app.maps.here.HereRouteCache.RouteInfo val$routeInfo;

        Anon1(com.navdy.service.library.events.navigation.NavigationRouteResult navigationRouteResult, com.navdy.hud.app.maps.here.HereRouteCache.RouteInfo routeInfo) {
            this.val$result = navigationRouteResult;
            this.val$routeInfo = routeInfo;
        }

        public void run() {
            com.navdy.hud.app.maps.here.HereNavigationManager hereNavigationManager = com.navdy.hud.app.maps.here.HereNavigationManager.getInstance();
            hereNavigationManager.setReroute(this.val$routeInfo.route, com.navdy.service.library.events.navigation.NavigationSessionRouteChange.RerouteReason.NAV_SESSION_ROUTE_PICKER, this.val$result.routeId, this.val$result.via, true, hereNavigationManager.isTrafficConsidered(this.val$result.routeId));
            if (hereNavigationManager.getNavigationSessionPreference().spokenTurnByTurn) {
                java.lang.String tts = com.navdy.hud.app.maps.here.HereRouteManager.buildChangeRouteTTS(this.val$result.routeId);
                com.navdy.hud.app.maps.notification.RouteCalcPickerHandler.sLogger.v("tts[" + tts + "]");
                com.squareup.otto.Bus bus = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getBus();
                bus.post(com.navdy.hud.app.maps.notification.RouteCalculationNotification.CANCEL_TBT_TTS);
                bus.post(com.navdy.hud.app.maps.notification.RouteCalculationNotification.CANCEL_CALC_TTS);
                com.navdy.hud.app.framework.voice.TTSUtils.sendSpeechRequest(tts, com.navdy.service.library.events.audio.Category.SPEECH_TURN_BY_TURN, com.navdy.hud.app.maps.notification.RouteCalculationNotification.ROUTE_CALC_TTS_ID);
            }
            com.navdy.hud.app.maps.notification.RouteCalcPickerHandler.sLogger.w("new route set: " + this.val$routeInfo.routeRequest);
        }
    }

    public RouteCalcPickerHandler(com.navdy.hud.app.maps.MapEvents.RouteCalculationEvent event2) {
        this.event = event2;
    }

    public boolean onItemClicked(int id, int pos, com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.DestinationPickerState state) {
        com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getUiStateManager().getNavigationView().cleanupMapOverviewRoutes();
        if (pos == 0) {
            sLogger.v("map-route- onItemClicked current route");
        } else {
            sLogger.v("map-route- onItemClicked new route");
            int len = this.event.response.results.size();
            if (pos >= len) {
                sLogger.w("map-route- invalid pos:" + pos + " len=" + len);
            } else {
                com.navdy.service.library.events.navigation.NavigationRouteResult result = (com.navdy.service.library.events.navigation.NavigationRouteResult) this.event.response.results.get(pos);
                com.navdy.hud.app.maps.here.HereRouteCache.RouteInfo routeInfo = com.navdy.hud.app.maps.here.HereRouteCache.getInstance().getRoute(result.routeId);
                if (routeInfo == null) {
                    sLogger.w("map-route- route not found in cache:" + result.routeId);
                } else {
                    state.doNotAddOriginalRoute = true;
                    com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.maps.notification.RouteCalcPickerHandler.Anon1(result, routeInfo), 20);
                }
            }
        }
        return true;
    }

    public boolean onItemSelected(int id, int pos, com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.DestinationPickerState state) {
        return false;
    }

    public void onDestinationPickerClosed() {
    }
}
