package com.navdy.hud.app.profile;

public class DriverProfileManager {
    private static final java.lang.String DEFAULT_PROFILE_NAME = "DefaultProfile";
    private static final com.navdy.hud.app.event.DriverProfileChanged DRIVER_PROFILE_CHANGED = new com.navdy.hud.app.event.DriverProfileChanged();
    private static final java.lang.String LOCAL_PREFERENCES_SUFFIX = "_preferences";
    private static final com.navdy.service.library.device.NavdyDeviceId MOCK_PROFILE_DEVICE_ID = com.navdy.service.library.device.NavdyDeviceId.UNKNOWN_ID;
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.profile.DriverProfileManager.class);
    private java.lang.String lastUserEmail;
    private java.lang.String lastUserName;
    private java.util.HashMap<java.lang.String, com.navdy.hud.app.profile.DriverProfile> mAllProfiles = new java.util.HashMap<>();
    /* access modifiers changed from: private */
    public com.squareup.otto.Bus mBus;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.profile.DriverProfile mCurrentProfile;
    private java.lang.String mProfileDirectoryName;
    private java.util.Set<com.navdy.hud.app.profile.DriverProfile> mPublicProfiles = new java.util.HashSet();
    /* access modifiers changed from: private */
    public com.navdy.hud.app.profile.DriverSessionPreferences mSessionPrefs;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.profile.DriverProfile theDefaultProfile;

    class Anon1 implements java.io.FileFilter {
        Anon1() {
        }

        public boolean accept(java.io.File pathname) {
            return pathname.isDirectory();
        }
    }

    class Anon10 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.service.library.events.preferences.NotificationPreferencesUpdate val$update;

        Anon10(com.navdy.service.library.events.preferences.NotificationPreferencesUpdate notificationPreferencesUpdate) {
            this.val$update = notificationPreferencesUpdate;
        }

        public void run() {
            com.navdy.hud.app.profile.DriverProfileManager.this.mCurrentProfile.setNotificationPreferences(this.val$update.preferences);
            com.navdy.hud.app.profile.DriverProfileManager.this.theDefaultProfile.setNotificationPreferences(this.val$update.preferences);
            com.navdy.hud.app.analytics.AnalyticsSupport.recordPreferenceChange(com.navdy.hud.app.profile.DriverProfileManager.this.mCurrentProfile);
            com.navdy.hud.app.profile.DriverProfileManager.this.mBus.post(com.navdy.hud.app.profile.DriverProfileManager.this.mCurrentProfile.getNotificationPreferences());
        }
    }

    class Anon11 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.service.library.events.glances.CannedMessagesUpdate val$update;

        Anon11(com.navdy.service.library.events.glances.CannedMessagesUpdate cannedMessagesUpdate) {
            this.val$update = cannedMessagesUpdate;
        }

        public void run() {
            com.navdy.hud.app.profile.DriverProfileManager.this.mCurrentProfile.setCannedMessages(this.val$update);
        }
    }

    class Anon12 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.service.library.events.preferences.LocalPreferences val$preferences;

        Anon12(com.navdy.service.library.events.preferences.LocalPreferences localPreferences) {
            this.val$preferences = localPreferences;
        }

        public void run() {
            com.navdy.hud.app.profile.DriverProfileManager.this.mCurrentProfile.setLocalPreferences(this.val$preferences);
            com.navdy.hud.app.profile.DriverProfileManager.this.theDefaultProfile.setLocalPreferences(this.val$preferences);
            com.navdy.hud.app.analytics.AnalyticsSupport.recordPreferenceChange(com.navdy.hud.app.profile.DriverProfileManager.this.mCurrentProfile);
            com.navdy.hud.app.profile.DriverProfileManager.this.mBus.post(this.val$preferences);
        }
    }

    class Anon13 implements java.lang.Runnable {
        Anon13() {
        }

        public void run() {
            com.navdy.hud.app.profile.DriverProfileManager.sLogger.v("copyCurrentProfileToDefault");
            com.navdy.hud.app.profile.DriverProfileManager.this.theDefaultProfile.copy(com.navdy.hud.app.profile.DriverProfileManager.this.mCurrentProfile);
        }
    }

    class Anon2 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.service.library.device.NavdyDeviceId val$deviceId;

        Anon2(com.navdy.service.library.device.NavdyDeviceId navdyDeviceId) {
            this.val$deviceId = navdyDeviceId;
        }

        public void run() {
            com.navdy.hud.app.profile.DriverProfileManager.sLogger.v("loading profile:" + this.val$deviceId);
            com.navdy.hud.app.profile.DriverProfile profile = com.navdy.hud.app.profile.DriverProfileManager.this.getProfileForId(this.val$deviceId);
            if (profile == null) {
                com.navdy.hud.app.profile.DriverProfileManager.sLogger.w("profile not loaded:" + this.val$deviceId);
                profile = com.navdy.hud.app.profile.DriverProfileManager.this.createProfileForId(this.val$deviceId);
            }
            com.navdy.hud.app.profile.DriverProfileManager.this.setCurrentProfile(profile);
        }
    }

    class Anon3 implements java.lang.Runnable {
        Anon3() {
        }

        public void run() {
            com.navdy.hud.app.profile.DriverProfileManager.this.changeLocale(false);
            com.navdy.hud.app.profile.DriverProfileManager.this.mBus.post(new com.navdy.hud.app.event.DriverProfileUpdated(com.navdy.hud.app.event.DriverProfileUpdated.State.UP_TO_DATE));
        }
    }

    class Anon4 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.service.library.events.preferences.DriverProfilePreferences val$preferences;

        Anon4(com.navdy.service.library.events.preferences.DriverProfilePreferences driverProfilePreferences) {
            this.val$preferences = driverProfilePreferences;
        }

        public void run() {
            com.navdy.hud.app.profile.DriverProfileManager.this.refreshProfileImageIfNeeded(this.val$preferences.photo_checksum);
        }
    }

    class Anon5 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.service.library.events.preferences.DriverProfilePreferences val$preferences;

        Anon5(com.navdy.service.library.events.preferences.DriverProfilePreferences driverProfilePreferences) {
            this.val$preferences = driverProfilePreferences;
        }

        public void run() {
            com.navdy.service.library.events.preferences.DriverProfilePreferences driverProfilePreferences = com.navdy.hud.app.profile.DriverProfileManager.this.findSupportedLocale(this.val$preferences);
            java.util.Locale oldLocale = com.navdy.hud.app.profile.DriverProfileManager.this.mCurrentProfile.getLocale();
            com.navdy.hud.app.profile.DriverProfileManager.this.mCurrentProfile.setDriverProfilePreferences(driverProfilePreferences);
            com.navdy.hud.app.profile.DriverProfileManager.this.theDefaultProfile.setDriverProfilePreferences(driverProfilePreferences);
            java.util.Locale newLocale = com.navdy.hud.app.profile.DriverProfileManager.this.mCurrentProfile.getLocale();
            boolean localeChanged = !oldLocale.equals(newLocale);
            com.navdy.hud.app.profile.DriverProfileManager.sLogger.i("[HUD-locale] changed[" + localeChanged + "] current[" + oldLocale + "] new[" + newLocale + "]");
            com.navdy.hud.app.profile.DriverProfileManager.this.changeLocale(localeChanged);
            com.navdy.hud.app.profile.DriverProfileManager.this.mBus.post(new com.navdy.hud.app.event.DriverProfileUpdated(com.navdy.hud.app.event.DriverProfileUpdated.State.UPDATED));
        }
    }

    class Anon6 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.service.library.events.preferences.NavigationPreferencesUpdate val$update;

        Anon6(com.navdy.service.library.events.preferences.NavigationPreferencesUpdate navigationPreferencesUpdate) {
            this.val$update = navigationPreferencesUpdate;
        }

        public void run() {
            com.navdy.hud.app.profile.DriverProfileManager.this.mCurrentProfile.setNavigationPreferences(this.val$update.preferences);
            com.navdy.hud.app.profile.DriverProfileManager.this.theDefaultProfile.setNavigationPreferences(this.val$update.preferences);
            com.navdy.service.library.events.preferences.NavigationPreferences preferences = com.navdy.hud.app.profile.DriverProfileManager.this.mCurrentProfile.getNavigationPreferences();
            com.navdy.hud.app.analytics.AnalyticsSupport.recordNavigationPreferenceChange(com.navdy.hud.app.profile.DriverProfileManager.this.mCurrentProfile);
            com.navdy.hud.app.profile.DriverProfileManager.this.mSessionPrefs.getNavigationSessionPreference().setDefault(preferences);
            com.navdy.hud.app.profile.DriverProfileManager.this.mBus.post(preferences);
        }
    }

    class Anon7 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.service.library.events.preferences.AudioPreferencesUpdate val$update;

        Anon7(com.navdy.service.library.events.preferences.AudioPreferencesUpdate audioPreferencesUpdate) {
            this.val$update = audioPreferencesUpdate;
        }

        public void run() {
            com.navdy.hud.app.profile.DriverProfileManager.this.mCurrentProfile.setAudioPreferences(this.val$update.preferences);
            com.navdy.hud.app.profile.DriverProfileManager.this.theDefaultProfile.setAudioPreferences(this.val$update.preferences);
            com.navdy.hud.app.analytics.AnalyticsSupport.recordPreferenceChange(com.navdy.hud.app.profile.DriverProfileManager.this.mCurrentProfile);
            com.navdy.hud.app.profile.DriverProfileManager.this.mBus.post(com.navdy.hud.app.profile.DriverProfileManager.this.mCurrentProfile.getAudioPreferences());
        }
    }

    class Anon8 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.service.library.events.preferences.InputPreferencesUpdate val$update;

        Anon8(com.navdy.service.library.events.preferences.InputPreferencesUpdate inputPreferencesUpdate) {
            this.val$update = inputPreferencesUpdate;
        }

        public void run() {
            com.navdy.hud.app.profile.DriverProfileManager.this.mCurrentProfile.setInputPreferences(this.val$update.preferences);
            com.navdy.hud.app.profile.DriverProfileManager.this.theDefaultProfile.setInputPreferences(this.val$update.preferences);
            com.navdy.hud.app.analytics.AnalyticsSupport.recordPreferenceChange(com.navdy.hud.app.profile.DriverProfileManager.this.mCurrentProfile);
            com.navdy.hud.app.profile.DriverProfileManager.this.mBus.post(com.navdy.hud.app.profile.DriverProfileManager.this.mCurrentProfile.getInputPreferences());
        }
    }

    class Anon9 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.service.library.events.preferences.DisplaySpeakerPreferencesUpdate val$update;

        Anon9(com.navdy.service.library.events.preferences.DisplaySpeakerPreferencesUpdate displaySpeakerPreferencesUpdate) {
            this.val$update = displaySpeakerPreferencesUpdate;
        }

        public void run() {
            com.navdy.hud.app.profile.DriverProfileManager.this.mCurrentProfile.setSpeakerPreferences(this.val$update.preferences);
            com.navdy.hud.app.profile.DriverProfileManager.this.theDefaultProfile.setSpeakerPreferences(this.val$update.preferences);
            com.navdy.hud.app.analytics.AnalyticsSupport.recordPreferenceChange(com.navdy.hud.app.profile.DriverProfileManager.this.mCurrentProfile);
            com.navdy.hud.app.profile.DriverProfileManager.this.mBus.post(com.navdy.hud.app.profile.DriverProfileManager.this.mCurrentProfile.getSpeakerPreferences());
        }
    }

    @javax.inject.Inject
    public DriverProfileManager(com.squareup.otto.Bus bus, com.navdy.hud.app.storage.PathManager pathManager, com.navdy.hud.app.common.TimeHelper timeHelper) {
        this.mProfileDirectoryName = pathManager.getDriverProfilesDir();
        this.mBus = bus;
        this.mBus.register(this);
        sLogger.i("initializing in " + this.mProfileDirectoryName);
        java.io.File[] profileDirectories = new java.io.File(this.mProfileDirectoryName).listFiles(new com.navdy.hud.app.profile.DriverProfileManager.Anon1());
        int length = profileDirectories.length;
        int i = 0;
        while (i < length) {
            java.io.File profileDirectory = profileDirectories[i];
            try {
                com.navdy.hud.app.profile.DriverProfile profile = new com.navdy.hud.app.profile.DriverProfile(profileDirectory);
                java.lang.String profileName = profileDirectory.getName();
                this.mAllProfiles.put(profileName, profile);
                if (profile.isProfilePublic()) {
                    this.mPublicProfiles.add(profile);
                }
                if (profileName.equals(profileNameForId(MOCK_PROFILE_DEVICE_ID))) {
                    this.theDefaultProfile = profile;
                }
                i++;
            } catch (java.lang.Exception e) {
                sLogger.e("could not load profile from " + profileDirectory, e);
            }
        }
        if (this.theDefaultProfile == null) {
            this.theDefaultProfile = createProfileForId(MOCK_PROFILE_DEVICE_ID);
        }
        com.navdy.service.library.events.preferences.NavigationPreferences navigationPreferences = this.theDefaultProfile.getNavigationPreferences();
        this.mSessionPrefs = new com.navdy.hud.app.profile.DriverSessionPreferences(this.mBus, this.theDefaultProfile, timeHelper);
        setCurrentProfile(this.theDefaultProfile);
    }

    public com.navdy.hud.app.profile.DriverProfile getCurrentProfile() {
        return this.mCurrentProfile;
    }

    public void loadProfileForId(com.navdy.service.library.device.NavdyDeviceId deviceId) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.profile.DriverProfileManager.Anon2(deviceId), 10);
    }

    public void setCurrentProfile(com.navdy.hud.app.profile.DriverProfile profile) {
        sLogger.i("setting current profile to " + profile);
        if (profile == null) {
            profile = this.theDefaultProfile;
        }
        if (this.mCurrentProfile != profile) {
            this.mCurrentProfile = profile;
            this.mSessionPrefs.setDefault(this.mCurrentProfile, isDefaultProfile(this.mCurrentProfile));
            this.theDefaultProfile.setTrafficEnabled(this.mCurrentProfile.isTrafficEnabled());
            copyCurrentProfileToDefault();
            this.mBus.post(DRIVER_PROFILE_CHANGED);
        }
        this.lastUserName = this.mCurrentProfile.getDriverName();
        this.lastUserEmail = this.mCurrentProfile.getDriverEmail();
        sLogger.v("lastUserEmail:" + this.lastUserEmail);
    }

    public com.navdy.hud.app.profile.DriverProfile getProfileForId(com.navdy.service.library.device.NavdyDeviceId deviceId) {
        return (com.navdy.hud.app.profile.DriverProfile) this.mAllProfiles.get(profileNameForId(deviceId));
    }

    public java.util.Collection<com.navdy.hud.app.profile.DriverProfile> getPublicProfiles() {
        return this.mPublicProfiles;
    }

    public com.navdy.hud.app.profile.DriverProfile createProfileForId(com.navdy.service.library.device.NavdyDeviceId deviceId) {
        java.lang.String profileName = profileNameForId(deviceId);
        try {
            com.navdy.hud.app.profile.DriverProfile newProfile = com.navdy.hud.app.profile.DriverProfile.createProfileForId(profileName, this.mProfileDirectoryName);
            this.mAllProfiles.put(profileName, newProfile);
            return newProfile;
        } catch (java.io.IOException e) {
            sLogger.e("could not create new profile: " + e);
            return null;
        }
    }

    private java.lang.String profileNameForId(com.navdy.service.library.device.NavdyDeviceId deviceId) {
        java.lang.String profileName = deviceId.getBluetoothAddress();
        if (profileName == null) {
            profileName = DEFAULT_PROFILE_NAME;
        }
        return com.navdy.hud.app.util.GenericUtil.normalizeToFilename(profileName);
    }

    private void requestPreferenceUpdates(com.navdy.hud.app.profile.DriverProfile profile) {
        sLogger.v("requestPreferenceUpdates:" + profile.getProfileName());
        sendEventToClient(new com.navdy.service.library.events.preferences.DriverProfilePreferencesRequest.Builder().serial_number(profile.mDriverProfilePreferences.serial_number).build());
        sLogger.v("requestPreferenceUpdates: nav:" + profile.mNavigationPreferences.serial_number);
        sendEventToClient(new com.navdy.service.library.events.preferences.NavigationPreferencesRequest.Builder().serial_number(profile.mNavigationPreferences.serial_number).build());
        sLogger.v("requestPreferenceUpdates: notif:" + profile.mNotificationPreferences.serial_number);
        sendEventToClient(new com.navdy.service.library.events.preferences.NotificationPreferencesRequest.Builder().serial_number(profile.mNotificationPreferences.serial_number).build());
        sLogger.v("requestPreferenceUpdates: input:" + profile.mInputPreferences.serial_number);
        sendEventToClient(new com.navdy.service.library.events.preferences.InputPreferencesRequest.Builder().serial_number(profile.mInputPreferences.serial_number).build());
        sLogger.v("requestPreferenceUpdates: audio:" + profile.mAudioPreferences.serial_number);
        sendEventToClient(new com.navdy.service.library.events.preferences.AudioPreferencesRequest.Builder().serial_number(profile.mAudioPreferences.serial_number).build());
    }

    private void requestUpdates(com.navdy.hud.app.profile.DriverProfile profile) {
        sLogger.v("requestUpdates(canned-msgs) [" + profile.mCannedMessages.serial_number + "]");
        sendEventToClient(new com.navdy.service.library.events.glances.CannedMessagesRequest.Builder().serial_number(profile.mCannedMessages.serial_number).build());
    }

    private void sendEventToClient(com.squareup.wire.Message message) {
        this.mBus.post(new com.navdy.hud.app.event.RemoteEvent(message));
    }

    @com.squareup.otto.Subscribe
    public void onDriverProfileChange(com.navdy.hud.app.event.DriverProfileChanged changed) {
        this.mBus.post(this.mCurrentProfile.getNavigationPreferences());
    }

    @com.squareup.otto.Subscribe
    public void onDeviceSyncRequired(com.navdy.hud.app.service.ConnectionHandler.DeviceSyncEvent event) {
        sLogger.v("calling requestPreferenceUpdates");
        requestPreferenceUpdates(this.mCurrentProfile);
        sLogger.v("calling requestUpdates");
        requestUpdates(this.mCurrentProfile);
    }

    @com.squareup.otto.Subscribe
    public void onDriverProfilePreferencesUpdate(com.navdy.service.library.events.preferences.DriverProfilePreferencesUpdate update) {
        if (this.mCurrentProfile != this.theDefaultProfile) {
            switch (update.status) {
                case REQUEST_VERSION_IS_CURRENT:
                    sLogger.i("prefs are up to date!");
                    com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.profile.DriverProfileManager.Anon3(), 10);
                    return;
                case REQUEST_SUCCESS:
                    sLogger.i("Received profile update, applying");
                    if (this.mCurrentProfile != null) {
                        com.navdy.service.library.events.preferences.DriverProfilePreferences preferences = update.preferences;
                        if (preferences != null) {
                            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.profile.DriverProfileManager.Anon4(preferences), 10);
                            if (((java.lang.Boolean) com.squareup.wire.Wire.get(preferences.profile_is_public, com.navdy.service.library.events.preferences.DriverProfilePreferences.DEFAULT_PROFILE_IS_PUBLIC)).booleanValue()) {
                                this.mPublicProfiles.add(this.mCurrentProfile);
                            } else {
                                this.mPublicProfiles.remove(this.mCurrentProfile);
                            }
                            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.profile.DriverProfileManager.Anon5(preferences), 10);
                            return;
                        }
                        sLogger.i("preferences update with no preferences!");
                        return;
                    }
                    return;
                default:
                    sLogger.i("profile status was " + update.status + ", which I didn't expect; ignoring.");
                    return;
            }
        }
    }

    /* access modifiers changed from: private */
    public com.navdy.service.library.events.preferences.DriverProfilePreferences findSupportedLocale(com.navdy.service.library.events.preferences.DriverProfilePreferences preferences) {
        com.navdy.service.library.events.preferences.DriverProfilePreferences driverProfilePreferences = preferences;
        if (com.navdy.hud.app.profile.HudLocale.isLocaleSupported(com.navdy.hud.app.profile.HudLocale.getLocaleForID(preferences.locale)) || driverProfilePreferences.additionalLocales == null) {
            return driverProfilePreferences;
        }
        java.util.Locale supportedAdditionalLocale = null;
        java.util.Iterator it = driverProfilePreferences.additionalLocales.iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            java.lang.String additionalLocaleStr = (java.lang.String) it.next();
            if (!android.text.TextUtils.isEmpty(additionalLocaleStr)) {
                java.util.Locale additionalLocale = com.navdy.hud.app.profile.HudLocale.getLocaleForID(additionalLocaleStr);
                if (com.navdy.hud.app.profile.HudLocale.isLocaleSupported(additionalLocale)) {
                    sLogger.v("HUD-locale additional locale supported:" + additionalLocale);
                    supportedAdditionalLocale = additionalLocale;
                    break;
                }
                sLogger.v("HUD-locale additional locale not supported:" + additionalLocale);
            }
        }
        if (supportedAdditionalLocale != null) {
            return new com.navdy.service.library.events.preferences.DriverProfilePreferences.Builder(driverProfilePreferences).locale(supportedAdditionalLocale.toString()).build();
        }
        return driverProfilePreferences;
    }

    /* access modifiers changed from: private */
    public void refreshProfileImageIfNeeded(java.lang.String remoteChecksum) {
        com.navdy.hud.app.framework.contacts.PhoneImageDownloader downloader = com.navdy.hud.app.framework.contacts.PhoneImageDownloader.getInstance();
        java.lang.String localChecksum = downloader.hashFor(com.navdy.hud.app.profile.DriverProfile.DRIVER_PROFILE_IMAGE, com.navdy.service.library.events.photo.PhotoType.PHOTO_DRIVER_PROFILE);
        if (!java.util.Objects.equals(remoteChecksum, localChecksum)) {
            sLogger.d("asking for new photo - local:" + localChecksum + " remote:" + remoteChecksum);
            downloader.submitDownload(com.navdy.hud.app.profile.DriverProfile.DRIVER_PROFILE_IMAGE, com.navdy.hud.app.framework.contacts.PhoneImageDownloader.Priority.NORMAL, com.navdy.service.library.events.photo.PhotoType.PHOTO_DRIVER_PROFILE, null);
        }
    }

    @com.squareup.otto.Subscribe
    public void onNavigationPreferencesUpdate(com.navdy.service.library.events.preferences.NavigationPreferencesUpdate update) {
        handleUpdateInBackground(update, update.status, update.preferences, new com.navdy.hud.app.profile.DriverProfileManager.Anon6(update));
    }

    @com.squareup.otto.Subscribe
    public void onAudioPreferencesUpdate(com.navdy.service.library.events.preferences.AudioPreferencesUpdate update) {
        handleUpdateInBackground(update, update.status, update.preferences, new com.navdy.hud.app.profile.DriverProfileManager.Anon7(update));
    }

    @com.squareup.otto.Subscribe
    public void onInputPreferencesUpdate(com.navdy.service.library.events.preferences.InputPreferencesUpdate update) {
        handleUpdateInBackground(update, update.status, update.preferences, new com.navdy.hud.app.profile.DriverProfileManager.Anon8(update));
    }

    @com.squareup.otto.Subscribe
    public void onDisplaySpeakerPreferencesUpdate(com.navdy.service.library.events.preferences.DisplaySpeakerPreferencesUpdate update) {
        handleUpdateInBackground(update, update.status, update.preferences, new com.navdy.hud.app.profile.DriverProfileManager.Anon9(update));
    }

    @com.squareup.otto.Subscribe
    public void onNotificationPreferencesUpdate(com.navdy.service.library.events.preferences.NotificationPreferencesUpdate update) {
        handleUpdateInBackground(update, update.status, update.preferences, new com.navdy.hud.app.profile.DriverProfileManager.Anon10(update));
    }

    @com.squareup.otto.Subscribe
    public void onCannedMessagesUpdate(com.navdy.service.library.events.glances.CannedMessagesUpdate update) {
        handleUpdateInBackground(update, update.status, update, new com.navdy.hud.app.profile.DriverProfileManager.Anon11(update));
    }

    public void updateLocalPreferences(com.navdy.service.library.events.preferences.LocalPreferences preferences) {
        if (this.mCurrentProfile != null) {
            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.profile.DriverProfileManager.Anon12(preferences), 10);
        }
    }

    public com.navdy.service.library.events.preferences.LocalPreferences getLocalPreferences() {
        return this.mCurrentProfile.getLocalPreferences();
    }

    private void handleUpdateInBackground(com.squareup.wire.Message request, com.navdy.service.library.events.RequestStatus status, com.squareup.wire.Message preferences, java.lang.Runnable runnable) {
        if (this.mCurrentProfile != this.theDefaultProfile) {
            java.lang.String requestType = request.getClass().getSimpleName();
            switch (status) {
                case REQUEST_VERSION_IS_CURRENT:
                    sLogger.i(requestType + " returned up to date!");
                    return;
                case REQUEST_SUCCESS:
                    sLogger.i("Received " + requestType + ", applying");
                    if (this.mCurrentProfile == null) {
                        return;
                    }
                    if (preferences != null) {
                        com.navdy.service.library.task.TaskManager.getInstance().execute(runnable, 10);
                        return;
                    } else {
                        sLogger.i(requestType + " with no preferences!");
                        return;
                    }
                default:
                    sLogger.i("profile status for " + requestType + " was " + status + ", which I didn't expect; ignoring.");
                    return;
            }
        }
    }

    @com.squareup.otto.Subscribe
    public void onPhotoDownload(com.navdy.hud.app.framework.contacts.PhoneImageDownloader.PhotoDownloadStatus photoStatus) {
        if (photoStatus.photoType != com.navdy.service.library.events.photo.PhotoType.PHOTO_DRIVER_PROFILE || photoStatus.alreadyDownloaded || this.mCurrentProfile == this.theDefaultProfile) {
            return;
        }
        if (photoStatus.success) {
            sLogger.d("applying new photo");
        } else {
            sLogger.i("driver photo not downloaded");
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean isDefaultProfile(com.navdy.hud.app.profile.DriverProfile profile) {
        return profile == this.theDefaultProfile;
    }

    public android.content.SharedPreferences getLocalPreferencesForDriverProfile(android.content.Context context, com.navdy.hud.app.profile.DriverProfile profile) {
        return context.getSharedPreferences(profile.getProfileName() + LOCAL_PREFERENCES_SUFFIX, 0);
    }

    public android.content.SharedPreferences getLocalPreferencesForCurrentDriverProfile(android.content.Context context) {
        return getLocalPreferencesForDriverProfile(context, getCurrentProfile());
    }

    public com.navdy.hud.app.profile.DriverSessionPreferences getSessionPreferences() {
        return this.mSessionPrefs;
    }

    public boolean isManualZoom() {
        com.navdy.service.library.events.preferences.LocalPreferences localPreferences = getLocalPreferences();
        if (localPreferences == null || !java.lang.Boolean.TRUE.equals(localPreferences.manualZoom)) {
            return false;
        }
        return true;
    }

    public java.util.Locale getCurrentLocale() {
        return this.mCurrentProfile.getLocale();
    }

    public java.lang.String getLastUserName() {
        return this.lastUserName;
    }

    public java.lang.String getLastUserEmail() {
        return this.lastUserEmail;
    }

    public void enableTraffic() {
        this.mCurrentProfile.setTrafficEnabled(true);
        this.theDefaultProfile.setTrafficEnabled(true);
    }

    public void disableTraffic() {
        this.mCurrentProfile.setTrafficEnabled(false);
        this.theDefaultProfile.setTrafficEnabled(false);
    }

    public boolean isTrafficEnabled() {
        return this.mCurrentProfile.isTrafficEnabled();
    }

    private void copyCurrentProfileToDefault() {
        if (this.mCurrentProfile != this.theDefaultProfile) {
            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.profile.DriverProfileManager.Anon13(), 10);
        }
    }

    /* access modifiers changed from: private */
    public void changeLocale(boolean showLanguageNotSupportedToast) {
        try {
            java.util.Locale locale = getCurrentLocale();
            sLogger.v("[HUD-locale] changeLocale current=" + locale);
            if (com.navdy.hud.app.profile.HudLocale.switchLocale(com.navdy.hud.app.HudApplication.getAppContext(), locale)) {
                sLogger.v("[HUD-locale] change, restarting...");
                this.mBus.post(new com.navdy.hud.app.event.InitEvents.InitPhase(com.navdy.hud.app.event.InitEvents.Phase.SWITCHING_LOCALE));
                com.navdy.hud.app.profile.HudLocale.showLocaleChangeToast();
                return;
            }
            sLogger.v("[HUD-locale] not changed");
            this.mBus.post(new com.navdy.hud.app.event.InitEvents.InitPhase(com.navdy.hud.app.event.InitEvents.Phase.LOCALE_UP_TO_DATE));
            if (com.navdy.hud.app.profile.HudLocale.isLocaleSupported(locale)) {
                com.navdy.hud.app.profile.HudLocale.dismissToast();
            } else if (showLanguageNotSupportedToast) {
                com.navdy.hud.app.profile.HudLocale.showLocaleNotSupportedToast(locale.getDisplayLanguage());
            }
        } catch (Throwable t) {
            sLogger.e("[HUD-locale] changeLocale", t);
        }
    }
}
