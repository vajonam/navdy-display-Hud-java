package com.navdy.hud.app.profile;

public final class DriverProfileManager$$InjectAdapter extends dagger.internal.Binding<com.navdy.hud.app.profile.DriverProfileManager> implements javax.inject.Provider<com.navdy.hud.app.profile.DriverProfileManager> {
    private dagger.internal.Binding<com.squareup.otto.Bus> bus;
    private dagger.internal.Binding<com.navdy.hud.app.storage.PathManager> pathManager;
    private dagger.internal.Binding<com.navdy.hud.app.common.TimeHelper> timeHelper;

    public DriverProfileManager$$InjectAdapter() {
        super("com.navdy.hud.app.profile.DriverProfileManager", "members/com.navdy.hud.app.profile.DriverProfileManager", false, com.navdy.hud.app.profile.DriverProfileManager.class);
    }

    public void attach(dagger.internal.Linker linker) {
        this.bus = linker.requestBinding("com.squareup.otto.Bus", com.navdy.hud.app.profile.DriverProfileManager.class, getClass().getClassLoader());
        this.pathManager = linker.requestBinding("com.navdy.hud.app.storage.PathManager", com.navdy.hud.app.profile.DriverProfileManager.class, getClass().getClassLoader());
        this.timeHelper = linker.requestBinding("com.navdy.hud.app.common.TimeHelper", com.navdy.hud.app.profile.DriverProfileManager.class, getClass().getClassLoader());
    }

    public void getDependencies(java.util.Set<dagger.internal.Binding<?>> getBindings, java.util.Set<dagger.internal.Binding<?>> set) {
        getBindings.add(this.bus);
        getBindings.add(this.pathManager);
        getBindings.add(this.timeHelper);
    }

    public com.navdy.hud.app.profile.DriverProfileManager get() {
        return new com.navdy.hud.app.profile.DriverProfileManager((com.squareup.otto.Bus) this.bus.get(), (com.navdy.hud.app.storage.PathManager) this.pathManager.get(), (com.navdy.hud.app.common.TimeHelper) this.timeHelper.get());
    }
}
