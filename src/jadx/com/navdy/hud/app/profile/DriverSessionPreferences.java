package com.navdy.hud.app.profile;

public class DriverSessionPreferences {
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.profile.DriverSessionPreferences.class);
    private com.squareup.otto.Bus bus;
    private com.navdy.service.library.events.settings.DateTimeConfiguration clockConfiguration;
    private com.navdy.service.library.events.settings.DateTimeConfiguration defaultClockConfiguration = new com.navdy.service.library.events.settings.DateTimeConfiguration(java.lang.Long.valueOf(0), null, com.navdy.service.library.events.settings.DateTimeConfiguration.Clock.CLOCK_12_HOUR);
    private com.navdy.hud.app.maps.NavSessionPreferences navSessionPreferences;
    private com.navdy.hud.app.common.TimeHelper timeHelper;

    public DriverSessionPreferences(com.squareup.otto.Bus bus2, com.navdy.hud.app.profile.DriverProfile profile, com.navdy.hud.app.common.TimeHelper timeHelper2) {
        this.bus = bus2;
        this.timeHelper = timeHelper2;
        timeHelper2.setFormat(com.navdy.service.library.events.settings.DateTimeConfiguration.Clock.CLOCK_12_HOUR);
        this.navSessionPreferences = new com.navdy.hud.app.maps.NavSessionPreferences(profile.getNavigationPreferences());
        setClockConfiguration(this.defaultClockConfiguration);
    }

    public com.navdy.hud.app.maps.NavSessionPreferences getNavigationSessionPreference() {
        return this.navSessionPreferences;
    }

    public void setDefault(com.navdy.hud.app.profile.DriverProfile profile, boolean isDefault) {
        com.navdy.service.library.events.preferences.LocalPreferences localPreferences = profile.getLocalPreferences();
        if (localPreferences != null && localPreferences.clockFormat != null) {
            setClockConfiguration(localPreferences.clockFormat);
        } else if (isDefault) {
            setClockConfiguration(this.defaultClockConfiguration);
        }
        this.navSessionPreferences.setDefault(profile.getNavigationPreferences());
    }

    public void setClockConfiguration(com.navdy.service.library.events.settings.DateTimeConfiguration clockConfiguration2) {
        this.clockConfiguration = clockConfiguration2;
        if (clockConfiguration2.format != null) {
            setClockConfiguration(clockConfiguration2.format);
        }
    }

    public void setClockConfiguration(com.navdy.service.library.events.settings.DateTimeConfiguration.Clock format) {
        if (format != null) {
            switch (format) {
                case CLOCK_12_HOUR:
                    this.timeHelper.setFormat(com.navdy.service.library.events.settings.DateTimeConfiguration.Clock.CLOCK_12_HOUR);
                    break;
                case CLOCK_24_HOUR:
                    this.timeHelper.setFormat(com.navdy.service.library.events.settings.DateTimeConfiguration.Clock.CLOCK_24_HOUR);
                    break;
            }
            sLogger.i("setClockConfiguration:" + format);
            this.bus.post(com.navdy.hud.app.common.TimeHelper.CLOCK_CHANGED);
        }
    }
}
