package com.navdy.hud.app.profile;

public class DriverProfile {
    private static final java.lang.String AUDIO_PREFERENCES = "AudioPreferences";
    private static final java.lang.String CANNED_MESSAGES = "CannedMessages";
    private static final java.lang.String CONTACTS_IMAGE_CACHE_DIR = "contacts";
    public static final java.lang.String DRIVER_PROFILE_IMAGE = "DriverImage";
    private static final java.lang.String DRIVER_PROFILE_PREFERENCES = "DriverProfilePreferences";
    private static final java.lang.String INPUT_PREFERENCES = "InputPreferences";
    private static final java.lang.String LOCALE_SEPARATOR = "_";
    private static final java.lang.String LOCAL_PREFERENCES = "LocalPreferences";
    private static final java.lang.String MUSIC_IMAGE_CACHE_DIR = "music";
    private static final java.lang.String NAVIGATION_PREFERENCES = "NavigationPreferences";
    private static final java.lang.String NOTIFICATION_PREFERENCES = "NotificationPreferences";
    private static final java.lang.String PLACES_IMAGE_CACHE_DIR = "places";
    private static final java.lang.String PREFERENCES_DIRECTORY = "Preferences";
    private static final java.lang.String SPEAKER_PREFERENCES = "SpeakerPreferences";
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.profile.DriverProfile.class);
    com.navdy.service.library.events.preferences.AudioPreferences mAudioPreferences;
    com.navdy.service.library.events.glances.CannedMessagesUpdate mCannedMessages;
    private java.io.File mContactsImageDirectory;
    private android.graphics.Bitmap mDriverImage;
    com.navdy.service.library.events.preferences.DriverProfilePreferences mDriverProfilePreferences;
    com.navdy.service.library.events.preferences.InputPreferences mInputPreferences;
    private com.navdy.service.library.events.preferences.LocalPreferences mLocalPreferences;
    private java.util.Locale mLocale;
    private com.navdy.service.library.events.MessageStore mMessageStore = new com.navdy.service.library.events.MessageStore(this.mPreferencesDirectory);
    private java.io.File mMusicImageDirectory;
    com.navdy.service.library.events.preferences.NavigationPreferences mNavigationPreferences;
    com.navdy.service.library.events.preferences.NotificationPreferences mNotificationPreferences;
    private com.navdy.hud.app.profile.NotificationSettings mNotificationSettings;
    private java.io.File mPlacesImageDirectory;
    /* access modifiers changed from: private */
    public java.io.File mPreferencesDirectory;
    /* access modifiers changed from: private */
    public java.io.File mProfileDirectory;
    private java.lang.String mProfileName;
    private com.navdy.service.library.events.preferences.DisplaySpeakerPreferences mSpeakerPreferences;
    private boolean mTrafficEnabled;

    class Anon1 extends com.navdy.service.library.events.NavdyEventUtil.Initializer<com.navdy.service.library.events.preferences.DriverProfilePreferences> {
        Anon1() {
        }

        public com.navdy.service.library.events.preferences.DriverProfilePreferences build(com.squareup.wire.Message.Builder<com.navdy.service.library.events.preferences.DriverProfilePreferences> builder) {
            return ((com.navdy.service.library.events.preferences.DriverProfilePreferences.Builder) builder).device_name("Phone " + com.navdy.hud.app.profile.DriverProfile.this.mProfileDirectory.getName()).build();
        }
    }

    class Anon2 implements java.lang.Runnable {
        Anon2() {
        }

        public void run() {
            com.navdy.service.library.util.IOUtils.deleteFile(com.navdy.hud.app.HudApplication.getAppContext(), new java.io.File(com.navdy.hud.app.profile.DriverProfile.this.mPreferencesDirectory, com.navdy.hud.app.profile.DriverProfile.DRIVER_PROFILE_IMAGE).getAbsolutePath());
        }
    }

    static com.navdy.hud.app.profile.DriverProfile createProfileForId(java.lang.String profileName, java.lang.String profileDirectoryName) throws java.io.IOException {
        if (profileName.contains(java.io.File.separator) || profileName.startsWith(com.navdy.hud.app.framework.glance.GlanceConstants.PERIOD)) {
            throw new java.lang.IllegalArgumentException("Profile names can't refer to directories");
        }
        java.lang.String profilePath = profileDirectoryName + java.io.File.separator + profileName;
        java.io.File profileDirectory = new java.io.File(profilePath);
        if (profileDirectory.mkdir() || profileDirectory.isDirectory()) {
            java.io.File preferencesDirectory = new java.io.File(profilePath + java.io.File.separator + PREFERENCES_DIRECTORY);
            if (preferencesDirectory.mkdir() || preferencesDirectory.isDirectory()) {
                return new com.navdy.hud.app.profile.DriverProfile(profileDirectory);
            }
            throw new java.io.IOException("could not create preferences directory");
        }
        throw new java.io.IOException("could not create profile");
    }

    protected DriverProfile(java.io.File profileDirectory) throws java.io.IOException {
        this.mProfileName = profileDirectory.getName();
        this.mProfileDirectory = profileDirectory;
        this.mPreferencesDirectory = new java.io.File(profileDirectory, PREFERENCES_DIRECTORY);
        this.mPlacesImageDirectory = new java.io.File(profileDirectory, PLACES_IMAGE_CACHE_DIR);
        com.navdy.service.library.util.IOUtils.createDirectory(this.mPlacesImageDirectory);
        this.mContactsImageDirectory = new java.io.File(profileDirectory, CONTACTS_IMAGE_CACHE_DIR);
        com.navdy.service.library.util.IOUtils.createDirectory(this.mContactsImageDirectory);
        this.mMusicImageDirectory = new java.io.File(profileDirectory, "music");
        com.navdy.service.library.util.IOUtils.createDirectory(this.mMusicImageDirectory);
        this.mDriverProfilePreferences = (com.navdy.service.library.events.preferences.DriverProfilePreferences) readPreference(DRIVER_PROFILE_PREFERENCES, com.navdy.service.library.events.preferences.DriverProfilePreferences.class, new com.navdy.hud.app.profile.DriverProfile.Anon1());
        this.mNavigationPreferences = (com.navdy.service.library.events.preferences.NavigationPreferences) readPreference(NAVIGATION_PREFERENCES, com.navdy.service.library.events.preferences.NavigationPreferences.class);
        setTraffic();
        this.mInputPreferences = (com.navdy.service.library.events.preferences.InputPreferences) readPreference(INPUT_PREFERENCES, com.navdy.service.library.events.preferences.InputPreferences.class);
        this.mSpeakerPreferences = (com.navdy.service.library.events.preferences.DisplaySpeakerPreferences) readPreference(SPEAKER_PREFERENCES, com.navdy.service.library.events.preferences.DisplaySpeakerPreferences.class);
        this.mAudioPreferences = (com.navdy.service.library.events.preferences.AudioPreferences) readPreference(AUDIO_PREFERENCES, com.navdy.service.library.events.preferences.AudioPreferences.class);
        this.mNotificationPreferences = (com.navdy.service.library.events.preferences.NotificationPreferences) readPreference(NOTIFICATION_PREFERENCES, com.navdy.service.library.events.preferences.NotificationPreferences.class);
        this.mNotificationSettings = new com.navdy.hud.app.profile.NotificationSettings(this.mNotificationPreferences);
        this.mLocalPreferences = (com.navdy.service.library.events.preferences.LocalPreferences) readPreference(LOCAL_PREFERENCES, com.navdy.service.library.events.preferences.LocalPreferences.class);
        this.mCannedMessages = (com.navdy.service.library.events.glances.CannedMessagesUpdate) readPreference(CANNED_MESSAGES, com.navdy.service.library.events.glances.CannedMessagesUpdate.class);
        java.io.InputStream driverImageStream = null;
        try {
            java.io.InputStream driverImageStream2 = new java.io.FileInputStream(new java.io.File(this.mPreferencesDirectory, DRIVER_PROFILE_IMAGE));
            try {
                this.mDriverImage = android.graphics.BitmapFactory.decodeStream(driverImageStream2);
                com.navdy.service.library.util.IOUtils.closeStream(driverImageStream2);
                java.io.FileInputStream fileInputStream = driverImageStream2;
            } catch (java.io.FileNotFoundException e) {
                driverImageStream = driverImageStream2;
                try {
                    this.mDriverImage = null;
                    com.navdy.service.library.util.IOUtils.closeStream(driverImageStream);
                    setLocale();
                } catch (Throwable th) {
                    th = th;
                    com.navdy.service.library.util.IOUtils.closeStream(driverImageStream);
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                driverImageStream = driverImageStream2;
                com.navdy.service.library.util.IOUtils.closeStream(driverImageStream);
                throw th;
            }
        } catch (java.io.FileNotFoundException e2) {
            this.mDriverImage = null;
            com.navdy.service.library.util.IOUtils.closeStream(driverImageStream);
            setLocale();
        }
        setLocale();
    }

    private <T extends com.squareup.wire.Message> T readPreference(java.lang.String filename, java.lang.Class<T> type) throws java.io.IOException {
        return this.mMessageStore.readMessage(filename, type);
    }

    private <T extends com.squareup.wire.Message> T readPreference(java.lang.String filename, java.lang.Class<T> type, com.navdy.service.library.events.NavdyEventUtil.Initializer<T> initializer) throws java.io.IOException {
        return this.mMessageStore.readMessage(filename, type, initializer);
    }

    /* access modifiers changed from: 0000 */
    public void setDriverProfilePreferences(com.navdy.service.library.events.preferences.DriverProfilePreferences preferences) {
        this.mDriverProfilePreferences = (com.navdy.service.library.events.preferences.DriverProfilePreferences) removeNulls(preferences);
        setLocale();
        sLogger.i("[" + this.mProfileName + "] updating driver profile preferences to ver[" + preferences.serial_number + "] " + this.mDriverProfilePreferences);
        writeMessageToFile(this.mDriverProfilePreferences, DRIVER_PROFILE_PREFERENCES);
    }

    public com.navdy.service.library.events.preferences.NavigationPreferences getNavigationPreferences() {
        return this.mNavigationPreferences;
    }

    public com.navdy.service.library.events.preferences.InputPreferences getInputPreferences() {
        return this.mInputPreferences;
    }

    public com.navdy.service.library.events.preferences.NotificationPreferences getNotificationPreferences() {
        return this.mNotificationPreferences;
    }

    public com.navdy.hud.app.profile.NotificationSettings getNotificationSettings() {
        return this.mNotificationSettings;
    }

    public com.navdy.service.library.events.preferences.DisplaySpeakerPreferences getSpeakerPreferences() {
        return this.mSpeakerPreferences;
    }

    public com.navdy.service.library.events.preferences.AudioPreferences getAudioPreferences() {
        return this.mAudioPreferences;
    }

    public com.navdy.service.library.events.preferences.LocalPreferences getLocalPreferences() {
        return this.mLocalPreferences;
    }

    public com.navdy.service.library.events.glances.CannedMessagesUpdate getCannedMessages() {
        return this.mCannedMessages;
    }

    /* access modifiers changed from: 0000 */
    public void setNavigationPreferences(com.navdy.service.library.events.preferences.NavigationPreferences preferences) {
        this.mNavigationPreferences = (com.navdy.service.library.events.preferences.NavigationPreferences) removeNulls(preferences);
        sLogger.i("[" + this.mProfileName + "] updating nav preferences to ver[" + preferences.serial_number + "] " + this.mNavigationPreferences);
        writeMessageToFile(this.mNavigationPreferences, NAVIGATION_PREFERENCES);
        setTraffic();
    }

    /* access modifiers changed from: 0000 */
    public void setInputPreferences(com.navdy.service.library.events.preferences.InputPreferences preferences) {
        this.mInputPreferences = (com.navdy.service.library.events.preferences.InputPreferences) removeNulls(preferences);
        sLogger.i("[" + this.mProfileName + "] updating input preferences to ver[" + preferences.serial_number + "] " + this.mInputPreferences);
        writeMessageToFile(this.mInputPreferences, INPUT_PREFERENCES);
    }

    /* access modifiers changed from: 0000 */
    public void setSpeakerPreferences(com.navdy.service.library.events.preferences.DisplaySpeakerPreferences preferences) {
        this.mSpeakerPreferences = (com.navdy.service.library.events.preferences.DisplaySpeakerPreferences) removeNulls(preferences);
        sLogger.i("[" + this.mProfileName + "] updating speaker preferences to ver[" + preferences.serial_number + "] " + this.mSpeakerPreferences);
        writeMessageToFile(this.mSpeakerPreferences, SPEAKER_PREFERENCES);
    }

    /* access modifiers changed from: 0000 */
    public void setAudioPreferences(com.navdy.service.library.events.preferences.AudioPreferences preferences) {
        this.mAudioPreferences = (com.navdy.service.library.events.preferences.AudioPreferences) removeNulls(preferences);
        sLogger.i("[" + this.mProfileName + "] updating input preferences to ver[" + preferences.serial_number + "] " + this.mAudioPreferences);
        writeMessageToFile(this.mAudioPreferences, AUDIO_PREFERENCES);
    }

    /* access modifiers changed from: 0000 */
    public void setNotificationPreferences(com.navdy.service.library.events.preferences.NotificationPreferences preferences) {
        this.mNotificationPreferences = (com.navdy.service.library.events.preferences.NotificationPreferences) removeNulls(preferences);
        sLogger.i("[" + this.mProfileName + "] updating notification preferences to ver[" + preferences.serial_number + "] " + this.mNotificationPreferences);
        writeMessageToFile(this.mNotificationPreferences, NOTIFICATION_PREFERENCES);
        this.mNotificationSettings.update(this.mNotificationPreferences);
    }

    public void setLocalPreferences(com.navdy.service.library.events.preferences.LocalPreferences preferences) {
        this.mLocalPreferences = (com.navdy.service.library.events.preferences.LocalPreferences) removeNulls(preferences);
        sLogger.i("[" + this.mProfileName + "] updating local preferences to " + this.mLocalPreferences);
        writeMessageToFile(this.mLocalPreferences, LOCAL_PREFERENCES);
    }

    /* access modifiers changed from: 0000 */
    public void setCannedMessages(com.navdy.service.library.events.glances.CannedMessagesUpdate cannedMessages) {
        this.mCannedMessages = (com.navdy.service.library.events.glances.CannedMessagesUpdate) removeNulls(cannedMessages);
        sLogger.i("[" + this.mProfileName + "] updating canned messages preferences to ver[" + cannedMessages.serial_number + "]");
        writeMessageToFile(this.mCannedMessages, CANNED_MESSAGES);
    }

    private void writeMessageToFile(com.squareup.wire.Message message, java.lang.String fileName) {
        this.mMessageStore.writeMessage(message, fileName);
    }

    private <T extends com.squareup.wire.Message> T removeNulls(T message) {
        return com.navdy.service.library.events.NavdyEventUtil.applyDefaults(message);
    }

    public java.lang.String getDriverName() {
        return this.mDriverProfilePreferences.driver_name;
    }

    public java.lang.String getFirstName() {
        java.lang.String driverName = this.mDriverProfilePreferences.driver_name;
        if (android.text.TextUtils.isEmpty(driverName)) {
            return driverName;
        }
        java.lang.String driverName2 = driverName.trim();
        int firstWordEnd = driverName2.indexOf(" ");
        return firstWordEnd != -1 ? driverName2.substring(0, firstWordEnd) : driverName2;
    }

    public java.lang.String getDriverEmail() {
        return this.mDriverProfilePreferences.driver_email;
    }

    public java.lang.String getDeviceName() {
        return this.mDriverProfilePreferences.device_name;
    }

    public java.lang.String getCarMake() {
        return this.mDriverProfilePreferences.car_make;
    }

    public java.lang.String getCarModel() {
        return this.mDriverProfilePreferences.car_model;
    }

    public java.lang.String getCarYear() {
        return this.mDriverProfilePreferences.car_year;
    }

    public com.navdy.service.library.events.preferences.DriverProfilePreferences.DisplayFormat getDisplayFormat() {
        return this.mDriverProfilePreferences.display_format;
    }

    public com.navdy.service.library.events.preferences.DriverProfilePreferences.DialLongPressAction getLongPressAction() {
        if (com.navdy.hud.app.ui.component.UISettings.isLongPressActionPlaceSearch()) {
            return com.navdy.service.library.events.preferences.DriverProfilePreferences.DialLongPressAction.DIAL_LONG_PRESS_PLACE_SEARCH;
        }
        return this.mDriverProfilePreferences.dial_long_press_action;
    }

    public com.navdy.service.library.events.preferences.DriverProfilePreferences.FeatureMode getFeatureMode() {
        return this.mDriverProfilePreferences.feature_mode;
    }

    public com.navdy.service.library.events.preferences.DriverProfilePreferences.UnitSystem getUnitSystem() {
        return this.mDriverProfilePreferences.unit_system;
    }

    public java.util.Locale getLocale() {
        if (this.mLocale != null) {
            return this.mLocale;
        }
        return java.util.Locale.getDefault();
    }

    private void setLocale() {
        try {
            this.mLocale = null;
            java.lang.String localeStr = this.mDriverProfilePreferences.locale;
            if (!android.text.TextUtils.isEmpty(localeStr)) {
                java.util.Locale locale = com.navdy.hud.app.profile.HudLocale.getLocaleForID(localeStr);
                sLogger.v("locale string:" + localeStr + " locale:" + locale + " profile:" + getProfileName() + " email:" + getDriverEmail());
                if (android.text.TextUtils.isEmpty(locale.getISO3Language())) {
                    sLogger.v("locale no language");
                } else {
                    this.mLocale = locale;
                }
            } else {
                sLogger.v("no locale string");
            }
        } catch (Throwable t) {
            sLogger.e("setLocale", t);
        }
    }

    public boolean isTrafficEnabled() {
        return this.mTrafficEnabled;
    }

    public void setTrafficEnabled(boolean b) {
        this.mTrafficEnabled = b;
    }

    private void setTraffic() {
        this.mTrafficEnabled = true;
    }

    public boolean isAutoOnEnabled() {
        return ((java.lang.Boolean) com.squareup.wire.Wire.get(this.mDriverProfilePreferences.auto_on_enabled, com.navdy.service.library.events.preferences.DriverProfilePreferences.DEFAULT_AUTO_ON_ENABLED)).booleanValue();
    }

    public com.navdy.service.library.events.preferences.DriverProfilePreferences.ObdScanSetting getObdScanSetting() {
        return this.mDriverProfilePreferences.obdScanSetting;
    }

    public long getObdBlacklistModificationTime() {
        if (this.mDriverProfilePreferences.obdBlacklistLastModified != null) {
            return this.mDriverProfilePreferences.obdBlacklistLastModified.longValue();
        }
        return 0;
    }

    public boolean isLimitBandwidthModeOn() {
        return java.lang.Boolean.TRUE.equals(this.mDriverProfilePreferences.limit_bandwidth);
    }

    public boolean isProfilePublic() {
        return java.lang.Boolean.TRUE.equals(this.mDriverProfilePreferences.profile_is_public);
    }

    public android.graphics.Bitmap getDriverImage() {
        return this.mDriverImage;
    }

    public java.io.File getDriverImageFile() {
        return new java.io.File(this.mPreferencesDirectory, DRIVER_PROFILE_IMAGE);
    }

    private void removeDriverImage() {
        this.mDriverImage = null;
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.profile.DriverProfile.Anon2(), 1);
    }

    public java.lang.String getProfileName() {
        return this.mProfileName;
    }

    public java.io.File getPlacesImageDir() {
        return this.mPlacesImageDirectory;
    }

    public java.io.File getContactsImageDir() {
        return this.mContactsImageDirectory;
    }

    public java.io.File getMusicImageDir() {
        return this.mMusicImageDirectory;
    }

    public java.io.File getPreferencesDirectory() {
        return this.mPreferencesDirectory;
    }

    public boolean isDefaultProfile() {
        return com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getDriverProfileManager().isDefaultProfile(this);
    }

    public java.lang.String toString() {
        return "DriverProfile{mProfileName='" + this.mProfileName + '\'' + '}';
    }

    /* access modifiers changed from: 0000 */
    public void copy(com.navdy.hud.app.profile.DriverProfile profile) {
        this.mDriverProfilePreferences = new com.navdy.service.library.events.preferences.DriverProfilePreferences.Builder(profile.mDriverProfilePreferences).build();
        writeMessageToFile(this.mDriverProfilePreferences, DRIVER_PROFILE_PREFERENCES);
        this.mNavigationPreferences = new com.navdy.service.library.events.preferences.NavigationPreferences.Builder(profile.mNavigationPreferences).build();
        writeMessageToFile(this.mNavigationPreferences, NAVIGATION_PREFERENCES);
        this.mInputPreferences = new com.navdy.service.library.events.preferences.InputPreferences.Builder(profile.mInputPreferences).build();
        writeMessageToFile(this.mInputPreferences, INPUT_PREFERENCES);
        this.mSpeakerPreferences = new com.navdy.service.library.events.preferences.DisplaySpeakerPreferences.Builder(profile.mSpeakerPreferences).build();
        writeMessageToFile(this.mSpeakerPreferences, SPEAKER_PREFERENCES);
        this.mNotificationPreferences = new com.navdy.service.library.events.preferences.NotificationPreferences.Builder(profile.mNotificationPreferences).build();
        writeMessageToFile(this.mNotificationPreferences, NOTIFICATION_PREFERENCES);
        this.mLocalPreferences = new com.navdy.service.library.events.preferences.LocalPreferences.Builder(profile.mLocalPreferences).build();
        writeMessageToFile(this.mLocalPreferences, LOCAL_PREFERENCES);
        sLogger.v("copy done:" + this.mPreferencesDirectory);
    }
}
