package com.navdy.hud.app.profile;

public class NotificationSettings {
    public static final java.lang.String APP_ID_CALENDAR = "com.apple.mobilecal";
    public static final java.lang.String APP_ID_MAIL = "com.apple.mobilemail";
    public static final java.lang.String APP_ID_PHONE = "com.apple.mobilephone";
    public static final java.lang.String APP_ID_REMINDERS = "com.apple.reminders";
    public static final java.lang.String APP_ID_SMS = "com.apple.MobileSMS";
    private static com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.profile.NotificationSettings.class);
    private java.util.Map<java.lang.String, java.lang.Boolean> enabledApps = new java.util.HashMap();
    private final java.lang.Object lock = new java.lang.Object();

    public NotificationSettings(com.navdy.service.library.events.preferences.NotificationPreferences preferences) {
        update(preferences);
    }

    public void update(com.navdy.service.library.events.preferences.NotificationPreferences preferences) {
        synchronized (this.lock) {
            this.enabledApps.clear();
            if (!(preferences == null || preferences.settings == null)) {
                for (com.navdy.service.library.events.notification.NotificationSetting setting : preferences.settings) {
                    this.enabledApps.put(setting.app, setting.enabled);
                }
            }
        }
    }

    public java.lang.Boolean enabled(java.lang.String appId) {
        java.lang.Boolean setting;
        synchronized (this.lock) {
            setting = (java.lang.Boolean) this.enabledApps.get(appId);
        }
        return setting;
    }

    public java.lang.Boolean enabled(com.navdy.ancs.AppleNotification notification) {
        return enabled(notification.getAppId());
    }

    public java.util.List<java.lang.String> enabledApps() {
        java.util.List<java.lang.String> result = new java.util.ArrayList<>();
        synchronized (this.lock) {
            for (java.util.Map.Entry<java.lang.String, java.lang.Boolean> entry : this.enabledApps.entrySet()) {
                if (java.lang.Boolean.TRUE.equals(entry.getValue())) {
                    result.add(entry.getKey());
                }
            }
        }
        return result;
    }
}
