package com.navdy.hud.app.profile;

public class HudLocale {
    public static final java.lang.String DEFAULT_LANGUAGE = "en";
    public static final java.lang.String DEFAULT_LOCALE_ID = "en_US";
    private static final java.lang.String LOCALE_SEPARATOR = "_";
    private static final java.lang.String LOCALE_TOAST_ID = "#locale#toast";
    private static java.lang.String MAP_ENGINE_PROCESS_NAME = "global.Here.Map.Service.v3";
    private static final java.lang.String SELECTED_LOCALE = "Locale.Helper.Selected.Language";
    private static final java.lang.String TAG = "[HUD-locale]";
    private static final java.util.HashSet<java.lang.String> hudLanguages = new java.util.HashSet<>();

    static class Anon1 implements com.navdy.hud.app.framework.toast.IToastCallback {
        android.animation.ObjectAnimator animator;
        final /* synthetic */ android.content.res.Resources val$resources;

        /* renamed from: com.navdy.hud.app.profile.HudLocale$Anon1$Anon1 reason: collision with other inner class name */
        class C0030Anon1 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
            C0030Anon1() {
            }

            public void onAnimationEnd(android.animation.Animator animation) {
                if (com.navdy.hud.app.profile.HudLocale.Anon1.this.animator != null) {
                    com.navdy.hud.app.profile.HudLocale.Anon1.this.animator.setStartDelay(33);
                    com.navdy.hud.app.profile.HudLocale.Anon1.this.animator.start();
                }
            }
        }

        Anon1(android.content.res.Resources resources) {
            this.val$resources = resources;
        }

        public void onStart(com.navdy.hud.app.view.ToastView view) {
            com.navdy.hud.app.ui.component.ConfirmationLayout lyt = view.getView();
            android.view.ViewGroup.MarginLayoutParams layoutParams = (android.view.ViewGroup.MarginLayoutParams) lyt.screenImage.getLayoutParams();
            layoutParams.width = this.val$resources.getDimensionPixelSize(com.navdy.hud.app.R.dimen.locale_change_icon_size);
            layoutParams.height = layoutParams.width;
            lyt.title1.setVisibility(8);
            lyt.title3.setVisibility(8);
            lyt.title4.setVisibility(8);
            ((android.widget.FrameLayout.LayoutParams) lyt.screenImage.getLayoutParams()).gravity = 17;
            lyt.findViewById(com.navdy.hud.app.R.id.infoContainer).setPadding(0, 0, 0, 0);
            if (this.animator == null) {
                this.animator = android.animation.ObjectAnimator.ofFloat(lyt.screenImage, android.view.View.ROTATION, new float[]{360.0f});
                this.animator.setDuration(500);
                this.animator.setInterpolator(new android.view.animation.AccelerateDecelerateInterpolator());
                this.animator.addListener(new com.navdy.hud.app.profile.HudLocale.Anon1.C0030Anon1());
            }
            if (!this.animator.isRunning()) {
                this.animator.start();
            }
        }

        public void onStop() {
            if (this.animator != null) {
                this.animator.removeAllListeners();
                this.animator.cancel();
            }
        }

        public boolean onKey(com.navdy.hud.app.manager.InputManager.CustomKeyEvent event) {
            return false;
        }

        public void executeChoiceItem(int pos, int id) {
        }
    }

    static class Anon2 implements java.lang.Runnable {
        Anon2() {
        }

        public void run() {
            com.navdy.hud.app.profile.HudLocale.killSelfAndMapEngine();
        }
    }

    static {
        hudLanguages.add(DEFAULT_LANGUAGE);
        hudLanguages.add("fr");
        hudLanguages.add("de");
        hudLanguages.add("it");
        hudLanguages.add("es");
    }

    public static boolean isLocaleSupported(java.util.Locale locale) {
        if (locale == null) {
            return false;
        }
        return hudLanguages.contains(getBaseLanguage(locale.getLanguage()));
    }

    public static android.content.Context onAttach(android.content.Context context) {
        return setLocale(context, getCurrentLocale(context));
    }

    private static android.content.Context setLocale(android.content.Context context, java.util.Locale locale) {
        android.util.Log.e(TAG, "setLocale [" + locale + "]");
        if (android.os.Build.VERSION.SDK_INT >= 24) {
            return updateResources(context, locale);
        }
        return updateResourcesLegacy(context, locale);
    }

    @android.annotation.TargetApi(24)
    private static android.content.Context updateResources(android.content.Context context, java.util.Locale locale) {
        java.util.Locale.setDefault(locale);
        android.content.res.Configuration configuration = context.getResources().getConfiguration();
        configuration.setLocale(locale);
        return context.createConfigurationContext(configuration);
    }

    private static android.content.Context updateResourcesLegacy(android.content.Context context, java.util.Locale locale) {
        java.util.Locale.setDefault(locale);
        android.content.res.Resources resources = context.getResources();
        android.content.res.Configuration configuration = resources.getConfiguration();
        configuration.locale = locale;
        resources.updateConfiguration(configuration, resources.getDisplayMetrics());
        return context;
    }

    public static java.util.Locale getCurrentLocale(android.content.Context context) {
        try {
            java.lang.String localeId = android.preference.PreferenceManager.getDefaultSharedPreferences(context).getString(SELECTED_LOCALE, "en_US");
            if (localeId.equals(DEFAULT_LANGUAGE)) {
                localeId = "en_US";
            }
            return getLocaleForID(localeId);
        } catch (Throwable t) {
            android.util.Log.e(TAG, "getCurrentLocale", t);
            return getLocaleForID("en_US");
        }
    }

    private static void setCurrentLocale(android.content.Context context, java.util.Locale locale) {
        android.content.SharedPreferences.Editor editor = android.preference.PreferenceManager.getDefaultSharedPreferences(context).edit();
        editor.putString(SELECTED_LOCALE, locale.toString());
        editor.commit();
    }

    public static boolean switchLocale(android.content.Context context, java.util.Locale locale) {
        try {
            com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
            if (!isLocaleSupported(locale)) {
                android.util.Log.e(TAG, "switchLocale hud does not support locale:" + locale);
                return false;
            }
            java.util.Locale currentLocale = getCurrentLocale(context);
            if (locale.equals(currentLocale)) {
                android.util.Log.e(TAG, "switchLocale language not changed:" + locale);
                return false;
            }
            setCurrentLocale(context, locale);
            android.util.Log.e(TAG, "switchLocale changed language from [" + currentLocale + "] to [" + locale + "], restarting hud app");
            return true;
        } catch (Throwable t) {
            android.util.Log.e(TAG, "switchLocale", t);
            return false;
        }
    }

    public static void showLocaleNotSupportedToast(java.lang.String language) {
        dismissToast();
        android.content.res.Resources resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        android.os.Bundle bundle = new android.os.Bundle();
        bundle.putBoolean(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_DEFAULT_CHOICE, true);
        bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_IMAGE, com.navdy.hud.app.R.drawable.icon_not_supported);
        bundle.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_2, resources.getString(com.navdy.hud.app.R.string.locale_not_supported, new java.lang.Object[]{language}));
        bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_2_STYLE, com.navdy.hud.app.R.style.Glances_1);
        com.navdy.hud.app.framework.toast.ToastManager.getInstance().addToast(new com.navdy.hud.app.framework.toast.ToastManager.ToastParams(LOCALE_TOAST_ID, bundle, null, true, true));
    }

    public static void dismissToast() {
        com.navdy.hud.app.framework.toast.ToastManager.getInstance().dismissToast(LOCALE_TOAST_ID);
    }

    public static void showLocaleChangeToast() {
        com.navdy.hud.app.framework.toast.ToastManager toastManager = com.navdy.hud.app.framework.toast.ToastManager.getInstance();
        android.content.res.Resources resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        android.os.Bundle bundle = new android.os.Bundle();
        toastManager.clearAllPendingToast();
        toastManager.disableToasts(false);
        toastManager.dismissCurrentToast(LOCALE_TOAST_ID);
        bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_IMAGE, com.navdy.hud.app.R.drawable.icon_sm_spinner);
        bundle.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_2, resources.getString(com.navdy.hud.app.R.string.updating_language));
        bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_2_STYLE, com.navdy.hud.app.R.style.Glances_1);
        toastManager.addToast(new com.navdy.hud.app.framework.toast.ToastManager.ToastParams(LOCALE_TOAST_ID, bundle, new com.navdy.hud.app.profile.HudLocale.Anon1(resources), true, true));
        new android.os.Handler(android.os.Looper.getMainLooper()).postDelayed(new com.navdy.hud.app.profile.HudLocale.Anon2(), 2000);
    }

    /* access modifiers changed from: private */
    public static void killSelfAndMapEngine() {
        java.util.Iterator it = ((android.app.ActivityManager) com.navdy.hud.app.HudApplication.getAppContext().getSystemService("activity")).getRunningAppProcesses().iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            android.app.ActivityManager.RunningAppProcessInfo processInfo = (android.app.ActivityManager.RunningAppProcessInfo) it.next();
            if (processInfo.processName.equals(MAP_ENGINE_PROCESS_NAME)) {
                android.os.Process.killProcess(processInfo.pid);
                break;
            }
        }
        java.lang.System.exit(0);
    }

    public static java.lang.String getBaseLanguage(java.lang.String language) {
        int index = 0;
        while (index < language.length()) {
            char c = language.charAt(index);
            if (c == '_' || c == '-') {
                break;
            }
            index++;
        }
        if (index <= 0 || index >= language.length()) {
            return language;
        }
        return language.substring(0, index);
    }

    public static java.util.Locale getLocaleForID(java.lang.String localeID) {
        if (android.text.TextUtils.isEmpty(localeID)) {
            return null;
        }
        int index = localeID.indexOf("_");
        if (index < 0) {
            return new java.util.Locale(localeID);
        }
        java.lang.String language = localeID.substring(0, index);
        java.lang.String localeID2 = localeID.substring(index + 1);
        int index2 = localeID2.indexOf("_");
        if (index2 > 0) {
            return new java.util.Locale(language, localeID2.substring(0, index2), localeID2.substring(index2 + 1));
        }
        return new java.util.Locale(language, localeID2);
    }
}
