package com.navdy.hud.app.bluetooth.pbap;

public class PBAPClientManager {
    private static final int CHECK_PBAP_CONNECTION_HANG_INTERVAL = 60000;
    private static final int CHECK_PBAP_PULL_PHONEBOOK_HANG_INTERVAL = 45000;
    private static final int INITIAL_CONNECTION_DELAY = 30000;
    public static final int MAX_RECENT_CALLS = 30;
    private static final int TYPE_HOME = 1;
    private static final int TYPE_MOBILE = 2;
    private static final int TYPE_WORK = 3;
    private static final int TYPE_WORK_MOBILE = 17;
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.bluetooth.pbap.PBAPClientManager.class);
    private static final com.navdy.hud.app.bluetooth.pbap.PBAPClientManager singleton = new com.navdy.hud.app.bluetooth.pbap.PBAPClientManager();
    private android.bluetooth.BluetoothAdapter bluetoothAdapter;
    /* access modifiers changed from: private */
    public java.lang.Runnable checkPbapConnectionHang = new com.navdy.hud.app.bluetooth.pbap.PBAPClientManager.Anon2();
    /* access modifiers changed from: private */
    public android.os.Handler handler;
    private android.os.Handler.Callback handlerCallback = new com.navdy.hud.app.bluetooth.pbap.PBAPClientManager.Anon1();
    private android.os.HandlerThread handlerThread;
    private long lastSyncAttemptTime;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient pbapClient;
    private com.navdy.hud.app.manager.RemoteDeviceManager remoteDeviceManager = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance();
    private java.lang.Runnable stopPbapClientRunnable = new com.navdy.hud.app.bluetooth.pbap.PBAPClientManager.Anon4();
    /* access modifiers changed from: private */
    public java.lang.Runnable syncRunnable = new com.navdy.hud.app.bluetooth.pbap.PBAPClientManager.Anon3();

    class Anon1 implements android.os.Handler.Callback {
        Anon1() {
        }

        public boolean handleMessage(android.os.Message msg) {
            try {
                switch (msg.what) {
                    case 1:
                        com.navdy.hud.app.bluetooth.pbap.PBAPClientManager.sLogger.v("pbap client set phone book done");
                        break;
                    case 2:
                        com.navdy.hud.app.bluetooth.pbap.PBAPClientManager.sLogger.v("pbap client pull phone book done");
                        com.navdy.hud.app.bluetooth.pbap.PBAPClientManager.this.handler.removeCallbacks(com.navdy.hud.app.bluetooth.pbap.PBAPClientManager.this.checkPbapConnectionHang);
                        com.navdy.hud.app.bluetooth.pbap.PBAPClientManager.this.buildRecentCallList((java.util.ArrayList) msg.obj);
                        break;
                    case 3:
                        com.navdy.hud.app.bluetooth.pbap.PBAPClientManager.sLogger.v("pbap client vcard listing done");
                        break;
                    case 4:
                        com.navdy.hud.app.bluetooth.pbap.PBAPClientManager.sLogger.v("pbap client pull vcard entry done");
                        break;
                    case 5:
                        com.navdy.hud.app.bluetooth.pbap.PBAPClientManager.sLogger.v("pbap client phone book size done:" + msg.arg1);
                        break;
                    case 6:
                        com.navdy.hud.app.bluetooth.pbap.PBAPClientManager.sLogger.v("pbap client vcard listing size done:" + msg.arg1);
                        break;
                    case com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient.EVENT_SET_PHONE_BOOK_ERROR /*101*/:
                        com.navdy.hud.app.bluetooth.pbap.PBAPClientManager.sLogger.e("pbap client set phone book error");
                        break;
                    case 102:
                        com.navdy.hud.app.bluetooth.pbap.PBAPClientManager.sLogger.e("pbap client pull phone book error");
                        break;
                    case com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient.EVENT_PULL_VCARD_LISTING_ERROR /*103*/:
                        com.navdy.hud.app.bluetooth.pbap.PBAPClientManager.sLogger.e("pbap client vcard listing error");
                        break;
                    case com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient.EVENT_PULL_VCARD_ENTRY_ERROR /*104*/:
                        com.navdy.hud.app.bluetooth.pbap.PBAPClientManager.sLogger.e("pbap client vcard entry error");
                        break;
                    case 105:
                        com.navdy.hud.app.bluetooth.pbap.PBAPClientManager.sLogger.e("pbap client phone book size error");
                        break;
                    case com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient.EVENT_PULL_VCARD_LISTING_SIZE_ERROR /*106*/:
                        com.navdy.hud.app.bluetooth.pbap.PBAPClientManager.sLogger.e("pbap client vcard listing size error");
                        break;
                    case 201:
                        com.navdy.hud.app.bluetooth.pbap.PBAPClientManager.sLogger.i("pbap client connected, pulling phonebook [telecom/cch.vcf]");
                        com.navdy.hud.app.bluetooth.pbap.PBAPClientManager.this.handler.removeCallbacks(com.navdy.hud.app.bluetooth.pbap.PBAPClientManager.this.checkPbapConnectionHang);
                        com.navdy.hud.app.bluetooth.pbap.PBAPClientManager.this.pbapClient.pullPhoneBook(com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient.CCH_PATH);
                        com.navdy.hud.app.bluetooth.pbap.PBAPClientManager.this.handler.postDelayed(com.navdy.hud.app.bluetooth.pbap.PBAPClientManager.this.checkPbapConnectionHang, 45000);
                        break;
                    case 202:
                        com.navdy.hud.app.bluetooth.pbap.PBAPClientManager.sLogger.i("pbap client dis-connected");
                        break;
                    case 203:
                        com.navdy.hud.app.bluetooth.pbap.PBAPClientManager.sLogger.w("pbap client auth-requested");
                        break;
                    case 204:
                        com.navdy.hud.app.bluetooth.pbap.PBAPClientManager.sLogger.w("pbap client auth-timeout");
                        break;
                }
            } catch (Throwable t) {
                com.navdy.hud.app.bluetooth.pbap.PBAPClientManager.sLogger.e(t);
            }
            return false;
        }
    }

    class Anon2 implements java.lang.Runnable {
        Anon2() {
        }

        public void run() {
            try {
                com.navdy.hud.app.bluetooth.pbap.PBAPClientManager.sLogger.v("pbap hang detected, reattempting...");
                com.navdy.hud.app.bluetooth.pbap.PBAPClientManager.this.stopPbapClient();
                com.navdy.hud.app.bluetooth.pbap.PBAPClientManager.this.handler.removeCallbacks(com.navdy.hud.app.bluetooth.pbap.PBAPClientManager.this.syncRunnable);
                com.navdy.hud.app.bluetooth.pbap.PBAPClientManager.this.handler.post(com.navdy.hud.app.bluetooth.pbap.PBAPClientManager.this.syncRunnable);
            } catch (Throwable t) {
                com.navdy.hud.app.bluetooth.pbap.PBAPClientManager.sLogger.e(t);
            }
        }
    }

    class Anon3 implements java.lang.Runnable {
        Anon3() {
        }

        public void run() {
            com.navdy.hud.app.bluetooth.pbap.PBAPClientManager.this.syncRecentCalls();
        }
    }

    class Anon4 implements java.lang.Runnable {
        Anon4() {
        }

        public void run() {
            com.navdy.hud.app.bluetooth.pbap.PBAPClientManager.this.stopPbapClient();
        }
    }

    public static com.navdy.hud.app.bluetooth.pbap.PBAPClientManager getInstance() {
        return singleton;
    }

    public PBAPClientManager() {
        this.remoteDeviceManager.getBus().register(this);
        this.handlerThread = new android.os.HandlerThread("HudPBAPClientManager");
        this.handlerThread.start();
        this.handler = new android.os.Handler(this.handlerThread.getLooper(), this.handlerCallback);
        android.bluetooth.BluetoothManager bluetoothManager = (android.bluetooth.BluetoothManager) com.navdy.hud.app.HudApplication.getAppContext().getSystemService("bluetooth");
        if (bluetoothManager != null) {
            this.bluetoothAdapter = bluetoothManager.getAdapter();
        }
        this.handler.removeCallbacks(this.syncRunnable);
        this.handler.postDelayed(this.syncRunnable, 30000);
    }

    @com.squareup.otto.Subscribe
    public void onConnectionStatusChange(com.navdy.service.library.events.connection.ConnectionStateChange event) {
        switch (event.state) {
            case CONNECTION_LINK_LOST:
                this.handler.post(this.stopPbapClientRunnable);
                com.navdy.hud.app.framework.recentcall.RecentCallManager.getInstance().clearRecentCalls();
                com.navdy.hud.app.framework.contacts.FavoriteContactsManager.getInstance().clearFavoriteContacts();
                return;
            default:
                return;
        }
    }

    @com.squareup.otto.Subscribe
    public void onDeviceSyncRequired(com.navdy.hud.app.service.ConnectionHandler.DeviceSyncEvent event) {
        boolean fullSync = true;
        if (event.amount != 1) {
            fullSync = false;
        }
        if (fullSync) {
            this.handler.removeCallbacks(this.syncRunnable);
            sLogger.v("trigger PBAP download");
            this.handler.post(this.syncRunnable);
        }
        com.navdy.hud.app.framework.contacts.FavoriteContactsManager.getInstance().syncFavoriteContacts(fullSync);
    }

    @com.squareup.otto.Subscribe
    public void onDriverProfileChanged(com.navdy.hud.app.event.DriverProfileChanged event) {
        com.navdy.hud.app.framework.recentcall.RecentCallManager recentCallManager = com.navdy.hud.app.framework.recentcall.RecentCallManager.getInstance();
        recentCallManager.clearContactLookupMap();
        recentCallManager.buildRecentCalls();
        com.navdy.hud.app.framework.contacts.FavoriteContactsManager.getInstance().buildFavoriteContacts();
    }

    private void startPbapClient(java.lang.String btAddress) {
        try {
            if (this.pbapClient == null) {
                sLogger.v("startPbapClient");
                this.pbapClient = new com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient(this.bluetoothAdapter.getRemoteDevice(btAddress), this.handler);
                this.pbapClient.connect();
                sLogger.v("started pbap client");
            }
        } catch (Throwable t) {
            sLogger.e("could not start pbap client", t);
        }
    }

    /* access modifiers changed from: private */
    public void stopPbapClient() {
        try {
            if (this.pbapClient != null) {
                sLogger.v("stopPbapClient");
                this.pbapClient.disconnect();
                sLogger.v("stopped pbap client");
                this.pbapClient = null;
                this.lastSyncAttemptTime = 0;
                this.handler.removeCallbacks(this.checkPbapConnectionHang);
            }
        } catch (Throwable t) {
            sLogger.e("error stopping pbap client", t);
        } finally {
            this.pbapClient = null;
            this.lastSyncAttemptTime = 0;
            this.handler.removeCallbacks(this.checkPbapConnectionHang);
        }
    }

    public void syncRecentCalls() {
        try {
            sLogger.v("syncRecentCalls");
            if (this.lastSyncAttemptTime > 0) {
                sLogger.v("syncRecentCalls in progress");
                return;
            }
            this.handler.removeCallbacks(this.checkPbapConnectionHang);
            com.navdy.service.library.events.DeviceInfo deviceInfo = this.remoteDeviceManager.getRemoteDeviceInfo();
            if (deviceInfo == null) {
                sLogger.v("no remote device, bailing out");
                return;
            }
            java.lang.String btAddress = new com.navdy.service.library.device.NavdyDeviceId(deviceInfo.deviceId).getBluetoothAddress();
            if (btAddress == null) {
                sLogger.i("BT Address n/a deviceId:" + deviceInfo.deviceId);
                return;
            }
            stopPbapClient();
            this.lastSyncAttemptTime = android.os.SystemClock.elapsedRealtime();
            sLogger.v("syncRecentCalls-attempting to connect");
            startPbapClient(btAddress);
            this.handler.postDelayed(this.checkPbapConnectionHang, 60000);
        } catch (Throwable t) {
            sLogger.e("syncRecentCalls", t);
            stopPbapClient();
        }
    }

    /* access modifiers changed from: private */
    public void buildRecentCallList(java.util.ArrayList<com.navdy.hud.app.bluetooth.vcard.VCardEntry> list) {
        try {
            sLogger.v("recent calls vcard-list size[" + list.size() + "]");
            boolean verbose = !com.navdy.hud.app.util.DeviceUtil.isUserBuild();
            java.util.HashMap<java.lang.Long, com.navdy.hud.app.framework.recentcall.RecentCall> callMap = new java.util.LinkedHashMap<>();
            com.google.i18n.phonenumbers.PhoneNumberUtil phoneNumberUtil = com.google.i18n.phonenumbers.PhoneNumberUtil.getInstance();
            java.lang.String currentCountry = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentLocale().getCountry();
            java.util.Iterator it = list.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                com.navdy.hud.app.bluetooth.vcard.VCardEntry entry = (com.navdy.hud.app.bluetooth.vcard.VCardEntry) it.next();
                if (verbose) {
                    sLogger.v("vcard entry:" + entry);
                }
                java.util.List<com.navdy.hud.app.bluetooth.vcard.VCardEntry.PhoneData> phoneData = entry.getPhoneList();
                java.lang.String phoneNumber = null;
                int phoneNumberType = 0;
                if (phoneData != null && phoneData.size() > 0) {
                    java.util.Iterator it2 = phoneData.iterator();
                    while (true) {
                        if (!it2.hasNext()) {
                            break;
                        }
                        com.navdy.hud.app.bluetooth.vcard.VCardEntry.PhoneData data = (com.navdy.hud.app.bluetooth.vcard.VCardEntry.PhoneData) it2.next();
                        java.lang.String number = data.getNumber();
                        if (!android.text.TextUtils.isEmpty(number)) {
                            phoneNumber = number;
                            phoneNumberType = data.getType();
                            break;
                        }
                    }
                }
                if (phoneNumber == null) {
                    sLogger.v("no phone number in vcard");
                } else {
                    try {
                        long nPhoneNum = phoneNumberUtil.parse(phoneNumber, currentCountry).getNationalNumber();
                        com.navdy.hud.app.framework.recentcall.RecentCall call = (com.navdy.hud.app.framework.recentcall.RecentCall) callMap.get(java.lang.Long.valueOf(nPhoneNum));
                        if (call == null || android.text.TextUtils.isEmpty(call.name)) {
                            java.lang.String displayName = entry.getDisplayName();
                            if (!com.navdy.hud.app.framework.contacts.ContactUtil.isDisplayNameValid(displayName, phoneNumber, nPhoneNum)) {
                                displayName = null;
                            }
                            int callType = entry.getCallType();
                            java.util.Date callTime = entry.getCallTime();
                            if (callTime == null) {
                                sLogger.v("ignoring spam number:" + phoneNumber);
                            } else {
                                com.navdy.hud.app.framework.recentcall.RecentCall recentCall = new com.navdy.hud.app.framework.recentcall.RecentCall(displayName, com.navdy.hud.app.framework.recentcall.RecentCall.Category.PHONE_CALL, phoneNumber, getNumberType(phoneNumberType), callTime, getCallType(callType), -1, nPhoneNum);
                                if (verbose) {
                                    sLogger.v("adding recent call:" + recentCall);
                                }
                                callMap.put(java.lang.Long.valueOf(nPhoneNum), recentCall);
                                if (callMap.size() == 30) {
                                    sLogger.v("exceeded max size");
                                    break;
                                }
                            }
                        } else if (verbose) {
                            sLogger.v("Ignoring entry with duplicate phone number: " + nPhoneNum);
                        }
                    } catch (Throwable t) {
                        sLogger.e(t);
                    }
                }
            }
            sLogger.v("recent calls map size[" + callMap.size() + "]");
            com.navdy.hud.app.profile.DriverProfile driverProfile = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile();
            java.util.ArrayList arrayList = new java.util.ArrayList();
            for (com.navdy.hud.app.framework.recentcall.RecentCall rc : callMap.values()) {
                arrayList.add(rc);
            }
            com.navdy.hud.app.storage.db.helper.RecentCallPersistenceHelper.storeRecentCalls(driverProfile.getProfileName(), arrayList, true);
        } catch (Throwable t2) {
            sLogger.e(t2);
        } finally {
            sLogger.v("buildRecentCallList: stopping client");
            stopPbapClient();
        }
    }

    private com.navdy.hud.app.framework.contacts.NumberType getNumberType(int type) {
        switch (type) {
            case 1:
                return com.navdy.hud.app.framework.contacts.NumberType.HOME;
            case 2:
                return com.navdy.hud.app.framework.contacts.NumberType.MOBILE;
            case 3:
                return com.navdy.hud.app.framework.contacts.NumberType.WORK;
            case 17:
                return com.navdy.hud.app.framework.contacts.NumberType.WORK_MOBILE;
            default:
                return com.navdy.hud.app.framework.contacts.NumberType.OTHER;
        }
    }

    private com.navdy.hud.app.framework.recentcall.RecentCall.CallType getCallType(int type) {
        switch (type) {
            case 10:
                return com.navdy.hud.app.framework.recentcall.RecentCall.CallType.INCOMING;
            case 11:
                return com.navdy.hud.app.framework.recentcall.RecentCall.CallType.OUTGOING;
            case 12:
                return com.navdy.hud.app.framework.recentcall.RecentCall.CallType.MISSED;
            default:
                return com.navdy.hud.app.framework.recentcall.RecentCall.CallType.UNNKNOWN;
        }
    }

    private void printCallMap(java.util.Collection<com.navdy.hud.app.framework.recentcall.RecentCall> calls) {
        for (com.navdy.hud.app.framework.recentcall.RecentCall call : calls) {
            sLogger.v("[" + call.category + "] " + "[" + call.number + "]" + "[" + call.numberType + "]" + "[" + call.name + "]" + "[" + call.callTime + "]" + "[" + call.callType + "]");
        }
    }
}
