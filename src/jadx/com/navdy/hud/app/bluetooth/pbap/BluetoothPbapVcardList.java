package com.navdy.hud.app.bluetooth.pbap;

class BluetoothPbapVcardList {
    /* access modifiers changed from: private */
    public final java.util.ArrayList<com.navdy.hud.app.bluetooth.vcard.VCardEntry> mCards = new java.util.ArrayList<>();

    class CardEntryHandler implements com.navdy.hud.app.bluetooth.vcard.VCardEntryHandler {
        CardEntryHandler() {
        }

        public void onStart() {
        }

        public void onEntryCreated(com.navdy.hud.app.bluetooth.vcard.VCardEntry entry) {
            com.navdy.hud.app.bluetooth.pbap.BluetoothPbapVcardList.this.mCards.add(entry);
        }

        public void onEnd() {
        }
    }

    public BluetoothPbapVcardList(java.io.InputStream in, byte format) throws java.io.IOException {
        parse(in, format);
    }

    private void parse(java.io.InputStream in, byte format) throws java.io.IOException {
        com.navdy.hud.app.bluetooth.vcard.VCardParser parser;
        if (format == 1) {
            parser = new com.navdy.hud.app.bluetooth.vcard.VCardParser_V30();
        } else {
            parser = new com.navdy.hud.app.bluetooth.vcard.VCardParser_V21();
        }
        com.navdy.hud.app.bluetooth.vcard.VCardEntryConstructor constructor = new com.navdy.hud.app.bluetooth.vcard.VCardEntryConstructor();
        com.navdy.hud.app.bluetooth.vcard.VCardEntryCounter counter = new com.navdy.hud.app.bluetooth.vcard.VCardEntryCounter();
        constructor.addEntryHandler(new com.navdy.hud.app.bluetooth.pbap.BluetoothPbapVcardList.CardEntryHandler());
        parser.addInterpreter(constructor);
        parser.addInterpreter(counter);
        try {
            parser.parse(in);
        } catch (com.navdy.hud.app.bluetooth.vcard.exception.VCardException e) {
            e.printStackTrace();
        }
    }

    public int getCount() {
        return this.mCards.size();
    }

    public java.util.ArrayList<com.navdy.hud.app.bluetooth.vcard.VCardEntry> getList() {
        return this.mCards;
    }

    public com.navdy.hud.app.bluetooth.vcard.VCardEntry getFirst() {
        return (com.navdy.hud.app.bluetooth.vcard.VCardEntry) this.mCards.get(0);
    }
}
