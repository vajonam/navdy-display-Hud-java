package com.navdy.hud.app.bluetooth.pbap;

public class BluetoothPbapCard {
    public final java.lang.String N;
    public final java.lang.String firstName;
    public final java.lang.String handle;
    public final java.lang.String lastName;
    public final java.lang.String middleName;
    public final java.lang.String prefix;
    public final java.lang.String suffix;

    public BluetoothPbapCard(java.lang.String handle2, java.lang.String name) {
        java.lang.String str = null;
        this.handle = handle2;
        this.N = name;
        java.lang.String[] parsedName = name.split(";", 5);
        this.lastName = parsedName.length < 1 ? null : parsedName[0];
        this.firstName = parsedName.length < 2 ? null : parsedName[1];
        this.middleName = parsedName.length < 3 ? null : parsedName[2];
        this.prefix = parsedName.length < 4 ? null : parsedName[3];
        if (parsedName.length >= 5) {
            str = parsedName[4];
        }
        this.suffix = str;
    }

    public java.lang.String toString() {
        org.json.JSONObject json = new org.json.JSONObject();
        try {
            json.put("handle", this.handle);
            json.put(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_N, this.N);
            json.put("lastName", this.lastName);
            json.put("firstName", this.firstName);
            json.put("middleName", this.middleName);
            json.put("prefix", this.prefix);
            json.put("suffix", this.suffix);
        } catch (org.json.JSONException e) {
        }
        return json.toString();
    }

    public static java.lang.String jsonifyVcardEntry(com.navdy.hud.app.bluetooth.vcard.VCardEntry vcard) {
        org.json.JSONObject json = new org.json.JSONObject();
        try {
            com.navdy.hud.app.bluetooth.vcard.VCardEntry.NameData name = vcard.getNameData();
            json.put("formatted", name.getFormatted());
            json.put("family", name.getFamily());
            json.put("given", name.getGiven());
            json.put("middle", name.getMiddle());
            json.put("prefix", name.getPrefix());
            json.put("suffix", name.getSuffix());
        } catch (org.json.JSONException e) {
        }
        try {
            org.json.JSONArray jsonPhones = new org.json.JSONArray();
            java.util.List<com.navdy.hud.app.bluetooth.vcard.VCardEntry.PhoneData> phones = vcard.getPhoneList();
            if (phones != null) {
                for (com.navdy.hud.app.bluetooth.vcard.VCardEntry.PhoneData phone : phones) {
                    org.json.JSONObject jsonPhone = new org.json.JSONObject();
                    jsonPhone.put("type", phone.getType());
                    jsonPhone.put("number", phone.getNumber());
                    jsonPhone.put("label", phone.getLabel());
                    jsonPhone.put("is_primary", phone.isPrimary());
                    jsonPhones.put(jsonPhone);
                }
                json.put("phones", jsonPhones);
            }
        } catch (org.json.JSONException e2) {
        }
        try {
            org.json.JSONArray jsonEmails = new org.json.JSONArray();
            java.util.List<com.navdy.hud.app.bluetooth.vcard.VCardEntry.EmailData> emails = vcard.getEmailList();
            if (emails != null) {
                for (com.navdy.hud.app.bluetooth.vcard.VCardEntry.EmailData email : emails) {
                    org.json.JSONObject jsonEmail = new org.json.JSONObject();
                    jsonEmail.put("type", email.getType());
                    jsonEmail.put("address", email.getAddress());
                    jsonEmail.put("label", email.getLabel());
                    jsonEmail.put("is_primary", email.isPrimary());
                    jsonEmails.put(jsonEmail);
                }
                json.put("emails", jsonEmails);
            }
        } catch (org.json.JSONException e3) {
        }
        return json.toString();
    }
}
