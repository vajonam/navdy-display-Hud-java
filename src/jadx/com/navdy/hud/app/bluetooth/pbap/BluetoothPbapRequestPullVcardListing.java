package com.navdy.hud.app.bluetooth.pbap;

final class BluetoothPbapRequestPullVcardListing extends com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequest {
    private static final java.lang.String TAG = "BTPbapReqPullVcardL";
    private static final java.lang.String TYPE = "x-bt/vcard-listing";
    private int mNewMissedCalls = -1;
    private com.navdy.hud.app.bluetooth.pbap.BluetoothPbapVcardListing mResponse = null;

    public BluetoothPbapRequestPullVcardListing(java.lang.String folder, byte order, byte searchAttr, java.lang.String searchVal, int maxListCount, int listStartOffset) {
        if (maxListCount < 0 || maxListCount > 65535) {
            throw new java.lang.IllegalArgumentException("maxListCount should be [0..65535]");
        } else if (listStartOffset < 0 || listStartOffset > 65535) {
            throw new java.lang.IllegalArgumentException("listStartOffset should be [0..65535]");
        } else {
            if (folder == null) {
                folder = "";
            }
            this.mHeaderSet.setHeader(1, folder);
            this.mHeaderSet.setHeader(66, TYPE);
            com.navdy.hud.app.bluetooth.pbap.utils.ObexAppParameters oap = new com.navdy.hud.app.bluetooth.pbap.utils.ObexAppParameters();
            if (order >= 0) {
                oap.add(1, order);
            }
            if (searchVal != null) {
                oap.add(3, searchAttr);
                oap.add(2, searchVal);
            }
            if (maxListCount > 0) {
                oap.add(4, (short) maxListCount);
            }
            if (listStartOffset > 0) {
                oap.add(5, (short) listStartOffset);
            }
            oap.addToHeaderSet(this.mHeaderSet);
        }
    }

    /* access modifiers changed from: protected */
    public void readResponse(java.io.InputStream stream) throws java.io.IOException {
        android.util.Log.v(TAG, "readResponse");
        this.mResponse = new com.navdy.hud.app.bluetooth.pbap.BluetoothPbapVcardListing(stream);
    }

    /* access modifiers changed from: protected */
    public void readResponseHeaders(com.navdy.hud.app.bluetooth.obex.HeaderSet headerset) {
        android.util.Log.v(TAG, "readResponseHeaders");
        com.navdy.hud.app.bluetooth.pbap.utils.ObexAppParameters oap = com.navdy.hud.app.bluetooth.pbap.utils.ObexAppParameters.fromHeaderSet(headerset);
        if (oap.exists(9)) {
            this.mNewMissedCalls = oap.getByte(9);
        }
    }

    public java.util.ArrayList<com.navdy.hud.app.bluetooth.pbap.BluetoothPbapCard> getList() {
        return this.mResponse.getList();
    }

    public int getNewMissedCalls() {
        return this.mNewMissedCalls;
    }
}
