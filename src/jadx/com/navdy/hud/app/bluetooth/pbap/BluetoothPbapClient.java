package com.navdy.hud.app.bluetooth.pbap;

public class BluetoothPbapClient {
    public static final java.lang.String CCH_PATH = "telecom/cch.vcf";
    public static final int EVENT_PULL_PHONE_BOOK_DONE = 2;
    public static final int EVENT_PULL_PHONE_BOOK_ERROR = 102;
    public static final int EVENT_PULL_PHONE_BOOK_SIZE_DONE = 5;
    public static final int EVENT_PULL_PHONE_BOOK_SIZE_ERROR = 105;
    public static final int EVENT_PULL_VCARD_ENTRY_DONE = 4;
    public static final int EVENT_PULL_VCARD_ENTRY_ERROR = 104;
    public static final int EVENT_PULL_VCARD_LISTING_DONE = 3;
    public static final int EVENT_PULL_VCARD_LISTING_ERROR = 103;
    public static final int EVENT_PULL_VCARD_LISTING_SIZE_DONE = 6;
    public static final int EVENT_PULL_VCARD_LISTING_SIZE_ERROR = 106;
    public static final int EVENT_SESSION_AUTH_REQUESTED = 203;
    public static final int EVENT_SESSION_AUTH_TIMEOUT = 204;
    public static final int EVENT_SESSION_CONNECTED = 201;
    public static final int EVENT_SESSION_DISCONNECTED = 202;
    public static final int EVENT_SET_PHONE_BOOK_DONE = 1;
    public static final int EVENT_SET_PHONE_BOOK_ERROR = 101;
    public static final java.lang.String ICH_PATH = "telecom/ich.vcf";
    public static final short MAX_LIST_COUNT = -1;
    public static final java.lang.String MCH_PATH = "telecom/mch.vcf";
    public static final java.lang.String OCH_PATH = "telecom/och.vcf";
    public static final byte ORDER_BY_ALPHABETICAL = 1;
    public static final byte ORDER_BY_DEFAULT = -1;
    public static final byte ORDER_BY_INDEXED = 0;
    public static final byte ORDER_BY_PHONETIC = 2;
    public static final java.lang.String PB_PATH = "telecom/pb.vcf";
    public static final byte SEARCH_ATTR_NAME = 0;
    public static final byte SEARCH_ATTR_NUMBER = 1;
    public static final byte SEARCH_ATTR_SOUND = 2;
    public static final java.lang.String SIM_CCH_PATH = "SIM1/telecom/cch.vcf";
    public static final java.lang.String SIM_ICH_PATH = "SIM1/telecom/ich.vcf";
    public static final java.lang.String SIM_MCH_PATH = "SIM1/telecom/mch.vcf";
    public static final java.lang.String SIM_OCH_PATH = "SIM1/telecom/och.vcf";
    public static final java.lang.String SIM_PB_PATH = "SIM1/telecom/pb.vcf";
    /* access modifiers changed from: private */
    public static final java.lang.String TAG = com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient.class.getSimpleName();
    public static final long VCARD_ATTR_ADDR = 32;
    public static final long VCARD_ATTR_AGENT = 32768;
    public static final long VCARD_ATTR_BDAY = 16;
    public static final long VCARD_ATTR_CATEGORIES = 16777216;
    public static final long VCARD_ATTR_CLASS = 67108864;
    public static final long VCARD_ATTR_EMAIL = 256;
    public static final long VCARD_ATTR_FN = 2;
    public static final long VCARD_ATTR_GEO = 2048;
    public static final long VCARD_ATTR_KEY = 4194304;
    public static final long VCARD_ATTR_LABEL = 64;
    public static final long VCARD_ATTR_LOGO = 16384;
    public static final long VCARD_ATTR_MAILER = 512;
    public static final long VCARD_ATTR_N = 4;
    public static final long VCARD_ATTR_NICKNAME = 8388608;
    public static final long VCARD_ATTR_NOTE = 131072;
    public static final long VCARD_ATTR_ORG = 65536;
    public static final long VCARD_ATTR_PHOTO = 8;
    public static final long VCARD_ATTR_PROID = 33554432;
    public static final long VCARD_ATTR_REV = 262144;
    public static final long VCARD_ATTR_ROLE = 8192;
    public static final long VCARD_ATTR_SORT_STRING = 134217728;
    public static final long VCARD_ATTR_SOUND = 524288;
    public static final long VCARD_ATTR_TEL = 128;
    public static final long VCARD_ATTR_TITLE = 4096;
    public static final long VCARD_ATTR_TZ = 1024;
    public static final long VCARD_ATTR_UID = 2097152;
    public static final long VCARD_ATTR_URL = 1048576;
    public static final long VCARD_ATTR_VERSION = 1;
    public static final long VCARD_ATTR_X_IRMC_CALL_DATETIME = 268435456;
    public static final byte VCARD_TYPE_21 = 0;
    public static final byte VCARD_TYPE_30 = 1;
    private final android.os.Handler mClientHandler;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient.ConnectionState mConnectionState = com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient.ConnectionState.DISCONNECTED;
    private final com.navdy.hud.app.bluetooth.pbap.BluetoothPbapSession mSession;
    private com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient.SessionHandler mSessionHandler;

    public enum ConnectionState {
        DISCONNECTED,
        CONNECTING,
        CONNECTED,
        DISCONNECTING
    }

    private static class SessionHandler extends android.os.Handler {
        private final java.lang.ref.WeakReference<com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient> mClient;

        SessionHandler(com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient client) {
            this.mClient = new java.lang.ref.WeakReference<>(client);
        }

        public void handleMessage(android.os.Message msg) {
            android.util.Log.d(com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient.TAG, "handleMessage: what=" + msg.what);
            com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient client = (com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient) this.mClient.get();
            if (client != null) {
                switch (msg.what) {
                    case 3:
                        com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequest req = (com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequest) msg.obj;
                        if (req instanceof com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestPullPhoneBookSize) {
                            client.sendToClient(5, ((com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestPullPhoneBookSize) req).getSize());
                            return;
                        } else if (req instanceof com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestPullVcardListingSize) {
                            client.sendToClient(6, ((com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestPullVcardListingSize) req).getSize());
                            return;
                        } else if (req instanceof com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestPullPhoneBook) {
                            com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestPullPhoneBook r = (com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestPullPhoneBook) req;
                            client.sendToClient(2, r.getNewMissedCalls(), r.getList());
                            return;
                        } else if (req instanceof com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestPullVcardListing) {
                            com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestPullVcardListing r2 = (com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestPullVcardListing) req;
                            client.sendToClient(3, r2.getNewMissedCalls(), r2.getList());
                            return;
                        } else if (req instanceof com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestPullVcardEntry) {
                            client.sendToClient(4, (java.lang.Object) ((com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestPullVcardEntry) req).getVcard());
                            return;
                        } else if (req instanceof com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestSetPath) {
                            client.sendToClient(1);
                            return;
                        } else {
                            return;
                        }
                    case 4:
                        com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequest req2 = (com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequest) msg.obj;
                        if (req2 instanceof com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestPullPhoneBookSize) {
                            client.sendToClient(105);
                            return;
                        } else if (req2 instanceof com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestPullVcardListingSize) {
                            client.sendToClient(com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient.EVENT_PULL_VCARD_LISTING_SIZE_ERROR);
                            return;
                        } else if (req2 instanceof com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestPullPhoneBook) {
                            client.sendToClient(102);
                            return;
                        } else if (req2 instanceof com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestPullVcardListing) {
                            client.sendToClient(com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient.EVENT_PULL_VCARD_LISTING_ERROR);
                            return;
                        } else if (req2 instanceof com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestPullVcardEntry) {
                            client.sendToClient(com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient.EVENT_PULL_VCARD_ENTRY_ERROR);
                            return;
                        } else if (req2 instanceof com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestSetPath) {
                            client.sendToClient(com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient.EVENT_SET_PHONE_BOOK_ERROR);
                            return;
                        } else {
                            return;
                        }
                    case 5:
                        client.mConnectionState = com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient.ConnectionState.CONNECTING;
                        return;
                    case 6:
                        client.mConnectionState = com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient.ConnectionState.CONNECTED;
                        client.sendToClient(201);
                        return;
                    case 7:
                        client.mConnectionState = com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient.ConnectionState.DISCONNECTED;
                        client.sendToClient(202);
                        return;
                    case 8:
                        client.sendToClient(203);
                        return;
                    case 9:
                        client.sendToClient(204);
                        return;
                    default:
                        return;
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void sendToClient(int eventId) {
        sendToClient(eventId, 0, null);
    }

    /* access modifiers changed from: private */
    public void sendToClient(int eventId, int param) {
        sendToClient(eventId, param, null);
    }

    /* access modifiers changed from: private */
    public void sendToClient(int eventId, java.lang.Object param) {
        sendToClient(eventId, 0, param);
    }

    /* access modifiers changed from: private */
    public void sendToClient(int eventId, int param1, java.lang.Object param2) {
        this.mClientHandler.obtainMessage(eventId, param1, 0, param2).sendToTarget();
    }

    public BluetoothPbapClient(android.bluetooth.BluetoothDevice device, android.os.Handler handler) {
        if (device == null) {
            throw new java.lang.NullPointerException("BluetothDevice is null");
        }
        this.mClientHandler = handler;
        this.mSessionHandler = new com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient.SessionHandler(this);
        this.mSession = new com.navdy.hud.app.bluetooth.pbap.BluetoothPbapSession(device, this.mSessionHandler);
    }

    public void connect() {
        this.mSession.start();
    }

    public void finalize() {
        if (this.mSession != null) {
            this.mSession.stop();
        }
    }

    public void disconnect() {
        this.mSession.stop();
    }

    public void abort() {
        this.mSession.abort();
    }

    public com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient.ConnectionState getState() {
        return this.mConnectionState;
    }

    public boolean setPhoneBookFolderRoot() {
        return this.mSession.makeRequest(new com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestSetPath(false));
    }

    public boolean setPhoneBookFolderUp() {
        return this.mSession.makeRequest(new com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestSetPath(true));
    }

    public boolean setPhoneBookFolderDown(java.lang.String folder) {
        return this.mSession.makeRequest(new com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestSetPath(folder));
    }

    public boolean pullPhoneBookSize(java.lang.String pbName) {
        return this.mSession.makeRequest(new com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestPullPhoneBookSize(pbName));
    }

    public boolean pullVcardListingSize(java.lang.String folder) {
        return this.mSession.makeRequest(new com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestPullVcardListingSize(folder));
    }

    public boolean pullPhoneBook(java.lang.String pbName) {
        return pullPhoneBook(pbName, 0, 0, 0, 0);
    }

    public boolean pullPhoneBook(java.lang.String pbName, long filter, byte format) {
        return pullPhoneBook(pbName, filter, format, 0, 0);
    }

    public boolean pullPhoneBook(java.lang.String pbName, int maxListCount, int listStartOffset) {
        return pullPhoneBook(pbName, 0, 0, maxListCount, listStartOffset);
    }

    public boolean pullPhoneBook(java.lang.String pbName, long filter, byte format, int maxListCount, int listStartOffset) {
        return this.mSession.makeRequest(new com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestPullPhoneBook(pbName, filter, format, maxListCount, listStartOffset));
    }

    public boolean pullVcardListing(java.lang.String folder) {
        return pullVcardListing(folder, -1, 0, null, 0, 0);
    }

    public boolean pullVcardListing(java.lang.String folder, byte order) {
        return pullVcardListing(folder, order, 0, null, 0, 0);
    }

    public boolean pullVcardListing(java.lang.String folder, byte searchAttr, java.lang.String searchVal) {
        return pullVcardListing(folder, -1, searchAttr, searchVal, 0, 0);
    }

    public boolean pullVcardListing(java.lang.String folder, byte order, int maxListCount, int listStartOffset) {
        return pullVcardListing(folder, order, 0, null, maxListCount, listStartOffset);
    }

    public boolean pullVcardListing(java.lang.String folder, int maxListCount, int listStartOffset) {
        return pullVcardListing(folder, -1, 0, null, maxListCount, listStartOffset);
    }

    public boolean pullVcardListing(java.lang.String folder, byte order, byte searchAttr, java.lang.String searchVal, int maxListCount, int listStartOffset) {
        return this.mSession.makeRequest(new com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestPullVcardListing(folder, order, searchAttr, searchVal, maxListCount, listStartOffset));
    }

    public boolean pullVcardEntry(java.lang.String handle) {
        return pullVcardEntry(handle, 0, 0);
    }

    public boolean pullVcardEntry(java.lang.String handle, long filter, byte format) {
        return this.mSession.makeRequest(new com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestPullVcardEntry(handle, filter, format));
    }

    public boolean setAuthResponse(java.lang.String key) {
        android.util.Log.d(TAG, " setAuthResponse key=" + key);
        return this.mSession.setAuthResponse(key);
    }
}
