package com.navdy.hud.app.bluetooth.pbap.utils;

public final class ObexAppParameters {
    private final java.util.HashMap<java.lang.Byte, byte[]> mParams = new java.util.HashMap<>();

    public ObexAppParameters() {
    }

    public ObexAppParameters(byte[] raw) {
        if (raw != null) {
            int i = 0;
            while (i < raw.length && raw.length - i >= 2) {
                int i2 = i + 1;
                byte tag = raw[i];
                int i3 = i2 + 1;
                byte len = raw[i2];
                if ((raw.length - i3) - len >= 0) {
                    byte[] val = new byte[len];
                    java.lang.System.arraycopy(raw, i3, val, 0, len);
                    add(tag, val);
                    i = i3 + len;
                } else {
                    return;
                }
            }
        }
    }

    public static com.navdy.hud.app.bluetooth.pbap.utils.ObexAppParameters fromHeaderSet(com.navdy.hud.app.bluetooth.obex.HeaderSet headerset) {
        try {
            return new com.navdy.hud.app.bluetooth.pbap.utils.ObexAppParameters((byte[]) headerset.getHeader(76));
        } catch (java.io.IOException e) {
            return null;
        }
    }

    public byte[] getHeader() {
        int length = 0;
        for (java.util.Map.Entry<java.lang.Byte, byte[]> entry : this.mParams.entrySet()) {
            length += ((byte[]) entry.getValue()).length + 2;
        }
        byte[] ret = new byte[length];
        int idx = 0;
        for (java.util.Map.Entry<java.lang.Byte, byte[]> entry2 : this.mParams.entrySet()) {
            int length2 = ((byte[]) entry2.getValue()).length;
            int idx2 = idx + 1;
            ret[idx] = ((java.lang.Byte) entry2.getKey()).byteValue();
            int idx3 = idx2 + 1;
            ret[idx2] = (byte) length2;
            java.lang.System.arraycopy(entry2.getValue(), 0, ret, idx3, length2);
            idx = idx3 + length2;
        }
        return ret;
    }

    public void addToHeaderSet(com.navdy.hud.app.bluetooth.obex.HeaderSet headerset) {
        if (this.mParams.size() > 0) {
            headerset.setHeader(76, getHeader());
        }
    }

    public boolean exists(byte tag) {
        return this.mParams.containsKey(java.lang.Byte.valueOf(tag));
    }

    public void add(byte tag, byte val) {
        this.mParams.put(java.lang.Byte.valueOf(tag), java.nio.ByteBuffer.allocate(1).put(val).array());
    }

    public void add(byte tag, short val) {
        this.mParams.put(java.lang.Byte.valueOf(tag), java.nio.ByteBuffer.allocate(2).putShort(val).array());
    }

    public void add(byte tag, int val) {
        this.mParams.put(java.lang.Byte.valueOf(tag), java.nio.ByteBuffer.allocate(4).putInt(val).array());
    }

    public void add(byte tag, long val) {
        this.mParams.put(java.lang.Byte.valueOf(tag), java.nio.ByteBuffer.allocate(8).putLong(val).array());
    }

    public void add(byte tag, java.lang.String val) {
        this.mParams.put(java.lang.Byte.valueOf(tag), val.getBytes());
    }

    public void add(byte tag, byte[] bval) {
        this.mParams.put(java.lang.Byte.valueOf(tag), bval);
    }

    public byte getByte(byte tag) {
        byte[] bval = (byte[]) this.mParams.get(java.lang.Byte.valueOf(tag));
        if (bval == null || bval.length < 1) {
            return 0;
        }
        return java.nio.ByteBuffer.wrap(bval).get();
    }

    public short getShort(byte tag) {
        byte[] bval = (byte[]) this.mParams.get(java.lang.Byte.valueOf(tag));
        if (bval == null || bval.length < 2) {
            return 0;
        }
        return java.nio.ByteBuffer.wrap(bval).getShort();
    }

    public int getInt(byte tag) {
        byte[] bval = (byte[]) this.mParams.get(java.lang.Byte.valueOf(tag));
        if (bval == null || bval.length < 4) {
            return 0;
        }
        return java.nio.ByteBuffer.wrap(bval).getInt();
    }

    public java.lang.String getString(byte tag) {
        byte[] bval = (byte[]) this.mParams.get(java.lang.Byte.valueOf(tag));
        if (bval == null) {
            return null;
        }
        return new java.lang.String(bval);
    }

    public byte[] getByteArray(byte tag) {
        return (byte[]) this.mParams.get(java.lang.Byte.valueOf(tag));
    }

    public java.lang.String toString() {
        return this.mParams.toString();
    }
}
