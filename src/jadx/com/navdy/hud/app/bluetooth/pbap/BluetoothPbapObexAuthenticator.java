package com.navdy.hud.app.bluetooth.pbap;

class BluetoothPbapObexAuthenticator implements com.navdy.hud.app.bluetooth.obex.Authenticator {
    private static final java.lang.String TAG = "BTPbapObexAuthenticator";
    private final android.os.Handler mCallback;
    private boolean mReplied;
    private java.lang.String mSessionKey;

    public BluetoothPbapObexAuthenticator(android.os.Handler callback) {
        this.mCallback = callback;
    }

    public synchronized void setReply(java.lang.String key) {
        android.util.Log.d(TAG, "setReply key=" + key);
        this.mSessionKey = key;
        this.mReplied = true;
        notify();
    }

    public com.navdy.hud.app.bluetooth.obex.PasswordAuthentication onAuthenticationChallenge(java.lang.String description, boolean isUserIdRequired, boolean isFullAccess) {
        this.mReplied = false;
        android.util.Log.d(TAG, "onAuthenticationChallenge: sending request");
        this.mCallback.obtainMessage(105).sendToTarget();
        synchronized (this) {
            while (!this.mReplied) {
                try {
                    android.util.Log.v(TAG, "onAuthenticationChallenge: waiting for response");
                    wait();
                } catch (java.lang.InterruptedException e) {
                    android.util.Log.e(TAG, "Interrupted while waiting for challenge response");
                }
            }
        }
        if (this.mSessionKey == null || this.mSessionKey.length() == 0) {
            android.util.Log.v(TAG, "onAuthenticationChallenge: mSessionKey is empty, timeout/cancel occured");
            return null;
        }
        android.util.Log.v(TAG, "onAuthenticationChallenge: mSessionKey=" + this.mSessionKey);
        return new com.navdy.hud.app.bluetooth.obex.PasswordAuthentication(null, this.mSessionKey.getBytes());
    }

    public byte[] onAuthenticationResponse(byte[] userName) {
        return null;
    }
}
