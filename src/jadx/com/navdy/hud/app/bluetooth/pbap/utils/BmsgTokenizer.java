package com.navdy.hud.app.bluetooth.pbap.utils;

public final class BmsgTokenizer {
    private final java.util.regex.Matcher mMatcher;
    private final int mOffset;
    private int mPos;
    private final java.lang.String mStr;

    public static class Property {
        public final java.lang.String name;
        public final java.lang.String value;

        public Property(java.lang.String name2, java.lang.String value2) {
            if (name2 == null || value2 == null) {
                throw new java.lang.IllegalArgumentException();
            }
            this.name = name2;
            this.value = value2;
            android.util.Log.v("BMSG >> ", toString());
        }

        public java.lang.String toString() {
            return this.name + com.navdy.hud.app.framework.glance.GlanceConstants.COLON_SEPARATOR + this.value;
        }

        public boolean equals(java.lang.Object o) {
            return (o instanceof com.navdy.hud.app.bluetooth.pbap.utils.BmsgTokenizer.Property) && ((com.navdy.hud.app.bluetooth.pbap.utils.BmsgTokenizer.Property) o).name.equals(this.name) && ((com.navdy.hud.app.bluetooth.pbap.utils.BmsgTokenizer.Property) o).value.equals(this.value);
        }
    }

    public BmsgTokenizer(java.lang.String str) {
        this(str, 0);
    }

    public BmsgTokenizer(java.lang.String str, int offset) {
        this.mPos = 0;
        this.mStr = str;
        this.mOffset = offset;
        this.mMatcher = java.util.regex.Pattern.compile("(([^:]*):(.*))?\r\n").matcher(str);
        this.mPos = this.mMatcher.regionStart();
    }

    public com.navdy.hud.app.bluetooth.pbap.utils.BmsgTokenizer.Property next(boolean alwaysReturn) throws java.text.ParseException {
        boolean found = false;
        do {
            this.mMatcher.region(this.mPos, this.mMatcher.regionEnd());
            if (this.mMatcher.lookingAt()) {
                this.mPos = this.mMatcher.end();
                if (this.mMatcher.group(1) != null) {
                    found = true;
                    continue;
                }
            } else if (alwaysReturn) {
                return null;
            } else {
                throw new java.text.ParseException("Property or empty line expected", pos());
            }
        } while (!found);
        return new com.navdy.hud.app.bluetooth.pbap.utils.BmsgTokenizer.Property(this.mMatcher.group(2), this.mMatcher.group(3));
    }

    public com.navdy.hud.app.bluetooth.pbap.utils.BmsgTokenizer.Property next() throws java.text.ParseException {
        return next(false);
    }

    public java.lang.String remaining() {
        return this.mStr.substring(this.mPos);
    }

    public int pos() {
        return this.mPos + this.mOffset;
    }
}
