package com.navdy.hud.app.bluetooth.pbap;

final class BluetoothPbapRequestPullPhoneBook extends com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequest {
    private static final java.lang.String TAG = "BTPbapReqPullPBook";
    private static final java.lang.String TYPE = "x-bt/phonebook";
    private final byte mFormat;
    private int mNewMissedCalls = -1;
    private com.navdy.hud.app.bluetooth.pbap.BluetoothPbapVcardList mResponse;

    public BluetoothPbapRequestPullPhoneBook(java.lang.String pbName, long filter, byte format, int maxListCount, int listStartOffset) {
        if (maxListCount < 0 || maxListCount > 65535) {
            throw new java.lang.IllegalArgumentException("maxListCount should be [0..65535]");
        } else if (listStartOffset < 0 || listStartOffset > 65535) {
            throw new java.lang.IllegalArgumentException("listStartOffset should be [0..65535]");
        } else {
            this.mHeaderSet.setHeader(1, pbName);
            this.mHeaderSet.setHeader(66, TYPE);
            com.navdy.hud.app.bluetooth.pbap.utils.ObexAppParameters oap = new com.navdy.hud.app.bluetooth.pbap.utils.ObexAppParameters();
            if (!(format == 0 || format == 1)) {
                format = 0;
            }
            if (filter != 0) {
                oap.add(6, filter);
            }
            oap.add(7, format);
            if (maxListCount > 0) {
                oap.add(4, (short) maxListCount);
            } else {
                oap.add(4, -1);
            }
            if (listStartOffset > 0) {
                oap.add(5, (short) listStartOffset);
            }
            oap.addToHeaderSet(this.mHeaderSet);
            this.mFormat = format;
        }
    }

    /* access modifiers changed from: protected */
    public void readResponse(java.io.InputStream stream) throws java.io.IOException {
        android.util.Log.v(TAG, "readResponse");
        this.mResponse = new com.navdy.hud.app.bluetooth.pbap.BluetoothPbapVcardList(stream, this.mFormat);
    }

    /* access modifiers changed from: protected */
    public void readResponseHeaders(com.navdy.hud.app.bluetooth.obex.HeaderSet headerset) {
        android.util.Log.v(TAG, "readResponse");
        com.navdy.hud.app.bluetooth.pbap.utils.ObexAppParameters oap = com.navdy.hud.app.bluetooth.pbap.utils.ObexAppParameters.fromHeaderSet(headerset);
        if (oap.exists(9)) {
            this.mNewMissedCalls = oap.getByte(9);
        }
    }

    public java.util.ArrayList<com.navdy.hud.app.bluetooth.vcard.VCardEntry> getList() {
        return this.mResponse.getList();
    }

    public int getNewMissedCalls() {
        return this.mNewMissedCalls;
    }
}
