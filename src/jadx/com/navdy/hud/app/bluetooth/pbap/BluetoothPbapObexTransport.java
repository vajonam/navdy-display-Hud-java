package com.navdy.hud.app.bluetooth.pbap;

class BluetoothPbapObexTransport implements com.navdy.hud.app.bluetooth.obex.ObexTransport {
    private android.bluetooth.BluetoothSocket mSocket = null;

    public BluetoothPbapObexTransport(android.bluetooth.BluetoothSocket rfs) {
        this.mSocket = rfs;
    }

    public void close() throws java.io.IOException {
        this.mSocket.close();
    }

    public java.io.DataInputStream openDataInputStream() throws java.io.IOException {
        return new java.io.DataInputStream(openInputStream());
    }

    public java.io.DataOutputStream openDataOutputStream() throws java.io.IOException {
        return new java.io.DataOutputStream(openOutputStream());
    }

    public java.io.InputStream openInputStream() throws java.io.IOException {
        return this.mSocket.getInputStream();
    }

    public java.io.OutputStream openOutputStream() throws java.io.IOException {
        return this.mSocket.getOutputStream();
    }

    public void connect() throws java.io.IOException {
    }

    public void create() throws java.io.IOException {
    }

    public void disconnect() throws java.io.IOException {
    }

    public void listen() throws java.io.IOException {
    }

    public boolean isConnected() throws java.io.IOException {
        return this.mSocket.isConnected();
    }

    public boolean isSrmSupported() {
        return false;
    }

    public int getMaxTransmitPacketSize() {
        return -1;
    }

    public int getMaxReceivePacketSize() {
        return -1;
    }
}
