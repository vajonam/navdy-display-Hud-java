package com.navdy.hud.app.bluetooth.pbap;

final class BluetoothPbapRequestPullVcardEntry extends com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequest {
    private static final java.lang.String TAG = "BTPbapReqPullVcardEntry";
    private static final java.lang.String TYPE = "x-bt/vcard";
    private final byte mFormat;
    private com.navdy.hud.app.bluetooth.pbap.BluetoothPbapVcardList mResponse;

    public BluetoothPbapRequestPullVcardEntry(java.lang.String handle, long filter, byte format) {
        this.mHeaderSet.setHeader(1, handle);
        this.mHeaderSet.setHeader(66, TYPE);
        if (!(format == 0 || format == 1)) {
            format = 0;
        }
        com.navdy.hud.app.bluetooth.pbap.utils.ObexAppParameters oap = new com.navdy.hud.app.bluetooth.pbap.utils.ObexAppParameters();
        if (filter != 0) {
            oap.add(6, filter);
        }
        oap.add(7, format);
        oap.addToHeaderSet(this.mHeaderSet);
        this.mFormat = format;
    }

    /* access modifiers changed from: protected */
    public void readResponse(java.io.InputStream stream) throws java.io.IOException {
        android.util.Log.v(TAG, "readResponse");
        this.mResponse = new com.navdy.hud.app.bluetooth.pbap.BluetoothPbapVcardList(stream, this.mFormat);
    }

    /* access modifiers changed from: protected */
    public void checkResponseCode(int responseCode) throws java.io.IOException {
        android.util.Log.v(TAG, "checkResponseCode");
        if (this.mResponse.getCount() != 0) {
            return;
        }
        if (responseCode == 196 || responseCode == 198) {
            android.util.Log.v(TAG, "Vcard Entry not found");
            return;
        }
        throw new java.io.IOException("Invalid response received:" + responseCode);
    }

    public com.navdy.hud.app.bluetooth.vcard.VCardEntry getVcard() {
        return this.mResponse.getFirst();
    }
}
