package com.navdy.hud.app.bluetooth.pbap;

final class BluetoothPbapObexSession {
    static final int OBEX_SESSION_AUTHENTICATION_REQUEST = 105;
    static final int OBEX_SESSION_AUTHENTICATION_TIMEOUT = 106;
    static final int OBEX_SESSION_CONNECTED = 100;
    static final int OBEX_SESSION_DISCONNECTED = 102;
    static final int OBEX_SESSION_FAILED = 101;
    static final int OBEX_SESSION_REQUEST_COMPLETED = 103;
    static final int OBEX_SESSION_REQUEST_FAILED = 104;
    /* access modifiers changed from: private */
    public static final byte[] PBAP_TARGET = {121, 97, 53, -16, -16, -59, 17, -40, 9, 102, 8, 0, 32, 12, -102, 102};
    private static final java.lang.String TAG = "BTPbapObexSession";
    /* access modifiers changed from: private */
    public com.navdy.hud.app.bluetooth.pbap.BluetoothPbapObexAuthenticator mAuth = null;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.bluetooth.pbap.BluetoothPbapObexSession.ObexClientThread mObexClientThread;
    /* access modifiers changed from: private */
    public android.os.Handler mSessionHandler;
    /* access modifiers changed from: private */
    public final com.navdy.hud.app.bluetooth.obex.ObexTransport mTransport;

    class Anon1 extends java.lang.Thread {
        Anon1() {
        }

        public void run() {
            com.navdy.hud.app.bluetooth.pbap.BluetoothPbapObexSession.this.mObexClientThread.mRequest.abort();
        }
    }

    private class ObexClientThread extends java.lang.Thread {
        private static final java.lang.String TAG = "ObexClientThread";
        private com.navdy.hud.app.bluetooth.obex.ClientSession mClientSession = null;
        /* access modifiers changed from: private */
        public com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequest mRequest = null;
        private volatile boolean mRunning = true;

        public ObexClientThread() {
        }

        public boolean isRunning() {
            return this.mRunning;
        }

        public void run() {
            super.run();
            if (!connect()) {
                com.navdy.hud.app.bluetooth.pbap.BluetoothPbapObexSession.this.mSessionHandler.obtainMessage(101).sendToTarget();
                return;
            }
            com.navdy.hud.app.bluetooth.pbap.BluetoothPbapObexSession.this.mSessionHandler.obtainMessage(100).sendToTarget();
            while (this.mRunning) {
                synchronized (this) {
                    try {
                        if (this.mRequest == null) {
                            wait();
                        }
                    } catch (java.lang.InterruptedException e) {
                        this.mRunning = false;
                    }
                }
                if (this.mRunning && this.mRequest != null) {
                    try {
                        this.mRequest.execute(this.mClientSession);
                    } catch (Throwable th) {
                        this.mRunning = false;
                    }
                    if (this.mRequest.isSuccess()) {
                        com.navdy.hud.app.bluetooth.pbap.BluetoothPbapObexSession.this.mSessionHandler.obtainMessage(103, this.mRequest).sendToTarget();
                    } else {
                        com.navdy.hud.app.bluetooth.pbap.BluetoothPbapObexSession.this.mSessionHandler.obtainMessage(104, this.mRequest).sendToTarget();
                    }
                }
                this.mRequest = null;
            }
            disconnect();
            com.navdy.hud.app.bluetooth.pbap.BluetoothPbapObexSession.this.mSessionHandler.obtainMessage(102).sendToTarget();
        }

        public synchronized boolean schedule(com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequest request) {
            boolean z;
            android.util.Log.d(TAG, "schedule: " + request.getClass().getSimpleName());
            if (this.mRequest != null) {
                z = false;
            } else {
                this.mRequest = request;
                notify();
                z = true;
            }
            return z;
        }

        private boolean connect() {
            android.util.Log.d(TAG, "connect");
            try {
                this.mClientSession = new com.navdy.hud.app.bluetooth.obex.ClientSession(com.navdy.hud.app.bluetooth.pbap.BluetoothPbapObexSession.this.mTransport);
                this.mClientSession.setAuthenticator(com.navdy.hud.app.bluetooth.pbap.BluetoothPbapObexSession.this.mAuth);
                com.navdy.hud.app.bluetooth.obex.HeaderSet hs = new com.navdy.hud.app.bluetooth.obex.HeaderSet();
                hs.setHeader(70, com.navdy.hud.app.bluetooth.pbap.BluetoothPbapObexSession.PBAP_TARGET);
                try {
                    if (this.mClientSession.connect(hs).getResponseCode() == 160) {
                        return true;
                    }
                    disconnect();
                    return false;
                } catch (Throwable th) {
                    return false;
                }
            } catch (Throwable th2) {
                return false;
            }
        }

        private void disconnect() {
            android.util.Log.d(TAG, "disconnect");
            if (this.mClientSession != null) {
                try {
                    this.mClientSession.disconnect(null);
                    this.mClientSession.close();
                } catch (Throwable th) {
                }
            }
        }
    }

    public BluetoothPbapObexSession(com.navdy.hud.app.bluetooth.obex.ObexTransport transport) {
        this.mTransport = transport;
    }

    public void start(android.os.Handler handler) {
        android.util.Log.d(TAG, "start");
        this.mSessionHandler = handler;
        this.mAuth = new com.navdy.hud.app.bluetooth.pbap.BluetoothPbapObexAuthenticator(this.mSessionHandler);
        this.mObexClientThread = new com.navdy.hud.app.bluetooth.pbap.BluetoothPbapObexSession.ObexClientThread();
        this.mObexClientThread.start();
    }

    public void stop() {
        android.util.Log.d(TAG, "stop");
        if (this.mObexClientThread != null) {
            boolean wait = false;
            try {
                wait = this.mObexClientThread.isAlive() && this.mObexClientThread.isRunning();
            } catch (Throwable th) {
            }
            if (wait) {
                try {
                    this.mObexClientThread.interrupt();
                } catch (Throwable th2) {
                }
                try {
                    this.mObexClientThread.join();
                } catch (Throwable th3) {
                }
            }
            this.mObexClientThread = null;
        }
    }

    public void abort() {
        android.util.Log.d(TAG, "abort");
        if (this.mObexClientThread != null && this.mObexClientThread.mRequest != null) {
            new com.navdy.hud.app.bluetooth.pbap.BluetoothPbapObexSession.Anon1().run();
        }
    }

    public boolean schedule(com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequest request) {
        android.util.Log.d(TAG, "schedule: " + request.getClass().getSimpleName());
        if (this.mObexClientThread != null) {
            return this.mObexClientThread.schedule(request);
        }
        android.util.Log.e(TAG, "OBEX session not started");
        return false;
    }

    public boolean setAuthReply(java.lang.String key) {
        android.util.Log.d(TAG, "setAuthReply key=" + key);
        if (this.mAuth == null) {
            return false;
        }
        this.mAuth.setReply(key);
        return true;
    }
}
