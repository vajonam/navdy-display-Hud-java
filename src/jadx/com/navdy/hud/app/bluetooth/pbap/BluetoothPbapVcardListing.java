package com.navdy.hud.app.bluetooth.pbap;

class BluetoothPbapVcardListing {
    private static final java.lang.String TAG = "BTPbapVcardList";
    java.util.ArrayList<com.navdy.hud.app.bluetooth.pbap.BluetoothPbapCard> mCards = new java.util.ArrayList<>();

    public BluetoothPbapVcardListing(java.io.InputStream in) throws java.io.IOException {
        parse(in);
    }

    private void parse(java.io.InputStream in) throws java.io.IOException {
        org.xmlpull.v1.XmlPullParser parser = android.util.Xml.newPullParser();
        try {
            parser.setInput(in, "UTF-8");
            for (int eventType = parser.getEventType(); eventType != 1; eventType = parser.next()) {
                if (eventType == 2 && parser.getName().equals("card")) {
                    this.mCards.add(new com.navdy.hud.app.bluetooth.pbap.BluetoothPbapCard(parser.getAttributeValue(null, "handle"), parser.getAttributeValue(null, "name")));
                }
            }
        } catch (org.xmlpull.v1.XmlPullParserException e) {
            android.util.Log.e(TAG, "XML parser error when parsing XML", e);
        }
    }

    public java.util.ArrayList<com.navdy.hud.app.bluetooth.pbap.BluetoothPbapCard> getList() {
        return this.mCards;
    }
}
