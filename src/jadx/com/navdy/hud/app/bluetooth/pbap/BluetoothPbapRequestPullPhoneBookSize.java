package com.navdy.hud.app.bluetooth.pbap;

class BluetoothPbapRequestPullPhoneBookSize extends com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequest {
    private static final java.lang.String TAG = "BTPbapReqPullPBookSize";
    private static final java.lang.String TYPE = "x-bt/phonebook";
    private int mSize;

    public BluetoothPbapRequestPullPhoneBookSize(java.lang.String pbName) {
        this.mHeaderSet.setHeader(1, pbName);
        this.mHeaderSet.setHeader(66, TYPE);
        com.navdy.hud.app.bluetooth.pbap.utils.ObexAppParameters oap = new com.navdy.hud.app.bluetooth.pbap.utils.ObexAppParameters();
        oap.add(4, 0);
        oap.addToHeaderSet(this.mHeaderSet);
    }

    /* access modifiers changed from: protected */
    public void readResponseHeaders(com.navdy.hud.app.bluetooth.obex.HeaderSet headerset) {
        android.util.Log.v(TAG, "readResponseHeaders");
        this.mSize = com.navdy.hud.app.bluetooth.pbap.utils.ObexAppParameters.fromHeaderSet(headerset).getShort(8);
    }

    public int getSize() {
        return this.mSize;
    }
}
