package com.navdy.hud.app.bluetooth.pbap.utils;

public final class ObexTime {
    private java.util.Date mDate;

    public ObexTime(java.lang.String time) {
        java.util.regex.Matcher m = java.util.regex.Pattern.compile("(\\d{4})(\\d{2})(\\d{2})T(\\d{2})(\\d{2})(\\d{2})(([+-])(\\d{2})(\\d{2}))?").matcher(time);
        if (m.matches()) {
            java.util.Calendar cal = java.util.Calendar.getInstance();
            cal.set(java.lang.Integer.parseInt(m.group(1)), java.lang.Integer.parseInt(m.group(2)) - 1, java.lang.Integer.parseInt(m.group(3)), java.lang.Integer.parseInt(m.group(4)), java.lang.Integer.parseInt(m.group(5)), java.lang.Integer.parseInt(m.group(6)));
            if (m.group(7) != null) {
                int ohh = java.lang.Integer.parseInt(m.group(9));
                int offset = ((ohh * 60) + java.lang.Integer.parseInt(m.group(10))) * 60 * 1000;
                if (m.group(8).equals("-")) {
                    offset = -offset;
                }
                java.util.TimeZone tz = java.util.TimeZone.getTimeZone("UTC");
                tz.setRawOffset(offset);
                cal.setTimeZone(tz);
            }
            this.mDate = cal.getTime();
        }
    }

    public ObexTime(java.util.Date date) {
        this.mDate = date;
    }

    public java.util.Date getTime() {
        return this.mDate;
    }

    public java.lang.String toString() {
        if (this.mDate == null) {
            return null;
        }
        java.util.Calendar cal = java.util.Calendar.getInstance();
        cal.setTime(this.mDate);
        return java.lang.String.format(java.util.Locale.US, "%04d%02d%02dT%02d%02d%02d", new java.lang.Object[]{java.lang.Integer.valueOf(cal.get(1)), java.lang.Integer.valueOf(cal.get(2) + 1), java.lang.Integer.valueOf(cal.get(5)), java.lang.Integer.valueOf(cal.get(11)), java.lang.Integer.valueOf(cal.get(12)), java.lang.Integer.valueOf(cal.get(13))});
    }
}
