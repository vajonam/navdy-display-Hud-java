package com.navdy.hud.app.bluetooth.pbap;

final class BluetoothPbapRequestSetPath extends com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequest {
    private static final java.lang.String TAG = "BTPbapReqSetPath";
    private com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestSetPath.SetPathDir mDir;

    private enum SetPathDir {
        ROOT,
        UP,
        DOWN
    }

    public BluetoothPbapRequestSetPath(java.lang.String name) {
        this.mDir = com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestSetPath.SetPathDir.DOWN;
        this.mHeaderSet.setHeader(1, name);
    }

    public BluetoothPbapRequestSetPath(boolean goUp) {
        this.mHeaderSet.setEmptyNameHeader();
        if (goUp) {
            this.mDir = com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestSetPath.SetPathDir.UP;
        } else {
            this.mDir = com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestSetPath.SetPathDir.ROOT;
        }
    }

    public void execute(com.navdy.hud.app.bluetooth.obex.ClientSession session) {
        android.util.Log.v(TAG, "execute");
        com.navdy.hud.app.bluetooth.obex.HeaderSet hs = null;
        try {
            switch (this.mDir) {
                case ROOT:
                case DOWN:
                    hs = session.setPath(this.mHeaderSet, false, false);
                    break;
                case UP:
                    hs = session.setPath(this.mHeaderSet, true, false);
                    break;
            }
            this.mResponseCode = hs.getResponseCode();
        } catch (java.io.IOException e) {
            this.mResponseCode = com.navdy.hud.app.bluetooth.obex.ResponseCodes.OBEX_HTTP_INTERNAL_ERROR;
        }
    }
}
