package com.navdy.hud.app.bluetooth.pbap;

abstract class BluetoothPbapRequest {
    protected static final byte OAP_TAGID_FILTER = 6;
    protected static final byte OAP_TAGID_FORMAT = 7;
    protected static final byte OAP_TAGID_LIST_START_OFFSET = 5;
    protected static final byte OAP_TAGID_MAX_LIST_COUNT = 4;
    protected static final byte OAP_TAGID_NEW_MISSED_CALLS = 9;
    protected static final byte OAP_TAGID_ORDER = 1;
    protected static final byte OAP_TAGID_PHONEBOOK_SIZE = 8;
    protected static final byte OAP_TAGID_SEARCH_ATTRIBUTE = 3;
    protected static final byte OAP_TAGID_SEARCH_VALUE = 2;
    private static final java.lang.String TAG = "BTPbapRequest";
    private boolean mAborted = false;
    protected com.navdy.hud.app.bluetooth.obex.HeaderSet mHeaderSet = new com.navdy.hud.app.bluetooth.obex.HeaderSet();
    private com.navdy.hud.app.bluetooth.obex.ClientOperation mOp = null;
    protected int mResponseCode;

    public final boolean isSuccess() {
        return this.mResponseCode == 160;
    }

    public void execute(com.navdy.hud.app.bluetooth.obex.ClientSession session) throws java.io.IOException {
        android.util.Log.v(TAG, "execute");
        if (this.mAborted) {
            this.mResponseCode = com.navdy.hud.app.bluetooth.obex.ResponseCodes.OBEX_HTTP_INTERNAL_ERROR;
            return;
        }
        try {
            this.mOp = (com.navdy.hud.app.bluetooth.obex.ClientOperation) session.get(this.mHeaderSet);
            this.mOp.setGetFinalFlag(true);
            this.mOp.continueOperation(true, false);
            readResponseHeaders(this.mOp.getReceivedHeader());
            java.io.InputStream is = this.mOp.openInputStream();
            readResponse(is);
            is.close();
            this.mOp.close();
            this.mResponseCode = this.mOp.getResponseCode();
            android.util.Log.d(TAG, "mResponseCode=" + this.mResponseCode);
            checkResponseCode(this.mResponseCode);
        } catch (java.io.IOException e) {
            android.util.Log.e(TAG, "IOException occured when processing request", e);
            this.mResponseCode = com.navdy.hud.app.bluetooth.obex.ResponseCodes.OBEX_HTTP_INTERNAL_ERROR;
            throw e;
        }
    }

    public void abort() {
        this.mAborted = true;
        if (this.mOp != null) {
            try {
                this.mOp.abort();
            } catch (java.io.IOException e) {
                android.util.Log.e(TAG, "Exception occured when trying to abort", e);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void readResponse(java.io.InputStream stream) throws java.io.IOException {
        android.util.Log.v(TAG, "readResponse");
    }

    /* access modifiers changed from: protected */
    public void readResponseHeaders(com.navdy.hud.app.bluetooth.obex.HeaderSet headerset) {
        android.util.Log.v(TAG, "readResponseHeaders");
    }

    /* access modifiers changed from: protected */
    public void checkResponseCode(int responseCode) throws java.io.IOException {
        android.util.Log.v(TAG, "checkResponseCode");
    }
}
