package com.navdy.hud.app.bluetooth.pbap;

class BluetoothPbapSession implements android.os.Handler.Callback {
    public static final int ACTION_LISTING = 14;
    public static final int ACTION_PHONEBOOK_SIZE = 16;
    public static final int ACTION_VCARD = 15;
    public static final int AUTH_REQUESTED = 8;
    public static final int AUTH_TIMEOUT = 9;
    private static final java.lang.String PBAP_UUID = "0000112f-0000-1000-8000-00805f9b34fb";
    public static final int REQUEST_COMPLETED = 3;
    public static final int REQUEST_FAILED = 4;
    private static final int RFCOMM_CONNECTED = 1;
    private static final int RFCOMM_FAILED = 2;
    public static final int SESSION_CONNECTED = 6;
    public static final int SESSION_CONNECTING = 5;
    public static final int SESSION_DISCONNECTED = 7;
    private static final java.lang.String TAG = "BTPbapSession";
    /* access modifiers changed from: private */
    public final android.bluetooth.BluetoothAdapter mAdapter = android.bluetooth.BluetoothAdapter.getDefaultAdapter();
    private com.navdy.hud.app.bluetooth.pbap.BluetoothPbapSession.RfcommConnectThread mConnectThread;
    /* access modifiers changed from: private */
    public final android.bluetooth.BluetoothDevice mDevice;
    private final android.os.HandlerThread mHandlerThread;
    private com.navdy.hud.app.bluetooth.pbap.BluetoothPbapObexSession mObexSession;
    private final android.os.Handler mParentHandler;
    private com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequest mPendingRequest = null;
    /* access modifiers changed from: private */
    public final android.os.Handler mSessionHandler;
    private com.navdy.hud.app.bluetooth.pbap.BluetoothPbapObexTransport mTransport;

    private class RfcommConnectThread extends java.lang.Thread {
        private static final java.lang.String TAG = "RfcommConnectThread";
        private android.bluetooth.BluetoothSocket mSocket;

        public RfcommConnectThread() {
            super(TAG);
        }

        public void run() {
            if (com.navdy.hud.app.bluetooth.pbap.BluetoothPbapSession.this.mAdapter.isDiscovering()) {
                android.util.Log.w(TAG, "pbap device currently discovering, might be slow to connect");
            }
            try {
                this.mSocket = com.navdy.hud.app.bluetooth.pbap.BluetoothPbapSession.this.mDevice.createRfcommSocketToServiceRecord(java.util.UUID.fromString(com.navdy.hud.app.bluetooth.pbap.BluetoothPbapSession.PBAP_UUID));
                this.mSocket.connect();
                com.navdy.hud.app.bluetooth.pbap.BluetoothPbapSession.this.mSessionHandler.obtainMessage(1, new com.navdy.hud.app.bluetooth.pbap.BluetoothPbapObexTransport(this.mSocket)).sendToTarget();
            } catch (Throwable th) {
                closeSocket();
                com.navdy.hud.app.bluetooth.pbap.BluetoothPbapSession.this.mSessionHandler.obtainMessage(2).sendToTarget();
            }
        }

        private void closeSocket() {
            com.navdy.service.library.util.IOUtils.closeStream(this.mSocket);
        }
    }

    public BluetoothPbapSession(android.bluetooth.BluetoothDevice device, android.os.Handler handler) {
        if (this.mAdapter == null) {
            throw new java.lang.NullPointerException("No Bluetooth adapter in the system");
        }
        this.mDevice = device;
        this.mParentHandler = handler;
        this.mConnectThread = null;
        this.mTransport = null;
        this.mObexSession = null;
        this.mHandlerThread = new android.os.HandlerThread("PBAP session handler", 10);
        this.mHandlerThread.start();
        this.mSessionHandler = new android.os.Handler(this.mHandlerThread.getLooper(), this);
    }

    public boolean handleMessage(android.os.Message msg) {
        android.util.Log.d(TAG, "Handler: msg: " + msg.what);
        switch (msg.what) {
            case 1:
                this.mConnectThread = null;
                this.mTransport = (com.navdy.hud.app.bluetooth.pbap.BluetoothPbapObexTransport) msg.obj;
                startObexSession();
                break;
            case 2:
                this.mConnectThread = null;
                this.mParentHandler.obtainMessage(7).sendToTarget();
                if (this.mPendingRequest != null) {
                    this.mParentHandler.obtainMessage(4, this.mPendingRequest).sendToTarget();
                    this.mPendingRequest = null;
                    break;
                }
                break;
            case 100:
                this.mParentHandler.obtainMessage(6).sendToTarget();
                if (this.mPendingRequest != null) {
                    this.mObexSession.schedule(this.mPendingRequest);
                    this.mPendingRequest = null;
                    break;
                }
                break;
            case com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient.EVENT_SET_PHONE_BOOK_ERROR /*101*/:
                stopObexSession();
                this.mParentHandler.obtainMessage(7).sendToTarget();
                if (this.mPendingRequest != null) {
                    this.mParentHandler.obtainMessage(4, this.mPendingRequest).sendToTarget();
                    this.mPendingRequest = null;
                    break;
                }
                break;
            case 102:
                this.mParentHandler.obtainMessage(7).sendToTarget();
                stopRfcomm();
                break;
            case com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient.EVENT_PULL_VCARD_LISTING_ERROR /*103*/:
                this.mParentHandler.obtainMessage(3, msg.obj).sendToTarget();
                break;
            case com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient.EVENT_PULL_VCARD_ENTRY_ERROR /*104*/:
                this.mParentHandler.obtainMessage(4, msg.obj).sendToTarget();
                break;
            case 105:
                this.mParentHandler.obtainMessage(8).sendToTarget();
                this.mSessionHandler.sendMessageDelayed(this.mSessionHandler.obtainMessage(com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient.EVENT_PULL_VCARD_LISTING_SIZE_ERROR), 30000);
                break;
            case com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient.EVENT_PULL_VCARD_LISTING_SIZE_ERROR /*106*/:
                setAuthResponse(null);
                this.mParentHandler.obtainMessage(9).sendToTarget();
                break;
            default:
                return false;
        }
        return true;
    }

    public void start() {
        android.util.Log.d(TAG, "start");
        startRfcomm();
    }

    public void stop() {
        android.util.Log.d(TAG, "Stop");
        stopObexSession();
        stopRfcomm();
        if (this.mHandlerThread.isAlive()) {
            android.util.Log.d(TAG, "Stopped handler thread:" + this.mHandlerThread.quit() + " id[" + java.lang.System.identityHashCode(this.mHandlerThread) + "]");
            return;
        }
        android.util.Log.d(TAG, "handler thread not running id[" + java.lang.System.identityHashCode(this.mHandlerThread) + "]");
    }

    public void abort() {
        android.util.Log.d(TAG, "abort");
        if (this.mPendingRequest != null) {
            this.mParentHandler.obtainMessage(4, this.mPendingRequest).sendToTarget();
            this.mPendingRequest = null;
        }
        if (this.mObexSession != null) {
            this.mObexSession.abort();
        }
    }

    public boolean makeRequest(com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequest request) {
        android.util.Log.v(TAG, "makeRequest: " + request.getClass().getSimpleName());
        if (this.mPendingRequest != null) {
            android.util.Log.w(TAG, "makeRequest: request already queued, exiting");
            return false;
        } else if (this.mObexSession != null) {
            return this.mObexSession.schedule(request);
        } else {
            this.mPendingRequest = request;
            startRfcomm();
            return true;
        }
    }

    public boolean setAuthResponse(java.lang.String key) {
        android.util.Log.d(TAG, "setAuthResponse key=" + key);
        this.mSessionHandler.removeMessages(com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient.EVENT_PULL_VCARD_LISTING_SIZE_ERROR);
        if (this.mObexSession == null) {
            return false;
        }
        return this.mObexSession.setAuthReply(key);
    }

    private void startRfcomm() {
        android.util.Log.d(TAG, "startRfcomm");
        if (this.mConnectThread == null && this.mObexSession == null) {
            this.mParentHandler.obtainMessage(5).sendToTarget();
            this.mConnectThread = new com.navdy.hud.app.bluetooth.pbap.BluetoothPbapSession.RfcommConnectThread();
            this.mConnectThread.start();
        }
    }

    private void stopRfcomm() {
        android.util.Log.d(TAG, "stopRfcomm");
        if (this.mConnectThread != null) {
            try {
                this.mConnectThread.join();
            } catch (Throwable th) {
            }
            this.mConnectThread = null;
        }
        if (this.mTransport != null) {
            try {
                this.mTransport.close();
            } catch (Throwable th2) {
            }
            this.mTransport = null;
        }
    }

    private void startObexSession() {
        android.util.Log.d(TAG, "startObexSession");
        this.mObexSession = new com.navdy.hud.app.bluetooth.pbap.BluetoothPbapObexSession(this.mTransport);
        this.mObexSession.start(this.mSessionHandler);
    }

    private void stopObexSession() {
        android.util.Log.d(TAG, "stopObexSession");
        try {
            if (this.mObexSession != null) {
                this.mObexSession.stop();
            }
        } catch (Throwable t) {
            android.util.Log.e(TAG, "", t);
        } finally {
            this.mObexSession = null;
        }
    }
}
