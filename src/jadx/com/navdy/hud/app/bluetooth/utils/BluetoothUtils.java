package com.navdy.hud.app.bluetooth.utils;

public final class BluetoothUtils {
    public static java.lang.String getDeviceType(int deviceType) {
        switch (deviceType) {
            case 1:
                return "Classic";
            case 2:
                return "LE";
            case 3:
                return "Dual";
            default:
                return "Unknown:" + deviceType;
        }
    }

    public static java.lang.String getBondState(int bondState) {
        switch (bondState) {
            case 10:
                return "Not Bonded";
            case 11:
                return "Bonding";
            case 12:
                return "Bonded";
            default:
                return "Unknown:" + bondState;
        }
    }

    public static java.lang.String getConnectedState(int state) {
        switch (state) {
            case 0:
                return "Disconnected";
            case 1:
                return "Connecting";
            case 2:
                return "Connected";
            case 3:
                return "Disconnecting";
            default:
                return "Unknown:" + state;
        }
    }
}
