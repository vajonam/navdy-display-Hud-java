package com.navdy.hud.app.bluetooth.vcard;

public class VCardSourceDetector implements com.navdy.hud.app.bluetooth.vcard.VCardInterpreter {
    private static java.util.Set<java.lang.String> APPLE_SIGNS = new java.util.HashSet(java.util.Arrays.asList(new java.lang.String[]{com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_X_PHONETIC_FIRST_NAME, com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_X_PHONETIC_MIDDLE_NAME, com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_X_PHONETIC_LAST_NAME, "X-ABADR", "X-ABUID"}));
    private static java.util.Set<java.lang.String> FOMA_SIGNS = new java.util.HashSet(java.util.Arrays.asList(new java.lang.String[]{"X-SD-VERN", "X-SD-FORMAT_VER", "X-SD-CATEGORIES", "X-SD-CLASS", "X-SD-DCREATED", "X-SD-DESCRIPTION"}));
    private static java.util.Set<java.lang.String> JAPANESE_MOBILE_PHONE_SIGNS = new java.util.HashSet(java.util.Arrays.asList(new java.lang.String[]{"X-GNO", "X-GN", com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_X_REDUCTION}));
    private static final java.lang.String LOG_TAG = "vCard";
    private static final int PARSE_TYPE_APPLE = 1;
    private static final int PARSE_TYPE_DOCOMO_FOMA = 3;
    private static final int PARSE_TYPE_MOBILE_PHONE_JP = 2;
    public static final int PARSE_TYPE_UNKNOWN = 0;
    private static final int PARSE_TYPE_WINDOWS_MOBILE_V65_JP = 4;
    private static java.lang.String TYPE_FOMA_CHARSET_SIGN = "X-SD-CHAR_CODE";
    private static java.util.Set<java.lang.String> WINDOWS_MOBILE_PHONE_SIGNS = new java.util.HashSet(java.util.Arrays.asList(new java.lang.String[]{"X-MICROSOFT-ASST_TEL", "X-MICROSOFT-ASSISTANT", "X-MICROSOFT-OFFICELOC"}));
    private int mParseType = 0;
    private java.lang.String mSpecifiedCharset;
    private int mVersion = -1;

    public void onVCardStarted() {
    }

    public void onVCardEnded() {
    }

    public void onEntryStarted() {
    }

    public void onEntryEnded() {
    }

    public void onPropertyCreated(com.navdy.hud.app.bluetooth.vcard.VCardProperty property) {
        java.lang.String propertyName = property.getName();
        java.util.List<java.lang.String> valueList = property.getValueList();
        if (propertyName.equalsIgnoreCase(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_VERSION) && valueList.size() > 0) {
            java.lang.String versionString = (java.lang.String) valueList.get(0);
            if (versionString.equals(com.navdy.hud.app.bluetooth.vcard.VCardConstants.VERSION_V21)) {
                this.mVersion = 0;
            } else if (versionString.equals(com.navdy.hud.app.bluetooth.vcard.VCardConstants.VERSION_V30)) {
                this.mVersion = 1;
            } else if (versionString.equals(com.navdy.hud.app.bluetooth.vcard.VCardConstants.VERSION_V40)) {
                this.mVersion = 2;
            } else {
                android.util.Log.w(LOG_TAG, "Invalid version string: " + versionString);
            }
        } else if (propertyName.equalsIgnoreCase(TYPE_FOMA_CHARSET_SIGN)) {
            this.mParseType = 3;
            if (valueList.size() > 0) {
                this.mSpecifiedCharset = (java.lang.String) valueList.get(0);
            }
        }
        if (this.mParseType == 0) {
            if (WINDOWS_MOBILE_PHONE_SIGNS.contains(propertyName)) {
                this.mParseType = 4;
            } else if (FOMA_SIGNS.contains(propertyName)) {
                this.mParseType = 3;
            } else if (JAPANESE_MOBILE_PHONE_SIGNS.contains(propertyName)) {
                this.mParseType = 2;
            } else if (APPLE_SIGNS.contains(propertyName)) {
                this.mParseType = 1;
            }
        }
    }

    public int getEstimatedType() {
        switch (this.mParseType) {
            case 2:
                return com.navdy.hud.app.bluetooth.vcard.VCardConfig.VCARD_TYPE_V21_JAPANESE_MOBILE;
            case 3:
                return com.navdy.hud.app.bluetooth.vcard.VCardConfig.VCARD_TYPE_DOCOMO;
            default:
                if (this.mVersion == 0) {
                    return com.navdy.hud.app.bluetooth.vcard.VCardConfig.VCARD_TYPE_V21_GENERIC;
                }
                if (this.mVersion == 1) {
                    return com.navdy.hud.app.bluetooth.vcard.VCardConfig.VCARD_TYPE_V30_GENERIC;
                }
                if (this.mVersion == 2) {
                    return com.navdy.hud.app.bluetooth.vcard.VCardConfig.VCARD_TYPE_V40_GENERIC;
                }
                return 0;
        }
    }

    public java.lang.String getEstimatedCharset() {
        if (android.text.TextUtils.isEmpty(this.mSpecifiedCharset)) {
            return this.mSpecifiedCharset;
        }
        switch (this.mParseType) {
            case 1:
                return "UTF-8";
            case 2:
            case 3:
            case 4:
                return "SHIFT_JIS";
            default:
                return null;
        }
    }
}
