package com.navdy.hud.app.bluetooth.vcard;

public class VCardComposer {
    private static final boolean DEBUG = false;
    public static final java.lang.String FAILURE_REASON_FAILED_TO_GET_DATABASE_INFO = "Failed to get database information";
    public static final java.lang.String FAILURE_REASON_NOT_INITIALIZED = "The vCard composer object is not correctly initialized";
    public static final java.lang.String FAILURE_REASON_NO_ENTRY = "There's no exportable in the database";
    public static final java.lang.String FAILURE_REASON_UNSUPPORTED_URI = "The Uri vCard composer received is not supported by the composer.";
    private static final java.lang.String LOG_TAG = "VCardComposer";
    public static final java.lang.String NO_ERROR = "No error";
    private static final java.lang.String SHIFT_JIS = "SHIFT_JIS";
    private static final java.lang.String UTF_8 = "UTF-8";
    private static final java.lang.String[] sContactsProjection = {com.amazonaws.mobileconnectors.s3.transferutility.TransferTable.COLUMN_ID};
    private static final java.util.Map<java.lang.Integer, java.lang.String> sImMap = new java.util.HashMap();
    private final java.lang.String mCharset;
    private final android.content.ContentResolver mContentResolver;
    private android.net.Uri mContentUriForRawContactsEntity;
    private android.database.Cursor mCursor;
    private boolean mCursorSuppliedFromOutside;
    private java.lang.String mErrorReason;
    private boolean mFirstVCardEmittedInDoCoMoCase;
    private int mIdColumn;
    private boolean mInitDone;
    private final boolean mIsDoCoMo;
    private com.navdy.hud.app.bluetooth.vcard.VCardPhoneNumberTranslationCallback mPhoneTranslationCallback;
    private boolean mTerminateCalled;
    private final int mVCardType;

    static {
        sImMap.put(java.lang.Integer.valueOf(0), com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_X_AIM);
        sImMap.put(java.lang.Integer.valueOf(1), com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_X_MSN);
        sImMap.put(java.lang.Integer.valueOf(2), com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_X_YAHOO);
        sImMap.put(java.lang.Integer.valueOf(6), com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_X_ICQ);
        sImMap.put(java.lang.Integer.valueOf(7), com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_X_JABBER);
        sImMap.put(java.lang.Integer.valueOf(3), com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_X_SKYPE_USERNAME);
    }

    public VCardComposer(android.content.Context context) {
        this(context, com.navdy.hud.app.bluetooth.vcard.VCardConfig.VCARD_TYPE_DEFAULT, null, true);
    }

    public VCardComposer(android.content.Context context, int vcardType) {
        this(context, vcardType, null, true);
    }

    public VCardComposer(android.content.Context context, int vcardType, java.lang.String charset) {
        this(context, vcardType, charset, true);
    }

    public VCardComposer(android.content.Context context, int vcardType, boolean careHandlerErrors) {
        this(context, vcardType, null, careHandlerErrors);
    }

    public VCardComposer(android.content.Context context, int vcardType, java.lang.String charset, boolean careHandlerErrors) {
        this(context, context.getContentResolver(), vcardType, charset, careHandlerErrors);
    }

    public VCardComposer(android.content.Context context, android.content.ContentResolver resolver, int vcardType, java.lang.String charset, boolean careHandlerErrors) {
        boolean shouldAppendCharsetParam = true;
        this.mErrorReason = NO_ERROR;
        this.mTerminateCalled = true;
        this.mVCardType = vcardType;
        this.mContentResolver = resolver;
        this.mIsDoCoMo = com.navdy.hud.app.bluetooth.vcard.VCardConfig.isDoCoMo(vcardType);
        if (android.text.TextUtils.isEmpty(charset)) {
            charset = "UTF-8";
        }
        if (com.navdy.hud.app.bluetooth.vcard.VCardConfig.isVersion30(vcardType) && "UTF-8".equalsIgnoreCase(charset)) {
            shouldAppendCharsetParam = false;
        }
        if (this.mIsDoCoMo || shouldAppendCharsetParam) {
            if (SHIFT_JIS.equalsIgnoreCase(charset)) {
                this.mCharset = charset;
            } else if (android.text.TextUtils.isEmpty(charset)) {
                this.mCharset = SHIFT_JIS;
            } else {
                this.mCharset = charset;
            }
        } else if (android.text.TextUtils.isEmpty(charset)) {
            this.mCharset = "UTF-8";
        } else {
            this.mCharset = charset;
        }
        android.util.Log.d(LOG_TAG, "Use the charset \"" + this.mCharset + "\"");
    }

    public boolean init() {
        return init(null, null);
    }

    @java.lang.Deprecated
    public boolean initWithRawContactsEntityUri(android.net.Uri contentUriForRawContactsEntity) {
        return init(android.provider.ContactsContract.Contacts.CONTENT_URI, sContactsProjection, null, null, null, contentUriForRawContactsEntity);
    }

    public boolean init(java.lang.String selection, java.lang.String[] selectionArgs) {
        return init(android.provider.ContactsContract.Contacts.CONTENT_URI, sContactsProjection, selection, selectionArgs, null, null);
    }

    public boolean init(android.net.Uri contentUri, java.lang.String selection, java.lang.String[] selectionArgs, java.lang.String sortOrder) {
        return init(contentUri, sContactsProjection, selection, selectionArgs, sortOrder, null);
    }

    public boolean init(android.net.Uri contentUri, java.lang.String selection, java.lang.String[] selectionArgs, java.lang.String sortOrder, android.net.Uri contentUriForRawContactsEntity) {
        return init(contentUri, sContactsProjection, selection, selectionArgs, sortOrder, contentUriForRawContactsEntity);
    }

    public boolean init(android.net.Uri contentUri, java.lang.String[] projection, java.lang.String selection, java.lang.String[] selectionArgs, java.lang.String sortOrder, android.net.Uri contentUriForRawContactsEntity) {
        if (!"com.android.contacts".equals(contentUri.getAuthority())) {
            this.mErrorReason = FAILURE_REASON_UNSUPPORTED_URI;
            return false;
        } else if (!initInterFirstPart(contentUriForRawContactsEntity) || !initInterCursorCreationPart(contentUri, projection, selection, selectionArgs, sortOrder) || !initInterMainPart()) {
            return false;
        } else {
            return initInterLastPart();
        }
    }

    public boolean init(android.database.Cursor cursor) {
        if (!initInterFirstPart(null)) {
            return false;
        }
        this.mCursorSuppliedFromOutside = true;
        this.mCursor = cursor;
        if (initInterMainPart()) {
            return initInterLastPart();
        }
        return false;
    }

    private boolean initInterFirstPart(android.net.Uri contentUriForRawContactsEntity) {
        if (contentUriForRawContactsEntity == null) {
            contentUriForRawContactsEntity = android.provider.ContactsContract.RawContactsEntity.CONTENT_URI;
        }
        this.mContentUriForRawContactsEntity = contentUriForRawContactsEntity;
        if (!this.mInitDone) {
            return true;
        }
        android.util.Log.e(LOG_TAG, "init() is already called");
        return false;
    }

    private boolean initInterCursorCreationPart(android.net.Uri contentUri, java.lang.String[] projection, java.lang.String selection, java.lang.String[] selectionArgs, java.lang.String sortOrder) {
        this.mCursorSuppliedFromOutside = false;
        this.mCursor = this.mContentResolver.query(contentUri, projection, selection, selectionArgs, sortOrder);
        if (this.mCursor != null) {
            return true;
        }
        android.util.Log.e(LOG_TAG, java.lang.String.format("Cursor became null unexpectedly", new java.lang.Object[0]));
        this.mErrorReason = FAILURE_REASON_FAILED_TO_GET_DATABASE_INFO;
        return false;
    }

    private boolean initInterMainPart() {
        if (this.mCursor.getCount() == 0 || !this.mCursor.moveToFirst()) {
            closeCursorIfAppropriate();
            return false;
        }
        this.mIdColumn = this.mCursor.getColumnIndex(com.amazonaws.mobileconnectors.s3.transferutility.TransferTable.COLUMN_ID);
        if (this.mIdColumn >= 0) {
            return true;
        }
        return false;
    }

    private boolean initInterLastPart() {
        this.mInitDone = true;
        this.mTerminateCalled = false;
        return true;
    }

    public java.lang.String createOneEntry() {
        return createOneEntry(null);
    }

    public java.lang.String createOneEntry(java.lang.reflect.Method getEntityIteratorMethod) {
        if (this.mIsDoCoMo && !this.mFirstVCardEmittedInDoCoMoCase) {
            this.mFirstVCardEmittedInDoCoMoCase = true;
        }
        java.lang.String vcard = createOneEntryInternal(this.mCursor.getString(this.mIdColumn), getEntityIteratorMethod);
        if (!this.mCursor.moveToNext()) {
            android.util.Log.e(LOG_TAG, "Cursor#moveToNext() returned false");
        }
        return vcard;
    }

    /* JADX WARNING: Removed duplicated region for block: B:37:0x00dc A[Catch:{ IllegalArgumentException -> 0x0049, IllegalAccessException -> 0x006e, InvocationTargetException -> 0x008c, all -> 0x0067 }] */
    private java.lang.String createOneEntryInternal(java.lang.String contactId, java.lang.reflect.Method getEntityIteratorMethod) {
        java.util.Map<java.lang.String, java.util.List<android.content.ContentValues>> contentValuesListMap = new java.util.HashMap<>();
        android.content.EntityIterator entityIterator = null;
        try {
            android.net.Uri uri = this.mContentUriForRawContactsEntity;
            java.lang.String str = "contact_id=?";
            java.lang.String[] selectionArgs = {contactId};
            if (getEntityIteratorMethod != null) {
                entityIterator = (android.content.EntityIterator) getEntityIteratorMethod.invoke(null, new java.lang.Object[]{this.mContentResolver, uri, "contact_id=?", selectionArgs, 0});
            } else {
                entityIterator = android.provider.ContactsContract.RawContacts.newEntityIterator(this.mContentResolver.query(uri, null, "contact_id=?", selectionArgs, null));
            }
        } catch (java.lang.IllegalArgumentException e) {
            android.util.Log.e(LOG_TAG, "IllegalArgumentException has been thrown: " + e.getMessage());
        } catch (java.lang.IllegalAccessException e2) {
            android.util.Log.e(LOG_TAG, "IllegalAccessException has been thrown: " + e2.getMessage());
        } catch (java.lang.reflect.InvocationTargetException e3) {
            android.util.Log.e(LOG_TAG, "InvocationTargetException has been thrown: ", e3);
            throw new java.lang.RuntimeException("InvocationTargetException has been thrown");
        } catch (Throwable th) {
            if (entityIterator != null) {
                entityIterator.close();
            }
            throw th;
        }
        if (entityIterator == null) {
            android.util.Log.e(LOG_TAG, "EntityIterator is null");
            java.lang.String str2 = "";
            if (entityIterator == null) {
                return str2;
            }
            entityIterator.close();
            return str2;
        } else if (!entityIterator.hasNext()) {
            android.util.Log.w(LOG_TAG, "Data does not exist. contactId: " + contactId);
            java.lang.String str3 = "";
            if (entityIterator == null) {
                return str3;
            }
            entityIterator.close();
            return str3;
        } else {
            while (!entityIterator.hasNext()) {
                java.util.Iterator it = ((android.content.Entity) entityIterator.next()).getSubValues().iterator();
                while (true) {
                    if (it.hasNext()) {
                        android.content.ContentValues contentValues = ((android.content.Entity.NamedContentValues) it.next()).values;
                        java.lang.String key = contentValues.getAsString("mimetype");
                        if (key != null) {
                            java.util.List<android.content.ContentValues> contentValuesList = (java.util.List) contentValuesListMap.get(key);
                            if (contentValuesList == null) {
                                contentValuesList = new java.util.ArrayList<>();
                                contentValuesListMap.put(key, contentValuesList);
                            }
                            contentValuesList.add(contentValues);
                        }
                    }
                }
                if (!entityIterator.hasNext()) {
                }
                break;
            }
            if (entityIterator != null) {
                entityIterator.close();
            }
            return buildVCard(contentValuesListMap);
        }
    }

    public void setPhoneNumberTranslationCallback(com.navdy.hud.app.bluetooth.vcard.VCardPhoneNumberTranslationCallback callback) {
        this.mPhoneTranslationCallback = callback;
    }

    public java.lang.String buildVCard(java.util.Map<java.lang.String, java.util.List<android.content.ContentValues>> contentValuesListMap) {
        if (contentValuesListMap == null) {
            android.util.Log.e(LOG_TAG, "The given map is null. Ignore and return empty String");
            return "";
        }
        com.navdy.hud.app.bluetooth.vcard.VCardBuilder builder = new com.navdy.hud.app.bluetooth.vcard.VCardBuilder(this.mVCardType, this.mCharset);
        builder.appendNameProperties((java.util.List) contentValuesListMap.get("vnd.android.cursor.item/name")).appendNickNames((java.util.List) contentValuesListMap.get("vnd.android.cursor.item/nickname")).appendPhones((java.util.List) contentValuesListMap.get("vnd.android.cursor.item/phone_v2"), this.mPhoneTranslationCallback).appendEmails((java.util.List) contentValuesListMap.get("vnd.android.cursor.item/email_v2")).appendPostals((java.util.List) contentValuesListMap.get("vnd.android.cursor.item/postal-address_v2")).appendOrganizations((java.util.List) contentValuesListMap.get("vnd.android.cursor.item/organization")).appendWebsites((java.util.List) contentValuesListMap.get("vnd.android.cursor.item/website"));
        if ((this.mVCardType & 8388608) == 0) {
            builder.appendPhotos((java.util.List) contentValuesListMap.get("vnd.android.cursor.item/photo"));
        }
        builder.appendNotes((java.util.List) contentValuesListMap.get("vnd.android.cursor.item/note")).appendEvents((java.util.List) contentValuesListMap.get("vnd.android.cursor.item/contact_event")).appendIms((java.util.List) contentValuesListMap.get("vnd.android.cursor.item/im")).appendSipAddresses((java.util.List) contentValuesListMap.get("vnd.android.cursor.item/sip_address")).appendRelation((java.util.List) contentValuesListMap.get("vnd.android.cursor.item/relation"));
        return builder.toString();
    }

    public void terminate() {
        closeCursorIfAppropriate();
        this.mTerminateCalled = true;
    }

    private void closeCursorIfAppropriate() {
        if (!this.mCursorSuppliedFromOutside && this.mCursor != null) {
            try {
                this.mCursor.close();
            } catch (android.database.sqlite.SQLiteException e) {
                android.util.Log.e(LOG_TAG, "SQLiteException on Cursor#close(): " + e.getMessage());
            }
            this.mCursor = null;
        }
    }

    /* access modifiers changed from: protected */
    public void finalize() throws java.lang.Throwable {
        try {
            if (!this.mTerminateCalled) {
                android.util.Log.e(LOG_TAG, "finalized() is called before terminate() being called");
            }
        } finally {
            super.finalize();
        }
    }

    public int getCount() {
        if (this.mCursor != null) {
            return this.mCursor.getCount();
        }
        android.util.Log.w(LOG_TAG, "This object is not ready yet.");
        return 0;
    }

    public boolean isAfterLast() {
        if (this.mCursor != null) {
            return this.mCursor.isAfterLast();
        }
        android.util.Log.w(LOG_TAG, "This object is not ready yet.");
        return false;
    }

    public java.lang.String getErrorReason() {
        return this.mErrorReason;
    }
}
