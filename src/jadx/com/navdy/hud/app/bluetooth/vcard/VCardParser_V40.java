package com.navdy.hud.app.bluetooth.vcard;

public class VCardParser_V40 extends com.navdy.hud.app.bluetooth.vcard.VCardParser {
    static final java.util.Set<java.lang.String> sAcceptableEncoding = java.util.Collections.unmodifiableSet(new java.util.HashSet(java.util.Arrays.asList(new java.lang.String[]{com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_ENCODING_8BIT, com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_ENCODING_B})));
    static final java.util.Set<java.lang.String> sKnownPropertyNameSet = java.util.Collections.unmodifiableSet(new java.util.HashSet(java.util.Arrays.asList(new java.lang.String[]{com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_BEGIN, com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_END, com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_VERSION, "SOURCE", "KIND", com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_FN, com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_N, com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_NICKNAME, com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_PHOTO, com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_BDAY, com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_ANNIVERSARY, com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_GENDER, com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_ADR, com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_TEL, com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_EMAIL, com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_IMPP, "LANG", "TZ", "GEO", com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_TITLE, com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_ROLE, com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_LOGO, com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_ORG, "MEMBER", com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_RELATED, com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_CATEGORIES, com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_NOTE, com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_PRODID, com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_REV, com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_SOUND, "UID", com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_CLIENTPIDMAP, "URL", com.navdy.hud.app.debug.DebugReceiver.EXTRA_KEY, com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_FBURL, "CALENDRURI", com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_CALURI, com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_XML})));
    private final com.navdy.hud.app.bluetooth.vcard.VCardParserImpl_V40 mVCardParserImpl;

    public VCardParser_V40() {
        this.mVCardParserImpl = new com.navdy.hud.app.bluetooth.vcard.VCardParserImpl_V40();
    }

    public VCardParser_V40(int vcardType) {
        this.mVCardParserImpl = new com.navdy.hud.app.bluetooth.vcard.VCardParserImpl_V40(vcardType);
    }

    public void addInterpreter(com.navdy.hud.app.bluetooth.vcard.VCardInterpreter interpreter) {
        this.mVCardParserImpl.addInterpreter(interpreter);
    }

    public void parse(java.io.InputStream is) throws java.io.IOException, com.navdy.hud.app.bluetooth.vcard.exception.VCardException {
        this.mVCardParserImpl.parse(is);
    }

    public void parseOne(java.io.InputStream is) throws java.io.IOException, com.navdy.hud.app.bluetooth.vcard.exception.VCardException {
        this.mVCardParserImpl.parseOne(is);
    }

    public void cancel() {
        this.mVCardParserImpl.cancel();
    }
}
