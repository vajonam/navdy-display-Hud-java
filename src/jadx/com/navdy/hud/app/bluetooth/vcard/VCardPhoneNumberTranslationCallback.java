package com.navdy.hud.app.bluetooth.vcard;

public interface VCardPhoneNumberTranslationCallback {
    java.lang.String onValueReceived(java.lang.String str, int i, java.lang.String str2, boolean z);
}
