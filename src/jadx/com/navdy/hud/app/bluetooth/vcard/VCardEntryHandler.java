package com.navdy.hud.app.bluetooth.vcard;

public interface VCardEntryHandler {
    void onEnd();

    void onEntryCreated(com.navdy.hud.app.bluetooth.vcard.VCardEntry vCardEntry);

    void onStart();
}
