package com.navdy.hud.app.bluetooth.vcard;

public class VCardEntryCommitter implements com.navdy.hud.app.bluetooth.vcard.VCardEntryHandler {
    public static java.lang.String LOG_TAG = "vCard";
    private final android.content.ContentResolver mContentResolver;
    private int mCounter;
    private final java.util.ArrayList<android.net.Uri> mCreatedUris = new java.util.ArrayList<>();
    private java.util.ArrayList<android.content.ContentProviderOperation> mOperationList;
    private long mTimeToCommit;

    public VCardEntryCommitter(android.content.ContentResolver resolver) {
        this.mContentResolver = resolver;
    }

    public void onStart() {
    }

    public void onEnd() {
        if (this.mOperationList != null) {
            this.mCreatedUris.add(pushIntoContentResolver(this.mOperationList));
        }
        if (com.navdy.hud.app.bluetooth.vcard.VCardConfig.showPerformanceLog()) {
            android.util.Log.d(LOG_TAG, java.lang.String.format("time to commit entries: %d ms", new java.lang.Object[]{java.lang.Long.valueOf(this.mTimeToCommit)}));
        }
    }

    public void onEntryCreated(com.navdy.hud.app.bluetooth.vcard.VCardEntry vcardEntry) {
        long start = java.lang.System.currentTimeMillis();
        this.mOperationList = vcardEntry.constructInsertOperations(this.mContentResolver, this.mOperationList);
        this.mCounter++;
        if (this.mCounter >= 20) {
            this.mCreatedUris.add(pushIntoContentResolver(this.mOperationList));
            this.mCounter = 0;
            this.mOperationList = null;
        }
        this.mTimeToCommit += java.lang.System.currentTimeMillis() - start;
    }

    private android.net.Uri pushIntoContentResolver(java.util.ArrayList<android.content.ContentProviderOperation> operationList) {
        try {
            android.content.ContentProviderResult[] results = this.mContentResolver.applyBatch("com.android.contacts", operationList);
            if (results == null || results.length == 0 || results[0] == null) {
                return null;
            }
            return results[0].uri;
        } catch (android.os.RemoteException e) {
            android.util.Log.e(LOG_TAG, java.lang.String.format("%s: %s", new java.lang.Object[]{e.toString(), e.getMessage()}));
            return null;
        } catch (android.content.OperationApplicationException e2) {
            android.util.Log.e(LOG_TAG, java.lang.String.format("%s: %s", new java.lang.Object[]{e2.toString(), e2.getMessage()}));
            return null;
        }
    }

    public java.util.ArrayList<android.net.Uri> getCreatedUris() {
        return this.mCreatedUris;
    }
}
