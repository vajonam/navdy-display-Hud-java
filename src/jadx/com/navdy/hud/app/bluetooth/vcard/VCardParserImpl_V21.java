package com.navdy.hud.app.bluetooth.vcard;

class VCardParserImpl_V21 {
    private static final java.lang.String DEFAULT_CHARSET = "UTF-8";
    private static final java.lang.String DEFAULT_ENCODING = "8BIT";
    private static final java.lang.String LOG_TAG = "vCard";
    private static final int STATE_GROUP_OR_PROPERTY_NAME = 0;
    private static final int STATE_PARAMS = 1;
    private static final int STATE_PARAMS_IN_DQUOTE = 2;
    private boolean mCanceled;
    protected java.lang.String mCurrentCharset;
    protected java.lang.String mCurrentEncoding;
    protected final java.lang.String mIntermediateCharset;
    private final java.util.List<com.navdy.hud.app.bluetooth.vcard.VCardInterpreter> mInterpreterList;
    protected com.navdy.hud.app.bluetooth.vcard.VCardParserImpl_V21.CustomBufferedReader mReader;
    protected final java.util.Set<java.lang.String> mUnknownTypeSet;
    protected final java.util.Set<java.lang.String> mUnknownValueSet;

    protected static final class CustomBufferedReader extends java.io.BufferedReader {
        private java.lang.String mNextLine;
        private boolean mNextLineIsValid;
        private long mTime;

        public CustomBufferedReader(java.io.Reader in) {
            super(in);
        }

        public java.lang.String readLine() throws java.io.IOException {
            if (this.mNextLineIsValid) {
                java.lang.String ret = this.mNextLine;
                this.mNextLine = null;
                this.mNextLineIsValid = false;
                return ret;
            }
            long start = java.lang.System.currentTimeMillis();
            java.lang.String line = super.readLine();
            this.mTime += java.lang.System.currentTimeMillis() - start;
            return line;
        }

        public java.lang.String peekLine() throws java.io.IOException {
            if (!this.mNextLineIsValid) {
                long start = java.lang.System.currentTimeMillis();
                java.lang.String line = super.readLine();
                this.mTime += java.lang.System.currentTimeMillis() - start;
                this.mNextLine = line;
                this.mNextLineIsValid = true;
            }
            return this.mNextLine;
        }

        public long getTotalmillisecond() {
            return this.mTime;
        }
    }

    public VCardParserImpl_V21() {
        this(com.navdy.hud.app.bluetooth.vcard.VCardConfig.VCARD_TYPE_DEFAULT);
    }

    public VCardParserImpl_V21(int vcardType) {
        this.mInterpreterList = new java.util.ArrayList();
        this.mUnknownTypeSet = new java.util.HashSet();
        this.mUnknownValueSet = new java.util.HashSet();
        this.mIntermediateCharset = com.navdy.hud.app.bluetooth.vcard.VCardConfig.DEFAULT_INTERMEDIATE_CHARSET;
    }

    /* access modifiers changed from: protected */
    public boolean isValidPropertyName(java.lang.String propertyName) {
        if (!getKnownPropertyNameSet().contains(propertyName.toUpperCase()) && !propertyName.startsWith("X-") && !this.mUnknownTypeSet.contains(propertyName)) {
            this.mUnknownTypeSet.add(propertyName);
            android.util.Log.w(LOG_TAG, "Property name unsupported by vCard 2.1: " + propertyName);
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public java.lang.String getLine() throws java.io.IOException {
        return this.mReader.readLine();
    }

    /* access modifiers changed from: protected */
    public java.lang.String peekLine() throws java.io.IOException {
        return this.mReader.peekLine();
    }

    /* access modifiers changed from: protected */
    public java.lang.String getNonEmptyLine() throws java.io.IOException, com.navdy.hud.app.bluetooth.vcard.exception.VCardException {
        java.lang.String line;
        do {
            line = getLine();
            if (line == null) {
                throw new com.navdy.hud.app.bluetooth.vcard.exception.VCardException("Reached end of buffer.");
            }
        } while (line.trim().length() <= 0);
        return line;
    }

    private boolean parseOneVCard() throws java.io.IOException, com.navdy.hud.app.bluetooth.vcard.exception.VCardException {
        this.mCurrentEncoding = "8BIT";
        this.mCurrentCharset = "UTF-8";
        if (!readBeginVCard(false)) {
            return false;
        }
        for (com.navdy.hud.app.bluetooth.vcard.VCardInterpreter interpreter : this.mInterpreterList) {
            interpreter.onEntryStarted();
        }
        parseItems();
        for (com.navdy.hud.app.bluetooth.vcard.VCardInterpreter interpreter2 : this.mInterpreterList) {
            interpreter2.onEntryEnded();
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean readBeginVCard(boolean allowGarbage) throws java.io.IOException, com.navdy.hud.app.bluetooth.vcard.exception.VCardException {
        while (true) {
            java.lang.String line = getLine();
            if (line == null) {
                return false;
            }
            if (line.trim().length() > 0) {
                java.lang.String[] strArray = line.split(com.navdy.hud.app.framework.glance.GlanceConstants.COLON_SEPARATOR, 2);
                if (strArray.length == 2 && strArray[0].trim().equalsIgnoreCase(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_BEGIN) && strArray[1].trim().equalsIgnoreCase("VCARD")) {
                    return true;
                }
                if (!allowGarbage) {
                    throw new com.navdy.hud.app.bluetooth.vcard.exception.VCardException("Expected String \"BEGIN:VCARD\" did not come (Instead, \"" + line + "\" came)");
                } else if (!allowGarbage) {
                    throw new com.navdy.hud.app.bluetooth.vcard.exception.VCardException("Reached where must not be reached.");
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void parseItems() throws java.io.IOException, com.navdy.hud.app.bluetooth.vcard.exception.VCardException {
        boolean ended = false;
        try {
            ended = parseItem();
        } catch (com.navdy.hud.app.bluetooth.vcard.exception.VCardInvalidCommentLineException e) {
            android.util.Log.e(LOG_TAG, "Invalid line which looks like some comment was found. Ignored.");
        }
        while (!ended) {
            try {
                ended = parseItem();
            } catch (com.navdy.hud.app.bluetooth.vcard.exception.VCardInvalidCommentLineException e2) {
                android.util.Log.e(LOG_TAG, "Invalid line which looks like some comment was found. Ignored.");
            }
        }
    }

    /* access modifiers changed from: protected */
    public boolean parseItem() throws java.io.IOException, com.navdy.hud.app.bluetooth.vcard.exception.VCardException {
        this.mCurrentEncoding = "8BIT";
        com.navdy.hud.app.bluetooth.vcard.VCardProperty propertyData = constructPropertyData(getNonEmptyLine());
        java.lang.String propertyNameUpper = propertyData.getName().toUpperCase();
        java.lang.String propertyRawValue = propertyData.getRawValue();
        if (propertyNameUpper.equals(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_BEGIN)) {
            if (propertyRawValue.equalsIgnoreCase("VCARD")) {
                handleNest();
            } else {
                throw new com.navdy.hud.app.bluetooth.vcard.exception.VCardException("Unknown BEGIN type: " + propertyRawValue);
            }
        } else if (!propertyNameUpper.equals(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_END)) {
            parseItemInter(propertyData, propertyNameUpper);
        } else if (propertyRawValue.equalsIgnoreCase("VCARD")) {
            return true;
        } else {
            throw new com.navdy.hud.app.bluetooth.vcard.exception.VCardException("Unknown END type: " + propertyRawValue);
        }
        return false;
    }

    private void parseItemInter(com.navdy.hud.app.bluetooth.vcard.VCardProperty property, java.lang.String propertyNameUpper) throws java.io.IOException, com.navdy.hud.app.bluetooth.vcard.exception.VCardException {
        java.lang.String propertyRawValue = property.getRawValue();
        if (propertyNameUpper.equals(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_AGENT)) {
            handleAgent(property);
        } else if (!isValidPropertyName(propertyNameUpper)) {
            throw new com.navdy.hud.app.bluetooth.vcard.exception.VCardException("Unknown property name: \"" + propertyNameUpper + "\"");
        } else if (!propertyNameUpper.equals(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_VERSION) || propertyRawValue.equals(getVersionString())) {
            handlePropertyValue(property, propertyNameUpper);
        } else {
            throw new com.navdy.hud.app.bluetooth.vcard.exception.VCardVersionException("Incompatible version: " + propertyRawValue + " != " + getVersionString());
        }
    }

    private void handleNest() throws java.io.IOException, com.navdy.hud.app.bluetooth.vcard.exception.VCardException {
        for (com.navdy.hud.app.bluetooth.vcard.VCardInterpreter interpreter : this.mInterpreterList) {
            interpreter.onEntryStarted();
        }
        parseItems();
        for (com.navdy.hud.app.bluetooth.vcard.VCardInterpreter interpreter2 : this.mInterpreterList) {
            interpreter2.onEntryEnded();
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x002c, code lost:
        r2 = r2 + 1;
     */
    public com.navdy.hud.app.bluetooth.vcard.VCardProperty constructPropertyData(java.lang.String line) throws com.navdy.hud.app.bluetooth.vcard.exception.VCardException {
        java.lang.String str;
        com.navdy.hud.app.bluetooth.vcard.VCardProperty propertyData = new com.navdy.hud.app.bluetooth.vcard.VCardProperty();
        int length = line.length();
        if (length <= 0 || line.charAt(0) != '#') {
            int state = 0;
            int nameIndex = 0;
            int i = 0;
            while (i < length) {
                char ch = line.charAt(i);
                switch (state) {
                    case 0:
                        if (ch != ':') {
                            if (ch != '.') {
                                if (ch != ';') {
                                    break;
                                } else {
                                    propertyData.setName(line.substring(nameIndex, i));
                                    nameIndex = i + 1;
                                    state = 1;
                                    break;
                                }
                            } else {
                                java.lang.String groupName = line.substring(nameIndex, i);
                                if (groupName.length() == 0) {
                                    android.util.Log.w(LOG_TAG, "Empty group found. Ignoring.");
                                } else {
                                    propertyData.addGroup(groupName);
                                }
                                nameIndex = i + 1;
                                continue;
                            }
                        } else {
                            propertyData.setName(line.substring(nameIndex, i));
                            if (i < length - 1) {
                                str = line.substring(i + 1);
                            } else {
                                str = "";
                            }
                            propertyData.setRawValue(str);
                            break;
                        }
                    case 1:
                        if (ch != '\"') {
                            if (ch != ';') {
                                if (ch != ':') {
                                    break;
                                } else {
                                    handleParams(propertyData, line.substring(nameIndex, i));
                                    propertyData.setRawValue(i < length + -1 ? line.substring(i + 1) : "");
                                    break;
                                }
                            } else {
                                handleParams(propertyData, line.substring(nameIndex, i));
                                nameIndex = i + 1;
                                break;
                            }
                        } else {
                            if (com.navdy.hud.app.bluetooth.vcard.VCardConstants.VERSION_V21.equalsIgnoreCase(getVersionString())) {
                                android.util.Log.w(LOG_TAG, "Double-quoted params found in vCard 2.1. Silently allow it");
                            }
                            state = 2;
                            continue;
                        }
                    case 2:
                        if (ch == '\"') {
                            if (com.navdy.hud.app.bluetooth.vcard.VCardConstants.VERSION_V21.equalsIgnoreCase(getVersionString())) {
                                android.util.Log.w(LOG_TAG, "Double-quoted params found in vCard 2.1. Silently allow it");
                            }
                            state = 1;
                            break;
                        } else {
                            continue;
                        }
                }
                return propertyData;
            }
            throw new com.navdy.hud.app.bluetooth.vcard.exception.VCardInvalidLineException("Invalid line: \"" + line + "\"");
        }
        throw new com.navdy.hud.app.bluetooth.vcard.exception.VCardInvalidCommentLineException();
    }

    /* access modifiers changed from: protected */
    public void handleParams(com.navdy.hud.app.bluetooth.vcard.VCardProperty propertyData, java.lang.String params) throws com.navdy.hud.app.bluetooth.vcard.exception.VCardException {
        java.lang.String[] strArray = params.split("=", 2);
        if (strArray.length == 2) {
            java.lang.String paramName = strArray[0].trim().toUpperCase();
            java.lang.String paramValue = strArray[1].trim();
            if (paramName.equals(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE)) {
                handleType(propertyData, paramValue);
            } else if (paramName.equals(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_VALUE)) {
                handleValue(propertyData, paramValue);
            } else if (paramName.equals(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_ENCODING)) {
                handleEncoding(propertyData, paramValue.toUpperCase());
            } else if (paramName.equals(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_CHARSET)) {
                handleCharset(propertyData, paramValue);
            } else if (paramName.equals(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_LANGUAGE)) {
                handleLanguage(propertyData, paramValue);
            } else if (paramName.startsWith("X-")) {
                handleAnyParam(propertyData, paramName, paramValue);
            } else {
                throw new com.navdy.hud.app.bluetooth.vcard.exception.VCardException("Unknown type \"" + paramName + "\"");
            }
        } else {
            handleParamWithoutName(propertyData, strArray[0]);
        }
    }

    /* access modifiers changed from: protected */
    public void handleParamWithoutName(com.navdy.hud.app.bluetooth.vcard.VCardProperty propertyData, java.lang.String paramValue) {
        handleType(propertyData, paramValue);
    }

    /* access modifiers changed from: protected */
    public void handleType(com.navdy.hud.app.bluetooth.vcard.VCardProperty propertyData, java.lang.String ptypeval) {
        if (!getKnownTypeSet().contains(ptypeval.toUpperCase()) && !ptypeval.startsWith("X-") && !this.mUnknownTypeSet.contains(ptypeval)) {
            this.mUnknownTypeSet.add(ptypeval);
            android.util.Log.w(LOG_TAG, java.lang.String.format("TYPE unsupported by %s: ", new java.lang.Object[]{java.lang.Integer.valueOf(getVersion()), ptypeval}));
        }
        propertyData.addParameter(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE, ptypeval);
    }

    /* access modifiers changed from: protected */
    public void handleValue(com.navdy.hud.app.bluetooth.vcard.VCardProperty propertyData, java.lang.String pvalueval) {
        if (!getKnownValueSet().contains(pvalueval.toUpperCase()) && !pvalueval.startsWith("X-") && !this.mUnknownValueSet.contains(pvalueval)) {
            this.mUnknownValueSet.add(pvalueval);
            android.util.Log.w(LOG_TAG, java.lang.String.format("The value unsupported by TYPE of %s: ", new java.lang.Object[]{java.lang.Integer.valueOf(getVersion()), pvalueval}));
        }
        propertyData.addParameter(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_VALUE, pvalueval);
    }

    /* access modifiers changed from: protected */
    public void handleEncoding(com.navdy.hud.app.bluetooth.vcard.VCardProperty propertyData, java.lang.String pencodingval) throws com.navdy.hud.app.bluetooth.vcard.exception.VCardException {
        if (getAvailableEncodingSet().contains(pencodingval) || pencodingval.startsWith("X-")) {
            propertyData.addParameter(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_ENCODING, pencodingval);
            this.mCurrentEncoding = pencodingval.toUpperCase();
            return;
        }
        throw new com.navdy.hud.app.bluetooth.vcard.exception.VCardException("Unknown encoding \"" + pencodingval + "\"");
    }

    /* access modifiers changed from: protected */
    public void handleCharset(com.navdy.hud.app.bluetooth.vcard.VCardProperty propertyData, java.lang.String charsetval) {
        this.mCurrentCharset = charsetval;
        propertyData.addParameter(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_CHARSET, charsetval);
    }

    /* access modifiers changed from: protected */
    public void handleLanguage(com.navdy.hud.app.bluetooth.vcard.VCardProperty propertyData, java.lang.String langval) throws com.navdy.hud.app.bluetooth.vcard.exception.VCardException {
        java.lang.String[] strArray = langval.split("-");
        if (strArray.length != 2) {
            throw new com.navdy.hud.app.bluetooth.vcard.exception.VCardException("Invalid Language: \"" + langval + "\"");
        }
        java.lang.String tmp = strArray[0];
        int length = tmp.length();
        for (int i = 0; i < length; i++) {
            if (!isAsciiLetter(tmp.charAt(i))) {
                throw new com.navdy.hud.app.bluetooth.vcard.exception.VCardException("Invalid Language: \"" + langval + "\"");
            }
        }
        java.lang.String tmp2 = strArray[1];
        int length2 = tmp2.length();
        for (int i2 = 0; i2 < length2; i2++) {
            if (!isAsciiLetter(tmp2.charAt(i2))) {
                throw new com.navdy.hud.app.bluetooth.vcard.exception.VCardException("Invalid Language: \"" + langval + "\"");
            }
        }
        propertyData.addParameter(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_LANGUAGE, langval);
    }

    private boolean isAsciiLetter(char ch) {
        if ((ch < 'a' || ch > 'z') && (ch < 'A' || ch > 'Z')) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void handleAnyParam(com.navdy.hud.app.bluetooth.vcard.VCardProperty propertyData, java.lang.String paramName, java.lang.String paramValue) {
        propertyData.addParameter(paramName, paramValue);
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x01f5  */
    public void handlePropertyValue(com.navdy.hud.app.bluetooth.vcard.VCardProperty property, java.lang.String propertyName) throws java.io.IOException, com.navdy.hud.app.bluetooth.vcard.exception.VCardException {
        java.lang.String propertyNameUpper = property.getName().toUpperCase();
        java.lang.String propertyRawValue = property.getRawValue();
        java.lang.String str = com.navdy.hud.app.bluetooth.vcard.VCardConfig.DEFAULT_INTERMEDIATE_CHARSET;
        java.util.Collection<java.lang.String> charsetCollection = property.getParameters(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_CHARSET);
        java.lang.String targetCharset = charsetCollection != null ? (java.lang.String) charsetCollection.iterator().next() : null;
        if (android.text.TextUtils.isEmpty(targetCharset)) {
            targetCharset = "UTF-8";
        }
        if (propertyNameUpper.equals(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_ADR) || propertyNameUpper.equals(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_ORG) || propertyNameUpper.equals(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_N)) {
            handleAdrOrgN(property, propertyRawValue, com.navdy.hud.app.bluetooth.vcard.VCardConfig.DEFAULT_INTERMEDIATE_CHARSET, targetCharset);
        } else if (this.mCurrentEncoding.equals(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_ENCODING_QP) || (propertyNameUpper.equals(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_FN) && property.getParameters(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_ENCODING) == null && com.navdy.hud.app.bluetooth.vcard.VCardUtils.appearsLikeAndroidVCardQuotedPrintable(propertyRawValue))) {
            java.lang.String quotedPrintablePart = getQuotedPrintablePart(propertyRawValue);
            java.lang.String propertyEncodedValue = com.navdy.hud.app.bluetooth.vcard.VCardUtils.parseQuotedPrintable(quotedPrintablePart, false, com.navdy.hud.app.bluetooth.vcard.VCardConfig.DEFAULT_INTERMEDIATE_CHARSET, targetCharset);
            property.setRawValue(quotedPrintablePart);
            property.setValues(propertyEncodedValue);
            for (com.navdy.hud.app.bluetooth.vcard.VCardInterpreter interpreter : this.mInterpreterList) {
                interpreter.onPropertyCreated(property);
            }
        } else if (this.mCurrentEncoding.equals(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_ENCODING_BASE64) || this.mCurrentEncoding.equals(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_ENCODING_B)) {
            try {
                property.setByteValue(android.util.Base64.decode(getBase64(propertyRawValue), 0));
                for (com.navdy.hud.app.bluetooth.vcard.VCardInterpreter interpreter2 : this.mInterpreterList) {
                    interpreter2.onPropertyCreated(property);
                }
            } catch (java.lang.IllegalArgumentException e) {
                throw new com.navdy.hud.app.bluetooth.vcard.exception.VCardException("Decode error on base64 photo: " + propertyRawValue);
            } catch (java.lang.OutOfMemoryError e2) {
                android.util.Log.e(LOG_TAG, "OutOfMemoryError happened during parsing BASE64 data!");
                for (com.navdy.hud.app.bluetooth.vcard.VCardInterpreter interpreter3 : this.mInterpreterList) {
                    interpreter3.onPropertyCreated(property);
                }
            }
        } else {
            if (!this.mCurrentEncoding.equals(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_ENCODING_7BIT) && !this.mCurrentEncoding.equals("8BIT") && !this.mCurrentEncoding.startsWith("X-")) {
                android.util.Log.w(LOG_TAG, java.lang.String.format("The encoding \"%s\" is unsupported by vCard %s", new java.lang.Object[]{this.mCurrentEncoding, getVersionString()}));
            }
            if (getVersion() == 0) {
                java.lang.StringBuilder builder = null;
                while (true) {
                    java.lang.String nextLine = peekLine();
                    if (!android.text.TextUtils.isEmpty(nextLine) && nextLine.charAt(0) == ' ' && !"END:VCARD".contains(nextLine.toUpperCase())) {
                        getLine();
                        if (builder == null) {
                            builder = new java.lang.StringBuilder();
                            builder.append(propertyRawValue);
                        }
                        builder.append(nextLine.substring(1));
                    } else if (builder != null) {
                        propertyRawValue = builder.toString();
                    }
                }
                if (builder != null) {
                }
            }
            java.util.ArrayList<java.lang.String> propertyValueList = new java.util.ArrayList<>();
            propertyValueList.add(com.navdy.hud.app.bluetooth.vcard.VCardUtils.convertStringCharset(maybeUnescapeText(propertyRawValue), com.navdy.hud.app.bluetooth.vcard.VCardConfig.DEFAULT_INTERMEDIATE_CHARSET, targetCharset));
            property.setValues((java.util.List<java.lang.String>) propertyValueList);
            for (com.navdy.hud.app.bluetooth.vcard.VCardInterpreter interpreter4 : this.mInterpreterList) {
                interpreter4.onPropertyCreated(property);
            }
        }
    }

    private void handleAdrOrgN(com.navdy.hud.app.bluetooth.vcard.VCardProperty property, java.lang.String propertyRawValue, java.lang.String sourceCharset, java.lang.String targetCharset) throws com.navdy.hud.app.bluetooth.vcard.exception.VCardException, java.io.IOException {
        java.util.List<java.lang.String> encodedValueList = new java.util.ArrayList<>();
        if (this.mCurrentEncoding.equals(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_ENCODING_QP)) {
            java.lang.String quotedPrintablePart = getQuotedPrintablePart(propertyRawValue);
            property.setRawValue(quotedPrintablePart);
            for (java.lang.String quotedPrintableValue : com.navdy.hud.app.bluetooth.vcard.VCardUtils.constructListFromValue(quotedPrintablePart, getVersion())) {
                encodedValueList.add(com.navdy.hud.app.bluetooth.vcard.VCardUtils.parseQuotedPrintable(quotedPrintableValue, false, sourceCharset, targetCharset));
            }
        } else {
            for (java.lang.String rawValue : com.navdy.hud.app.bluetooth.vcard.VCardUtils.constructListFromValue(getPotentialMultiline(propertyRawValue), getVersion())) {
                encodedValueList.add(com.navdy.hud.app.bluetooth.vcard.VCardUtils.convertStringCharset(rawValue, sourceCharset, targetCharset));
            }
        }
        property.setValues(encodedValueList);
        for (com.navdy.hud.app.bluetooth.vcard.VCardInterpreter interpreter : this.mInterpreterList) {
            interpreter.onPropertyCreated(property);
        }
    }

    private java.lang.String getQuotedPrintablePart(java.lang.String firstString) throws java.io.IOException, com.navdy.hud.app.bluetooth.vcard.exception.VCardException {
        if (!firstString.trim().endsWith("=")) {
            return firstString;
        }
        int pos = firstString.length() - 1;
        do {
        } while (firstString.charAt(pos) != '=');
        java.lang.StringBuilder builder = new java.lang.StringBuilder();
        builder.append(firstString.substring(0, pos + 1));
        builder.append(com.navdy.hud.app.bluetooth.vcard.VCardBuilder.VCARD_END_OF_LINE);
        while (true) {
            java.lang.String line = getLine();
            if (line == null) {
                throw new com.navdy.hud.app.bluetooth.vcard.exception.VCardException("File ended during parsing a Quoted-Printable String");
            } else if (line.trim().endsWith("=")) {
                int pos2 = line.length() - 1;
                do {
                } while (line.charAt(pos2) != '=');
                builder.append(line.substring(0, pos2 + 1));
                builder.append(com.navdy.hud.app.bluetooth.vcard.VCardBuilder.VCARD_END_OF_LINE);
            } else {
                builder.append(line);
                return builder.toString();
            }
        }
    }

    private java.lang.String getPotentialMultiline(java.lang.String firstString) throws java.io.IOException {
        java.lang.StringBuilder builder = new java.lang.StringBuilder();
        builder.append(firstString);
        while (true) {
            java.lang.String line = peekLine();
            if (line != null && line.length() != 0 && getPropertyNameUpperCase(line) == null) {
                getLine();
                builder.append(" ").append(line);
            }
        }
        return builder.toString();
    }

    /* access modifiers changed from: protected */
    public java.lang.String getBase64(java.lang.String firstString) throws java.io.IOException, com.navdy.hud.app.bluetooth.vcard.exception.VCardException {
        java.lang.StringBuilder builder = new java.lang.StringBuilder();
        builder.append(firstString);
        while (true) {
            java.lang.String line = peekLine();
            if (line != null) {
                java.lang.String propertyName = getPropertyNameUpperCase(line);
                if (!getKnownPropertyNameSet().contains(propertyName) && !com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_X_ANDROID_CUSTOM.equals(propertyName)) {
                    getLine();
                    if (line.length() == 0) {
                        break;
                    }
                    builder.append(line.trim());
                } else {
                    android.util.Log.w(LOG_TAG, "Found a next property during parsing a BASE64 string, which must not contain semi-colon or colon. Treat the line as next property.");
                    android.util.Log.w(LOG_TAG, "Problematic line: " + line.trim());
                }
            } else {
                throw new com.navdy.hud.app.bluetooth.vcard.exception.VCardException("File ended during parsing BASE64 binary");
            }
        }
        return builder.toString();
    }

    private java.lang.String getPropertyNameUpperCase(java.lang.String line) {
        int minIndex;
        int colonIndex = line.indexOf(com.navdy.hud.app.framework.glance.GlanceConstants.COLON_SEPARATOR);
        if (colonIndex <= -1) {
            return null;
        }
        int semiColonIndex = line.indexOf(";");
        if (colonIndex == -1) {
            minIndex = semiColonIndex;
        } else if (semiColonIndex == -1) {
            minIndex = colonIndex;
        } else {
            minIndex = java.lang.Math.min(colonIndex, semiColonIndex);
        }
        return line.substring(0, minIndex).toUpperCase();
    }

    /* access modifiers changed from: protected */
    public void handleAgent(com.navdy.hud.app.bluetooth.vcard.VCardProperty property) throws com.navdy.hud.app.bluetooth.vcard.exception.VCardException {
        if (!property.getRawValue().toUpperCase().contains("BEGIN:VCARD")) {
            for (com.navdy.hud.app.bluetooth.vcard.VCardInterpreter interpreter : this.mInterpreterList) {
                interpreter.onPropertyCreated(property);
            }
            return;
        }
        throw new com.navdy.hud.app.bluetooth.vcard.exception.VCardAgentNotSupportedException("AGENT Property is not supported now.");
    }

    /* access modifiers changed from: protected */
    public java.lang.String maybeUnescapeText(java.lang.String text) {
        return text;
    }

    /* access modifiers changed from: protected */
    public java.lang.String maybeUnescapeCharacter(char ch) {
        return unescapeCharacter(ch);
    }

    static java.lang.String unescapeCharacter(char ch) {
        if (ch == '\\' || ch == ';' || ch == ':' || ch == ',') {
            return java.lang.String.valueOf(ch);
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public int getVersion() {
        return 0;
    }

    /* access modifiers changed from: protected */
    public java.lang.String getVersionString() {
        return com.navdy.hud.app.bluetooth.vcard.VCardConstants.VERSION_V21;
    }

    /* access modifiers changed from: protected */
    public java.util.Set<java.lang.String> getKnownPropertyNameSet() {
        return com.navdy.hud.app.bluetooth.vcard.VCardParser_V21.sKnownPropertyNameSet;
    }

    /* access modifiers changed from: protected */
    public java.util.Set<java.lang.String> getKnownTypeSet() {
        return com.navdy.hud.app.bluetooth.vcard.VCardParser_V21.sKnownTypeSet;
    }

    /* access modifiers changed from: protected */
    public java.util.Set<java.lang.String> getKnownValueSet() {
        return com.navdy.hud.app.bluetooth.vcard.VCardParser_V21.sKnownValueSet;
    }

    /* access modifiers changed from: protected */
    public java.util.Set<java.lang.String> getAvailableEncodingSet() {
        return com.navdy.hud.app.bluetooth.vcard.VCardParser_V21.sAvailableEncoding;
    }

    /* access modifiers changed from: protected */
    public java.lang.String getDefaultEncoding() {
        return "8BIT";
    }

    /* access modifiers changed from: protected */
    public java.lang.String getDefaultCharset() {
        return "UTF-8";
    }

    /* access modifiers changed from: protected */
    public java.lang.String getCurrentCharset() {
        return this.mCurrentCharset;
    }

    public void addInterpreter(com.navdy.hud.app.bluetooth.vcard.VCardInterpreter interpreter) {
        this.mInterpreterList.add(interpreter);
    }

    public void parse(java.io.InputStream is) throws java.io.IOException, com.navdy.hud.app.bluetooth.vcard.exception.VCardException {
        if (is == null) {
            throw new java.lang.NullPointerException("InputStream must not be null.");
        }
        this.mReader = new com.navdy.hud.app.bluetooth.vcard.VCardParserImpl_V21.CustomBufferedReader(new java.io.InputStreamReader(is, this.mIntermediateCharset));
        long currentTimeMillis = java.lang.System.currentTimeMillis();
        for (com.navdy.hud.app.bluetooth.vcard.VCardInterpreter interpreter : this.mInterpreterList) {
            interpreter.onVCardStarted();
        }
        while (true) {
            synchronized (this) {
                if (!this.mCanceled) {
                    if (!parseOneVCard()) {
                        break;
                    }
                } else {
                    android.util.Log.i(LOG_TAG, "Cancel request has come. exitting parse operation.");
                    break;
                }
            }
        }
        for (com.navdy.hud.app.bluetooth.vcard.VCardInterpreter interpreter2 : this.mInterpreterList) {
            interpreter2.onVCardEnded();
        }
    }

    public void parseOne(java.io.InputStream is) throws java.io.IOException, com.navdy.hud.app.bluetooth.vcard.exception.VCardException {
        if (is == null) {
            throw new java.lang.NullPointerException("InputStream must not be null.");
        }
        this.mReader = new com.navdy.hud.app.bluetooth.vcard.VCardParserImpl_V21.CustomBufferedReader(new java.io.InputStreamReader(is, this.mIntermediateCharset));
        long currentTimeMillis = java.lang.System.currentTimeMillis();
        for (com.navdy.hud.app.bluetooth.vcard.VCardInterpreter interpreter : this.mInterpreterList) {
            interpreter.onVCardStarted();
        }
        parseOneVCard();
        for (com.navdy.hud.app.bluetooth.vcard.VCardInterpreter interpreter2 : this.mInterpreterList) {
            interpreter2.onVCardEnded();
        }
    }

    public final synchronized void cancel() {
        android.util.Log.i(LOG_TAG, "ParserImpl received cancel operation.");
        this.mCanceled = true;
    }
}
