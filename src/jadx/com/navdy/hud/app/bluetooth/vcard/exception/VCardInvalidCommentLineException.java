package com.navdy.hud.app.bluetooth.vcard.exception;

public class VCardInvalidCommentLineException extends com.navdy.hud.app.bluetooth.vcard.exception.VCardInvalidLineException {
    public VCardInvalidCommentLineException() {
    }

    public VCardInvalidCommentLineException(java.lang.String message) {
        super(message);
    }
}
