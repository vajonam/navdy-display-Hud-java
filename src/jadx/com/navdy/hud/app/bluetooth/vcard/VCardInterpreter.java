package com.navdy.hud.app.bluetooth.vcard;

public interface VCardInterpreter {
    void onEntryEnded();

    void onEntryStarted();

    void onPropertyCreated(com.navdy.hud.app.bluetooth.vcard.VCardProperty vCardProperty);

    void onVCardEnded();

    void onVCardStarted();
}
