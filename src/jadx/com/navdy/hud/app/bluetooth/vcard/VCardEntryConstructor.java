package com.navdy.hud.app.bluetooth.vcard;

public class VCardEntryConstructor implements com.navdy.hud.app.bluetooth.vcard.VCardInterpreter {
    private static java.lang.String LOG_TAG = "vCard";
    private final android.accounts.Account mAccount;
    private com.navdy.hud.app.bluetooth.vcard.VCardEntry mCurrentEntry;
    private final java.util.List<com.navdy.hud.app.bluetooth.vcard.VCardEntryHandler> mEntryHandlers;
    private final java.util.List<com.navdy.hud.app.bluetooth.vcard.VCardEntry> mEntryStack;
    private final int mVCardType;

    public VCardEntryConstructor() {
        this(com.navdy.hud.app.bluetooth.vcard.VCardConfig.VCARD_TYPE_V21_GENERIC, null, null);
    }

    public VCardEntryConstructor(int vcardType) {
        this(vcardType, null, null);
    }

    public VCardEntryConstructor(int vcardType, android.accounts.Account account) {
        this(vcardType, account, null);
    }

    @java.lang.Deprecated
    public VCardEntryConstructor(int vcardType, android.accounts.Account account, java.lang.String targetCharset) {
        this.mEntryStack = new java.util.ArrayList();
        this.mEntryHandlers = new java.util.ArrayList();
        this.mVCardType = vcardType;
        this.mAccount = account;
    }

    public void addEntryHandler(com.navdy.hud.app.bluetooth.vcard.VCardEntryHandler entryHandler) {
        this.mEntryHandlers.add(entryHandler);
    }

    public void onVCardStarted() {
        for (com.navdy.hud.app.bluetooth.vcard.VCardEntryHandler entryHandler : this.mEntryHandlers) {
            entryHandler.onStart();
        }
    }

    public void onVCardEnded() {
        for (com.navdy.hud.app.bluetooth.vcard.VCardEntryHandler entryHandler : this.mEntryHandlers) {
            entryHandler.onEnd();
        }
    }

    public void clear() {
        this.mCurrentEntry = null;
        this.mEntryStack.clear();
    }

    public void onEntryStarted() {
        this.mCurrentEntry = new com.navdy.hud.app.bluetooth.vcard.VCardEntry(this.mVCardType, this.mAccount);
        this.mEntryStack.add(this.mCurrentEntry);
    }

    public void onEntryEnded() {
        this.mCurrentEntry.consolidateFields();
        for (com.navdy.hud.app.bluetooth.vcard.VCardEntryHandler entryHandler : this.mEntryHandlers) {
            entryHandler.onEntryCreated(this.mCurrentEntry);
        }
        int size = this.mEntryStack.size();
        if (size > 1) {
            com.navdy.hud.app.bluetooth.vcard.VCardEntry parent = (com.navdy.hud.app.bluetooth.vcard.VCardEntry) this.mEntryStack.get(size - 2);
            parent.addChild(this.mCurrentEntry);
            this.mCurrentEntry = parent;
        } else {
            this.mCurrentEntry = null;
        }
        this.mEntryStack.remove(size - 1);
    }

    public void onPropertyCreated(com.navdy.hud.app.bluetooth.vcard.VCardProperty property) {
        this.mCurrentEntry.addProperty(property);
    }
}
