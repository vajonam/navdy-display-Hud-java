package com.navdy.hud.app.bluetooth.vcard.exception;

public class VCardNestedException extends com.navdy.hud.app.bluetooth.vcard.exception.VCardNotSupportedException {
    public VCardNestedException() {
    }

    public VCardNestedException(java.lang.String message) {
        super(message);
    }
}
