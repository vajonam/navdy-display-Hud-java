package com.navdy.hud.app.bluetooth.vcard;

public class VCardParser_V30 extends com.navdy.hud.app.bluetooth.vcard.VCardParser {
    static final java.util.Set<java.lang.String> sAcceptableEncoding = java.util.Collections.unmodifiableSet(new java.util.HashSet(java.util.Arrays.asList(new java.lang.String[]{com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_ENCODING_7BIT, com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_ENCODING_8BIT, com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_ENCODING_BASE64, com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_ENCODING_B})));
    static final java.util.Set<java.lang.String> sKnownPropertyNameSet = java.util.Collections.unmodifiableSet(new java.util.HashSet(java.util.Arrays.asList(new java.lang.String[]{com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_BEGIN, com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_END, com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_LOGO, com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_PHOTO, "LABEL", com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_FN, com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_TITLE, com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_SOUND, com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_VERSION, com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_TEL, com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_EMAIL, "TZ", "GEO", com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_NOTE, "URL", com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_BDAY, com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_ROLE, com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_REV, "UID", com.navdy.hud.app.debug.DebugReceiver.EXTRA_KEY, "MAILER", com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_NAME, "PROFILE", "SOURCE", com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_NICKNAME, "CLASS", com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_SORT_STRING, com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_CATEGORIES, com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_PRODID, com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_IMPP})));
    private final com.navdy.hud.app.bluetooth.vcard.VCardParserImpl_V30 mVCardParserImpl;

    public VCardParser_V30() {
        this.mVCardParserImpl = new com.navdy.hud.app.bluetooth.vcard.VCardParserImpl_V30();
    }

    public VCardParser_V30(int vcardType) {
        this.mVCardParserImpl = new com.navdy.hud.app.bluetooth.vcard.VCardParserImpl_V30(vcardType);
    }

    public void addInterpreter(com.navdy.hud.app.bluetooth.vcard.VCardInterpreter interpreter) {
        this.mVCardParserImpl.addInterpreter(interpreter);
    }

    public void parse(java.io.InputStream is) throws java.io.IOException, com.navdy.hud.app.bluetooth.vcard.exception.VCardException {
        this.mVCardParserImpl.parse(is);
    }

    public void parseOne(java.io.InputStream is) throws java.io.IOException, com.navdy.hud.app.bluetooth.vcard.exception.VCardException {
        this.mVCardParserImpl.parseOne(is);
    }

    public void cancel() {
        this.mVCardParserImpl.cancel();
    }
}
