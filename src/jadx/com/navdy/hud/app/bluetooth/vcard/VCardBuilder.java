package com.navdy.hud.app.bluetooth.vcard;

public class VCardBuilder {
    public static final int DEFAULT_EMAIL_TYPE = 3;
    public static final int DEFAULT_PHONE_TYPE = 1;
    public static final int DEFAULT_POSTAL_TYPE = 1;
    private static final java.lang.String LOG_TAG = "vCard";
    private static final java.lang.String SHIFT_JIS = "SHIFT_JIS";
    private static final java.lang.String VCARD_DATA_PUBLIC = "PUBLIC";
    private static final java.lang.String VCARD_DATA_SEPARATOR = ":";
    private static final java.lang.String VCARD_DATA_VCARD = "VCARD";
    public static final java.lang.String VCARD_END_OF_LINE = "\r\n";
    private static final java.lang.String VCARD_ITEM_SEPARATOR = ";";
    private static final java.lang.String VCARD_PARAM_ENCODING_BASE64_AS_B = "ENCODING=B";
    private static final java.lang.String VCARD_PARAM_ENCODING_BASE64_V21 = "ENCODING=BASE64";
    private static final java.lang.String VCARD_PARAM_ENCODING_QP = "ENCODING=QUOTED-PRINTABLE";
    private static final java.lang.String VCARD_PARAM_EQUAL = "=";
    private static final java.lang.String VCARD_PARAM_SEPARATOR = ";";
    private static final java.lang.String VCARD_WS = " ";
    private static final java.util.Set<java.lang.String> sAllowedAndroidPropertySet = java.util.Collections.unmodifiableSet(new java.util.HashSet(java.util.Arrays.asList(new java.lang.String[]{"vnd.android.cursor.item/nickname", "vnd.android.cursor.item/contact_event", "vnd.android.cursor.item/relation"})));
    private static final java.util.Map<java.lang.Integer, java.lang.Integer> sPostalTypePriorityMap = new java.util.HashMap();
    private final boolean mAppendTypeParamName;
    private java.lang.StringBuilder mBuilder;
    private final java.lang.String mCharset;
    private boolean mEndAppended;
    private final boolean mIsDoCoMo;
    private final boolean mIsJapaneseMobilePhone;
    private final boolean mIsV30OrV40;
    private final boolean mNeedsToConvertPhoneticString;
    private final boolean mOnlyOneNoteFieldIsAvailable;
    private final boolean mRefrainsQPToNameProperties;
    private final boolean mShouldAppendCharsetParam;
    private final boolean mShouldUseQuotedPrintable;
    private final boolean mUsesAndroidProperty;
    private final boolean mUsesDefactProperty;
    private final java.lang.String mVCardCharsetParameter;
    private final int mVCardType;

    private static class PostalStruct {
        final java.lang.String addressData;
        final boolean appendCharset;
        final boolean reallyUseQuotedPrintable;

        public PostalStruct(boolean reallyUseQuotedPrintable2, boolean appendCharset2, java.lang.String addressData2) {
            this.reallyUseQuotedPrintable = reallyUseQuotedPrintable2;
            this.appendCharset = appendCharset2;
            this.addressData = addressData2;
        }
    }

    static {
        sPostalTypePriorityMap.put(java.lang.Integer.valueOf(1), java.lang.Integer.valueOf(0));
        sPostalTypePriorityMap.put(java.lang.Integer.valueOf(2), java.lang.Integer.valueOf(1));
        sPostalTypePriorityMap.put(java.lang.Integer.valueOf(3), java.lang.Integer.valueOf(2));
        sPostalTypePriorityMap.put(java.lang.Integer.valueOf(0), java.lang.Integer.valueOf(3));
    }

    public VCardBuilder(int vcardType) {
        this(vcardType, null);
    }

    public VCardBuilder(int vcardType, java.lang.String charset) {
        boolean z = false;
        this.mVCardType = vcardType;
        if (com.navdy.hud.app.bluetooth.vcard.VCardConfig.isVersion40(vcardType)) {
            android.util.Log.w(LOG_TAG, "Should not use vCard 4.0 when building vCard. It is not officially published yet.");
        }
        this.mIsV30OrV40 = com.navdy.hud.app.bluetooth.vcard.VCardConfig.isVersion30(vcardType) || com.navdy.hud.app.bluetooth.vcard.VCardConfig.isVersion40(vcardType);
        this.mShouldUseQuotedPrintable = com.navdy.hud.app.bluetooth.vcard.VCardConfig.shouldUseQuotedPrintable(vcardType);
        this.mIsDoCoMo = com.navdy.hud.app.bluetooth.vcard.VCardConfig.isDoCoMo(vcardType);
        this.mIsJapaneseMobilePhone = com.navdy.hud.app.bluetooth.vcard.VCardConfig.needsToConvertPhoneticString(vcardType);
        this.mOnlyOneNoteFieldIsAvailable = com.navdy.hud.app.bluetooth.vcard.VCardConfig.onlyOneNoteFieldIsAvailable(vcardType);
        this.mUsesAndroidProperty = com.navdy.hud.app.bluetooth.vcard.VCardConfig.usesAndroidSpecificProperty(vcardType);
        this.mUsesDefactProperty = com.navdy.hud.app.bluetooth.vcard.VCardConfig.usesDefactProperty(vcardType);
        this.mRefrainsQPToNameProperties = com.navdy.hud.app.bluetooth.vcard.VCardConfig.shouldRefrainQPToNameProperties(vcardType);
        this.mAppendTypeParamName = com.navdy.hud.app.bluetooth.vcard.VCardConfig.appendTypeParamName(vcardType);
        this.mNeedsToConvertPhoneticString = com.navdy.hud.app.bluetooth.vcard.VCardConfig.needsToConvertPhoneticString(vcardType);
        if (!com.navdy.hud.app.bluetooth.vcard.VCardConfig.isVersion30(vcardType) || !"UTF-8".equalsIgnoreCase(charset)) {
            z = true;
        }
        this.mShouldAppendCharsetParam = z;
        if (com.navdy.hud.app.bluetooth.vcard.VCardConfig.isDoCoMo(vcardType)) {
            if (SHIFT_JIS.equalsIgnoreCase(charset)) {
                this.mCharset = charset;
            } else if (android.text.TextUtils.isEmpty(charset)) {
                this.mCharset = SHIFT_JIS;
            } else {
                this.mCharset = charset;
            }
            this.mVCardCharsetParameter = "CHARSET=SHIFT_JIS";
        } else if (android.text.TextUtils.isEmpty(charset)) {
            android.util.Log.i(LOG_TAG, "Use the charset \"UTF-8\" for export.");
            this.mCharset = "UTF-8";
            this.mVCardCharsetParameter = "CHARSET=UTF-8";
        } else {
            this.mCharset = charset;
            this.mVCardCharsetParameter = "CHARSET=" + charset;
        }
        clear();
    }

    public void clear() {
        this.mBuilder = new java.lang.StringBuilder();
        this.mEndAppended = false;
        appendLine(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_BEGIN, VCARD_DATA_VCARD);
        if (com.navdy.hud.app.bluetooth.vcard.VCardConfig.isVersion40(this.mVCardType)) {
            appendLine(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_VERSION, com.navdy.hud.app.bluetooth.vcard.VCardConstants.VERSION_V40);
        } else if (com.navdy.hud.app.bluetooth.vcard.VCardConfig.isVersion30(this.mVCardType)) {
            appendLine(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_VERSION, com.navdy.hud.app.bluetooth.vcard.VCardConstants.VERSION_V30);
        } else {
            if (!com.navdy.hud.app.bluetooth.vcard.VCardConfig.isVersion21(this.mVCardType)) {
                android.util.Log.w(LOG_TAG, "Unknown vCard version detected.");
            }
            appendLine(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_VERSION, com.navdy.hud.app.bluetooth.vcard.VCardConstants.VERSION_V21);
        }
    }

    private boolean containsNonEmptyName(android.content.ContentValues contentValues) {
        return !android.text.TextUtils.isEmpty(contentValues.getAsString("data3")) || !android.text.TextUtils.isEmpty(contentValues.getAsString("data5")) || !android.text.TextUtils.isEmpty(contentValues.getAsString("data2")) || !android.text.TextUtils.isEmpty(contentValues.getAsString("data4")) || !android.text.TextUtils.isEmpty(contentValues.getAsString("data6")) || !android.text.TextUtils.isEmpty(contentValues.getAsString("data9")) || !android.text.TextUtils.isEmpty(contentValues.getAsString("data8")) || !android.text.TextUtils.isEmpty(contentValues.getAsString("data7")) || !android.text.TextUtils.isEmpty(contentValues.getAsString("data1"));
    }

    private android.content.ContentValues getPrimaryContentValueWithStructuredName(java.util.List<android.content.ContentValues> contentValuesList) {
        android.content.ContentValues primaryContentValues = null;
        android.content.ContentValues subprimaryContentValues = null;
        java.util.Iterator it = contentValuesList.iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            android.content.ContentValues contentValues = (android.content.ContentValues) it.next();
            if (contentValues != null) {
                java.lang.Integer isSuperPrimary = contentValues.getAsInteger("is_super_primary");
                if (isSuperPrimary != null && isSuperPrimary.intValue() > 0) {
                    primaryContentValues = contentValues;
                    break;
                } else if (primaryContentValues == null) {
                    java.lang.Integer isPrimary = contentValues.getAsInteger("is_primary");
                    if (isPrimary != null && isPrimary.intValue() > 0 && containsNonEmptyName(contentValues)) {
                        primaryContentValues = contentValues;
                    } else if (subprimaryContentValues == null && containsNonEmptyName(contentValues)) {
                        subprimaryContentValues = contentValues;
                    }
                }
            }
        }
        if (primaryContentValues != null) {
            return primaryContentValues;
        }
        if (subprimaryContentValues != null) {
            return subprimaryContentValues;
        }
        return new android.content.ContentValues();
    }

    private com.navdy.hud.app.bluetooth.vcard.VCardBuilder appendNamePropertiesV40(java.util.List<android.content.ContentValues> contentValuesList) {
        if (this.mIsDoCoMo || this.mNeedsToConvertPhoneticString) {
            android.util.Log.w(LOG_TAG, "Invalid flag is used in vCard 4.0 construction. Ignored.");
        }
        if (contentValuesList == null || contentValuesList.isEmpty()) {
            appendLine(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_FN, "");
        } else {
            android.content.ContentValues contentValues = getPrimaryContentValueWithStructuredName(contentValuesList);
            java.lang.String familyName = contentValues.getAsString("data3");
            java.lang.String middleName = contentValues.getAsString("data5");
            java.lang.String givenName = contentValues.getAsString("data2");
            java.lang.String prefix = contentValues.getAsString("data4");
            java.lang.String suffix = contentValues.getAsString("data6");
            java.lang.String formattedName = contentValues.getAsString("data1");
            if (android.text.TextUtils.isEmpty(familyName) && android.text.TextUtils.isEmpty(givenName) && android.text.TextUtils.isEmpty(middleName) && android.text.TextUtils.isEmpty(prefix) && android.text.TextUtils.isEmpty(suffix)) {
                if (android.text.TextUtils.isEmpty(formattedName)) {
                    appendLine(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_FN, "");
                } else {
                    familyName = formattedName;
                }
            }
            java.lang.String phoneticFamilyName = contentValues.getAsString("data9");
            java.lang.String phoneticMiddleName = contentValues.getAsString("data8");
            java.lang.String phoneticGivenName = contentValues.getAsString("data7");
            java.lang.String escapedFamily = escapeCharacters(familyName);
            java.lang.String escapedGiven = escapeCharacters(givenName);
            java.lang.String escapedMiddle = escapeCharacters(middleName);
            java.lang.String escapedPrefix = escapeCharacters(prefix);
            java.lang.String escapedSuffix = escapeCharacters(suffix);
            this.mBuilder.append(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_N);
            if (!android.text.TextUtils.isEmpty(phoneticFamilyName) || !android.text.TextUtils.isEmpty(phoneticMiddleName) || !android.text.TextUtils.isEmpty(phoneticGivenName)) {
                this.mBuilder.append(";");
                this.mBuilder.append("SORT-AS=").append(com.navdy.hud.app.bluetooth.vcard.VCardUtils.toStringAsV40ParamValue(escapeCharacters(phoneticFamilyName) + ';' + escapeCharacters(phoneticGivenName) + ';' + escapeCharacters(phoneticMiddleName)));
            }
            this.mBuilder.append(":");
            this.mBuilder.append(escapedFamily);
            this.mBuilder.append(";");
            this.mBuilder.append(escapedGiven);
            this.mBuilder.append(";");
            this.mBuilder.append(escapedMiddle);
            this.mBuilder.append(";");
            this.mBuilder.append(escapedPrefix);
            this.mBuilder.append(";");
            this.mBuilder.append(escapedSuffix);
            this.mBuilder.append(VCARD_END_OF_LINE);
            if (android.text.TextUtils.isEmpty(formattedName)) {
                android.util.Log.w(LOG_TAG, "DISPLAY_NAME is empty.");
                java.lang.String escaped = escapeCharacters(com.navdy.hud.app.bluetooth.vcard.VCardUtils.constructNameFromElements(com.navdy.hud.app.bluetooth.vcard.VCardConfig.getNameOrderType(this.mVCardType), familyName, middleName, givenName, prefix, suffix));
                appendLine(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_FN, escaped);
            } else {
                java.lang.String escapedFormatted = escapeCharacters(formattedName);
                this.mBuilder.append(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_FN);
                this.mBuilder.append(":");
                this.mBuilder.append(escapedFormatted);
                this.mBuilder.append(VCARD_END_OF_LINE);
            }
            appendPhoneticNameFields(contentValues);
        }
        return this;
    }

    /* Debug info: failed to restart local var, previous not found, register: 22 */
    /* JADX INFO: used method not loaded: com.navdy.hud.app.bluetooth.vcard.VCardUtils.containsOnlyNonCrLfPrintableAscii(java.lang.String[]):null, types can be incorrect */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x00e0, code lost:
        if (com.navdy.hud.app.bluetooth.vcard.VCardUtils.containsOnlyNonCrLfPrintableAscii(r7) == false) goto L_0x00e2;
     */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00ea  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00ff  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0110  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0130  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0149  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x01d5  */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x01f1  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x022f  */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x0241  */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x0261  */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x026b  */
    public com.navdy.hud.app.bluetooth.vcard.VCardBuilder appendNameProperties(java.util.List<android.content.ContentValues> contentValuesList) {
        boolean reallyUseQuotedPrintableToName;
        java.lang.String formattedName;
        boolean reallyAppendCharsetParameterToFN;
        boolean reallyUseQuotedPrintableToFN;
        java.lang.String encodedFamily;
        java.lang.String encodedGiven;
        java.lang.String encodedMiddle;
        java.lang.String encodedPrefix;
        java.lang.String encodedSuffix;
        java.lang.String encodedFormattedname;
        if (com.navdy.hud.app.bluetooth.vcard.VCardConfig.isVersion40(this.mVCardType)) {
            return appendNamePropertiesV40(contentValuesList);
        }
        if (contentValuesList != null && !contentValuesList.isEmpty()) {
            android.content.ContentValues contentValues = getPrimaryContentValueWithStructuredName(contentValuesList);
            java.lang.String familyName = contentValues.getAsString("data3");
            java.lang.String middleName = contentValues.getAsString("data5");
            java.lang.String givenName = contentValues.getAsString("data2");
            java.lang.String prefix = contentValues.getAsString("data4");
            java.lang.String suffix = contentValues.getAsString("data6");
            java.lang.String displayName = contentValues.getAsString("data1");
            if (!android.text.TextUtils.isEmpty(familyName) || !android.text.TextUtils.isEmpty(givenName)) {
                boolean reallyAppendCharsetParameterToName = shouldAppendCharsetParam(familyName, givenName, middleName, prefix, suffix);
                if (!this.mRefrainsQPToNameProperties) {
                    if (com.navdy.hud.app.bluetooth.vcard.VCardUtils.containsOnlyNonCrLfPrintableAscii(familyName)) {
                        if (com.navdy.hud.app.bluetooth.vcard.VCardUtils.containsOnlyNonCrLfPrintableAscii(givenName)) {
                            if (com.navdy.hud.app.bluetooth.vcard.VCardUtils.containsOnlyNonCrLfPrintableAscii(middleName)) {
                                if (com.navdy.hud.app.bluetooth.vcard.VCardUtils.containsOnlyNonCrLfPrintableAscii(prefix)) {
                                }
                            }
                        }
                    }
                    reallyUseQuotedPrintableToName = true;
                    if (android.text.TextUtils.isEmpty(displayName)) {
                        formattedName = displayName;
                    } else {
                        formattedName = com.navdy.hud.app.bluetooth.vcard.VCardUtils.constructNameFromElements(com.navdy.hud.app.bluetooth.vcard.VCardConfig.getNameOrderType(this.mVCardType), familyName, middleName, givenName, prefix, suffix);
                    }
                    reallyAppendCharsetParameterToFN = shouldAppendCharsetParam(formattedName);
                    if (!this.mRefrainsQPToNameProperties) {
                        if (!com.navdy.hud.app.bluetooth.vcard.VCardUtils.containsOnlyNonCrLfPrintableAscii(formattedName)) {
                            reallyUseQuotedPrintableToFN = true;
                            if (reallyUseQuotedPrintableToName) {
                                encodedFamily = encodeQuotedPrintable(familyName);
                                encodedGiven = encodeQuotedPrintable(givenName);
                                encodedMiddle = encodeQuotedPrintable(middleName);
                                encodedPrefix = encodeQuotedPrintable(prefix);
                                encodedSuffix = encodeQuotedPrintable(suffix);
                            } else {
                                encodedFamily = escapeCharacters(familyName);
                                encodedGiven = escapeCharacters(givenName);
                                encodedMiddle = escapeCharacters(middleName);
                                encodedPrefix = escapeCharacters(prefix);
                                encodedSuffix = escapeCharacters(suffix);
                            }
                            if (reallyUseQuotedPrintableToFN) {
                                encodedFormattedname = encodeQuotedPrintable(formattedName);
                            } else {
                                encodedFormattedname = escapeCharacters(formattedName);
                            }
                            this.mBuilder.append(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_N);
                            if (this.mIsDoCoMo) {
                                if (reallyAppendCharsetParameterToName) {
                                    this.mBuilder.append(";");
                                    this.mBuilder.append(this.mVCardCharsetParameter);
                                }
                                if (reallyUseQuotedPrintableToName) {
                                    this.mBuilder.append(";");
                                    this.mBuilder.append(VCARD_PARAM_ENCODING_QP);
                                }
                                this.mBuilder.append(":");
                                this.mBuilder.append(formattedName);
                                this.mBuilder.append(";");
                                this.mBuilder.append(";");
                                this.mBuilder.append(";");
                                this.mBuilder.append(";");
                            } else {
                                if (reallyAppendCharsetParameterToName) {
                                    this.mBuilder.append(";");
                                    this.mBuilder.append(this.mVCardCharsetParameter);
                                }
                                if (reallyUseQuotedPrintableToName) {
                                    this.mBuilder.append(";");
                                    this.mBuilder.append(VCARD_PARAM_ENCODING_QP);
                                }
                                this.mBuilder.append(":");
                                this.mBuilder.append(encodedFamily);
                                this.mBuilder.append(";");
                                this.mBuilder.append(encodedGiven);
                                this.mBuilder.append(";");
                                this.mBuilder.append(encodedMiddle);
                                this.mBuilder.append(";");
                                this.mBuilder.append(encodedPrefix);
                                this.mBuilder.append(";");
                                this.mBuilder.append(encodedSuffix);
                            }
                            this.mBuilder.append(VCARD_END_OF_LINE);
                            this.mBuilder.append(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_FN);
                            if (reallyAppendCharsetParameterToFN) {
                                this.mBuilder.append(";");
                                this.mBuilder.append(this.mVCardCharsetParameter);
                            }
                            if (reallyUseQuotedPrintableToFN) {
                                this.mBuilder.append(";");
                                this.mBuilder.append(VCARD_PARAM_ENCODING_QP);
                            }
                            this.mBuilder.append(":");
                            this.mBuilder.append(encodedFormattedname);
                            this.mBuilder.append(VCARD_END_OF_LINE);
                        }
                    }
                    reallyUseQuotedPrintableToFN = false;
                    if (reallyUseQuotedPrintableToName) {
                    }
                    if (reallyUseQuotedPrintableToFN) {
                    }
                    this.mBuilder.append(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_N);
                    if (this.mIsDoCoMo) {
                    }
                    this.mBuilder.append(VCARD_END_OF_LINE);
                    this.mBuilder.append(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_FN);
                    if (reallyAppendCharsetParameterToFN) {
                    }
                    if (reallyUseQuotedPrintableToFN) {
                    }
                    this.mBuilder.append(":");
                    this.mBuilder.append(encodedFormattedname);
                    this.mBuilder.append(VCARD_END_OF_LINE);
                }
                reallyUseQuotedPrintableToName = false;
                if (android.text.TextUtils.isEmpty(displayName)) {
                }
                reallyAppendCharsetParameterToFN = shouldAppendCharsetParam(formattedName);
                if (!this.mRefrainsQPToNameProperties) {
                }
                reallyUseQuotedPrintableToFN = false;
                if (reallyUseQuotedPrintableToName) {
                }
                if (reallyUseQuotedPrintableToFN) {
                }
                this.mBuilder.append(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_N);
                if (this.mIsDoCoMo) {
                }
                this.mBuilder.append(VCARD_END_OF_LINE);
                this.mBuilder.append(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_FN);
                if (reallyAppendCharsetParameterToFN) {
                }
                if (reallyUseQuotedPrintableToFN) {
                }
                this.mBuilder.append(":");
                this.mBuilder.append(encodedFormattedname);
                this.mBuilder.append(VCARD_END_OF_LINE);
            } else if (!android.text.TextUtils.isEmpty(displayName)) {
                buildSinglePartNameField(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_N, displayName);
                this.mBuilder.append(";");
                this.mBuilder.append(";");
                this.mBuilder.append(";");
                this.mBuilder.append(";");
                this.mBuilder.append(VCARD_END_OF_LINE);
                buildSinglePartNameField(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_FN, displayName);
                this.mBuilder.append(VCARD_END_OF_LINE);
            } else if (com.navdy.hud.app.bluetooth.vcard.VCardConfig.isVersion30(this.mVCardType)) {
                appendLine(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_N, "");
                appendLine(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_FN, "");
            } else if (this.mIsDoCoMo) {
                appendLine(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_N, "");
            }
            appendPhoneticNameFields(contentValues);
            return this;
        } else if (com.navdy.hud.app.bluetooth.vcard.VCardConfig.isVersion30(this.mVCardType)) {
            appendLine(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_N, "");
            appendLine(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_FN, "");
            return this;
        } else if (!this.mIsDoCoMo) {
            return this;
        } else {
            appendLine(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_N, "");
            return this;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x0036  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0053  */
    /* JADX WARNING: Removed duplicated region for block: B:6:0x0013  */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0026  */
    private void buildSinglePartNameField(java.lang.String property, java.lang.String part) {
        boolean reallyUseQuotedPrintable;
        java.lang.String encodedPart;
        if (!this.mRefrainsQPToNameProperties) {
            if (!com.navdy.hud.app.bluetooth.vcard.VCardUtils.containsOnlyNonCrLfPrintableAscii(part)) {
                reallyUseQuotedPrintable = true;
                if (!reallyUseQuotedPrintable) {
                    encodedPart = encodeQuotedPrintable(part);
                } else {
                    encodedPart = escapeCharacters(part);
                }
                this.mBuilder.append(property);
                if (shouldAppendCharsetParam(part)) {
                    this.mBuilder.append(";");
                    this.mBuilder.append(this.mVCardCharsetParameter);
                }
                if (reallyUseQuotedPrintable) {
                    this.mBuilder.append(";");
                    this.mBuilder.append(VCARD_PARAM_ENCODING_QP);
                }
                this.mBuilder.append(":");
                this.mBuilder.append(encodedPart);
            }
        }
        reallyUseQuotedPrintable = false;
        if (!reallyUseQuotedPrintable) {
        }
        this.mBuilder.append(property);
        if (shouldAppendCharsetParam(part)) {
        }
        if (reallyUseQuotedPrintable) {
        }
        this.mBuilder.append(":");
        this.mBuilder.append(encodedPart);
    }

    /* JADX INFO: used method not loaded: com.navdy.hud.app.bluetooth.vcard.VCardUtils.containsOnlyNonCrLfPrintableAscii(java.lang.String[]):null, types can be incorrect */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x023c, code lost:
        if (com.navdy.hud.app.bluetooth.vcard.VCardUtils.containsOnlyNonCrLfPrintableAscii(r5) == false) goto L_0x023e;
     */
    /* JADX WARNING: Removed duplicated region for block: B:101:0x02cb  */
    /* JADX WARNING: Removed duplicated region for block: B:104:0x02e4  */
    /* JADX WARNING: Removed duplicated region for block: B:106:0x02ed  */
    /* JADX WARNING: Removed duplicated region for block: B:108:0x02f6  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00a1  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00b8  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00c8  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0102  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0119  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0129  */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x0163  */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x017a  */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x018a  */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x0241  */
    /* JADX WARNING: Removed duplicated region for block: B:85:0x025f  */
    /* JADX WARNING: Removed duplicated region for block: B:88:0x027b  */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x0287  */
    /* JADX WARNING: Removed duplicated region for block: B:96:0x0295  */
    private void appendPhoneticNameFields(android.content.ContentValues contentValues) {
        java.lang.String phoneticFamilyName;
        java.lang.String phoneticMiddleName;
        java.lang.String phoneticGivenName;
        boolean reallyUseQuotedPrintable;
        java.lang.String encodedPhoneticFamilyName;
        java.lang.String encodedPhoneticMiddleName;
        java.lang.String encodedPhoneticGivenName;
        boolean reallyUseQuotedPrintable2;
        java.lang.String encodedPhoneticFamilyName2;
        boolean reallyUseQuotedPrintable3;
        java.lang.String encodedPhoneticMiddleName2;
        boolean reallyUseQuotedPrintable4;
        java.lang.String encodedPhoneticGivenName2;
        java.lang.String tmpPhoneticFamilyName = contentValues.getAsString("data9");
        java.lang.String tmpPhoneticMiddleName = contentValues.getAsString("data8");
        java.lang.String tmpPhoneticGivenName = contentValues.getAsString("data7");
        if (this.mNeedsToConvertPhoneticString) {
            phoneticFamilyName = com.navdy.hud.app.bluetooth.vcard.VCardUtils.toHalfWidthString(tmpPhoneticFamilyName);
            phoneticMiddleName = com.navdy.hud.app.bluetooth.vcard.VCardUtils.toHalfWidthString(tmpPhoneticMiddleName);
            phoneticGivenName = com.navdy.hud.app.bluetooth.vcard.VCardUtils.toHalfWidthString(tmpPhoneticGivenName);
        } else {
            phoneticFamilyName = tmpPhoneticFamilyName;
            phoneticMiddleName = tmpPhoneticMiddleName;
            phoneticGivenName = tmpPhoneticGivenName;
        }
        if (!android.text.TextUtils.isEmpty(phoneticFamilyName) || !android.text.TextUtils.isEmpty(phoneticMiddleName) || !android.text.TextUtils.isEmpty(phoneticGivenName)) {
            if (!com.navdy.hud.app.bluetooth.vcard.VCardConfig.isVersion40(this.mVCardType)) {
                if (com.navdy.hud.app.bluetooth.vcard.VCardConfig.isVersion30(this.mVCardType)) {
                    java.lang.String sortString = com.navdy.hud.app.bluetooth.vcard.VCardUtils.constructNameFromElements(this.mVCardType, phoneticFamilyName, phoneticMiddleName, phoneticGivenName);
                    this.mBuilder.append(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_SORT_STRING);
                    if (com.navdy.hud.app.bluetooth.vcard.VCardConfig.isVersion30(this.mVCardType)) {
                        if (shouldAppendCharsetParam(sortString)) {
                            this.mBuilder.append(";");
                            this.mBuilder.append(this.mVCardCharsetParameter);
                        }
                    }
                    this.mBuilder.append(":");
                    this.mBuilder.append(escapeCharacters(sortString));
                    this.mBuilder.append(VCARD_END_OF_LINE);
                } else if (this.mIsJapaneseMobilePhone) {
                    this.mBuilder.append(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_SOUND);
                    this.mBuilder.append(";");
                    this.mBuilder.append("X-IRMC-N");
                    if (!this.mRefrainsQPToNameProperties) {
                        if (com.navdy.hud.app.bluetooth.vcard.VCardUtils.containsOnlyNonCrLfPrintableAscii(phoneticFamilyName)) {
                            if (com.navdy.hud.app.bluetooth.vcard.VCardUtils.containsOnlyNonCrLfPrintableAscii(phoneticMiddleName)) {
                            }
                        }
                        reallyUseQuotedPrintable = true;
                        if (!reallyUseQuotedPrintable) {
                            encodedPhoneticFamilyName = encodeQuotedPrintable(phoneticFamilyName);
                            encodedPhoneticMiddleName = encodeQuotedPrintable(phoneticMiddleName);
                            encodedPhoneticGivenName = encodeQuotedPrintable(phoneticGivenName);
                        } else {
                            encodedPhoneticFamilyName = escapeCharacters(phoneticFamilyName);
                            encodedPhoneticMiddleName = escapeCharacters(phoneticMiddleName);
                            encodedPhoneticGivenName = escapeCharacters(phoneticGivenName);
                        }
                        if (shouldAppendCharsetParam(encodedPhoneticFamilyName, encodedPhoneticMiddleName, encodedPhoneticGivenName)) {
                            this.mBuilder.append(";");
                            this.mBuilder.append(this.mVCardCharsetParameter);
                        }
                        this.mBuilder.append(":");
                        boolean first = true;
                        if (!android.text.TextUtils.isEmpty(encodedPhoneticFamilyName)) {
                            this.mBuilder.append(encodedPhoneticFamilyName);
                            first = false;
                        }
                        if (!android.text.TextUtils.isEmpty(encodedPhoneticMiddleName)) {
                            if (first) {
                                first = false;
                            } else {
                                this.mBuilder.append(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.SPACE_CHAR);
                            }
                            this.mBuilder.append(encodedPhoneticMiddleName);
                        }
                        if (!android.text.TextUtils.isEmpty(encodedPhoneticGivenName)) {
                            if (!first) {
                                this.mBuilder.append(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.SPACE_CHAR);
                            }
                            this.mBuilder.append(encodedPhoneticGivenName);
                        }
                        this.mBuilder.append(";");
                        this.mBuilder.append(";");
                        this.mBuilder.append(";");
                        this.mBuilder.append(";");
                        this.mBuilder.append(VCARD_END_OF_LINE);
                    }
                    reallyUseQuotedPrintable = false;
                    if (!reallyUseQuotedPrintable) {
                    }
                    if (shouldAppendCharsetParam(encodedPhoneticFamilyName, encodedPhoneticMiddleName, encodedPhoneticGivenName)) {
                    }
                    this.mBuilder.append(":");
                    boolean first2 = true;
                    if (!android.text.TextUtils.isEmpty(encodedPhoneticFamilyName)) {
                    }
                    if (!android.text.TextUtils.isEmpty(encodedPhoneticMiddleName)) {
                    }
                    if (!android.text.TextUtils.isEmpty(encodedPhoneticGivenName)) {
                    }
                    this.mBuilder.append(";");
                    this.mBuilder.append(";");
                    this.mBuilder.append(";");
                    this.mBuilder.append(";");
                    this.mBuilder.append(VCARD_END_OF_LINE);
                }
            }
            if (this.mUsesDefactProperty) {
                if (!android.text.TextUtils.isEmpty(phoneticGivenName)) {
                    if (this.mShouldUseQuotedPrintable) {
                        if (!com.navdy.hud.app.bluetooth.vcard.VCardUtils.containsOnlyNonCrLfPrintableAscii(phoneticGivenName)) {
                            reallyUseQuotedPrintable4 = true;
                            if (!reallyUseQuotedPrintable4) {
                                encodedPhoneticGivenName2 = encodeQuotedPrintable(phoneticGivenName);
                            } else {
                                encodedPhoneticGivenName2 = escapeCharacters(phoneticGivenName);
                            }
                            this.mBuilder.append(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_X_PHONETIC_FIRST_NAME);
                            if (shouldAppendCharsetParam(phoneticGivenName)) {
                                this.mBuilder.append(";");
                                this.mBuilder.append(this.mVCardCharsetParameter);
                            }
                            if (reallyUseQuotedPrintable4) {
                                this.mBuilder.append(";");
                                this.mBuilder.append(VCARD_PARAM_ENCODING_QP);
                            }
                            this.mBuilder.append(":");
                            this.mBuilder.append(encodedPhoneticGivenName2);
                            this.mBuilder.append(VCARD_END_OF_LINE);
                        }
                    }
                    reallyUseQuotedPrintable4 = false;
                    if (!reallyUseQuotedPrintable4) {
                    }
                    this.mBuilder.append(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_X_PHONETIC_FIRST_NAME);
                    if (shouldAppendCharsetParam(phoneticGivenName)) {
                    }
                    if (reallyUseQuotedPrintable4) {
                    }
                    this.mBuilder.append(":");
                    this.mBuilder.append(encodedPhoneticGivenName2);
                    this.mBuilder.append(VCARD_END_OF_LINE);
                }
                if (!android.text.TextUtils.isEmpty(phoneticMiddleName)) {
                    if (this.mShouldUseQuotedPrintable) {
                        if (!com.navdy.hud.app.bluetooth.vcard.VCardUtils.containsOnlyNonCrLfPrintableAscii(phoneticMiddleName)) {
                            reallyUseQuotedPrintable3 = true;
                            if (!reallyUseQuotedPrintable3) {
                                encodedPhoneticMiddleName2 = encodeQuotedPrintable(phoneticMiddleName);
                            } else {
                                encodedPhoneticMiddleName2 = escapeCharacters(phoneticMiddleName);
                            }
                            this.mBuilder.append(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_X_PHONETIC_MIDDLE_NAME);
                            if (shouldAppendCharsetParam(phoneticMiddleName)) {
                                this.mBuilder.append(";");
                                this.mBuilder.append(this.mVCardCharsetParameter);
                            }
                            if (reallyUseQuotedPrintable3) {
                                this.mBuilder.append(";");
                                this.mBuilder.append(VCARD_PARAM_ENCODING_QP);
                            }
                            this.mBuilder.append(":");
                            this.mBuilder.append(encodedPhoneticMiddleName2);
                            this.mBuilder.append(VCARD_END_OF_LINE);
                        }
                    }
                    reallyUseQuotedPrintable3 = false;
                    if (!reallyUseQuotedPrintable3) {
                    }
                    this.mBuilder.append(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_X_PHONETIC_MIDDLE_NAME);
                    if (shouldAppendCharsetParam(phoneticMiddleName)) {
                    }
                    if (reallyUseQuotedPrintable3) {
                    }
                    this.mBuilder.append(":");
                    this.mBuilder.append(encodedPhoneticMiddleName2);
                    this.mBuilder.append(VCARD_END_OF_LINE);
                }
                if (!android.text.TextUtils.isEmpty(phoneticFamilyName)) {
                    if (this.mShouldUseQuotedPrintable) {
                        if (!com.navdy.hud.app.bluetooth.vcard.VCardUtils.containsOnlyNonCrLfPrintableAscii(phoneticFamilyName)) {
                            reallyUseQuotedPrintable2 = true;
                            if (!reallyUseQuotedPrintable2) {
                                encodedPhoneticFamilyName2 = encodeQuotedPrintable(phoneticFamilyName);
                            } else {
                                encodedPhoneticFamilyName2 = escapeCharacters(phoneticFamilyName);
                            }
                            this.mBuilder.append(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_X_PHONETIC_LAST_NAME);
                            if (shouldAppendCharsetParam(phoneticFamilyName)) {
                                this.mBuilder.append(";");
                                this.mBuilder.append(this.mVCardCharsetParameter);
                            }
                            if (reallyUseQuotedPrintable2) {
                                this.mBuilder.append(";");
                                this.mBuilder.append(VCARD_PARAM_ENCODING_QP);
                            }
                            this.mBuilder.append(":");
                            this.mBuilder.append(encodedPhoneticFamilyName2);
                            this.mBuilder.append(VCARD_END_OF_LINE);
                        }
                    }
                    reallyUseQuotedPrintable2 = false;
                    if (!reallyUseQuotedPrintable2) {
                    }
                    this.mBuilder.append(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_X_PHONETIC_LAST_NAME);
                    if (shouldAppendCharsetParam(phoneticFamilyName)) {
                    }
                    if (reallyUseQuotedPrintable2) {
                    }
                    this.mBuilder.append(":");
                    this.mBuilder.append(encodedPhoneticFamilyName2);
                    this.mBuilder.append(VCARD_END_OF_LINE);
                }
            }
        } else if (this.mIsDoCoMo) {
            this.mBuilder.append(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_SOUND);
            this.mBuilder.append(";");
            this.mBuilder.append("X-IRMC-N");
            this.mBuilder.append(":");
            this.mBuilder.append(";");
            this.mBuilder.append(";");
            this.mBuilder.append(";");
            this.mBuilder.append(";");
            this.mBuilder.append(VCARD_END_OF_LINE);
        }
    }

    public com.navdy.hud.app.bluetooth.vcard.VCardBuilder appendNickNames(java.util.List<android.content.ContentValues> contentValuesList) {
        boolean useAndroidProperty;
        if (this.mIsV30OrV40) {
            useAndroidProperty = false;
        } else {
            if (this.mUsesAndroidProperty) {
                useAndroidProperty = true;
            }
            return this;
        }
        if (contentValuesList != null) {
            for (android.content.ContentValues contentValues : contentValuesList) {
                java.lang.String nickname = contentValues.getAsString("data1");
                if (!android.text.TextUtils.isEmpty(nickname)) {
                    if (useAndroidProperty) {
                        appendAndroidSpecificProperty("vnd.android.cursor.item/nickname", contentValues);
                    } else {
                        appendLineWithCharsetAndQPDetection(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_NICKNAME, nickname);
                    }
                }
            }
        }
        return this;
    }

    public com.navdy.hud.app.bluetooth.vcard.VCardBuilder appendPhones(java.util.List<android.content.ContentValues> contentValuesList, com.navdy.hud.app.bluetooth.vcard.VCardPhoneNumberTranslationCallback translationCallback) {
        java.lang.String formatted;
        boolean phoneLineExists = false;
        if (contentValuesList != null) {
            java.util.HashSet hashSet = new java.util.HashSet();
            for (android.content.ContentValues contentValues : contentValuesList) {
                java.lang.Integer typeAsObject = contentValues.getAsInteger("data2");
                java.lang.String label = contentValues.getAsString("data3");
                java.lang.Integer isPrimaryAsInteger = contentValues.getAsInteger("is_primary");
                boolean isPrimary = isPrimaryAsInteger != null ? isPrimaryAsInteger.intValue() > 0 : false;
                java.lang.String phoneNumber = contentValues.getAsString("data1");
                if (phoneNumber != null) {
                    phoneNumber = phoneNumber.trim();
                }
                if (!android.text.TextUtils.isEmpty(phoneNumber)) {
                    int type = typeAsObject != null ? typeAsObject.intValue() : 1;
                    if (translationCallback != null) {
                        java.lang.String phoneNumber2 = translationCallback.onValueReceived(phoneNumber, type, label, isPrimary);
                        if (!hashSet.contains(phoneNumber2)) {
                            hashSet.add(phoneNumber2);
                            appendTelLine(java.lang.Integer.valueOf(type), label, phoneNumber2, isPrimary);
                        }
                    } else if (type == 6 || com.navdy.hud.app.bluetooth.vcard.VCardConfig.refrainPhoneNumberFormatting(this.mVCardType)) {
                        phoneLineExists = true;
                        if (!hashSet.contains(phoneNumber)) {
                            hashSet.add(phoneNumber);
                            appendTelLine(java.lang.Integer.valueOf(type), label, phoneNumber, isPrimary);
                        }
                    } else {
                        java.util.List<java.lang.String> phoneNumberList = splitPhoneNumbers(phoneNumber);
                        if (!phoneNumberList.isEmpty()) {
                            phoneLineExists = true;
                            for (java.lang.String actualPhoneNumber : phoneNumberList) {
                                if (!hashSet.contains(actualPhoneNumber)) {
                                    java.lang.String numberWithControlSequence = actualPhoneNumber.replace(',', 'p').replace(';', 'w');
                                    if (android.text.TextUtils.equals(numberWithControlSequence, actualPhoneNumber)) {
                                        java.lang.StringBuilder digitsOnlyBuilder = new java.lang.StringBuilder();
                                        int length = actualPhoneNumber.length();
                                        for (int i = 0; i < length; i++) {
                                            char ch = actualPhoneNumber.charAt(i);
                                            if (java.lang.Character.isDigit(ch) || ch == '+') {
                                                digitsOnlyBuilder.append(ch);
                                            }
                                        }
                                        formatted = com.navdy.hud.app.bluetooth.vcard.VCardUtils.PhoneNumberUtilsPort.formatNumber(digitsOnlyBuilder.toString(), com.navdy.hud.app.bluetooth.vcard.VCardUtils.getPhoneNumberFormat(this.mVCardType));
                                    } else {
                                        formatted = numberWithControlSequence;
                                    }
                                    if (com.navdy.hud.app.bluetooth.vcard.VCardConfig.isVersion40(this.mVCardType) && !android.text.TextUtils.isEmpty(formatted) && !formatted.startsWith("tel:")) {
                                        formatted = "tel:" + formatted;
                                    }
                                    hashSet.add(actualPhoneNumber);
                                    appendTelLine(java.lang.Integer.valueOf(type), label, formatted, isPrimary);
                                }
                            }
                        }
                    }
                }
            }
        }
        if (!phoneLineExists && this.mIsDoCoMo) {
            appendTelLine(java.lang.Integer.valueOf(1), "", "", false);
        }
        return this;
    }

    private java.util.List<java.lang.String> splitPhoneNumbers(java.lang.String phoneNumber) {
        java.util.List<java.lang.String> phoneList = new java.util.ArrayList<>();
        java.lang.StringBuilder builder = new java.lang.StringBuilder();
        int length = phoneNumber.length();
        for (int i = 0; i < length; i++) {
            char ch = phoneNumber.charAt(i);
            if (ch != 10 || builder.length() <= 0) {
                builder.append(ch);
            } else {
                phoneList.add(builder.toString());
                builder = new java.lang.StringBuilder();
            }
        }
        if (builder.length() > 0) {
            phoneList.add(builder.toString());
        }
        return phoneList;
    }

    public com.navdy.hud.app.bluetooth.vcard.VCardBuilder appendEmails(java.util.List<android.content.ContentValues> contentValuesList) {
        boolean emailAddressExists = false;
        if (contentValuesList != null) {
            java.util.Set<java.lang.String> addressSet = new java.util.HashSet<>();
            for (android.content.ContentValues contentValues : contentValuesList) {
                java.lang.String emailAddress = contentValues.getAsString("data1");
                if (emailAddress != null) {
                    emailAddress = emailAddress.trim();
                }
                if (!android.text.TextUtils.isEmpty(emailAddress)) {
                    java.lang.Integer typeAsObject = contentValues.getAsInteger("data2");
                    int type = typeAsObject != null ? typeAsObject.intValue() : 3;
                    java.lang.String label = contentValues.getAsString("data3");
                    java.lang.Integer isPrimaryAsInteger = contentValues.getAsInteger("is_primary");
                    boolean isPrimary = isPrimaryAsInteger != null ? isPrimaryAsInteger.intValue() > 0 : false;
                    emailAddressExists = true;
                    if (!addressSet.contains(emailAddress)) {
                        addressSet.add(emailAddress);
                        appendEmailLine(type, label, emailAddress, isPrimary);
                    }
                }
            }
        }
        if (!emailAddressExists && this.mIsDoCoMo) {
            appendEmailLine(1, "", "", false);
        }
        return this;
    }

    public com.navdy.hud.app.bluetooth.vcard.VCardBuilder appendPostals(java.util.List<android.content.ContentValues> contentValuesList) {
        if (contentValuesList == null || contentValuesList.isEmpty()) {
            if (this.mIsDoCoMo) {
                this.mBuilder.append(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_ADR);
                this.mBuilder.append(";");
                this.mBuilder.append(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE_HOME);
                this.mBuilder.append(":");
                this.mBuilder.append(VCARD_END_OF_LINE);
            }
        } else if (this.mIsDoCoMo) {
            appendPostalsForDoCoMo(contentValuesList);
        } else {
            appendPostalsForGeneric(contentValuesList);
        }
        return this;
    }

    private void appendPostalsForDoCoMo(java.util.List<android.content.ContentValues> contentValuesList) {
        int currentPriority = Integer.MAX_VALUE;
        int currentType = Integer.MAX_VALUE;
        android.content.ContentValues currentContentValues = null;
        for (android.content.ContentValues contentValues : contentValuesList) {
            if (contentValues != null) {
                java.lang.Integer typeAsInteger = contentValues.getAsInteger("data2");
                java.lang.Integer priorityAsInteger = (java.lang.Integer) sPostalTypePriorityMap.get(typeAsInteger);
                int priority = priorityAsInteger != null ? priorityAsInteger.intValue() : Integer.MAX_VALUE;
                if (priority < currentPriority) {
                    currentPriority = priority;
                    currentType = typeAsInteger.intValue();
                    currentContentValues = contentValues;
                    if (priority == 0) {
                        break;
                    }
                } else {
                    continue;
                }
            }
        }
        if (currentContentValues == null) {
            android.util.Log.w(LOG_TAG, "Should not come here. Must have at least one postal data.");
        } else {
            appendPostalLine(currentType, currentContentValues.getAsString("data3"), currentContentValues, false, true);
        }
    }

    private void appendPostalsForGeneric(java.util.List<android.content.ContentValues> contentValuesList) {
        int type;
        for (android.content.ContentValues contentValues : contentValuesList) {
            if (contentValues != null) {
                java.lang.Integer typeAsInteger = contentValues.getAsInteger("data2");
                if (typeAsInteger != null) {
                    type = typeAsInteger.intValue();
                } else {
                    type = 1;
                }
                java.lang.String label = contentValues.getAsString("data3");
                java.lang.Integer isPrimaryAsInteger = contentValues.getAsInteger("is_primary");
                boolean isPrimary = isPrimaryAsInteger != null ? isPrimaryAsInteger.intValue() > 0 : false;
                appendPostalLine(type, label, contentValues, isPrimary, false);
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:36:0x01c0  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x01c3  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x020f  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0211  */
    private com.navdy.hud.app.bluetooth.vcard.VCardBuilder.PostalStruct tryConstructPostalStruct(android.content.ContentValues contentValues) {
        boolean reallyUseQuotedPrintable;
        java.lang.String encodedFormattedAddress;
        boolean appendCharset;
        java.lang.String rawLocality2;
        java.lang.String encodedPoBox;
        java.lang.String encodedStreet;
        java.lang.String encodedLocality;
        java.lang.String encodedRegion;
        java.lang.String encodedPostalCode;
        java.lang.String encodedCountry;
        java.lang.String rawPoBox = contentValues.getAsString("data5");
        java.lang.String rawNeighborhood = contentValues.getAsString("data6");
        java.lang.String rawStreet = contentValues.getAsString("data4");
        java.lang.String rawLocality = contentValues.getAsString("data7");
        java.lang.String rawRegion = contentValues.getAsString("data8");
        java.lang.String rawPostalCode = contentValues.getAsString("data9");
        java.lang.String rawCountry = contentValues.getAsString("data10");
        java.lang.String[] rawAddressArray = {rawPoBox, rawNeighborhood, rawStreet, rawLocality, rawRegion, rawPostalCode, rawCountry};
        if (!com.navdy.hud.app.bluetooth.vcard.VCardUtils.areAllEmpty(rawAddressArray)) {
            boolean reallyUseQuotedPrintable2 = this.mShouldUseQuotedPrintable && !com.navdy.hud.app.bluetooth.vcard.VCardUtils.containsOnlyNonCrLfPrintableAscii(rawAddressArray);
            if (!com.navdy.hud.app.bluetooth.vcard.VCardUtils.containsOnlyPrintableAscii(rawAddressArray)) {
                appendCharset = true;
            } else {
                appendCharset = false;
            }
            if (android.text.TextUtils.isEmpty(rawLocality)) {
                if (android.text.TextUtils.isEmpty(rawNeighborhood)) {
                    rawLocality2 = "";
                } else {
                    rawLocality2 = rawNeighborhood;
                }
            } else if (android.text.TextUtils.isEmpty(rawNeighborhood)) {
                rawLocality2 = rawLocality;
            } else {
                rawLocality2 = rawLocality + " " + rawNeighborhood;
            }
            if (reallyUseQuotedPrintable2) {
                encodedPoBox = encodeQuotedPrintable(rawPoBox);
                encodedStreet = encodeQuotedPrintable(rawStreet);
                encodedLocality = encodeQuotedPrintable(rawLocality2);
                encodedRegion = encodeQuotedPrintable(rawRegion);
                encodedPostalCode = encodeQuotedPrintable(rawPostalCode);
                encodedCountry = encodeQuotedPrintable(rawCountry);
            } else {
                encodedPoBox = escapeCharacters(rawPoBox);
                encodedStreet = escapeCharacters(rawStreet);
                encodedLocality = escapeCharacters(rawLocality2);
                encodedRegion = escapeCharacters(rawRegion);
                encodedPostalCode = escapeCharacters(rawPostalCode);
                encodedCountry = escapeCharacters(rawCountry);
                escapeCharacters(rawNeighborhood);
            }
            java.lang.StringBuilder addressBuilder = new java.lang.StringBuilder();
            addressBuilder.append(encodedPoBox);
            addressBuilder.append(";");
            addressBuilder.append(";");
            addressBuilder.append(encodedStreet);
            addressBuilder.append(";");
            addressBuilder.append(encodedLocality);
            addressBuilder.append(";");
            addressBuilder.append(encodedRegion);
            addressBuilder.append(";");
            addressBuilder.append(encodedPostalCode);
            addressBuilder.append(";");
            addressBuilder.append(encodedCountry);
            com.navdy.hud.app.bluetooth.vcard.VCardBuilder.PostalStruct postalStruct = new com.navdy.hud.app.bluetooth.vcard.VCardBuilder.PostalStruct(reallyUseQuotedPrintable2, appendCharset, addressBuilder.toString());
            return postalStruct;
        }
        java.lang.String rawFormattedAddress = contentValues.getAsString("data1");
        if (android.text.TextUtils.isEmpty(rawFormattedAddress)) {
            return null;
        }
        if (this.mShouldUseQuotedPrintable) {
            if (!com.navdy.hud.app.bluetooth.vcard.VCardUtils.containsOnlyNonCrLfPrintableAscii(rawFormattedAddress)) {
                reallyUseQuotedPrintable = true;
                boolean appendCharset2 = com.navdy.hud.app.bluetooth.vcard.VCardUtils.containsOnlyPrintableAscii(rawFormattedAddress);
                if (!reallyUseQuotedPrintable) {
                    encodedFormattedAddress = encodeQuotedPrintable(rawFormattedAddress);
                } else {
                    encodedFormattedAddress = escapeCharacters(rawFormattedAddress);
                }
                java.lang.StringBuilder addressBuilder2 = new java.lang.StringBuilder();
                addressBuilder2.append(";");
                addressBuilder2.append(encodedFormattedAddress);
                addressBuilder2.append(";");
                addressBuilder2.append(";");
                addressBuilder2.append(";");
                addressBuilder2.append(";");
                addressBuilder2.append(";");
                com.navdy.hud.app.bluetooth.vcard.VCardBuilder.PostalStruct postalStruct2 = new com.navdy.hud.app.bluetooth.vcard.VCardBuilder.PostalStruct(reallyUseQuotedPrintable, appendCharset2, addressBuilder2.toString());
                return postalStruct2;
            }
        }
        reallyUseQuotedPrintable = false;
        if (com.navdy.hud.app.bluetooth.vcard.VCardUtils.containsOnlyPrintableAscii(rawFormattedAddress)) {
        }
        if (!reallyUseQuotedPrintable) {
        }
        java.lang.StringBuilder addressBuilder22 = new java.lang.StringBuilder();
        addressBuilder22.append(";");
        addressBuilder22.append(encodedFormattedAddress);
        addressBuilder22.append(";");
        addressBuilder22.append(";");
        addressBuilder22.append(";");
        addressBuilder22.append(";");
        addressBuilder22.append(";");
        com.navdy.hud.app.bluetooth.vcard.VCardBuilder.PostalStruct postalStruct22 = new com.navdy.hud.app.bluetooth.vcard.VCardBuilder.PostalStruct(reallyUseQuotedPrintable, appendCharset2, addressBuilder22.toString());
        return postalStruct22;
    }

    public com.navdy.hud.app.bluetooth.vcard.VCardBuilder appendIms(java.util.List<android.content.ContentValues> contentValuesList) {
        int i;
        java.lang.String typeAsString;
        if (contentValuesList != null) {
            for (android.content.ContentValues contentValues : contentValuesList) {
                java.lang.Integer protocolAsObject = contentValues.getAsInteger("data5");
                if (protocolAsObject != null) {
                    java.lang.String propertyName = com.navdy.hud.app.bluetooth.vcard.VCardUtils.getPropertyNameForIm(protocolAsObject.intValue());
                    if (propertyName != null) {
                        java.lang.String data = contentValues.getAsString("data1");
                        if (data != null) {
                            data = data.trim();
                        }
                        if (!android.text.TextUtils.isEmpty(data)) {
                            java.lang.Integer typeAsInteger = contentValues.getAsInteger("data2");
                            if (typeAsInteger != null) {
                                i = typeAsInteger.intValue();
                            } else {
                                i = 3;
                            }
                            switch (i) {
                                case 0:
                                    java.lang.String label = contentValues.getAsString("data3");
                                    if (label == null) {
                                        typeAsString = null;
                                        break;
                                    } else {
                                        typeAsString = "X-" + label;
                                        break;
                                    }
                                case 1:
                                    typeAsString = com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE_HOME;
                                    break;
                                case 2:
                                    typeAsString = com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE_WORK;
                                    break;
                                default:
                                    typeAsString = null;
                                    break;
                            }
                            java.util.List<java.lang.String> parameterList = new java.util.ArrayList<>();
                            if (!android.text.TextUtils.isEmpty(typeAsString)) {
                                parameterList.add(typeAsString);
                            }
                            java.lang.Integer isPrimaryAsInteger = contentValues.getAsInteger("is_primary");
                            boolean isPrimary = isPrimaryAsInteger != null ? isPrimaryAsInteger.intValue() > 0 : false;
                            if (isPrimary) {
                                parameterList.add(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE_PREF);
                            }
                            appendLineWithCharsetAndQPDetection(propertyName, parameterList, data);
                        }
                    }
                }
            }
        }
        return this;
    }

    public com.navdy.hud.app.bluetooth.vcard.VCardBuilder appendWebsites(java.util.List<android.content.ContentValues> contentValuesList) {
        if (contentValuesList != null) {
            for (android.content.ContentValues contentValues : contentValuesList) {
                java.lang.String website = contentValues.getAsString("data1");
                if (website != null) {
                    website = website.trim();
                }
                if (!android.text.TextUtils.isEmpty(website)) {
                    appendLineWithCharsetAndQPDetection("URL", website);
                }
            }
        }
        return this;
    }

    /* JADX WARNING: Removed duplicated region for block: B:33:0x0083  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x0008 A[SYNTHETIC] */
    public com.navdy.hud.app.bluetooth.vcard.VCardBuilder appendOrganizations(java.util.List<android.content.ContentValues> contentValuesList) {
        boolean z;
        boolean z2;
        boolean z3;
        if (contentValuesList != null) {
            for (android.content.ContentValues contentValues : contentValuesList) {
                java.lang.String company = contentValues.getAsString("data1");
                if (company != null) {
                    company = company.trim();
                }
                java.lang.String department = contentValues.getAsString("data5");
                if (department != null) {
                    department = department.trim();
                }
                java.lang.String title = contentValues.getAsString("data4");
                if (title != null) {
                    title = title.trim();
                }
                java.lang.StringBuilder orgBuilder = new java.lang.StringBuilder();
                if (!android.text.TextUtils.isEmpty(company)) {
                    orgBuilder.append(company);
                }
                if (!android.text.TextUtils.isEmpty(department)) {
                    if (orgBuilder.length() > 0) {
                        orgBuilder.append(';');
                    }
                    orgBuilder.append(department);
                }
                java.lang.String orgline = orgBuilder.toString();
                java.lang.String str = com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_ORG;
                boolean z4 = !com.navdy.hud.app.bluetooth.vcard.VCardUtils.containsOnlyPrintableAscii(orgline);
                if (this.mShouldUseQuotedPrintable) {
                    if (!com.navdy.hud.app.bluetooth.vcard.VCardUtils.containsOnlyNonCrLfPrintableAscii(orgline)) {
                        z = true;
                        appendLine(str, orgline, z4, z);
                        if (android.text.TextUtils.isEmpty(title)) {
                            java.lang.String str2 = com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_TITLE;
                            if (!com.navdy.hud.app.bluetooth.vcard.VCardUtils.containsOnlyPrintableAscii(title)) {
                                z2 = true;
                            } else {
                                z2 = false;
                            }
                            if (this.mShouldUseQuotedPrintable) {
                                if (!com.navdy.hud.app.bluetooth.vcard.VCardUtils.containsOnlyNonCrLfPrintableAscii(title)) {
                                    z3 = true;
                                    appendLine(str2, title, z2, z3);
                                }
                            }
                            z3 = false;
                            appendLine(str2, title, z2, z3);
                        }
                    }
                }
                z = false;
                appendLine(str, orgline, z4, z);
                if (android.text.TextUtils.isEmpty(title)) {
                }
            }
        }
        return this;
    }

    public com.navdy.hud.app.bluetooth.vcard.VCardBuilder appendPhotos(java.util.List<android.content.ContentValues> contentValuesList) {
        if (contentValuesList != null) {
            for (android.content.ContentValues contentValues : contentValuesList) {
                if (contentValues != null) {
                    byte[] data = contentValues.getAsByteArray("data15");
                    if (data != null) {
                        java.lang.String photoType = com.navdy.hud.app.bluetooth.vcard.VCardUtils.guessImageType(data);
                        if (photoType == null) {
                            android.util.Log.d(LOG_TAG, "Unknown photo type. Ignored.");
                        } else {
                            java.lang.String photoString = new java.lang.String(android.util.Base64.encode(data, 2));
                            if (!android.text.TextUtils.isEmpty(photoString)) {
                                appendPhotoLine(photoString, photoType);
                            }
                        }
                    }
                }
            }
        }
        return this;
    }

    public com.navdy.hud.app.bluetooth.vcard.VCardBuilder appendNotes(java.util.List<android.content.ContentValues> contentValuesList) {
        boolean shouldAppendCharsetInfo;
        boolean reallyUseQuotedPrintable;
        boolean shouldAppendCharsetInfo2;
        boolean reallyUseQuotedPrintable2;
        if (contentValuesList != null) {
            if (this.mOnlyOneNoteFieldIsAvailable) {
                java.lang.StringBuilder noteBuilder = new java.lang.StringBuilder();
                boolean first = true;
                for (android.content.ContentValues contentValues : contentValuesList) {
                    java.lang.String note = contentValues.getAsString("data1");
                    if (note == null) {
                        note = "";
                    }
                    if (note.length() > 0) {
                        if (first) {
                            first = false;
                        } else {
                            noteBuilder.append(10);
                        }
                        noteBuilder.append(note);
                    }
                }
                java.lang.String noteStr = noteBuilder.toString();
                if (!com.navdy.hud.app.bluetooth.vcard.VCardUtils.containsOnlyPrintableAscii(noteStr)) {
                    shouldAppendCharsetInfo2 = true;
                } else {
                    shouldAppendCharsetInfo2 = false;
                }
                if (this.mShouldUseQuotedPrintable) {
                    if (!com.navdy.hud.app.bluetooth.vcard.VCardUtils.containsOnlyNonCrLfPrintableAscii(noteStr)) {
                        reallyUseQuotedPrintable2 = true;
                        appendLine(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_NOTE, noteStr, shouldAppendCharsetInfo2, reallyUseQuotedPrintable2);
                    }
                }
                reallyUseQuotedPrintable2 = false;
                appendLine(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_NOTE, noteStr, shouldAppendCharsetInfo2, reallyUseQuotedPrintable2);
            } else {
                for (android.content.ContentValues contentValues2 : contentValuesList) {
                    java.lang.String noteStr2 = contentValues2.getAsString("data1");
                    if (!android.text.TextUtils.isEmpty(noteStr2)) {
                        if (!com.navdy.hud.app.bluetooth.vcard.VCardUtils.containsOnlyPrintableAscii(noteStr2)) {
                            shouldAppendCharsetInfo = true;
                        } else {
                            shouldAppendCharsetInfo = false;
                        }
                        if (this.mShouldUseQuotedPrintable) {
                            if (!com.navdy.hud.app.bluetooth.vcard.VCardUtils.containsOnlyNonCrLfPrintableAscii(noteStr2)) {
                                reallyUseQuotedPrintable = true;
                                appendLine(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_NOTE, noteStr2, shouldAppendCharsetInfo, reallyUseQuotedPrintable);
                            }
                        }
                        reallyUseQuotedPrintable = false;
                        appendLine(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_NOTE, noteStr2, shouldAppendCharsetInfo, reallyUseQuotedPrintable);
                    }
                }
            }
        }
        return this;
    }

    public com.navdy.hud.app.bluetooth.vcard.VCardBuilder appendEvents(java.util.List<android.content.ContentValues> contentValuesList) {
        int eventType;
        if (contentValuesList != null) {
            java.lang.String primaryBirthday = null;
            java.lang.String secondaryBirthday = null;
            java.util.Iterator it = contentValuesList.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                android.content.ContentValues contentValues = (android.content.ContentValues) it.next();
                if (contentValues != null) {
                    java.lang.Integer eventTypeAsInteger = contentValues.getAsInteger("data2");
                    if (eventTypeAsInteger != null) {
                        eventType = eventTypeAsInteger.intValue();
                    } else {
                        eventType = 2;
                    }
                    if (eventType == 3) {
                        java.lang.String birthdayCandidate = contentValues.getAsString("data1");
                        if (birthdayCandidate != null) {
                            java.lang.Integer isSuperPrimaryAsInteger = contentValues.getAsInteger("is_super_primary");
                            boolean isSuperPrimary = isSuperPrimaryAsInteger != null ? isSuperPrimaryAsInteger.intValue() > 0 : false;
                            if (isSuperPrimary) {
                                primaryBirthday = birthdayCandidate;
                                break;
                            }
                            java.lang.Integer isPrimaryAsInteger = contentValues.getAsInteger("is_primary");
                            boolean isPrimary = isPrimaryAsInteger != null ? isPrimaryAsInteger.intValue() > 0 : false;
                            if (isPrimary) {
                                primaryBirthday = birthdayCandidate;
                            } else if (secondaryBirthday == null) {
                                secondaryBirthday = birthdayCandidate;
                            }
                        } else {
                            continue;
                        }
                    } else if (this.mUsesAndroidProperty) {
                        appendAndroidSpecificProperty("vnd.android.cursor.item/contact_event", contentValues);
                    }
                }
            }
            if (primaryBirthday != null) {
                appendLineWithCharsetAndQPDetection(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_BDAY, primaryBirthday.trim());
            } else if (secondaryBirthday != null) {
                appendLineWithCharsetAndQPDetection(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_BDAY, secondaryBirthday.trim());
            }
        }
        return this;
    }

    public com.navdy.hud.app.bluetooth.vcard.VCardBuilder appendRelation(java.util.List<android.content.ContentValues> contentValuesList) {
        if (this.mUsesAndroidProperty && contentValuesList != null) {
            for (android.content.ContentValues contentValues : contentValuesList) {
                if (contentValues != null) {
                    appendAndroidSpecificProperty("vnd.android.cursor.item/relation", contentValues);
                }
            }
        }
        return this;
    }

    public void appendPostalLine(int type, java.lang.String label, android.content.ContentValues contentValues, boolean isPrimary, boolean emitEveryTime) {
        boolean reallyUseQuotedPrintable;
        boolean appendCharset;
        java.lang.String addressValue;
        com.navdy.hud.app.bluetooth.vcard.VCardBuilder.PostalStruct postalStruct = tryConstructPostalStruct(contentValues);
        if (postalStruct != null) {
            reallyUseQuotedPrintable = postalStruct.reallyUseQuotedPrintable;
            appendCharset = postalStruct.appendCharset;
            addressValue = postalStruct.addressData;
        } else if (emitEveryTime) {
            reallyUseQuotedPrintable = false;
            appendCharset = false;
            addressValue = "";
        } else {
            return;
        }
        java.util.List<java.lang.String> parameterList = new java.util.ArrayList<>();
        if (isPrimary) {
            parameterList.add(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE_PREF);
        }
        switch (type) {
            case 0:
                if (!android.text.TextUtils.isEmpty(label)) {
                    if (com.navdy.hud.app.bluetooth.vcard.VCardUtils.containsOnlyAlphaDigitHyphen(label)) {
                        parameterList.add("X-" + label);
                        break;
                    }
                }
                break;
            case 1:
                parameterList.add(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE_HOME);
                break;
            case 2:
                parameterList.add(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE_WORK);
                break;
            case 3:
                break;
            default:
                android.util.Log.e(LOG_TAG, "Unknown StructuredPostal type: " + type);
                break;
        }
        this.mBuilder.append(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_ADR);
        if (!parameterList.isEmpty()) {
            this.mBuilder.append(";");
            appendTypeParameters(parameterList);
        }
        if (appendCharset) {
            this.mBuilder.append(";");
            this.mBuilder.append(this.mVCardCharsetParameter);
        }
        if (reallyUseQuotedPrintable) {
            this.mBuilder.append(";");
            this.mBuilder.append(VCARD_PARAM_ENCODING_QP);
        }
        this.mBuilder.append(":");
        this.mBuilder.append(addressValue);
        this.mBuilder.append(VCARD_END_OF_LINE);
    }

    public void appendEmailLine(int type, java.lang.String label, java.lang.String rawValue, boolean isPrimary) {
        java.lang.String typeAsString;
        switch (type) {
            case 0:
                if (!com.navdy.hud.app.bluetooth.vcard.VCardUtils.isMobilePhoneLabel(label)) {
                    if (!android.text.TextUtils.isEmpty(label)) {
                        if (com.navdy.hud.app.bluetooth.vcard.VCardUtils.containsOnlyAlphaDigitHyphen(label)) {
                            typeAsString = "X-" + label;
                            break;
                        }
                    }
                    typeAsString = null;
                    break;
                } else {
                    typeAsString = com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE_CELL;
                    break;
                }
            case 1:
                typeAsString = com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE_HOME;
                break;
            case 2:
                typeAsString = com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE_WORK;
                break;
            case 3:
                typeAsString = null;
                break;
            case 4:
                typeAsString = com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE_CELL;
                break;
            default:
                android.util.Log.e(LOG_TAG, "Unknown Email type: " + type);
                typeAsString = null;
                break;
        }
        java.util.List<java.lang.String> parameterList = new java.util.ArrayList<>();
        if (isPrimary) {
            parameterList.add(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE_PREF);
        }
        if (!android.text.TextUtils.isEmpty(typeAsString)) {
            parameterList.add(typeAsString);
        }
        appendLineWithCharsetAndQPDetection(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_EMAIL, parameterList, rawValue);
    }

    public void appendTelLine(java.lang.Integer typeAsInteger, java.lang.String label, java.lang.String encodedValue, boolean isPrimary) {
        int type;
        this.mBuilder.append(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_TEL);
        this.mBuilder.append(";");
        if (typeAsInteger == null) {
            type = 7;
        } else {
            type = typeAsInteger.intValue();
        }
        java.util.ArrayList<java.lang.String> parameterList = new java.util.ArrayList<>();
        switch (type) {
            case 0:
                if (!android.text.TextUtils.isEmpty(label)) {
                    if (!com.navdy.hud.app.bluetooth.vcard.VCardUtils.isMobilePhoneLabel(label)) {
                        if (!this.mIsV30OrV40) {
                            java.lang.String upperLabel = label.toUpperCase();
                            if (!com.navdy.hud.app.bluetooth.vcard.VCardUtils.isValidInV21ButUnknownToContactsPhoteType(upperLabel)) {
                                if (com.navdy.hud.app.bluetooth.vcard.VCardUtils.containsOnlyAlphaDigitHyphen(label)) {
                                    parameterList.add("X-" + label);
                                    break;
                                }
                            } else {
                                parameterList.add(upperLabel);
                                break;
                            }
                        } else {
                            parameterList.add(label);
                            break;
                        }
                    } else {
                        parameterList.add(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE_CELL);
                        break;
                    }
                } else {
                    parameterList.add(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE_VOICE);
                    break;
                }
                break;
            case 1:
                parameterList.addAll(java.util.Arrays.asList(new java.lang.String[]{com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE_HOME}));
                break;
            case 2:
                parameterList.add(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE_CELL);
                break;
            case 3:
                parameterList.addAll(java.util.Arrays.asList(new java.lang.String[]{com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE_WORK}));
                break;
            case 4:
                parameterList.addAll(java.util.Arrays.asList(new java.lang.String[]{com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE_WORK, com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE_FAX}));
                break;
            case 5:
                parameterList.addAll(java.util.Arrays.asList(new java.lang.String[]{com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE_HOME, com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE_FAX}));
                break;
            case 6:
                if (!this.mIsDoCoMo) {
                    parameterList.add(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE_PAGER);
                    break;
                } else {
                    parameterList.add(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE_VOICE);
                    break;
                }
            case 7:
                parameterList.add(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE_VOICE);
                break;
            case 9:
                parameterList.add(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE_CAR);
                break;
            case 10:
                parameterList.add(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE_WORK);
                isPrimary = true;
                break;
            case 11:
                parameterList.add(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE_ISDN);
                break;
            case 12:
                isPrimary = true;
                break;
            case 13:
                parameterList.add(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE_FAX);
                break;
            case 15:
                parameterList.add(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE_TLX);
                break;
            case 17:
                parameterList.addAll(java.util.Arrays.asList(new java.lang.String[]{com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE_WORK, com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE_CELL}));
                break;
            case 18:
                parameterList.add(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE_WORK);
                if (!this.mIsDoCoMo) {
                    parameterList.add(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE_PAGER);
                    break;
                } else {
                    parameterList.add(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE_VOICE);
                    break;
                }
            case 20:
                parameterList.add(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE_MSG);
                break;
        }
        if (isPrimary) {
            parameterList.add(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE_PREF);
        }
        if (parameterList.isEmpty()) {
            appendUncommonPhoneType(this.mBuilder, java.lang.Integer.valueOf(type));
        } else {
            appendTypeParameters(parameterList);
        }
        this.mBuilder.append(":");
        this.mBuilder.append(encodedValue);
        this.mBuilder.append(VCARD_END_OF_LINE);
    }

    private void appendUncommonPhoneType(java.lang.StringBuilder builder, java.lang.Integer type) {
        if (this.mIsDoCoMo) {
            builder.append(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE_VOICE);
            return;
        }
        java.lang.String phoneType = com.navdy.hud.app.bluetooth.vcard.VCardUtils.getPhoneTypeString(type);
        if (phoneType != null) {
            appendTypeParameter(phoneType);
        } else {
            android.util.Log.e(LOG_TAG, "Unknown or unsupported (by vCard) Phone type: " + type);
        }
    }

    public void appendPhotoLine(java.lang.String encodedValue, java.lang.String photoType) {
        java.lang.StringBuilder tmpBuilder = new java.lang.StringBuilder();
        tmpBuilder.append(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_PHOTO);
        tmpBuilder.append(";");
        if (this.mIsV30OrV40) {
            tmpBuilder.append(VCARD_PARAM_ENCODING_BASE64_AS_B);
        } else {
            tmpBuilder.append(VCARD_PARAM_ENCODING_BASE64_V21);
        }
        tmpBuilder.append(";");
        appendTypeParameter(tmpBuilder, photoType);
        tmpBuilder.append(":");
        tmpBuilder.append(encodedValue);
        java.lang.String tmpStr = tmpBuilder.toString();
        java.lang.StringBuilder tmpBuilder2 = new java.lang.StringBuilder();
        int lineCount = 0;
        int length = tmpStr.length();
        int maxNumForFirstLine = 75 - VCARD_END_OF_LINE.length();
        int maxNumInGeneral = maxNumForFirstLine - " ".length();
        int maxNum = maxNumForFirstLine;
        for (int i = 0; i < length; i++) {
            tmpBuilder2.append(tmpStr.charAt(i));
            lineCount++;
            if (lineCount > maxNum) {
                tmpBuilder2.append(VCARD_END_OF_LINE);
                tmpBuilder2.append(" ");
                maxNum = maxNumInGeneral;
                lineCount = 0;
            }
        }
        this.mBuilder.append(tmpBuilder2.toString());
        this.mBuilder.append(VCARD_END_OF_LINE);
        this.mBuilder.append(VCARD_END_OF_LINE);
    }

    public com.navdy.hud.app.bluetooth.vcard.VCardBuilder appendSipAddresses(java.util.List<android.content.ContentValues> contentValuesList) {
        boolean useXProperty;
        java.lang.String propertyName;
        if (this.mIsV30OrV40) {
            useXProperty = false;
        } else {
            if (this.mUsesDefactProperty) {
                useXProperty = true;
            }
            return this;
        }
        if (contentValuesList != null) {
            for (android.content.ContentValues contentValues : contentValuesList) {
                java.lang.String sipAddress = contentValues.getAsString("data1");
                if (!android.text.TextUtils.isEmpty(sipAddress)) {
                    if (useXProperty) {
                        if (sipAddress.startsWith("sip:")) {
                            if (sipAddress.length() != 4) {
                                sipAddress = sipAddress.substring(4);
                            }
                        }
                        appendLineWithCharsetAndQPDetection(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_X_SIP, sipAddress);
                    } else {
                        if (!sipAddress.startsWith("sip:")) {
                            sipAddress = "sip:" + sipAddress;
                        }
                        if (com.navdy.hud.app.bluetooth.vcard.VCardConfig.isVersion40(this.mVCardType)) {
                            propertyName = com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_TEL;
                        } else {
                            propertyName = com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_IMPP;
                        }
                        appendLineWithCharsetAndQPDetection(propertyName, sipAddress);
                    }
                }
            }
        }
        return this;
    }

    public void appendAndroidSpecificProperty(java.lang.String mimeType, android.content.ContentValues contentValues) {
        boolean needCharset;
        boolean reallyUseQuotedPrintable;
        java.lang.String encodedValue;
        if (sAllowedAndroidPropertySet.contains(mimeType)) {
            java.util.List<java.lang.String> rawValueList = new java.util.ArrayList<>();
            for (int i = 1; i <= 15; i++) {
                java.lang.String value = contentValues.getAsString("data" + i);
                if (value == null) {
                    value = "";
                }
                rawValueList.add(value);
            }
            if (!this.mShouldAppendCharsetParam || com.navdy.hud.app.bluetooth.vcard.VCardUtils.containsOnlyNonCrLfPrintableAscii((java.util.Collection<java.lang.String>) rawValueList)) {
                needCharset = false;
            } else {
                needCharset = true;
            }
            if (!this.mShouldUseQuotedPrintable || com.navdy.hud.app.bluetooth.vcard.VCardUtils.containsOnlyNonCrLfPrintableAscii((java.util.Collection<java.lang.String>) rawValueList)) {
                reallyUseQuotedPrintable = false;
            } else {
                reallyUseQuotedPrintable = true;
            }
            this.mBuilder.append(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_X_ANDROID_CUSTOM);
            if (needCharset) {
                this.mBuilder.append(";");
                this.mBuilder.append(this.mVCardCharsetParameter);
            }
            if (reallyUseQuotedPrintable) {
                this.mBuilder.append(";");
                this.mBuilder.append(VCARD_PARAM_ENCODING_QP);
            }
            this.mBuilder.append(":");
            this.mBuilder.append(mimeType);
            for (java.lang.String rawValue : rawValueList) {
                if (reallyUseQuotedPrintable) {
                    encodedValue = encodeQuotedPrintable(rawValue);
                } else {
                    encodedValue = escapeCharacters(rawValue);
                }
                this.mBuilder.append(";");
                this.mBuilder.append(encodedValue);
            }
            this.mBuilder.append(VCARD_END_OF_LINE);
        }
    }

    public void appendLineWithCharsetAndQPDetection(java.lang.String propertyName, java.lang.String rawValue) {
        appendLineWithCharsetAndQPDetection(propertyName, null, rawValue);
    }

    public void appendLineWithCharsetAndQPDetection(java.lang.String propertyName, java.util.List<java.lang.String> rawValueList) {
        appendLineWithCharsetAndQPDetection(propertyName, null, rawValueList);
    }

    public void appendLineWithCharsetAndQPDetection(java.lang.String propertyName, java.util.List<java.lang.String> parameterList, java.lang.String rawValue) {
        boolean needCharset;
        boolean reallyUseQuotedPrintable;
        if (!com.navdy.hud.app.bluetooth.vcard.VCardUtils.containsOnlyPrintableAscii(rawValue)) {
            needCharset = true;
        } else {
            needCharset = false;
        }
        if (this.mShouldUseQuotedPrintable) {
            if (!com.navdy.hud.app.bluetooth.vcard.VCardUtils.containsOnlyNonCrLfPrintableAscii(rawValue)) {
                reallyUseQuotedPrintable = true;
                appendLine(propertyName, parameterList, rawValue, needCharset, reallyUseQuotedPrintable);
            }
        }
        reallyUseQuotedPrintable = false;
        appendLine(propertyName, parameterList, rawValue, needCharset, reallyUseQuotedPrintable);
    }

    public void appendLineWithCharsetAndQPDetection(java.lang.String propertyName, java.util.List<java.lang.String> parameterList, java.util.List<java.lang.String> rawValueList) {
        boolean needCharset;
        boolean reallyUseQuotedPrintable;
        if (!this.mShouldAppendCharsetParam || com.navdy.hud.app.bluetooth.vcard.VCardUtils.containsOnlyNonCrLfPrintableAscii((java.util.Collection<java.lang.String>) rawValueList)) {
            needCharset = false;
        } else {
            needCharset = true;
        }
        if (!this.mShouldUseQuotedPrintable || com.navdy.hud.app.bluetooth.vcard.VCardUtils.containsOnlyNonCrLfPrintableAscii((java.util.Collection<java.lang.String>) rawValueList)) {
            reallyUseQuotedPrintable = false;
        } else {
            reallyUseQuotedPrintable = true;
        }
        appendLine(propertyName, parameterList, rawValueList, needCharset, reallyUseQuotedPrintable);
    }

    public void appendLine(java.lang.String propertyName, java.lang.String rawValue) {
        appendLine(propertyName, rawValue, false, false);
    }

    public void appendLine(java.lang.String propertyName, java.util.List<java.lang.String> rawValueList) {
        appendLine(propertyName, rawValueList, false, false);
    }

    public void appendLine(java.lang.String propertyName, java.lang.String rawValue, boolean needCharset, boolean reallyUseQuotedPrintable) {
        appendLine(propertyName, null, rawValue, needCharset, reallyUseQuotedPrintable);
    }

    public void appendLine(java.lang.String propertyName, java.util.List<java.lang.String> parameterList, java.lang.String rawValue) {
        appendLine(propertyName, parameterList, rawValue, false, false);
    }

    public void appendLine(java.lang.String propertyName, java.util.List<java.lang.String> parameterList, java.lang.String rawValue, boolean needCharset, boolean reallyUseQuotedPrintable) {
        java.lang.String encodedValue;
        this.mBuilder.append(propertyName);
        if (parameterList != null && parameterList.size() > 0) {
            this.mBuilder.append(";");
            appendTypeParameters(parameterList);
        }
        if (needCharset) {
            this.mBuilder.append(";");
            this.mBuilder.append(this.mVCardCharsetParameter);
        }
        if (reallyUseQuotedPrintable) {
            this.mBuilder.append(";");
            this.mBuilder.append(VCARD_PARAM_ENCODING_QP);
            encodedValue = encodeQuotedPrintable(rawValue);
        } else {
            encodedValue = escapeCharacters(rawValue);
        }
        this.mBuilder.append(":");
        this.mBuilder.append(encodedValue);
        this.mBuilder.append(VCARD_END_OF_LINE);
    }

    public void appendLine(java.lang.String propertyName, java.util.List<java.lang.String> rawValueList, boolean needCharset, boolean needQuotedPrintable) {
        appendLine(propertyName, null, rawValueList, needCharset, needQuotedPrintable);
    }

    public void appendLine(java.lang.String propertyName, java.util.List<java.lang.String> parameterList, java.util.List<java.lang.String> rawValueList, boolean needCharset, boolean needQuotedPrintable) {
        java.lang.String encodedValue;
        this.mBuilder.append(propertyName);
        if (parameterList != null && parameterList.size() > 0) {
            this.mBuilder.append(";");
            appendTypeParameters(parameterList);
        }
        if (needCharset) {
            this.mBuilder.append(";");
            this.mBuilder.append(this.mVCardCharsetParameter);
        }
        if (needQuotedPrintable) {
            this.mBuilder.append(";");
            this.mBuilder.append(VCARD_PARAM_ENCODING_QP);
        }
        this.mBuilder.append(":");
        boolean first = true;
        for (java.lang.String rawValue : rawValueList) {
            if (needQuotedPrintable) {
                encodedValue = encodeQuotedPrintable(rawValue);
            } else {
                encodedValue = escapeCharacters(rawValue);
            }
            if (first) {
                first = false;
            } else {
                this.mBuilder.append(";");
            }
            this.mBuilder.append(encodedValue);
        }
        this.mBuilder.append(VCARD_END_OF_LINE);
    }

    private void appendTypeParameters(java.util.List<java.lang.String> types) {
        java.lang.String encoded;
        boolean first = true;
        for (java.lang.String typeValue : types) {
            if (com.navdy.hud.app.bluetooth.vcard.VCardConfig.isVersion30(this.mVCardType) || com.navdy.hud.app.bluetooth.vcard.VCardConfig.isVersion40(this.mVCardType)) {
                if (com.navdy.hud.app.bluetooth.vcard.VCardConfig.isVersion40(this.mVCardType)) {
                    encoded = com.navdy.hud.app.bluetooth.vcard.VCardUtils.toStringAsV40ParamValue(typeValue);
                } else {
                    encoded = com.navdy.hud.app.bluetooth.vcard.VCardUtils.toStringAsV30ParamValue(typeValue);
                }
                if (!android.text.TextUtils.isEmpty(encoded)) {
                    if (first) {
                        first = false;
                    } else {
                        this.mBuilder.append(";");
                    }
                    appendTypeParameter(encoded);
                }
            } else if (com.navdy.hud.app.bluetooth.vcard.VCardUtils.isV21Word(typeValue)) {
                if (first) {
                    first = false;
                } else {
                    this.mBuilder.append(";");
                }
                appendTypeParameter(typeValue);
            }
        }
    }

    private void appendTypeParameter(java.lang.String type) {
        appendTypeParameter(this.mBuilder, type);
    }

    private void appendTypeParameter(java.lang.StringBuilder builder, java.lang.String type) {
        if (com.navdy.hud.app.bluetooth.vcard.VCardConfig.isVersion40(this.mVCardType) || ((com.navdy.hud.app.bluetooth.vcard.VCardConfig.isVersion30(this.mVCardType) || this.mAppendTypeParamName) && !this.mIsDoCoMo)) {
            builder.append(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE).append(VCARD_PARAM_EQUAL);
        }
        builder.append(type);
    }

    private boolean shouldAppendCharsetParam(java.lang.String... propertyValueList) {
        if (!this.mShouldAppendCharsetParam) {
            return false;
        }
        int length = propertyValueList.length;
        for (int i = 0; i < length; i++) {
            if (!com.navdy.hud.app.bluetooth.vcard.VCardUtils.containsOnlyPrintableAscii(propertyValueList[i])) {
                return true;
            }
        }
        return false;
    }

    private java.lang.String encodeQuotedPrintable(java.lang.String str) {
        byte[] strArray;
        if (android.text.TextUtils.isEmpty(str)) {
            return "";
        }
        java.lang.StringBuilder builder = new java.lang.StringBuilder();
        int index = 0;
        int lineCount = 0;
        try {
            strArray = str.getBytes(this.mCharset);
        } catch (java.io.UnsupportedEncodingException e) {
            android.util.Log.e(LOG_TAG, "Charset " + this.mCharset + " cannot be used. " + "Try default charset");
            strArray = str.getBytes();
        }
        while (index < strArray.length) {
            builder.append(java.lang.String.format("=%02X", new java.lang.Object[]{java.lang.Byte.valueOf(strArray[index])}));
            index++;
            lineCount += 3;
            if (lineCount >= 67) {
                builder.append("=\r\n");
                lineCount = 0;
            }
        }
        return builder.toString();
    }

    private java.lang.String escapeCharacters(java.lang.String unescaped) {
        if (android.text.TextUtils.isEmpty(unescaped)) {
            return "";
        }
        java.lang.StringBuilder tmpBuilder = new java.lang.StringBuilder();
        int length = unescaped.length();
        for (int i = 0; i < length; i++) {
            char ch = unescaped.charAt(i);
            switch (ch) {
                case 13:
                    if (i + 1 < length && unescaped.charAt(i) == 10) {
                        break;
                    }
                case 10:
                    tmpBuilder.append("\\n");
                    break;
                case ',':
                    if (!this.mIsV30OrV40) {
                        tmpBuilder.append(ch);
                        break;
                    } else {
                        tmpBuilder.append("\\,");
                        break;
                    }
                case ';':
                    tmpBuilder.append('\\');
                    tmpBuilder.append(';');
                    break;
                case '\\':
                    if (this.mIsV30OrV40) {
                        tmpBuilder.append("\\\\");
                        break;
                    }
                case '<':
                case '>':
                    if (!this.mIsDoCoMo) {
                        tmpBuilder.append(ch);
                        break;
                    } else {
                        tmpBuilder.append('\\');
                        tmpBuilder.append(ch);
                        break;
                    }
                default:
                    tmpBuilder.append(ch);
                    break;
            }
        }
        return tmpBuilder.toString();
    }

    public java.lang.String toString() {
        if (!this.mEndAppended) {
            if (this.mIsDoCoMo) {
                appendLine(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_X_CLASS, VCARD_DATA_PUBLIC);
                appendLine(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_X_REDUCTION, "");
                appendLine(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_X_NO, "");
                appendLine(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_X_DCM_HMN_MODE, "");
            }
            appendLine(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_END, VCARD_DATA_VCARD);
            this.mEndAppended = true;
        }
        return this.mBuilder.toString();
    }
}
