package com.navdy.hud.app.bluetooth.vcard.exception;

public class VCardException extends java.lang.Exception {
    public VCardException() {
    }

    public VCardException(java.lang.String message) {
        super(message);
    }
}
