package com.navdy.hud.app.bluetooth.vcard;

public class VCardEntryCounter implements com.navdy.hud.app.bluetooth.vcard.VCardInterpreter {
    private int mCount;

    public int getCount() {
        return this.mCount;
    }

    public void onVCardStarted() {
    }

    public void onVCardEnded() {
    }

    public void onEntryStarted() {
    }

    public void onEntryEnded() {
        this.mCount++;
    }

    public void onPropertyCreated(com.navdy.hud.app.bluetooth.vcard.VCardProperty property) {
    }
}
