package com.navdy.hud.app.bluetooth.vcard;

public class VCardUtils {
    private static final java.lang.String LOG_TAG = "vCard";
    private static final int[] sEscapeIndicatorsV30 = {58, 59, 44, 32};
    private static final int[] sEscapeIndicatorsV40 = {59, 58};
    private static final java.util.Map<java.lang.Integer, java.lang.String> sKnownImPropNameMap_ItoS = new java.util.HashMap();
    private static final java.util.Map<java.lang.String, java.lang.Integer> sKnownPhoneTypeMap_StoI = new java.util.HashMap();
    private static final java.util.Map<java.lang.Integer, java.lang.String> sKnownPhoneTypesMap_ItoS = new java.util.HashMap();
    private static final java.util.Set<java.lang.String> sMobilePhoneLabelSet = new java.util.HashSet(java.util.Arrays.asList(new java.lang.String[]{"MOBILE", "\u643a\u5e2f\u96fb\u8a71", "\u643a\u5e2f", "\u30b1\u30a4\u30bf\u30a4", "\uff79\uff72\uff80\uff72"}));
    private static final java.util.Set<java.lang.String> sPhoneTypesUnknownToContactsSet = new java.util.HashSet();
    private static final java.util.Set<java.lang.Character> sUnAcceptableAsciiInV21WordSet = new java.util.HashSet(java.util.Arrays.asList(new java.lang.Character[]{java.lang.Character.valueOf('['), java.lang.Character.valueOf(']'), java.lang.Character.valueOf('='), java.lang.Character.valueOf(':'), java.lang.Character.valueOf('.'), java.lang.Character.valueOf(','), java.lang.Character.valueOf(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.SPACE_CHAR)}));

    private static class DecoderException extends java.lang.Exception {
        public DecoderException(java.lang.String pMessage) {
            super(pMessage);
        }
    }

    public static class PhoneNumberUtilsPort {
        public static java.lang.String formatNumber(java.lang.String source, int defaultFormattingType) {
            android.text.SpannableStringBuilder text = new android.text.SpannableStringBuilder(source);
            android.telephony.PhoneNumberUtils.formatNumber(text, defaultFormattingType);
            return text.toString();
        }
    }

    private static class QuotedPrintableCodecPort {
        private static byte ESCAPE_CHAR = 61;

        private QuotedPrintableCodecPort() {
        }

        public static final byte[] decodeQuotedPrintable(byte[] bytes) throws com.navdy.hud.app.bluetooth.vcard.VCardUtils.DecoderException {
            if (bytes == null) {
                return null;
            }
            java.io.ByteArrayOutputStream buffer = new java.io.ByteArrayOutputStream();
            int i = 0;
            while (i < bytes.length) {
                byte b = bytes[i];
                if (b == ESCAPE_CHAR) {
                    int i2 = i + 1;
                    try {
                        int u = java.lang.Character.digit((char) bytes[i2], 16);
                        i = i2 + 1;
                        int l = java.lang.Character.digit((char) bytes[i], 16);
                        if (u == -1 || l == -1) {
                            throw new com.navdy.hud.app.bluetooth.vcard.VCardUtils.DecoderException("Invalid quoted-printable encoding");
                        }
                        buffer.write((char) ((u << 4) + l));
                    } catch (java.lang.ArrayIndexOutOfBoundsException e) {
                        throw new com.navdy.hud.app.bluetooth.vcard.VCardUtils.DecoderException("Invalid quoted-printable encoding");
                    }
                } else {
                    buffer.write(b);
                }
                i++;
            }
            return buffer.toByteArray();
        }
    }

    public static class TextUtilsPort {
        public static boolean isPrintableAscii(char c) {
            return (' ' <= c && c <= '~') || c == 13 || c == 10;
        }

        public static boolean isPrintableAsciiOnly(java.lang.CharSequence str) {
            int len = str.length();
            for (int i = 0; i < len; i++) {
                if (!isPrintableAscii(str.charAt(i))) {
                    return false;
                }
            }
            return true;
        }
    }

    static {
        sKnownPhoneTypesMap_ItoS.put(java.lang.Integer.valueOf(9), com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE_CAR);
        sKnownPhoneTypeMap_StoI.put(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE_CAR, java.lang.Integer.valueOf(9));
        sKnownPhoneTypesMap_ItoS.put(java.lang.Integer.valueOf(6), com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE_PAGER);
        sKnownPhoneTypeMap_StoI.put(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE_PAGER, java.lang.Integer.valueOf(6));
        sKnownPhoneTypesMap_ItoS.put(java.lang.Integer.valueOf(11), com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE_ISDN);
        sKnownPhoneTypeMap_StoI.put(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE_ISDN, java.lang.Integer.valueOf(11));
        sKnownPhoneTypeMap_StoI.put(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE_HOME, java.lang.Integer.valueOf(1));
        sKnownPhoneTypeMap_StoI.put(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE_WORK, java.lang.Integer.valueOf(3));
        sKnownPhoneTypeMap_StoI.put(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE_CELL, java.lang.Integer.valueOf(2));
        sKnownPhoneTypeMap_StoI.put("OTHER", java.lang.Integer.valueOf(7));
        sKnownPhoneTypeMap_StoI.put(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_PHONE_EXTRA_TYPE_CALLBACK, java.lang.Integer.valueOf(8));
        sKnownPhoneTypeMap_StoI.put(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_PHONE_EXTRA_TYPE_COMPANY_MAIN, java.lang.Integer.valueOf(10));
        sKnownPhoneTypeMap_StoI.put(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_PHONE_EXTRA_TYPE_RADIO, java.lang.Integer.valueOf(14));
        sKnownPhoneTypeMap_StoI.put(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_PHONE_EXTRA_TYPE_TTY_TDD, java.lang.Integer.valueOf(16));
        sKnownPhoneTypeMap_StoI.put(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_PHONE_EXTRA_TYPE_ASSISTANT, java.lang.Integer.valueOf(19));
        sKnownPhoneTypeMap_StoI.put(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE_VOICE, java.lang.Integer.valueOf(7));
        sPhoneTypesUnknownToContactsSet.add(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE_MODEM);
        sPhoneTypesUnknownToContactsSet.add(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE_MSG);
        sPhoneTypesUnknownToContactsSet.add(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE_BBS);
        sPhoneTypesUnknownToContactsSet.add(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE_VIDEO);
        sKnownImPropNameMap_ItoS.put(java.lang.Integer.valueOf(0), com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_X_AIM);
        sKnownImPropNameMap_ItoS.put(java.lang.Integer.valueOf(1), com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_X_MSN);
        sKnownImPropNameMap_ItoS.put(java.lang.Integer.valueOf(2), com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_X_YAHOO);
        sKnownImPropNameMap_ItoS.put(java.lang.Integer.valueOf(3), com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_X_SKYPE_USERNAME);
        sKnownImPropNameMap_ItoS.put(java.lang.Integer.valueOf(5), com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_X_GOOGLE_TALK);
        sKnownImPropNameMap_ItoS.put(java.lang.Integer.valueOf(6), com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_X_ICQ);
        sKnownImPropNameMap_ItoS.put(java.lang.Integer.valueOf(7), com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_X_JABBER);
        sKnownImPropNameMap_ItoS.put(java.lang.Integer.valueOf(4), com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_X_QQ);
        sKnownImPropNameMap_ItoS.put(java.lang.Integer.valueOf(8), com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_X_NETMEETING);
    }

    public static java.lang.String getPhoneTypeString(java.lang.Integer type) {
        return (java.lang.String) sKnownPhoneTypesMap_ItoS.get(type);
    }

    public static java.lang.Object getPhoneTypeFromStrings(java.util.Collection<java.lang.String> types, java.lang.String number) {
        java.lang.String labelCandidate;
        if (number == null) {
            number = "";
        }
        int type = -1;
        java.lang.String label = null;
        boolean isFax = false;
        boolean hasPref = false;
        if (types != null) {
            for (java.lang.String typeStringOrg : types) {
                if (typeStringOrg != null) {
                    java.lang.String typeStringUpperCase = typeStringOrg.toUpperCase();
                    if (typeStringUpperCase.equals(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE_PREF)) {
                        hasPref = true;
                    } else if (typeStringUpperCase.equals(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE_FAX)) {
                        isFax = true;
                    } else {
                        if (!typeStringUpperCase.startsWith("X-") || type >= 0) {
                            labelCandidate = typeStringOrg;
                        } else {
                            labelCandidate = typeStringOrg.substring(2);
                        }
                        if (labelCandidate.length() != 0) {
                            java.lang.Integer tmp = (java.lang.Integer) sKnownPhoneTypeMap_StoI.get(labelCandidate.toUpperCase());
                            if (tmp != null) {
                                int typeCandidate = tmp.intValue();
                                int indexOfAt = number.indexOf(com.navdy.hud.app.framework.glance.GlanceConstants.AT_SEPARATOR);
                                if ((typeCandidate == 6 && indexOfAt > 0 && indexOfAt < number.length() - 1) || type < 0 || type == 0 || type == 7) {
                                    type = tmp.intValue();
                                }
                            } else if (type < 0) {
                                type = 0;
                                label = labelCandidate;
                            }
                        }
                    }
                }
            }
        }
        if (type < 0) {
            if (hasPref) {
                type = 12;
            } else {
                type = 1;
            }
        }
        if (isFax) {
            if (type == 1) {
                type = 5;
            } else if (type == 3) {
                type = 4;
            } else if (type == 7) {
                type = 13;
            }
        }
        return type == 0 ? label : java.lang.Integer.valueOf(type);
    }

    public static boolean isMobilePhoneLabel(java.lang.String label) {
        return "_AUTO_CELL".equals(label) || sMobilePhoneLabelSet.contains(label);
    }

    public static boolean isValidInV21ButUnknownToContactsPhoteType(java.lang.String label) {
        return sPhoneTypesUnknownToContactsSet.contains(label);
    }

    public static java.lang.String getPropertyNameForIm(int protocol) {
        return (java.lang.String) sKnownImPropNameMap_ItoS.get(java.lang.Integer.valueOf(protocol));
    }

    public static java.lang.String[] sortNameElements(int nameOrder, java.lang.String familyName, java.lang.String middleName, java.lang.String givenName) {
        java.lang.String[] list = new java.lang.String[3];
        switch (com.navdy.hud.app.bluetooth.vcard.VCardConfig.getNameOrderType(nameOrder)) {
            case 4:
                list[0] = middleName;
                list[1] = givenName;
                list[2] = familyName;
                break;
            case 8:
                if (containsOnlyPrintableAscii(familyName)) {
                    if (containsOnlyPrintableAscii(givenName)) {
                        list[0] = givenName;
                        list[1] = middleName;
                        list[2] = familyName;
                        break;
                    }
                }
                list[0] = familyName;
                list[1] = middleName;
                list[2] = givenName;
                break;
            default:
                list[0] = givenName;
                list[1] = middleName;
                list[2] = familyName;
                break;
        }
        return list;
    }

    public static int getPhoneNumberFormat(int vcardType) {
        if (com.navdy.hud.app.bluetooth.vcard.VCardConfig.isJapaneseDevice(vcardType)) {
            return 2;
        }
        return 1;
    }

    public static java.lang.String constructNameFromElements(int nameOrder, java.lang.String familyName, java.lang.String middleName, java.lang.String givenName) {
        return constructNameFromElements(nameOrder, familyName, middleName, givenName, null, null);
    }

    public static java.lang.String constructNameFromElements(int nameOrder, java.lang.String familyName, java.lang.String middleName, java.lang.String givenName, java.lang.String prefix, java.lang.String suffix) {
        java.lang.StringBuilder builder = new java.lang.StringBuilder();
        java.lang.String[] nameList = sortNameElements(nameOrder, familyName, middleName, givenName);
        boolean first = true;
        if (!android.text.TextUtils.isEmpty(prefix)) {
            first = false;
            builder.append(prefix);
        }
        for (java.lang.String namePart : nameList) {
            if (!android.text.TextUtils.isEmpty(namePart)) {
                if (first) {
                    first = false;
                } else {
                    builder.append(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.SPACE_CHAR);
                }
                builder.append(namePart);
            }
        }
        if (!android.text.TextUtils.isEmpty(suffix)) {
            if (!first) {
                builder.append(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.SPACE_CHAR);
            }
            builder.append(suffix);
        }
        return builder.toString();
    }

    public static java.util.List<java.lang.String> constructListFromValue(java.lang.String value, int vcardType) {
        java.lang.String unescapedString;
        java.util.List<java.lang.String> list = new java.util.ArrayList<>();
        java.lang.StringBuilder builder = new java.lang.StringBuilder();
        int length = value.length();
        int i = 0;
        while (i < length) {
            char ch = value.charAt(i);
            if (ch == '\\' && i < length - 1) {
                char nextCh = value.charAt(i + 1);
                if (com.navdy.hud.app.bluetooth.vcard.VCardConfig.isVersion40(vcardType)) {
                    unescapedString = com.navdy.hud.app.bluetooth.vcard.VCardParserImpl_V40.unescapeCharacter(nextCh);
                } else if (com.navdy.hud.app.bluetooth.vcard.VCardConfig.isVersion30(vcardType)) {
                    unescapedString = com.navdy.hud.app.bluetooth.vcard.VCardParserImpl_V30.unescapeCharacter(nextCh);
                } else {
                    if (!com.navdy.hud.app.bluetooth.vcard.VCardConfig.isVersion21(vcardType)) {
                        android.util.Log.w(LOG_TAG, "Unknown vCard type");
                    }
                    unescapedString = com.navdy.hud.app.bluetooth.vcard.VCardParserImpl_V21.unescapeCharacter(nextCh);
                }
                if (unescapedString != null) {
                    builder.append(unescapedString);
                    i++;
                } else {
                    builder.append(ch);
                }
            } else if (ch == ';') {
                list.add(builder.toString());
                builder = new java.lang.StringBuilder();
            } else {
                builder.append(ch);
            }
            i++;
        }
        list.add(builder.toString());
        return list;
    }

    public static boolean containsOnlyPrintableAscii(java.lang.String... values) {
        if (values == null) {
            return true;
        }
        return containsOnlyPrintableAscii((java.util.Collection<java.lang.String>) java.util.Arrays.asList(values));
    }

    public static boolean containsOnlyPrintableAscii(java.util.Collection<java.lang.String> values) {
        if (values == null) {
            return true;
        }
        for (java.lang.String value : values) {
            if (!android.text.TextUtils.isEmpty(value) && !com.navdy.hud.app.bluetooth.vcard.VCardUtils.TextUtilsPort.isPrintableAsciiOnly(value)) {
                return false;
            }
        }
        return true;
    }

    public static boolean containsOnlyNonCrLfPrintableAscii(java.lang.String... values) {
        if (values == null) {
            return true;
        }
        return containsOnlyNonCrLfPrintableAscii((java.util.Collection<java.lang.String>) java.util.Arrays.asList(values));
    }

    public static boolean containsOnlyNonCrLfPrintableAscii(java.util.Collection<java.lang.String> values) {
        if (values == null) {
            return true;
        }
        for (java.lang.String value : values) {
            if (!android.text.TextUtils.isEmpty(value)) {
                int length = value.length();
                for (int i = 0; i < length; i = value.offsetByCodePoints(i, 1)) {
                    int c = value.codePointAt(i);
                    if (32 > c || c > 126) {
                        return false;
                    }
                }
                continue;
            }
        }
        return true;
    }

    public static boolean containsOnlyAlphaDigitHyphen(java.lang.String... values) {
        if (values == null) {
            return true;
        }
        return containsOnlyAlphaDigitHyphen((java.util.Collection<java.lang.String>) java.util.Arrays.asList(values));
    }

    public static boolean containsOnlyAlphaDigitHyphen(java.util.Collection<java.lang.String> values) {
        if (values == null) {
            return true;
        }
        for (java.lang.String str : values) {
            if (!android.text.TextUtils.isEmpty(str)) {
                int length = str.length();
                for (int i = 0; i < length; i = str.offsetByCodePoints(i, 1)) {
                    int codepoint = str.codePointAt(i);
                    if ((97 > codepoint || codepoint >= 123) && ((65 > codepoint || codepoint >= 91) && ((48 > codepoint || codepoint >= 58) && codepoint != 45))) {
                        return false;
                    }
                }
                continue;
            }
        }
        return true;
    }

    public static boolean containsOnlyWhiteSpaces(java.lang.String... values) {
        if (values == null) {
            return true;
        }
        return containsOnlyWhiteSpaces((java.util.Collection<java.lang.String>) java.util.Arrays.asList(values));
    }

    public static boolean containsOnlyWhiteSpaces(java.util.Collection<java.lang.String> values) {
        if (values == null) {
            return true;
        }
        for (java.lang.String str : values) {
            if (!android.text.TextUtils.isEmpty(str)) {
                int length = str.length();
                for (int i = 0; i < length; i = str.offsetByCodePoints(i, 1)) {
                    if (!java.lang.Character.isWhitespace(str.codePointAt(i))) {
                        return false;
                    }
                }
                continue;
            }
        }
        return true;
    }

    public static boolean isV21Word(java.lang.String value) {
        if (android.text.TextUtils.isEmpty(value)) {
            return true;
        }
        int length = value.length();
        int i = 0;
        while (i < length) {
            int c = value.codePointAt(i);
            if (32 > c || c > 126 || sUnAcceptableAsciiInV21WordSet.contains(java.lang.Character.valueOf((char) c))) {
                return false;
            }
            i = value.offsetByCodePoints(i, 1);
        }
        return true;
    }

    public static java.lang.String toStringAsV30ParamValue(java.lang.String value) {
        return toStringAsParamValue(value, sEscapeIndicatorsV30);
    }

    public static java.lang.String toStringAsV40ParamValue(java.lang.String value) {
        return toStringAsParamValue(value, sEscapeIndicatorsV40);
    }

    private static java.lang.String toStringAsParamValue(java.lang.String value, int[] escapeIndicators) {
        if (android.text.TextUtils.isEmpty(value)) {
            value = "";
        }
        java.lang.StringBuilder builder = new java.lang.StringBuilder();
        int length = value.length();
        boolean needQuote = false;
        for (int i = 0; i < length; i = value.offsetByCodePoints(i, 1)) {
            int codePoint = value.codePointAt(i);
            if (codePoint >= 32 && codePoint != 34) {
                builder.appendCodePoint(codePoint);
                int length2 = escapeIndicators.length;
                int i2 = 0;
                while (true) {
                    if (i2 >= length2) {
                        break;
                    } else if (codePoint == escapeIndicators[i2]) {
                        needQuote = true;
                        break;
                    } else {
                        i2++;
                    }
                }
            }
        }
        java.lang.String result = builder.toString();
        if (!result.isEmpty()) {
            if (!containsOnlyWhiteSpaces(result)) {
                return needQuote ? kotlin.text.Typography.quote + result + kotlin.text.Typography.quote : result;
            }
        }
        return "";
    }

    public static java.lang.String toHalfWidthString(java.lang.String orgString) {
        if (android.text.TextUtils.isEmpty(orgString)) {
            return null;
        }
        java.lang.StringBuilder builder = new java.lang.StringBuilder();
        int length = orgString.length();
        int i = 0;
        while (i < length) {
            char ch = orgString.charAt(i);
            java.lang.String halfWidthText = com.navdy.hud.app.bluetooth.vcard.JapaneseUtils.tryGetHalfWidthText(ch);
            if (halfWidthText != null) {
                builder.append(halfWidthText);
            } else {
                builder.append(ch);
            }
            i = orgString.offsetByCodePoints(i, 1);
        }
        return builder.toString();
    }

    public static java.lang.String guessImageType(byte[] input) {
        if (input == null) {
            return null;
        }
        if (input.length >= 3 && input[0] == 71 && input[1] == 73 && input[2] == 70) {
            return "GIF";
        }
        if (input.length >= 4 && input[0] == -119 && input[1] == 80 && input[2] == 78 && input[3] == 71) {
            return "PNG";
        }
        if (input.length >= 2 && input[0] == -1 && input[1] == -40) {
            return "JPEG";
        }
        return null;
    }

    public static boolean areAllEmpty(java.lang.String... values) {
        if (values == null) {
            return true;
        }
        for (java.lang.String value : values) {
            if (!android.text.TextUtils.isEmpty(value)) {
                return false;
            }
        }
        return true;
    }

    public static boolean appearsLikeAndroidVCardQuotedPrintable(java.lang.String value) {
        int remainder = value.length() % 3;
        if (value.length() < 2) {
            return false;
        }
        if (remainder != 1 && remainder != 0) {
            return false;
        }
        for (int i = 0; i < value.length(); i += 3) {
            if (value.charAt(i) != '=') {
                return false;
            }
        }
        return true;
    }

    public static java.lang.String parseQuotedPrintable(java.lang.String value, boolean strictLineBreaking, java.lang.String sourceCharset, java.lang.String targetCharset) {
        java.lang.String[] lines;
        byte[] rawBytes;
        byte[] decodedBytes;
        java.lang.StringBuilder builder = new java.lang.StringBuilder();
        int length = value.length();
        int i = 0;
        while (i < length) {
            char ch = value.charAt(i);
            if (ch == '=' && i < length - 1) {
                char nextCh = value.charAt(i + 1);
                if (nextCh == ' ' || nextCh == 9) {
                    builder.append(nextCh);
                    i++;
                    i++;
                }
            }
            builder.append(ch);
            i++;
        }
        java.lang.String quotedPrintable = builder.toString();
        if (strictLineBreaking) {
            lines = quotedPrintable.split(com.navdy.hud.app.bluetooth.vcard.VCardBuilder.VCARD_END_OF_LINE);
        } else {
            java.lang.StringBuilder builder2 = new java.lang.StringBuilder();
            int length2 = quotedPrintable.length();
            java.util.ArrayList<java.lang.String> list = new java.util.ArrayList<>();
            int i2 = 0;
            while (i2 < length2) {
                char ch2 = quotedPrintable.charAt(i2);
                if (ch2 == 10) {
                    list.add(builder2.toString());
                    builder2 = new java.lang.StringBuilder();
                } else if (ch2 == 13) {
                    list.add(builder2.toString());
                    builder2 = new java.lang.StringBuilder();
                    if (i2 < length2 - 1 && quotedPrintable.charAt(i2 + 1) == 10) {
                        i2++;
                    }
                } else {
                    builder2.append(ch2);
                }
                i2++;
            }
            java.lang.String lastLine = builder2.toString();
            if (lastLine.length() > 0) {
                list.add(lastLine);
            }
            lines = (java.lang.String[]) list.toArray(new java.lang.String[0]);
        }
        java.lang.StringBuilder builder3 = new java.lang.StringBuilder();
        int length3 = lines.length;
        for (int i3 = 0; i3 < length3; i3++) {
            java.lang.String line = lines[i3];
            if (line.endsWith("=")) {
                line = line.substring(0, line.length() - 1);
            }
            builder3.append(line);
        }
        java.lang.String rawString = builder3.toString();
        if (android.text.TextUtils.isEmpty(rawString)) {
            android.util.Log.w(LOG_TAG, "Given raw string is empty.");
        }
        try {
            rawBytes = rawString.getBytes(sourceCharset);
        } catch (java.io.UnsupportedEncodingException e) {
            android.util.Log.w(LOG_TAG, "Failed to decode: " + sourceCharset);
            rawBytes = rawString.getBytes();
        }
        try {
            decodedBytes = com.navdy.hud.app.bluetooth.vcard.VCardUtils.QuotedPrintableCodecPort.decodeQuotedPrintable(rawBytes);
        } catch (com.navdy.hud.app.bluetooth.vcard.VCardUtils.DecoderException e2) {
            android.util.Log.e(LOG_TAG, "DecoderException is thrown.");
            decodedBytes = rawBytes;
        }
        try {
            java.lang.String str = new java.lang.String(decodedBytes, targetCharset);
            return str;
        } catch (java.io.UnsupportedEncodingException e3) {
            android.util.Log.e(LOG_TAG, "Failed to encode: charset=" + targetCharset);
            java.lang.String str2 = new java.lang.String(decodedBytes);
            return str2;
        }
    }

    public static final com.navdy.hud.app.bluetooth.vcard.VCardParser getAppropriateParser(int vcardType) throws com.navdy.hud.app.bluetooth.vcard.exception.VCardException {
        if (com.navdy.hud.app.bluetooth.vcard.VCardConfig.isVersion21(vcardType)) {
            return new com.navdy.hud.app.bluetooth.vcard.VCardParser_V21();
        }
        if (com.navdy.hud.app.bluetooth.vcard.VCardConfig.isVersion30(vcardType)) {
            return new com.navdy.hud.app.bluetooth.vcard.VCardParser_V30();
        }
        if (com.navdy.hud.app.bluetooth.vcard.VCardConfig.isVersion40(vcardType)) {
            return new com.navdy.hud.app.bluetooth.vcard.VCardParser_V40();
        }
        throw new com.navdy.hud.app.bluetooth.vcard.exception.VCardException("Version is not specified");
    }

    public static final java.lang.String convertStringCharset(java.lang.String originalString, java.lang.String sourceCharset, java.lang.String targetCharset) {
        if (sourceCharset.equalsIgnoreCase(targetCharset)) {
            return originalString;
        }
        java.nio.ByteBuffer byteBuffer = java.nio.charset.Charset.forName(sourceCharset).encode(originalString);
        byte[] bytes = new byte[byteBuffer.remaining()];
        byteBuffer.get(bytes);
        try {
            return new java.lang.String(bytes, targetCharset);
        } catch (java.io.UnsupportedEncodingException e) {
            android.util.Log.e(LOG_TAG, "Failed to encode: charset=" + targetCharset);
            return null;
        }
    }

    private VCardUtils() {
    }
}
