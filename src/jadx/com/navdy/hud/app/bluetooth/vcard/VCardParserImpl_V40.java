package com.navdy.hud.app.bluetooth.vcard;

class VCardParserImpl_V40 extends com.navdy.hud.app.bluetooth.vcard.VCardParserImpl_V30 {
    public VCardParserImpl_V40() {
    }

    public VCardParserImpl_V40(int vcardType) {
        super(vcardType);
    }

    /* access modifiers changed from: protected */
    public int getVersion() {
        return 2;
    }

    /* access modifiers changed from: protected */
    public java.lang.String getVersionString() {
        return com.navdy.hud.app.bluetooth.vcard.VCardConstants.VERSION_V40;
    }

    /* access modifiers changed from: protected */
    public java.lang.String maybeUnescapeText(java.lang.String text) {
        return unescapeText(text);
    }

    public static java.lang.String unescapeText(java.lang.String text) {
        java.lang.StringBuilder builder = new java.lang.StringBuilder();
        int length = text.length();
        int i = 0;
        while (i < length) {
            char ch = text.charAt(i);
            if (ch != '\\' || i >= length - 1) {
                builder.append(ch);
            } else {
                i++;
                char next_ch = text.charAt(i);
                if (next_ch == 'n' || next_ch == 'N') {
                    builder.append(com.navdy.hud.app.framework.glance.GlanceConstants.NEWLINE);
                } else {
                    builder.append(next_ch);
                }
            }
            i++;
        }
        return builder.toString();
    }

    public static java.lang.String unescapeCharacter(char ch) {
        if (ch == 'n' || ch == 'N') {
            return com.navdy.hud.app.framework.glance.GlanceConstants.NEWLINE;
        }
        return java.lang.String.valueOf(ch);
    }

    /* access modifiers changed from: protected */
    public java.util.Set<java.lang.String> getKnownPropertyNameSet() {
        return com.navdy.hud.app.bluetooth.vcard.VCardParser_V40.sKnownPropertyNameSet;
    }
}
