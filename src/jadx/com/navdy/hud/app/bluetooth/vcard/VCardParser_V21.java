package com.navdy.hud.app.bluetooth.vcard;

public final class VCardParser_V21 extends com.navdy.hud.app.bluetooth.vcard.VCardParser {
    static final java.util.Set<java.lang.String> sAvailableEncoding = java.util.Collections.unmodifiableSet(new java.util.HashSet(java.util.Arrays.asList(new java.lang.String[]{com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_ENCODING_7BIT, com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_ENCODING_8BIT, com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_ENCODING_QP, com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_ENCODING_BASE64, com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_ENCODING_B})));
    static final java.util.Set<java.lang.String> sKnownPropertyNameSet = java.util.Collections.unmodifiableSet(new java.util.HashSet(java.util.Arrays.asList(new java.lang.String[]{com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_BEGIN, com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_END, com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_LOGO, com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_PHOTO, "LABEL", com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_FN, com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_TITLE, com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_SOUND, com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_VERSION, com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_TEL, com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_EMAIL, "TZ", "GEO", com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_NOTE, "URL", com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_BDAY, com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_ROLE, com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_REV, "UID", com.navdy.hud.app.debug.DebugReceiver.EXTRA_KEY, "MAILER"})));
    static final java.util.Set<java.lang.String> sKnownTypeSet = java.util.Collections.unmodifiableSet(new java.util.HashSet(java.util.Arrays.asList(new java.lang.String[]{com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_ADR_TYPE_DOM, com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_ADR_TYPE_INTL, "POSTAL", com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_ADR_TYPE_PARCEL, com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE_HOME, com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE_WORK, com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE_PREF, com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE_VOICE, com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE_FAX, com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE_MSG, com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE_CELL, com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE_PAGER, com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE_BBS, com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE_MODEM, com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE_CAR, com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE_ISDN, com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE_VIDEO, "AOL", "APPLELINK", "ATTMAIL", "CIS", "EWORLD", com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE_INTERNET, "IBMMAIL", "MCIMAIL", "POWERSHARE", "PRODIGY", com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE_TLX, "X400", "GIF", "CGM", "WMF", "BMP", "MET", "PMB", "DIB", "PICT", "TIFF", "PDF", "PS", "JPEG", "QTIME", "MPEG", "MPEG2", "AVI", "WAVE", "AIFF", "PCM", "X509", "PGP"})));
    static final java.util.Set<java.lang.String> sKnownValueSet = java.util.Collections.unmodifiableSet(new java.util.HashSet(java.util.Arrays.asList(new java.lang.String[]{"INLINE", "URL", "CONTENT-ID", "CID"})));
    private final com.navdy.hud.app.bluetooth.vcard.VCardParserImpl_V21 mVCardParserImpl;

    public VCardParser_V21() {
        this.mVCardParserImpl = new com.navdy.hud.app.bluetooth.vcard.VCardParserImpl_V21();
    }

    public VCardParser_V21(int vcardType) {
        this.mVCardParserImpl = new com.navdy.hud.app.bluetooth.vcard.VCardParserImpl_V21(vcardType);
    }

    public void addInterpreter(com.navdy.hud.app.bluetooth.vcard.VCardInterpreter interpreter) {
        this.mVCardParserImpl.addInterpreter(interpreter);
    }

    public void parse(java.io.InputStream is) throws java.io.IOException, com.navdy.hud.app.bluetooth.vcard.exception.VCardException {
        this.mVCardParserImpl.parse(is);
    }

    public void parseOne(java.io.InputStream is) throws java.io.IOException, com.navdy.hud.app.bluetooth.vcard.exception.VCardException {
        this.mVCardParserImpl.parseOne(is);
    }

    public void cancel() {
        this.mVCardParserImpl.cancel();
    }
}
