package com.navdy.hud.app.bluetooth.vcard.exception;

public class VCardNotSupportedException extends com.navdy.hud.app.bluetooth.vcard.exception.VCardException {
    public VCardNotSupportedException() {
    }

    public VCardNotSupportedException(java.lang.String message) {
        super(message);
    }
}
