package com.navdy.hud.app.bluetooth.vcard;

class VCardParserImpl_V30 extends com.navdy.hud.app.bluetooth.vcard.VCardParserImpl_V21 {
    private static final java.lang.String LOG_TAG = "vCard";
    private boolean mEmittedAgentWarning = false;
    private java.lang.String mPreviousLine;

    public VCardParserImpl_V30() {
    }

    public VCardParserImpl_V30(int vcardType) {
        super(vcardType);
    }

    /* access modifiers changed from: protected */
    public int getVersion() {
        return 1;
    }

    /* access modifiers changed from: protected */
    public java.lang.String getVersionString() {
        return com.navdy.hud.app.bluetooth.vcard.VCardConstants.VERSION_V30;
    }

    /* access modifiers changed from: protected */
    public java.lang.String getLine() throws java.io.IOException {
        if (this.mPreviousLine == null) {
            return this.mReader.readLine();
        }
        java.lang.String ret = this.mPreviousLine;
        this.mPreviousLine = null;
        return ret;
    }

    /* access modifiers changed from: protected */
    public java.lang.String getNonEmptyLine() throws java.io.IOException, com.navdy.hud.app.bluetooth.vcard.exception.VCardException {
        java.lang.String line;
        java.lang.StringBuilder builder = null;
        while (true) {
            line = this.mReader.readLine();
            if (line != null) {
                if (line.length() != 0) {
                    if (line.charAt(0) != ' ' && line.charAt(0) != 9) {
                        if (builder != null || this.mPreviousLine != null) {
                            break;
                        }
                        this.mPreviousLine = line;
                    } else {
                        if (builder == null) {
                            builder = new java.lang.StringBuilder();
                        }
                        if (this.mPreviousLine != null) {
                            builder.append(this.mPreviousLine);
                            this.mPreviousLine = null;
                        }
                        builder.append(line.substring(1));
                    }
                }
            } else {
                break;
            }
        }
        java.lang.String ret = null;
        if (builder != null) {
            ret = builder.toString();
        } else if (this.mPreviousLine != null) {
            ret = this.mPreviousLine;
        }
        this.mPreviousLine = line;
        if (ret != null) {
            return ret;
        }
        throw new com.navdy.hud.app.bluetooth.vcard.exception.VCardException("Reached end of buffer.");
    }

    /* access modifiers changed from: protected */
    public boolean readBeginVCard(boolean allowGarbage) throws java.io.IOException, com.navdy.hud.app.bluetooth.vcard.exception.VCardException {
        return super.readBeginVCard(allowGarbage);
    }

    /* access modifiers changed from: protected */
    public void handleParams(com.navdy.hud.app.bluetooth.vcard.VCardProperty propertyData, java.lang.String params) throws com.navdy.hud.app.bluetooth.vcard.exception.VCardException {
        try {
            super.handleParams(propertyData, params);
        } catch (com.navdy.hud.app.bluetooth.vcard.exception.VCardException e) {
            java.lang.String[] strArray = params.split("=", 2);
            if (strArray.length == 2) {
                handleAnyParam(propertyData, strArray[0], strArray[1]);
                return;
            }
            throw new com.navdy.hud.app.bluetooth.vcard.exception.VCardException("Unknown params value: " + params);
        }
    }

    /* access modifiers changed from: protected */
    public void handleAnyParam(com.navdy.hud.app.bluetooth.vcard.VCardProperty propertyData, java.lang.String paramName, java.lang.String paramValue) {
        splitAndPutParam(propertyData, paramName, paramValue);
    }

    /* access modifiers changed from: protected */
    public void handleParamWithoutName(com.navdy.hud.app.bluetooth.vcard.VCardProperty property, java.lang.String paramValue) {
        handleType(property, paramValue);
    }

    /* access modifiers changed from: protected */
    public void handleType(com.navdy.hud.app.bluetooth.vcard.VCardProperty property, java.lang.String paramValue) {
        splitAndPutParam(property, com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE, paramValue);
    }

    private void splitAndPutParam(com.navdy.hud.app.bluetooth.vcard.VCardProperty property, java.lang.String paramName, java.lang.String paramValue) {
        java.lang.StringBuilder builder = null;
        boolean insideDquote = false;
        int length = paramValue.length();
        for (int i = 0; i < length; i++) {
            char ch = paramValue.charAt(i);
            if (ch == '\"') {
                if (insideDquote) {
                    property.addParameter(paramName, encodeParamValue(builder.toString()));
                    builder = null;
                    insideDquote = false;
                } else {
                    if (builder != null) {
                        if (builder.length() > 0) {
                            android.util.Log.w(LOG_TAG, "Unexpected Dquote inside property.");
                        } else {
                            property.addParameter(paramName, encodeParamValue(builder.toString()));
                        }
                    }
                    insideDquote = true;
                }
            } else if (ch != ',' || insideDquote) {
                if (builder == null) {
                    builder = new java.lang.StringBuilder();
                }
                builder.append(ch);
            } else if (builder == null) {
                android.util.Log.w(LOG_TAG, "Comma is used before actual string comes. (" + paramValue + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.CLOSE_BRACKET);
            } else {
                property.addParameter(paramName, encodeParamValue(builder.toString()));
                builder = null;
            }
        }
        if (insideDquote) {
            android.util.Log.d(LOG_TAG, "Dangling Dquote.");
        }
        if (builder == null) {
            return;
        }
        if (builder.length() == 0) {
            android.util.Log.w(LOG_TAG, "Unintended behavior. We must not see empty StringBuilder at the end of parameter value parsing.");
        } else {
            property.addParameter(paramName, encodeParamValue(builder.toString()));
        }
    }

    /* access modifiers changed from: protected */
    public java.lang.String encodeParamValue(java.lang.String paramValue) {
        return com.navdy.hud.app.bluetooth.vcard.VCardUtils.convertStringCharset(paramValue, com.navdy.hud.app.bluetooth.vcard.VCardConfig.DEFAULT_INTERMEDIATE_CHARSET, "UTF-8");
    }

    /* access modifiers changed from: protected */
    public void handleAgent(com.navdy.hud.app.bluetooth.vcard.VCardProperty property) {
        if (!this.mEmittedAgentWarning) {
            android.util.Log.w(LOG_TAG, "AGENT in vCard 3.0 is not supported yet. Ignore it");
            this.mEmittedAgentWarning = true;
        }
    }

    /* access modifiers changed from: protected */
    public java.lang.String getBase64(java.lang.String firstString) throws java.io.IOException, com.navdy.hud.app.bluetooth.vcard.exception.VCardException {
        return firstString;
    }

    /* access modifiers changed from: protected */
    public java.lang.String maybeUnescapeText(java.lang.String text) {
        return unescapeText(text);
    }

    public static java.lang.String unescapeText(java.lang.String text) {
        java.lang.StringBuilder builder = new java.lang.StringBuilder();
        int length = text.length();
        int i = 0;
        while (i < length) {
            char ch = text.charAt(i);
            if (ch != '\\' || i >= length - 1) {
                builder.append(ch);
            } else {
                i++;
                char next_ch = text.charAt(i);
                if (next_ch == 'n' || next_ch == 'N') {
                    builder.append(com.navdy.hud.app.framework.glance.GlanceConstants.NEWLINE);
                } else {
                    builder.append(next_ch);
                }
            }
            i++;
        }
        return builder.toString();
    }

    /* access modifiers changed from: protected */
    public java.lang.String maybeUnescapeCharacter(char ch) {
        return unescapeCharacter(ch);
    }

    public static java.lang.String unescapeCharacter(char ch) {
        if (ch == 'n' || ch == 'N') {
            return com.navdy.hud.app.framework.glance.GlanceConstants.NEWLINE;
        }
        return java.lang.String.valueOf(ch);
    }

    /* access modifiers changed from: protected */
    public java.util.Set<java.lang.String> getKnownPropertyNameSet() {
        return com.navdy.hud.app.bluetooth.vcard.VCardParser_V30.sKnownPropertyNameSet;
    }
}
