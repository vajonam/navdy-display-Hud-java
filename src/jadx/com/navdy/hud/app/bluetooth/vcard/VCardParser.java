package com.navdy.hud.app.bluetooth.vcard;

public abstract class VCardParser {
    public abstract void addInterpreter(com.navdy.hud.app.bluetooth.vcard.VCardInterpreter vCardInterpreter);

    public abstract void cancel();

    public abstract void parse(java.io.InputStream inputStream) throws java.io.IOException, com.navdy.hud.app.bluetooth.vcard.exception.VCardException;

    public abstract void parseOne(java.io.InputStream inputStream) throws java.io.IOException, com.navdy.hud.app.bluetooth.vcard.exception.VCardException;

    @java.lang.Deprecated
    public void parse(java.io.InputStream is, com.navdy.hud.app.bluetooth.vcard.VCardInterpreter interpreter) throws java.io.IOException, com.navdy.hud.app.bluetooth.vcard.exception.VCardException {
        addInterpreter(interpreter);
        parse(is);
    }
}
