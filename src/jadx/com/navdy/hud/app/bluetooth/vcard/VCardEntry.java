package com.navdy.hud.app.bluetooth.vcard;

public class VCardEntry {
    private static final int DEFAULT_ORGANIZATION_TYPE = 1;
    private static final java.lang.String LOG_TAG = "vCard";
    private static final java.util.List<java.lang.String> sEmptyList = java.util.Collections.unmodifiableList(new java.util.ArrayList(0));
    private static final java.util.Map<java.lang.String, java.lang.Integer> sImMap = new java.util.HashMap();
    private final android.accounts.Account mAccount;
    private java.util.List<com.navdy.hud.app.bluetooth.vcard.VCardEntry.AndroidCustomData> mAndroidCustomDataList;
    private com.navdy.hud.app.bluetooth.vcard.VCardEntry.AnniversaryData mAnniversary;
    private com.navdy.hud.app.bluetooth.vcard.VCardEntry.BirthdayData mBirthday;
    private java.util.Date mCallTime;
    private int mCallType;
    private java.util.List<com.navdy.hud.app.bluetooth.vcard.VCardEntry> mChildren;
    private java.util.List<com.navdy.hud.app.bluetooth.vcard.VCardEntry.EmailData> mEmailList;
    private java.util.List<com.navdy.hud.app.bluetooth.vcard.VCardEntry.ImData> mImList;
    private final com.navdy.hud.app.bluetooth.vcard.VCardEntry.NameData mNameData;
    private java.util.List<com.navdy.hud.app.bluetooth.vcard.VCardEntry.NicknameData> mNicknameList;
    private java.util.List<com.navdy.hud.app.bluetooth.vcard.VCardEntry.NoteData> mNoteList;
    private java.util.List<com.navdy.hud.app.bluetooth.vcard.VCardEntry.OrganizationData> mOrganizationList;
    private java.util.List<com.navdy.hud.app.bluetooth.vcard.VCardEntry.PhoneData> mPhoneList;
    private java.util.List<com.navdy.hud.app.bluetooth.vcard.VCardEntry.PhotoData> mPhotoList;
    private java.util.List<com.navdy.hud.app.bluetooth.vcard.VCardEntry.PostalData> mPostalList;
    private java.util.List<com.navdy.hud.app.bluetooth.vcard.VCardEntry.SipData> mSipList;
    private final int mVCardType;
    private java.util.List<com.navdy.hud.app.bluetooth.vcard.VCardEntry.WebsiteData> mWebsiteList;

    public static class AndroidCustomData implements com.navdy.hud.app.bluetooth.vcard.VCardEntry.EntryElement {
        private final java.util.List<java.lang.String> mDataList;
        private final java.lang.String mMimeType;

        public AndroidCustomData(java.lang.String mimeType, java.util.List<java.lang.String> dataList) {
            this.mMimeType = mimeType;
            this.mDataList = dataList;
        }

        public static com.navdy.hud.app.bluetooth.vcard.VCardEntry.AndroidCustomData constructAndroidCustomData(java.util.List<java.lang.String> list) {
            java.lang.String mimeType;
            java.util.List<java.lang.String> dataList;
            int max = 16;
            if (list == null) {
                mimeType = null;
                dataList = null;
            } else if (list.size() < 2) {
                mimeType = (java.lang.String) list.get(0);
                dataList = null;
            } else {
                if (list.size() < 16) {
                    max = list.size();
                }
                mimeType = (java.lang.String) list.get(0);
                dataList = list.subList(1, max);
            }
            return new com.navdy.hud.app.bluetooth.vcard.VCardEntry.AndroidCustomData(mimeType, dataList);
        }

        public void constructInsertOperation(java.util.List<android.content.ContentProviderOperation> operationList, int backReferenceIndex) {
            android.content.ContentProviderOperation.Builder builder = android.content.ContentProviderOperation.newInsert(android.provider.ContactsContract.Data.CONTENT_URI);
            builder.withValueBackReference("raw_contact_id", backReferenceIndex);
            builder.withValue("mimetype", this.mMimeType);
            for (int i = 0; i < this.mDataList.size(); i++) {
                java.lang.String value = (java.lang.String) this.mDataList.get(i);
                if (!android.text.TextUtils.isEmpty(value)) {
                    builder.withValue("data" + (i + 1), value);
                }
            }
            operationList.add(builder.build());
        }

        public boolean isEmpty() {
            return android.text.TextUtils.isEmpty(this.mMimeType) || this.mDataList == null || this.mDataList.size() == 0;
        }

        public boolean equals(java.lang.Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof com.navdy.hud.app.bluetooth.vcard.VCardEntry.AndroidCustomData)) {
                return false;
            }
            com.navdy.hud.app.bluetooth.vcard.VCardEntry.AndroidCustomData data = (com.navdy.hud.app.bluetooth.vcard.VCardEntry.AndroidCustomData) obj;
            if (!android.text.TextUtils.equals(this.mMimeType, data.mMimeType)) {
                return false;
            }
            if (this.mDataList == null) {
                return data.mDataList == null;
            }
            int size = this.mDataList.size();
            if (size != data.mDataList.size()) {
                return false;
            }
            for (int i = 0; i < size; i++) {
                if (!android.text.TextUtils.equals((java.lang.CharSequence) this.mDataList.get(i), (java.lang.CharSequence) data.mDataList.get(i))) {
                    return false;
                }
            }
            return true;
        }

        public int hashCode() {
            int hash;
            int i;
            if (this.mMimeType != null) {
                hash = this.mMimeType.hashCode();
            } else {
                hash = 0;
            }
            if (this.mDataList != null) {
                for (java.lang.String data : this.mDataList) {
                    int i2 = hash * 31;
                    if (data != null) {
                        i = data.hashCode();
                    } else {
                        i = 0;
                    }
                    hash = i2 + i;
                }
            }
            return hash;
        }

        public java.lang.String toString() {
            java.lang.StringBuilder builder = new java.lang.StringBuilder();
            builder.append("android-custom: " + this.mMimeType + ", data: ");
            builder.append(this.mDataList == null ? com.amazonaws.services.s3.internal.Constants.NULL_VERSION_ID : java.util.Arrays.toString(this.mDataList.toArray()));
            return builder.toString();
        }

        public com.navdy.hud.app.bluetooth.vcard.VCardEntry.EntryLabel getEntryLabel() {
            return com.navdy.hud.app.bluetooth.vcard.VCardEntry.EntryLabel.ANDROID_CUSTOM;
        }

        public java.lang.String getMimeType() {
            return this.mMimeType;
        }

        public java.util.List<java.lang.String> getDataList() {
            return this.mDataList;
        }
    }

    public static class AnniversaryData implements com.navdy.hud.app.bluetooth.vcard.VCardEntry.EntryElement {
        private final java.lang.String mAnniversary;

        public AnniversaryData(java.lang.String anniversary) {
            this.mAnniversary = anniversary;
        }

        public void constructInsertOperation(java.util.List<android.content.ContentProviderOperation> operationList, int backReferenceIndex) {
            android.content.ContentProviderOperation.Builder builder = android.content.ContentProviderOperation.newInsert(android.provider.ContactsContract.Data.CONTENT_URI);
            builder.withValueBackReference("raw_contact_id", backReferenceIndex);
            builder.withValue("mimetype", "vnd.android.cursor.item/contact_event");
            builder.withValue("data1", this.mAnniversary);
            builder.withValue("data2", java.lang.Integer.valueOf(1));
            operationList.add(builder.build());
        }

        public boolean isEmpty() {
            return android.text.TextUtils.isEmpty(this.mAnniversary);
        }

        public boolean equals(java.lang.Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof com.navdy.hud.app.bluetooth.vcard.VCardEntry.AnniversaryData)) {
                return false;
            }
            return android.text.TextUtils.equals(this.mAnniversary, ((com.navdy.hud.app.bluetooth.vcard.VCardEntry.AnniversaryData) obj).mAnniversary);
        }

        public int hashCode() {
            if (this.mAnniversary != null) {
                return this.mAnniversary.hashCode();
            }
            return 0;
        }

        public java.lang.String toString() {
            return "anniversary: " + this.mAnniversary;
        }

        public com.navdy.hud.app.bluetooth.vcard.VCardEntry.EntryLabel getEntryLabel() {
            return com.navdy.hud.app.bluetooth.vcard.VCardEntry.EntryLabel.ANNIVERSARY;
        }

        public java.lang.String getAnniversary() {
            return this.mAnniversary;
        }
    }

    public static class BirthdayData implements com.navdy.hud.app.bluetooth.vcard.VCardEntry.EntryElement {
        /* access modifiers changed from: private */
        public final java.lang.String mBirthday;

        public BirthdayData(java.lang.String birthday) {
            this.mBirthday = birthday;
        }

        public void constructInsertOperation(java.util.List<android.content.ContentProviderOperation> operationList, int backReferenceIndex) {
            android.content.ContentProviderOperation.Builder builder = android.content.ContentProviderOperation.newInsert(android.provider.ContactsContract.Data.CONTENT_URI);
            builder.withValueBackReference("raw_contact_id", backReferenceIndex);
            builder.withValue("mimetype", "vnd.android.cursor.item/contact_event");
            builder.withValue("data1", this.mBirthday);
            builder.withValue("data2", java.lang.Integer.valueOf(3));
            operationList.add(builder.build());
        }

        public boolean isEmpty() {
            return android.text.TextUtils.isEmpty(this.mBirthday);
        }

        public boolean equals(java.lang.Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof com.navdy.hud.app.bluetooth.vcard.VCardEntry.BirthdayData)) {
                return false;
            }
            return android.text.TextUtils.equals(this.mBirthday, ((com.navdy.hud.app.bluetooth.vcard.VCardEntry.BirthdayData) obj).mBirthday);
        }

        public int hashCode() {
            if (this.mBirthday != null) {
                return this.mBirthday.hashCode();
            }
            return 0;
        }

        public java.lang.String toString() {
            return "birthday: " + this.mBirthday;
        }

        public com.navdy.hud.app.bluetooth.vcard.VCardEntry.EntryLabel getEntryLabel() {
            return com.navdy.hud.app.bluetooth.vcard.VCardEntry.EntryLabel.BIRTHDAY;
        }

        public java.lang.String getBirthday() {
            return this.mBirthday;
        }
    }

    public static class EmailData implements com.navdy.hud.app.bluetooth.vcard.VCardEntry.EntryElement {
        /* access modifiers changed from: private */
        public final java.lang.String mAddress;
        private final boolean mIsPrimary;
        private final java.lang.String mLabel;
        private final int mType;

        public EmailData(java.lang.String data, int type, java.lang.String label, boolean isPrimary) {
            this.mType = type;
            this.mAddress = data;
            this.mLabel = label;
            this.mIsPrimary = isPrimary;
        }

        public void constructInsertOperation(java.util.List<android.content.ContentProviderOperation> operationList, int backReferenceIndex) {
            android.content.ContentProviderOperation.Builder builder = android.content.ContentProviderOperation.newInsert(android.provider.ContactsContract.Data.CONTENT_URI);
            builder.withValueBackReference("raw_contact_id", backReferenceIndex);
            builder.withValue("mimetype", "vnd.android.cursor.item/email_v2");
            builder.withValue("data2", java.lang.Integer.valueOf(this.mType));
            if (this.mType == 0) {
                builder.withValue("data3", this.mLabel);
            }
            builder.withValue("data1", this.mAddress);
            if (this.mIsPrimary) {
                builder.withValue("is_primary", java.lang.Integer.valueOf(1));
            }
            operationList.add(builder.build());
        }

        public boolean isEmpty() {
            return android.text.TextUtils.isEmpty(this.mAddress);
        }

        public boolean equals(java.lang.Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof com.navdy.hud.app.bluetooth.vcard.VCardEntry.EmailData)) {
                return false;
            }
            com.navdy.hud.app.bluetooth.vcard.VCardEntry.EmailData emailData = (com.navdy.hud.app.bluetooth.vcard.VCardEntry.EmailData) obj;
            if (this.mType != emailData.mType || !android.text.TextUtils.equals(this.mAddress, emailData.mAddress) || !android.text.TextUtils.equals(this.mLabel, emailData.mLabel) || this.mIsPrimary != emailData.mIsPrimary) {
                return false;
            }
            return true;
        }

        public int hashCode() {
            int i;
            int i2 = 0;
            int i3 = this.mType * 31;
            if (this.mAddress != null) {
                i = this.mAddress.hashCode();
            } else {
                i = 0;
            }
            int i4 = (i3 + i) * 31;
            if (this.mLabel != null) {
                i2 = this.mLabel.hashCode();
            }
            return ((i4 + i2) * 31) + (this.mIsPrimary ? 1231 : 1237);
        }

        public java.lang.String toString() {
            return java.lang.String.format("type: %d, data: %s, label: %s, isPrimary: %s", new java.lang.Object[]{java.lang.Integer.valueOf(this.mType), this.mAddress, this.mLabel, java.lang.Boolean.valueOf(this.mIsPrimary)});
        }

        public final com.navdy.hud.app.bluetooth.vcard.VCardEntry.EntryLabel getEntryLabel() {
            return com.navdy.hud.app.bluetooth.vcard.VCardEntry.EntryLabel.EMAIL;
        }

        public java.lang.String getAddress() {
            return this.mAddress;
        }

        public int getType() {
            return this.mType;
        }

        public java.lang.String getLabel() {
            return this.mLabel;
        }

        public boolean isPrimary() {
            return this.mIsPrimary;
        }
    }

    public interface EntryElement {
        void constructInsertOperation(java.util.List<android.content.ContentProviderOperation> list, int i);

        com.navdy.hud.app.bluetooth.vcard.VCardEntry.EntryLabel getEntryLabel();

        boolean isEmpty();
    }

    public interface EntryElementIterator {
        boolean onElement(com.navdy.hud.app.bluetooth.vcard.VCardEntry.EntryElement entryElement);

        void onElementGroupEnded();

        void onElementGroupStarted(com.navdy.hud.app.bluetooth.vcard.VCardEntry.EntryLabel entryLabel);

        void onIterationEnded();

        void onIterationStarted();
    }

    public enum EntryLabel {
        NAME,
        PHONE,
        EMAIL,
        POSTAL_ADDRESS,
        ORGANIZATION,
        IM,
        PHOTO,
        WEBSITE,
        SIP,
        NICKNAME,
        NOTE,
        BIRTHDAY,
        ANNIVERSARY,
        ANDROID_CUSTOM
    }

    public static class ImData implements com.navdy.hud.app.bluetooth.vcard.VCardEntry.EntryElement {
        private final java.lang.String mAddress;
        private final java.lang.String mCustomProtocol;
        private final boolean mIsPrimary;
        private final int mProtocol;
        private final int mType;

        public ImData(int protocol, java.lang.String customProtocol, java.lang.String address, int type, boolean isPrimary) {
            this.mProtocol = protocol;
            this.mCustomProtocol = customProtocol;
            this.mType = type;
            this.mAddress = address;
            this.mIsPrimary = isPrimary;
        }

        public void constructInsertOperation(java.util.List<android.content.ContentProviderOperation> operationList, int backReferenceIndex) {
            android.content.ContentProviderOperation.Builder builder = android.content.ContentProviderOperation.newInsert(android.provider.ContactsContract.Data.CONTENT_URI);
            builder.withValueBackReference("raw_contact_id", backReferenceIndex);
            builder.withValue("mimetype", "vnd.android.cursor.item/im");
            builder.withValue("data2", java.lang.Integer.valueOf(this.mType));
            builder.withValue("data5", java.lang.Integer.valueOf(this.mProtocol));
            builder.withValue("data1", this.mAddress);
            if (this.mProtocol == -1) {
                builder.withValue("data6", this.mCustomProtocol);
            }
            if (this.mIsPrimary) {
                builder.withValue("is_primary", java.lang.Integer.valueOf(1));
            }
            operationList.add(builder.build());
        }

        public boolean isEmpty() {
            return android.text.TextUtils.isEmpty(this.mAddress);
        }

        public boolean equals(java.lang.Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof com.navdy.hud.app.bluetooth.vcard.VCardEntry.ImData)) {
                return false;
            }
            com.navdy.hud.app.bluetooth.vcard.VCardEntry.ImData imData = (com.navdy.hud.app.bluetooth.vcard.VCardEntry.ImData) obj;
            if (this.mType == imData.mType && this.mProtocol == imData.mProtocol && android.text.TextUtils.equals(this.mCustomProtocol, imData.mCustomProtocol) && android.text.TextUtils.equals(this.mAddress, imData.mAddress) && this.mIsPrimary == imData.mIsPrimary) {
                return true;
            }
            return false;
        }

        public int hashCode() {
            int i;
            int i2 = 0;
            int i3 = ((this.mType * 31) + this.mProtocol) * 31;
            if (this.mCustomProtocol != null) {
                i = this.mCustomProtocol.hashCode();
            } else {
                i = 0;
            }
            int i4 = (i3 + i) * 31;
            if (this.mAddress != null) {
                i2 = this.mAddress.hashCode();
            }
            return ((i4 + i2) * 31) + (this.mIsPrimary ? 1231 : 1237);
        }

        public java.lang.String toString() {
            return java.lang.String.format("type: %d, protocol: %d, custom_protcol: %s, data: %s, isPrimary: %s", new java.lang.Object[]{java.lang.Integer.valueOf(this.mType), java.lang.Integer.valueOf(this.mProtocol), this.mCustomProtocol, this.mAddress, java.lang.Boolean.valueOf(this.mIsPrimary)});
        }

        public final com.navdy.hud.app.bluetooth.vcard.VCardEntry.EntryLabel getEntryLabel() {
            return com.navdy.hud.app.bluetooth.vcard.VCardEntry.EntryLabel.IM;
        }

        public java.lang.String getAddress() {
            return this.mAddress;
        }

        public int getProtocol() {
            return this.mProtocol;
        }

        public java.lang.String getCustomProtocol() {
            return this.mCustomProtocol;
        }

        public int getType() {
            return this.mType;
        }

        public boolean isPrimary() {
            return this.mIsPrimary;
        }
    }

    private class InsertOperationConstrutor implements com.navdy.hud.app.bluetooth.vcard.VCardEntry.EntryElementIterator {
        private final int mBackReferenceIndex;
        private final java.util.List<android.content.ContentProviderOperation> mOperationList;

        public InsertOperationConstrutor(java.util.List<android.content.ContentProviderOperation> operationList, int backReferenceIndex) {
            this.mOperationList = operationList;
            this.mBackReferenceIndex = backReferenceIndex;
        }

        public void onIterationStarted() {
        }

        public void onIterationEnded() {
        }

        public void onElementGroupStarted(com.navdy.hud.app.bluetooth.vcard.VCardEntry.EntryLabel label) {
        }

        public void onElementGroupEnded() {
        }

        public boolean onElement(com.navdy.hud.app.bluetooth.vcard.VCardEntry.EntryElement elem) {
            if (!elem.isEmpty()) {
                elem.constructInsertOperation(this.mOperationList, this.mBackReferenceIndex);
            }
            return true;
        }
    }

    private class IsIgnorableIterator implements com.navdy.hud.app.bluetooth.vcard.VCardEntry.EntryElementIterator {
        private boolean mEmpty;

        private IsIgnorableIterator() {
            this.mEmpty = true;
        }

        public void onIterationStarted() {
        }

        public void onIterationEnded() {
        }

        public void onElementGroupStarted(com.navdy.hud.app.bluetooth.vcard.VCardEntry.EntryLabel label) {
        }

        public void onElementGroupEnded() {
        }

        public boolean onElement(com.navdy.hud.app.bluetooth.vcard.VCardEntry.EntryElement elem) {
            if (elem.isEmpty()) {
                return true;
            }
            this.mEmpty = false;
            return false;
        }

        public boolean getResult() {
            return this.mEmpty;
        }
    }

    public static class NameData implements com.navdy.hud.app.bluetooth.vcard.VCardEntry.EntryElement {
        public java.lang.String displayName;
        /* access modifiers changed from: private */
        public java.lang.String mFamily;
        /* access modifiers changed from: private */
        public java.lang.String mFormatted;
        /* access modifiers changed from: private */
        public java.lang.String mGiven;
        /* access modifiers changed from: private */
        public java.lang.String mMiddle;
        /* access modifiers changed from: private */
        public java.lang.String mPhoneticFamily;
        /* access modifiers changed from: private */
        public java.lang.String mPhoneticGiven;
        /* access modifiers changed from: private */
        public java.lang.String mPhoneticMiddle;
        /* access modifiers changed from: private */
        public java.lang.String mPrefix;
        /* access modifiers changed from: private */
        public java.lang.String mSortString;
        /* access modifiers changed from: private */
        public java.lang.String mSuffix;

        public boolean emptyStructuredName() {
            return android.text.TextUtils.isEmpty(this.mFamily) && android.text.TextUtils.isEmpty(this.mGiven) && android.text.TextUtils.isEmpty(this.mMiddle) && android.text.TextUtils.isEmpty(this.mPrefix) && android.text.TextUtils.isEmpty(this.mSuffix);
        }

        public boolean emptyPhoneticStructuredName() {
            return android.text.TextUtils.isEmpty(this.mPhoneticFamily) && android.text.TextUtils.isEmpty(this.mPhoneticGiven) && android.text.TextUtils.isEmpty(this.mPhoneticMiddle);
        }

        public void constructInsertOperation(java.util.List<android.content.ContentProviderOperation> operationList, int backReferenceIndex) {
            android.content.ContentProviderOperation.Builder builder = android.content.ContentProviderOperation.newInsert(android.provider.ContactsContract.Data.CONTENT_URI);
            builder.withValueBackReference("raw_contact_id", backReferenceIndex);
            builder.withValue("mimetype", "vnd.android.cursor.item/name");
            if (!android.text.TextUtils.isEmpty(this.mGiven)) {
                builder.withValue("data2", this.mGiven);
            }
            if (!android.text.TextUtils.isEmpty(this.mFamily)) {
                builder.withValue("data3", this.mFamily);
            }
            if (!android.text.TextUtils.isEmpty(this.mMiddle)) {
                builder.withValue("data5", this.mMiddle);
            }
            if (!android.text.TextUtils.isEmpty(this.mPrefix)) {
                builder.withValue("data4", this.mPrefix);
            }
            if (!android.text.TextUtils.isEmpty(this.mSuffix)) {
                builder.withValue("data6", this.mSuffix);
            }
            boolean phoneticNameSpecified = false;
            if (!android.text.TextUtils.isEmpty(this.mPhoneticGiven)) {
                builder.withValue("data7", this.mPhoneticGiven);
                phoneticNameSpecified = true;
            }
            if (!android.text.TextUtils.isEmpty(this.mPhoneticFamily)) {
                builder.withValue("data9", this.mPhoneticFamily);
                phoneticNameSpecified = true;
            }
            if (!android.text.TextUtils.isEmpty(this.mPhoneticMiddle)) {
                builder.withValue("data8", this.mPhoneticMiddle);
                phoneticNameSpecified = true;
            }
            if (!phoneticNameSpecified) {
                builder.withValue("data7", this.mSortString);
            }
            builder.withValue("data1", this.displayName);
            operationList.add(builder.build());
        }

        public boolean isEmpty() {
            return android.text.TextUtils.isEmpty(this.mFamily) && android.text.TextUtils.isEmpty(this.mMiddle) && android.text.TextUtils.isEmpty(this.mGiven) && android.text.TextUtils.isEmpty(this.mPrefix) && android.text.TextUtils.isEmpty(this.mSuffix) && android.text.TextUtils.isEmpty(this.mFormatted) && android.text.TextUtils.isEmpty(this.mPhoneticFamily) && android.text.TextUtils.isEmpty(this.mPhoneticMiddle) && android.text.TextUtils.isEmpty(this.mPhoneticGiven) && android.text.TextUtils.isEmpty(this.mSortString);
        }

        public boolean equals(java.lang.Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof com.navdy.hud.app.bluetooth.vcard.VCardEntry.NameData)) {
                return false;
            }
            com.navdy.hud.app.bluetooth.vcard.VCardEntry.NameData nameData = (com.navdy.hud.app.bluetooth.vcard.VCardEntry.NameData) obj;
            if (!android.text.TextUtils.equals(this.mFamily, nameData.mFamily) || !android.text.TextUtils.equals(this.mMiddle, nameData.mMiddle) || !android.text.TextUtils.equals(this.mGiven, nameData.mGiven) || !android.text.TextUtils.equals(this.mPrefix, nameData.mPrefix) || !android.text.TextUtils.equals(this.mSuffix, nameData.mSuffix) || !android.text.TextUtils.equals(this.mFormatted, nameData.mFormatted) || !android.text.TextUtils.equals(this.mPhoneticFamily, nameData.mPhoneticFamily) || !android.text.TextUtils.equals(this.mPhoneticMiddle, nameData.mPhoneticMiddle) || !android.text.TextUtils.equals(this.mPhoneticGiven, nameData.mPhoneticGiven) || !android.text.TextUtils.equals(this.mSortString, nameData.mSortString)) {
                return false;
            }
            return true;
        }

        public int hashCode() {
            java.lang.String[] hashTargets;
            int i;
            int hash = 0;
            for (java.lang.String hashTarget : new java.lang.String[]{this.mFamily, this.mMiddle, this.mGiven, this.mPrefix, this.mSuffix, this.mFormatted, this.mPhoneticFamily, this.mPhoneticMiddle, this.mPhoneticGiven, this.mSortString}) {
                int i2 = hash * 31;
                if (hashTarget != null) {
                    i = hashTarget.hashCode();
                } else {
                    i = 0;
                }
                hash = i2 + i;
            }
            return hash;
        }

        public java.lang.String toString() {
            return java.lang.String.format("family: %s, given: %s, middle: %s, prefix: %s, suffix: %s", new java.lang.Object[]{this.mFamily, this.mGiven, this.mMiddle, this.mPrefix, this.mSuffix});
        }

        public final com.navdy.hud.app.bluetooth.vcard.VCardEntry.EntryLabel getEntryLabel() {
            return com.navdy.hud.app.bluetooth.vcard.VCardEntry.EntryLabel.NAME;
        }

        public java.lang.String getFamily() {
            return this.mFamily;
        }

        public java.lang.String getMiddle() {
            return this.mMiddle;
        }

        public java.lang.String getGiven() {
            return this.mGiven;
        }

        public java.lang.String getPrefix() {
            return this.mPrefix;
        }

        public java.lang.String getSuffix() {
            return this.mSuffix;
        }

        public java.lang.String getFormatted() {
            return this.mFormatted;
        }

        public java.lang.String getSortString() {
            return this.mSortString;
        }

        public void setFamily(java.lang.String family) {
            this.mFamily = family;
        }

        public void setMiddle(java.lang.String middle) {
            this.mMiddle = middle;
        }

        public void setGiven(java.lang.String given) {
            this.mGiven = given;
        }

        public void setPrefix(java.lang.String prefix) {
            this.mPrefix = prefix;
        }

        public void setSuffix(java.lang.String suffix) {
            this.mSuffix = suffix;
        }
    }

    public static class NicknameData implements com.navdy.hud.app.bluetooth.vcard.VCardEntry.EntryElement {
        private final java.lang.String mNickname;

        public NicknameData(java.lang.String nickname) {
            this.mNickname = nickname;
        }

        public void constructInsertOperation(java.util.List<android.content.ContentProviderOperation> operationList, int backReferenceIndex) {
            android.content.ContentProviderOperation.Builder builder = android.content.ContentProviderOperation.newInsert(android.provider.ContactsContract.Data.CONTENT_URI);
            builder.withValueBackReference("raw_contact_id", backReferenceIndex);
            builder.withValue("mimetype", "vnd.android.cursor.item/nickname");
            builder.withValue("data2", java.lang.Integer.valueOf(1));
            builder.withValue("data1", this.mNickname);
            operationList.add(builder.build());
        }

        public boolean isEmpty() {
            return android.text.TextUtils.isEmpty(this.mNickname);
        }

        public boolean equals(java.lang.Object obj) {
            if (!(obj instanceof com.navdy.hud.app.bluetooth.vcard.VCardEntry.NicknameData)) {
                return false;
            }
            return android.text.TextUtils.equals(this.mNickname, ((com.navdy.hud.app.bluetooth.vcard.VCardEntry.NicknameData) obj).mNickname);
        }

        public int hashCode() {
            if (this.mNickname != null) {
                return this.mNickname.hashCode();
            }
            return 0;
        }

        public java.lang.String toString() {
            return "nickname: " + this.mNickname;
        }

        public com.navdy.hud.app.bluetooth.vcard.VCardEntry.EntryLabel getEntryLabel() {
            return com.navdy.hud.app.bluetooth.vcard.VCardEntry.EntryLabel.NICKNAME;
        }

        public java.lang.String getNickname() {
            return this.mNickname;
        }
    }

    public static class NoteData implements com.navdy.hud.app.bluetooth.vcard.VCardEntry.EntryElement {
        public final java.lang.String mNote;

        public NoteData(java.lang.String note) {
            this.mNote = note;
        }

        public void constructInsertOperation(java.util.List<android.content.ContentProviderOperation> operationList, int backReferenceIndex) {
            android.content.ContentProviderOperation.Builder builder = android.content.ContentProviderOperation.newInsert(android.provider.ContactsContract.Data.CONTENT_URI);
            builder.withValueBackReference("raw_contact_id", backReferenceIndex);
            builder.withValue("mimetype", "vnd.android.cursor.item/note");
            builder.withValue("data1", this.mNote);
            operationList.add(builder.build());
        }

        public boolean isEmpty() {
            return android.text.TextUtils.isEmpty(this.mNote);
        }

        public boolean equals(java.lang.Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof com.navdy.hud.app.bluetooth.vcard.VCardEntry.NoteData)) {
                return false;
            }
            return android.text.TextUtils.equals(this.mNote, ((com.navdy.hud.app.bluetooth.vcard.VCardEntry.NoteData) obj).mNote);
        }

        public int hashCode() {
            if (this.mNote != null) {
                return this.mNote.hashCode();
            }
            return 0;
        }

        public java.lang.String toString() {
            return "note: " + this.mNote;
        }

        public com.navdy.hud.app.bluetooth.vcard.VCardEntry.EntryLabel getEntryLabel() {
            return com.navdy.hud.app.bluetooth.vcard.VCardEntry.EntryLabel.NOTE;
        }

        public java.lang.String getNote() {
            return this.mNote;
        }
    }

    public static class OrganizationData implements com.navdy.hud.app.bluetooth.vcard.VCardEntry.EntryElement {
        /* access modifiers changed from: private */
        public java.lang.String mDepartmentName;
        /* access modifiers changed from: private */
        public boolean mIsPrimary;
        /* access modifiers changed from: private */
        public java.lang.String mOrganizationName;
        private final java.lang.String mPhoneticName;
        /* access modifiers changed from: private */
        public java.lang.String mTitle;
        private final int mType;

        public OrganizationData(java.lang.String organizationName, java.lang.String departmentName, java.lang.String titleName, java.lang.String phoneticName, int type, boolean isPrimary) {
            this.mType = type;
            this.mOrganizationName = organizationName;
            this.mDepartmentName = departmentName;
            this.mTitle = titleName;
            this.mPhoneticName = phoneticName;
            this.mIsPrimary = isPrimary;
        }

        public java.lang.String getFormattedString() {
            java.lang.StringBuilder builder = new java.lang.StringBuilder();
            if (!android.text.TextUtils.isEmpty(this.mOrganizationName)) {
                builder.append(this.mOrganizationName);
            }
            if (!android.text.TextUtils.isEmpty(this.mDepartmentName)) {
                if (builder.length() > 0) {
                    builder.append(", ");
                }
                builder.append(this.mDepartmentName);
            }
            if (!android.text.TextUtils.isEmpty(this.mTitle)) {
                if (builder.length() > 0) {
                    builder.append(", ");
                }
                builder.append(this.mTitle);
            }
            return builder.toString();
        }

        public void constructInsertOperation(java.util.List<android.content.ContentProviderOperation> operationList, int backReferenceIndex) {
            android.content.ContentProviderOperation.Builder builder = android.content.ContentProviderOperation.newInsert(android.provider.ContactsContract.Data.CONTENT_URI);
            builder.withValueBackReference("raw_contact_id", backReferenceIndex);
            builder.withValue("mimetype", "vnd.android.cursor.item/organization");
            builder.withValue("data2", java.lang.Integer.valueOf(this.mType));
            if (this.mOrganizationName != null) {
                builder.withValue("data1", this.mOrganizationName);
            }
            if (this.mDepartmentName != null) {
                builder.withValue("data5", this.mDepartmentName);
            }
            if (this.mTitle != null) {
                builder.withValue("data4", this.mTitle);
            }
            if (this.mPhoneticName != null) {
                builder.withValue("data8", this.mPhoneticName);
            }
            if (this.mIsPrimary) {
                builder.withValue("is_primary", java.lang.Integer.valueOf(1));
            }
            operationList.add(builder.build());
        }

        public boolean isEmpty() {
            return android.text.TextUtils.isEmpty(this.mOrganizationName) && android.text.TextUtils.isEmpty(this.mDepartmentName) && android.text.TextUtils.isEmpty(this.mTitle) && android.text.TextUtils.isEmpty(this.mPhoneticName);
        }

        public boolean equals(java.lang.Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof com.navdy.hud.app.bluetooth.vcard.VCardEntry.OrganizationData)) {
                return false;
            }
            com.navdy.hud.app.bluetooth.vcard.VCardEntry.OrganizationData organization = (com.navdy.hud.app.bluetooth.vcard.VCardEntry.OrganizationData) obj;
            if (this.mType != organization.mType || !android.text.TextUtils.equals(this.mOrganizationName, organization.mOrganizationName) || !android.text.TextUtils.equals(this.mDepartmentName, organization.mDepartmentName) || !android.text.TextUtils.equals(this.mTitle, organization.mTitle) || this.mIsPrimary != organization.mIsPrimary) {
                return false;
            }
            return true;
        }

        public int hashCode() {
            int i;
            int i2;
            int i3 = 0;
            int i4 = this.mType * 31;
            if (this.mOrganizationName != null) {
                i = this.mOrganizationName.hashCode();
            } else {
                i = 0;
            }
            int i5 = (i4 + i) * 31;
            if (this.mDepartmentName != null) {
                i2 = this.mDepartmentName.hashCode();
            } else {
                i2 = 0;
            }
            int i6 = (i5 + i2) * 31;
            if (this.mTitle != null) {
                i3 = this.mTitle.hashCode();
            }
            return ((i6 + i3) * 31) + (this.mIsPrimary ? 1231 : 1237);
        }

        public java.lang.String toString() {
            return java.lang.String.format("type: %d, organization: %s, department: %s, title: %s, isPrimary: %s", new java.lang.Object[]{java.lang.Integer.valueOf(this.mType), this.mOrganizationName, this.mDepartmentName, this.mTitle, java.lang.Boolean.valueOf(this.mIsPrimary)});
        }

        public final com.navdy.hud.app.bluetooth.vcard.VCardEntry.EntryLabel getEntryLabel() {
            return com.navdy.hud.app.bluetooth.vcard.VCardEntry.EntryLabel.ORGANIZATION;
        }

        public java.lang.String getOrganizationName() {
            return this.mOrganizationName;
        }

        public java.lang.String getDepartmentName() {
            return this.mDepartmentName;
        }

        public java.lang.String getTitle() {
            return this.mTitle;
        }

        public java.lang.String getPhoneticName() {
            return this.mPhoneticName;
        }

        public int getType() {
            return this.mType;
        }

        public boolean isPrimary() {
            return this.mIsPrimary;
        }
    }

    public static class PhoneData implements com.navdy.hud.app.bluetooth.vcard.VCardEntry.EntryElement {
        private boolean mIsPrimary;
        private final java.lang.String mLabel;
        /* access modifiers changed from: private */
        public final java.lang.String mNumber;
        private final int mType;

        public PhoneData(java.lang.String data, int type, java.lang.String label, boolean isPrimary) {
            this.mNumber = data;
            this.mType = type;
            this.mLabel = label;
            this.mIsPrimary = isPrimary;
        }

        public void constructInsertOperation(java.util.List<android.content.ContentProviderOperation> operationList, int backReferenceIndex) {
            android.content.ContentProviderOperation.Builder builder = android.content.ContentProviderOperation.newInsert(android.provider.ContactsContract.Data.CONTENT_URI);
            builder.withValueBackReference("raw_contact_id", backReferenceIndex);
            builder.withValue("mimetype", "vnd.android.cursor.item/phone_v2");
            builder.withValue("data2", java.lang.Integer.valueOf(this.mType));
            if (this.mType == 0) {
                builder.withValue("data3", this.mLabel);
            }
            builder.withValue("data1", this.mNumber);
            if (this.mIsPrimary) {
                builder.withValue("is_primary", java.lang.Integer.valueOf(1));
            }
            operationList.add(builder.build());
        }

        public boolean isEmpty() {
            return android.text.TextUtils.isEmpty(this.mNumber);
        }

        public boolean equals(java.lang.Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof com.navdy.hud.app.bluetooth.vcard.VCardEntry.PhoneData)) {
                return false;
            }
            com.navdy.hud.app.bluetooth.vcard.VCardEntry.PhoneData phoneData = (com.navdy.hud.app.bluetooth.vcard.VCardEntry.PhoneData) obj;
            if (this.mType != phoneData.mType || !android.text.TextUtils.equals(this.mNumber, phoneData.mNumber) || !android.text.TextUtils.equals(this.mLabel, phoneData.mLabel) || this.mIsPrimary != phoneData.mIsPrimary) {
                return false;
            }
            return true;
        }

        public int hashCode() {
            int i;
            int i2 = 0;
            int i3 = this.mType * 31;
            if (this.mNumber != null) {
                i = this.mNumber.hashCode();
            } else {
                i = 0;
            }
            int i4 = (i3 + i) * 31;
            if (this.mLabel != null) {
                i2 = this.mLabel.hashCode();
            }
            return ((i4 + i2) * 31) + (this.mIsPrimary ? 1231 : 1237);
        }

        public java.lang.String toString() {
            return java.lang.String.format("type: %d, data: %s, label: %s, isPrimary: %s", new java.lang.Object[]{java.lang.Integer.valueOf(this.mType), this.mNumber, this.mLabel, java.lang.Boolean.valueOf(this.mIsPrimary)});
        }

        public final com.navdy.hud.app.bluetooth.vcard.VCardEntry.EntryLabel getEntryLabel() {
            return com.navdy.hud.app.bluetooth.vcard.VCardEntry.EntryLabel.PHONE;
        }

        public java.lang.String getNumber() {
            return this.mNumber;
        }

        public int getType() {
            return this.mType;
        }

        public java.lang.String getLabel() {
            return this.mLabel;
        }

        public boolean isPrimary() {
            return this.mIsPrimary;
        }
    }

    public static class PhotoData implements com.navdy.hud.app.bluetooth.vcard.VCardEntry.EntryElement {
        private final byte[] mBytes;
        private final java.lang.String mFormat;
        private java.lang.Integer mHashCode = null;
        private final boolean mIsPrimary;

        public PhotoData(java.lang.String format, byte[] photoBytes, boolean isPrimary) {
            this.mFormat = format;
            this.mBytes = photoBytes;
            this.mIsPrimary = isPrimary;
        }

        public void constructInsertOperation(java.util.List<android.content.ContentProviderOperation> operationList, int backReferenceIndex) {
            android.content.ContentProviderOperation.Builder builder = android.content.ContentProviderOperation.newInsert(android.provider.ContactsContract.Data.CONTENT_URI);
            builder.withValueBackReference("raw_contact_id", backReferenceIndex);
            builder.withValue("mimetype", "vnd.android.cursor.item/photo");
            builder.withValue("data15", this.mBytes);
            if (this.mIsPrimary) {
                builder.withValue("is_primary", java.lang.Integer.valueOf(1));
            }
            operationList.add(builder.build());
        }

        public boolean isEmpty() {
            return this.mBytes == null || this.mBytes.length == 0;
        }

        public boolean equals(java.lang.Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof com.navdy.hud.app.bluetooth.vcard.VCardEntry.PhotoData)) {
                return false;
            }
            com.navdy.hud.app.bluetooth.vcard.VCardEntry.PhotoData photoData = (com.navdy.hud.app.bluetooth.vcard.VCardEntry.PhotoData) obj;
            if (!android.text.TextUtils.equals(this.mFormat, photoData.mFormat) || !java.util.Arrays.equals(this.mBytes, photoData.mBytes) || this.mIsPrimary != photoData.mIsPrimary) {
                return false;
            }
            return true;
        }

        public int hashCode() {
            int hash;
            if (this.mHashCode != null) {
                return this.mHashCode.intValue();
            }
            if (this.mFormat != null) {
                hash = this.mFormat.hashCode();
            } else {
                hash = 0;
            }
            int hash2 = hash * 31;
            if (this.mBytes != null) {
                for (byte b : this.mBytes) {
                    hash2 += b;
                }
            }
            int hash3 = (hash2 * 31) + (this.mIsPrimary ? 1231 : 1237);
            this.mHashCode = java.lang.Integer.valueOf(hash3);
            return hash3;
        }

        public java.lang.String toString() {
            return java.lang.String.format("format: %s: size: %d, isPrimary: %s", new java.lang.Object[]{this.mFormat, java.lang.Integer.valueOf(this.mBytes.length), java.lang.Boolean.valueOf(this.mIsPrimary)});
        }

        public final com.navdy.hud.app.bluetooth.vcard.VCardEntry.EntryLabel getEntryLabel() {
            return com.navdy.hud.app.bluetooth.vcard.VCardEntry.EntryLabel.PHOTO;
        }

        public java.lang.String getFormat() {
            return this.mFormat;
        }

        public byte[] getBytes() {
            return this.mBytes;
        }

        public boolean isPrimary() {
            return this.mIsPrimary;
        }
    }

    public static class PostalData implements com.navdy.hud.app.bluetooth.vcard.VCardEntry.EntryElement {
        private static final int ADDR_MAX_DATA_SIZE = 7;
        private final java.lang.String mCountry;
        private final java.lang.String mExtendedAddress;
        private boolean mIsPrimary;
        private final java.lang.String mLabel;
        private final java.lang.String mLocalty;
        private final java.lang.String mPobox;
        private final java.lang.String mPostalCode;
        private final java.lang.String mRegion;
        private final java.lang.String mStreet;
        private final int mType;
        private int mVCardType;

        public PostalData(java.lang.String pobox, java.lang.String extendedAddress, java.lang.String street, java.lang.String localty, java.lang.String region, java.lang.String postalCode, java.lang.String country, int type, java.lang.String label, boolean isPrimary, int vcardType) {
            this.mType = type;
            this.mPobox = pobox;
            this.mExtendedAddress = extendedAddress;
            this.mStreet = street;
            this.mLocalty = localty;
            this.mRegion = region;
            this.mPostalCode = postalCode;
            this.mCountry = country;
            this.mLabel = label;
            this.mIsPrimary = isPrimary;
            this.mVCardType = vcardType;
        }

        public static com.navdy.hud.app.bluetooth.vcard.VCardEntry.PostalData constructPostalData(java.util.List<java.lang.String> propValueList, int type, java.lang.String label, boolean isPrimary, int vcardType) {
            java.lang.String[] dataArray = new java.lang.String[7];
            int size = propValueList.size();
            if (size > 7) {
                size = 7;
            }
            int i = 0;
            for (java.lang.String addressElement : propValueList) {
                dataArray[i] = addressElement;
                i++;
                if (i >= size) {
                    break;
                }
            }
            while (true) {
                int i2 = i;
                if (i2 >= 7) {
                    return new com.navdy.hud.app.bluetooth.vcard.VCardEntry.PostalData(dataArray[0], dataArray[1], dataArray[2], dataArray[3], dataArray[4], dataArray[5], dataArray[6], type, label, isPrimary, vcardType);
                }
                i = i2 + 1;
                dataArray[i2] = null;
            }
        }

        public void constructInsertOperation(java.util.List<android.content.ContentProviderOperation> operationList, int backReferenceIndex) {
            java.lang.String streetString;
            android.content.ContentProviderOperation.Builder builder = android.content.ContentProviderOperation.newInsert(android.provider.ContactsContract.Data.CONTENT_URI);
            builder.withValueBackReference("raw_contact_id", backReferenceIndex);
            builder.withValue("mimetype", "vnd.android.cursor.item/postal-address_v2");
            builder.withValue("data2", java.lang.Integer.valueOf(this.mType));
            if (this.mType == 0) {
                builder.withValue("data3", this.mLabel);
            }
            if (android.text.TextUtils.isEmpty(this.mStreet)) {
                if (android.text.TextUtils.isEmpty(this.mExtendedAddress)) {
                    streetString = null;
                } else {
                    streetString = this.mExtendedAddress;
                }
            } else if (android.text.TextUtils.isEmpty(this.mExtendedAddress)) {
                streetString = this.mStreet;
            } else {
                streetString = this.mStreet + " " + this.mExtendedAddress;
            }
            builder.withValue("data5", this.mPobox);
            builder.withValue("data4", streetString);
            builder.withValue("data7", this.mLocalty);
            builder.withValue("data8", this.mRegion);
            builder.withValue("data9", this.mPostalCode);
            builder.withValue("data10", this.mCountry);
            builder.withValue("data1", getFormattedAddress(this.mVCardType));
            if (this.mIsPrimary) {
                builder.withValue("is_primary", java.lang.Integer.valueOf(1));
            }
            operationList.add(builder.build());
        }

        public java.lang.String getFormattedAddress(int vcardType) {
            java.lang.StringBuilder builder = new java.lang.StringBuilder();
            boolean empty = true;
            java.lang.String[] dataArray = {this.mPobox, this.mExtendedAddress, this.mStreet, this.mLocalty, this.mRegion, this.mPostalCode, this.mCountry};
            if (com.navdy.hud.app.bluetooth.vcard.VCardConfig.isJapaneseDevice(vcardType)) {
                for (int i = 6; i >= 0; i--) {
                    java.lang.String addressPart = dataArray[i];
                    if (!android.text.TextUtils.isEmpty(addressPart)) {
                        if (!empty) {
                            builder.append(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.SPACE_CHAR);
                        } else {
                            empty = false;
                        }
                        builder.append(addressPart);
                    }
                }
            } else {
                for (int i2 = 0; i2 < 7; i2++) {
                    java.lang.String addressPart2 = dataArray[i2];
                    if (!android.text.TextUtils.isEmpty(addressPart2)) {
                        if (!empty) {
                            builder.append(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.SPACE_CHAR);
                        } else {
                            empty = false;
                        }
                        builder.append(addressPart2);
                    }
                }
            }
            return builder.toString().trim();
        }

        public boolean isEmpty() {
            return android.text.TextUtils.isEmpty(this.mPobox) && android.text.TextUtils.isEmpty(this.mExtendedAddress) && android.text.TextUtils.isEmpty(this.mStreet) && android.text.TextUtils.isEmpty(this.mLocalty) && android.text.TextUtils.isEmpty(this.mRegion) && android.text.TextUtils.isEmpty(this.mPostalCode) && android.text.TextUtils.isEmpty(this.mCountry);
        }

        public boolean equals(java.lang.Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof com.navdy.hud.app.bluetooth.vcard.VCardEntry.PostalData)) {
                return false;
            }
            com.navdy.hud.app.bluetooth.vcard.VCardEntry.PostalData postalData = (com.navdy.hud.app.bluetooth.vcard.VCardEntry.PostalData) obj;
            if (this.mType != postalData.mType || ((this.mType == 0 && !android.text.TextUtils.equals(this.mLabel, postalData.mLabel)) || this.mIsPrimary != postalData.mIsPrimary || !android.text.TextUtils.equals(this.mPobox, postalData.mPobox) || !android.text.TextUtils.equals(this.mExtendedAddress, postalData.mExtendedAddress) || !android.text.TextUtils.equals(this.mStreet, postalData.mStreet) || !android.text.TextUtils.equals(this.mLocalty, postalData.mLocalty) || !android.text.TextUtils.equals(this.mRegion, postalData.mRegion) || !android.text.TextUtils.equals(this.mPostalCode, postalData.mPostalCode) || !android.text.TextUtils.equals(this.mCountry, postalData.mCountry))) {
                return false;
            }
            return true;
        }

        public int hashCode() {
            int i;
            java.lang.String[] hashTargets;
            int i2;
            int i3 = this.mType * 31;
            if (this.mLabel != null) {
                i = this.mLabel.hashCode();
            } else {
                i = 0;
            }
            int hash = ((i3 + i) * 31) + (this.mIsPrimary ? 1231 : 1237);
            for (java.lang.String hashTarget : new java.lang.String[]{this.mPobox, this.mExtendedAddress, this.mStreet, this.mLocalty, this.mRegion, this.mPostalCode, this.mCountry}) {
                int i4 = hash * 31;
                if (hashTarget != null) {
                    i2 = hashTarget.hashCode();
                } else {
                    i2 = 0;
                }
                hash = i4 + i2;
            }
            return hash;
        }

        public java.lang.String toString() {
            return java.lang.String.format("type: %d, label: %s, isPrimary: %s, pobox: %s, extendedAddress: %s, street: %s, localty: %s, region: %s, postalCode %s, country: %s", new java.lang.Object[]{java.lang.Integer.valueOf(this.mType), this.mLabel, java.lang.Boolean.valueOf(this.mIsPrimary), this.mPobox, this.mExtendedAddress, this.mStreet, this.mLocalty, this.mRegion, this.mPostalCode, this.mCountry});
        }

        public final com.navdy.hud.app.bluetooth.vcard.VCardEntry.EntryLabel getEntryLabel() {
            return com.navdy.hud.app.bluetooth.vcard.VCardEntry.EntryLabel.POSTAL_ADDRESS;
        }

        public java.lang.String getPobox() {
            return this.mPobox;
        }

        public java.lang.String getExtendedAddress() {
            return this.mExtendedAddress;
        }

        public java.lang.String getStreet() {
            return this.mStreet;
        }

        public java.lang.String getLocalty() {
            return this.mLocalty;
        }

        public java.lang.String getRegion() {
            return this.mRegion;
        }

        public java.lang.String getPostalCode() {
            return this.mPostalCode;
        }

        public java.lang.String getCountry() {
            return this.mCountry;
        }

        public int getType() {
            return this.mType;
        }

        public java.lang.String getLabel() {
            return this.mLabel;
        }

        public boolean isPrimary() {
            return this.mIsPrimary;
        }
    }

    public static class SipData implements com.navdy.hud.app.bluetooth.vcard.VCardEntry.EntryElement {
        private final java.lang.String mAddress;
        private final boolean mIsPrimary;
        private final java.lang.String mLabel;
        private final int mType;

        public SipData(java.lang.String rawSip, int type, java.lang.String label, boolean isPrimary) {
            if (rawSip.startsWith("sip:")) {
                this.mAddress = rawSip.substring(4);
            } else {
                this.mAddress = rawSip;
            }
            this.mType = type;
            this.mLabel = label;
            this.mIsPrimary = isPrimary;
        }

        public void constructInsertOperation(java.util.List<android.content.ContentProviderOperation> operationList, int backReferenceIndex) {
            android.content.ContentProviderOperation.Builder builder = android.content.ContentProviderOperation.newInsert(android.provider.ContactsContract.Data.CONTENT_URI);
            builder.withValueBackReference("raw_contact_id", backReferenceIndex);
            builder.withValue("mimetype", "vnd.android.cursor.item/sip_address");
            builder.withValue("data1", this.mAddress);
            builder.withValue("data2", java.lang.Integer.valueOf(this.mType));
            if (this.mType == 0) {
                builder.withValue("data3", this.mLabel);
            }
            if (this.mIsPrimary) {
                builder.withValue("is_primary", java.lang.Boolean.valueOf(this.mIsPrimary));
            }
            operationList.add(builder.build());
        }

        public boolean isEmpty() {
            return android.text.TextUtils.isEmpty(this.mAddress);
        }

        public boolean equals(java.lang.Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof com.navdy.hud.app.bluetooth.vcard.VCardEntry.SipData)) {
                return false;
            }
            com.navdy.hud.app.bluetooth.vcard.VCardEntry.SipData sipData = (com.navdy.hud.app.bluetooth.vcard.VCardEntry.SipData) obj;
            if (this.mType != sipData.mType || !android.text.TextUtils.equals(this.mLabel, sipData.mLabel) || !android.text.TextUtils.equals(this.mAddress, sipData.mAddress) || this.mIsPrimary != sipData.mIsPrimary) {
                return false;
            }
            return true;
        }

        public int hashCode() {
            int i;
            int i2 = 0;
            int i3 = this.mType * 31;
            if (this.mLabel != null) {
                i = this.mLabel.hashCode();
            } else {
                i = 0;
            }
            int i4 = (i3 + i) * 31;
            if (this.mAddress != null) {
                i2 = this.mAddress.hashCode();
            }
            return ((i4 + i2) * 31) + (this.mIsPrimary ? 1231 : 1237);
        }

        public java.lang.String toString() {
            return "sip: " + this.mAddress;
        }

        public com.navdy.hud.app.bluetooth.vcard.VCardEntry.EntryLabel getEntryLabel() {
            return com.navdy.hud.app.bluetooth.vcard.VCardEntry.EntryLabel.SIP;
        }

        public java.lang.String getAddress() {
            return this.mAddress;
        }

        public int getType() {
            return this.mType;
        }

        public java.lang.String getLabel() {
            return this.mLabel;
        }
    }

    private class ToStringIterator implements com.navdy.hud.app.bluetooth.vcard.VCardEntry.EntryElementIterator {
        private java.lang.StringBuilder mBuilder;
        private boolean mFirstElement;

        private ToStringIterator() {
        }

        public void onIterationStarted() {
            this.mBuilder = new java.lang.StringBuilder();
            this.mBuilder.append("[[hash: " + com.navdy.hud.app.bluetooth.vcard.VCardEntry.this.hashCode() + com.navdy.hud.app.framework.glance.GlanceConstants.NEWLINE);
        }

        public void onElementGroupStarted(com.navdy.hud.app.bluetooth.vcard.VCardEntry.EntryLabel label) {
            this.mBuilder.append(label.toString() + ": ");
            this.mFirstElement = true;
        }

        public boolean onElement(com.navdy.hud.app.bluetooth.vcard.VCardEntry.EntryElement elem) {
            if (!this.mFirstElement) {
                this.mBuilder.append(", ");
                this.mFirstElement = false;
            }
            this.mBuilder.append("[").append(elem.toString()).append("]");
            return true;
        }

        public void onElementGroupEnded() {
            this.mBuilder.append(com.navdy.hud.app.framework.glance.GlanceConstants.NEWLINE);
        }

        public void onIterationEnded() {
            this.mBuilder.append("]]\n");
        }

        public java.lang.String toString() {
            return this.mBuilder.toString();
        }
    }

    public static class WebsiteData implements com.navdy.hud.app.bluetooth.vcard.VCardEntry.EntryElement {
        private final java.lang.String mWebsite;

        public WebsiteData(java.lang.String website) {
            this.mWebsite = website;
        }

        public void constructInsertOperation(java.util.List<android.content.ContentProviderOperation> operationList, int backReferenceIndex) {
            android.content.ContentProviderOperation.Builder builder = android.content.ContentProviderOperation.newInsert(android.provider.ContactsContract.Data.CONTENT_URI);
            builder.withValueBackReference("raw_contact_id", backReferenceIndex);
            builder.withValue("mimetype", "vnd.android.cursor.item/website");
            builder.withValue("data1", this.mWebsite);
            builder.withValue("data2", java.lang.Integer.valueOf(1));
            operationList.add(builder.build());
        }

        public boolean isEmpty() {
            return android.text.TextUtils.isEmpty(this.mWebsite);
        }

        public boolean equals(java.lang.Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof com.navdy.hud.app.bluetooth.vcard.VCardEntry.WebsiteData)) {
                return false;
            }
            return android.text.TextUtils.equals(this.mWebsite, ((com.navdy.hud.app.bluetooth.vcard.VCardEntry.WebsiteData) obj).mWebsite);
        }

        public int hashCode() {
            if (this.mWebsite != null) {
                return this.mWebsite.hashCode();
            }
            return 0;
        }

        public java.lang.String toString() {
            return "website: " + this.mWebsite;
        }

        public com.navdy.hud.app.bluetooth.vcard.VCardEntry.EntryLabel getEntryLabel() {
            return com.navdy.hud.app.bluetooth.vcard.VCardEntry.EntryLabel.WEBSITE;
        }

        public java.lang.String getWebsite() {
            return this.mWebsite;
        }
    }

    static {
        sImMap.put(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_X_AIM, java.lang.Integer.valueOf(0));
        sImMap.put(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_X_MSN, java.lang.Integer.valueOf(1));
        sImMap.put(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_X_YAHOO, java.lang.Integer.valueOf(2));
        sImMap.put(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_X_ICQ, java.lang.Integer.valueOf(6));
        sImMap.put(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_X_JABBER, java.lang.Integer.valueOf(7));
        sImMap.put(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_X_SKYPE_USERNAME, java.lang.Integer.valueOf(3));
        sImMap.put(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_X_GOOGLE_TALK, java.lang.Integer.valueOf(5));
        sImMap.put(com.navdy.hud.app.bluetooth.vcard.VCardConstants.ImportOnly.PROPERTY_X_GOOGLE_TALK_WITH_SPACE, java.lang.Integer.valueOf(5));
    }

    public final void iterateAllData(com.navdy.hud.app.bluetooth.vcard.VCardEntry.EntryElementIterator iterator) {
        iterator.onIterationStarted();
        iterator.onElementGroupStarted(this.mNameData.getEntryLabel());
        iterator.onElement(this.mNameData);
        iterator.onElementGroupEnded();
        iterateOneList(this.mPhoneList, iterator);
        iterateOneList(this.mEmailList, iterator);
        iterateOneList(this.mPostalList, iterator);
        iterateOneList(this.mOrganizationList, iterator);
        iterateOneList(this.mImList, iterator);
        iterateOneList(this.mPhotoList, iterator);
        iterateOneList(this.mWebsiteList, iterator);
        iterateOneList(this.mSipList, iterator);
        iterateOneList(this.mNicknameList, iterator);
        iterateOneList(this.mNoteList, iterator);
        iterateOneList(this.mAndroidCustomDataList, iterator);
        if (this.mBirthday != null) {
            iterator.onElementGroupStarted(this.mBirthday.getEntryLabel());
            iterator.onElement(this.mBirthday);
            iterator.onElementGroupEnded();
        }
        if (this.mAnniversary != null) {
            iterator.onElementGroupStarted(this.mAnniversary.getEntryLabel());
            iterator.onElement(this.mAnniversary);
            iterator.onElementGroupEnded();
        }
        iterator.onIterationEnded();
    }

    private void iterateOneList(java.util.List<? extends com.navdy.hud.app.bluetooth.vcard.VCardEntry.EntryElement> elemList, com.navdy.hud.app.bluetooth.vcard.VCardEntry.EntryElementIterator iterator) {
        if (elemList != null && elemList.size() > 0) {
            iterator.onElementGroupStarted(((com.navdy.hud.app.bluetooth.vcard.VCardEntry.EntryElement) elemList.get(0)).getEntryLabel());
            for (com.navdy.hud.app.bluetooth.vcard.VCardEntry.EntryElement elem : elemList) {
                iterator.onElement(elem);
            }
            iterator.onElementGroupEnded();
        }
    }

    public java.lang.String toString() {
        com.navdy.hud.app.bluetooth.vcard.VCardEntry.ToStringIterator iterator = new com.navdy.hud.app.bluetooth.vcard.VCardEntry.ToStringIterator();
        iterateAllData(iterator);
        return iterator.toString();
    }

    public VCardEntry() {
        this(com.navdy.hud.app.bluetooth.vcard.VCardConfig.VCARD_TYPE_V21_GENERIC);
    }

    public VCardEntry(int vcardType) {
        this(vcardType, null);
    }

    public VCardEntry(int vcardType, android.accounts.Account account) {
        this.mNameData = new com.navdy.hud.app.bluetooth.vcard.VCardEntry.NameData();
        this.mVCardType = vcardType;
        this.mAccount = account;
    }

    private void addPhone(int type, java.lang.String data, java.lang.String label, boolean isPrimary) {
        java.lang.String formattedNumber;
        if (this.mPhoneList == null) {
            this.mPhoneList = new java.util.ArrayList();
        }
        java.lang.StringBuilder builder = new java.lang.StringBuilder();
        java.lang.String trimmed = data.trim();
        if (type == 6 || com.navdy.hud.app.bluetooth.vcard.VCardConfig.refrainPhoneNumberFormatting(this.mVCardType)) {
            formattedNumber = trimmed;
        } else {
            boolean hasPauseOrWait = false;
            int length = trimmed.length();
            for (int i = 0; i < length; i++) {
                char ch = trimmed.charAt(i);
                if (ch == 'p' || ch == 'P') {
                    builder.append(',');
                    hasPauseOrWait = true;
                } else if (ch == 'w' || ch == 'W') {
                    builder.append(';');
                    hasPauseOrWait = true;
                } else if (('0' <= ch && ch <= '9') || (i == 0 && ch == '+')) {
                    builder.append(ch);
                }
            }
            if (!hasPauseOrWait) {
                formattedNumber = com.navdy.hud.app.bluetooth.vcard.VCardUtils.PhoneNumberUtilsPort.formatNumber(builder.toString(), com.navdy.hud.app.bluetooth.vcard.VCardUtils.getPhoneNumberFormat(this.mVCardType));
            } else {
                formattedNumber = builder.toString();
            }
        }
        this.mPhoneList.add(new com.navdy.hud.app.bluetooth.vcard.VCardEntry.PhoneData(formattedNumber, type, label, isPrimary));
    }

    private void addCallTime(int callType, java.util.Date date) {
        this.mCallType = callType;
        this.mCallTime = date;
    }

    private void addSip(java.lang.String sipData, int type, java.lang.String label, boolean isPrimary) {
        if (this.mSipList == null) {
            this.mSipList = new java.util.ArrayList();
        }
        this.mSipList.add(new com.navdy.hud.app.bluetooth.vcard.VCardEntry.SipData(sipData, type, label, isPrimary));
    }

    private void addNickName(java.lang.String nickName) {
        if (this.mNicknameList == null) {
            this.mNicknameList = new java.util.ArrayList();
        }
        this.mNicknameList.add(new com.navdy.hud.app.bluetooth.vcard.VCardEntry.NicknameData(nickName));
    }

    private void addEmail(int type, java.lang.String data, java.lang.String label, boolean isPrimary) {
        if (this.mEmailList == null) {
            this.mEmailList = new java.util.ArrayList();
        }
        this.mEmailList.add(new com.navdy.hud.app.bluetooth.vcard.VCardEntry.EmailData(data, type, label, isPrimary));
    }

    private void addPostal(int type, java.util.List<java.lang.String> propValueList, java.lang.String label, boolean isPrimary) {
        if (this.mPostalList == null) {
            this.mPostalList = new java.util.ArrayList(0);
        }
        this.mPostalList.add(com.navdy.hud.app.bluetooth.vcard.VCardEntry.PostalData.constructPostalData(propValueList, type, label, isPrimary, this.mVCardType));
    }

    private void addNewOrganization(java.lang.String organizationName, java.lang.String departmentName, java.lang.String titleName, java.lang.String phoneticName, int type, boolean isPrimary) {
        if (this.mOrganizationList == null) {
            this.mOrganizationList = new java.util.ArrayList();
        }
        this.mOrganizationList.add(new com.navdy.hud.app.bluetooth.vcard.VCardEntry.OrganizationData(organizationName, departmentName, titleName, phoneticName, type, isPrimary));
    }

    private java.lang.String buildSinglePhoneticNameFromSortAsParam(java.util.Map<java.lang.String, java.util.Collection<java.lang.String>> paramMap) {
        java.util.Collection<java.lang.String> sortAsCollection = (java.util.Collection) paramMap.get(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_SORT_AS);
        if (sortAsCollection == null || sortAsCollection.size() == 0) {
            return null;
        }
        if (sortAsCollection.size() > 1) {
            android.util.Log.w(LOG_TAG, "Incorrect multiple SORT_AS parameters detected: " + java.util.Arrays.toString(sortAsCollection.toArray()));
        }
        java.util.List<java.lang.String> sortNames = com.navdy.hud.app.bluetooth.vcard.VCardUtils.constructListFromValue((java.lang.String) sortAsCollection.iterator().next(), this.mVCardType);
        java.lang.StringBuilder builder = new java.lang.StringBuilder();
        for (java.lang.String elem : sortNames) {
            builder.append(elem);
        }
        return builder.toString();
    }

    private void handleOrgValue(int type, java.util.List<java.lang.String> orgList, java.util.Map<java.lang.String, java.util.Collection<java.lang.String>> paramMap, boolean isPrimary) {
        java.lang.String organizationName;
        java.lang.String departmentName;
        java.lang.String phoneticName = buildSinglePhoneticNameFromSortAsParam(paramMap);
        if (orgList == null) {
            orgList = sEmptyList;
        }
        int size = orgList.size();
        switch (size) {
            case 0:
                organizationName = "";
                departmentName = null;
                break;
            case 1:
                organizationName = (java.lang.String) orgList.get(0);
                departmentName = null;
                break;
            default:
                organizationName = (java.lang.String) orgList.get(0);
                java.lang.StringBuilder builder = new java.lang.StringBuilder();
                for (int i = 1; i < size; i++) {
                    if (i > 1) {
                        builder.append(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.SPACE_CHAR);
                    }
                    builder.append((java.lang.String) orgList.get(i));
                }
                departmentName = builder.toString();
                break;
        }
        if (this.mOrganizationList == null) {
            addNewOrganization(organizationName, departmentName, null, phoneticName, type, isPrimary);
            return;
        }
        for (com.navdy.hud.app.bluetooth.vcard.VCardEntry.OrganizationData organizationData : this.mOrganizationList) {
            if (organizationData.mOrganizationName == null && organizationData.mDepartmentName == null) {
                organizationData.mOrganizationName = organizationName;
                organizationData.mDepartmentName = departmentName;
                organizationData.mIsPrimary = isPrimary;
                return;
            }
        }
        addNewOrganization(organizationName, departmentName, null, phoneticName, type, isPrimary);
    }

    private void handleTitleValue(java.lang.String title) {
        if (this.mOrganizationList == null) {
            addNewOrganization(null, null, title, null, 1, false);
            return;
        }
        for (com.navdy.hud.app.bluetooth.vcard.VCardEntry.OrganizationData organizationData : this.mOrganizationList) {
            if (organizationData.mTitle == null) {
                organizationData.mTitle = title;
                return;
            }
        }
        addNewOrganization(null, null, title, null, 1, false);
    }

    private void addIm(int protocol, java.lang.String customProtocol, java.lang.String propValue, int type, boolean isPrimary) {
        if (this.mImList == null) {
            this.mImList = new java.util.ArrayList();
        }
        this.mImList.add(new com.navdy.hud.app.bluetooth.vcard.VCardEntry.ImData(protocol, customProtocol, propValue, type, isPrimary));
    }

    private void addNote(java.lang.String note) {
        if (this.mNoteList == null) {
            this.mNoteList = new java.util.ArrayList(1);
        }
        this.mNoteList.add(new com.navdy.hud.app.bluetooth.vcard.VCardEntry.NoteData(note));
    }

    private void addPhotoBytes(java.lang.String formatName, byte[] photoBytes, boolean isPrimary) {
        if (this.mPhotoList == null) {
            this.mPhotoList = new java.util.ArrayList(1);
        }
        this.mPhotoList.add(new com.navdy.hud.app.bluetooth.vcard.VCardEntry.PhotoData(formatName, photoBytes, isPrimary));
    }

    private void tryHandleSortAsName(java.util.Map<java.lang.String, java.util.Collection<java.lang.String>> paramMap) {
        if (!com.navdy.hud.app.bluetooth.vcard.VCardConfig.isVersion30(this.mVCardType) || (android.text.TextUtils.isEmpty(this.mNameData.mPhoneticFamily) && android.text.TextUtils.isEmpty(this.mNameData.mPhoneticMiddle) && android.text.TextUtils.isEmpty(this.mNameData.mPhoneticGiven))) {
            java.util.Collection<java.lang.String> sortAsCollection = (java.util.Collection) paramMap.get(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_SORT_AS);
            if (sortAsCollection != null && sortAsCollection.size() != 0) {
                if (sortAsCollection.size() > 1) {
                    android.util.Log.w(LOG_TAG, "Incorrect multiple SORT_AS parameters detected: " + java.util.Arrays.toString(sortAsCollection.toArray()));
                }
                java.util.List<java.lang.String> sortNames = com.navdy.hud.app.bluetooth.vcard.VCardUtils.constructListFromValue((java.lang.String) sortAsCollection.iterator().next(), this.mVCardType);
                int size = sortNames.size();
                if (size > 3) {
                    size = 3;
                }
                switch (size) {
                    case 2:
                        break;
                    case 3:
                        this.mNameData.mPhoneticMiddle = (java.lang.String) sortNames.get(2);
                        break;
                }
                this.mNameData.mPhoneticGiven = (java.lang.String) sortNames.get(1);
                this.mNameData.mPhoneticFamily = (java.lang.String) sortNames.get(0);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x002d, code lost:
        com.navdy.hud.app.bluetooth.vcard.VCardEntry.NameData.access$Anon902(r4.mNameData, (java.lang.String) r5.get(3));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0039, code lost:
        com.navdy.hud.app.bluetooth.vcard.VCardEntry.NameData.access$Anon1002(r4.mNameData, (java.lang.String) r5.get(2));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0045, code lost:
        com.navdy.hud.app.bluetooth.vcard.VCardEntry.NameData.access$Anon1102(r4.mNameData, (java.lang.String) r5.get(1));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0014, code lost:
        com.navdy.hud.app.bluetooth.vcard.VCardEntry.NameData.access$Anon1202(r4.mNameData, (java.lang.String) r5.get(0));
     */
    private void handleNProperty(java.util.List<java.lang.String> paramValues, java.util.Map<java.lang.String, java.util.Collection<java.lang.String>> paramMap) {
        tryHandleSortAsName(paramMap);
        if (paramValues != null) {
            int size = paramValues.size();
            if (size >= 1) {
                if (size > 5) {
                    size = 5;
                }
                switch (size) {
                    case 2:
                        break;
                    case 3:
                        break;
                    case 4:
                        break;
                    case 5:
                        this.mNameData.mSuffix = (java.lang.String) paramValues.get(4);
                        break;
                }
            }
        }
    }

    private void handlePhoneticNameFromSound(java.util.List<java.lang.String> elems) {
        if (android.text.TextUtils.isEmpty(this.mNameData.mPhoneticFamily) && android.text.TextUtils.isEmpty(this.mNameData.mPhoneticMiddle) && android.text.TextUtils.isEmpty(this.mNameData.mPhoneticGiven) && elems != null) {
            int size = elems.size();
            if (size >= 1) {
                if (size > 3) {
                    size = 3;
                }
                if (((java.lang.String) elems.get(0)).length() > 0) {
                    boolean onlyFirstElemIsNonEmpty = true;
                    int i = 1;
                    while (true) {
                        if (i >= size) {
                            break;
                        } else if (((java.lang.String) elems.get(i)).length() > 0) {
                            onlyFirstElemIsNonEmpty = false;
                            break;
                        } else {
                            i++;
                        }
                    }
                    if (onlyFirstElemIsNonEmpty) {
                        java.lang.String[] namesArray = ((java.lang.String) elems.get(0)).split(" ");
                        int nameArrayLength = namesArray.length;
                        if (nameArrayLength == 3) {
                            this.mNameData.mPhoneticFamily = namesArray[0];
                            this.mNameData.mPhoneticMiddle = namesArray[1];
                            this.mNameData.mPhoneticGiven = namesArray[2];
                            return;
                        } else if (nameArrayLength == 2) {
                            this.mNameData.mPhoneticFamily = namesArray[0];
                            this.mNameData.mPhoneticGiven = namesArray[1];
                            return;
                        } else {
                            this.mNameData.mPhoneticGiven = (java.lang.String) elems.get(0);
                            return;
                        }
                    }
                }
                switch (size) {
                    case 2:
                        break;
                    case 3:
                        this.mNameData.mPhoneticMiddle = (java.lang.String) elems.get(2);
                        break;
                }
                this.mNameData.mPhoneticGiven = (java.lang.String) elems.get(1);
                this.mNameData.mPhoneticFamily = (java.lang.String) elems.get(0);
            }
        }
    }

    public void addProperty(com.navdy.hud.app.bluetooth.vcard.VCardProperty property) {
        boolean isPrimary;
        int type;
        java.lang.String label;
        boolean isPrimary2;
        java.lang.String propertyName = property.getName();
        java.util.Map<java.lang.String, java.util.Collection<java.lang.String>> paramMap = property.getParameterMap();
        java.util.List<java.lang.String> propertyValueList = property.getValueList();
        byte[] propertyBytes = property.getByteValue();
        if ((propertyValueList != null && propertyValueList.size() != 0) || propertyBytes != null) {
            java.lang.String propValue = propertyValueList != null ? listToString(propertyValueList).trim() : null;
            if (!propertyName.equals(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_VERSION)) {
                if (propertyName.equals(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_FN)) {
                    this.mNameData.mFormatted = propValue;
                    return;
                }
                if (!propertyName.equals(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_NAME)) {
                    if (propertyName.equals(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_N)) {
                        handleNProperty(propertyValueList, paramMap);
                        return;
                    }
                    if (propertyName.equals(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_SORT_STRING)) {
                        this.mNameData.mSortString = propValue;
                        return;
                    }
                    if (!propertyName.equals(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_NICKNAME)) {
                        if (!propertyName.equals(com.navdy.hud.app.bluetooth.vcard.VCardConstants.ImportOnly.PROPERTY_X_NICKNAME)) {
                            if (propertyName.equals(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_SOUND)) {
                                java.util.Collection<java.lang.String> typeCollection = (java.util.Collection) paramMap.get(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE);
                                if (typeCollection != null) {
                                    if (typeCollection.contains("X-IRMC-N")) {
                                        handlePhoneticNameFromSound(com.navdy.hud.app.bluetooth.vcard.VCardUtils.constructListFromValue(propValue, this.mVCardType));
                                        return;
                                    }
                                    return;
                                }
                                return;
                            }
                            if (propertyName.equals(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_ADR)) {
                                boolean valuesAreAllEmpty = true;
                                java.util.Iterator it = propertyValueList.iterator();
                                while (true) {
                                    if (it.hasNext()) {
                                        if (!android.text.TextUtils.isEmpty((java.lang.String) it.next())) {
                                            valuesAreAllEmpty = false;
                                            break;
                                        }
                                    } else {
                                        break;
                                    }
                                }
                                if (!valuesAreAllEmpty) {
                                    int type2 = -1;
                                    java.lang.String label2 = null;
                                    boolean isPrimary3 = false;
                                    java.util.Collection<java.lang.String> typeCollection2 = (java.util.Collection) paramMap.get(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE);
                                    if (typeCollection2 != null) {
                                        for (java.lang.String typeStringOrg : typeCollection2) {
                                            java.lang.String typeStringUpperCase = typeStringOrg.toUpperCase();
                                            if (typeStringUpperCase.equals(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE_PREF)) {
                                                isPrimary3 = true;
                                            } else {
                                                if (typeStringUpperCase.equals(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE_HOME)) {
                                                    type2 = 1;
                                                    label2 = null;
                                                } else {
                                                    if (!typeStringUpperCase.equals(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE_WORK)) {
                                                        if (!typeStringUpperCase.equalsIgnoreCase(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_EXTRA_TYPE_COMPANY)) {
                                                            if (!typeStringUpperCase.equals(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_ADR_TYPE_PARCEL)) {
                                                                if (!typeStringUpperCase.equals(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_ADR_TYPE_DOM)) {
                                                                    if (!typeStringUpperCase.equals(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_ADR_TYPE_INTL) && type2 < 0) {
                                                                        type2 = 0;
                                                                        if (typeStringUpperCase.startsWith("X-")) {
                                                                            label2 = typeStringOrg.substring(2);
                                                                        } else {
                                                                            label2 = typeStringOrg;
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    type2 = 2;
                                                    label2 = null;
                                                }
                                            }
                                        }
                                    }
                                    if (type2 < 0) {
                                        type2 = 1;
                                    }
                                    addPostal(type2, propertyValueList, label2, isPrimary3);
                                    return;
                                }
                                return;
                            }
                            if (propertyName.equals(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_EMAIL)) {
                                int type3 = -1;
                                java.lang.String label3 = null;
                                boolean isPrimary4 = false;
                                java.util.Collection<java.lang.String> typeCollection3 = (java.util.Collection) paramMap.get(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE);
                                if (typeCollection3 != null) {
                                    for (java.lang.String typeStringOrg2 : typeCollection3) {
                                        java.lang.String typeStringUpperCase2 = typeStringOrg2.toUpperCase();
                                        if (typeStringUpperCase2.equals(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE_PREF)) {
                                            isPrimary4 = true;
                                        } else {
                                            if (typeStringUpperCase2.equals(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE_HOME)) {
                                                type3 = 1;
                                            } else {
                                                if (typeStringUpperCase2.equals(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE_WORK)) {
                                                    type3 = 2;
                                                } else {
                                                    if (typeStringUpperCase2.equals(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE_CELL)) {
                                                        type3 = 4;
                                                    } else if (type3 < 0) {
                                                        if (typeStringUpperCase2.startsWith("X-")) {
                                                            label3 = typeStringOrg2.substring(2);
                                                        } else {
                                                            label3 = typeStringOrg2;
                                                        }
                                                        type3 = 0;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                if (type3 < 0) {
                                    type3 = 3;
                                }
                                addEmail(type3, propValue, label3, isPrimary4);
                                return;
                            }
                            if (propertyName.equals(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_ORG)) {
                                boolean isPrimary5 = false;
                                java.util.Collection<java.lang.String> typeCollection4 = (java.util.Collection) paramMap.get(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE);
                                if (typeCollection4 != null) {
                                    for (java.lang.String equals : typeCollection4) {
                                        if (equals.equals(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE_PREF)) {
                                            isPrimary5 = true;
                                        }
                                    }
                                }
                                handleOrgValue(1, propertyValueList, paramMap, isPrimary5);
                                return;
                            }
                            if (propertyName.equals(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_TITLE)) {
                                handleTitleValue(propValue);
                                return;
                            }
                            if (!propertyName.equals(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_ROLE)) {
                                if (!propertyName.equals(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_PHOTO)) {
                                    if (!propertyName.equals(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_LOGO)) {
                                        if (propertyName.equals(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_TEL)) {
                                            java.lang.String phoneNumber = null;
                                            boolean isSip = false;
                                            if (!com.navdy.hud.app.bluetooth.vcard.VCardConfig.isVersion40(this.mVCardType)) {
                                                phoneNumber = propValue;
                                            } else if (propValue.startsWith("sip:")) {
                                                isSip = true;
                                            } else if (propValue.startsWith("tel:")) {
                                                phoneNumber = propValue.substring(4);
                                            } else {
                                                phoneNumber = propValue;
                                            }
                                            if (isSip) {
                                                handleSipCase(propValue, (java.util.Collection) paramMap.get(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE));
                                                return;
                                            } else if (propValue.length() != 0) {
                                                java.util.Collection<java.lang.String> typeCollection5 = (java.util.Collection) paramMap.get(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE);
                                                java.lang.Object typeObject = com.navdy.hud.app.bluetooth.vcard.VCardUtils.getPhoneTypeFromStrings(typeCollection5, phoneNumber);
                                                if (typeObject instanceof java.lang.Integer) {
                                                    type = ((java.lang.Integer) typeObject).intValue();
                                                    label = null;
                                                } else {
                                                    type = 0;
                                                    label = typeObject.toString();
                                                }
                                                if (typeCollection5 != null) {
                                                    if (typeCollection5.contains(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE_PREF)) {
                                                        isPrimary2 = true;
                                                        addPhone(type, phoneNumber, label, isPrimary2);
                                                        return;
                                                    }
                                                }
                                                isPrimary2 = false;
                                                addPhone(type, phoneNumber, label, isPrimary2);
                                                return;
                                            } else {
                                                return;
                                            }
                                        } else {
                                            if (propertyName.equals(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_DATETIME)) {
                                                java.lang.String dateTimeStr = propValue;
                                                if (!android.text.TextUtils.isEmpty(dateTimeStr)) {
                                                    java.util.Collection<java.lang.String> typeCollection6 = (java.util.Collection) paramMap.get(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE);
                                                    int callType = 0;
                                                    if (typeCollection6 == null || typeCollection6.size() > 0) {
                                                        java.lang.Object o = typeCollection6.iterator().next();
                                                        if (o instanceof java.lang.String) {
                                                            java.lang.String str = (java.lang.String) o;
                                                            if (com.navdy.hud.app.bluetooth.vcard.VCardConstants.DIALED.equalsIgnoreCase(str)) {
                                                                callType = 11;
                                                            } else if (com.navdy.hud.app.bluetooth.vcard.VCardConstants.MISSED.equalsIgnoreCase(str)) {
                                                                callType = 12;
                                                            } else if (com.navdy.hud.app.bluetooth.vcard.VCardConstants.RECEIVED.equalsIgnoreCase(str)) {
                                                                callType = 10;
                                                            }
                                                        }
                                                    }
                                                    java.util.Date date = com.navdy.hud.app.util.DateUtil.parseIrmcDateStr(dateTimeStr);
                                                    if (date != null) {
                                                        addCallTime(callType, date);
                                                        return;
                                                    }
                                                    return;
                                                }
                                                return;
                                            }
                                            if (propertyName.equals(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_X_SKYPE_PSTNNUMBER)) {
                                                java.util.Collection<java.lang.String> typeCollection7 = (java.util.Collection) paramMap.get(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE);
                                                if (typeCollection7 != null) {
                                                    if (typeCollection7.contains(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE_PREF)) {
                                                        isPrimary = true;
                                                        addPhone(7, propValue, null, isPrimary);
                                                        return;
                                                    }
                                                }
                                                isPrimary = false;
                                                addPhone(7, propValue, null, isPrimary);
                                                return;
                                            } else if (sImMap.containsKey(propertyName)) {
                                                int protocol = ((java.lang.Integer) sImMap.get(propertyName)).intValue();
                                                boolean isPrimary6 = false;
                                                int type4 = -1;
                                                java.util.Collection<java.lang.String> typeCollection8 = (java.util.Collection) paramMap.get(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE);
                                                if (typeCollection8 != null) {
                                                    for (java.lang.String typeString : typeCollection8) {
                                                        if (typeString.equals(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE_PREF)) {
                                                            isPrimary6 = true;
                                                        } else if (type4 < 0) {
                                                            if (typeString.equalsIgnoreCase(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE_HOME)) {
                                                                type4 = 1;
                                                            } else {
                                                                if (typeString.equalsIgnoreCase(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE_WORK)) {
                                                                    type4 = 2;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                if (type4 < 0) {
                                                    type4 = 1;
                                                }
                                                addIm(protocol, null, propValue, type4, isPrimary6);
                                                return;
                                            } else {
                                                if (propertyName.equals(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_NOTE)) {
                                                    addNote(propValue);
                                                    return;
                                                }
                                                if (propertyName.equals("URL")) {
                                                    if (this.mWebsiteList == null) {
                                                        this.mWebsiteList = new java.util.ArrayList(1);
                                                    }
                                                    this.mWebsiteList.add(new com.navdy.hud.app.bluetooth.vcard.VCardEntry.WebsiteData(propValue));
                                                    return;
                                                }
                                                if (propertyName.equals(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_BDAY)) {
                                                    this.mBirthday = new com.navdy.hud.app.bluetooth.vcard.VCardEntry.BirthdayData(propValue);
                                                    return;
                                                }
                                                if (propertyName.equals(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_ANNIVERSARY)) {
                                                    this.mAnniversary = new com.navdy.hud.app.bluetooth.vcard.VCardEntry.AnniversaryData(propValue);
                                                    return;
                                                }
                                                if (propertyName.equals(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_X_PHONETIC_FIRST_NAME)) {
                                                    this.mNameData.mPhoneticGiven = propValue;
                                                    return;
                                                }
                                                if (propertyName.equals(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_X_PHONETIC_MIDDLE_NAME)) {
                                                    this.mNameData.mPhoneticMiddle = propValue;
                                                    return;
                                                }
                                                if (propertyName.equals(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_X_PHONETIC_LAST_NAME)) {
                                                    this.mNameData.mPhoneticFamily = propValue;
                                                    return;
                                                }
                                                if (!propertyName.equals(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_IMPP)) {
                                                    if (!propertyName.equals(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_X_SIP)) {
                                                        if (propertyName.equals(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PROPERTY_X_ANDROID_CUSTOM)) {
                                                            handleAndroidCustomProperty(com.navdy.hud.app.bluetooth.vcard.VCardUtils.constructListFromValue(propValue, this.mVCardType));
                                                            return;
                                                        }
                                                        return;
                                                    } else if (!android.text.TextUtils.isEmpty(propValue)) {
                                                        handleSipCase(propValue, (java.util.Collection) paramMap.get(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE));
                                                        return;
                                                    } else {
                                                        return;
                                                    }
                                                } else if (propValue.startsWith("sip:")) {
                                                    handleSipCase(propValue, (java.util.Collection) paramMap.get(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE));
                                                    return;
                                                } else {
                                                    return;
                                                }
                                            }
                                        }
                                    }
                                }
                                java.util.Collection<java.lang.String> paramMapValue = (java.util.Collection) paramMap.get(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_VALUE);
                                if (paramMapValue != null) {
                                    if (paramMapValue.contains("URL")) {
                                        return;
                                    }
                                }
                                java.util.Collection<java.lang.String> typeCollection9 = (java.util.Collection) paramMap.get(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE);
                                java.lang.String formatName = null;
                                boolean isPrimary7 = false;
                                if (typeCollection9 != null) {
                                    for (java.lang.String typeValue : typeCollection9) {
                                        if (com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE_PREF.equals(typeValue)) {
                                            isPrimary7 = true;
                                        } else if (formatName == null) {
                                            formatName = typeValue;
                                        }
                                    }
                                }
                                addPhotoBytes(formatName, propertyBytes, isPrimary7);
                                return;
                            }
                            return;
                        }
                    }
                    addNickName(propValue);
                } else if (android.text.TextUtils.isEmpty(this.mNameData.mFormatted)) {
                    this.mNameData.mFormatted = propValue;
                }
            }
        }
    }

    private void handleSipCase(java.lang.String propValue, java.util.Collection<java.lang.String> typeCollection) {
        if (!android.text.TextUtils.isEmpty(propValue)) {
            if (propValue.startsWith("sip:")) {
                propValue = propValue.substring(4);
                if (propValue.length() == 0) {
                    return;
                }
            }
            int type = -1;
            java.lang.String label = null;
            boolean isPrimary = false;
            if (typeCollection != null) {
                for (java.lang.String typeStringOrg : typeCollection) {
                    java.lang.String typeStringUpperCase = typeStringOrg.toUpperCase();
                    if (typeStringUpperCase.equals(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE_PREF)) {
                        isPrimary = true;
                    } else if (typeStringUpperCase.equals(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE_HOME)) {
                        type = 1;
                    } else if (typeStringUpperCase.equals(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE_WORK)) {
                        type = 2;
                    } else if (type < 0) {
                        if (typeStringUpperCase.startsWith("X-")) {
                            label = typeStringOrg.substring(2);
                        } else {
                            label = typeStringOrg;
                        }
                        type = 0;
                    }
                }
            }
            if (type < 0) {
                type = 3;
            }
            addSip(propValue, type, label, isPrimary);
        }
    }

    public void addChild(com.navdy.hud.app.bluetooth.vcard.VCardEntry child) {
        if (this.mChildren == null) {
            this.mChildren = new java.util.ArrayList();
        }
        this.mChildren.add(child);
    }

    private void handleAndroidCustomProperty(java.util.List<java.lang.String> customPropertyList) {
        if (this.mAndroidCustomDataList == null) {
            this.mAndroidCustomDataList = new java.util.ArrayList();
        }
        this.mAndroidCustomDataList.add(com.navdy.hud.app.bluetooth.vcard.VCardEntry.AndroidCustomData.constructAndroidCustomData(customPropertyList));
    }

    private java.lang.String constructDisplayName() {
        java.lang.String displayName = null;
        if (!android.text.TextUtils.isEmpty(this.mNameData.mFormatted)) {
            displayName = this.mNameData.mFormatted;
        } else if (!this.mNameData.emptyStructuredName()) {
            displayName = com.navdy.hud.app.bluetooth.vcard.VCardUtils.constructNameFromElements(this.mVCardType, this.mNameData.mFamily, this.mNameData.mMiddle, this.mNameData.mGiven, this.mNameData.mPrefix, this.mNameData.mSuffix);
        } else if (!this.mNameData.emptyPhoneticStructuredName()) {
            displayName = com.navdy.hud.app.bluetooth.vcard.VCardUtils.constructNameFromElements(this.mVCardType, this.mNameData.mPhoneticFamily, this.mNameData.mPhoneticMiddle, this.mNameData.mPhoneticGiven);
        } else if (this.mEmailList != null && this.mEmailList.size() > 0) {
            displayName = ((com.navdy.hud.app.bluetooth.vcard.VCardEntry.EmailData) this.mEmailList.get(0)).mAddress;
        } else if (this.mPhoneList != null && this.mPhoneList.size() > 0) {
            displayName = ((com.navdy.hud.app.bluetooth.vcard.VCardEntry.PhoneData) this.mPhoneList.get(0)).mNumber;
        } else if (this.mPostalList != null && this.mPostalList.size() > 0) {
            displayName = ((com.navdy.hud.app.bluetooth.vcard.VCardEntry.PostalData) this.mPostalList.get(0)).getFormattedAddress(this.mVCardType);
        } else if (this.mOrganizationList != null && this.mOrganizationList.size() > 0) {
            displayName = ((com.navdy.hud.app.bluetooth.vcard.VCardEntry.OrganizationData) this.mOrganizationList.get(0)).getFormattedString();
        }
        if (displayName == null) {
            return "";
        }
        return displayName;
    }

    public void consolidateFields() {
        this.mNameData.displayName = constructDisplayName();
    }

    public boolean isIgnorable() {
        com.navdy.hud.app.bluetooth.vcard.VCardEntry.IsIgnorableIterator iterator = new com.navdy.hud.app.bluetooth.vcard.VCardEntry.IsIgnorableIterator();
        iterateAllData(iterator);
        return iterator.getResult();
    }

    public java.util.ArrayList<android.content.ContentProviderOperation> constructInsertOperations(android.content.ContentResolver resolver, java.util.ArrayList<android.content.ContentProviderOperation> operationList) {
        if (operationList == null) {
            operationList = new java.util.ArrayList<>();
        }
        if (!isIgnorable()) {
            int backReferenceIndex = operationList.size();
            android.content.ContentProviderOperation.Builder builder = android.content.ContentProviderOperation.newInsert(android.provider.ContactsContract.RawContacts.CONTENT_URI);
            if (this.mAccount != null) {
                builder.withValue("account_name", this.mAccount.name);
                builder.withValue("account_type", this.mAccount.type);
            } else {
                builder.withValue("account_name", null);
                builder.withValue("account_type", null);
            }
            operationList.add(builder.build());
            int size = operationList.size();
            iterateAllData(new com.navdy.hud.app.bluetooth.vcard.VCardEntry.InsertOperationConstrutor(operationList, backReferenceIndex));
            int size2 = operationList.size();
        }
        return operationList;
    }

    public static com.navdy.hud.app.bluetooth.vcard.VCardEntry buildFromResolver(android.content.ContentResolver resolver) {
        return buildFromResolver(resolver, android.provider.ContactsContract.Contacts.CONTENT_URI);
    }

    public static com.navdy.hud.app.bluetooth.vcard.VCardEntry buildFromResolver(android.content.ContentResolver resolver, android.net.Uri uri) {
        return null;
    }

    private java.lang.String listToString(java.util.List<java.lang.String> list) {
        int size = list.size();
        if (size > 1) {
            java.lang.StringBuilder builder = new java.lang.StringBuilder();
            for (java.lang.String type : list) {
                builder.append(type);
                if (0 < size - 1) {
                    builder.append(";");
                }
            }
            return builder.toString();
        } else if (size == 1) {
            return (java.lang.String) list.get(0);
        } else {
            return "";
        }
    }

    public final com.navdy.hud.app.bluetooth.vcard.VCardEntry.NameData getNameData() {
        return this.mNameData;
    }

    public final java.util.List<com.navdy.hud.app.bluetooth.vcard.VCardEntry.NicknameData> getNickNameList() {
        return this.mNicknameList;
    }

    public final java.lang.String getBirthday() {
        if (this.mBirthday != null) {
            return this.mBirthday.mBirthday;
        }
        return null;
    }

    public final java.util.List<com.navdy.hud.app.bluetooth.vcard.VCardEntry.NoteData> getNotes() {
        return this.mNoteList;
    }

    public final java.util.List<com.navdy.hud.app.bluetooth.vcard.VCardEntry.PhoneData> getPhoneList() {
        return this.mPhoneList;
    }

    public final java.util.List<com.navdy.hud.app.bluetooth.vcard.VCardEntry.EmailData> getEmailList() {
        return this.mEmailList;
    }

    public final java.util.List<com.navdy.hud.app.bluetooth.vcard.VCardEntry.PostalData> getPostalList() {
        return this.mPostalList;
    }

    public final java.util.List<com.navdy.hud.app.bluetooth.vcard.VCardEntry.OrganizationData> getOrganizationList() {
        return this.mOrganizationList;
    }

    public final java.util.List<com.navdy.hud.app.bluetooth.vcard.VCardEntry.ImData> getImList() {
        return this.mImList;
    }

    public final java.util.List<com.navdy.hud.app.bluetooth.vcard.VCardEntry.PhotoData> getPhotoList() {
        return this.mPhotoList;
    }

    public final java.util.List<com.navdy.hud.app.bluetooth.vcard.VCardEntry.WebsiteData> getWebsiteList() {
        return this.mWebsiteList;
    }

    public final java.util.List<com.navdy.hud.app.bluetooth.vcard.VCardEntry> getChildlen() {
        return this.mChildren;
    }

    public java.lang.String getDisplayName() {
        if (this.mNameData.displayName == null) {
            this.mNameData.displayName = constructDisplayName();
        }
        return this.mNameData.displayName;
    }

    public int getCallType() {
        return this.mCallType;
    }

    public java.util.Date getCallTime() {
        return this.mCallTime;
    }
}
