package com.navdy.hud.app.bluetooth.vcard;

public class VCardProperty {
    private static final java.lang.String LOG_TAG = "vCard";
    private byte[] mByteValue;
    private java.util.List<java.lang.String> mGroupList;
    private java.lang.String mName;
    private java.util.Map<java.lang.String, java.util.Collection<java.lang.String>> mParameterMap = new java.util.HashMap();
    private java.lang.String mRawValue;
    private java.util.List<java.lang.String> mValueList;

    public void setName(java.lang.String name) {
        if (this.mName != null) {
            android.util.Log.w(LOG_TAG, java.lang.String.format("Property name is re-defined (existing: %s, requested: %s", new java.lang.Object[]{this.mName, name}));
        }
        this.mName = name;
    }

    public void addGroup(java.lang.String group) {
        if (this.mGroupList == null) {
            this.mGroupList = new java.util.ArrayList();
        }
        this.mGroupList.add(group);
    }

    public void setParameter(java.lang.String paramName, java.lang.String paramValue) {
        this.mParameterMap.clear();
        addParameter(paramName, paramValue);
    }

    public void addParameter(java.lang.String paramName, java.lang.String paramValue) {
        java.util.Collection<java.lang.String> values;
        if (!this.mParameterMap.containsKey(paramName)) {
            if (paramName.equals(com.navdy.hud.app.bluetooth.vcard.VCardConstants.PARAM_TYPE)) {
                values = new java.util.HashSet<>();
            } else {
                values = new java.util.ArrayList<>();
            }
            this.mParameterMap.put(paramName, values);
        } else {
            values = (java.util.Collection) this.mParameterMap.get(paramName);
        }
        values.add(paramValue);
    }

    public void setRawValue(java.lang.String rawValue) {
        this.mRawValue = rawValue;
    }

    public void setValues(java.lang.String... propertyValues) {
        this.mValueList = java.util.Arrays.asList(propertyValues);
    }

    public void setValues(java.util.List<java.lang.String> propertyValueList) {
        this.mValueList = propertyValueList;
    }

    public void addValues(java.lang.String... propertyValues) {
        if (this.mValueList == null) {
            this.mValueList = java.util.Arrays.asList(propertyValues);
        } else {
            this.mValueList.addAll(java.util.Arrays.asList(propertyValues));
        }
    }

    public void addValues(java.util.List<java.lang.String> propertyValueList) {
        if (this.mValueList == null) {
            this.mValueList = new java.util.ArrayList(propertyValueList);
        } else {
            this.mValueList.addAll(propertyValueList);
        }
    }

    public void setByteValue(byte[] byteValue) {
        this.mByteValue = byteValue;
    }

    public java.lang.String getName() {
        return this.mName;
    }

    public java.util.List<java.lang.String> getGroupList() {
        return this.mGroupList;
    }

    public java.util.Map<java.lang.String, java.util.Collection<java.lang.String>> getParameterMap() {
        return this.mParameterMap;
    }

    public java.util.Collection<java.lang.String> getParameters(java.lang.String type) {
        return (java.util.Collection) this.mParameterMap.get(type);
    }

    public java.lang.String getRawValue() {
        return this.mRawValue;
    }

    public java.util.List<java.lang.String> getValueList() {
        return this.mValueList;
    }

    public byte[] getByteValue() {
        return this.mByteValue;
    }
}
