package com.navdy.hud.app.bluetooth.obex;

public final class ObexHelper {
    public static final int BASE_PACKET_LENGTH = 3;
    public static final int LOWER_LIMIT_MAX_PACKET_SIZE = 255;
    public static final int MAX_CLIENT_PACKET_SIZE = 64512;
    public static final int MAX_PACKET_SIZE_INT = 65534;
    public static final int OBEX_AUTH_REALM_CHARSET_ASCII = 0;
    public static final int OBEX_AUTH_REALM_CHARSET_ISO_8859_1 = 1;
    public static final int OBEX_AUTH_REALM_CHARSET_ISO_8859_2 = 2;
    public static final int OBEX_AUTH_REALM_CHARSET_ISO_8859_3 = 3;
    public static final int OBEX_AUTH_REALM_CHARSET_ISO_8859_4 = 4;
    public static final int OBEX_AUTH_REALM_CHARSET_ISO_8859_5 = 5;
    public static final int OBEX_AUTH_REALM_CHARSET_ISO_8859_6 = 6;
    public static final int OBEX_AUTH_REALM_CHARSET_ISO_8859_7 = 7;
    public static final int OBEX_AUTH_REALM_CHARSET_ISO_8859_8 = 8;
    public static final int OBEX_AUTH_REALM_CHARSET_ISO_8859_9 = 9;
    public static final int OBEX_AUTH_REALM_CHARSET_UNICODE = 255;
    public static final int OBEX_OPCODE_ABORT = 255;
    public static final int OBEX_OPCODE_CONNECT = 128;
    public static final int OBEX_OPCODE_DISCONNECT = 129;
    public static final int OBEX_OPCODE_FINAL_BIT_MASK = 128;
    public static final int OBEX_OPCODE_GET = 3;
    public static final int OBEX_OPCODE_GET_FINAL = 131;
    public static final int OBEX_OPCODE_PUT = 2;
    public static final int OBEX_OPCODE_PUT_FINAL = 130;
    public static final int OBEX_OPCODE_RESERVED = 4;
    public static final int OBEX_OPCODE_RESERVED_FINAL = 132;
    public static final int OBEX_OPCODE_SETPATH = 133;
    public static final byte OBEX_SRMP_WAIT = 1;
    public static final byte OBEX_SRM_DISABLE = 0;
    public static final byte OBEX_SRM_ENABLE = 1;
    public static final byte OBEX_SRM_SUPPORT = 2;
    private static final java.lang.String TAG = "ObexHelper";
    public static final boolean VDBG = false;

    private ObexHelper() {
    }

    public static byte[] updateHeaderSet(com.navdy.hud.app.bluetooth.obex.HeaderSet header, byte[] headerArray) throws java.io.IOException {
        int index = 0;
        byte[] body = null;
        com.navdy.hud.app.bluetooth.obex.HeaderSet headerImpl = header;
        while (index < headerArray.length) {
            try {
                int headerID = headerArray[index] & 255;
                switch (headerID & 192) {
                    case 0:
                    case 64:
                        boolean trimTail = true;
                        int index2 = index + 1;
                        int index3 = index2 + 1;
                        int length = (((headerArray[index2] & 255) << 8) + (headerArray[index3] & com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient.ORDER_BY_DEFAULT)) - 3;
                        int index4 = index3 + 1;
                        byte[] value = new byte[length];
                        java.lang.System.arraycopy(headerArray, index4, value, 0, length);
                        if (length == 0 || (length > 0 && value[length - 1] != 0)) {
                            trimTail = false;
                        }
                        switch (headerID) {
                            case com.navdy.hud.app.bluetooth.obex.HeaderSet.TYPE /*66*/:
                                if (trimTail) {
                                    headerImpl.setHeader(headerID, new java.lang.String(value, 0, value.length - 1, "ISO8859_1"));
                                    break;
                                } else {
                                    headerImpl.setHeader(headerID, new java.lang.String(value, 0, value.length, "ISO8859_1"));
                                    break;
                                }
                            case com.navdy.hud.app.bluetooth.obex.HeaderSet.TIME_ISO_8601 /*68*/:
                                java.lang.String dateString = new java.lang.String(value, "ISO8859_1");
                                java.util.Calendar temp = java.util.Calendar.getInstance();
                                if (dateString.length() == 16 && dateString.charAt(15) == 'Z') {
                                    temp.setTimeZone(java.util.TimeZone.getTimeZone("UTC"));
                                }
                                temp.set(1, java.lang.Integer.parseInt(dateString.substring(0, 4)));
                                temp.set(2, java.lang.Integer.parseInt(dateString.substring(4, 6)));
                                temp.set(5, java.lang.Integer.parseInt(dateString.substring(6, 8)));
                                temp.set(11, java.lang.Integer.parseInt(dateString.substring(9, 11)));
                                temp.set(12, java.lang.Integer.parseInt(dateString.substring(11, 13)));
                                temp.set(13, java.lang.Integer.parseInt(dateString.substring(13, 15)));
                                headerImpl.setHeader(68, temp);
                                break;
                            case com.navdy.hud.app.bluetooth.obex.HeaderSet.BODY /*72*/:
                            case com.navdy.hud.app.bluetooth.obex.HeaderSet.END_OF_BODY /*73*/:
                                body = new byte[(length + 1)];
                                body[0] = (byte) headerID;
                                java.lang.System.arraycopy(headerArray, index4, body, 1, length);
                                break;
                            case com.navdy.hud.app.bluetooth.obex.HeaderSet.AUTH_CHALLENGE /*77*/:
                                headerImpl.mAuthChall = new byte[length];
                                java.lang.System.arraycopy(headerArray, index4, headerImpl.mAuthChall, 0, length);
                                break;
                            case com.navdy.hud.app.bluetooth.obex.HeaderSet.AUTH_RESPONSE /*78*/:
                                headerImpl.mAuthResp = new byte[length];
                                java.lang.System.arraycopy(headerArray, index4, headerImpl.mAuthResp, 0, length);
                                break;
                            default:
                                if ((headerID & 192) != 0) {
                                    headerImpl.setHeader(headerID, value);
                                    break;
                                } else {
                                    headerImpl.setHeader(headerID, convertToUnicode(value, true));
                                    break;
                                }
                        }
                        index = index4 + length;
                        break;
                    case 128:
                        int index5 = index + 1;
                        try {
                            headerImpl.setHeader(headerID, java.lang.Byte.valueOf(headerArray[index5]));
                        } catch (java.lang.Exception e) {
                        }
                        index = index5 + 1;
                        break;
                    case 192:
                        int index6 = index + 1;
                        byte[] value2 = new byte[4];
                        java.lang.System.arraycopy(headerArray, index6, value2, 0, 4);
                        if (headerID == 196) {
                            java.util.Calendar temp2 = java.util.Calendar.getInstance();
                            temp2.setTime(new java.util.Date(convertToLong(value2) * 1000));
                            headerImpl.setHeader(196, temp2);
                        } else if (headerID == 203) {
                            headerImpl.mConnectionID = new byte[4];
                            java.lang.System.arraycopy(value2, 0, headerImpl.mConnectionID, 0, 4);
                        } else {
                            headerImpl.setHeader(headerID, java.lang.Long.valueOf(convertToLong(value2)));
                        }
                        index = index6 + 4;
                        break;
                }
            } catch (java.lang.Exception e2) {
                throw new java.io.IOException("Header was not formatted properly", e2);
            } catch (java.io.UnsupportedEncodingException e3) {
                throw e3;
            } catch (java.io.UnsupportedEncodingException e4) {
                throw e4;
            } catch (java.io.IOException e5) {
                throw new java.io.IOException("Header was not formatted properly", e5);
            }
        }
        return body;
    }

    public static byte[] createHeader(com.navdy.hud.app.bluetooth.obex.HeaderSet head, boolean nullOut) {
        byte[] result;
        byte[] lengthArray = new byte[2];
        java.io.ByteArrayOutputStream out = new java.io.ByteArrayOutputStream();
        com.navdy.hud.app.bluetooth.obex.HeaderSet headImpl = head;
        try {
            if (headImpl.mConnectionID != null && headImpl.getHeader(70) == null) {
                out.write(-53);
                out.write(headImpl.mConnectionID);
            }
            java.lang.Long intHeader = (java.lang.Long) headImpl.getHeader(192);
            if (intHeader != null) {
                out.write(-64);
                out.write(convertToByteArray(intHeader.longValue()));
                if (nullOut) {
                    headImpl.setHeader(192, null);
                }
            }
            java.lang.String stringHeader = (java.lang.String) headImpl.getHeader(1);
            if (stringHeader != null) {
                out.write(1);
                byte[] value = convertToUnicodeByteArray(stringHeader);
                int length = value.length + 3;
                lengthArray[0] = (byte) ((length >> 8) & 255);
                lengthArray[1] = (byte) (length & 255);
                out.write(lengthArray);
                out.write(value);
                if (nullOut) {
                    headImpl.setHeader(1, null);
                }
            } else if (headImpl.getEmptyNameHeader()) {
                out.write(1);
                lengthArray[0] = 0;
                lengthArray[1] = 3;
                out.write(lengthArray);
            }
            java.lang.String stringHeader2 = (java.lang.String) headImpl.getHeader(66);
            if (stringHeader2 != null) {
                out.write(66);
                byte[] value2 = stringHeader2.getBytes("ISO8859_1");
                int length2 = value2.length + 4;
                lengthArray[0] = (byte) ((length2 >> 8) & 255);
                lengthArray[1] = (byte) (length2 & 255);
                out.write(lengthArray);
                out.write(value2);
                out.write(0);
                if (nullOut) {
                    headImpl.setHeader(66, null);
                }
            }
            java.lang.Long intHeader2 = (java.lang.Long) headImpl.getHeader(195);
            if (intHeader2 != null) {
                out.write(-61);
                out.write(convertToByteArray(intHeader2.longValue()));
                if (nullOut) {
                    headImpl.setHeader(195, null);
                }
            }
            java.util.Calendar dateHeader = (java.util.Calendar) headImpl.getHeader(68);
            if (dateHeader != null) {
                java.lang.StringBuffer buffer = new java.lang.StringBuffer();
                try {
                    int temp = dateHeader.get(1);
                    for (int i = temp; i < 1000; i *= 10) {
                        buffer.append("0");
                    }
                    buffer.append(temp);
                    int temp2 = dateHeader.get(2);
                    if (temp2 < 10) {
                        buffer.append("0");
                    }
                    buffer.append(temp2);
                    int temp3 = dateHeader.get(5);
                    if (temp3 < 10) {
                        buffer.append("0");
                    }
                    buffer.append(temp3);
                    buffer.append("T");
                    int temp4 = dateHeader.get(11);
                    if (temp4 < 10) {
                        buffer.append("0");
                    }
                    buffer.append(temp4);
                    int temp5 = dateHeader.get(12);
                    if (temp5 < 10) {
                        buffer.append("0");
                    }
                    buffer.append(temp5);
                    int temp6 = dateHeader.get(13);
                    if (temp6 < 10) {
                        buffer.append("0");
                    }
                    buffer.append(temp6);
                    if (dateHeader.getTimeZone().getID().equals("UTC")) {
                        buffer.append("Z");
                    }
                    byte[] value3 = buffer.toString().getBytes("ISO8859_1");
                    int length3 = value3.length + 3;
                    lengthArray[0] = (byte) ((length3 >> 8) & 255);
                    lengthArray[1] = (byte) (length3 & 255);
                    out.write(68);
                    out.write(lengthArray);
                    out.write(value3);
                    if (nullOut) {
                        headImpl.setHeader(68, null);
                    }
                    java.lang.StringBuffer stringBuffer = buffer;
                } catch (java.io.UnsupportedEncodingException e) {
                    throw e;
                } catch (java.io.IOException e2) {
                    java.lang.StringBuffer stringBuffer2 = buffer;
                } catch (Throwable th) {
                    th = th;
                    java.lang.StringBuffer stringBuffer3 = buffer;
                    byte[] result2 = out.toByteArray();
                    try {
                        out.close();
                    } catch (java.lang.Exception e3) {
                    }
                    throw th;
                }
            }
            java.util.Calendar dateHeader2 = (java.util.Calendar) headImpl.getHeader(196);
            if (dateHeader2 != null) {
                out.write(196);
                out.write(convertToByteArray(dateHeader2.getTime().getTime() / 1000));
                if (nullOut) {
                    headImpl.setHeader(196, null);
                }
            }
            java.lang.String stringHeader3 = (java.lang.String) headImpl.getHeader(5);
            if (stringHeader3 != null) {
                out.write(5);
                byte[] value4 = convertToUnicodeByteArray(stringHeader3);
                int length4 = value4.length + 3;
                lengthArray[0] = (byte) ((length4 >> 8) & 255);
                lengthArray[1] = (byte) (length4 & 255);
                out.write(lengthArray);
                out.write(value4);
                if (nullOut) {
                    headImpl.setHeader(5, null);
                }
            }
            byte[] value5 = (byte[]) headImpl.getHeader(70);
            if (value5 != null) {
                out.write(70);
                int length5 = value5.length + 3;
                lengthArray[0] = (byte) ((length5 >> 8) & 255);
                lengthArray[1] = (byte) (length5 & 255);
                out.write(lengthArray);
                out.write(value5);
                if (nullOut) {
                    headImpl.setHeader(70, null);
                }
            }
            byte[] value6 = (byte[]) headImpl.getHeader(71);
            if (value6 != null) {
                out.write(71);
                int length6 = value6.length + 3;
                lengthArray[0] = (byte) ((length6 >> 8) & 255);
                lengthArray[1] = (byte) (length6 & 255);
                out.write(lengthArray);
                out.write(value6);
                if (nullOut) {
                    headImpl.setHeader(71, null);
                }
            }
            byte[] value7 = (byte[]) headImpl.getHeader(74);
            if (value7 != null) {
                out.write(74);
                int length7 = value7.length + 3;
                lengthArray[0] = (byte) ((length7 >> 8) & 255);
                lengthArray[1] = (byte) (length7 & 255);
                out.write(lengthArray);
                out.write(value7);
                if (nullOut) {
                    headImpl.setHeader(74, null);
                }
            }
            byte[] value8 = (byte[]) headImpl.getHeader(76);
            if (value8 != null) {
                out.write(76);
                int length8 = value8.length + 3;
                lengthArray[0] = (byte) ((length8 >> 8) & 255);
                lengthArray[1] = (byte) (length8 & 255);
                out.write(lengthArray);
                out.write(value8);
                if (nullOut) {
                    headImpl.setHeader(76, null);
                }
            }
            byte[] value9 = (byte[]) headImpl.getHeader(79);
            if (value9 != null) {
                out.write(79);
                int length9 = value9.length + 3;
                lengthArray[0] = (byte) ((length9 >> 8) & 255);
                lengthArray[1] = (byte) (length9 & 255);
                out.write(lengthArray);
                out.write(value9);
                if (nullOut) {
                    headImpl.setHeader(79, null);
                }
            }
            for (int i2 = 0; i2 < 16; i2++) {
                java.lang.String stringHeader4 = (java.lang.String) headImpl.getHeader(i2 + 48);
                if (stringHeader4 != null) {
                    out.write(((byte) i2) + com.navdy.hud.app.service.pandora.messages.BaseMessage.PNDR_EVENT_TRACK_PLAY);
                    byte[] value10 = convertToUnicodeByteArray(stringHeader4);
                    int length10 = value10.length + 3;
                    lengthArray[0] = (byte) ((length10 >> 8) & 255);
                    lengthArray[1] = (byte) (length10 & 255);
                    out.write(lengthArray);
                    out.write(value10);
                    if (nullOut) {
                        headImpl.setHeader(i2 + 48, null);
                    }
                }
                byte[] value11 = (byte[]) headImpl.getHeader(i2 + 112);
                if (value11 != null) {
                    out.write(((byte) i2) + 112);
                    int length11 = value11.length + 3;
                    lengthArray[0] = (byte) ((length11 >> 8) & 255);
                    lengthArray[1] = (byte) (length11 & 255);
                    out.write(lengthArray);
                    out.write(value11);
                    if (nullOut) {
                        headImpl.setHeader(i2 + 112, null);
                    }
                }
                java.lang.Byte byteHeader = (java.lang.Byte) headImpl.getHeader(i2 + com.navdy.hud.app.bluetooth.obex.ResponseCodes.OBEX_HTTP_MULT_CHOICE);
                if (byteHeader != null) {
                    out.write(((byte) i2) + 176);
                    out.write(byteHeader.byteValue());
                    if (nullOut) {
                        headImpl.setHeader(i2 + com.navdy.hud.app.bluetooth.obex.ResponseCodes.OBEX_HTTP_MULT_CHOICE, null);
                    }
                }
                java.lang.Long intHeader3 = (java.lang.Long) headImpl.getHeader(i2 + com.glympse.android.lib.StaticConfig.PLACE_SEARCH_DISTANCE_FILTER);
                if (intHeader3 != null) {
                    out.write(((byte) i2) + 240);
                    out.write(convertToByteArray(intHeader3.longValue()));
                    if (nullOut) {
                        headImpl.setHeader(i2 + com.glympse.android.lib.StaticConfig.PLACE_SEARCH_DISTANCE_FILTER, null);
                    }
                }
            }
            if (headImpl.mAuthChall != null) {
                out.write(77);
                int length12 = headImpl.mAuthChall.length + 3;
                lengthArray[0] = (byte) ((length12 >> 8) & 255);
                lengthArray[1] = (byte) (length12 & 255);
                out.write(lengthArray);
                out.write(headImpl.mAuthChall);
                if (nullOut) {
                    headImpl.mAuthChall = null;
                }
            }
            if (headImpl.mAuthResp != null) {
                out.write(78);
                int length13 = headImpl.mAuthResp.length + 3;
                lengthArray[0] = (byte) ((length13 >> 8) & 255);
                lengthArray[1] = (byte) (length13 & 255);
                out.write(lengthArray);
                out.write(headImpl.mAuthResp);
                if (nullOut) {
                    headImpl.mAuthResp = null;
                }
            }
            java.lang.Byte byteHeader2 = (java.lang.Byte) headImpl.getHeader(com.navdy.hud.app.bluetooth.obex.HeaderSet.SINGLE_RESPONSE_MODE);
            if (byteHeader2 != null) {
                out.write(-105);
                out.write(byteHeader2.byteValue());
                if (nullOut) {
                    headImpl.setHeader(com.navdy.hud.app.bluetooth.obex.HeaderSet.SINGLE_RESPONSE_MODE, null);
                }
            }
            java.lang.Byte byteHeader3 = (java.lang.Byte) headImpl.getHeader(com.navdy.hud.app.bluetooth.obex.HeaderSet.SINGLE_RESPONSE_MODE_PARAMETER);
            if (byteHeader3 != null) {
                out.write(-104);
                out.write(byteHeader3.byteValue());
                if (nullOut) {
                    headImpl.setHeader(com.navdy.hud.app.bluetooth.obex.HeaderSet.SINGLE_RESPONSE_MODE_PARAMETER, null);
                }
            }
            result = out.toByteArray();
            try {
                out.close();
            } catch (java.lang.Exception e4) {
            }
        } catch (java.io.UnsupportedEncodingException e5) {
            throw e5;
        } catch (java.io.IOException e6) {
        } catch (Throwable th2) {
            th = th2;
            byte[] result22 = out.toByteArray();
            out.close();
            throw th;
        }
        return result;
        result = out.toByteArray();
        try {
            out.close();
        } catch (java.lang.Exception e7) {
        }
        return result;
    }

    public static int findHeaderEnd(byte[] headerArray, int start, int maxSize) {
        int fullLength = 0;
        int lastLength = -1;
        int index = start;
        while (fullLength < maxSize && index < headerArray.length) {
            lastLength = fullLength;
            switch ((headerArray[index] < 0 ? headerArray[index] + 256 : headerArray[index]) & true) {
                case false:
                case true:
                    int index2 = index + 1;
                    int length = (headerArray[index2] < 0 ? headerArray[index2] + 256 : headerArray[index2]) << 8;
                    int index3 = index2 + 1;
                    int length2 = (length + (headerArray[index3] < 0 ? headerArray[index3] + 256 : headerArray[index3])) - 3;
                    index = index3 + 1 + length2;
                    fullLength += length2 + 3;
                    break;
                case true:
                    index = index + 1 + 1;
                    fullLength += 2;
                    break;
                case true:
                    index += 5;
                    fullLength += 5;
                    break;
            }
        }
        if (lastLength != 0) {
            return lastLength + start;
        }
        if (fullLength < maxSize) {
            return headerArray.length;
        }
        return -1;
    }

    public static long convertToLong(byte[] b) {
        long result = 0;
        long power = 0;
        for (int i = b.length - 1; i >= 0; i--) {
            long value = (long) b[i];
            if (value < 0) {
                value += 256;
            }
            result |= value << ((int) power);
            power += 8;
        }
        return result;
    }

    public static byte[] convertToByteArray(long l) {
        return new byte[]{(byte) ((int) ((l >> 24) & 255)), (byte) ((int) ((l >> 16) & 255)), (byte) ((int) ((l >> 8) & 255)), (byte) ((int) (255 & l))};
    }

    public static byte[] convertToUnicodeByteArray(java.lang.String s) {
        if (s == null) {
            return null;
        }
        char[] c = s.toCharArray();
        byte[] result = new byte[((c.length * 2) + 2)];
        for (int i = 0; i < c.length; i++) {
            result[i * 2] = (byte) (c[i] >> 8);
            result[(i * 2) + 1] = (byte) c[i];
        }
        result[result.length - 2] = 0;
        result[result.length - 1] = 0;
        return result;
    }

    public static byte[] getTagValue(byte tag, byte[] triplet) {
        int index = findTag(tag, triplet);
        if (index == -1) {
            return null;
        }
        int index2 = index + 1;
        byte length = triplet[index2] & com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient.ORDER_BY_DEFAULT;
        byte[] result = new byte[length];
        java.lang.System.arraycopy(triplet, index2 + 1, result, 0, length);
        return result;
    }

    public static int findTag(byte tag, byte[] value) {
        if (value == null) {
            return -1;
        }
        int index = 0;
        while (index < value.length && value[index] != tag) {
            index += (value[index + 1] & 255) + 2;
        }
        if (index >= value.length) {
            return -1;
        }
        return index;
    }

    /* JADX WARNING: Incorrect type for immutable var: ssa=byte, code=int, for r3v0, types: [byte, int] */
    /* JADX WARNING: Incorrect type for immutable var: ssa=byte, code=int, for r4v0, types: [byte, int] */
    /* JADX WARNING: Multi-variable type inference failed */
    public static java.lang.String convertToUnicode(byte[] b, boolean includesNull) {
        if (b == 0 || b.length == 0) {
            return null;
        }
        int arrayLength = b.length;
        if (arrayLength % 2 != 0) {
            throw new java.lang.IllegalArgumentException("Byte array not of a valid form");
        }
        int arrayLength2 = arrayLength >> 1;
        if (includesNull) {
            arrayLength2--;
        }
        char[] c = new char[arrayLength2];
        for (int i = 0; i < arrayLength2; i++) {
            int upper = b[i * 2];
            int lower = b[(i * 2) + 1];
            if (upper < 0) {
                upper += 256;
            }
            if (lower < 0) {
                lower += 256;
            }
            if (upper == 0 && lower == 0) {
                return new java.lang.String(c, 0, i);
            }
            c[i] = (char) ((upper << 8) | lower);
        }
        return new java.lang.String(c);
    }

    public static byte[] computeMd5Hash(byte[] in) {
        try {
            return java.security.MessageDigest.getInstance("MD5").digest(in);
        } catch (java.security.NoSuchAlgorithmException e) {
            throw new java.lang.RuntimeException(e);
        }
    }

    public static byte[] computeAuthenticationChallenge(byte[] nonce, java.lang.String realm, boolean access, boolean userID) throws java.io.IOException {
        byte[] authChall;
        if (nonce.length != 16) {
            throw new java.lang.IllegalArgumentException("Nonce must be 16 bytes long");
        }
        if (realm == null) {
            authChall = new byte[21];
        } else if (realm.length() >= 255) {
            throw new java.lang.IllegalArgumentException("Realm must be less then 255 bytes");
        } else {
            authChall = new byte[(realm.length() + 24)];
            authChall[21] = 2;
            authChall[22] = (byte) (realm.length() + 1);
            authChall[23] = 1;
            java.lang.System.arraycopy(realm.getBytes("ISO8859_1"), 0, authChall, 24, realm.length());
        }
        authChall[0] = 0;
        authChall[1] = 16;
        java.lang.System.arraycopy(nonce, 0, authChall, 2, 16);
        authChall[18] = 1;
        authChall[19] = 1;
        authChall[20] = 0;
        if (!access) {
            authChall[20] = (byte) (authChall[20] | 2);
        }
        if (userID) {
            authChall[20] = (byte) (authChall[20] | 1);
        }
        return authChall;
    }

    public static int getMaxTxPacketSize(com.navdy.hud.app.bluetooth.obex.ObexTransport transport) {
        return validateMaxPacketSize(transport.getMaxTransmitPacketSize());
    }

    public static int getMaxRxPacketSize(com.navdy.hud.app.bluetooth.obex.ObexTransport transport) {
        return validateMaxPacketSize(transport.getMaxReceivePacketSize());
    }

    private static int validateMaxPacketSize(int size) {
        if (size == -1) {
            return MAX_PACKET_SIZE_INT;
        }
        if (size >= 255) {
            return size;
        }
        throw new java.lang.IllegalArgumentException(size + " is less that the lower limit: " + 255);
    }
}
