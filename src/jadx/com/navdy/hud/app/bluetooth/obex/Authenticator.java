package com.navdy.hud.app.bluetooth.obex;

public interface Authenticator {
    com.navdy.hud.app.bluetooth.obex.PasswordAuthentication onAuthenticationChallenge(java.lang.String str, boolean z, boolean z2);

    byte[] onAuthenticationResponse(byte[] bArr);
}
