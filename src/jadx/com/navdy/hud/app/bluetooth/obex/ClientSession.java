package com.navdy.hud.app.bluetooth.obex;

public final class ClientSession extends com.navdy.hud.app.bluetooth.obex.ObexSession {
    private static final java.lang.String TAG = "ClientSession";
    private byte[] mConnectionId = null;
    private final java.io.InputStream mInput;
    private final boolean mLocalSrmSupported;
    private int mMaxTxPacketSize = 255;
    private boolean mObexConnected;
    private boolean mOpen;
    private final java.io.OutputStream mOutput;
    private boolean mRequestActive;
    private final com.navdy.hud.app.bluetooth.obex.ObexTransport mTransport;

    public ClientSession(com.navdy.hud.app.bluetooth.obex.ObexTransport trans) throws java.io.IOException {
        this.mInput = trans.openInputStream();
        this.mOutput = trans.openOutputStream();
        this.mOpen = true;
        this.mRequestActive = false;
        this.mLocalSrmSupported = trans.isSrmSupported();
        this.mTransport = trans;
    }

    public ClientSession(com.navdy.hud.app.bluetooth.obex.ObexTransport trans, boolean supportsSrm) throws java.io.IOException {
        this.mInput = trans.openInputStream();
        this.mOutput = trans.openOutputStream();
        this.mOpen = true;
        this.mRequestActive = false;
        this.mLocalSrmSupported = supportsSrm;
        this.mTransport = trans;
    }

    public com.navdy.hud.app.bluetooth.obex.HeaderSet connect(com.navdy.hud.app.bluetooth.obex.HeaderSet header) throws java.io.IOException {
        ensureOpen();
        if (this.mObexConnected) {
            throw new java.io.IOException("Already connected to server");
        }
        setRequestActive();
        int totalLength = 4;
        byte[] head = null;
        if (header != null) {
            if (header.nonce != null) {
                this.mChallengeDigest = new byte[16];
                java.lang.System.arraycopy(header.nonce, 0, this.mChallengeDigest, 0, 16);
            }
            head = com.navdy.hud.app.bluetooth.obex.ObexHelper.createHeader(header, false);
            totalLength = 4 + head.length;
        }
        byte[] requestPacket = new byte[totalLength];
        int maxRxPacketSize = com.navdy.hud.app.bluetooth.obex.ObexHelper.getMaxRxPacketSize(this.mTransport);
        requestPacket[0] = 16;
        requestPacket[1] = 0;
        requestPacket[2] = (byte) (maxRxPacketSize >> 8);
        requestPacket[3] = (byte) (maxRxPacketSize & 255);
        if (head != null) {
            java.lang.System.arraycopy(head, 0, requestPacket, 4, head.length);
        }
        if (requestPacket.length + 3 > 65534) {
            throw new java.io.IOException("Packet size exceeds max packet size for connect");
        }
        com.navdy.hud.app.bluetooth.obex.HeaderSet returnHeaderSet = new com.navdy.hud.app.bluetooth.obex.HeaderSet();
        sendRequest(128, requestPacket, returnHeaderSet, null, false);
        if (returnHeaderSet.responseCode == 160) {
            this.mObexConnected = true;
        }
        setRequestInactive();
        return returnHeaderSet;
    }

    public com.navdy.hud.app.bluetooth.obex.Operation get(com.navdy.hud.app.bluetooth.obex.HeaderSet header) throws java.io.IOException {
        com.navdy.hud.app.bluetooth.obex.HeaderSet head;
        if (!this.mObexConnected) {
            throw new java.io.IOException("Not connected to the server");
        }
        setRequestActive();
        ensureOpen();
        if (header == null) {
            head = new com.navdy.hud.app.bluetooth.obex.HeaderSet();
        } else {
            head = header;
            if (head.nonce != null) {
                this.mChallengeDigest = new byte[16];
                java.lang.System.arraycopy(head.nonce, 0, this.mChallengeDigest, 0, 16);
            }
        }
        if (this.mConnectionId != null) {
            head.mConnectionID = new byte[4];
            java.lang.System.arraycopy(this.mConnectionId, 0, head.mConnectionID, 0, 4);
        }
        if (this.mLocalSrmSupported) {
            head.setHeader(com.navdy.hud.app.bluetooth.obex.HeaderSet.SINGLE_RESPONSE_MODE, java.lang.Byte.valueOf(1));
        }
        return new com.navdy.hud.app.bluetooth.obex.ClientOperation(this.mMaxTxPacketSize, this, head, true);
    }

    public void setConnectionID(long id) {
        if (id < 0 || id > 4294967295L) {
            throw new java.lang.IllegalArgumentException("Connection ID is not in a valid range");
        }
        this.mConnectionId = com.navdy.hud.app.bluetooth.obex.ObexHelper.convertToByteArray(id);
    }

    public com.navdy.hud.app.bluetooth.obex.HeaderSet delete(com.navdy.hud.app.bluetooth.obex.HeaderSet header) throws java.io.IOException {
        com.navdy.hud.app.bluetooth.obex.Operation op = put(header);
        op.getResponseCode();
        com.navdy.hud.app.bluetooth.obex.HeaderSet returnValue = op.getReceivedHeader();
        op.close();
        return returnValue;
    }

    public com.navdy.hud.app.bluetooth.obex.HeaderSet disconnect(com.navdy.hud.app.bluetooth.obex.HeaderSet header) throws java.io.IOException {
        if (!this.mObexConnected) {
            throw new java.io.IOException("Not connected to the server");
        }
        setRequestActive();
        ensureOpen();
        byte[] head = null;
        if (header != null) {
            if (header.nonce != null) {
                this.mChallengeDigest = new byte[16];
                java.lang.System.arraycopy(header.nonce, 0, this.mChallengeDigest, 0, 16);
            }
            if (this.mConnectionId != null) {
                header.mConnectionID = new byte[4];
                java.lang.System.arraycopy(this.mConnectionId, 0, header.mConnectionID, 0, 4);
            }
            head = com.navdy.hud.app.bluetooth.obex.ObexHelper.createHeader(header, false);
            if (head.length + 3 > this.mMaxTxPacketSize) {
                throw new java.io.IOException("Packet size exceeds max packet size");
            }
        } else if (this.mConnectionId != null) {
            head = new byte[5];
            head[0] = -53;
            java.lang.System.arraycopy(this.mConnectionId, 0, head, 1, 4);
        }
        com.navdy.hud.app.bluetooth.obex.HeaderSet returnHeaderSet = new com.navdy.hud.app.bluetooth.obex.HeaderSet();
        sendRequest(com.navdy.hud.app.bluetooth.obex.ObexHelper.OBEX_OPCODE_DISCONNECT, head, returnHeaderSet, null, false);
        synchronized (this) {
            this.mObexConnected = false;
            setRequestInactive();
        }
        return returnHeaderSet;
    }

    public long getConnectionID() {
        if (this.mConnectionId == null) {
            return -1;
        }
        return com.navdy.hud.app.bluetooth.obex.ObexHelper.convertToLong(this.mConnectionId);
    }

    public com.navdy.hud.app.bluetooth.obex.Operation put(com.navdy.hud.app.bluetooth.obex.HeaderSet header) throws java.io.IOException {
        com.navdy.hud.app.bluetooth.obex.HeaderSet head;
        if (!this.mObexConnected) {
            throw new java.io.IOException("Not connected to the server");
        }
        setRequestActive();
        ensureOpen();
        if (header == null) {
            head = new com.navdy.hud.app.bluetooth.obex.HeaderSet();
        } else {
            head = header;
            if (head.nonce != null) {
                this.mChallengeDigest = new byte[16];
                java.lang.System.arraycopy(head.nonce, 0, this.mChallengeDigest, 0, 16);
            }
        }
        if (this.mConnectionId != null) {
            head.mConnectionID = new byte[4];
            java.lang.System.arraycopy(this.mConnectionId, 0, head.mConnectionID, 0, 4);
        }
        if (this.mLocalSrmSupported) {
            head.setHeader(com.navdy.hud.app.bluetooth.obex.HeaderSet.SINGLE_RESPONSE_MODE, java.lang.Byte.valueOf(1));
        }
        return new com.navdy.hud.app.bluetooth.obex.ClientOperation(this.mMaxTxPacketSize, this, head, false);
    }

    public void setAuthenticator(com.navdy.hud.app.bluetooth.obex.Authenticator auth) throws java.io.IOException {
        if (auth == null) {
            throw new java.io.IOException("Authenticator may not be null");
        }
        this.mAuthenticator = auth;
    }

    public com.navdy.hud.app.bluetooth.obex.HeaderSet setPath(com.navdy.hud.app.bluetooth.obex.HeaderSet header, boolean backup, boolean create) throws java.io.IOException {
        com.navdy.hud.app.bluetooth.obex.HeaderSet headset;
        if (!this.mObexConnected) {
            throw new java.io.IOException("Not connected to the server");
        }
        setRequestActive();
        ensureOpen();
        if (header == null) {
            headset = new com.navdy.hud.app.bluetooth.obex.HeaderSet();
        } else {
            headset = header;
            if (headset.nonce != null) {
                this.mChallengeDigest = new byte[16];
                java.lang.System.arraycopy(headset.nonce, 0, this.mChallengeDigest, 0, 16);
            }
        }
        if (headset.nonce != null) {
            this.mChallengeDigest = new byte[16];
            java.lang.System.arraycopy(headset.nonce, 0, this.mChallengeDigest, 0, 16);
        }
        if (this.mConnectionId != null) {
            headset.mConnectionID = new byte[4];
            java.lang.System.arraycopy(this.mConnectionId, 0, headset.mConnectionID, 0, 4);
        }
        byte[] head = com.navdy.hud.app.bluetooth.obex.ObexHelper.createHeader(headset, false);
        int totalLength = 2 + head.length;
        if (totalLength > this.mMaxTxPacketSize) {
            throw new java.io.IOException("Packet size exceeds max packet size");
        }
        int flags = 0;
        if (backup) {
            flags = 0 + 1;
        }
        if (!create) {
            flags |= 2;
        }
        byte[] packet = new byte[totalLength];
        packet[0] = (byte) flags;
        packet[1] = 0;
        if (headset != null) {
            java.lang.System.arraycopy(head, 0, packet, 2, head.length);
        }
        com.navdy.hud.app.bluetooth.obex.HeaderSet returnHeaderSet = new com.navdy.hud.app.bluetooth.obex.HeaderSet();
        sendRequest(133, packet, returnHeaderSet, null, false);
        setRequestInactive();
        return returnHeaderSet;
    }

    public synchronized void ensureOpen() throws java.io.IOException {
        if (!this.mOpen) {
            throw new java.io.IOException("Connection closed");
        }
    }

    /* access modifiers changed from: 0000 */
    public synchronized void setRequestInactive() {
        this.mRequestActive = false;
    }

    private synchronized void setRequestActive() throws java.io.IOException {
        if (this.mRequestActive) {
            throw new java.io.IOException("OBEX request is already being performed");
        }
        this.mRequestActive = true;
    }

    public boolean sendRequest(int opCode, byte[] head, com.navdy.hud.app.bluetooth.obex.HeaderSet header, com.navdy.hud.app.bluetooth.obex.PrivateInputStream privateInput, boolean srmActive) throws java.io.IOException {
        byte[] data;
        if (head == null || head.length + 3 <= 65534) {
            boolean skipSend = false;
            boolean skipReceive = false;
            if (srmActive) {
                if (opCode == 2) {
                    skipReceive = true;
                } else if (opCode == 3) {
                    skipReceive = true;
                } else if (opCode == 131) {
                    skipSend = true;
                }
            }
            java.io.ByteArrayOutputStream out = new java.io.ByteArrayOutputStream();
            out.write((byte) opCode);
            if (head == null) {
                out.write(0);
                out.write(3);
            } else {
                out.write((byte) ((head.length + 3) >> 8));
                out.write((byte) (head.length + 3));
                out.write(head);
            }
            if (!skipSend) {
                this.mOutput.write(out.toByteArray());
                this.mOutput.flush();
            }
            if (!skipReceive) {
                header.responseCode = this.mInput.read();
                int length = (this.mInput.read() << 8) | this.mInput.read();
                if (length > com.navdy.hud.app.bluetooth.obex.ObexHelper.getMaxRxPacketSize(this.mTransport)) {
                    throw new java.io.IOException("Packet received exceeds packet size limit");
                } else if (length > 3) {
                    if (opCode == 128) {
                        int read = this.mInput.read();
                        int read2 = this.mInput.read();
                        this.mMaxTxPacketSize = (this.mInput.read() << 8) + this.mInput.read();
                        if (this.mMaxTxPacketSize > 64512) {
                            this.mMaxTxPacketSize = com.navdy.hud.app.bluetooth.obex.ObexHelper.MAX_CLIENT_PACKET_SIZE;
                        }
                        if (this.mMaxTxPacketSize > com.navdy.hud.app.bluetooth.obex.ObexHelper.getMaxTxPacketSize(this.mTransport)) {
                            android.util.Log.w(TAG, "An OBEX packet size of " + this.mMaxTxPacketSize + "was" + " requested. Transport only allows: " + com.navdy.hud.app.bluetooth.obex.ObexHelper.getMaxTxPacketSize(this.mTransport) + " Lowering limit to this value.");
                            this.mMaxTxPacketSize = com.navdy.hud.app.bluetooth.obex.ObexHelper.getMaxTxPacketSize(this.mTransport);
                        }
                        if (length <= 7) {
                            return true;
                        }
                        data = new byte[(length - 7)];
                        int bytesReceived = this.mInput.read(data);
                        while (bytesReceived != length - 7) {
                            bytesReceived += this.mInput.read(data, bytesReceived, data.length - bytesReceived);
                        }
                    } else {
                        data = new byte[(length - 3)];
                        int bytesReceived2 = this.mInput.read(data);
                        while (bytesReceived2 != length - 3) {
                            bytesReceived2 += this.mInput.read(data, bytesReceived2, data.length - bytesReceived2);
                        }
                        if (opCode == 255) {
                            return true;
                        }
                    }
                    byte[] body = com.navdy.hud.app.bluetooth.obex.ObexHelper.updateHeaderSet(header, data);
                    if (!(privateInput == null || body == null)) {
                        privateInput.writeBytes(body, 1);
                    }
                    if (header.mConnectionID != null) {
                        this.mConnectionId = new byte[4];
                        java.lang.System.arraycopy(header.mConnectionID, 0, this.mConnectionId, 0, 4);
                    }
                    if (header.mAuthResp != null) {
                        if (!handleAuthResp(header.mAuthResp)) {
                            setRequestInactive();
                            throw new java.io.IOException("Authentication Failed");
                        }
                    }
                    if (header.responseCode == 193 && header.mAuthChall != null && handleAuthChall(header)) {
                        out.write(78);
                        out.write((byte) ((header.mAuthResp.length + 3) >> 8));
                        out.write((byte) (header.mAuthResp.length + 3));
                        out.write(header.mAuthResp);
                        header.mAuthChall = null;
                        header.mAuthResp = null;
                        byte[] sendHeaders = new byte[(out.size() - 3)];
                        java.lang.System.arraycopy(out.toByteArray(), 3, sendHeaders, 0, sendHeaders.length);
                        return sendRequest(opCode, sendHeaders, header, privateInput, false);
                    }
                }
            }
            return true;
        }
        throw new java.io.IOException("header too large ");
    }

    public void close() throws java.io.IOException {
        this.mOpen = false;
        this.mInput.close();
        this.mOutput.close();
    }

    public boolean isSrmSupported() {
        return this.mLocalSrmSupported;
    }
}
