package com.navdy.hud.app.bluetooth.obex;

public class ServerRequestHandler {
    private long mConnectionId = -1;

    protected ServerRequestHandler() {
    }

    public void setConnectionId(long connectionId) {
        if (connectionId < -1 || connectionId > 4294967295L) {
            throw new java.lang.IllegalArgumentException("Illegal Connection ID");
        }
        this.mConnectionId = connectionId;
    }

    public long getConnectionId() {
        return this.mConnectionId;
    }

    public int onConnect(com.navdy.hud.app.bluetooth.obex.HeaderSet request, com.navdy.hud.app.bluetooth.obex.HeaderSet reply) {
        return com.navdy.hud.app.bluetooth.obex.ResponseCodes.OBEX_HTTP_OK;
    }

    public void onDisconnect(com.navdy.hud.app.bluetooth.obex.HeaderSet request, com.navdy.hud.app.bluetooth.obex.HeaderSet reply) {
    }

    public int onSetPath(com.navdy.hud.app.bluetooth.obex.HeaderSet request, com.navdy.hud.app.bluetooth.obex.HeaderSet reply, boolean backup, boolean create) {
        return com.navdy.hud.app.bluetooth.obex.ResponseCodes.OBEX_HTTP_NOT_IMPLEMENTED;
    }

    public int onDelete(com.navdy.hud.app.bluetooth.obex.HeaderSet request, com.navdy.hud.app.bluetooth.obex.HeaderSet reply) {
        return com.navdy.hud.app.bluetooth.obex.ResponseCodes.OBEX_HTTP_NOT_IMPLEMENTED;
    }

    public int onAbort(com.navdy.hud.app.bluetooth.obex.HeaderSet request, com.navdy.hud.app.bluetooth.obex.HeaderSet reply) {
        return com.navdy.hud.app.bluetooth.obex.ResponseCodes.OBEX_HTTP_NOT_IMPLEMENTED;
    }

    public int onPut(com.navdy.hud.app.bluetooth.obex.Operation operation) {
        return com.navdy.hud.app.bluetooth.obex.ResponseCodes.OBEX_HTTP_NOT_IMPLEMENTED;
    }

    public int onGet(com.navdy.hud.app.bluetooth.obex.Operation operation) {
        return com.navdy.hud.app.bluetooth.obex.ResponseCodes.OBEX_HTTP_NOT_IMPLEMENTED;
    }

    public void onAuthenticationFailure(byte[] userName) {
    }

    public void updateStatus(java.lang.String message) {
    }

    public void onClose() {
    }

    public boolean isSrmSupported() {
        return false;
    }
}
