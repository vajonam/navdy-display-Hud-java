package com.navdy.hud.app.bluetooth.obex;

public class ObexPacket {
    public int mHeaderId;
    public int mLength;
    public byte[] mPayload = null;

    private ObexPacket(int headerId, int length) {
        this.mHeaderId = headerId;
        this.mLength = length;
    }

    public static com.navdy.hud.app.bluetooth.obex.ObexPacket read(java.io.InputStream is) throws java.io.IOException {
        return read(is.read(), is);
    }

    public static com.navdy.hud.app.bluetooth.obex.ObexPacket read(int headerId, java.io.InputStream is) throws java.io.IOException {
        int length = (is.read() << 8) + is.read();
        com.navdy.hud.app.bluetooth.obex.ObexPacket newPacket = new com.navdy.hud.app.bluetooth.obex.ObexPacket(headerId, length);
        byte[] temp = null;
        if (length > 3) {
            temp = new byte[(length - 3)];
            int bytesReceived = is.read(temp);
            while (bytesReceived != temp.length) {
                bytesReceived += is.read(temp, bytesReceived, temp.length - bytesReceived);
            }
        }
        newPacket.mPayload = temp;
        return newPacket;
    }
}
