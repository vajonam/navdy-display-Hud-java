package com.navdy.hud.app.bluetooth.obex;

public interface Operation {
    void abort() throws java.io.IOException;

    void close() throws java.io.IOException;

    java.lang.String getEncoding();

    int getHeaderLength();

    long getLength();

    int getMaxPacketSize();

    com.navdy.hud.app.bluetooth.obex.HeaderSet getReceivedHeader() throws java.io.IOException;

    int getResponseCode() throws java.io.IOException;

    java.lang.String getType();

    void noBodyHeader();

    java.io.DataInputStream openDataInputStream() throws java.io.IOException;

    java.io.DataOutputStream openDataOutputStream() throws java.io.IOException;

    java.io.InputStream openInputStream() throws java.io.IOException;

    java.io.OutputStream openOutputStream() throws java.io.IOException;

    void sendHeaders(com.navdy.hud.app.bluetooth.obex.HeaderSet headerSet) throws java.io.IOException;
}
