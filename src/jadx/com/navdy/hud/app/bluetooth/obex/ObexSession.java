package com.navdy.hud.app.bluetooth.obex;

public class ObexSession {
    private static final java.lang.String TAG = "ObexSession";
    private static final boolean V = false;
    protected com.navdy.hud.app.bluetooth.obex.Authenticator mAuthenticator;
    protected byte[] mChallengeDigest;

    public boolean handleAuthChall(com.navdy.hud.app.bluetooth.obex.HeaderSet header) throws java.io.IOException {
        if (this.mAuthenticator == null) {
            return false;
        }
        byte[] challenge = com.navdy.hud.app.bluetooth.obex.ObexHelper.getTagValue(0, header.mAuthChall);
        byte[] option = com.navdy.hud.app.bluetooth.obex.ObexHelper.getTagValue(1, header.mAuthChall);
        byte[] description = com.navdy.hud.app.bluetooth.obex.ObexHelper.getTagValue(2, header.mAuthChall);
        java.lang.String realm = null;
        if (description != null) {
            byte[] realmString = new byte[(description.length - 1)];
            java.lang.System.arraycopy(description, 1, realmString, 0, realmString.length);
            switch (description[0] & com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient.ORDER_BY_DEFAULT) {
                case 0:
                case 1:
                    try {
                        realm = new java.lang.String(realmString, "ISO8859_1");
                        break;
                    } catch (java.lang.Exception e) {
                        throw new java.io.IOException("Unsupported Encoding Scheme");
                    }
                case 255:
                    realm = com.navdy.hud.app.bluetooth.obex.ObexHelper.convertToUnicode(realmString, false);
                    break;
                default:
                    throw new java.io.IOException("Unsupported Encoding Scheme");
            }
        }
        boolean isUserIDRequired = false;
        boolean isFullAccess = true;
        if (option != null) {
            if ((option[0] & 1) != 0) {
                isUserIDRequired = true;
            }
            if ((option[0] & 2) != 0) {
                isFullAccess = false;
            }
        }
        header.mAuthChall = null;
        try {
            com.navdy.hud.app.bluetooth.obex.PasswordAuthentication result = this.mAuthenticator.onAuthenticationChallenge(realm, isUserIDRequired, isFullAccess);
            if (result == null) {
                return false;
            }
            byte[] password = result.getPassword();
            if (password == null) {
                return false;
            }
            byte[] userName = result.getUserName();
            if (userName != null) {
                header.mAuthResp = new byte[(userName.length + 38)];
                header.mAuthResp[36] = 1;
                header.mAuthResp[37] = (byte) userName.length;
                java.lang.System.arraycopy(userName, 0, header.mAuthResp, 38, userName.length);
            } else {
                header.mAuthResp = new byte[36];
            }
            byte[] digest = new byte[(challenge.length + password.length + 1)];
            java.lang.System.arraycopy(challenge, 0, digest, 0, challenge.length);
            digest[challenge.length] = 58;
            java.lang.System.arraycopy(password, 0, digest, challenge.length + 1, password.length);
            header.mAuthResp[0] = 0;
            header.mAuthResp[1] = 16;
            java.lang.System.arraycopy(com.navdy.hud.app.bluetooth.obex.ObexHelper.computeMd5Hash(digest), 0, header.mAuthResp, 2, 16);
            header.mAuthResp[18] = 2;
            header.mAuthResp[19] = 16;
            java.lang.System.arraycopy(challenge, 0, header.mAuthResp, 20, 16);
            return true;
        } catch (java.lang.Exception e2) {
            return false;
        }
    }

    public boolean handleAuthResp(byte[] authResp) {
        if (this.mAuthenticator == null) {
            return false;
        }
        byte[] correctPassword = this.mAuthenticator.onAuthenticationResponse(com.navdy.hud.app.bluetooth.obex.ObexHelper.getTagValue(1, authResp));
        if (correctPassword == null) {
            return false;
        }
        byte[] temp = new byte[(correctPassword.length + 16)];
        java.lang.System.arraycopy(this.mChallengeDigest, 0, temp, 0, 16);
        java.lang.System.arraycopy(correctPassword, 0, temp, 16, correctPassword.length);
        byte[] correctResponse = com.navdy.hud.app.bluetooth.obex.ObexHelper.computeMd5Hash(temp);
        byte[] actualResponse = com.navdy.hud.app.bluetooth.obex.ObexHelper.getTagValue(0, authResp);
        for (int i = 0; i < 16; i++) {
            if (correctResponse[i] != actualResponse[i]) {
                return false;
            }
        }
        return true;
    }
}
