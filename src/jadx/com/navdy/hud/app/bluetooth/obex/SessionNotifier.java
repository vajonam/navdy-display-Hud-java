package com.navdy.hud.app.bluetooth.obex;

public interface SessionNotifier {
    com.navdy.hud.app.bluetooth.obex.ObexSession acceptAndOpen(com.navdy.hud.app.bluetooth.obex.ServerRequestHandler serverRequestHandler) throws java.io.IOException;

    com.navdy.hud.app.bluetooth.obex.ObexSession acceptAndOpen(com.navdy.hud.app.bluetooth.obex.ServerRequestHandler serverRequestHandler, com.navdy.hud.app.bluetooth.obex.Authenticator authenticator) throws java.io.IOException;
}
