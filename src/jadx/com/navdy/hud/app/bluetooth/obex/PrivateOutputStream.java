package com.navdy.hud.app.bluetooth.obex;

public final class PrivateOutputStream extends java.io.OutputStream {
    private java.io.ByteArrayOutputStream mArray = new java.io.ByteArrayOutputStream();
    private int mMaxPacketSize;
    private boolean mOpen;
    private com.navdy.hud.app.bluetooth.obex.BaseStream mParent;

    public PrivateOutputStream(com.navdy.hud.app.bluetooth.obex.BaseStream p, int maxSize) {
        this.mParent = p;
        this.mMaxPacketSize = maxSize;
        this.mOpen = true;
    }

    public int size() {
        return this.mArray.size();
    }

    public synchronized void write(int b) throws java.io.IOException {
        ensureOpen();
        this.mParent.ensureNotDone();
        this.mArray.write(b);
        if (this.mArray.size() == this.mMaxPacketSize) {
            this.mParent.continueOperation(true, false);
        }
    }

    public void write(byte[] buffer) throws java.io.IOException {
        write(buffer, 0, buffer.length);
    }

    public synchronized void write(byte[] buffer, int offset, int count) throws java.io.IOException {
        int offset1 = offset;
        int remainLength = count;
        if (buffer == null) {
            throw new java.io.IOException("buffer is null");
        }
        if ((offset | count) >= 0) {
            if (count <= buffer.length - offset) {
                ensureOpen();
                this.mParent.ensureNotDone();
                while (this.mArray.size() + remainLength >= this.mMaxPacketSize) {
                    int bufferLeft = this.mMaxPacketSize - this.mArray.size();
                    this.mArray.write(buffer, offset1, bufferLeft);
                    offset1 += bufferLeft;
                    remainLength -= bufferLeft;
                    this.mParent.continueOperation(true, false);
                }
                if (remainLength > 0) {
                    this.mArray.write(buffer, offset1, remainLength);
                }
            }
        }
        throw new java.lang.IndexOutOfBoundsException("index outof bound");
    }

    public synchronized byte[] readBytes(int size) {
        byte[] result;
        if (this.mArray.size() > 0) {
            byte[] temp = this.mArray.toByteArray();
            this.mArray.reset();
            result = new byte[size];
            java.lang.System.arraycopy(temp, 0, result, 0, size);
            if (temp.length != size) {
                this.mArray.write(temp, size, temp.length - size);
            }
        } else {
            result = null;
        }
        return result;
    }

    private void ensureOpen() throws java.io.IOException {
        this.mParent.ensureOpen();
        if (!this.mOpen) {
            throw new java.io.IOException("Output stream is closed");
        }
    }

    public void close() throws java.io.IOException {
        this.mOpen = false;
        this.mParent.streamClosed(false);
    }

    public boolean isClosed() {
        return !this.mOpen;
    }
}
