package com.navdy.hud.app.bluetooth.obex;

public final class ServerSession extends com.navdy.hud.app.bluetooth.obex.ObexSession implements java.lang.Runnable {
    private static final java.lang.String TAG = "Obex ServerSession";
    private static final boolean V = false;
    private boolean mClosed;
    private java.io.InputStream mInput = this.mTransport.openInputStream();
    private com.navdy.hud.app.bluetooth.obex.ServerRequestHandler mListener;
    private int mMaxPacketLength;
    private java.io.OutputStream mOutput = this.mTransport.openOutputStream();
    private java.lang.Thread mProcessThread;
    private com.navdy.hud.app.bluetooth.obex.ObexTransport mTransport;

    public ServerSession(com.navdy.hud.app.bluetooth.obex.ObexTransport trans, com.navdy.hud.app.bluetooth.obex.ServerRequestHandler handler, com.navdy.hud.app.bluetooth.obex.Authenticator auth) throws java.io.IOException {
        this.mAuthenticator = auth;
        this.mTransport = trans;
        this.mListener = handler;
        this.mMaxPacketLength = 256;
        this.mClosed = false;
        this.mProcessThread = new java.lang.Thread(this);
        this.mProcessThread.start();
    }

    public void run() {
        boolean done = false;
        while (!done) {
            try {
                if (!this.mClosed) {
                    int requestType = this.mInput.read();
                    switch (requestType) {
                        case -1:
                            done = true;
                            break;
                        case 2:
                        case 130:
                            handlePutRequest(requestType);
                            break;
                        case 3:
                        case com.navdy.hud.app.bluetooth.obex.ObexHelper.OBEX_OPCODE_GET_FINAL /*131*/:
                            handleGetRequest(requestType);
                            break;
                        case 128:
                            handleConnectRequest();
                            break;
                        case com.navdy.hud.app.bluetooth.obex.ObexHelper.OBEX_OPCODE_DISCONNECT /*129*/:
                            handleDisconnectRequest();
                            done = true;
                            break;
                        case 133:
                            handleSetPathRequest();
                            break;
                        case 255:
                            handleAbortRequest();
                            break;
                        default:
                            int length = (this.mInput.read() << 8) + this.mInput.read();
                            for (int i = 3; i < length; i++) {
                                this.mInput.read();
                            }
                            sendResponse(com.navdy.hud.app.bluetooth.obex.ResponseCodes.OBEX_HTTP_NOT_IMPLEMENTED, null);
                            break;
                    }
                } else {
                    close();
                }
            } catch (java.lang.NullPointerException e) {
                android.util.Log.d(TAG, "Exception occured - ignoring", e);
            } catch (java.lang.Exception e2) {
                android.util.Log.d(TAG, "Exception occured - ignoring", e2);
            }
        }
        close();
    }

    private void handleAbortRequest() throws java.io.IOException {
        int code;
        com.navdy.hud.app.bluetooth.obex.HeaderSet request = new com.navdy.hud.app.bluetooth.obex.HeaderSet();
        com.navdy.hud.app.bluetooth.obex.HeaderSet reply = new com.navdy.hud.app.bluetooth.obex.HeaderSet();
        int length = (this.mInput.read() << 8) + this.mInput.read();
        if (length > com.navdy.hud.app.bluetooth.obex.ObexHelper.getMaxRxPacketSize(this.mTransport)) {
            code = com.navdy.hud.app.bluetooth.obex.ResponseCodes.OBEX_HTTP_REQ_TOO_LARGE;
        } else {
            for (int i = 3; i < length; i++) {
                this.mInput.read();
            }
            int code2 = this.mListener.onAbort(request, reply);
            android.util.Log.v(TAG, "onAbort request handler return value- " + code2);
            code = validateResponseCode(code2);
        }
        sendResponse(code, null);
    }

    private void handlePutRequest(int type) throws java.io.IOException {
        int response;
        com.navdy.hud.app.bluetooth.obex.ServerOperation op = new com.navdy.hud.app.bluetooth.obex.ServerOperation(this, this.mInput, type, this.mMaxPacketLength, this.mListener);
        try {
            if (!op.finalBitSet || op.isValidBody()) {
                response = validateResponseCode(this.mListener.onPut(op));
            } else {
                response = validateResponseCode(this.mListener.onDelete(op.requestHeader, op.replyHeader));
            }
            if (response != 160 && !op.isAborted) {
                op.sendReply(response);
            } else if (!op.isAborted) {
                while (!op.finalBitSet) {
                    op.sendReply(com.navdy.hud.app.bluetooth.obex.ResponseCodes.OBEX_HTTP_CONTINUE);
                }
                op.sendReply(response);
            }
        } catch (java.lang.Exception e) {
            if (!op.isAborted) {
                sendResponse(com.navdy.hud.app.bluetooth.obex.ResponseCodes.OBEX_HTTP_INTERNAL_ERROR, null);
            }
        }
    }

    private void handleGetRequest(int type) throws java.io.IOException {
        com.navdy.hud.app.bluetooth.obex.ServerOperation op = new com.navdy.hud.app.bluetooth.obex.ServerOperation(this, this.mInput, type, this.mMaxPacketLength, this.mListener);
        try {
            int response = validateResponseCode(this.mListener.onGet(op));
            if (!op.isAborted) {
                op.sendReply(response);
            }
        } catch (java.lang.Exception e) {
            sendResponse(com.navdy.hud.app.bluetooth.obex.ResponseCodes.OBEX_HTTP_INTERNAL_ERROR, null);
        }
    }

    public void sendResponse(int code, byte[] header) throws java.io.IOException {
        byte[] data;
        java.io.OutputStream op = this.mOutput;
        if (op != null) {
            if (header != null) {
                int totalLength = 3 + header.length;
                data = new byte[totalLength];
                data[0] = (byte) code;
                data[1] = (byte) (totalLength >> 8);
                data[2] = (byte) totalLength;
                java.lang.System.arraycopy(header, 0, data, 3, header.length);
            } else {
                data = new byte[]{(byte) code, 0, (byte) 3};
            }
            op.write(data);
            op.flush();
        }
    }

    private void handleSetPathRequest() throws java.io.IOException {
        int totalLength = 3;
        byte[] head = null;
        int code = -1;
        com.navdy.hud.app.bluetooth.obex.HeaderSet request = new com.navdy.hud.app.bluetooth.obex.HeaderSet();
        com.navdy.hud.app.bluetooth.obex.HeaderSet reply = new com.navdy.hud.app.bluetooth.obex.HeaderSet();
        int length = (this.mInput.read() << 8) + this.mInput.read();
        int flags = this.mInput.read();
        int read = this.mInput.read();
        if (length > com.navdy.hud.app.bluetooth.obex.ObexHelper.getMaxRxPacketSize(this.mTransport)) {
            code = com.navdy.hud.app.bluetooth.obex.ResponseCodes.OBEX_HTTP_REQ_TOO_LARGE;
            totalLength = 3;
        } else {
            if (length > 5) {
                byte[] headers = new byte[(length - 5)];
                int bytesReceived = this.mInput.read(headers);
                while (bytesReceived != headers.length) {
                    bytesReceived += this.mInput.read(headers, bytesReceived, headers.length - bytesReceived);
                }
                com.navdy.hud.app.bluetooth.obex.ObexHelper.updateHeaderSet(request, headers);
                if (this.mListener.getConnectionId() == -1 || request.mConnectionID == null) {
                    this.mListener.setConnectionId(1);
                } else {
                    this.mListener.setConnectionId(com.navdy.hud.app.bluetooth.obex.ObexHelper.convertToLong(request.mConnectionID));
                }
                if (request.mAuthResp != null) {
                    if (!handleAuthResp(request.mAuthResp)) {
                        code = com.navdy.hud.app.bluetooth.obex.ResponseCodes.OBEX_HTTP_UNAUTHORIZED;
                        this.mListener.onAuthenticationFailure(com.navdy.hud.app.bluetooth.obex.ObexHelper.getTagValue(1, request.mAuthResp));
                    }
                    request.mAuthResp = null;
                }
            }
            if (code != 193) {
                if (request.mAuthChall != null) {
                    handleAuthChall(request);
                    reply.mAuthResp = new byte[request.mAuthResp.length];
                    java.lang.System.arraycopy(request.mAuthResp, 0, reply.mAuthResp, 0, reply.mAuthResp.length);
                    request.mAuthChall = null;
                    request.mAuthResp = null;
                }
                boolean backup = false;
                boolean create = true;
                if ((flags & 1) != 0) {
                    backup = true;
                }
                if ((flags & 2) != 0) {
                    create = false;
                }
                try {
                    code = validateResponseCode(this.mListener.onSetPath(request, reply, backup, create));
                    if (reply.nonce != null) {
                        this.mChallengeDigest = new byte[16];
                        java.lang.System.arraycopy(reply.nonce, 0, this.mChallengeDigest, 0, 16);
                    } else {
                        this.mChallengeDigest = null;
                    }
                    long id = this.mListener.getConnectionId();
                    if (id == -1) {
                        reply.mConnectionID = null;
                    } else {
                        reply.mConnectionID = com.navdy.hud.app.bluetooth.obex.ObexHelper.convertToByteArray(id);
                    }
                    head = com.navdy.hud.app.bluetooth.obex.ObexHelper.createHeader(reply, false);
                    totalLength = 3 + head.length;
                    if (totalLength > this.mMaxPacketLength) {
                        totalLength = 3;
                        head = null;
                        code = com.navdy.hud.app.bluetooth.obex.ResponseCodes.OBEX_HTTP_INTERNAL_ERROR;
                    }
                } catch (java.lang.Exception e) {
                    sendResponse(com.navdy.hud.app.bluetooth.obex.ResponseCodes.OBEX_HTTP_INTERNAL_ERROR, null);
                    return;
                }
            }
        }
        byte[] replyData = new byte[totalLength];
        replyData[0] = (byte) code;
        replyData[1] = (byte) (totalLength >> 8);
        replyData[2] = (byte) totalLength;
        if (head != null) {
            java.lang.System.arraycopy(head, 0, replyData, 3, head.length);
        }
        this.mOutput.write(replyData);
        this.mOutput.flush();
    }

    private void handleDisconnectRequest() throws java.io.IOException {
        byte[] replyData;
        int code = com.navdy.hud.app.bluetooth.obex.ResponseCodes.OBEX_HTTP_OK;
        int totalLength = 3;
        byte[] head = null;
        com.navdy.hud.app.bluetooth.obex.HeaderSet request = new com.navdy.hud.app.bluetooth.obex.HeaderSet();
        com.navdy.hud.app.bluetooth.obex.HeaderSet reply = new com.navdy.hud.app.bluetooth.obex.HeaderSet();
        int length = (this.mInput.read() << 8) + this.mInput.read();
        if (length > com.navdy.hud.app.bluetooth.obex.ObexHelper.getMaxRxPacketSize(this.mTransport)) {
            code = com.navdy.hud.app.bluetooth.obex.ResponseCodes.OBEX_HTTP_REQ_TOO_LARGE;
            totalLength = 3;
        } else {
            if (length > 3) {
                byte[] headers = new byte[(length - 3)];
                int bytesReceived = this.mInput.read(headers);
                while (bytesReceived != headers.length) {
                    bytesReceived += this.mInput.read(headers, bytesReceived, headers.length - bytesReceived);
                }
                com.navdy.hud.app.bluetooth.obex.ObexHelper.updateHeaderSet(request, headers);
            }
            if (this.mListener.getConnectionId() == -1 || request.mConnectionID == null) {
                this.mListener.setConnectionId(1);
            } else {
                this.mListener.setConnectionId(com.navdy.hud.app.bluetooth.obex.ObexHelper.convertToLong(request.mConnectionID));
            }
            if (request.mAuthResp != null) {
                if (!handleAuthResp(request.mAuthResp)) {
                    code = com.navdy.hud.app.bluetooth.obex.ResponseCodes.OBEX_HTTP_UNAUTHORIZED;
                    this.mListener.onAuthenticationFailure(com.navdy.hud.app.bluetooth.obex.ObexHelper.getTagValue(1, request.mAuthResp));
                }
                request.mAuthResp = null;
            }
            if (code != 193) {
                if (request.mAuthChall != null) {
                    handleAuthChall(request);
                    request.mAuthChall = null;
                }
                try {
                    this.mListener.onDisconnect(request, reply);
                    long id = this.mListener.getConnectionId();
                    if (id == -1) {
                        reply.mConnectionID = null;
                    } else {
                        reply.mConnectionID = com.navdy.hud.app.bluetooth.obex.ObexHelper.convertToByteArray(id);
                    }
                    head = com.navdy.hud.app.bluetooth.obex.ObexHelper.createHeader(reply, false);
                    totalLength = 3 + head.length;
                    if (totalLength > this.mMaxPacketLength) {
                        totalLength = 3;
                        head = null;
                        code = com.navdy.hud.app.bluetooth.obex.ResponseCodes.OBEX_HTTP_INTERNAL_ERROR;
                    }
                } catch (java.lang.Exception e) {
                    sendResponse(com.navdy.hud.app.bluetooth.obex.ResponseCodes.OBEX_HTTP_INTERNAL_ERROR, null);
                    return;
                }
            }
        }
        if (head != null) {
            replyData = new byte[(head.length + 3)];
        } else {
            replyData = new byte[3];
        }
        replyData[0] = (byte) code;
        replyData[1] = (byte) (totalLength >> 8);
        replyData[2] = (byte) totalLength;
        if (head != null) {
            java.lang.System.arraycopy(head, 0, replyData, 3, head.length);
        }
        this.mOutput.write(replyData);
        this.mOutput.flush();
    }

    private void handleConnectRequest() throws java.io.IOException {
        int totalLength = 7;
        byte[] head = null;
        int code = -1;
        com.navdy.hud.app.bluetooth.obex.HeaderSet request = new com.navdy.hud.app.bluetooth.obex.HeaderSet();
        com.navdy.hud.app.bluetooth.obex.HeaderSet reply = new com.navdy.hud.app.bluetooth.obex.HeaderSet();
        int packetLength = (this.mInput.read() << 8) + this.mInput.read();
        int read = this.mInput.read();
        int read2 = this.mInput.read();
        this.mMaxPacketLength = this.mInput.read();
        this.mMaxPacketLength = (this.mMaxPacketLength << 8) + this.mInput.read();
        if (this.mMaxPacketLength > 65534) {
            this.mMaxPacketLength = com.navdy.hud.app.bluetooth.obex.ObexHelper.MAX_PACKET_SIZE_INT;
        }
        if (this.mMaxPacketLength > com.navdy.hud.app.bluetooth.obex.ObexHelper.getMaxTxPacketSize(this.mTransport)) {
            android.util.Log.w(TAG, "Requested MaxObexPacketSize " + this.mMaxPacketLength + " is larger than the max size supported by the transport: " + com.navdy.hud.app.bluetooth.obex.ObexHelper.getMaxTxPacketSize(this.mTransport) + " Reducing to this size.");
            this.mMaxPacketLength = com.navdy.hud.app.bluetooth.obex.ObexHelper.getMaxTxPacketSize(this.mTransport);
        }
        if (packetLength > com.navdy.hud.app.bluetooth.obex.ObexHelper.getMaxRxPacketSize(this.mTransport)) {
            code = com.navdy.hud.app.bluetooth.obex.ResponseCodes.OBEX_HTTP_REQ_TOO_LARGE;
            totalLength = 7;
        } else {
            if (packetLength > 7) {
                byte[] headers = new byte[(packetLength - 7)];
                int bytesReceived = this.mInput.read(headers);
                while (bytesReceived != headers.length) {
                    bytesReceived += this.mInput.read(headers, bytesReceived, headers.length - bytesReceived);
                }
                com.navdy.hud.app.bluetooth.obex.ObexHelper.updateHeaderSet(request, headers);
            }
            if (this.mListener.getConnectionId() == -1 || request.mConnectionID == null) {
                this.mListener.setConnectionId(1);
            } else {
                this.mListener.setConnectionId(com.navdy.hud.app.bluetooth.obex.ObexHelper.convertToLong(request.mConnectionID));
            }
            if (request.mAuthResp != null) {
                if (!handleAuthResp(request.mAuthResp)) {
                    code = com.navdy.hud.app.bluetooth.obex.ResponseCodes.OBEX_HTTP_UNAUTHORIZED;
                    this.mListener.onAuthenticationFailure(com.navdy.hud.app.bluetooth.obex.ObexHelper.getTagValue(1, request.mAuthResp));
                }
                request.mAuthResp = null;
            }
            if (code != 193) {
                if (request.mAuthChall != null) {
                    handleAuthChall(request);
                    reply.mAuthResp = new byte[request.mAuthResp.length];
                    java.lang.System.arraycopy(request.mAuthResp, 0, reply.mAuthResp, 0, reply.mAuthResp.length);
                    request.mAuthChall = null;
                    request.mAuthResp = null;
                }
                try {
                    code = validateResponseCode(this.mListener.onConnect(request, reply));
                    if (reply.nonce != null) {
                        this.mChallengeDigest = new byte[16];
                        java.lang.System.arraycopy(reply.nonce, 0, this.mChallengeDigest, 0, 16);
                    } else {
                        this.mChallengeDigest = null;
                    }
                    long id = this.mListener.getConnectionId();
                    if (id == -1) {
                        reply.mConnectionID = null;
                    } else {
                        reply.mConnectionID = com.navdy.hud.app.bluetooth.obex.ObexHelper.convertToByteArray(id);
                    }
                    head = com.navdy.hud.app.bluetooth.obex.ObexHelper.createHeader(reply, false);
                    totalLength = 7 + head.length;
                    if (totalLength > this.mMaxPacketLength) {
                        totalLength = 7;
                        head = null;
                        code = com.navdy.hud.app.bluetooth.obex.ResponseCodes.OBEX_HTTP_INTERNAL_ERROR;
                    }
                } catch (java.lang.Exception e) {
                    totalLength = 7;
                    head = null;
                    code = com.navdy.hud.app.bluetooth.obex.ResponseCodes.OBEX_HTTP_INTERNAL_ERROR;
                }
            }
        }
        byte[] length = com.navdy.hud.app.bluetooth.obex.ObexHelper.convertToByteArray((long) totalLength);
        byte[] sendData = new byte[totalLength];
        int maxRxLength = com.navdy.hud.app.bluetooth.obex.ObexHelper.getMaxRxPacketSize(this.mTransport);
        sendData[0] = (byte) code;
        sendData[1] = length[2];
        sendData[2] = length[3];
        sendData[3] = 16;
        sendData[4] = 0;
        sendData[5] = (byte) (maxRxLength >> 8);
        sendData[6] = (byte) (maxRxLength & 255);
        if (head != null) {
            java.lang.System.arraycopy(head, 0, sendData, 7, head.length);
        }
        this.mOutput.write(sendData);
        this.mOutput.flush();
    }

    public synchronized void close() {
        if (this.mListener != null) {
            this.mListener.onClose();
        }
        try {
            this.mClosed = true;
            if (this.mInput != null) {
                this.mInput.close();
            }
            if (this.mOutput != null) {
                this.mOutput.close();
            }
            if (this.mTransport != null) {
                this.mTransport.close();
            }
        } catch (java.lang.Exception e) {
        }
        this.mTransport = null;
        this.mInput = null;
        this.mOutput = null;
        this.mListener = null;
    }

    private int validateResponseCode(int code) {
        if (code >= 160 && code <= 166) {
            return code;
        }
        if (code >= 176 && code <= 181) {
            return code;
        }
        if (code >= 192 && code <= 207) {
            return code;
        }
        if (code >= 208 && code <= 213) {
            return code;
        }
        if (code < 224 || code > 225) {
            return com.navdy.hud.app.bluetooth.obex.ResponseCodes.OBEX_HTTP_INTERNAL_ERROR;
        }
        return code;
    }

    public com.navdy.hud.app.bluetooth.obex.ObexTransport getTransport() {
        return this.mTransport;
    }
}
