package com.navdy.hud.app.bluetooth.obex;

public final class ServerOperation implements com.navdy.hud.app.bluetooth.obex.Operation, com.navdy.hud.app.bluetooth.obex.BaseStream {
    private static final java.lang.String TAG = "ServerOperation";
    private static final boolean V = false;
    public boolean finalBitSet;
    public boolean isAborted = false;
    private boolean mClosed;
    private java.lang.String mExceptionString;
    private boolean mGetOperation;
    private boolean mHasBody;
    private java.io.InputStream mInput;
    private com.navdy.hud.app.bluetooth.obex.ServerRequestHandler mListener;
    private int mMaxPacketLength;
    private com.navdy.hud.app.bluetooth.obex.ServerSession mParent;
    private com.navdy.hud.app.bluetooth.obex.PrivateInputStream mPrivateInput;
    private com.navdy.hud.app.bluetooth.obex.PrivateOutputStream mPrivateOutput;
    private boolean mPrivateOutputOpen;
    private boolean mRequestFinished;
    private int mResponseSize;
    private boolean mSendBodyHeader = true;
    private boolean mSrmActive = false;
    private boolean mSrmEnabled = false;
    private boolean mSrmLocalWait = false;
    private boolean mSrmResponseSent = false;
    private boolean mSrmWaitingForRemote = true;
    private com.navdy.hud.app.bluetooth.obex.ObexTransport mTransport;
    public com.navdy.hud.app.bluetooth.obex.HeaderSet replyHeader;
    public com.navdy.hud.app.bluetooth.obex.HeaderSet requestHeader;

    public ServerOperation(com.navdy.hud.app.bluetooth.obex.ServerSession p, java.io.InputStream in, int request, int maxSize, com.navdy.hud.app.bluetooth.obex.ServerRequestHandler listen) throws java.io.IOException {
        this.mParent = p;
        this.mInput = in;
        this.mMaxPacketLength = maxSize;
        this.mClosed = false;
        this.requestHeader = new com.navdy.hud.app.bluetooth.obex.HeaderSet();
        this.replyHeader = new com.navdy.hud.app.bluetooth.obex.HeaderSet();
        this.mPrivateInput = new com.navdy.hud.app.bluetooth.obex.PrivateInputStream(this);
        this.mResponseSize = 3;
        this.mListener = listen;
        this.mRequestFinished = false;
        this.mPrivateOutputOpen = false;
        this.mHasBody = false;
        this.mTransport = p.getTransport();
        if (request == 2 || request == 130) {
            this.mGetOperation = false;
            if ((request & 128) == 0) {
                this.finalBitSet = false;
            } else {
                this.finalBitSet = true;
                this.mRequestFinished = true;
            }
        } else if (request == 3 || request == 131) {
            this.mGetOperation = true;
            this.finalBitSet = false;
            if (request == 131) {
                this.mRequestFinished = true;
            }
        } else {
            throw new java.io.IOException("ServerOperation can not handle such request");
        }
        com.navdy.hud.app.bluetooth.obex.ObexPacket packet = com.navdy.hud.app.bluetooth.obex.ObexPacket.read(request, this.mInput);
        if (packet.mLength > com.navdy.hud.app.bluetooth.obex.ObexHelper.getMaxRxPacketSize(this.mTransport)) {
            this.mParent.sendResponse(com.navdy.hud.app.bluetooth.obex.ResponseCodes.OBEX_HTTP_REQ_TOO_LARGE, null);
            throw new java.io.IOException("Packet received was too large. Length: " + packet.mLength + " maxLength: " + com.navdy.hud.app.bluetooth.obex.ObexHelper.getMaxRxPacketSize(this.mTransport));
        }
        if (packet.mLength > 3) {
            if (handleObexPacket(packet)) {
                if (!this.mHasBody) {
                    while (!this.mGetOperation && !this.finalBitSet) {
                        sendReply(com.navdy.hud.app.bluetooth.obex.ResponseCodes.OBEX_HTTP_CONTINUE);
                        if (this.mPrivateInput.available() > 0) {
                            break;
                        }
                    }
                }
            } else {
                return;
            }
        }
        while (!this.mGetOperation && !this.finalBitSet && this.mPrivateInput.available() == 0) {
            sendReply(com.navdy.hud.app.bluetooth.obex.ResponseCodes.OBEX_HTTP_CONTINUE);
            if (this.mPrivateInput.available() > 0) {
                break;
            }
        }
        while (this.mGetOperation && !this.mRequestFinished) {
            sendReply(com.navdy.hud.app.bluetooth.obex.ResponseCodes.OBEX_HTTP_CONTINUE);
        }
    }

    private boolean handleObexPacket(com.navdy.hud.app.bluetooth.obex.ObexPacket packet) throws java.io.IOException {
        byte[] body = updateRequestHeaders(packet);
        if (body != null) {
            this.mHasBody = true;
        }
        if (this.mListener.getConnectionId() == -1 || this.requestHeader.mConnectionID == null) {
            this.mListener.setConnectionId(1);
        } else {
            this.mListener.setConnectionId(com.navdy.hud.app.bluetooth.obex.ObexHelper.convertToLong(this.requestHeader.mConnectionID));
        }
        if (this.requestHeader.mAuthResp != null) {
            if (!this.mParent.handleAuthResp(this.requestHeader.mAuthResp)) {
                this.mExceptionString = "Authentication Failed";
                this.mParent.sendResponse(com.navdy.hud.app.bluetooth.obex.ResponseCodes.OBEX_HTTP_UNAUTHORIZED, null);
                this.mClosed = true;
                this.requestHeader.mAuthResp = null;
                return false;
            }
            this.requestHeader.mAuthResp = null;
        }
        if (this.requestHeader.mAuthChall != null) {
            this.mParent.handleAuthChall(this.requestHeader);
            this.replyHeader.mAuthResp = new byte[this.requestHeader.mAuthResp.length];
            java.lang.System.arraycopy(this.requestHeader.mAuthResp, 0, this.replyHeader.mAuthResp, 0, this.replyHeader.mAuthResp.length);
            this.requestHeader.mAuthResp = null;
            this.requestHeader.mAuthChall = null;
        }
        if (body != null) {
            this.mPrivateInput.writeBytes(body, 1);
        }
        return true;
    }

    private byte[] updateRequestHeaders(com.navdy.hud.app.bluetooth.obex.ObexPacket packet) throws java.io.IOException {
        byte[] body = null;
        if (packet.mPayload != null) {
            body = com.navdy.hud.app.bluetooth.obex.ObexHelper.updateHeaderSet(this.requestHeader, packet.mPayload);
        }
        java.lang.Byte srmMode = (java.lang.Byte) this.requestHeader.getHeader(com.navdy.hud.app.bluetooth.obex.HeaderSet.SINGLE_RESPONSE_MODE);
        if (this.mTransport.isSrmSupported() && srmMode != null && srmMode.byteValue() == 1) {
            this.mSrmEnabled = true;
        }
        checkForSrmWait(packet.mHeaderId);
        if (!this.mSrmWaitingForRemote && this.mSrmEnabled) {
            this.mSrmActive = true;
        }
        return body;
    }

    private void checkForSrmWait(int headerId) {
        if (!this.mSrmEnabled) {
            return;
        }
        if (headerId == 3 || headerId == 131 || headerId == 2) {
            try {
                this.mSrmWaitingForRemote = false;
                java.lang.Byte srmp = (java.lang.Byte) this.requestHeader.getHeader(com.navdy.hud.app.bluetooth.obex.HeaderSet.SINGLE_RESPONSE_MODE_PARAMETER);
                if (srmp != null && srmp.byteValue() == 1) {
                    this.mSrmWaitingForRemote = true;
                    this.requestHeader.setHeader(com.navdy.hud.app.bluetooth.obex.HeaderSet.SINGLE_RESPONSE_MODE_PARAMETER, null);
                }
            } catch (java.io.IOException e) {
            }
        }
    }

    public boolean isValidBody() {
        return this.mHasBody;
    }

    public synchronized boolean continueOperation(boolean sendEmpty, boolean inStream) throws java.io.IOException {
        boolean z = true;
        synchronized (this) {
            if (this.mGetOperation) {
                sendReply(com.navdy.hud.app.bluetooth.obex.ResponseCodes.OBEX_HTTP_CONTINUE);
            } else if (this.finalBitSet) {
                z = false;
            } else if (sendEmpty) {
                sendReply(com.navdy.hud.app.bluetooth.obex.ResponseCodes.OBEX_HTTP_CONTINUE);
            } else if (this.mResponseSize > 3 || this.mPrivateOutput.size() > 0) {
                sendReply(com.navdy.hud.app.bluetooth.obex.ResponseCodes.OBEX_HTTP_CONTINUE);
            } else {
                z = false;
            }
        }
        return z;
    }

    public synchronized boolean sendReply(int type) throws java.io.IOException {
        boolean z;
        java.io.ByteArrayOutputStream out = new java.io.ByteArrayOutputStream();
        boolean skipSend = false;
        boolean skipReceive = false;
        boolean srmRespSendPending = false;
        long id = this.mListener.getConnectionId();
        if (id == -1) {
            this.replyHeader.mConnectionID = null;
        } else {
            this.replyHeader.mConnectionID = com.navdy.hud.app.bluetooth.obex.ObexHelper.convertToByteArray(id);
        }
        if (this.mSrmEnabled && !this.mSrmResponseSent) {
            this.replyHeader.setHeader(com.navdy.hud.app.bluetooth.obex.HeaderSet.SINGLE_RESPONSE_MODE, java.lang.Byte.valueOf(1));
            srmRespSendPending = true;
        }
        if (this.mSrmEnabled && !this.mGetOperation && this.mSrmLocalWait) {
            this.replyHeader.setHeader(com.navdy.hud.app.bluetooth.obex.HeaderSet.SINGLE_RESPONSE_MODE, java.lang.Byte.valueOf(1));
        }
        byte[] headerArray = com.navdy.hud.app.bluetooth.obex.ObexHelper.createHeader(this.replyHeader, true);
        int bodyLength = -1;
        int orginalBodyLength = -1;
        if (this.mPrivateOutput != null) {
            bodyLength = this.mPrivateOutput.size();
            orginalBodyLength = bodyLength;
        }
        if (headerArray.length + 3 > this.mMaxPacketLength) {
            int end = 0;
            int start = 0;
            while (end != headerArray.length) {
                end = com.navdy.hud.app.bluetooth.obex.ObexHelper.findHeaderEnd(headerArray, start, this.mMaxPacketLength - 3);
                if (end == -1) {
                    this.mClosed = true;
                    if (this.mPrivateInput != null) {
                        this.mPrivateInput.close();
                    }
                    if (this.mPrivateOutput != null) {
                        this.mPrivateOutput.close();
                    }
                    this.mParent.sendResponse(com.navdy.hud.app.bluetooth.obex.ResponseCodes.OBEX_HTTP_INTERNAL_ERROR, null);
                    throw new java.io.IOException("OBEX Packet exceeds max packet size");
                }
                byte[] sendHeader = new byte[(end - start)];
                java.lang.System.arraycopy(headerArray, start, sendHeader, 0, sendHeader.length);
                this.mParent.sendResponse(type, sendHeader);
                start = end;
            }
            if (bodyLength > 0) {
                z = true;
            } else {
                z = false;
            }
        } else {
            out.write(headerArray);
            if (this.mGetOperation && type == 160) {
                this.finalBitSet = true;
            }
            if (this.mSrmActive) {
                if (!this.mGetOperation && type == 144 && this.mSrmResponseSent) {
                    skipSend = true;
                } else if (this.mGetOperation && !this.mRequestFinished && this.mSrmResponseSent) {
                    skipSend = true;
                } else if (this.mGetOperation && this.mRequestFinished) {
                    skipReceive = true;
                }
            }
            if (srmRespSendPending) {
                this.mSrmResponseSent = true;
            }
            if ((this.finalBitSet || headerArray.length < this.mMaxPacketLength - 20) && bodyLength > 0) {
                if (bodyLength > (this.mMaxPacketLength - headerArray.length) - 6) {
                    bodyLength = (this.mMaxPacketLength - headerArray.length) - 6;
                }
                byte[] body = this.mPrivateOutput.readBytes(bodyLength);
                if (this.finalBitSet || this.mPrivateOutput.isClosed()) {
                    if (this.mSendBodyHeader) {
                        out.write(73);
                        int bodyLength2 = bodyLength + 3;
                        out.write((byte) (bodyLength2 >> 8));
                        out.write((byte) bodyLength2);
                        out.write(body);
                    }
                } else if (this.mSendBodyHeader) {
                    out.write(72);
                    int bodyLength3 = bodyLength + 3;
                    out.write((byte) (bodyLength3 >> 8));
                    out.write((byte) bodyLength3);
                    out.write(body);
                }
            }
            if (this.finalBitSet && type == 160 && orginalBodyLength <= 0 && this.mSendBodyHeader) {
                out.write(73);
                out.write((byte) 0);
                out.write((byte) 3);
            }
            if (!skipSend) {
                this.mResponseSize = 3;
                this.mParent.sendResponse(type, out.toByteArray());
            }
            if (type == 144) {
                if (!this.mGetOperation || !skipReceive) {
                    com.navdy.hud.app.bluetooth.obex.ObexPacket packet = com.navdy.hud.app.bluetooth.obex.ObexPacket.read(this.mInput);
                    int headerId = packet.mHeaderId;
                    if (headerId == 2 || headerId == 130 || headerId == 3 || headerId == 131) {
                        if (headerId == 130) {
                            this.finalBitSet = true;
                        } else if (headerId == 131) {
                            this.mRequestFinished = true;
                        }
                        if (packet.mLength > com.navdy.hud.app.bluetooth.obex.ObexHelper.getMaxRxPacketSize(this.mTransport)) {
                            this.mParent.sendResponse(com.navdy.hud.app.bluetooth.obex.ResponseCodes.OBEX_HTTP_REQ_TOO_LARGE, null);
                            throw new java.io.IOException("Packet received was too large");
                        } else if ((packet.mLength > 3 || (this.mSrmEnabled && packet.mLength == 3)) && !handleObexPacket(packet)) {
                            z = false;
                        }
                    } else if (headerId == 255) {
                        handleRemoteAbort();
                    } else {
                        this.mParent.sendResponse(192, null);
                        this.mClosed = true;
                        this.mExceptionString = "Bad Request Received";
                        throw new java.io.IOException("Bad Request Received");
                    }
                } else {
                    checkSrmRemoteAbort();
                }
                z = true;
            } else {
                z = false;
            }
        }
        return z;
    }

    private void checkSrmRemoteAbort() throws java.io.IOException {
        if (this.mInput.available() > 0) {
            com.navdy.hud.app.bluetooth.obex.ObexPacket packet = com.navdy.hud.app.bluetooth.obex.ObexPacket.read(this.mInput);
            if (packet.mHeaderId == 255) {
                handleRemoteAbort();
            } else {
                android.util.Log.w(TAG, "Received unexpected request from client - discarding...\n   headerId: " + packet.mHeaderId + " length: " + packet.mLength);
            }
        }
    }

    private void handleRemoteAbort() throws java.io.IOException {
        this.mParent.sendResponse(com.navdy.hud.app.bluetooth.obex.ResponseCodes.OBEX_HTTP_OK, null);
        this.mClosed = true;
        this.isAborted = true;
        this.mExceptionString = "Abort Received";
        throw new java.io.IOException("Abort Received");
    }

    public void abort() throws java.io.IOException {
        throw new java.io.IOException("Called from a server");
    }

    public com.navdy.hud.app.bluetooth.obex.HeaderSet getReceivedHeader() throws java.io.IOException {
        ensureOpen();
        return this.requestHeader;
    }

    public void sendHeaders(com.navdy.hud.app.bluetooth.obex.HeaderSet headers) throws java.io.IOException {
        ensureOpen();
        if (headers == null) {
            throw new java.io.IOException("Headers may not be null");
        }
        int[] headerList = headers.getHeaderList();
        if (headerList != null) {
            for (int i = 0; i < headerList.length; i++) {
                this.replyHeader.setHeader(headerList[i], headers.getHeader(headerList[i]));
            }
        }
    }

    public int getResponseCode() throws java.io.IOException {
        throw new java.io.IOException("Called from a server");
    }

    public java.lang.String getEncoding() {
        return null;
    }

    public java.lang.String getType() {
        try {
            return (java.lang.String) this.requestHeader.getHeader(66);
        } catch (java.io.IOException e) {
            return null;
        }
    }

    public long getLength() {
        try {
            java.lang.Long temp = (java.lang.Long) this.requestHeader.getHeader(195);
            if (temp == null) {
                return -1;
            }
            return temp.longValue();
        } catch (java.io.IOException e) {
            return -1;
        }
    }

    public int getMaxPacketSize() {
        return (this.mMaxPacketLength - 6) - getHeaderLength();
    }

    public int getHeaderLength() {
        long id = this.mListener.getConnectionId();
        if (id == -1) {
            this.replyHeader.mConnectionID = null;
        } else {
            this.replyHeader.mConnectionID = com.navdy.hud.app.bluetooth.obex.ObexHelper.convertToByteArray(id);
        }
        return com.navdy.hud.app.bluetooth.obex.ObexHelper.createHeader(this.replyHeader, false).length;
    }

    public java.io.InputStream openInputStream() throws java.io.IOException {
        ensureOpen();
        return this.mPrivateInput;
    }

    public java.io.DataInputStream openDataInputStream() throws java.io.IOException {
        return new java.io.DataInputStream(openInputStream());
    }

    public java.io.OutputStream openOutputStream() throws java.io.IOException {
        ensureOpen();
        if (this.mPrivateOutputOpen) {
            throw new java.io.IOException("no more input streams available, stream already opened");
        } else if (!this.mRequestFinished) {
            throw new java.io.IOException("no  output streams available ,request not finished");
        } else {
            if (this.mPrivateOutput == null) {
                this.mPrivateOutput = new com.navdy.hud.app.bluetooth.obex.PrivateOutputStream(this, getMaxPacketSize());
            }
            this.mPrivateOutputOpen = true;
            return this.mPrivateOutput;
        }
    }

    public java.io.DataOutputStream openDataOutputStream() throws java.io.IOException {
        return new java.io.DataOutputStream(openOutputStream());
    }

    public void close() throws java.io.IOException {
        ensureOpen();
        this.mClosed = true;
    }

    public void ensureOpen() throws java.io.IOException {
        if (this.mExceptionString != null) {
            throw new java.io.IOException(this.mExceptionString);
        } else if (this.mClosed) {
            throw new java.io.IOException("Operation has already ended");
        }
    }

    public void ensureNotDone() throws java.io.IOException {
    }

    public void streamClosed(boolean inStream) throws java.io.IOException {
    }

    public void noBodyHeader() {
        this.mSendBodyHeader = false;
    }
}
