package com.navdy.hud.app.bluetooth.obex;

public final class PrivateInputStream extends java.io.InputStream {
    private byte[] mData = new byte[0];
    private int mIndex = 0;
    private boolean mOpen = true;
    private com.navdy.hud.app.bluetooth.obex.BaseStream mParent;

    public PrivateInputStream(com.navdy.hud.app.bluetooth.obex.BaseStream p) {
        this.mParent = p;
    }

    public synchronized int available() throws java.io.IOException {
        ensureOpen();
        return this.mData.length - this.mIndex;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:?, code lost:
        r0 = r3.mData;
        r1 = r3.mIndex;
        r3.mIndex = r1 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0022, code lost:
        r0 = r0[r1] & com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient.ORDER_BY_DEFAULT;
     */
    public synchronized int read() throws java.io.IOException {
        byte b;
        ensureOpen();
        while (true) {
            if (this.mData.length == this.mIndex) {
                if (!this.mParent.continueOperation(true, true)) {
                    b = -1;
                    break;
                }
            } else {
                break;
            }
        }
        return b;
    }

    public int read(byte[] b) throws java.io.IOException {
        return read(b, 0, b.length);
    }

    public synchronized int read(byte[] b, int offset, int length) throws java.io.IOException {
        int i;
        if (b == null) {
            throw new java.io.IOException("buffer is null");
        }
        if ((offset | length) >= 0) {
            if (length <= b.length - offset) {
                ensureOpen();
                int currentDataLength = this.mData.length - this.mIndex;
                int remainReadLength = length;
                int offset1 = offset;
                int result = 0;
                while (true) {
                    if (currentDataLength <= remainReadLength) {
                        java.lang.System.arraycopy(this.mData, this.mIndex, b, offset1, currentDataLength);
                        this.mIndex += currentDataLength;
                        offset1 += currentDataLength;
                        result += currentDataLength;
                        remainReadLength -= currentDataLength;
                        if (!this.mParent.continueOperation(true, true)) {
                            i = result == 0 ? -1 : result;
                        } else {
                            currentDataLength = this.mData.length - this.mIndex;
                        }
                    } else {
                        if (remainReadLength > 0) {
                            java.lang.System.arraycopy(this.mData, this.mIndex, b, offset1, remainReadLength);
                            this.mIndex += remainReadLength;
                            result += remainReadLength;
                        }
                        i = result;
                    }
                }
            }
        }
        throw new java.lang.ArrayIndexOutOfBoundsException("index outof bound");
        return i;
    }

    public synchronized void writeBytes(byte[] body, int start) {
        byte[] temp = new byte[((body.length - start) + (this.mData.length - this.mIndex))];
        java.lang.System.arraycopy(this.mData, this.mIndex, temp, 0, this.mData.length - this.mIndex);
        java.lang.System.arraycopy(body, start, temp, this.mData.length - this.mIndex, body.length - start);
        this.mData = temp;
        this.mIndex = 0;
        notifyAll();
    }

    private void ensureOpen() throws java.io.IOException {
        this.mParent.ensureOpen();
        if (!this.mOpen) {
            throw new java.io.IOException("Input stream is closed");
        }
    }

    public void close() throws java.io.IOException {
        this.mOpen = false;
        this.mParent.streamClosed(true);
    }
}
