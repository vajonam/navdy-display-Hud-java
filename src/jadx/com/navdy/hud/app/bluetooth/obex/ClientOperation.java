package com.navdy.hud.app.bluetooth.obex;

public final class ClientOperation implements com.navdy.hud.app.bluetooth.obex.Operation, com.navdy.hud.app.bluetooth.obex.BaseStream {
    private static final java.lang.String TAG = "ClientOperation";
    private static final boolean V = false;
    private boolean mEndOfBodySent;
    private java.lang.String mExceptionMessage;
    private boolean mGetFinalFlag;
    private boolean mGetOperation;
    private boolean mInputOpen;
    private int mMaxPacketSize;
    private boolean mOperationDone;
    private com.navdy.hud.app.bluetooth.obex.ClientSession mParent;
    private com.navdy.hud.app.bluetooth.obex.PrivateInputStream mPrivateInput;
    private boolean mPrivateInputOpen;
    private com.navdy.hud.app.bluetooth.obex.PrivateOutputStream mPrivateOutput;
    private boolean mPrivateOutputOpen;
    private com.navdy.hud.app.bluetooth.obex.HeaderSet mReplyHeader;
    private com.navdy.hud.app.bluetooth.obex.HeaderSet mRequestHeader;
    private boolean mSendBodyHeader = true;
    private boolean mSrmActive = false;
    private boolean mSrmEnabled = false;
    private boolean mSrmWaitingForRemote = true;

    public ClientOperation(int maxSize, com.navdy.hud.app.bluetooth.obex.ClientSession p, com.navdy.hud.app.bluetooth.obex.HeaderSet header, boolean type) throws java.io.IOException {
        this.mParent = p;
        this.mEndOfBodySent = false;
        this.mInputOpen = true;
        this.mOperationDone = false;
        this.mMaxPacketSize = maxSize;
        this.mGetOperation = type;
        this.mGetFinalFlag = false;
        this.mPrivateInputOpen = false;
        this.mPrivateOutputOpen = false;
        this.mPrivateInput = null;
        this.mPrivateOutput = null;
        this.mReplyHeader = new com.navdy.hud.app.bluetooth.obex.HeaderSet();
        this.mRequestHeader = new com.navdy.hud.app.bluetooth.obex.HeaderSet();
        int[] headerList = header.getHeaderList();
        if (headerList != null) {
            for (int i = 0; i < headerList.length; i++) {
                this.mRequestHeader.setHeader(headerList[i], header.getHeader(headerList[i]));
            }
        }
        if (header.mAuthChall != null) {
            this.mRequestHeader.mAuthChall = new byte[header.mAuthChall.length];
            java.lang.System.arraycopy(header.mAuthChall, 0, this.mRequestHeader.mAuthChall, 0, header.mAuthChall.length);
        }
        if (header.mAuthResp != null) {
            this.mRequestHeader.mAuthResp = new byte[header.mAuthResp.length];
            java.lang.System.arraycopy(header.mAuthResp, 0, this.mRequestHeader.mAuthResp, 0, header.mAuthResp.length);
        }
        if (header.mConnectionID != null) {
            this.mRequestHeader.mConnectionID = new byte[4];
            java.lang.System.arraycopy(header.mConnectionID, 0, this.mRequestHeader.mConnectionID, 0, 4);
        }
    }

    public void setGetFinalFlag(boolean flag) {
        this.mGetFinalFlag = flag;
    }

    public synchronized void abort() throws java.io.IOException {
        ensureOpen();
        if (!this.mOperationDone || this.mReplyHeader.responseCode == 144) {
            this.mExceptionMessage = "Operation aborted";
            if (!this.mOperationDone && this.mReplyHeader.responseCode == 144) {
                this.mOperationDone = true;
                this.mParent.sendRequest(255, null, this.mReplyHeader, null, false);
                if (this.mReplyHeader.responseCode != 160) {
                    throw new java.io.IOException("Invalid response code from server");
                }
                this.mExceptionMessage = null;
            }
            close();
        } else {
            throw new java.io.IOException("Operation has already ended");
        }
    }

    public synchronized int getResponseCode() throws java.io.IOException {
        if (this.mReplyHeader.responseCode == -1 || this.mReplyHeader.responseCode == 144) {
            validateConnection();
        }
        return this.mReplyHeader.responseCode;
    }

    public java.lang.String getEncoding() {
        return null;
    }

    public java.lang.String getType() {
        try {
            return (java.lang.String) this.mReplyHeader.getHeader(66);
        } catch (java.io.IOException e) {
            return null;
        }
    }

    public long getLength() {
        try {
            java.lang.Long temp = (java.lang.Long) this.mReplyHeader.getHeader(195);
            if (temp == null) {
                return -1;
            }
            return temp.longValue();
        } catch (java.io.IOException e) {
            return -1;
        }
    }

    public java.io.InputStream openInputStream() throws java.io.IOException {
        ensureOpen();
        if (this.mPrivateInputOpen) {
            throw new java.io.IOException("no more input streams available");
        }
        if (this.mGetOperation) {
            validateConnection();
        } else if (this.mPrivateInput == null) {
            this.mPrivateInput = new com.navdy.hud.app.bluetooth.obex.PrivateInputStream(this);
        }
        this.mPrivateInputOpen = true;
        return this.mPrivateInput;
    }

    public java.io.DataInputStream openDataInputStream() throws java.io.IOException {
        return new java.io.DataInputStream(openInputStream());
    }

    public java.io.OutputStream openOutputStream() throws java.io.IOException {
        ensureOpen();
        ensureNotDone();
        if (this.mPrivateOutputOpen) {
            throw new java.io.IOException("no more output streams available");
        }
        if (this.mPrivateOutput == null) {
            this.mPrivateOutput = new com.navdy.hud.app.bluetooth.obex.PrivateOutputStream(this, getMaxPacketSize());
        }
        this.mPrivateOutputOpen = true;
        return this.mPrivateOutput;
    }

    public int getMaxPacketSize() {
        return (this.mMaxPacketSize - 6) - getHeaderLength();
    }

    public int getHeaderLength() {
        return com.navdy.hud.app.bluetooth.obex.ObexHelper.createHeader(this.mRequestHeader, false).length;
    }

    public java.io.DataOutputStream openDataOutputStream() throws java.io.IOException {
        return new java.io.DataOutputStream(openOutputStream());
    }

    public void close() throws java.io.IOException {
        this.mInputOpen = false;
        this.mPrivateInputOpen = false;
        this.mPrivateOutputOpen = false;
        this.mParent.setRequestInactive();
    }

    public com.navdy.hud.app.bluetooth.obex.HeaderSet getReceivedHeader() throws java.io.IOException {
        ensureOpen();
        return this.mReplyHeader;
    }

    public void sendHeaders(com.navdy.hud.app.bluetooth.obex.HeaderSet headers) throws java.io.IOException {
        ensureOpen();
        if (this.mOperationDone) {
            throw new java.io.IOException("Operation has already exchanged all data");
        } else if (headers == null) {
            throw new java.io.IOException("Headers may not be null");
        } else {
            int[] headerList = headers.getHeaderList();
            if (headerList != null) {
                for (int i = 0; i < headerList.length; i++) {
                    this.mRequestHeader.setHeader(headerList[i], headers.getHeader(headerList[i]));
                }
            }
        }
    }

    public void ensureNotDone() throws java.io.IOException {
        if (this.mOperationDone) {
            throw new java.io.IOException("Operation has completed");
        }
    }

    public void ensureOpen() throws java.io.IOException {
        this.mParent.ensureOpen();
        if (this.mExceptionMessage != null) {
            throw new java.io.IOException(this.mExceptionMessage);
        } else if (!this.mInputOpen) {
            throw new java.io.IOException("Operation has already ended");
        }
    }

    private void validateConnection() throws java.io.IOException {
        ensureOpen();
        if (this.mPrivateInput == null) {
            startProcessing();
        }
    }

    private boolean sendRequest(int opCode) throws java.io.IOException {
        boolean returnValue = false;
        java.io.ByteArrayOutputStream out = new java.io.ByteArrayOutputStream();
        int bodyLength = -1;
        byte[] headerArray = com.navdy.hud.app.bluetooth.obex.ObexHelper.createHeader(this.mRequestHeader, true);
        if (this.mPrivateOutput != null) {
            bodyLength = this.mPrivateOutput.size();
        }
        if (headerArray.length + 3 + 3 > this.mMaxPacketSize) {
            int end = 0;
            int start = 0;
            while (end != headerArray.length) {
                end = com.navdy.hud.app.bluetooth.obex.ObexHelper.findHeaderEnd(headerArray, start, this.mMaxPacketSize - 3);
                if (end == -1) {
                    this.mOperationDone = true;
                    abort();
                    this.mExceptionMessage = "Header larger then can be sent in a packet";
                    this.mInputOpen = false;
                    if (this.mPrivateInput != null) {
                        this.mPrivateInput.close();
                    }
                    if (this.mPrivateOutput != null) {
                        this.mPrivateOutput.close();
                    }
                    throw new java.io.IOException("OBEX Packet exceeds max packet size");
                }
                byte[] sendHeader = new byte[(end - start)];
                java.lang.System.arraycopy(headerArray, start, sendHeader, 0, sendHeader.length);
                if (!this.mParent.sendRequest(opCode, sendHeader, this.mReplyHeader, this.mPrivateInput, false)) {
                    return false;
                }
                if (this.mReplyHeader.responseCode != 144) {
                    return false;
                }
                start = end;
            }
            checkForSrm();
            if (bodyLength > 0) {
                return true;
            }
            return false;
        }
        if (!this.mSendBodyHeader) {
            opCode |= 128;
        }
        out.write(headerArray);
        if (bodyLength > 0) {
            if (bodyLength > (this.mMaxPacketSize - headerArray.length) - 6) {
                returnValue = true;
                bodyLength = (this.mMaxPacketSize - headerArray.length) - 6;
            }
            byte[] body = this.mPrivateOutput.readBytes(bodyLength);
            if (!this.mPrivateOutput.isClosed() || returnValue || this.mEndOfBodySent || (opCode & 128) == 0) {
                out.write(72);
            } else {
                out.write(73);
                this.mEndOfBodySent = true;
            }
            bodyLength += 3;
            out.write((byte) (bodyLength >> 8));
            out.write((byte) bodyLength);
            if (body != null) {
                out.write(body);
            }
        }
        if (this.mPrivateOutputOpen && bodyLength <= 0 && !this.mEndOfBodySent) {
            if ((opCode & 128) == 0) {
                out.write(72);
            } else {
                out.write(73);
                this.mEndOfBodySent = true;
            }
            out.write((byte) 0);
            out.write((byte) 3);
        }
        if (out.size() == 0) {
            if (!this.mParent.sendRequest(opCode, null, this.mReplyHeader, this.mPrivateInput, this.mSrmActive)) {
                return false;
            }
            checkForSrm();
            return returnValue;
        }
        if (out.size() > 0) {
            if (!this.mParent.sendRequest(opCode, out.toByteArray(), this.mReplyHeader, this.mPrivateInput, this.mSrmActive)) {
                return false;
            }
        }
        checkForSrm();
        if (this.mPrivateOutput != null && this.mPrivateOutput.size() > 0) {
            returnValue = true;
        }
        return returnValue;
    }

    private void checkForSrm() throws java.io.IOException {
        java.lang.Byte srmMode = (java.lang.Byte) this.mReplyHeader.getHeader(com.navdy.hud.app.bluetooth.obex.HeaderSet.SINGLE_RESPONSE_MODE);
        if (this.mParent.isSrmSupported() && srmMode != null && srmMode.byteValue() == 1) {
            this.mSrmEnabled = true;
        }
        if (this.mSrmEnabled) {
            this.mSrmWaitingForRemote = false;
            java.lang.Byte srmp = (java.lang.Byte) this.mReplyHeader.getHeader(com.navdy.hud.app.bluetooth.obex.HeaderSet.SINGLE_RESPONSE_MODE_PARAMETER);
            if (srmp != null && srmp.byteValue() == 1) {
                this.mSrmWaitingForRemote = true;
                this.mReplyHeader.setHeader(com.navdy.hud.app.bluetooth.obex.HeaderSet.SINGLE_RESPONSE_MODE_PARAMETER, null);
            }
        }
        if (!this.mSrmWaitingForRemote && this.mSrmEnabled) {
            this.mSrmActive = true;
        }
    }

    private synchronized void startProcessing() throws java.io.IOException {
        if (this.mPrivateInput == null) {
            this.mPrivateInput = new com.navdy.hud.app.bluetooth.obex.PrivateInputStream(this);
        }
        boolean more = true;
        if (!this.mGetOperation) {
            if (!this.mOperationDone) {
                this.mReplyHeader.responseCode = com.navdy.hud.app.bluetooth.obex.ResponseCodes.OBEX_HTTP_CONTINUE;
                while (more && this.mReplyHeader.responseCode == 144) {
                    more = sendRequest(2);
                }
            }
            if (this.mReplyHeader.responseCode == 144) {
                this.mParent.sendRequest(130, null, this.mReplyHeader, this.mPrivateInput, this.mSrmActive);
            }
            if (this.mReplyHeader.responseCode != 144) {
                this.mOperationDone = true;
            }
        } else if (!this.mOperationDone) {
            this.mReplyHeader.responseCode = com.navdy.hud.app.bluetooth.obex.ResponseCodes.OBEX_HTTP_CONTINUE;
            while (more && this.mReplyHeader.responseCode == 144) {
                more = sendRequest(3);
            }
            if (this.mReplyHeader.responseCode == 144) {
                this.mParent.sendRequest(com.navdy.hud.app.bluetooth.obex.ObexHelper.OBEX_OPCODE_GET_FINAL, null, this.mReplyHeader, this.mPrivateInput, this.mSrmActive);
            }
            if (this.mReplyHeader.responseCode != 144) {
                this.mOperationDone = true;
            } else {
                checkForSrm();
            }
        }
    }

    public synchronized boolean continueOperation(boolean sendEmpty, boolean inStream) throws java.io.IOException {
        boolean z = false;
        synchronized (this) {
            if (this.mGetOperation) {
                if (!inStream || this.mOperationDone) {
                    if (!inStream) {
                        if (!this.mOperationDone) {
                            if (this.mPrivateInput == null) {
                                this.mPrivateInput = new com.navdy.hud.app.bluetooth.obex.PrivateInputStream(this);
                            }
                            sendRequest(3);
                            z = true;
                        }
                    }
                    if (this.mOperationDone) {
                    }
                } else {
                    this.mParent.sendRequest(com.navdy.hud.app.bluetooth.obex.ObexHelper.OBEX_OPCODE_GET_FINAL, null, this.mReplyHeader, this.mPrivateInput, this.mSrmActive);
                    if (this.mReplyHeader.responseCode != 144) {
                        this.mOperationDone = true;
                    } else {
                        checkForSrm();
                    }
                    z = true;
                }
            } else if (!inStream && !this.mOperationDone) {
                if (this.mReplyHeader.responseCode == -1) {
                    this.mReplyHeader.responseCode = com.navdy.hud.app.bluetooth.obex.ResponseCodes.OBEX_HTTP_CONTINUE;
                }
                sendRequest(2);
                z = true;
            } else if ((!inStream || this.mOperationDone) && !this.mOperationDone) {
            }
        }
        return z;
    }

    public void streamClosed(boolean inStream) throws java.io.IOException {
        if (!this.mGetOperation) {
            if (!inStream && !this.mOperationDone) {
                boolean more = true;
                if (this.mPrivateOutput != null && this.mPrivateOutput.size() <= 0 && com.navdy.hud.app.bluetooth.obex.ObexHelper.createHeader(this.mRequestHeader, false).length <= 0) {
                    more = false;
                }
                if (this.mReplyHeader.responseCode == -1) {
                    this.mReplyHeader.responseCode = com.navdy.hud.app.bluetooth.obex.ResponseCodes.OBEX_HTTP_CONTINUE;
                }
                while (more && this.mReplyHeader.responseCode == 144) {
                    more = sendRequest(2);
                }
                while (this.mReplyHeader.responseCode == 144) {
                    sendRequest(130);
                }
                this.mOperationDone = true;
            } else if (inStream && this.mOperationDone) {
                this.mOperationDone = true;
            }
        } else if (inStream && !this.mOperationDone) {
            if (this.mReplyHeader.responseCode == -1) {
                this.mReplyHeader.responseCode = com.navdy.hud.app.bluetooth.obex.ResponseCodes.OBEX_HTTP_CONTINUE;
            }
            while (this.mReplyHeader.responseCode == 144 && !this.mOperationDone) {
                if (!sendRequest(com.navdy.hud.app.bluetooth.obex.ObexHelper.OBEX_OPCODE_GET_FINAL)) {
                    break;
                }
            }
            while (this.mReplyHeader.responseCode == 144 && !this.mOperationDone) {
                this.mParent.sendRequest(com.navdy.hud.app.bluetooth.obex.ObexHelper.OBEX_OPCODE_GET_FINAL, null, this.mReplyHeader, this.mPrivateInput, false);
            }
            this.mOperationDone = true;
        } else if (!inStream && !this.mOperationDone) {
            boolean more2 = true;
            if (this.mPrivateOutput != null && this.mPrivateOutput.size() <= 0 && com.navdy.hud.app.bluetooth.obex.ObexHelper.createHeader(this.mRequestHeader, false).length <= 0) {
                more2 = false;
            }
            if (this.mPrivateInput == null) {
                this.mPrivateInput = new com.navdy.hud.app.bluetooth.obex.PrivateInputStream(this);
            }
            if (this.mPrivateOutput != null && this.mPrivateOutput.size() <= 0) {
                more2 = false;
            }
            this.mReplyHeader.responseCode = com.navdy.hud.app.bluetooth.obex.ResponseCodes.OBEX_HTTP_CONTINUE;
            while (more2 && this.mReplyHeader.responseCode == 144) {
                more2 = sendRequest(3);
            }
            sendRequest(com.navdy.hud.app.bluetooth.obex.ObexHelper.OBEX_OPCODE_GET_FINAL);
            if (this.mReplyHeader.responseCode != 144) {
                this.mOperationDone = true;
            }
        }
    }

    public void noBodyHeader() {
        this.mSendBodyHeader = false;
    }
}
