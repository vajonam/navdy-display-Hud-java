package com.navdy.hud.app.bluetooth.obex;

public final class HeaderSet {
    public static final int APPLICATION_PARAMETER = 76;
    public static final int AUTH_CHALLENGE = 77;
    public static final int AUTH_RESPONSE = 78;
    public static final int BODY = 72;
    public static final int CONNECTION_ID = 203;
    public static final int COUNT = 192;
    public static final int DESCRIPTION = 5;
    public static final int END_OF_BODY = 73;
    public static final int HTTP = 71;
    public static final int LENGTH = 195;
    public static final int NAME = 1;
    public static final int OBJECT_CLASS = 79;
    public static final int SINGLE_RESPONSE_MODE = 151;
    public static final int SINGLE_RESPONSE_MODE_PARAMETER = 152;
    public static final int TARGET = 70;
    public static final int TIME_4_BYTE = 196;
    public static final int TIME_ISO_8601 = 68;
    public static final int TYPE = 66;
    public static final int WHO = 74;
    private byte[] mAppParam;
    public byte[] mAuthChall;
    public byte[] mAuthResp;
    private java.util.Calendar mByteTime;
    private java.lang.Byte[] mByteUserDefined = new java.lang.Byte[16];
    public byte[] mConnectionID;
    private java.lang.Long mCount;
    private java.lang.String mDescription;
    private boolean mEmptyName;
    private byte[] mHttpHeader;
    private java.lang.Long[] mIntegerUserDefined = new java.lang.Long[16];
    private java.util.Calendar mIsoTime;
    private java.lang.Long mLength;
    private java.lang.String mName;
    private byte[] mObjectClass;
    private java.security.SecureRandom mRandom = null;
    private byte[][] mSequenceUserDefined = new byte[16][];
    private java.lang.Byte mSingleResponseMode;
    private java.lang.Byte mSrmParam;
    private byte[] mTarget;
    private java.lang.String mType;
    private java.lang.String[] mUnicodeUserDefined = new java.lang.String[16];
    private byte[] mWho;
    byte[] nonce;
    public int responseCode = -1;

    public void setEmptyNameHeader() {
        this.mName = null;
        this.mEmptyName = true;
    }

    public boolean getEmptyNameHeader() {
        return this.mEmptyName;
    }

    public void setHeader(int headerID, java.lang.Object headerValue) {
        switch (headerID) {
            case 1:
                if (headerValue == null || (headerValue instanceof java.lang.String)) {
                    this.mEmptyName = false;
                    this.mName = (java.lang.String) headerValue;
                    return;
                }
                throw new java.lang.IllegalArgumentException("Name must be a String");
            case 5:
                if (headerValue == null || (headerValue instanceof java.lang.String)) {
                    this.mDescription = (java.lang.String) headerValue;
                    return;
                }
                throw new java.lang.IllegalArgumentException("Description must be a String");
            case TYPE /*66*/:
                if (headerValue == null || (headerValue instanceof java.lang.String)) {
                    this.mType = (java.lang.String) headerValue;
                    return;
                }
                throw new java.lang.IllegalArgumentException("Type must be a String");
            case TIME_ISO_8601 /*68*/:
                if (headerValue == null || (headerValue instanceof java.util.Calendar)) {
                    this.mIsoTime = (java.util.Calendar) headerValue;
                    return;
                }
                throw new java.lang.IllegalArgumentException("Time ISO 8601 must be a Calendar");
            case TARGET /*70*/:
                if (headerValue == null) {
                    this.mTarget = null;
                    return;
                } else if (!(headerValue instanceof byte[])) {
                    throw new java.lang.IllegalArgumentException("Target must be a byte array");
                } else {
                    this.mTarget = new byte[((byte[]) headerValue).length];
                    java.lang.System.arraycopy(headerValue, 0, this.mTarget, 0, this.mTarget.length);
                    return;
                }
            case HTTP /*71*/:
                if (headerValue == null) {
                    this.mHttpHeader = null;
                    return;
                } else if (!(headerValue instanceof byte[])) {
                    throw new java.lang.IllegalArgumentException("HTTP must be a byte array");
                } else {
                    this.mHttpHeader = new byte[((byte[]) headerValue).length];
                    java.lang.System.arraycopy(headerValue, 0, this.mHttpHeader, 0, this.mHttpHeader.length);
                    return;
                }
            case WHO /*74*/:
                if (headerValue == null) {
                    this.mWho = null;
                    return;
                } else if (!(headerValue instanceof byte[])) {
                    throw new java.lang.IllegalArgumentException("WHO must be a byte array");
                } else {
                    this.mWho = new byte[((byte[]) headerValue).length];
                    java.lang.System.arraycopy(headerValue, 0, this.mWho, 0, this.mWho.length);
                    return;
                }
            case APPLICATION_PARAMETER /*76*/:
                if (headerValue == null) {
                    this.mAppParam = null;
                    return;
                } else if (!(headerValue instanceof byte[])) {
                    throw new java.lang.IllegalArgumentException("Application Parameter must be a byte array");
                } else {
                    this.mAppParam = new byte[((byte[]) headerValue).length];
                    java.lang.System.arraycopy(headerValue, 0, this.mAppParam, 0, this.mAppParam.length);
                    return;
                }
            case OBJECT_CLASS /*79*/:
                if (headerValue == null) {
                    this.mObjectClass = null;
                    return;
                } else if (!(headerValue instanceof byte[])) {
                    throw new java.lang.IllegalArgumentException("Object Class must be a byte array");
                } else {
                    this.mObjectClass = new byte[((byte[]) headerValue).length];
                    java.lang.System.arraycopy(headerValue, 0, this.mObjectClass, 0, this.mObjectClass.length);
                    return;
                }
            case SINGLE_RESPONSE_MODE /*151*/:
                if (headerValue == null) {
                    this.mSingleResponseMode = null;
                    return;
                } else if (!(headerValue instanceof java.lang.Byte)) {
                    throw new java.lang.IllegalArgumentException("Single Response Mode must be a Byte");
                } else {
                    this.mSingleResponseMode = (java.lang.Byte) headerValue;
                    return;
                }
            case SINGLE_RESPONSE_MODE_PARAMETER /*152*/:
                if (headerValue == null) {
                    this.mSrmParam = null;
                    return;
                } else if (!(headerValue instanceof java.lang.Byte)) {
                    throw new java.lang.IllegalArgumentException("Single Response Mode Parameter must be a Byte");
                } else {
                    this.mSrmParam = (java.lang.Byte) headerValue;
                    return;
                }
            case 192:
                if (headerValue instanceof java.lang.Long) {
                    long temp = ((java.lang.Long) headerValue).longValue();
                    if (temp < 0 || temp > 4294967295L) {
                        throw new java.lang.IllegalArgumentException("Count must be between 0 and 0xFFFFFFFF");
                    }
                    this.mCount = (java.lang.Long) headerValue;
                    return;
                } else if (headerValue == null) {
                    this.mCount = null;
                    return;
                } else {
                    throw new java.lang.IllegalArgumentException("Count must be a Long");
                }
            case 195:
                if (headerValue instanceof java.lang.Long) {
                    long temp2 = ((java.lang.Long) headerValue).longValue();
                    if (temp2 < 0 || temp2 > 4294967295L) {
                        throw new java.lang.IllegalArgumentException("Length must be between 0 and 0xFFFFFFFF");
                    }
                    this.mLength = (java.lang.Long) headerValue;
                    return;
                } else if (headerValue == null) {
                    this.mLength = null;
                    return;
                } else {
                    throw new java.lang.IllegalArgumentException("Length must be a Long");
                }
            case 196:
                if (headerValue == null || (headerValue instanceof java.util.Calendar)) {
                    this.mByteTime = (java.util.Calendar) headerValue;
                    return;
                }
                throw new java.lang.IllegalArgumentException("Time 4 Byte must be a Calendar");
            default:
                if (headerID < 48 || headerID > 63) {
                    if (headerID < 112 || headerID > 127) {
                        if (headerID < 176 || headerID > 191) {
                            if (headerID < 240 || headerID > 255) {
                                throw new java.lang.IllegalArgumentException("Invalid Header Identifier");
                            } else if (headerValue instanceof java.lang.Long) {
                                long temp3 = ((java.lang.Long) headerValue).longValue();
                                if (temp3 < 0 || temp3 > 4294967295L) {
                                    throw new java.lang.IllegalArgumentException("Integer User Defined must be between 0 and 0xFFFFFFFF");
                                }
                                this.mIntegerUserDefined[headerID - 240] = (java.lang.Long) headerValue;
                                return;
                            } else if (headerValue == null) {
                                this.mIntegerUserDefined[headerID - 240] = null;
                                return;
                            } else {
                                throw new java.lang.IllegalArgumentException("Integer User Defined must be a Long");
                            }
                        } else if (headerValue == null || (headerValue instanceof java.lang.Byte)) {
                            this.mByteUserDefined[headerID - 176] = (java.lang.Byte) headerValue;
                            return;
                        } else {
                            throw new java.lang.IllegalArgumentException("ByteUser Defined must be a Byte");
                        }
                    } else if (headerValue == null) {
                        this.mSequenceUserDefined[headerID - 112] = null;
                        return;
                    } else if (!(headerValue instanceof byte[])) {
                        throw new java.lang.IllegalArgumentException("Byte Sequence User Defined must be a byte array");
                    } else {
                        this.mSequenceUserDefined[headerID - 112] = new byte[((byte[]) headerValue).length];
                        java.lang.System.arraycopy(headerValue, 0, this.mSequenceUserDefined[headerID - 112], 0, this.mSequenceUserDefined[headerID - 112].length);
                        return;
                    }
                } else if (headerValue == null || (headerValue instanceof java.lang.String)) {
                    this.mUnicodeUserDefined[headerID - 48] = (java.lang.String) headerValue;
                    return;
                } else {
                    throw new java.lang.IllegalArgumentException("Unicode String User Defined must be a String");
                }
        }
    }

    public java.lang.Object getHeader(int headerID) throws java.io.IOException {
        switch (headerID) {
            case 1:
                return this.mName;
            case 5:
                return this.mDescription;
            case TYPE /*66*/:
                return this.mType;
            case TIME_ISO_8601 /*68*/:
                return this.mIsoTime;
            case TARGET /*70*/:
                return this.mTarget;
            case HTTP /*71*/:
                return this.mHttpHeader;
            case WHO /*74*/:
                return this.mWho;
            case APPLICATION_PARAMETER /*76*/:
                return this.mAppParam;
            case OBJECT_CLASS /*79*/:
                return this.mObjectClass;
            case SINGLE_RESPONSE_MODE /*151*/:
                return this.mSingleResponseMode;
            case SINGLE_RESPONSE_MODE_PARAMETER /*152*/:
                return this.mSrmParam;
            case 192:
                return this.mCount;
            case 195:
                return this.mLength;
            case 196:
                return this.mByteTime;
            case 203:
                return this.mConnectionID;
            default:
                if (headerID >= 48 && headerID <= 63) {
                    return this.mUnicodeUserDefined[headerID - 48];
                }
                if (headerID >= 112 && headerID <= 127) {
                    return this.mSequenceUserDefined[headerID - 112];
                }
                if (headerID >= 176 && headerID <= 191) {
                    return this.mByteUserDefined[headerID - 176];
                }
                if (headerID >= 240 && headerID <= 255) {
                    return this.mIntegerUserDefined[headerID - 240];
                }
                throw new java.lang.IllegalArgumentException("Invalid Header Identifier");
        }
    }

    public int[] getHeaderList() throws java.io.IOException {
        java.io.ByteArrayOutputStream out = new java.io.ByteArrayOutputStream();
        if (this.mCount != null) {
            out.write(192);
        }
        if (this.mName != null) {
            out.write(1);
        }
        if (this.mType != null) {
            out.write(66);
        }
        if (this.mLength != null) {
            out.write(195);
        }
        if (this.mIsoTime != null) {
            out.write(68);
        }
        if (this.mByteTime != null) {
            out.write(196);
        }
        if (this.mDescription != null) {
            out.write(5);
        }
        if (this.mTarget != null) {
            out.write(70);
        }
        if (this.mHttpHeader != null) {
            out.write(71);
        }
        if (this.mWho != null) {
            out.write(74);
        }
        if (this.mAppParam != null) {
            out.write(76);
        }
        if (this.mObjectClass != null) {
            out.write(79);
        }
        if (this.mSingleResponseMode != null) {
            out.write(SINGLE_RESPONSE_MODE);
        }
        if (this.mSrmParam != null) {
            out.write(SINGLE_RESPONSE_MODE_PARAMETER);
        }
        for (int i = 48; i < 64; i++) {
            if (this.mUnicodeUserDefined[i - 48] != null) {
                out.write(i);
            }
        }
        for (int i2 = 112; i2 < 128; i2++) {
            if (this.mSequenceUserDefined[i2 - 112] != null) {
                out.write(i2);
            }
        }
        for (int i3 = com.navdy.hud.app.bluetooth.obex.ResponseCodes.OBEX_HTTP_MULT_CHOICE; i3 < 192; i3++) {
            if (this.mByteUserDefined[i3 - 176] != null) {
                out.write(i3);
            }
        }
        for (int i4 = com.glympse.android.lib.StaticConfig.PLACE_SEARCH_DISTANCE_FILTER; i4 < 256; i4++) {
            if (this.mIntegerUserDefined[i4 - 240] != null) {
                out.write(i4);
            }
        }
        byte[] headers = out.toByteArray();
        out.close();
        if (headers == null || headers.length == 0) {
            return null;
        }
        int[] result = new int[headers.length];
        for (int i5 = 0; i5 < headers.length; i5++) {
            result[i5] = headers[i5] & com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient.ORDER_BY_DEFAULT;
        }
        return result;
    }

    public void createAuthenticationChallenge(java.lang.String realm, boolean userID, boolean access) throws java.io.IOException {
        this.nonce = new byte[16];
        if (this.mRandom == null) {
            this.mRandom = new java.security.SecureRandom();
        }
        for (int i = 0; i < 16; i++) {
            this.nonce[i] = (byte) this.mRandom.nextInt();
        }
        this.mAuthChall = com.navdy.hud.app.bluetooth.obex.ObexHelper.computeAuthenticationChallenge(this.nonce, realm, access, userID);
    }

    public int getResponseCode() throws java.io.IOException {
        if (this.responseCode != -1) {
            return this.responseCode;
        }
        throw new java.io.IOException("May not be called on a server");
    }
}
