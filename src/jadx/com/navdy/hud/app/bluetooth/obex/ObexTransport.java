package com.navdy.hud.app.bluetooth.obex;

public interface ObexTransport {
    void close() throws java.io.IOException;

    void connect() throws java.io.IOException;

    void create() throws java.io.IOException;

    void disconnect() throws java.io.IOException;

    int getMaxReceivePacketSize();

    int getMaxTransmitPacketSize();

    boolean isSrmSupported();

    void listen() throws java.io.IOException;

    java.io.DataInputStream openDataInputStream() throws java.io.IOException;

    java.io.DataOutputStream openDataOutputStream() throws java.io.IOException;

    java.io.InputStream openInputStream() throws java.io.IOException;

    java.io.OutputStream openOutputStream() throws java.io.IOException;
}
