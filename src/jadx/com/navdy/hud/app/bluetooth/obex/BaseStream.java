package com.navdy.hud.app.bluetooth.obex;

public interface BaseStream {
    boolean continueOperation(boolean z, boolean z2) throws java.io.IOException;

    void ensureNotDone() throws java.io.IOException;

    void ensureOpen() throws java.io.IOException;

    void streamClosed(boolean z) throws java.io.IOException;
}
