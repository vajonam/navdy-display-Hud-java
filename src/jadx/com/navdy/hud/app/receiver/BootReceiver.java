package com.navdy.hud.app.receiver;

public class BootReceiver extends android.content.BroadcastReceiver {
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.receiver.BootReceiver.class);

    public void onReceive(android.content.Context context, android.content.Intent intent) {
        boolean startOnBoot = context.getSharedPreferences(com.navdy.hud.app.common.ProdModule.PREF_NAME, 0).getBoolean(com.navdy.hud.app.debug.SettingsActivity.START_ON_BOOT_KEY, true);
        sLogger.d("Start on boot:" + startOnBoot);
        if (startOnBoot) {
            android.content.Intent myIntent = new android.content.Intent(context, com.navdy.hud.app.ui.activity.MainActivity.class);
            myIntent.addFlags(268435456);
            context.startActivity(myIntent);
        }
    }
}
