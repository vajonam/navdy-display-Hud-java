package com.navdy.hud.app.receiver;

public class LogLevelReceiver extends android.content.BroadcastReceiver {
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.receiver.LogLevelReceiver.class);

    public void onReceive(android.content.Context context, android.content.Intent intent) {
        try {
            if (intent.getAction().equals(com.navdy.service.library.log.Logger.ACTION_RELOAD)) {
                sLogger.i("Forcing loggers to reload cached log levels");
                com.navdy.service.library.log.Logger.reloadLogLevels();
            }
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }
}
