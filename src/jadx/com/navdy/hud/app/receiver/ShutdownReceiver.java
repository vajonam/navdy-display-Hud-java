package com.navdy.hud.app.receiver;

public class ShutdownReceiver extends android.content.BroadcastReceiver {
    public void onReceive(android.content.Context context, android.content.Intent intent) {
        com.navdy.hud.app.HudApplication.getApplication().shutdown();
    }
}
