package com.navdy.hud.app.settings;

public class HUDSettings {
    private static final java.lang.String ADAPTIVE_AUTOBRIGHTNESS_PROP = "persist.sys.autobright_adaptive";
    public static final java.lang.String AUTO_BRIGHTNESS = "screen.auto_brightness";
    public static final java.lang.String AUTO_BRIGHTNESS_ADJUSTMENT = "screen.auto_brightness_adj";
    public static final java.lang.String BRIGHTNESS = "screen.brightness";
    public static final float BRIGHTNESS_SCALE = 255.0f;
    public static final java.lang.String GESTURE_ENGINE = "gesture.engine";
    public static final java.lang.String GESTURE_PREVIEW = "gesture.preview";
    public static final java.lang.String LED_BRIGHTNESS = "screen.led_brightness";
    public static final java.lang.String MAP_ANIMATION_MODE = "map.animation.mode";
    public static final java.lang.String MAP_SCHEME = "map.scheme";
    public static final java.lang.String MAP_TILT = "map.tilt";
    public static final java.lang.String MAP_ZOOM = "map.zoom";
    public static final boolean USE_ADAPTIVE_AUTOBRIGHTNESS = com.navdy.hud.app.util.os.SystemProperties.getBoolean(ADAPTIVE_AUTOBRIGHTNESS_PROP, true);
    private static com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.settings.HUDSettings.class);
    private android.content.SharedPreferences preferences;

    public HUDSettings(android.content.SharedPreferences preferences2) {
        this.preferences = preferences2;
    }

    public java.util.List<java.lang.String> availableSettings() {
        java.util.List<java.lang.String> result = new java.util.ArrayList<>();
        result.addAll(this.preferences.getAll().keySet());
        return result;
    }

    public void updateSettings(java.util.List<com.navdy.service.library.events.settings.Setting> newSettings) {
        android.content.SharedPreferences.Editor editor = this.preferences.edit();
        for (int i = 0; i < newSettings.size(); i++) {
            com.navdy.service.library.events.settings.Setting newSetting = (com.navdy.service.library.events.settings.Setting) newSettings.get(i);
            if (newSetting.value.equals("-1")) {
                java.lang.String str = newSetting.key;
                char c = 65535;
                switch (str.hashCode()) {
                    case 133816335:
                        if (str.equals(MAP_TILT)) {
                            c = 0;
                            break;
                        }
                        break;
                    case 134000933:
                        if (str.equals(MAP_ZOOM)) {
                            c = 1;
                            break;
                        }
                        break;
                }
                switch (c) {
                    case 0:
                        set(editor, newSetting.key, java.lang.String.valueOf(60.0f));
                        break;
                    case 1:
                        set(editor, newSetting.key, java.lang.String.valueOf(16.5f));
                        break;
                }
            } else {
                set(editor, newSetting.key, newSetting.value);
            }
        }
        editor.apply();
    }

    public java.util.List<com.navdy.service.library.events.settings.Setting> readSettings(java.util.List<java.lang.String> keys) {
        java.util.List<com.navdy.service.library.events.settings.Setting> result = new java.util.ArrayList<>();
        java.util.Map<java.lang.String, ?> allPreferences = this.preferences.getAll();
        for (int i = 0; i < keys.size(); i++) {
            java.lang.String key = (java.lang.String) keys.get(i);
            java.lang.Object object = allPreferences.get(key);
            if (object != null) {
                result.add(new com.navdy.service.library.events.settings.Setting(key, object.toString()));
            }
        }
        return result;
    }

    private java.lang.Object get(java.lang.String key) {
        return this.preferences.getAll().get(key);
    }

    private void set(android.content.SharedPreferences.Editor editor, java.lang.String key, java.lang.String value) {
        java.lang.Object oldValue = get(key);
        if (oldValue instanceof java.lang.String) {
            editor.putString(key, value);
        } else if (oldValue instanceof java.lang.Boolean) {
            editor.putBoolean(key, java.lang.Boolean.valueOf(value).booleanValue());
        }
    }
}
