package com.navdy.hud.app.settings;

public class MainScreenSettings implements android.os.Parcelable {
    public static final android.os.Parcelable.Creator<com.navdy.hud.app.settings.MainScreenSettings> CREATOR = new com.navdy.hud.app.settings.MainScreenSettings.Anon1();
    protected com.navdy.hud.app.settings.MainScreenSettings.ScreenConfiguration landscapeConfiguration;
    protected com.navdy.hud.app.settings.MainScreenSettings.ScreenConfiguration portraitConfiguration;

    static class Anon1 implements android.os.Parcelable.Creator<com.navdy.hud.app.settings.MainScreenSettings> {
        Anon1() {
        }

        public com.navdy.hud.app.settings.MainScreenSettings createFromParcel(android.os.Parcel source) {
            return new com.navdy.hud.app.settings.MainScreenSettings(source);
        }

        public com.navdy.hud.app.settings.MainScreenSettings[] newArray(int size) {
            return new com.navdy.hud.app.settings.MainScreenSettings[size];
        }
    }

    public static class ScreenConfiguration implements android.os.Parcelable {
        public static final android.os.Parcelable.Creator<com.navdy.hud.app.settings.MainScreenSettings.ScreenConfiguration> CREATOR = new com.navdy.hud.app.settings.MainScreenSettings.ScreenConfiguration.Anon1();
        protected android.graphics.Rect margins;
        protected float scaleX;
        protected float scaleY;

        static class Anon1 implements android.os.Parcelable.Creator<com.navdy.hud.app.settings.MainScreenSettings.ScreenConfiguration> {
            Anon1() {
            }

            public com.navdy.hud.app.settings.MainScreenSettings.ScreenConfiguration createFromParcel(android.os.Parcel source) {
                return new com.navdy.hud.app.settings.MainScreenSettings.ScreenConfiguration(source);
            }

            public com.navdy.hud.app.settings.MainScreenSettings.ScreenConfiguration[] newArray(int size) {
                return new com.navdy.hud.app.settings.MainScreenSettings.ScreenConfiguration[size];
            }
        }

        public ScreenConfiguration() {
            this(new android.graphics.Rect(), 1.0f, 1.0f);
        }

        public ScreenConfiguration(android.graphics.Rect margins2, float scaleX2, float scaleY2) {
            this.margins = margins2;
            this.scaleX = scaleX2;
            this.scaleY = scaleY2;
        }

        public ScreenConfiguration(android.os.Parcel in) {
            this((android.graphics.Rect) in.readParcelable(com.navdy.hud.app.settings.MainScreenSettings.ScreenConfiguration.class.getClassLoader()), in.readFloat(), in.readFloat());
        }

        public android.graphics.Rect getMargins() {
            return this.margins;
        }

        public void setMargins(android.graphics.Rect margins2) {
            this.margins = margins2;
        }

        public float getScaleX() {
            return this.scaleX;
        }

        public void setScaleX(float scaleX2) {
            this.scaleX = scaleX2;
        }

        public float getScaleY() {
            return this.scaleY;
        }

        public void setScaleY(float scaleY2) {
            this.scaleY = scaleY2;
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(android.os.Parcel dest, int flags) {
            dest.writeParcelable(this.margins, 0);
            dest.writeFloat(this.scaleX);
            dest.writeFloat(this.scaleY);
        }
    }

    public MainScreenSettings() {
        this(new com.navdy.hud.app.settings.MainScreenSettings.ScreenConfiguration(), new com.navdy.hud.app.settings.MainScreenSettings.ScreenConfiguration());
    }

    public MainScreenSettings(com.navdy.hud.app.settings.MainScreenSettings.ScreenConfiguration portraitConfiguration2, com.navdy.hud.app.settings.MainScreenSettings.ScreenConfiguration landscapeConfiguration2) {
        this.portraitConfiguration = portraitConfiguration2;
        this.landscapeConfiguration = landscapeConfiguration2;
    }

    public MainScreenSettings(android.os.Parcel in) {
        this((com.navdy.hud.app.settings.MainScreenSettings.ScreenConfiguration) in.readParcelable(com.navdy.hud.app.settings.MainScreenSettings.class.getClassLoader()), (com.navdy.hud.app.settings.MainScreenSettings.ScreenConfiguration) in.readParcelable(com.navdy.hud.app.settings.MainScreenSettings.class.getClassLoader()));
    }

    public com.navdy.hud.app.settings.MainScreenSettings.ScreenConfiguration getPortraitConfiguration() {
        return this.portraitConfiguration;
    }

    public void setPortraitConfiguration(com.navdy.hud.app.settings.MainScreenSettings.ScreenConfiguration portraitConfiguration2) {
        this.portraitConfiguration = portraitConfiguration2;
    }

    public com.navdy.hud.app.settings.MainScreenSettings.ScreenConfiguration getLandscapeConfiguration() {
        return this.landscapeConfiguration;
    }

    public void setLandscapeConfiguration(com.navdy.hud.app.settings.MainScreenSettings.ScreenConfiguration landscapeConfiguration2) {
        this.landscapeConfiguration = landscapeConfiguration2;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeParcelable(this.portraitConfiguration, 0);
        dest.writeParcelable(this.landscapeConfiguration, 0);
    }
}
