package com.navdy.hud.app.settings;

public class AdaptiveBrightnessControl implements android.content.SharedPreferences.OnSharedPreferenceChangeListener {
    public static final java.lang.String AUTOBRIGHTNESSD_PROPERTY = "hw.navdy.autobrightnessd";
    public static final java.lang.String AUTO_BRIGHTNESS_PROPERTY = "persist.sys.autobrightness";
    public static final java.lang.String DEFAULT_AUTO_BRIGHTNESS = "false";
    public static final java.lang.String DEFAULT_AUTO_BRIGHTNESS_ADJUSTMENT = "0";
    public static final java.lang.String DEFAULT_BRIGHTNESS = "128";
    public static final java.lang.String DEFAULT_LED_BRIGHTNESS = "255";
    public static final java.lang.String DISABLED = "disabled";
    public static final java.lang.String ENABLED = "enabled";
    private static final int MIN_INITIAL_BRIGHTNESS = 128;
    public static final java.lang.String OFF = "off";
    public static final java.lang.String ON = "on";
    private static com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.settings.AdaptiveBrightnessControl.class);
    private java.lang.String autoBrightnessAdjustmentKey;
    private java.lang.String autoBrightnessKey;
    private java.lang.String brightnessKey;
    private com.squareup.otto.Bus bus;
    /* access modifiers changed from: private */
    public android.content.Context context;
    /* access modifiers changed from: private */
    public float lastBrightness = 0.5f;
    private java.lang.String ledBrightnessKey;
    /* access modifiers changed from: private */
    public android.os.Handler mainHandler;
    private android.content.SharedPreferences preferences;

    class Anon1 implements java.lang.Runnable {
        final /* synthetic */ int val$val;

        Anon1(int i) {
            this.val$val = i;
        }

        public void run() {
            android.provider.Settings.System.putInt(com.navdy.hud.app.settings.AdaptiveBrightnessControl.this.context.getContentResolver(), "screen_brightness", this.val$val);
        }
    }

    class Anon2 implements java.lang.Runnable {
        final /* synthetic */ boolean val$autoBrightness;

        class Anon1 implements java.lang.Runnable {
            Anon1() {
            }

            public void run() {
                float access$Anon100;
                com.navdy.hud.app.settings.AdaptiveBrightnessControl adaptiveBrightnessControl = com.navdy.hud.app.settings.AdaptiveBrightnessControl.this;
                if (com.navdy.hud.app.settings.AdaptiveBrightnessControl.Anon2.this.val$autoBrightness) {
                    access$Anon100 = -1.0f;
                } else {
                    access$Anon100 = com.navdy.hud.app.settings.AdaptiveBrightnessControl.this.lastBrightness;
                }
                adaptiveBrightnessControl.updateWindowBrightness(access$Anon100);
            }
        }

        Anon2(boolean z) {
            this.val$autoBrightness = z;
        }

        public void run() {
            android.provider.Settings.System.putInt(com.navdy.hud.app.settings.AdaptiveBrightnessControl.this.context.getContentResolver(), "screen_brightness_mode", this.val$autoBrightness ? 1 : 0);
            com.navdy.hud.app.settings.AdaptiveBrightnessControl.this.mainHandler.post(new com.navdy.hud.app.settings.AdaptiveBrightnessControl.Anon2.Anon1());
        }
    }

    class Anon3 implements java.lang.Runnable {
        final /* synthetic */ int val$val;

        Anon3(int i) {
            this.val$val = i;
        }

        public void run() {
            float normalizedBrightnessAdjustment = ((float) this.val$val) / 255.0f;
            android.util.Log.d(com.navdy.hud.app.debug.DebugReceiver.EXTRA_TEST, "Normalized : " + normalizedBrightnessAdjustment);
            android.provider.Settings.System.putFloat(com.navdy.hud.app.settings.AdaptiveBrightnessControl.this.context.getContentResolver(), "screen_auto_brightness_adj", normalizedBrightnessAdjustment);
        }
    }

    public AdaptiveBrightnessControl(android.content.Context context2, com.squareup.otto.Bus bus2, android.content.SharedPreferences preferences2, java.lang.String brightnessKey2, java.lang.String autoBrightnessKey2, java.lang.String autoBrightnessAdjustmentKey2, java.lang.String ledBrightnessKey2) {
        this.context = context2;
        this.bus = bus2;
        bus2.register(this);
        this.preferences = preferences2;
        this.mainHandler = new android.os.Handler(context2.getMainLooper());
        preferences2.registerOnSharedPreferenceChangeListener(this);
        this.brightnessKey = brightnessKey2;
        this.autoBrightnessKey = autoBrightnessKey2;
        this.autoBrightnessAdjustmentKey = autoBrightnessAdjustmentKey2;
        this.ledBrightnessKey = ledBrightnessKey2;
        com.navdy.hud.app.util.os.SystemProperties.set("hw.navdy.autobrightnessd", "0");
        boolean autoBrightnessEnabled = getAutoBrightnessProperty();
        preferences2.edit().putString(autoBrightnessKey2, autoBrightnessEnabled ? "true" : "false").apply();
        if (!autoBrightnessEnabled) {
            if (getBrightnessPreference() < 128) {
                preferences2.edit().putString(brightnessKey2, java.lang.String.valueOf(128)).apply();
            }
            onSharedPreferenceChanged(preferences2, this.brightnessKey);
        }
        onSharedPreferenceChanged(preferences2, autoBrightnessAdjustmentKey2);
        onSharedPreferenceChanged(preferences2, autoBrightnessKey2);
        onSharedPreferenceChanged(preferences2, ledBrightnessKey2);
    }

    public java.lang.String getValue() {
        return java.lang.Integer.toString(android.provider.Settings.System.getInt(this.context.getContentResolver(), "screen_brightness", -1));
    }

    public void setBrightnessValue(int val) {
        if (val >= 0 && val <= 255) {
            sLogger.d("setting brightness to " + val);
            this.lastBrightness = ((float) val) / 255.0f;
            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.settings.AdaptiveBrightnessControl.Anon1(val), 1);
            updateWindowBrightness(this.lastBrightness);
        }
    }

    public void setLEDValue(java.lang.String value) {
        int val = java.lang.Integer.parseInt(value);
        if (val >= 0 && val <= 255) {
            com.navdy.hud.app.util.os.SystemProperties.set("hw.navdy.led_max_brightness", java.lang.String.format("%f", new java.lang.Object[]{java.lang.Float.valueOf(((float) val) / 255.0f)}));
        }
    }

    public void toggleAutoBrightness(boolean autoBrightness) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.settings.AdaptiveBrightnessControl.Anon2(autoBrightness), 1);
    }

    public void onSharedPreferenceChanged(android.content.SharedPreferences sharedPreferences, java.lang.String key) {
        if (key.equals(this.brightnessKey)) {
            setBrightnessValue(getBrightnessPreference());
        } else if (key.equals(this.autoBrightnessAdjustmentKey)) {
            setAutoBrightnessAdjustment(getAutoBrightnessAdjustmentPreference());
        } else if (key.equals(this.autoBrightnessKey)) {
            toggleAutoBrightness(java.lang.Boolean.valueOf(sharedPreferences.getString(key, "false")).booleanValue());
        } else if (key.equals(this.ledBrightnessKey)) {
            setLEDValue(sharedPreferences.getString(key, "255"));
        }
    }

    private final boolean getAutoBrightnessProperty() {
        java.lang.String autoBrightness = com.navdy.hud.app.util.os.SystemProperties.get("persist.sys.autobrightness", "on");
        return autoBrightness.equals("on") || autoBrightness.equals("enabled");
    }

    private void setAutoBrightnessAdjustment(int val) {
        if (val >= 0 && ((float) val) <= 255.0f) {
            java.lang.String str = "screen_auto_brightness_adj";
            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.settings.AdaptiveBrightnessControl.Anon3(val), 1);
        }
    }

    /* access modifiers changed from: private */
    public void updateWindowBrightness(float brightness) {
        android.view.WindowManager.LayoutParams layout = ((android.app.Activity) this.context).getWindow().getAttributes();
        layout.screenBrightness = brightness;
        ((android.app.Activity) this.context).getWindow().setAttributes(layout);
    }

    private int getBrightnessPreference() {
        return java.lang.Integer.parseInt(this.preferences.getString(this.brightnessKey, "128"));
    }

    private int getAutoBrightnessAdjustmentPreference() {
        return java.lang.Integer.parseInt(this.preferences.getString(this.autoBrightnessAdjustmentKey, "0"));
    }
}
