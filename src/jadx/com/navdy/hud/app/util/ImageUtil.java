package com.navdy.hud.app.util;

public class ImageUtil {
    public static android.graphics.Bitmap hueShift(android.graphics.Bitmap bitmap, float hue) {
        android.graphics.Bitmap newBitmap = android.graphics.Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), bitmap.getConfig());
        android.graphics.Canvas canvasResult = new android.graphics.Canvas(newBitmap);
        android.graphics.Paint paint = new android.graphics.Paint();
        android.graphics.ColorMatrix colorMatrix = new android.graphics.ColorMatrix();
        float value = (java.lang.Math.min(hue, java.lang.Math.max(-hue, 180.0f)) / 180.0f) * 3.1415927f;
        if (value == 0.0f) {
            return null;
        }
        float cosVal = (float) java.lang.Math.cos((double) value);
        float sinVal = (float) java.lang.Math.sin((double) value);
        colorMatrix.postConcat(new android.graphics.ColorMatrix(new float[]{((1.0f - 0.213f) * cosVal) + 0.213f + ((-0.213f) * sinVal), ((-0.715f) * cosVal) + 0.715f + ((-0.715f) * sinVal), ((-0.072f) * cosVal) + 0.072f + ((1.0f - 0.072f) * sinVal), 0.0f, 0.0f, ((-0.213f) * cosVal) + 0.213f + (0.143f * sinVal), ((1.0f - 0.715f) * cosVal) + 0.715f + (0.14f * sinVal), ((-0.072f) * cosVal) + 0.072f + (-0.283f * sinVal), 0.0f, 0.0f, ((-0.213f) * cosVal) + 0.213f + ((-(1.0f - 0.213f)) * sinVal), ((-0.715f) * cosVal) + 0.715f + (sinVal * 0.715f), ((1.0f - 0.072f) * cosVal) + 0.072f + (sinVal * 0.072f), 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f}));
        paint.setColorFilter(new android.graphics.ColorMatrixColorFilter(colorMatrix));
        canvasResult.drawBitmap(bitmap, 0.0f, 0.0f, paint);
        return newBitmap;
    }

    public static android.graphics.Bitmap applySaturation(android.graphics.Bitmap bitmap, float saturation) {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        android.graphics.Bitmap newBitmap = android.graphics.Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), bitmap.getConfig());
        android.graphics.Canvas canvasResult = new android.graphics.Canvas(newBitmap);
        android.graphics.Paint paint = new android.graphics.Paint();
        android.graphics.ColorMatrix colorMatrix = new android.graphics.ColorMatrix();
        colorMatrix.setSaturation(saturation);
        paint.setColorFilter(new android.graphics.ColorMatrixColorFilter(colorMatrix));
        canvasResult.drawBitmap(bitmap, 0.0f, 0.0f, paint);
        return newBitmap;
    }

    public static android.graphics.Bitmap blend(android.graphics.Bitmap base, android.graphics.Bitmap blend) {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        android.graphics.Bitmap newBitmap = base.copy(base.getConfig(), true);
        android.graphics.Paint p = new android.graphics.Paint();
        p.setXfermode(new android.graphics.PorterDuffXfermode(android.graphics.PorterDuff.Mode.MULTIPLY));
        new android.graphics.Canvas(newBitmap).drawBitmap(blend, 0.0f, 0.0f, p);
        return newBitmap;
    }
}
