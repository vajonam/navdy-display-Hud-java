package com.navdy.hud.app.util;

public final class FeatureUtil {
    public static final java.lang.String FEATURE_ACTION = "com.navdy.hud.app.feature";
    public static final java.lang.String FEATURE_FUEL_ROUTING = "com.navdy.hud.app.feature.fuelRouting";
    public static final java.lang.String FEATURE_GESTURE_COLLECTOR = "com.navdy.hud.app.feature.gestureCollector";
    public static final java.lang.String FEATURE_GESTURE_ENGINE = "com.navdy.hud.app.feature.gestureEngine";
    public static final java.lang.String FEATURE_VOICE_SEARCH_ADDITIONAL_RESULTS_PROPERTY = "persist.voice.search.additional.results";
    public static final java.lang.String FEATURE_VOICE_SEARCH_LIST_PROPERTY = "persist.sys.voicesearch.list";
    private static final java.lang.String GESTURE_CALIBRATED_PROPERTY = "persist.sys.swiped.calib";
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.util.FeatureUtil.class);
    private java.util.HashSet<com.navdy.hud.app.util.FeatureUtil.Feature> featuresEnabledMap = new java.util.HashSet<>();
    private android.content.BroadcastReceiver receiver = new com.navdy.hud.app.util.FeatureUtil.Anon1();
    /* access modifiers changed from: private */
    public android.content.SharedPreferences sharedPreferences;

    class Anon1 extends android.content.BroadcastReceiver {

        /* renamed from: com.navdy.hud.app.util.FeatureUtil$Anon1$Anon1 reason: collision with other inner class name */
        class C0035Anon1 implements java.lang.Runnable {
            final /* synthetic */ android.os.Bundle val$bundle;

            C0035Anon1(android.os.Bundle bundle) {
                this.val$bundle = bundle;
            }

            public void run() {
                try {
                    java.lang.String featureName = (java.lang.String) this.val$bundle.keySet().iterator().next();
                    boolean state = this.val$bundle.getBoolean(featureName);
                    com.navdy.hud.app.util.FeatureUtil.Feature feature = com.navdy.hud.app.util.FeatureUtil.this.getFeatureFromName(featureName);
                    if (feature == null) {
                        com.navdy.hud.app.util.FeatureUtil.sLogger.i("invalid feature:" + featureName);
                        return;
                    }
                    if (feature == com.navdy.hud.app.util.FeatureUtil.Feature.GESTURE_ENGINE) {
                        com.navdy.hud.app.util.FeatureUtil.this.sharedPreferences.edit().putBoolean(com.navdy.hud.app.settings.HUDSettings.GESTURE_ENGINE, state).apply();
                        com.navdy.hud.app.util.FeatureUtil.sLogger.v("gesture engine setting set to " + state);
                        if (!com.navdy.hud.app.util.DeviceUtil.supportsCamera()) {
                            com.navdy.hud.app.util.FeatureUtil.sLogger.v("gesture engine cannot be enabled/disabled, no camera");
                            return;
                        }
                    } else if (feature == com.navdy.hud.app.util.FeatureUtil.Feature.GESTURE_COLLECTOR) {
                        com.navdy.hud.app.util.FeatureUtil.sLogger.v("gesture collector setting set to " + state);
                        if (!com.navdy.hud.app.util.DeviceUtil.supportsCamera()) {
                            com.navdy.hud.app.util.FeatureUtil.sLogger.v("gesture collector cannot be enabled/disabled, no camera");
                            return;
                        } else if (!com.navdy.hud.app.util.FeatureUtil.this.isGestureCollectorInstalled()) {
                            return;
                        }
                    }
                    com.navdy.hud.app.util.FeatureUtil.this.setFeature(feature, state);
                } catch (Throwable t) {
                    com.navdy.hud.app.util.FeatureUtil.sLogger.e(t);
                }
            }
        }

        Anon1() {
        }

        public void onReceive(android.content.Context context, android.content.Intent intent) {
            try {
                if (com.navdy.hud.app.util.FeatureUtil.FEATURE_ACTION.equalsIgnoreCase(intent.getAction())) {
                    android.os.Bundle bundle = intent.getExtras();
                    if (bundle != null && bundle.size() == 1) {
                        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.util.FeatureUtil.Anon1.C0035Anon1(bundle), 1);
                    }
                }
            } catch (Throwable t) {
                com.navdy.hud.app.util.FeatureUtil.sLogger.e(t);
            }
        }
    }

    public enum Feature {
        GESTURE_ENGINE,
        GESTURE_COLLECTOR,
        FUEL_ROUTING,
        GESTURE_PROGRESS
    }

    public FeatureUtil(android.content.SharedPreferences sharedPreferences2) {
        boolean calibrated;
        boolean enabled;
        this.sharedPreferences = sharedPreferences2;
        if (!android.text.TextUtils.isEmpty(com.navdy.hud.app.util.os.SystemProperties.get(GESTURE_CALIBRATED_PROPERTY, ""))) {
            calibrated = true;
        } else {
            calibrated = false;
        }
        if (sharedPreferences2.getBoolean(com.navdy.hud.app.settings.HUDSettings.GESTURE_ENGINE, true) || calibrated) {
            enabled = true;
        } else {
            enabled = false;
        }
        boolean z = com.navdy.hud.app.util.os.SystemProperties.getBoolean(FEATURE_VOICE_SEARCH_LIST_PROPERTY, false);
        boolean z2 = com.navdy.hud.app.util.os.SystemProperties.getBoolean(FEATURE_VOICE_SEARCH_ADDITIONAL_RESULTS_PROPERTY, true);
        if (enabled) {
            this.featuresEnabledMap.add(com.navdy.hud.app.util.FeatureUtil.Feature.GESTURE_ENGINE);
            sLogger.v("enabled gesture engine");
            if (isGestureCollectorInstalled()) {
                this.featuresEnabledMap.add(com.navdy.hud.app.util.FeatureUtil.Feature.GESTURE_COLLECTOR);
                sLogger.v("enabled gesture collector");
            }
        } else {
            sLogger.v("not enabled gesture engine");
        }
        this.featuresEnabledMap.add(com.navdy.hud.app.util.FeatureUtil.Feature.FUEL_ROUTING);
        com.navdy.hud.app.HudApplication.getAppContext().registerReceiver(this.receiver, new android.content.IntentFilter(FEATURE_ACTION));
    }

    public synchronized boolean isFeatureEnabled(com.navdy.hud.app.util.FeatureUtil.Feature feature) {
        return this.featuresEnabledMap.contains(feature);
    }

    /* access modifiers changed from: private */
    public synchronized void setFeature(com.navdy.hud.app.util.FeatureUtil.Feature feature, boolean enable) {
        if (enable) {
            this.featuresEnabledMap.add(feature);
            sLogger.v("enabled featured:" + feature);
        } else {
            this.featuresEnabledMap.remove(feature);
            sLogger.v("disabled featured:" + feature);
        }
    }

    /* access modifiers changed from: private */
    public com.navdy.hud.app.util.FeatureUtil.Feature getFeatureFromName(java.lang.String name) {
        if (name == null) {
            return null;
        }
        char c = 65535;
        switch (name.hashCode()) {
            case -1256323375:
                if (name.equals(FEATURE_FUEL_ROUTING)) {
                    c = 2;
                    break;
                }
                break;
            case -1149811389:
                if (name.equals(FEATURE_GESTURE_COLLECTOR)) {
                    c = 0;
                    break;
                }
                break;
            case -23581364:
                if (name.equals(FEATURE_GESTURE_ENGINE)) {
                    c = 1;
                    break;
                }
                break;
        }
        switch (c) {
            case 0:
                return com.navdy.hud.app.util.FeatureUtil.Feature.GESTURE_COLLECTOR;
            case 1:
                return com.navdy.hud.app.util.FeatureUtil.Feature.GESTURE_ENGINE;
            case 2:
                return com.navdy.hud.app.util.FeatureUtil.Feature.FUEL_ROUTING;
            default:
                return null;
        }
    }

    /* access modifiers changed from: private */
    public boolean isGestureCollectorInstalled() {
        try {
            com.navdy.hud.app.HudApplication.getAppContext().getPackageManager().getPackageInfo("com.navdy.collector", 1);
            return true;
        } catch (Throwable e) {
            if (!(e instanceof android.content.pm.PackageManager.NameNotFoundException)) {
                sLogger.v("gesture collector not enabled", e);
            } else {
                sLogger.v("gesture collector not found");
            }
            return false;
        }
    }
}
