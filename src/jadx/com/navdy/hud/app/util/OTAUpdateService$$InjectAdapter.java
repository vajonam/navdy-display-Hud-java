package com.navdy.hud.app.util;

public final class OTAUpdateService$$InjectAdapter extends dagger.internal.Binding<com.navdy.hud.app.util.OTAUpdateService> implements javax.inject.Provider<com.navdy.hud.app.util.OTAUpdateService>, dagger.MembersInjector<com.navdy.hud.app.util.OTAUpdateService> {
    private dagger.internal.Binding<com.squareup.otto.Bus> bus;
    private dagger.internal.Binding<android.content.SharedPreferences> sharedPreferences;

    public OTAUpdateService$$InjectAdapter() {
        super("com.navdy.hud.app.util.OTAUpdateService", "members/com.navdy.hud.app.util.OTAUpdateService", false, com.navdy.hud.app.util.OTAUpdateService.class);
    }

    public void attach(dagger.internal.Linker linker) {
        this.sharedPreferences = linker.requestBinding("android.content.SharedPreferences", com.navdy.hud.app.util.OTAUpdateService.class, getClass().getClassLoader());
        this.bus = linker.requestBinding("com.squareup.otto.Bus", com.navdy.hud.app.util.OTAUpdateService.class, getClass().getClassLoader());
    }

    public void getDependencies(java.util.Set<dagger.internal.Binding<?>> set, java.util.Set<dagger.internal.Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.sharedPreferences);
        injectMembersBindings.add(this.bus);
    }

    public com.navdy.hud.app.util.OTAUpdateService get() {
        com.navdy.hud.app.util.OTAUpdateService result = new com.navdy.hud.app.util.OTAUpdateService();
        injectMembers(result);
        return result;
    }

    public void injectMembers(com.navdy.hud.app.util.OTAUpdateService object) {
        object.sharedPreferences = (android.content.SharedPreferences) this.sharedPreferences.get();
        object.bus = (com.squareup.otto.Bus) this.bus.get();
    }
}
