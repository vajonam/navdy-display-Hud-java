package com.navdy.hud.app.util;

public class DrawableLocator {
    private android.graphics.drawable.Drawable mDrawable;
    private android.view.View mParent;
    private int mResourceId = 0;

    public DrawableLocator(android.view.View parent, android.content.res.TypedArray array, int index, int defaultId) {
        this.mParent = parent;
        if (parent.isInEditMode()) {
            this.mDrawable = array.getDrawable(index);
            return;
        }
        this.mResourceId = array.getResourceId(index, -1);
        if (this.mResourceId == -1) {
            this.mResourceId = defaultId;
        }
    }

    public boolean isSet() {
        return (this.mDrawable == null && this.mResourceId == 0) ? false : true;
    }

    public android.graphics.Bitmap getBitmap() {
        if (this.mDrawable != null) {
            return drawableToBitmap(this.mDrawable);
        }
        if (this.mResourceId == 0) {
            return null;
        }
        return android.graphics.BitmapFactory.decodeResource(this.mParent.getResources(), this.mResourceId);
    }

    public static android.graphics.Bitmap drawableToBitmap(android.graphics.drawable.Drawable drawable) {
        if (drawable instanceof android.graphics.drawable.BitmapDrawable) {
            return ((android.graphics.drawable.BitmapDrawable) drawable).getBitmap();
        }
        android.graphics.Bitmap bitmap = android.graphics.Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), android.graphics.Bitmap.Config.ARGB_8888);
        android.graphics.Canvas canvas = new android.graphics.Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }
}
