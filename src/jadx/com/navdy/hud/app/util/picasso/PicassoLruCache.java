package com.navdy.hud.app.util.picasso;

public class PicassoLruCache extends com.squareup.picasso.LruCache implements com.navdy.hud.app.util.picasso.PicassoItemCacheListener<java.lang.String> {
    private java.util.HashMap<java.lang.String, java.lang.String> keyMap = new java.util.HashMap<>();

    public PicassoLruCache(int maxSize) {
        super(maxSize);
    }

    public void init() throws java.lang.Exception {
        com.navdy.hud.app.util.picasso.PicassoCacheMap<java.lang.String, android.graphics.Bitmap> map = new com.navdy.hud.app.util.picasso.PicassoCacheMap<>(0, 0.75f);
        map.setListener(this);
        java.lang.reflect.Field f = com.squareup.picasso.LruCache.class.getDeclaredField("map");
        f.setAccessible(true);
        f.set(this, map);
    }

    public synchronized void itemAdded(java.lang.String key) {
        if (key != null) {
            this.keyMap.put(getKey(key), key);
        }
    }

    public synchronized void itemRemoved(java.lang.String key) {
        if (key != null) {
            this.keyMap.remove(getKey(key));
        }
    }

    private java.lang.String getKey(java.lang.String key) {
        if (key == null) {
            return null;
        }
        int index = key.indexOf(com.navdy.hud.app.framework.glance.GlanceConstants.NEWLINE);
        if (index != -1) {
            return key.substring(0, index);
        }
        return key;
    }

    public synchronized android.graphics.Bitmap getBitmap(java.lang.String key) {
        android.graphics.Bitmap bitmap = null;
        synchronized (this) {
            if (key != null) {
                java.lang.String mappedKey = (java.lang.String) this.keyMap.get(key);
                if (mappedKey != null) {
                    bitmap = super.get(mappedKey);
                }
            }
        }
        return bitmap;
    }

    public synchronized void setBitmap(java.lang.String key, android.graphics.Bitmap bitmap) {
        if (!(key == null || bitmap == null)) {
            this.keyMap.put(getKey(key), key);
            super.set(key, bitmap);
        }
    }
}
