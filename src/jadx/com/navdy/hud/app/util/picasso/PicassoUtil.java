package com.navdy.hud.app.util.picasso;

public class PicassoUtil {
    private static final java.lang.String DISK_CACHE_SCHEME = "diskcache://";
    private static final java.lang.String DISK_CACHE_SCHEME_NAME = "diskcache";
    private static final int DISK_CACHE_SIZE = 10485760;
    public static final int IMAGE_MEMORY_CACHE = 8388608;
    /* access modifiers changed from: private */
    public static com.navdy.hud.app.storage.cache.DiskLruCache diskLruCache;
    private static volatile boolean initialized;
    private static java.lang.Object lockObj = new java.lang.Object();
    private static com.navdy.hud.app.util.picasso.PicassoLruCache lruCache;
    private static com.squareup.picasso.Picasso picasso;
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.util.picasso.PicassoUtil.class);

    static class Anon1 extends com.squareup.picasso.RequestHandler {
        Anon1() {
        }

        public boolean canHandleRequest(com.squareup.picasso.Request data) {
            return com.navdy.hud.app.util.picasso.PicassoUtil.DISK_CACHE_SCHEME_NAME.equals(data.uri.getScheme());
        }

        public com.squareup.picasso.RequestHandler.Result load(com.squareup.picasso.Request request, int networkPolicy) throws java.io.IOException {
            java.lang.String resourceName = request.uri.getHost();
            com.navdy.hud.app.util.picasso.PicassoUtil.sLogger.v("load:" + resourceName);
            if (com.navdy.hud.app.util.picasso.PicassoUtil.diskLruCache == null) {
                throw new java.io.IOException();
            }
            byte[] data = com.navdy.hud.app.util.picasso.PicassoUtil.diskLruCache.get(resourceName);
            if (data != null) {
                return new com.squareup.picasso.RequestHandler.Result((java.io.InputStream) new java.io.ByteArrayInputStream(data), com.squareup.picasso.Picasso.LoadedFrom.DISK);
            }
            throw new java.io.IOException();
        }
    }

    public static void initPicasso(android.content.Context context) {
        java.lang.String imageDiskCacheFolder;
        if (!initialized) {
            synchronized (lockObj) {
                if (!initialized) {
                    try {
                        lruCache = new com.navdy.hud.app.util.picasso.PicassoLruCache(8388608);
                        lruCache.init();
                        imageDiskCacheFolder = com.navdy.hud.app.storage.PathManager.getInstance().getImageDiskCacheFolder();
                        diskLruCache = new com.navdy.hud.app.storage.cache.DiskLruCache("imagecache", imageDiskCacheFolder, DISK_CACHE_SIZE);
                    } catch (Throwable t2) {
                        sLogger.e("Error re-creating disk cache: " + t2);
                    }
                    picasso = new com.squareup.picasso.Picasso.Builder(context).memoryCache(lruCache).addRequestHandler(new com.navdy.hud.app.util.picasso.PicassoUtil.Anon1()).build();
                    com.squareup.picasso.Picasso.setSingletonInstance(picasso);
                    initialized = true;
                }
            }
        }
    }

    public static void clearCache() {
        if (lruCache != null) {
            lruCache.clear();
        }
    }

    public static android.graphics.Bitmap getBitmapfromCache(java.io.File file) {
        if (lruCache != null) {
            return lruCache.getBitmap("file://" + file.getAbsolutePath());
        }
        return null;
    }

    public static android.graphics.Bitmap getBitmapfromCache(java.lang.String key) {
        if (lruCache != null) {
            return lruCache.getBitmap(key);
        }
        return null;
    }

    public static void setBitmapInCache(java.lang.String key, android.graphics.Bitmap bitmap) {
        if (lruCache != null) {
            lruCache.setBitmap(key, bitmap);
        }
    }

    public static com.squareup.picasso.Picasso getInstance() {
        if (!initialized) {
            initPicasso(com.navdy.hud.app.HudApplication.getAppContext());
        }
        return picasso;
    }

    public static boolean isImageAvailableInCache(java.io.File file) {
        if (getBitmapfromCache(file) != null) {
            return true;
        }
        return false;
    }

    public static com.navdy.hud.app.storage.cache.DiskLruCache getDiskLruCache() {
        return diskLruCache;
    }

    public static android.net.Uri getDiskcacheUri(java.lang.String name) {
        return android.net.Uri.parse(DISK_CACHE_SCHEME + name);
    }
}
