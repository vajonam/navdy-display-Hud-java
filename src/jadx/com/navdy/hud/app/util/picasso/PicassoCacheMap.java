package com.navdy.hud.app.util.picasso;

public class PicassoCacheMap<K, V> extends java.util.LinkedHashMap<K, V> {
    private com.navdy.hud.app.util.picasso.PicassoItemCacheListener listener;

    PicassoCacheMap(int initialCapacity, float loadFactor) {
        super(initialCapacity, loadFactor, true);
    }

    /* access modifiers changed from: 0000 */
    public void setListener(com.navdy.hud.app.util.picasso.PicassoItemCacheListener listener2) {
        this.listener = listener2;
    }

    public V remove(java.lang.Object key) {
        this.listener.itemRemoved(key);
        return super.remove(key);
    }

    public V put(K key, V value) {
        V ret = super.put(key, value);
        this.listener.itemAdded(key);
        return ret;
    }
}
