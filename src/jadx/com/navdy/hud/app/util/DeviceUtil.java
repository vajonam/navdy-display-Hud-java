package com.navdy.hud.app.util;

public class DeviceUtil {
    public static final java.lang.String BUILD_TYPE_USER = "user";
    private static final com.navdy.hud.app.util.DeviceUtil.HereSdkVersion CURRENT_HERE_SDK = com.navdy.hud.app.util.DeviceUtil.HereSdkVersion.SDK;
    public static final java.lang.String TELEMETRY_FILE_NAME = "telemetry";
    private static java.lang.Boolean hasCamera;
    private static boolean hudDevice = true;
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.util.DeviceUtil.class);

    private enum HereSdkVersion {
        SDK(com.here.android.mpa.common.Version.getSdkVersion(), "964739bdbfbc7c3618a86dec433d9b6d206c1bb9");
        
        /* access modifiers changed from: private */
        public java.lang.String folderName;
        /* access modifiers changed from: private */
        public java.lang.String version;

        private HereSdkVersion(java.lang.String version2, java.lang.String timestamp) {
            this.version = version2;
            this.folderName = timestamp;
        }
    }

    static {
        if (android.os.Build.MODEL.equalsIgnoreCase("NAVDY_HUD-MX6DL") || android.os.Build.MODEL.equalsIgnoreCase("Display")) {
            sLogger.i("Running on Navdy Device:" + android.os.Build.MODEL);
            return;
        }
        sLogger.i("Not running on Navdy Device:" + android.os.Build.MODEL);
    }

    public static boolean isNavdyDevice() {
        return hudDevice;
    }

    public static boolean supportsCamera() {
        if (hasCamera == null) {
            sLogger.v("Found " + android.hardware.Camera.getNumberOfCameras() + " cameras");
            android.hardware.Camera camera = null;
            try {
                camera = android.hardware.Camera.open(0);
                hasCamera = java.lang.Boolean.valueOf(true);
                if (camera != null) {
                    camera.release();
                }
            } catch (java.lang.Exception e) {
                sLogger.e("Failed to open camera", e);
                hasCamera = java.lang.Boolean.valueOf(false);
                if (camera != null) {
                    camera.release();
                }
            } catch (Throwable th) {
                if (camera != null) {
                    camera.release();
                }
                throw th;
            }
        }
        return hasCamera.booleanValue();
    }

    public static void takeDeviceScreenShot(java.lang.String absolutePath) {
        try {
            java.lang.Runtime.getRuntime().exec("screencap -p " + absolutePath).waitFor();
        } catch (java.io.IOException e) {
            sLogger.e("Error while taking screen shot", e);
        } catch (java.lang.InterruptedException e2) {
            sLogger.e("Error while taking screen shot", e2);
        }
    }

    public static boolean isUserBuild() {
        return "user".equals(android.os.Build.TYPE);
    }

    public static void copyHEREMapsDataInfo(java.lang.String destination) {
        java.io.File dir = new java.io.File(com.navdy.hud.app.storage.PathManager.getInstance().getHereMapsDataDirectory());
        if (!dir.exists()) {
            sLogger.d("Here maps data directory on the maps partition does not exists");
            return;
        }
        java.io.File metaData = new java.io.File(dir, com.navdy.hud.app.storage.PathManager.HERE_MAP_META_JSON_FILE);
        if (!metaData.exists()) {
            sLogger.d("Meta json file not found");
            return;
        }
        try {
            com.navdy.service.library.util.IOUtils.copyFile(metaData.getAbsolutePath(), destination + java.io.File.separator + "HERE_meta.json");
        } catch (java.io.IOException e) {
            sLogger.e("Error copying the HERE maps data meta json file");
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:28:0x0066 A[SYNTHETIC, Splitter:B:28:0x0066] */
    public static java.lang.String getHEREMapsDataInfo() {
        java.lang.String str = null;
        java.io.File dir = new java.io.File(com.navdy.hud.app.storage.PathManager.getInstance().getHereMapsDataDirectory());
        if (!dir.exists()) {
            sLogger.d("Here maps data directory on the maps partition does not exists");
        } else {
            java.io.File metaData = new java.io.File(dir, com.navdy.hud.app.storage.PathManager.HERE_MAP_META_JSON_FILE);
            if (!metaData.exists()) {
                sLogger.d("Meta json file not found");
            } else {
                java.io.FileInputStream fis = null;
                try {
                    java.io.FileInputStream fis2 = new java.io.FileInputStream(metaData);
                    try {
                        str = com.navdy.service.library.util.IOUtils.convertInputStreamToString(fis2, "UTF-8");
                        if (fis2 != null) {
                            try {
                                fis2.close();
                            } catch (java.io.IOException e) {
                                sLogger.e("Error closing the FileInputStream", e);
                            }
                        }
                    } catch (java.lang.Exception e2) {
                        e = e2;
                        fis = fis2;
                    } catch (Throwable th) {
                        th = th;
                        fis = fis2;
                        if (fis != null) {
                        }
                        throw th;
                    }
                } catch (java.lang.Exception e3) {
                    e = e3;
                    try {
                        sLogger.e("Error reading the Meta data,", e);
                        if (fis != null) {
                            try {
                                fis.close();
                            } catch (java.io.IOException e4) {
                                sLogger.e("Error closing the FileInputStream", e4);
                            }
                        }
                        return str;
                    } catch (Throwable th2) {
                        th = th2;
                        if (fis != null) {
                            try {
                                fis.close();
                            } catch (java.io.IOException e5) {
                                sLogger.e("Error closing the FileInputStream", e5);
                            }
                        }
                        throw th;
                    }
                }
            }
        }
        return str;
    }

    public static java.lang.String getCurrentHereSdkVersion() {
        return CURRENT_HERE_SDK.version;
    }

    public static java.lang.String getCurrentHereSdkTimestamp() {
        return CURRENT_HERE_SDK.folderName;
    }
}
