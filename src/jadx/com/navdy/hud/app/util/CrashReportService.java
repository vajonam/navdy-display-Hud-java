package com.navdy.hud.app.util;

public class CrashReportService extends android.app.IntentService {
    private static final java.lang.String ACTION_ADD_REPORT = "AddReport";
    private static final java.lang.String ACTION_DUMP_CRASH_REPORT = "DumpCrashReport";
    private static java.lang.String CRASH_INFO_TEXT_FILE_NAME = "info.txt";
    private static java.lang.String CRASH_LOG_TEXT_FILE_NAME = "log.txt";
    private static final java.text.SimpleDateFormat DATE_FORMAT = new java.text.SimpleDateFormat("yyyy-MM-dd'_'HH:mm:ss.SSS", java.util.Locale.US);
    public static final java.lang.String EXTRA_CRASH_TYPE = "EXTRA_CRASH_TYPE";
    public static final java.lang.String EXTRA_FILE_PATH = "EXTRA_FILE_PATH";
    private static int LOG_FILE_SIZE = 51200;
    private static final int MAX_NON_FATAL_CRASH_REPORTS_OUT_STANDING = 10;
    private static final java.text.SimpleDateFormat TIME_FORMAT_FOR_REPORT = new java.text.SimpleDateFormat("dd MM yyyy' 'HH:mm:ss.SSS", java.util.Locale.US);
    private static boolean mIsInitialized = false;
    private static java.lang.String sCrashReportsFolder;
    private static java.util.concurrent.PriorityBlockingQueue<java.io.File> sCrashReportsToSend = new java.util.concurrent.PriorityBlockingQueue<>(10, new com.navdy.hud.app.util.CrashReportService.FilesModifiedTimeComparator());
    private static com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.util.CrashReportService.class);
    private static int sOutStandingCrashReportsToBeCleared = 0;

    public enum CrashType {
        BLUETOOTH_DISCONNECTED,
        OBD_RESET,
        GENERIC_NON_FATAL_CRASH
    }

    public static class FilesModifiedTimeComparator implements java.util.Comparator<java.io.File> {
        public int compare(java.io.File file, java.io.File t1) {
            return (int) (file.lastModified() - t1.lastModified());
        }
    }

    public CrashReportService() {
        super("CrashReportService");
    }

    public void onCreate() {
        super.onCreate();
        if (!mIsInitialized) {
            sCrashReportsFolder = com.navdy.hud.app.storage.PathManager.getInstance().getNonFatalCrashReportDir();
            java.io.File[] files = new java.io.File(sCrashReportsFolder).listFiles();
            sLogger.d("Number of Fatal crash reports :" + (files != null ? files.length : 0));
            if (files != null) {
                populateFilesQueue(files, sCrashReportsToSend, 10);
            }
            mIsInitialized = true;
        }
    }

    /* access modifiers changed from: protected */
    public void onHandleIntent(android.content.Intent intent) {
        try {
            sLogger.d("onHandleIntent " + intent.toString());
            if (ACTION_DUMP_CRASH_REPORT.equals(intent.getAction())) {
                java.lang.String crashName = intent.getStringExtra(EXTRA_CRASH_TYPE);
                if (crashName != null) {
                    sLogger.d("Dumping a crash " + crashName);
                    dumpCrashReport(crashName);
                    return;
                }
                sLogger.e("Missing crash type");
                return;
            }
            if (ACTION_ADD_REPORT.equals(intent.getAction())) {
                java.lang.String absolutePath = intent.getStringExtra(EXTRA_FILE_PATH);
                if (!android.text.TextUtils.isEmpty(absolutePath)) {
                    java.io.File reportFile = new java.io.File(absolutePath);
                    if (reportFile.exists() && reportFile.isFile()) {
                        addReport(reportFile);
                    }
                }
            }
        } catch (Throwable t) {
            sLogger.e("Exception while handling intent ", t);
        }
    }

    private void addReport(java.io.File file) {
        synchronized (sCrashReportsToSend) {
            sCrashReportsToSend.add(file);
            if (sCrashReportsToSend.size() > 10) {
                java.io.File oldestFile = (java.io.File) sCrashReportsToSend.poll();
                if (oldestFile != null) {
                    com.navdy.service.library.util.IOUtils.deleteFile(com.navdy.hud.app.HudApplication.getAppContext(), oldestFile.getAbsolutePath());
                }
                if (sOutStandingCrashReportsToBeCleared > 0) {
                    sOutStandingCrashReportsToBeCleared--;
                }
            }
        }
    }

    public static com.navdy.hud.app.util.CrashReportService.CrashType getCrashTypeForId(int id) {
        com.navdy.hud.app.util.CrashReportService.CrashType[] values = com.navdy.hud.app.util.CrashReportService.CrashType.values();
        if (id < 0 || id >= values.length) {
            return null;
        }
        return values[id];
    }

    private void dumpCrashReport(java.lang.String crashName) {
        java.lang.StringBuilder amPmMarker = new java.lang.StringBuilder();
        java.util.Date currentTime = new java.util.Date(java.lang.System.currentTimeMillis());
        java.lang.String time = TIME_FORMAT_FOR_REPORT.format(currentTime);
        java.io.File infoTextFile = new java.io.File(sCrashReportsFolder + java.io.File.separator + CRASH_INFO_TEXT_FILE_NAME);
        if (infoTextFile.exists()) {
            com.navdy.service.library.util.IOUtils.deleteFile(this, infoTextFile.getAbsolutePath());
        }
        java.io.File snapShotDirectory = infoTextFile.getParentFile();
        if (!snapShotDirectory.exists()) {
            snapShotDirectory.mkdirs();
        }
        java.io.FileWriter fileWriter = null;
        try {
            infoTextFile.createNewFile();
            java.io.FileWriter fileWriter2 = new java.io.FileWriter(infoTextFile);
            try {
                fileWriter2.write("Crash Type : " + crashName + com.navdy.hud.app.framework.glance.GlanceConstants.NEWLINE);
                fileWriter2.write("Report Time : " + time + " , " + amPmMarker.toString() + com.navdy.hud.app.framework.glance.GlanceConstants.NEWLINE);
                fileWriter2.write(com.navdy.hud.app.util.os.PropsFileUpdater.readProps());
                com.navdy.service.library.events.DeviceInfo deviceInfo = com.navdy.service.library.device.RemoteDevice.sLastSeenDeviceInfo.getDeviceInfo();
                if (deviceInfo != null) {
                    fileWriter2.write(printDeviceInfo(deviceInfo));
                }
                fileWriter2.flush();
                fileWriter2.close();
                java.io.File file = new java.io.File(sCrashReportsFolder + java.io.File.separator + "temp");
                if (file.exists()) {
                    com.navdy.service.library.util.IOUtils.deleteDirectory(this, file);
                }
                file.mkdirs();
                com.navdy.service.library.util.LogUtils.copySnapshotSystemLogs(file.getAbsolutePath());
                java.io.File[] logFiles = file.listFiles();
                java.io.File[] files = new java.io.File[(logFiles != null ? logFiles.length + 1 : 1)];
                files[0] = infoTextFile;
                if (logFiles != null) {
                    int length = logFiles.length;
                    int i = 0;
                    int i2 = 1;
                    while (i < length) {
                        int i3 = i2 + 1;
                        files[i2] = logFiles[i];
                        i++;
                        i2 = i3;
                    }
                    int i4 = i2;
                }
                java.io.File zipFile = new java.io.File(sCrashReportsFolder + java.io.File.separator + (DATE_FORMAT.format(currentTime) + com.navdy.service.library.util.MusicDataUtils.ALTERNATE_SEPARATOR + crashName + ".zip"));
                com.navdy.service.library.util.IOUtils.compressFilesToZip(com.navdy.hud.app.HudApplication.getAppContext(), files, zipFile.getAbsolutePath());
                com.navdy.service.library.util.IOUtils.deleteFile(this, infoTextFile.getAbsolutePath());
                com.navdy.service.library.util.IOUtils.deleteDirectory(this, file);
                addReport(zipFile);
                try {
                    fileWriter2.close();
                    java.io.FileWriter fileWriter3 = fileWriter2;
                } catch (java.io.IOException e) {
                    sLogger.d("Error closing the File Writer");
                    java.io.FileWriter fileWriter4 = fileWriter2;
                }
            } catch (Throwable th) {
                th = th;
                fileWriter = fileWriter2;
                fileWriter.close();
                throw th;
            }
        } catch (Throwable th2) {
            e = th2;
            sLogger.e("Exception while creating the crash report ", e);
            fileWriter.close();
        }
    }

    public static void compressCrashReportsToZip(java.io.File[] additionalFiles, java.lang.String outputFilePath) {
        java.io.File[] files;
        java.io.File file = new java.io.File(outputFilePath);
        if (file.exists()) {
            com.navdy.service.library.util.IOUtils.deleteFile(com.navdy.hud.app.HudApplication.getAppContext(), outputFilePath);
        }
        try {
            file.createNewFile();
        } catch (java.io.IOException e) {
            sLogger.e("Failed to create new file at the specified output file path " + outputFilePath, e);
        }
        int totalSize = 0;
        if (additionalFiles != null && additionalFiles.length > 0) {
            totalSize = additionalFiles.length;
        }
        synchronized (sCrashReportsToSend) {
            files = new java.io.File[(totalSize + sCrashReportsToSend.size())];
            int i = 0;
            if (additionalFiles != null) {
                int length = additionalFiles.length;
                int i2 = 0;
                int i3 = 0;
                while (i2 < length) {
                    int i4 = i3 + 1;
                    files[i3] = additionalFiles[i2];
                    i2++;
                    i3 = i4;
                }
                i = i3;
            }
            sOutStandingCrashReportsToBeCleared = sCrashReportsToSend.size();
            java.util.Iterator<java.io.File> filesIterator = sCrashReportsToSend.iterator();
            int i5 = i;
            while (filesIterator.hasNext()) {
                int i6 = i5 + 1;
                files[i5] = (java.io.File) filesIterator.next();
                i5 = i6;
            }
        }
        com.navdy.service.library.util.IOUtils.compressFilesToZip(com.navdy.hud.app.HudApplication.getAppContext(), files, outputFilePath);
    }

    public static void clearCrashReports() {
        synchronized (sCrashReportsToSend) {
            while (sOutStandingCrashReportsToBeCleared > 0) {
                java.io.File oldestFile = (java.io.File) sCrashReportsToSend.poll();
                if (oldestFile != null) {
                    com.navdy.service.library.util.IOUtils.deleteFile(com.navdy.hud.app.HudApplication.getAppContext(), oldestFile.getAbsolutePath());
                    sOutStandingCrashReportsToBeCleared--;
                }
            }
        }
    }

    public static void addSnapshotAsync(java.lang.String absolutePath) {
        android.content.Intent intent = new android.content.Intent(com.navdy.hud.app.HudApplication.getAppContext(), com.navdy.hud.app.util.CrashReportService.class);
        intent.setAction(ACTION_ADD_REPORT);
        intent.putExtra(EXTRA_FILE_PATH, absolutePath);
        com.navdy.hud.app.HudApplication.getAppContext().startService(intent);
    }

    public static void dumpCrashReportAsync(com.navdy.hud.app.util.CrashReportService.CrashType type) {
        android.content.Intent intent = new android.content.Intent(com.navdy.hud.app.HudApplication.getAppContext(), com.navdy.hud.app.util.CrashReportService.class);
        intent.setAction(ACTION_DUMP_CRASH_REPORT);
        intent.putExtra(EXTRA_CRASH_TYPE, type.name());
        com.navdy.hud.app.HudApplication.getAppContext().startService(intent);
    }

    public static void populateFilesQueue(java.io.File[] files, java.util.concurrent.PriorityBlockingQueue<java.io.File> filesQueue, int maxSize) {
        if (files != null) {
            for (java.io.File file : files) {
                if (file.isFile()) {
                    sLogger.d("File " + file.getName());
                    filesQueue.add(file);
                    if (filesQueue.size() == maxSize) {
                        java.io.File oldestFile = (java.io.File) filesQueue.poll();
                        sLogger.d("Deleting the old file " + oldestFile.getName());
                        com.navdy.service.library.util.IOUtils.deleteFile(com.navdy.hud.app.HudApplication.getAppContext(), oldestFile.getAbsolutePath());
                    }
                }
            }
        }
    }

    public static java.lang.String printDeviceInfo(com.navdy.service.library.events.DeviceInfo deviceInfo) {
        java.lang.StringBuilder builder = new java.lang.StringBuilder();
        builder.append("\nDevice Information\n");
        builder.append("-----------------------\n\n");
        builder.append("Platform :" + deviceInfo.platform.name() + com.navdy.hud.app.framework.glance.GlanceConstants.NEWLINE);
        builder.append("Name :" + deviceInfo.deviceName + com.navdy.hud.app.framework.glance.GlanceConstants.NEWLINE);
        builder.append("System Version :" + deviceInfo.systemVersion + com.navdy.hud.app.framework.glance.GlanceConstants.NEWLINE);
        builder.append("Model :" + deviceInfo.model + com.navdy.hud.app.framework.glance.GlanceConstants.NEWLINE);
        builder.append("Device ID :" + deviceInfo.deviceId + com.navdy.hud.app.framework.glance.GlanceConstants.NEWLINE);
        builder.append("Make :" + deviceInfo.deviceMake + com.navdy.hud.app.framework.glance.GlanceConstants.NEWLINE);
        builder.append("Build Type :" + deviceInfo.buildType + com.navdy.hud.app.framework.glance.GlanceConstants.NEWLINE);
        builder.append("API level :" + deviceInfo.systemApiLevel + com.navdy.hud.app.framework.glance.GlanceConstants.NEWLINE);
        builder.append("Protocol version :" + deviceInfo.protocolVersion + com.navdy.hud.app.framework.glance.GlanceConstants.NEWLINE);
        builder.append("Force Full Update Flag :" + deviceInfo.forceFullUpdate + com.navdy.hud.app.framework.glance.GlanceConstants.NEWLINE);
        return builder.toString();
    }
}
