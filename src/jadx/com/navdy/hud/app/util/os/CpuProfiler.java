package com.navdy.hud.app.util.os;

public class CpuProfiler {
    /* access modifiers changed from: private */
    public static final com.navdy.hud.app.util.os.CpuProfiler.CpuUsage HIGH_CPU = new com.navdy.hud.app.util.os.CpuProfiler.CpuUsage(com.navdy.hud.app.util.os.CpuProfiler.Usage.HIGH);
    private static final java.lang.String[] HIGH_CPU_THREAD_NAME_PATTERN = {"NAVDY-HOGGER-", "Thread-"};
    private static final int HIGH_CPU_USAGE_THRESHOLD = 85;
    private static final int INITIAL_PERIODIC_INTERVAL = ((int) java.util.concurrent.TimeUnit.SECONDS.toMillis(45));
    private static final int LOW_NICE_PRIORITY = 19;
    private static final java.lang.String NAVDY_PROCESS_NAME = "com.navdy.hud.app";
    /* access modifiers changed from: private */
    public static final com.navdy.hud.app.util.os.CpuProfiler.CpuUsage NORMAL_CPU = new com.navdy.hud.app.util.os.CpuProfiler.CpuUsage(com.navdy.hud.app.util.os.CpuProfiler.Usage.NORMAL);
    private static final int NORMAL_CPU_USAGE_THRESHOLD = 70;
    /* access modifiers changed from: private */
    public static final int PERIODIC_INTERVAL = ((int) java.util.concurrent.TimeUnit.SECONDS.toMillis(10));
    private static final int PROCESS_CPU_USAGE_THRESHOLD = 20;
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.util.os.CpuProfiler.class);
    private static final com.navdy.hud.app.util.os.CpuProfiler singleton = new com.navdy.hud.app.util.os.CpuProfiler();
    /* access modifiers changed from: private */
    public com.squareup.otto.Bus bus = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getBus();
    /* access modifiers changed from: private */
    public android.os.Handler handler = new android.os.Handler(android.os.Looper.getMainLooper());
    /* access modifiers changed from: private */
    public com.navdy.service.library.util.SystemUtils.ProcessCpuInfo highCpuThreadInfo;
    private int highCpuThreadOrigPrio;
    /* access modifiers changed from: private */
    public boolean highCpuUsage;
    /* access modifiers changed from: private */
    public java.lang.Runnable periodicRunnable = new com.navdy.hud.app.util.os.CpuProfiler.Anon1();
    /* access modifiers changed from: private */
    public java.lang.Runnable periodicRunnableBk = new com.navdy.hud.app.util.os.CpuProfiler.Anon2();
    /* access modifiers changed from: private */
    public boolean running;

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            com.navdy.service.library.task.TaskManager.getInstance().execute(com.navdy.hud.app.util.os.CpuProfiler.this.periodicRunnableBk, 1);
        }
    }

    class Anon2 implements java.lang.Runnable {
        Anon2() {
        }

        public void run() {
            try {
                if (com.navdy.hud.app.util.os.CpuProfiler.this.running) {
                    com.navdy.service.library.util.SystemUtils.CpuInfo cpuInfo = com.navdy.service.library.util.SystemUtils.getCpuUsage();
                    int user = cpuInfo.getCpuUser();
                    int system = cpuInfo.getCpuSystem();
                    int total = user + system;
                    if (!com.navdy.hud.app.util.os.CpuProfiler.this.highCpuUsage) {
                        if (total > com.navdy.hud.app.util.os.CpuProfiler.HIGH_CPU_USAGE_THRESHOLD) {
                            com.navdy.hud.app.util.os.CpuProfiler.this.highCpuUsage = true;
                            com.navdy.hud.app.util.os.CpuProfiler.this.bus.post(com.navdy.hud.app.util.os.CpuProfiler.HIGH_CPU);
                            com.navdy.hud.app.util.os.CpuProfiler.this.takeCorrectiveAction(cpuInfo);
                            com.navdy.hud.app.util.os.CpuProfiler.this.printCpuInfo("HIGH", total, user, system, cpuInfo.getList());
                        } else if (com.navdy.hud.app.util.os.CpuProfiler.sLogger.isLoggable(2)) {
                            com.navdy.hud.app.util.os.CpuProfiler.this.printCpuInfo("NORMAL", total, user, system, cpuInfo.getList());
                        }
                    } else if (total < 70) {
                        com.navdy.hud.app.util.os.CpuProfiler.this.highCpuUsage = false;
                        com.navdy.hud.app.util.os.CpuProfiler.this.bus.post(com.navdy.hud.app.util.os.CpuProfiler.NORMAL_CPU);
                        com.navdy.hud.app.util.os.CpuProfiler.this.revertCorrectiveAction(cpuInfo);
                        com.navdy.hud.app.util.os.CpuProfiler.this.printCpuInfo("NOW NORMAL", total, user, system, cpuInfo.getList());
                    } else {
                        com.navdy.hud.app.util.os.CpuProfiler.this.printCpuInfo("STILL HIGH", total, user, system, cpuInfo.getList());
                        if (com.navdy.hud.app.util.os.CpuProfiler.this.highCpuThreadInfo == null) {
                            com.navdy.hud.app.util.os.CpuProfiler.this.takeCorrectiveAction(cpuInfo);
                        }
                    }
                    if (com.navdy.hud.app.util.os.CpuProfiler.this.running) {
                        com.navdy.hud.app.util.os.CpuProfiler.this.handler.postDelayed(com.navdy.hud.app.util.os.CpuProfiler.this.periodicRunnable, (long) com.navdy.hud.app.util.os.CpuProfiler.PERIODIC_INTERVAL);
                    }
                } else if (com.navdy.hud.app.util.os.CpuProfiler.this.running) {
                    com.navdy.hud.app.util.os.CpuProfiler.this.handler.postDelayed(com.navdy.hud.app.util.os.CpuProfiler.this.periodicRunnable, (long) com.navdy.hud.app.util.os.CpuProfiler.PERIODIC_INTERVAL);
                }
            } catch (Throwable th) {
                if (com.navdy.hud.app.util.os.CpuProfiler.this.running) {
                    com.navdy.hud.app.util.os.CpuProfiler.this.handler.postDelayed(com.navdy.hud.app.util.os.CpuProfiler.this.periodicRunnable, (long) com.navdy.hud.app.util.os.CpuProfiler.PERIODIC_INTERVAL);
                }
                throw th;
            }
        }
    }

    public static class CpuUsage {
        public com.navdy.hud.app.util.os.CpuProfiler.Usage usage;

        CpuUsage(com.navdy.hud.app.util.os.CpuProfiler.Usage usage2) {
            this.usage = usage2;
        }
    }

    public enum Usage {
        HIGH,
        NORMAL
    }

    public static com.navdy.hud.app.util.os.CpuProfiler getInstance() {
        return singleton;
    }

    private CpuProfiler() {
    }

    public synchronized void start() {
        if (this.running) {
            sLogger.v("already running");
        } else {
            this.handler.removeCallbacks(this.periodicRunnable);
            this.handler.postDelayed(this.periodicRunnable, (long) INITIAL_PERIODIC_INTERVAL);
            this.running = true;
            sLogger.v("running");
        }
    }

    public synchronized void stop() {
        if (!this.running) {
            sLogger.v("not running");
        } else {
            this.handler.removeCallbacks(this.periodicRunnable);
            this.running = false;
            sLogger.v("stopped");
        }
    }

    /* access modifiers changed from: private */
    public void printCpuInfo(java.lang.String tag, int total, int user, int system, java.util.ArrayList<com.navdy.service.library.util.SystemUtils.ProcessCpuInfo> list) {
        sLogger.v("[CPU] " + tag + " cpu=" + total + "% user=" + user + "% system=" + system + "%");
        java.util.Iterator it = list.iterator();
        while (it.hasNext()) {
            com.navdy.service.library.util.SystemUtils.ProcessCpuInfo info = (com.navdy.service.library.util.SystemUtils.ProcessCpuInfo) it.next();
            if (!"top".equals(info.getProcessName())) {
                sLogger.v("[CPU] " + tag + " " + info.getCpu() + "% process=" + info.getProcessName() + " thread=" + info.getThreadName() + " tid=" + info.getTid() + " pid=" + info.getPid() + "");
            }
        }
    }

    /* access modifiers changed from: private */
    public void takeCorrectiveAction(com.navdy.service.library.util.SystemUtils.CpuInfo cpuInfo) {
        try {
            sLogger.i("takeCorrectiveAction");
            com.navdy.service.library.util.SystemUtils.ProcessCpuInfo highCpuThread = getKnownHighCpuThread(cpuInfo);
            if (highCpuThread == null) {
                sLogger.i("takeCorrectiveAction high cpu thread not found");
            } else if (highCpuThread.getTid() != 0) {
                int prio = android.os.Process.getThreadPriority(highCpuThread.getTid());
                sLogger.v("takeCorrectiveAction: high cpu thread, priority[" + prio + "]");
                if (this.highCpuThreadInfo == null) {
                    this.highCpuThreadInfo = highCpuThread;
                    this.highCpuThreadOrigPrio = prio;
                    android.os.Process.setThreadPriority(this.highCpuThreadInfo.getTid(), 19);
                    sLogger.i("takeCorrectiveAction high cpu thread, priority changed from " + prio + " to " + android.os.Process.getThreadPriority(highCpuThread.getTid()));
                    return;
                }
                sLogger.i("takeCorrectiveAction high cpu thread, priority already reduced");
            } else {
                sLogger.i("takeCorrectiveAction high cpu thread tid is not valid");
            }
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    public void revertCorrectiveAction(com.navdy.service.library.util.SystemUtils.CpuInfo cpuInfo) {
        try {
            sLogger.i("revertCorrectiveAction");
            if (this.highCpuThreadInfo != null) {
                android.os.Process.setThreadPriority(this.highCpuThreadInfo.getTid(), this.highCpuThreadOrigPrio);
                sLogger.i("revertCorrectiveAction high cpu thread priority reduced from 19 to " + this.highCpuThreadOrigPrio);
                this.highCpuThreadInfo = null;
                this.highCpuThreadOrigPrio = 0;
                return;
            }
            sLogger.i("revertCorrectiveAction high cpu thread was not detected previously");
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }

    private com.navdy.service.library.util.SystemUtils.ProcessCpuInfo getKnownHighCpuThread(com.navdy.service.library.util.SystemUtils.CpuInfo cpuInfo) {
        java.util.Iterator it = cpuInfo.getList().iterator();
        while (it.hasNext()) {
            com.navdy.service.library.util.SystemUtils.ProcessCpuInfo info = (com.navdy.service.library.util.SystemUtils.ProcessCpuInfo) it.next();
            if (android.text.TextUtils.equals(info.getProcessName(), "com.navdy.hud.app")) {
                java.lang.String threadName = info.getThreadName();
                if (threadName != null) {
                    for (java.lang.String startsWith : HIGH_CPU_THREAD_NAME_PATTERN) {
                        if (threadName.startsWith(startsWith)) {
                            if (info.getCpu() >= 20) {
                                sLogger.v("high cpu thread found " + info.getThreadName() + " " + info.getCpu() + "%");
                                return info;
                            }
                            sLogger.v("Thread found but cpu usage < threshold" + info.getThreadName() + " " + info.getCpu() + "%");
                        }
                    }
                    continue;
                } else {
                    continue;
                }
            }
        }
        return null;
    }
}
