package com.navdy.hud.app.util.os;

public class PropsFileUpdater {
    private static final java.lang.String CHAR_SET = "UTF-8";
    private static final java.lang.String COMMAND_GETPROP = "getprop";
    private static final int PROPS_FILE_INTENT = 2;
    private static final java.lang.String PROPS_FILE_NAME = "system_info.json";
    private static final java.lang.String PROPS_FILE_PATH_PROP_NAME = "ro.maps_partition";
    private static final java.util.regex.Pattern PROPS_KEY_VALUE_SEPARATOR = java.util.regex.Pattern.compile("\\]: \\[");
    private static final int PROPS_READOUT_DELAY = 15000;
    private static final java.lang.String PROPS_TEMP_FILE_SUFFIX = "~";
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.util.os.PropsFileUpdater.class);

    static class Anon1 implements java.lang.Runnable {

        class UpdatePropsFileRunnable implements java.lang.Runnable {
            UpdatePropsFileRunnable() {
            }

            public void run() {
                try {
                    com.navdy.hud.app.util.os.PropsFileUpdater.updatePropsFile(com.navdy.hud.app.util.os.PropsFileUpdater.readProps());
                } catch (Throwable t) {
                    com.navdy.hud.app.util.os.PropsFileUpdater.sLogger.e("Error while updating props file", t);
                }
            }
        }

        Anon1() {
        }

        public void run() {
            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.util.os.PropsFileUpdater.Anon1.UpdatePropsFileRunnable(), 1);
        }
    }

    public static void run() {
        new android.os.Handler(android.os.Looper.getMainLooper()).postDelayed(new com.navdy.hud.app.util.os.PropsFileUpdater.Anon1(), com.glympse.android.lib.StaticConfig.PERMISSION_CHECK_INTERVAL);
    }

    /* access modifiers changed from: private */
    public static void updatePropsFile(@android.support.annotation.Nullable java.lang.String props) {
        if (props == null) {
            sLogger.w("Cannot update file with empty props string");
            return;
        }
        try {
            org.json.JSONObject json = propsToJSON(props);
            updatePropsFile(json.getString(PROPS_FILE_PATH_PROP_NAME), json.toString(2));
        } catch (org.json.JSONException e) {
            sLogger.e("Cannot build JSON string with props or read path for output file", e);
        }
    }

    private static void updatePropsFile(@android.support.annotation.NonNull java.lang.String destinationFileFullPath, @android.support.annotation.NonNull java.lang.String json) {
        java.lang.String propsFileFullPath = new java.io.File(destinationFileFullPath, PROPS_FILE_NAME).getAbsolutePath();
        java.lang.String propsTempFileFullPath = propsFileFullPath + PROPS_TEMP_FILE_SUFFIX;
        try {
            com.navdy.service.library.util.IOUtils.copyFile(propsTempFileFullPath, (java.io.InputStream) new java.io.ByteArrayInputStream(json.getBytes()));
            try {
                android.system.Os.rename(propsTempFileFullPath, propsFileFullPath);
                sLogger.i("Props file updated successfully");
            } catch (android.system.ErrnoException e) {
                sLogger.e("Cannot overwrite props file with the new one", e);
            }
        } catch (java.io.IOException e2) {
            sLogger.e("Exception while writing into file: " + propsTempFileFullPath, e2);
        }
    }

    @android.support.annotation.NonNull
    private static org.json.JSONObject propsToJSON(@android.support.annotation.NonNull java.lang.String props) {
        java.lang.String[] propLines = props.split(java.lang.System.getProperty("line.separator"));
        org.json.JSONObject jsonObject = new org.json.JSONObject();
        for (java.lang.String propLine : propLines) {
            java.lang.String[] keyValuePair = PROPS_KEY_VALUE_SEPARATOR.split(propLine.trim().substring(1, propLine.length() - 1), 2);
            try {
                jsonObject.put(keyValuePair[0], keyValuePair[1]);
            } catch (org.json.JSONException e) {
                sLogger.e("Cannot create json for prop: " + propLine + " - skipping", e);
            }
        }
        return jsonObject;
    }

    @android.support.annotation.Nullable
    public static java.lang.String readProps() {
        java.lang.Process readerProcess = null;
        java.lang.String props = null;
        try {
            readerProcess = new java.lang.ProcessBuilder(new java.lang.String[0]).command(new java.lang.String[]{COMMAND_GETPROP}).start();
            readerProcess.waitFor();
            sLogger.d("Reading props process ended with exit value: " + readerProcess.exitValue());
            props = com.navdy.service.library.util.IOUtils.convertInputStreamToString(readerProcess.getInputStream(), "UTF-8");
            if (readerProcess != null) {
                readerProcess.destroy();
            }
        } catch (Throwable th) {
            if (readerProcess != null) {
                readerProcess.destroy();
            }
            throw th;
        }
        return props;
    }
}
