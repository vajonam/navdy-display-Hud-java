package com.navdy.hud.app.util;

public class ScreenOrientation {
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.util.ScreenOrientation.class);

    public static boolean isPortrait(android.app.Activity activity) {
        return isPortrait(getCurrentOrientation(activity));
    }

    public static boolean isPortrait(int orientation) {
        return orientation == 1 || orientation == 9;
    }

    public static int getCurrentOrientation(android.app.Activity activity) {
        int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
        android.util.DisplayMetrics dm = new android.util.DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        int width = dm.widthPixels;
        int height = dm.heightPixels;
        if (((rotation == 0 || rotation == 2) && height > width) || ((rotation == 1 || rotation == 3) && width > height)) {
            switch (rotation) {
                case 0:
                    return 1;
                case 1:
                    return 0;
                case 2:
                    return 9;
                case 3:
                    return 8;
                default:
                    sLogger.e("Unknown screen orientation. Defaulting to portrait.");
                    return 1;
            }
        } else {
            switch (rotation) {
                case 0:
                    return 0;
                case 1:
                    return 1;
                case 2:
                    return 8;
                case 3:
                    return 9;
                default:
                    sLogger.e("Unknown screen orientation. Defaulting to landscape.");
                    return 0;
            }
        }
    }
}
