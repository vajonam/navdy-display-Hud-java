package com.navdy.hud.app.util;

public class SerialNumber {
    public static com.navdy.hud.app.util.SerialNumber instance = parse(android.os.Build.SERIAL);
    public final java.lang.String configurationCode;
    public final int dayOfWeek;
    public final java.lang.String factory;
    public final java.lang.String revisionCode;
    public final java.lang.String serialNo;
    public final int week;
    public final int year;

    public SerialNumber(java.lang.String factory2, int year2, int week2, int dayOfWeek2, java.lang.String serialNo2, java.lang.String configuration, java.lang.String revisionCode2) {
        this.factory = factory2;
        this.year = year2;
        this.week = week2;
        this.dayOfWeek = dayOfWeek2;
        this.serialNo = serialNo2;
        this.configurationCode = configuration;
        this.revisionCode = revisionCode2;
    }

    public static com.navdy.hud.app.util.SerialNumber parse(java.lang.String serial) {
        if (serial == null || serial.length() != 16) {
            return new com.navdy.hud.app.util.SerialNumber("NDY", 0, 0, 0, "0", "0", "0");
        }
        java.lang.String factory2 = serial.substring(0, 3);
        int year2 = parseNumber(serial.substring(3, 4), 0, "year");
        int week2 = parseNumber(serial.substring(4, 6), 1, "week");
        if (week2 < 1 || week2 > 52) {
            week2 = 1;
        }
        int dayOfWeek2 = parseNumber(serial.substring(6, 7), 1, "dayOfWeek");
        if (dayOfWeek2 < 1 || dayOfWeek2 > 7) {
            dayOfWeek2 = 1;
        }
        return new com.navdy.hud.app.util.SerialNumber(factory2, year2, week2, dayOfWeek2, serial.substring(7, 10), serial.substring(10, 14), serial.substring(14, 15));
    }

    private static int parseNumber(java.lang.String string, int defaultValue, java.lang.String fieldName) {
        int result = defaultValue;
        try {
            return java.lang.Integer.parseInt(string);
        } catch (java.lang.NumberFormatException e) {
            android.util.Log.e("SERIAL", "Invalid serial number field " + fieldName + " value:" + string);
            return result;
        }
    }
}
