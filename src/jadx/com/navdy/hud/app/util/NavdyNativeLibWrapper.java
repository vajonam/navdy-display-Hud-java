package com.navdy.hud.app.util;

public class NavdyNativeLibWrapper {
    private static boolean loaded;
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.util.NavdyNativeLibWrapper.class);

    public static native void crashSegv();

    public static native void crashSegvOnNewThread();

    public static native synchronized void startCpuHog();

    public static native synchronized void stopCpuHog();

    public static synchronized void loadlibrary() {
        synchronized (com.navdy.hud.app.util.NavdyNativeLibWrapper.class) {
            if (!loaded) {
                loaded = true;
                try {
                    sLogger.v("loading navdy lib");
                    java.lang.System.loadLibrary("Navdy");
                    sLogger.v("loaded navdy lib");
                } catch (Throwable t) {
                    sLogger.e(t);
                }
            }
        }
        return;
    }
}
