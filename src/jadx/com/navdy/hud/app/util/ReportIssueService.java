package com.navdy.hud.app.util;

public class ReportIssueService extends android.app.IntentService {
    private static final java.lang.String ACTION_DUMP = "Dump";
    private static final java.lang.String ACTION_SNAPSHOT = "Snapshot";
    private static final java.lang.String ACTION_SUBMIT_JIRA = "Jira";
    private static final java.lang.String ACTION_SYNC = "Sync";
    public static final java.lang.String ASSIGNEE = "assignee";
    public static final java.lang.String ASSIGNEE_EMAIL = "assigneeEmail";
    public static final java.lang.String ATTACHMENT = "attachment";
    private static final java.text.SimpleDateFormat DATE_FORMAT = new java.text.SimpleDateFormat("yyyy-MM-dd'_'HH:mm:ss.SSS", java.util.Locale.US);
    public static final java.lang.String ENVIRONMENT = "Environment";
    public static final java.lang.String EXTRA_ISSUE_TYPE = "EXTRA_ISSUE_TYPE";
    public static final java.lang.String EXTRA_SNAPSHOT_TITLE = "EXTRA_SNAPSHOT_TITLE";
    private static final java.lang.String HUD_ISSUE_TYPE = "Issue";
    private static final java.lang.String HUD_PROJECT = "HUD";
    private static final java.lang.String ISSUE_TYPE = "Task";
    private static final java.lang.String JIRA_CREDENTIALS_META = "JIRA_CREDENTIALS";
    private static final int MAX_FILES_OUT_STANDING = 10;
    private static final java.lang.String NAME = "REPORT_ISSUE";
    private static final java.lang.String PROJECT_NAME = "NP";
    public static final long RETRY_INTERVAL = java.util.concurrent.TimeUnit.MINUTES.toMillis(3);
    private static final java.text.SimpleDateFormat SNAPSHOT_DATE_FORMAT = new java.text.SimpleDateFormat("yyyy-MM-dd-HH-mm-ss", java.util.Locale.US);
    private static final java.text.SimpleDateFormat SNAPSHOT_JIRA_TICKET_DATE_FORMAT = new java.text.SimpleDateFormat("MMM,dd hh:mm a", java.util.Locale.US);
    public static final java.lang.String SUMMARY = "Summary";
    public static final int SYNC_REQ = 128;
    public static final java.lang.String TICKET_ID = "ticketId";
    private static java.io.File lastSnapShotFile;
    private static boolean mIsInitialized = false;
    private static java.util.concurrent.PriorityBlockingQueue<java.io.File> mIssuesToSync = new java.util.concurrent.PriorityBlockingQueue<>(5, new com.navdy.hud.app.util.ReportIssueService.FilesModifiedTimeComparator());
    private static long mLastIssueSubmittedAt = 0;
    private static java.util.concurrent.atomic.AtomicBoolean mSyncing = new java.util.concurrent.atomic.AtomicBoolean(false);
    private static java.lang.String navigationIssuesFolder;
    /* access modifiers changed from: private */
    public static com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.util.ReportIssueService.class);
    @javax.inject.Inject
    com.squareup.otto.Bus bus;
    @javax.inject.Inject
    com.navdy.hud.app.debug.DriveRecorder driveRecorder;
    @javax.inject.Inject
    com.navdy.hud.app.gesture.GestureServiceConnector gestureService;
    @javax.inject.Inject
    com.navdy.service.library.network.http.IHttpManager mHttpManager;
    com.navdy.service.library.network.http.services.JiraClient mJiraClient;

    class Anon1 implements com.navdy.service.library.network.http.services.JiraClient.ResultCallback {
        final /* synthetic */ java.io.File val$fileToSync;

        Anon1(java.io.File file) {
            this.val$fileToSync = file;
        }

        public void onSuccess(java.lang.Object object) {
            com.navdy.hud.app.util.ReportIssueService.sLogger.d("Files attached successfully");
            com.navdy.hud.app.util.ReportIssueService.this.onSyncComplete(this.val$fileToSync);
        }

        public void onError(java.lang.Throwable t) {
            com.navdy.hud.app.util.ReportIssueService.sLogger.e("onError, error uploading the files ", t);
            com.navdy.hud.app.util.ReportIssueService.this.syncLater();
        }
    }

    class Anon2 implements com.navdy.service.library.network.http.services.JiraClient.ResultCallback {
        final /* synthetic */ java.lang.String val$assigneeEmail;
        final /* synthetic */ java.lang.String val$assigneeName;
        final /* synthetic */ java.io.File val$attachmentFile;
        final /* synthetic */ java.io.File val$fileToSync;
        final /* synthetic */ org.json.JSONObject val$jsonObject;

        class Anon1 implements com.navdy.service.library.network.http.services.JiraClient.ResultCallback {

            /* renamed from: com.navdy.hud.app.util.ReportIssueService$Anon2$Anon1$Anon1 reason: collision with other inner class name */
            class C0036Anon1 implements com.navdy.service.library.network.http.services.JiraClient.ResultCallback {
                C0036Anon1() {
                }

                public void onSuccess(java.lang.Object object) {
                    com.navdy.hud.app.util.ReportIssueService.sLogger.d("Assigned the ticket successfully to " + object);
                    com.navdy.hud.app.util.ReportIssueService.this.onSyncComplete(com.navdy.hud.app.util.ReportIssueService.Anon2.this.val$fileToSync);
                }

                public void onError(java.lang.Throwable t) {
                    com.navdy.hud.app.util.ReportIssueService.sLogger.e("Error Assigning the ticket", t);
                    com.navdy.hud.app.util.ReportIssueService.this.onSyncComplete(com.navdy.hud.app.util.ReportIssueService.Anon2.this.val$fileToSync);
                }
            }

            Anon1() {
            }

            public void onSuccess(java.lang.Object object) {
                com.navdy.hud.app.util.ReportIssueService.sLogger.d("Files successfully attached, Assigning the ticket ot " + com.navdy.hud.app.util.ReportIssueService.Anon2.this.val$assigneeName + ", Email : " + com.navdy.hud.app.util.ReportIssueService.Anon2.this.val$assigneeEmail);
                com.navdy.hud.app.util.ReportIssueService.this.mJiraClient.assignTicketForName((java.lang.String) object, com.navdy.hud.app.util.ReportIssueService.Anon2.this.val$assigneeName, com.navdy.hud.app.util.ReportIssueService.Anon2.this.val$assigneeEmail, new com.navdy.hud.app.util.ReportIssueService.Anon2.Anon1.C0036Anon1());
            }

            public void onError(java.lang.Throwable t) {
                com.navdy.hud.app.util.ReportIssueService.this.syncLater();
            }
        }

        Anon2(org.json.JSONObject jSONObject, java.io.File file, java.io.File file2, java.lang.String str, java.lang.String str2) {
            this.val$jsonObject = jSONObject;
            this.val$fileToSync = file;
            this.val$attachmentFile = file2;
            this.val$assigneeName = str;
            this.val$assigneeEmail = str2;
        }

        public void onSuccess(java.lang.Object object) {
            java.lang.String ticketId = null;
            if (object instanceof java.lang.String) {
                ticketId = (java.lang.String) object;
                com.navdy.hud.app.util.ReportIssueService.sLogger.d("Issue reported " + ticketId);
            }
            try {
                this.val$jsonObject.put(com.navdy.hud.app.util.ReportIssueService.TICKET_ID, ticketId);
                java.lang.String data = this.val$jsonObject.toString();
                java.io.FileWriter writer = null;
                try {
                    java.io.FileWriter writer2 = new java.io.FileWriter(this.val$fileToSync);
                    try {
                        writer2.write(data);
                        writer2.close();
                        com.navdy.service.library.util.IOUtils.closeStream(writer2);
                        java.io.FileWriter fileWriter = writer2;
                    } catch (java.io.IOException e) {
                        e = e;
                        writer = writer2;
                        try {
                            com.navdy.hud.app.util.ReportIssueService.sLogger.e("Error while dumping the issue " + e.getMessage(), e);
                            com.navdy.service.library.util.IOUtils.closeStream(writer);
                            com.navdy.hud.app.util.ReportIssueService.sLogger.d("Attaching files to the submitted ticket");
                            com.navdy.hud.app.util.ReportIssueService.this.attachFilesToTheJiraTicket(ticketId, this.val$attachmentFile, new com.navdy.hud.app.util.ReportIssueService.Anon2.Anon1());
                        } catch (Throwable th) {
                            th = th;
                            com.navdy.service.library.util.IOUtils.closeStream(writer);
                            throw th;
                        }
                    } catch (Throwable th2) {
                        th = th2;
                        writer = writer2;
                        com.navdy.service.library.util.IOUtils.closeStream(writer);
                        throw th;
                    }
                } catch (java.io.IOException e2) {
                    e = e2;
                    com.navdy.hud.app.util.ReportIssueService.sLogger.e("Error while dumping the issue " + e.getMessage(), e);
                    com.navdy.service.library.util.IOUtils.closeStream(writer);
                    com.navdy.hud.app.util.ReportIssueService.sLogger.d("Attaching files to the submitted ticket");
                    com.navdy.hud.app.util.ReportIssueService.this.attachFilesToTheJiraTicket(ticketId, this.val$attachmentFile, new com.navdy.hud.app.util.ReportIssueService.Anon2.Anon1());
                }
            } catch (org.json.JSONException e3) {
                com.navdy.hud.app.util.ReportIssueService.sLogger.e("JSONException while writing the meta data to the file");
            }
            com.navdy.hud.app.util.ReportIssueService.sLogger.d("Attaching files to the submitted ticket");
            com.navdy.hud.app.util.ReportIssueService.this.attachFilesToTheJiraTicket(ticketId, this.val$attachmentFile, new com.navdy.hud.app.util.ReportIssueService.Anon2.Anon1());
        }

        public void onError(java.lang.Throwable t) {
            com.navdy.hud.app.util.ReportIssueService.sLogger.e("Error during sync " + t);
            com.navdy.hud.app.util.ReportIssueService.this.syncLater();
        }
    }

    class Anon3 implements com.navdy.service.library.network.http.services.JiraClient.ResultCallback {
        final /* synthetic */ java.io.File val$fileToSync;

        Anon3(java.io.File file) {
            this.val$fileToSync = file;
        }

        public void onSuccess(java.lang.Object object) {
            if (object instanceof java.lang.String) {
                com.navdy.hud.app.util.ReportIssueService.sLogger.d("Issue reported " + object);
            }
            com.navdy.hud.app.util.ReportIssueService.sLogger.d("Sync succeeded");
            com.navdy.hud.app.util.ReportIssueService.this.onSyncComplete(this.val$fileToSync);
        }

        public void onError(java.lang.Throwable t) {
            com.navdy.hud.app.util.ReportIssueService.sLogger.e("Error during sync " + t);
            com.navdy.hud.app.util.ReportIssueService.this.syncLater();
        }
    }

    static class Anon4 implements java.util.Comparator<java.io.File> {
        Anon4() {
        }

        public int compare(java.io.File o1, java.io.File o2) {
            return java.lang.Long.compare(o1.lastModified(), o2.lastModified());
        }
    }

    static class FilesModifiedTimeComparator implements java.util.Comparator<java.io.File> {
        FilesModifiedTimeComparator() {
        }

        public int compare(java.io.File file, java.io.File t1) {
            return (int) (file.lastModified() - t1.lastModified());
        }
    }

    public enum IssueType {
        INEFFICIENT_ROUTE_ETA_TRAFFIC(com.navdy.hud.app.R.string.eta_inaccurate_traffic, 0, 107),
        INEFFICIENT_ROUTE_SELECTED(com.navdy.hud.app.R.string.route_inefficient, 0, com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient.EVENT_PULL_VCARD_LISTING_SIZE_ERROR),
        ROAD_CLOSED(com.navdy.hud.app.R.string.road_closed, com.navdy.hud.app.R.string.road_closed_message, com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient.EVENT_PULL_VCARD_LISTING_ERROR),
        WRONG_DIRECTION(com.navdy.hud.app.R.string.wrong_direction, 0, 102),
        ROAD_NAME(com.navdy.hud.app.R.string.road_name, 0, com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient.EVENT_PULL_VCARD_ENTRY_ERROR),
        ROAD_CLOSED_PERMANENT(com.navdy.hud.app.R.string.permanent_closure, 0, 108),
        OTHER(com.navdy.hud.app.R.string.other_navigation_issue, 0, 105);
        
        private int issueTypeCode;
        private int messageStringResource;
        private int titleStringResource;

        private IssueType(int title, int message, int id) {
            this.titleStringResource = title;
            this.messageStringResource = message;
            this.issueTypeCode = id;
        }

        public int getTitleStringResource() {
            return this.titleStringResource;
        }

        public int getMessageStringResource() {
            return this.messageStringResource;
        }

        public int getIssueTypeCode() {
            return this.issueTypeCode;
        }
    }

    public ReportIssueService() {
        super(NAME);
    }

    public void onCreate() {
        super.onCreate();
        mortar.Mortar.inject(com.navdy.hud.app.HudApplication.getAppContext(), this);
        sLogger.d("ReportIssue service started");
        this.mJiraClient = new com.navdy.service.library.network.http.services.JiraClient(this.mHttpManager.getClientCopy().readTimeout(120, java.util.concurrent.TimeUnit.SECONDS).connectTimeout(120, java.util.concurrent.TimeUnit.SECONDS).build());
        this.mJiraClient.setEncodedCredentials(com.navdy.service.library.util.CredentialUtil.getCredentials(this, JIRA_CREDENTIALS_META));
    }

    /* access modifiers changed from: protected */
    public void onHandleIntent(android.content.Intent intent) {
        try {
            sLogger.d("onHandleIntent " + intent.toString());
            if (ACTION_DUMP.equals(intent.getAction())) {
                com.navdy.hud.app.util.ReportIssueService.IssueType issueType = (com.navdy.hud.app.util.ReportIssueService.IssueType) intent.getSerializableExtra(EXTRA_ISSUE_TYPE);
                sLogger.d("Dumping an issue " + issueType);
                if (issueType != null) {
                    dumpIssue(issueType);
                }
            } else if (ACTION_SNAPSHOT.equals(intent.getAction())) {
                dumpSnapshot();
            } else {
                if (ACTION_SUBMIT_JIRA.equals(intent.getAction())) {
                    java.lang.String title = intent.getStringExtra(EXTRA_SNAPSHOT_TITLE);
                    if (android.text.TextUtils.isEmpty(title)) {
                        title = HUD_PROJECT;
                    }
                    submitJiraTicket(title);
                    return;
                }
                sLogger.d("Handling Sync request");
                sync();
            }
        } catch (Throwable t) {
            sLogger.e("Exception while handling intent ", t);
        }
    }

    private void submitJiraTicket(java.lang.String title) {
        sLogger.d("Submitting jira ticket with title " + title);
        try {
            java.util.Date date = new java.util.Date(java.lang.System.currentTimeMillis());
            com.navdy.hud.app.service.ConnectionHandler connectionHandler = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getConnectionHandler();
            org.json.JSONObject jsonObject = new org.json.JSONObject();
            jsonObject.put(ATTACHMENT, lastSnapShotFile.getAbsolutePath());
            java.lang.StringBuilder subjectBuilder = new java.lang.StringBuilder();
            com.navdy.hud.app.ui.framework.UIStateManager uiStateManager = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getUiStateManager();
            java.lang.String timeStamp = SNAPSHOT_JIRA_TICKET_DATE_FORMAT.format(date);
            com.navdy.hud.app.profile.DriverProfile driverProfile = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile();
            subjectBuilder.append("[Snapshot " + title + " " + timeStamp + "] " + (com.navdy.hud.app.ui.component.homescreen.HomeScreen.DisplayMode.MAP.equals(uiStateManager.getHomescreenView().getDisplayMode()) ? "Map" : "Dash"));
            java.lang.String roadName = com.navdy.hud.app.maps.here.HereMapUtil.getCurrentRoadName();
            if (!android.text.TextUtils.isEmpty(roadName)) {
                subjectBuilder.append(" ").append(roadName);
            }
            if (!driverProfile.isDefaultProfile()) {
                subjectBuilder.append(" , From ").append(driverProfile.getDriverEmail());
            }
            jsonObject.put(SUMMARY, subjectBuilder.toString());
            if (!driverProfile.isDefaultProfile()) {
                java.lang.String driverName = driverProfile.getDriverName();
                if (!android.text.TextUtils.isEmpty(driverName)) {
                    java.lang.String assigneeName = driverName.split(" ")[0].trim();
                    sLogger.d("Assignee name for the snapshot " + assigneeName);
                    jsonObject.put("assignee", assigneeName);
                }
                java.lang.String email = driverProfile.getDriverEmail();
                if (!android.text.TextUtils.isEmpty(email)) {
                    jsonObject.put(ASSIGNEE_EMAIL, email);
                }
            }
            java.lang.StringBuilder environmentBuilder = new java.lang.StringBuilder();
            environmentBuilder.append("HUD " + android.os.Build.VERSION.INCREMENTAL + com.navdy.hud.app.framework.glance.GlanceConstants.NEWLINE);
            if (connectionHandler != null) {
                com.navdy.service.library.events.DeviceInfo lastConnectedDeviceInfo = connectionHandler.getLastConnectedDeviceInfo();
                if (lastConnectedDeviceInfo != null) {
                    environmentBuilder.append(com.navdy.service.library.events.DeviceInfo.Platform.PLATFORM_iOS.equals(lastConnectedDeviceInfo.platform) ? "iPhone" : com.navdy.hud.app.analytics.AnalyticsSupport.ATTR_PLATFORM_ANDROID);
                    environmentBuilder.append(" V" + lastConnectedDeviceInfo.clientVersion + com.navdy.hud.app.framework.glance.GlanceConstants.NEWLINE);
                }
            }
            environmentBuilder.append("Language : " + com.navdy.hud.app.profile.HudLocale.getCurrentLocale(com.navdy.hud.app.HudApplication.getAppContext()).getLanguage());
            jsonObject.put(ENVIRONMENT, environmentBuilder.toString());
            saveJiraTicketDescriptionToFile(jsonObject.toString(), true);
        } catch (org.json.JSONException e) {
            e.printStackTrace();
        }
    }

    private void dumpSnapshot() {
        sLogger.d("Creating the snapshot of the HUD for support ticket");
        com.navdy.hud.app.device.light.HUDLightUtils.showSnapshotCollection(com.navdy.hud.app.HudApplication.getAppContext(), com.navdy.hud.app.device.light.LightManager.getInstance(), false);
        java.lang.String folder = com.navdy.hud.app.storage.PathManager.getInstance().getNonFatalCrashReportDir();
        com.navdy.hud.app.util.DeviceUtil.takeDeviceScreenShot(folder + java.io.File.separator + "screen.png");
        java.util.ArrayList<java.io.File> arrayList = new java.util.ArrayList<>();
        if (android.os.Build.TYPE.equals("eng")) {
            this.gestureService.dumpRecording();
        }
        this.driveRecorder.flushRecordings();
        this.gestureService.takeSnapShot(com.navdy.hud.app.gesture.GestureServiceConnector.SNAPSHOT_PATH);
        java.io.File deviceInfoTextFile = new java.io.File(folder + java.io.File.separator + "deviceInfo.txt");
        if (deviceInfoTextFile.exists()) {
            com.navdy.service.library.util.IOUtils.deleteFile(this, deviceInfoTextFile.getAbsolutePath());
        }
        java.io.FileWriter fileWriter = new java.io.FileWriter(deviceInfoTextFile);
        com.navdy.hud.app.service.ConnectionHandler connectionHandler = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getConnectionHandler();
        if (connectionHandler != null) {
            com.navdy.service.library.events.DeviceInfo lastConnectedDeviceInfo = connectionHandler.getLastConnectedDeviceInfo();
            if (lastConnectedDeviceInfo != null) {
                fileWriter.write(com.navdy.hud.app.util.CrashReportService.printDeviceInfo(lastConnectedDeviceInfo));
            }
        }
        fileWriter.flush();
        try {
            fileWriter.close();
        } catch (java.io.IOException e) {
            sLogger.e("Error closing the file writer for log file", e);
        } catch (Throwable t) {
            try {
                sLogger.e("Error creating a snapshot ", t);
                return;
            } finally {
                com.navdy.hud.app.device.light.HUDLightUtils.showSnapshotCollection(com.navdy.hud.app.HudApplication.getAppContext(), com.navdy.hud.app.device.light.LightManager.getInstance(), true);
            }
        }
        java.io.File file = new java.io.File(folder + java.io.File.separator + "temp");
        if (file.exists()) {
            com.navdy.service.library.util.IOUtils.deleteDirectory(this, file);
        }
        if (!file.mkdirs()) {
            sLogger.e("could not create tempDirectory");
            com.navdy.hud.app.device.light.HUDLightUtils.showSnapshotCollection(com.navdy.hud.app.HudApplication.getAppContext(), com.navdy.hud.app.device.light.LightManager.getInstance(), true);
            return;
        }
        java.lang.String stagingPath = file.getAbsolutePath();
        com.navdy.service.library.util.LogUtils.copySnapshotSystemLogs(stagingPath);
        com.navdy.hud.app.device.gps.GpsDeadReckoningManager.getInstance().dumpGpsInfo(stagingPath);
        com.navdy.hud.app.storage.PathManager.getInstance().collectEnvironmentInfo(stagingPath);
        java.io.File[] logFiles = file.listFiles();
        java.io.File file2 = new java.io.File(folder + java.io.File.separator + "route.log");
        if (file2.exists()) {
            com.navdy.service.library.util.IOUtils.deleteFile(this, file2.getAbsolutePath());
        }
        java.lang.String routeDescription = buildDescription();
        java.io.FileWriter fileWriter2 = new java.io.FileWriter(file2);
        fileWriter2.write(routeDescription);
        fileWriter2.flush();
        try {
            fileWriter2.close();
        } catch (java.io.IOException e2) {
            sLogger.e("Error closing the file writer for route log file", e2);
        }
        arrayList.add(new java.io.File(com.navdy.hud.app.gesture.GestureServiceConnector.SNAPSHOT_PATH));
        arrayList.add(new java.io.File(folder + java.io.File.separator + "screen.png"));
        arrayList.add(file2);
        arrayList.add(deviceInfoTextFile);
        if (logFiles != null) {
            java.util.Collections.addAll(arrayList, logFiles);
        }
        java.util.HashSet<java.io.File> driveLogFilesSet = new java.util.HashSet<>();
        java.util.List<java.io.File> latestFiles = getLatestDriveLogFiles(1);
        if (latestFiles != null && latestFiles.size() > 0) {
            arrayList.addAll(latestFiles);
            for (java.io.File latestFile : latestFiles) {
                driveLogFilesSet.add(latestFile);
            }
        }
        java.io.File file3 = new java.io.File(folder + java.io.File.separator + ("snapshot-" + android.os.Build.SERIAL + "-" + android.os.Build.VERSION.INCREMENTAL + com.navdy.service.library.util.MusicDataUtils.ALTERNATE_SEPARATOR + SNAPSHOT_DATE_FORMAT.format(new java.util.Date(java.lang.System.currentTimeMillis())) + ".zip"));
        if (file3.exists()) {
            com.navdy.service.library.util.IOUtils.deleteFile(com.navdy.hud.app.HudApplication.getAppContext(), file3.getAbsolutePath());
        }
        java.io.File[] files = new java.io.File[arrayList.size()];
        arrayList.toArray(files);
        if (!file3.createNewFile()) {
            sLogger.e("snapshot file could not be created");
            com.navdy.hud.app.device.light.HUDLightUtils.showSnapshotCollection(com.navdy.hud.app.HudApplication.getAppContext(), com.navdy.hud.app.device.light.LightManager.getInstance(), true);
            return;
        }
        com.navdy.service.library.util.IOUtils.compressFilesToZip(this, files, file3.getAbsolutePath());
        for (java.io.File file4 : arrayList) {
            if (!driveLogFilesSet.contains(file4)) {
                com.navdy.service.library.util.IOUtils.deleteFile(com.navdy.hud.app.HudApplication.getAppContext(), file4.getAbsolutePath());
            }
        }
        com.navdy.service.library.util.IOUtils.deleteDirectory(this, file);
        lastSnapShotFile = file3;
        sLogger.d("Snapshot File created " + lastSnapShotFile);
        com.navdy.hud.app.util.CrashReportService.addSnapshotAsync(file3.getAbsolutePath());
        com.navdy.hud.app.device.light.HUDLightUtils.showSnapshotCollection(com.navdy.hud.app.HudApplication.getAppContext(), com.navdy.hud.app.device.light.LightManager.getInstance(), true);
    }

    private void dumpIssue(com.navdy.hud.app.util.ReportIssueService.IssueType issueType) {
        if (com.navdy.hud.app.maps.here.HereMapsManager.getInstance().isNavigationModeOn()) {
            java.lang.StringBuilder descriptionBuilder = new java.lang.StringBuilder();
            descriptionBuilder.append(issueType.ordinal() + com.navdy.hud.app.framework.glance.GlanceConstants.NEWLINE);
            descriptionBuilder.append(buildDescription());
            saveJiraTicketDescriptionToFile(descriptionBuilder.toString(), false);
            mLastIssueSubmittedAt = java.lang.System.currentTimeMillis();
        }
    }

    private void saveJiraTicketDescriptionToFile(java.lang.String data, boolean isJson) {
        java.io.File file = new java.io.File(navigationIssuesFolder + java.io.File.separator + DATE_FORMAT.format(new java.util.Date(java.lang.System.currentTimeMillis())) + (isJson ? ".json" : ""));
        if (file.exists()) {
            com.navdy.service.library.util.IOUtils.deleteFile(this, file.getAbsolutePath());
        }
        java.io.FileWriter writer = null;
        try {
            if (file.createNewFile()) {
                java.io.FileWriter writer2 = new java.io.FileWriter(file);
                try {
                    writer2.write(data);
                    writer2.close();
                    mIssuesToSync.add(file);
                    writer = writer2;
                    dispatchSync();
                } catch (java.io.IOException e) {
                    e = e;
                    writer = writer2;
                    try {
                        sLogger.e("Error while dumping the issue " + e.getMessage(), e);
                        com.navdy.service.library.util.IOUtils.closeStream(writer);
                    } catch (Throwable th) {
                        th = th;
                        com.navdy.service.library.util.IOUtils.closeStream(writer);
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    writer = writer2;
                    com.navdy.service.library.util.IOUtils.closeStream(writer);
                    throw th;
                }
            }
            com.navdy.service.library.util.IOUtils.closeStream(writer);
        } catch (java.io.IOException e2) {
            e = e2;
            sLogger.e("Error while dumping the issue " + e.getMessage(), e);
            com.navdy.service.library.util.IOUtils.closeStream(writer);
        }
    }

    /* access modifiers changed from: private */
    public void onSyncComplete(java.io.File ticketFile) {
        if (ticketFile != null) {
            sLogger.d("Removed the file from list :" + mIssuesToSync.remove(ticketFile));
            com.navdy.service.library.util.IOUtils.deleteFile(this, ticketFile.getAbsolutePath());
            dispatchSync();
        }
        mSyncing.set(false);
    }

    /* access modifiers changed from: private */
    public void syncLater() {
        mSyncing.set(false);
        scheduleSync();
    }

    private void sync() {
        if (!com.navdy.hud.app.framework.network.NetworkBandwidthController.getInstance().isNetworkAccessAllowed(com.navdy.hud.app.framework.network.NetworkBandwidthController.Component.JIRA)) {
            scheduleSync();
            return;
        }
        boolean deviceConnected = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().isRemoteDeviceConnected();
        boolean internetConnected = com.navdy.hud.app.framework.network.NetworkStateManager.isConnectedToNetwork(this);
        sLogger.d("Trying to sync , already syncing:" + mSyncing.get() + ", Navigation mode on:" + com.navdy.hud.app.maps.here.HereMapsManager.getInstance().isNavigationModeOn() + ", Device connected:" + deviceConnected + ", Internet connected :" + internetConnected);
        if (!deviceConnected || !internetConnected) {
            sLogger.d("Already sync is running, scheduling for a later time");
            scheduleSync();
        } else if (mSyncing.compareAndSet(false, true)) {
            java.io.File fileToSync = (java.io.File) mIssuesToSync.peek();
            if (fileToSync == null || !fileToSync.exists()) {
                sLogger.d("Ticket file does not exist anymore");
                onSyncComplete(fileToSync);
            } else if (fileToSync.getName().endsWith(".json")) {
                try {
                    sLogger.d("Submitting ticket from JSON file");
                    org.json.JSONObject jsonObject = new org.json.JSONObject(com.navdy.service.library.util.IOUtils.convertFileToString(fileToSync.getAbsolutePath()));
                    java.lang.String summary = jsonObject.getString(SUMMARY);
                    java.lang.String environment = jsonObject.has(ENVIRONMENT) ? jsonObject.getString(ENVIRONMENT) : "";
                    java.lang.String assigneedNameTemp = jsonObject.has("assignee") ? jsonObject.getString("assignee") : "";
                    java.lang.String assigneeEmailTemp = jsonObject.has(ASSIGNEE_EMAIL) ? jsonObject.getString(ASSIGNEE_EMAIL) : "";
                    if (android.text.TextUtils.isEmpty(assigneedNameTemp)) {
                        com.navdy.hud.app.profile.DriverProfile profile = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile();
                        java.lang.String driverName = profile.getDriverName();
                        if (!android.text.TextUtils.isEmpty(driverName)) {
                            assigneedNameTemp = driverName.trim().split(" ")[0];
                        }
                        assigneeEmailTemp = profile.getDriverEmail();
                    }
                    java.lang.String assigneeName = assigneedNameTemp;
                    java.lang.String assigneeEmail = assigneeEmailTemp;
                    sLogger.d("Assignee name " + assigneeName);
                    java.lang.String attachment = jsonObject.has(ATTACHMENT) ? jsonObject.getString(ATTACHMENT) : "";
                    sLogger.d("Attachment " + attachment);
                    java.io.File attachmentFile = new java.io.File(attachment);
                    if (!attachmentFile.exists()) {
                        sLogger.e("Snapshot file has been deleted, not creating a ticket ");
                        onSyncComplete(fileToSync);
                        return;
                    }
                    java.lang.String ticketId = jsonObject.has(TICKET_ID) ? jsonObject.getString(TICKET_ID) : "";
                    if (!android.text.TextUtils.isEmpty(ticketId)) {
                        sLogger.d("Ticket already exists " + ticketId + " Attaching the files to the ticket");
                        attachFilesToTheJiraTicket(ticketId, attachmentFile, new com.navdy.hud.app.util.ReportIssueService.Anon1(fileToSync));
                        return;
                    }
                    sLogger.d("Submitting a new ticket from JSON file");
                    this.mJiraClient.submitTicket(HUD_PROJECT, ISSUE_TYPE, summary, "Snapshot, fill in the details", environment, new com.navdy.hud.app.util.ReportIssueService.Anon2(jsonObject, fileToSync, attachmentFile, assigneeName, assigneeEmail));
                } catch (java.io.IOException e) {
                    sLogger.e("Not able to read json file ", e);
                    syncLater();
                } catch (org.json.JSONException je) {
                    sLogger.e("Not able to read json file ", je);
                    syncLater();
                }
            } else {
                java.io.FileReader reader = null;
                try {
                    java.io.FileReader fileReader = new java.io.FileReader(fileToSync);
                    try {
                        java.io.BufferedReader bufferedReader = new java.io.BufferedReader(fileReader);
                        com.navdy.hud.app.util.ReportIssueService.IssueType issueType = getIssueTypeForId(java.lang.Integer.parseInt(bufferedReader.readLine().trim()));
                        if (issueType == null) {
                            throw new java.io.IOException("Bad file format");
                        }
                        java.lang.String summary2 = getSummary(issueType);
                        java.lang.StringBuilder builder = new java.lang.StringBuilder();
                        while (true) {
                            java.lang.String line = bufferedReader.readLine();
                            if (line != null) {
                                builder.append(line).append(com.navdy.hud.app.framework.glance.GlanceConstants.NEWLINE);
                            } else {
                                com.navdy.service.library.util.IOUtils.closeStream(fileReader);
                                java.lang.String description = builder.toString();
                                com.navdy.service.library.network.http.services.JiraClient jiraClient = this.mJiraClient;
                                java.lang.String str = PROJECT_NAME;
                                java.lang.String str2 = ISSUE_TYPE;
                                com.navdy.hud.app.util.ReportIssueService.Anon3 anon3 = new com.navdy.hud.app.util.ReportIssueService.Anon3(fileToSync);
                                jiraClient.submitTicket(str, str2, summary2, description, anon3);
                                java.io.FileReader fileReader2 = fileReader;
                                return;
                            }
                        }
                    } catch (java.lang.Exception e2) {
                        reader = fileReader;
                        com.navdy.service.library.util.IOUtils.closeStream(reader);
                        onSyncComplete(fileToSync);
                    }
                } catch (java.lang.Exception e3) {
                    com.navdy.service.library.util.IOUtils.closeStream(reader);
                    onSyncComplete(fileToSync);
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void attachFilesToTheJiraTicket(java.lang.String ticket, java.io.File snapShotFile, com.navdy.service.library.network.http.services.JiraClient.ResultCallback callback) {
        if (snapShotFile.exists() && snapShotFile.getName().endsWith(".zip")) {
            java.io.File file = new java.io.File(snapShotFile.getParentFile(), "Staging");
            if (file.exists()) {
                if (file.isDirectory()) {
                    com.navdy.service.library.util.IOUtils.deleteDirectory(com.navdy.hud.app.HudApplication.getAppContext(), file);
                } else {
                    com.navdy.service.library.util.IOUtils.deleteFile(com.navdy.hud.app.HudApplication.getAppContext(), file.getAbsolutePath());
                }
            }
            boolean mkdir = file.mkdir();
            try {
                com.navdy.service.library.util.IOUtils.deCompressZipToDirectory(com.navdy.hud.app.HudApplication.getAppContext(), snapShotFile, file);
                java.io.File[] decompressedFiles = file.listFiles();
                java.util.ArrayList<java.io.File> logFiles = new java.util.ArrayList<>();
                java.util.ArrayList<java.io.File> pngFiles = new java.util.ArrayList<>();
                int length = decompressedFiles.length;
                for (int i = 0; i < length; i++) {
                    java.io.File file2 = decompressedFiles[i];
                    if (file2.getName().endsWith(".png")) {
                        pngFiles.add(file2);
                    } else {
                        logFiles.add(file2);
                    }
                }
                java.io.File[] logFilesArray = (java.io.File[]) logFiles.toArray(new java.io.File[logFiles.size()]);
                java.io.File logFilesZip = new java.io.File(file, "logs.zip");
                com.navdy.service.library.util.IOUtils.compressFilesToZip(com.navdy.hud.app.HudApplication.getAppContext(), logFilesArray, logFilesZip.getAbsolutePath());
                java.util.ArrayList<com.navdy.service.library.network.http.services.JiraClient.Attachment> attachments = new java.util.ArrayList<>();
                com.navdy.service.library.network.http.services.JiraClient.Attachment logFilesZipAttachment = new com.navdy.service.library.network.http.services.JiraClient.Attachment();
                logFilesZipAttachment.filePath = logFilesZip.getAbsolutePath();
                logFilesZipAttachment.mimeType = com.navdy.service.library.network.http.HttpUtils.MIME_TYPE_ZIP;
                attachments.add(logFilesZipAttachment);
                java.util.Iterator it = pngFiles.iterator();
                while (it.hasNext()) {
                    java.io.File pngFile = (java.io.File) it.next();
                    com.navdy.service.library.network.http.services.JiraClient.Attachment pngFileAttachment = new com.navdy.service.library.network.http.services.JiraClient.Attachment();
                    pngFileAttachment.filePath = pngFile.getAbsolutePath();
                    pngFileAttachment.mimeType = com.navdy.service.library.network.http.HttpUtils.MIME_TYPE_IMAGE_PNG;
                    attachments.add(pngFileAttachment);
                }
                this.mJiraClient.attachFilesToTicket(ticket, attachments, callback);
            } catch (java.io.IOException e) {
                sLogger.e("Error decompressing the zip file to get the attachments", e);
            }
        }
    }

    private java.lang.String getSummary(com.navdy.hud.app.util.ReportIssueService.IssueType issue) {
        if (issue.getMessageStringResource() != 0) {
            return "Navigation issue: [" + issue.getIssueTypeCode() + "] " + getString(issue.getTitleStringResource()) + ": " + getString(issue.getMessageStringResource());
        }
        return "Navigation issue: [" + issue.getIssueTypeCode() + "] " + getString(issue.getTitleStringResource());
    }

    private java.lang.String buildDescription() {
        java.lang.String userName;
        java.lang.String userEmail;
        com.navdy.service.library.device.NavdyDeviceId displayDeviceId = com.navdy.service.library.device.NavdyDeviceId.getThisDevice(this);
        java.lang.StringBuilder builder = new java.lang.StringBuilder();
        builder.append("\n#############\n");
        builder.append("HERE metadata: ");
        builder.append("\n#############\n\n");
        java.lang.String prettyJsonString = "";
        java.lang.String hereSdkVersion = com.navdy.hud.app.util.DeviceUtil.getCurrentHereSdkVersion();
        java.lang.String metaData = com.navdy.hud.app.util.DeviceUtil.getHEREMapsDataInfo();
        if (metaData != null) {
            try {
                prettyJsonString = new com.google.gson.GsonBuilder().setPrettyPrinting().create().toJson(new com.google.gson.JsonParser().parse(metaData));
            } catch (Throwable t) {
                sLogger.e(t);
            }
        }
        if (!android.text.TextUtils.isEmpty(hereSdkVersion)) {
            builder.append("HERE mSDK for Android v").append(hereSdkVersion).append(com.navdy.hud.app.framework.glance.GlanceConstants.NEWLINE);
        }
        if (!android.text.TextUtils.isEmpty(prettyJsonString)) {
            builder.append("HERE Map Data:\n").append(metaData).append(com.navdy.hud.app.framework.glance.GlanceConstants.NEWLINE);
        }
        builder.append("\n#############\n");
        builder.append("Navigation details:");
        builder.append("\n#############\n\n");
        com.here.android.mpa.common.GeoPosition position = com.navdy.hud.app.maps.here.HereMapsManager.getInstance().getLastGeoPosition();
        if (position == null || position.getCoordinate() == null) {
            builder.append("Current Location: N/A\n");
        } else {
            builder.append("Current Location: ").append(position.getCoordinate().getLatitude()).append(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA).append(position.getCoordinate().getLongitude()).append(com.navdy.hud.app.framework.glance.GlanceConstants.NEWLINE);
        }
        builder.append("Current Road: ").append(com.navdy.hud.app.maps.here.HereMapUtil.getCurrentRoadName()).append(com.navdy.hud.app.framework.glance.GlanceConstants.NEWLINE);
        com.here.android.mpa.common.RoadElement roadElement = com.navdy.hud.app.maps.here.HereMapUtil.getCurrentRoadElement();
        if (roadElement != null) {
            builder.append("Current road element ID: ").append(roadElement.getIdentifier()).append(com.navdy.hud.app.framework.glance.GlanceConstants.NEWLINE);
        }
        com.navdy.hud.app.framework.DriverProfileHelper driverProfileHelper = com.navdy.hud.app.framework.DriverProfileHelper.getInstance();
        com.navdy.hud.app.profile.DriverProfile currentProfile = driverProfileHelper.getCurrentProfile();
        if (com.navdy.hud.app.maps.here.HereMapsManager.getInstance().isNavigationModeOn()) {
            com.navdy.hud.app.maps.here.HereNavigationManager hereNavigationManager = com.navdy.hud.app.maps.here.HereNavigationManager.getInstance();
            com.here.android.mpa.routing.Route route = hereNavigationManager.getCurrentRoute();
            com.navdy.hud.app.maps.here.HereNavController hereNavController = hereNavigationManager.getNavController();
            if (route != null) {
                com.here.android.mpa.routing.RouteTta routeTta = hereNavController.getTta(com.here.android.mpa.routing.Route.TrafficPenaltyMode.OPTIMAL, true);
                com.here.android.mpa.common.GeoCoordinate startingPoint = hereNavigationManager.getStartLocation();
                if (startingPoint != null) {
                    builder.append("Route Starting point: ").append(startingPoint.getLatitude()).append(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA).append(startingPoint.getLongitude()).append(com.navdy.hud.app.framework.glance.GlanceConstants.NEWLINE);
                } else {
                    builder.append("Route Starting point: N/A\n");
                }
                com.here.android.mpa.common.GeoCoordinate destination = route.getDestination();
                if (destination != null) {
                    builder.append("Route Ending point: ").append(destination.getLatitude()).append(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA).append(destination.getLongitude()).append(com.navdy.hud.app.framework.glance.GlanceConstants.NEWLINE);
                } else {
                    builder.append("Route Ending point: N/A\n");
                }
                builder.append("Traffic Used: ").append(hereNavigationManager.isTrafficConsidered(hereNavigationManager.getCurrentRouteId())).append(com.navdy.hud.app.framework.glance.GlanceConstants.NEWLINE);
                long tripStartedTimestamp = com.navdy.hud.app.analytics.NavigationQualityTracker.getInstance().getTripStartUtc();
                if (tripStartedTimestamp > 0) {
                    java.text.SimpleDateFormat dateFormat = new java.text.SimpleDateFormat("MMMM dd HH:mm:ss zzzz yyyy Z", java.util.Locale.US);
                    java.lang.StringBuilder append = builder.append("Trip started: ");
                    java.util.Date date = new java.util.Date(tripStartedTimestamp);
                    append.append(dateFormat.format(date)).append(com.navdy.hud.app.framework.glance.GlanceConstants.NEWLINE);
                }
                com.navdy.hud.app.analytics.NavigationQualityTracker navigationQualityTracker = com.navdy.hud.app.analytics.NavigationQualityTracker.getInstance();
                int durationSoFar = navigationQualityTracker.getActualDurationSoFar();
                int initialDuration = navigationQualityTracker.getExpectedDuration();
                int initialDistance = navigationQualityTracker.getExpectedDistance();
                long destinationDistance = hereNavController.getDestinationDistance();
                if (initialDuration > 0) {
                    builder.append("Expected initial ETA according to HERE: ").append(initialDuration).append(" seconds\n");
                }
                if (initialDistance > 0) {
                    builder.append("Expected initial distance according to HERE: ").append(initialDistance).append(" meters\n");
                }
                if (routeTta != null) {
                    builder.append("Time To Arrival remaining according to HERE: ").append(routeTta.getDuration()).append(" seconds\n");
                }
                if (destinationDistance > 0) {
                    builder.append("Distance remaining according to HERE: ").append(destinationDistance).append(" meters\n");
                }
                if (durationSoFar > 0) {
                    builder.append("Duration of the trip so far: ").append(durationSoFar).append(" seconds\n");
                }
            }
            java.lang.String destinationLabel = hereNavigationManager.getDestinationLabel();
            builder.append("Destination: ").append(destinationLabel).append(", ").append(hereNavigationManager.getDestinationStreetAddress()).append(com.navdy.hud.app.framework.glance.GlanceConstants.NEWLINE);
            builder.append("\n#############\n");
            builder.append("User route preferences:");
            builder.append("\n#############\n\n");
            com.navdy.service.library.events.preferences.NavigationPreferences userNavPrefs = currentProfile.getNavigationPreferences();
            builder.append("Route calculation: ").append(userNavPrefs.routingType == com.navdy.service.library.events.preferences.NavigationPreferences.RoutingType.ROUTING_FASTEST ? "fastest" : "shortest").append(com.navdy.hud.app.framework.glance.GlanceConstants.NEWLINE);
            builder.append("Highways: ").append(userNavPrefs.allowHighways).append(com.navdy.hud.app.framework.glance.GlanceConstants.NEWLINE);
            builder.append("Toll roads: ").append(userNavPrefs.allowTollRoads).append(com.navdy.hud.app.framework.glance.GlanceConstants.NEWLINE);
            builder.append("Ferries: ").append(userNavPrefs.allowFerries).append(com.navdy.hud.app.framework.glance.GlanceConstants.NEWLINE);
            builder.append("Tunnels: ").append(userNavPrefs.allowTunnels).append(com.navdy.hud.app.framework.glance.GlanceConstants.NEWLINE);
            builder.append("Unpaved Roads: ").append(userNavPrefs.allowUnpavedRoads).append(com.navdy.hud.app.framework.glance.GlanceConstants.NEWLINE);
            builder.append("Auto Trains: ").append(userNavPrefs.allowAutoTrains).append(com.navdy.hud.app.framework.glance.GlanceConstants.NEWLINE);
            builder.append("\n#############\n");
            builder.append("Maneuvers (entire route):");
            builder.append("\n#############\n\n");
            com.navdy.hud.app.maps.here.HereMapUtil.printRouteDetails(route, destinationLabel, null, builder);
            builder.append("\n#############\n");
            builder.append("Waypoints (entire route):");
            builder.append("\n#############\n\n");
            int idx = 0;
            for (com.here.android.mpa.routing.Maneuver maneuver : route.getManeuvers()) {
                com.here.android.mpa.common.GeoCoordinate coordinate = maneuver.getCoordinate();
                if (coordinate != null) {
                    builder.append("waypoint").append(idx).append(": ").append(coordinate.getLatitude()).append(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA).append(coordinate.getLongitude()).append(com.navdy.hud.app.framework.glance.GlanceConstants.NEWLINE);
                    idx++;
                }
            }
            builder.append("\n#############\n");
            builder.append("Maneuvers (remaining route):");
            builder.append("\n#############\n\n");
            com.navdy.hud.app.maps.here.HereMapUtil.printRouteDetails(route, destinationLabel, null, builder, hereNavigationManager.getNextManeuver());
        }
        builder.append("\n#############\n");
        builder.append("Navdy info:");
        builder.append("\n#############\n\n");
        builder.append("Device ID: ").append(displayDeviceId.toString()).append(com.navdy.hud.app.framework.glance.GlanceConstants.NEWLINE);
        builder.append("HUD app version: 1.3.3080-bb1b512\n");
        builder.append("Serial number: ").append(android.os.Build.SERIAL).append(com.navdy.hud.app.framework.glance.GlanceConstants.NEWLINE);
        builder.append("Model: ").append(android.os.Build.MODEL).append(com.navdy.hud.app.framework.glance.GlanceConstants.NEWLINE);
        builder.append("API Level: ").append(android.os.Build.VERSION.SDK_INT).append(com.navdy.hud.app.framework.glance.GlanceConstants.NEWLINE);
        builder.append("Build type: ").append(android.os.Build.TYPE).append(com.navdy.hud.app.framework.glance.GlanceConstants.NEWLINE);
        if (!currentProfile.isDefaultProfile()) {
            userName = currentProfile.getDriverName();
            userEmail = currentProfile.getDriverEmail();
        } else {
            userName = driverProfileHelper.getDriverProfileManager().getLastUserName();
            userEmail = driverProfileHelper.getDriverProfileManager().getLastUserEmail();
        }
        if (!android.text.TextUtils.isEmpty(userName) && !android.text.TextUtils.isEmpty(userEmail)) {
            builder.append("Username: ").append(userName).append(com.navdy.hud.app.framework.glance.GlanceConstants.NEWLINE);
            builder.append("Email: ").append(userEmail).append(com.navdy.hud.app.framework.glance.GlanceConstants.NEWLINE);
        }
        return builder.toString();
    }

    public static com.navdy.hud.app.util.ReportIssueService.IssueType getIssueTypeForId(int id) {
        com.navdy.hud.app.util.ReportIssueService.IssueType[] values = com.navdy.hud.app.util.ReportIssueService.IssueType.values();
        if (id < 0 || id >= values.length) {
            return null;
        }
        return values[id];
    }

    public static void dispatchReportNewIssue(com.navdy.hud.app.util.ReportIssueService.IssueType issueType) {
        android.content.Intent intent = new android.content.Intent(com.navdy.hud.app.HudApplication.getAppContext(), com.navdy.hud.app.util.ReportIssueService.class);
        intent.setAction(ACTION_DUMP);
        intent.putExtra(EXTRA_ISSUE_TYPE, issueType);
        com.navdy.hud.app.HudApplication.getAppContext().startService(intent);
    }

    public static void dumpSnapshotAsync() {
        android.content.Intent intent = new android.content.Intent(com.navdy.hud.app.HudApplication.getAppContext(), com.navdy.hud.app.util.ReportIssueService.class);
        intent.setAction(ACTION_SNAPSHOT);
        com.navdy.hud.app.HudApplication.getAppContext().startService(intent);
    }

    public static void submitJiraTicketAsync(java.lang.String title) {
        android.content.Intent intent = new android.content.Intent(com.navdy.hud.app.HudApplication.getAppContext(), com.navdy.hud.app.util.ReportIssueService.class);
        intent.setAction(ACTION_SUBMIT_JIRA);
        intent.putExtra(EXTRA_SNAPSHOT_TITLE, title);
        com.navdy.hud.app.HudApplication.getAppContext().startService(intent);
    }

    public static void dispatchSync() {
        android.content.Intent intent = new android.content.Intent(com.navdy.hud.app.HudApplication.getAppContext(), com.navdy.hud.app.util.ReportIssueService.class);
        intent.setAction(ACTION_SYNC);
        com.navdy.hud.app.HudApplication.getAppContext().startService(intent);
    }

    public static void scheduleSync() {
        android.content.Intent intent = new android.content.Intent(com.navdy.hud.app.HudApplication.getAppContext(), com.navdy.hud.app.util.ReportIssueService.class);
        intent.setAction(ACTION_SYNC);
        ((android.app.AlarmManager) com.navdy.hud.app.HudApplication.getAppContext().getSystemService("alarm")).set(3, android.os.SystemClock.elapsedRealtime() + RETRY_INTERVAL, android.app.PendingIntent.getService(com.navdy.hud.app.HudApplication.getAppContext(), 128, intent, 268435456));
    }

    public static boolean canReportIssue() {
        return mIssuesToSync.size() <= 10;
    }

    public static void initialize() {
        int i;
        if (!mIsInitialized) {
            sLogger.d("Initializing the service statically");
            navigationIssuesFolder = com.navdy.hud.app.storage.PathManager.getInstance().getNavigationIssuesDir();
            java.io.File[] files = new java.io.File(navigationIssuesFolder).listFiles();
            com.navdy.service.library.log.Logger logger = sLogger;
            java.lang.StringBuilder append = new java.lang.StringBuilder().append("Number of Files waiting for sync :");
            if (files != null) {
                i = files.length;
            } else {
                i = 0;
            }
            logger.d(append.append(i).toString());
            if (files != null) {
                for (java.io.File file : files) {
                    sLogger.d("File " + file.getName());
                    mIssuesToSync.add(file);
                    if (mIssuesToSync.size() == 10) {
                        java.io.File oldestFile = (java.io.File) mIssuesToSync.poll();
                        sLogger.d("Deleting the old file " + oldestFile.getName());
                        com.navdy.service.library.util.IOUtils.deleteFile(com.navdy.hud.app.HudApplication.getAppContext(), oldestFile.getAbsolutePath());
                    }
                }
            }
            mIsInitialized = true;
        }
    }

    public static java.util.List<java.io.File> getLatestDriveLogFiles(int numberOfSets) {
        java.util.ArrayList<java.io.File> latestDriveLogFiles = null;
        if (numberOfSets > 0) {
            java.io.File[] driveLogFiles = com.navdy.hud.app.debug.DriveRecorder.getDriveLogsDir("").listFiles();
            if (!(driveLogFiles == null || driveLogFiles.length == 0)) {
                int n = numberOfSets * 4;
                latestDriveLogFiles = new java.util.ArrayList<>();
                java.util.Arrays.sort(driveLogFiles, new com.navdy.hud.app.util.ReportIssueService.Anon4());
                for (int i = driveLogFiles.length - 1; i >= 0 && latestDriveLogFiles.size() < n; i--) {
                    latestDriveLogFiles.add(driveLogFiles[i]);
                }
            }
        }
        return latestDriveLogFiles;
    }

    public static void showSnapshotMenu() {
        android.os.Bundle args = new android.os.Bundle();
        args.putInt(com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.ARG_MENU_MODE, com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.MenuMode.SNAPSHOT_TITLE_PICKER.ordinal());
        com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getBus().post(new com.navdy.hud.app.event.ShowScreenWithArgs(com.navdy.service.library.events.ui.Screen.SCREEN_MAIN_MENU, args, null, false));
    }
}
