package com.navdy.hud.app.util;

public final class ReportIssueService$$InjectAdapter extends dagger.internal.Binding<com.navdy.hud.app.util.ReportIssueService> implements javax.inject.Provider<com.navdy.hud.app.util.ReportIssueService>, dagger.MembersInjector<com.navdy.hud.app.util.ReportIssueService> {
    private dagger.internal.Binding<com.squareup.otto.Bus> bus;
    private dagger.internal.Binding<com.navdy.hud.app.debug.DriveRecorder> driveRecorder;
    private dagger.internal.Binding<com.navdy.hud.app.gesture.GestureServiceConnector> gestureService;
    private dagger.internal.Binding<com.navdy.service.library.network.http.IHttpManager> mHttpManager;

    public ReportIssueService$$InjectAdapter() {
        super("com.navdy.hud.app.util.ReportIssueService", "members/com.navdy.hud.app.util.ReportIssueService", false, com.navdy.hud.app.util.ReportIssueService.class);
    }

    public void attach(dagger.internal.Linker linker) {
        this.mHttpManager = linker.requestBinding("com.navdy.service.library.network.http.IHttpManager", com.navdy.hud.app.util.ReportIssueService.class, getClass().getClassLoader());
        this.gestureService = linker.requestBinding("com.navdy.hud.app.gesture.GestureServiceConnector", com.navdy.hud.app.util.ReportIssueService.class, getClass().getClassLoader());
        this.bus = linker.requestBinding("com.squareup.otto.Bus", com.navdy.hud.app.util.ReportIssueService.class, getClass().getClassLoader());
        this.driveRecorder = linker.requestBinding("com.navdy.hud.app.debug.DriveRecorder", com.navdy.hud.app.util.ReportIssueService.class, getClass().getClassLoader());
    }

    public void getDependencies(java.util.Set<dagger.internal.Binding<?>> set, java.util.Set<dagger.internal.Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.mHttpManager);
        injectMembersBindings.add(this.gestureService);
        injectMembersBindings.add(this.bus);
        injectMembersBindings.add(this.driveRecorder);
    }

    public com.navdy.hud.app.util.ReportIssueService get() {
        com.navdy.hud.app.util.ReportIssueService result = new com.navdy.hud.app.util.ReportIssueService();
        injectMembers(result);
        return result;
    }

    public void injectMembers(com.navdy.hud.app.util.ReportIssueService object) {
        object.mHttpManager = (com.navdy.service.library.network.http.IHttpManager) this.mHttpManager.get();
        object.gestureService = (com.navdy.hud.app.gesture.GestureServiceConnector) this.gestureService.get();
        object.bus = (com.squareup.otto.Bus) this.bus.get();
        object.driveRecorder = (com.navdy.hud.app.debug.DriveRecorder) this.driveRecorder.get();
    }
}
