package com.navdy.hud.app.util;

public class CustomDimension {
    private android.util.TypedValue mAttribute;
    private float mValue;

    public static com.navdy.hud.app.util.CustomDimension getDimension(android.view.View v, android.content.res.TypedArray a, int index, float defValue) {
        if (v.isInEditMode()) {
            java.lang.String value = a.getString(index);
            if (value == null) {
                return new com.navdy.hud.app.util.CustomDimension(defValue);
            }
            if (!value.endsWith("%") && !value.endsWith("%p")) {
                return new com.navdy.hud.app.util.CustomDimension(a.getDimension(index, defValue));
            }
            android.util.TypedValue attributeValue = new android.util.TypedValue();
            a.getValue(index, attributeValue);
            return new com.navdy.hud.app.util.CustomDimension(attributeValue);
        }
        android.util.TypedValue attributeValue2 = new android.util.TypedValue();
        a.getValue(index, attributeValue2);
        if (attributeValue2.type == 0) {
            return new com.navdy.hud.app.util.CustomDimension(defValue);
        }
        if (attributeValue2.type == 5) {
            return new com.navdy.hud.app.util.CustomDimension(a.getDimension(index, defValue));
        }
        return new com.navdy.hud.app.util.CustomDimension(attributeValue2);
    }

    public static boolean hasDimension(android.view.View v, android.content.res.TypedArray a, int index) {
        if (!v.isInEditMode()) {
            android.util.TypedValue attributeValue = new android.util.TypedValue();
            a.getValue(index, attributeValue);
            if (attributeValue.type == 0) {
                return false;
            }
            return true;
        } else if (a.getString(index) != null) {
            return true;
        } else {
            return false;
        }
    }

    public CustomDimension(float fixedValue) {
        this.mValue = fixedValue;
    }

    public CustomDimension(android.util.TypedValue attribute) {
        this.mAttribute = attribute;
    }

    public float getSize(android.view.View v, float base, float pBase) {
        if (this.mAttribute == null) {
            return this.mValue;
        }
        switch (this.mAttribute.type) {
            case 4:
                return this.mAttribute.getFloat() * base;
            case 6:
                return this.mAttribute.getFraction(base, pBase);
            default:
                throw new java.lang.IllegalArgumentException("Attribute must have type fraction or float - it is " + this.mAttribute.type);
        }
    }
}
