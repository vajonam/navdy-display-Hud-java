package com.navdy.hud.app.util;

public class PhoneUtil {
    private static final java.util.Map<java.lang.String, java.lang.Integer> sISOCountryCodeMap = new java.util.HashMap(310);
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.util.PhoneUtil.class);
    private static final com.google.i18n.phonenumbers.PhoneNumberUtil sPhoneNumberUtil = com.google.i18n.phonenumbers.PhoneNumberUtil.getInstance();

    static {
        sISOCountryCodeMap.put("US", java.lang.Integer.valueOf(1));
        sISOCountryCodeMap.put("RU", java.lang.Integer.valueOf(7));
        sISOCountryCodeMap.put("KZ", java.lang.Integer.valueOf(7));
        sISOCountryCodeMap.put("EG", java.lang.Integer.valueOf(20));
        sISOCountryCodeMap.put("ZA", java.lang.Integer.valueOf(27));
        sISOCountryCodeMap.put("GR", java.lang.Integer.valueOf(30));
        sISOCountryCodeMap.put("NL", java.lang.Integer.valueOf(31));
        sISOCountryCodeMap.put("BE", java.lang.Integer.valueOf(32));
        sISOCountryCodeMap.put("FR", java.lang.Integer.valueOf(33));
        sISOCountryCodeMap.put("ES", java.lang.Integer.valueOf(34));
        sISOCountryCodeMap.put("HU", java.lang.Integer.valueOf(36));
        sISOCountryCodeMap.put("IT", java.lang.Integer.valueOf(39));
        sISOCountryCodeMap.put("VA", java.lang.Integer.valueOf(39));
        sISOCountryCodeMap.put("RO", java.lang.Integer.valueOf(40));
        sISOCountryCodeMap.put("CH", java.lang.Integer.valueOf(41));
        sISOCountryCodeMap.put("AT", java.lang.Integer.valueOf(43));
        sISOCountryCodeMap.put("GB", java.lang.Integer.valueOf(44));
        sISOCountryCodeMap.put("GG", java.lang.Integer.valueOf(44));
        sISOCountryCodeMap.put("IM", java.lang.Integer.valueOf(44));
        sISOCountryCodeMap.put("JE", java.lang.Integer.valueOf(44));
        sISOCountryCodeMap.put("DK", java.lang.Integer.valueOf(45));
        sISOCountryCodeMap.put("SE", java.lang.Integer.valueOf(46));
        sISOCountryCodeMap.put("NO", java.lang.Integer.valueOf(47));
        sISOCountryCodeMap.put("SJ", java.lang.Integer.valueOf(47));
        sISOCountryCodeMap.put("PL", java.lang.Integer.valueOf(48));
        sISOCountryCodeMap.put("DE", java.lang.Integer.valueOf(49));
        sISOCountryCodeMap.put("PE", java.lang.Integer.valueOf(51));
        sISOCountryCodeMap.put("MX", java.lang.Integer.valueOf(52));
        sISOCountryCodeMap.put("CU", java.lang.Integer.valueOf(53));
        sISOCountryCodeMap.put("AR", java.lang.Integer.valueOf(54));
        sISOCountryCodeMap.put("BR", java.lang.Integer.valueOf(55));
        sISOCountryCodeMap.put("CL", java.lang.Integer.valueOf(56));
        sISOCountryCodeMap.put("CO", java.lang.Integer.valueOf(57));
        sISOCountryCodeMap.put("VE", java.lang.Integer.valueOf(58));
        sISOCountryCodeMap.put("MY", java.lang.Integer.valueOf(60));
        sISOCountryCodeMap.put("AU", java.lang.Integer.valueOf(61));
        sISOCountryCodeMap.put("CC", java.lang.Integer.valueOf(61));
        sISOCountryCodeMap.put("CX", java.lang.Integer.valueOf(61));
        sISOCountryCodeMap.put("ID", java.lang.Integer.valueOf(62));
        sISOCountryCodeMap.put("PH", java.lang.Integer.valueOf(63));
        sISOCountryCodeMap.put("NZ", java.lang.Integer.valueOf(64));
        sISOCountryCodeMap.put("SG", java.lang.Integer.valueOf(65));
        sISOCountryCodeMap.put("TH", java.lang.Integer.valueOf(66));
        sISOCountryCodeMap.put("JP", java.lang.Integer.valueOf(81));
        sISOCountryCodeMap.put("KR", java.lang.Integer.valueOf(82));
        sISOCountryCodeMap.put("VN", java.lang.Integer.valueOf(84));
        sISOCountryCodeMap.put("CN", java.lang.Integer.valueOf(86));
        sISOCountryCodeMap.put("TR", java.lang.Integer.valueOf(90));
        sISOCountryCodeMap.put("IN", java.lang.Integer.valueOf(91));
        sISOCountryCodeMap.put("IN", java.lang.Integer.valueOf(91));
        sISOCountryCodeMap.put("IN", java.lang.Integer.valueOf(91));
        sISOCountryCodeMap.put("IN", java.lang.Integer.valueOf(91));
        sISOCountryCodeMap.put("PK", java.lang.Integer.valueOf(92));
        sISOCountryCodeMap.put("AF", java.lang.Integer.valueOf(93));
        sISOCountryCodeMap.put("LK", java.lang.Integer.valueOf(94));
        sISOCountryCodeMap.put("MM", java.lang.Integer.valueOf(95));
        sISOCountryCodeMap.put("IR", java.lang.Integer.valueOf(98));
        sISOCountryCodeMap.put("SS", java.lang.Integer.valueOf(com.navdy.hud.app.bluetooth.obex.ResponseCodes.OBEX_HTTP_UNAVAILABLE));
        sISOCountryCodeMap.put("MA", java.lang.Integer.valueOf(com.navdy.hud.app.bluetooth.obex.ResponseCodes.OBEX_HTTP_GATEWAY_TIMEOUT));
        sISOCountryCodeMap.put("EH", java.lang.Integer.valueOf(com.navdy.hud.app.bluetooth.obex.ResponseCodes.OBEX_HTTP_GATEWAY_TIMEOUT));
        sISOCountryCodeMap.put("DZ", java.lang.Integer.valueOf(com.navdy.hud.app.bluetooth.obex.ResponseCodes.OBEX_HTTP_VERSION));
        sISOCountryCodeMap.put("TN", java.lang.Integer.valueOf(216));
        sISOCountryCodeMap.put("LY", java.lang.Integer.valueOf(218));
        sISOCountryCodeMap.put("GM", java.lang.Integer.valueOf(220));
        sISOCountryCodeMap.put("SN", java.lang.Integer.valueOf(221));
        sISOCountryCodeMap.put("MR", java.lang.Integer.valueOf(222));
        sISOCountryCodeMap.put("ML", java.lang.Integer.valueOf(223));
        sISOCountryCodeMap.put("GN", java.lang.Integer.valueOf(com.navdy.hud.app.bluetooth.obex.ResponseCodes.OBEX_DATABASE_FULL));
        sISOCountryCodeMap.put("CI", java.lang.Integer.valueOf(com.navdy.hud.app.bluetooth.obex.ResponseCodes.OBEX_DATABASE_LOCKED));
        sISOCountryCodeMap.put("BF", java.lang.Integer.valueOf(226));
        sISOCountryCodeMap.put("NE", java.lang.Integer.valueOf(227));
        sISOCountryCodeMap.put("TG", java.lang.Integer.valueOf(228));
        sISOCountryCodeMap.put("BJ", java.lang.Integer.valueOf(229));
        sISOCountryCodeMap.put("MU", java.lang.Integer.valueOf(com.navdy.hud.app.ui.component.vlist.VerticalList.ITEM_MOVE_ANIMATION_DURATION));
        sISOCountryCodeMap.put("LR", java.lang.Integer.valueOf(231));
        sISOCountryCodeMap.put("SL", java.lang.Integer.valueOf(232));
        sISOCountryCodeMap.put("GH", java.lang.Integer.valueOf(233));
        sISOCountryCodeMap.put("NG", java.lang.Integer.valueOf(234));
        sISOCountryCodeMap.put("TD", java.lang.Integer.valueOf(235));
        sISOCountryCodeMap.put("CF", java.lang.Integer.valueOf(236));
        sISOCountryCodeMap.put("CM", java.lang.Integer.valueOf(237));
        sISOCountryCodeMap.put("CV", java.lang.Integer.valueOf(238));
        sISOCountryCodeMap.put("ST", java.lang.Integer.valueOf(239));
        sISOCountryCodeMap.put("ST", java.lang.Integer.valueOf(239));
        sISOCountryCodeMap.put("GQ", java.lang.Integer.valueOf(com.glympse.android.lib.StaticConfig.PLACE_SEARCH_DISTANCE_FILTER));
        sISOCountryCodeMap.put("GA", java.lang.Integer.valueOf(241));
        sISOCountryCodeMap.put("CG", java.lang.Integer.valueOf(242));
        sISOCountryCodeMap.put("CD", java.lang.Integer.valueOf(243));
        sISOCountryCodeMap.put("AO", java.lang.Integer.valueOf(244));
        sISOCountryCodeMap.put("GW", java.lang.Integer.valueOf(245));
        sISOCountryCodeMap.put("IO", java.lang.Integer.valueOf(246));
        sISOCountryCodeMap.put("AC", java.lang.Integer.valueOf(247));
        sISOCountryCodeMap.put("SC", java.lang.Integer.valueOf(248));
        sISOCountryCodeMap.put("SD", java.lang.Integer.valueOf(249));
        sISOCountryCodeMap.put("RW", java.lang.Integer.valueOf(250));
        sISOCountryCodeMap.put("ET", java.lang.Integer.valueOf(251));
        sISOCountryCodeMap.put("SO", java.lang.Integer.valueOf(252));
        sISOCountryCodeMap.put("DJ", java.lang.Integer.valueOf(253));
        sISOCountryCodeMap.put("KE", java.lang.Integer.valueOf(254));
        sISOCountryCodeMap.put("TZ", java.lang.Integer.valueOf(255));
        sISOCountryCodeMap.put("UG", java.lang.Integer.valueOf(256));
        sISOCountryCodeMap.put("BI", java.lang.Integer.valueOf(257));
        sISOCountryCodeMap.put("MZ", java.lang.Integer.valueOf(com.navdy.obd.Pids.ENGINE_OIL_PRESSURE));
        sISOCountryCodeMap.put("ZM", java.lang.Integer.valueOf(com.navdy.obd.Pids.TOTAL_VEHICLE_DISTANCE));
        sISOCountryCodeMap.put("MG", java.lang.Integer.valueOf(261));
        sISOCountryCodeMap.put("KE", java.lang.Integer.valueOf(254));
        sISOCountryCodeMap.put("YT", java.lang.Integer.valueOf(262));
        sISOCountryCodeMap.put("KE", java.lang.Integer.valueOf(262));
        sISOCountryCodeMap.put("ZW", java.lang.Integer.valueOf(263));
        sISOCountryCodeMap.put("NA", java.lang.Integer.valueOf(264));
        sISOCountryCodeMap.put("MW", java.lang.Integer.valueOf(265));
        sISOCountryCodeMap.put("LS", java.lang.Integer.valueOf(266));
        sISOCountryCodeMap.put("BW", java.lang.Integer.valueOf(267));
        sISOCountryCodeMap.put("SZ", java.lang.Integer.valueOf(268));
        sISOCountryCodeMap.put("KM", java.lang.Integer.valueOf(269));
        sISOCountryCodeMap.put("SH", java.lang.Integer.valueOf(290));
        sISOCountryCodeMap.put("TA", java.lang.Integer.valueOf(290));
        sISOCountryCodeMap.put("ER", java.lang.Integer.valueOf(291));
        sISOCountryCodeMap.put("AW", java.lang.Integer.valueOf(297));
        sISOCountryCodeMap.put("FO", java.lang.Integer.valueOf(298));
        sISOCountryCodeMap.put("GL", java.lang.Integer.valueOf(299));
        sISOCountryCodeMap.put("GI", java.lang.Integer.valueOf(350));
        sISOCountryCodeMap.put("PT", java.lang.Integer.valueOf(351));
        sISOCountryCodeMap.put("LU", java.lang.Integer.valueOf(352));
        sISOCountryCodeMap.put("IE", java.lang.Integer.valueOf(353));
        sISOCountryCodeMap.put("IS", java.lang.Integer.valueOf(354));
        sISOCountryCodeMap.put("AL", java.lang.Integer.valueOf(355));
        sISOCountryCodeMap.put("MT", java.lang.Integer.valueOf(356));
        sISOCountryCodeMap.put("CY", java.lang.Integer.valueOf(357));
        sISOCountryCodeMap.put("FI", java.lang.Integer.valueOf(358));
        sISOCountryCodeMap.put("AX", java.lang.Integer.valueOf(358));
        sISOCountryCodeMap.put("BG", java.lang.Integer.valueOf(359));
        sISOCountryCodeMap.put("LT", java.lang.Integer.valueOf(370));
        sISOCountryCodeMap.put("LV", java.lang.Integer.valueOf(371));
        sISOCountryCodeMap.put("EE", java.lang.Integer.valueOf(372));
        sISOCountryCodeMap.put("MD", java.lang.Integer.valueOf(373));
        sISOCountryCodeMap.put("AM", java.lang.Integer.valueOf(374));
        sISOCountryCodeMap.put("BY", java.lang.Integer.valueOf(375));
        sISOCountryCodeMap.put("AD", java.lang.Integer.valueOf(376));
        sISOCountryCodeMap.put("MC", java.lang.Integer.valueOf(377));
        sISOCountryCodeMap.put("SM", java.lang.Integer.valueOf(378));
        sISOCountryCodeMap.put("UA", java.lang.Integer.valueOf(380));
        sISOCountryCodeMap.put("RS", java.lang.Integer.valueOf(381));
        sISOCountryCodeMap.put("ME", java.lang.Integer.valueOf(382));
        sISOCountryCodeMap.put("HR", java.lang.Integer.valueOf(385));
        sISOCountryCodeMap.put("SI", java.lang.Integer.valueOf(386));
        sISOCountryCodeMap.put("BA", java.lang.Integer.valueOf(387));
        sISOCountryCodeMap.put("MK", java.lang.Integer.valueOf(389));
        sISOCountryCodeMap.put("CZ", java.lang.Integer.valueOf(420));
        sISOCountryCodeMap.put("SK", java.lang.Integer.valueOf(421));
        sISOCountryCodeMap.put("LI", java.lang.Integer.valueOf(423));
        sISOCountryCodeMap.put("FK", java.lang.Integer.valueOf(500));
        sISOCountryCodeMap.put("BZ", java.lang.Integer.valueOf(501));
        sISOCountryCodeMap.put("GT", java.lang.Integer.valueOf(502));
        sISOCountryCodeMap.put("SV", java.lang.Integer.valueOf(503));
        sISOCountryCodeMap.put("HN", java.lang.Integer.valueOf(504));
        sISOCountryCodeMap.put("NI", java.lang.Integer.valueOf(505));
        sISOCountryCodeMap.put("CR", java.lang.Integer.valueOf(506));
        sISOCountryCodeMap.put("PA", java.lang.Integer.valueOf(507));
        sISOCountryCodeMap.put("PM", java.lang.Integer.valueOf(508));
        sISOCountryCodeMap.put("HT", java.lang.Integer.valueOf(509));
        sISOCountryCodeMap.put("GP", java.lang.Integer.valueOf(590));
        sISOCountryCodeMap.put("BL", java.lang.Integer.valueOf(590));
        sISOCountryCodeMap.put("MF", java.lang.Integer.valueOf(590));
        sISOCountryCodeMap.put("BO", java.lang.Integer.valueOf(591));
        sISOCountryCodeMap.put("GY", java.lang.Integer.valueOf(592));
        sISOCountryCodeMap.put("EC", java.lang.Integer.valueOf(593));
        sISOCountryCodeMap.put("GF", java.lang.Integer.valueOf(594));
        sISOCountryCodeMap.put("PY", java.lang.Integer.valueOf(595));
        sISOCountryCodeMap.put("MQ", java.lang.Integer.valueOf(596));
        sISOCountryCodeMap.put("SR", java.lang.Integer.valueOf(597));
        sISOCountryCodeMap.put("UY", java.lang.Integer.valueOf(598));
        sISOCountryCodeMap.put("CW", java.lang.Integer.valueOf(com.glympse.android.lib.HttpJob.RESPONSE_CODE_MAX_RETRY_UPPER_BOUND));
        sISOCountryCodeMap.put("BQ", java.lang.Integer.valueOf(com.glympse.android.lib.HttpJob.RESPONSE_CODE_MAX_RETRY_UPPER_BOUND));
        sISOCountryCodeMap.put("TL", java.lang.Integer.valueOf(670));
        sISOCountryCodeMap.put("NF", java.lang.Integer.valueOf(672));
        sISOCountryCodeMap.put("BN", java.lang.Integer.valueOf(673));
        sISOCountryCodeMap.put("NR", java.lang.Integer.valueOf(674));
        sISOCountryCodeMap.put("PG", java.lang.Integer.valueOf(675));
        sISOCountryCodeMap.put("TO", java.lang.Integer.valueOf(676));
        sISOCountryCodeMap.put("SB", java.lang.Integer.valueOf(677));
        sISOCountryCodeMap.put("VU", java.lang.Integer.valueOf(678));
        sISOCountryCodeMap.put("FJ", java.lang.Integer.valueOf(679));
        sISOCountryCodeMap.put("PW", java.lang.Integer.valueOf(680));
        sISOCountryCodeMap.put("WF", java.lang.Integer.valueOf(681));
        sISOCountryCodeMap.put("CK", java.lang.Integer.valueOf(682));
        sISOCountryCodeMap.put("NU", java.lang.Integer.valueOf(683));
        sISOCountryCodeMap.put("WS", java.lang.Integer.valueOf(685));
        sISOCountryCodeMap.put("KI", java.lang.Integer.valueOf(686));
        sISOCountryCodeMap.put("NC", java.lang.Integer.valueOf(687));
        sISOCountryCodeMap.put("TV", java.lang.Integer.valueOf(688));
        sISOCountryCodeMap.put("PF", java.lang.Integer.valueOf(689));
        sISOCountryCodeMap.put("TK", java.lang.Integer.valueOf(690));
        sISOCountryCodeMap.put("FM", java.lang.Integer.valueOf(691));
        sISOCountryCodeMap.put("MH", java.lang.Integer.valueOf(692));
        sISOCountryCodeMap.put("KP", java.lang.Integer.valueOf(850));
        sISOCountryCodeMap.put("HK", java.lang.Integer.valueOf(852));
        sISOCountryCodeMap.put("MO", java.lang.Integer.valueOf(853));
        sISOCountryCodeMap.put("KH", java.lang.Integer.valueOf(855));
        sISOCountryCodeMap.put("LA", java.lang.Integer.valueOf(856));
        sISOCountryCodeMap.put("BD", java.lang.Integer.valueOf(880));
        sISOCountryCodeMap.put("TW", java.lang.Integer.valueOf(886));
        sISOCountryCodeMap.put("MV", java.lang.Integer.valueOf(960));
        sISOCountryCodeMap.put("LB", java.lang.Integer.valueOf(961));
        sISOCountryCodeMap.put("JO", java.lang.Integer.valueOf(962));
        sISOCountryCodeMap.put("SY", java.lang.Integer.valueOf(963));
        sISOCountryCodeMap.put("IQ", java.lang.Integer.valueOf(964));
        sISOCountryCodeMap.put("KW", java.lang.Integer.valueOf(965));
        sISOCountryCodeMap.put("SA", java.lang.Integer.valueOf(966));
        sISOCountryCodeMap.put("YE", java.lang.Integer.valueOf(967));
        sISOCountryCodeMap.put("OM", java.lang.Integer.valueOf(968));
        sISOCountryCodeMap.put("PS", java.lang.Integer.valueOf(970));
        sISOCountryCodeMap.put("AE", java.lang.Integer.valueOf(971));
        sISOCountryCodeMap.put("IL", java.lang.Integer.valueOf(972));
        sISOCountryCodeMap.put("BH", java.lang.Integer.valueOf(973));
        sISOCountryCodeMap.put("QA", java.lang.Integer.valueOf(974));
        sISOCountryCodeMap.put("BT", java.lang.Integer.valueOf(975));
        sISOCountryCodeMap.put("MN", java.lang.Integer.valueOf(976));
        sISOCountryCodeMap.put("NP", java.lang.Integer.valueOf(977));
        sISOCountryCodeMap.put("TJ", java.lang.Integer.valueOf(992));
        sISOCountryCodeMap.put("TM", java.lang.Integer.valueOf(993));
        sISOCountryCodeMap.put("AZ", java.lang.Integer.valueOf(994));
        sISOCountryCodeMap.put("GE", java.lang.Integer.valueOf(995));
        sISOCountryCodeMap.put("KG", java.lang.Integer.valueOf(996));
        sISOCountryCodeMap.put("UZ", java.lang.Integer.valueOf(998));
    }

    public static synchronized java.lang.String formatPhoneNumber(java.lang.String phoneNumber) {
        synchronized (com.navdy.hud.app.util.PhoneUtil.class) {
            try {
                if (android.text.TextUtils.isEmpty(phoneNumber)) {
                    phoneNumber = "";
                } else {
                    java.util.Locale currentLocale = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentLocale();
                    int countryPhoneCode = getCountryPhoneCode(currentLocale.getCountry());
                    com.google.i18n.phonenumbers.Phonenumber.PhoneNumber number = sPhoneNumberUtil.parse(phoneNumber, currentLocale.getCountry());
                    boolean local = true;
                    boolean countryCodeMatch = false;
                    if (countryPhoneCode > 0 && number.hasCountryCode()) {
                        int cc = number.getCountryCode();
                        if (cc == countryPhoneCode) {
                            countryCodeMatch = true;
                        } else if (phoneNumber.startsWith(java.lang.String.valueOf(cc)) || phoneNumber.startsWith("+")) {
                            local = false;
                        }
                    }
                    if (local && !countryCodeMatch && phoneNumber.startsWith("+")) {
                        local = false;
                    }
                    if (local) {
                        phoneNumber = sPhoneNumberUtil.format(number, com.google.i18n.phonenumbers.PhoneNumberUtil.PhoneNumberFormat.NATIONAL);
                    } else {
                        phoneNumber = sPhoneNumberUtil.format(number, com.google.i18n.phonenumbers.PhoneNumberUtil.PhoneNumberFormat.INTERNATIONAL);
                    }
                }
            } catch (Throwable t) {
                if (sLogger.isLoggable(2)) {
                    sLogger.e(t);
                }
            }
        }
        return phoneNumber;
    }

    private static int getCountryPhoneCode(java.lang.String isoCountryCode) {
        if (android.text.TextUtils.isEmpty(isoCountryCode)) {
            return -1;
        }
        java.lang.Integer code = (java.lang.Integer) sISOCountryCodeMap.get(isoCountryCode);
        if (code != null) {
            return code.intValue();
        }
        return -1;
    }

    public static java.lang.String normalizeNumber(java.lang.String number) {
        return android.text.TextUtils.isEmpty(number) ? number : number.replaceAll("[^0-9]+", "");
    }

    public static java.lang.String convertToE164Format(java.lang.String number) {
        try {
            return sPhoneNumberUtil.format(sPhoneNumberUtil.parse(number, com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentLocale().getCountry()), com.google.i18n.phonenumbers.PhoneNumberUtil.PhoneNumberFormat.E164);
        } catch (Throwable t) {
            sLogger.e(t);
            return number;
        }
    }
}
