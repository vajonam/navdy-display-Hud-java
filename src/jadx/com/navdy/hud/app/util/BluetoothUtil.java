package com.navdy.hud.app.util;

public class BluetoothUtil {
    public static final int PAIRING_CONSENT = 3;
    public static final int PAIRING_VARIANT_DISPLAY_PASSKEY = 4;
    public static final int PAIRING_VARIANT_DISPLAY_PIN = 5;
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.util.BluetoothUtil.class);

    public static byte[] convertPinToBytes(java.lang.String pin) {
        if (pin == null) {
            return null;
        }
        try {
            byte[] pinBytes = pin.getBytes("UTF-8");
            if (pinBytes.length <= 0 || pinBytes.length > 16) {
                return null;
            }
            return pinBytes;
        } catch (java.io.UnsupportedEncodingException e) {
            sLogger.e("UTF-8 not supported?!?");
            return null;
        }
    }

    public static void removeBond(android.bluetooth.BluetoothDevice device) {
        try {
            java.lang.reflect.Method m = device.getClass().getMethod("removeBond", null);
            if (m != null) {
                sLogger.i("removingBond for " + device);
                m.invoke(device, null);
                sLogger.i("removedBond for " + device);
                return;
            }
            sLogger.e("cannot get to removeBond api");
        } catch (Throwable e) {
            sLogger.e("Failed to removeBond", e);
        }
    }

    public static void cancelBondProcess(android.bluetooth.BluetoothDevice device) {
        try {
            java.lang.reflect.Method m = device.getClass().getMethod("cancelBondProcess", null);
            if (m != null) {
                sLogger.i("cancellingBond for " + device);
                m.invoke(device, null);
                sLogger.i("cancelledBond for " + device);
                return;
            }
            sLogger.e("cannot get to cancelBondProcess api");
        } catch (Throwable e) {
            sLogger.e("Failed to cancelBondProcess", e);
        }
    }

    public static void cancelUserConfirmation(android.bluetooth.BluetoothDevice device) {
        try {
            sLogger.e("cancelling cancelPairingUserInput");
            device.getClass().getMethod("cancelPairingUserInput", new java.lang.Class[]{java.lang.Boolean.TYPE}).invoke(device, new java.lang.Object[0]);
            sLogger.e("cancelled cancelPairingUserInput");
        } catch (Throwable throwable) {
            sLogger.e(throwable);
        }
    }

    public static void confirmPairing(android.bluetooth.BluetoothDevice device, int variant, int pin) {
        sLogger.i("Confirming pairing for " + device + " variant" + variant + " pin:" + pin);
        if (variant == 4 || variant == 2) {
            device.setPairingConfirmation(true);
        } else if (variant == 5) {
            device.setPin(convertPinToBytes(java.lang.String.format("%04d", new java.lang.Object[]{java.lang.Integer.valueOf(pin)})));
        } else if (variant == 3) {
            device.setPairingConfirmation(true);
        }
    }
}
