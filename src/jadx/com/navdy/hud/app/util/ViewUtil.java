package com.navdy.hud.app.util;

public class ViewUtil {
    private static final int LINE_SPACING_INDEX = 2;
    private static final int MAX_LINE_INDEX = 0;
    private static final int[] MULTI_LINE_ATTRS = {16843091, 16843101, 16843288};
    private static final int SINGLE_LINE_INDEX = 1;
    static android.util.SparseArray<float[]> TEXT_SIZES = new android.util.SparseArray<>();

    public static void autosize(android.widget.TextView view, int maxLines, int maxWidth, @android.support.annotation.ArrayRes int arrayId) {
        autosize(view, maxLines, maxWidth, lookupSizes(view.getResources(), arrayId));
    }

    public static void adjustPadding(android.view.View v, int left, int top, int right, int bottom) {
        v.setPadding(v.getPaddingLeft() + left, v.getPaddingTop() + top, v.getPaddingRight() + right, v.getPaddingBottom() + bottom);
    }

    public static void setBottomPadding(android.view.View v, int bottom) {
        v.setPadding(v.getPaddingLeft(), v.getPaddingTop(), v.getPaddingRight(), bottom);
    }

    public static float autosize(android.widget.TextView view, int maxLines, int maxWidth, float[] textSizes) {
        int[] maxLinesArray = new int[textSizes.length];
        for (int i = 0; i < textSizes.length; i++) {
            maxLinesArray[i] = maxLines;
        }
        return autosize(view, maxLinesArray, maxWidth, textSizes, null);
    }

    public static float autosize(android.widget.TextView view, int[] maxLinesArray, int maxWidth, float[] textSizes, int[] indexHolder) {
        if (maxLinesArray.length != textSizes.length) {
            throw new java.lang.IllegalArgumentException("maxLinesArray length must match textSizes length");
        }
        java.lang.CharSequence sequence = view.getText();
        android.text.TextPaint p = view.getPaint();
        int minLines = -1;
        int index = textSizes.length - 1;
        float maxSize = textSizes[index];
        for (int i = 0; i < textSizes.length; i++) {
            int maxLines = maxLinesArray[i];
            view.setSingleLine(maxLines == 1);
            int limit = maxLines;
            float textSize = textSizes[i];
            p.setTextSize(textSize);
            int lines = new android.text.StaticLayout(sequence, p, maxWidth, android.text.Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false).getLineCount();
            if (lines <= limit && (minLines == -1 || lines < minLines)) {
                minLines = lines;
                maxSize = textSize;
                index = i;
            }
            if (lines == 1) {
                break;
            }
        }
        if (indexHolder != null) {
            indexHolder[0] = index;
            if (minLines == -1) {
                minLines = maxLinesArray[index];
            }
            indexHolder[1] = minLines;
        }
        view.setTextSize(maxSize);
        return maxSize;
    }

    private static float[] lookupSizes(android.content.res.Resources r, @android.support.annotation.ArrayRes int arrayId) {
        float[] cache = (float[]) TEXT_SIZES.get(arrayId);
        if (cache != null) {
            return cache;
        }
        int[] array = r.getIntArray(arrayId);
        float[] copy = new float[array.length];
        for (int i = 0; i < array.length; i++) {
            copy[i] = (float) array[i];
        }
        float[] cache2 = copy;
        TEXT_SIZES.put(arrayId, cache2);
        return cache2;
    }

    public static int applyTextAndStyle(android.widget.TextView view, java.lang.CharSequence text, int maxLines, @android.support.annotation.StyleRes int style) {
        boolean singleLine;
        boolean singleLine2;
        android.content.Context context = view.getContext();
        if (android.text.TextUtils.isEmpty(text)) {
            view.setVisibility(8);
            return 0;
        }
        view.setVisibility(0);
        if (maxLines == 1) {
            singleLine = true;
        } else {
            singleLine = false;
        }
        float lineSpacing = 1.0f;
        if (style != -1) {
            android.content.res.TypedArray ta = context.obtainStyledAttributes(style, MULTI_LINE_ATTRS);
            maxLines = ta.getInt(0, maxLines);
            singleLine2 = ta.hasValue(1) ? ta.getBoolean(1, true) : maxLines == 1;
            lineSpacing = ta.getFloat(2, 1.0f);
            ta.recycle();
            view.setTextAppearance(context, style);
        }
        view.setText(text);
        view.setLineSpacing(0.0f, lineSpacing);
        view.setSingleLine(singleLine2);
        view.setMaxLines(maxLines);
        return maxLines;
    }
}
