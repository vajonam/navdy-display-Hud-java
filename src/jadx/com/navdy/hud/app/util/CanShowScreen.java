package com.navdy.hud.app.util;

public interface CanShowScreen<S extends mortar.Blueprint> {
    void showScreen(S s, flow.Flow.Direction direction, int i, int i2);
}
