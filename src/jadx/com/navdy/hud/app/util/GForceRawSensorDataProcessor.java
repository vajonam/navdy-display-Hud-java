package com.navdy.hud.app.util;

public class GForceRawSensorDataProcessor {
    private static com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.util.GForceRawSensorDataProcessor.class);
    private boolean calibrated;
    private android.renderscript.Matrix3f rotationMatrix;
    private float[] v = new float[3];
    public float xAccel;
    public float yAccel;
    public float zAccel;

    public void onRawData(com.navdy.hud.app.device.gps.RawSensorData sensorData) {
        this.v[0] = sensorData.x;
        this.v[1] = sensorData.y;
        this.v[2] = sensorData.z;
        if (!this.calibrated) {
            float m = magnitude(this.v);
            if (((double) m) > 0.9d && ((double) m) < 1.1d) {
                this.rotationMatrix = calculateRotationMatrix(this.v);
                this.calibrated = true;
            }
        }
        if (this.calibrated) {
            multiply(this.rotationMatrix, this.v);
        }
        this.xAccel = this.v[0];
        this.yAccel = this.v[1];
        this.zAccel = this.v[2];
    }

    private float magnitude(float[] v2) {
        return (float) java.lang.Math.sqrt((double) ((v2[0] * v2[0]) + (v2[1] * v2[1]) + (v2[2] * v2[2])));
    }

    private void multiply(android.renderscript.Matrix3f matrix, float[] v2) {
        float[] r = matrix.getArray();
        float x = v2[0];
        float y = v2[1];
        float z = v2[2];
        float ry = (r[3] * x) + (r[4] * y) + (r[5] * z);
        float rz = (r[6] * x) + (r[7] * y) + (r[8] * z);
        v2[0] = (r[0] * x) + (r[1] * y) + (r[2] * z);
        v2[1] = ry;
        v2[2] = rz;
    }

    private android.renderscript.Matrix3f calculateRotationMatrix(float[] v2) {
        android.renderscript.Matrix3f result = new android.renderscript.Matrix3f();
        float x = v2[0];
        float y = v2[1];
        float z = v2[2];
        double roll = -java.lang.Math.atan2((double) y, (double) z);
        double pitch = -java.lang.Math.atan2((double) (-x), java.lang.Math.sqrt((double) ((y * y) + (z * z))));
        float cr = (float) java.lang.Math.cos(roll);
        float sr = (float) java.lang.Math.sin(roll);
        float cp = (float) java.lang.Math.cos(pitch);
        float sp = (float) java.lang.Math.sin(pitch);
        float[] ry = {cp, 0.0f, -sp, 0.0f, 1.0f, 0.0f, sp, 0.0f, cp};
        android.renderscript.Matrix3f matrix3f = new android.renderscript.Matrix3f(new float[]{1.0f, 0.0f, 0.0f, 0.0f, cr, sr, 0.0f, -sr, cr});
        android.renderscript.Matrix3f matrix3f2 = new android.renderscript.Matrix3f(ry);
        result.loadMultiply(matrix3f, matrix3f2);
        logger.d("acceleration vector: " + x + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA + y + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA + z + " derived rotation matrix: " + java.util.Arrays.toString(result.getArray()));
        return result;
    }

    public void setCalibrated(boolean calibrated2) {
        this.calibrated = calibrated2;
    }

    public boolean isCalibrated() {
        return this.calibrated;
    }
}
