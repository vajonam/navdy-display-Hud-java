package com.navdy.hud.app.util;

public final class CrashReporter {
    private static final int ANR_LOG_LIMIT = 32768;
    private static final java.lang.String ANR_PATH = "/data/anr/traces.txt";
    public static final java.lang.String HOCKEY_APP_ID = "20625a4f769df641ecf46825a71c1911";
    public static final java.lang.String HOCKEY_APP_STABLE_ID = "4dd8d996ee304ef2994965aa7f6d4f15";
    public static final java.lang.String HOCKEY_APP_UNSTABLE_ID = "ad4b9dbcbb3b4083a49af48878026e5a";
    private static final int KERNEL_CRASH_INFO_LIMIT = 32768;
    private static final java.lang.String LOG_ANR_MARKER = "Wrote stack traces to '/data/anr/traces.txt'";
    private static final java.lang.String LOG_BEGIN_MARKER = "--------- beginning of main";
    private static final int PERIOD_DELIVERY_CHECK_INITIAL_INTERVAL = ((int) java.util.concurrent.TimeUnit.MINUTES.toMillis(2));
    /* access modifiers changed from: private */
    public static final int PERIOD_DELIVERY_CHECK_INTERVAL = ((int) java.util.concurrent.TimeUnit.MINUTES.toMillis(5));
    private static final java.util.regex.Pattern START_OF_OOPS = java.util.regex.Pattern.compile("(Unable to handle)|(Internal error:)");
    private static final int SYSTEM_LOG_LIMIT = 32768;
    private static final java.lang.String TOMBSTONE_FILE_PATTERN = "tombstone_light_";
    private static final java.lang.String TOMBSTONE_PATH = "/data/tombstones";
    public static final java.util.regex.Pattern USER_REBOOT = java.util.regex.Pattern.compile("(reboot: Restarting system with command)|(do_powerctl:)");
    /* access modifiers changed from: private */
    public static com.navdy.hud.app.profile.DriverProfileManager driverProfileManager;
    private static final com.navdy.hud.app.util.CrashReporter sInstance = new com.navdy.hud.app.util.CrashReporter();
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.util.CrashReporter.class);
    private static volatile boolean stopCrashReporting;
    /* access modifiers changed from: private */
    public static java.lang.String userId = android.os.Build.SERIAL;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.util.CrashReporter.NavdyCrashListener crashManagerListener = new com.navdy.hud.app.util.CrashReporter.NavdyCrashListener();
    /* access modifiers changed from: private */
    public java.lang.Runnable deliveryRunnable = new com.navdy.hud.app.util.CrashReporter.Anon2();
    /* access modifiers changed from: private */
    public android.os.Handler handler = new android.os.Handler(android.os.Looper.getMainLooper());
    private boolean installed;
    private com.navdy.hud.app.util.OTAUpdateService.OTAFailedException otaFailure = null;
    /* access modifiers changed from: private */
    public java.lang.Runnable periodicRunnable = new com.navdy.hud.app.util.CrashReporter.Anon1();

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            com.navdy.service.library.task.TaskManager.getInstance().execute(com.navdy.hud.app.util.CrashReporter.this.deliveryRunnable, 1);
        }
    }

    class Anon2 implements java.lang.Runnable {
        Anon2() {
        }

        public void run() {
            boolean checkAgain = true;
            try {
                if (com.navdy.hud.app.util.CrashReporter.this.checkForCrashes()) {
                    com.navdy.hud.app.util.CrashReporter.this.handler.postDelayed(com.navdy.hud.app.util.CrashReporter.this.periodicRunnable, (long) com.navdy.hud.app.util.CrashReporter.PERIOD_DELIVERY_CHECK_INTERVAL);
                }
            } catch (Throwable th) {
                if (checkAgain) {
                    com.navdy.hud.app.util.CrashReporter.this.handler.postDelayed(com.navdy.hud.app.util.CrashReporter.this.periodicRunnable, (long) com.navdy.hud.app.util.CrashReporter.PERIOD_DELIVERY_CHECK_INTERVAL);
                }
                throw th;
            }
        }
    }

    class Anon3 implements java.lang.Thread.UncaughtExceptionHandler {
        final /* synthetic */ java.lang.Thread.UncaughtExceptionHandler val$hockeyUncaughtHandler;

        Anon3(java.lang.Thread.UncaughtExceptionHandler uncaughtExceptionHandler) {
            this.val$hockeyUncaughtHandler = uncaughtExceptionHandler;
        }

        public void uncaughtException(java.lang.Thread thread, java.lang.Throwable exception) {
            com.navdy.hud.app.util.CrashReporter.this.handleUncaughtException(thread, exception, this.val$hockeyUncaughtHandler);
        }
    }

    class Anon4 implements java.lang.Runnable {
        final /* synthetic */ java.lang.String[] val$kernelCrashFiles;

        Anon4(java.lang.String[] strArr) {
            this.val$kernelCrashFiles = strArr;
        }

        public void run() {
            try {
                com.navdy.hud.app.util.CrashReporter.sLogger.v("checking for kernel crashes");
                java.io.File file = null;
                java.lang.String[] strArr = this.val$kernelCrashFiles;
                int length = strArr.length;
                int i = 0;
                while (true) {
                    if (i >= length) {
                        break;
                    }
                    java.io.File f = new java.io.File(strArr[i]);
                    if (f.exists()) {
                        file = f;
                        com.navdy.hud.app.util.CrashReporter.sLogger.v("found file:" + f.getAbsolutePath());
                        break;
                    }
                    i++;
                }
                if (file != null) {
                    java.lang.String str = com.navdy.hud.app.util.CrashReporter.this.filterKernelCrash(file);
                    if (!android.text.TextUtils.isEmpty(str)) {
                        com.navdy.hud.app.util.CrashReporter.this.reportNonFatalException(new com.navdy.hud.app.util.CrashReporter.KernelCrashException(str));
                        com.navdy.hud.app.util.CrashReporter.sLogger.v("kernel crash created");
                    }
                    com.navdy.hud.app.util.CrashReporter.this.cleanupKernelCrashes(this.val$kernelCrashFiles);
                    return;
                }
                com.navdy.hud.app.util.CrashReporter.sLogger.v("no kernel crash files");
            } catch (Throwable t) {
                com.navdy.hud.app.util.CrashReporter.sLogger.e(t);
            }
        }
    }

    class Anon5 implements java.io.FilenameFilter {
        Anon5() {
        }

        public boolean accept(java.io.File dir, java.lang.String filename) {
            if (filename.contains(com.navdy.hud.app.util.CrashReporter.TOMBSTONE_FILE_PATTERN)) {
                return true;
            }
            return false;
        }
    }

    class Anon6 implements java.lang.Runnable {
        final /* synthetic */ java.lang.Throwable val$throwable;

        Anon6(java.lang.Throwable th) {
            this.val$throwable = th;
        }

        public void run() {
            try {
                com.navdy.hud.app.util.CrashReporter.this.crashManagerListener.saveException(this.val$throwable);
            } catch (Throwable t) {
                com.navdy.hud.app.util.CrashReporter.sLogger.e(t);
            }
        }
    }

    public static class AnrException extends java.lang.Exception {
        public AnrException(java.lang.String str) {
            super(str);
        }
    }

    public static class CrashException extends java.lang.Exception {
        public CrashException(java.lang.String detailMessage) {
            super(detailMessage);
        }

        public java.util.regex.Pattern getLocationPattern() {
            return null;
        }

        public java.lang.String filter(java.lang.String msg) {
            return msg;
        }

        public java.lang.String getCrashLocation() {
            return extractCrashLocation(getLocalizedMessage());
        }

        public java.lang.String toString() {
            java.lang.String name = getClass().getSimpleName();
            java.lang.String msg = getLocalizedMessage();
            return name + ": " + getCrashLocation() + (msg != null ? filter(msg) : "");
        }

        /* access modifiers changed from: 0000 */
        public java.lang.String extractCrashLocation(java.lang.String msg) {
            java.lang.String crashLocation = "";
            java.util.regex.Pattern pattern = getLocationPattern();
            if (pattern == null || msg == null) {
                return crashLocation;
            }
            java.util.regex.Matcher match = pattern.matcher(msg);
            if (!match.find()) {
                return crashLocation;
            }
            if (match.groupCount() < 1) {
                return match.group();
            }
            for (int i = 1; i <= match.groupCount(); i++) {
                if (match.group(i) != null) {
                    return match.group(i);
                }
            }
            return crashLocation;
        }
    }

    public static class KernelCrashException extends com.navdy.hud.app.util.CrashReporter.CrashException {
        private static final java.util.regex.Pattern stackMatcher = java.util.regex.Pattern.compile("PC is at\\s+([^\n]+\n)");

        public KernelCrashException(java.lang.String str) {
            super(str);
        }

        public java.util.regex.Pattern getLocationPattern() {
            return stackMatcher;
        }
    }

    public static class NavdyCrashListener extends net.hockeyapp.android.CrashManagerListener {
        public java.lang.Throwable exception = null;

        public boolean shouldAutoUploadCrashes() {
            return true;
        }

        public java.lang.String getUserID() {
            return com.navdy.hud.app.util.CrashReporter.userId;
        }

        public java.lang.String getContact() {
            return com.navdy.hud.app.util.CrashReporter.driverProfileManager.getLastUserEmail();
        }

        public java.lang.String getDescription() {
            if (this.exception instanceof com.navdy.hud.app.util.CrashReporter.TombstoneException) {
                return ((com.navdy.hud.app.util.CrashReporter.TombstoneException) this.exception).description;
            }
            if (this.exception instanceof com.navdy.hud.app.util.CrashReporter.AnrException) {
                return com.navdy.service.library.util.LogUtils.systemLogStr(32768, com.navdy.hud.app.util.CrashReporter.LOG_ANR_MARKER);
            }
            if (this.exception instanceof com.navdy.hud.app.util.CrashReporter.KernelCrashException) {
                return com.navdy.service.library.util.LogUtils.systemLogStr(32768, com.navdy.hud.app.util.CrashReporter.LOG_BEGIN_MARKER);
            }
            if (!(this.exception instanceof com.navdy.hud.app.util.OTAUpdateService.OTAFailedException)) {
                return com.navdy.service.library.util.LogUtils.systemLogStr(32768, null);
            }
            com.navdy.hud.app.util.OTAUpdateService.OTAFailedException e = (com.navdy.hud.app.util.OTAUpdateService.OTAFailedException) this.exception;
            return e.last_install + com.navdy.hud.app.framework.glance.GlanceConstants.NEWLINE + e.last_log;
        }

        public void saveException(java.lang.Throwable t) {
            this.exception = t;
            net.hockeyapp.android.ExceptionHandler.saveException(t, this);
            this.exception = null;
        }
    }

    public static class TombstoneException extends com.navdy.hud.app.util.CrashReporter.CrashException {
        private static final java.util.regex.Pattern stackMatcher = java.util.regex.Pattern.compile("Abort message: ('[^']+'\n)|#00 pc \\w+\\s+([^\n]+\n)");
        public final java.lang.String description;

        public TombstoneException(java.lang.String str) {
            super("");
            this.description = str;
        }

        public java.lang.String getCrashLocation() {
            return extractCrashLocation(this.description);
        }

        public java.util.regex.Pattern getLocationPattern() {
            return stackMatcher;
        }
    }

    public static boolean isEnabled() {
        return com.navdy.hud.app.util.DeviceUtil.isNavdyDevice() && !com.navdy.hud.app.HudApplication.isDeveloperBuild();
    }

    public static com.navdy.hud.app.util.CrashReporter getInstance() {
        return sInstance;
    }

    private CrashReporter() {
        this.handler.postDelayed(this.periodicRunnable, (long) PERIOD_DELIVERY_CHECK_INITIAL_INTERVAL);
        driverProfileManager = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getDriverProfileManager();
    }

    public synchronized void installCrashHandler(android.content.Context context) {
        if (!this.installed) {
            if (!com.navdy.hud.app.util.DeviceUtil.isUserBuild()) {
                userId = android.os.Build.SERIAL + "-eng";
            }
            java.lang.Thread.UncaughtExceptionHandler defaultUncaughtHandler = java.lang.Thread.getDefaultUncaughtExceptionHandler();
            if (defaultUncaughtHandler != null) {
                sLogger.v("default uncaught handler:" + defaultUncaughtHandler.getClass().getName());
            } else {
                sLogger.v("default uncaught handler is null");
            }
            setHockeyAppCrashUploading(false);
            java.lang.String appId = com.navdy.hud.app.util.DeviceUtil.isUserBuild() ? HOCKEY_APP_ID : com.navdy.hud.app.BuildConfig.DEBUG ? HOCKEY_APP_UNSTABLE_ID : HOCKEY_APP_STABLE_ID;
            net.hockeyapp.android.CrashManager.register(context, appId, this.crashManagerListener);
            net.hockeyapp.android.metrics.MetricsManager.register(context, com.navdy.hud.app.HudApplication.getApplication(), appId);
            sLogger.v("enabled metrics manager");
            setHockeyAppCrashUploading(true);
            java.lang.Thread.UncaughtExceptionHandler hockeyUncaughtHandler = java.lang.Thread.getDefaultUncaughtExceptionHandler();
            if (hockeyUncaughtHandler != null) {
                sLogger.v("hockey uncaught handler:" + hockeyUncaughtHandler.getClass().getName());
            } else {
                sLogger.v("default hockey handler is null");
            }
            java.lang.Thread.setDefaultUncaughtExceptionHandler(new com.navdy.hud.app.util.CrashReporter.Anon3(hockeyUncaughtHandler));
            this.installed = true;
        }
    }

    public void reportNonFatalException(java.lang.Throwable throwable) {
        if (this.installed) {
            try {
                saveHockeyAppException(throwable);
            } catch (Throwable t) {
                sLogger.e(t);
            }
        }
    }

    public void log(java.lang.String msg) {
    }

    /* access modifiers changed from: private */
    public void handleUncaughtException(java.lang.Thread thread, java.lang.Throwable exception, java.lang.Thread.UncaughtExceptionHandler exceptionHandler) {
        if (stopCrashReporting) {
            sLogger.i("FATAL-reporting-turned-off", exception);
            return;
        }
        stopCrashReporting = true;
        java.lang.String tag = "FATAL-CRASH";
        android.util.Log.e(tag, tag + " Uncaught exception - " + thread.getName() + com.navdy.hud.app.framework.glance.GlanceConstants.COLON_SEPARATOR + thread.getId() + " ui thread id:" + android.os.Looper.getMainLooper().getThread().getId(), exception);
        sLogger.i("closing logger");
        com.navdy.service.library.log.Logger.close();
        if (exceptionHandler != null) {
            exceptionHandler.uncaughtException(thread, exception);
        }
    }

    public void checkForKernelCrashes(java.lang.String[] kernelCrashFiles) {
        if (kernelCrashFiles != null && kernelCrashFiles.length != 0) {
            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.util.CrashReporter.Anon4(kernelCrashFiles), 1);
        }
    }

    /* access modifiers changed from: private */
    public java.lang.String filterKernelCrash(java.io.File file) {
        try {
            java.lang.String str = com.navdy.service.library.util.IOUtils.convertFileToString(file.getAbsolutePath());
            if (str == null) {
                return str;
            }
            if (!USER_REBOOT.matcher(str).find()) {
                java.util.regex.Matcher match = START_OF_OOPS.matcher(str);
                int start = str.length() - 32768;
                if (match.find() && match.start() < start) {
                    start = match.start();
                }
                int start2 = java.lang.Math.max(0, start);
                return str.substring(start2, java.lang.Math.min(str.length() - start2, 32768));
            }
            sLogger.v("Ignoring user reboot log");
            return null;
        } catch (Throwable t) {
            sLogger.e(t);
            return null;
        }
    }

    /* access modifiers changed from: private */
    public void cleanupKernelCrashes(java.lang.String[] kernelCrashFiles) {
        for (java.lang.String crashFile : kernelCrashFiles) {
            java.io.File f = new java.io.File(crashFile);
            if (f.exists()) {
                sLogger.v("Trying to delete " + crashFile);
                if (!f.delete()) {
                    sLogger.e("Unable to delete kernel crash file: " + crashFile);
                }
            }
        }
    }

    public void checkForTombstones() {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        try {
            sLogger.v("checking for tombstones");
            java.io.File f = new java.io.File(TOMBSTONE_PATH);
            if (!f.exists() || !f.isDirectory()) {
                sLogger.v("no tombstones");
                return;
            }
            java.lang.String[] list = f.list(new com.navdy.hud.app.util.CrashReporter.Anon5());
            if (list == null || list.length == 0) {
                sLogger.v("no tombstones");
                return;
            }
            for (int i = 0; i < list.length; i++) {
                java.io.File tombstoneFile = new java.io.File(TOMBSTONE_PATH + java.io.File.separator + list[i]);
                if (tombstoneFile.exists()) {
                    java.lang.String fileName = tombstoneFile.getAbsolutePath();
                    java.lang.String str = com.navdy.service.library.util.IOUtils.convertFileToString(fileName);
                    if (!android.text.TextUtils.isEmpty(str)) {
                        reportNonFatalException(new com.navdy.hud.app.util.CrashReporter.TombstoneException(str));
                        sLogger.v("tombstone exception logged file[" + fileName + "] size[" + str.length() + "]");
                    }
                    sLogger.v("tombstone deleted: " + tombstoneFile.delete());
                }
            }
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }

    public void stopCrashReporting(boolean b) {
        setCrashReporting(b);
    }

    private static void setCrashReporting(boolean b) {
        stopCrashReporting = b;
    }

    /* access modifiers changed from: private */
    public boolean checkForCrashes() throws java.lang.Throwable {
        if (com.navdy.hud.app.framework.network.NetworkBandwidthController.getInstance().isNetworkAccessAllowed(com.navdy.hud.app.framework.network.NetworkBandwidthController.Component.HOCKEY)) {
            try {
                net.hockeyapp.android.CrashManager.submitStackTraces(new java.lang.ref.WeakReference(com.navdy.hud.app.HudApplication.getAppContext()), this.crashManagerListener);
            } catch (Throwable t) {
                sLogger.e(t);
            }
        }
        return true;
    }

    public void checkForAnr() {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        try {
            sLogger.v("checking for anr's");
            if (new java.io.File(ANR_PATH).exists()) {
                java.lang.String str = com.navdy.service.library.util.IOUtils.convertFileToString(ANR_PATH);
                if (!android.text.TextUtils.isEmpty(str)) {
                    reportNonFatalException(new com.navdy.hud.app.util.CrashReporter.AnrException(str));
                    sLogger.v("anr crash created size[" + str.length() + "]");
                }
                cleanupAnrFiles();
                return;
            }
            sLogger.v("no anr files");
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }

    public synchronized void reportOTAFailure(com.navdy.hud.app.util.OTAUpdateService.OTAFailedException failure) {
        if (this.installed) {
            reportNonFatalException(failure);
        } else {
            if (this.otaFailure != null) {
                sLogger.e("multiple OTAFailedException reports");
            }
            this.otaFailure = failure;
        }
    }

    public synchronized void checkForOTAFailure() {
        if (this.otaFailure != null) {
            reportNonFatalException(this.otaFailure);
            this.otaFailure = null;
        }
    }

    private void cleanupAnrFiles() {
        java.io.File f = new java.io.File(ANR_PATH);
        if (f.exists()) {
            sLogger.v("delete /data/anr/traces.txt :" + f.delete());
        }
    }

    private void saveHockeyAppException(java.lang.Throwable throwable) {
        try {
            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.util.CrashReporter.Anon6(throwable), 1);
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }

    public boolean isInstalled() {
        return this.installed;
    }

    private void setHockeyAppCrashUploading(boolean enable) {
        boolean z = true;
        try {
            sLogger.v("setHockeyAppCrashUploading: try to enabled:" + enable);
            java.lang.reflect.Field f = net.hockeyapp.android.CrashManager.class.getDeclaredField("submitting");
            f.setAccessible(true);
            if (enable) {
                z = false;
            }
            f.set(null, java.lang.Boolean.valueOf(z));
            sLogger.v("setHockeyAppCrashUploading: enabled=" + enable);
        } catch (Throwable t) {
            sLogger.e("setHockeyAppCrashUploading", t);
        }
    }
}
