package com.navdy.hud.app.util;

public class MusicArtworkCache {
    private static final java.lang.String SEPARATOR = "_";
    /* access modifiers changed from: private */
    public static java.util.Map<com.navdy.service.library.events.audio.MusicCollectionType, java.lang.String> collectionTypeMap = new java.util.HashMap();
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.util.MusicArtworkCache.class);
    private final java.lang.Object dbLock = new java.lang.Object();

    class Anon1 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.hud.app.util.MusicArtworkCache.CacheEntryHelper val$cacheEntryHelper;
        final /* synthetic */ byte[] val$data;

        Anon1(com.navdy.hud.app.util.MusicArtworkCache.CacheEntryHelper cacheEntryHelper, byte[] bArr) {
            this.val$cacheEntryHelper = cacheEntryHelper;
            this.val$data = bArr;
        }

        public void run() {
            com.navdy.hud.app.storage.cache.DiskLruCache diskLruCache = com.navdy.hud.app.util.picasso.PicassoUtil.getDiskLruCache();
            if (diskLruCache == null) {
                com.navdy.hud.app.util.MusicArtworkCache.sLogger.e("No disk cache");
                return;
            }
            diskLruCache.put(this.val$cacheEntryHelper.getFileName(), this.val$data);
            com.navdy.hud.app.util.MusicArtworkCache.this.createDbEntry(this.val$cacheEntryHelper);
        }
    }

    class Anon2 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.hud.app.util.MusicArtworkCache.CacheEntryHelper val$cacheEntryHelper;
        final /* synthetic */ com.navdy.hud.app.util.MusicArtworkCache.Callback val$callback;

        Anon2(com.navdy.hud.app.util.MusicArtworkCache.CacheEntryHelper cacheEntryHelper, com.navdy.hud.app.util.MusicArtworkCache.Callback callback) {
            this.val$cacheEntryHelper = cacheEntryHelper;
            this.val$callback = callback;
        }

        public void run() {
            java.lang.String fileName = com.navdy.hud.app.util.MusicArtworkCache.this.getFileNameFromDb(this.val$cacheEntryHelper);
            if (fileName == null) {
                com.navdy.hud.app.util.MusicArtworkCache.sLogger.i("No entry for " + this.val$cacheEntryHelper);
                this.val$callback.onMiss();
                return;
            }
            com.navdy.hud.app.storage.cache.DiskLruCache diskLruCache = com.navdy.hud.app.util.picasso.PicassoUtil.getDiskLruCache();
            if (diskLruCache == null) {
                com.navdy.hud.app.util.MusicArtworkCache.sLogger.e("No disk cache");
                this.val$callback.onMiss();
                return;
            }
            byte[] data = diskLruCache.get(fileName);
            if (data != null) {
                this.val$callback.onHit(data);
            } else {
                this.val$callback.onMiss();
            }
        }
    }

    private class CacheEntryHelper {
        java.lang.String album;
        java.lang.String author;
        java.lang.String fileName;
        java.lang.String name;
        @android.support.annotation.NonNull
        java.lang.String type;

        CacheEntryHelper(java.lang.String author2, java.lang.String album2, java.lang.String name2) {
            this.type = com.navdy.hud.app.storage.db.table.MusicArtworkCacheTable.TYPE_MUSIC;
            this.author = author2;
            this.album = album2;
            this.name = name2;
        }

        CacheEntryHelper(@android.support.annotation.NonNull com.navdy.service.library.events.audio.MusicCollectionInfo collectionInfo) {
            this.type = (java.lang.String) com.navdy.hud.app.util.MusicArtworkCache.collectionTypeMap.get(collectionInfo.collectionType);
            switch (collectionInfo.collectionType) {
                case COLLECTION_TYPE_UNKNOWN:
                    com.navdy.hud.app.util.MusicArtworkCache.sLogger.e("Unknown type");
                    return;
                case COLLECTION_TYPE_PLAYLISTS:
                    this.name = collectionInfo.name;
                    return;
                case COLLECTION_TYPE_ARTISTS:
                    this.author = collectionInfo.name;
                    return;
                case COLLECTION_TYPE_ALBUMS:
                    this.album = collectionInfo.name;
                    this.author = collectionInfo.subtitle;
                    return;
                case COLLECTION_TYPE_PODCASTS:
                    this.album = collectionInfo.name;
                    return;
                case COLLECTION_TYPE_AUDIOBOOKS:
                    return;
                default:
                    this.name = collectionInfo.name;
                    return;
            }
        }

        /* access modifiers changed from: private */
        public java.lang.String getFileName() {
            if (this.fileName == null) {
                java.lang.StringBuilder builder = new java.lang.StringBuilder(this.type);
                if (!android.text.TextUtils.isEmpty(this.author)) {
                    builder.append("_");
                    builder.append(this.author);
                }
                if (!android.text.TextUtils.isEmpty(this.album)) {
                    builder.append("_");
                    builder.append(this.album);
                }
                if (!android.text.TextUtils.isEmpty(this.name)) {
                    builder.append("_");
                    builder.append(this.name);
                }
                this.fileName = formatIdentifier(builder);
            }
            return this.fileName;
        }

        private java.lang.String formatIdentifier(java.lang.CharSequence unformatted) {
            java.util.regex.Matcher matcher = java.util.regex.Pattern.compile("[^A-Za-z0-9_]").matcher(unformatted);
            if (!android.text.TextUtils.isEmpty(unformatted)) {
                return matcher.replaceAll("_");
            }
            return null;
        }

        public java.lang.String toString() {
            return this.type + ", " + this.author + " - " + this.album + " - " + this.name;
        }
    }

    public interface Callback {
        void onHit(@android.support.annotation.NonNull byte[] bArr);

        void onMiss();
    }

    static {
        collectionTypeMap.put(com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_PLAYLISTS, com.navdy.hud.app.storage.db.table.MusicArtworkCacheTable.TYPE_PLAYLIST);
        collectionTypeMap.put(com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_ARTISTS, com.navdy.hud.app.storage.db.table.MusicArtworkCacheTable.TYPE_MUSIC);
        collectionTypeMap.put(com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_ALBUMS, com.navdy.hud.app.storage.db.table.MusicArtworkCacheTable.TYPE_MUSIC);
        collectionTypeMap.put(com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_PODCASTS, com.navdy.hud.app.storage.db.table.MusicArtworkCacheTable.TYPE_PODCAST);
        collectionTypeMap.put(com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_AUDIOBOOKS, com.navdy.hud.app.storage.db.table.MusicArtworkCacheTable.TYPE_AUDIOBOOK);
    }

    public void putArtwork(java.lang.String author, java.lang.String album, java.lang.String name, byte[] artwork) {
        putArtworkInternal(new com.navdy.hud.app.util.MusicArtworkCache.CacheEntryHelper(author, album, name), artwork);
    }

    public void putArtwork(@android.support.annotation.NonNull com.navdy.service.library.events.audio.MusicCollectionInfo collectionInfo, byte[] artwork) {
        putArtworkInternal(new com.navdy.hud.app.util.MusicArtworkCache.CacheEntryHelper(collectionInfo), artwork);
    }

    private void putArtworkInternal(com.navdy.hud.app.util.MusicArtworkCache.CacheEntryHelper cacheEntryHelper, byte[] data) {
        if (cacheEntryHelper.getFileName() == null) {
            sLogger.e("Couldn't generate a filename for: " + cacheEntryHelper);
        } else {
            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.util.MusicArtworkCache.Anon1(cacheEntryHelper, data), 22);
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:35:?, code lost:
        return;
     */
    public void createDbEntry(com.navdy.hud.app.util.MusicArtworkCache.CacheEntryHelper cacheEntryHelper) {
        sLogger.d("createDbEntry: " + cacheEntryHelper);
        if (cacheEntryHelper.author == null && cacheEntryHelper.album == null && cacheEntryHelper.name == null) {
            sLogger.w("Not cacheable");
            return;
        }
        android.content.ContentValues values = new android.content.ContentValues();
        values.put("type", cacheEntryHelper.type);
        if (cacheEntryHelper.author != null) {
            values.put(com.navdy.hud.app.storage.db.table.MusicArtworkCacheTable.AUTHOR, cacheEntryHelper.author);
        }
        if (cacheEntryHelper.album != null) {
            values.put(com.navdy.hud.app.storage.db.table.MusicArtworkCacheTable.ALBUM, cacheEntryHelper.album);
        }
        if (cacheEntryHelper.author != null) {
            values.put("name", cacheEntryHelper.name);
        }
        values.put(com.navdy.hud.app.storage.db.table.MusicArtworkCacheTable.FILE_NAME, cacheEntryHelper.getFileName());
        synchronized (this.dbLock) {
            android.database.sqlite.SQLiteDatabase db = com.navdy.hud.app.storage.db.HudDatabase.getInstance().getWritableDatabase();
            if (db == null) {
                sLogger.e("Couldn't get db");
                return;
            }
            long rowId = db.insertWithOnConflict(com.navdy.hud.app.storage.db.table.MusicArtworkCacheTable.TABLE_NAME, null, values, 4);
            if (rowId == -1) {
                db.update(com.navdy.hud.app.storage.db.table.MusicArtworkCacheTable.TABLE_NAME, values, "rowid = ?", new java.lang.String[]{java.lang.String.valueOf(rowId)});
            }
        }
    }

    public void getArtwork(@android.support.annotation.NonNull java.lang.String artist, java.lang.String album, java.lang.String name, com.navdy.hud.app.util.MusicArtworkCache.Callback callback) {
        getArtworkInternal(new com.navdy.hud.app.util.MusicArtworkCache.CacheEntryHelper(artist, album, name), callback);
    }

    public void getArtwork(@android.support.annotation.NonNull com.navdy.service.library.events.audio.MusicCollectionInfo musicCollectionInfo, @android.support.annotation.NonNull com.navdy.hud.app.util.MusicArtworkCache.Callback callback) {
        getArtworkInternal(new com.navdy.hud.app.util.MusicArtworkCache.CacheEntryHelper(musicCollectionInfo), callback);
    }

    private void getArtworkInternal(@android.support.annotation.NonNull com.navdy.hud.app.util.MusicArtworkCache.CacheEntryHelper cacheEntryHelper, @android.support.annotation.NonNull com.navdy.hud.app.util.MusicArtworkCache.Callback callback) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.util.MusicArtworkCache.Anon2(cacheEntryHelper, callback), 22);
    }

    /* access modifiers changed from: private */
    public java.lang.String getFileNameFromDb(@android.support.annotation.NonNull com.navdy.hud.app.util.MusicArtworkCache.CacheEntryHelper cacheEntryHelper) {
        sLogger.d("getFileNameFromDb: " + cacheEntryHelper);
        android.database.Cursor cursor = null;
        try {
            java.lang.String[] columns = {com.navdy.hud.app.storage.db.table.MusicArtworkCacheTable.FILE_NAME};
            java.util.ArrayList<java.lang.String> args = new java.util.ArrayList<>();
            java.lang.StringBuilder selection = new java.lang.StringBuilder("type = ?");
            args.add(cacheEntryHelper.type);
            if (cacheEntryHelper.author != null) {
                selection.append(" AND author = ?");
                args.add(cacheEntryHelper.author);
            }
            if (cacheEntryHelper.album != null) {
                selection.append(" AND album = ?");
                args.add(cacheEntryHelper.album);
            }
            if (!android.text.TextUtils.equals(cacheEntryHelper.type, com.navdy.hud.app.storage.db.table.MusicArtworkCacheTable.TYPE_MUSIC) || (cacheEntryHelper.name != null && cacheEntryHelper.album == null)) {
                selection.append(" AND name = ?");
                args.add(cacheEntryHelper.name != null ? cacheEntryHelper.name : "");
            }
            synchronized (this.dbLock) {
                android.database.sqlite.SQLiteDatabase db = com.navdy.hud.app.storage.db.HudDatabase.getInstance().getWritableDatabase();
                if (db == null) {
                    sLogger.e("Couldn't get db");
                    com.navdy.service.library.util.IOUtils.closeObject(null);
                    return null;
                }
                cursor = db.query(com.navdy.hud.app.storage.db.table.MusicArtworkCacheTable.TABLE_NAME, columns, selection.toString(), (java.lang.String[]) args.toArray(new java.lang.String[args.size()]), null, null, null, com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE);
                if (cursor == null || !cursor.moveToFirst()) {
                    com.navdy.service.library.util.IOUtils.closeObject(cursor);
                    return null;
                }
                java.lang.String string = cursor.getString(cursor.getColumnIndex(com.navdy.hud.app.storage.db.table.MusicArtworkCacheTable.FILE_NAME));
                com.navdy.service.library.util.IOUtils.closeObject(cursor);
                return string;
            }
        } catch (Throwable th) {
            com.navdy.service.library.util.IOUtils.closeObject(cursor);
            throw th;
        }
    }
}
