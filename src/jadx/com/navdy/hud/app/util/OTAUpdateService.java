package com.navdy.hud.app.util;

public class OTAUpdateService extends android.app.Service {
    public static final int CAPTURE_GROUP_FROM_VERSION = 2;
    public static final int CAPTURE_GROUP_TARGET_VERSION = 3;
    public static final java.lang.String COMMAND_CHECK_UPDATE_ON_REBOOT = "CHECK_UPDATE";
    public static final java.lang.String COMMAND_DO_NOT_PROMPT = "DO_NOT_PROMPT";
    public static final java.lang.String COMMAND_INSTALL_UPDATE = "INSTALL_UPDATE";
    public static final java.lang.String COMMAND_INSTALL_UPDATE_REBOOT_QUIET = "INSTALL_UPDATE_REBOOT_QUIET";
    public static final java.lang.String COMMAND_INSTALL_UPDATE_SHUTDOWN = "INSTALL_UPDATE_SHUTDOWN";
    public static final java.lang.String COMMAND_VERIFY_UPDATE = "VERIFY_UPDATE";
    public static final java.lang.String DO_NOT_PROMPT = "do_not_prompt";
    public static final java.lang.String EXTRA_COMMAND = "COMMAND";
    private static final java.lang.String FAILED_OTA_FILE = "failed_ota_file";
    private static final long MAX_FILE_SIZE = 2147483648L;
    public static final int MAX_RETRY_COUNT = 3;
    private static final int MAX_ZIP_ENTRIES = 1000;
    public static final java.lang.String RECOVERY_LAST_INSTALL_FILE_PATH = "/cache/recovery/last_install";
    private static final java.lang.String RECOVERY_LAST_LOG_FILE_PATH = "/cache/recovery/last_log";
    public static final java.lang.String RETRY_COUNT = "retry_count";
    public static final java.lang.String UPDATE_FILE = "last_update_file_received";
    private static final java.lang.String UPDATE_FROM_VERSION = "update_from_version";
    public static final java.lang.String VERIFIED = "verified";
    public static final java.lang.String VERSION_PREFIX = "1.0.";
    private static final java.util.regex.Pattern sFileNamePattern = java.util.regex.Pattern.compile("^navdy_ota_(([0-9]+)_to_)?([0-9]+)\\.zip");
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.util.OTAUpdateService.class);
    /* access modifiers changed from: private */
    public static java.lang.String swVersion;
    @javax.inject.Inject
    com.squareup.otto.Bus bus;
    /* access modifiers changed from: private */
    public boolean installing;
    @javax.inject.Inject
    android.content.SharedPreferences sharedPreferences;

    class Anon1 implements java.lang.Runnable {
        final /* synthetic */ java.lang.String val$postUpdateCommand;

        Anon1(java.lang.String str) {
            this.val$postUpdateCommand = str;
        }

        public void run() {
            try {
                com.navdy.hud.app.util.OTAUpdateService.sLogger.d("Applying the update");
                java.io.File file = com.navdy.hud.app.util.OTAUpdateService.checkUpdateFile(com.navdy.hud.app.util.OTAUpdateService.this, com.navdy.hud.app.util.OTAUpdateService.this.sharedPreferences);
                if (file != null) {
                    try {
                        int retryCount = com.navdy.hud.app.util.OTAUpdateService.this.sharedPreferences.getInt(com.navdy.hud.app.util.OTAUpdateService.RETRY_COUNT, 0);
                        com.navdy.hud.app.util.OTAUpdateService.this.sharedPreferences.edit().putInt(com.navdy.hud.app.util.OTAUpdateService.RETRY_COUNT, retryCount + 1).putString(com.navdy.hud.app.util.OTAUpdateService.UPDATE_FROM_VERSION, android.os.Build.VERSION.INCREMENTAL).commit();
                        com.navdy.hud.app.util.OTAUpdateService.sLogger.d("Trying to the install the update, retry count :" + (retryCount + 1));
                        com.navdy.hud.app.analytics.AnalyticsSupport.recordInstallOTAUpdate(com.navdy.hud.app.util.OTAUpdateService.this.sharedPreferences);
                        com.navdy.hud.app.util.CrashReporter.getInstance().stopCrashReporting(true);
                    } catch (Throwable e) {
                        com.navdy.hud.app.util.OTAUpdateService.sLogger.e("Exception while running update OTA package", e);
                        com.navdy.hud.app.util.CrashReporter.getInstance().reportNonFatalException(e);
                        com.navdy.hud.app.util.OTAUpdateService.this.installing = false;
                        return;
                    }
                    com.navdy.hud.app.util.OTAUpdateService.sLogger.i(com.navdy.hud.app.HudApplication.NOT_A_CRASH);
                    com.navdy.hud.app.obd.ObdDeviceConfigurationManager.turnOffOBDSleep();
                    com.navdy.hud.app.device.dial.DialManager.getInstance().requestDialReboot(false);
                    java.lang.Thread.sleep(2000);
                    com.navdy.hud.app.util.OTAUpdateService.this.installPackage(com.navdy.hud.app.util.OTAUpdateService.this, file, this.val$postUpdateCommand);
                } else {
                    com.navdy.hud.app.util.OTAUpdateService.clearUpdate(file, com.navdy.hud.app.util.OTAUpdateService.this, com.navdy.hud.app.util.OTAUpdateService.this.sharedPreferences);
                }
            } catch (java.lang.Exception e2) {
                com.navdy.hud.app.util.OTAUpdateService.sLogger.e("Failed to install OTA package", e2);
            } finally {
                com.navdy.hud.app.util.OTAUpdateService.this.installing = false;
            }
        }
    }

    class Anon2 implements java.lang.Runnable {
        Anon2() {
        }

        public void run() {
            com.navdy.hud.app.util.OTAUpdateService.sLogger.d("Checking the update");
            java.io.File file = com.navdy.hud.app.util.OTAUpdateService.checkUpdateFile(com.navdy.hud.app.util.OTAUpdateService.this, com.navdy.hud.app.util.OTAUpdateService.this.sharedPreferences);
            if (file == null) {
                com.navdy.hud.app.util.OTAUpdateService.sLogger.e("No update file found");
                com.navdy.hud.app.util.OTAUpdateService.this.checkIfIncrementalUpdatesFailed(null);
                com.navdy.hud.app.util.OTAUpdateService.clearUpdate(file, com.navdy.hud.app.util.OTAUpdateService.this, com.navdy.hud.app.util.OTAUpdateService.this.sharedPreferences);
                return;
            }
            boolean currentUpdateValid = com.navdy.hud.app.util.OTAUpdateService.this.checkIfIncrementalUpdatesFailed(file);
            com.navdy.hud.app.util.OTAUpdateService.sLogger.d("Current update valid ? :" + currentUpdateValid);
            if (currentUpdateValid) {
                if (!com.navdy.hud.app.util.OTAUpdateService.isOTAUpdateVerified(com.navdy.hud.app.util.OTAUpdateService.this.sharedPreferences)) {
                    if (!com.navdy.hud.app.util.OTAUpdateService.this.bVerifyUpdate(file, com.navdy.hud.app.util.OTAUpdateService.this.sharedPreferences)) {
                        com.navdy.hud.app.util.OTAUpdateService.clearUpdate(file, com.navdy.hud.app.util.OTAUpdateService.this, com.navdy.hud.app.util.OTAUpdateService.this.sharedPreferences);
                        return;
                    } else {
                        com.navdy.hud.app.util.OTAUpdateService.this.sharedPreferences.edit().putBoolean(com.navdy.hud.app.util.OTAUpdateService.VERIFIED, true).commit();
                        com.navdy.hud.app.util.OTAUpdateService.this.bus.post(new com.navdy.hud.app.util.OTAUpdateService.UpdateVerified());
                    }
                }
                com.navdy.hud.app.util.OTAUpdateService.swVersion = com.navdy.hud.app.util.OTAUpdateService.getUpdateVersion(com.navdy.hud.app.util.OTAUpdateService.this.sharedPreferences);
                com.navdy.hud.app.util.OTAUpdateService.sLogger.v("verified swVersion:" + com.navdy.hud.app.util.OTAUpdateService.swVersion);
                return;
            }
            com.navdy.hud.app.util.OTAUpdateService.clearUpdate(file, com.navdy.hud.app.util.OTAUpdateService.this, com.navdy.hud.app.util.OTAUpdateService.this.sharedPreferences);
        }
    }

    class Anon3 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.hud.app.util.OTAUpdateService.OTAFailedException val$exception;

        Anon3(com.navdy.hud.app.util.OTAUpdateService.OTAFailedException oTAFailedException) {
            this.val$exception = oTAFailedException;
        }

        public void run() {
            com.navdy.hud.app.util.CrashReporter.getInstance().reportOTAFailure(this.val$exception);
        }
    }

    class Anon4 implements java.lang.Runnable {
        Anon4() {
        }

        public void run() {
            int incrementalVersion = -1;
            try {
                incrementalVersion = java.lang.Integer.parseInt(android.os.Build.VERSION.INCREMENTAL);
            } catch (java.lang.NumberFormatException e) {
                com.navdy.hud.app.util.OTAUpdateService.sLogger.e("Cannot parse the incremental version of the build :" + android.os.Build.VERSION.INCREMENTAL);
            }
            if (incrementalVersion == -1) {
                java.lang.String fileName = com.navdy.hud.app.util.OTAUpdateService.this.sharedPreferences.getString(com.navdy.hud.app.util.OTAUpdateService.UPDATE_FILE, null);
                if (!android.text.TextUtils.isEmpty(fileName)) {
                    incrementalVersion = com.navdy.hud.app.util.OTAUpdateService.extractIncrementalVersion(fileName);
                }
            }
            if (incrementalVersion > 0) {
                java.io.File otaUpdatesFileDirectory = new java.io.File(com.navdy.hud.app.storage.PathManager.getInstance().getDirectoryForFileType(com.navdy.service.library.events.file.FileType.FILE_TYPE_OTA));
                if (otaUpdatesFileDirectory.exists()) {
                    java.io.File[] children = otaUpdatesFileDirectory.listFiles();
                    if (children != null) {
                        for (java.io.File file : children) {
                            if (file.isFile() && com.navdy.hud.app.util.OTAUpdateService.isOtaFile(file.getName()) && com.navdy.hud.app.util.OTAUpdateService.extractIncrementalVersion(file.getName()) <= incrementalVersion) {
                                com.navdy.hud.app.util.OTAUpdateService.sLogger.e("Clearing the old update file :" + file.getName());
                                com.navdy.service.library.util.IOUtils.deleteFile(com.navdy.hud.app.HudApplication.getAppContext(), file.getAbsolutePath());
                            }
                        }
                    }
                }
            }
        }
    }

    class Anon5 implements android.os.RecoverySystem.ProgressListener {
        Anon5() {
        }

        public void onProgress(int progress) {
            com.navdy.hud.app.util.OTAUpdateService.sLogger.d(java.lang.String.format("Verify progress: %d%% completed", new java.lang.Object[]{java.lang.Integer.valueOf(progress)}));
        }
    }

    public static class IncrementalOTAFailureDetected {
    }

    public static class InstallingUpdate {
    }

    public static class OTADownloadIntentsReceiver extends android.content.BroadcastReceiver {
        /* access modifiers changed from: private */
        public android.content.SharedPreferences sharedPreferences;

        class Anon1 implements java.lang.Runnable {
            final /* synthetic */ android.content.Context val$context;
            final /* synthetic */ java.lang.String val$path;

            Anon1(java.lang.String str, android.content.Context context) {
                this.val$path = str;
                this.val$context = context;
            }

            public void run() {
                java.io.File file = new java.io.File(this.val$path);
                if (com.navdy.hud.app.util.OTAUpdateService.isOtaFile(file.getName())) {
                    java.lang.String oldUpdateFile = com.navdy.hud.app.util.OTAUpdateService.OTADownloadIntentsReceiver.this.sharedPreferences.getString(com.navdy.hud.app.util.OTAUpdateService.UPDATE_FILE, null);
                    java.lang.String newFileName = file.getAbsolutePath();
                    com.navdy.hud.app.util.OTAUpdateService.OTADownloadIntentsReceiver.this.sharedPreferences.edit().putString(com.navdy.hud.app.util.OTAUpdateService.UPDATE_FILE, newFileName).remove(com.navdy.hud.app.util.OTAUpdateService.DO_NOT_PROMPT).remove(com.navdy.hud.app.util.OTAUpdateService.RETRY_COUNT).remove(com.navdy.hud.app.util.OTAUpdateService.VERIFIED).commit();
                    if (!newFileName.equals(oldUpdateFile)) {
                        if (oldUpdateFile != null) {
                            com.navdy.service.library.util.IOUtils.deleteFile(this.val$context, oldUpdateFile);
                            com.navdy.hud.app.util.OTAUpdateService.sLogger.d("Delete the old update file " + oldUpdateFile + ", new update :" + newFileName);
                        }
                        com.navdy.hud.app.analytics.AnalyticsSupport.recordDownloadOTAUpdate(com.navdy.hud.app.util.OTAUpdateService.OTADownloadIntentsReceiver.this.sharedPreferences);
                    }
                    com.navdy.hud.app.util.OTAUpdateService.startServiceToVerifyUpdate();
                }
            }
        }

        public OTADownloadIntentsReceiver(android.content.SharedPreferences prefs) {
            this.sharedPreferences = prefs;
        }

        public void onReceive(android.content.Context context, android.content.Intent intent) {
            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.util.OTAUpdateService.OTADownloadIntentsReceiver.Anon1(intent.getStringExtra("path"), context), 1);
        }
    }

    public static class OTAFailedException extends java.lang.Exception {
        public java.lang.String last_install;
        public java.lang.String last_log;

        OTAFailedException(java.lang.String install, java.lang.String log) {
            super("OTA installation failed");
            this.last_install = install;
            this.last_log = log;
        }
    }

    public static class UpdateVerified {
    }

    public void onCreate() {
        super.onCreate();
        mortar.Mortar.inject(com.navdy.hud.app.HudApplication.getAppContext(), this);
    }

    public int onStartCommand(android.content.Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        java.lang.String command = intent.getStringExtra("COMMAND");
        sLogger.d("OTA Update service started command[" + command + "]");
        if (intent.hasExtra("COMMAND")) {
            char c = 65535;
            switch (command.hashCode()) {
                case -2036275187:
                    if (command.equals(COMMAND_INSTALL_UPDATE)) {
                        c = 2;
                        break;
                    }
                    break;
                case -1672139796:
                    if (command.equals(COMMAND_INSTALL_UPDATE_REBOOT_QUIET)) {
                        c = 0;
                        break;
                    }
                    break;
                case 227671624:
                    if (command.equals(COMMAND_INSTALL_UPDATE_SHUTDOWN)) {
                        c = 1;
                        break;
                    }
                    break;
                case 1813668687:
                    if (command.equals(COMMAND_VERIFY_UPDATE)) {
                        c = 4;
                        break;
                    }
                    break;
                case 1870386340:
                    if (command.equals(COMMAND_DO_NOT_PROMPT)) {
                        c = 3;
                        break;
                    }
                    break;
            }
            switch (c) {
                case 0:
                    update("--reboot_quiet_after");
                    break;
                case 1:
                    update("--shutdown_after");
                    break;
                case 2:
                    update(null);
                    break;
                case 3:
                    this.sharedPreferences.edit().putBoolean(DO_NOT_PROMPT, true).commit();
                    break;
                case 4:
                    checkForUpdate();
                    cleanupOldFiles();
                    break;
                default:
                    sLogger.e("onStartCommand() invalid command: " + command);
                    break;
            }
        }
        return 2;
    }

    /* access modifiers changed from: private */
    public void installPackage(android.content.Context context, java.io.File file, java.lang.String postUpdateCommand) throws java.io.IOException {
        com.navdy.hud.app.analytics.AnalyticsSupport.recordShutdown(com.navdy.hud.app.event.Shutdown.Reason.OTA, false);
        try {
            android.os.RecoverySystem.class.getMethod("installPackage", new java.lang.Class[]{android.content.Context.class, java.io.File.class, java.lang.String.class}).invoke(null, new java.lang.Object[]{context, file, postUpdateCommand});
        } catch (java.lang.reflect.InvocationTargetException ie) {
            java.lang.Throwable e = ie.getCause();
            if (e instanceof java.io.IOException) {
                throw ((java.io.IOException) e);
            }
            sLogger.e("unexpected exception from RecoverySystem.installPackage()" + e);
        } catch (java.lang.Exception e2) {
            sLogger.e("error invoking Recovery.installPackage(): " + e2);
            android.os.RecoverySystem.installPackage(context, file);
        }
    }

    private void update(java.lang.String postUpdateCommand) {
        if (!this.installing) {
            this.installing = true;
            this.bus.post(new com.navdy.hud.app.util.OTAUpdateService.InstallingUpdate());
            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.util.OTAUpdateService.Anon1(postUpdateCommand), 1);
        }
    }

    public void checkForUpdate() {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.util.OTAUpdateService.Anon2(), 1);
    }

    private void reportOTAFailure(java.lang.String updateFileName) {
        java.lang.String last_install;
        java.lang.String last_log;
        if (updateFileName.equals(this.sharedPreferences.getString(FAILED_OTA_FILE, ""))) {
            sLogger.v("update failure already reported for " + updateFileName);
            return;
        }
        java.lang.String str = "";
        java.lang.String str2 = "";
        try {
            last_install = "\n========== /cache/recovery/last_install\n" + com.navdy.service.library.util.IOUtils.convertFileToString(RECOVERY_LAST_INSTALL_FILE_PATH);
        } catch (java.io.IOException e) {
            last_install = "\n*** failed to read /cache/recovery/last_install";
            sLogger.e("exception reading /cache/recovery/last_install", e);
        }
        try {
            last_log = "\n========== /cache/recovery/last_log\n" + com.navdy.service.library.util.IOUtils.convertFileToString(RECOVERY_LAST_LOG_FILE_PATH);
        } catch (java.io.IOException e2) {
            last_log = "\n*** failed to read /cache/recovery/last_log";
            sLogger.e("exception reading /cache/recovery/last_log", e2);
        }
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.util.OTAUpdateService.Anon3(new com.navdy.hud.app.util.OTAUpdateService.OTAFailedException(last_install, last_log)), 1);
        this.sharedPreferences.edit().putString(FAILED_OTA_FILE, updateFileName).commit();
    }

    /* access modifiers changed from: private */
    public boolean checkIfIncrementalUpdatesFailed(java.io.File currentUpdate) {
        java.io.File lastInstallFile = new java.io.File(RECOVERY_LAST_INSTALL_FILE_PATH);
        if (!lastInstallFile.exists()) {
            return true;
        }
        try {
            java.lang.String lastInstallInfo = com.navdy.service.library.util.IOUtils.convertFileToString(lastInstallFile.getAbsolutePath());
            if (android.text.TextUtils.isEmpty(lastInstallInfo)) {
                return true;
            }
            java.lang.String[] lines = lastInstallInfo.split(com.navdy.hud.app.framework.glance.GlanceConstants.NEWLINE);
            if (lines.length < 2) {
                return true;
            }
            java.lang.String fileName = lines[0];
            sLogger.d("Last installed update was " + fileName);
            java.lang.String updateBaseName = new java.io.File(fileName).getName();
            boolean lastInstallSucceeded = java.lang.Integer.parseInt(lines[1]) != 0;
            if (!lastInstallSucceeded) {
                reportOTAFailure(updateBaseName);
            }
            sLogger.d("Last install succeeded: " + (lastInstallSucceeded ? com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE : "0"));
            int fromVersion = extractFromVersion(updateBaseName);
            boolean isIncremental = fromVersion != -1;
            java.lang.String priorVersion = this.sharedPreferences.getString(UPDATE_FROM_VERSION, "");
            if (!android.text.TextUtils.isEmpty(priorVersion)) {
                com.navdy.hud.app.analytics.AnalyticsSupport.recordOTAInstallResult(isIncremental, priorVersion, lastInstallSucceeded);
                this.sharedPreferences.edit().remove(UPDATE_FROM_VERSION).commit();
            }
            if (!isIncremental) {
                return true;
            }
            sLogger.d("Last install was an incremental update");
            int incrementalVersion = 0;
            try {
                incrementalVersion = java.lang.Integer.parseInt(android.os.Build.VERSION.INCREMENTAL);
            } catch (java.lang.NumberFormatException e) {
                sLogger.e("Cannot parse the incremental version of the build :" + android.os.Build.VERSION.INCREMENTAL);
            }
            if (incrementalVersion != fromVersion) {
                return true;
            }
            sLogger.d("The last installed incremental from version is same as current version :" + incrementalVersion);
            if (lastInstallSucceeded) {
                return true;
            }
            sLogger.e("Last incremental update has failed");
            this.bus.post(new com.navdy.hud.app.util.OTAUpdateService.IncrementalOTAFailureDetected());
            if (currentUpdate == null || !currentUpdate.getAbsolutePath().equals(fileName)) {
                return true;
            }
            sLogger.d("Current update is the same as the one failed during last install");
            return false;
        } catch (java.io.IOException e2) {
            sLogger.e("Error reading the file :" + lastInstallFile.getAbsolutePath(), e2);
            return true;
        }
    }

    private void cleanupOldFiles() {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.util.OTAUpdateService.Anon4(), 1);
    }

    /* access modifiers changed from: private */
    public static int extractIncrementalVersion(java.lang.String fileName) {
        java.util.regex.Matcher match = sFileNamePattern.matcher(fileName);
        if (match.find() && match.groupCount() > 0) {
            try {
                return java.lang.Integer.parseInt(match.group(3));
            } catch (java.lang.NumberFormatException e) {
                sLogger.e("Cannot parse the file to retrieve the target version" + e);
                e.printStackTrace();
            }
        }
        return -1;
    }

    private static int extractFromVersion(java.lang.String fileName) {
        int i = -1;
        java.util.regex.Matcher match = sFileNamePattern.matcher(fileName);
        if (!match.find() || match.groupCount() <= 0) {
            return i;
        }
        java.lang.String versionString = match.group(2);
        if (versionString == null) {
            return i;
        }
        try {
            return java.lang.Integer.parseInt(versionString);
        } catch (java.lang.NumberFormatException e) {
            sLogger.e("Cannot parse the file to retrieve the from version" + e);
            e.printStackTrace();
            return i;
        }
    }

    public static boolean isOtaFile(java.lang.String fileName) {
        java.util.regex.Matcher match = sFileNamePattern.matcher(fileName);
        return match != null && match.find();
    }

    public android.os.IBinder onBind(android.content.Intent intent) {
        return null;
    }

    /* access modifiers changed from: private */
    public static java.io.File checkUpdateFile(android.content.Context context, android.content.SharedPreferences sharedPreferences2) {
        sLogger.d("Checking for update file");
        swVersion = null;
        java.lang.String fileName = sharedPreferences2.getString(UPDATE_FILE, null);
        if (fileName == null) {
            sLogger.e("No update available");
            return null;
        }
        java.io.File file = new java.io.File(fileName);
        if (!file.isFile() || !file.exists() || !file.canRead()) {
            sLogger.e("Invalid update file");
            return null;
        }
        int incrementalVersion = 0;
        try {
            incrementalVersion = java.lang.Integer.parseInt(android.os.Build.VERSION.INCREMENTAL);
        } catch (java.lang.NumberFormatException e) {
            sLogger.e("Cannot parse the incremental version of the build :" + android.os.Build.VERSION.INCREMENTAL);
        }
        sLogger.d("Incremental version of the build on the device:" + incrementalVersion);
        int updateVersion = extractIncrementalVersion(file.getName());
        int fromVersion = extractFromVersion(file.getName());
        sLogger.d("Update from version " + fromVersion);
        if (fromVersion != -1 && incrementalVersion != 0 && fromVersion != incrementalVersion) {
            sLogger.e("Update from version mismatch " + fromVersion + " " + incrementalVersion);
            clearUpdate(file, context, sharedPreferences2);
            return null;
        } else if (updateVersion <= incrementalVersion) {
            sLogger.e("Already up to date :" + incrementalVersion);
            clearUpdate(file, context, sharedPreferences2);
            return null;
        } else if (sharedPreferences2.getInt(RETRY_COUNT, 0) < 3) {
            return file;
        } else {
            sLogger.d("Maximum retries. Deleting the update file.");
            clearUpdate(file, context, sharedPreferences2);
            return null;
        }
    }

    public static java.lang.String getIncrementalUpdateVersion(android.content.SharedPreferences sharedPreferences2) {
        java.lang.String fileName = sharedPreferences2.getString(UPDATE_FILE, null);
        if (fileName == null) {
            return null;
        }
        int updateVersion = extractIncrementalVersion(new java.io.File(fileName).getName());
        if (updateVersion > 0) {
            return java.lang.String.valueOf(updateVersion);
        }
        return null;
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0040  */
    /* JADX WARNING: Removed duplicated region for block: B:24:? A[RETURN, SYNTHETIC] */
    public static java.lang.String getUpdateVersion(android.content.SharedPreferences sharedPreferences2) {
        java.lang.String fileName = sharedPreferences2.getString(UPDATE_FILE, null);
        if (fileName == null) {
            return null;
        }
        java.util.zip.ZipFile zipFile = null;
        java.io.InputStream inputStream = null;
        java.lang.String versionName = null;
        try {
            java.util.zip.ZipFile zipFile2 = new java.util.zip.ZipFile(new java.io.File(fileName));
            try {
                java.util.zip.ZipEntry entry = zipFile2.getEntry("META-INF/com/android/metadata");
                java.util.Properties properties = new java.util.Properties();
                inputStream = zipFile2.getInputStream(entry);
                properties.load(inputStream);
                versionName = shortVersion(properties.getProperty("version-name"));
                com.navdy.service.library.util.IOUtils.closeObject(inputStream);
                com.navdy.service.library.util.IOUtils.closeObject(zipFile2);
                java.util.zip.ZipFile zipFile3 = zipFile2;
            } catch (java.io.IOException e) {
                e = e;
                zipFile = zipFile2;
                try {
                    sLogger.w("Error extracting version-name from ota metadata ", e);
                    com.navdy.service.library.util.IOUtils.closeObject(inputStream);
                    com.navdy.service.library.util.IOUtils.closeObject(zipFile);
                    if (android.text.TextUtils.isEmpty(versionName)) {
                    }
                } catch (Throwable th) {
                    th = th;
                    com.navdy.service.library.util.IOUtils.closeObject(inputStream);
                    com.navdy.service.library.util.IOUtils.closeObject(zipFile);
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                zipFile = zipFile2;
                com.navdy.service.library.util.IOUtils.closeObject(inputStream);
                com.navdy.service.library.util.IOUtils.closeObject(zipFile);
                throw th;
            }
        } catch (java.io.IOException e2) {
            e = e2;
            sLogger.w("Error extracting version-name from ota metadata ", e);
            com.navdy.service.library.util.IOUtils.closeObject(inputStream);
            com.navdy.service.library.util.IOUtils.closeObject(zipFile);
            if (android.text.TextUtils.isEmpty(versionName)) {
            }
        }
        if (android.text.TextUtils.isEmpty(versionName)) {
            return "1.0." + getIncrementalUpdateVersion(sharedPreferences2);
        }
        return versionName;
    }

    public static java.lang.String shortVersion(java.lang.String versionName) {
        if (versionName != null) {
            return versionName.split("-")[0];
        }
        return null;
    }

    public static java.lang.String getCurrentVersion() {
        return shortVersion(com.navdy.hud.app.BuildConfig.VERSION_NAME);
    }

    public static void clearUpdate(java.io.File file, android.content.Context context, android.content.SharedPreferences sharedPreferences2) {
        if (file != null) {
            sLogger.d("Clearing the update");
            com.navdy.service.library.util.IOUtils.deleteFile(context, file.getAbsolutePath());
        }
        sharedPreferences2.edit().remove(UPDATE_FILE).remove(RETRY_COUNT).remove(DO_NOT_PROMPT).remove(VERIFIED).commit();
    }

    public static boolean isUpdateAvailable() {
        return !android.text.TextUtils.isEmpty(swVersion);
    }

    public boolean bVerifyUpdate(java.io.File file, android.content.SharedPreferences preferences) {
        try {
            android.os.RecoverySystem.verifyPackage(file, new com.navdy.hud.app.util.OTAUpdateService.Anon5(), null);
            verifyOTAZipFile(file, 1000, MAX_FILE_SIZE);
            return true;
        } catch (java.io.IOException | java.security.GeneralSecurityException e) {
            sLogger.e("Exception while verifying the update package " + e);
            return false;
        }
    }

    public static boolean isOTAUpdateVerified(android.content.SharedPreferences preferences) {
        return preferences.getBoolean(VERIFIED, false);
    }

    public static boolean verifyOTAZipFile(java.io.File file, int maxEntries, long maxSize) {
        java.util.zip.ZipFile zipFile = null;
        try {
            java.io.File destinationDirectory = file.getParentFile();
            java.lang.String destinationCanonicalPath = destinationDirectory.getCanonicalPath();
            java.util.zip.ZipFile zipFile2 = new java.util.zip.ZipFile(file);
            try {
                java.util.Enumeration entries = zipFile2.entries();
                int entryCount = 0;
                long uncompressedSize = 0;
                while (entries.hasMoreElements()) {
                    java.util.zip.ZipEntry entry = (java.util.zip.ZipEntry) entries.nextElement();
                    entryCount++;
                    if (!entry.isDirectory()) {
                        long size = entry.getSize();
                        if (size > 0) {
                            uncompressedSize += size;
                        }
                    }
                    if (entryCount > maxEntries) {
                        sLogger.d("OTA zip file failed verification : Too many entries " + entryCount);
                        com.navdy.service.library.util.IOUtils.closeObject(zipFile2);
                        java.util.zip.ZipFile zipFile3 = zipFile2;
                        return false;
                    } else if (uncompressedSize > maxSize) {
                        sLogger.d("OTA zip file failed verification : Exceeded max uncompressed size " + maxSize);
                        com.navdy.service.library.util.IOUtils.closeObject(zipFile2);
                        java.util.zip.ZipFile zipFile4 = zipFile2;
                        return false;
                    } else {
                        java.lang.String zipEntryCanonicalPath = new java.io.File(destinationDirectory, entry.getName()).getCanonicalPath();
                        if (!android.text.TextUtils.isEmpty(zipEntryCanonicalPath)) {
                            if (!zipEntryCanonicalPath.startsWith(destinationCanonicalPath)) {
                            }
                        }
                        sLogger.d("OTA zip failed verification : Zip entry tried to write outside destination " + entry.getName() + " , Canonical path " + zipEntryCanonicalPath);
                        com.navdy.service.library.util.IOUtils.closeObject(zipFile2);
                        java.util.zip.ZipFile zipFile5 = zipFile2;
                        return false;
                    }
                }
                com.navdy.service.library.util.IOUtils.closeObject(zipFile2);
                java.util.zip.ZipFile zipFile6 = zipFile2;
                return true;
            } catch (java.io.IOException e) {
                e = e;
                zipFile = zipFile2;
                try {
                    sLogger.e("Exception while verifying the OTA package " + e);
                    e.printStackTrace();
                    com.navdy.service.library.util.IOUtils.closeObject(zipFile);
                    return false;
                } catch (Throwable th) {
                    th = th;
                    com.navdy.service.library.util.IOUtils.closeObject(zipFile);
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                zipFile = zipFile2;
                com.navdy.service.library.util.IOUtils.closeObject(zipFile);
                throw th;
            }
        } catch (java.io.IOException e2) {
            e = e2;
            sLogger.e("Exception while verifying the OTA package " + e);
            e.printStackTrace();
            com.navdy.service.library.util.IOUtils.closeObject(zipFile);
            return false;
        }
    }

    public static void startServiceToVerifyUpdate() {
        android.content.Context context = com.navdy.hud.app.HudApplication.getAppContext();
        android.content.Intent updateServiceIntent = new android.content.Intent(context, com.navdy.hud.app.util.OTAUpdateService.class);
        updateServiceIntent.putExtra("COMMAND", COMMAND_VERIFY_UPDATE);
        context.startService(updateServiceIntent);
    }

    public static java.lang.String getSWUpdateVersion() {
        return swVersion;
    }
}
