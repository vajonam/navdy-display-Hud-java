package com.navdy.hud.app.util;

public class GenericUtil {
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.util.GenericUtil.class);

    public static java.lang.String getErrorMessage(java.lang.Throwable t) {
        if (t == null) {
            return com.navdy.hud.app.HudApplication.getAppContext().getResources().getString(com.navdy.hud.app.R.string.operation_failed);
        }
        java.lang.String str = t.getMessage();
        return android.text.TextUtils.isEmpty(str) ? t.toString() : str;
    }

    public static int getDrawableResourceIdFromName(java.lang.String idName) {
        android.content.Context context = com.navdy.hud.app.HudApplication.getAppContext();
        return context.getResources().getIdentifier(idName, "drawable", context.getPackageName());
    }

    public static int getIdsResourceIdFromName(java.lang.String idName) {
        android.content.Context context = com.navdy.hud.app.HudApplication.getAppContext();
        return context.getResources().getIdentifier(idName, "id", context.getPackageName());
    }

    public static int getStringResourceIdFromName(java.lang.String idName) {
        android.content.Context context = com.navdy.hud.app.HudApplication.getAppContext();
        return context.getResources().getIdentifier(idName, "string", context.getPackageName());
    }

    public static void checkNotOnMainThread() {
        if (android.os.Looper.myLooper() == android.os.Looper.getMainLooper()) {
            throw new java.lang.RuntimeException("cannot call on mainthread");
        }
    }

    public static boolean isMainThread() {
        if (android.os.Looper.myLooper() == android.os.Looper.getMainLooper()) {
            return true;
        }
        return false;
    }

    public static java.lang.String normalizeToFilename(java.lang.String str) {
        return android.text.TextUtils.isEmpty(str) ? str : str.replaceAll("[^a-zA-Z0-9\\._]+", "");
    }

    public static java.lang.String removePunctuation(java.lang.String str) {
        return android.text.TextUtils.isEmpty(str) ? str : str.replaceAll("\\p{Punct}+", " ");
    }

    public static void sleep(int millis) {
        try {
            java.lang.Thread.sleep((long) millis);
        } catch (Throwable th) {
        }
    }

    public static int indexOf(byte[] srcBytes, byte[] searchBytes, int startIndex, int endIndex) {
        int loopEndIdx;
        if (searchBytes.length == 0 || (endIndex - startIndex) + 1 < searchBytes.length) {
            return -1;
        }
        int maxScanStartPosIdx = srcBytes.length - searchBytes.length;
        if (endIndex < maxScanStartPosIdx) {
            loopEndIdx = endIndex;
        } else {
            loopEndIdx = maxScanStartPosIdx;
        }
        for (int i = startIndex; i <= loopEndIdx; i++) {
            boolean found = true;
            int j = 0;
            while (true) {
                if (j >= searchBytes.length) {
                    break;
                } else if (srcBytes[i + j] != searchBytes[j]) {
                    found = false;
                    break;
                } else {
                    j++;
                }
            }
            if (found) {
                return i;
            }
        }
        return -1;
    }

    public static void clearInputMethodManagerFocusLeak() {
        android.view.inputmethod.InputMethodManager inputMethodManager = (android.view.inputmethod.InputMethodManager) com.navdy.hud.app.HudApplication.getAppContext().getSystemService("input_method");
        try {
            java.lang.reflect.Method finishInputLockedMethod = android.view.inputmethod.InputMethodManager.class.getDeclaredMethod("finishInputLocked", new java.lang.Class[0]);
            finishInputLockedMethod.setAccessible(true);
            sLogger.v("calling finishInputLocked");
            finishInputLockedMethod.invoke(inputMethodManager, new java.lang.Object[0]);
            sLogger.v("called finishInputLocked");
        } catch (Throwable t) {
            sLogger.v("finishInputLocked", t);
        }
    }

    public static boolean isClientiOS() {
        com.navdy.service.library.events.DeviceInfo deviceInfo = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getRemoteDeviceInfo();
        if (deviceInfo == null || deviceInfo.platform != com.navdy.service.library.events.DeviceInfo.Platform.PLATFORM_iOS) {
            return false;
        }
        return true;
    }
}
