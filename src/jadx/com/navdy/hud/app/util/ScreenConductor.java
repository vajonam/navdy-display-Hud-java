package com.navdy.hud.app.util;

public class ScreenConductor<S extends mortar.Blueprint> implements com.navdy.hud.app.util.CanShowScreen<S> {
    private final android.view.ViewGroup container;
    private final android.content.Context context;
    com.navdy.hud.app.ui.component.homescreen.HomeScreenView homeScreenView;
    private final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(getClass());
    com.navdy.hud.app.ui.framework.UIStateManager uiStateManager;

    class Anon1 implements android.view.animation.Animation.AnimationListener {
        final /* synthetic */ int val$animIn;
        final /* synthetic */ com.navdy.hud.app.screen.BaseScreen val$newScreen;
        final /* synthetic */ com.navdy.hud.app.screen.BaseScreen val$oldScreen;

        Anon1(com.navdy.hud.app.screen.BaseScreen baseScreen, int i, com.navdy.hud.app.screen.BaseScreen baseScreen2) {
            this.val$oldScreen = baseScreen;
            this.val$animIn = i;
            this.val$newScreen = baseScreen2;
        }

        public void onAnimationStart(android.view.animation.Animation animation) {
            this.val$oldScreen.onAnimationOutStart();
            if (this.val$animIn == -1) {
                com.navdy.hud.app.util.ScreenConductor.this.uiStateManager.postScreenAnimationEvent(true, this.val$newScreen, this.val$oldScreen);
            }
        }

        public void onAnimationEnd(android.view.animation.Animation animation) {
            this.val$oldScreen.onAnimationOutEnd();
            if (this.val$animIn == -1) {
                com.navdy.hud.app.util.ScreenConductor.this.uiStateManager.postScreenAnimationEvent(false, this.val$newScreen, this.val$oldScreen);
            }
        }

        public void onAnimationRepeat(android.view.animation.Animation animation) {
        }
    }

    class Anon2 implements android.view.animation.Animation.AnimationListener {
        final /* synthetic */ com.navdy.hud.app.screen.BaseScreen val$newScreen;
        final /* synthetic */ com.navdy.hud.app.screen.BaseScreen val$oldScreen;

        Anon2(com.navdy.hud.app.screen.BaseScreen baseScreen, com.navdy.hud.app.screen.BaseScreen baseScreen2) {
            this.val$newScreen = baseScreen;
            this.val$oldScreen = baseScreen2;
        }

        public void onAnimationStart(android.view.animation.Animation animation) {
            this.val$newScreen.onAnimationInStart();
            com.navdy.hud.app.util.ScreenConductor.this.uiStateManager.postScreenAnimationEvent(true, this.val$newScreen, this.val$oldScreen);
        }

        public void onAnimationEnd(android.view.animation.Animation animation) {
            this.val$newScreen.onAnimationInEnd();
            com.navdy.hud.app.util.ScreenConductor.this.uiStateManager.postScreenAnimationEvent(false, this.val$newScreen, this.val$oldScreen);
        }

        public void onAnimationRepeat(android.view.animation.Animation animation) {
        }
    }

    public ScreenConductor(android.content.Context context2, android.view.ViewGroup container2, com.navdy.hud.app.ui.framework.UIStateManager uiStateManager2) {
        this.context = context2;
        this.container = container2;
        this.uiStateManager = uiStateManager2;
    }

    public void createScreens() {
        if (this.homeScreenView == null) {
            com.navdy.hud.app.screen.BaseScreen screen = new com.navdy.hud.app.ui.component.homescreen.HomeScreen();
            this.homeScreenView = (com.navdy.hud.app.ui.component.homescreen.HomeScreenView) flow.Layouts.createView(mortar.Mortar.getScope(this.context).requireChild(screen).createContext(this.context), (java.lang.Object) screen);
            this.homeScreenView.setVisibility(4);
            this.container.addView(this.homeScreenView, 0);
            this.uiStateManager.setHomescreenView(this.homeScreenView);
            this.logger.v("createScreens:homescreen created and added");
        }
    }

    public void showScreen(S screen, flow.Flow.Direction direction, int animIn, int animOut) {
        mortar.MortarScope newChildScope = mortar.Mortar.getScope(this.context).requireChild(screen);
        android.view.View oldChild = getChildView();
        android.view.View newChild = null;
        if (oldChild == null || !mortar.Mortar.getScope(oldChild.getContext()).getName().equals(screen.getMortarScopeName()) || oldChild.getVisibility() != 0) {
            boolean homescreen = screen instanceof com.navdy.hud.app.ui.component.homescreen.HomeScreen;
            boolean homescreenCreated = false;
            if (homescreen && this.homeScreenView != null) {
                this.logger.v("reusing homescreen view");
                newChild = this.homeScreenView;
            }
            if (newChild == null) {
                newChild = flow.Layouts.createView(newChildScope.createContext(this.context), (java.lang.Object) screen);
                if (homescreen) {
                    this.homeScreenView = (com.navdy.hud.app.ui.component.homescreen.HomeScreenView) newChild;
                    homescreenCreated = true;
                }
            }
            com.navdy.hud.app.screen.BaseScreen oldScreen = this.uiStateManager.getCurrentScreen();
            com.navdy.hud.app.screen.BaseScreen newScreen = (com.navdy.hud.app.screen.BaseScreen) screen;
            setAnimation(direction, oldChild, newChild, oldScreen, newScreen, animIn, animOut);
            if (!(oldChild == null || oldChild == this.homeScreenView)) {
                this.container.removeView(oldChild);
            }
            if (homescreen) {
                if (this.homeScreenView != null) {
                    this.homeScreenView.setVisibility(4);
                }
                if (homescreenCreated) {
                    this.logger.v("home screen added");
                    this.container.addView(newChild);
                }
                this.homeScreenView.onResume();
                this.homeScreenView.setVisibility(0);
            } else {
                if (this.homeScreenView != null) {
                    com.navdy.hud.app.ui.framework.UIStateManager uIStateManager = this.uiStateManager;
                    if (com.navdy.hud.app.ui.framework.UIStateManager.isPauseHomescreen(((com.navdy.hud.app.screen.BaseScreen) screen).getScreen())) {
                        this.homeScreenView.onPause();
                        this.homeScreenView.setVisibility(4);
                    }
                }
                this.container.addView(newChild);
            }
            com.navdy.hud.app.util.GenericUtil.clearInputMethodManagerFocusLeak();
            this.uiStateManager.setMainActiveScreen((com.navdy.hud.app.screen.BaseScreen) screen);
            if (((com.navdy.hud.app.screen.BaseScreen) screen).getScreen() != com.navdy.service.library.events.ui.Screen.SCREEN_BACK) {
                com.navdy.hud.app.analytics.AnalyticsSupport.recordScreen((com.navdy.hud.app.screen.BaseScreen) screen);
            }
            if (animIn == -1 && animOut == -1) {
                this.uiStateManager.postScreenAnimationEvent(false, newScreen, oldScreen);
                return;
            }
            return;
        }
        com.navdy.hud.app.screen.BaseScreen baseScreen = (com.navdy.hud.app.screen.BaseScreen) screen;
        this.uiStateManager.postScreenAnimationEvent(true, baseScreen, baseScreen);
        this.uiStateManager.postScreenAnimationEvent(false, baseScreen, baseScreen);
    }

    /* access modifiers changed from: protected */
    public void setAnimation(flow.Flow.Direction direction, android.view.View oldChild, android.view.View newChild, com.navdy.hud.app.screen.BaseScreen oldScreen, com.navdy.hud.app.screen.BaseScreen newScreen, int animIn, int animOut) {
        if (oldChild == null) {
            if (newScreen != null) {
                newScreen.onAnimationInStart();
                this.uiStateManager.postScreenAnimationEvent(true, newScreen, null);
                newScreen.onAnimationInEnd();
                this.uiStateManager.postScreenAnimationEvent(false, newScreen, null);
            }
        } else if (animIn == -1 && animOut == -1) {
            this.uiStateManager.postScreenAnimationEvent(true, newScreen, oldScreen);
        } else {
            if (animOut != -1) {
                android.view.animation.Animation animation = android.view.animation.AnimationUtils.loadAnimation(this.context, animOut);
                if (oldScreen != null) {
                    animation.setAnimationListener(new com.navdy.hud.app.util.ScreenConductor.Anon1(oldScreen, animIn, newScreen));
                }
                oldChild.setAnimation(animation);
            }
            if (animIn != -1) {
                android.view.animation.Animation animation2 = android.view.animation.AnimationUtils.loadAnimation(this.context, animIn);
                if (newScreen != null) {
                    animation2.setAnimationListener(new com.navdy.hud.app.util.ScreenConductor.Anon2(newScreen, oldScreen));
                }
                newChild.setAnimation(animation2);
            }
        }
    }

    public android.view.View getChildView() {
        int count = this.container.getChildCount();
        if (count == 0) {
            return null;
        }
        for (int count2 = count - 1; count2 >= 0; count2--) {
            android.view.View view = this.container.getChildAt(count2);
            if (view.getVisibility() == 0) {
                return view;
            }
        }
        return null;
    }
}
