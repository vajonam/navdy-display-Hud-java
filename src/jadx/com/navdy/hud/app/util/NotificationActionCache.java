package com.navdy.hud.app.util;

public class NotificationActionCache {
    private java.lang.String handler;
    private int lastId = 1;

    public NotificationActionCache(java.lang.Class handler2) {
        this.handler = handler2.getName();
    }

    public boolean validAction(com.navdy.service.library.events.notification.NotificationAction action) {
        return action.handler.equals(this.handler);
    }

    public void markComplete(com.navdy.service.library.events.notification.NotificationAction action) {
    }

    public com.navdy.service.library.events.notification.NotificationAction buildAction(int notificationId, int actionId, int labelId) {
        return buildAction(notificationId, actionId, labelId, (java.lang.String) null);
    }

    public com.navdy.service.library.events.notification.NotificationAction buildAction(int notificationId, int actionId, int labelId, java.lang.String label) {
        java.lang.String str = this.handler;
        int i = this.lastId;
        this.lastId = i + 1;
        return new com.navdy.service.library.events.notification.NotificationAction(str, java.lang.Integer.valueOf(i), java.lang.Integer.valueOf(notificationId), java.lang.Integer.valueOf(actionId), java.lang.Integer.valueOf(labelId), label, null, null);
    }

    public com.navdy.service.library.events.notification.NotificationAction buildAction(int notificationId, int actionId, int iconId, int focusedIconId) {
        java.lang.String str = this.handler;
        int i = this.lastId;
        this.lastId = i + 1;
        return new com.navdy.service.library.events.notification.NotificationAction(str, java.lang.Integer.valueOf(i), java.lang.Integer.valueOf(notificationId), java.lang.Integer.valueOf(actionId), null, null, java.lang.Integer.valueOf(iconId), java.lang.Integer.valueOf(focusedIconId));
    }
}
