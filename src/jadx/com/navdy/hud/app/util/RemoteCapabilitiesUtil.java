package com.navdy.hud.app.util;

public class RemoteCapabilitiesUtil {
    public static boolean supportsVoiceSearch() {
        com.navdy.service.library.events.Capabilities capabilities = getRemoteCapabilities();
        if (capabilities != null) {
            return java.lang.Boolean.TRUE.equals(capabilities.voiceSearch);
        }
        return false;
    }

    public static boolean supportsPlayAudioRequest() {
        com.navdy.service.library.events.Capabilities capabilities = getRemoteCapabilities();
        if (capabilities != null) {
            return java.lang.Boolean.TRUE.equals(capabilities.supportsPlayAudioRequest);
        }
        return false;
    }

    public static boolean supportsPlaceSearch() {
        com.navdy.service.library.events.Capabilities capabilities = getRemoteCapabilities();
        if (capabilities != null) {
            return java.lang.Boolean.TRUE.equals(capabilities.placeTypeSearch);
        }
        return com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().doesRemoteDeviceHasCapability(com.navdy.service.library.events.LegacyCapability.CAPABILITY_PLACE_TYPE_SEARCH);
    }

    private static com.navdy.service.library.events.Capabilities getRemoteCapabilities() {
        com.navdy.service.library.events.DeviceInfo deviceInfo = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getRemoteDeviceInfo();
        if (deviceInfo != null) {
            return deviceInfo.capabilities;
        }
        return null;
    }

    public static boolean supportsNavigationCoordinateLookup() {
        com.navdy.service.library.events.Capabilities capabilities = getRemoteCapabilities();
        return capabilities != null && java.lang.Boolean.TRUE.equals(capabilities.navCoordsLookup);
    }

    public static boolean supportsVoiceSearchNewIOSPauseBehaviors() {
        com.navdy.service.library.events.Capabilities capabilities = getRemoteCapabilities();
        return capabilities != null && java.lang.Boolean.TRUE.equals(capabilities.voiceSearchNewIOSPauseBehaviors);
    }
}
