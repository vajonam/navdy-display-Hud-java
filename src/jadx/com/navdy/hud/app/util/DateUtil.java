package com.navdy.hud.app.util;

public class DateUtil {
    private static final int HOUR_HAND_ANGLE_PER_HOUR = 30;
    private static final int MINUTE_HAND_ANGLE_PER_MINUTE = 6;
    private static java.text.SimpleDateFormat dateLabelFormat = new java.text.SimpleDateFormat("d MMMM", java.util.Locale.US);
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.util.DateUtil.class);

    public static java.util.Date parseIrmcDateStr(java.lang.String str) {
        if (str == null) {
            return null;
        }
        try {
            if (str.length() != 15) {
                return null;
            }
            java.util.Calendar calendar = java.util.Calendar.getInstance();
            calendar.setTimeInMillis(0);
            calendar.set(1, java.lang.Integer.parseInt(str.substring(0, 4)));
            calendar.set(2, java.lang.Integer.parseInt(str.substring(4, 6)) - 1);
            calendar.set(5, java.lang.Integer.parseInt(str.substring(6, 8)));
            calendar.set(11, java.lang.Integer.parseInt(str.substring(9, 11)));
            calendar.set(12, java.lang.Integer.parseInt(str.substring(11, 13)));
            calendar.set(13, java.lang.Integer.parseInt(str.substring(13, 15)));
            return calendar.getTime();
        } catch (Throwable t) {
            sLogger.e(t);
            return null;
        }
    }

    public static float getClockAngleForHour(int hour, int minutes) {
        return ((float) (((((hour % 12) - 3) * 30) + 360) % 360)) + ((((float) minutes) / 60.0f) * 30.0f);
    }

    public static float getClockAngleForMinutes(int minutes) {
        return (float) ((((minutes - 15) * 6) + 360) % 360);
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    public static java.lang.String getDateLabel(java.util.Date d) {
        java.lang.String format;
        try {
            if (android.text.format.DateUtils.isToday(d.getTime())) {
                return com.navdy.hud.app.HudApplication.getAppContext().getResources().getString(com.navdy.hud.app.R.string.today);
            }
            java.util.Date now = new java.util.Date();
            java.util.Calendar nowCal = java.util.Calendar.getInstance();
            nowCal.setTime(now);
            java.util.Calendar dCal = java.util.Calendar.getInstance();
            dCal.setTime(d);
            int year1 = nowCal.get(1);
            int year2 = dCal.get(1);
            int dayOfYear1 = nowCal.get(6);
            int dayOfYear2 = dCal.get(6);
            if (year1 == year2 && dayOfYear1 - dayOfYear2 == 1) {
                return com.navdy.hud.app.HudApplication.getAppContext().getResources().getString(com.navdy.hud.app.R.string.yesterday);
            }
            synchronized (dateLabelFormat) {
                format = dateLabelFormat.format(d);
            }
            return format;
        } catch (Throwable t) {
            sLogger.e(t);
            return null;
        }
    }
}
