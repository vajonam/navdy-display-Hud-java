package com.navdy.hud.app.config;

public class Setting {
    private static int MAX_PROP_LEN = 31;
    private java.lang.String description;
    private java.lang.String name;
    private com.navdy.hud.app.config.Setting.IObserver observer;
    private java.lang.String path;
    private java.lang.String property;

    interface IObserver {
        void onChanged(com.navdy.hud.app.config.Setting setting);
    }

    public Setting(java.lang.String name2, java.lang.String path2, java.lang.String description2) {
        this(name2, path2, description2, "persist.sys." + path2);
    }

    public Setting(java.lang.String name2, java.lang.String path2, java.lang.String description2, java.lang.String property2) {
        this.name = name2;
        this.path = path2;
        this.description = description2;
        if (property2 == null || property2.length() <= MAX_PROP_LEN) {
            this.property = property2;
            return;
        }
        throw new java.lang.IllegalArgumentException("property name (" + property2 + ") too long");
    }

    public java.lang.String getName() {
        return this.name;
    }

    public java.lang.String getPath() {
        return this.path;
    }

    public java.lang.String getDescription() {
        return this.description;
    }

    public java.lang.String getProperty() {
        return this.property;
    }

    public boolean isEnabled() {
        return true;
    }

    /* access modifiers changed from: protected */
    public void changed() {
        if (this.observer != null) {
            this.observer.onChanged(this);
        }
    }

    public void setObserver(com.navdy.hud.app.config.Setting.IObserver observer2) {
        this.observer = observer2;
    }
}
