package com.navdy.hud.app.config;

public class BooleanSetting extends com.navdy.hud.app.config.Setting {
    private boolean changed;
    private boolean defaultValue;
    private boolean enabled;
    private com.navdy.hud.app.config.BooleanSetting.Scope scope;

    public enum Scope {
        NEVER,
        ENG,
        BETA,
        ALWAYS,
        CUSTOM
    }

    public BooleanSetting(java.lang.String name, com.navdy.hud.app.config.BooleanSetting.Scope scope2, java.lang.String path, java.lang.String description) {
        this(name, scope2, false, path, description);
    }

    public BooleanSetting(java.lang.String name, com.navdy.hud.app.config.BooleanSetting.Scope scope2, boolean defaultValue2, java.lang.String path, java.lang.String description) {
        super(name, path, description);
        this.scope = scope2;
        this.defaultValue = defaultValue2;
        load();
    }

    public boolean isEnabled() {
        boolean z = true;
        switch (this.scope) {
            case CUSTOM:
                return this.enabled;
            case ENG:
                if (com.navdy.hud.app.util.DeviceUtil.isUserBuild()) {
                    z = false;
                }
                return z;
            case ALWAYS:
                return true;
            default:
                return false;
        }
    }

    public void setEnabled(boolean enabled2) {
        if (this.scope != com.navdy.hud.app.config.BooleanSetting.Scope.CUSTOM) {
            this.scope = com.navdy.hud.app.config.BooleanSetting.Scope.CUSTOM;
        }
        if (this.enabled != enabled2) {
            this.enabled = enabled2;
            this.changed = true;
            changed();
            save();
        }
    }

    public void save() {
        if (this.changed) {
            this.changed = false;
            java.lang.String propName = getProperty();
            if (propName != null) {
                com.navdy.hud.app.util.os.SystemProperties.set(propName, java.lang.Boolean.toString(this.enabled));
            }
        }
    }

    public void load() {
        java.lang.String propName = getProperty();
        if (propName != null) {
            java.lang.String setting = com.navdy.hud.app.util.os.SystemProperties.get(propName);
            if (!android.text.TextUtils.isEmpty(setting)) {
                this.scope = com.navdy.hud.app.config.BooleanSetting.Scope.CUSTOM;
                this.enabled = java.lang.Boolean.parseBoolean(setting) || setting.equalsIgnoreCase(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE) || setting.equalsIgnoreCase("yes");
                return;
            }
            this.enabled = this.defaultValue;
        }
    }

    public java.lang.String toString() {
        java.lang.StringBuilder sb = new java.lang.StringBuilder("BooleanSetting{");
        sb.append("name=").append(getName());
        sb.append(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA);
        sb.append("enabled=").append(this.enabled);
        sb.append('}');
        return sb.toString();
    }
}
