package com.navdy.hud.app.config;

public class SettingsManager implements com.navdy.hud.app.config.Setting.IObserver {
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.config.SettingsManager.class);
    private final com.squareup.otto.Bus bus;
    private final android.content.SharedPreferences preferences;

    public static class SettingsChanged {
        public final java.lang.String path;
        public final com.navdy.hud.app.config.Setting setting;

        public SettingsChanged(java.lang.String path2, com.navdy.hud.app.config.Setting setting2) {
            this.path = path2;
            this.setting = setting2;
        }
    }

    public SettingsManager(com.squareup.otto.Bus bus2, android.content.SharedPreferences preferences2) {
        this.bus = bus2;
        this.preferences = preferences2;
    }

    public void addSetting(com.navdy.hud.app.config.Setting setting) {
        sLogger.i("Adding setting " + setting);
        setting.setObserver(this);
    }

    public void onChanged(com.navdy.hud.app.config.Setting setting) {
        sLogger.i("Setting changed:" + setting);
        this.bus.post(new com.navdy.hud.app.config.SettingsManager.SettingsChanged(setting.getPath(), setting));
    }
}
