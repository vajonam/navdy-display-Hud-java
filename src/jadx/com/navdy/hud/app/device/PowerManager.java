package com.navdy.hud.app.device;

public class PowerManager {
    public static final java.lang.String BOOT_POWER_MODE = com.navdy.hud.app.util.os.SystemProperties.get(POWER_MODE_PROPERTY);
    private static final java.lang.String COOLING_DEVICE = "/sys/devices/virtual/thermal/cooling_device0/cur_state";
    /* access modifiers changed from: private */
    public static final long COOLING_MONITOR_INTERVAL = java.util.concurrent.TimeUnit.SECONDS.toMillis(15);
    private static final long DEFAULT_INSTANT_ON_TIMEOUT = java.util.concurrent.TimeUnit.HOURS.toMillis(4);
    private static final java.lang.String INSTANT_ON = "persist.sys.instanton";
    private static final long INSTANT_ON_TIMEOUT = com.navdy.hud.app.util.os.SystemProperties.getLong(INSTANT_ON_TIMEOUT_PROPERTRY, DEFAULT_INSTANT_ON_TIMEOUT);
    private static final java.lang.String INSTANT_ON_TIMEOUT_PROPERTRY = "persist.sys.instanton.timeout";
    public static final java.lang.String LAST_LOW_VOLTAGE_EVENT = "last_low_voltage_event";
    public static final java.lang.String NORMAL_MODE = "normal";
    public static final java.lang.String POWER_MODE_PROPERTY = "sys.power.mode";
    public static final java.lang.String QUIET_MODE = "quiet";
    public static final long RECHARGE_TIME = java.util.concurrent.TimeUnit.DAYS.toMillis(7);
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.device.PowerManager.class);
    private boolean awake;
    /* access modifiers changed from: private */
    public com.squareup.otto.Bus bus;
    /* access modifiers changed from: private */
    public java.lang.Runnable checkCoolingState;
    /* access modifiers changed from: private */
    public android.os.Handler handler;
    private long lastLowVoltage;
    private java.lang.reflect.Method mIPowerManagerShutdownMethod;
    private java.lang.Object mPowerManager;
    private java.lang.String oldCoolingState;
    private android.os.PowerManager powerManager;
    private android.content.SharedPreferences preferences;
    private java.lang.Runnable quietModeTimeout;
    private com.navdy.hud.app.device.PowerManager.RunState runState;

    class Anon1 implements java.lang.Runnable {

        /* renamed from: com.navdy.hud.app.device.PowerManager$Anon1$Anon1 reason: collision with other inner class name */
        class C0005Anon1 implements java.lang.Runnable {
            C0005Anon1() {
            }

            public void run() {
                if (com.navdy.hud.app.device.PowerManager.this.updateCoolingState()) {
                    com.navdy.hud.app.device.PowerManager.this.handler.postDelayed(com.navdy.hud.app.device.PowerManager.this.checkCoolingState, com.navdy.hud.app.device.PowerManager.COOLING_MONITOR_INTERVAL);
                }
            }
        }

        Anon1() {
        }

        public void run() {
            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.device.PowerManager.Anon1.C0005Anon1(), 10);
        }
    }

    class Anon2 implements java.lang.Runnable {
        Anon2() {
        }

        public void run() {
            com.navdy.hud.app.device.PowerManager.sLogger.i("Quiet mode has timed out - forcing full shutdown");
            com.navdy.hud.app.device.PowerManager.this.bus.post(new com.navdy.hud.app.event.Shutdown(com.navdy.hud.app.event.Shutdown.Reason.TIMEOUT));
        }
    }

    private enum RunState {
        Unknown,
        Booting,
        Waking,
        Running
    }

    public PowerManager(com.squareup.otto.Bus bus2, android.content.Context context, android.content.SharedPreferences preferences2) {
        this.awake = !QUIET_MODE.equals(BOOT_POWER_MODE);
        this.handler = new android.os.Handler(android.os.Looper.getMainLooper());
        this.checkCoolingState = new com.navdy.hud.app.device.PowerManager.Anon1();
        this.quietModeTimeout = new com.navdy.hud.app.device.PowerManager.Anon2();
        this.mIPowerManagerShutdownMethod = null;
        this.mPowerManager = null;
        this.bus = bus2;
        bus2.register(this);
        this.powerManager = (android.os.PowerManager) context.getSystemService("power");
        this.preferences = preferences2;
        this.lastLowVoltage = preferences2.getLong(LAST_LOW_VOLTAGE_EVENT, -1);
        setupShutdown();
        com.navdy.hud.app.util.os.SystemProperties.set(POWER_MODE_PROPERTY, inQuietMode() ? QUIET_MODE : NORMAL_MODE);
        if (!inQuietMode()) {
            startOverheatMonitoring(com.navdy.hud.app.device.PowerManager.RunState.Booting);
        }
        sLogger.i("quietMode:" + inQuietMode());
    }

    private boolean isCoolingActive(java.lang.String coolingState) {
        return coolingState != null && !coolingState.equals("0");
    }

    /* access modifiers changed from: private */
    public boolean updateCoolingState() {
        try {
            java.lang.String newState = com.navdy.service.library.util.IOUtils.convertFileToString(COOLING_DEVICE).trim();
            if (!newState.equals(this.oldCoolingState)) {
                sLogger.i("Cooling state changed from:" + this.oldCoolingState + " to:" + newState);
                if (!isCoolingActive(this.oldCoolingState) && isCoolingActive(newState)) {
                    com.navdy.hud.app.analytics.AnalyticsSupport.recordCpuOverheat(this.runState.name());
                }
                this.oldCoolingState = newState;
            }
            this.runState = com.navdy.hud.app.device.PowerManager.RunState.Running;
            return true;
        } catch (java.io.IOException exception) {
            sLogger.e("Failed to read cooling device state - stopping monitoring", exception);
            return false;
        }
    }

    private void startOverheatMonitoring(com.navdy.hud.app.device.PowerManager.RunState state) {
        this.runState = state;
        this.handler.removeCallbacks(this.checkCoolingState);
        this.handler.post(this.checkCoolingState);
    }

    public boolean inQuietMode() {
        return !this.awake;
    }

    public static boolean isAwake() {
        return !com.navdy.hud.app.util.os.SystemProperties.get(POWER_MODE_PROPERTY, "").equals(QUIET_MODE);
    }

    public void wakeUp(com.navdy.hud.app.analytics.AnalyticsSupport.WakeupReason reason) {
        if (!this.awake) {
            sLogger.d("waking up");
            this.awake = true;
            com.navdy.hud.app.util.os.SystemProperties.set(POWER_MODE_PROPERTY, NORMAL_MODE);
            this.handler.removeCallbacks(this.quietModeTimeout);
            com.navdy.hud.app.device.light.HUDLightUtils.resetFrontLED(com.navdy.hud.app.device.light.LightManager.getInstance());
            com.navdy.hud.app.device.light.LED.writeToSysfs(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE, "/sys/dlpc/led_enable");
            androidWakeup();
            startOverheatMonitoring(com.navdy.hud.app.device.PowerManager.RunState.Waking);
            this.bus.post(new com.navdy.hud.app.event.Wakeup(reason));
        }
    }

    public void enterSleepMode() {
        this.handler.postDelayed(this.quietModeTimeout, INSTANT_ON_TIMEOUT);
        com.navdy.hud.app.device.light.HUDLightUtils.turnOffFrontLED(com.navdy.hud.app.device.light.LightManager.getInstance());
        androidSleep();
    }

    @com.squareup.otto.Subscribe
    public void onKey(android.view.KeyEvent event) {
        wakeUp(com.navdy.hud.app.analytics.AnalyticsSupport.WakeupReason.DIAL);
    }

    private void androidWakeup() {
        com.navdy.hud.app.framework.toast.ToastManager.getInstance().disableToasts(false);
        try {
            android.os.PowerManager.class.getMethod("wakeUp", new java.lang.Class[]{java.lang.Long.TYPE}).invoke(this.powerManager, new java.lang.Object[]{java.lang.Long.valueOf(android.os.SystemClock.uptimeMillis())});
        } catch (java.lang.Exception e) {
            sLogger.e("error invoking PowerManager.wakeUp(): " + e);
        }
    }

    private void androidSleep() {
        sLogger.d("androidSleep()");
        com.navdy.hud.app.framework.toast.ToastManager.getInstance().disableToasts(true);
        try {
            android.os.PowerManager.class.getMethod("goToSleep", new java.lang.Class[]{java.lang.Long.TYPE}).invoke(this.powerManager, new java.lang.Object[]{java.lang.Long.valueOf(android.os.SystemClock.uptimeMillis())});
        } catch (java.lang.Exception e) {
            sLogger.e("error invoking PowerManager.goToSleep(): " + e);
        }
    }

    public boolean quietModeEnabled() {
        boolean recharging;
        long delta = java.lang.System.currentTimeMillis() - this.lastLowVoltage;
        if (this.lastLowVoltage == -1 || delta >= RECHARGE_TIME) {
            recharging = false;
        } else {
            recharging = true;
        }
        if (recharging) {
            sLogger.d("disabling quiet mode since we had a low voltage event " + java.util.concurrent.TimeUnit.MILLISECONDS.toHours(delta) + " hours ago");
        }
        if (!com.navdy.hud.app.util.os.SystemProperties.getBoolean(INSTANT_ON, true) || recharging) {
            return false;
        }
        return true;
    }

    @android.annotation.SuppressLint({"ApplySharedPref"})
    public void androidShutdown(com.navdy.hud.app.event.Shutdown.Reason reason, boolean forceFullShutdown) {
        boolean z = true;
        sLogger.d("androidShutdown: " + reason + " forceFullShutdown:" + forceFullShutdown);
        com.navdy.hud.app.device.dial.DialManager.getInstance().requestDialReboot(false);
        if (reason == com.navdy.hud.app.event.Shutdown.Reason.LOW_VOLTAGE || reason == com.navdy.hud.app.event.Shutdown.Reason.CRITICAL_VOLTAGE) {
            this.preferences.edit().putLong(LAST_LOW_VOLTAGE_EVENT, java.lang.System.currentTimeMillis()).commit();
        }
        if (forceFullShutdown) {
            z = false;
        }
        com.navdy.hud.app.analytics.AnalyticsSupport.recordShutdown(reason, z);
        com.navdy.hud.app.device.light.LED.writeToSysfs("0", "/sys/dlpc/led_enable");
        if (this.mPowerManager == null || this.mIPowerManagerShutdownMethod == null) {
            sLogger.e("shutdown was not properly initialized");
        } else if (forceFullShutdown) {
            try {
                this.mIPowerManagerShutdownMethod.invoke(this.mPowerManager, new java.lang.Object[]{java.lang.Boolean.valueOf(false), java.lang.Boolean.valueOf(false)});
            } catch (java.lang.Exception e) {
                sLogger.e("exception invoking IPowerManager.shutdown()", e);
            }
        } else {
            this.powerManager.reboot(QUIET_MODE);
        }
    }

    private void setupShutdown() {
        try {
            android.os.IBinder powerBinder = (android.os.IBinder) java.lang.Class.forName("android.os.ServiceManager").getDeclaredMethod("getService", new java.lang.Class[]{java.lang.String.class}).invoke(null, new java.lang.Object[]{"power"});
            this.mPowerManager = java.lang.Class.forName("android.os.IPowerManager$Stub").getDeclaredMethod("asInterface", new java.lang.Class[]{android.os.IBinder.class}).invoke(null, new java.lang.Object[]{powerBinder});
            try {
                this.mIPowerManagerShutdownMethod = java.lang.Class.forName("android.os.IPowerManager").getDeclaredMethod("shutdown", new java.lang.Class[]{java.lang.Boolean.TYPE, java.lang.Boolean.TYPE});
            } catch (java.lang.Exception e) {
                sLogger.e("exception getting IPowerManager.shutdown() method", e);
            }
        } catch (java.lang.Exception e2) {
            sLogger.e("exception invoking IPowerManager.Stub.asInterface()", e2);
        }
    }
}
