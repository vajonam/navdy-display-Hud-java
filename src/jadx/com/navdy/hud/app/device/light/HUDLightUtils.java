package com.navdy.hud.app.device.light;

public class HUDLightUtils {
    public static com.navdy.hud.app.device.light.LED.Settings gestureDetectedLedSettings = new com.navdy.hud.app.device.light.LED.Settings.Builder().setName("GestureDetected").setColor(com.navdy.hud.app.HudApplication.getAppContext().getResources().getColor(com.navdy.hud.app.R.color.led_gesture_color)).setIsBlinking(true).setBlinkInfinite(false).setBlinkPulseCount(2).setBlinkFrequency(com.navdy.hud.app.device.light.LED.BlinkFrequency.HIGH).build();
    public static com.navdy.hud.app.device.light.LED.Settings pairingLedSettings = new com.navdy.hud.app.device.light.LED.Settings.Builder().setName("Pairing").setColor(com.navdy.hud.app.HudApplication.getAppContext().getResources().getColor(com.navdy.hud.app.R.color.led_dial_pairing_color)).setIsBlinking(true).setBlinkInfinite(true).setBlinkFrequency(com.navdy.hud.app.device.light.LED.BlinkFrequency.LOW).build();
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.device.light.HUDLightUtils.class);
    public static com.navdy.hud.app.device.light.LED.Settings snapShotCollectionEnd;
    public static com.navdy.hud.app.device.light.LED.Settings snapshotCollectionStart;

    static {
        int color = com.navdy.hud.app.HudApplication.getAppContext().getResources().getColor(com.navdy.hud.app.R.color.led_usb_power_color);
        snapshotCollectionStart = new com.navdy.hud.app.device.light.LED.Settings.Builder().setName("SnapshotStart").setColor(color).setIsBlinking(false).build();
        snapShotCollectionEnd = new com.navdy.hud.app.device.light.LED.Settings.Builder().setName("SnapshotEnd").setColor(color).setIsBlinking(true).setBlinkInfinite(false).setBlinkPulseCount(3).setBlinkFrequency(com.navdy.hud.app.device.light.LED.BlinkFrequency.HIGH).build();
    }

    public static void showPairing(android.content.Context context, com.navdy.hud.app.device.light.LightManager manager, boolean show) {
        com.navdy.hud.app.device.light.LED frontLed = (com.navdy.hud.app.device.light.LED) manager.getLight(0);
        if (frontLed == null) {
            return;
        }
        if (show) {
            sLogger.d("Front LED, showing dial pairing");
            frontLed.pushSetting(pairingLedSettings);
            return;
        }
        sLogger.d("Front LED, stop showing dial pairing");
        frontLed.removeSetting(pairingLedSettings);
    }

    public static com.navdy.hud.app.device.light.LED.Settings showGestureDetectionEnabled(android.content.Context context, com.navdy.hud.app.device.light.LightManager manager, java.lang.String logName) {
        com.navdy.hud.app.device.light.LED frontLed = (com.navdy.hud.app.device.light.LED) manager.getLight(0);
        if (frontLed == null) {
            return null;
        }
        sLogger.d("Front LED, showing gesture detection enabled");
        com.navdy.hud.app.device.light.LED.Settings settings = new com.navdy.hud.app.device.light.LED.Settings.Builder().setName("GestureDetectionEnabled-" + logName).setColor(context.getResources().getColor(com.navdy.hud.app.R.color.led_gesture_color)).setIsBlinking(true).setBlinkInfinite(true).setBlinkFrequency(com.navdy.hud.app.device.light.LED.BlinkFrequency.LOW).build();
        frontLed.pushSetting(settings);
        return settings;
    }

    public static void showSnapshotCollection(android.content.Context context, com.navdy.hud.app.device.light.LightManager manager, boolean finished) {
        com.navdy.hud.app.device.light.LED.Settings settings = finished ? snapShotCollectionEnd : snapshotCollectionStart;
        com.navdy.hud.app.device.light.LED frontLed = (com.navdy.hud.app.device.light.LED) manager.getLight(0);
        if (frontLed != null) {
            frontLed.pushSetting(settings);
            if (finished) {
                frontLed.removeSetting(snapShotCollectionEnd);
                frontLed.removeSetting(snapshotCollectionStart);
            }
        }
    }

    public static void removeSettings(com.navdy.hud.app.device.light.LED.Settings settings) {
        if (settings != null) {
            com.navdy.hud.app.device.light.LED frontLed = (com.navdy.hud.app.device.light.LED) com.navdy.hud.app.device.light.LightManager.getInstance().getLight(0);
            if (frontLed != null) {
                frontLed.removeSetting(settings);
            }
        }
    }

    public static void showGestureDetected(android.content.Context context, com.navdy.hud.app.device.light.LightManager manager) {
        com.navdy.hud.app.device.light.LED frontLed = (com.navdy.hud.app.device.light.LED) manager.getLight(0);
        if (frontLed != null) {
            sLogger.d("Front LED, showing gesture detected");
            frontLed.pushSetting(gestureDetectedLedSettings);
            frontLed.removeSetting(gestureDetectedLedSettings);
        }
    }

    public static void dialActionDetected(android.content.Context context, com.navdy.hud.app.device.light.LightManager manager) {
        com.navdy.hud.app.device.light.LED frontLed = (com.navdy.hud.app.device.light.LED) manager.getLight(0);
        if (frontLed != null) {
            sLogger.d("Front LED, showing dial action detected");
            frontLed.startActivityBlink();
        }
    }

    public static void resetFrontLED(com.navdy.hud.app.device.light.LightManager manager) {
        com.navdy.hud.app.device.light.LED frontLed = (com.navdy.hud.app.device.light.LED) manager.getLight(0);
        if (frontLed != null) {
            sLogger.d("resetFrontLED called");
            frontLed.reset();
        }
    }

    public static void turnOffFrontLED(com.navdy.hud.app.device.light.LightManager manager) {
        com.navdy.hud.app.device.light.LED frontLed = (com.navdy.hud.app.device.light.LED) manager.getLight(0);
        if (frontLed != null) {
            sLogger.d("turnOffFrontLED called");
            frontLed.turnOff();
        }
    }

    public static void showShutDown(com.navdy.hud.app.device.light.LightManager manager) {
        com.navdy.hud.app.device.light.LED frontLed = (com.navdy.hud.app.device.light.LED) manager.getLight(0);
        if (frontLed != null) {
            sLogger.d("Front LED, showing shutdown");
            frontLed.pushSetting(new com.navdy.hud.app.device.light.LED.Settings.Builder().setColor(com.navdy.hud.app.device.light.LED.DEFAULT_COLOR).setIsBlinking(true).setBlinkInfinite(true).setBlinkFrequency(com.navdy.hud.app.device.light.LED.BlinkFrequency.LOW).build());
        }
    }

    public static void showUSBPowerShutDown(android.content.Context context, com.navdy.hud.app.device.light.LightManager manager) {
        com.navdy.hud.app.device.light.LED frontLed = (com.navdy.hud.app.device.light.LED) manager.getLight(0);
        if (frontLed != null) {
            sLogger.d("Front LED, showing USB power shutdown");
            frontLed.pushSetting(new com.navdy.hud.app.device.light.LED.Settings.Builder().setColor(context.getResources().getColor(com.navdy.hud.app.R.color.led_usb_power_color)).setIsBlinking(true).setBlinkInfinite(true).setBlinkFrequency(com.navdy.hud.app.device.light.LED.BlinkFrequency.LOW).build());
        }
    }

    public static void showUSBPowerOn(android.content.Context context, com.navdy.hud.app.device.light.LightManager manager) {
        com.navdy.hud.app.device.light.LED frontLed = (com.navdy.hud.app.device.light.LED) manager.getLight(0);
        if (frontLed != null) {
            sLogger.d("Front LED, showing USB power on, not blinking");
            frontLed.pushSetting(new com.navdy.hud.app.device.light.LED.Settings.Builder().setColor(context.getResources().getColor(com.navdy.hud.app.R.color.led_usb_power_color)).setIsBlinking(false).build());
        }
    }

    public static void showUSBTransfer(android.content.Context context, com.navdy.hud.app.device.light.LightManager manager) {
        com.navdy.hud.app.device.light.LED frontLed = (com.navdy.hud.app.device.light.LED) manager.getLight(0);
        if (frontLed != null) {
            sLogger.d("Front LED, showing USB transfer");
            frontLed.pushSetting(new com.navdy.hud.app.device.light.LED.Settings.Builder().setColor(context.getResources().getColor(com.navdy.hud.app.R.color.led_usb_power_color)).setIsBlinking(true).setBlinkInfinite(true).setBlinkFrequency(com.navdy.hud.app.device.light.LED.BlinkFrequency.HIGH).build());
        }
    }

    public static void showError(android.content.Context context, com.navdy.hud.app.device.light.LightManager manager) {
        com.navdy.hud.app.device.light.LED frontLed = (com.navdy.hud.app.device.light.LED) manager.getLight(0);
        if (frontLed != null) {
            sLogger.d("Front LED, showing error, not blinking");
            frontLed.pushSetting(new com.navdy.hud.app.device.light.LED.Settings.Builder().setColor(context.getResources().getColor(com.navdy.hud.app.R.color.led_error_color)).setIsBlinking(false).build());
        }
    }

    public static void showGestureNotRecognized(android.content.Context context, com.navdy.hud.app.device.light.LightManager manager) {
        com.navdy.hud.app.device.light.LED frontLed = (com.navdy.hud.app.device.light.LED) manager.getLight(0);
        if (frontLed != null) {
            sLogger.d("Front LED, showing gesture not recognized");
            frontLed.pushSetting(new com.navdy.hud.app.device.light.LED.Settings.Builder().setColor(context.getResources().getColor(com.navdy.hud.app.R.color.led_gesture_not_recognized_color)).setIsBlinking(false).setBlinkFrequency(com.navdy.hud.app.device.light.LED.BlinkFrequency.LOW).build());
        }
    }
}
