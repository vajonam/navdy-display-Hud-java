package com.navdy.hud.app.device.light;

public class LightManager {
    public static final int FRONT_LED = 0;
    private static final java.lang.String LED_DEVICE = "/sys/class/leds/as3668";
    private static final com.navdy.hud.app.device.light.LightManager sInstance = new com.navdy.hud.app.device.light.LightManager();
    private android.util.SparseArray<com.navdy.hud.app.device.light.ILight> mLights = new android.util.SparseArray<>();

    public static com.navdy.hud.app.device.light.LightManager getInstance() {
        return sInstance;
    }

    private LightManager() {
        com.navdy.hud.app.device.light.LED frontLED = new com.navdy.hud.app.device.light.LED(LED_DEVICE);
        if (frontLED.isAvailable()) {
            this.mLights.append(0, frontLED);
        }
    }

    public com.navdy.hud.app.device.light.ILight getLight(int id) {
        return (com.navdy.hud.app.device.light.ILight) this.mLights.get(id);
    }
}
