package com.navdy.hud.app.device.light;

public class LED implements com.navdy.hud.app.device.light.ILight {
    private static final java.lang.String BLINK_ATTRIBUTE = "blink";
    public static final int CLEAR_COLOR = 0;
    private static final java.lang.String COLOR_ATTRIBUTE = "color";
    public static final int DEFAULT_COLOR = android.graphics.Color.parseColor("#ff000000");
    private static final com.navdy.hud.app.device.light.LED.Settings DEFAULT_SETTINGS = new com.navdy.hud.app.device.light.LED.Settings.Builder().setName("DEFAULT").setColor(DEFAULT_COLOR).setIsBlinking(false).build();
    private static final java.lang.String SLOPE_DOWN_ATTRUTE = "slope_down";
    private static final java.lang.String SLOPE_UP_ATTRIBUTE = "slope_up";
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.device.light.LED.class);
    private java.util.concurrent.atomic.AtomicBoolean activityBlinkRunning = new java.util.concurrent.atomic.AtomicBoolean(false);
    private java.lang.String blinkPath;
    private java.lang.String colorPath;
    private java.util.LinkedList<com.navdy.hud.app.device.light.LightSettings> mLightSettingsStack;
    private java.util.concurrent.ExecutorService mSerialExecutor;
    private java.lang.String slopeDownPath;
    private java.lang.String slopeUpPath;

    class Anon1 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.hud.app.device.light.LightSettings val$settings;

        Anon1(com.navdy.hud.app.device.light.LightSettings lightSettings) {
            this.val$settings = lightSettings;
        }

        public void run() {
            if (this.val$settings instanceof com.navdy.hud.app.device.light.LED.Settings) {
                com.navdy.hud.app.device.light.LED.Settings ledSettings = (com.navdy.hud.app.device.light.LED.Settings) this.val$settings;
                if (ledSettings.getActivityBlink()) {
                    com.navdy.hud.app.device.light.LED.this.activityBlink();
                } else if (ledSettings.isTurnedOn()) {
                    com.navdy.hud.app.device.light.LED.this.setColor(ledSettings.getColor());
                    if (!ledSettings.isBlinking()) {
                        com.navdy.hud.app.device.light.LED.this.stopBlinking();
                    } else if (ledSettings.isBlinkInfinite()) {
                        com.navdy.hud.app.device.light.LED.this.setBlinkFrequency(ledSettings.getBlinkFrequency());
                        com.navdy.hud.app.device.light.LED.this.startBlinking();
                    } else {
                        com.navdy.hud.app.device.light.LED.this.startBlinking(ledSettings.getBlinkPulseCount(), ledSettings.getColor(), ledSettings.getBlinkFrequency());
                    }
                } else {
                    com.navdy.hud.app.device.light.LED.this.turnOff();
                }
            }
        }
    }

    enum BlinkFrequency {
        LOW(1000),
        HIGH(100);
        
        /* access modifiers changed from: private */
        public int blinkDelay;

        public int getBlinkDelay() {
            return this.blinkDelay;
        }

        private BlinkFrequency(int delay) {
            this.blinkDelay = delay;
        }
    }

    public static class Settings extends com.navdy.hud.app.device.light.LightSettings {
        private boolean activityBlink;
        private com.navdy.hud.app.device.light.LED.BlinkFrequency blinkFrequency;
        private boolean blinkInfinite;
        private int blinkPulseCount;
        private java.lang.String name;

        public static class Builder {
            private boolean activityBlink = false;
            private com.navdy.hud.app.device.light.LED.BlinkFrequency blinkFrequency = com.navdy.hud.app.device.light.LED.BlinkFrequency.LOW;
            private boolean blinkInfinite = true;
            private int blinkPulseCount = 0;
            private int color = com.navdy.hud.app.device.light.LED.DEFAULT_COLOR;
            private boolean isBlinking = true;
            private boolean isTurnedOn = true;
            private java.lang.String name = "UNKNOWN";

            public com.navdy.hud.app.device.light.LED.Settings.Builder setColor(int color2) {
                this.color = color2;
                return this;
            }

            public com.navdy.hud.app.device.light.LED.Settings.Builder setIsTurnedOn(boolean isTurnedOn2) {
                this.isTurnedOn = isTurnedOn2;
                return this;
            }

            public com.navdy.hud.app.device.light.LED.Settings.Builder setIsBlinking(boolean isBlinking2) {
                this.isBlinking = isBlinking2;
                return this;
            }

            public com.navdy.hud.app.device.light.LED.Settings.Builder setBlinkFrequency(com.navdy.hud.app.device.light.LED.BlinkFrequency blinkFrequency2) {
                this.blinkFrequency = blinkFrequency2;
                return this;
            }

            public com.navdy.hud.app.device.light.LED.Settings.Builder setBlinkInfinite(boolean blinkInfinite2) {
                this.blinkInfinite = blinkInfinite2;
                return this;
            }

            public com.navdy.hud.app.device.light.LED.Settings.Builder setBlinkPulseCount(int blinkPulseCount2) {
                this.blinkPulseCount = blinkPulseCount2;
                return this;
            }

            public com.navdy.hud.app.device.light.LED.Settings.Builder setActivityBlink(boolean activityBlink2) {
                this.activityBlink = activityBlink2;
                return this;
            }

            public com.navdy.hud.app.device.light.LED.Settings.Builder setName(java.lang.String name2) {
                this.name = name2;
                return this;
            }

            public com.navdy.hud.app.device.light.LED.Settings build() {
                return new com.navdy.hud.app.device.light.LED.Settings(this.color, this.isTurnedOn, this.isBlinking, this.blinkFrequency, this.blinkInfinite, this.blinkPulseCount, this.activityBlink, this.name);
            }
        }

        public Settings(int color, boolean isTurnedOn, boolean isBlinking, com.navdy.hud.app.device.light.LED.BlinkFrequency blinkFrequency2, boolean blinkInfinite2, int blinkPulseCount2, boolean activityBlink2, java.lang.String name2) {
            super(color, isTurnedOn, isBlinking);
            this.blinkFrequency = blinkFrequency2;
            this.blinkInfinite = blinkInfinite2;
            this.blinkPulseCount = blinkPulseCount2;
            this.activityBlink = activityBlink2;
            this.name = name2;
        }

        public com.navdy.hud.app.device.light.LED.BlinkFrequency getBlinkFrequency() {
            return this.blinkFrequency;
        }

        public boolean isBlinkInfinite() {
            return this.blinkInfinite;
        }

        public int getBlinkPulseCount() {
            return this.blinkPulseCount;
        }

        public boolean getActivityBlink() {
            return this.activityBlink;
        }
    }

    public LED(java.lang.String ledDevice) {
        if (new java.io.File(ledDevice).exists()) {
            this.colorPath = ledDevice + java.io.File.separator + COLOR_ATTRIBUTE;
            this.slopeUpPath = ledDevice + java.io.File.separator + SLOPE_UP_ATTRIBUTE;
            this.slopeDownPath = ledDevice + java.io.File.separator + SLOPE_DOWN_ATTRUTE;
            this.blinkPath = ledDevice + java.io.File.separator + BLINK_ATTRIBUTE;
            this.mLightSettingsStack = new java.util.LinkedList<>();
            this.mSerialExecutor = java.util.concurrent.Executors.newSingleThreadExecutor();
            this.mLightSettingsStack.push(DEFAULT_SETTINGS);
            return;
        }
        sLogger.w("Unable to open led at " + ledDevice);
    }

    public boolean isAvailable() {
        return this.mLightSettingsStack != null;
    }

    public synchronized void turnOn() {
        stopBlinking();
        setColor(DEFAULT_COLOR);
    }

    public synchronized void turnOff() {
        stopBlinking();
        setColor(0);
    }

    public static void writeToSysfs(java.lang.String data, java.lang.String filepath) {
        sLogger.d("writing '" + data + "' to sysfs file '" + filepath + "'");
        writeToKernelDevice(data, filepath);
    }

    /* access modifiers changed from: private */
    public void setColor(int color) {
        writeToKernelDevice(java.lang.String.format("%08x", new java.lang.Object[]{java.lang.Integer.valueOf(color)}), this.colorPath);
    }

    /* access modifiers changed from: private */
    public void startBlinking() {
        writeToKernelDevice(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE, this.blinkPath);
    }

    /* access modifiers changed from: private */
    public void startBlinking(int pulseCount, int color, com.navdy.hud.app.device.light.LED.BlinkFrequency frequency) {
        stopBlinking();
        int blinkDelay = frequency.blinkDelay;
        if (pulseCount > 0) {
            for (int i = 0; i < pulseCount; i++) {
                try {
                    setColor(color);
                    java.lang.Thread.sleep((long) (blinkDelay / 2));
                    setColor(0);
                    java.lang.Thread.sleep((long) (blinkDelay / 2));
                } catch (java.lang.InterruptedException e) {
                    sLogger.e("Interrupted exception while blinking");
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void stopBlinking() {
        if (sLogger.isLoggable(3)) {
            sLogger.d("Stop blinking");
        }
        writeToKernelDevice("0", this.blinkPath);
    }

    /* access modifiers changed from: private */
    public void activityBlink() {
        try {
            stopBlinking();
            setBlinkFrequency(com.navdy.hud.app.device.light.LED.BlinkFrequency.HIGH);
            startBlinking();
            java.lang.Thread.sleep((long) (com.navdy.hud.app.device.light.LED.BlinkFrequency.HIGH.blinkDelay / 2));
        } catch (java.lang.InterruptedException e) {
            sLogger.e("Interrupted exception while activity blinking");
        } catch (Throwable th) {
            this.activityBlinkRunning.set(false);
            throw th;
        }
        stopBlinking();
        this.activityBlinkRunning.set(false);
    }

    public synchronized void pushSetting(com.navdy.hud.app.device.light.LightSettings settings) {
        this.mLightSettingsStack.push(settings);
        applySettings(settings);
    }

    public synchronized void popSetting() {
        if (this.mLightSettingsStack.size() > 1) {
            com.navdy.hud.app.device.light.LightSettings lightSettings = (com.navdy.hud.app.device.light.LightSettings) this.mLightSettingsStack.pop();
            if (lightSettings != null) {
                com.navdy.hud.app.device.light.LightSettings lightSettings2 = (com.navdy.hud.app.device.light.LED.Settings) lightSettings;
            }
        }
        applySettings((com.navdy.hud.app.device.light.LightSettings) this.mLightSettingsStack.peek());
    }

    public synchronized void removeSetting(com.navdy.hud.app.device.light.LightSettings settings) {
        if (this.mLightSettingsStack.peek() == settings) {
            popSetting();
        } else {
            this.mLightSettingsStack.remove(settings);
        }
    }

    public synchronized void reset() {
        this.mLightSettingsStack.clear();
        pushSetting(DEFAULT_SETTINGS);
    }

    /* access modifiers changed from: private */
    public void setBlinkFrequency(com.navdy.hud.app.device.light.LED.BlinkFrequency frequency) {
        if (frequency != null) {
            writeToKernelDevice(java.lang.String.valueOf(frequency.getBlinkDelay()), this.slopeUpPath);
            writeToKernelDevice(java.lang.String.valueOf(frequency.getBlinkDelay()), this.slopeDownPath);
        }
    }

    private void applySettings(com.navdy.hud.app.device.light.LightSettings settings) {
        this.mSerialExecutor.execute(new com.navdy.hud.app.device.light.LED.Anon1(settings));
    }

    private static void writeToKernelDevice(java.lang.String data, java.lang.String kernelFilePath) {
        try {
            try {
                java.io.FileWriter writer = new java.io.FileWriter(new java.io.File(kernelFilePath));
                writer.write(data);
                writer.flush();
                writer.close();
            } catch (java.io.IOException e) {
                sLogger.e("Error writing to the sysfsfile ", e);
            }
        } catch (java.lang.Exception e2) {
            sLogger.e("Exception while trying to write to the sysfs ");
        }
    }

    public void startActivityBlink() {
        if (this.activityBlinkRunning.compareAndSet(false, true)) {
            com.navdy.hud.app.device.light.LED.Settings settings = new com.navdy.hud.app.device.light.LED.Settings.Builder().setName("ActivityBlink").setActivityBlink(true).build();
            pushSetting(settings);
            removeSetting(settings);
        }
    }
}
