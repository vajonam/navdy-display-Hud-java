package com.navdy.hud.app.device.light;

public class LightSettings {
    protected int color;
    protected boolean isBlinking;
    protected boolean isTurnedOn;

    public LightSettings(int color2, boolean isTurnedOn2, boolean isBlinking2) {
        this.color = color2;
        this.isTurnedOn = isTurnedOn2;
        this.isBlinking = isBlinking2;
    }

    public int getColor() {
        return this.color;
    }

    public boolean isTurnedOn() {
        return this.isTurnedOn;
    }

    public boolean isBlinking() {
        return this.isBlinking;
    }
}
