package com.navdy.hud.app.device.light;

public interface ILight {
    void popSetting();

    void pushSetting(com.navdy.hud.app.device.light.LightSettings lightSettings);

    void removeSetting(com.navdy.hud.app.device.light.LightSettings lightSettings);

    void reset();

    void turnOff();

    void turnOn();
}
