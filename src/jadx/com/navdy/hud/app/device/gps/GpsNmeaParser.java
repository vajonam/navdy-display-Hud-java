package com.navdy.hud.app.device.gps;

public class GpsNmeaParser {
    private static final java.lang.String COLON = ":";
    private static final java.lang.String COMMA = ",";
    private static final char COMMA_CHAR = ',';
    private static final java.lang.String DB = "db";
    private static final java.lang.String EQUAL = "=";
    public static final int FIX_TYPE_2D_3D = 1;
    public static final int FIX_TYPE_DIFFERENTIAL = 2;
    public static final int FIX_TYPE_DR = 6;
    public static final int FIX_TYPE_FRTK = 5;
    public static final int FIX_TYPE_NONE = 0;
    public static final int FIX_TYPE_PPS = 3;
    public static final int FIX_TYPE_RTK = 4;
    private static final java.lang.String GGA = "GGA";
    private static final java.lang.String GLL = "GLL";
    private static final java.lang.String GNS = "GNS";
    private static final java.lang.String GSV = "GSV";
    private static final java.lang.String RMC = "RMC";
    private static final int SATELLITE_REPORT_INTERVAL = 5000;
    private static final java.lang.String SPACE = " ";
    private static final java.lang.String SVID = "SV-";
    private static final java.lang.String VTG = "VTG";
    private int fixType;
    private android.os.Handler handler;
    private long lastSatelliteReportTime;
    private int messageCount;
    private java.util.ArrayList<java.lang.String> messages = new java.util.ArrayList<>();
    private java.lang.String[] nmeaResult = new java.lang.String[200];
    private com.navdy.service.library.log.Logger sLogger;
    private java.util.HashMap<java.lang.String, java.util.HashMap<java.lang.Integer, java.lang.Integer>> satellitesSeen = new java.util.HashMap<>();
    private java.util.HashMap<java.lang.String, java.lang.Integer> satellitesUsed = new java.util.HashMap<>();

    enum NmeaMessage {
        NOT_SUPPORTED,
        GGA,
        GLL,
        GNS,
        RMC,
        VTG,
        GSV
    }

    GpsNmeaParser(com.navdy.service.library.log.Logger logger, android.os.Handler handler2) {
        this.sLogger = logger;
        this.handler = handler2;
    }

    public void parseNmeaMessage(java.lang.String nmea) {
        if (nmea != null) {
            char[] nmeaChars = nmea.toCharArray();
            if (nmeaChars.length >= 7 && nmeaChars[0] == '$') {
                com.navdy.hud.app.device.gps.GpsNmeaParser.NmeaMessage nmeaType = getMessageType(nmeaChars[3], nmeaChars[4], nmeaChars[5]);
                if (nmeaType != com.navdy.hud.app.device.gps.GpsNmeaParser.NmeaMessage.NOT_SUPPORTED) {
                    for (int i = 0; i < this.nmeaResult.length; i++) {
                        this.nmeaResult[i] = null;
                    }
                    switch (nmeaType) {
                        case GGA:
                            processGGA(nmea);
                            return;
                        case GNS:
                            processGNS(nmea);
                            return;
                        case GSV:
                            processGSV(nmea);
                            return;
                        default:
                            return;
                    }
                }
            }
        }
    }

    static com.navdy.hud.app.device.gps.GpsNmeaParser.NmeaMessage getMessageType(char ch1, char ch2, char ch3) {
        switch (ch1) {
            case com.navdy.hud.app.bluetooth.obex.HeaderSet.HTTP /*71*/:
                if (ch2 == 'S' && ch3 == 'V') {
                    return com.navdy.hud.app.device.gps.GpsNmeaParser.NmeaMessage.GSV;
                }
                if (ch2 == 'G' && ch3 == 'A') {
                    return com.navdy.hud.app.device.gps.GpsNmeaParser.NmeaMessage.GGA;
                }
                if (ch2 == 'L' && ch3 == 'L') {
                    return com.navdy.hud.app.device.gps.GpsNmeaParser.NmeaMessage.GLL;
                }
                if (ch2 == 'N' && ch3 == 'S') {
                    return com.navdy.hud.app.device.gps.GpsNmeaParser.NmeaMessage.GNS;
                }
                return com.navdy.hud.app.device.gps.GpsNmeaParser.NmeaMessage.NOT_SUPPORTED;
            case 'R':
                if (ch2 == 'M' && ch3 == 'C') {
                    return com.navdy.hud.app.device.gps.GpsNmeaParser.NmeaMessage.RMC;
                }
                return com.navdy.hud.app.device.gps.GpsNmeaParser.NmeaMessage.NOT_SUPPORTED;
            case 'V':
                if (ch2 == 'T' && ch3 == 'G') {
                    return com.navdy.hud.app.device.gps.GpsNmeaParser.NmeaMessage.VTG;
                }
                return com.navdy.hud.app.device.gps.GpsNmeaParser.NmeaMessage.NOT_SUPPORTED;
            default:
                return com.navdy.hud.app.device.gps.GpsNmeaParser.NmeaMessage.NOT_SUPPORTED;
        }
    }

    private void processGGA(java.lang.String nmea) {
        parseData(nmea, this.nmeaResult);
        try {
            this.fixType = java.lang.Integer.parseInt(this.nmeaResult[6]);
        } catch (Throwable th) {
            this.fixType = 0;
        }
    }

    private void processGLL(java.lang.String nmea) {
    }

    private void processGNS(java.lang.String nmea) {
        parseData(nmea, this.nmeaResult);
        java.lang.String gnsType = this.nmeaResult[0].substring(1, 3);
        java.lang.String lat = this.nmeaResult[2];
        java.lang.String lng = this.nmeaResult[4];
        java.lang.String satellites = this.nmeaResult[7];
        if (android.text.TextUtils.isEmpty(lat) || android.text.TextUtils.isEmpty(lng) || android.text.TextUtils.isEmpty(satellites)) {
            this.satellitesUsed.put(gnsType, java.lang.Integer.valueOf(0));
        } else {
            this.satellitesUsed.put(gnsType, java.lang.Integer.valueOf(java.lang.Integer.parseInt(satellites)));
        }
    }

    private void processRMC(java.lang.String nmea) {
    }

    private void processVTG(java.lang.String nmea) {
    }

    private void processGSV(java.lang.String nmea) {
        if (parseData(nmea, this.nmeaResult) >= 8) {
            java.lang.String gnsType = this.nmeaResult[0].substring(1, 3);
            int messageLen = java.lang.Integer.parseInt(this.nmeaResult[1]);
            int messageId = java.lang.Integer.parseInt(this.nmeaResult[2]);
            if (messageId == 1) {
                this.messages.clear();
                this.messageCount = messageLen;
            } else if (this.messages.size() == 0) {
                return;
            }
            this.messages.add(nmea);
            if (messageId == this.messageCount) {
                java.util.HashMap<java.lang.Integer, java.lang.Integer> data = (java.util.HashMap) this.satellitesSeen.get(gnsType);
                if (data == null) {
                    data = new java.util.HashMap<>();
                    this.satellitesSeen.put(gnsType, data);
                } else {
                    data.clear();
                }
                int len = this.messages.size();
                for (int i = 0; i < len; i++) {
                    int elements = parseData((java.lang.String) this.messages.get(i), this.nmeaResult);
                    for (int j = 4; j + 4 < elements; j += 4) {
                        java.lang.String satelliteId = this.nmeaResult[j];
                        java.lang.String elevation = this.nmeaResult[j + 1];
                        java.lang.String azmuth = this.nmeaResult[j + 2];
                        java.lang.String cNO = this.nmeaResult[j + 3];
                        if (!android.text.TextUtils.isEmpty(satelliteId)) {
                            if (android.text.TextUtils.isEmpty(elevation) || android.text.TextUtils.isEmpty(azmuth) || android.text.TextUtils.isEmpty(cNO)) {
                                data.remove(satelliteId);
                            } else {
                                try {
                                    int nSatelliteId = java.lang.Integer.parseInt(satelliteId);
                                    try {
                                        int cNOVal = java.lang.Integer.parseInt(cNO);
                                        data.put(java.lang.Integer.valueOf(nSatelliteId), java.lang.Integer.valueOf(cNOVal));
                                    } catch (java.lang.NumberFormatException e) {
                                    }
                                } catch (java.lang.NumberFormatException e2) {
                                }
                            }
                        }
                    }
                }
                this.messageCount = 0;
                this.messages.clear();
                long now = android.os.SystemClock.elapsedRealtime();
                if (now - this.lastSatelliteReportTime >= 5000) {
                    sendGpsSatelliteStatus();
                    this.lastSatelliteReportTime = now;
                }
            }
        }
    }

    static int parseData(java.lang.String nmea, java.lang.String[] result) {
        int resultIndex = 0;
        int start = 0;
        char[] ch = nmea.toCharArray();
        int i = 0;
        while (i < ch.length) {
            if (ch[i] == ',') {
                int resultIndex2 = resultIndex + 1;
                result[resultIndex] = new java.lang.String(ch, start, i - start);
                start = i + 1;
                resultIndex = resultIndex2;
            }
            i++;
        }
        int resultIndex3 = resultIndex + 1;
        result[resultIndex] = new java.lang.String(ch, start, (i - start) - 2);
        return resultIndex3;
    }

    private void sendGpsSatelliteStatus() {
        android.os.Bundle bundle = new android.os.Bundle();
        int counter = 0;
        int maxDB = 0;
        for (java.util.Map.Entry<java.lang.String, java.util.HashMap<java.lang.Integer, java.lang.Integer>> data : this.satellitesSeen.entrySet()) {
            java.lang.String key = (java.lang.String) data.getKey();
            for (java.util.Map.Entry<java.lang.Integer, java.lang.Integer> sData : ((java.util.HashMap) data.getValue()).entrySet()) {
                int id = ((java.lang.Integer) sData.getKey()).intValue();
                int cno = ((java.lang.Integer) sData.getValue()).intValue();
                counter++;
                bundle.putString(com.navdy.hud.app.device.gps.GpsConstants.GPS_SATELLITE_PROVIDER + counter, key);
                bundle.putInt(com.navdy.hud.app.device.gps.GpsConstants.GPS_SATELLITE_ID + counter, id);
                bundle.putInt(com.navdy.hud.app.device.gps.GpsConstants.GPS_SATELLITE_DB + counter, cno);
                if (cno > maxDB) {
                    maxDB = cno;
                }
            }
        }
        bundle.putInt(com.navdy.hud.app.device.gps.GpsConstants.GPS_SATELLITE_SEEN, counter);
        bundle.putInt(com.navdy.hud.app.device.gps.GpsConstants.GPS_SATELLITE_MAX_DB, maxDB);
        int nSatellitesUsed = 0;
        for (java.util.Map.Entry<java.lang.String, java.lang.Integer> data2 : this.satellitesUsed.entrySet()) {
            nSatellitesUsed += ((java.lang.Integer) data2.getValue()).intValue();
        }
        bundle.putInt(com.navdy.hud.app.device.gps.GpsConstants.GPS_SATELLITE_USED, nSatellitesUsed);
        bundle.putInt(com.navdy.hud.app.device.gps.GpsConstants.GPS_FIX_TYPE, this.fixType);
        android.os.Message msg = this.handler.obtainMessage(2);
        msg.obj = bundle;
        this.handler.sendMessage(msg);
    }

    private java.lang.String getGnssProviderName(java.lang.String id) {
        char c = 65535;
        switch (id.hashCode()) {
            case 2266:
                if (id.equals("GA")) {
                    c = 2;
                    break;
                }
                break;
            case 2267:
                if (id.equals("GB")) {
                    c = 3;
                    break;
                }
                break;
            case 2277:
                if (id.equals("GL")) {
                    c = 1;
                    break;
                }
                break;
            case 2279:
                if (id.equals("GN")) {
                    c = 4;
                    break;
                }
                break;
            case 2281:
                if (id.equals("GP")) {
                    c = 0;
                    break;
                }
                break;
        }
        switch (c) {
            case 0:
                return "GPS";
            case 1:
                return "GLONASS";
            case 2:
                return "Galileo";
            case 3:
                return "BeiDou";
            case 4:
                return "MultiGNSS";
            default:
                return "UNK";
        }
    }
}
