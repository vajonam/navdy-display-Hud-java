package com.navdy.hud.app.device.gps;

public class GpsManager {
    private static final int ACCEPTABLE_THRESHOLD = 2;
    private static final java.lang.String DISCARD_TIME = "DISCARD_TIME";
    private static final java.lang.String GPS_SYSTEM_PROPERTY = "persist.sys.hud_gps";
    public static final long INACCURATE_GPS_REPORT_INTERVAL = java.util.concurrent.TimeUnit.SECONDS.toMillis(30);
    private static final int INITIAL_INTERVAL = 15000;
    private static final int KEEP_PHONE_GPS_ON = -1;
    private static final int LOCATION_ACCURACY_THROW_AWAY_THRESHOLD = 30;
    private static final int LOCATION_TIME_THROW_AWAY_THRESHOLD = 2000;
    public static final double MAXIMUM_STOPPED_SPEED = 0.22353333333333333d;
    private static final double METERS_PER_MILE = 1609.44d;
    public static final long MINIMUM_DRIVE_TIME = 3000;
    public static final double MINIMUM_DRIVING_SPEED = 2.2353333333333336d;
    static final int MSG_NMEA = 1;
    static final int MSG_SATELLITE_STATUS = 2;
    private static final int SECONDS_PER_HOUR = 3600;
    private static final java.lang.String SET_LOCATION_DISCARD_TIME_THRESHOLD = "SET_LOCATION_DISCARD_TIME_THRESHOLD";
    private static final java.lang.String SET_UBLOX_ACCURACY = "SET_UBLOX_ACCURACY";
    private static final com.navdy.service.library.events.location.TransmitLocation START_SENDING_LOCATION = new com.navdy.service.library.events.location.TransmitLocation(java.lang.Boolean.valueOf(true));
    private static final java.lang.String START_UBLOX = "START_REPORTING_UBLOX_LOCATION";
    private static final com.navdy.service.library.events.location.TransmitLocation STOP_SENDING_LOCATION = new com.navdy.service.library.events.location.TransmitLocation(java.lang.Boolean.valueOf(false));
    private static final java.lang.String STOP_UBLOX = "STOP_REPORTING_UBLOX_LOCATION";
    private static final java.lang.String TAG_GPS = "[Gps-i] ";
    private static final java.lang.String TAG_GPS_LOC = "[Gps-loc] ";
    private static final java.lang.String TAG_GPS_LOC_NAVDY = "[Gps-loc-navdy] ";
    private static final java.lang.String TAG_GPS_NMEA = "[Gps-nmea] ";
    private static final java.lang.String TAG_GPS_STATUS = "[Gps-stat] ";
    private static final java.lang.String TAG_PHONE_LOC = "[Phone-loc] ";
    private static final java.lang.String UBLOX_ACCURACY = "UBLOX_ACCURACY";
    private static final float UBLOX_MIN_ACCEPTABLE_THRESHOLD = 5.0f;
    /* access modifiers changed from: private */
    public static final long WARM_RESET_INTERVAL = java.util.concurrent.TimeUnit.SECONDS.toMillis(160);
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.device.gps.GpsManager.class);
    private static final com.navdy.hud.app.device.gps.GpsManager singleton = new com.navdy.hud.app.device.gps.GpsManager();
    /* access modifiers changed from: private */
    public com.navdy.hud.app.device.gps.GpsManager.LocationSource activeLocationSource;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.service.HudConnectionService connectionService;
    private boolean driving;
    private android.content.BroadcastReceiver eventReceiver = new com.navdy.hud.app.device.gps.GpsManager.Anon11();
    /* access modifiers changed from: private */
    public volatile boolean extrapolationOn;
    /* access modifiers changed from: private */
    public volatile long extrapolationStartTime;
    /* access modifiers changed from: private */
    public boolean firstSwitchToUbloxFromPhone;
    private float gpsAccuracy;
    /* access modifiers changed from: private */
    public long gpsLastEventTime;
    /* access modifiers changed from: private */
    public long gpsManagerStartTime;
    /* access modifiers changed from: private */
    public android.os.Handler handler = new android.os.Handler(android.os.Looper.getMainLooper());
    private long inaccurateGpsCount;
    private boolean isDebugTTSEnabled;
    private boolean keepPhoneGpsOn;
    private long lastInaccurateGpsTime;
    /* access modifiers changed from: private */
    public com.navdy.service.library.events.location.Coordinate lastPhoneCoordinate;
    /* access modifiers changed from: private */
    public long lastPhoneCoordinateTime;
    /* access modifiers changed from: private */
    public android.location.Location lastUbloxCoordinate;
    /* access modifiers changed from: private */
    public long lastUbloxCoordinateTime;
    /* access modifiers changed from: private */
    public java.lang.Runnable locationFixRunnable = new com.navdy.hud.app.device.gps.GpsManager.Anon8();
    private android.location.LocationManager locationManager;
    private android.location.LocationListener navdyGpsLocationListener = new com.navdy.hud.app.device.gps.GpsManager.Anon3();
    private android.os.Handler.Callback nmeaCallback = new com.navdy.hud.app.device.gps.GpsManager.Anon9();
    private android.os.Handler nmeaHandler;
    private android.os.HandlerThread nmeaHandlerThread;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.device.gps.GpsNmeaParser nmeaParser;
    private java.lang.Runnable noLocationUpdatesRunnable = new com.navdy.hud.app.device.gps.GpsManager.Anon6();
    /* access modifiers changed from: private */
    public java.lang.Runnable noPhoneLocationFixRunnable = new com.navdy.hud.app.device.gps.GpsManager.Anon5();
    /* access modifiers changed from: private */
    public java.util.ArrayList<android.content.Intent> queue = new java.util.ArrayList<>();
    private java.lang.Runnable queueCallback = new com.navdy.hud.app.device.gps.GpsManager.Anon10();
    /* access modifiers changed from: private */
    public volatile boolean startUbloxCalled;
    /* access modifiers changed from: private */
    public boolean switchBetweenPhoneUbloxAllowed;
    private long transitionStartTime;
    private boolean transitioning;
    private android.location.LocationListener ubloxGpsLocationListener = new com.navdy.hud.app.device.gps.GpsManager.Anon2();
    private android.location.GpsStatus.NmeaListener ubloxGpsNmeaListener = new com.navdy.hud.app.device.gps.GpsManager.Anon1();
    private android.location.GpsStatus.Listener ubloxGpsStatusListener = new com.navdy.hud.app.device.gps.GpsManager.Anon4();
    /* access modifiers changed from: private */
    public boolean warmResetCancelled;
    private java.lang.Runnable warmResetRunnable = new com.navdy.hud.app.device.gps.GpsManager.Anon7();

    class Anon1 implements android.location.GpsStatus.NmeaListener {
        Anon1() {
        }

        public void onNmeaReceived(long timestamp, java.lang.String nmea) {
            try {
                if (com.navdy.hud.app.device.gps.GpsManager.sLogger.isLoggable(2)) {
                    com.navdy.hud.app.device.gps.GpsManager.sLogger.v(com.navdy.hud.app.device.gps.GpsManager.TAG_GPS_NMEA + nmea);
                }
                com.navdy.hud.app.device.gps.GpsManager.this.processNmea(nmea);
            } catch (Throwable t) {
                com.navdy.hud.app.device.gps.GpsManager.sLogger.e(t);
            }
        }
    }

    class Anon10 implements java.lang.Runnable {
        Anon10() {
        }

        public void run() {
            try {
                android.content.Context context = com.navdy.hud.app.HudApplication.getAppContext();
                android.os.UserHandle handle = android.os.Process.myUserHandle();
                int len = com.navdy.hud.app.device.gps.GpsManager.this.queue.size();
                if (len > 0) {
                    for (int i = 0; i < len; i++) {
                        android.content.Intent intent = (android.content.Intent) com.navdy.hud.app.device.gps.GpsManager.this.queue.get(i);
                        context.sendBroadcastAsUser(intent, handle);
                        com.navdy.hud.app.device.gps.GpsManager.sLogger.v("sent-queue gps event user:" + intent.getAction());
                    }
                    com.navdy.hud.app.device.gps.GpsManager.this.queue.clear();
                }
            } catch (Throwable t) {
                com.navdy.hud.app.device.gps.GpsManager.sLogger.e(t);
            }
        }
    }

    class Anon11 extends android.content.BroadcastReceiver {
        Anon11() {
        }

        public void onReceive(android.content.Context context, android.content.Intent intent) {
            char c = 0;
            try {
                java.lang.String action = intent.getAction();
                if (action != null) {
                    com.navdy.hud.app.debug.RouteRecorder routeRecorder = com.navdy.hud.app.debug.RouteRecorder.getInstance();
                    switch (action.hashCode()) {
                        case -1801456507:
                            if (action.equals(com.navdy.hud.app.device.gps.GpsConstants.GPS_EVENT_DEAD_RECKONING_STARTED)) {
                                break;
                            }
                        case -1788590639:
                            if (action.equals(com.navdy.hud.app.device.gps.GpsConstants.GPS_EVENT_DEAD_RECKONING_STOPPED)) {
                                c = 1;
                                break;
                            }
                        case -47674184:
                            if (action.equals(com.navdy.hud.app.device.gps.GpsConstants.EXTRAPOLATION)) {
                                c = 2;
                                break;
                            }
                    }
                    c = 65535;
                    switch (c) {
                        case 0:
                            if (routeRecorder.isRecording()) {
                                java.lang.String marker = intent.getStringExtra(com.navdy.hud.app.device.gps.GpsConstants.GPS_EXTRA_DR_TYPE);
                                if (marker == null) {
                                    marker = "";
                                }
                                routeRecorder.injectMarker("GPS_DR_STARTED " + marker);
                                return;
                            }
                            return;
                        case 1:
                            if (routeRecorder.isRecording()) {
                                routeRecorder.injectMarker(com.navdy.hud.app.device.gps.GpsConstants.GPS_EVENT_DEAD_RECKONING_STOPPED);
                                return;
                            }
                            return;
                        case 2:
                            com.navdy.hud.app.device.gps.GpsManager.this.extrapolationOn = intent.getBooleanExtra(com.navdy.hud.app.device.gps.GpsConstants.EXTRAPOLATION_FLAG, false);
                            if (com.navdy.hud.app.device.gps.GpsManager.this.extrapolationOn) {
                                com.navdy.hud.app.device.gps.GpsManager.this.extrapolationStartTime = android.os.SystemClock.elapsedRealtime();
                            } else {
                                com.navdy.hud.app.device.gps.GpsManager.this.extrapolationStartTime = 0;
                            }
                            com.navdy.hud.app.device.gps.GpsManager.sLogger.v("extrapolation is:" + com.navdy.hud.app.device.gps.GpsManager.this.extrapolationOn + " time:" + com.navdy.hud.app.device.gps.GpsManager.this.extrapolationStartTime);
                            return;
                        default:
                            return;
                    }
                }
            } catch (Throwable t) {
                com.navdy.hud.app.device.gps.GpsManager.sLogger.e(t);
            }
        }
    }

    class Anon2 implements android.location.LocationListener {
        Anon2() {
        }

        public void onLocationChanged(android.location.Location location) {
            float f;
            java.lang.Object valueOf;
            try {
                if (com.navdy.hud.app.device.gps.GpsManager.sLogger.isLoggable(2)) {
                    com.navdy.hud.app.device.gps.GpsManager.sLogger.d(com.navdy.hud.app.device.gps.GpsManager.TAG_GPS_LOC + (location.isFromMockProvider() ? "[Mock] " : "") + location);
                }
                if (com.navdy.hud.app.device.gps.GpsManager.this.startUbloxCalled && !location.isFromMockProvider() && com.navdy.hud.app.device.gps.GpsManager.this.activeLocationSource != com.navdy.hud.app.device.gps.GpsManager.LocationSource.UBLOX) {
                    com.navdy.hud.app.device.gps.GpsManager.LocationSource last = com.navdy.hud.app.device.gps.GpsManager.this.activeLocationSource;
                    com.navdy.hud.app.device.gps.GpsManager.this.activeLocationSource = com.navdy.hud.app.device.gps.GpsManager.LocationSource.UBLOX;
                    com.navdy.hud.app.device.gps.GpsManager.this.handler.removeCallbacks(com.navdy.hud.app.device.gps.GpsManager.this.noPhoneLocationFixRunnable);
                    com.navdy.hud.app.device.gps.GpsManager.this.handler.removeCallbacks(com.navdy.hud.app.device.gps.GpsManager.this.locationFixRunnable);
                    com.navdy.hud.app.device.gps.GpsManager.this.handler.postDelayed(com.navdy.hud.app.device.gps.GpsManager.this.locationFixRunnable, 1000);
                    com.navdy.hud.app.device.gps.GpsManager.this.stopSendingLocation();
                    int second = ((int) (android.os.SystemClock.elapsedRealtime() - com.navdy.hud.app.device.gps.GpsManager.this.gpsLastEventTime)) / 1000;
                    float phoneAccuracy = -1.0f;
                    long elapsedTime = android.os.SystemClock.elapsedRealtime() - com.navdy.hud.app.device.gps.GpsManager.this.lastPhoneCoordinateTime;
                    if (com.navdy.hud.app.device.gps.GpsManager.this.lastPhoneCoordinate != null && elapsedTime < com.navdy.hud.app.device.gps.GpsManager.MINIMUM_DRIVE_TIME) {
                        phoneAccuracy = com.navdy.hud.app.device.gps.GpsManager.this.lastPhoneCoordinate.accuracy.floatValue();
                    }
                    if (com.navdy.hud.app.device.gps.GpsManager.this.lastUbloxCoordinate != null) {
                        f = com.navdy.hud.app.device.gps.GpsManager.this.lastUbloxCoordinate.getAccuracy();
                    } else {
                        f = -1.0f;
                    }
                    int accuracy = (int) f;
                    com.navdy.hud.app.service.ConnectionServiceAnalyticsSupport.recordGpsAcquireLocation(java.lang.String.valueOf(second), java.lang.String.valueOf(accuracy));
                    com.navdy.hud.app.device.gps.GpsManager.this.gpsLastEventTime = android.os.SystemClock.elapsedRealtime();
                    com.navdy.hud.app.device.gps.GpsManager.this.markLocationFix(com.navdy.hud.app.device.gps.GpsConstants.DEBUG_TTS_UBLOX_SWITCH, "Phone =" + (phoneAccuracy == -1.0f ? "n/a" : java.lang.Float.valueOf(phoneAccuracy)) + " ublox =" + accuracy, false, true);
                    com.navdy.service.library.log.Logger access$Anon000 = com.navdy.hud.app.device.gps.GpsManager.sLogger;
                    java.lang.StringBuilder append = new java.lang.StringBuilder().append("[Gps-loc] [LocationProvider] switched from [").append(last).append("] to [").append(com.navdy.hud.app.device.gps.GpsManager.LocationSource.UBLOX).append("] ").append("Phone =");
                    if (phoneAccuracy == -1.0f) {
                        valueOf = "n/a";
                    } else {
                        valueOf = java.lang.Float.valueOf(phoneAccuracy);
                    }
                    access$Anon000.v(append.append(valueOf).append(" ublox =").append(accuracy).append(" time=").append(elapsedTime).toString());
                }
            } catch (Throwable t) {
                com.navdy.hud.app.device.gps.GpsManager.sLogger.e(t);
            }
        }

        public void onStatusChanged(java.lang.String provider, int status, android.os.Bundle extras) {
            if (com.navdy.hud.app.device.PowerManager.isAwake()) {
                com.navdy.hud.app.device.gps.GpsManager.sLogger.i(com.navdy.hud.app.device.gps.GpsManager.TAG_GPS_STATUS + provider + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA + status);
            }
        }

        public void onProviderEnabled(java.lang.String provider) {
            com.navdy.hud.app.device.gps.GpsManager.sLogger.i(com.navdy.hud.app.device.gps.GpsManager.TAG_GPS_STATUS + provider + "[enabled]");
        }

        public void onProviderDisabled(java.lang.String provider) {
            com.navdy.hud.app.device.gps.GpsManager.sLogger.i(com.navdy.hud.app.device.gps.GpsManager.TAG_GPS_STATUS + provider + "[disabled]");
        }
    }

    class Anon3 implements android.location.LocationListener {
        private float accuracyMax = 0.0f;
        private float accuracyMin = 0.0f;
        private int accuracySampleCount = 0;
        private long accuracySampleStartTime = android.os.SystemClock.elapsedRealtime();
        private float accuracySum = 0.0f;

        Anon3() {
        }

        private void collectAccuracyStats(float accuracy) {
            if (accuracy != 0.0f) {
                this.accuracySum += accuracy;
                this.accuracySampleCount++;
                if (this.accuracyMin == 0.0f || accuracy < this.accuracyMin) {
                    this.accuracyMin = accuracy;
                }
                if (this.accuracyMax < accuracy) {
                    this.accuracyMax = accuracy;
                }
            }
            if (com.navdy.hud.app.device.gps.GpsManager.this.lastUbloxCoordinateTime - this.accuracySampleStartTime > 300000) {
                if (this.accuracySampleCount > 0) {
                    com.navdy.hud.app.service.ConnectionServiceAnalyticsSupport.recordGpsAccuracy(java.lang.Integer.toString(java.lang.Math.round(this.accuracyMin)), java.lang.Integer.toString(java.lang.Math.round(this.accuracyMax)), java.lang.Integer.toString(java.lang.Math.round(this.accuracySum / ((float) this.accuracySampleCount))));
                    this.accuracySum = 0.0f;
                    this.accuracyMax = 0.0f;
                    this.accuracyMin = 0.0f;
                    this.accuracySampleCount = 0;
                }
                this.accuracySampleStartTime = com.navdy.hud.app.device.gps.GpsManager.this.lastUbloxCoordinateTime;
            }
        }

        public void onLocationChanged(android.location.Location location) {
            try {
                if (!com.navdy.hud.app.device.gps.GpsManager.this.warmResetCancelled) {
                    com.navdy.hud.app.device.gps.GpsManager.this.cancelUbloxResetRunnable();
                }
                if (!com.navdy.hud.app.debug.RouteRecorder.getInstance().isPlaying()) {
                    com.navdy.hud.app.device.gps.GpsManager.this.updateDrivingState(location);
                    com.navdy.hud.app.device.gps.GpsManager.this.lastUbloxCoordinate = location;
                    com.navdy.hud.app.device.gps.GpsManager.this.lastUbloxCoordinateTime = android.os.SystemClock.elapsedRealtime();
                    collectAccuracyStats(location.getAccuracy());
                    if (com.navdy.hud.app.device.gps.GpsManager.this.connectionService != null && com.navdy.hud.app.device.gps.GpsManager.this.connectionService.isConnected()) {
                        if (com.navdy.hud.app.device.gps.GpsManager.this.activeLocationSource != com.navdy.hud.app.device.gps.GpsManager.LocationSource.UBLOX) {
                            long time = android.os.SystemClock.elapsedRealtime() - com.navdy.hud.app.device.gps.GpsManager.this.lastPhoneCoordinateTime;
                            if (time > com.navdy.hud.app.device.gps.GpsManager.MINIMUM_DRIVE_TIME) {
                                com.navdy.hud.app.device.gps.GpsManager.sLogger.v("phone threshold exceeded= " + time + ", start uBloxReporting");
                                com.navdy.hud.app.device.gps.GpsManager.this.startUbloxReporting();
                                return;
                            }
                        }
                        if (!com.navdy.hud.app.device.gps.GpsManager.this.firstSwitchToUbloxFromPhone || com.navdy.hud.app.device.gps.GpsManager.this.switchBetweenPhoneUbloxAllowed) {
                            com.navdy.hud.app.device.gps.GpsManager.this.checkGpsToPhoneAccuracy(location);
                        }
                    } else if (com.navdy.hud.app.device.gps.GpsManager.this.activeLocationSource != com.navdy.hud.app.device.gps.GpsManager.LocationSource.UBLOX) {
                        com.navdy.hud.app.device.gps.GpsManager.sLogger.v("not connected with phone, start uBloxReporting");
                        com.navdy.hud.app.device.gps.GpsManager.this.startUbloxReporting();
                    }
                }
            } catch (Throwable t) {
                com.navdy.hud.app.device.gps.GpsManager.sLogger.e(t);
            }
        }

        public void onStatusChanged(java.lang.String provider, int status, android.os.Bundle extras) {
        }

        public void onProviderEnabled(java.lang.String provider) {
        }

        public void onProviderDisabled(java.lang.String provider) {
        }
    }

    class Anon4 implements android.location.GpsStatus.Listener {
        Anon4() {
        }

        public void onGpsStatusChanged(int event) {
            switch (event) {
                case 1:
                    com.navdy.hud.app.device.gps.GpsManager.sLogger.i("[Gps-stat] gps searching...");
                    return;
                case 2:
                    com.navdy.hud.app.device.gps.GpsManager.sLogger.w("[Gps-stat] gps stopped searching");
                    return;
                case 3:
                    com.navdy.hud.app.device.gps.GpsManager.sLogger.i("[Gps-stat] gps got first fix");
                    return;
                default:
                    return;
            }
        }
    }

    class Anon5 implements java.lang.Runnable {
        Anon5() {
        }

        public void run() {
            boolean resetRunnable = true;
            try {
                if (com.navdy.hud.app.device.gps.GpsManager.this.activeLocationSource == com.navdy.hud.app.device.gps.GpsManager.LocationSource.UBLOX) {
                    com.navdy.hud.app.device.gps.GpsManager.sLogger.i("[Gps-loc] noPhoneLocationFixRunnable: has location fix now");
                    resetRunnable = false;
                } else {
                    com.navdy.hud.app.device.gps.GpsManager.this.updateDrivingState(null);
                    com.navdy.hud.app.device.gps.GpsManager.this.startSendingLocation();
                }
                if (resetRunnable) {
                    com.navdy.hud.app.device.gps.GpsManager.this.handler.postDelayed(com.navdy.hud.app.device.gps.GpsManager.this.noPhoneLocationFixRunnable, 5000);
                }
            } catch (Throwable th) {
                if (1 != 0) {
                    com.navdy.hud.app.device.gps.GpsManager.this.handler.postDelayed(com.navdy.hud.app.device.gps.GpsManager.this.noPhoneLocationFixRunnable, 5000);
                }
                throw th;
            }
        }
    }

    class Anon6 implements java.lang.Runnable {
        Anon6() {
        }

        public void run() {
            com.navdy.hud.app.device.gps.GpsManager.this.updateDrivingState(null);
        }
    }

    class Anon7 implements java.lang.Runnable {
        Anon7() {
        }

        public void run() {
            try {
                com.navdy.hud.app.device.gps.GpsManager.sLogger.v("issuing warm reset to ublox, no location report in last " + com.navdy.hud.app.device.gps.GpsManager.WARM_RESET_INTERVAL);
                com.navdy.hud.app.device.gps.GpsManager.this.sendGpsEventBroadcast(com.navdy.hud.app.device.gps.GpsConstants.GPS_EVENT_WARM_RESET_UBLOX, null);
            } catch (Throwable t) {
                com.navdy.hud.app.device.gps.GpsManager.sLogger.e(t);
            }
        }
    }

    class Anon8 implements java.lang.Runnable {
        Anon8() {
        }

        public void run() {
            boolean resetRunnable = true;
            try {
                long time = android.os.SystemClock.elapsedRealtime() - com.navdy.hud.app.device.gps.GpsManager.this.lastUbloxCoordinateTime;
                if (time >= com.navdy.hud.app.device.gps.GpsManager.MINIMUM_DRIVE_TIME) {
                    com.navdy.hud.app.device.gps.GpsManager.sLogger.i("[Gps-loc] locationFixRunnable: lost location fix[" + time + "]");
                    if (com.navdy.hud.app.device.gps.GpsManager.this.extrapolationOn) {
                        long diff = android.os.SystemClock.elapsedRealtime() - com.navdy.hud.app.device.gps.GpsManager.this.extrapolationStartTime;
                        com.navdy.hud.app.device.gps.GpsManager.sLogger.i("[Gps-loc] locationFixRunnable: extrapolation on time:" + diff);
                        if (diff < 5000) {
                            com.navdy.hud.app.device.gps.GpsManager.sLogger.i("[Gps-loc] locationFixRunnable: reset");
                            if (1 != 0) {
                                com.navdy.hud.app.device.gps.GpsManager.this.handler.postDelayed(com.navdy.hud.app.device.gps.GpsManager.this.locationFixRunnable, 1000);
                                return;
                            }
                            return;
                        }
                        com.navdy.hud.app.device.gps.GpsManager.this.extrapolationOn = false;
                        com.navdy.hud.app.device.gps.GpsManager.this.extrapolationStartTime = 0;
                        com.navdy.hud.app.device.gps.GpsManager.sLogger.i("[Gps-loc] locationFixRunnable: expired");
                    }
                    resetRunnable = false;
                    com.navdy.hud.app.device.gps.GpsManager.this.updateDrivingState(null);
                    com.navdy.hud.app.device.gps.GpsManager.sLogger.e("[Gps-loc] lost gps fix");
                    com.navdy.hud.app.device.gps.GpsManager.this.handler.removeCallbacks(com.navdy.hud.app.device.gps.GpsManager.this.locationFixRunnable);
                    com.navdy.hud.app.device.gps.GpsManager.this.handler.removeCallbacks(com.navdy.hud.app.device.gps.GpsManager.this.noPhoneLocationFixRunnable);
                    com.navdy.hud.app.device.gps.GpsManager.this.handler.postDelayed(com.navdy.hud.app.device.gps.GpsManager.this.noPhoneLocationFixRunnable, 5000);
                    com.navdy.hud.app.device.gps.GpsManager.this.startSendingLocation();
                    com.navdy.hud.app.service.ConnectionServiceAnalyticsSupport.recordGpsLostLocation(java.lang.String.valueOf((android.os.SystemClock.elapsedRealtime() - com.navdy.hud.app.device.gps.GpsManager.this.gpsLastEventTime) / 1000));
                    com.navdy.hud.app.device.gps.GpsManager.this.gpsLastEventTime = android.os.SystemClock.elapsedRealtime();
                }
                if (resetRunnable) {
                    com.navdy.hud.app.device.gps.GpsManager.this.handler.postDelayed(com.navdy.hud.app.device.gps.GpsManager.this.locationFixRunnable, 1000);
                }
            } catch (Throwable th) {
                if (1 != 0) {
                    com.navdy.hud.app.device.gps.GpsManager.this.handler.postDelayed(com.navdy.hud.app.device.gps.GpsManager.this.locationFixRunnable, 1000);
                }
                throw th;
            }
        }
    }

    class Anon9 implements android.os.Handler.Callback {
        Anon9() {
        }

        public boolean handleMessage(android.os.Message msg) {
            try {
                switch (msg.what) {
                    case 1:
                        com.navdy.hud.app.device.gps.GpsManager.this.nmeaParser.parseNmeaMessage((java.lang.String) msg.obj);
                        break;
                    case 2:
                        if (android.os.SystemClock.elapsedRealtime() - com.navdy.hud.app.device.gps.GpsManager.this.gpsManagerStartTime > com.glympse.android.lib.StaticConfig.PERMISSION_CHECK_INTERVAL) {
                            android.os.Bundle dataBundle = (android.os.Bundle) msg.obj;
                            android.os.Bundle bundle = new android.os.Bundle();
                            bundle.putBundle(com.navdy.hud.app.device.gps.GpsConstants.GPS_EVENT_SATELLITE_DATA, dataBundle);
                            com.navdy.hud.app.device.gps.GpsManager.this.sendGpsEventBroadcast(com.navdy.hud.app.device.gps.GpsConstants.GPS_SATELLITE_STATUS, bundle);
                            break;
                        }
                        break;
                }
            } catch (Throwable t) {
                com.navdy.hud.app.device.gps.GpsManager.sLogger.e(t);
            }
            return true;
        }
    }

    enum LocationSource {
        UBLOX,
        PHONE
    }

    public static com.navdy.hud.app.device.gps.GpsManager getInstance() {
        return singleton;
    }

    private GpsManager() {
        android.content.Context context = com.navdy.hud.app.HudApplication.getAppContext();
        try {
            int n = com.navdy.hud.app.util.os.SystemProperties.getInt(GPS_SYSTEM_PROPERTY, 0);
            if (n == -1) {
                this.keepPhoneGpsOn = true;
                this.switchBetweenPhoneUbloxAllowed = true;
                sLogger.v("switch between phone and ublox allowed");
            }
            sLogger.v("keepPhoneGpsOn[" + this.keepPhoneGpsOn + "] val[" + n + "]");
            this.isDebugTTSEnabled = new java.io.File("/sdcard/debug_tts").exists();
            setUbloxAccuracyThreshold();
            setUbloxTimeDiscardThreshold();
        } catch (Throwable t) {
            sLogger.e(t);
        }
        android.location.LocationProvider gpsProvider = null;
        this.locationManager = (android.location.LocationManager) context.getSystemService("location");
        if (this.locationManager.isProviderEnabled("gps")) {
            gpsProvider = this.locationManager.getProvider("gps");
            if (gpsProvider != null) {
                this.nmeaHandlerThread = new android.os.HandlerThread("navdynmea");
                this.nmeaHandlerThread.start();
                this.nmeaHandler = new android.os.Handler(this.nmeaHandlerThread.getLooper(), this.nmeaCallback);
                this.nmeaParser = new com.navdy.hud.app.device.gps.GpsNmeaParser(sLogger, this.nmeaHandler);
                this.locationManager.addGpsStatusListener(this.ubloxGpsStatusListener);
                this.locationManager.addNmeaListener(this.ubloxGpsNmeaListener);
            }
        }
        if (com.navdy.hud.app.util.DeviceUtil.isNavdyDevice()) {
            sLogger.i("[Gps-i] setting up ublox gps");
            if (gpsProvider != null) {
                this.locationManager.requestLocationUpdates("gps", 0, 0.0f, this.ubloxGpsLocationListener);
                try {
                    this.locationManager.requestLocationUpdates(com.navdy.hud.app.device.gps.GpsConstants.NAVDY_GPS_PROVIDER, 0, 0.0f, this.navdyGpsLocationListener);
                    sLogger.v("requestLocationUpdates successful for NAVDY_GPS_PROVIDER");
                } catch (Throwable t2) {
                    sLogger.e("requestLocationUpdates", t2);
                }
                sLogger.i("[Gps-i] ublox gps listeners installed");
            } else {
                sLogger.e("[Gps-i] gps provider not found");
            }
        } else {
            sLogger.i("[Gps-i] not a Hud device,not setting up ublox gps");
        }
        this.locationManager.addTestProvider("network", false, true, false, false, true, true, true, 0, 3);
        this.locationManager.setTestProviderEnabled("network", true);
        this.locationManager.setTestProviderStatus("network", 2, null, java.lang.System.currentTimeMillis());
        sLogger.i("[Gps-i] added mock network provider");
        this.gpsManagerStartTime = android.os.SystemClock.elapsedRealtime();
        this.gpsLastEventTime = this.gpsManagerStartTime;
        com.navdy.hud.app.service.ConnectionServiceAnalyticsSupport.recordGpsAttemptAcquireLocation();
        if (com.navdy.hud.app.util.DeviceUtil.isNavdyDevice()) {
            setUbloxResetRunnable();
        }
        android.content.IntentFilter filter = new android.content.IntentFilter();
        filter.addAction(com.navdy.hud.app.device.gps.GpsConstants.GPS_EVENT_DEAD_RECKONING_STARTED);
        filter.addAction(com.navdy.hud.app.device.gps.GpsConstants.GPS_EVENT_DEAD_RECKONING_STOPPED);
        filter.addAction(com.navdy.hud.app.device.gps.GpsConstants.EXTRAPOLATION);
        context.registerReceiver(this.eventReceiver, filter);
    }

    public void setConnectionService(com.navdy.hud.app.service.HudConnectionService connectionService2) {
        this.connectionService = connectionService2;
    }

    public void feedLocation(com.navdy.service.library.events.location.Coordinate coordinate) {
        boolean hasAtleastOneLocation = this.lastPhoneCoordinateTime > 0 || this.lastUbloxCoordinateTime > 0;
        long now = java.lang.System.currentTimeMillis();
        if (coordinate.accuracy.floatValue() < 30.0f || !hasAtleastOneLocation) {
            long timeDiff = now - coordinate.timestamp.longValue();
            if (timeDiff >= 2000 && hasAtleastOneLocation) {
                sLogger.e("OLD location from phone(discard) time:" + timeDiff + ", timestamp:" + coordinate.timestamp + " now:" + now + " lat:" + coordinate.latitude + " lng:" + coordinate.longitude);
            } else if (this.lastPhoneCoordinate != null && this.connectionService.getDevicePlatform() == com.navdy.service.library.events.DeviceInfo.Platform.PLATFORM_iOS && this.lastPhoneCoordinate.latitude == coordinate.latitude && this.lastPhoneCoordinate.longitude == coordinate.longitude && this.lastPhoneCoordinate.accuracy == coordinate.accuracy && this.lastPhoneCoordinate.altitude == coordinate.altitude && this.lastPhoneCoordinate.bearing == coordinate.bearing && this.lastPhoneCoordinate.speed == coordinate.speed) {
                sLogger.e("same location(discard) diff=" + (coordinate.timestamp.longValue() - this.lastPhoneCoordinate.timestamp.longValue()));
            } else {
                this.lastPhoneCoordinate = coordinate;
                this.lastPhoneCoordinateTime = android.os.SystemClock.elapsedRealtime();
                this.handler.removeCallbacks(this.noPhoneLocationFixRunnable);
                android.location.Location androidLocation = null;
                com.navdy.hud.app.debug.RouteRecorder routeRecorder = com.navdy.hud.app.debug.RouteRecorder.getInstance();
                if (routeRecorder.isRecording()) {
                    androidLocation = androidLocationFromCoordinate(coordinate);
                    routeRecorder.injectLocation(androidLocation, true);
                }
                if (this.activeLocationSource == null) {
                    sLogger.v("[Gps-loc] [LocationProvider] switched from [" + this.activeLocationSource + "] to [" + com.navdy.hud.app.device.gps.GpsManager.LocationSource.PHONE + "]");
                    stopUbloxReporting();
                    this.activeLocationSource = com.navdy.hud.app.device.gps.GpsManager.LocationSource.PHONE;
                    feedLocationToProvider(androidLocation, coordinate);
                    markLocationFix(com.navdy.hud.app.device.gps.GpsConstants.DEBUG_TTS_PHONE_SWITCH, "Phone =" + this.lastPhoneCoordinate.accuracy + " ublox=n/a", true, false);
                } else if (this.activeLocationSource == com.navdy.hud.app.device.gps.GpsManager.LocationSource.PHONE) {
                    feedLocationToProvider(androidLocation, coordinate);
                } else if (android.os.SystemClock.elapsedRealtime() - this.lastUbloxCoordinateTime > MINIMUM_DRIVE_TIME) {
                    sLogger.v("[Gps-loc] [LocationProvider] switched from [" + this.activeLocationSource + "] to [" + com.navdy.hud.app.device.gps.GpsManager.LocationSource.PHONE + "]");
                    this.activeLocationSource = com.navdy.hud.app.device.gps.GpsManager.LocationSource.PHONE;
                    stopUbloxReporting();
                    feedLocationToProvider(androidLocation, coordinate);
                    markLocationFix(com.navdy.hud.app.device.gps.GpsConstants.DEBUG_TTS_PHONE_SWITCH, "Phone =" + this.lastPhoneCoordinate.accuracy + " ublox =lost", true, false);
                }
            }
        } else {
            this.inaccurateGpsCount++;
            this.gpsAccuracy += coordinate.accuracy.floatValue();
            if (this.lastInaccurateGpsTime == 0 || now - this.lastInaccurateGpsTime >= INACCURATE_GPS_REPORT_INTERVAL) {
                sLogger.e("BAD gps accuracy (discarding) avg:" + (this.gpsAccuracy / ((float) this.inaccurateGpsCount)) + ", count=" + this.inaccurateGpsCount + ", last coord: " + coordinate);
                this.inaccurateGpsCount = 0;
                this.gpsAccuracy = 0.0f;
                this.lastInaccurateGpsTime = now;
            }
        }
    }

    private void feedLocationToProvider(android.location.Location androidLocation, com.navdy.service.library.events.location.Coordinate coordinate) {
        try {
            if (sLogger.isLoggable(3)) {
                sLogger.d(TAG_PHONE_LOC + coordinate);
            }
            if (androidLocation == null) {
                androidLocation = androidLocationFromCoordinate(coordinate);
            }
            updateDrivingState(androidLocation);
            this.locationManager.setTestProviderLocation(androidLocation.getProvider(), androidLocation);
        } catch (Throwable t) {
            sLogger.e("feedLocation", t);
        }
    }

    private static android.location.Location androidLocationFromCoordinate(com.navdy.service.library.events.location.Coordinate c) {
        android.location.Location location = new android.location.Location("network");
        location.setLatitude(c.latitude.doubleValue());
        location.setLongitude(c.longitude.doubleValue());
        location.setAccuracy(c.accuracy.floatValue());
        location.setAltitude(c.altitude.doubleValue());
        location.setBearing(c.bearing.floatValue());
        location.setSpeed(c.speed.floatValue());
        location.setTime(java.lang.System.currentTimeMillis());
        location.setElapsedRealtimeNanos(android.os.SystemClock.elapsedRealtimeNanos());
        return location;
    }

    /* access modifiers changed from: private */
    public void startSendingLocation() {
        if (this.connectionService != null && this.connectionService.isConnected()) {
            try {
                sLogger.i("[Gps-loc] phone start sending location");
                this.connectionService.sendMessage(START_SENDING_LOCATION);
            } catch (Throwable t) {
                sLogger.e(t);
            }
        }
    }

    private void setDriving(boolean driving2) {
        if (this.driving != driving2) {
            this.driving = driving2;
            sendGpsEventBroadcast(driving2 ? com.navdy.hud.app.device.gps.GpsConstants.GPS_EVENT_DRIVING_STARTED : com.navdy.hud.app.device.gps.GpsConstants.GPS_EVENT_DRIVING_STOPPED, null);
        }
    }

    /* access modifiers changed from: private */
    public void updateDrivingState(android.location.Location location) {
        if (this.driving) {
            if (location != null && ((double) location.getSpeed()) >= 0.22353333333333333d) {
                this.transitioning = false;
            } else if (!this.transitioning) {
                this.transitioning = true;
                this.transitionStartTime = android.os.SystemClock.elapsedRealtime();
            } else if (android.os.SystemClock.elapsedRealtime() - this.transitionStartTime > MINIMUM_DRIVE_TIME) {
                setDriving(false);
                this.transitioning = false;
            }
        } else if (location == null || ((double) location.getSpeed()) <= 2.2353333333333336d) {
            this.transitioning = false;
        } else if (!this.transitioning) {
            this.transitioning = true;
            this.transitionStartTime = android.os.SystemClock.elapsedRealtime();
        } else if (android.os.SystemClock.elapsedRealtime() - this.transitionStartTime > MINIMUM_DRIVE_TIME) {
            setDriving(true);
            this.transitioning = false;
        }
        this.handler.removeCallbacks(this.noLocationUpdatesRunnable);
        if (this.driving) {
            this.handler.postDelayed(this.noLocationUpdatesRunnable, MINIMUM_DRIVE_TIME);
        }
    }

    /* access modifiers changed from: private */
    public void stopSendingLocation() {
        if (this.keepPhoneGpsOn) {
            sLogger.v("stopSendingLocation: keeping phone gps on");
        } else if (this.isDebugTTSEnabled) {
            sLogger.v("stopSendingLocation: not stopping, debug_tts enabled");
        } else if (this.connectionService != null && this.connectionService.isConnected()) {
            try {
                sLogger.i("[Gps-loc] phone stop sending location");
                this.connectionService.sendMessage(STOP_SENDING_LOCATION);
            } catch (Throwable t) {
                sLogger.e(t);
            }
        }
    }

    private void sendSpeechRequest(com.navdy.service.library.events.audio.SpeechRequest speechRequest) {
        if (this.connectionService != null && this.connectionService.isConnected()) {
            try {
                sLogger.i("[Gps-loc] send speech request");
                this.connectionService.sendMessage(speechRequest);
            } catch (Throwable t) {
                sLogger.e(t);
            }
        }
    }

    /* access modifiers changed from: private */
    public void processNmea(java.lang.String nmea) {
        android.os.Message msg = this.nmeaHandler.obtainMessage(1);
        msg.obj = nmea;
        this.nmeaHandler.sendMessage(msg);
    }

    /* access modifiers changed from: private */
    public void sendGpsEventBroadcast(java.lang.String eventName, android.os.Bundle extras) {
        try {
            android.content.Intent intent = new android.content.Intent(eventName);
            if (extras != null) {
                intent.putExtras(extras);
            }
            if (android.os.SystemClock.elapsedRealtime() - this.gpsManagerStartTime <= com.glympse.android.lib.StaticConfig.PERMISSION_CHECK_INTERVAL) {
                this.queue.add(intent);
                this.handler.removeCallbacks(this.queueCallback);
                this.handler.postDelayed(this.queueCallback, com.glympse.android.lib.StaticConfig.PERMISSION_CHECK_INTERVAL);
                return;
            }
            android.content.Context context = com.navdy.hud.app.HudApplication.getAppContext();
            android.os.UserHandle handle = android.os.Process.myUserHandle();
            if (this.queue.size() > 0) {
                this.queueCallback.run();
            }
            context.sendBroadcastAsUser(intent, handle);
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }

    public void startUbloxReporting() {
        try {
            com.navdy.hud.app.HudApplication.getAppContext().sendBroadcastAsUser(new android.content.Intent(START_UBLOX), android.os.Process.myUserHandle());
            this.startUbloxCalled = true;
            sLogger.v("started ublox reporting");
        } catch (Throwable t) {
            sLogger.e("startUbloxReporting", t);
        }
    }

    public void stopUbloxReporting() {
        try {
            this.startUbloxCalled = false;
            com.navdy.hud.app.HudApplication.getAppContext().sendBroadcastAsUser(new android.content.Intent(STOP_UBLOX), android.os.Process.myUserHandle());
            sLogger.v("stopped ublox reporting");
        } catch (Throwable t) {
            sLogger.e("stopUbloxReporting", t);
        }
    }

    private void setUbloxAccuracyThreshold() {
        try {
            android.content.Context context = com.navdy.hud.app.HudApplication.getAppContext();
            android.os.UserHandle handle = android.os.Process.myUserHandle();
            android.content.Intent intent = new android.content.Intent(SET_UBLOX_ACCURACY);
            intent.putExtra(UBLOX_ACCURACY, 30);
            context.sendBroadcastAsUser(intent, handle);
            sLogger.v("setUbloxAccuracyThreshold:30");
        } catch (Throwable t) {
            sLogger.e("setUbloxAccuracyThreshold", t);
        }
    }

    private void setUbloxTimeDiscardThreshold() {
        try {
            android.content.Context context = com.navdy.hud.app.HudApplication.getAppContext();
            android.os.UserHandle handle = android.os.Process.myUserHandle();
            android.content.Intent intent = new android.content.Intent(SET_LOCATION_DISCARD_TIME_THRESHOLD);
            intent.putExtra(DISCARD_TIME, 2000);
            context.sendBroadcastAsUser(intent, handle);
            sLogger.v("setUbloxTimeDiscardThreshold:2000");
        } catch (Throwable t) {
            sLogger.e("setUbloxTimeDiscardThreshold", t);
        }
    }

    /* access modifiers changed from: private */
    public void checkGpsToPhoneAccuracy(android.location.Location location) {
        float accuracy = location.getAccuracy();
        long time = android.os.SystemClock.elapsedRealtime() - this.lastPhoneCoordinateTime;
        if (sLogger.isLoggable(3)) {
            sLogger.v("[Gps-loc-navdy] checkGpsToPhoneAccuracy: ublox[" + location.getAccuracy() + "] phone[" + (this.lastPhoneCoordinate != null ? this.lastPhoneCoordinate.accuracy : "n/a") + "] lastPhoneTime [" + (this.lastPhoneCoordinate != null ? java.lang.Long.valueOf(time) : "n/a") + "]");
        }
        if (accuracy != 0.0f) {
            if (this.activeLocationSource == com.navdy.hud.app.device.gps.GpsManager.LocationSource.UBLOX) {
                if (this.lastPhoneCoordinate != null && android.os.SystemClock.elapsedRealtime() - this.lastPhoneCoordinateTime <= MINIMUM_DRIVE_TIME) {
                    if (this.lastPhoneCoordinate.accuracy.floatValue() + 2.0f < accuracy) {
                        if (sLogger.isLoggable(3)) {
                            sLogger.v("[Gps-loc-navdy]  ublox accuracy [" + accuracy + "] > phone accuracy[" + this.lastPhoneCoordinate.accuracy + "]");
                        }
                        if (!this.startUbloxCalled) {
                            return;
                        }
                        if (accuracy <= UBLOX_MIN_ACCEPTABLE_THRESHOLD) {
                            sLogger.v("ublox accuracy [" + accuracy + "] > phone accuracy[" + this.lastPhoneCoordinate.accuracy + "], but not above threshold");
                            stopSendingLocation();
                            return;
                        }
                        sLogger.v("ublox accuracy [" + accuracy + "] > phone accuracy[" + this.lastPhoneCoordinate.accuracy + "], stop uBloxReporting");
                        this.handler.removeCallbacks(this.locationFixRunnable);
                        this.handler.removeCallbacks(this.noPhoneLocationFixRunnable);
                        stopUbloxReporting();
                        sLogger.v("[Gps-loc] [LocationProvider] switched from [" + this.activeLocationSource + "] to [" + com.navdy.hud.app.device.gps.GpsManager.LocationSource.PHONE + "]");
                        this.activeLocationSource = com.navdy.hud.app.device.gps.GpsManager.LocationSource.PHONE;
                        markLocationFix(com.navdy.hud.app.device.gps.GpsConstants.DEBUG_TTS_PHONE_SWITCH, "Phone =" + this.lastPhoneCoordinate.accuracy + " ublox =" + accuracy, true, false);
                    } else if (sLogger.isLoggable(3)) {
                        sLogger.v("ublox accuracy [" + accuracy + "] < phone accuracy[" + this.lastPhoneCoordinate.accuracy + "] no action");
                    }
                }
            } else if (accuracy + 2.0f < this.lastPhoneCoordinate.accuracy.floatValue()) {
                sLogger.v("ublox accuracy [" + accuracy + "] < phone accuracy[" + this.lastPhoneCoordinate.accuracy + "], start uBloxReporting, switch complete");
                this.firstSwitchToUbloxFromPhone = true;
                markLocationFix(com.navdy.hud.app.device.gps.GpsConstants.DEBUG_TTS_UBLOX_SWITCH, "Phone =" + this.lastPhoneCoordinate.accuracy + " ublox =" + accuracy, false, false);
                startUbloxReporting();
            } else if (sLogger.isLoggable(3)) {
                sLogger.v("ublox accuracy [" + accuracy + "] > phone accuracy[" + this.lastPhoneCoordinate.accuracy + "] no action");
            }
        }
    }

    /* access modifiers changed from: private */
    public void markLocationFix(java.lang.String title, java.lang.String info, boolean usingPhone, boolean usingUblox) {
        android.os.Bundle bundle = new android.os.Bundle();
        bundle.putString("title", title);
        bundle.putString("info", info);
        bundle.putBoolean(com.navdy.hud.app.device.gps.GpsConstants.USING_PHONE_LOCATION, usingPhone);
        bundle.putBoolean(com.navdy.hud.app.device.gps.GpsConstants.USING_UBLOX_LOCATION, usingUblox);
        sendGpsEventBroadcast(com.navdy.hud.app.device.gps.GpsConstants.GPS_EVENT_SWITCH, bundle);
    }

    private void setUbloxResetRunnable() {
        sLogger.v("setUbloxResetRunnable");
        this.handler.postDelayed(this.warmResetRunnable, WARM_RESET_INTERVAL);
    }

    /* access modifiers changed from: private */
    public void cancelUbloxResetRunnable() {
        this.handler.removeCallbacks(this.warmResetRunnable);
        this.warmResetCancelled = true;
    }

    public void shutdown() {
        if (this.locationManager != null) {
            this.locationManager.removeUpdates(this.ubloxGpsLocationListener);
            this.locationManager.removeUpdates(this.navdyGpsLocationListener);
            this.locationManager.removeGpsStatusListener(this.ubloxGpsStatusListener);
            this.locationManager.removeNmeaListener(this.ubloxGpsNmeaListener);
        }
    }
}
