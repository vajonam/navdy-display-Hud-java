package com.navdy.hud.app.device.gps;

public class GpsUtils {
    public static com.navdy.hud.app.config.BooleanSetting SHOW_RAW_GPS = new com.navdy.hud.app.config.BooleanSetting("Show Raw GPS", com.navdy.hud.app.config.BooleanSetting.Scope.NEVER, "map.raw_gps", "Add map indicators showing raw and map matched GPS");
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.device.gps.GpsUtils.class);

    public static class GpsSatelliteData {
        public android.os.Bundle data;

        public GpsSatelliteData(android.os.Bundle data2) {
            this.data = data2;
        }
    }

    public static class GpsSwitch {
        public boolean usingPhone;
        public boolean usingUblox;

        public GpsSwitch(boolean usingPhone2, boolean usingUblox2) {
            this.usingPhone = usingPhone2;
            this.usingUblox = usingUblox2;
        }

        public java.lang.String toString() {
            return "GpsSwitch [UsingPhone :" + this.usingPhone + ", UsingUblox : " + this.usingUblox + "]";
        }
    }

    public enum HeadingDirection {
        N,
        NE,
        E,
        SE,
        S,
        SW,
        W,
        NW
    }

    public static boolean isDebugRawGpsPosEnabled() {
        return SHOW_RAW_GPS.isEnabled();
    }

    public static com.navdy.hud.app.device.gps.GpsUtils.HeadingDirection getHeadingDirection(double heading) {
        com.navdy.hud.app.device.gps.GpsUtils.HeadingDirection headingDirection = com.navdy.hud.app.device.gps.GpsUtils.HeadingDirection.N;
        if (heading < 0.0d || heading > 360.0d) {
            return headingDirection;
        }
        return com.navdy.hud.app.device.gps.GpsUtils.HeadingDirection.values()[(int) (((float) ((22.5d + heading) % 360.0d)) / 45.0f)];
    }

    public static void sendEventBroadcast(java.lang.String eventName, android.os.Bundle extras) {
        try {
            android.content.Intent intent = new android.content.Intent(eventName);
            if (extras != null) {
                intent.putExtras(extras);
            }
            com.navdy.hud.app.HudApplication.getAppContext().sendBroadcastAsUser(intent, android.os.Process.myUserHandle());
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }
}
