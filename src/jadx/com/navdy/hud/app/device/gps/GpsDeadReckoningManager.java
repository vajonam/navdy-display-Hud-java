package com.navdy.hud.app.device.gps;

public class GpsDeadReckoningManager implements java.lang.Runnable {
    private static final int ALIGNMENT_DONE = 3;
    private static final int ALIGNMENT_PACKET_LENGTH = 24;
    private static final int ALIGNMENT_PACKET_PAYLOAD_LENGTH = 16;
    private static final byte[] ALIGNMENT_PATTERN = {-75, 98, 16, com.navdy.hud.app.service.pandora.messages.BaseMessage.PNDR_GET_TRACK_ALBUM_ART};
    private static final long ALIGNMENT_POLL_FREQUENCY = 30000;
    private static final java.lang.String ALIGNMENT_TIME = "alignment_time";
    private static final byte[] AUTO_ALIGNMENT = {-75, 98, 6, 86, 12, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 105, 29};
    /* access modifiers changed from: private */
    public static final byte[] CFG_ESF_RAW = {-75, 98, 6, 1, 3, 0, 16, 3, 1, 0, 0};
    private static final byte[] CFG_ESF_RAW_OFF = {-75, 98, 6, 1, 3, 0, 16, 3, 0, 0, 0};
    private static final byte[] CFG_RST_COLD = {-75, 98, 6, 4, 4, 0, -1, -1, 2, 0, 14, 97};
    /* access modifiers changed from: private */
    public static final byte[] CFG_RST_WARM = {-75, 98, 6, 4, 4, 0, 1, 0, 2, 0, 17, 108};
    private static final byte DEAD_RECKONING_ONLY = 1;
    private static final int ESF_PACKET_LENGTH = 19;
    private static final byte[] ESF_RAW_PATTERN = {-75, 98, 16, 3};
    private static final byte[] ESF_STATUS_PATTERN = {-75, 98, 16, 16};
    private static final int FAILURE_RETRY_INTERVAL = 10000;
    private static final byte FIX_2D = 2;
    private static final byte FIX_3D = 3;
    private static final int FUSION_DONE = 1;
    /* access modifiers changed from: private */
    public static final byte[] GET_ALIGNMENT = {-75, 98, 16, com.navdy.hud.app.service.pandora.messages.BaseMessage.PNDR_GET_TRACK_ALBUM_ART, 0, 0, 36, 124};
    /* access modifiers changed from: private */
    public static final byte[] GET_ESF_STATUS = {-75, 98, 16, 16, 0, 0, 32, 112};
    private static final java.lang.String GPS = "gps";
    private static final byte GPS_DEAD_RECKONING_COMBINED = 4;
    private static final java.lang.String GPS_LOG = "gps.log";
    private static final char[] HEX = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
    private static final byte[] INJECT_OBD_SPEED = {-75, 98, 16, 2, 12, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 11, 0, 0};
    private static final int MSG_ALIGNMENT_VALUE = 1;
    private static final byte[] NAV_STATUS_PATTERN = {-75, 98, 1, 3};
    private static final byte NO_FIX = 0;
    private static final byte[] PASSPHRASE = {110, 97, 118, 100, 121};
    private static final java.lang.String PITCH = "pitch";
    private static final long POLL_FREQUENCY = 10000;
    private static final byte[] READ_BUF = new byte[8192];
    private static final java.lang.String ROLL = "roll";
    private static final int SPEED_INJECTION_FREQUENCY = 100;
    private static final int SPEED_TIME_TAG_COUNTER = 100;
    private static final java.lang.String TCP_SERVER_HOST = "127.0.0.1";
    private static final int TCP_SERVER_PORT = 42434;
    private static final byte[] TEMP_BUF = new byte[128];
    private static final java.util.concurrent.atomic.AtomicInteger THREAD_COUNTER = new java.util.concurrent.atomic.AtomicInteger(1);
    private static final byte TIME_ONLY = 5;
    private static final java.lang.String YAW = "yaw";
    private static final com.navdy.hud.app.device.gps.GpsDeadReckoningManager sInstance = new com.navdy.hud.app.device.gps.GpsDeadReckoningManager();
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.device.gps.GpsDeadReckoningManager.class);
    private volatile boolean alignmentChecked;
    private com.navdy.hud.app.device.gps.GpsDeadReckoningManager.Alignment alignmentInfo;
    /* access modifiers changed from: private */
    public final com.squareup.otto.Bus bus;
    /* access modifiers changed from: private */
    public volatile boolean deadReckoningInjectionStarted;
    private boolean deadReckoningOn;
    private byte deadReckoningType = -1;
    private java.lang.Runnable enableEsfRunnable = new com.navdy.hud.app.device.gps.GpsDeadReckoningManager.Anon9();
    /* access modifiers changed from: private */
    public final java.lang.Runnable getAlignmentRunnable = new com.navdy.hud.app.device.gps.GpsDeadReckoningManager.Anon5();
    /* access modifiers changed from: private */
    public final java.lang.Runnable getAlignmentRunnableRetry = new com.navdy.hud.app.device.gps.GpsDeadReckoningManager.Anon6();
    /* access modifiers changed from: private */
    public final java.lang.Runnable getFusionStatusRunnable = new com.navdy.hud.app.device.gps.GpsDeadReckoningManager.Anon3();
    /* access modifiers changed from: private */
    public final java.lang.Runnable getFusionStatusRunnableRetry = new com.navdy.hud.app.device.gps.GpsDeadReckoningManager.Anon4();
    /* access modifiers changed from: private */
    public android.os.Handler handler;
    private android.os.HandlerThread handlerThread;
    private final java.lang.Runnable injectRunnable = new com.navdy.hud.app.device.gps.GpsDeadReckoningManager.Anon2();
    /* access modifiers changed from: private */
    public final com.navdy.hud.app.device.gps.GpsDeadReckoningManager.CommandWriter injectSpeed = new com.navdy.hud.app.device.gps.GpsDeadReckoningManager.Anon1();
    private java.io.InputStream inputStream;
    /* access modifiers changed from: private */
    public long lastConnectionFailure;
    /* access modifiers changed from: private */
    public java.io.OutputStream outputStream;
    private java.lang.Runnable resetRunnable = new com.navdy.hud.app.device.gps.GpsDeadReckoningManager.Anon10();
    private org.json.JSONObject rootInfo;
    private volatile boolean runThread;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.util.GForceRawSensorDataProcessor sensorDataProcessor;
    private java.net.Socket socket;
    private final com.navdy.hud.app.manager.SpeedManager speedManager = com.navdy.hud.app.manager.SpeedManager.getInstance();
    private java.lang.Thread thread;
    private int timeStampCounter = 0;
    /* access modifiers changed from: private */
    public boolean waitForAutoAlignment;
    /* access modifiers changed from: private */
    public boolean waitForFusionStatus;

    private static class Alignment {
        boolean done;
        float pitch;
        float roll;
        float yaw;

        Alignment(float yaw2, float pitch2, float roll2, boolean done2) {
            this.yaw = yaw2;
            this.pitch = pitch2;
            this.roll = roll2;
            this.done = done2;
        }

        public java.lang.String toString() {
            java.lang.StringBuilder sb = new java.lang.StringBuilder("Alignment{");
            sb.append("yaw=").append(this.yaw);
            sb.append(", pitch=").append(this.pitch);
            sb.append(", roll=").append(this.roll);
            sb.append(", done=").append(this.done);
            sb.append('}');
            return sb.toString();
        }
    }

    class Anon1 implements com.navdy.hud.app.device.gps.GpsDeadReckoningManager.CommandWriter {
        Anon1() {
        }

        public void run() throws java.io.IOException {
            com.navdy.hud.app.device.gps.GpsDeadReckoningManager.this.sendObdSpeed();
        }
    }

    class Anon10 implements java.lang.Runnable {
        Anon10() {
        }

        public void run() {
            com.navdy.hud.app.device.gps.GpsDeadReckoningManager.this.invokeUblox(com.navdy.hud.app.device.gps.GpsDeadReckoningManager.CFG_RST_WARM, "RST-WARM (warm reset)");
        }
    }

    class Anon11 implements com.navdy.hud.app.device.gps.GpsDeadReckoningManager.CommandWriter {
        Anon11() {
        }

        public void run() throws java.io.IOException {
            com.navdy.hud.app.device.gps.GpsDeadReckoningManager.this.sendAutoAlignment();
        }
    }

    class Anon2 implements java.lang.Runnable {
        Anon2() {
        }

        public void run() {
            com.navdy.hud.app.device.gps.GpsDeadReckoningManager.this.invokeUblox(com.navdy.hud.app.device.gps.GpsDeadReckoningManager.this.injectSpeed);
            if (!com.navdy.hud.app.device.gps.GpsDeadReckoningManager.this.deadReckoningInjectionStarted || com.navdy.hud.app.device.gps.GpsDeadReckoningManager.this.lastConnectionFailure != 0) {
                com.navdy.hud.app.device.gps.GpsDeadReckoningManager.this.handler.removeCallbacks(this);
            } else {
                com.navdy.hud.app.device.gps.GpsDeadReckoningManager.this.handler.postDelayed(this, 100);
            }
        }
    }

    class Anon3 implements java.lang.Runnable {
        Anon3() {
        }

        public void run() {
            if (com.navdy.hud.app.device.gps.GpsDeadReckoningManager.this.invokeUblox(com.navdy.hud.app.device.gps.GpsDeadReckoningManager.GET_ESF_STATUS, "get fusion status")) {
                com.navdy.hud.app.device.gps.GpsDeadReckoningManager.this.handler.removeCallbacks(com.navdy.hud.app.device.gps.GpsDeadReckoningManager.this.getFusionStatusRunnableRetry);
                com.navdy.hud.app.device.gps.GpsDeadReckoningManager.this.handler.postDelayed(com.navdy.hud.app.device.gps.GpsDeadReckoningManager.this.getFusionStatusRunnableRetry, com.navdy.hud.app.device.gps.GpsDeadReckoningManager.POLL_FREQUENCY);
                return;
            }
            com.navdy.hud.app.device.gps.GpsDeadReckoningManager.this.postFusionRunnable(true);
        }
    }

    class Anon4 implements java.lang.Runnable {
        Anon4() {
        }

        public void run() {
            com.navdy.hud.app.device.gps.GpsDeadReckoningManager.sLogger.v("getFusionStatusRunnableRetry");
            com.navdy.hud.app.device.gps.GpsDeadReckoningManager.this.getFusionStatusRunnable.run();
        }
    }

    class Anon5 implements java.lang.Runnable {
        Anon5() {
        }

        public void run() {
            if (com.navdy.hud.app.device.gps.GpsDeadReckoningManager.this.invokeUblox(com.navdy.hud.app.device.gps.GpsDeadReckoningManager.GET_ALIGNMENT, "get alignment")) {
                com.navdy.hud.app.device.gps.GpsDeadReckoningManager.this.handler.removeCallbacks(com.navdy.hud.app.device.gps.GpsDeadReckoningManager.this.getAlignmentRunnableRetry);
                com.navdy.hud.app.device.gps.GpsDeadReckoningManager.this.handler.postDelayed(com.navdy.hud.app.device.gps.GpsDeadReckoningManager.this.getAlignmentRunnableRetry, 30000);
                return;
            }
            com.navdy.hud.app.device.gps.GpsDeadReckoningManager.this.postAlignmentRunnable(true);
        }
    }

    class Anon6 implements java.lang.Runnable {
        Anon6() {
        }

        public void run() {
            com.navdy.hud.app.device.gps.GpsDeadReckoningManager.sLogger.v("getAlignmentRunnableRetry");
            com.navdy.hud.app.device.gps.GpsDeadReckoningManager.this.getAlignmentRunnable.run();
        }
    }

    class Anon7 implements android.os.Handler.Callback {
        Anon7() {
        }

        public boolean handleMessage(android.os.Message msg) {
            switch (msg.what) {
                case 1:
                    try {
                        com.navdy.hud.app.device.gps.GpsDeadReckoningManager.this.handleAutoAlignmentResult((com.navdy.hud.app.device.gps.GpsDeadReckoningManager.Alignment) msg.obj);
                        break;
                    } catch (Throwable t) {
                        com.navdy.hud.app.device.gps.GpsDeadReckoningManager.sLogger.e(t);
                        if (com.navdy.hud.app.device.gps.GpsDeadReckoningManager.this.waitForAutoAlignment) {
                            if (com.navdy.hud.app.device.gps.GpsDeadReckoningManager.this.waitForFusionStatus) {
                                com.navdy.hud.app.device.gps.GpsDeadReckoningManager.this.postFusionRunnable(true);
                                break;
                            }
                        } else {
                            com.navdy.hud.app.device.gps.GpsDeadReckoningManager.this.postAlignmentRunnable(true);
                            break;
                        }
                    }
                    break;
            }
            return false;
        }
    }

    class Anon8 implements com.navdy.hud.app.device.gps.GpsDeadReckoningManager.CommandWriter {
        final /* synthetic */ java.lang.String val$description;
        final /* synthetic */ byte[] val$message;

        Anon8(java.lang.String str, byte[] bArr) {
            this.val$description = str;
            this.val$message = bArr;
        }

        public void run() throws java.io.IOException {
            if (com.navdy.hud.app.device.gps.GpsDeadReckoningManager.this.outputStream != null) {
                if (this.val$description != null) {
                    com.navdy.hud.app.device.gps.GpsDeadReckoningManager.sLogger.v(this.val$description + " [" + com.navdy.hud.app.device.gps.GpsDeadReckoningManager.bytesToHex(this.val$message, 0, this.val$message.length) + "]");
                }
                com.navdy.hud.app.device.gps.GpsDeadReckoningManager.this.outputStream.write(this.val$message);
                return;
            }
            throw new java.io.IOException("Disconnected socket - failed to " + this.val$description);
        }
    }

    class Anon9 implements java.lang.Runnable {
        Anon9() {
        }

        public void run() {
            com.navdy.hud.app.device.gps.GpsDeadReckoningManager.this.invokeUblox(com.navdy.hud.app.device.gps.GpsDeadReckoningManager.CFG_ESF_RAW, "ESF-RAW");
            com.navdy.hud.app.device.gps.GpsDeadReckoningManager.this.sensorDataProcessor.setCalibrated(false);
            com.navdy.hud.app.device.gps.GpsDeadReckoningManager.this.bus.post(new com.navdy.hud.app.device.gps.CalibratedGForceData(0.0f, 0.0f, 0.0f));
        }
    }

    interface CommandWriter {
        void run() throws java.io.IOException;
    }

    static {
        calculateChecksum(CFG_ESF_RAW);
        calculateChecksum(CFG_ESF_RAW_OFF);
    }

    public static com.navdy.hud.app.device.gps.GpsDeadReckoningManager getInstance() {
        return sInstance;
    }

    private GpsDeadReckoningManager() {
        sLogger.v("ctor()");
        this.sensorDataProcessor = new com.navdy.hud.app.util.GForceRawSensorDataProcessor();
        this.handlerThread = new android.os.HandlerThread("gpsDeadReckoningHandler");
        this.handlerThread.start();
        this.handler = new android.os.Handler(this.handlerThread.getLooper(), new com.navdy.hud.app.device.gps.GpsDeadReckoningManager.Anon7());
        com.navdy.hud.app.obd.ObdManager obdManager = com.navdy.hud.app.obd.ObdManager.getInstance();
        if (obdManager.isConnected()) {
            sLogger.v("ctor() obd is connected");
            if (obdManager.isSpeedPidAvailable()) {
                sLogger.v("ctor() speed pid is available");
                checkAlignment();
                startDeadReckoning();
            } else {
                sLogger.v("ctor() speed pid is not available");
            }
        } else {
            sLogger.v("ctor() obd is not connected, waiting for obd to connect");
        }
        this.bus = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getBus();
        this.bus.register(this);
        sLogger.v("ctor() initialized");
        this.handler.post(this.enableEsfRunnable);
    }

    /* access modifiers changed from: private */
    public boolean invokeUblox(byte[] message, java.lang.String description) {
        return invokeUblox(new com.navdy.hud.app.device.gps.GpsDeadReckoningManager.Anon8(description, message));
    }

    /* access modifiers changed from: private */
    public boolean invokeUblox(com.navdy.hud.app.device.gps.GpsDeadReckoningManager.CommandWriter callback) {
        boolean succeeded = false;
        try {
            if (this.lastConnectionFailure > 0) {
                long diff = android.os.SystemClock.elapsedRealtime() - this.lastConnectionFailure;
                if (diff < POLL_FREQUENCY) {
                    if (sLogger.isLoggable(2)) {
                        sLogger.v("would retry time[" + diff + "]");
                    }
                    return false;
                }
            }
            if (connectSocket()) {
                this.lastConnectionFailure = 0;
                callback.run();
                succeeded = true;
            } else {
                this.lastConnectionFailure = android.os.SystemClock.elapsedRealtime();
            }
        } catch (java.io.IOException e) {
            sLogger.e("Failed to ");
            closeSocket();
            this.lastConnectionFailure = android.os.SystemClock.elapsedRealtime();
        }
        return succeeded;
    }

    @com.squareup.otto.Subscribe
    public void onSupportedPidEventsChange(com.navdy.hud.app.obd.ObdManager.ObdSupportedPidsChangedEvent event) {
        initDeadReckoning();
    }

    @com.squareup.otto.Subscribe
    public void ObdConnectionStatusEvent(com.navdy.hud.app.obd.ObdManager.ObdConnectionStatusEvent event) {
        if (event.connected) {
            sLogger.v("got obd connected");
            initDeadReckoning();
            return;
        }
        sLogger.v("got obd dis-connected");
        stopDeadReckoning();
    }

    private void initDeadReckoning() {
        if (com.navdy.hud.app.obd.ObdManager.getInstance().isSpeedPidAvailable()) {
            sLogger.v("speed pid is available");
            checkAlignment();
            startDeadReckoning();
            return;
        }
        sLogger.v("speed pid is not available");
    }

    private void closeSocket() {
        this.runThread = false;
        com.navdy.service.library.util.IOUtils.closeStream(this.inputStream);
        com.navdy.service.library.util.IOUtils.closeStream(this.outputStream);
        com.navdy.service.library.util.IOUtils.closeStream(this.socket);
        this.inputStream = null;
        this.outputStream = null;
        this.socket = null;
        if (this.thread != null) {
            try {
                if (this.thread.isAlive()) {
                    this.thread.interrupt();
                    sLogger.v("waiting for thread");
                    this.thread.join();
                    sLogger.v("waited");
                } else {
                    sLogger.v("thread is not alive");
                }
            } catch (Throwable t) {
                sLogger.e(t);
            } finally {
                this.thread = null;
            }
        }
        this.deadReckoningOn = false;
        this.deadReckoningType = -1;
    }

    private boolean connectSocket() {
        try {
            if (this.socket != null) {
                return true;
            }
            this.socket = new java.net.Socket(TCP_SERVER_HOST, TCP_SERVER_PORT);
            this.inputStream = this.socket.getInputStream();
            this.outputStream = this.socket.getOutputStream();
            sLogger.v("connected to 42434");
            this.outputStream.write(PASSPHRASE);
            sLogger.v("sent passphrase");
            this.thread = new java.lang.Thread(this);
            this.thread.setName("GpsDeadReckoningManager-" + THREAD_COUNTER.getAndIncrement());
            this.runThread = true;
            this.thread.start();
            com.navdy.hud.app.util.GenericUtil.sleep(2000);
            return true;
        } catch (Throwable t) {
            sLogger.e(t);
            closeSocket();
            return false;
        }
    }

    /* access modifiers changed from: private */
    public void sendObdSpeed() throws java.io.IOException {
        boolean verbose = sLogger.isLoggable(2);
        long speed = (long) this.speedManager.getRawObdSpeed();
        if (speed >= 0) {
            this.timeStampCounter += 100;
            if (this.timeStampCounter < 0) {
                this.timeStampCounter = 100;
            }
            INJECT_OBD_SPEED[6] = (byte) (this.timeStampCounter >> 0);
            INJECT_OBD_SPEED[7] = (byte) (this.timeStampCounter >> 8);
            INJECT_OBD_SPEED[8] = (byte) (this.timeStampCounter >> 16);
            INJECT_OBD_SPEED[9] = (byte) (this.timeStampCounter >> 24);
            long speed2 = (long) ((int) (com.navdy.hud.app.manager.SpeedManager.convertWithPrecision((double) speed, com.navdy.hud.app.manager.SpeedManager.SpeedUnit.KILOMETERS_PER_HOUR, com.navdy.hud.app.manager.SpeedManager.SpeedUnit.METERS_PER_SECOND) * 1000.0f));
            INJECT_OBD_SPEED[14] = (byte) ((int) (speed2 >> 0));
            INJECT_OBD_SPEED[15] = (byte) ((int) (speed2 >> 8));
            INJECT_OBD_SPEED[16] = (byte) ((int) (speed2 >> 16));
            calculateChecksum(INJECT_OBD_SPEED);
            if (verbose) {
                sLogger.v("inject obd speed [" + bytesToHex(INJECT_OBD_SPEED, 0, INJECT_OBD_SPEED.length) + "]");
            }
            this.outputStream.write(INJECT_OBD_SPEED);
        } else if (verbose) {
            sLogger.i("invalid obd speed:" + speed);
        }
    }

    private static void calculateChecksum(byte[] message) {
        int length = message.length;
        byte ck_a = 0;
        byte ck_b = 0;
        for (int i = 2; i < length - 2; i++) {
            ck_a = (byte) (message[i] + ck_a);
            ck_b = (byte) (ck_b + ck_a);
        }
        message[length - 2] = ck_a;
        message[length - 1] = ck_b;
    }

    /* access modifiers changed from: private */
    public static java.lang.String bytesToHex(byte[] bytes, int offset, int count) {
        char[] hexChars = new char[(count * 2)];
        for (int i = 0; i < count; i++) {
            int v = bytes[i + offset] & 255;
            hexChars[i * 2] = HEX[v >>> 4];
            hexChars[(i * 2) + 1] = HEX[v & 15];
        }
        return new java.lang.String(hexChars);
    }

    public void run() {
        sLogger.v("start thread");
        while (this.runThread) {
            try {
                int nread = this.inputStream.read(READ_BUF);
                if (nread == -1) {
                    sLogger.v("eof");
                    this.runThread = false;
                } else if (nread > 0) {
                    int index = com.navdy.hud.app.util.GenericUtil.indexOf(READ_BUF, ESF_RAW_PATTERN, 0, nread - 1);
                    if (index != -1) {
                        handleEsfRaw(index, nread);
                    } else {
                        int index2 = com.navdy.hud.app.util.GenericUtil.indexOf(READ_BUF, NAV_STATUS_PATTERN, 0, nread - 1);
                        if (index2 != -1) {
                            handleNavStatus(index2, nread);
                        } else {
                            if (!this.alignmentChecked) {
                                int index3 = com.navdy.hud.app.util.GenericUtil.indexOf(READ_BUF, ALIGNMENT_PATTERN, 0, nread - 1);
                                if (index3 != -1) {
                                    try {
                                        handleAlignment(index3, nread);
                                    } catch (Throwable t) {
                                        sLogger.e(t);
                                        postAlignmentRunnable(true);
                                    }
                                }
                            }
                            if (this.waitForFusionStatus) {
                                int index4 = com.navdy.hud.app.util.GenericUtil.indexOf(READ_BUF, ESF_STATUS_PATTERN, 0, nread - 1);
                                if (index4 != -1) {
                                    try {
                                        handleFusion(index4, nread);
                                    } catch (Throwable t2) {
                                        sLogger.e(t2);
                                        postFusionRunnable(true);
                                    }
                                }
                            }
                        }
                    }
                }
            } catch (Throwable t3) {
                if (!(t3 instanceof java.lang.InterruptedException)) {
                    sLogger.e(t3);
                }
                this.runThread = false;
            }
        }
        sLogger.v("end thread");
    }

    private void startDeadReckoning() {
        if (!this.deadReckoningInjectionStarted) {
            sLogger.v("starting dead reckoning injection");
            this.deadReckoningInjectionStarted = true;
            this.handler.removeCallbacks(this.injectRunnable);
            this.handler.postDelayed(this.injectRunnable, 100);
        }
    }

    private void stopDeadReckoning() {
        if (this.deadReckoningInjectionStarted) {
            this.waitForAutoAlignment = false;
            this.waitForFusionStatus = false;
            this.alignmentInfo = null;
            this.rootInfo = null;
            this.alignmentChecked = false;
            sLogger.v("stopping dead rekoning injection");
            this.deadReckoningInjectionStarted = false;
            this.handler.removeCallbacks(this.injectRunnable);
            this.handler.removeCallbacks(this.getAlignmentRunnable);
            this.handler.removeCallbacks(this.getAlignmentRunnableRetry);
            this.handler.removeCallbacks(this.getFusionStatusRunnable);
            this.handler.removeCallbacks(this.getFusionStatusRunnableRetry);
        }
    }

    public void sendWarmReset() {
        this.handler.postAtFrontOfQueue(this.resetRunnable);
        this.handler.postDelayed(this.enableEsfRunnable, 2000);
    }

    public void enableEsfRaw() {
        this.handler.post(this.enableEsfRunnable);
    }

    private static java.lang.String getDRType(byte b) {
        switch (b) {
            case 0:
                return "NO_FIX";
            case 1:
                return com.navdy.hud.app.device.gps.GpsConstants.DEAD_RECKONING_ONLY;
            case 2:
                return "FIX_2D";
            case 3:
                return "FIX_3D";
            case 4:
                return com.navdy.hud.app.device.gps.GpsConstants.GPS_DEAD_RECKONING_COMBINED;
            case 5:
                return "TIME_ONLY";
            default:
                return "UNKNOWN";
        }
    }

    private void checkAlignment() {
        if (!this.alignmentChecked) {
            sLogger.v("checking alignment");
            postAlignmentRunnable(false);
            return;
        }
        sLogger.v("alignment already checked");
    }

    /* access modifiers changed from: private */
    public void sendAutoAlignment() throws java.io.IOException {
        if (!com.navdy.hud.app.obd.ObdManager.getInstance().isConnected()) {
            sLogger.v("not connected to obd manager any more");
            return;
        }
        sLogger.v("send auto alignment [" + bytesToHex(AUTO_ALIGNMENT, 0, AUTO_ALIGNMENT.length) + "]");
        this.waitForAutoAlignment = true;
        this.outputStream.write(AUTO_ALIGNMENT);
        postAlignmentRunnable(true);
        com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent(com.navdy.hud.app.analytics.AnalyticsSupport.ANALYTICS_EVENT_GPS_CALIBRATION_START, new java.lang.String[0]);
        com.navdy.hud.app.framework.voice.TTSUtils.debugShowGpsCalibrationStarted();
    }

    private void handleEsfRaw(int index, int nread) {
        this.sensorDataProcessor.onRawData(new com.navdy.hud.app.device.gps.RawSensorData(READ_BUF, index));
        if (this.sensorDataProcessor.isCalibrated()) {
            this.bus.post(new com.navdy.hud.app.device.gps.CalibratedGForceData(this.sensorDataProcessor.xAccel, this.sensorDataProcessor.yAccel, this.sensorDataProcessor.zAccel));
        }
    }

    @com.squareup.otto.Subscribe
    public void onDrivingStateChange(com.navdy.hud.app.event.DrivingStateChange event) {
        if (!event.driving) {
            this.sensorDataProcessor.setCalibrated(false);
        }
    }

    private void handleNavStatus(int index, int nread) throws java.lang.Throwable {
        int gpsFixIndex = index + 10;
        if (gpsFixIndex <= nread - 1) {
            byte val = READ_BUF[gpsFixIndex];
            switch (val) {
                case 1:
                case 4:
                    if (!this.deadReckoningOn) {
                        this.deadReckoningOn = true;
                        this.deadReckoningType = val;
                        java.lang.String drType = getDRType(val);
                        com.navdy.hud.app.framework.voice.TTSUtils.debugShowDRStarted(drType);
                        sLogger.v("dead reckoning on[" + val + "] " + drType);
                        android.os.Bundle bundle = new android.os.Bundle();
                        bundle.putString(com.navdy.hud.app.device.gps.GpsConstants.GPS_EXTRA_DR_TYPE, val == 1 ? com.navdy.hud.app.device.gps.GpsConstants.DEAD_RECKONING_ONLY : com.navdy.hud.app.device.gps.GpsConstants.GPS_DEAD_RECKONING_COMBINED);
                        com.navdy.hud.app.device.gps.GpsUtils.sendEventBroadcast(com.navdy.hud.app.device.gps.GpsConstants.GPS_EVENT_DEAD_RECKONING_STARTED, bundle);
                        return;
                    } else if (this.deadReckoningType != val) {
                        java.lang.String drType2 = getDRType(val);
                        sLogger.v("dead reckoning type changed from [" + this.deadReckoningType + "] to [" + val + "] " + drType2);
                        this.deadReckoningType = val;
                        com.navdy.hud.app.framework.voice.TTSUtils.debugShowDRStarted(drType2);
                        android.os.Bundle bundle2 = new android.os.Bundle();
                        bundle2.putString(com.navdy.hud.app.device.gps.GpsConstants.GPS_EXTRA_DR_TYPE, val == 1 ? com.navdy.hud.app.device.gps.GpsConstants.DEAD_RECKONING_ONLY : com.navdy.hud.app.device.gps.GpsConstants.GPS_DEAD_RECKONING_COMBINED);
                        com.navdy.hud.app.device.gps.GpsUtils.sendEventBroadcast(com.navdy.hud.app.device.gps.GpsConstants.GPS_EVENT_DEAD_RECKONING_STARTED, bundle2);
                        return;
                    } else {
                        return;
                    }
                default:
                    if (this.deadReckoningOn) {
                        this.deadReckoningOn = false;
                        this.deadReckoningType = -1;
                        com.navdy.hud.app.framework.voice.TTSUtils.debugShowDREnded();
                        sLogger.v("dead reckoning stopped:" + val);
                        com.navdy.hud.app.device.gps.GpsUtils.sendEventBroadcast(com.navdy.hud.app.device.gps.GpsConstants.GPS_EVENT_DEAD_RECKONING_STOPPED, null);
                        return;
                    }
                    return;
            }
        }
    }

    private void handleAlignment(int index, int nread) throws java.lang.Throwable {
        if (index + 24 <= nread) {
            sLogger.v("alignment raw data[" + bytesToHex(READ_BUF, index, 24) + "]");
            java.nio.ByteBuffer buffer = java.nio.ByteBuffer.wrap(READ_BUF, index, 24);
            buffer.order(java.nio.ByteOrder.LITTLE_ENDIAN);
            buffer.get(TEMP_BUF, 0, ALIGNMENT_PATTERN.length);
            short len = buffer.getShort();
            if (len != 16) {
                throw new java.lang.RuntimeException("len[" + len + "] expected [" + 16 + "]");
            }
            buffer.get(TEMP_BUF, 0, 4);
            byte b = buffer.get();
            int flags = buffer.get();
            int alignmentStatus = (flags & 14) >> 1;
            boolean autoAlignment = (flags & 1) == 1;
            boolean done = alignmentStatus == 3;
            byte b2 = buffer.get();
            buffer.get();
            float yaw = ((float) buffer.getInt()) / 100.0f;
            float pitch = ((float) buffer.getShort()) / 100.0f;
            float roll = ((float) buffer.getShort()) / 100.0f;
            android.os.Message msg = android.os.Message.obtain();
            msg.what = 1;
            com.navdy.hud.app.device.gps.GpsDeadReckoningManager.Alignment alignment = new com.navdy.hud.app.device.gps.GpsDeadReckoningManager.Alignment(yaw, pitch, roll, done);
            msg.obj = alignment;
            sLogger.v("Alignment bitField[" + flags + "] alignment[" + alignmentStatus + "] autoAlignment[" + autoAlignment + "] version[" + b + "] " + alignment);
            this.handler.removeCallbacks(this.getAlignmentRunnableRetry);
            this.handler.sendMessage(msg);
            return;
        }
        postAlignmentRunnable(true);
    }

    /* access modifiers changed from: private */
    public void handleAutoAlignmentResult(com.navdy.hud.app.device.gps.GpsDeadReckoningManager.Alignment alignment) {
        if (!this.waitForAutoAlignment) {
            sLogger.v("waiting for align:false");
            java.lang.String vin = com.navdy.hud.app.obd.ObdManager.getInstance().getVin();
            if (vin == null) {
                vin = com.navdy.hud.app.storage.db.table.VinInformationTable.UNKNOWN_VIN;
            }
            sLogger.v("Vin is " + vin);
            java.lang.String info = com.navdy.hud.app.storage.db.helper.VinInformationHelper.getVinInfo(vin);
            boolean triggerAlignment = false;
            if (android.text.TextUtils.isEmpty(info)) {
                sLogger.v("no info found for vin,  need auto alignment");
                triggerAlignment = true;
            } else {
                try {
                    sLogger.v("VinInfo is " + info);
                    if (alignment.yaw == 0.0f && alignment.pitch == 0.0f && alignment.roll == 0.0f) {
                        sLogger.w("VinInfo exists but u-blox alignment is lost, trigger alignment");
                        com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent(com.navdy.hud.app.analytics.AnalyticsSupport.ANALYTICS_EVENT_GPS_CALIBRATION_LOST, new java.lang.String[0]);
                        triggerAlignment = true;
                        com.navdy.hud.app.storage.db.helper.VinInformationHelper.deleteVinInfo(vin);
                    } else {
                        this.rootInfo = new org.json.JSONObject(info);
                        sLogger.v("elapsed=" + (java.lang.System.currentTimeMillis() - java.lang.Long.parseLong(this.rootInfo.getJSONObject(GPS).getString(ALIGNMENT_TIME))));
                    }
                } catch (Throwable t) {
                    sLogger.e(t);
                }
            }
            java.lang.String lastActiveVin = com.navdy.hud.app.storage.db.helper.VinInformationHelper.getVinPreference().getString("vin", null);
            sLogger.v("last vin:" + lastActiveVin + " current vin:" + vin);
            if (android.text.TextUtils.isEmpty(lastActiveVin) || android.text.TextUtils.equals(lastActiveVin, vin)) {
                sLogger.v("vin switch not detected");
            } else {
                sLogger.v("vin has switched: trigger auto alignment");
                triggerAlignment = true;
                com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent(com.navdy.hud.app.analytics.AnalyticsSupport.ANALYTICS_EVENT_GPS_CALIBRATION_VIN_SWITCH, new java.lang.String[0]);
            }
            if (triggerAlignment) {
                sLogger.v("alignment required");
                com.navdy.hud.app.storage.db.helper.VinInformationHelper.getVinPreference().edit().remove("vin").commit();
                sLogger.v("last pref removed");
                if (!invokeUblox(new com.navdy.hud.app.device.gps.GpsDeadReckoningManager.Anon11())) {
                    postAlignmentRunnable(true);
                    return;
                }
                return;
            }
            this.alignmentChecked = true;
            sLogger.v("alignment not reqd");
            com.navdy.hud.app.framework.voice.TTSUtils.debugShowGpsSensorCalibrationNotNeeded();
        } else if (!alignment.done) {
            sLogger.v("alignment not done yet, try again");
            postAlignmentRunnable(true);
        } else {
            this.alignmentInfo = alignment;
            com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent(com.navdy.hud.app.analytics.AnalyticsSupport.ANALYTICS_EVENT_GPS_CALIBRATION_IMU_DONE, new java.lang.String[0]);
            com.navdy.hud.app.framework.voice.TTSUtils.debugShowGpsImuCalibrationDone();
            this.waitForAutoAlignment = false;
            this.waitForFusionStatus = true;
            postFusionRunnable(false);
            sLogger.v("got alignment, wait for fusion status");
        }
    }

    private void handleFusion(int index, int nread) throws java.lang.Throwable {
        if (index + 19 <= nread) {
            sLogger.v("esf raw data[" + bytesToHex(READ_BUF, index, 19) + "]");
            java.nio.ByteBuffer buffer = java.nio.ByteBuffer.wrap(READ_BUF, index, 19);
            buffer.order(java.nio.ByteOrder.LITTLE_ENDIAN);
            buffer.get(TEMP_BUF, 0, ALIGNMENT_PATTERN.length);
            short len = buffer.getShort();
            buffer.get(TEMP_BUF, 0, 4);
            int version = buffer.get();
            int initStatus1 = buffer.get();
            int initStatus2 = buffer.get();
            buffer.get(TEMP_BUF, 0, 5);
            int fusionMode = buffer.get();
            this.handler.removeCallbacks(this.getFusionStatusRunnableRetry);
            if (fusionMode != 1) {
                sLogger.v("Fusion not done retry len[" + len + "] fusionMode[" + fusionMode + "] initStatus1[" + initStatus1 + "] initStatus2[" + initStatus2 + "] version[" + version + "]");
                postFusionRunnable(true);
                return;
            }
            sLogger.v("Fusion Done len[" + len + "] fusionMode[" + fusionMode + "] initStatus1[" + initStatus1 + "] initStatus2[" + initStatus2 + "] version[" + version + "]");
            storeVinInfoInDb();
            return;
        }
        postFusionRunnable(true);
    }

    private void storeVinInfoInDb() {
        org.json.JSONObject gps;
        try {
            java.lang.String vin = com.navdy.hud.app.obd.ObdManager.getInstance().getVin();
            if (vin == null) {
                vin = com.navdy.hud.app.storage.db.table.VinInformationTable.UNKNOWN_VIN;
            }
            sLogger.v("store alignment info in db for vin[" + vin + "]");
            if (this.rootInfo == null) {
                this.rootInfo = new org.json.JSONObject();
                gps = new org.json.JSONObject();
                this.rootInfo.put(GPS, gps);
            } else {
                gps = this.rootInfo.getJSONObject(GPS);
            }
            gps.put(YAW, java.lang.String.valueOf(this.alignmentInfo.yaw));
            gps.put(PITCH, java.lang.String.valueOf(this.alignmentInfo.pitch));
            gps.put(ROLL, java.lang.String.valueOf(this.alignmentInfo.roll));
            gps.put(ALIGNMENT_TIME, java.lang.String.valueOf(java.lang.System.currentTimeMillis()));
            com.navdy.hud.app.storage.db.helper.VinInformationHelper.storeVinInfo(vin, this.rootInfo.toString());
            com.navdy.hud.app.storage.db.helper.VinInformationHelper.getVinPreference().edit().putString("vin", vin).commit();
            sLogger.v("store last vin pref [" + vin + "]");
            this.waitForAutoAlignment = false;
            this.waitForFusionStatus = false;
            this.alignmentChecked = true;
            com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent(com.navdy.hud.app.analytics.AnalyticsSupport.ANALYTICS_EVENT_GPS_CALIBRATION_SENSOR_DONE, new java.lang.String[0]);
            com.navdy.hud.app.framework.voice.TTSUtils.debugShowGpsSensorCalibrationDone();
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }

    public void dumpGpsInfo(java.lang.String stagingPath) {
        java.io.FileOutputStream fout = null;
        try {
            java.io.FileOutputStream fout2 = new java.io.FileOutputStream(stagingPath + java.io.File.separator + GPS_LOG);
            try {
                java.io.PrintWriter writer = new java.io.PrintWriter(new java.io.OutputStreamWriter(fout2));
                com.navdy.hud.app.obd.ObdManager obdManager = com.navdy.hud.app.obd.ObdManager.getInstance();
                writer.write("obd_connected=" + obdManager.isConnected() + com.navdy.hud.app.framework.glance.GlanceConstants.NEWLINE);
                if (this.deadReckoningInjectionStarted) {
                    java.lang.String vin = obdManager.getVin();
                    if (vin == null) {
                        vin = com.navdy.hud.app.storage.db.table.VinInformationTable.UNKNOWN_VIN;
                    }
                    writer.write("vin=" + vin + com.navdy.hud.app.framework.glance.GlanceConstants.NEWLINE);
                    writer.write("calibrated=" + (this.rootInfo == null ? "no" : this.rootInfo.toString()));
                    writer.write(com.navdy.hud.app.framework.glance.GlanceConstants.NEWLINE);
                }
                writer.flush();
                sLogger.i("gps log written:" + stagingPath);
                com.navdy.service.library.util.IOUtils.fileSync(fout2);
                com.navdy.service.library.util.IOUtils.closeStream(fout2);
                java.io.FileOutputStream fileOutputStream = fout2;
            } catch (Throwable th) {
                th = th;
                fout = fout2;
                com.navdy.service.library.util.IOUtils.fileSync(fout);
                com.navdy.service.library.util.IOUtils.closeStream(fout);
                throw th;
            }
        } catch (Throwable th2) {
            t = th2;
            try {
                sLogger.e(t);
                com.navdy.service.library.util.IOUtils.fileSync(fout);
                com.navdy.service.library.util.IOUtils.closeStream(fout);
            } catch (Throwable th3) {
                th = th3;
                com.navdy.service.library.util.IOUtils.fileSync(fout);
                com.navdy.service.library.util.IOUtils.closeStream(fout);
                throw th;
            }
        }
    }

    public java.lang.String getDeadReckoningStatus() {
        android.content.res.Resources resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        com.navdy.hud.app.obd.ObdManager obdManager = com.navdy.hud.app.obd.ObdManager.getInstance();
        if (!obdManager.isConnected()) {
            return resources.getString(com.navdy.hud.app.R.string.obd_not_connected);
        }
        if (!obdManager.isSpeedPidAvailable()) {
            return resources.getString(com.navdy.hud.app.R.string.obd_no_speed_pid);
        }
        if (!this.deadReckoningInjectionStarted) {
            return resources.getString(com.navdy.hud.app.R.string.gps_calibration_unknown);
        }
        if (this.rootInfo != null) {
            return resources.getString(com.navdy.hud.app.R.string.gps_calibrated) + " " + this.rootInfo.toString();
        }
        if (this.waitForAutoAlignment) {
            return resources.getString(com.navdy.hud.app.R.string.gps_calibrating_sensor);
        }
        if (this.waitForFusionStatus) {
            return resources.getString(com.navdy.hud.app.R.string.gps_calibrating_fusion);
        }
        return resources.getString(com.navdy.hud.app.R.string.gps_calibration_unknown);
    }

    /* access modifiers changed from: private */
    public void postAlignmentRunnable(boolean delayed) {
        this.handler.removeCallbacks(this.getAlignmentRunnable);
        this.handler.removeCallbacks(this.getAlignmentRunnableRetry);
        if (delayed) {
            this.handler.postDelayed(this.getAlignmentRunnable, 30000);
        } else {
            this.handler.post(this.getAlignmentRunnable);
        }
    }

    /* access modifiers changed from: private */
    public void postFusionRunnable(boolean delayed) {
        this.handler.removeCallbacks(this.getFusionStatusRunnable);
        this.handler.removeCallbacks(this.getFusionStatusRunnableRetry);
        if (delayed) {
            this.handler.postDelayed(this.getFusionStatusRunnable, POLL_FREQUENCY);
        } else {
            this.handler.post(this.getFusionStatusRunnable);
        }
    }
}
