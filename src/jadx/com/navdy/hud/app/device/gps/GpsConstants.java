package com.navdy.hud.app.device.gps;

public class GpsConstants {
    public static final int ACCURACY_REPORTING_INTERVAL = 300000;
    public static final java.lang.String DEAD_RECKONING_ONLY = "DEAD_RECKONING_ONLY";
    public static final java.lang.String DEBUG_TTS_PHONE_SWITCH = "Switched to Phone Gps";
    public static final java.lang.String DEBUG_TTS_UBLOX_SWITCH = "Switched to Ublox Gps";
    public static final java.lang.String DRIVE_LOGS_FOLDER = "drive_logs";
    public static final java.lang.String EMPTY = "";
    public static final java.lang.String EXTRAPOLATION = "EXTRAPOLATION";
    public static final int EXTRAPOLATION_EXPIRY_INTERVAL = 5000;
    public static final java.lang.String EXTRAPOLATION_FLAG = "EXTRAPOLATION_FLAG";
    public static final java.lang.String GPS_COLLECT_LOGS = "GPS_COLLECT_LOGS";
    public static final java.lang.String GPS_DEAD_RECKONING_COMBINED = "GPS_DEAD_RECKONING_COMBINED";
    public static final java.lang.String GPS_EVENT_ACCURACY = "accuracy";
    public static final java.lang.String GPS_EVENT_ACCURACY_AVERAGE = "Average_Accuracy";
    public static final java.lang.String GPS_EVENT_ACCURACY_MAX = "Max_Accuracy";
    public static final java.lang.String GPS_EVENT_ACCURACY_MIN = "Min_Accuracy";
    public static final java.lang.String GPS_EVENT_DEAD_RECKONING_STARTED = "GPS_DR_STARTED";
    public static final java.lang.String GPS_EVENT_DEAD_RECKONING_STOPPED = "GPS_DR_STOPPED";
    public static final java.lang.String GPS_EVENT_DRIVING_STARTED = "driving_started";
    public static final java.lang.String GPS_EVENT_DRIVING_STOPPED = "driving_stopped";
    public static final java.lang.String GPS_EVENT_ENABLE_ESF_RAW = "GPS_ENABLE_ESF_RAW";
    public static final java.lang.String GPS_EVENT_SATELLITE_DATA = "satellite_data";
    public static final java.lang.String GPS_EVENT_SWITCH = "GPS_Switch";
    public static final java.lang.String GPS_EVENT_TIME = "time";
    public static final java.lang.String GPS_EVENT_WARM_RESET_UBLOX = "GPS_WARM_RESET_UBLOX";
    public static final java.lang.String GPS_EXTRA_DR_TYPE = "drtype";
    public static final java.lang.String GPS_EXTRA_LOG_PATH = "logPath";
    public static final java.lang.String GPS_FIX_TYPE = "SAT_FIX_TYPE";
    public static final java.lang.String GPS_PHONE_MARKER = "P";
    public static final java.lang.String GPS_SATELLITE_DB = "SAT_DB_";
    public static final java.lang.String GPS_SATELLITE_ID = "SAT_ID_";
    public static final java.lang.String GPS_SATELLITE_MAX_DB = "SAT_MAX_DB";
    public static final java.lang.String GPS_SATELLITE_PROVIDER = "SAT_PROVIDER_";
    public static final java.lang.String GPS_SATELLITE_SEEN = "SAT_SEEN";
    public static final java.lang.String GPS_SATELLITE_STATUS = "GPS_SATELLITE_STATUS";
    public static final java.lang.String GPS_SATELLITE_USED = "SAT_USED";
    public static final java.lang.String GPS_UBLOX_MARKER = "U";
    public static final java.lang.String INFO = "info";
    public static final int LOCATION_FIX_CHECK_INTERVAL = 1000;
    public static final int LOCATION_FIX_THRESHOLD = 3000;
    public static final java.lang.String NAVDY_GPS_PROVIDER = "NAVDY_GPS_PROVIDER";
    public static final int NO_LOCATION_FIX_CHECK_INTERVAL = 5000;
    public static final java.lang.String SPACE = " ";
    public static final java.lang.String TITLE = "title";
    public static final java.lang.String USING_PHONE_LOCATION = "phone";
    public static final java.lang.String USING_UBLOX_LOCATION = "ublox";
}
