package com.navdy.hud.app.device.gps;

public class RawSensorData {
    private static final float ACCEL_SCALE_FACTOR = ((float) java.lang.Math.pow(2.0d, -10.0d));
    public static final int ACCEL_X = 16;
    public static final int ACCEL_Y = 17;
    public static final int ACCEL_Z = 18;
    private static final float GYRO_SCALE_FACTOR = ((float) java.lang.Math.pow(2.0d, -12.0d));
    public static final int GYRO_TEMP = 12;
    public static final int GYRO_X = 13;
    public static final int GYRO_Y = 14;
    public static final int GYRO_Z = 5;
    public static final float MAX_ACCEL = 2.0f;
    public final float x;
    public final float y;
    public final float z;

    public RawSensorData(byte[] rawMessage, int offset) {
        java.nio.ByteBuffer buffer = java.nio.ByteBuffer.wrap(rawMessage, offset + 4, (rawMessage.length - offset) - 4);
        buffer.order(java.nio.ByteOrder.LITTLE_ENDIAN);
        int samples = (buffer.getShort() - 4) / 8;
        buffer.mark();
        this.x = clamp(average(buffer, samples, 16, ACCEL_SCALE_FACTOR) / 9.8f, -2.0f, 2.0f);
        this.y = clamp(average(buffer, samples, 17, ACCEL_SCALE_FACTOR) / 9.8f, -2.0f, 2.0f);
        this.z = clamp(average(buffer, samples, 18, ACCEL_SCALE_FACTOR) / 9.8f, -2.0f, 2.0f);
    }

    private float clamp(float val, float min, float max) {
        if (val < min) {
            return min;
        }
        if (val > max) {
            return max;
        }
        return val;
    }

    private float average(java.nio.ByteBuffer buffer, int samples, int type, float scaleFactor) {
        buffer.reset();
        int i = buffer.getInt();
        int count = 0;
        float sum = 0.0f;
        for (int i2 = 0; i2 < samples && buffer.position() < buffer.limit(); i2++) {
            int data = buffer.getInt();
            int i3 = buffer.getInt();
            if ((data >> 24) == type) {
                int data2 = data & android.support.v4.view.ViewCompat.MEASURED_SIZE_MASK;
                if ((8388608 & data2) != 0) {
                    data2 |= -16777216;
                }
                sum += ((float) data2) * scaleFactor;
                count++;
            }
        }
        if (count > 0) {
            return sum / ((float) count);
        }
        return 0.0f;
    }
}
