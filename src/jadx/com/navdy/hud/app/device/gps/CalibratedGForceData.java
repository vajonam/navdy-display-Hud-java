package com.navdy.hud.app.device.gps;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0007\n\u0002\b\f\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\b\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\f\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\r\u001a\u00020\u0003H\u00c6\u0003J'\u0010\u000e\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\u000f\u001a\u00020\u00102\b\u0010\u0011\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0012\u001a\u00020\u0013H\u00d6\u0001J\t\u0010\u0014\u001a\u00020\u0015H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\bR\u0011\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\b\u00a8\u0006\u0016"}, d2 = {"Lcom/navdy/hud/app/device/gps/CalibratedGForceData;", "", "xAccel", "", "yAccel", "zAccel", "(FFF)V", "getXAccel", "()F", "getYAccel", "getZAccel", "component1", "component2", "component3", "copy", "equals", "", "other", "hashCode", "", "toString", "", "app_hudDebug"}, k = 1, mv = {1, 1, 6})
/* compiled from: CalibratedGForceData.kt */
public final class CalibratedGForceData {
    private final float xAccel;
    private final float yAccel;
    private final float zAccel;

    @org.jetbrains.annotations.NotNull
    public static /* bridge */ /* synthetic */ com.navdy.hud.app.device.gps.CalibratedGForceData copy$default(com.navdy.hud.app.device.gps.CalibratedGForceData calibratedGForceData, float f, float f2, float f3, int i, java.lang.Object obj) {
        if ((i & 1) != 0) {
            f = calibratedGForceData.xAccel;
        }
        if ((i & 2) != 0) {
            f2 = calibratedGForceData.yAccel;
        }
        if ((i & 4) != 0) {
            f3 = calibratedGForceData.zAccel;
        }
        return calibratedGForceData.copy(f, f2, f3);
    }

    public final float component1() {
        return this.xAccel;
    }

    public final float component2() {
        return this.yAccel;
    }

    public final float component3() {
        return this.zAccel;
    }

    @org.jetbrains.annotations.NotNull
    public final com.navdy.hud.app.device.gps.CalibratedGForceData copy(float xAccel2, float yAccel2, float zAccel2) {
        return new com.navdy.hud.app.device.gps.CalibratedGForceData(xAccel2, yAccel2, zAccel2);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0024, code lost:
        if (java.lang.Float.compare(r2.zAccel, r3.zAccel) == 0) goto L_0x0026;
     */
    public boolean equals(java.lang.Object obj) {
        if (this != obj) {
            if (obj instanceof com.navdy.hud.app.device.gps.CalibratedGForceData) {
                com.navdy.hud.app.device.gps.CalibratedGForceData calibratedGForceData = (com.navdy.hud.app.device.gps.CalibratedGForceData) obj;
                if (java.lang.Float.compare(this.xAccel, calibratedGForceData.xAccel) == 0) {
                    if (java.lang.Float.compare(this.yAccel, calibratedGForceData.yAccel) == 0) {
                    }
                }
            }
            return false;
        }
        return true;
    }

    public int hashCode() {
        return (((java.lang.Float.floatToIntBits(this.xAccel) * 31) + java.lang.Float.floatToIntBits(this.yAccel)) * 31) + java.lang.Float.floatToIntBits(this.zAccel);
    }

    public java.lang.String toString() {
        return "CalibratedGForceData(xAccel=" + this.xAccel + ", yAccel=" + this.yAccel + ", zAccel=" + this.zAccel + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.CLOSE_BRACKET;
    }

    public CalibratedGForceData(float xAccel2, float yAccel2, float zAccel2) {
        this.xAccel = xAccel2;
        this.yAccel = yAccel2;
        this.zAccel = zAccel2;
    }

    public final float getXAccel() {
        return this.xAccel;
    }

    public final float getYAccel() {
        return this.yAccel;
    }

    public final float getZAccel() {
        return this.zAccel;
    }
}
