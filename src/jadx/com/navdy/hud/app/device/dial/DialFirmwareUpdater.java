package com.navdy.hud.app.device.dial;

public class DialFirmwareUpdater {
    private static final int FIRMWARE_CHUNK_SIZE = 16;
    public static final java.lang.String FIRMWARE_INCREMENTAL_VERSION_PREFIX = "Firmware incremental version: ";
    public static final java.lang.String FIRMWARE_LOCAL_FILE = "/system/etc/firmware/dial.ota";
    public static final java.lang.String FIRMWARE_VERSION_PREFIX = "Firmware version: ";
    public static final int LOG_PROGRESS_PERIOD = 30;
    private static final int OTA_COMMAND_BOOT_DFU = 3;
    private static final int OTA_COMMAND_ERASE_BLOCK_0 = 0;
    private static final int OTA_COMMAND_ERASE_BLOCK_1 = 1;
    private static final int OTA_COMMAND_PREPARE = 4;
    private static final int OTA_COMMAND_RESET_DFU_PTR = 2;
    private static final int OTA_STATE_CANCELLED = 1;
    private static final int OTA_STATE_NONE = 0;
    private static final int OTA_STATE_READY_TO_REBOOT = 32;
    private static final int OTA_STATE_STARTED = 16;
    private static final int RESP_ERROR_OTA_ALREADY_STARTED = 131;
    private static final int RESP_ERROR_OTA_INVALID_COMMAND = 128;
    private static final int RESP_ERROR_OTA_TOO_LARGE = 129;
    private static final int RESP_ERROR_OTA_UNKNOWN_STATE = 130;
    private static final int RESP_OK = 0;
    private static final int UNDETERMINED_FIRMWARE_VERSION = -1;
    private static final long UPDATE_FINISH_DELAY = 5000;
    private static final long UPDATE_INTERPACKET_DELAY = 50;
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.device.dial.DialFirmwareUpdater.class);
    private int commandSent;
    private android.bluetooth.BluetoothGattCharacteristic deviceInfoFirmwareRevisionCharacteristic;
    private final com.navdy.hud.app.device.dial.DialManager dialManager;
    private java.lang.String dialName = null;
    private byte[] firmwareData;
    private int firmwareOffset;
    private android.os.Handler handler;
    private android.bluetooth.BluetoothGattCharacteristic otaControlCharacteristic;
    private android.bluetooth.BluetoothGattCharacteristic otaDataCharacteristic;
    private android.bluetooth.BluetoothGattCharacteristic otaIncrementalVersionCharacteristic;
    private java.lang.Runnable sendFirmwareChunkRunnable = new com.navdy.hud.app.device.dial.DialFirmwareUpdater.Anon1();
    private com.navdy.hud.app.device.dial.DialFirmwareUpdater.UpdateListener updateListener;
    private com.navdy.hud.app.device.dial.DialFirmwareUpdater.UpdateProgressListener updateProgressListener;
    private java.util.concurrent.atomic.AtomicBoolean updateRunning = new java.util.concurrent.atomic.AtomicBoolean(false);
    private com.navdy.hud.app.device.dial.DialFirmwareUpdater.Versions versions = new com.navdy.hud.app.device.dial.DialFirmwareUpdater.Versions();

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            com.navdy.hud.app.device.dial.DialFirmwareUpdater.this.sendFirmwareChunk();
        }
    }

    class Anon2 implements java.lang.Runnable {
        final /* synthetic */ android.bluetooth.BluetoothDevice val$device;

        Anon2(android.bluetooth.BluetoothDevice bluetoothDevice) {
            this.val$device = bluetoothDevice;
        }

        public void run() {
            com.navdy.hud.app.device.dial.DialManager.getInstance().forgetDial(this.val$device);
            com.navdy.hud.app.device.dial.DialFirmwareUpdater.this.finishUpdate(com.navdy.hud.app.device.dial.DialFirmwareUpdater.Error.NONE, null);
        }
    }

    public enum Error {
        NONE,
        INVALID_IMAGE_SIZE,
        INVALID_COMMAND,
        INVALID_RESPONSE,
        CANCELLED,
        TIMEDOUT
    }

    public interface UpdateListener {
        void onUpdateState(boolean z);
    }

    public interface UpdateProgressListener {
        void onFinished(com.navdy.hud.app.device.dial.DialFirmwareUpdater.Error error, java.lang.String str);

        void onProgress(int i);
    }

    public class Versions {
        public com.navdy.hud.app.device.dial.DialFirmwareUpdater.Versions.Version dial = new com.navdy.hud.app.device.dial.DialFirmwareUpdater.Versions.Version();
        public com.navdy.hud.app.device.dial.DialFirmwareUpdater.Versions.Version local = new com.navdy.hud.app.device.dial.DialFirmwareUpdater.Versions.Version();

        public class Version {
            public int incrementalVersion;
            public java.lang.String revisionString;

            Version() {
                reset();
            }

            /* access modifiers changed from: 0000 */
            public void reset() {
                this.revisionString = null;
                this.incrementalVersion = -1;
            }
        }

        public Versions() {
        }

        public boolean isUpdateAvailable() {
            if (this.local.incrementalVersion == -1 || this.dial.incrementalVersion == -1 || this.dial.incrementalVersion >= this.local.incrementalVersion || !isUpdateOK()) {
                return false;
            }
            return true;
        }

        private boolean isUpdateOK() {
            if (this.dial.incrementalVersion >= 29) {
                return true;
            }
            com.navdy.hud.app.device.dial.DialFirmwareUpdater.sLogger.i("New dial firmware update logic is disabled (Current version < 29)");
            return false;
        }
    }

    public com.navdy.hud.app.device.dial.DialFirmwareUpdater.Versions getVersions() {
        return this.versions;
    }

    public void setUpdateListener(com.navdy.hud.app.device.dial.DialFirmwareUpdater.UpdateListener updateListener2) {
        this.updateListener = updateListener2;
    }

    public DialFirmwareUpdater(com.navdy.hud.app.device.dial.DialManager dialManager2, android.os.Handler handler2) {
        this.dialManager = dialManager2;
        this.handler = handler2;
        try {
            this.firmwareData = com.navdy.service.library.util.IOUtils.readBinaryFile(FIRMWARE_LOCAL_FILE);
            this.versions.local.reset();
            java.lang.String v = extractVersion(this.firmwareData, FIRMWARE_INCREMENTAL_VERSION_PREFIX);
            if (v != null) {
                this.versions.local.incrementalVersion = java.lang.Integer.parseInt(v);
            }
            this.versions.local.revisionString = extractVersion(this.firmwareData, FIRMWARE_VERSION_PREFIX);
        } catch (java.io.FileNotFoundException e) {
            sLogger.w("no dial firmware OTA file found");
        } catch (Throwable e2) {
            sLogger.e("can't read dial firmware OTA file", e2);
        }
    }

    private static java.lang.String extractVersion(byte[] firmwareData2, java.lang.String prefix) {
        int j = indexOf(firmwareData2, prefix.getBytes());
        if (j < 0) {
            return null;
        }
        for (int i = j; i < firmwareData2.length; i++) {
            if (firmwareData2[i] == 0 || firmwareData2[i] == 13) {
                return new java.lang.String(java.util.Arrays.copyOfRange(firmwareData2, prefix.length() + j, i));
            }
        }
        return null;
    }

    public static int indexOf(byte[] array, byte[] target) {
        for (int i = 0; i < (array.length - target.length) + 1; i++) {
            boolean found = true;
            int j = 0;
            while (true) {
                if (j >= target.length) {
                    break;
                } else if (array[i + j] != target[j]) {
                    found = false;
                    break;
                } else {
                    j++;
                }
            }
            if (found) {
                return i;
            }
        }
        return -1;
    }

    public boolean characteristicsAvailable() {
        return (this.otaControlCharacteristic == null || this.otaDataCharacteristic == null || this.otaIncrementalVersionCharacteristic == null || this.deviceInfoFirmwareRevisionCharacteristic == null) ? false : true;
    }

    public void onServicesDiscovered(android.bluetooth.BluetoothGatt gatt) {
        this.otaControlCharacteristic = null;
        this.otaDataCharacteristic = null;
        this.otaIncrementalVersionCharacteristic = null;
        this.deviceInfoFirmwareRevisionCharacteristic = null;
        this.dialName = this.dialManager.getDialName();
        for (android.bluetooth.BluetoothGattService service : gatt.getServices()) {
            java.util.UUID uuid = service.getUuid();
            sLogger.d("service: " + uuid.toString());
            if (com.navdy.hud.app.device.dial.DialConstants.OTA_SERVICE_UUID.equals(uuid)) {
                for (android.bluetooth.BluetoothGattCharacteristic characteristic : service.getCharacteristics()) {
                    if (com.navdy.hud.app.device.dial.DialConstants.OTA_CONTROL_CHARACTERISTIC_UUID.equals(characteristic.getUuid())) {
                        this.otaControlCharacteristic = characteristic;
                        this.otaControlCharacteristic.setWriteType(2);
                    } else if (com.navdy.hud.app.device.dial.DialConstants.OTA_DATA_CHARACTERISTIC_UUID.equals(characteristic.getUuid())) {
                        this.otaDataCharacteristic = characteristic;
                        this.otaDataCharacteristic.setWriteType(1);
                    } else if (com.navdy.hud.app.device.dial.DialConstants.OTA_INCREMENTAL_VERSION_CHARACTERISTIC_UUID.equals(characteristic.getUuid())) {
                        this.otaIncrementalVersionCharacteristic = characteristic;
                    }
                }
            } else if (com.navdy.hud.app.device.dial.DialConstants.DEVICE_INFO_SERVICE_UUID.equals(uuid)) {
                for (android.bluetooth.BluetoothGattCharacteristic characteristic2 : service.getCharacteristics()) {
                    if (com.navdy.hud.app.device.dial.DialConstants.FIRMWARE_REVISION_CHARACTERISTIC_UUID.equals(characteristic2.getUuid())) {
                        this.deviceInfoFirmwareRevisionCharacteristic = characteristic2;
                    }
                }
            }
        }
        if (characteristicsAvailable()) {
            sLogger.d("required OTA characteristics found");
            startVersionDetection();
            return;
        }
        sLogger.e("required OTA characteristics not found");
    }

    public void onCharacteristicRead(android.bluetooth.BluetoothGattCharacteristic characteristic, int status) {
        if (characteristicsAvailable()) {
            if (characteristic == this.otaIncrementalVersionCharacteristic) {
                if (status == 0) {
                    java.lang.String ver = new java.lang.String(characteristic.getValue());
                    this.versions.dial.incrementalVersion = java.lang.Integer.parseInt(ver);
                    sLogger.i(java.lang.String.format("updateAvailable: %s (%d -> %d)", new java.lang.Object[]{java.lang.Boolean.valueOf(this.versions.isUpdateAvailable()), java.lang.Integer.valueOf(this.versions.dial.incrementalVersion), java.lang.Integer.valueOf(this.versions.local.incrementalVersion)}));
                    if (this.updateListener != null) {
                        this.updateListener.onUpdateState(this.versions.isUpdateAvailable());
                    }
                } else {
                    sLogger.e("can't read dial firmware incremental version");
                }
            }
            if (characteristic != this.deviceInfoFirmwareRevisionCharacteristic) {
                return;
            }
            if (status == 0) {
                this.versions.dial.revisionString = new java.lang.String(characteristic.getValue());
                sLogger.i("dial.revisionString: " + this.versions.dial.revisionString);
                this.dialManager.queueRead(this.otaIncrementalVersionCharacteristic);
                return;
            }
            sLogger.e("can't read dial firmware revisionString string");
        }
    }

    public void onCharacteristicWrite(android.bluetooth.BluetoothGatt gatt, android.bluetooth.BluetoothGattCharacteristic characteristic, int status) {
        if (characteristicsAvailable()) {
            if (characteristic == this.otaControlCharacteristic) {
                sLogger.d("dial OTA response for command " + this.commandSent + ", status: " + status);
                if (status == 0) {
                    switch (this.commandSent) {
                        case 0:
                            sendCommand(1);
                            return;
                        case 1:
                            sendCommand(2);
                            return;
                        case 2:
                            startFirmwareTransfer();
                            return;
                        case 4:
                            sendCommand(0);
                            return;
                        default:
                            return;
                    }
                } else if (this.commandSent != 3) {
                    finishUpdate(com.navdy.hud.app.device.dial.DialFirmwareUpdater.Error.INVALID_COMMAND, java.lang.String.valueOf(this.commandSent));
                }
            } else if (characteristic != this.otaDataCharacteristic) {
            } else {
                if (status != 0) {
                    sLogger.d("dial OTA error response for data chunk, status: " + status);
                    finishUpdate(com.navdy.hud.app.device.dial.DialFirmwareUpdater.Error.INVALID_RESPONSE, java.lang.String.valueOf(status));
                } else if (!this.updateRunning.get()) {
                } else {
                    if (this.firmwareOffset < this.firmwareData.length) {
                        this.handler.postDelayed(this.sendFirmwareChunkRunnable, UPDATE_INTERPACKET_DELAY);
                        return;
                    }
                    sLogger.v("f/w upgrade complete, send OTA_COMMAND_BOOT_DFU, mark updating false");
                    this.updateRunning.set(false);
                    sendCommand(3);
                    this.handler.postDelayed(new com.navdy.hud.app.device.dial.DialFirmwareUpdater.Anon2(gatt.getDevice()), 5000);
                }
            }
        }
    }

    private void sendCommand(int command) {
        sLogger.d("sendCommand: " + command);
        this.commandSent = command;
        this.dialManager.queueWrite(this.otaControlCharacteristic, new byte[]{(byte) command});
    }

    public boolean startVersionDetection() {
        this.versions.dial.reset();
        if (!characteristicsAvailable()) {
            return false;
        }
        this.dialManager.queueRead(this.deviceInfoFirmwareRevisionCharacteristic);
        return true;
    }

    public boolean startUpdate(com.navdy.hud.app.device.dial.DialFirmwareUpdater.UpdateProgressListener progressListener) {
        sLogger.i("starting dial firmware update");
        if (this.firmwareData.length % 16 != 0) {
            java.lang.String err = "dial firmware OTA file size is not divisible by 16, aborting update";
            sLogger.e(err);
            progressListener.onFinished(com.navdy.hud.app.device.dial.DialFirmwareUpdater.Error.INVALID_IMAGE_SIZE, err);
            return false;
        } else if (!this.updateRunning.compareAndSet(false, true)) {
            return false;
        } else {
            this.updateProgressListener = progressListener;
            sendCommand(4);
            return true;
        }
    }

    public void cancelUpdate() {
        if (this.updateRunning.compareAndSet(true, false)) {
            finishUpdate(com.navdy.hud.app.device.dial.DialFirmwareUpdater.Error.CANCELLED, null);
        }
    }

    public java.lang.String getDialName() {
        return this.dialName;
    }

    private void startFirmwareTransfer() {
        this.firmwareOffset = 0;
        sendFirmwareChunk();
    }

    /* access modifiers changed from: private */
    public void sendFirmwareChunk() {
        if ((this.firmwareOffset / 16) % 30 == 0) {
            sLogger.d("firmwareOffset: " + this.firmwareOffset);
            if (this.updateProgressListener != null) {
                this.updateProgressListener.onProgress((this.firmwareOffset * 100) / this.firmwareData.length);
            }
        }
        this.dialManager.queueWrite(this.otaDataCharacteristic, java.util.Arrays.copyOfRange(this.firmwareData, this.firmwareOffset, this.firmwareOffset + 16));
        this.firmwareOffset += 16;
    }

    /* access modifiers changed from: private */
    public void finishUpdate(com.navdy.hud.app.device.dial.DialFirmwareUpdater.Error error, java.lang.String detail) {
        sLogger.i("finished dial update error: " + error + " , " + detail);
        if (this.updateProgressListener != null) {
            this.updateProgressListener.onFinished(error, detail);
        }
    }
}
