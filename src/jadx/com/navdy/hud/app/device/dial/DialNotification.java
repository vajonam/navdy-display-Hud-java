package com.navdy.hud.app.device.dial;

public class DialNotification {
    private static final java.lang.String[] BATTERY_TOASTS = {DIAL_LOW_BATTERY_ID, DIAL_VERY_LOW_BATTERY_ID, DIAL_EXTREMELY_LOW_BATTERY_ID};
    private static final int DIAL_BATTERY_TIMEOUT = 2000;
    private static final int DIAL_BATTERY_TIMEOUT_EXTREMELY_LOW = 20000;
    public static final int DIAL_CONNECTED_TIMEOUT = 5000;
    public static final java.lang.String DIAL_CONNECT_ID = "dial-connect";
    private static final int DIAL_DISCONNECTED_TIMEOUT = 1000;
    public static final java.lang.String DIAL_DISCONNECT_ID = "dial-disconnect";
    public static final java.lang.String DIAL_EXTREMELY_LOW_BATTERY_ID = "dial-exlow-battery";
    public static final java.lang.String DIAL_FORGOTTEN_ID = "dial-forgotten";
    private static final int DIAL_FORGOTTEN_TIMEOUT = 2000;
    public static final java.lang.String DIAL_LOW_BATTERY_ID = "dial-low-battery";
    private static final java.lang.String[] DIAL_TOASTS = {DIAL_CONNECT_ID, DIAL_DISCONNECT_ID, DIAL_FORGOTTEN_ID, DIAL_LOW_BATTERY_ID, DIAL_VERY_LOW_BATTERY_ID, DIAL_EXTREMELY_LOW_BATTERY_ID};
    public static final java.lang.String DIAL_VERY_LOW_BATTERY_ID = "dial-vlow-battery";
    private static final java.lang.String EMPTY = "";
    private static final java.lang.String battery_unknown = resources.getString(com.navdy.hud.app.R.string.question_mark);
    private static final java.lang.String connected = resources.getString(com.navdy.hud.app.R.string.dial_paired_text);
    private static final java.lang.String disconnected = resources.getString(com.navdy.hud.app.R.string.connection_status_disconnected);
    private static final java.lang.String forgotten = resources.getString(com.navdy.hud.app.R.string.dial_forgotten);
    private static final java.lang.String forgotten_plural = resources.getString(com.navdy.hud.app.R.string.dial_forgotten_multiple);
    private static android.content.res.Resources resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.device.dial.DialNotification.class);

    public static void showConnectedToast() {
        android.os.Bundle bundle = new android.os.Bundle();
        bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_TIMEOUT, 5000);
        bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_IMAGE, com.navdy.hud.app.R.drawable.icon_dial_2);
        bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_SIDE_IMAGE, com.navdy.hud.app.R.drawable.icon_sm_success);
        bundle.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_2, resources.getString(com.navdy.hud.app.R.string.navdy_dial));
        bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_2_STYLE, com.navdy.hud.app.R.style.Glances_1_bold);
        java.lang.String dialName = getDialAddressPart(getDialName());
        if (!android.text.TextUtils.isEmpty(dialName)) {
            bundle.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_3, resources.getString(com.navdy.hud.app.R.string.navdy_dial_name, new java.lang.Object[]{dialName}));
        }
        bundle.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_TTS, com.navdy.hud.app.framework.voice.TTSUtils.TTS_DIAL_CONNECTED);
        showDialToast(DIAL_CONNECT_ID, bundle, true);
    }

    public static void showDisconnectedToast(java.lang.String dialName) {
        com.navdy.hud.app.screen.BaseScreen screen = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getUiStateManager().getCurrentScreen();
        if (screen == null || screen.getScreen() != com.navdy.service.library.events.ui.Screen.SCREEN_DIAL_PAIRING) {
            android.os.Bundle bundle = new android.os.Bundle();
            bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_TIMEOUT, 1000);
            bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_IMAGE, com.navdy.hud.app.R.drawable.icon_dial_forgotten);
            bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_SIDE_IMAGE, com.navdy.hud.app.R.drawable.icon_sm_forgotten);
            bundle.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_2, disconnected);
            bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_2_STYLE, com.navdy.hud.app.R.style.Glances_1);
            if (!android.text.TextUtils.isEmpty(dialName)) {
                bundle.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_3, dialName);
            }
            bundle.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_TTS, com.navdy.hud.app.framework.voice.TTSUtils.TTS_DIAL_DISCONNECTED);
            showDialToast(DIAL_DISCONNECT_ID, bundle, true);
            return;
        }
        sLogger.v("not showing dial disconnected toast");
    }

    public static void showForgottenToast(boolean plural, java.lang.String name) {
        android.os.Bundle bundle = new android.os.Bundle();
        bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_TIMEOUT, 2000);
        bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_IMAGE, com.navdy.hud.app.R.drawable.icon_dial_forgotten);
        bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_SIDE_IMAGE, com.navdy.hud.app.R.drawable.icon_sm_forgotten);
        if (plural) {
            bundle.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_2, forgotten_plural);
        } else {
            bundle.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_2, forgotten);
        }
        java.lang.String dialName = name;
        if (!plural && !android.text.TextUtils.isEmpty(dialName)) {
            bundle.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_3, dialName);
            bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_3_STYLE, com.navdy.hud.app.R.style.title3_single);
        }
        bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_2_STYLE, com.navdy.hud.app.R.style.Glances_1);
        bundle.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_TTS, com.navdy.hud.app.framework.voice.TTSUtils.TTS_DIAL_FORGOTTEN);
        showDialToast(DIAL_FORGOTTEN_ID, bundle, true);
    }

    public static void showLowBatteryToast() {
        android.os.Bundle bundle = new android.os.Bundle();
        bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_TIMEOUT, 2000);
        bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_IMAGE, com.navdy.hud.app.R.drawable.icon_toast_dial_battery_low);
        java.lang.String level = getDialBatteryLevel();
        bundle.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_2, resources.getString(com.navdy.hud.app.R.string.dial_low_battery, new java.lang.Object[]{level}));
        bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_2_STYLE, com.navdy.hud.app.R.style.Glances_1);
        bundle.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_TTS, com.navdy.hud.app.framework.voice.TTSUtils.TTS_DIAL_BATTERY_LOW);
        showDialToast(DIAL_LOW_BATTERY_ID, bundle, true);
    }

    public static void showVeryLowBatteryToast() {
        android.os.Bundle bundle = new android.os.Bundle();
        bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_TIMEOUT, 2000);
        bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_IMAGE, com.navdy.hud.app.R.drawable.icon_toast_dial_battery_low);
        java.lang.String level = getDialBatteryLevel();
        bundle.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_2, resources.getString(com.navdy.hud.app.R.string.dial_low_battery, new java.lang.Object[]{level}));
        bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_2_STYLE, com.navdy.hud.app.R.style.Glances_1);
        bundle.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_TTS, com.navdy.hud.app.framework.voice.TTSUtils.TTS_DIAL_BATTERY_VERY_LOW);
        showDialToast(DIAL_VERY_LOW_BATTERY_ID, bundle, false);
    }

    public static void showExtremelyLowBatteryToast() {
        android.os.Bundle bundle = new android.os.Bundle();
        bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_TIMEOUT, DIAL_BATTERY_TIMEOUT_EXTREMELY_LOW);
        bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_IMAGE, com.navdy.hud.app.R.drawable.icon_toast_dial_battery_very_low);
        java.lang.String level = getDialBatteryLevel();
        bundle.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_2, resources.getString(com.navdy.hud.app.R.string.dial_ex_low_battery, new java.lang.Object[]{level}));
        bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_2_STYLE, com.navdy.hud.app.R.style.Glances_1);
        bundle.putBoolean(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_DEFAULT_CHOICE, true);
        bundle.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_TTS, com.navdy.hud.app.framework.voice.TTSUtils.TTS_DIAL_BATTERY_EXTREMELY_LOW);
        showDialToast(DIAL_EXTREMELY_LOW_BATTERY_ID, bundle, false);
    }

    private static void showDialToast(java.lang.String toastId, android.os.Bundle bundle, boolean removeOnDisable) {
        java.lang.String[] strArr;
        com.navdy.hud.app.framework.toast.ToastManager toastManager = com.navdy.hud.app.framework.toast.ToastManager.getInstance();
        java.lang.String currentToastId = toastManager.getCurrentToastId();
        toastManager.clearPendingToast(DIAL_TOASTS);
        for (java.lang.String s : DIAL_TOASTS) {
            if (!android.text.TextUtils.equals(s, toastId)) {
                toastManager.dismissCurrentToast(s);
            }
        }
        if (!android.text.TextUtils.equals(toastId, currentToastId)) {
            com.navdy.hud.app.framework.toast.ToastManager.getInstance().addToast(new com.navdy.hud.app.framework.toast.ToastManager.ToastParams(toastId, bundle, null, removeOnDisable, false));
        }
    }

    public static void dismissAllBatteryToasts() {
        com.navdy.hud.app.framework.toast.ToastManager toastManager = com.navdy.hud.app.framework.toast.ToastManager.getInstance();
        toastManager.dismissCurrentToast(BATTERY_TOASTS);
        toastManager.clearPendingToast(BATTERY_TOASTS);
    }

    public static java.lang.String getDialName() {
        android.bluetooth.BluetoothDevice device = com.navdy.hud.app.device.dial.DialManager.getInstance().getDialDevice();
        if (device == null) {
            return null;
        }
        java.lang.String dialName = device.getName();
        if (!android.text.TextUtils.isEmpty(dialName)) {
            return dialName;
        }
        java.lang.String dialName2 = device.getAddress();
        int len = dialName2.length();
        if (len > 4) {
            dialName2 = dialName2.substring(len - 4);
        }
        return com.navdy.hud.app.HudApplication.getAppContext().getString(com.navdy.hud.app.R.string.dial_str, new java.lang.Object[]{dialName2});
    }

    public static java.lang.String getDialAddressPart(java.lang.String dialName) {
        if (android.text.TextUtils.isEmpty(dialName)) {
            return dialName;
        }
        int index = dialName.indexOf(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.OPEN_BRACKET);
        if (index < 0) {
            return dialName;
        }
        java.lang.String dialName2 = dialName.substring(index + 1);
        int index2 = dialName2.indexOf(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.CLOSE_BRACKET);
        if (index2 >= 0) {
            return dialName2.substring(0, index2);
        }
        return dialName2;
    }

    public static java.lang.String getDialBatteryLevel() {
        int level = com.navdy.hud.app.device.dial.DialManager.getInstance().getLastKnownBatteryLevel();
        if (level == -1) {
            return battery_unknown;
        }
        return java.lang.String.valueOf(com.navdy.hud.app.device.dial.DialManager.getDisplayBatteryLevel(level));
    }
}
