package com.navdy.hud.app.device.dial;

public class DialManagerHelper {
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = com.navdy.hud.app.device.dial.DialManager.sLogger;

    static class Anon1 implements android.bluetooth.BluetoothProfile.ServiceListener {
        final /* synthetic */ com.navdy.hud.app.device.dial.DialManagerHelper.IDialConnectionStatus val$callBack;
        final /* synthetic */ android.bluetooth.BluetoothDevice val$device;

        /* renamed from: com.navdy.hud.app.device.dial.DialManagerHelper$Anon1$Anon1 reason: collision with other inner class name */
        class C0007Anon1 implements java.lang.Runnable {
            final /* synthetic */ android.bluetooth.BluetoothProfile val$proxy;

            C0007Anon1(android.bluetooth.BluetoothProfile bluetoothProfile) {
                this.val$proxy = bluetoothProfile;
            }

            public void run() {
                try {
                    int state = this.val$proxy.getConnectionState(com.navdy.hud.app.device.dial.DialManagerHelper.Anon1.this.val$device);
                    com.navdy.hud.app.device.dial.DialManagerHelper.sLogger.v("[Dial]Device " + com.navdy.hud.app.device.dial.DialManagerHelper.Anon1.this.val$device.getName() + " state is " + com.navdy.hud.app.bluetooth.utils.BluetoothUtils.getConnectedState(state));
                    if (state == 2) {
                        com.navdy.hud.app.device.dial.DialManagerHelper.sLogger.v("[Dial]already connected");
                        com.navdy.hud.app.device.dial.DialManagerHelper.Anon1.this.val$callBack.onStatus(true);
                        return;
                    }
                    com.navdy.hud.app.device.dial.DialManagerHelper.Anon1.this.val$callBack.onStatus(false);
                } catch (Throwable t) {
                    com.navdy.hud.app.device.dial.DialManagerHelper.sLogger.e("[Dial]", t);
                    com.navdy.hud.app.device.dial.DialManagerHelper.Anon1.this.val$callBack.onStatus(false);
                }
            }
        }

        class Anon2 implements java.lang.Runnable {
            Anon2() {
            }

            public void run() {
                try {
                    com.navdy.hud.app.device.dial.DialManagerHelper.sLogger.v("[Dial]onServiceDisconnected: could not connect proxy");
                    com.navdy.hud.app.device.dial.DialManagerHelper.Anon1.this.val$callBack.onStatus(false);
                } catch (Throwable t) {
                    com.navdy.hud.app.device.dial.DialManagerHelper.sLogger.e("[Dial]", t);
                }
            }
        }

        Anon1(android.bluetooth.BluetoothDevice bluetoothDevice, com.navdy.hud.app.device.dial.DialManagerHelper.IDialConnectionStatus iDialConnectionStatus) {
            this.val$device = bluetoothDevice;
            this.val$callBack = iDialConnectionStatus;
        }

        public void onServiceConnected(int profile, android.bluetooth.BluetoothProfile proxy) {
            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.device.dial.DialManagerHelper.Anon1.C0007Anon1(proxy), 1);
        }

        public void onServiceDisconnected(int profile) {
            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.device.dial.DialManagerHelper.Anon1.Anon2(), 1);
        }
    }

    static class Anon10 implements java.lang.Runnable {
        final /* synthetic */ android.bluetooth.BluetoothDevice val$device;
        final /* synthetic */ java.lang.String val$result;
        final /* synthetic */ boolean val$successful;

        Anon10(java.lang.String str, boolean z, android.bluetooth.BluetoothDevice bluetoothDevice) {
            this.val$result = str;
            this.val$successful = z;
            this.val$device = bluetoothDevice;
        }

        public void run() {
            try {
                java.util.Map<java.lang.String, java.lang.String> map = new java.util.HashMap<>();
                map.put(com.navdy.hud.app.device.dial.DialConstants.DIAL_EVENT_TYPE, com.navdy.hud.app.device.dial.DialConstants.DIAL_EVENT_REBOOT);
                map.put(com.navdy.hud.app.device.dial.DialConstants.DIAL_RESULT, this.val$result);
                map.put(com.navdy.hud.app.device.dial.DialConstants.DIAL_PAIRING_SUCCESS, this.val$successful ? "true" : "false");
                java.lang.String address = null;
                if (this.val$device != null) {
                    address = this.val$device.getAddress();
                }
                map.put(com.navdy.hud.app.device.dial.DialConstants.DIAL_ADDRESS, address);
                com.navdy.hud.app.device.dial.DialManager dialManager = com.navdy.hud.app.device.dial.DialManager.getInstance();
                java.lang.String battery = java.lang.String.valueOf(dialManager.getLastKnownBatteryLevel());
                java.lang.String rawBattery = java.lang.String.valueOf(dialManager.getLastKnownRawBatteryLevel());
                java.lang.String temperature = java.lang.String.valueOf(dialManager.getLastKnownSystemTemperature());
                java.lang.String fw = dialManager.getFirmWareVersion();
                java.lang.String hw = dialManager.getHardwareVersion();
                int incremental = dialManager.getDialFirmwareUpdater().getVersions().dial.incrementalVersion;
                map.put(com.navdy.hud.app.device.dial.DialConstants.DIAL_BATTERY_LEVEL, battery);
                map.put(com.navdy.hud.app.device.dial.DialConstants.DIAL_RAW_BATTERY_LEVEL, rawBattery);
                map.put(com.navdy.hud.app.device.dial.DialConstants.DIAL_TEMPERATURE, temperature);
                map.put(com.navdy.hud.app.device.dial.DialConstants.DIAL_FW_VERSION, fw);
                map.put(com.navdy.hud.app.device.dial.DialConstants.DIAL_HW_VERSION, hw);
                map.put(com.navdy.hud.app.device.dial.DialConstants.DIAL_INCREMENTAL_VERSION, java.lang.Integer.toString(incremental));
                com.navdy.hud.app.device.dial.DialManagerHelper.sLogger.v("sending foundDial[" + this.val$successful + "] battery[" + battery + "] rawBattery[" + rawBattery + "] temperature[" + temperature + "]" + " fw[" + fw + "] hw[" + hw + "] incremental[" + incremental + "] type[" + com.navdy.hud.app.device.dial.DialConstants.DIAL_EVENT_REBOOT + "] result [" + this.val$result + "] address [" + address + "]");
                com.localytics.android.Localytics.tagEvent(com.navdy.hud.app.device.dial.DialConstants.DIAL_PAIRING_EVENT, map);
            } catch (Throwable t) {
                com.navdy.hud.app.device.dial.DialManagerHelper.sLogger.e(t);
            }
        }
    }

    static class Anon11 implements java.lang.Runnable {
        final /* synthetic */ android.bluetooth.BluetoothDevice val$device;
        final /* synthetic */ boolean val$firstTime;
        final /* synthetic */ boolean val$foundDial;
        final /* synthetic */ boolean val$pair;
        final /* synthetic */ java.lang.String val$result;

        Anon11(boolean z, boolean z2, boolean z3, java.lang.String str, android.bluetooth.BluetoothDevice bluetoothDevice) {
            this.val$firstTime = z;
            this.val$foundDial = z2;
            this.val$pair = z3;
            this.val$result = str;
            this.val$device = bluetoothDevice;
        }

        public void run() {
            try {
                java.util.Map<java.lang.String, java.lang.String> map = new java.util.HashMap<>();
                map.put(com.navdy.hud.app.device.dial.DialConstants.DIAL_FIRST_TIME, this.val$firstTime ? "true" : "false");
                map.put(com.navdy.hud.app.device.dial.DialConstants.DIAL_PAIRING_SUCCESS, this.val$foundDial ? "true" : "false");
                java.lang.String type = this.val$pair ? com.navdy.hud.app.device.dial.DialConstants.DIAL_EVENT_PAIR : com.navdy.hud.app.device.dial.DialConstants.DIAL_EVENT_CONNECT;
                map.put(com.navdy.hud.app.device.dial.DialConstants.DIAL_EVENT_TYPE, type);
                map.put(com.navdy.hud.app.device.dial.DialConstants.DIAL_RESULT, this.val$result);
                java.lang.String address = null;
                if (this.val$device != null) {
                    address = this.val$device.getAddress();
                }
                map.put(com.navdy.hud.app.device.dial.DialConstants.DIAL_ADDRESS, address);
                if (this.val$foundDial) {
                    com.navdy.hud.app.device.dial.DialManager dialManager = com.navdy.hud.app.device.dial.DialManager.getInstance();
                    java.lang.String battery = java.lang.String.valueOf(dialManager.getLastKnownBatteryLevel());
                    java.lang.String rawBattery = java.lang.String.valueOf(dialManager.getLastKnownRawBatteryLevel());
                    java.lang.String temperature = java.lang.String.valueOf(dialManager.getLastKnownSystemTemperature());
                    java.lang.String fw = dialManager.getFirmWareVersion();
                    java.lang.String hw = dialManager.getHardwareVersion();
                    int incremental = dialManager.getDialFirmwareUpdater().getVersions().dial.incrementalVersion;
                    map.put(com.navdy.hud.app.device.dial.DialConstants.DIAL_BATTERY_LEVEL, battery);
                    map.put(com.navdy.hud.app.device.dial.DialConstants.DIAL_RAW_BATTERY_LEVEL, rawBattery);
                    map.put(com.navdy.hud.app.device.dial.DialConstants.DIAL_TEMPERATURE, temperature);
                    map.put(com.navdy.hud.app.device.dial.DialConstants.DIAL_FW_VERSION, fw);
                    map.put(com.navdy.hud.app.device.dial.DialConstants.DIAL_HW_VERSION, hw);
                    map.put(com.navdy.hud.app.device.dial.DialConstants.DIAL_INCREMENTAL_VERSION, java.lang.Integer.toString(incremental));
                    com.navdy.hud.app.device.dial.DialManagerHelper.sLogger.v("sending firstTime [" + this.val$firstTime + "] foundDial[" + this.val$foundDial + "] battery[" + battery + "] rawBattery[" + rawBattery + "] temperature[" + temperature + "]" + " fw[" + fw + "] hw[" + hw + "] incremental[" + incremental + "] type[" + type + "] result [" + this.val$result + "] address [" + address + "]");
                } else {
                    com.navdy.hud.app.device.dial.DialManagerHelper.sLogger.v("sending firstTime [" + this.val$firstTime + "] foundDial[" + this.val$foundDial + "] type[" + type + "] result [" + this.val$result + "] address [" + address + "]");
                }
                com.localytics.android.Localytics.tagEvent(com.navdy.hud.app.device.dial.DialConstants.DIAL_PAIRING_EVENT, map);
            } catch (Throwable t) {
                com.navdy.hud.app.device.dial.DialManagerHelper.sLogger.e(t);
            }
        }
    }

    static class Anon2 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.hud.app.device.dial.DialManagerHelper.IDialConnectionStatus val$callBack;

        Anon2(com.navdy.hud.app.device.dial.DialManagerHelper.IDialConnectionStatus iDialConnectionStatus) {
            this.val$callBack = iDialConnectionStatus;
        }

        public void run() {
            this.val$callBack.onStatus(false);
        }
    }

    static class Anon3 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.hud.app.device.dial.DialManagerHelper.IDialConnectionStatus val$callBack;

        Anon3(com.navdy.hud.app.device.dial.DialManagerHelper.IDialConnectionStatus iDialConnectionStatus) {
            this.val$callBack = iDialConnectionStatus;
        }

        public void run() {
            this.val$callBack.onStatus(false);
        }
    }

    static class Anon4 implements android.bluetooth.BluetoothProfile.ServiceListener {
        boolean eventSent = false;
        final /* synthetic */ com.navdy.hud.app.device.dial.DialManagerHelper.IDialConnection val$callBack;
        final /* synthetic */ android.bluetooth.BluetoothDevice val$device;

        class Anon1 implements java.lang.Runnable {
            final /* synthetic */ android.bluetooth.BluetoothProfile val$proxy;

            Anon1(android.bluetooth.BluetoothProfile bluetoothProfile) {
                this.val$proxy = bluetoothProfile;
            }

            public void run() {
                try {
                    int state = this.val$proxy.getConnectionState(com.navdy.hud.app.device.dial.DialManagerHelper.Anon4.this.val$device);
                    com.navdy.hud.app.device.dial.DialManagerHelper.sLogger.v("[Dial]connectToDial Device " + com.navdy.hud.app.device.dial.DialManagerHelper.Anon4.this.val$device.getName() + " state is " + com.navdy.hud.app.bluetooth.utils.BluetoothUtils.getConnectedState(state));
                    if (state == 2) {
                        com.navdy.hud.app.device.dial.DialManagerHelper.sLogger.v("[Dial]connectToDial already connected:" + com.navdy.hud.app.device.dial.DialManagerHelper.Anon4.this.val$device);
                        if (!com.navdy.hud.app.device.dial.DialManagerHelper.Anon4.this.eventSent) {
                            com.navdy.hud.app.device.dial.DialManagerHelper.Anon4.this.eventSent = true;
                            com.navdy.hud.app.device.dial.DialManagerHelper.Anon4.this.val$callBack.onAttemptConnection(true);
                        }
                    } else if (!com.navdy.hud.app.device.dial.DialManagerHelper.forceConnect(this.val$proxy, com.navdy.hud.app.device.dial.DialManagerHelper.Anon4.this.val$device)) {
                        com.navdy.hud.app.device.dial.DialManagerHelper.sLogger.v("[Dial]connectToDial proxy-connect to hid device failed");
                        if (!com.navdy.hud.app.device.dial.DialManagerHelper.Anon4.this.eventSent) {
                            com.navdy.hud.app.device.dial.DialManagerHelper.Anon4.this.eventSent = true;
                            com.navdy.hud.app.device.dial.DialManagerHelper.Anon4.this.val$callBack.onAttemptConnection(false);
                        }
                    } else {
                        com.navdy.hud.app.device.dial.DialManagerHelper.sLogger.v("[Dial]connectToDial proxy-connect to hid device successful");
                        if (!com.navdy.hud.app.device.dial.DialManagerHelper.Anon4.this.eventSent) {
                            com.navdy.hud.app.device.dial.DialManagerHelper.Anon4.this.eventSent = true;
                            com.navdy.hud.app.device.dial.DialManagerHelper.Anon4.this.val$callBack.onAttemptConnection(true);
                        }
                    }
                } catch (Throwable t) {
                    com.navdy.hud.app.device.dial.DialManagerHelper.sLogger.e("[Dial] connectToDial", t);
                    if (!com.navdy.hud.app.device.dial.DialManagerHelper.Anon4.this.eventSent) {
                        com.navdy.hud.app.device.dial.DialManagerHelper.Anon4.this.eventSent = true;
                        com.navdy.hud.app.device.dial.DialManagerHelper.Anon4.this.val$callBack.onAttemptConnection(false);
                    }
                }
            }
        }

        class Anon2 implements java.lang.Runnable {
            Anon2() {
            }

            public void run() {
                try {
                    com.navdy.hud.app.device.dial.DialManagerHelper.sLogger.v("[Dial]connectToDial onServiceDisconnected: could not connect proxy");
                    if (!com.navdy.hud.app.device.dial.DialManagerHelper.Anon4.this.eventSent) {
                        com.navdy.hud.app.device.dial.DialManagerHelper.Anon4.this.eventSent = true;
                        com.navdy.hud.app.device.dial.DialManagerHelper.Anon4.this.val$callBack.onAttemptConnection(false);
                    }
                } catch (Throwable t) {
                    com.navdy.hud.app.device.dial.DialManagerHelper.sLogger.e("[Dial]", t);
                }
            }
        }

        Anon4(android.bluetooth.BluetoothDevice bluetoothDevice, com.navdy.hud.app.device.dial.DialManagerHelper.IDialConnection iDialConnection) {
            this.val$device = bluetoothDevice;
            this.val$callBack = iDialConnection;
        }

        public void onServiceConnected(int profile, android.bluetooth.BluetoothProfile proxy) {
            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.device.dial.DialManagerHelper.Anon4.Anon1(proxy), 1);
        }

        public void onServiceDisconnected(int profile) {
            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.device.dial.DialManagerHelper.Anon4.Anon2(), 1);
        }
    }

    static class Anon5 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.hud.app.device.dial.DialManagerHelper.IDialConnection val$callBack;

        Anon5(com.navdy.hud.app.device.dial.DialManagerHelper.IDialConnection iDialConnection) {
            this.val$callBack = iDialConnection;
        }

        public void run() {
            this.val$callBack.onAttemptConnection(false);
        }
    }

    static class Anon6 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.hud.app.device.dial.DialManagerHelper.IDialConnection val$callBack;

        Anon6(com.navdy.hud.app.device.dial.DialManagerHelper.IDialConnection iDialConnection) {
            this.val$callBack = iDialConnection;
        }

        public void run() {
            this.val$callBack.onAttemptConnection(false);
        }
    }

    static class Anon7 implements android.bluetooth.BluetoothProfile.ServiceListener {
        boolean eventSent = false;
        final /* synthetic */ com.navdy.hud.app.device.dial.DialManagerHelper.IDialConnection val$callBack;
        final /* synthetic */ android.bluetooth.BluetoothDevice val$device;

        class Anon1 implements java.lang.Runnable {
            final /* synthetic */ android.bluetooth.BluetoothProfile val$proxy;

            Anon1(android.bluetooth.BluetoothProfile bluetoothProfile) {
                this.val$proxy = bluetoothProfile;
            }

            public void run() {
                try {
                    int state = this.val$proxy.getConnectionState(com.navdy.hud.app.device.dial.DialManagerHelper.Anon7.this.val$device);
                    com.navdy.hud.app.device.dial.DialManagerHelper.sLogger.v("[Dial]disconnectFromDial Device " + com.navdy.hud.app.device.dial.DialManagerHelper.Anon7.this.val$device.getName() + " state is " + com.navdy.hud.app.bluetooth.utils.BluetoothUtils.getConnectedState(state));
                    if (state != 2) {
                        com.navdy.hud.app.device.dial.DialManagerHelper.sLogger.v("[Dial]disconnectFromDial already disconnected:" + com.navdy.hud.app.device.dial.DialManagerHelper.Anon7.this.val$device);
                        if (!com.navdy.hud.app.device.dial.DialManagerHelper.Anon7.this.eventSent) {
                            com.navdy.hud.app.device.dial.DialManagerHelper.Anon7.this.eventSent = true;
                            com.navdy.hud.app.device.dial.DialManagerHelper.Anon7.this.val$callBack.onAttemptDisconnection(true);
                        }
                    } else if (!com.navdy.hud.app.device.dial.DialManagerHelper.forceDisconnect(this.val$proxy, com.navdy.hud.app.device.dial.DialManagerHelper.Anon7.this.val$device)) {
                        com.navdy.hud.app.device.dial.DialManagerHelper.sLogger.v("[Dial]disconnectFromDial proxy-disconnect from hid device failed");
                        if (!com.navdy.hud.app.device.dial.DialManagerHelper.Anon7.this.eventSent) {
                            com.navdy.hud.app.device.dial.DialManagerHelper.Anon7.this.eventSent = true;
                            com.navdy.hud.app.device.dial.DialManagerHelper.Anon7.this.val$callBack.onAttemptDisconnection(false);
                        }
                    } else {
                        com.navdy.hud.app.device.dial.DialManagerHelper.sLogger.v("[Dial]disconnectFromDial proxy-disconnect from hid device successful");
                        if (!com.navdy.hud.app.device.dial.DialManagerHelper.Anon7.this.eventSent) {
                            com.navdy.hud.app.device.dial.DialManagerHelper.Anon7.this.eventSent = true;
                            com.navdy.hud.app.device.dial.DialManagerHelper.Anon7.this.val$callBack.onAttemptDisconnection(true);
                        }
                    }
                } catch (Throwable t) {
                    com.navdy.hud.app.device.dial.DialManagerHelper.sLogger.e("[Dial] disconnectFromDial", t);
                    if (!com.navdy.hud.app.device.dial.DialManagerHelper.Anon7.this.eventSent) {
                        com.navdy.hud.app.device.dial.DialManagerHelper.Anon7.this.eventSent = true;
                        com.navdy.hud.app.device.dial.DialManagerHelper.Anon7.this.val$callBack.onAttemptDisconnection(false);
                    }
                }
            }
        }

        class Anon2 implements java.lang.Runnable {
            Anon2() {
            }

            public void run() {
                try {
                    com.navdy.hud.app.device.dial.DialManagerHelper.sLogger.v("[Dial]disconnectFromDial onServiceDisconnected: could not connect proxy");
                    if (!com.navdy.hud.app.device.dial.DialManagerHelper.Anon7.this.eventSent) {
                        com.navdy.hud.app.device.dial.DialManagerHelper.Anon7.this.eventSent = true;
                        com.navdy.hud.app.device.dial.DialManagerHelper.Anon7.this.val$callBack.onAttemptDisconnection(false);
                    }
                } catch (Throwable t) {
                    com.navdy.hud.app.device.dial.DialManagerHelper.sLogger.e("[Dial]", t);
                }
            }
        }

        Anon7(android.bluetooth.BluetoothDevice bluetoothDevice, com.navdy.hud.app.device.dial.DialManagerHelper.IDialConnection iDialConnection) {
            this.val$device = bluetoothDevice;
            this.val$callBack = iDialConnection;
        }

        public void onServiceConnected(int profile, android.bluetooth.BluetoothProfile proxy) {
            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.device.dial.DialManagerHelper.Anon7.Anon1(proxy), 1);
        }

        public void onServiceDisconnected(int profile) {
            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.device.dial.DialManagerHelper.Anon7.Anon2(), 1);
        }
    }

    static class Anon8 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.hud.app.device.dial.DialManagerHelper.IDialConnection val$callBack;

        Anon8(com.navdy.hud.app.device.dial.DialManagerHelper.IDialConnection iDialConnection) {
            this.val$callBack = iDialConnection;
        }

        public void run() {
            this.val$callBack.onAttemptDisconnection(false);
        }
    }

    static class Anon9 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.hud.app.device.dial.DialManagerHelper.IDialConnection val$callBack;

        Anon9(com.navdy.hud.app.device.dial.DialManagerHelper.IDialConnection iDialConnection) {
            this.val$callBack = iDialConnection;
        }

        public void run() {
            this.val$callBack.onAttemptDisconnection(false);
        }
    }

    public interface IDialConnection {
        void onAttemptConnection(boolean z);

        void onAttemptDisconnection(boolean z);
    }

    public interface IDialConnectionStatus {
        void onStatus(boolean z);
    }

    public interface IDialForgotten {
        void onForgotten();
    }

    public static void isDialConnected(android.bluetooth.BluetoothAdapter bluetoothAdapter, android.bluetooth.BluetoothDevice device, com.navdy.hud.app.device.dial.DialManagerHelper.IDialConnectionStatus callBack) {
        if (bluetoothAdapter == null || device == null || callBack == null) {
            try {
                throw new java.lang.IllegalArgumentException();
            } catch (Throwable t) {
                sLogger.e("isDialConnected", t);
                com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.device.dial.DialManagerHelper.Anon3(callBack), 1);
            }
        } else {
            boolean ret = bluetoothAdapter.getProfileProxy(com.navdy.hud.app.HudApplication.getAppContext(), new com.navdy.hud.app.device.dial.DialManagerHelper.Anon1(device, callBack), 4);
            sLogger.v("[Dial]getProfileProxy returned:" + ret);
            if (!ret) {
                com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.device.dial.DialManagerHelper.Anon2(callBack), 1);
            }
        }
    }

    public static void connectToDial(android.bluetooth.BluetoothAdapter bluetoothAdapter, android.bluetooth.BluetoothDevice device, com.navdy.hud.app.device.dial.DialManagerHelper.IDialConnection callBack) {
        if (bluetoothAdapter == null || device == null || callBack == null) {
            try {
                throw new java.lang.IllegalArgumentException();
            } catch (Throwable t) {
                sLogger.e("connectToDial", t);
                com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.device.dial.DialManagerHelper.Anon6(callBack), 1);
            }
        } else {
            boolean ret = bluetoothAdapter.getProfileProxy(com.navdy.hud.app.HudApplication.getAppContext(), new com.navdy.hud.app.device.dial.DialManagerHelper.Anon4(device, callBack), 4);
            sLogger.v("[Dial]connectToDial getProfileProxy returned:" + ret);
            if (!ret) {
                com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.device.dial.DialManagerHelper.Anon5(callBack), 1);
            }
        }
    }

    public static void disconnectFromDial(android.bluetooth.BluetoothAdapter bluetoothAdapter, android.bluetooth.BluetoothDevice device, com.navdy.hud.app.device.dial.DialManagerHelper.IDialConnection callBack) {
        if (bluetoothAdapter == null || device == null || callBack == null) {
            try {
                throw new java.lang.IllegalArgumentException();
            } catch (Throwable t) {
                sLogger.e("disconnectFromDial", t);
                com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.device.dial.DialManagerHelper.Anon9(callBack), 1);
            }
        } else {
            boolean ret = bluetoothAdapter.getProfileProxy(com.navdy.hud.app.HudApplication.getAppContext(), new com.navdy.hud.app.device.dial.DialManagerHelper.Anon7(device, callBack), 4);
            sLogger.v("[Dial]disconnectFromDial getProfileProxy returned:" + ret);
            if (!ret) {
                com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.device.dial.DialManagerHelper.Anon8(callBack), 1);
            }
        }
    }

    /* access modifiers changed from: private */
    public static boolean forceConnect(android.bluetooth.BluetoothProfile proxy, android.bluetooth.BluetoothDevice device) {
        return invokeMethod(proxy, device, "connect");
    }

    /* access modifiers changed from: private */
    public static boolean forceDisconnect(android.bluetooth.BluetoothProfile proxy, android.bluetooth.BluetoothDevice device) {
        return invokeMethod(proxy, device, "disconnect");
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    private static boolean invokeMethod(android.bluetooth.BluetoothProfile proxy, android.bluetooth.BluetoothDevice device, java.lang.String methodName) {
        java.lang.Boolean returnValue;
        try {
            java.lang.Boolean valueOf = java.lang.Boolean.valueOf(false);
            returnValue = (java.lang.Boolean) java.lang.Class.forName("android.bluetooth.BluetoothInputDevice").getMethod(methodName, new java.lang.Class[]{android.bluetooth.BluetoothDevice.class}).invoke(proxy, new java.lang.Object[]{device});
        } catch (Throwable t) {
            sLogger.e("[Dial]", t);
            return false;
        }
        return returnValue.booleanValue();
    }

    public static void sendRebootLocalyticsEvent(android.os.Handler handler, boolean successful, android.bluetooth.BluetoothDevice device, java.lang.String result) {
        handler.post(new com.navdy.hud.app.device.dial.DialManagerHelper.Anon10(result, successful, device));
    }

    public static void sendLocalyticsEvent(android.os.Handler handler, boolean firstTime, boolean foundDial, int delay, boolean pair) {
        sendLocalyticsEvent(handler, firstTime, foundDial, delay, pair, null);
    }

    public static void sendLocalyticsEvent(android.os.Handler handler, boolean firstTime, boolean foundDial, int delay, boolean pair, java.lang.String result) {
        long j;
        com.navdy.hud.app.device.dial.DialManagerHelper.Anon11 anon11 = new com.navdy.hud.app.device.dial.DialManagerHelper.Anon11(firstTime, foundDial, pair, result, com.navdy.hud.app.device.dial.DialManager.getInstance().getDialDevice());
        if (foundDial) {
            j = (long) delay;
        } else {
            j = 0;
        }
        handler.postDelayed(anon11, j);
    }
}
