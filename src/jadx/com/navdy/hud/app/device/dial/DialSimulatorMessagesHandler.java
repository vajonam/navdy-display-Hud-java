package com.navdy.hud.app.device.dial;

public class DialSimulatorMessagesHandler {
    private static final int LONG_PRESS_KEY_UP_FLAGS_SIMULATION = 640;
    /* access modifiers changed from: private */
    public static final java.util.Map<com.navdy.service.library.events.input.DialSimulationEvent.DialInputAction, java.lang.Integer> SIMULATION_TO_KEY_EVENT = new com.navdy.hud.app.device.dial.DialSimulatorMessagesHandler.Anon1();
    /* access modifiers changed from: private */
    public int longPressTimeThreshold = android.view.ViewConfiguration.getLongPressTimeout();

    static class Anon1 extends java.util.HashMap<com.navdy.service.library.events.input.DialSimulationEvent.DialInputAction, java.lang.Integer> {
        Anon1() {
            put(com.navdy.service.library.events.input.DialSimulationEvent.DialInputAction.DIAL_LEFT_TURN, java.lang.Integer.valueOf(21));
            put(com.navdy.service.library.events.input.DialSimulationEvent.DialInputAction.DIAL_RIGHT_TURN, java.lang.Integer.valueOf(22));
            put(com.navdy.service.library.events.input.DialSimulationEvent.DialInputAction.DIAL_CLICK, java.lang.Integer.valueOf(66));
        }
    }

    private class SimulatedKeyEventRunnable implements java.lang.Runnable {
        private com.navdy.service.library.events.input.DialSimulationEvent.DialInputAction eventAction;

        public SimulatedKeyEventRunnable(com.navdy.service.library.events.input.DialSimulationEvent.DialInputAction eventAction2) {
            this.eventAction = eventAction2;
        }

        public void run() {
            android.app.Instrumentation instrumentation = new android.app.Instrumentation();
            if (this.eventAction == com.navdy.service.library.events.input.DialSimulationEvent.DialInputAction.DIAL_LONG_CLICK) {
                android.view.KeyEvent event = new android.view.KeyEvent(0, 66);
                int initFlags = event.getFlags();
                android.view.KeyEvent event2 = android.view.KeyEvent.changeTimeRepeat(event, event.getEventTime(), 1, initFlags | 128);
                instrumentation.sendKeySync(event2);
                instrumentation.sendKeySync(android.view.KeyEvent.changeAction(android.view.KeyEvent.changeTimeRepeat(event2, event2.getEventTime() + ((long) com.navdy.hud.app.device.dial.DialSimulatorMessagesHandler.this.longPressTimeThreshold), 0, initFlags | com.navdy.hud.app.device.dial.DialSimulatorMessagesHandler.LONG_PRESS_KEY_UP_FLAGS_SIMULATION), 1));
                return;
            }
            instrumentation.sendKeyDownUpSync(((java.lang.Integer) com.navdy.hud.app.device.dial.DialSimulatorMessagesHandler.SIMULATION_TO_KEY_EVENT.get(this.eventAction)).intValue());
        }
    }

    public DialSimulatorMessagesHandler(android.content.Context context) {
        android.view.ViewConfiguration.get(context);
    }

    @com.squareup.otto.Subscribe
    public void onDialSimulationEvent(com.navdy.service.library.events.input.DialSimulationEvent event) {
        if (event != null) {
            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.device.dial.DialSimulatorMessagesHandler.SimulatedKeyEventRunnable(event.dialAction), 1);
        }
    }
}
