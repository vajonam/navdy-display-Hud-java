package com.navdy.hud.app.device.dial;

public class DialManager {
    private static final java.lang.String BALANCED = "balanced";
    private static final com.navdy.hud.app.device.dial.DialConstants.DialConnectionStatus DIAL_CONNECTED = new com.navdy.hud.app.device.dial.DialConstants.DialConnectionStatus(com.navdy.hud.app.device.dial.DialConstants.DialConnectionStatus.Status.CONNECTED);
    private static final com.navdy.hud.app.device.dial.DialConstants.DialConnectionStatus DIAL_CONNECTING = new com.navdy.hud.app.device.dial.DialConstants.DialConnectionStatus(com.navdy.hud.app.device.dial.DialConstants.DialConnectionStatus.Status.CONNECTING);
    /* access modifiers changed from: private */
    public static final com.navdy.hud.app.device.dial.DialConstants.DialConnectionStatus DIAL_CONNECTION_FAILED = new com.navdy.hud.app.device.dial.DialConstants.DialConnectionStatus(com.navdy.hud.app.device.dial.DialConstants.DialConnectionStatus.Status.CONNECTION_FAILED);
    /* access modifiers changed from: private */
    public static final com.navdy.hud.app.device.dial.DialConstants.DialConnectionStatus DIAL_NOT_CONNECTED = new com.navdy.hud.app.device.dial.DialConstants.DialConnectionStatus(com.navdy.hud.app.device.dial.DialConstants.DialConnectionStatus.Status.DISCONNECTED);
    private static final com.navdy.hud.app.device.dial.DialConstants.DialConnectionStatus DIAL_NOT_FOUND = new com.navdy.hud.app.device.dial.DialConstants.DialConnectionStatus(com.navdy.hud.app.device.dial.DialConstants.DialConnectionStatus.Status.NO_DIAL_FOUND);
    private static final java.lang.String DIAL_POWER_SETTING = "persist.sys.bt.dial.pwr";
    private static final java.lang.String HIGH_POWER = "high";
    private static final java.lang.String LOW_POWER = "low";
    /* access modifiers changed from: private */
    public static final java.util.Map<java.lang.String, java.lang.Integer> PowerMap = new java.util.HashMap();
    static final java.lang.String TAG_DIAL = "[Dial]";
    static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.device.dial.DialManager.class);
    private static final com.navdy.hud.app.device.dial.DialManager singleton = new com.navdy.hud.app.device.dial.DialManager();
    /* access modifiers changed from: private */
    public android.bluetooth.BluetoothGattCharacteristic batteryCharateristic;
    /* access modifiers changed from: private */
    public java.lang.Runnable batteryReadRunnable = new com.navdy.hud.app.device.dial.DialManager.Anon10();
    /* access modifiers changed from: private */
    public android.bluetooth.BluetoothAdapter bluetoothAdapter;
    private android.content.BroadcastReceiver bluetoothBondingReceiver = new com.navdy.hud.app.device.dial.DialManager.Anon6();
    private android.content.BroadcastReceiver bluetoothConnectivityReceiver = new com.navdy.hud.app.device.dial.DialManager.Anon9();
    private android.bluetooth.BluetoothGatt bluetoothGatt;
    private android.bluetooth.le.BluetoothLeScanner bluetoothLEScanner;
    private android.bluetooth.BluetoothManager bluetoothManager;
    private android.content.BroadcastReceiver bluetoothOnOffReceiver = new com.navdy.hud.app.device.dial.DialManager.Anon7();
    /* access modifiers changed from: private */
    public java.lang.Runnable bondHangRunnable = new com.navdy.hud.app.device.dial.DialManager.Anon2();
    /* access modifiers changed from: private */
    public int bondTries;
    /* access modifiers changed from: private */
    public java.util.ArrayList<android.bluetooth.BluetoothDevice> bondedDials = new java.util.ArrayList<>();
    /* access modifiers changed from: private */
    public android.bluetooth.BluetoothDevice bondingDial;
    /* access modifiers changed from: private */
    public com.squareup.otto.Bus bus;
    /* access modifiers changed from: private */
    public volatile android.bluetooth.BluetoothDevice connectedDial;
    private java.lang.Runnable connectionErrorRunnable = new com.navdy.hud.app.device.dial.DialManager.Anon8();
    /* access modifiers changed from: private */
    public java.lang.Runnable deviceInfoGattReadRunnable = new com.navdy.hud.app.device.dial.DialManager.Anon11();
    private java.lang.Thread dialEventsListenerThread;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.device.dial.DialFirmwareUpdater dialFirmwareUpdater;
    /* access modifiers changed from: private */
    public android.bluetooth.BluetoothGattCharacteristic dialForgetKeysCharacteristic;
    /* access modifiers changed from: private */
    public volatile boolean dialManagerInitialized;
    /* access modifiers changed from: private */
    public android.bluetooth.BluetoothGattCharacteristic dialRebootCharacteristic;
    /* access modifiers changed from: private */
    public java.lang.String disconnectedDial;
    private int encryptionRetryCount;
    /* access modifiers changed from: private */
    public java.lang.String firmWareVersion;
    /* access modifiers changed from: private */
    public android.bluetooth.BluetoothGattCharacteristic firmwareVersionCharacteristic;
    private android.bluetooth.BluetoothGattCallback gattCallback = new com.navdy.hud.app.device.dial.DialManager.Anon13();
    /* access modifiers changed from: private */
    public com.navdy.hud.app.device.dial.DialManager.CharacteristicCommandProcessor gattCharacteristicProcessor;
    /* access modifiers changed from: private */
    public android.os.Handler handler;
    private android.os.HandlerThread handlerThread;
    /* access modifiers changed from: private */
    public java.lang.String hardwareVersion;
    /* access modifiers changed from: private */
    public android.bluetooth.BluetoothGattCharacteristic hardwareVersionCharacteristic;
    /* access modifiers changed from: private */
    public android.bluetooth.BluetoothGattCharacteristic hidHostReadyCharacteristic;
    /* access modifiers changed from: private */
    public java.lang.Runnable hidHostReadyReadRunnable = new com.navdy.hud.app.device.dial.DialManager.Anon12();
    private java.util.Set<android.bluetooth.BluetoothDevice> ignoredNonNavdyDials = new java.util.HashSet();
    /* access modifiers changed from: private */
    public volatile boolean isGattOn;
    private volatile boolean isScanning;
    private int lastKnownBatteryLevel = -1;
    private java.lang.Integer lastKnownRawBatteryLevel;
    private java.lang.Integer lastKnownSystemTemperature;
    private android.bluetooth.le.ScanCallback leScanCallback = new com.navdy.hud.app.device.dial.DialManager.Anon4();
    private com.navdy.hud.app.device.dial.DialConstants.NotificationReason notificationReasonBattery = com.navdy.hud.app.device.dial.DialConstants.NotificationReason.OK_BATTERY;
    /* access modifiers changed from: private */
    public android.bluetooth.BluetoothGattCharacteristic rawBatteryCharateristic;
    private java.lang.Runnable scanHangRunnable = new com.navdy.hud.app.device.dial.DialManager.Anon1();
    private java.util.Map<android.bluetooth.BluetoothDevice, android.bluetooth.le.ScanRecord> scannedNavdyDials = new java.util.LinkedHashMap();
    /* access modifiers changed from: private */
    public java.lang.Runnable sendLocalyticsSuccessRunnable = new com.navdy.hud.app.device.dial.DialManager.Anon3();
    /* access modifiers changed from: private */
    public android.content.SharedPreferences sharedPreferences;
    private java.lang.Runnable stopScanRunnable = new com.navdy.hud.app.device.dial.DialManager.Anon5();
    /* access modifiers changed from: private */
    public volatile android.bluetooth.BluetoothDevice tempConnectedDial;
    /* access modifiers changed from: private */
    public android.bluetooth.BluetoothGattCharacteristic temperatureCharateristic;

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            try {
                com.navdy.hud.app.device.dial.DialManager.sLogger.v("scan hang runnable");
                com.navdy.hud.app.device.dial.DialManager.this.stopScan(true);
            } catch (Throwable t) {
                com.navdy.hud.app.device.dial.DialManager.sLogger.e(com.navdy.hud.app.device.dial.DialManager.TAG_DIAL, t);
            }
        }
    }

    class Anon10 implements java.lang.Runnable {
        Anon10() {
        }

        public void run() {
            try {
                if (com.navdy.hud.app.device.dial.DialManager.this.batteryCharateristic != null) {
                    com.navdy.hud.app.device.dial.DialManager.this.queueRead(com.navdy.hud.app.device.dial.DialManager.this.batteryCharateristic);
                }
                if (com.navdy.hud.app.device.dial.DialManager.this.rawBatteryCharateristic != null) {
                    com.navdy.hud.app.device.dial.DialManager.this.queueRead(com.navdy.hud.app.device.dial.DialManager.this.rawBatteryCharateristic);
                }
                if (com.navdy.hud.app.device.dial.DialManager.this.temperatureCharateristic != null) {
                    com.navdy.hud.app.device.dial.DialManager.this.queueRead(com.navdy.hud.app.device.dial.DialManager.this.temperatureCharateristic);
                }
                com.navdy.hud.app.device.dial.DialManager.this.handler.postDelayed(this, 300000);
            } catch (Throwable t) {
                com.navdy.hud.app.device.dial.DialManager.sLogger.e(com.navdy.hud.app.device.dial.DialManager.TAG_DIAL, t);
            }
        }
    }

    class Anon11 implements java.lang.Runnable {
        Anon11() {
        }

        public void run() {
            try {
                if (com.navdy.hud.app.device.dial.DialManager.this.firmwareVersionCharacteristic != null) {
                    com.navdy.hud.app.device.dial.DialManager.this.queueRead(com.navdy.hud.app.device.dial.DialManager.this.firmwareVersionCharacteristic);
                }
                if (com.navdy.hud.app.device.dial.DialManager.this.hardwareVersionCharacteristic != null) {
                    com.navdy.hud.app.device.dial.DialManager.this.queueRead(com.navdy.hud.app.device.dial.DialManager.this.hardwareVersionCharacteristic);
                }
            } catch (Throwable t) {
                com.navdy.hud.app.device.dial.DialManager.sLogger.e(com.navdy.hud.app.device.dial.DialManager.TAG_DIAL, t);
            }
        }
    }

    class Anon12 implements java.lang.Runnable {
        Anon12() {
        }

        public void run() {
            try {
                if (com.navdy.hud.app.device.dial.DialManager.this.hidHostReadyCharacteristic != null) {
                    com.navdy.hud.app.device.dial.DialManager.sLogger.v("queuing hid host ready characteristic");
                    com.navdy.hud.app.device.dial.DialManager.this.queueRead(com.navdy.hud.app.device.dial.DialManager.this.hidHostReadyCharacteristic);
                    return;
                }
                com.navdy.hud.app.device.dial.DialManager.sLogger.v("not queuing hid host ready characteristic: null");
            } catch (Throwable t) {
                com.navdy.hud.app.device.dial.DialManager.sLogger.e(com.navdy.hud.app.device.dial.DialManager.TAG_DIAL, t);
            }
        }
    }

    class Anon13 extends android.bluetooth.BluetoothGattCallback {
        Anon13() {
        }

        public void onConnectionStateChange(android.bluetooth.BluetoothGatt gatt, int status, int newState) {
            java.lang.Integer powerLevel = null;
            try {
                com.navdy.hud.app.device.dial.DialManager.sLogger.v("[Dial]gatt state change:" + status + " newState=" + newState);
                if (newState == 2) {
                    if (gatt != null) {
                        if (com.navdy.hud.app.util.DeviceUtil.isNavdyDevice()) {
                            java.lang.String powerSetting = com.navdy.hud.app.util.os.SystemProperties.get(com.navdy.hud.app.device.dial.DialManager.DIAL_POWER_SETTING);
                            if (powerSetting != null) {
                                powerLevel = (java.lang.Integer) com.navdy.hud.app.device.dial.DialManager.PowerMap.get(powerSetting);
                            }
                            if (powerLevel != null) {
                                com.navdy.hud.app.device.dial.DialManager.sLogger.v("[Dial] setting BT power to " + powerSetting);
                                gatt.requestConnectionPriority(powerLevel.intValue());
                            }
                        }
                        com.navdy.hud.app.device.dial.DialManager.this.gattCharacteristicProcessor = new com.navdy.hud.app.device.dial.DialManager.CharacteristicCommandProcessor(gatt, com.navdy.hud.app.device.dial.DialManager.this.handler);
                        com.navdy.hud.app.device.dial.DialManager.sLogger.v("[Dial]gatt connected, discover");
                        gatt.discoverServices();
                        return;
                    }
                    com.navdy.hud.app.device.dial.DialManager.sLogger.v("[Dial]gatt is null, cannot discover");
                } else if (newState == 0) {
                    com.navdy.hud.app.device.dial.DialManager.sLogger.v("[Dial]gatt disconnected");
                    if (com.navdy.hud.app.device.dial.DialManager.this.gattCharacteristicProcessor != null) {
                        com.navdy.hud.app.device.dial.DialManager.this.gattCharacteristicProcessor.release();
                        com.navdy.hud.app.device.dial.DialManager.this.gattCharacteristicProcessor = null;
                    }
                    com.navdy.hud.app.device.dial.DialManager.this.stopGATTClient();
                }
            } catch (Throwable t) {
                com.navdy.hud.app.device.dial.DialManager.sLogger.e(com.navdy.hud.app.device.dial.DialManager.TAG_DIAL, t);
            }
        }

        public void onServicesDiscovered(android.bluetooth.BluetoothGatt gatt, int status) {
            com.navdy.hud.app.device.dial.DialManager.this.hardwareVersionCharacteristic = null;
            com.navdy.hud.app.device.dial.DialManager.this.firmwareVersionCharacteristic = null;
            com.navdy.hud.app.device.dial.DialManager.this.hidHostReadyCharacteristic = null;
            com.navdy.hud.app.device.dial.DialManager.this.dialForgetKeysCharacteristic = null;
            com.navdy.hud.app.device.dial.DialManager.this.dialRebootCharacteristic = null;
            com.navdy.hud.app.device.dial.DialManager.this.batteryCharateristic = null;
            com.navdy.hud.app.device.dial.DialManager.this.rawBatteryCharateristic = null;
            com.navdy.hud.app.device.dial.DialManager.this.temperatureCharateristic = null;
            try {
                if (!com.navdy.hud.app.device.dial.DialManager.this.isGattOn) {
                    com.navdy.hud.app.device.dial.DialManager.sLogger.w("gatt is not on but onServicesDiscovered called");
                } else if (status == 0) {
                    com.navdy.hud.app.device.dial.DialManager.sLogger.i("[Dial]onServicesDiscovered");
                    com.navdy.hud.app.device.dial.DialManager.this.dialFirmwareUpdater.onServicesDiscovered(gatt);
                    for (android.bluetooth.BluetoothGattService service : gatt.getServices()) {
                        java.util.UUID uuid = service.getUuid();
                        if (com.navdy.hud.app.device.dial.DialConstants.HID_SERVICE_UUID.equals(uuid)) {
                            com.navdy.hud.app.device.dial.DialManager.sLogger.d("Hid service found " + uuid);
                            com.navdy.hud.app.device.dial.DialManager.this.hidHostReadyCharacteristic = service.getCharacteristic(com.navdy.hud.app.device.dial.DialConstants.HID_HOST_READY_UUID);
                            if (com.navdy.hud.app.device.dial.DialManager.this.hidHostReadyCharacteristic == null) {
                                com.navdy.hud.app.device.dial.DialManager.sLogger.e("Hid host ready Characteristic is null");
                            } else {
                                com.navdy.hud.app.device.dial.DialManager.sLogger.v("Found Hid host ready Characteristic");
                                com.navdy.hud.app.device.dial.DialManager.this.sharedPreferences.edit().putBoolean(com.navdy.hud.app.device.dial.DialConstants.VIDEO_SHOWN_PREF, true).apply();
                            }
                            com.navdy.hud.app.device.dial.DialManager.this.handler.post(com.navdy.hud.app.device.dial.DialManager.this.hidHostReadyReadRunnable);
                        } else if (com.navdy.hud.app.device.dial.DialConstants.BATTERY_SERVICE_UUID.equals(uuid)) {
                            com.navdy.hud.app.device.dial.DialManager.sLogger.i("[Dial]Service: " + service.getUuid().toString());
                            java.util.List<android.bluetooth.BluetoothGattCharacteristic> characteristics = service.getCharacteristics();
                            if (characteristics == null) {
                                com.navdy.hud.app.device.dial.DialManager.sLogger.i("[Dial]No characteristic");
                                return;
                            }
                            for (android.bluetooth.BluetoothGattCharacteristic characteristic : characteristics) {
                                if (com.navdy.hud.app.device.dial.DialConstants.BATTERY_LEVEL_UUID.equals(characteristic.getUuid())) {
                                    com.navdy.hud.app.device.dial.DialManager.this.batteryCharateristic = characteristic;
                                } else if (com.navdy.hud.app.device.dial.DialConstants.RAW_BATTERY_LEVEL_UUID.equals(characteristic.getUuid())) {
                                    com.navdy.hud.app.device.dial.DialManager.this.rawBatteryCharateristic = characteristic;
                                } else if (com.navdy.hud.app.device.dial.DialConstants.SYSTEM_TEMPERATURE_UUID.equals(characteristic.getUuid())) {
                                    com.navdy.hud.app.device.dial.DialManager.this.temperatureCharateristic = characteristic;
                                }
                            }
                            if (com.navdy.hud.app.device.dial.DialManager.this.batteryCharateristic != null) {
                                com.navdy.hud.app.device.dial.DialManager.this.handler.postDelayed(com.navdy.hud.app.device.dial.DialManager.this.batteryReadRunnable, 5000);
                                com.navdy.hud.app.device.dial.DialManager.this.handler.postDelayed(com.navdy.hud.app.device.dial.DialManager.this.sendLocalyticsSuccessRunnable, 10000);
                            } else {
                                com.navdy.hud.app.device.dial.DialManager.sLogger.v("[Dial] battery characteristic not found");
                            }
                        } else if (com.navdy.hud.app.device.dial.DialConstants.DEVICE_INFO_SERVICE_UUID.equals(uuid)) {
                            com.navdy.hud.app.device.dial.DialManager.sLogger.d("Device info service found " + uuid);
                            com.navdy.hud.app.device.dial.DialManager.this.firmwareVersionCharacteristic = service.getCharacteristic(com.navdy.hud.app.device.dial.DialConstants.DEVICE_INFO_FIRMWARE_VERSION_UUID);
                            if (com.navdy.hud.app.device.dial.DialManager.this.firmwareVersionCharacteristic == null) {
                                com.navdy.hud.app.device.dial.DialManager.sLogger.e("Firmware Version Characteristic is null");
                            }
                            com.navdy.hud.app.device.dial.DialManager.this.hardwareVersionCharacteristic = service.getCharacteristic(com.navdy.hud.app.device.dial.DialConstants.DEVICE_INFO_HARDWARE_VERSION_UUID);
                            if (com.navdy.hud.app.device.dial.DialManager.this.hardwareVersionCharacteristic == null) {
                                com.navdy.hud.app.device.dial.DialManager.sLogger.e("Hardware Version Characteristic is null");
                            }
                            com.navdy.hud.app.device.dial.DialManager.this.handler.postDelayed(com.navdy.hud.app.device.dial.DialManager.this.deviceInfoGattReadRunnable, 5000);
                        } else if (com.navdy.hud.app.device.dial.DialConstants.OTA_SERVICE_UUID.equals(uuid)) {
                            com.navdy.hud.app.device.dial.DialManager.this.dialForgetKeysCharacteristic = service.getCharacteristic(com.navdy.hud.app.device.dial.DialConstants.DIAL_FORGET_KEYS_UUID);
                            if (com.navdy.hud.app.device.dial.DialManager.this.dialForgetKeysCharacteristic == null) {
                                com.navdy.hud.app.device.dial.DialManager.sLogger.d("Dial key forget is null");
                            }
                            com.navdy.hud.app.device.dial.DialManager.this.dialRebootCharacteristic = service.getCharacteristic(com.navdy.hud.app.device.dial.DialConstants.DIAL_REBOOT_UUID);
                            if (com.navdy.hud.app.device.dial.DialManager.this.dialRebootCharacteristic == null) {
                                com.navdy.hud.app.device.dial.DialManager.sLogger.d("Dial reboot is null");
                            }
                        } else {
                            com.navdy.hud.app.device.dial.DialManager.sLogger.v("[Dial]Ignoring service:" + uuid);
                        }
                    }
                } else {
                    com.navdy.hud.app.device.dial.DialManager.sLogger.e("[Dial]onServicesDiscovered error:" + status);
                }
            } catch (Throwable t) {
                com.navdy.hud.app.device.dial.DialManager.sLogger.e(com.navdy.hud.app.device.dial.DialManager.TAG_DIAL, t);
            }
        }

        public void onCharacteristicRead(android.bluetooth.BluetoothGatt gatt, android.bluetooth.BluetoothGattCharacteristic characteristic, int status) {
            com.navdy.hud.app.device.dial.DialManager.this.dialFirmwareUpdater.onCharacteristicRead(characteristic, status);
            if (status == 0) {
                print(characteristic);
            } else {
                com.navdy.hud.app.device.dial.DialManager.sLogger.v("[Dial]onCharacteristicRead fail:" + status);
            }
            com.navdy.hud.app.device.dial.DialManager.sLogger.d("onCharacteristicRead finished");
            if (com.navdy.hud.app.device.dial.DialManager.this.gattCharacteristicProcessor != null) {
                com.navdy.hud.app.device.dial.DialManager.this.gattCharacteristicProcessor.commandFinished();
            }
        }

        public void onCharacteristicWrite(android.bluetooth.BluetoothGatt gatt, android.bluetooth.BluetoothGattCharacteristic characteristic, int status) {
            com.navdy.hud.app.device.dial.DialManager.this.dialFirmwareUpdater.onCharacteristicWrite(gatt, characteristic, status);
            if (characteristic == com.navdy.hud.app.device.dial.DialManager.this.dialForgetKeysCharacteristic) {
                com.navdy.hud.app.device.dial.DialManager.this.forgetDial(gatt.getDevice());
            }
            if (characteristic == com.navdy.hud.app.device.dial.DialManager.this.dialRebootCharacteristic) {
                boolean successful = status == 0;
                if (successful) {
                    com.navdy.hud.app.device.dial.DialManager.this.sharedPreferences.edit().putLong(com.navdy.hud.app.device.dial.DialConstants.DIAL_REBOOT_PROPERTY, java.lang.System.currentTimeMillis()).apply();
                }
                com.navdy.hud.app.device.dial.DialManagerHelper.sendRebootLocalyticsEvent(com.navdy.hud.app.device.dial.DialManager.this.handler, successful, gatt.getDevice(), null);
            }
            if (com.navdy.hud.app.device.dial.DialManager.this.gattCharacteristicProcessor != null) {
                com.navdy.hud.app.device.dial.DialManager.this.gattCharacteristicProcessor.commandFinished();
            }
        }

        public void onCharacteristicChanged(android.bluetooth.BluetoothGatt gatt, android.bluetooth.BluetoothGattCharacteristic characteristic) {
            com.navdy.hud.app.device.dial.DialManager.sLogger.v("[Dial]onCharacteristicChanged");
            print(characteristic);
        }

        public void onDescriptorRead(android.bluetooth.BluetoothGatt gatt, android.bluetooth.BluetoothGattDescriptor descriptor, int status) {
            com.navdy.hud.app.device.dial.DialManager.sLogger.v("[Dial]onDescriptorRead");
        }

        public void onDescriptorWrite(android.bluetooth.BluetoothGatt gatt, android.bluetooth.BluetoothGattDescriptor descriptor, int status) {
            com.navdy.hud.app.device.dial.DialManager.sLogger.v("[Dial]onDescriptorWrite : " + (status == 0 ? net.hockeyapp.android.tasks.LoginTask.BUNDLE_SUCCESS : "fail"));
        }

        /* access modifiers changed from: 0000 */
        public void print(android.bluetooth.BluetoothGattCharacteristic characteristic) {
            if (com.navdy.hud.app.device.dial.DialConstants.HID_HOST_READY_UUID.equals(characteristic.getUuid())) {
                com.navdy.hud.app.device.dial.DialManager.sLogger.v("[Dial]onCharacteristicRead hid host ready, length " + characteristic.getValue().length);
            } else if (com.navdy.hud.app.device.dial.DialConstants.BATTERY_LEVEL_UUID.equals(characteristic.getUuid())) {
                com.navdy.hud.app.device.dial.DialManager.sLogger.v("[Dial]onCharacteristicRead battery");
                java.lang.Integer level = characteristic.getIntValue(33, 0);
                if (level != null) {
                    com.navdy.hud.app.device.dial.DialManager.sLogger.v("[Dial]battery level is [" + level + "] %");
                    com.navdy.hud.app.device.dial.DialManager.this.processBatteryLevel(level.intValue());
                    return;
                }
                com.navdy.hud.app.device.dial.DialManager.sLogger.v("[Dial]battery level no data");
            } else if (com.navdy.hud.app.device.dial.DialConstants.RAW_BATTERY_LEVEL_UUID.equals(characteristic.getUuid())) {
                com.navdy.hud.app.device.dial.DialManager.sLogger.v("[Dial]onCharacteristicRead rawBattery");
                java.lang.Integer level2 = characteristic.getIntValue(34, 0);
                if (level2 != null) {
                    com.navdy.hud.app.device.dial.DialManager.sLogger.v("[Dial]raw battery level is [" + level2 + "]");
                    com.navdy.hud.app.device.dial.DialManager.this.processRawBatteryLevel(level2);
                    return;
                }
                com.navdy.hud.app.device.dial.DialManager.sLogger.v("[Dial]raw battery level no data");
            } else if (com.navdy.hud.app.device.dial.DialConstants.SYSTEM_TEMPERATURE_UUID.equals(characteristic.getUuid())) {
                com.navdy.hud.app.device.dial.DialManager.sLogger.v("[Dial]onCharacteristicRead temperature");
                java.lang.Integer level3 = characteristic.getIntValue(34, 0);
                if (level3 != null) {
                    com.navdy.hud.app.device.dial.DialManager.sLogger.v("[Dial]system temperature is [" + level3 + "]");
                    com.navdy.hud.app.device.dial.DialManager.this.processSystemTemperature(level3);
                    return;
                }
                com.navdy.hud.app.device.dial.DialManager.sLogger.v("[Dial]system temperature no data");
            } else if (com.navdy.hud.app.device.dial.DialConstants.DEVICE_INFO_HARDWARE_VERSION_UUID.equals(characteristic.getUuid())) {
                com.navdy.hud.app.device.dial.DialManager.sLogger.v("[Dial]onCharacteristicRead Hardware version");
                java.lang.String value = characteristic.getStringValue(0);
                if (!android.text.TextUtils.isEmpty(value)) {
                    com.navdy.hud.app.device.dial.DialManager.this.hardwareVersion = value.trim();
                    com.navdy.hud.app.device.dial.DialManager.sLogger.d("Device Info hardware version received: " + com.navdy.hud.app.device.dial.DialManager.this.hardwareVersion);
                }
            } else if (com.navdy.hud.app.device.dial.DialConstants.DEVICE_INFO_FIRMWARE_VERSION_UUID.equals(characteristic.getUuid())) {
                com.navdy.hud.app.device.dial.DialManager.sLogger.v("[Dial]onCharacteristicRead Firmware version");
                java.lang.String value2 = characteristic.getStringValue(0);
                if (!android.text.TextUtils.isEmpty(value2)) {
                    com.navdy.hud.app.device.dial.DialManager.this.firmWareVersion = value2.trim();
                    com.navdy.hud.app.device.dial.DialManager.sLogger.d("Device Info firmware version received: " + com.navdy.hud.app.device.dial.DialManager.this.firmWareVersion);
                }
            } else {
                com.navdy.hud.app.device.dial.DialManager.sLogger.v("[Dial]onCharacteristicRead:" + characteristic.getUuid());
            }
        }
    }

    class Anon14 implements java.lang.Runnable {
        Anon14() {
        }

        public void run() {
            if (!com.navdy.hud.app.device.dial.DialManager.this.dialManagerInitialized) {
                com.navdy.hud.app.device.dial.DialManager.this.buildDialList();
                com.navdy.hud.app.device.dial.DialManager.this.checkDialConnections();
                com.navdy.hud.app.device.dial.DialManager.this.dialManagerInitialized = true;
                com.navdy.hud.app.device.dial.DialManager.this.bus.post(com.navdy.hud.app.device.dial.DialConstants.INIT_EVENT);
                com.navdy.hud.app.device.dial.DialManager.sLogger.v("dial manager initialized");
            }
        }
    }

    class Anon15 implements java.lang.Runnable {

        class Anon1 implements com.navdy.hud.app.device.dial.DialFirmwareUpdater.UpdateListener {
            Anon1() {
            }

            public void onUpdateState(boolean available) {
                com.navdy.hud.app.device.dial.DialManager.this.bus.post(new com.navdy.hud.app.device.dial.DialManager.DialUpdateStatus(available));
            }
        }

        Anon15() {
        }

        public void run() {
            if (com.navdy.hud.app.device.dial.DialManager.this.dialFirmwareUpdater == null) {
                com.navdy.hud.app.device.dial.DialManager.this.dialFirmwareUpdater = new com.navdy.hud.app.device.dial.DialFirmwareUpdater(com.navdy.hud.app.device.dial.DialManager.this, com.navdy.hud.app.device.dial.DialManager.this.handler);
                com.navdy.hud.app.device.dial.DialManager.this.dialFirmwareUpdater.setUpdateListener(new com.navdy.hud.app.device.dial.DialManager.Anon15.Anon1());
            }
        }
    }

    class Anon16 implements com.navdy.hud.app.device.dial.DialManagerHelper.IDialConnectionStatus {
        final /* synthetic */ android.bluetooth.BluetoothDevice val$device;

        Anon16(android.bluetooth.BluetoothDevice bluetoothDevice) {
            this.val$device = bluetoothDevice;
        }

        public void onStatus(boolean connected) {
            if (!connected) {
                com.navdy.hud.app.device.dial.DialManager.sLogger.v("checkDialConnections: dial " + this.val$device.getName() + " not connected");
                return;
            }
            com.navdy.hud.app.device.dial.DialManager.sLogger.v("checkDialConnections: dial " + this.val$device.getName() + " connected");
            if (com.navdy.hud.app.device.dial.DialManager.this.connectedDial == null) {
                com.navdy.hud.app.device.dial.DialManager.this.connectedDial = this.val$device;
                com.navdy.hud.app.device.dial.DialManager.sLogger.v("marking dial [" + this.val$device.getName() + "] as connected");
                com.navdy.hud.app.device.dial.DialManager.this.handleDialConnection();
            }
        }
    }

    class Anon17 implements com.navdy.hud.app.device.dial.DialManagerHelper.IDialForgotten {
        final /* synthetic */ android.bluetooth.BluetoothDevice val$device;

        Anon17(android.bluetooth.BluetoothDevice bluetoothDevice) {
            this.val$device = bluetoothDevice;
        }

        public void onForgotten() {
            synchronized (com.navdy.hud.app.device.dial.DialManager.this.bondedDials) {
                java.util.Iterator<android.bluetooth.BluetoothDevice> iterator = com.navdy.hud.app.device.dial.DialManager.this.bondedDials.iterator();
                while (true) {
                    if (iterator.hasNext()) {
                        if (android.text.TextUtils.equals(((android.bluetooth.BluetoothDevice) iterator.next()).getName(), this.val$device.getName())) {
                            iterator.remove();
                            break;
                        }
                    } else {
                        break;
                    }
                }
            }
            com.navdy.hud.app.device.dial.DialManager.this.bondTries = 0;
            com.navdy.hud.app.device.dial.DialManager.DIAL_NOT_CONNECTED.dialName = this.val$device.getName();
            com.navdy.hud.app.device.dial.DialManager.this.bus.post(com.navdy.hud.app.device.dial.DialManager.DIAL_NOT_CONNECTED);
        }
    }

    class Anon18 implements com.navdy.hud.app.device.dial.DialManagerHelper.IDialConnection {
        final /* synthetic */ com.navdy.hud.app.device.dial.DialManagerHelper.IDialForgotten val$callback;
        final /* synthetic */ int val$delay;
        final /* synthetic */ android.bluetooth.BluetoothDevice val$device;

        Anon18(android.bluetooth.BluetoothDevice bluetoothDevice, int i, com.navdy.hud.app.device.dial.DialManagerHelper.IDialForgotten iDialForgotten) {
            this.val$device = bluetoothDevice;
            this.val$delay = i;
            this.val$callback = iDialForgotten;
        }

        public void onAttemptConnection(boolean success) {
        }

        public void onAttemptDisconnection(boolean success) {
            com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
            com.navdy.hud.app.device.dial.DialManager.sLogger.v("attempt dis-connection [" + this.val$device.getName() + " ] success[" + success + "] ");
            if (this.val$delay > 0) {
                com.navdy.hud.app.util.GenericUtil.sleep(this.val$delay);
            }
            com.navdy.hud.app.device.dial.DialManager.this.removeBond(this.val$device);
            if (this.val$callback != null) {
                this.val$callback.onForgotten();
            }
        }
    }

    class Anon19 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.hud.app.device.dial.DialManagerHelper.IDialForgotten val$callback;

        Anon19(com.navdy.hud.app.device.dial.DialManagerHelper.IDialForgotten iDialForgotten) {
            this.val$callback = iDialForgotten;
        }

        public void run() {
            com.navdy.hud.app.device.dial.DialManager.this.forgetAllDials(this.val$callback);
        }
    }

    class Anon2 implements java.lang.Runnable {
        Anon2() {
        }

        public void run() {
            try {
                com.navdy.hud.app.device.dial.DialManager.sLogger.e("[Dial]bond hang detected");
                java.lang.String dialName = null;
                if (com.navdy.hud.app.device.dial.DialManager.this.bondingDial != null) {
                    dialName = com.navdy.hud.app.device.dial.DialManager.this.bondingDial.getName();
                }
                com.navdy.hud.app.device.dial.DialManager.this.bondingDial = null;
                com.navdy.hud.app.device.dial.DialManager.DIAL_NOT_CONNECTED.dialName = dialName;
                com.navdy.hud.app.device.dial.DialManager.this.bus.post(com.navdy.hud.app.device.dial.DialManager.DIAL_NOT_CONNECTED);
            } catch (Throwable t) {
                com.navdy.hud.app.device.dial.DialManager.sLogger.e(com.navdy.hud.app.device.dial.DialManager.TAG_DIAL, t);
            }
        }
    }

    class Anon20 implements com.navdy.hud.app.device.dial.DialManagerHelper.IDialForgotten {
        final /* synthetic */ com.navdy.hud.app.device.dial.DialManagerHelper.IDialForgotten val$callback;
        final /* synthetic */ java.util.concurrent.atomic.AtomicInteger val$counter;

        Anon20(java.util.concurrent.atomic.AtomicInteger atomicInteger, com.navdy.hud.app.device.dial.DialManagerHelper.IDialForgotten iDialForgotten) {
            this.val$counter = atomicInteger;
            this.val$callback = iDialForgotten;
        }

        public void onForgotten() {
            int val = this.val$counter.decrementAndGet();
            com.navdy.hud.app.device.dial.DialManager.sLogger.v("forgetAllDials counter:" + val);
            if (val == 0) {
                com.navdy.hud.app.device.dial.DialManager.DIAL_NOT_CONNECTED.dialName = null;
                com.navdy.hud.app.device.dial.DialManager.this.bus.post(com.navdy.hud.app.device.dial.DialManager.DIAL_NOT_CONNECTED);
                if (this.val$callback != null) {
                    this.val$callback.onForgotten();
                }
            }
        }
    }

    class Anon21 implements java.lang.Runnable {
        Anon21() {
        }

        public void run() {
            java.net.DatagramSocket socket = null;
            try {
                com.navdy.hud.app.device.dial.DialManager.sLogger.v("DialEventsListener thread enter");
                java.net.DatagramSocket socket2 = new java.net.DatagramSocket(23654, java.net.InetAddress.getByName("127.0.0.1"));
                try {
                    byte[] data = new byte[56];
                    java.net.DatagramPacket packet = new java.net.DatagramPacket(data, data.length);
                    while (true) {
                        socket2.receive(packet);
                        java.lang.String str = new java.lang.String(data, 0, packet.getLength());
                        com.navdy.hud.app.device.dial.DialManager.sLogger.v("[DialEventsListener] [" + str + "]");
                        int index = str.indexOf(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA);
                        if (index >= 0) {
                            java.lang.String event = str.substring(0, index);
                            java.lang.String addr = str.substring(index + 1).toUpperCase();
                            com.navdy.hud.app.device.dial.DialManager.this.bluetoothAdapter;
                            if (android.bluetooth.BluetoothAdapter.checkBluetoothAddress(addr)) {
                                android.bluetooth.BluetoothDevice bluetoothDevice = com.navdy.hud.app.device.dial.DialManager.this.bluetoothAdapter.getRemoteDevice(addr);
                                com.navdy.hud.app.device.dial.DialManager.sLogger.v("[DialEventsListener] event for [" + bluetoothDevice + "]");
                                if (com.navdy.hud.app.device.dial.DialManager.this.isPresentInBondList(bluetoothDevice)) {
                                    char c = 65535;
                                    switch (event.hashCode()) {
                                        case -1425556143:
                                            if (event.equals(com.navdy.hud.app.device.dial.DialConstants.DIAL_EVENT_INPUT_REPORT_DESCRIPTOR)) {
                                                c = 3;
                                                break;
                                            }
                                            break;
                                        case -1324212103:
                                            if (event.equals("ENCRYPTION_FAILED")) {
                                                c = 2;
                                                break;
                                            }
                                            break;
                                        case 1015497884:
                                            if (event.equals("DISCONNECT")) {
                                                c = 1;
                                                break;
                                            }
                                            break;
                                        case 1669334218:
                                            if (event.equals("CONNECT")) {
                                                c = 0;
                                                break;
                                            }
                                            break;
                                    }
                                    switch (c) {
                                        case 0:
                                            com.navdy.hud.app.device.dial.DialManager.this.connectEvent(bluetoothDevice);
                                            break;
                                        case 1:
                                            com.navdy.hud.app.device.dial.DialManager.this.disconnectEvent(bluetoothDevice);
                                            break;
                                        case 2:
                                            com.navdy.hud.app.device.dial.DialManager.this.encryptionFailedEvent(bluetoothDevice);
                                            break;
                                        case 3:
                                            com.navdy.hud.app.device.dial.DialManager.this.handleInputReportDescriptor(bluetoothDevice);
                                            break;
                                    }
                                } else {
                                    com.navdy.hud.app.device.dial.DialManager.sLogger.v("[DialEventsListener] " + addr + " not present in bond list, ignore");
                                }
                            } else {
                                com.navdy.hud.app.device.dial.DialManager.sLogger.e("[DialEventsListener] not a valid bluetooth address");
                            }
                        } else {
                            com.navdy.hud.app.device.dial.DialManager.sLogger.v("[DialEventsListener] invalid data");
                        }
                    }
                } catch (Throwable th) {
                    t = th;
                    socket = socket2;
                }
            } catch (Throwable th2) {
                t = th2;
                com.navdy.hud.app.device.dial.DialManager.sLogger.e(t);
                if (socket != null) {
                    com.navdy.service.library.util.IOUtils.closeStream(socket);
                }
                com.navdy.hud.app.device.dial.DialManager.sLogger.v("DialEventsListener thread exit");
            }
        }
    }

    class Anon22 implements com.navdy.hud.app.device.dial.DialManagerHelper.IDialConnection {
        Anon22() {
        }

        public void onAttemptConnection(boolean success) {
        }

        public void onAttemptDisconnection(boolean success) {
            com.navdy.hud.app.device.dial.DialManagerHelper.sendLocalyticsEvent(com.navdy.hud.app.device.dial.DialManager.this.handler, false, false, 0, false, "Encryption failed, retrying");
        }
    }

    class Anon23 implements com.navdy.hud.app.device.dial.DialManagerHelper.IDialConnection {
        final /* synthetic */ android.bluetooth.BluetoothDevice val$device;

        class Anon1 implements java.lang.Runnable {
            Anon1() {
            }

            public void run() {
                com.navdy.hud.app.device.dial.DialManager.this.forgetDial(com.navdy.hud.app.device.dial.DialManager.Anon23.this.val$device);
                com.navdy.hud.app.device.dial.DialManager.sLogger.e("[DialEvent-encryptfail] bond removed :-/ connected[" + com.navdy.hud.app.device.dial.DialManager.this.connectedDial + "] event[" + com.navdy.hud.app.device.dial.DialManager.Anon23.this.val$device + "]");
                if (com.navdy.hud.app.device.dial.DialManager.Anon23.this.val$device.equals(com.navdy.hud.app.device.dial.DialManager.this.connectedDial)) {
                    com.navdy.hud.app.device.dial.DialManager.this.disconnectedDial = com.navdy.hud.app.device.dial.DialManager.this.connectedDial.getName();
                    com.navdy.hud.app.device.dial.DialManager.this.clearConnectedDial();
                    com.navdy.hud.app.device.dial.DialManager.DIAL_CONNECTION_FAILED.dialName = com.navdy.hud.app.device.dial.DialManager.this.disconnectedDial;
                    com.navdy.hud.app.device.dial.DialManager.this.bus.post(com.navdy.hud.app.device.dial.DialManager.DIAL_CONNECTION_FAILED);
                    com.navdy.hud.app.device.dial.DialManager.sLogger.e("[DialEvent-encryptfail] current dial is disconnected");
                }
                com.navdy.hud.app.device.dial.DialManagerHelper.sendLocalyticsEvent(com.navdy.hud.app.device.dial.DialManager.this.handler, false, false, 0, false, "Encryption failed, deleting bond");
            }
        }

        Anon23(android.bluetooth.BluetoothDevice bluetoothDevice) {
            this.val$device = bluetoothDevice;
        }

        public void onAttemptConnection(boolean success) {
        }

        public void onAttemptDisconnection(boolean success) {
            com.navdy.hud.app.device.dial.DialManager.sLogger.e("[DialEvent-encryptfail] disconnected");
            com.navdy.hud.app.device.dial.DialManager.this.handler.postDelayed(new com.navdy.hud.app.device.dial.DialManager.Anon23.Anon1(), com.navdy.hud.app.device.gps.GpsManager.MINIMUM_DRIVE_TIME);
        }
    }

    class Anon3 implements java.lang.Runnable {
        Anon3() {
        }

        public void run() {
            if (com.navdy.hud.app.device.dial.DialManager.this.connectedDial != null) {
                com.navdy.hud.app.device.dial.DialManagerHelper.sendLocalyticsEvent(com.navdy.hud.app.device.dial.DialManager.this.handler, false, true, 0, false);
            }
        }
    }

    class Anon4 extends android.bluetooth.le.ScanCallback {
        Anon4() {
        }

        public void onScanResult(int callbackType, android.bluetooth.le.ScanResult result) {
            try {
                android.bluetooth.le.ScanRecord record = result.getScanRecord();
                com.navdy.hud.app.device.dial.DialManager.this.handleScannedDevice(result.getDevice(), record.getTxPowerLevel(), record);
            } catch (Throwable t) {
                com.navdy.hud.app.device.dial.DialManager.sLogger.e(com.navdy.hud.app.device.dial.DialManager.TAG_DIAL, t);
            }
        }

        public void onScanFailed(int errorCode) {
            com.navdy.hud.app.device.dial.DialManager.sLogger.e("btle scan failed:" + errorCode);
            com.navdy.hud.app.device.dial.DialManager.this.stopScan(true);
        }
    }

    class Anon5 implements java.lang.Runnable {
        Anon5() {
        }

        public void run() {
            try {
                com.navdy.hud.app.device.dial.DialManager.this.stopScan(false);
            } catch (Throwable t) {
                com.navdy.hud.app.device.dial.DialManager.sLogger.e(com.navdy.hud.app.device.dial.DialManager.TAG_DIAL, t);
            }
        }
    }

    class Anon6 extends android.content.BroadcastReceiver {

        class Anon1 implements com.navdy.hud.app.device.dial.DialManagerHelper.IDialConnection {
            final /* synthetic */ android.bluetooth.BluetoothDevice val$device;

            Anon1(android.bluetooth.BluetoothDevice bluetoothDevice) {
                this.val$device = bluetoothDevice;
            }

            public void onAttemptConnection(boolean success) {
                com.navdy.hud.app.device.dial.DialManager.sLogger.v("btConnRecvr: attempt connection [" + this.val$device.getName() + " ] success[" + success + "]");
            }

            public void onAttemptDisconnection(boolean success) {
            }
        }

        class Anon2 implements java.lang.Runnable {
            final /* synthetic */ android.bluetooth.BluetoothDevice val$device;

            Anon2(android.bluetooth.BluetoothDevice bluetoothDevice) {
                this.val$device = bluetoothDevice;
            }

            public void run() {
                com.navdy.hud.app.device.dial.DialManager.sLogger.v("btBondRecvr removed runnable");
                com.navdy.hud.app.device.dial.DialManager.this.handler.removeCallbacks(com.navdy.hud.app.device.dial.DialManager.this.bondHangRunnable);
                this.val$device.createBond();
                com.navdy.hud.app.device.dial.DialManager.this.handler.postDelayed(com.navdy.hud.app.device.dial.DialManager.this.bondHangRunnable, 30000);
                com.navdy.hud.app.device.dial.DialManager.sLogger.v("btBondRecvr posted runnable");
            }
        }

        Anon6() {
        }

        public void onReceive(android.content.Context context, android.content.Intent intent) {
            try {
                java.lang.String action = intent.getAction();
                android.bluetooth.BluetoothDevice device = (android.bluetooth.BluetoothDevice) intent.getParcelableExtra("android.bluetooth.device.extra.DEVICE");
                if (device != null && com.navdy.hud.app.device.dial.DialManager.this.bondingDial != null) {
                    if (!com.navdy.hud.app.device.dial.DialManager.this.isDialDevice(device)) {
                        com.navdy.hud.app.device.dial.DialManager.sLogger.i("[Dial]btBondRecvr: notification not for dial:" + device.getName() + " addr:" + device.getAddress());
                        return;
                    }
                    com.navdy.hud.app.device.dial.DialManager.sLogger.v("[Dial]btBondRecvr [" + device.getName() + "] action[" + action + "]");
                    if ("android.bluetooth.device.action.BOND_STATE_CHANGED".equals(action)) {
                        int state = intent.getIntExtra("android.bluetooth.device.extra.BOND_STATE", Integer.MIN_VALUE);
                        com.navdy.hud.app.device.dial.DialManager.sLogger.v("[Dial]btBondRecvr: prev state:" + intent.getIntExtra("android.bluetooth.device.extra.PREVIOUS_BOND_STATE", Integer.MIN_VALUE) + " new state:" + state);
                        if (state == 12) {
                            com.navdy.hud.app.device.dial.DialManager.sLogger.v("btBondRecvr removed hang runnable");
                            com.navdy.hud.app.device.dial.DialManager.this.handler.removeCallbacks(com.navdy.hud.app.device.dial.DialManager.this.bondHangRunnable);
                            com.navdy.hud.app.device.dial.DialManager.sLogger.v("[Dial]btBondRecvr: got pairing event");
                            if (com.navdy.hud.app.device.dial.DialManager.this.bondingDial == null || !com.navdy.hud.app.device.dial.DialManager.this.bondingDial.equals(device)) {
                                com.navdy.hud.app.device.dial.DialManager.sLogger.v("[Dial]no bonding active bondDial[" + com.navdy.hud.app.device.dial.DialManager.this.bondingDial + "] device[" + device + "]");
                                return;
                            }
                            com.navdy.hud.app.device.dial.DialManager.sLogger.v("[Dial]btBondRecvr: bonding active bondDial[" + com.navdy.hud.app.device.dial.DialManager.this.bondingDial + "] device[" + device + "]");
                            com.navdy.hud.app.device.dial.DialManager.sLogger.v("btBondRecvr:" + device + "state is:" + device.getBondState());
                            if (com.navdy.hud.app.device.dial.DialManager.this.addtoBondList(device)) {
                                com.navdy.hud.app.device.dial.DialManager.sLogger.v("btBondRecvr: added to bond list");
                            } else {
                                com.navdy.hud.app.device.dial.DialManager.sLogger.v("btConnRecvr: already in bond list");
                            }
                            com.navdy.hud.app.device.dial.DialManager.sLogger.v("btConnRecvr: attempt connection reqd");
                            com.navdy.hud.app.device.dial.DialManager.this.bondingDial = null;
                            com.navdy.hud.app.device.dial.DialManagerHelper.connectToDial(com.navdy.hud.app.device.dial.DialManager.this.bluetoothAdapter, device, new com.navdy.hud.app.device.dial.DialManager.Anon6.Anon1(device));
                        } else if (state == 10) {
                            com.navdy.hud.app.device.dial.DialManager.sLogger.e("[Dial]btBondRecvr: not paired tries[" + com.navdy.hud.app.device.dial.DialManager.this.bondTries + "]");
                            if (com.navdy.hud.app.device.dial.DialManager.this.bondTries == 3) {
                                com.navdy.hud.app.device.dial.DialManager.sLogger.v("btBondRecvr removed runnable");
                                com.navdy.hud.app.device.dial.DialManager.this.handler.removeCallbacks(com.navdy.hud.app.device.dial.DialManager.this.bondHangRunnable);
                                java.lang.String dialName = null;
                                if (com.navdy.hud.app.device.dial.DialManager.this.bondingDial != null) {
                                    dialName = com.navdy.hud.app.device.dial.DialManager.this.bondingDial.getName();
                                }
                                com.navdy.hud.app.device.dial.DialManager.this.bondingDial = null;
                                com.navdy.hud.app.device.dial.DialManager.sLogger.e("[Dial]btBondRecvr: giving up");
                                com.navdy.hud.app.device.dial.DialManager.DIAL_NOT_CONNECTED.dialName = dialName;
                                com.navdy.hud.app.device.dial.DialManager.this.bus.post(com.navdy.hud.app.device.dial.DialManager.DIAL_NOT_CONNECTED);
                                return;
                            }
                            com.navdy.hud.app.device.dial.DialManager.sLogger.e("[Dial]btBondRecvr: trying to bond again");
                            com.navdy.hud.app.device.dial.DialManager.this.bondTries = com.navdy.hud.app.device.dial.DialManager.this.bondTries + 1;
                            com.navdy.hud.app.device.dial.DialManager.this.handler.postDelayed(new com.navdy.hud.app.device.dial.DialManager.Anon6.Anon2(device), 4000);
                        }
                    }
                }
            } catch (Throwable t) {
                com.navdy.hud.app.device.dial.DialManager.sLogger.e(com.navdy.hud.app.device.dial.DialManager.TAG_DIAL, t);
            }
        }
    }

    class Anon7 extends android.content.BroadcastReceiver {
        Anon7() {
        }

        public void onReceive(android.content.Context context, android.content.Intent intent) {
            try {
                if (intent.getAction().equals("android.bluetooth.adapter.action.STATE_CHANGED")) {
                    switch (intent.getIntExtra("android.bluetooth.adapter.extra.STATE", Integer.MIN_VALUE)) {
                        case 10:
                            com.navdy.hud.app.device.dial.DialManager.sLogger.v("[Dial]onoff: state off");
                            return;
                        case 11:
                            com.navdy.hud.app.device.dial.DialManager.sLogger.v("[Dial]onoff: state turning on");
                            return;
                        case 12:
                            com.navdy.hud.app.device.dial.DialManager.sLogger.v("[Dial]onoff: state on");
                            if (!com.navdy.hud.app.device.dial.DialManager.this.dialManagerInitialized) {
                                com.navdy.hud.app.device.dial.DialManager.this.init();
                                return;
                            }
                            return;
                        case 13:
                            com.navdy.hud.app.device.dial.DialManager.sLogger.v("[Dial]onoff: state turning off");
                            return;
                        default:
                            return;
                    }
                    com.navdy.hud.app.device.dial.DialManager.sLogger.e(com.navdy.hud.app.device.dial.DialManager.TAG_DIAL, t);
                }
            } catch (Throwable t) {
                com.navdy.hud.app.device.dial.DialManager.sLogger.e(com.navdy.hud.app.device.dial.DialManager.TAG_DIAL, t);
            }
        }
    }

    class Anon8 implements java.lang.Runnable {

        class Anon1 implements com.navdy.hud.app.device.dial.DialManagerHelper.IDialForgotten {
            final /* synthetic */ java.lang.String val$dialName;

            /* renamed from: com.navdy.hud.app.device.dial.DialManager$Anon8$Anon1$Anon1 reason: collision with other inner class name */
            class C0006Anon1 implements java.lang.Runnable {
                C0006Anon1() {
                }

                public void run() {
                    com.navdy.hud.app.device.dial.DialManagerHelper.sendLocalyticsEvent(com.navdy.hud.app.device.dial.DialManager.this.handler, false, false, 0, false, "Connection error");
                }
            }

            Anon1(java.lang.String str) {
                this.val$dialName = str;
            }

            public void onForgotten() {
                com.navdy.hud.app.device.dial.DialManager.sLogger.v("connectionErrorRunnable: dial forgotten");
                com.navdy.hud.app.device.dial.DialManager.DIAL_CONNECTION_FAILED.dialName = this.val$dialName;
                com.navdy.hud.app.device.dial.DialManager.this.bus.post(com.navdy.hud.app.device.dial.DialManager.DIAL_CONNECTION_FAILED);
                com.navdy.hud.app.device.dial.DialManager.this.handler.post(new com.navdy.hud.app.device.dial.DialManager.Anon8.Anon1.C0006Anon1());
            }
        }

        Anon8() {
        }

        public void run() {
            com.navdy.hud.app.device.dial.DialManager.sLogger.v("connectionErrorRunnable no input descriptor event:" + com.navdy.hud.app.device.dial.DialManager.this.tempConnectedDial);
            if (com.navdy.hud.app.device.dial.DialManager.this.tempConnectedDial != null) {
                com.navdy.hud.app.device.dial.DialManager.this.disconectAndRemoveBond(com.navdy.hud.app.device.dial.DialManager.this.tempConnectedDial, new com.navdy.hud.app.device.dial.DialManager.Anon8.Anon1(com.navdy.hud.app.device.dial.DialManager.this.tempConnectedDial.getName()), 2000);
                return;
            }
            com.navdy.hud.app.device.dial.DialManager.sLogger.v("connectionErrorRunnable no temp connected dial");
        }
    }

    class Anon9 extends android.content.BroadcastReceiver {

        class Anon1 implements com.navdy.hud.app.device.dial.DialManagerHelper.IDialConnection {
            final /* synthetic */ android.bluetooth.BluetoothDevice val$device;

            Anon1(android.bluetooth.BluetoothDevice bluetoothDevice) {
                this.val$device = bluetoothDevice;
            }

            public void onAttemptConnection(boolean success) {
                com.navdy.hud.app.device.dial.DialManager.sLogger.v("attempt connection [" + this.val$device.getName() + " ] success[" + success + "]");
                if (success) {
                    com.navdy.hud.app.device.dial.DialManager.this.handleDialConnection();
                    com.navdy.hud.app.device.dial.DialManager.sLogger.i("[Dial]btConnRecvr: dial connected [" + this.val$device.getName() + "] addr:" + this.val$device.getAddress());
                }
            }

            public void onAttemptDisconnection(boolean success) {
            }
        }

        Anon9() {
        }

        public void onReceive(android.content.Context context, android.content.Intent intent) {
            if (intent != null) {
                try {
                    java.lang.String action = intent.getAction();
                    android.bluetooth.BluetoothDevice device = (android.bluetooth.BluetoothDevice) intent.getParcelableExtra("android.bluetooth.device.extra.DEVICE");
                    if (device == null) {
                        return;
                    }
                    if (!com.navdy.hud.app.device.dial.DialManager.this.isDialDevice(device)) {
                        com.navdy.hud.app.device.dial.DialManager.sLogger.i("[Dial]btConnRecvr: notification not for dial:" + device.getName() + " addr:" + device.getAddress());
                        return;
                    }
                    com.navdy.hud.app.device.dial.DialManager.sLogger.v("[Dial]btConnRecvr [" + device.getName() + "] action [" + action + "]");
                    if (!com.navdy.hud.app.util.DeviceUtil.isNavdyDevice()) {
                        boolean isConnected = false;
                        if (com.navdy.hud.app.device.dial.DialConstants.ACTION_CONNECTION_STATE_CHANGED.equals(action)) {
                            int oldState = intent.getIntExtra("android.bluetooth.profile.extra.PREVIOUS_STATE", -1);
                            int newState = intent.getIntExtra("android.bluetooth.profile.extra.STATE", -1);
                            com.navdy.hud.app.device.dial.DialManager.sLogger.v("[Dial]btConnRecvr: dial connection state changed old[" + oldState + "] new[" + newState + "]");
                            if (newState == 2) {
                                isConnected = true;
                            }
                        }
                        if (com.navdy.hud.app.device.dial.DialManager.this.connectedDial != null && !android.text.TextUtils.equals(com.navdy.hud.app.device.dial.DialManager.this.connectedDial.getName(), device.getName())) {
                            com.navdy.hud.app.device.dial.DialManager.sLogger.v("btConnRecvr: notification for not current connected dial");
                            com.navdy.hud.app.device.dial.DialManager.this.handler.removeCallbacks(com.navdy.hud.app.device.dial.DialManager.this.bondHangRunnable);
                        } else if ("android.bluetooth.device.action.ACL_CONNECTED".equals(action) || isConnected) {
                            com.navdy.hud.app.device.dial.DialManager.this.handler.removeCallbacks(com.navdy.hud.app.device.dial.DialManager.this.bondHangRunnable);
                            com.navdy.hud.app.device.dial.DialManager.this.connectedDial = device;
                            if (com.navdy.hud.app.device.dial.DialManager.this.addtoBondList(device)) {
                                com.navdy.hud.app.device.dial.DialManager.sLogger.v("attempt connection reqd");
                                com.navdy.hud.app.device.dial.DialManagerHelper.connectToDial(com.navdy.hud.app.device.dial.DialManager.this.bluetoothAdapter, device, new com.navdy.hud.app.device.dial.DialManager.Anon9.Anon1(device));
                                return;
                            }
                            com.navdy.hud.app.device.dial.DialManager.sLogger.v("attempt connection not read");
                            com.navdy.hud.app.device.dial.DialManager.this.handleDialConnection();
                        } else if ("android.bluetooth.device.action.ACL_DISCONNECTED".equals(action)) {
                            com.navdy.hud.app.device.dial.DialManager.this.handler.removeCallbacks(com.navdy.hud.app.device.dial.DialManager.this.bondHangRunnable);
                            if (com.navdy.hud.app.device.dial.DialManager.this.connectedDial != null) {
                                com.navdy.hud.app.device.dial.DialManager.this.disconnectedDial = com.navdy.hud.app.device.dial.DialManager.this.connectedDial.getName();
                            }
                            com.navdy.hud.app.device.dial.DialManager.DIAL_NOT_CONNECTED.dialName = com.navdy.hud.app.device.dial.DialManager.this.disconnectedDial;
                            com.navdy.hud.app.device.dial.DialManager.this.bus.post(com.navdy.hud.app.device.dial.DialManager.DIAL_NOT_CONNECTED);
                            if (com.navdy.hud.app.device.dial.DialManager.this.getBondedDialCount() > 0) {
                            }
                            com.navdy.hud.app.device.dial.DialManager.this.clearConnectedDial();
                            com.navdy.hud.app.device.dial.DialManager.this.stopGATTClient();
                            com.navdy.hud.app.device.dial.DialManager.sLogger.e("[Dial]btConnRecvr: dial NOT connected [" + device.getName() + "] addr:" + device.getAddress());
                        }
                    }
                } catch (Throwable t) {
                    com.navdy.hud.app.device.dial.DialManager.sLogger.e(com.navdy.hud.app.device.dial.DialManager.TAG_DIAL, t);
                }
            }
        }
    }

    static class CharacteristicCommandProcessor {
        /* access modifiers changed from: private */
        public android.bluetooth.BluetoothGatt bluetoothGatt;
        private java.util.LinkedList<com.navdy.hud.app.device.dial.DialManager.CharacteristicCommandProcessor.Command> commandsQueue = new java.util.LinkedList<>();
        private android.os.Handler handler;

        class Anon1 implements java.lang.Runnable {
            final /* synthetic */ com.navdy.hud.app.device.dial.DialManager.CharacteristicCommandProcessor.Command val$command;

            Anon1(com.navdy.hud.app.device.dial.DialManager.CharacteristicCommandProcessor.Command command) {
                this.val$command = command;
            }

            public void run() {
                try {
                    if (com.navdy.hud.app.device.dial.DialManager.CharacteristicCommandProcessor.this.bluetoothGatt != null) {
                        this.val$command.process(com.navdy.hud.app.device.dial.DialManager.CharacteristicCommandProcessor.this.bluetoothGatt);
                    }
                } catch (Throwable t) {
                    com.navdy.hud.app.device.dial.DialManager.sLogger.d("Error while executing GATT command", t);
                }
            }
        }

        static abstract class Command {
            android.bluetooth.BluetoothGattCharacteristic characteristic;

            public abstract void process(android.bluetooth.BluetoothGatt bluetoothGatt);

            public Command(android.bluetooth.BluetoothGattCharacteristic characteristic2) {
                this.characteristic = characteristic2;
            }
        }

        static class ReadCommand extends com.navdy.hud.app.device.dial.DialManager.CharacteristicCommandProcessor.Command {
            public ReadCommand(android.bluetooth.BluetoothGattCharacteristic characteristic) {
                super(characteristic);
            }

            public void process(android.bluetooth.BluetoothGatt gatt) {
                if (gatt.readCharacteristic(this.characteristic)) {
                    com.navdy.hud.app.device.dial.DialManager.sLogger.d("Processing GATT read succeeded " + this.characteristic.getUuid().toString());
                } else {
                    com.navdy.hud.app.device.dial.DialManager.sLogger.d("Processing GATT read failed " + this.characteristic.getUuid().toString());
                }
            }
        }

        static class WriteCommand extends com.navdy.hud.app.device.dial.DialManager.CharacteristicCommandProcessor.Command {
            byte[] data;

            public WriteCommand(android.bluetooth.BluetoothGattCharacteristic characteristic, byte[] data2) {
                super(characteristic);
                this.data = data2;
            }

            public void process(android.bluetooth.BluetoothGatt gatt) {
                this.characteristic.setValue(this.data);
                if (!gatt.writeCharacteristic(this.characteristic)) {
                    com.navdy.hud.app.device.dial.DialManager.sLogger.d("Processing GATT write failed " + this.characteristic.getUuid().toString());
                } else if (!this.characteristic.getUuid().equals(com.navdy.hud.app.device.dial.DialConstants.OTA_DATA_CHARACTERISTIC_UUID)) {
                    com.navdy.hud.app.device.dial.DialManager.sLogger.d("Processing GATT write succeeded " + this.characteristic.getUuid().toString());
                }
            }
        }

        public CharacteristicCommandProcessor(android.bluetooth.BluetoothGatt bluetoothGatt2, android.os.Handler handler2) {
            this.bluetoothGatt = bluetoothGatt2;
            this.handler = handler2;
        }

        public synchronized void process(com.navdy.hud.app.device.dial.DialManager.CharacteristicCommandProcessor.Command command) {
            this.commandsQueue.add(command);
            if (this.commandsQueue.size() == 1) {
                submitNext();
            }
        }

        public synchronized void submitNext() {
            com.navdy.hud.app.device.dial.DialManager.CharacteristicCommandProcessor.Command command = (com.navdy.hud.app.device.dial.DialManager.CharacteristicCommandProcessor.Command) this.commandsQueue.peek();
            if (command != null) {
                this.handler.post(new com.navdy.hud.app.device.dial.DialManager.CharacteristicCommandProcessor.Anon1(command));
            }
        }

        public synchronized void commandFinished() {
            if (this.commandsQueue.size() > 0) {
                com.navdy.hud.app.device.dial.DialManager.CharacteristicCommandProcessor.Command command = (com.navdy.hud.app.device.dial.DialManager.CharacteristicCommandProcessor.Command) this.commandsQueue.poll();
                submitNext();
            }
        }

        public void release() {
            this.commandsQueue.clear();
            this.handler = null;
            this.bluetoothGatt = null;
        }
    }

    public static class DialUpdateStatus {
        public boolean available;

        DialUpdateStatus(boolean available2) {
            this.available = available2;
        }
    }

    static {
        PowerMap.put(LOW_POWER, java.lang.Integer.valueOf(2));
        PowerMap.put(BALANCED, java.lang.Integer.valueOf(0));
        PowerMap.put(HIGH_POWER, java.lang.Integer.valueOf(1));
    }

    public static com.navdy.hud.app.device.dial.DialManager getInstance() {
        return singleton;
    }

    public void queueRead(android.bluetooth.BluetoothGattCharacteristic characteristic) {
        if (this.gattCharacteristicProcessor == null) {
            sLogger.w("gattCharacteristicProcessor not initialized");
        } else {
            this.gattCharacteristicProcessor.process(new com.navdy.hud.app.device.dial.DialManager.CharacteristicCommandProcessor.ReadCommand(characteristic));
        }
    }

    public void queueWrite(android.bluetooth.BluetoothGattCharacteristic characteristic, byte[] data) {
        if (this.gattCharacteristicProcessor == null) {
            sLogger.w("gattCharacteristicProcessor not initialized");
        } else {
            this.gattCharacteristicProcessor.process(new com.navdy.hud.app.device.dial.DialManager.CharacteristicCommandProcessor.WriteCommand(characteristic, data));
        }
    }

    public DialManager() {
        sLogger.v("[Dial]initializing...");
        if (com.navdy.hud.app.HudApplication.getApplication() != null) {
            this.bus = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getBus();
            this.sharedPreferences = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getSharedPreferences();
            this.bluetoothManager = (android.bluetooth.BluetoothManager) com.navdy.hud.app.HudApplication.getAppContext().getSystemService("bluetooth");
            if (this.bluetoothManager == null) {
                sLogger.e("[Dial]Bluetooth manager not available");
                return;
            }
            this.bluetoothAdapter = this.bluetoothManager.getAdapter();
            if (this.bluetoothAdapter == null) {
                sLogger.e("[Dial]Bluetooth is not available");
                return;
            }
            this.handlerThread = new android.os.HandlerThread("DialHandlerThread");
            this.handlerThread.start();
            this.handler = new android.os.Handler(this.handlerThread.getLooper());
            if (!this.bluetoothAdapter.isEnabled()) {
                sLogger.i("[Dial]Bluetooth is not enabled, should be booting up, wait for the event");
            } else {
                sLogger.i("[Dial]Bluetooth is enabled");
                init();
            }
            startDialEventListener();
            registerReceivers();
        }
    }

    /* access modifiers changed from: private */
    public synchronized void init() {
        sLogger.v("[Dial]Client Address:" + this.bluetoothAdapter.getAddress() + " name:" + this.bluetoothAdapter.getName());
        this.bluetoothLEScanner = this.bluetoothAdapter.getBluetoothLeScanner();
        sLogger.v("[Dial]btle scanner:" + this.bluetoothLEScanner);
        this.handler.post(new com.navdy.hud.app.device.dial.DialManager.Anon14());
        this.handler.post(new com.navdy.hud.app.device.dial.DialManager.Anon15());
    }

    /* access modifiers changed from: 0000 */
    public android.bluetooth.BluetoothDevice getDialDevice() {
        return this.connectedDial;
    }

    /* access modifiers changed from: private */
    public void buildDialList() {
        try {
            sLogger.v("[Dial]trying to find paired dials");
            java.util.Set<android.bluetooth.BluetoothDevice> bondedDevices = this.bluetoothAdapter.getBondedDevices();
            sLogger.v("[Dial]Bonded devices:" + (bondedDevices == null ? 0 : bondedDevices.size()));
            java.util.ArrayList<android.bluetooth.BluetoothDevice> foundDevices = new java.util.ArrayList<>(8);
            for (android.bluetooth.BluetoothDevice device : bondedDevices) {
                sLogger.v("[Dial]Bonded Device Address[" + device.getAddress() + "] name[" + device.getName() + " type[" + com.navdy.hud.app.bluetooth.utils.BluetoothUtils.getDeviceType(device.getType()) + "]");
                if (isDialDeviceName(device.getName())) {
                    sLogger.v("[Dial]found paired dial Address[" + device + "] name[" + device.getName() + "]");
                    foundDevices.add(device);
                }
            }
            if (foundDevices.size() == 0) {
                sLogger.v("[Dial]no dial device found");
                return;
            }
            sLogger.v("[Dial]found paired dials:" + foundDevices.size());
            synchronized (this.bondedDials) {
                this.bondedDials.clear();
                this.bondedDials.addAll(foundDevices);
            }
        } catch (Throwable t) {
            sLogger.e(TAG_DIAL, t);
        }
    }

    /* access modifiers changed from: private */
    public void checkDialConnections() {
        try {
            synchronized (this.bondedDials) {
                java.util.Iterator it = this.bondedDials.iterator();
                while (it.hasNext()) {
                    android.bluetooth.BluetoothDevice device = (android.bluetooth.BluetoothDevice) it.next();
                    com.navdy.hud.app.device.dial.DialManagerHelper.isDialConnected(this.bluetoothAdapter, device, new com.navdy.hud.app.device.dial.DialManager.Anon16(device));
                }
            }
        } catch (Throwable t) {
            sLogger.e(TAG_DIAL, t);
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0040, code lost:
        r1 = r9.getName();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0048, code lost:
        if (android.text.TextUtils.isEmpty(r1) != false) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x004e, code lost:
        if (isDialDevice(r9) != false) goto L_0x007c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0056, code lost:
        if (r8.ignoredNonNavdyDials.contains(r9) != false) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0058, code lost:
        sLogger.v("[Dial]ignoring [" + r1 + "]");
        r8.ignoredNonNavdyDials.add(r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x007c, code lost:
        r3 = r11.getServiceUuids();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0080, code lost:
        if (r3 != null) goto L_0x00a2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0082, code lost:
        sLogger.v("[Dial]no service uuid found [" + r1 + "]");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x00a2, code lost:
        r0 = false;
        r5 = r3.iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00ab, code lost:
        if (r5.hasNext() == false) goto L_0x00c0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00bd, code lost:
        if (((android.os.ParcelUuid) r5.next()).getUuid().equals(com.navdy.hud.app.device.dial.DialConstants.HID_SERVICE_UUID) == false) goto L_0x00a7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00bf, code lost:
        r0 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00c0, code lost:
        if (r0 != false) goto L_0x00ef;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00c8, code lost:
        if (r8.ignoredNonNavdyDials.contains(r9) != false) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00ca, code lost:
        sLogger.v("[Dial]HID service not found ignoring [" + r1 + "]");
        r8.ignoredNonNavdyDials.add(r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00ef, code lost:
        sLogger.v("[Dial]Scanned name[" + r9.getName() + "] mac[" + r9.getAddress() + "] bonded[" + com.navdy.hud.app.bluetooth.utils.BluetoothUtils.getBondState(r9.getBondState()) + "] rssi[" + r10 + "]" + " type[" + com.navdy.hud.app.bluetooth.utils.BluetoothUtils.getDeviceType(r9.getType()) + "]");
        r6 = r8.scannedNavdyDials;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0155, code lost:
        monitor-enter(r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:?, code lost:
        r8.scannedNavdyDials.put(r9, r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x015b, code lost:
        monitor-exit(r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0160, code lost:
        if (com.navdy.hud.app.device.dial.DialConstants.PAIRING_RULE != com.navdy.hud.app.device.dial.DialConstants.PairingRule.FIRST) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0162, code lost:
        sLogger.v("[Dial]pairing rule is first, stopping scan...");
        r8.handler.removeCallbacks(r8.stopScanRunnable);
        r8.handler.post(r8.stopScanRunnable);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:?, code lost:
        return;
     */
    public void handleScannedDevice(android.bluetooth.BluetoothDevice device, int rssi, android.bluetooth.le.ScanRecord scanRecord) {
        if (device.getType() == 2) {
            if (!this.isScanning) {
                sLogger.i("[Dial]not scanning anymore [" + device.getName() + "]");
                return;
            }
            synchronized (this.scannedNavdyDials) {
                if (this.scannedNavdyDials.containsKey(device)) {
                }
            }
        }
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    public void startScan() {
        try {
            if (this.isScanning) {
                sLogger.v("scan already running");
                return;
            }
            this.handler.removeCallbacks(this.scanHangRunnable);
            this.bondingDial = null;
            synchronized (this.scannedNavdyDials) {
                this.scannedNavdyDials.clear();
                this.ignoredNonNavdyDials.clear();
            }
            this.bluetoothLEScanner.startScan(this.leScanCallback);
            this.handler.postDelayed(this.scanHangRunnable, 30000);
            this.isScanning = true;
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:29:?, code lost:
        return;
     */
    public void stopScan(boolean sendEvent) {
        if (this.isScanning) {
            sLogger.v("[Dial]stopping btle scan...");
            this.handler.removeCallbacks(this.scanHangRunnable);
            this.isScanning = false;
            try {
                this.bluetoothLEScanner.stopScan(this.leScanCallback);
            } catch (Throwable t) {
                sLogger.e(t);
            }
            synchronized (this.scannedNavdyDials) {
                this.ignoredNonNavdyDials.clear();
                sLogger.v("[Dial]stopscan dials found:" + this.scannedNavdyDials.size());
                if (this.scannedNavdyDials.size() == 0) {
                    sLogger.i("[Dial]No dials found");
                    if (sendEvent) {
                        this.bus.post(DIAL_NOT_FOUND);
                    }
                } else {
                    java.util.Map.Entry<android.bluetooth.BluetoothDevice, android.bluetooth.le.ScanRecord> entry = (java.util.Map.Entry) this.scannedNavdyDials.entrySet().iterator().next();
                    android.bluetooth.BluetoothDevice deviceToBond = (android.bluetooth.BluetoothDevice) entry.getKey();
                    android.bluetooth.le.ScanRecord deviceScanRecord = (android.bluetooth.le.ScanRecord) entry.getValue();
                    this.scannedNavdyDials.clear();
                    bondWithDial(deviceToBond, deviceScanRecord);
                }
            }
        } else {
            sLogger.w("[Dial]not currently scanning");
        }
    }

    private void registerReceivers() {
        android.content.Context context = com.navdy.hud.app.HudApplication.getAppContext();
        context.registerReceiver(this.bluetoothBondingReceiver, new android.content.IntentFilter("android.bluetooth.device.action.BOND_STATE_CHANGED"));
        android.content.IntentFilter intent = new android.content.IntentFilter();
        intent.addAction("android.bluetooth.device.action.ACL_CONNECTED");
        intent.addAction("android.bluetooth.device.action.ACL_DISCONNECTED");
        intent.addAction(com.navdy.hud.app.device.dial.DialConstants.ACTION_CONNECTION_STATE_CHANGED);
        context.registerReceiver(this.bluetoothConnectivityReceiver, intent);
        context.registerReceiver(this.bluetoothOnOffReceiver, new android.content.IntentFilter("android.bluetooth.adapter.action.STATE_CHANGED"));
    }

    private void bondWithDial(android.bluetooth.BluetoothDevice device, android.bluetooth.le.ScanRecord scanRecord) {
        this.bondTries = 0;
        this.bondingDial = device;
        sLogger.v("[Dial]calling createBond [" + device.getName() + "] addr[" + device.getAddress() + "]" + " rssi[" + scanRecord.getTxPowerLevel() + "] state[" + com.navdy.hud.app.bluetooth.utils.BluetoothUtils.getBondState(device.getBondState()) + "]");
        boolean removed = removeFromBondList(device);
        if (removed || device.getBondState() == 12) {
            if (removed) {
                sLogger.v("[Dial]present in bond list:" + device.getName() + " invalid state");
            }
            if (device.getBondState() == 12) {
                sLogger.v("[Dial]already bonded:" + device.getName() + " invalid state");
            }
            this.bondingDial = null;
            disconectAndRemoveBond(device, new com.navdy.hud.app.device.dial.DialManager.Anon17(device), 2000);
            return;
        }
        this.bondTries++;
        this.handler.removeCallbacks(this.bondHangRunnable);
        DIAL_CONNECTING.dialName = device.getName();
        this.bus.post(DIAL_CONNECTING);
        device.createBond();
        this.handler.postDelayed(this.bondHangRunnable, 30000);
    }

    private synchronized void startGATTClient() {
        try {
            if (this.bluetoothGatt == null) {
                sLogger.v("[Dial]startGATTClient, launched gatt");
                this.isGattOn = true;
                this.bluetoothGatt = this.connectedDial.connectGatt(com.navdy.hud.app.HudApplication.getAppContext(), true, this.gattCallback);
            }
        } catch (Throwable t) {
            sLogger.e(TAG_DIAL, t);
            this.isGattOn = false;
        }
        return;
    }

    /* access modifiers changed from: private */
    public synchronized void stopGATTClient() {
        try {
            if (this.bluetoothGatt == null) {
                this.bluetoothGatt = null;
                this.isGattOn = false;
            } else {
                this.handler.removeCallbacks(this.batteryReadRunnable);
                this.bluetoothGatt.close();
                sLogger.v("[Dial]stopGATTClient, gatt closed");
                this.dialFirmwareUpdater.cancelUpdate();
                this.bluetoothGatt = null;
                this.isGattOn = false;
            }
        } catch (Throwable th) {
            this.bluetoothGatt = null;
            this.isGattOn = false;
            throw th;
        }
    }

    /* access modifiers changed from: private */
    public void disconectAndRemoveBond(android.bluetooth.BluetoothDevice device, com.navdy.hud.app.device.dial.DialManagerHelper.IDialForgotten callback, int delay) {
        com.navdy.hud.app.device.dial.DialManagerHelper.disconnectFromDial(this.bluetoothAdapter, device, new com.navdy.hud.app.device.dial.DialManager.Anon18(device, delay, callback));
    }

    /* access modifiers changed from: private */
    public void removeBond(android.bluetooth.BluetoothDevice device) {
        try {
            java.lang.reflect.Method m = device.getClass().getMethod("removeBond", null);
            if (m != null) {
                sLogger.e("[Dial]removingBond [" + device.getName() + "]");
                m.invoke(device, null);
                sLogger.e("[Dial]removedBond");
                return;
            }
            sLogger.e("[Dial]cannot get to removeBond api");
        } catch (Throwable e) {
            sLogger.e(TAG_DIAL, e);
        }
    }

    public void requestDialForgetKeys() {
        try {
            if (this.dialForgetKeysCharacteristic != null) {
                sLogger.v("Queueing dial forget keys request");
                queueWrite(this.dialForgetKeysCharacteristic, com.navdy.hud.app.device.dial.DialConstants.DIAL_FORGET_KEYS_MAGIC);
                return;
            }
            sLogger.v("Not queuing dial forget keys request: null");
        } catch (Throwable t) {
            sLogger.e(TAG_DIAL, t);
        }
    }

    public void requestDialReboot(boolean force) {
        if (force || java.lang.System.currentTimeMillis() - this.sharedPreferences.getLong(com.navdy.hud.app.device.dial.DialConstants.DIAL_REBOOT_PROPERTY, 0) >= com.navdy.hud.app.device.dial.DialConstants.MIN_TIME_BETWEEN_REBOOTS) {
            try {
                if (this.dialRebootCharacteristic != null) {
                    sLogger.v("Queueing dial reboot request");
                    queueWrite(this.dialRebootCharacteristic, com.navdy.hud.app.device.dial.DialConstants.DIAL_REBOOT_MAGIC);
                    return;
                }
                sLogger.v("Not queuing dial reboot request: null");
            } catch (Throwable t) {
                sLogger.e(TAG_DIAL, t);
            }
        } else {
            sLogger.v("Not rebooting dial, last reboot too recent.");
        }
    }

    public com.navdy.hud.app.device.dial.DialConstants.NotificationReason getBatteryNotificationReason() {
        return this.notificationReasonBattery;
    }

    public static com.navdy.hud.app.device.dial.DialConstants.NotificationReason getBatteryLevelCategory(int level) {
        if (level > 50 && level <= 80) {
            return com.navdy.hud.app.device.dial.DialConstants.NotificationReason.LOW_BATTERY;
        }
        if (level > 10 && level <= 50) {
            return com.navdy.hud.app.device.dial.DialConstants.NotificationReason.VERY_LOW_BATTERY;
        }
        if (level <= 10) {
            return com.navdy.hud.app.device.dial.DialConstants.NotificationReason.EXTREMELY_LOW_BATTERY;
        }
        return com.navdy.hud.app.device.dial.DialConstants.NotificationReason.OK_BATTERY;
    }

    public static int getDisplayBatteryLevel(int level) {
        return (int) (((float) level) * 0.1f);
    }

    /* access modifiers changed from: private */
    public void processBatteryLevel(int level) {
        com.navdy.hud.app.device.dial.DialConstants.NotificationReason reason = getBatteryLevelCategory(level);
        this.lastKnownBatteryLevel = level;
        sLogger.v("battery status current[" + reason + "] previous[" + this.notificationReasonBattery + "]");
        if (reason == com.navdy.hud.app.device.dial.DialConstants.NotificationReason.OK_BATTERY) {
            if (!(this.notificationReasonBattery == null || this.notificationReasonBattery == com.navdy.hud.app.device.dial.DialConstants.NotificationReason.OK_BATTERY)) {
                this.notificationReasonBattery = null;
            }
            com.navdy.hud.app.device.dial.DialNotification.dismissAllBatteryToasts();
        } else if (this.notificationReasonBattery == reason) {
            sLogger.v("battery no change");
        } else {
            switch (reason) {
                case EXTREMELY_LOW_BATTERY:
                    sLogger.v("battery notification change");
                    this.notificationReasonBattery = reason;
                    return;
                case LOW_BATTERY:
                    if (this.notificationReasonBattery == com.navdy.hud.app.device.dial.DialConstants.NotificationReason.EXTREMELY_LOW_BATTERY || this.notificationReasonBattery == com.navdy.hud.app.device.dial.DialConstants.NotificationReason.VERY_LOW_BATTERY) {
                        sLogger.v("battery threshold change-ignore");
                        return;
                    }
                    sLogger.v("battery notification change");
                    this.notificationReasonBattery = reason;
                    return;
                case VERY_LOW_BATTERY:
                    if (this.notificationReasonBattery == com.navdy.hud.app.device.dial.DialConstants.NotificationReason.EXTREMELY_LOW_BATTERY) {
                        sLogger.v("battery threshold change-ignore");
                        return;
                    }
                    sLogger.v("battery notification change");
                    this.notificationReasonBattery = reason;
                    return;
                default:
                    return;
            }
        }
    }

    /* access modifiers changed from: private */
    public void processRawBatteryLevel(java.lang.Integer level) {
        this.lastKnownRawBatteryLevel = level;
    }

    /* access modifiers changed from: private */
    public void processSystemTemperature(java.lang.Integer level) {
        this.lastKnownSystemTemperature = level;
    }

    public boolean isInitialized() {
        return this.dialManagerInitialized;
    }

    /* access modifiers changed from: private */
    public void handleDialConnection() {
        this.encryptionRetryCount = 0;
        com.navdy.hud.app.framework.toast.ToastManager toastManager = com.navdy.hud.app.framework.toast.ToastManager.getInstance();
        com.navdy.hud.app.device.dial.DialNotification.dismissAllBatteryToasts();
        toastManager.dismissCurrentToast(com.navdy.hud.app.device.dial.DialNotification.DIAL_DISCONNECT_ID);
        toastManager.clearPendingToast(com.navdy.hud.app.device.dial.DialNotification.DIAL_DISCONNECT_ID);
        DIAL_CONNECTED.dialName = getDialName();
        this.bus.post(DIAL_CONNECTED);
        startGATTClient();
    }

    public int getLastKnownBatteryLevel() {
        return this.lastKnownBatteryLevel;
    }

    public java.lang.Integer getLastKnownRawBatteryLevel() {
        return this.lastKnownRawBatteryLevel;
    }

    public java.lang.Integer getLastKnownSystemTemperature() {
        return this.lastKnownSystemTemperature;
    }

    public java.lang.String getDialName() {
        if (this.connectedDial != null) {
            return this.connectedDial.getName();
        }
        return null;
    }

    public boolean reportInputEvent(android.view.KeyEvent event) {
        android.view.InputDevice device = event.getDevice();
        if (device == null || device.isVirtual() || !isDialDeviceName(device.getName())) {
            return false;
        }
        if (!isDialConnected()) {
            sLogger.v("reportInputEvent:got dial connection via input:" + device.getName());
            this.connectedDial = getBluetoothDevice(device.getName());
            this.handler.removeCallbacks(this.connectionErrorRunnable);
            this.handler.removeCallbacks(this.bondHangRunnable);
            sLogger.v("reportInputEvent remove connectionErrorRunnable");
            if (this.connectedDial != null) {
                sLogger.v("reportInputEvent: got device from bonded list");
                handleDialConnection();
            } else {
                sLogger.i("reportInputEvent: did not get device from bonded list");
                buildDialList();
                this.connectedDial = getBluetoothDevice(device.getName());
                if (this.connectedDial != null) {
                    sLogger.v("reportInputEvent: got device from new bonded list");
                    handleDialConnection();
                } else {
                    sLogger.v("reportInputEvent: did not get device from new bonded list");
                }
            }
        } else {
            com.navdy.hud.app.screen.BaseScreen screen = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getUiStateManager().getCurrentScreen();
            if (screen != null && screen.getScreen() == com.navdy.service.library.events.ui.Screen.SCREEN_DIAL_PAIRING) {
                sLogger.v("reportInputEvent: sent connected event");
                DIAL_CONNECTED.dialName = getDialName();
                this.bus.post(DIAL_CONNECTED);
            }
        }
        return true;
    }

    public com.navdy.hud.app.device.dial.DialFirmwareUpdater getDialFirmwareUpdater() {
        return this.dialFirmwareUpdater;
    }

    public java.lang.String getFirmWareVersion() {
        return this.firmWareVersion;
    }

    public java.lang.String getHardwareVersion() {
        return this.hardwareVersion;
    }

    public boolean isDialConnected() {
        return this.connectedDial != null;
    }

    /* access modifiers changed from: private */
    public void clearConnectedDial() {
        this.firmWareVersion = null;
        this.hardwareVersion = null;
        this.lastKnownBatteryLevel = -1;
        this.lastKnownRawBatteryLevel = null;
        this.lastKnownSystemTemperature = null;
        this.connectedDial = null;
    }

    public boolean isDialPaired() {
        return getBondedDialCount() > 0;
    }

    public int getBondedDialCount() {
        int size;
        synchronized (this.bondedDials) {
            size = this.bondedDials.size();
        }
        return size;
    }

    public java.lang.String getBondedDialList() {
        java.lang.StringBuilder builder = new java.lang.StringBuilder();
        synchronized (this.bondedDials) {
            int len = this.bondedDials.size();
            for (int i = 0; i < len; i++) {
                builder.append(((android.bluetooth.BluetoothDevice) this.bondedDials.get(i)).getName());
                if (i + 1 < len) {
                    builder.append(", ");
                }
            }
        }
        return builder.toString();
    }

    private android.bluetooth.BluetoothDevice getBluetoothDevice(java.lang.String name) {
        synchronized (this.bondedDials) {
            java.util.Iterator it = this.bondedDials.iterator();
            while (it.hasNext()) {
                android.bluetooth.BluetoothDevice device = (android.bluetooth.BluetoothDevice) it.next();
                if (android.text.TextUtils.equals(device.getName(), name)) {
                    return device;
                }
            }
            return null;
        }
    }

    public boolean isDialDevice(android.bluetooth.BluetoothDevice device) {
        if (device == null) {
            return false;
        }
        java.lang.String name = device.getName();
        if (name == null || !isDialDeviceName(name)) {
            return false;
        }
        return true;
    }

    public static boolean isDialDeviceName(java.lang.String name) {
        if (name == null) {
            return false;
        }
        for (java.lang.String contains : com.navdy.hud.app.device.dial.DialConstants.NAVDY_DIAL_FILTER) {
            if (name.contains(contains)) {
                return true;
            }
        }
        return false;
    }

    public void forgetAllDials(boolean showToast, com.navdy.hud.app.device.dial.DialManagerHelper.IDialForgotten callback) {
        boolean z = true;
        int count = getBondedDialCount();
        java.lang.String dialName = getBondedDialList();
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.device.dial.DialManager.Anon19(callback), 1);
        if (showToast) {
            com.navdy.hud.app.framework.toast.ToastManager toastManager = com.navdy.hud.app.framework.toast.ToastManager.getInstance();
            toastManager.dismissCurrentToast();
            toastManager.clearAllPendingToast();
            toastManager.disableToasts(false);
            if (count <= 1) {
                z = false;
            }
            com.navdy.hud.app.device.dial.DialNotification.showForgottenToast(z, dialName);
        }
    }

    public void forgetDial(android.bluetooth.BluetoothDevice dial) {
        removeBond(dial);
        removeFromBondList(dial);
    }

    public void forgetAllDials(com.navdy.hud.app.device.dial.DialManagerHelper.IDialForgotten callback) {
        requestDialForgetKeys();
        this.handler.removeCallbacks(this.bondHangRunnable);
        synchronized (this.bondedDials) {
            sLogger.v("forget all dials:" + this.bondedDials.size());
            java.util.ArrayList<android.bluetooth.BluetoothDevice> copy = new java.util.ArrayList<>(this.bondedDials);
            this.bondedDials.clear();
            clearConnectedDial();
            java.util.concurrent.atomic.AtomicInteger counter = new java.util.concurrent.atomic.AtomicInteger(copy.size());
            java.util.Iterator it = copy.iterator();
            while (it.hasNext()) {
                disconectAndRemoveBond((android.bluetooth.BluetoothDevice) it.next(), new com.navdy.hud.app.device.dial.DialManager.Anon20(counter, callback), 2000);
            }
        }
    }

    public boolean isScanning() {
        return this.isScanning;
    }

    /* access modifiers changed from: private */
    public boolean addtoBondList(android.bluetooth.BluetoothDevice device) {
        boolean z = false;
        if (device != null) {
            synchronized (this.bondedDials) {
                boolean found = false;
                java.util.Iterator it = this.bondedDials.iterator();
                while (it.hasNext()) {
                    if (((android.bluetooth.BluetoothDevice) it.next()).equals(device)) {
                        found = true;
                    }
                }
                if (!found) {
                    this.bondedDials.add(device);
                    sLogger.v("bonded device added to list [" + device.getName() + "]");
                    z = true;
                }
            }
        }
        return z;
    }

    private boolean removeFromBondList(android.bluetooth.BluetoothDevice device) {
        boolean z = false;
        if (device != null) {
            synchronized (this.bondedDials) {
                boolean found = false;
                java.util.Iterator it = this.bondedDials.iterator();
                while (it.hasNext()) {
                    if (((android.bluetooth.BluetoothDevice) it.next()).equals(device)) {
                        found = true;
                    }
                }
                if (found) {
                    this.bondedDials.remove(device);
                    z = true;
                }
            }
        }
        return z;
    }

    /* access modifiers changed from: private */
    public boolean isPresentInBondList(android.bluetooth.BluetoothDevice device) {
        boolean z = false;
        if (device != null) {
            synchronized (this.bondedDials) {
                java.util.Iterator it = this.bondedDials.iterator();
                while (true) {
                    if (it.hasNext()) {
                        if (((android.bluetooth.BluetoothDevice) it.next()).equals(device)) {
                            z = true;
                            break;
                        }
                    } else {
                        break;
                    }
                }
            }
        }
        return z;
    }

    private synchronized void startDialEventListener() {
        if (this.dialEventsListenerThread == null) {
            this.dialEventsListenerThread = new java.lang.Thread(new com.navdy.hud.app.device.dial.DialManager.Anon21());
            this.dialEventsListenerThread.setName("dialEventListener");
            this.dialEventsListenerThread.start();
            sLogger.v("dialEventListener thread started");
        }
    }

    /* access modifiers changed from: private */
    public void connectEvent(android.bluetooth.BluetoothDevice device) {
        sLogger.v("[DialEvent-connect] " + device);
        this.handler.removeCallbacks(this.bondHangRunnable);
        this.handler.removeCallbacks(this.connectionErrorRunnable);
        if (this.connectedDial != null) {
            sLogger.v("[DialEvent-connect] ignore already connected dial:" + this.connectedDial);
        } else if (isPresentInBondList(device)) {
            sLogger.v("[DialEvent-connect] not on dial pairing screen, mark as connected");
            this.connectedDial = device;
            handleDialConnection();
        } else {
            sLogger.v("launch connectionErrorRunnable");
            this.tempConnectedDial = device;
            this.handler.postDelayed(this.connectionErrorRunnable, 5000);
        }
    }

    /* access modifiers changed from: private */
    public void handleInputReportDescriptor(android.bluetooth.BluetoothDevice device) {
        this.handler.removeCallbacks(this.connectionErrorRunnable);
        sLogger.v("[DialEvent-handleInputReportDescriptor] " + device);
        if (this.tempConnectedDial == null) {
            sLogger.v("[DialEvent-handleInputReportDescriptor] no temp connected dial");
        } else if (this.connectedDial != null) {
            sLogger.v("[DialEvent-handleInputReportDescriptor] dial already connected[" + this.connectedDial + "]");
            if (this.connectedDial.equals(device)) {
                DIAL_CONNECTED.dialName = getDialName();
                this.bus.post(DIAL_CONNECTED);
            }
        } else {
            sLogger.v("[DialEvent-handleInputReportDescriptor] done");
            this.tempConnectedDial = null;
            this.connectedDial = device;
            handleDialConnection();
        }
    }

    /* access modifiers changed from: private */
    public void disconnectEvent(android.bluetooth.BluetoothDevice device) {
        this.handler.removeCallbacks(this.bondHangRunnable);
        this.handler.removeCallbacks(this.connectionErrorRunnable);
        this.tempConnectedDial = null;
        if (this.connectedDial == null) {
            sLogger.v("[DialEvent-disconnect] no connected dial");
        } else if (!this.connectedDial.equals(device)) {
            sLogger.v("[DialEvent-disconnect] notif not for connected dial connected[" + this.connectedDial + "]");
        } else {
            this.disconnectedDial = this.connectedDial.getName();
            clearConnectedDial();
            stopGATTClient();
            DIAL_NOT_CONNECTED.dialName = this.disconnectedDial;
            this.bus.post(DIAL_NOT_CONNECTED);
            sLogger.v("[DialEvent-disconnect] dial marked not connected");
        }
    }

    /* access modifiers changed from: private */
    public void encryptionFailedEvent(android.bluetooth.BluetoothDevice device) {
        if (!isPresentInBondList(device)) {
            this.encryptionRetryCount = 0;
            sLogger.e("[DialEventsListener] device not in bonded dial list");
        } else if (this.encryptionRetryCount < 2) {
            this.encryptionRetryCount++;
            sLogger.e("[DialEvent-encryptfail] Attempt " + this.encryptionRetryCount + " will retry.");
            stopGATTClient();
            com.navdy.hud.app.util.GenericUtil.sleep(2000);
            com.navdy.hud.app.device.dial.DialManagerHelper.disconnectFromDial(this.bluetoothAdapter, device, new com.navdy.hud.app.device.dial.DialManager.Anon22());
        } else {
            removeFromBondList(device);
            sLogger.e("[DialEvent-encryptfail] device found in bonded dial list, stop gatt");
            stopGATTClient();
            sLogger.e("[DialEvent-encryptfail] sleeping");
            com.navdy.hud.app.util.GenericUtil.sleep(2000);
            sLogger.e("[DialEvent-encryptfail] sleep");
            com.navdy.hud.app.device.dial.DialManagerHelper.disconnectFromDial(this.bluetoothAdapter, device, new com.navdy.hud.app.device.dial.DialManager.Anon23(device));
        }
    }
}
