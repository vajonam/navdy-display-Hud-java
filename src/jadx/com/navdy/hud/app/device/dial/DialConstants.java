package com.navdy.hud.app.device.dial;

public class DialConstants {
    public static final java.lang.String ACTION_CONNECTION_STATE_CHANGED = "android.bluetooth.input.profile.action.CONNECTION_STATE_CHANGED";
    static final com.navdy.hud.app.device.dial.DialConstants.BatteryChangeEvent BATTERY_CHANGE_EVENT = new com.navdy.hud.app.device.dial.DialConstants.BatteryChangeEvent();
    static final int BATTERY_CHECK_INTERVAL = 300000;
    static final float BATTERY_DISPLAY_SCALE = 0.1f;
    public static final int BATTERY_LEVEL_UNKNOWN = -1;
    static final java.util.UUID BATTERY_LEVEL_UUID = java.util.UUID.fromString("00002a19-0000-1000-8000-00805f9b34fb");
    static final java.util.UUID BATTERY_SERVICE_UUID = java.util.UUID.fromString("0000180F-0000-1000-8000-00805f9b34fb");
    static final java.lang.String CONNECT = "CONNECT";
    static final int CONNECTION_NOT_DONE_TIMEOUT = 5000;
    static final java.util.UUID DEVICE_INFO_FIRMWARE_VERSION_UUID = java.util.UUID.fromString("00002a26-0000-1000-8000-00805f9b34fb");
    static final java.util.UUID DEVICE_INFO_HARDWARE_VERSION_UUID = java.util.UUID.fromString("00002a27-0000-1000-8000-00805f9b34fb");
    static final java.util.UUID DEVICE_INFO_SERVICE_UUID = java.util.UUID.fromString("0000180a-0000-1000-8000-00805f9b34fb");
    public static final java.lang.String DIAL_ADDRESS = "Dial_Address";
    public static final java.lang.String DIAL_BATTERY_LEVEL = "Battery_Level";
    static final int DIAL_EVENTS_PORT = 23654;
    public static final java.lang.String DIAL_EVENT_CONNECT = "Connect";
    public static final java.lang.String DIAL_EVENT_INPUT_REPORT_DESCRIPTOR = "INPUTDESCRIPTOR_WRITTEN";
    public static final java.lang.String DIAL_EVENT_PAIR = "Pair_Attempt";
    public static final java.lang.String DIAL_EVENT_REBOOT = "Reboot";
    public static final java.lang.String DIAL_EVENT_TYPE = "Type";
    static final java.lang.String DIAL_EXTREMELY_LOW_BATTERY_PROPERTY = "last_extremely_low_battery_notification";
    public static final java.lang.String DIAL_FIRST_TIME = "First_Time";
    static final byte[] DIAL_FORGET_KEYS_MAGIC = "DIE".getBytes();
    static final java.util.UUID DIAL_FORGET_KEYS_UUID = java.util.UUID.fromString("87c72b94-46fc-44ae-bd8d-d307bceb3b9f");
    public static final java.lang.String DIAL_FW_VERSION = "FW_Version";
    public static final java.lang.String DIAL_HW_VERSION = "HW_Version";
    public static final java.lang.String DIAL_INCREMENTAL_VERSION = "Incremental_Version";
    public static final java.lang.String DIAL_PAIRING_EVENT = "Dial_Pairing";
    public static final java.lang.String DIAL_PAIRING_SUCCESS = "Success";
    public static final java.lang.String DIAL_RAW_BATTERY_LEVEL = "Raw_Battery_Level";
    static final byte[] DIAL_REBOOT_MAGIC = "REBOOT".getBytes();
    public static final java.lang.String DIAL_REBOOT_PROPERTY = "last_dial_reboot";
    static final java.util.UUID DIAL_REBOOT_UUID = java.util.UUID.fromString("87c72b00-46fc-44ae-bd8d-d307bceb3b9f");
    public static final java.lang.String DIAL_RESULT = "Result";
    public static final java.lang.String DIAL_TEMPERATURE = "System_Temperature";
    static final java.lang.String DIAL_VERY_LOW_BATTERY_PROPERTY = "last_very_low_battery_notification";
    static final java.lang.String DISCONNECT = "DISCONNECT";
    static final int DISCONNECTED_THRESHOLD = 5000;
    static final java.lang.String ENCRYPTION_FAILED = "ENCRYPTION_FAILED";
    static final int EXTREMELY_LOW_BATTERY = 10;
    static final java.util.UUID FIRMWARE_REVISION_CHARACTERISTIC_UUID = java.util.UUID.fromString("00002a26-0000-1000-8000-00805f9b34fb");
    public static final int GATT_READ_DELAY = 5000;
    static final java.util.UUID HID_HOST_READY_UUID = java.util.UUID.fromString("40cc0c40-3caa-11e6-b55c-0002a5d5c51b");
    static final int HID_PROFILE = 4;
    static final java.util.UUID HID_SERVICE_UUID = java.util.UUID.fromString("00001812-0000-1000-8000-00805f9b34fb");
    static final com.navdy.hud.app.device.dial.DialConstants.DialManagerInitEvent INIT_EVENT = new com.navdy.hud.app.device.dial.DialConstants.DialManagerInitEvent();
    static final int LOW_BATTERY = 80;
    static final int MAXIMUM_BOND_TIME = 30000;
    static final int MAX_BOND_RETRIES = 3;
    static final int MAX_ENCRYPTION_RETRY = 2;
    static final long MIN_TIME_BETWEEN_EXTREMELY_LOW_WARNINGS = java.util.concurrent.TimeUnit.DAYS.toMillis(30);
    static final long MIN_TIME_BETWEEN_REBOOTS = java.util.concurrent.TimeUnit.DAYS.toMillis(30);
    static final long MIN_TIME_BETWEEN_VERY_LOW_WARNINGS = java.util.concurrent.TimeUnit.DAYS.toMillis(7);
    static final java.lang.String[] NAVDY_DIAL_FILTER = {"Navdy Dial"};
    static final java.util.UUID OTA_CONTROL_CHARACTERISTIC_UUID = java.util.UUID.fromString("f7bf3564-fb6d-4e53-88a4-5e37e0326063");
    static final java.util.UUID OTA_DATA_CHARACTERISTIC_UUID = java.util.UUID.fromString("984227f3-34fc-4045-a5d0-2c581f81a153");
    public static final java.lang.String OTA_DIAL_NAME_KEY = "OtaDialNameKey";
    static final java.util.UUID OTA_INCREMENTAL_VERSION_CHARACTERISTIC_UUID = java.util.UUID.fromString("2a9e7517-f977-4775-a993-b827ae0fff34");
    static final java.util.UUID OTA_SERVICE_UUID = java.util.UUID.fromString("1d14d6ee-fd63-4fa1-bfa4-8f47b42119f0");
    static final int PAIRING_RETRY_DELAY = 4000;
    static final com.navdy.hud.app.device.dial.DialConstants.PairingRule PAIRING_RULE = com.navdy.hud.app.device.dial.DialConstants.PairingRule.FIRST;
    static final java.util.UUID RAW_BATTERY_LEVEL_UUID = java.util.UUID.fromString("40cc0c40-3caa-11e6-b55c-0002a5d5c51c");
    static final int SCAN_HANG_TIME = 30000;
    static final java.util.UUID SYSTEM_TEMPERATURE_UUID = java.util.UUID.fromString("00002a6e-0000-1000-8000-00805f9b34fb");
    public static final java.lang.String VERSION_PREFIX = "1.0.";
    static final int VERY_LOW_BATTERY = 50;
    public static final java.lang.String VIDEO_SHOWN_PREF = "first_run_dial_video_shown";

    public static class BatteryChangeEvent {
    }

    public static class DialConnectionStatus {
        public java.lang.String dialName;
        public com.navdy.hud.app.device.dial.DialConstants.DialConnectionStatus.Status status;

        public enum Status {
            CONNECTED,
            DISCONNECTED,
            CONNECTING,
            CONNECTION_FAILED,
            NO_DIAL_FOUND
        }

        DialConnectionStatus(com.navdy.hud.app.device.dial.DialConstants.DialConnectionStatus.Status status2) {
            this.status = status2;
        }
    }

    public static class DialManagerInitEvent {
    }

    public enum NotificationReason {
        DISCONNECTED,
        CONNECTED,
        LOW_BATTERY,
        VERY_LOW_BATTERY,
        EXTREMELY_LOW_BATTERY,
        OK_BATTERY
    }

    enum PairingRule {
        FIRST
    }
}
