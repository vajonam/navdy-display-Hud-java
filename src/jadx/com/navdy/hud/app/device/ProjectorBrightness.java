package com.navdy.hud.app.device;

public class ProjectorBrightness {
    private static final java.util.List<java.lang.String> AUTO_BRIGHTNESS_MAPPING = readAutoBrightnessMapping();
    private static final java.util.List<java.lang.String> AUTO_BRIGHTNESS_MAPPING_FILES = new com.navdy.hud.app.device.ProjectorBrightness.Anon1();
    private static final int BRIGHTNESS_SCALE_VALUES_COUNT = 256;
    private static final java.lang.String DATA_FILE = "/sys/dlpc/RGB_Brightness";
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.device.ProjectorBrightness.class);

    static class Anon1 extends java.util.ArrayList<java.lang.String> {
        Anon1() {
            add("/device/etc/calibration/dlpc_brightness");
            add("/system/etc/calibration/dlpc_brightness");
        }
    }

    public static void init() {
    }

    private static java.util.List<java.lang.String> readAutoBrightnessMapping() {
        java.util.List<java.lang.String> result = java.util.Collections.emptyList();
        java.lang.String suffix = "_v4";
        if (com.navdy.hud.app.util.SerialNumber.instance.revisionCode.equals(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_1_STYLE)) {
            suffix = "_v3";
        }
        java.util.Iterator it = AUTO_BRIGHTNESS_MAPPING_FILES.iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            java.lang.String file = ((java.lang.String) it.next()) + suffix;
            result = new java.util.ArrayList<>(256);
            java.io.FileInputStream fileIS = null;
            java.io.BufferedReader reader = null;
            try {
                java.io.FileInputStream fileIS2 = new java.io.FileInputStream(file);
                try {
                    java.io.BufferedReader reader2 = new java.io.BufferedReader(new java.io.InputStreamReader(fileIS2));
                    try {
                        for (java.lang.String line = reader2.readLine(); line != null && result.size() < 256; line = reader2.readLine()) {
                            result.add(line.trim());
                        }
                        if (result.size() >= 256) {
                            sLogger.i("Successfully read mapping from file " + file);
                            com.navdy.service.library.util.IOUtils.closeStream(reader2);
                            com.navdy.service.library.util.IOUtils.closeStream(fileIS2);
                            break;
                        }
                        sLogger.w("File " + file + " not complete - skipping it");
                        com.navdy.service.library.util.IOUtils.closeStream(reader2);
                        com.navdy.service.library.util.IOUtils.closeStream(fileIS2);
                        java.io.BufferedReader bufferedReader = reader2;
                        java.io.FileInputStream fileInputStream = fileIS2;
                    } catch (java.io.FileNotFoundException e) {
                        reader = reader2;
                        fileIS = fileIS2;
                    } catch (java.io.IOException e2) {
                        e = e2;
                        reader = reader2;
                        fileIS = fileIS2;
                        sLogger.e("Cannot read auto-brightness mapping file " + file + " : " + e.getMessage());
                        com.navdy.service.library.util.IOUtils.closeStream(reader);
                        com.navdy.service.library.util.IOUtils.closeStream(fileIS);
                    } catch (Throwable th) {
                        th = th;
                        reader = reader2;
                        fileIS = fileIS2;
                        com.navdy.service.library.util.IOUtils.closeStream(reader);
                        com.navdy.service.library.util.IOUtils.closeStream(fileIS);
                        throw th;
                    }
                } catch (java.io.FileNotFoundException e3) {
                    fileIS = fileIS2;
                    try {
                        sLogger.w(file + " not found - checking next file");
                        com.navdy.service.library.util.IOUtils.closeStream(reader);
                        com.navdy.service.library.util.IOUtils.closeStream(fileIS);
                    } catch (Throwable th2) {
                        th = th2;
                        com.navdy.service.library.util.IOUtils.closeStream(reader);
                        com.navdy.service.library.util.IOUtils.closeStream(fileIS);
                        throw th;
                    }
                } catch (java.io.IOException e4) {
                    e = e4;
                    fileIS = fileIS2;
                    sLogger.e("Cannot read auto-brightness mapping file " + file + " : " + e.getMessage());
                    com.navdy.service.library.util.IOUtils.closeStream(reader);
                    com.navdy.service.library.util.IOUtils.closeStream(fileIS);
                } catch (Throwable th3) {
                    th = th3;
                    fileIS = fileIS2;
                    com.navdy.service.library.util.IOUtils.closeStream(reader);
                    com.navdy.service.library.util.IOUtils.closeStream(fileIS);
                    throw th;
                }
            } catch (java.io.FileNotFoundException e5) {
                sLogger.w(file + " not found - checking next file");
                com.navdy.service.library.util.IOUtils.closeStream(reader);
                com.navdy.service.library.util.IOUtils.closeStream(fileIS);
            } catch (java.io.IOException e6) {
                e = e6;
                sLogger.e("Cannot read auto-brightness mapping file " + file + " : " + e.getMessage());
                com.navdy.service.library.util.IOUtils.closeStream(reader);
                com.navdy.service.library.util.IOUtils.closeStream(fileIS);
            }
        }
        if (result.size() >= 256) {
            return result;
        }
        sLogger.e("No auto-brightness mapping file found");
        return java.util.Collections.emptyList();
    }

    public static int getValue() throws java.io.IOException {
        try {
            return getValue(com.navdy.service.library.util.IOUtils.convertFileToString(DATA_FILE).trim());
        } catch (java.lang.NumberFormatException e) {
            sLogger.e("Exception while reading auto-brightness value", e);
            throw new java.io.IOException("Corrupted data in file with auto-brightness value");
        }
    }

    protected static int getValue(java.lang.String readLine) {
        int value = AUTO_BRIGHTNESS_MAPPING.indexOf(readLine);
        if (value >= 0) {
            return value;
        }
        sLogger.e("Cannot get brightness value for auto-brightness string '" + readLine + "'");
        return 0;
    }
}
