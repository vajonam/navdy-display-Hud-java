package com.navdy.hud.app.debug;

public class RouteRecorderService extends android.app.Service {
    /* access modifiers changed from: private */
    public com.navdy.hud.app.debug.RouteRecorder mRouteRecorder = com.navdy.hud.app.debug.RouteRecorder.getInstance();

    class Anon1 extends com.navdy.hud.app.debug.IRouteRecorder.Stub {
        Anon1() {
        }

        public void prepare(java.lang.String fileName, boolean secondary) throws android.os.RemoteException {
            com.navdy.hud.app.debug.RouteRecorderService.this.mRouteRecorder.prepare(fileName, secondary);
        }

        public java.lang.String startRecording(java.lang.String label) throws android.os.RemoteException {
            return com.navdy.hud.app.debug.RouteRecorderService.this.mRouteRecorder.startRecording(label, true);
        }

        public void stopRecording() throws android.os.RemoteException {
            com.navdy.hud.app.debug.RouteRecorderService.this.mRouteRecorder.stopRecording(true);
        }

        public boolean isRecording() throws android.os.RemoteException {
            return com.navdy.hud.app.debug.RouteRecorderService.this.mRouteRecorder.isRecording();
        }

        public void startPlayback(java.lang.String fileName, boolean secondary, boolean loop) throws android.os.RemoteException {
            com.navdy.hud.app.debug.RouteRecorderService.this.mRouteRecorder.startPlayback(fileName, secondary, loop, true);
        }

        public void pausePlayback() throws android.os.RemoteException {
            com.navdy.hud.app.debug.RouteRecorderService.this.mRouteRecorder.pausePlayback();
        }

        public void resumePlayback() throws android.os.RemoteException {
            com.navdy.hud.app.debug.RouteRecorderService.this.mRouteRecorder.resumePlayback();
        }

        public void stopPlayback() throws android.os.RemoteException {
            com.navdy.hud.app.debug.RouteRecorderService.this.mRouteRecorder.stopPlayback();
        }

        public boolean restartPlayback() throws android.os.RemoteException {
            return com.navdy.hud.app.debug.RouteRecorderService.this.mRouteRecorder.restartPlayback();
        }

        public boolean isPlaying() throws android.os.RemoteException {
            return com.navdy.hud.app.debug.RouteRecorderService.this.mRouteRecorder.isPlaying();
        }

        public boolean isPaused() throws android.os.RemoteException {
            return com.navdy.hud.app.debug.RouteRecorderService.this.mRouteRecorder.isPaused();
        }

        public boolean isStopped() throws android.os.RemoteException {
            return com.navdy.hud.app.debug.RouteRecorderService.this.mRouteRecorder.isStopped();
        }
    }

    public android.os.IBinder onBind(android.content.Intent intent) {
        return new com.navdy.hud.app.debug.RouteRecorderService.Anon1();
    }
}
