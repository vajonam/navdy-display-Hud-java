package com.navdy.hud.app.debug;

public interface IRouteRecorder extends android.os.IInterface {

    public static abstract class Stub extends android.os.Binder implements com.navdy.hud.app.debug.IRouteRecorder {
        private static final java.lang.String DESCRIPTOR = "com.navdy.hud.app.debug.IRouteRecorder";
        static final int TRANSACTION_isPaused = 11;
        static final int TRANSACTION_isPlaying = 10;
        static final int TRANSACTION_isRecording = 4;
        static final int TRANSACTION_isStopped = 12;
        static final int TRANSACTION_pausePlayback = 6;
        static final int TRANSACTION_prepare = 1;
        static final int TRANSACTION_restartPlayback = 9;
        static final int TRANSACTION_resumePlayback = 7;
        static final int TRANSACTION_startPlayback = 5;
        static final int TRANSACTION_startRecording = 2;
        static final int TRANSACTION_stopPlayback = 8;
        static final int TRANSACTION_stopRecording = 3;

        private static class Proxy implements com.navdy.hud.app.debug.IRouteRecorder {
            private android.os.IBinder mRemote;

            Proxy(android.os.IBinder remote) {
                this.mRemote = remote;
            }

            public android.os.IBinder asBinder() {
                return this.mRemote;
            }

            public java.lang.String getInterfaceDescriptor() {
                return com.navdy.hud.app.debug.IRouteRecorder.Stub.DESCRIPTOR;
            }

            public void prepare(java.lang.String fileName, boolean secondary) throws android.os.RemoteException {
                int i = 1;
                android.os.Parcel _data = android.os.Parcel.obtain();
                android.os.Parcel _reply = android.os.Parcel.obtain();
                try {
                    _data.writeInterfaceToken(com.navdy.hud.app.debug.IRouteRecorder.Stub.DESCRIPTOR);
                    _data.writeString(fileName);
                    if (!secondary) {
                        i = 0;
                    }
                    _data.writeInt(i);
                    this.mRemote.transact(1, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public java.lang.String startRecording(java.lang.String label) throws android.os.RemoteException {
                android.os.Parcel _data = android.os.Parcel.obtain();
                android.os.Parcel _reply = android.os.Parcel.obtain();
                try {
                    _data.writeInterfaceToken(com.navdy.hud.app.debug.IRouteRecorder.Stub.DESCRIPTOR);
                    _data.writeString(label);
                    this.mRemote.transact(2, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readString();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void stopRecording() throws android.os.RemoteException {
                android.os.Parcel _data = android.os.Parcel.obtain();
                android.os.Parcel _reply = android.os.Parcel.obtain();
                try {
                    _data.writeInterfaceToken(com.navdy.hud.app.debug.IRouteRecorder.Stub.DESCRIPTOR);
                    this.mRemote.transact(3, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean isRecording() throws android.os.RemoteException {
                boolean _result = false;
                android.os.Parcel _data = android.os.Parcel.obtain();
                android.os.Parcel _reply = android.os.Parcel.obtain();
                try {
                    _data.writeInterfaceToken(com.navdy.hud.app.debug.IRouteRecorder.Stub.DESCRIPTOR);
                    this.mRemote.transact(4, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void startPlayback(java.lang.String fileName, boolean secondary, boolean loop) throws android.os.RemoteException {
                int i = 1;
                android.os.Parcel _data = android.os.Parcel.obtain();
                android.os.Parcel _reply = android.os.Parcel.obtain();
                try {
                    _data.writeInterfaceToken(com.navdy.hud.app.debug.IRouteRecorder.Stub.DESCRIPTOR);
                    _data.writeString(fileName);
                    _data.writeInt(secondary ? 1 : 0);
                    if (!loop) {
                        i = 0;
                    }
                    _data.writeInt(i);
                    this.mRemote.transact(5, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void pausePlayback() throws android.os.RemoteException {
                android.os.Parcel _data = android.os.Parcel.obtain();
                android.os.Parcel _reply = android.os.Parcel.obtain();
                try {
                    _data.writeInterfaceToken(com.navdy.hud.app.debug.IRouteRecorder.Stub.DESCRIPTOR);
                    this.mRemote.transact(6, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void resumePlayback() throws android.os.RemoteException {
                android.os.Parcel _data = android.os.Parcel.obtain();
                android.os.Parcel _reply = android.os.Parcel.obtain();
                try {
                    _data.writeInterfaceToken(com.navdy.hud.app.debug.IRouteRecorder.Stub.DESCRIPTOR);
                    this.mRemote.transact(7, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void stopPlayback() throws android.os.RemoteException {
                android.os.Parcel _data = android.os.Parcel.obtain();
                android.os.Parcel _reply = android.os.Parcel.obtain();
                try {
                    _data.writeInterfaceToken(com.navdy.hud.app.debug.IRouteRecorder.Stub.DESCRIPTOR);
                    this.mRemote.transact(8, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean restartPlayback() throws android.os.RemoteException {
                boolean _result = false;
                android.os.Parcel _data = android.os.Parcel.obtain();
                android.os.Parcel _reply = android.os.Parcel.obtain();
                try {
                    _data.writeInterfaceToken(com.navdy.hud.app.debug.IRouteRecorder.Stub.DESCRIPTOR);
                    this.mRemote.transact(9, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean isPlaying() throws android.os.RemoteException {
                boolean _result = false;
                android.os.Parcel _data = android.os.Parcel.obtain();
                android.os.Parcel _reply = android.os.Parcel.obtain();
                try {
                    _data.writeInterfaceToken(com.navdy.hud.app.debug.IRouteRecorder.Stub.DESCRIPTOR);
                    this.mRemote.transact(10, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean isPaused() throws android.os.RemoteException {
                boolean _result = false;
                android.os.Parcel _data = android.os.Parcel.obtain();
                android.os.Parcel _reply = android.os.Parcel.obtain();
                try {
                    _data.writeInterfaceToken(com.navdy.hud.app.debug.IRouteRecorder.Stub.DESCRIPTOR);
                    this.mRemote.transact(11, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean isStopped() throws android.os.RemoteException {
                boolean _result = false;
                android.os.Parcel _data = android.os.Parcel.obtain();
                android.os.Parcel _reply = android.os.Parcel.obtain();
                try {
                    _data.writeInterfaceToken(com.navdy.hud.app.debug.IRouteRecorder.Stub.DESCRIPTOR);
                    this.mRemote.transact(12, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }
        }

        public Stub() {
            attachInterface(this, DESCRIPTOR);
        }

        public static com.navdy.hud.app.debug.IRouteRecorder asInterface(android.os.IBinder obj) {
            if (obj == null) {
                return null;
            }
            android.os.IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
            if (iin == null || !(iin instanceof com.navdy.hud.app.debug.IRouteRecorder)) {
                return new com.navdy.hud.app.debug.IRouteRecorder.Stub.Proxy(obj);
            }
            return (com.navdy.hud.app.debug.IRouteRecorder) iin;
        }

        public android.os.IBinder asBinder() {
            return this;
        }

        public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException {
            boolean _arg1;
            boolean _arg2;
            boolean _arg12;
            int i = 0;
            switch (code) {
                case 1:
                    data.enforceInterface(DESCRIPTOR);
                    java.lang.String _arg0 = data.readString();
                    if (data.readInt() != 0) {
                        _arg12 = true;
                    } else {
                        _arg12 = false;
                    }
                    prepare(_arg0, _arg12);
                    reply.writeNoException();
                    return true;
                case 2:
                    data.enforceInterface(DESCRIPTOR);
                    java.lang.String _result = startRecording(data.readString());
                    reply.writeNoException();
                    reply.writeString(_result);
                    return true;
                case 3:
                    data.enforceInterface(DESCRIPTOR);
                    stopRecording();
                    reply.writeNoException();
                    return true;
                case 4:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result2 = isRecording();
                    reply.writeNoException();
                    if (_result2) {
                        i = 1;
                    }
                    reply.writeInt(i);
                    return true;
                case 5:
                    data.enforceInterface(DESCRIPTOR);
                    java.lang.String _arg02 = data.readString();
                    if (data.readInt() != 0) {
                        _arg1 = true;
                    } else {
                        _arg1 = false;
                    }
                    if (data.readInt() != 0) {
                        _arg2 = true;
                    } else {
                        _arg2 = false;
                    }
                    startPlayback(_arg02, _arg1, _arg2);
                    reply.writeNoException();
                    return true;
                case 6:
                    data.enforceInterface(DESCRIPTOR);
                    pausePlayback();
                    reply.writeNoException();
                    return true;
                case 7:
                    data.enforceInterface(DESCRIPTOR);
                    resumePlayback();
                    reply.writeNoException();
                    return true;
                case 8:
                    data.enforceInterface(DESCRIPTOR);
                    stopPlayback();
                    reply.writeNoException();
                    return true;
                case 9:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result3 = restartPlayback();
                    reply.writeNoException();
                    if (_result3) {
                        i = 1;
                    }
                    reply.writeInt(i);
                    return true;
                case 10:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result4 = isPlaying();
                    reply.writeNoException();
                    if (_result4) {
                        i = 1;
                    }
                    reply.writeInt(i);
                    return true;
                case 11:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result5 = isPaused();
                    reply.writeNoException();
                    if (_result5) {
                        i = 1;
                    }
                    reply.writeInt(i);
                    return true;
                case 12:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result6 = isStopped();
                    reply.writeNoException();
                    if (_result6) {
                        i = 1;
                    }
                    reply.writeInt(i);
                    return true;
                case 1598968902:
                    reply.writeString(DESCRIPTOR);
                    return true;
                default:
                    return super.onTransact(code, data, reply, flags);
            }
        }
    }

    boolean isPaused() throws android.os.RemoteException;

    boolean isPlaying() throws android.os.RemoteException;

    boolean isRecording() throws android.os.RemoteException;

    boolean isStopped() throws android.os.RemoteException;

    void pausePlayback() throws android.os.RemoteException;

    void prepare(java.lang.String str, boolean z) throws android.os.RemoteException;

    boolean restartPlayback() throws android.os.RemoteException;

    void resumePlayback() throws android.os.RemoteException;

    void startPlayback(java.lang.String str, boolean z, boolean z2) throws android.os.RemoteException;

    java.lang.String startRecording(java.lang.String str) throws android.os.RemoteException;

    void stopPlayback() throws android.os.RemoteException;

    void stopRecording() throws android.os.RemoteException;
}
