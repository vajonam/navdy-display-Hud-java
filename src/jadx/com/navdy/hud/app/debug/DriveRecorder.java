package com.navdy.hud.app.debug;

public class DriveRecorder implements android.content.ServiceConnection, com.navdy.hud.app.debug.SerialExecutor {
    private static final java.lang.String AUTO_DRIVERECORD_PROP = "persist.sys.driverecord.auto";
    public static final java.lang.String AUTO_RECORD_LABEL = "auto";
    public static final int BUFFER_SIZE = 500;
    public static final int BUFFER_SIZE_OBD_LOG = 250;
    public static final java.lang.String DEMO_LOG_FILE_NAME = "demo_log.log";
    public static final java.lang.String DRIVE_LOGS_FOLDER = "drive_logs";
    public static final int MAXIMUM_FILES_IN_THE_FOLDER = 20;
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.debug.DriveRecorder.class);
    com.squareup.otto.Bus bus;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.service.ConnectionHandler connectionHandler;
    private final android.content.Context context = com.navdy.hud.app.HudApplication.getAppContext();
    /* access modifiers changed from: private */
    public volatile int currentObdDataInjectionIndex;
    boolean demoObdLogFileExists = false;
    boolean demoRouteLogFileExists = false;
    com.navdy.hud.app.debug.AsyncBufferedFileWriter driveScoreDataBufferedFileWriter;
    private java.lang.StringBuilder gforceDataBuilder = new java.lang.StringBuilder(40);
    private final android.os.Handler handler = new android.os.Handler(android.os.Looper.getMainLooper());
    /* access modifiers changed from: private */
    public final java.lang.Runnable injectFakeObdData = new com.navdy.hud.app.debug.DriveRecorder.Anon5();
    private boolean isEngineInitialized;
    /* access modifiers changed from: private */
    public volatile boolean isLooping = false;
    /* access modifiers changed from: private */
    public volatile boolean isRecording;
    /* access modifiers changed from: private */
    public volatile boolean isSupportedPidsWritten;
    com.navdy.hud.app.debug.AsyncBufferedFileWriter obdAsyncBufferedFileWriter;
    /* access modifiers changed from: private */
    public android.os.Handler obdDataPlaybackHandler;
    /* access modifiers changed from: private */
    public android.os.HandlerThread obdPlaybackThread;
    /* access modifiers changed from: private */
    public final com.navdy.obd.IPidListener pidListener = new com.navdy.hud.app.debug.DriveRecorder.Anon6();
    private volatile com.navdy.hud.app.debug.DriveRecorder.State playbackState = com.navdy.hud.app.debug.DriveRecorder.State.STOPPED;
    private volatile long preparedFileLastModifiedTime;
    private volatile java.lang.String preparedFileName;
    volatile boolean realObdConnected = false;
    /* access modifiers changed from: private */
    public java.util.List<java.util.Map.Entry<java.lang.Long, java.util.List<com.navdy.obd.Pid>>> recordedObdData;
    private com.navdy.hud.app.debug.IRouteRecorder routeRecorder;

    public enum Action {
        PRELOAD,
        PLAY,
        PAUSE,
        RESUME,
        RESTART,
        STOP
    }

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            com.navdy.hud.app.debug.DriveRecorder.this.isRecording = false;
            com.navdy.hud.app.debug.DriveRecorder.this.isSupportedPidsWritten = false;
            com.navdy.hud.app.obd.ObdManager.getInstance().setRecordingPidListener(null);
            com.navdy.hud.app.debug.DriveRecorder.this.obdAsyncBufferedFileWriter.flushAndClose();
            com.navdy.hud.app.debug.DriveRecorder.this.driveScoreDataBufferedFileWriter.flushAndClose();
        }
    }

    class Anon2 implements java.lang.Runnable {
        final /* synthetic */ java.lang.String val$fileName;

        Anon2(java.lang.String str) {
            this.val$fileName = str;
        }

        public void run() {
            if (com.navdy.hud.app.debug.DriveRecorder.this.bPrepare(this.val$fileName)) {
                com.navdy.hud.app.debug.DriveRecorder.sLogger.d("Prepare succeeded");
            } else {
                com.navdy.hud.app.debug.DriveRecorder.sLogger.e("Prepare failed");
            }
        }
    }

    class Anon3 implements java.lang.Runnable {
        final /* synthetic */ java.lang.String val$fileName;

        Anon3(java.lang.String str) {
            this.val$fileName = str;
        }

        public void run() {
            if (!com.navdy.hud.app.debug.DriveRecorder.this.bPrepare(this.val$fileName)) {
                com.navdy.hud.app.debug.DriveRecorder.sLogger.e("Failed to prepare for playback , file : " + this.val$fileName);
                com.navdy.hud.app.debug.DriveRecorder.this.stopPlayback();
                return;
            }
            com.navdy.hud.app.debug.DriveRecorder.sLogger.d("Prepare succeeded");
            try {
                com.navdy.hud.app.debug.DriveRecorder.this.currentObdDataInjectionIndex = 0;
                com.navdy.hud.app.debug.DriveRecorder.this.obdPlaybackThread = new android.os.HandlerThread("ObdPlayback");
                com.navdy.hud.app.debug.DriveRecorder.this.obdPlaybackThread.start();
                com.navdy.hud.app.debug.DriveRecorder.this.obdDataPlaybackHandler = new android.os.Handler(com.navdy.hud.app.debug.DriveRecorder.this.obdPlaybackThread.getLooper());
                com.navdy.hud.app.debug.DriveRecorder.this.obdDataPlaybackHandler.post(com.navdy.hud.app.debug.DriveRecorder.this.injectFakeObdData);
            } catch (Throwable t) {
                com.navdy.hud.app.debug.DriveRecorder.this.stopPlayback();
                com.navdy.hud.app.debug.DriveRecorder.sLogger.e(t);
            }
        }
    }

    class Anon4 implements java.lang.Runnable {
        Anon4() {
        }

        public void run() {
            if (com.navdy.hud.app.debug.DriveRecorder.this.isRecording) {
                com.navdy.hud.app.debug.DriveRecorder.this.obdAsyncBufferedFileWriter.flush();
                com.navdy.hud.app.debug.DriveRecorder.this.driveScoreDataBufferedFileWriter.flush();
            }
        }
    }

    class Anon5 implements java.lang.Runnable {
        Anon5() {
        }

        public void run() {
            if (com.navdy.hud.app.debug.DriveRecorder.this.recordedObdData.size() > com.navdy.hud.app.debug.DriveRecorder.this.currentObdDataInjectionIndex + 1) {
                java.util.Map.Entry<java.lang.Long, java.util.List<com.navdy.obd.Pid>> data = (java.util.Map.Entry) com.navdy.hud.app.debug.DriveRecorder.this.recordedObdData.get(com.navdy.hud.app.debug.DriveRecorder.this.currentObdDataInjectionIndex);
                long timeStamp = ((java.lang.Long) data.getKey()).longValue();
                com.navdy.hud.app.obd.ObdManager.getInstance().injectObdData((java.util.List) data.getValue(), (java.util.List) data.getValue());
                if (com.navdy.hud.app.debug.DriveRecorder.this.recordedObdData.size() > com.navdy.hud.app.debug.DriveRecorder.this.currentObdDataInjectionIndex + 1) {
                    com.navdy.hud.app.debug.DriveRecorder.this.obdDataPlaybackHandler.postDelayed(com.navdy.hud.app.debug.DriveRecorder.this.injectFakeObdData, ((java.lang.Long) ((java.util.Map.Entry) com.navdy.hud.app.debug.DriveRecorder.this.recordedObdData.get(com.navdy.hud.app.debug.DriveRecorder.access$Anon204(com.navdy.hud.app.debug.DriveRecorder.this))).getKey()).longValue() - timeStamp);
                }
            } else if (com.navdy.hud.app.debug.DriveRecorder.this.isLooping) {
                com.navdy.hud.app.debug.DriveRecorder.this.currentObdDataInjectionIndex = 0;
                com.navdy.hud.app.debug.DriveRecorder.this.obdDataPlaybackHandler.post(com.navdy.hud.app.debug.DriveRecorder.this.injectFakeObdData);
            } else {
                com.navdy.hud.app.debug.DriveRecorder.sLogger.d("Done playing the recorded data. Stopping the playback");
                com.navdy.hud.app.debug.DriveRecorder.this.stopPlayback();
            }
        }
    }

    class Anon6 implements com.navdy.obd.IPidListener {
        Anon6() {
        }

        public void pidsRead(java.util.List<com.navdy.obd.Pid> list, java.util.List<com.navdy.obd.Pid> pidsChanged) throws android.os.RemoteException {
            if (pidsChanged != null && com.navdy.hud.app.debug.DriveRecorder.this.isRecording) {
                if (!com.navdy.hud.app.debug.DriveRecorder.this.isSupportedPidsWritten) {
                    com.navdy.hud.app.obd.ObdManager.getInstance().setRecordingPidListener(null);
                    try {
                        com.navdy.hud.app.debug.DriveRecorder.this.bRecordSupportedPids();
                    } catch (Throwable th) {
                        com.navdy.hud.app.debug.DriveRecorder.sLogger.d("Error while recording the supported PIDs");
                    }
                    com.navdy.hud.app.obd.ObdManager.getInstance().setRecordingPidListener(com.navdy.hud.app.debug.DriveRecorder.this.pidListener);
                }
                com.navdy.hud.app.debug.DriveRecorder.this.obdAsyncBufferedFileWriter.write(java.lang.System.currentTimeMillis() + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA);
                for (com.navdy.obd.Pid pid : pidsChanged) {
                    com.navdy.hud.app.debug.DriveRecorder.this.obdAsyncBufferedFileWriter.write(pid.getId() + com.navdy.hud.app.framework.glance.GlanceConstants.COLON_SEPARATOR + pid.getValue());
                    if (pid != pidsChanged.get(pidsChanged.size() - 1)) {
                        com.navdy.hud.app.debug.DriveRecorder.this.obdAsyncBufferedFileWriter.write(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA);
                    } else {
                        com.navdy.hud.app.debug.DriveRecorder.this.obdAsyncBufferedFileWriter.write(com.navdy.hud.app.framework.glance.GlanceConstants.NEWLINE);
                    }
                }
            }
        }

        public void pidsChanged(java.util.List<com.navdy.obd.Pid> list) throws android.os.RemoteException {
        }

        public void onConnectionStateChange(int newState) throws android.os.RemoteException {
        }

        public android.os.IBinder asBinder() {
            return null;
        }
    }

    class Anon7 implements java.lang.Runnable {
        final /* synthetic */ java.lang.String val$fileName;

        Anon7(java.lang.String str) {
            this.val$fileName = str;
        }

        public void run() {
            java.io.File driveLogsDir = com.navdy.hud.app.debug.DriveRecorder.getDriveLogsDir(this.val$fileName);
            if (!driveLogsDir.exists()) {
                driveLogsDir.mkdirs();
            }
            java.io.File driveLogFile = new java.io.File(driveLogsDir, this.val$fileName);
            java.io.File obdDataLogFile = new java.io.File(driveLogFile.getAbsolutePath() + ".obd");
            java.io.File gforceLogFile = new java.io.File(driveLogFile.getAbsolutePath() + ".drive");
            try {
                if (obdDataLogFile.createNewFile()) {
                    com.navdy.hud.app.debug.DriveRecorder.this.obdAsyncBufferedFileWriter = new com.navdy.hud.app.debug.AsyncBufferedFileWriter(obdDataLogFile.getAbsolutePath(), com.navdy.hud.app.debug.DriveRecorder.this, 250);
                    com.navdy.hud.app.obd.ObdManager.getInstance().setRecordingPidListener(null);
                    com.navdy.hud.app.debug.DriveRecorder.this.bRecordSupportedPids();
                    com.navdy.hud.app.obd.ObdManager.getInstance().setRecordingPidListener(com.navdy.hud.app.debug.DriveRecorder.this.pidListener);
                }
                if (gforceLogFile.createNewFile()) {
                    com.navdy.hud.app.debug.DriveRecorder.this.driveScoreDataBufferedFileWriter = new com.navdy.hud.app.debug.AsyncBufferedFileWriter(gforceLogFile.getAbsolutePath(), com.navdy.hud.app.debug.DriveRecorder.this, 500);
                }
            } catch (java.io.IOException e) {
                com.navdy.hud.app.debug.DriveRecorder.sLogger.e((java.lang.Throwable) e);
                com.navdy.hud.app.debug.DriveRecorder.this.isRecording = false;
                com.navdy.hud.app.debug.DriveRecorder.this.isSupportedPidsWritten = false;
            }
        }
    }

    class Anon8 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.hud.app.debug.DriveRecorder.Action val$action;
        final /* synthetic */ boolean val$loop;

        Anon8(com.navdy.hud.app.debug.DriveRecorder.Action action, boolean z) {
            this.val$action = action;
            this.val$loop = z;
        }

        public void run() {
            if (com.navdy.hud.app.debug.DriveRecorder.this.isDemoAvailable()) {
                switch (this.val$action) {
                    case PLAY:
                        com.navdy.hud.app.debug.DriveRecorder.this.playDemo(this.val$loop);
                        return;
                    case PAUSE:
                        com.navdy.hud.app.debug.DriveRecorder.this.pauseDemo();
                        return;
                    case RESUME:
                        com.navdy.hud.app.debug.DriveRecorder.this.resumeDemo();
                        return;
                    case RESTART:
                        com.navdy.hud.app.debug.DriveRecorder.this.restartDemo(this.val$loop);
                        return;
                    case STOP:
                        com.navdy.hud.app.debug.DriveRecorder.this.stopDemo();
                        return;
                    case PRELOAD:
                        com.navdy.hud.app.debug.DriveRecorder.this.prepareDemo();
                        return;
                    default:
                        return;
                }
            }
        }
    }

    class Anon9 implements java.lang.Runnable {
        Anon9() {
        }

        public void run() {
            com.navdy.hud.app.debug.DriveRecorder.sLogger.d("Auto recording is enabled so starting the auto record");
            java.io.File[] files = new java.io.File(com.navdy.hud.app.storage.PathManager.getInstance().getMapsPartitionPath() + java.io.File.separator + "drive_logs").listFiles();
            if (files != null && files.length > 20) {
                java.util.Arrays.sort(files, new com.navdy.hud.app.debug.DriveRecorder.FilesModifiedTimeComparator());
                for (int i = 0; i < files.length - 20; i++) {
                    java.io.File lastFile = files[i];
                    com.navdy.hud.app.debug.DriveRecorder.sLogger.d("Deleting the drive record " + lastFile.getAbsolutePath() + " as its old ");
                    com.navdy.service.library.util.IOUtils.deleteFile(com.navdy.hud.app.HudApplication.getAppContext(), lastFile.getAbsolutePath());
                }
            }
            com.navdy.hud.app.debug.DriveRecorder.this.connectionHandler.sendLocalMessage(new com.navdy.service.library.events.debug.StartDriveRecordingEvent("auto"));
        }
    }

    public enum DemoPreference {
        NA,
        DISABLED,
        ENABLED
    }

    public static class FilesModifiedTimeComparator implements java.util.Comparator<java.io.File> {
        public int compare(java.io.File file, java.io.File t1) {
            return java.lang.Long.compare(file.lastModified(), t1.lastModified());
        }
    }

    enum State {
        PLAYING,
        PAUSED,
        STOPPED
    }

    static /* synthetic */ int access$Anon204(com.navdy.hud.app.debug.DriveRecorder x0) {
        int i = x0.currentObdDataInjectionIndex + 1;
        x0.currentObdDataInjectionIndex = i;
        return i;
    }

    public void onServiceConnected(android.content.ComponentName name, android.os.IBinder service) {
        this.routeRecorder = com.navdy.hud.app.debug.IRouteRecorder.Stub.asInterface(service);
    }

    public void onServiceDisconnected(android.content.ComponentName name) {
        this.routeRecorder = null;
    }

    public void execute(java.lang.Runnable runnable) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(runnable, 9);
    }

    public void stopRecording() {
        if (!this.isRecording) {
            sLogger.v("already stopped, no-op");
            return;
        }
        sLogger.d("Stopping the Obd recording");
        this.handler.post(new com.navdy.hud.app.debug.DriveRecorder.Anon1());
    }

    public boolean bPrepare(java.lang.String fileName) {
        java.io.File obdDataLogFile = new java.io.File(getDriveLogsDir(fileName), fileName + ".obd");
        if (!obdDataLogFile.exists()) {
            return false;
        }
        long lastModifiedTime = obdDataLogFile.lastModified();
        if (!android.text.TextUtils.equals(this.preparedFileName, fileName) || this.preparedFileLastModifiedTime != lastModifiedTime) {
            this.preparedFileName = fileName;
            this.preparedFileLastModifiedTime = lastModifiedTime;
            java.io.BufferedReader reader = null;
            try {
                java.io.FileInputStream fileInputStream = new java.io.FileInputStream(obdDataLogFile);
                java.io.BufferedReader bufferedReader = new java.io.BufferedReader(new java.io.InputStreamReader(fileInputStream, "utf-8"));
                try {
                    sLogger.v("startPlayback of Obd data");
                    this.recordedObdData = new java.util.ArrayList();
                    java.io.FileInputStream fileInputStream2 = new java.io.FileInputStream(obdDataLogFile);
                    reader = new java.io.BufferedReader(new java.io.InputStreamReader(fileInputStream2, "utf-8"));
                    java.lang.String line = reader.readLine();
                    com.navdy.obd.PidSet supportedPids = new com.navdy.obd.PidSet();
                    if (line != null) {
                        sLogger.d("Supported pids recorded " + line);
                        if (line.trim().equals("-1")) {
                            sLogger.d("There are no supported pids when trying to playback the data");
                        } else {
                            java.lang.String[] pids = line.split(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA);
                            int length = pids.length;
                            for (int i = 0; i < length; i++) {
                                supportedPids.add(java.lang.Integer.parseInt(pids[i]));
                            }
                        }
                    }
                    com.navdy.hud.app.obd.ObdManager.getInstance().setSupportedPidSet(supportedPids);
                    while (true) {
                        java.lang.String line2 = reader.readLine();
                        if (line2 != null) {
                            java.lang.String[] lineSplit = line2.split(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA);
                            if (lineSplit != null && lineSplit.length > 1) {
                                try {
                                    long timeStamp = java.lang.Long.parseLong(lineSplit[0]);
                                    java.util.List<com.navdy.obd.Pid> pids2 = new java.util.ArrayList<>();
                                    for (int i2 = 1; i2 < lineSplit.length; i2++) {
                                        java.lang.String[] pidFields = lineSplit[i2].split(com.navdy.hud.app.framework.glance.GlanceConstants.COLON_SEPARATOR);
                                        int id = java.lang.Integer.parseInt(pidFields[0]);
                                        double value = java.lang.Double.parseDouble(pidFields[1]);
                                        com.navdy.obd.Pid pid = new com.navdy.obd.Pid(id);
                                        pid.setValue(value);
                                        pids2.add(pid);
                                        java.util.List<java.util.Map.Entry<java.lang.Long, java.util.List<com.navdy.obd.Pid>>> list = this.recordedObdData;
                                        java.util.AbstractMap.SimpleEntry simpleEntry = new java.util.AbstractMap.SimpleEntry(java.lang.Long.valueOf(timeStamp), pids2);
                                        list.add(simpleEntry);
                                    }
                                } catch (java.lang.NumberFormatException ne) {
                                    sLogger.e("Error parsing the Obd data from the file ", ne);
                                }
                            }
                        } else {
                            com.navdy.service.library.util.IOUtils.closeStream(reader);
                            return true;
                        }
                    }
                } catch (Throwable th) {
                    th = th;
                    reader = bufferedReader;
                    com.navdy.service.library.util.IOUtils.closeStream(reader);
                    throw th;
                }
            } catch (java.lang.Exception e) {
                sLogger.e("Exception while parsing the supported Pids");
            } catch (Throwable th2) {
                t = th2;
            }
        } else {
            sLogger.d("File already prepared");
            return true;
        }
    }

    public void prepare(java.lang.String fileName) {
        if (isPlaying() || isPaused()) {
            sLogger.e("Cannot prepare for playback as the playback is in progress");
        } else {
            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.debug.DriveRecorder.Anon2(fileName), 9);
        }
    }

    public void startPlayback(java.lang.String fileName, boolean loop) {
        if (isPlaying() || this.isRecording) {
            sLogger.v("already busy, no-op");
            return;
        }
        sLogger.d("Starting the playback");
        this.playbackState = com.navdy.hud.app.debug.DriveRecorder.State.PLAYING;
        this.isLooping = loop;
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.debug.DriveRecorder.Anon3(fileName), 9);
    }

    public void stopPlayback() {
        if (!isPlaying()) {
            sLogger.v("already stopped, no-op");
            return;
        }
        sLogger.d("Stopping the playback");
        this.currentObdDataInjectionIndex = 0;
        if (this.obdDataPlaybackHandler != null) {
            this.obdDataPlaybackHandler.removeCallbacks(this.injectFakeObdData);
        }
        if (this.obdPlaybackThread != null && this.obdPlaybackThread.isAlive()) {
            sLogger.v("obd handler thread quit:" + this.obdPlaybackThread.quit());
        }
        this.obdPlaybackThread = null;
        this.isLooping = false;
        this.playbackState = com.navdy.hud.app.debug.DriveRecorder.State.STOPPED;
    }

    public void release() {
        stopPlayback();
        if (this.recordedObdData != null) {
            this.recordedObdData = null;
        }
        this.preparedFileName = null;
        this.preparedFileLastModifiedTime = -1;
    }

    public void pausePlayback() {
        if (!isPlaying()) {
            sLogger.d("Playback is not happening so not pausing");
            return;
        }
        sLogger.d("Pausing the playback");
        this.obdDataPlaybackHandler.removeCallbacks(this.injectFakeObdData);
        this.playbackState = com.navdy.hud.app.debug.DriveRecorder.State.PAUSED;
    }

    public void resumePlayback() {
        if (isPaused()) {
            this.playbackState = com.navdy.hud.app.debug.DriveRecorder.State.PLAYING;
            this.obdDataPlaybackHandler.post(this.injectFakeObdData);
        }
    }

    public boolean restartPlayback() {
        if (!isPlaying() && !isPaused()) {
            return false;
        }
        this.playbackState = com.navdy.hud.app.debug.DriveRecorder.State.PLAYING;
        this.currentObdDataInjectionIndex = 0;
        this.obdDataPlaybackHandler.removeCallbacks(this.injectFakeObdData);
        this.obdDataPlaybackHandler.post(this.injectFakeObdData);
        return true;
    }

    public void flushRecordings() {
        this.handler.post(new com.navdy.hud.app.debug.DriveRecorder.Anon4());
    }

    public DriveRecorder(com.squareup.otto.Bus bus2, com.navdy.hud.app.service.ConnectionHandler connectionHandler2) {
        this.connectionHandler = connectionHandler2;
        this.isRecording = false;
        this.playbackState = com.navdy.hud.app.debug.DriveRecorder.State.STOPPED;
        this.isSupportedPidsWritten = false;
        this.bus = bus2;
        this.bus.register(this);
        com.navdy.hud.app.obd.ObdManager.getInstance().setDriveRecorder(this);
        this.context.bindService(new android.content.Intent(this.context, com.navdy.hud.app.debug.RouteRecorderService.class), this, 1);
    }

    public void startRecording(java.lang.String label) {
        if (this.isRecording || isPlaying()) {
            sLogger.v("Obd data recorder is already busy");
            return;
        }
        sLogger.d("Starting the recording");
        this.isRecording = true;
        this.isSupportedPidsWritten = false;
        com.navdy.service.library.task.TaskManager.getInstance().execute(createDriveRecord(label), 9);
    }

    private java.lang.Runnable createDriveRecord(java.lang.String fileName) {
        return new com.navdy.hud.app.debug.DriveRecorder.Anon7(fileName);
    }

    /* access modifiers changed from: private */
    public void bRecordSupportedPids() throws java.io.IOException {
        com.navdy.hud.app.obd.ObdManager obdManager = com.navdy.hud.app.obd.ObdManager.getInstance();
        com.navdy.obd.PidSet supportedPidSet = obdManager.getSupportedPids();
        if (supportedPidSet != null) {
            java.util.List<com.navdy.obd.Pid> supportedPidsList = supportedPidSet.asList();
            if (supportedPidsList == null || supportedPidsList.size() <= 0) {
                this.obdAsyncBufferedFileWriter.write("-1\n");
            } else {
                for (com.navdy.obd.Pid pid : supportedPidsList) {
                    if (pid != supportedPidsList.get(supportedPidsList.size() - 1)) {
                        this.obdAsyncBufferedFileWriter.write(pid.getId() + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA);
                    } else {
                        this.obdAsyncBufferedFileWriter.write(pid.getId() + com.navdy.hud.app.framework.glance.GlanceConstants.NEWLINE);
                    }
                }
            }
            this.obdAsyncBufferedFileWriter.write(java.lang.System.currentTimeMillis() + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA);
            this.obdAsyncBufferedFileWriter.write("47:" + obdManager.getFuelLevel() + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA);
            this.obdAsyncBufferedFileWriter.write("13:" + com.navdy.hud.app.manager.SpeedManager.getInstance().getObdSpeed() + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA);
            this.obdAsyncBufferedFileWriter.write("12:" + obdManager.getEngineRpm() + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA);
            this.obdAsyncBufferedFileWriter.write("256:" + obdManager.getInstantFuelConsumption() + com.navdy.hud.app.framework.glance.GlanceConstants.NEWLINE, true);
            this.isSupportedPidsWritten = true;
            sLogger.d("Obd data record created successfully and starting to listening to the PID changes");
        }
    }

    @com.squareup.otto.Subscribe
    public void onKeyEvent(android.view.KeyEvent event) {
        if (event != null && event.getKeyCode() == 36 && event.getAction() == 1) {
            performDemoPlaybackAction(com.navdy.hud.app.debug.DriveRecorder.Action.STOP, false);
        } else if (event != null && event.getKeyCode() == 53 && event.getAction() == 1) {
            performDemoPlaybackAction(com.navdy.hud.app.debug.DriveRecorder.Action.RESTART, false);
        } else if (event != null && event.getKeyCode() == 44 && event.getAction() == 1) {
            performDemoPlaybackAction(com.navdy.hud.app.debug.DriveRecorder.Action.PRELOAD, false);
        }
    }

    @com.squareup.otto.Subscribe
    public void onStartRecordingEvent(com.navdy.service.library.events.debug.StartDriveRecordingEvent startRecordingEvent) {
        sLogger.d("Start recording event received");
        startRecording(startRecordingEvent.label);
    }

    @com.squareup.otto.Subscribe
    public void onStopRecordingEvent(com.navdy.service.library.events.debug.StopDriveRecordingEvent stopRecordingEvent) {
        sLogger.d("Stop recording event received");
        stopRecording();
    }

    @com.squareup.otto.Subscribe
    public void onStartPlayback(com.navdy.service.library.events.debug.StartDrivePlaybackEvent startPlayback) {
        sLogger.d("Start playback event received");
        startPlayback(startPlayback.label, false);
    }

    @com.squareup.otto.Subscribe
    public void onStopPlayback(com.navdy.service.library.events.debug.StopDrivePlaybackEvent stopDrivePlaybackEvent) {
        sLogger.d("Stop playback event received");
        stopPlayback();
    }

    @com.squareup.otto.Subscribe
    public void onCalibratedGForceData(com.navdy.hud.app.device.gps.CalibratedGForceData calibratedGForceData) {
        if (this.isRecording && this.driveScoreDataBufferedFileWriter != null) {
            this.gforceDataBuilder.setLength(0);
            this.driveScoreDataBufferedFileWriter.write(this.gforceDataBuilder.append(java.lang.System.currentTimeMillis()).append(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA).append("G,").append(calibratedGForceData.getXAccel()).append(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA).append(calibratedGForceData.getYAccel()).append(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA).append(calibratedGForceData.getZAccel()).append(com.navdy.hud.app.framework.glance.GlanceConstants.NEWLINE).toString());
        }
    }

    @com.squareup.otto.Subscribe
    public void onDriveScoreUpdatedEvent(com.navdy.hud.app.analytics.TelemetryDataManager.DriveScoreUpdated driveScoreUpdated) {
        if (this.isRecording && this.driveScoreDataBufferedFileWriter != null) {
            this.gforceDataBuilder.setLength(0);
            this.driveScoreDataBufferedFileWriter.write(this.gforceDataBuilder.append(java.lang.System.currentTimeMillis()).append(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA).append("D,").append("DS:").append(driveScoreUpdated.getDriveScore()).append(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA).append("DE:").append(driveScoreUpdated.getInterestingEvent()).append(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA).append("SD:").append(com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getSessionDuration()).append(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA).append("RD:").append(com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getSessionRollingDuration()).append(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA).append("SPD:").append(com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getSpeedingDuration()).append(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA).append("ESD:").append(com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getExcessiveSpeedingDuration()).append(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA).append("HA:").append(com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getSessionHardAccelerationCount()).append(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA).append("HB:").append(com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getSessionHardBrakingCount()).append(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA).append("HG:").append(com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getSessionHighGCount()).append(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA).append("LT:").append(getLatitude()).append(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA).append("LN:").append(getLongitude()).append(com.navdy.hud.app.framework.glance.GlanceConstants.NEWLINE).toString());
        }
    }

    private static double getLatitude() {
        if (com.navdy.hud.app.maps.here.HereMapsManager.getInstance().isInitialized()) {
            com.here.android.mpa.common.GeoCoordinate c = com.navdy.hud.app.maps.here.HereMapsManager.getInstance().getLocationFixManager().getLastGeoCoordinate();
            if (c != null) {
                return c.getLatitude();
            }
        }
        return 0.0d;
    }

    private static double getLongitude() {
        if (com.navdy.hud.app.maps.here.HereMapsManager.getInstance().isInitialized()) {
            com.here.android.mpa.common.GeoCoordinate c = com.navdy.hud.app.maps.here.HereMapsManager.getInstance().getLocationFixManager().getLastGeoCoordinate();
            if (c != null) {
                return c.getLongitude();
            }
        }
        return 0.0d;
    }

    @com.squareup.otto.Subscribe
    public void onMapEngineInitialized(com.navdy.hud.app.maps.MapEvents.MapEngineInitialize mapEngineInitializedEvent) {
        sLogger.d("onMapEnigneInitialzed : message received. Initialized :" + mapEngineInitializedEvent.initialized);
        this.isEngineInitialized = mapEngineInitializedEvent.initialized;
        checkAndStartAutoRecording();
    }

    public void performDemoPlaybackAction(com.navdy.hud.app.debug.DriveRecorder.Action action, boolean loop) {
        sLogger.d(":performDemoPlaybackAction");
        if (!this.isEngineInitialized) {
            sLogger.e("Engine is not initialized so can not start the playback");
        } else {
            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.debug.DriveRecorder.Anon8(action, loop), 1);
        }
    }

    public void load() {
        sLogger.d("Loading the Obd Data recorder");
        this.demoRouteLogFileExists = new java.io.File(com.navdy.hud.app.storage.PathManager.getInstance().getMapsPartitionPath() + java.io.File.separator + DEMO_LOG_FILE_NAME).exists();
        sLogger.d("Demo file exists : " + this.demoRouteLogFileExists);
        this.demoObdLogFileExists = new java.io.File(com.navdy.hud.app.storage.PathManager.getInstance().getMapsPartitionPath() + java.io.File.separator + DEMO_LOG_FILE_NAME + ".obd").exists();
        sLogger.d("Demo obd file exists : " + this.demoObdLogFileExists);
    }

    public boolean isDemoAvailable() {
        if (this.demoRouteLogFileExists && !this.realObdConnected) {
            return true;
        }
        sLogger.d(!this.demoRouteLogFileExists ? "Demo log file does not exist in the map partition. So Demo preference : NA" : "Obd has been connected so skipping the demo");
        return false;
    }

    private void checkAndStartAutoRecording() {
        if (isAutoRecordingEnabled()) {
            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.debug.DriveRecorder.Anon9(), 9);
        }
    }

    public void setRealObdConnected(boolean realObdConnected2) {
        this.realObdConnected = realObdConnected2;
        sLogger.d("setRealObdConnected : state " + realObdConnected2);
        if (this.realObdConnected) {
            performDemoPlaybackAction(com.navdy.hud.app.debug.DriveRecorder.Action.STOP, true);
        }
    }

    public static java.io.File getDriveLogsDir(java.lang.String fileName) {
        if (DEMO_LOG_FILE_NAME.equals(fileName)) {
            return new java.io.File(com.navdy.hud.app.storage.PathManager.getInstance().getMapsPartitionPath());
        }
        return new java.io.File(com.navdy.hud.app.storage.PathManager.getInstance().getMapsPartitionPath() + java.io.File.separator + "drive_logs");
    }

    public static boolean isAutoRecordingEnabled() {
        return com.navdy.hud.app.util.os.SystemProperties.getBoolean(AUTO_DRIVERECORD_PROP, false) || !com.navdy.hud.app.util.DeviceUtil.isUserBuild();
    }

    private boolean isPlaying() {
        return this.playbackState == com.navdy.hud.app.debug.DriveRecorder.State.PLAYING;
    }

    private boolean isPaused() {
        return this.playbackState == com.navdy.hud.app.debug.DriveRecorder.State.PAUSED;
    }

    private boolean isStopped() {
        return this.playbackState == com.navdy.hud.app.debug.DriveRecorder.State.STOPPED;
    }

    public boolean isDemoPlaying() {
        try {
            return isPlaying() || this.routeRecorder.isPlaying();
        } catch (Throwable e) {
            sLogger.e(e);
            return false;
        }
    }

    public boolean isDemoPaused() {
        try {
            return isPaused() || this.routeRecorder.isPaused();
        } catch (Throwable e) {
            sLogger.e(e);
            return false;
        }
    }

    public boolean isDemoStopped() {
        try {
            return isStopped() && this.routeRecorder.isStopped();
        } catch (Throwable e) {
            sLogger.e(e);
            return false;
        }
    }

    /* access modifiers changed from: private */
    public void prepareDemo() {
        prepare(DEMO_LOG_FILE_NAME);
        try {
            this.routeRecorder.prepare(DEMO_LOG_FILE_NAME, false);
        } catch (Throwable e) {
            sLogger.e("Error preparing the route recorder for demo", e);
        }
    }

    /* access modifiers changed from: private */
    public void playDemo(boolean loop) {
        stopRecordingBeforeDemo();
        if (!isPlaying()) {
            if (isPaused()) {
                resumePlayback();
            } else {
                startPlayback(DEMO_LOG_FILE_NAME, loop);
            }
        }
        try {
            if (this.routeRecorder.isPaused()) {
                this.routeRecorder.resumePlayback();
            } else if (!this.routeRecorder.isPlaying()) {
                this.routeRecorder.startPlayback(DEMO_LOG_FILE_NAME, false, loop);
            }
        } catch (Throwable e) {
            sLogger.e("Error playing the route playback", e);
        }
    }

    private void stopRecordingBeforeDemo() {
        if (this.isRecording) {
            stopRecording();
            try {
                java.lang.Thread.sleep(1000);
            } catch (java.lang.InterruptedException e) {
                sLogger.e("Interrupted ", e);
            }
        }
        try {
            if (this.routeRecorder.isRecording()) {
                this.routeRecorder.stopRecording();
                try {
                    java.lang.Thread.sleep(1000);
                } catch (java.lang.InterruptedException e2) {
                    sLogger.e("Interrupted while stopping the recording");
                }
            }
        } catch (java.lang.Exception e3) {
            sLogger.e("Exception while stopping the recording", e3);
        }
    }

    /* access modifiers changed from: private */
    public void pauseDemo() {
        stopRecordingBeforeDemo();
        if (isPlaying()) {
            pausePlayback();
        }
        try {
            if (this.routeRecorder.isPlaying()) {
                this.routeRecorder.pausePlayback();
            }
        } catch (java.lang.Exception e) {
            sLogger.e("Error pausing the route playback");
        }
    }

    /* access modifiers changed from: private */
    public void stopDemo() {
        if (isPlaying()) {
            stopPlayback();
        }
        try {
            if (this.routeRecorder.isPlaying() || this.routeRecorder.isPaused()) {
                this.routeRecorder.stopPlayback();
            }
        } catch (java.lang.Exception e) {
            sLogger.e("Error stopping the route playback", e);
        }
    }

    /* access modifiers changed from: private */
    public void resumeDemo() {
        stopRecordingBeforeDemo();
        if (isPaused()) {
            resumePlayback();
        }
        try {
            if (this.routeRecorder.isPaused()) {
                this.routeRecorder.resumePlayback();
            }
        } catch (java.lang.Exception e) {
            sLogger.e("Error resuming the route playback", e);
        }
    }

    /* access modifiers changed from: private */
    public void restartDemo(boolean loop) {
        stopRecordingBeforeDemo();
        if (!restartPlayback()) {
            startPlayback(DEMO_LOG_FILE_NAME, loop);
        }
        try {
            if (!this.routeRecorder.restartPlayback()) {
                this.routeRecorder.startPlayback(DEMO_LOG_FILE_NAME, false, loop);
            }
        } catch (java.lang.Exception e) {
            sLogger.e("Error restarting the route playback", e);
        }
    }
}
