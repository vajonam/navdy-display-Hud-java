package com.navdy.hud.app.debug;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0000\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\bJ\u0006\u0010\r\u001a\u00020\u000eJ\u0006\u0010\u000f\u001a\u00020\u000eJ\u001a\u0010\u0010\u001a\u00020\u000e2\u0006\u0010\u0011\u001a\u00020\u00032\b\b\u0002\u0010\u0012\u001a\u00020\u0013H\u0007R\u0011\u0010\t\u001a\u00020\n\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0014"}, d2 = {"Lcom/navdy/hud/app/debug/AsyncBufferedFileWriter;", "", "filePath", "", "executor", "Lcom/navdy/hud/app/debug/SerialExecutor;", "bufferSize", "", "(Ljava/lang/String;Lcom/navdy/hud/app/debug/SerialExecutor;I)V", "bufferedWriter", "Ljava/io/BufferedWriter;", "getBufferedWriter", "()Ljava/io/BufferedWriter;", "flush", "", "flushAndClose", "write", "data", "forceToDisk", "", "app_hudDebug"}, k = 1, mv = {1, 1, 6})
/* compiled from: AsyncBufferedFileWriter.kt */
public final class AsyncBufferedFileWriter {
    @org.jetbrains.annotations.NotNull
    private final java.io.BufferedWriter bufferedWriter;
    private final com.navdy.hud.app.debug.SerialExecutor executor;

    @kotlin.jvm.JvmOverloads
    public final void write(@org.jetbrains.annotations.NotNull java.lang.String data) {
        write$default(this, data, false, 2, null);
    }

    public AsyncBufferedFileWriter(@org.jetbrains.annotations.NotNull java.lang.String filePath, @org.jetbrains.annotations.NotNull com.navdy.hud.app.debug.SerialExecutor executor2, int bufferSize) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(filePath, "filePath");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(executor2, "executor");
        this.executor = executor2;
        this.bufferedWriter = new java.io.BufferedWriter(new java.io.OutputStreamWriter(new java.io.FileOutputStream(new java.io.File(filePath)), "utf-8"), bufferSize);
    }

    @org.jetbrains.annotations.NotNull
    public final java.io.BufferedWriter getBufferedWriter() {
        return this.bufferedWriter;
    }

    @kotlin.jvm.JvmOverloads
    public static /* bridge */ /* synthetic */ void write$default(com.navdy.hud.app.debug.AsyncBufferedFileWriter asyncBufferedFileWriter, java.lang.String str, boolean z, int i, java.lang.Object obj) {
        if ((i & 2) != 0) {
            z = false;
        }
        asyncBufferedFileWriter.write(str, z);
    }

    @kotlin.jvm.JvmOverloads
    public final void write(@org.jetbrains.annotations.NotNull java.lang.String data, boolean forceToDisk) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(data, "data");
        this.executor.execute(new com.navdy.hud.app.debug.AsyncBufferedFileWriter$write$Anon1(this, data, forceToDisk));
    }

    public final void flush() {
        this.executor.execute(new com.navdy.hud.app.debug.AsyncBufferedFileWriter$flush$Anon1(this));
    }

    public final void flushAndClose() {
        this.executor.execute(new com.navdy.hud.app.debug.AsyncBufferedFileWriter$flushAndClose$Anon1(this));
    }
}
