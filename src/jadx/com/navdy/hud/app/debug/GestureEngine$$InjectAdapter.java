package com.navdy.hud.app.debug;

public final class GestureEngine$$InjectAdapter extends dagger.internal.Binding<com.navdy.hud.app.debug.GestureEngine> implements dagger.MembersInjector<com.navdy.hud.app.debug.GestureEngine> {
    private dagger.internal.Binding<com.navdy.hud.app.gesture.GestureServiceConnector> gestureServiceConnector;

    public GestureEngine$$InjectAdapter() {
        super(null, "members/com.navdy.hud.app.debug.GestureEngine", false, com.navdy.hud.app.debug.GestureEngine.class);
    }

    public void attach(dagger.internal.Linker linker) {
        this.gestureServiceConnector = linker.requestBinding("com.navdy.hud.app.gesture.GestureServiceConnector", com.navdy.hud.app.debug.GestureEngine.class, getClass().getClassLoader());
    }

    public void getDependencies(java.util.Set<dagger.internal.Binding<?>> set, java.util.Set<dagger.internal.Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.gestureServiceConnector);
    }

    public void injectMembers(com.navdy.hud.app.debug.GestureEngine object) {
        object.gestureServiceConnector = (com.navdy.hud.app.gesture.GestureServiceConnector) this.gestureServiceConnector.get();
    }
}
