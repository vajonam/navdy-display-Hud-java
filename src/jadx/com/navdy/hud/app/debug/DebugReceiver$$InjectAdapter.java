package com.navdy.hud.app.debug;

public final class DebugReceiver$$InjectAdapter extends dagger.internal.Binding<com.navdy.hud.app.debug.DebugReceiver> implements javax.inject.Provider<com.navdy.hud.app.debug.DebugReceiver>, dagger.MembersInjector<com.navdy.hud.app.debug.DebugReceiver> {
    private dagger.internal.Binding<com.squareup.otto.Bus> mBus;

    public DebugReceiver$$InjectAdapter() {
        super("com.navdy.hud.app.debug.DebugReceiver", "members/com.navdy.hud.app.debug.DebugReceiver", false, com.navdy.hud.app.debug.DebugReceiver.class);
    }

    public void attach(dagger.internal.Linker linker) {
        this.mBus = linker.requestBinding("com.squareup.otto.Bus", com.navdy.hud.app.debug.DebugReceiver.class, getClass().getClassLoader());
    }

    public void getDependencies(java.util.Set<dagger.internal.Binding<?>> set, java.util.Set<dagger.internal.Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.mBus);
    }

    public com.navdy.hud.app.debug.DebugReceiver get() {
        com.navdy.hud.app.debug.DebugReceiver result = new com.navdy.hud.app.debug.DebugReceiver();
        injectMembers(result);
        return result;
    }

    public void injectMembers(com.navdy.hud.app.debug.DebugReceiver object) {
        object.mBus = (com.squareup.otto.Bus) this.mBus.get();
    }
}
