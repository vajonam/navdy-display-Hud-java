package com.navdy.hud.app.debug;

public class BusLeakDetector {
    private static final java.lang.String EMPTY = "";
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.debug.BusLeakDetector.class);
    private static final com.navdy.hud.app.debug.BusLeakDetector singleton = new com.navdy.hud.app.debug.BusLeakDetector();
    private java.util.HashMap<java.lang.Class, java.lang.Integer> registrationMap = new java.util.HashMap<>(100);

    public BusLeakDetector() {
        sLogger.v("::ctor::");
    }

    public static com.navdy.hud.app.debug.BusLeakDetector getInstance() {
        return singleton;
    }

    public synchronized void addReference(java.lang.Class clz) {
        java.lang.Integer count = (java.lang.Integer) this.registrationMap.get(clz);
        if (count == null) {
            this.registrationMap.put(clz, java.lang.Integer.valueOf(1));
        } else {
            this.registrationMap.put(clz, java.lang.Integer.valueOf(count.intValue() + 1));
        }
    }

    public synchronized void removeReference(java.lang.Class clz) {
        java.lang.Integer count = (java.lang.Integer) this.registrationMap.get(clz);
        if (count != null) {
            java.lang.Integer count2 = java.lang.Integer.valueOf(count.intValue() - 1);
            if (count2.intValue() == 0) {
                this.registrationMap.remove(clz);
            } else {
                this.registrationMap.put(clz, count2);
            }
        }
    }

    public synchronized void printReferences() {
        java.lang.String suffix;
        sLogger.v("::printReferences: [" + this.registrationMap.size() + "]");
        for (java.util.Map.Entry<java.lang.Class, java.lang.Integer> entry : this.registrationMap.entrySet()) {
            int count = ((java.lang.Integer) entry.getValue()).intValue();
            switch (count) {
                case 1:
                    suffix = "";
                    break;
                case 2:
                    suffix = " **";
                    break;
                case 3:
                    suffix = " ****";
                    break;
                case 4:
                    suffix = " ********";
                    break;
                default:
                    suffix = " ****************";
                    break;
            }
            sLogger.v(((java.lang.Class) entry.getKey()).getName() + " = " + count + suffix);
        }
    }
}
