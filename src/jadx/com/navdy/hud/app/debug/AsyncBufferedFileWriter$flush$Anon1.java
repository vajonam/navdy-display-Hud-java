package com.navdy.hud.app.debug;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\b\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\b\u0002"}, d2 = {"<anonymous>", "", "run"}, k = 3, mv = {1, 1, 6})
/* compiled from: AsyncBufferedFileWriter.kt */
final class AsyncBufferedFileWriter$flush$Anon1 implements java.lang.Runnable {
    final /* synthetic */ com.navdy.hud.app.debug.AsyncBufferedFileWriter this$Anon0;

    AsyncBufferedFileWriter$flush$Anon1(com.navdy.hud.app.debug.AsyncBufferedFileWriter asyncBufferedFileWriter) {
        this.this$Anon0 = asyncBufferedFileWriter;
    }

    public final void run() {
        this.this$Anon0.getBufferedWriter().flush();
    }
}
