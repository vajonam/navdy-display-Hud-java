package com.navdy.hud.app.debug;

public class DebugReceiver extends android.content.BroadcastReceiver {
    public static final java.lang.String ACTION_ANR = "com.navdy.app.debug.ANR";
    public static final java.lang.String ACTION_BROADCAST_ANR = "com.navdy.app.debug.BROADCAST_ANR";
    public static final java.lang.String ACTION_CHANGE_HEADING = "com.navdy.app.debug.CHANGE_HEADING";
    public static final java.lang.String ACTION_CHECK_FOR_MAP_UPDATES = "com.navdy.app.debug.CHECK_FOR_MAP_DATA_UPDATE";
    public static final java.lang.String ACTION_CLEAR_MANEUVER = "com.navdy.app.debug.CLEAR_MANEUVER";
    public static final java.lang.String ACTION_CRASH = "com.navdy.app.debug.CRASH";
    public static final java.lang.String ACTION_DNS_LOOKUP = "com.navdy.app.debug.DNS_LOOKUP";
    public static final java.lang.String ACTION_DNS_LOOKUP_TEST = "com.navdy.app.debug.DNS_LOOKUP_TEST";
    public static final java.lang.String ACTION_DUMP_THREAD_STACK = "com.navdy.hud.debug.DUMP_THREAD_STACK";
    public static final java.lang.String ACTION_EJECT_JUNCTION_VIEW = "com.navdy.app.debug.EJECT_JUNC_VIEW";
    public static final java.lang.String ACTION_GET_GOOGLE = "com.navdy.app.debug.GET_GOOGLE";
    public static final java.lang.String ACTION_HIDE_LANE_INFO = "com.navdy.app.debug.HIDE_LANE_INFO";
    public static final java.lang.String ACTION_HIDE_RECALC = "com.navdy.app.debug.HIDE_RECALC";
    public static final java.lang.String ACTION_INJECT_JUNCTION_VIEW = "com.navdy.app.debug.INJECT_JUNC_VIEW";
    public static final java.lang.String ACTION_LAUNCH_MUSIC_DETAILS = "com.navdy.app.debug.MUSIC_DETAILS";
    public static final java.lang.String ACTION_LAUNCH_PICKER = "com.navdy.app.debug.LAUNCH_PICKER";
    public static final java.lang.String ACTION_NATIVE_CRASH = "com.navdy.app.debug.NATIVE_CRASH";
    public static final java.lang.String ACTION_NATIVE_CRASH_SEPARATE_THREAD = "com.navdy.app.debug.NATIVE_CRASH_SEPARATE_THREAD";
    public static final java.lang.String ACTION_NETSTAT = "com.navdy.app.debug.NETSTAT";
    public static final java.lang.String ACTION_PLAYBACK_ROUTE = "com.navdy.app.debug.PLAYBACK_ROUTE";
    public static final java.lang.String ACTION_PLAYBACK_ROUTE_SECONDARY = "com.navdy.app.debug.PLAYBACK_ROUTE_SECONDARY";
    public static final java.lang.String ACTION_PRINT_BUS_REGISTRATION = "com.navdy.app.debug.PRINT_BUS_REGISTRATION";
    public static final java.lang.String ACTION_PRINT_MAP_NAV_MODE = "com.navdy.app.debug.PRINT_MAP_NAV_MODE";
    public static final java.lang.String ACTION_PRINT_MAP_ZOOM = "com.navdy.app.debug.PRINT_MAP_ZOOM";
    public static final java.lang.String ACTION_SCALE_DISPLAY = "com.navdy.app.debug.SCALE_DISPLAY";
    public static final java.lang.String ACTION_SEND_NOW_MANEUVER = "com.navdy.app.debug.NOW_MANEUVER";
    public static final java.lang.String ACTION_SEND_SOON_MANEUVER = "com.navdy.app.debug.SOON_MANEUVER";
    public static final java.lang.String ACTION_SEND_STAY_MANEUVER = "com.navdy.app.debug.STAY_MANEUVER";
    public static final java.lang.String ACTION_SET_DISTANCE_MANEUVER = "com.navdy.app.debug.DISTANCE_MANEUVER";
    public static final java.lang.String ACTION_SET_ICON_MANEUVER = "com.navdy.app.debug.ICON_MANEUVER";
    public static final java.lang.String ACTION_SET_INSTRUCTION_MANEUVER = "com.navdy.app.debug.INSTRUCTION_MANEUVER";
    public static final java.lang.String ACTION_SET_SIMULATION_SPEED = "com.navdy.app.debug.SET_SIM_SPEED";
    public static final java.lang.String ACTION_SET_START_ROUTE_CALC_POINT = "com.navdy.app.debug.START_ROUTE_POINT";
    public static final java.lang.String ACTION_SET_THEN_MANEUVER = "com.navdy.app.debug.THEN_MANEUVER";
    public static final java.lang.String ACTION_SHOW_LANE_INFO = "com.navdy.app.debug.SHOW_LANE_INFO";
    public static final java.lang.String ACTION_SHOW_RECALC = "com.navdy.app.debug.SHOW_RECALC";
    public static final java.lang.String ACTION_START_BUS_PROFILING = "com.navdy.app.debug.START_BUS_PROFILING";
    public static final java.lang.String ACTION_START_CPU_HOG = "com.navdy.app.debug.START_CPU_HOG";
    public static final java.lang.String ACTION_START_MAIN_THREAD_PROFILING = "com.navdy.app.debug.START_MAIN_THREAD_PROFILING";
    public static final java.lang.String ACTION_START_MAP_RENDERING = "com.navdy.app.debug.START_MAP_RENDERING";
    public static final java.lang.String ACTION_STOP_BUS_PROFILING = "com.navdy.app.debug.STOP_BUS_PROFILING";
    public static final java.lang.String ACTION_STOP_CPU_HOG = "com.navdy.app.debug.STOP_CPU_HOG";
    public static final java.lang.String ACTION_STOP_MAIN_THREAD_PROFILING = "com.navdy.app.debug.STOP_MAIN_THREAD_PROFILING";
    public static final java.lang.String ACTION_STOP_MAP_RENDERING = "com.navdy.app.debug.STOP_MAP_RENDERING";
    public static final java.lang.String ACTION_STOP_PLAYBACK_ROUTE = "com.navdy.app.debug.STOP_PLAYBACK_ROUTE";
    public static final java.lang.String ACTION_TEST = "com.navdy.hud.debug.TEST";
    public static final java.lang.String ACTION_TEST_TBT_TTS = "com.navdy.app.debug.TEST_TBT_TTS";
    public static final java.lang.String ACTION_TEST_TBT_TTS_CANCEL = "com.navdy.app.debug.TEST_TBT_TTS_CANCEL";
    public static final java.lang.String ACTION_UPDATE_MAP_DATA = "com.navdy.app.debug.UPDATE_MAP_DATA";
    public static final java.lang.String ACTION_WAKEUP = "com.navdy.app.debug.WAKEUP";
    public static final int CONNECTION_TIMEOUT_MILLIS = 30000;
    public static final int DESTINATION_SUGGESTION_TYPE_FAVORITE = 2;
    public static final int DESTINATION_SUGGESTION_TYPE_HOME = 0;
    public static final int DESTINATION_SUGGESTION_TYPE_NEW_PLACE = 3;
    public static final int DESTINATION_SUGGESTION_TYPE_WORK = 1;
    public static final java.lang.String DISABLE_HERE_LOC_DBG = "com.navdy.app.debug.DISABLE_HERE_LOC_DBG";
    public static final java.lang.String DNS_LOOKUP_HOST_NAME = "DNS_LOOKUP_HOST_NAME";
    private static final boolean ENABLED = true;
    public static final java.lang.String ENABLE_HERE_LOC_DBG = "com.navdy.app.debug.ENABLE_HERE_LOC_DBG";
    public static final java.lang.String EXTRA_BANDWIDTH_LEVEL = "BANDWIDTH_LEVEL";
    public static final java.lang.String EXTRA_COMMAND = "COMMAND";
    public static final java.lang.String EXTRA_DEEP = "DEEP";
    public static final java.lang.String EXTRA_DESTINATION_SUGGESTION_TYPE = "DESTINATION_SUGGESTION_TYPE";
    public static final java.lang.String EXTRA_KEY = "KEY";
    public static final java.lang.String EXTRA_LIGHT = "LIGHT";
    public static final java.lang.String EXTRA_MANEUVER_DISTANCE = "EXTRA_MANEUVER_DISTANCE";
    public static final java.lang.String EXTRA_MANEUVER_ICON = "EXTRA_MANEUVER_ICON";
    public static final java.lang.String EXTRA_MANEUVER_INSTRUCTION = "EXTRA_MANEUVER_INSTRUCTION";
    public static final java.lang.String EXTRA_MANEUVER_THEN_ICON = "EXTRA_MANEUVER_THEN_ICON";
    public static final java.lang.String EXTRA_MODE = "MODE";
    public static final java.lang.String EXTRA_NOTIFICATION_EVENT_APP_ID = "appId";
    public static final java.lang.String EXTRA_NOTIFICATION_EVENT_APP_NAME = "appName";
    public static final java.lang.String EXTRA_NOTIFICATION_EVENT_ID = "id";
    public static final java.lang.String EXTRA_NOTIFICATION_EVENT_MESSAGE = "message";
    public static final java.lang.String EXTRA_NOTIFICATION_EVENT_SUB_TITLE = "subtitle";
    public static final java.lang.String EXTRA_NOTIFICATION_EVENT_TITLE = "title";
    public static final java.lang.String EXTRA_OBD_CONFIG = "CONFIG";
    public static final java.lang.String EXTRA_SIZE = "SIZE";
    public static final java.lang.String EXTRA_TEST = "TEST";
    public static final java.lang.String EXTRA_URL = "URL";
    public static final java.lang.String HEADING = "HEADING";
    private static final int MAIN_THREAD_DEFAULT_PROFILING_THRESHOLD = 0;
    public static final java.lang.String MAIN_THREAD_EXTRA_PROFILING_THRESHOLD = "PROFILING_THRESHOLD";
    public static final int READ_TIMEOUT_MILLIS = 30000;
    public static final java.lang.String SPEED = "SPEED";
    public static final java.lang.String TEST_DESTINATION_SUGGESTION = "TEST_DESTINATION_SUGGESTION";
    public static final java.lang.String TEST_DIAL = "TEST_DIAL";
    public static final java.lang.String TEST_DISABLE_OBD_PIDS_SCANNING = "TEST_DISABLE_OBD";
    public static final java.lang.String TEST_ENABLE_OBD_PIDS_SCANNING = "TEST_ENABLE_OBD";
    public static final java.lang.String TEST_HID = "HID";
    public static final java.lang.String TEST_LAUNCH_APP = "LAUNCH_APP";
    public static final java.lang.String TEST_LIGHT = "TEST_LIGHT";
    public static final java.lang.String TEST_OBD_CONFIG = "TEST_OBD";
    public static final java.lang.String TEST_OBD_FLASH_NEW_FIRMWARE = "TEST_OBD_FLASH_NEW_FIRMWARE";
    public static final java.lang.String TEST_OBD_FLASH_OLD_FIRMWARE = "TEST_OBD_FLASH_OLD_FIRMWARE";
    public static final java.lang.String TEST_OBD_RESET = "TEST_OBD_RESET";
    public static final java.lang.String TEST_OBD_SET_MODE_J1939 = "TEST_OBD_SET_MODE_J1939";
    public static final java.lang.String TEST_OBD_SET_MODE_OBD2 = "TEST_OBD_SET_MODE_OBD2";
    public static final java.lang.String TEST_OBD_SLEEP = "TEST_OBD_SLEEP";
    public static final java.lang.String TEST_PROXY_FILE_DOWNLOAD = "TEST_PROXY_FILE_DOWNLOAD";
    public static final java.lang.String TEST_PROXY_FILE_UPLOAD = "TEST_PROXY_FILE_UPLOAD";
    public static final java.lang.String TEST_SEND_NOTIFICATION_EVENT = "TEST_SEND_NOTIFICATION_EVENT";
    public static final java.lang.String TEST_SHUT_DOWN = "SHUTDOWN";
    public static final java.lang.String TEST_SWITCH_BANDWIDTH_LEVEL = "TEST_SWITCH_BANDWIDTH";
    public static final java.lang.String TEST_TTS = "TEST_TTS";
    private static final java.lang.String[] TTS_TEXTS = {"Turn left after 100 feet", "Turn right after 500 feet. 1, 2, 3, 4, 5, 6, 7, 8,9,10", "There is a slight traffic ahead, be on the right lane.", "Hi how are you doing today? Testing a long long long long text", "Testing testing still testing."};
    static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.debug.DebugReceiver.class);
    @javax.inject.Inject
    com.squareup.otto.Bus mBus;
    private com.navdy.hud.app.debug.DebugReceiver.PrinterImpl mainThreadProfilingPrinter = new com.navdy.hud.app.debug.DebugReceiver.PrinterImpl(null);

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            com.navdy.hud.app.maps.here.HereMapsManager hereMapsManager = com.navdy.hud.app.maps.here.HereMapsManager.getInstance();
            if (!hereMapsManager.isInitialized()) {
                com.navdy.hud.app.debug.DebugReceiver.sLogger.v("[MAP-ZOOM] HERE Maps engine not initialized yet");
                return;
            }
            com.navdy.hud.app.maps.here.HereMapController mapController = hereMapsManager.getMapController();
            com.navdy.hud.app.debug.DebugReceiver.sLogger.v("[MAP-ZOOM] Zoom[" + mapController.getZoomLevel() + "] Tilt[" + mapController.getTilt() + "]");
        }
    }

    class Anon2 extends java.util.TimerTask {
        final /* synthetic */ int val$mediaKey;

        Anon2(int i) {
            this.val$mediaKey = i;
        }

        public void run() {
            com.navdy.hud.app.debug.DebugReceiver.this.mBus.post(new com.navdy.hud.app.event.RemoteEvent(new com.navdy.service.library.events.input.MediaRemoteKeyEvent(com.navdy.service.library.events.input.MediaRemoteKey.values()[this.val$mediaKey], com.navdy.service.library.events.input.KeyEvent.KEY_UP)));
        }
    }

    class Anon3 implements java.lang.Runnable {
        final /* synthetic */ int val$size;

        Anon3(int i) {
            this.val$size = i;
        }

        public void run() {
            int finalSize;
            try {
                java.net.HttpURLConnection urlConnection = (java.net.HttpURLConnection) new java.net.URL("http://test.mjpmz4phvm.us-west-2.elasticbeanstalk.com/file").openConnection();
                urlConnection.setConnectTimeout(30000);
                urlConnection.setReadTimeout(30000);
                urlConnection.setRequestMethod("POST");
                java.io.OutputStream os = urlConnection.getOutputStream();
                if (this.val$size < 0 || this.val$size > 2000) {
                    finalSize = 100000;
                } else {
                    finalSize = this.val$size * 1000;
                }
                com.navdy.hud.app.debug.DebugReceiver.sLogger.d("Uploading " + android.text.format.Formatter.formatFileSize(com.navdy.hud.app.HudApplication.getAppContext(), (long) finalSize));
                byte[] data = new byte[finalSize];
                new java.util.Random().nextBytes(data);
                os.write(data);
                if (urlConnection.getResponseCode() == 200) {
                    com.navdy.hud.app.debug.DebugReceiver.sLogger.d("POST succeeded");
                }
                double timeElapsed = (double) (android.os.SystemClock.elapsedRealtime() - 0);
                com.navdy.hud.app.debug.DebugReceiver.sLogger.e("Upload finished, Size : " + (0 / 1000) + ", Time Taken :" + ((long) timeElapsed) + " milliseconds , RawSpeed :" + ((float) (((double) (((float) 0) / 1000.0f)) / (timeElapsed / 1000.0d))) + " kBps");
                com.navdy.service.library.util.IOUtils.closeStream(null);
            } catch (java.net.SocketTimeoutException se) {
                com.navdy.hud.app.debug.DebugReceiver.sLogger.e("Socket timed out Exception ", se);
                double timeElapsed2 = (double) (android.os.SystemClock.elapsedRealtime() - 0);
                com.navdy.hud.app.debug.DebugReceiver.sLogger.e("Upload finished, Size : " + (0 / 1000) + ", Time Taken :" + ((long) timeElapsed2) + " milliseconds , RawSpeed :" + ((float) (((double) (((float) 0) / 1000.0f)) / (timeElapsed2 / 1000.0d))) + " kBps");
                com.navdy.service.library.util.IOUtils.closeStream(null);
            } catch (java.io.IOException e) {
                com.navdy.hud.app.debug.DebugReceiver.sLogger.e("IOException ", e);
                double timeElapsed3 = (double) (android.os.SystemClock.elapsedRealtime() - 0);
                com.navdy.hud.app.debug.DebugReceiver.sLogger.e("Upload finished, Size : " + (0 / 1000) + ", Time Taken :" + ((long) timeElapsed3) + " milliseconds , RawSpeed :" + ((float) (((double) (((float) 0) / 1000.0f)) / (timeElapsed3 / 1000.0d))) + " kBps");
                com.navdy.service.library.util.IOUtils.closeStream(null);
            } catch (java.lang.Exception e2) {
                com.navdy.hud.app.debug.DebugReceiver.sLogger.e("Exception ", e2);
                double timeElapsed4 = (double) (android.os.SystemClock.elapsedRealtime() - 0);
                com.navdy.hud.app.debug.DebugReceiver.sLogger.e("Upload finished, Size : " + (0 / 1000) + ", Time Taken :" + ((long) timeElapsed4) + " milliseconds , RawSpeed :" + ((float) (((double) (((float) 0) / 1000.0f)) / (timeElapsed4 / 1000.0d))) + " kBps");
                com.navdy.service.library.util.IOUtils.closeStream(null);
            } catch (Throwable th) {
                double timeElapsed5 = (double) (android.os.SystemClock.elapsedRealtime() - 0);
                com.navdy.hud.app.debug.DebugReceiver.sLogger.e("Upload finished, Size : " + (0 / 1000) + ", Time Taken :" + ((long) timeElapsed5) + " milliseconds , RawSpeed :" + ((float) (((double) (((float) 0) / 1000.0f)) / (timeElapsed5 / 1000.0d))) + " kBps");
                com.navdy.service.library.util.IOUtils.closeStream(null);
                throw th;
            }
        }
    }

    class Anon4 implements java.lang.Runnable {
        final /* synthetic */ java.lang.String val$address;
        final /* synthetic */ int val$size;

        Anon4(int i, java.lang.String str) {
            this.val$size = i;
            this.val$address = str;
        }

        public void run() {
            java.io.InputStream in = null;
            long startTime = 0;
            long totalBytesRead = 0;
            try {
                com.navdy.hud.app.debug.DebugReceiver.sLogger.d("Intended Size to be dowloaded is " + this.val$size);
                java.net.URL url = new java.net.URL(this.val$address);
                java.net.HttpURLConnection urlConnection = (java.net.HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.setConnectTimeout(30000);
                urlConnection.setReadTimeout(30000);
                int responseCode = urlConnection.getResponseCode();
                if (responseCode == 200) {
                    com.navdy.hud.app.debug.DebugReceiver.sLogger.d("Starting to download");
                    in = urlConnection.getInputStream();
                    byte[] buffer = new byte[102400];
                    startTime = android.os.SystemClock.elapsedRealtime();
                    while (true) {
                        long bytesRead = (long) in.read(buffer);
                        if (bytesRead == -1) {
                            break;
                        }
                        totalBytesRead += bytesRead;
                    }
                } else {
                    com.navdy.hud.app.debug.DebugReceiver.sLogger.e("Response code not OK " + responseCode);
                }
                double timeElapsed = (double) (android.os.SystemClock.elapsedRealtime() - startTime);
                com.navdy.hud.app.debug.DebugReceiver.sLogger.e("Download finished, Size : " + (totalBytesRead / 1000) + ", Time Taken :" + ((long) timeElapsed) + " milliseconds , RawSpeed :" + ((float) (((double) (((float) totalBytesRead) / 1000.0f)) / (timeElapsed / 1000.0d))) + " kBps");
                com.navdy.service.library.util.IOUtils.closeStream(in);
            } catch (java.net.SocketTimeoutException se) {
                com.navdy.hud.app.debug.DebugReceiver.sLogger.e("Socket timed out Exception ", se);
                double timeElapsed2 = (double) (android.os.SystemClock.elapsedRealtime() - 0);
                com.navdy.hud.app.debug.DebugReceiver.sLogger.e("Download finished, Size : " + (0 / 1000) + ", Time Taken :" + ((long) timeElapsed2) + " milliseconds , RawSpeed :" + ((float) (((double) (((float) 0) / 1000.0f)) / (timeElapsed2 / 1000.0d))) + " kBps");
                com.navdy.service.library.util.IOUtils.closeStream(null);
            } catch (java.io.IOException e) {
                com.navdy.hud.app.debug.DebugReceiver.sLogger.e("IOException ", e);
                double timeElapsed3 = (double) (android.os.SystemClock.elapsedRealtime() - 0);
                com.navdy.hud.app.debug.DebugReceiver.sLogger.e("Download finished, Size : " + (0 / 1000) + ", Time Taken :" + ((long) timeElapsed3) + " milliseconds , RawSpeed :" + ((float) (((double) (((float) 0) / 1000.0f)) / (timeElapsed3 / 1000.0d))) + " kBps");
                com.navdy.service.library.util.IOUtils.closeStream(null);
            } catch (java.lang.Exception e2) {
                com.navdy.hud.app.debug.DebugReceiver.sLogger.e("Exception ", e2);
                double timeElapsed4 = (double) (android.os.SystemClock.elapsedRealtime() - 0);
                com.navdy.hud.app.debug.DebugReceiver.sLogger.e("Download finished, Size : " + (0 / 1000) + ", Time Taken :" + ((long) timeElapsed4) + " milliseconds , RawSpeed :" + ((float) (((double) (((float) 0) / 1000.0f)) / (timeElapsed4 / 1000.0d))) + " kBps");
                com.navdy.service.library.util.IOUtils.closeStream(null);
            } catch (Throwable th) {
                double timeElapsed5 = (double) (android.os.SystemClock.elapsedRealtime() - 0);
                com.navdy.hud.app.debug.DebugReceiver.sLogger.e("Download finished, Size : " + (0 / 1000) + ", Time Taken :" + ((long) timeElapsed5) + " milliseconds , RawSpeed :" + ((float) (((double) (((float) 0) / 1000.0f)) / (timeElapsed5 / 1000.0d))) + " kBps");
                com.navdy.service.library.util.IOUtils.closeStream(null);
                throw th;
            }
        }
    }

    class Anon5 implements java.lang.Runnable {
        Anon5() {
        }

        public void run() {
            com.navdy.hud.app.debug.DebugReceiver.this.busywait(10000);
        }
    }

    class Anon6 implements java.lang.Runnable {
        Anon6() {
        }

        public void run() {
            java.net.HttpURLConnection connection = null;
            java.io.InputStream inputStream = null;
            try {
                com.navdy.hud.app.debug.DebugReceiver.sLogger.v("getGoogleHomePage");
                connection = (java.net.HttpURLConnection) new java.net.URL("https://www.google.com").openConnection();
                connection.setRequestMethod("GET");
                inputStream = connection.getInputStream();
                java.lang.String data = com.navdy.service.library.util.IOUtils.convertInputStreamToString(inputStream, "UTF-8");
                java.util.Map<java.lang.String, java.util.List<java.lang.String>> headers = connection.getHeaderFields();
                for (java.lang.String key : headers.keySet()) {
                    java.lang.String val = (java.lang.String) ((java.util.List) headers.get(key)).get(0);
                    if (com.amazonaws.services.s3.internal.Constants.NULL_VERSION_ID.equals(key) || key == null) {
                        com.navdy.hud.app.debug.DebugReceiver.sLogger.v("getGoogleHomePage " + val);
                    } else {
                        com.navdy.hud.app.debug.DebugReceiver.sLogger.v("getGoogleHomePage " + key + ":  " + val);
                    }
                }
                com.navdy.hud.app.debug.DebugReceiver.sLogger.v("getGoogleHomePage " + data);
                com.navdy.hud.app.debug.DebugReceiver.sLogger.v("getGoogleHomePage LEN = " + data.length());
                com.navdy.service.library.util.IOUtils.closeStream(inputStream);
                if (connection != null) {
                    try {
                        connection.disconnect();
                    } catch (Throwable t) {
                        com.navdy.hud.app.debug.DebugReceiver.sLogger.e(t);
                    }
                }
            } catch (Throwable t2) {
                com.navdy.hud.app.debug.DebugReceiver.sLogger.e(t2);
            }
        }
    }

    class Anon7 implements java.lang.Runnable {
        final /* synthetic */ java.lang.String val$host;

        Anon7(java.lang.String str) {
            this.val$host = str;
        }

        public void run() {
            try {
                java.lang.String hostName = this.val$host;
                if (hostName == null) {
                    hostName = "analytics.localytics.com";
                }
                com.navdy.hud.app.debug.DebugReceiver.sLogger.v("dnslookup :" + hostName);
                java.net.InetAddress[] addresses = java.net.InetAddress.getAllByName(hostName);
                if (addresses != null) {
                    com.navdy.hud.app.debug.DebugReceiver.sLogger.v("dnslookup total:" + addresses.length);
                    for (java.net.InetAddress hostAddress : addresses) {
                        com.navdy.hud.app.debug.DebugReceiver.sLogger.v("dnslookup address :" + hostAddress.getHostAddress());
                    }
                    return;
                }
                com.navdy.hud.app.debug.DebugReceiver.sLogger.v("dnslookup no address");
            } catch (Throwable t) {
                com.navdy.hud.app.debug.DebugReceiver.sLogger.e(t);
            }
        }
    }

    class Anon8 implements java.lang.Runnable {
        Anon8() {
        }

        public void run() {
            try {
                java.lang.String[] hosts = {"version.hybrid.api.here.com", "reverse.geocoder.api.here.com", "tpeg.traffic.api.here.com", "tpeg.hybrid.api.here.com", "download.hybrid.api.here.com", "v102-62-30-8.route.hybrid.api.here.com", "analytics.localytics.com", "profile.localytics.com", "sdk.hockeyapp.net", "navdyhud.atlassian.net"};
                com.navdy.hud.app.debug.DebugReceiver.sLogger.v("dns_lookup_test");
                for (int i = 0; i < hosts.length; i++) {
                    java.net.InetAddress[] addresses = java.net.InetAddress.getAllByName(hosts[i]);
                    if (addresses != null) {
                        com.navdy.hud.app.debug.DebugReceiver.sLogger.v("dns_lookup_test[" + hosts[i] + "] addresses = " + addresses.length);
                        for (java.net.InetAddress hostAddress : addresses) {
                            com.navdy.hud.app.debug.DebugReceiver.sLogger.v("dns_lookup_test[" + hosts[i] + "]  addr[" + hostAddress.getHostAddress() + "]");
                        }
                    } else {
                        com.navdy.hud.app.debug.DebugReceiver.sLogger.v("dns_lookup_test[" + hosts[i] + "] no address");
                    }
                }
            } catch (Throwable t) {
                com.navdy.hud.app.debug.DebugReceiver.sLogger.e(t);
            }
        }
    }

    private static class PrinterImpl implements android.util.Printer {
        private PrinterImpl() {
        }

        /* synthetic */ PrinterImpl(com.navdy.hud.app.debug.DebugReceiver.Anon1 x0) {
            this();
        }

        public void println(java.lang.String str) {
            com.navdy.hud.app.debug.DebugReceiver.sLogger.v(str);
        }
    }

    public void onReceive(android.content.Context context, android.content.Intent intent) {
        com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.ManeuverState state;
        if (com.navdy.hud.app.BuildConfig.DEBUG) {
            android.content.res.Resources resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
            if (this.mBus == null) {
                mortar.Mortar.inject(com.navdy.hud.app.HudApplication.getAppContext(), this);
            }
            java.lang.String action = intent.getAction();
            sLogger.v("action=" + action);
            char c = 65535;
            switch (action.hashCode()) {
                case -2101248873:
                    if (action.equals(ACTION_SET_SIMULATION_SPEED)) {
                        c = '!';
                        break;
                    }
                    break;
                case -2089995553:
                    if (action.equals(ACTION_PRINT_MAP_NAV_MODE)) {
                        c = 11;
                        break;
                    }
                    break;
                case -1968085686:
                    if (action.equals(ACTION_SHOW_LANE_INFO)) {
                        c = 17;
                        break;
                    }
                    break;
                case -1941214184:
                    if (action.equals(ACTION_SET_DISTANCE_MANEUVER)) {
                        c = '0';
                        break;
                    }
                    break;
                case -1939990980:
                    if (action.equals(ACTION_CRASH)) {
                        c = 3;
                        break;
                    }
                    break;
                case -1885821101:
                    if (action.equals(ACTION_PRINT_MAP_ZOOM)) {
                        c = 13;
                        break;
                    }
                    break;
                case -1875086992:
                    if (action.equals(ACTION_SET_THEN_MANEUVER)) {
                        c = '.';
                        break;
                    }
                    break;
                case -1767847257:
                    if (action.equals(ACTION_TEST)) {
                        c = 0;
                        break;
                    }
                    break;
                case -1651982344:
                    if (action.equals(ACTION_UPDATE_MAP_DATA)) {
                        c = '2';
                        break;
                    }
                    break;
                case -1494888723:
                    if (action.equals(ACTION_SEND_NOW_MANEUVER)) {
                        c = ')';
                        break;
                    }
                    break;
                case -1467749358:
                    if (action.equals(ACTION_SEND_SOON_MANEUVER)) {
                        c = '*';
                        break;
                    }
                    break;
                case -1364380688:
                    if (action.equals(ACTION_PLAYBACK_ROUTE)) {
                        c = 6;
                        break;
                    }
                    break;
                case -1267899786:
                    if (action.equals(ACTION_DNS_LOOKUP_TEST)) {
                        c = 27;
                        break;
                    }
                    break;
                case -1168228003:
                    if (action.equals(DISABLE_HERE_LOC_DBG)) {
                        c = '(';
                        break;
                    }
                    break;
                case -1129282191:
                    if (action.equals(ACTION_NATIVE_CRASH_SEPARATE_THREAD)) {
                        c = 5;
                        break;
                    }
                    break;
                case -1118754335:
                    if (action.equals(ACTION_TEST_TBT_TTS_CANCEL)) {
                        c = kotlin.text.Typography.dollar;
                        break;
                    }
                    break;
                case -1113155266:
                    if (action.equals(ACTION_CHANGE_HEADING)) {
                        c = '3';
                        break;
                    }
                    break;
                case -1090060907:
                    if (action.equals(ACTION_PRINT_BUS_REGISTRATION)) {
                        c = 14;
                        break;
                    }
                    break;
                case -943057193:
                    if (action.equals(ACTION_STOP_PLAYBACK_ROUTE)) {
                        c = 8;
                        break;
                    }
                    break;
                case -778616767:
                    if (action.equals(ACTION_INJECT_JUNCTION_VIEW)) {
                        c = '%';
                        break;
                    }
                    break;
                case -740170429:
                    if (action.equals(ACTION_CHECK_FOR_MAP_UPDATES)) {
                        c = '1';
                        break;
                    }
                    break;
                case -662236452:
                    if (action.equals(ACTION_BROADCAST_ANR)) {
                        c = 2;
                        break;
                    }
                    break;
                case -660570207:
                    if (action.equals(ACTION_START_CPU_HOG)) {
                        c = 23;
                        break;
                    }
                    break;
                case -590528811:
                    if (action.equals(ACTION_STOP_BUS_PROFILING)) {
                        c = 20;
                        break;
                    }
                    break;
                case -462260586:
                    if (action.equals(ACTION_CLEAR_MANEUVER)) {
                        c = ',';
                        break;
                    }
                    break;
                case -427047973:
                    if (action.equals(ACTION_DNS_LOOKUP)) {
                        c = 26;
                        break;
                    }
                    break;
                case -329707059:
                    if (action.equals(ACTION_GET_GOOGLE)) {
                        c = 25;
                        break;
                    }
                    break;
                case -261238342:
                    if (action.equals(ACTION_ANR)) {
                        c = 1;
                        break;
                    }
                    break;
                case -134867633:
                    if (action.equals(ACTION_HIDE_LANE_INFO)) {
                        c = 18;
                        break;
                    }
                    break;
                case -130610207:
                    if (action.equals(ACTION_START_MAP_RENDERING)) {
                        c = 21;
                        break;
                    }
                    break;
                case -82711916:
                    if (action.equals(ACTION_SET_ICON_MANEUVER)) {
                        c = com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.SLASH_CHAR;
                        break;
                    }
                    break;
                case -78416200:
                    if (action.equals(ACTION_TEST_TBT_TTS)) {
                        c = '#';
                        break;
                    }
                    break;
                case 17174837:
                    if (action.equals(ACTION_SET_INSTRUCTION_MANEUVER)) {
                        c = '-';
                        break;
                    }
                    break;
                case 213838479:
                    if (action.equals(ACTION_START_MAIN_THREAD_PROFILING)) {
                        c = 15;
                        break;
                    }
                    break;
                case 350926914:
                    if (action.equals(ACTION_SCALE_DISPLAY)) {
                        c = 10;
                        break;
                    }
                    break;
                case 502462150:
                    if (action.equals(ACTION_NETSTAT)) {
                        c = 28;
                        break;
                    }
                    break;
                case 527998295:
                    if (action.equals(ACTION_STOP_CPU_HOG)) {
                        c = 24;
                        break;
                    }
                    break;
                case 546989866:
                    if (action.equals(ACTION_WAKEUP)) {
                        c = 29;
                        break;
                    }
                    break;
                case 549827532:
                    if (action.equals(ENABLE_HERE_LOC_DBG)) {
                        c = '\'';
                        break;
                    }
                    break;
                case 627556457:
                    if (action.equals(ACTION_DUMP_THREAD_STACK)) {
                        c = 12;
                        break;
                    }
                    break;
                case 698498228:
                    if (action.equals(ACTION_SEND_STAY_MANEUVER)) {
                        c = '+';
                        break;
                    }
                    break;
                case 742819919:
                    if (action.equals(ACTION_LAUNCH_PICKER)) {
                        c = 30;
                        break;
                    }
                    break;
                case 768824218:
                    if (action.equals(ACTION_HIDE_RECALC)) {
                        c = com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.SPACE_CHAR;
                        break;
                    }
                    break;
                case 821152171:
                    if (action.equals(ACTION_EJECT_JUNCTION_VIEW)) {
                        c = kotlin.text.Typography.amp;
                        break;
                    }
                    break;
                case 916312517:
                    if (action.equals(ACTION_STOP_MAIN_THREAD_PROFILING)) {
                        c = 16;
                        break;
                    }
                    break;
                case 1024361413:
                    if (action.equals(ACTION_PLAYBACK_ROUTE_SECONDARY)) {
                        c = 7;
                        break;
                    }
                    break;
                case 1277394303:
                    if (action.equals(ACTION_SHOW_RECALC)) {
                        c = 31;
                        break;
                    }
                    break;
                case 1523880594:
                    if (action.equals(ACTION_SET_START_ROUTE_CALC_POINT)) {
                        c = 9;
                        break;
                    }
                    break;
                case 1737539999:
                    if (action.equals(ACTION_START_BUS_PROFILING)) {
                        c = 19;
                        break;
                    }
                    break;
                case 1836288279:
                    if (action.equals(ACTION_STOP_MAP_RENDERING)) {
                        c = 22;
                        break;
                    }
                    break;
                case 1949920157:
                    if (action.equals(ACTION_LAUNCH_MUSIC_DETAILS)) {
                        c = kotlin.text.Typography.quote;
                        break;
                    }
                    break;
                case 2131637514:
                    if (action.equals(ACTION_NATIVE_CRASH)) {
                        c = 4;
                        break;
                    }
                    break;
            }
            switch (c) {
                case 0:
                    java.lang.String test = intent.getStringExtra(EXTRA_TEST);
                    if (!android.text.TextUtils.isEmpty(test)) {
                        char c2 = 65535;
                        switch (test.hashCode()) {
                            case -1542504236:
                                if (test.equals(TEST_OBD_RESET)) {
                                    c2 = 14;
                                    break;
                                }
                                break;
                            case -1541385636:
                                if (test.equals(TEST_OBD_SLEEP)) {
                                    c2 = 13;
                                    break;
                                }
                                break;
                            case -1403724419:
                                if (test.equals(TEST_OBD_FLASH_OLD_FIRMWARE)) {
                                    c2 = 16;
                                    break;
                                }
                                break;
                            case -1277838812:
                                if (test.equals(TEST_OBD_FLASH_NEW_FIRMWARE)) {
                                    c2 = 15;
                                    break;
                                }
                                break;
                            case -1056202413:
                                if (test.equals(TEST_SWITCH_BANDWIDTH_LEVEL)) {
                                    c2 = 8;
                                    break;
                                }
                                break;
                            case -1040948652:
                                if (test.equals(TEST_OBD_SET_MODE_J1939)) {
                                    c2 = 17;
                                    break;
                                }
                                break;
                            case -830101751:
                                if (test.equals(TEST_LIGHT)) {
                                    c2 = 3;
                                    break;
                                }
                                break;
                            case -449055355:
                                if (test.equals(TEST_OBD_SET_MODE_OBD2)) {
                                    c2 = 18;
                                    break;
                                }
                                break;
                            case -362871900:
                                if (test.equals(TEST_OBD_CONFIG)) {
                                    c2 = 6;
                                    break;
                                }
                                break;
                            case -362866522:
                                if (test.equals(TEST_TTS)) {
                                    c2 = 4;
                                    break;
                                }
                                break;
                            case 71523:
                                if (test.equals(TEST_HID)) {
                                    c2 = 0;
                                    break;
                                }
                                break;
                            case 104851938:
                                if (test.equals(TEST_ENABLE_OBD_PIDS_SCANNING)) {
                                    c2 = 12;
                                    break;
                                }
                                break;
                            case 468498573:
                                if (test.equals(TEST_PROXY_FILE_DOWNLOAD)) {
                                    c2 = 9;
                                    break;
                                }
                                break;
                            case 613283414:
                                if (test.equals(TEST_SHUT_DOWN)) {
                                    c2 = 2;
                                    break;
                                }
                                break;
                            case 997449616:
                                if (test.equals(TEST_SEND_NOTIFICATION_EVENT)) {
                                    c2 = 19;
                                    break;
                                }
                                break;
                            case 1032794997:
                                if (test.equals(TEST_LAUNCH_APP)) {
                                    c2 = 1;
                                    break;
                                }
                                break;
                            case 1180516166:
                                if (test.equals(TEST_PROXY_FILE_UPLOAD)) {
                                    c2 = 10;
                                    break;
                                }
                                break;
                            case 1635551997:
                                if (test.equals(TEST_DIAL)) {
                                    c2 = 5;
                                    break;
                                }
                                break;
                            case 1709488813:
                                if (test.equals(TEST_DISABLE_OBD_PIDS_SCANNING)) {
                                    c2 = 11;
                                    break;
                                }
                                break;
                            case 2084450626:
                                if (test.equals(TEST_DESTINATION_SUGGESTION)) {
                                    c2 = 7;
                                    break;
                                }
                                break;
                        }
                        switch (c2) {
                            case 0:
                                testHID(intent);
                                return;
                            case 1:
                                testAppLaunch(intent);
                                return;
                            case 2:
                                testShutDown();
                                return;
                            case 3:
                                testLight(intent);
                                return;
                            case 4:
                                testTTS(intent);
                                return;
                            case 5:
                                testDial(intent);
                                return;
                            case 6:
                                testObdConfig(intent);
                                return;
                            case 7:
                                testDestinationSuggestion(intent);
                                return;
                            case 8:
                                testSwitchBandwidthLevel(intent);
                                return;
                            case 9:
                                testProxyFileDownload(intent);
                                return;
                            case 10:
                                testProxyFileUpload(intent);
                                return;
                            case 11:
                                com.navdy.hud.app.obd.ObdManager.getInstance().enableObdScanning(false);
                                return;
                            case 12:
                                com.navdy.hud.app.obd.ObdManager.getInstance().enableObdScanning(true);
                                return;
                            case 13:
                                com.navdy.hud.app.obd.ObdManager.getInstance().sleep(intent.getBooleanExtra(EXTRA_DEEP, false));
                                return;
                            case 14:
                                com.navdy.hud.app.obd.ObdManager.getInstance().wakeup();
                                return;
                            case 15:
                                com.navdy.hud.app.obd.ObdManager.getInstance().updateFirmware(com.navdy.hud.app.obd.ObdManager.Firmware.NEW_421);
                                break;
                            case 16:
                                break;
                            case 17:
                                com.navdy.hud.app.obd.ObdManager.getInstance().setMode(1, true);
                                return;
                            case 18:
                                com.navdy.hud.app.obd.ObdManager.getInstance().setMode(0, true);
                                return;
                            case 19:
                                testSendNotificationEvent(intent);
                                return;
                            default:
                                return;
                        }
                        com.navdy.hud.app.obd.ObdManager.getInstance().updateFirmware(com.navdy.hud.app.obd.ObdManager.Firmware.OLD_320);
                        return;
                    }
                    return;
                case 1:
                    sLogger.i("Triggering an ANR on main thread");
                    triggerANR();
                    return;
                case 2:
                    sLogger.i("Triggering an ANR in receiver");
                    triggerBroadcastANR();
                    return;
                case 3:
                    sLogger.i("Triggering a crash");
                    triggerCrash();
                    return;
                case 4:
                    sLogger.i("Triggering a native crash");
                    com.navdy.hud.app.util.NavdyNativeLibWrapper.crashSegv();
                    return;
                case 5:
                    sLogger.i("Triggering a native crash on a native thread");
                    com.navdy.hud.app.util.NavdyNativeLibWrapper.crashSegvOnNewThread();
                    return;
                case 6:
                    java.lang.String fileName = intent.getStringExtra("drive_log_filename");
                    sLogger.i("Triggering a route playback");
                    com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getConnectionHandler().sendLocalMessage(new com.navdy.service.library.events.debug.StartDrivePlaybackEvent(fileName, java.lang.Boolean.valueOf(false)));
                    return;
                case 7:
                    java.lang.String fileName2 = intent.getStringExtra("drive_log_filename");
                    sLogger.i("Triggering a secondary route playback");
                    com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getConnectionHandler().sendLocalMessage(new com.navdy.service.library.events.debug.StartDrivePlaybackEvent(fileName2, java.lang.Boolean.valueOf(true)));
                    return;
                case 8:
                    sLogger.i("Triggering a stop route playback");
                    com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getConnectionHandler().sendLocalMessage(new com.navdy.service.library.events.debug.StopDrivePlaybackEvent());
                    return;
                case 9:
                    try {
                        com.navdy.hud.app.maps.here.HereMapsManager hereMapsManager = com.navdy.hud.app.maps.here.HereMapsManager.getInstance();
                        if (!hereMapsManager.isInitialized()) {
                            sLogger.v("set routeStartPoint: engine not initialized");
                            return;
                        }
                        android.os.Bundle bundle = intent.getExtras();
                        if (bundle == null) {
                            sLogger.v("set routeStartPoint: no extras");
                            return;
                        }
                        java.lang.String latStr = bundle.getString("LAT");
                        java.lang.String lngStr = bundle.getString("LNG");
                        if (android.text.TextUtils.isEmpty(latStr) || android.text.TextUtils.isEmpty(lngStr)) {
                            sLogger.v("set routeStartPoint: lat/lng not specified");
                            return;
                        }
                        double lat = java.lang.Double.parseDouble(latStr);
                        double lng = java.lang.Double.parseDouble(lngStr);
                        if (lat == 0.0d && lng == 0.0d) {
                            hereMapsManager.setRouteStartPoint(null);
                            sLogger.v("set routeStartPoint to null");
                            return;
                        }
                        com.here.android.mpa.common.GeoCoordinate geoCoordinate = new com.here.android.mpa.common.GeoCoordinate(lat, lng);
                        hereMapsManager.setRouteStartPoint(geoCoordinate);
                        if (hereMapsManager.getLocationFixManager().getLastGeoCoordinate() == null) {
                            sLogger.v("set routeStartPoint to center");
                            android.location.Location location = new android.location.Location("network");
                            location.setLatitude(lat);
                            location.setLongitude(lng);
                            location.setAltitude(0.0d);
                            hereMapsManager.getLocationFixManager().setMaptoLocation(location);
                        }
                        sLogger.v("set routeStartPoint to " + geoCoordinate);
                        return;
                    } catch (Throwable t) {
                        sLogger.e(t);
                        return;
                    }
                case 10:
                    java.lang.String mode = intent.getExtras().getString("format", com.navdy.hud.app.device.PowerManager.NORMAL_MODE);
                    com.navdy.service.library.events.preferences.DriverProfilePreferences.DisplayFormat format = com.navdy.service.library.events.preferences.DriverProfilePreferences.DisplayFormat.DISPLAY_FORMAT_NORMAL;
                    if ("compact".equals(mode)) {
                        format = com.navdy.service.library.events.preferences.DriverProfilePreferences.DisplayFormat.DISPLAY_FORMAT_COMPACT;
                    }
                    sLogger.v("setting display format to " + format);
                    this.mBus.post(new com.navdy.hud.app.event.DisplayScaleChange(format));
                    return;
                case 11:
                    try {
                        com.navdy.hud.app.maps.here.HereMapsManager hereMapsManager2 = com.navdy.hud.app.maps.here.HereMapsManager.getInstance();
                        if (!hereMapsManager2.isInitialized()) {
                            sLogger.v("print nav mode: engine not initialized");
                            return;
                        }
                        com.navdy.hud.app.maps.here.HereNavController controller = com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().getNavController();
                        sLogger.v("[MAP-UPDATE-MODE]" + hereMapsManager2.getMapAnimator().getAnimationMode());
                        sLogger.v("[MAP-NAV-MODE]" + controller.getState());
                        return;
                    } catch (Throwable t2) {
                        sLogger.e(t2);
                        return;
                    }
                case 12:
                    java.util.Map<java.lang.Thread, java.lang.StackTraceElement[]> maps = java.lang.Thread.getAllStackTraces();
                    sLogger.v("Total threads: " + maps.size());
                    for (java.util.Map.Entry<java.lang.Thread, java.lang.StackTraceElement[]> entry : maps.entrySet()) {
                        java.lang.Thread t3 = (java.lang.Thread) entry.getKey();
                        java.lang.StackTraceElement[] elements = (java.lang.StackTraceElement[]) entry.getValue();
                        sLogger.v("Thread " + t3.getId() + " -- " + t3.getName());
                        for (java.lang.StackTraceElement element : elements) {
                            sLogger.v("     " + element.getClassName() + com.navdy.hud.app.framework.glance.GlanceConstants.PERIOD + element.getMethodName() + " (" + element.getFileName() + com.navdy.hud.app.framework.glance.GlanceConstants.COLON_SEPARATOR + (element.isNativeMethod() ? "" : java.lang.Integer.valueOf(element.getLineNumber())) + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.CLOSE_BRACKET + (element.isNativeMethod() ? " native" : ""));
                        }
                        sLogger.v(" ");
                    }
                    return;
                case 13:
                    com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.debug.DebugReceiver.Anon1(), 1);
                    return;
                case 14:
                    com.navdy.hud.app.debug.BusLeakDetector.getInstance().printReferences();
                    return;
                case 15:
                    int threshold = intent.getIntExtra(MAIN_THREAD_EXTRA_PROFILING_THRESHOLD, 0);
                    android.os.Looper looper = android.os.Looper.getMainLooper();
                    if (com.navdy.hud.app.util.DeviceUtil.isNavdyDevice()) {
                        try {
                            java.lang.reflect.Field field = looper.getClass().getDeclaredField("mLoggingThreshold");
                            field.setAccessible(true);
                            field.setInt(looper, threshold);
                            sLogger.v("START main thread threshold set:" + threshold);
                        } catch (Throwable t4) {
                            sLogger.e(t4);
                        }
                    }
                    looper.setMessageLogging(this.mainThreadProfilingPrinter);
                    sLogger.v("START main thread profiling");
                    return;
                case 16:
                    android.os.Looper.getMainLooper().setMessageLogging(null);
                    sLogger.v("STOP main thread profiling");
                    return;
                case 17:
                    java.util.ArrayList<com.navdy.hud.app.maps.MapEvents.LaneData> laneData = new java.util.ArrayList<>();
                    laneData.add(new com.navdy.hud.app.maps.MapEvents.LaneData(com.navdy.hud.app.maps.MapEvents.LaneData.Position.OFF_ROUTE, new android.graphics.drawable.Drawable[]{resources.getDrawable(com.navdy.hud.app.R.drawable.icon_lg_compboth_left), resources.getDrawable(com.navdy.hud.app.R.drawable.icon_lg_compboth_straight)}));
                    laneData.add(new com.navdy.hud.app.maps.MapEvents.LaneData(com.navdy.hud.app.maps.MapEvents.LaneData.Position.OFF_ROUTE, new android.graphics.drawable.Drawable[]{resources.getDrawable(com.navdy.hud.app.R.drawable.icon_lg_left_only)}));
                    laneData.add(new com.navdy.hud.app.maps.MapEvents.LaneData(com.navdy.hud.app.maps.MapEvents.LaneData.Position.OFF_ROUTE, new android.graphics.drawable.Drawable[]{resources.getDrawable(com.navdy.hud.app.R.drawable.icon_lg_rec_straight_only)}));
                    laneData.add(new com.navdy.hud.app.maps.MapEvents.LaneData(com.navdy.hud.app.maps.MapEvents.LaneData.Position.ON_ROUTE, new android.graphics.drawable.Drawable[]{resources.getDrawable(com.navdy.hud.app.R.drawable.icon_lg_rec_straight_only)}));
                    laneData.add(new com.navdy.hud.app.maps.MapEvents.LaneData(com.navdy.hud.app.maps.MapEvents.LaneData.Position.ON_ROUTE, new android.graphics.drawable.Drawable[]{resources.getDrawable(com.navdy.hud.app.R.drawable.icon_lg_right_only), resources.getDrawable(com.navdy.hud.app.R.drawable.icon_lg_compright_rec_straight)}));
                    com.navdy.hud.app.maps.MapEvents.DisplayTrafficLaneInfo displayTrafficLaneInfo = new com.navdy.hud.app.maps.MapEvents.DisplayTrafficLaneInfo(laneData);
                    this.mBus.post(displayTrafficLaneInfo);
                    sLogger.v("show lane info");
                    return;
                case 18:
                    this.mBus.post(new com.navdy.hud.app.maps.MapEvents.HideTrafficLaneInfo());
                    sLogger.v("hide lane info");
                    return;
                case 19:
                    int threshold2 = intent.getIntExtra(MAIN_THREAD_EXTRA_PROFILING_THRESHOLD, 0);
                    if (threshold2 <= 0) {
                        sLogger.v("START BUS profiling, invalid thrshold:" + threshold2);
                        return;
                    }
                    ((com.navdy.hud.app.common.MainThreadBus) this.mBus).threshold = threshold2;
                    sLogger.v("START BUS profiling");
                    return;
                case 20:
                    sLogger.v("STOP BUS profiling");
                    ((com.navdy.hud.app.common.MainThreadBus) this.mBus).threshold = 0;
                    return;
                case 21:
                    com.navdy.hud.app.maps.here.HereMapsManager.getInstance().startMapRendering();
                    return;
                case 22:
                    com.navdy.hud.app.maps.here.HereMapsManager.getInstance().stopMapRendering();
                    return;
                case 23:
                    com.navdy.hud.app.util.NavdyNativeLibWrapper.startCpuHog();
                    return;
                case 24:
                    com.navdy.hud.app.util.NavdyNativeLibWrapper.stopCpuHog();
                    return;
                case 25:
                    getGoogleHomePage();
                    return;
                case 26:
                    dnsLookup(intent.getStringExtra(DNS_LOOKUP_HOST_NAME));
                    return;
                case 27:
                    dnsLookupTest();
                    return;
                case 28:
                    com.navdy.hud.app.framework.network.NetworkBandwidthController.getInstance().netStat();
                    return;
                case 29:
                    sLogger.v("send wake up");
                    this.mBus.post(new com.navdy.hud.app.event.Wakeup(com.navdy.hud.app.analytics.AnalyticsSupport.WakeupReason.POWERBUTTON));
                    return;
                case 30:
                    sLogger.v("launch picker screen");
                    android.os.Bundle args = new android.os.Bundle();
                    args.putBoolean(com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.PICKER_SHOW_DESTINATION_MAP, true);
                    args.putString(com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.PICKER_LEFT_TITLE, resources.getString(com.navdy.hud.app.R.string.quick_search));
                    args.putInt(com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.PICKER_LEFT_ICON, com.navdy.hud.app.R.drawable.icon_mm_search_2);
                    args.putInt(com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.PICKER_LEFT_ICON_BKCOLOR, resources.getColor(com.navdy.hud.app.R.color.mm_search));
                    args.putString(com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.PICKER_TITLE, "5 Results for \"Gas\"");
                    args.putParcelableArray(com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.PICKER_DESTINATIONS, new com.navdy.hud.app.ui.component.destination.DestinationParcelable[]{new com.navdy.hud.app.ui.component.destination.DestinationParcelable(0, "Shell Oil", "123 Boulevard Road", false, "<b>4.2</b> mi", true, "1355 Van Ness Ave San Francisco CA 94107", 37.773583d, -122.408827d, 37.773583d, -122.408827d, com.navdy.hud.app.R.drawable.icon_places_gas_2, 0, resources.getColor(com.navdy.hud.app.R.color.mm_place_gas), resources.getColor(com.navdy.hud.app.R.color.icon_bk_color_unselected), com.navdy.hud.app.ui.component.destination.DestinationParcelable.DestinationType.DESTINATION, com.navdy.service.library.events.places.PlaceType.PLACE_TYPE_GAS), new com.navdy.hud.app.ui.component.destination.DestinationParcelable(0, "Chevron Texaco Gas Stat", "45655 Airport Rd, San Francisco", false, "5.3 mi", false, "123 Somewhere street", 37.777162d, -122.404857d, 37.777162d, -122.404857d, com.navdy.hud.app.R.drawable.icon_places_gas_2, 0, resources.getColor(com.navdy.hud.app.R.color.mm_place_gas), resources.getColor(com.navdy.hud.app.R.color.icon_bk_color_unselected), com.navdy.hud.app.ui.component.destination.DestinationParcelable.DestinationType.DESTINATION, com.navdy.service.library.events.places.PlaceType.PLACE_TYPE_GAS), new com.navdy.hud.app.ui.component.destination.DestinationParcelable(0, "Arco at Fremont and Washington", "Mission Blvd", false, "<b>16.1</b> mi", true, "123 Somewhere street", 37.768523d, -122.420071d, 37.768523d, -122.420071d, com.navdy.hud.app.R.drawable.icon_places_gas_2, 0, resources.getColor(com.navdy.hud.app.R.color.mm_place_gas), resources.getColor(com.navdy.hud.app.R.color.icon_bk_color_unselected), com.navdy.hud.app.ui.component.destination.DestinationParcelable.DestinationType.DESTINATION, com.navdy.service.library.events.places.PlaceType.PLACE_TYPE_GAS), new com.navdy.hud.app.ui.component.destination.DestinationParcelable(0, "Fig gas station, 111 Mission Blvd, Fremont CA", "Market St", false, "<b>6.1</b> mi", true, "123 Somewhere street", 37.768523d, -122.420071d, 37.768523d, -122.420071d, com.navdy.hud.app.R.drawable.icon_places_gas_2, 0, resources.getColor(com.navdy.hud.app.R.color.mm_place_gas), resources.getColor(com.navdy.hud.app.R.color.icon_bk_color_unselected), com.navdy.hud.app.ui.component.destination.DestinationParcelable.DestinationType.DESTINATION, com.navdy.service.library.events.places.PlaceType.PLACE_TYPE_GAS)});
                    this.mBus.post(new com.navdy.hud.app.event.ShowScreenWithArgs(com.navdy.service.library.events.ui.Screen.SCREEN_DESTINATION_PICKER, args, false));
                    return;
                case 31:
                    if (com.navdy.hud.app.maps.here.HereMapsManager.getInstance().isInitialized() && com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().isNavigationModeOn()) {
                        this.mBus.post(com.navdy.hud.app.maps.here.HereRerouteListener.REROUTE_STARTED);
                        return;
                    }
                    return;
                case ' ':
                    if (com.navdy.hud.app.maps.here.HereMapsManager.getInstance().isInitialized() && com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().isNavigationModeOn()) {
                        this.mBus.post(com.navdy.hud.app.maps.here.HereRerouteListener.REROUTE_FINISHED);
                        return;
                    }
                    return;
                case '!':
                    int speed = intent.getIntExtra(SPEED, 0);
                    com.navdy.hud.app.maps.MapSettings.setSimulationSpeed(speed);
                    sLogger.v("simulation speed =" + speed);
                    return;
                case '\"':
                    this.mBus.post(new com.navdy.hud.app.event.ShowScreenWithArgs(com.navdy.service.library.events.ui.Screen.SCREEN_MUSIC_DETAILS, null, false));
                    return;
                case '#':
                    int i = ((int) (java.lang.System.currentTimeMillis() % 6)) + 5;
                    sLogger.v("test tbt tts:" + i);
                    java.lang.StringBuilder b = new java.lang.StringBuilder();
                    for (int j = 0; j < i; j++) {
                        b.append("message number " + i + " ");
                    }
                    this.mBus.post(com.navdy.hud.app.maps.notification.RouteCalculationNotification.CANCEL_TBT_TTS);
                    com.navdy.hud.app.framework.voice.TTSUtils.sendSpeechRequest(b.toString(), com.navdy.service.library.events.audio.Category.SPEECH_TURN_BY_TURN, com.navdy.hud.app.maps.notification.RouteCalculationNotification.ROUTE_TBT_TTS_ID);
                    return;
                case '$':
                    sLogger.v("test tbt tts cancel");
                    this.mBus.post(com.navdy.hud.app.maps.notification.RouteCalculationNotification.CANCEL_TBT_TTS);
                    return;
                case '%':
                    sLogger.v("inject right section");
                    com.navdy.hud.app.ui.component.homescreen.HomeScreenView homeScreenView = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getUiStateManager().getHomescreenView();
                    if (homeScreenView == null) {
                        sLogger.v("home screen view null");
                        return;
                    } else if (!homeScreenView.isNavigationActive()) {
                        sLogger.v("nav not active");
                        return;
                    } else {
                        android.view.ViewGroup viewGroup = (android.view.ViewGroup) android.view.LayoutInflater.from(homeScreenView.getContext()).inflate(com.navdy.hud.app.R.layout.junction_view_lyt, null);
                        android.widget.ImageView view = (android.widget.ImageView) viewGroup.findViewById(com.navdy.hud.app.R.id.image);
                        android.widget.FrameLayout.LayoutParams layoutParams = new android.widget.FrameLayout.LayoutParams(resources.getDimensionPixelSize(com.navdy.hud.app.R.dimen.traffic_junc_notif_img_w), resources.getDimensionPixelSize(com.navdy.hud.app.R.dimen.traffic_junc_notif_img_h));
                        layoutParams.gravity = 83;
                        viewGroup.setLayoutParams(layoutParams);
                        view.setImageResource(com.navdy.hud.app.R.drawable.junction);
                        homeScreenView.injectRightSection(viewGroup);
                        return;
                    }
                case '&':
                    sLogger.v("eject right section");
                    com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getUiStateManager().getHomescreenView().ejectRightSection();
                    return;
                case '\'':
                    setHereDebugLocation(true);
                    return;
                case '(':
                    setHereDebugLocation(false);
                    return;
                case ')':
                case '*':
                case '+':
                case ',':
                    if (action.equals(ACTION_SEND_NOW_MANEUVER)) {
                        state = com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.ManeuverState.NOW;
                    } else {
                        if (action.equals(ACTION_SEND_STAY_MANEUVER)) {
                            state = com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.ManeuverState.STAY;
                        } else {
                            state = com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.ManeuverState.SOON;
                        }
                    }
                    if (com.navdy.hud.app.maps.here.HereMapsManager.getInstance().isInitialized()) {
                        com.navdy.hud.app.maps.here.HereNavigationManager hereNavigationManager = com.navdy.hud.app.maps.here.HereNavigationManager.getInstance();
                        if (hereNavigationManager.isNavigationModeOn()) {
                            hereNavigationManager.setDebugManeuverState(state);
                            hereNavigationManager.postManeuverDisplay();
                            return;
                        }
                        return;
                    }
                    return;
                case '-':
                    java.lang.String instruction = intent.getStringExtra(EXTRA_MANEUVER_INSTRUCTION);
                    if (com.navdy.hud.app.maps.here.HereMapsManager.getInstance().isInitialized()) {
                        com.navdy.hud.app.maps.here.HereNavigationManager hereNavigationManager2 = com.navdy.hud.app.maps.here.HereNavigationManager.getInstance();
                        if (hereNavigationManager2.isNavigationModeOn()) {
                            hereNavigationManager2.setDebugManeuverInstruction(instruction);
                            hereNavigationManager2.postManeuverDisplay();
                            return;
                        }
                        return;
                    }
                    return;
                case '.':
                    int iconId = 0;
                    java.lang.String icon = intent.getStringExtra(EXTRA_MANEUVER_THEN_ICON);
                    if (icon != null) {
                        iconId = com.navdy.hud.app.util.GenericUtil.getDrawableResourceIdFromName(icon);
                    }
                    if (com.navdy.hud.app.maps.here.HereMapsManager.getInstance().isInitialized()) {
                        com.navdy.hud.app.maps.here.HereNavigationManager hereNavigationManager3 = com.navdy.hud.app.maps.here.HereNavigationManager.getInstance();
                        if (hereNavigationManager3.isNavigationModeOn()) {
                            hereNavigationManager3.setDebugNextManeuverIcon(iconId);
                            hereNavigationManager3.postManeuverDisplay();
                            return;
                        }
                        return;
                    }
                    return;
                case '/':
                    int iconId2 = 0;
                    java.lang.String icon2 = intent.getStringExtra(EXTRA_MANEUVER_ICON);
                    if (icon2 != null) {
                        iconId2 = com.navdy.hud.app.util.GenericUtil.getDrawableResourceIdFromName(icon2);
                    }
                    if (com.navdy.hud.app.maps.here.HereMapsManager.getInstance().isInitialized()) {
                        com.navdy.hud.app.maps.here.HereNavigationManager hereNavigationManager4 = com.navdy.hud.app.maps.here.HereNavigationManager.getInstance();
                        if (hereNavigationManager4.isNavigationModeOn()) {
                            hereNavigationManager4.setDebugManeuverIcon(iconId2);
                            hereNavigationManager4.postManeuverDisplay();
                            return;
                        }
                        return;
                    }
                    return;
                case '0':
                    java.lang.String distance = intent.getStringExtra(EXTRA_MANEUVER_DISTANCE);
                    if (com.navdy.hud.app.maps.here.HereMapsManager.getInstance().isInitialized()) {
                        com.navdy.hud.app.maps.here.HereNavigationManager hereNavigationManager5 = com.navdy.hud.app.maps.here.HereNavigationManager.getInstance();
                        if (hereNavigationManager5.isNavigationModeOn()) {
                            hereNavigationManager5.setDebugManeuverDistance(distance);
                            hereNavigationManager5.postManeuverDisplay();
                            return;
                        }
                        return;
                    }
                    return;
                case '1':
                    com.navdy.hud.app.maps.here.HereMapsManager.getInstance().checkForMapDataUpdate();
                    return;
                case '3':
                    float heading = intent.getFloatExtra(HEADING, 0.0f);
                    android.location.Location location2 = new android.location.Location("gps");
                    location2.setBearing(heading);
                    sLogger.v("posting location with heading:" + heading);
                    this.mBus.post(location2);
                    return;
                default:
                    return;
            }
        }
    }

    public static void setHereDebugLocation(boolean b) {
        try {
            android.os.Looper.class.getDeclaredField("enableHereLocationDebugging").setBoolean(null, b);
            sLogger.v("setHereDebugLocation set to " + b);
        } catch (Throwable t) {
            sLogger.e("setHereDebugLocation", t);
        }
    }

    private void testHID(android.content.Intent intent) {
        try {
            int mediaKey = intent.getIntExtra(EXTRA_KEY, 0);
            this.mBus.post(new com.navdy.hud.app.event.RemoteEvent(new com.navdy.service.library.events.input.MediaRemoteKeyEvent(com.navdy.service.library.events.input.MediaRemoteKey.values()[mediaKey], com.navdy.service.library.events.input.KeyEvent.KEY_DOWN)));
            new java.util.Timer().schedule(new com.navdy.hud.app.debug.DebugReceiver.Anon2(mediaKey), 100);
        } catch (java.lang.Exception e) {
            sLogger.e("Exception getting the extra");
        }
    }

    private void testAppLaunch(android.content.Intent intent) {
        this.mBus.post(new com.navdy.hud.app.event.RemoteEvent(new com.navdy.service.library.events.input.LaunchAppEvent((java.lang.String) null)));
    }

    private void testShutDown() {
        this.mBus.post(new com.navdy.hud.app.event.ShowScreenWithArgs(com.navdy.service.library.events.ui.Screen.SCREEN_SHUTDOWN_CONFIRMATION, com.navdy.hud.app.event.Shutdown.Reason.POWER_BUTTON.asBundle(), false));
    }

    private void testLight(android.content.Intent intent) {
        int light = intent.getIntExtra(EXTRA_LIGHT, 0);
        com.navdy.hud.app.device.light.LightManager manager = com.navdy.hud.app.device.light.LightManager.getInstance();
        switch (light) {
            case 0:
                com.navdy.hud.app.device.light.HUDLightUtils.showPairing(com.navdy.hud.app.HudApplication.getAppContext(), manager, true);
                return;
            case 1:
                com.navdy.hud.app.device.light.HUDLightUtils.showShutDown(manager);
                return;
            case 2:
                com.navdy.hud.app.device.light.HUDLightUtils.showGestureDetectionEnabled(com.navdy.hud.app.HudApplication.getAppContext(), manager, "Debug");
                return;
            case 3:
                com.navdy.hud.app.device.light.HUDLightUtils.showGestureNotRecognized(com.navdy.hud.app.HudApplication.getAppContext(), manager);
                return;
            case 4:
                com.navdy.hud.app.device.light.HUDLightUtils.showGestureDetected(com.navdy.hud.app.HudApplication.getAppContext(), manager);
                return;
            case 5:
                com.navdy.hud.app.device.light.HUDLightUtils.showUSBPowerOn(com.navdy.hud.app.HudApplication.getAppContext(), manager);
                return;
            case 6:
                com.navdy.hud.app.device.light.HUDLightUtils.showUSBPowerShutDown(com.navdy.hud.app.HudApplication.getAppContext(), manager);
                return;
            case 7:
                com.navdy.hud.app.device.light.HUDLightUtils.showUSBTransfer(com.navdy.hud.app.HudApplication.getAppContext(), manager);
                return;
            case 8:
                com.navdy.hud.app.device.light.HUDLightUtils.showError(com.navdy.hud.app.HudApplication.getAppContext(), manager);
                return;
            default:
                return;
        }
    }

    private void testTTS(android.content.Intent intent) {
        com.navdy.hud.app.framework.voice.TTSUtils.sendSpeechRequest(TTS_TEXTS[new java.util.Random().nextInt(TTS_TEXTS.length)], com.navdy.service.library.events.audio.Category.SPEECH_TURN_BY_TURN, null);
    }

    private void testDial(android.content.Intent intent) {
        java.lang.String cmd = intent.getStringExtra("COMMAND");
        if (cmd.equals("PowerButtonDoubleClick")) {
            com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getInputManager().injectKey(com.navdy.hud.app.manager.InputManager.CustomKeyEvent.POWER_BUTTON_DOUBLE_CLICK);
        } else if (cmd.equals("rebond")) {
            this.mBus.post(new com.navdy.service.library.events.dial.DialBondRequest(com.navdy.service.library.events.dial.DialBondRequest.DialAction.DIAL_CLEAR_BOND));
            this.mBus.post(new com.navdy.service.library.events.dial.DialBondRequest(com.navdy.service.library.events.dial.DialBondRequest.DialAction.DIAL_BOND));
        }
    }

    private void testObdConfig(android.content.Intent intent) {
        switch (intent.getIntExtra(EXTRA_OBD_CONFIG, 1)) {
            case 0:
                com.navdy.hud.app.obd.ObdManager.getInstance().getObdDeviceConfigurationManager().setConfigurationFile("debug");
                return;
            case 1:
                com.navdy.hud.app.obd.ObdManager.getInstance().getObdDeviceConfigurationManager().setConfigurationFile(com.navdy.hud.app.obd.ObdDeviceConfigurationManager.DEFAULT_ON_CONFIGURATION);
                return;
            default:
                return;
        }
    }

    private void testDestinationSuggestion(android.content.Intent intent) {
        com.navdy.service.library.events.places.SuggestedDestination suggestedDestination = null;
        switch (intent.getIntExtra(EXTRA_DESTINATION_SUGGESTION_TYPE, 0)) {
            case 0:
                suggestedDestination = new com.navdy.service.library.events.places.SuggestedDestination(new com.navdy.service.library.events.destination.Destination.Builder().navigation_position(new com.navdy.service.library.events.location.LatLong(java.lang.Double.valueOf(37.7786444d), java.lang.Double.valueOf(-122.4462313d))).display_position(new com.navdy.service.library.events.location.LatLong(java.lang.Double.valueOf(37.7786444d), java.lang.Double.valueOf(-122.4462313d))).full_address("201 8th St, San Francisco, CA 94103").destination_title("Home").destination_subtitle("").favorite_type(com.navdy.service.library.events.destination.Destination.FavoriteType.FAVORITE_HOME).identifier(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE).suggestion_type(com.navdy.service.library.events.destination.Destination.DEFAULT_SUGGESTION_TYPE).is_recommendation(java.lang.Boolean.valueOf(true)).last_navigated_to(java.lang.Long.valueOf(0)).build(), java.lang.Integer.valueOf(600), com.navdy.service.library.events.places.SuggestedDestination.SuggestionType.SUGGESTION_RECOMMENDATION);
                break;
            case 1:
                suggestedDestination = new com.navdy.service.library.events.places.SuggestedDestination(new com.navdy.service.library.events.destination.Destination.Builder().navigation_position(new com.navdy.service.library.events.location.LatLong(java.lang.Double.valueOf(37.7735078d), java.lang.Double.valueOf(-122.4055828d))).display_position(new com.navdy.service.library.events.location.LatLong(java.lang.Double.valueOf(37.7735078d), java.lang.Double.valueOf(-122.4055828d))).full_address("575 7th St, San Francisco, CA 94103").destination_title("Work").destination_subtitle("").favorite_type(com.navdy.service.library.events.destination.Destination.FavoriteType.FAVORITE_WORK).identifier(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_1).suggestion_type(com.navdy.service.library.events.destination.Destination.DEFAULT_SUGGESTION_TYPE).is_recommendation(java.lang.Boolean.valueOf(true)).last_navigated_to(java.lang.Long.valueOf(0)).build(), java.lang.Integer.valueOf(3600), com.navdy.service.library.events.places.SuggestedDestination.SuggestionType.SUGGESTION_RECOMMENDATION);
                break;
            case 2:
                suggestedDestination = new com.navdy.service.library.events.places.SuggestedDestination(new com.navdy.service.library.events.destination.Destination.Builder().navigation_position(new com.navdy.service.library.events.location.LatLong(java.lang.Double.valueOf(37.7879373d), java.lang.Double.valueOf(-122.4096868d))).display_position(new com.navdy.service.library.events.location.LatLong(java.lang.Double.valueOf(37.7879373d), java.lang.Double.valueOf(-122.4096868d))).full_address("Union Square, San Francisco, CA 94108").destination_title("Union Square").destination_subtitle("").favorite_type(com.navdy.service.library.events.destination.Destination.FavoriteType.FAVORITE_CUSTOM).identifier(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_1_STYLE).suggestion_type(com.navdy.service.library.events.destination.Destination.DEFAULT_SUGGESTION_TYPE).is_recommendation(java.lang.Boolean.valueOf(true)).last_navigated_to(java.lang.Long.valueOf(0)).build(), java.lang.Integer.valueOf(com.glympse.android.lib.HttpJob.RESPONSE_CODE_MAX_RETRY_LOWER_BOUND), com.navdy.service.library.events.places.SuggestedDestination.SuggestionType.SUGGESTION_RECOMMENDATION);
                break;
            case 3:
                suggestedDestination = new com.navdy.service.library.events.places.SuggestedDestination(new com.navdy.service.library.events.destination.Destination.Builder().navigation_position(new com.navdy.service.library.events.location.LatLong(java.lang.Double.valueOf(37.7766916d), java.lang.Double.valueOf(-122.3970457d))).display_position(new com.navdy.service.library.events.location.LatLong(java.lang.Double.valueOf(37.7766916d), java.lang.Double.valueOf(-122.3970457d))).full_address("700 4th St, San Francisco, CA 94107").destination_title("").destination_subtitle("").favorite_type(com.navdy.service.library.events.destination.Destination.FavoriteType.FAVORITE_NONE).identifier(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_2).suggestion_type(com.navdy.service.library.events.destination.Destination.DEFAULT_SUGGESTION_TYPE).is_recommendation(java.lang.Boolean.valueOf(true)).last_navigated_to(java.lang.Long.valueOf(0)).build(), java.lang.Integer.valueOf(-1), com.navdy.service.library.events.places.SuggestedDestination.SuggestionType.SUGGESTION_RECOMMENDATION);
                break;
        }
        this.mBus.post(suggestedDestination);
    }

    private void testSwitchBandwidthLevel(android.content.Intent intent) {
        int bandwidthLevel = intent.getIntExtra(EXTRA_BANDWIDTH_LEVEL, 1);
        android.content.Intent boradcast = new android.content.Intent();
        boradcast.setAction(com.navdy.service.library.device.connection.ConnectionService.ACTION_LINK_BANDWIDTH_LEVEL_CHANGED);
        boradcast.addCategory(com.navdy.service.library.device.connection.ConnectionService.CATEGORY_NAVDY_LINK);
        boradcast.putExtra(com.navdy.service.library.device.connection.ConnectionService.EXTRA_BANDWIDTH_LEVEL, bandwidthLevel);
        com.navdy.hud.app.HudApplication.getAppContext().sendBroadcast(boradcast);
    }

    private void testProxyFileUpload(android.content.Intent intent) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.debug.DebugReceiver.Anon3(intent.getIntExtra(EXTRA_SIZE, 100)), 1);
    }

    private void testProxyFileDownload(android.content.Intent intent) {
        java.lang.String address;
        int size = intent.getIntExtra(EXTRA_SIZE, 100);
        java.lang.String defaultAddress = "http://test.mjpmz4phvm.us-west-2.elasticbeanstalk.com/file?size=" + size;
        java.lang.String extra = intent.getStringExtra("URL");
        if (extra != null) {
            address = extra;
        } else {
            address = defaultAddress;
        }
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.debug.DebugReceiver.Anon4(size, address), 1);
    }

    private void triggerCrash() {
        throw new java.lang.IllegalStateException("Oops");
    }

    private void triggerANR() {
        new android.os.Handler(android.os.Looper.getMainLooper()).post(new com.navdy.hud.app.debug.DebugReceiver.Anon5());
    }

    private void triggerBroadcastANR() {
        busywait(12000);
    }

    /* access modifiers changed from: private */
    public void busywait(int ms) {
        long start = android.os.SystemClock.elapsedRealtime();
        for (long now = android.os.SystemClock.elapsedRealtime(); now - start < ((long) ms); now = android.os.SystemClock.elapsedRealtime()) {
        }
    }

    private void getGoogleHomePage() {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.debug.DebugReceiver.Anon6(), 1);
    }

    private void dnsLookup(java.lang.String host) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.debug.DebugReceiver.Anon7(host), 1);
    }

    private void testSendNotificationEvent(android.content.Intent intent) {
        try {
            com.navdy.service.library.events.notification.NotificationEvent.Builder notificationEventBuilder = new com.navdy.service.library.events.notification.NotificationEvent.Builder();
            if (intent != null) {
                notificationEventBuilder.id(java.lang.Integer.valueOf(intent.getIntExtra("id", 1)));
                notificationEventBuilder.category(com.navdy.service.library.events.notification.NotificationCategory.CATEGORY_OTHER);
                notificationEventBuilder.title(intent.getStringExtra("title"));
                notificationEventBuilder.subtitle(intent.getStringExtra(EXTRA_NOTIFICATION_EVENT_SUB_TITLE));
                notificationEventBuilder.message(intent.getStringExtra(EXTRA_NOTIFICATION_EVENT_MESSAGE));
                notificationEventBuilder.appId(intent.getStringExtra(EXTRA_NOTIFICATION_EVENT_APP_ID));
                notificationEventBuilder.appName(intent.getStringExtra(EXTRA_NOTIFICATION_EVENT_APP_NAME));
                this.mBus.post(new com.navdy.hud.app.event.RemoteEvent(notificationEventBuilder.build()));
            }
        } catch (java.lang.Exception e) {
            sLogger.e("Error while sending test Notification event ", e);
        }
    }

    private void dnsLookupTest() {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.debug.DebugReceiver.Anon8(), 1);
    }
}
