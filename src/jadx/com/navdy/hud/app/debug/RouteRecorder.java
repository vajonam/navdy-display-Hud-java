package com.navdy.hud.app.debug;

public class RouteRecorder {
    private static final java.text.SimpleDateFormat DATE_FORMAT = new java.text.SimpleDateFormat("yyyy-MM-dd'_'HH_mm_ss.SSS", java.util.Locale.US);
    public static final java.lang.String SECONDARY_LOCATION_TAG = "[secondary] ";
    private static final com.navdy.hud.app.debug.RouteRecorder sInstance = new com.navdy.hud.app.debug.RouteRecorder();
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.debug.RouteRecorder.class);
    /* access modifiers changed from: private */
    public com.navdy.hud.app.service.HudConnectionService connectionService;
    private final android.content.Context context = com.navdy.hud.app.HudApplication.getAppContext();
    /* access modifiers changed from: private */
    public volatile int currentInjectionIndex = 0;
    /* access modifiers changed from: private */
    public java.util.List<com.navdy.service.library.events.location.Coordinate> currentLocations;
    /* access modifiers changed from: private */
    public java.io.BufferedWriter driveLogWriter;
    /* access modifiers changed from: private */
    public final com.navdy.hud.app.device.gps.GpsManager gpsManager = com.navdy.hud.app.device.gps.GpsManager.getInstance();
    /* access modifiers changed from: private */
    public final android.os.Handler handler = new android.os.Handler(android.os.Looper.getMainLooper());
    /* access modifiers changed from: private */
    public final java.lang.Runnable injectFakeLocationRunnable = new com.navdy.hud.app.debug.RouteRecorder.Anon2();
    /* access modifiers changed from: private */
    public volatile boolean isLooping;
    /* access modifiers changed from: private */
    public volatile boolean isRecording = false;
    boolean lastReportedDST = false;
    private java.util.TimeZone lastReportedTimeZone = null;
    /* access modifiers changed from: private */
    public final android.location.LocationListener locationListener = new com.navdy.hud.app.debug.RouteRecorder.Anon3();
    /* access modifiers changed from: private */
    public final android.location.LocationManager locationManager = ((android.location.LocationManager) this.context.getSystemService("location"));
    /* access modifiers changed from: private */
    public final com.navdy.hud.app.storage.PathManager pathManager = com.navdy.hud.app.storage.PathManager.getInstance();
    private com.navdy.hud.app.debug.DriveRecorder.State playbackState = com.navdy.hud.app.debug.DriveRecorder.State.STOPPED;
    private volatile boolean prepared;
    private volatile long preparedFileLastModifiedTime;
    private volatile java.lang.String preparedFileName;
    private java.lang.String recordingLabel;
    /* access modifiers changed from: private */
    public java.lang.Runnable startLocationUpdates = new com.navdy.hud.app.debug.RouteRecorder.Anon1();

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            if (com.navdy.hud.app.debug.RouteRecorder.this.locationManager.isProviderEnabled(com.navdy.hud.app.device.gps.GpsConstants.NAVDY_GPS_PROVIDER)) {
                com.navdy.hud.app.debug.RouteRecorder.this.locationManager.requestLocationUpdates(com.navdy.hud.app.device.gps.GpsConstants.NAVDY_GPS_PROVIDER, 0, 0.0f, com.navdy.hud.app.debug.RouteRecorder.this.locationListener);
            }
            if (com.navdy.hud.app.debug.RouteRecorder.this.locationManager.isProviderEnabled("network")) {
                com.navdy.hud.app.debug.RouteRecorder.this.locationManager.requestLocationUpdates("network", 0, 0.0f, com.navdy.hud.app.debug.RouteRecorder.this.locationListener);
            }
        }
    }

    class Anon10 implements java.lang.Runnable {
        final /* synthetic */ java.lang.String val$marker;

        Anon10(java.lang.String str) {
            this.val$marker = str;
        }

        public void run() {
            if (com.navdy.hud.app.debug.RouteRecorder.this.isRecording) {
                try {
                    com.navdy.hud.app.debug.RouteRecorder.this.driveLogWriter.write(this.val$marker + com.navdy.hud.app.framework.glance.GlanceConstants.NEWLINE);
                } catch (java.io.IOException e) {
                    com.navdy.hud.app.debug.RouteRecorder.sLogger.e((java.lang.Throwable) e);
                }
            }
        }
    }

    class Anon2 implements java.lang.Runnable {
        Anon2() {
        }

        public void run() {
            if (com.navdy.hud.app.debug.RouteRecorder.this.currentLocations != null && com.navdy.hud.app.debug.RouteRecorder.this.currentLocations.size() > com.navdy.hud.app.debug.RouteRecorder.this.currentInjectionIndex) {
                com.navdy.hud.app.debug.RouteRecorder.this.gpsManager.feedLocation(new com.navdy.service.library.events.location.Coordinate.Builder((com.navdy.service.library.events.location.Coordinate) com.navdy.hud.app.debug.RouteRecorder.this.currentLocations.get(com.navdy.hud.app.debug.RouteRecorder.this.currentInjectionIndex)).timestamp(java.lang.Long.valueOf(java.lang.System.currentTimeMillis())).build());
                if (com.navdy.hud.app.debug.RouteRecorder.this.currentLocations.size() > com.navdy.hud.app.debug.RouteRecorder.this.currentInjectionIndex + 1) {
                    long timeDiff = ((com.navdy.service.library.events.location.Coordinate) com.navdy.hud.app.debug.RouteRecorder.this.currentLocations.get(com.navdy.hud.app.debug.RouteRecorder.this.currentInjectionIndex + 1)).timestamp.longValue() - ((com.navdy.service.library.events.location.Coordinate) com.navdy.hud.app.debug.RouteRecorder.this.currentLocations.get(com.navdy.hud.app.debug.RouteRecorder.this.currentInjectionIndex)).timestamp.longValue();
                    com.navdy.hud.app.debug.RouteRecorder.this.currentInjectionIndex = com.navdy.hud.app.debug.RouteRecorder.this.currentInjectionIndex + 1;
                    com.navdy.hud.app.debug.RouteRecorder.this.handler.postDelayed(this, timeDiff);
                } else if (com.navdy.hud.app.debug.RouteRecorder.this.isLooping) {
                    com.navdy.hud.app.debug.RouteRecorder.this.currentInjectionIndex = 0;
                    com.navdy.hud.app.debug.RouteRecorder.this.handler.post(com.navdy.hud.app.debug.RouteRecorder.this.injectFakeLocationRunnable);
                } else {
                    com.navdy.hud.app.debug.RouteRecorder.this.stopPlayback();
                }
            } else if (!com.navdy.hud.app.debug.RouteRecorder.this.isLooping || com.navdy.hud.app.debug.RouteRecorder.this.currentLocations == null) {
                com.navdy.hud.app.debug.RouteRecorder.this.stopPlayback();
            } else {
                com.navdy.hud.app.debug.RouteRecorder.this.currentInjectionIndex = 0;
                com.navdy.hud.app.debug.RouteRecorder.this.handler.post(com.navdy.hud.app.debug.RouteRecorder.this.injectFakeLocationRunnable);
            }
        }
    }

    class Anon3 implements android.location.LocationListener {
        Anon3() {
        }

        public void onLocationChanged(android.location.Location location) {
            com.navdy.hud.app.debug.RouteRecorder.this.persistLocationToFile(location, false);
        }

        public void onStatusChanged(java.lang.String provider, int status, android.os.Bundle extras) {
        }

        public void onProviderEnabled(java.lang.String provider) {
        }

        public void onProviderDisabled(java.lang.String provider) {
        }
    }

    class Anon4 implements java.lang.Runnable {
        final /* synthetic */ boolean val$local;

        class Anon1 implements java.lang.Runnable {
            Anon1() {
            }

            public void run() {
                com.navdy.hud.app.debug.RouteRecorder.this.bStopRecording(com.navdy.hud.app.debug.RouteRecorder.Anon4.this.val$local);
            }
        }

        Anon4(boolean z) {
            this.val$local = z;
        }

        public void run() {
            com.navdy.hud.app.debug.RouteRecorder.this.locationManager.removeUpdates(com.navdy.hud.app.debug.RouteRecorder.this.locationListener);
            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.debug.RouteRecorder.Anon4.Anon1(), 9);
        }
    }

    class Anon5 implements java.lang.Runnable {
        Anon5() {
        }

        public void run() {
            java.util.List<java.lang.String> fileNames = new java.util.ArrayList<>();
            java.io.File[] subFiles = new java.io.File(com.navdy.hud.app.debug.RouteRecorder.this.pathManager.getMapsPartitionPath() + java.io.File.separator + "drive_logs").listFiles();
            if (subFiles != null) {
                for (java.io.File file : subFiles) {
                    if (!file.getName().endsWith(".obd")) {
                        fileNames.add(file.getName());
                    }
                }
            }
            com.navdy.hud.app.debug.RouteRecorder.this.connectionService.sendMessage(new com.navdy.service.library.events.debug.DriveRecordingsResponse(fileNames));
        }
    }

    class Anon6 implements java.lang.Runnable {
        final /* synthetic */ java.lang.String val$fileName;
        final /* synthetic */ boolean val$secondary;

        Anon6(java.lang.String str, boolean z) {
            this.val$fileName = str;
            this.val$secondary = z;
        }

        public void run() {
            if (!com.navdy.hud.app.debug.RouteRecorder.this.bPrepare(this.val$fileName, this.val$secondary)) {
                com.navdy.hud.app.debug.RouteRecorder.sLogger.e("Prepare failed ");
            } else {
                com.navdy.hud.app.debug.RouteRecorder.sLogger.e("Prepare succeeded ");
            }
        }
    }

    class Anon7 implements java.lang.Runnable {
        final /* synthetic */ java.lang.String val$fileName;
        final /* synthetic */ boolean val$local;
        final /* synthetic */ boolean val$secondary;

        Anon7(java.lang.String str, boolean z, boolean z2) {
            this.val$fileName = str;
            this.val$secondary = z;
            this.val$local = z2;
        }

        public void run() {
            try {
                if (!com.navdy.hud.app.debug.RouteRecorder.this.bPrepare(this.val$fileName, this.val$secondary)) {
                    if (!this.val$local) {
                        com.navdy.hud.app.debug.RouteRecorder.this.connectionService.sendMessage(new com.navdy.service.library.events.debug.StartDrivePlaybackResponse(com.navdy.service.library.events.RequestStatus.REQUEST_SERVICE_ERROR));
                    }
                    com.navdy.hud.app.debug.RouteRecorder.this.stopPlayback();
                    return;
                }
                com.navdy.hud.app.debug.RouteRecorder.sLogger.d("Prepare succeeded");
                if (!this.val$local) {
                    com.navdy.hud.app.debug.RouteRecorder.this.connectionService.sendMessage(new com.navdy.service.library.events.debug.StartDrivePlaybackResponse(com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS));
                }
                com.navdy.hud.app.debug.RouteRecorder.sLogger.d("Starting playback");
                com.navdy.hud.app.debug.RouteRecorder.this.currentInjectionIndex = 0;
                com.navdy.hud.app.debug.RouteRecorder.this.handler.post(com.navdy.hud.app.debug.RouteRecorder.this.injectFakeLocationRunnable);
            } catch (Throwable t) {
                if (!this.val$local) {
                    com.navdy.hud.app.debug.RouteRecorder.this.connectionService.sendMessage(new com.navdy.service.library.events.debug.StartDrivePlaybackResponse(com.navdy.service.library.events.RequestStatus.REQUEST_SERVICE_ERROR));
                }
                com.navdy.hud.app.debug.RouteRecorder.this.stopPlayback();
                com.navdy.hud.app.debug.RouteRecorder.sLogger.e(t);
            }
        }
    }

    class Anon8 implements java.lang.Runnable {
        final /* synthetic */ java.lang.String val$fileName;
        final /* synthetic */ boolean val$local;

        Anon8(java.lang.String str, boolean z) {
            this.val$fileName = str;
            this.val$local = z;
        }

        public void run() {
            java.io.File driveLogsDir = com.navdy.hud.app.debug.DriveRecorder.getDriveLogsDir(this.val$fileName);
            if (!driveLogsDir.exists()) {
                driveLogsDir.mkdirs();
            }
            java.io.File driveLogFile = new java.io.File(driveLogsDir, this.val$fileName);
            try {
                if (driveLogFile.createNewFile()) {
                    com.navdy.hud.app.debug.RouteRecorder.this.driveLogWriter = new java.io.BufferedWriter(new java.io.OutputStreamWriter(new java.io.FileOutputStream(driveLogFile), "utf-8"));
                    com.navdy.hud.app.debug.RouteRecorder.sLogger.v("writing drive log: " + this.val$fileName);
                    com.navdy.hud.app.debug.RouteRecorder.this.handler.post(com.navdy.hud.app.debug.RouteRecorder.this.startLocationUpdates);
                    if (!this.val$local) {
                        com.navdy.hud.app.debug.RouteRecorder.this.connectionService.sendMessage(new com.navdy.service.library.events.debug.StartDriveRecordingResponse(com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS));
                    }
                }
            } catch (java.io.IOException e) {
                com.navdy.hud.app.debug.RouteRecorder.sLogger.e((java.lang.Throwable) e);
            }
        }
    }

    class Anon9 implements java.lang.Runnable {
        final /* synthetic */ java.lang.String val$locationString;

        Anon9(java.lang.String str) {
            this.val$locationString = str;
        }

        public void run() {
            if (com.navdy.hud.app.debug.RouteRecorder.this.isRecording) {
                try {
                    com.navdy.hud.app.debug.RouteRecorder.this.driveLogWriter.write(this.val$locationString + com.navdy.hud.app.framework.glance.GlanceConstants.NEWLINE);
                    com.navdy.hud.app.debug.RouteRecorder.this.driveLogWriter.flush();
                } catch (java.io.IOException e) {
                    com.navdy.hud.app.debug.RouteRecorder.sLogger.e((java.lang.Throwable) e);
                }
            }
        }
    }

    public static com.navdy.hud.app.debug.RouteRecorder getInstance() {
        return sInstance;
    }

    private RouteRecorder() {
    }

    public java.lang.String startRecording(java.lang.String label, boolean local) {
        if (this.isRecording || isPlaying()) {
            sLogger.v("already busy");
            if (!local) {
                this.connectionService.sendMessage(new com.navdy.service.library.events.debug.StartDriveRecordingResponse(com.navdy.service.library.events.RequestStatus.REQUEST_INVALID_STATE));
            }
            return null;
        }
        sLogger.d("Starting the drive recording");
        this.isRecording = true;
        this.recordingLabel = label;
        java.lang.String fileName = com.navdy.hud.app.util.GenericUtil.normalizeToFilename(label) + com.navdy.service.library.util.MusicDataUtils.ALTERNATE_SEPARATOR + DATE_FORMAT.format(new java.util.Date()) + ".log";
        com.navdy.service.library.task.TaskManager.getInstance().execute(createDriveRecord(fileName, local), 9);
        return fileName;
    }

    public void stopRecording(boolean local) {
        if (!this.isRecording) {
            sLogger.v("already stopped, no-op");
            return;
        }
        sLogger.d("Stopping the drive recording");
        this.handler.post(new com.navdy.hud.app.debug.RouteRecorder.Anon4(local));
    }

    /* access modifiers changed from: private */
    public void bStopRecording(boolean local) {
        com.navdy.service.library.util.IOUtils.closeStream(this.driveLogWriter);
        this.isRecording = false;
        this.recordingLabel = null;
        if (!local) {
            this.connectionService.sendMessage(new com.navdy.service.library.events.debug.StopDriveRecordingResponse(com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS));
        }
        sLogger.v("recording stopped");
    }

    public void requestRecordings() {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.debug.RouteRecorder.Anon5(), 1);
    }

    public void startPlayback(java.lang.String fileName, boolean secondary, boolean local) {
        startPlayback(fileName, secondary, false, local);
    }

    public void prepare(java.lang.String fileName, boolean secondary) {
        if (isPlaying() || isPaused()) {
            sLogger.e("Playback is not stopped , current state " + this.playbackState.name());
        } else {
            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.debug.RouteRecorder.Anon6(fileName, secondary), 9);
        }
    }

    public synchronized boolean bPrepare(java.lang.String fileName, boolean secondary) {
        boolean z;
        java.io.File driveLogFile = new java.io.File(com.navdy.hud.app.debug.DriveRecorder.getDriveLogsDir(fileName), fileName);
        long modifiedTime = driveLogFile.lastModified();
        if (!android.text.TextUtils.equals(fileName, this.preparedFileName) || this.preparedFileLastModifiedTime != modifiedTime) {
            sLogger.d("Preparing for playback from file : " + fileName);
            this.preparedFileName = fileName;
            this.preparedFileLastModifiedTime = modifiedTime;
            java.io.BufferedReader reader = null;
            try {
                java.io.BufferedReader reader2 = new java.io.BufferedReader(new java.io.InputStreamReader(new java.io.FileInputStream(driveLogFile), "utf-8"));
                try {
                    this.currentLocations = new java.util.ArrayList();
                    while (true) {
                        java.lang.String line = reader2.readLine();
                        if (line == null) {
                            break;
                        } else if (!line.startsWith("#")) {
                            int index = line.indexOf(SECONDARY_LOCATION_TAG);
                            boolean hasSecondaryTag = index >= 0;
                            if (hasSecondaryTag) {
                                line = line.substring(SECONDARY_LOCATION_TAG.length() + index);
                            }
                            if (!secondary) {
                                if (hasSecondaryTag) {
                                }
                            } else if (!hasSecondaryTag) {
                            }
                            java.lang.String[] lineSplit = line.split(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA);
                            if (lineSplit.length == 8) {
                                this.currentLocations.add(new com.navdy.service.library.events.location.Coordinate.Builder().latitude(java.lang.Double.valueOf(java.lang.Double.parseDouble(lineSplit[0]))).longitude(java.lang.Double.valueOf(java.lang.Double.parseDouble(lineSplit[1]))).bearing(java.lang.Float.valueOf(java.lang.Float.parseFloat(lineSplit[2]))).speed(java.lang.Float.valueOf(java.lang.Float.parseFloat(lineSplit[3]))).accuracy(java.lang.Float.valueOf(java.lang.Float.parseFloat(lineSplit[4]))).altitude(java.lang.Double.valueOf(java.lang.Double.parseDouble(lineSplit[5]))).timestamp(java.lang.Long.valueOf(java.lang.Long.parseLong(lineSplit[6]))).build());
                            } else if (lineSplit.length == 6) {
                                this.currentLocations.add(new com.navdy.service.library.events.location.Coordinate.Builder().latitude(java.lang.Double.valueOf(java.lang.Double.parseDouble(lineSplit[0]))).longitude(java.lang.Double.valueOf(java.lang.Double.parseDouble(lineSplit[1]))).bearing(java.lang.Float.valueOf(java.lang.Float.parseFloat(lineSplit[2]))).speed(java.lang.Float.valueOf(java.lang.Float.parseFloat(lineSplit[3]))).accuracy(java.lang.Float.valueOf(1.0f)).altitude(java.lang.Double.valueOf(0.0d)).timestamp(java.lang.Long.valueOf(java.lang.Long.parseLong(lineSplit[4]))).build());
                            }
                        }
                    }
                    z = true;
                    com.navdy.service.library.util.IOUtils.closeStream(reader2);
                } catch (java.lang.Exception e) {
                    e = e;
                    reader = reader2;
                    try {
                        sLogger.e("Error parsing the file, prepare failed", e);
                        z = false;
                        com.navdy.service.library.util.IOUtils.closeStream(reader);
                        return z;
                    } catch (Throwable th) {
                        th = th;
                        com.navdy.service.library.util.IOUtils.closeStream(reader);
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    reader = reader2;
                    com.navdy.service.library.util.IOUtils.closeStream(reader);
                    throw th;
                }
            } catch (java.lang.Exception e2) {
                e = e2;
                sLogger.e("Error parsing the file, prepare failed", e);
                z = false;
                com.navdy.service.library.util.IOUtils.closeStream(reader);
                return z;
            }
        } else {
            sLogger.d("Already prepared, ready for playback");
            z = true;
        }
        return z;
    }

    public void startPlayback(java.lang.String fileName, boolean secondary, boolean loop, boolean local) {
        if (isPlaying() || this.isRecording) {
            sLogger.v("already busy, no-op");
            if (!local) {
                this.connectionService.sendMessage(new com.navdy.service.library.events.debug.StartDrivePlaybackResponse(com.navdy.service.library.events.RequestStatus.REQUEST_INVALID_STATE));
                return;
            }
            return;
        }
        this.playbackState = com.navdy.hud.app.debug.DriveRecorder.State.PLAYING;
        this.isLooping = loop;
        this.gpsManager.stopUbloxReporting();
        this.connectionService.setSimulatingGpsCoordinates(true);
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.debug.RouteRecorder.Anon7(fileName, secondary, local), 1);
    }

    public void pausePlayback() {
        if (!isPlaying()) {
            sLogger.v("already stopped, no-op");
            return;
        }
        this.handler.removeCallbacks(this.injectFakeLocationRunnable);
        this.playbackState = com.navdy.hud.app.debug.DriveRecorder.State.PAUSED;
        this.gpsManager.startUbloxReporting();
        this.connectionService.setSimulatingGpsCoordinates(false);
    }

    public void resumePlayback() {
        if (isPaused()) {
            this.gpsManager.stopUbloxReporting();
            this.playbackState = com.navdy.hud.app.debug.DriveRecorder.State.PLAYING;
            this.connectionService.setSimulatingGpsCoordinates(true);
            this.handler.post(this.injectFakeLocationRunnable);
        }
    }

    public void stopPlayback() {
        if (isStopped()) {
            sLogger.v("already stopped, no-op");
            return;
        }
        this.handler.removeCallbacks(this.injectFakeLocationRunnable);
        this.currentInjectionIndex = 0;
        this.playbackState = com.navdy.hud.app.debug.DriveRecorder.State.STOPPED;
        this.gpsManager.startUbloxReporting();
        this.isLooping = false;
        this.connectionService.setSimulatingGpsCoordinates(false);
    }

    public synchronized void release() {
        if (this.currentLocations != null) {
            this.currentLocations.clear();
            this.currentLocations = null;
        }
        this.preparedFileName = null;
        this.preparedFileLastModifiedTime = -1;
    }

    public boolean restartPlayback() {
        if (!isPlaying() && !isPaused()) {
            return false;
        }
        if (isPaused()) {
            this.gpsManager.stopUbloxReporting();
            this.connectionService.setSimulatingGpsCoordinates(false);
        }
        this.playbackState = com.navdy.hud.app.debug.DriveRecorder.State.PLAYING;
        this.currentInjectionIndex = 0;
        this.handler.removeCallbacks(this.injectFakeLocationRunnable);
        this.handler.post(this.injectFakeLocationRunnable);
        return true;
    }

    public void setConnectionService(com.navdy.hud.app.service.HudConnectionService connectionService2) {
        this.connectionService = connectionService2;
    }

    private java.lang.Runnable createDriveRecord(java.lang.String fileName, boolean local) {
        return new com.navdy.hud.app.debug.RouteRecorder.Anon8(fileName, local);
    }

    private java.lang.Runnable writeLocation(java.lang.String locationString) {
        return new com.navdy.hud.app.debug.RouteRecorder.Anon9(locationString);
    }

    private java.lang.Runnable writeMarker(java.lang.String marker) {
        return new com.navdy.hud.app.debug.RouteRecorder.Anon10(marker);
    }

    /* access modifiers changed from: private */
    public void persistLocationToFile(android.location.Location location, boolean secondary) {
        if (this.isRecording) {
            java.lang.String provider = location.getProvider();
            if (com.navdy.hud.app.device.gps.GpsConstants.NAVDY_GPS_PROVIDER.equals(provider)) {
                provider = "gps";
            }
            java.lang.String zoneString = "";
            java.util.TimeZone tz = java.util.TimeZone.getDefault();
            boolean inDST = tz.inDaylightTime(new java.util.Date());
            if (!tz.equals(this.lastReportedTimeZone) || inDST != this.lastReportedDST) {
                java.lang.String str = "#zone=\"%s\", dst=%s\n";
                java.lang.Object[] objArr = new java.lang.Object[2];
                objArr[0] = tz.getID();
                objArr[1] = inDST ? "T" : "F";
                zoneString = java.lang.String.format(str, objArr);
                this.lastReportedTimeZone = tz;
                this.lastReportedDST = inDST;
                sLogger.v(java.lang.String.format("setting zoneinfo:  %s dst=%b", new java.lang.Object[]{tz.getID(), java.lang.Boolean.valueOf(inDST)}));
            }
            com.navdy.service.library.task.TaskManager.getInstance().execute(writeLocation(zoneString + ((secondary ? SECONDARY_LOCATION_TAG : "") + location.getLatitude() + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA + location.getLongitude() + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA + location.getBearing() + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA + location.getSpeed() + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA + location.getAccuracy() + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA + location.getAltitude() + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA + location.getTime() + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA + provider)), 9);
        }
    }

    public void injectLocation(android.location.Location location, boolean secondary) {
        persistLocationToFile(location, secondary);
    }

    public void injectMarker(java.lang.String marker) {
        if (this.isRecording) {
            com.navdy.service.library.task.TaskManager.getInstance().execute(writeMarker(marker), 9);
        }
    }

    public boolean isRecording() {
        return this.isRecording;
    }

    public java.lang.String getLabel() {
        return this.recordingLabel;
    }

    public boolean isPlaying() {
        return this.playbackState == com.navdy.hud.app.debug.DriveRecorder.State.PLAYING;
    }

    public boolean isPaused() {
        return this.playbackState == com.navdy.hud.app.debug.DriveRecorder.State.PAUSED;
    }

    public boolean isStopped() {
        return this.playbackState == com.navdy.hud.app.debug.DriveRecorder.State.STOPPED;
    }
}
