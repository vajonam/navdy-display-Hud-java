package com.navdy.hud.app.debug;

public class GestureEngine$$ViewInjector {

    /* compiled from: GestureEngine$$ViewInjector */
    static class Anon1 implements android.widget.CompoundButton.OnCheckedChangeListener {
        final /* synthetic */ com.navdy.hud.app.debug.GestureEngine val$target;

        Anon1(com.navdy.hud.app.debug.GestureEngine gestureEngine) {
            this.val$target = gestureEngine;
        }

        public void onCheckedChanged(android.widget.CompoundButton p0, boolean p1) {
            this.val$target.onToggleGesture(p1);
        }
    }

    /* compiled from: GestureEngine$$ViewInjector */
    static class Anon2 implements android.widget.CompoundButton.OnCheckedChangeListener {
        final /* synthetic */ com.navdy.hud.app.debug.GestureEngine val$target;

        Anon2(com.navdy.hud.app.debug.GestureEngine gestureEngine) {
            this.val$target = gestureEngine;
        }

        public void onCheckedChanged(android.widget.CompoundButton p0, boolean p1) {
            this.val$target.onToggleDiscreteMode(p1);
        }
    }

    /* compiled from: GestureEngine$$ViewInjector */
    static class Anon3 implements android.widget.CompoundButton.OnCheckedChangeListener {
        final /* synthetic */ com.navdy.hud.app.debug.GestureEngine val$target;

        Anon3(com.navdy.hud.app.debug.GestureEngine gestureEngine) {
            this.val$target = gestureEngine;
        }

        public void onCheckedChanged(android.widget.CompoundButton p0, boolean p1) {
            this.val$target.onTogglePreview(p1);
        }
    }

    public static void inject(butterknife.ButterKnife.Finder finder, com.navdy.hud.app.debug.GestureEngine target, java.lang.Object source) {
        ((android.widget.CompoundButton) finder.findRequiredView(source, com.navdy.hud.app.R.id.enable_gesture, "method 'onToggleGesture'")).setOnCheckedChangeListener(new com.navdy.hud.app.debug.GestureEngine$$ViewInjector.Anon1(target));
        ((android.widget.CompoundButton) finder.findRequiredView(source, com.navdy.hud.app.R.id.enable_discrete_mode, "method 'onToggleDiscreteMode'")).setOnCheckedChangeListener(new com.navdy.hud.app.debug.GestureEngine$$ViewInjector.Anon2(target));
        ((android.widget.CompoundButton) finder.findRequiredView(source, com.navdy.hud.app.R.id.enable_preview, "method 'onTogglePreview'")).setOnCheckedChangeListener(new com.navdy.hud.app.debug.GestureEngine$$ViewInjector.Anon3(target));
    }

    public static void reset(com.navdy.hud.app.debug.GestureEngine target) {
    }
}
