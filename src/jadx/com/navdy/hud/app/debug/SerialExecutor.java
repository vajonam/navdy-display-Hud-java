package com.navdy.hud.app.debug;

public interface SerialExecutor {
    void execute(java.lang.Runnable runnable);
}
