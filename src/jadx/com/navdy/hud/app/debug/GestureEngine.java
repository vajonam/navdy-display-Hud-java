package com.navdy.hud.app.debug;

public class GestureEngine extends android.widget.LinearLayout {
    @javax.inject.Inject
    com.navdy.hud.app.gesture.GestureServiceConnector gestureServiceConnector;

    class Anon1 implements java.lang.Runnable {
        final /* synthetic */ boolean val$checked;

        Anon1(boolean z) {
            this.val$checked = z;
        }

        public void run() {
            if (this.val$checked) {
                com.navdy.hud.app.debug.GestureEngine.this.gestureServiceConnector.start();
            } else {
                com.navdy.hud.app.debug.GestureEngine.this.gestureServiceConnector.stop();
            }
        }
    }

    public GestureEngine(android.content.Context context) {
        this(context, null);
    }

    public GestureEngine(android.content.Context context, android.util.AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) {
            mortar.Mortar.inject(context, this);
        }
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
        butterknife.ButterKnife.inject((android.view.View) this);
        ((android.widget.CheckBox) findViewById(com.navdy.hud.app.R.id.enable_gesture)).setChecked(this.gestureServiceConnector.isRunning());
    }

    /* access modifiers changed from: 0000 */
    @butterknife.OnCheckedChanged({2131624156})
    public void onToggleGesture(boolean checked) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.debug.GestureEngine.Anon1(checked), 1);
    }

    @butterknife.OnCheckedChanged({2131624158})
    public void onToggleDiscreteMode(boolean checked) {
        this.gestureServiceConnector.setDiscreteMode(checked);
    }

    @butterknife.OnCheckedChanged({2131624157})
    public void onTogglePreview(boolean checked) {
        this.gestureServiceConnector.enablePreview(checked);
    }
}
