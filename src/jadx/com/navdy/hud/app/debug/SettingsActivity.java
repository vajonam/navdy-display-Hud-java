package com.navdy.hud.app.debug;

public class SettingsActivity extends android.preference.PreferenceActivity {
    public static final java.lang.String RESTART_ON_CRASH_KEY = "restart_on_crash_preference";
    public static final java.lang.String START_ON_BOOT_KEY = "start_on_boot_preference";
    public static final java.lang.String START_VIDEO_ON_BOOT_KEY = "start_video_on_boot_preference";

    public static class DeveloperPreferencesFragment extends android.preference.PreferenceFragment {
        public void onCreate(android.os.Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(com.navdy.hud.app.R.xml.developer_preferences);
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void onBuildHeaders(java.util.List<android.preference.PreferenceActivity.Header> target) {
        loadHeadersFromResource(com.navdy.hud.app.R.xml.preference_headers, target);
    }
}
