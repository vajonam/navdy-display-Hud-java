package com.navdy.hud.app.debug;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\b\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\b\u0002"}, d2 = {"<anonymous>", "", "run"}, k = 3, mv = {1, 1, 6})
/* compiled from: AsyncBufferedFileWriter.kt */
final class AsyncBufferedFileWriter$write$Anon1 implements java.lang.Runnable {
    final /* synthetic */ java.lang.String $data;
    final /* synthetic */ boolean $forceToDisk;
    final /* synthetic */ com.navdy.hud.app.debug.AsyncBufferedFileWriter this$Anon0;

    AsyncBufferedFileWriter$write$Anon1(com.navdy.hud.app.debug.AsyncBufferedFileWriter asyncBufferedFileWriter, java.lang.String str, boolean z) {
        this.this$Anon0 = asyncBufferedFileWriter;
        this.$data = str;
        this.$forceToDisk = z;
    }

    public final void run() {
        this.this$Anon0.getBufferedWriter().write(this.$data);
        if (this.$forceToDisk) {
            this.this$Anon0.getBufferedWriter().flush();
        }
    }
}
