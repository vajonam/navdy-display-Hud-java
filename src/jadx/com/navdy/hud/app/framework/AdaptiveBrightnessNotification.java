package com.navdy.hud.app.framework;

public class AdaptiveBrightnessNotification extends android.database.ContentObserver implements com.navdy.hud.app.framework.notifications.INotification, android.content.SharedPreferences.OnSharedPreferenceChangeListener {
    private static final int DEFAULT_BRIGHTNESS_INT = java.lang.Integer.parseInt("128");
    private static final long DETENT_TIMEOUT = 250;
    private static final int INITIAL_CHANGING_STEP = 1;
    private static final int MAX_BRIGHTNESS = 255;
    private static final int MAX_STEP = 16;
    private static final int NOTIFICATION_TIMEOUT = 10000;
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.framework.AdaptiveBrightnessNotification.class);
    private final boolean AUTO_BRIGHTNESS_ADJ_ENABLED = true;
    private int changingStep = 1;
    private com.navdy.hud.app.ui.component.ChoiceLayout2 choiceLayout;
    private android.view.ViewGroup container;
    private com.navdy.hud.app.framework.notifications.INotificationController controller;
    /* access modifiers changed from: private */
    public int currentValue;
    private boolean isAutoBrightnessOn = getCurrentIsAutoBrightnessOn();
    private boolean isLastChangeIncreasing = false;
    private long lastChangeActionTime = -251;
    private com.navdy.hud.app.view.Gauge progressBar;
    private android.content.SharedPreferences sharedPreferences = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getSharedPreferences();
    /* access modifiers changed from: private */
    public android.content.SharedPreferences.Editor sharedPreferencesEditor = this.sharedPreferences.edit();
    private android.widget.TextView textIndicator;

    class Anon1 implements com.navdy.hud.app.ui.component.ChoiceLayout2.IListener {
        Anon1() {
        }

        public void executeItem(com.navdy.hud.app.ui.component.ChoiceLayout2.Selection selection) {
            com.navdy.hud.app.framework.AdaptiveBrightnessNotification.this.sharedPreferencesEditor.putString(com.navdy.hud.app.settings.HUDSettings.AUTO_BRIGHTNESS_ADJUSTMENT, java.lang.String.valueOf(com.navdy.hud.app.framework.AdaptiveBrightnessNotification.this.currentValue));
            com.navdy.hud.app.framework.AdaptiveBrightnessNotification.this.sharedPreferencesEditor.apply();
            com.navdy.hud.app.framework.AdaptiveBrightnessNotification.this.close();
        }

        public void itemSelected(com.navdy.hud.app.ui.component.ChoiceLayout2.Selection selection) {
        }
    }

    public AdaptiveBrightnessNotification() {
        super(new android.os.Handler());
    }

    public void onChange(boolean selfChange, android.net.Uri uri) {
    }

    public com.navdy.hud.app.framework.notifications.NotificationType getType() {
        return com.navdy.hud.app.framework.notifications.NotificationType.BRIGHTNESS;
    }

    public java.lang.String getId() {
        return com.navdy.hud.app.framework.notifications.NotificationId.BRIGHTNESS_NOTIFICATION_ID;
    }

    public android.view.View getView(android.content.Context context) {
        if (this.container == null) {
            this.container = (android.view.ViewGroup) android.view.LayoutInflater.from(context).inflate(com.navdy.hud.app.R.layout.notification_adaptive_auto_brightness, null);
            this.textIndicator = (android.widget.TextView) this.container.findViewById(com.navdy.hud.app.R.id.subTitle);
            this.progressBar = (com.navdy.hud.app.view.Gauge) this.container.findViewById(com.navdy.hud.app.R.id.circle_progress);
            this.progressBar.setAnimated(false);
            showFixedChoices(this.container);
        }
        return this.container;
    }

    public android.view.View getExpandedView(android.content.Context context, java.lang.Object data) {
        return null;
    }

    public int getExpandedViewIndicatorColor() {
        return 0;
    }

    private void showFixedChoices(android.view.ViewGroup container2) {
        this.choiceLayout = (com.navdy.hud.app.ui.component.ChoiceLayout2) container2.findViewById(com.navdy.hud.app.R.id.choiceLayout);
        java.util.List<com.navdy.hud.app.ui.component.ChoiceLayout2.Choice> choices = new java.util.ArrayList<>(3);
        android.content.res.Resources resources = container2.getResources();
        java.lang.String ok = resources.getString(com.navdy.hud.app.R.string.glances_ok);
        int okColor = resources.getColor(com.navdy.hud.app.R.color.glance_ok_blue);
        choices.add(new com.navdy.hud.app.ui.component.ChoiceLayout2.Choice(2, com.navdy.hud.app.R.drawable.icon_glances_ok, okColor, com.navdy.hud.app.R.drawable.icon_glances_ok, -16777216, ok, okColor));
        this.choiceLayout.setChoices(choices, 1, new com.navdy.hud.app.framework.AdaptiveBrightnessNotification.Anon1());
        this.choiceLayout.setVisibility(0);
    }

    public void onStart(com.navdy.hud.app.framework.notifications.INotificationController controller2) {
        this.currentValue = getCurrentBrightness();
        this.sharedPreferencesEditor.putString(com.navdy.hud.app.settings.HUDSettings.AUTO_BRIGHTNESS, "false");
        this.sharedPreferencesEditor.apply();
        this.sharedPreferencesEditor.putString(com.navdy.hud.app.settings.HUDSettings.BRIGHTNESS, java.lang.String.valueOf(this.currentValue));
        this.sharedPreferencesEditor.apply();
        this.isAutoBrightnessOn = false;
        this.controller = controller2;
        this.sharedPreferences.registerOnSharedPreferenceChangeListener(this);
        updateState();
    }

    public void updateState() {
        if (this.controller == null) {
            sLogger.v("brightness notif offscreen");
            return;
        }
        this.currentValue = getCurrentBrightness();
        this.progressBar.setValue(this.currentValue);
    }

    public void onUpdate() {
        if (this.controller != null) {
            this.controller.resetTimeout();
        }
        updateState();
    }

    public void onStop() {
        this.sharedPreferencesEditor.putString(com.navdy.hud.app.settings.HUDSettings.AUTO_BRIGHTNESS, "true");
        this.sharedPreferencesEditor.apply();
        this.isAutoBrightnessOn = false;
        this.sharedPreferences.unregisterOnSharedPreferenceChangeListener(this);
        this.controller = null;
    }

    public int getTimeout() {
        return 10000;
    }

    public boolean isAlive() {
        return false;
    }

    public boolean isPurgeable() {
        return false;
    }

    public boolean canAddToStackIfCurrentExists() {
        return false;
    }

    public int getColor() {
        return 0;
    }

    public void onNotificationEvent(com.navdy.hud.app.ui.framework.UIStateManager.Mode mode) {
    }

    public void onExpandedNotificationEvent(com.navdy.hud.app.ui.framework.UIStateManager.Mode mode) {
    }

    public void onExpandedNotificationSwitched() {
    }

    public android.animation.AnimatorSet getViewSwitchAnimation(boolean viewIn) {
        return null;
    }

    public boolean expandNotification() {
        return false;
    }

    public boolean supportScroll() {
        return false;
    }

    public static void showNotification() {
        com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().addNotification(new com.navdy.hud.app.framework.AdaptiveBrightnessNotification());
    }

    /* access modifiers changed from: private */
    public void close() {
        com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().removeNotification(getId());
        com.navdy.hud.app.analytics.AnalyticsSupport.recordAdaptiveAutobrightnessAdjustmentChanged(getCurrentBrightness());
    }

    public void onClick() {
        close();
    }

    public void onTrackHand(float normalized) {
    }

    public boolean onGesture(com.navdy.service.library.events.input.GestureEvent event) {
        return false;
    }

    private void detentChangingStep(boolean isChangeIncreasing) {
        long now = android.os.SystemClock.elapsedRealtime();
        if (isChangeIncreasing != this.isLastChangeIncreasing || this.lastChangeActionTime + 250 < now) {
            this.changingStep = 1;
        } else {
            this.changingStep <<= 1;
            if (this.changingStep > 16) {
                this.changingStep = 16;
            }
        }
        this.isLastChangeIncreasing = isChangeIncreasing;
        this.lastChangeActionTime = now;
    }

    public boolean onKey(com.navdy.hud.app.manager.InputManager.CustomKeyEvent event) {
        if (this.controller == null) {
            return false;
        }
        if (event != com.navdy.hud.app.manager.InputManager.CustomKeyEvent.SELECT) {
            this.controller.resetTimeout();
        }
        switch (event) {
            case SELECT:
                if (this.choiceLayout != null) {
                    this.choiceLayout.executeSelectedItem();
                } else {
                    close();
                }
                return true;
            case LEFT:
                sLogger.v("Decreasing brightness");
                detentChangingStep(false);
                updateBrightness(-this.changingStep);
                return true;
            case RIGHT:
                sLogger.v("Increasing brightness");
                detentChangingStep(true);
                updateBrightness(this.changingStep);
                return true;
            default:
                return false;
        }
    }

    public com.navdy.hud.app.manager.InputManager.IInputHandler nextHandler() {
        return null;
    }

    private boolean getCurrentIsAutoBrightnessOn() {
        return "true".equals(this.sharedPreferences.getString(com.navdy.hud.app.settings.HUDSettings.AUTO_BRIGHTNESS, ""));
    }

    private int getCurrentBrightness() {
        if (this.isAutoBrightnessOn) {
            try {
                return android.provider.Settings.System.getInt(com.navdy.hud.app.HudApplication.getAppContext().getContentResolver(), "screen_brightness");
            } catch (android.provider.Settings.SettingNotFoundException e) {
                sLogger.e("Settings not found exception ", e);
                return DEFAULT_BRIGHTNESS_INT;
            }
        } else {
            try {
                return java.lang.Integer.parseInt(this.sharedPreferences.getString(com.navdy.hud.app.settings.HUDSettings.BRIGHTNESS, "128"));
            } catch (java.lang.NumberFormatException e2) {
                sLogger.e("Cannot parse brightness from the shared preferences", e2);
                return DEFAULT_BRIGHTNESS_INT;
            }
        }
    }

    public void onSharedPreferenceChanged(android.content.SharedPreferences sharedPreferences2, java.lang.String key) {
        if (key.equals(com.navdy.hud.app.settings.HUDSettings.BRIGHTNESS)) {
            updateState();
        }
    }

    private void updateBrightness(int delta) {
        if (delta != 0) {
            int newValue = this.currentValue + delta;
            if (newValue < 0) {
                newValue = 0;
            }
            if (newValue > 255) {
                newValue = 255;
            }
            this.sharedPreferencesEditor.putString(com.navdy.hud.app.settings.HUDSettings.BRIGHTNESS, java.lang.String.valueOf(newValue));
            this.sharedPreferencesEditor.apply();
        }
    }
}
