package com.navdy.hud.app.framework.glance;

public enum GlanceApp {
    FUEL(com.navdy.hud.app.framework.glance.GlanceConstants.colorFuelLevel, com.navdy.hud.app.R.drawable.icon_car_alert, com.navdy.hud.app.R.drawable.icon_glance_fuel_low, false, com.navdy.hud.app.framework.notifications.NotificationType.LOW_FUEL),
    GOOGLE_CALENDAR(com.navdy.hud.app.framework.glance.GlanceConstants.colorGoogleCalendar, com.navdy.hud.app.R.drawable.icon_glance_google_calendar, -1, false, com.navdy.hud.app.framework.notifications.NotificationType.GLANCE),
    GOOGLE_MAIL(com.navdy.hud.app.framework.glance.GlanceConstants.colorGoogleMail, com.navdy.hud.app.R.drawable.icon_glance_gmail, -1, true, com.navdy.hud.app.framework.notifications.NotificationType.GLANCE),
    GOOGLE_HANGOUT(com.navdy.hud.app.framework.glance.GlanceConstants.colorGoogleHangout, com.navdy.hud.app.R.drawable.icon_glance_hangout, -1, true, com.navdy.hud.app.framework.notifications.NotificationType.GLANCE),
    SLACK(com.navdy.hud.app.framework.glance.GlanceConstants.colorSlack, com.navdy.hud.app.R.drawable.icon_glance_slack, -1, true, com.navdy.hud.app.framework.notifications.NotificationType.GLANCE),
    WHATS_APP(com.navdy.hud.app.framework.glance.GlanceConstants.colorWhatsapp, com.navdy.hud.app.R.drawable.icon_glance_whatsapp, -1, true, com.navdy.hud.app.framework.notifications.NotificationType.GLANCE),
    FACEBOOK_MESSENGER(com.navdy.hud.app.framework.glance.GlanceConstants.colorFacebookMessenger, com.navdy.hud.app.R.drawable.icon_glance_facebook_messenger, -1, true, com.navdy.hud.app.framework.notifications.NotificationType.GLANCE),
    FACEBOOK(com.navdy.hud.app.framework.glance.GlanceConstants.colorFacebook, com.navdy.hud.app.R.drawable.icon_glance_facebook, com.navdy.hud.app.R.drawable.icon_glance_large_generic, false, com.navdy.hud.app.framework.notifications.NotificationType.GLANCE),
    TWITTER(com.navdy.hud.app.framework.glance.GlanceConstants.colorTwitter, com.navdy.hud.app.R.drawable.icon_glance_twitter, -1, true, com.navdy.hud.app.framework.notifications.NotificationType.GLANCE),
    IMESSAGE(com.navdy.hud.app.framework.glance.GlanceConstants.colorIMessage, com.navdy.hud.app.R.drawable.icon_message_blue, -1, true, com.navdy.hud.app.framework.notifications.NotificationType.GLANCE),
    APPLE_CALENDAR(com.navdy.hud.app.framework.glance.GlanceConstants.colorAppleCalendar, com.navdy.hud.app.R.drawable.icon_glance_calendar, -1, false, com.navdy.hud.app.framework.notifications.NotificationType.GLANCE),
    APPLE_MAIL(com.navdy.hud.app.framework.glance.GlanceConstants.colorAppleMail, com.navdy.hud.app.R.drawable.icon_glance_email, -1, true, com.navdy.hud.app.framework.notifications.NotificationType.GLANCE),
    SMS(com.navdy.hud.app.framework.glance.GlanceConstants.colorSms, com.navdy.hud.app.R.drawable.icon_message_blue, -1, true, com.navdy.hud.app.framework.notifications.NotificationType.GLANCE),
    GENERIC(com.navdy.hud.app.framework.glance.GlanceConstants.colorGeneric, com.navdy.hud.app.R.drawable.icon_glance_generic, com.navdy.hud.app.R.drawable.icon_glance_large_generic, false, com.navdy.hud.app.framework.notifications.NotificationType.GLANCE),
    GOOGLE_INBOX(com.navdy.hud.app.framework.glance.GlanceConstants.colorGoogleInbox, com.navdy.hud.app.R.drawable.icon_glance_google_inbox, -1, true, com.navdy.hud.app.framework.notifications.NotificationType.GLANCE),
    GENERIC_MAIL(com.navdy.hud.app.framework.glance.GlanceConstants.colorAppleMail, com.navdy.hud.app.R.drawable.icon_glance_email, -1, true, com.navdy.hud.app.framework.notifications.NotificationType.GLANCE),
    GENERIC_CALENDAR(com.navdy.hud.app.framework.glance.GlanceConstants.colorAppleCalendar, com.navdy.hud.app.R.drawable.icon_glance_calendar, -1, false, com.navdy.hud.app.framework.notifications.NotificationType.GLANCE),
    GENERIC_MESSAGE(com.navdy.hud.app.framework.glance.GlanceConstants.colorSms, com.navdy.hud.app.R.drawable.icon_message_blue, -1, true, com.navdy.hud.app.framework.notifications.NotificationType.GLANCE),
    GENERIC_SOCIAL(com.navdy.hud.app.framework.glance.GlanceConstants.colorTwitter, com.navdy.hud.app.R.drawable.icon_social_blue, -1, true, com.navdy.hud.app.framework.notifications.NotificationType.GLANCE);
    
    int color;
    boolean defaultPhotoBasedOnId;
    int mainIcon;
    com.navdy.hud.app.framework.notifications.NotificationType notificationType;
    int sideIcon;

    private GlanceApp(int color2, int sideIcon2, int mainIcon2, boolean defaultPhotoBasedOnId2, com.navdy.hud.app.framework.notifications.NotificationType notificationType2) {
        this.color = color2;
        this.sideIcon = sideIcon2;
        this.mainIcon = mainIcon2;
        this.defaultPhotoBasedOnId = defaultPhotoBasedOnId2;
        this.notificationType = notificationType2;
    }

    public int getSideIcon() {
        return this.sideIcon;
    }

    public int getMainIcon() {
        return this.mainIcon;
    }

    public int getColor() {
        return this.color;
    }

    public boolean isDefaultIconBasedOnId() {
        return this.defaultPhotoBasedOnId;
    }
}
