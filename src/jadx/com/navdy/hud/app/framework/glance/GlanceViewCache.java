package com.navdy.hud.app.framework.glance;

public class GlanceViewCache {
    private static final boolean VERBOSE = false;
    private static java.util.ArrayList<android.view.View> bigCalendarViewCache = new java.util.ArrayList<>(com.navdy.hud.app.framework.glance.GlanceViewCache.ViewType.BIG_CALENDAR.cacheSize);
    private static java.util.ArrayList<android.view.View> bigGlanceMessageSingleViewCache = new java.util.ArrayList<>(com.navdy.hud.app.framework.glance.GlanceViewCache.ViewType.BIG_GLANCE_MESSAGE_SINGLE.cacheSize);
    private static java.util.ArrayList<android.view.View> bigGlanceMessageViewCache = new java.util.ArrayList<>(com.navdy.hud.app.framework.glance.GlanceViewCache.ViewType.BIG_GLANCE_MESSAGE.cacheSize);
    private static java.util.ArrayList<android.view.View> bigMultiTextViewCache = new java.util.ArrayList<>(com.navdy.hud.app.framework.glance.GlanceViewCache.ViewType.BIG_MULTI_TEXT.cacheSize);
    private static java.util.ArrayList<android.view.View> bigTextViewCache = new java.util.ArrayList<>(com.navdy.hud.app.framework.glance.GlanceViewCache.ViewType.BIG_TEXT.cacheSize);
    private static java.util.HashMap<com.navdy.hud.app.framework.glance.GlanceViewCache.ViewType, java.util.ArrayList<android.view.View>> cachedViewMap = new java.util.HashMap<>();
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.framework.glance.GlanceViewCache.class);
    private static java.util.ArrayList<android.view.View> smallGlanceMessageViewCache = new java.util.ArrayList<>(com.navdy.hud.app.framework.glance.GlanceViewCache.ViewType.SMALL_GLANCE_MESSAGE.cacheSize);
    private static java.util.ArrayList<android.view.View> smallImageViewCache = new java.util.ArrayList<>(com.navdy.hud.app.framework.glance.GlanceViewCache.ViewType.SMALL_IMAGE.cacheSize);
    private static java.util.ArrayList<android.view.View> smallSignViewCache = new java.util.ArrayList<>(com.navdy.hud.app.framework.glance.GlanceViewCache.ViewType.SMALL_SIGN.cacheSize);
    private static java.util.HashSet<android.view.View> viewSet = new java.util.HashSet<>();

    public enum ViewType {
        SMALL_GLANCE_MESSAGE(com.navdy.hud.app.R.layout.glance_small_message, 2),
        BIG_GLANCE_MESSAGE(com.navdy.hud.app.R.layout.glance_large_message, 2),
        BIG_GLANCE_MESSAGE_SINGLE(com.navdy.hud.app.R.layout.glance_large_message_single, 2),
        SMALL_IMAGE(com.navdy.hud.app.R.layout.glance_small_image, 2),
        BIG_TEXT(com.navdy.hud.app.R.layout.glance_large_text, 2),
        BIG_MULTI_TEXT(com.navdy.hud.app.R.layout.glance_large_multi_text, 2),
        SMALL_SIGN(com.navdy.hud.app.R.layout.glance_small_sign, 2),
        BIG_CALENDAR(com.navdy.hud.app.R.layout.glance_large_calendar, 2);
        
        final int cacheSize;
        final int layoutId;

        private ViewType(int layoutId2, int cacheSize2) {
            this.layoutId = layoutId2;
            this.cacheSize = cacheSize2;
        }
    }

    static {
        cachedViewMap.put(com.navdy.hud.app.framework.glance.GlanceViewCache.ViewType.SMALL_GLANCE_MESSAGE, smallGlanceMessageViewCache);
        cachedViewMap.put(com.navdy.hud.app.framework.glance.GlanceViewCache.ViewType.BIG_GLANCE_MESSAGE, bigGlanceMessageViewCache);
        cachedViewMap.put(com.navdy.hud.app.framework.glance.GlanceViewCache.ViewType.BIG_GLANCE_MESSAGE_SINGLE, bigGlanceMessageSingleViewCache);
        cachedViewMap.put(com.navdy.hud.app.framework.glance.GlanceViewCache.ViewType.SMALL_IMAGE, smallImageViewCache);
        cachedViewMap.put(com.navdy.hud.app.framework.glance.GlanceViewCache.ViewType.BIG_TEXT, bigTextViewCache);
        cachedViewMap.put(com.navdy.hud.app.framework.glance.GlanceViewCache.ViewType.BIG_MULTI_TEXT, bigMultiTextViewCache);
        cachedViewMap.put(com.navdy.hud.app.framework.glance.GlanceViewCache.ViewType.SMALL_SIGN, smallSignViewCache);
        cachedViewMap.put(com.navdy.hud.app.framework.glance.GlanceViewCache.ViewType.BIG_CALENDAR, bigCalendarViewCache);
    }

    public static android.view.View getView(com.navdy.hud.app.framework.glance.GlanceViewCache.ViewType viewType, android.content.Context context) {
        android.view.View cachedView = null;
        java.util.ArrayList<android.view.View> list = (java.util.ArrayList) cachedViewMap.get(viewType);
        if (list.size() > 0) {
            cachedView = (android.view.View) list.remove(0);
            viewSet.remove(cachedView);
            if (cachedView.getParent() != null) {
                sLogger.e(":-/ view already has parent:" + viewType);
            }
        }
        if (cachedView == null) {
            cachedView = android.view.LayoutInflater.from(context).inflate(viewType.layoutId, null);
            if (sLogger.isLoggable(2)) {
                sLogger.v(" creating view for " + viewType);
            }
        } else if (sLogger.isLoggable(2)) {
            sLogger.v(" reusing cache for " + viewType);
        }
        return cachedView;
    }

    public static void putView(com.navdy.hud.app.framework.glance.GlanceViewCache.ViewType viewType, android.view.View view) {
        if (view != null) {
            if (viewSet.contains(view)) {
                sLogger.e(":-/ view already in cache:" + viewType);
                return;
            }
            if (view.getParent() != null) {
                sLogger.e(":-/ view already has parent:" + viewType);
            }
            view.setTranslationX(0.0f);
            view.setTranslationY(0.0f);
            view.setTag(null);
            java.util.ArrayList<android.view.View> list = (java.util.ArrayList) cachedViewMap.get(viewType);
            if (list.size() < viewType.cacheSize) {
                list.add(view);
                viewSet.add(view);
            }
            if (sLogger.isLoggable(2)) {
                sLogger.v(" putView cache for " + viewType);
            }
        }
    }

    public static void clearCache() {
        smallGlanceMessageViewCache.clear();
        bigGlanceMessageViewCache.clear();
        bigGlanceMessageSingleViewCache.clear();
        smallImageViewCache.clear();
        bigTextViewCache.clear();
        bigMultiTextViewCache.clear();
        smallSignViewCache.clear();
        bigCalendarViewCache.clear();
        viewSet.clear();
    }

    public static boolean supportScroll(com.navdy.hud.app.framework.glance.GlanceViewCache.ViewType viewType) {
        switch (viewType) {
            case BIG_GLANCE_MESSAGE:
                return true;
            default:
                return false;
        }
    }
}
