package com.navdy.hud.app.framework.glance;

public class GlanceHelper {
    private static final java.util.HashMap<java.lang.String, com.navdy.hud.app.framework.glance.GlanceApp> APP_ID_MAP = new java.util.HashMap<>();
    public static final java.lang.String DELETE_ALL_GLANCES_TOAST_ID = "glance-deleted";
    public static final java.lang.String FUEL_PACKAGE = "com.navdy.fuel";
    private static final java.util.concurrent.atomic.AtomicLong ID = new java.util.concurrent.atomic.AtomicLong(1);
    public static final java.lang.String IOS_PHONE_PACKAGE = "com.apple.mobilephone";
    public static final java.lang.String MICROSOFT_OUTLOOK = "com.microsoft.Office.Outlook";
    public static final java.lang.String MUSIC_PACKAGE = "com.navdy.music";
    public static final java.lang.String PHONE_PACKAGE = "com.navdy.phone";
    public static final java.lang.String SMS_PACKAGE = "com.navdy.sms";
    public static final java.lang.String TRAFFIC_PACKAGE = "com.navdy.traffic";
    public static final java.lang.String VOICE_SEARCH_PACKAGE = "com.navdy.voice.search";
    private static final java.lang.StringBuilder builder = new java.lang.StringBuilder();
    private static final com.navdy.hud.app.gesture.GestureServiceConnector gestureServiceConnector = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getGestureServiceConnector();
    private static final android.content.res.Resources resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.framework.glance.GlanceHelper.class);

    static {
        APP_ID_MAP.put(FUEL_PACKAGE, com.navdy.hud.app.framework.glance.GlanceApp.FUEL);
        APP_ID_MAP.put("com.google.android.calendar", com.navdy.hud.app.framework.glance.GlanceApp.GOOGLE_CALENDAR);
        APP_ID_MAP.put("com.google.android.gm", com.navdy.hud.app.framework.glance.GlanceApp.GOOGLE_MAIL);
        APP_ID_MAP.put("com.google.android.talk", com.navdy.hud.app.framework.glance.GlanceApp.GOOGLE_HANGOUT);
        APP_ID_MAP.put("com.Slack", com.navdy.hud.app.framework.glance.GlanceApp.SLACK);
        APP_ID_MAP.put("com.whatsapp", com.navdy.hud.app.framework.glance.GlanceApp.WHATS_APP);
        APP_ID_MAP.put("com.facebook.orca", com.navdy.hud.app.framework.glance.GlanceApp.FACEBOOK_MESSENGER);
        APP_ID_MAP.put("com.facebook.katana", com.navdy.hud.app.framework.glance.GlanceApp.FACEBOOK);
        APP_ID_MAP.put("com.twitter.android", com.navdy.hud.app.framework.glance.GlanceApp.TWITTER);
        APP_ID_MAP.put(SMS_PACKAGE, com.navdy.hud.app.framework.glance.GlanceApp.SMS);
        APP_ID_MAP.put("com.google.android.apps.inbox", com.navdy.hud.app.framework.glance.GlanceApp.GOOGLE_INBOX);
        APP_ID_MAP.put("com.google.calendar", com.navdy.hud.app.framework.glance.GlanceApp.GOOGLE_CALENDAR);
        APP_ID_MAP.put("com.google.Gmail", com.navdy.hud.app.framework.glance.GlanceApp.GOOGLE_MAIL);
        APP_ID_MAP.put("com.google.hangouts", com.navdy.hud.app.framework.glance.GlanceApp.GOOGLE_HANGOUT);
        APP_ID_MAP.put("com.tinyspeck.chatlyio", com.navdy.hud.app.framework.glance.GlanceApp.SLACK);
        APP_ID_MAP.put("net.whatsapp.WhatsApp", com.navdy.hud.app.framework.glance.GlanceApp.WHATS_APP);
        APP_ID_MAP.put("com.facebook.Messenger", com.navdy.hud.app.framework.glance.GlanceApp.FACEBOOK_MESSENGER);
        APP_ID_MAP.put("com.facebook.Facebook", com.navdy.hud.app.framework.glance.GlanceApp.FACEBOOK);
        APP_ID_MAP.put("com.atebits.Tweetie2", com.navdy.hud.app.framework.glance.GlanceApp.TWITTER);
        APP_ID_MAP.put(com.navdy.hud.app.profile.NotificationSettings.APP_ID_SMS, com.navdy.hud.app.framework.glance.GlanceApp.IMESSAGE);
        APP_ID_MAP.put(com.navdy.hud.app.profile.NotificationSettings.APP_ID_MAIL, com.navdy.hud.app.framework.glance.GlanceApp.APPLE_MAIL);
        APP_ID_MAP.put(com.navdy.hud.app.profile.NotificationSettings.APP_ID_CALENDAR, com.navdy.hud.app.framework.glance.GlanceApp.APPLE_CALENDAR);
        APP_ID_MAP.put(MICROSOFT_OUTLOOK, com.navdy.hud.app.framework.glance.GlanceApp.GENERIC_MAIL);
    }

    public static java.lang.String getId() {
        return java.lang.String.valueOf(ID.getAndIncrement());
    }

    public static com.navdy.hud.app.framework.glance.GlanceApp getGlancesApp(java.lang.String appId) {
        return (com.navdy.hud.app.framework.glance.GlanceApp) APP_ID_MAP.get(appId);
    }

    public static com.navdy.hud.app.framework.glance.GlanceApp getGlancesApp(com.navdy.service.library.events.glances.GlanceEvent event) {
        if (event == null) {
            return null;
        }
        com.navdy.hud.app.framework.glance.GlanceApp app = (com.navdy.hud.app.framework.glance.GlanceApp) APP_ID_MAP.get(event.provider);
        if (app != null) {
            return app;
        }
        switch (event.glanceType) {
            case GLANCE_TYPE_EMAIL:
                return com.navdy.hud.app.framework.glance.GlanceApp.GENERIC_MAIL;
            case GLANCE_TYPE_CALENDAR:
                return com.navdy.hud.app.framework.glance.GlanceApp.GENERIC_CALENDAR;
            case GLANCE_TYPE_MESSAGE:
                return com.navdy.hud.app.framework.glance.GlanceApp.GENERIC_MESSAGE;
            case GLANCE_TYPE_SOCIAL:
                return com.navdy.hud.app.framework.glance.GlanceApp.GENERIC_SOCIAL;
            default:
                return null;
        }
    }

    public static java.util.Map<java.lang.String, java.lang.String> buildDataMap(com.navdy.service.library.events.glances.GlanceEvent event) {
        java.util.Map<java.lang.String, java.lang.String> map = new java.util.HashMap<>();
        if (event.glanceData != null) {
            for (com.navdy.service.library.events.glances.KeyValue keyValue : event.glanceData) {
                map.put(keyValue.key, com.navdy.hud.app.framework.contacts.ContactUtil.sanitizeString(keyValue.value));
            }
        }
        return map;
    }

    public static java.lang.String getNotificationId(com.navdy.service.library.events.glances.GlanceEvent event) {
        return getNotificationId(event.glanceType, event.id);
    }

    public static java.lang.String getNotificationId(com.navdy.service.library.events.glances.GlanceEvent.GlanceType glanceType, java.lang.String id) {
        java.lang.String prefix;
        switch (glanceType) {
            case GLANCE_TYPE_EMAIL:
                prefix = com.navdy.hud.app.framework.glance.GlanceConstants.EMAIL_PREFIX;
                break;
            case GLANCE_TYPE_CALENDAR:
                prefix = com.navdy.hud.app.framework.glance.GlanceConstants.CALENDAR_PREFIX;
                break;
            case GLANCE_TYPE_MESSAGE:
                prefix = com.navdy.hud.app.framework.glance.GlanceConstants.MESSAGE_PREFIX;
                break;
            case GLANCE_TYPE_SOCIAL:
                prefix = com.navdy.hud.app.framework.glance.GlanceConstants.SOCIAL_PREFIX;
                break;
            case GLANCE_TYPE_FUEL:
                prefix = com.navdy.hud.app.framework.glance.GlanceConstants.FUEL_PREFIX;
                break;
            default:
                prefix = com.navdy.hud.app.framework.glance.GlanceConstants.GENERIC_PREFIX;
                break;
        }
        return prefix + id;
    }

    public static java.lang.String getTitle(com.navdy.hud.app.framework.glance.GlanceApp app, java.util.Map<java.lang.String, java.lang.String> data) {
        java.lang.String title;
        java.lang.String title2 = "";
        if (data == null) {
            return title2;
        }
        switch (app) {
            case GOOGLE_HANGOUT:
                title = (java.lang.String) data.get(com.navdy.service.library.events.glances.MessageConstants.MESSAGE_FROM.name());
                break;
            case SLACK:
            case WHATS_APP:
            case FACEBOOK_MESSENGER:
            case GENERIC_MESSAGE:
                title = (java.lang.String) data.get(com.navdy.service.library.events.glances.MessageConstants.MESSAGE_FROM.name());
                break;
            case FACEBOOK:
                title = (java.lang.String) data.get(com.navdy.service.library.events.glances.SocialConstants.SOCIAL_FROM.name());
                if (android.text.TextUtils.isEmpty(title)) {
                    title = com.navdy.hud.app.framework.glance.GlanceConstants.facebook;
                    break;
                }
                break;
            case TWITTER:
            case GENERIC_SOCIAL:
                title = (java.lang.String) data.get(com.navdy.service.library.events.glances.SocialConstants.SOCIAL_FROM.name());
                break;
            case IMESSAGE:
                title = (java.lang.String) data.get(com.navdy.service.library.events.glances.MessageConstants.MESSAGE_FROM.name());
                if (title == null) {
                    title = (java.lang.String) data.get(com.navdy.service.library.events.glances.MessageConstants.MESSAGE_FROM_NUMBER.name());
                    break;
                }
                break;
            case GOOGLE_CALENDAR:
            case APPLE_CALENDAR:
            case GENERIC_CALENDAR:
                title = (java.lang.String) data.get(com.navdy.service.library.events.glances.CalendarConstants.CALENDAR_TITLE.name());
                break;
            case GOOGLE_MAIL:
            case APPLE_MAIL:
            case GENERIC_MAIL:
            case GOOGLE_INBOX:
                title = (java.lang.String) data.get(com.navdy.service.library.events.glances.EmailConstants.EMAIL_FROM_NAME.name());
                if (title == null) {
                    title = (java.lang.String) data.get(com.navdy.service.library.events.glances.EmailConstants.EMAIL_FROM_EMAIL.name());
                    break;
                }
                break;
            case SMS:
                title = (java.lang.String) data.get(com.navdy.service.library.events.glances.MessageConstants.MESSAGE_FROM.name());
                if (title == null) {
                    title = (java.lang.String) data.get(com.navdy.service.library.events.glances.MessageConstants.MESSAGE_FROM_NUMBER.name());
                    break;
                }
                break;
            case FUEL:
                if (((java.lang.String) data.get(com.navdy.service.library.events.glances.FuelConstants.FUEL_LEVEL.name())) == null) {
                    title = resources.getString(com.navdy.hud.app.R.string.gas_station);
                    break;
                } else {
                    title = java.lang.String.format("%s %s%%", new java.lang.Object[]{com.navdy.hud.app.framework.glance.GlanceConstants.fuelNotificationTitle, data.get(com.navdy.service.library.events.glances.FuelConstants.FUEL_LEVEL.name())});
                    break;
                }
            default:
                title = com.navdy.hud.app.framework.glance.GlanceConstants.notification;
                break;
        }
        if (title == null) {
            title = "";
        }
        return title;
    }

    public static java.lang.String getSubTitle(com.navdy.hud.app.framework.glance.GlanceApp app, java.util.Map<java.lang.String, java.lang.String> data) {
        java.lang.String subTitle = "";
        if (data == null) {
            return subTitle;
        }
        switch (app) {
            case SLACK:
                subTitle = (java.lang.String) data.get(com.navdy.service.library.events.glances.MessageConstants.MESSAGE_DOMAIN.name());
                if (subTitle != null) {
                    subTitle = resources.getString(com.navdy.hud.app.R.string.slack_team, new java.lang.Object[]{subTitle});
                    break;
                }
                break;
            case GOOGLE_CALENDAR:
            case APPLE_CALENDAR:
            case GENERIC_CALENDAR:
                subTitle = (java.lang.String) data.get(com.navdy.service.library.events.glances.CalendarConstants.CALENDAR_TIME_STR.name());
                break;
            case GOOGLE_MAIL:
            case APPLE_MAIL:
            case GENERIC_MAIL:
            case GOOGLE_INBOX:
                subTitle = (java.lang.String) data.get(com.navdy.service.library.events.glances.EmailConstants.EMAIL_SUBJECT.name());
                break;
            case FUEL:
                if (data.get(com.navdy.service.library.events.glances.FuelConstants.NO_ROUTE.name()) == null) {
                    subTitle = com.navdy.hud.app.framework.glance.GlanceConstants.fuelNotificationSubtitle;
                    break;
                }
                break;
            case GENERIC:
                subTitle = (java.lang.String) data.get(com.navdy.service.library.events.glances.GenericConstants.GENERIC_TITLE.name());
                break;
        }
        if (subTitle == null) {
            subTitle = "";
        }
        return subTitle;
    }

    public static java.lang.String getNumber(com.navdy.hud.app.framework.glance.GlanceApp app, java.util.Map<java.lang.String, java.lang.String> data) {
        java.lang.String number = null;
        if (data == null) {
            return null;
        }
        switch (app) {
            case GOOGLE_HANGOUT:
            case IMESSAGE:
            case SMS:
                number = (java.lang.String) data.get(com.navdy.service.library.events.glances.MessageConstants.MESSAGE_FROM_NUMBER.name());
                break;
        }
        return number;
    }

    public static java.lang.String getFrom(com.navdy.hud.app.framework.glance.GlanceApp app, java.util.Map<java.lang.String, java.lang.String> data) {
        java.lang.String from = null;
        if (data == null) {
            return null;
        }
        switch (app) {
            case GOOGLE_HANGOUT:
            case IMESSAGE:
            case SMS:
                from = (java.lang.String) data.get(com.navdy.service.library.events.glances.MessageConstants.MESSAGE_FROM.name());
                break;
        }
        return from;
    }

    public static boolean isPhotoCheckRequired(com.navdy.hud.app.framework.glance.GlanceApp app) {
        switch (app) {
            case GOOGLE_HANGOUT:
            case IMESSAGE:
            case SMS:
                return true;
            default:
                return false;
        }
    }

    public static synchronized java.lang.String getGlanceMessage(com.navdy.hud.app.framework.glance.GlanceApp glanceApp, java.util.Map<java.lang.String, java.lang.String> dataMap) {
        java.lang.String message;
        synchronized (com.navdy.hud.app.framework.glance.GlanceHelper.class) {
            switch (glanceApp) {
                case GOOGLE_HANGOUT:
                case SLACK:
                case WHATS_APP:
                case FACEBOOK_MESSENGER:
                case GENERIC_MESSAGE:
                case IMESSAGE:
                case SMS:
                    java.lang.String body = (java.lang.String) dataMap.get(com.navdy.service.library.events.glances.MessageConstants.MESSAGE_BODY.name());
                    if (body != null) {
                        builder.append(body);
                        break;
                    }
                    break;
                case FACEBOOK:
                case TWITTER:
                case GENERIC_SOCIAL:
                    java.lang.String body2 = (java.lang.String) dataMap.get(com.navdy.service.library.events.glances.SocialConstants.SOCIAL_MESSAGE.name());
                    if (body2 != null) {
                        builder.append(body2);
                        break;
                    }
                    break;
                case GOOGLE_MAIL:
                case APPLE_MAIL:
                case GENERIC_MAIL:
                case GOOGLE_INBOX:
                    java.lang.String body3 = (java.lang.String) dataMap.get(com.navdy.service.library.events.glances.EmailConstants.EMAIL_BODY.name());
                    if (body3 != null) {
                        builder.append(body3);
                        break;
                    }
                    break;
                case GENERIC:
                    java.lang.String body4 = (java.lang.String) dataMap.get(com.navdy.service.library.events.glances.GenericConstants.GENERIC_MESSAGE.name());
                    if (body4 != null) {
                        builder.append(body4);
                        break;
                    }
                    break;
            }
            message = builder.toString();
            builder.setLength(0);
        }
        return message;
    }

    public static synchronized java.lang.String getTtsMessage(com.navdy.hud.app.framework.glance.GlanceApp glanceApp, java.util.Map<java.lang.String, java.lang.String> dataMap, java.lang.StringBuilder stringBuilder1, java.lang.StringBuilder stringBuilder2, com.navdy.hud.app.common.TimeHelper timeHelper) {
        java.lang.String tts;
        synchronized (com.navdy.hud.app.framework.glance.GlanceHelper.class) {
            switch (glanceApp) {
                case GOOGLE_HANGOUT:
                case SLACK:
                case WHATS_APP:
                case FACEBOOK_MESSENGER:
                case GENERIC_MESSAGE:
                case IMESSAGE:
                case SMS:
                    java.lang.String body = (java.lang.String) dataMap.get(com.navdy.service.library.events.glances.MessageConstants.MESSAGE_BODY.name());
                    if (body != null) {
                        builder.append(body);
                        break;
                    }
                    break;
                case FACEBOOK:
                case TWITTER:
                case GENERIC_SOCIAL:
                    java.lang.String body2 = (java.lang.String) dataMap.get(com.navdy.service.library.events.glances.SocialConstants.SOCIAL_MESSAGE.name());
                    if (body2 != null) {
                        builder.append(body2);
                        break;
                    }
                    break;
                case GOOGLE_CALENDAR:
                case APPLE_CALENDAR:
                case GENERIC_CALENDAR:
                    java.lang.String location = (java.lang.String) dataMap.get(com.navdy.service.library.events.glances.CalendarConstants.CALENDAR_LOCATION.name());
                    java.lang.String title = (java.lang.String) dataMap.get(com.navdy.service.library.events.glances.CalendarConstants.CALENDAR_TITLE.name());
                    long millis = 0;
                    java.lang.String str = (java.lang.String) dataMap.get(com.navdy.service.library.events.glances.CalendarConstants.CALENDAR_TIME.name());
                    if (str != null) {
                        try {
                            millis = java.lang.Long.parseLong(str);
                        } catch (Throwable t) {
                            sLogger.e(t);
                        }
                    }
                    java.lang.String time = null;
                    if (millis > 0) {
                        java.lang.String data = getCalendarTime(millis, stringBuilder1, stringBuilder2, timeHelper);
                        if (android.text.TextUtils.isEmpty(stringBuilder1.toString())) {
                            java.lang.String pmMarker = stringBuilder2.toString();
                            if (!android.text.TextUtils.isEmpty(pmMarker)) {
                                time = resources.getString(com.navdy.hud.app.R.string.tts_calendar_time_at, new java.lang.Object[]{data, android.text.TextUtils.equals(pmMarker, com.navdy.hud.app.framework.glance.GlanceConstants.pmMarker) ? com.navdy.hud.app.framework.glance.GlanceConstants.pm : com.navdy.hud.app.framework.glance.GlanceConstants.am});
                            } else {
                                time = data + com.navdy.hud.app.framework.glance.GlanceConstants.PERIOD;
                            }
                        } else {
                            try {
                                time = java.lang.Long.parseLong(data) > 1 ? resources.getString(com.navdy.hud.app.R.string.tts_calendar_time_mins, new java.lang.Object[]{data}) : resources.getString(com.navdy.hud.app.R.string.tts_calendar_time_min, new java.lang.Object[]{data});
                            } catch (Throwable th) {
                                sLogger.e(th);
                            }
                        }
                    }
                    if (title != null) {
                        builder.append(resources.getString(com.navdy.hud.app.R.string.tts_calendar_title, new java.lang.Object[]{title}));
                    }
                    if (time != null) {
                        builder.append(" ");
                        builder.append(time);
                    }
                    if (location != null) {
                        builder.append(" ");
                        builder.append(resources.getString(com.navdy.hud.app.R.string.tts_calendar_location, new java.lang.Object[]{location}));
                        break;
                    }
                    break;
                case GOOGLE_MAIL:
                case APPLE_MAIL:
                case GENERIC_MAIL:
                case GOOGLE_INBOX:
                    java.lang.String subject = (java.lang.String) dataMap.get(com.navdy.service.library.events.glances.EmailConstants.EMAIL_SUBJECT.name());
                    if (subject != null) {
                        builder.append(subject);
                    }
                    java.lang.String body3 = (java.lang.String) dataMap.get(com.navdy.service.library.events.glances.EmailConstants.EMAIL_BODY.name());
                    if (body3 != null) {
                        if (builder.length() > 0) {
                            builder.append(com.navdy.hud.app.framework.glance.GlanceConstants.PERIOD);
                            builder.append(" ");
                        }
                        builder.append(body3);
                        break;
                    }
                    break;
                case FUEL:
                    if (dataMap.containsKey(com.navdy.service.library.events.glances.FuelConstants.FUEL_LEVEL.name())) {
                        builder.append(resources.getString(com.navdy.hud.app.R.string.your_fuel_level_is_low_tts));
                    }
                    builder.append(resources.getString(com.navdy.hud.app.R.string.i_can_add_trip_gas_station, new java.lang.Object[]{dataMap.get(com.navdy.service.library.events.glances.FuelConstants.GAS_STATION_NAME.name())}));
                    builder.append(com.navdy.hud.app.framework.glance.GlanceConstants.PERIOD);
                    break;
                case GENERIC:
                    java.lang.String body4 = (java.lang.String) dataMap.get(com.navdy.service.library.events.glances.GenericConstants.GENERIC_MESSAGE.name());
                    if (body4 != null) {
                        builder.append(body4);
                        break;
                    }
                    break;
            }
            tts = builder.toString();
            builder.setLength(0);
        }
        return tts;
    }

    public static com.navdy.hud.app.framework.glance.GlanceViewCache.ViewType getSmallViewType(com.navdy.hud.app.framework.glance.GlanceApp glanceApp) {
        switch (glanceApp) {
            case GOOGLE_CALENDAR:
            case APPLE_CALENDAR:
            case GENERIC_CALENDAR:
                return com.navdy.hud.app.framework.glance.GlanceViewCache.ViewType.SMALL_SIGN;
            default:
                return com.navdy.hud.app.framework.glance.GlanceViewCache.ViewType.SMALL_GLANCE_MESSAGE;
        }
    }

    public static com.navdy.hud.app.framework.glance.GlanceViewCache.ViewType getLargeViewType(com.navdy.hud.app.framework.glance.GlanceApp glanceApp) {
        switch (glanceApp) {
            case GOOGLE_CALENDAR:
            case APPLE_CALENDAR:
            case GENERIC_CALENDAR:
                return com.navdy.hud.app.framework.glance.GlanceViewCache.ViewType.BIG_CALENDAR;
            case FUEL:
                return com.navdy.hud.app.framework.glance.GlanceViewCache.ViewType.BIG_MULTI_TEXT;
            default:
                return com.navdy.hud.app.framework.glance.GlanceViewCache.ViewType.BIG_GLANCE_MESSAGE_SINGLE;
        }
    }

    public static boolean usesMessageLayout(com.navdy.hud.app.framework.glance.GlanceApp glanceApp) {
        switch (glanceApp) {
            case GOOGLE_HANGOUT:
            case SLACK:
            case WHATS_APP:
            case FACEBOOK_MESSENGER:
            case GENERIC_MESSAGE:
            case FACEBOOK:
            case TWITTER:
            case GENERIC_SOCIAL:
            case IMESSAGE:
            case GOOGLE_MAIL:
            case APPLE_MAIL:
            case GENERIC_MAIL:
            case GOOGLE_INBOX:
            case SMS:
            case GENERIC:
                return true;
            default:
                return false;
        }
    }

    public static java.lang.String getTimeStr(long now, java.util.Date date, com.navdy.hud.app.common.TimeHelper timeHelper) {
        if (now - date.getTime() <= 60000) {
            return com.navdy.hud.app.framework.glance.GlanceConstants.nowStr;
        }
        return timeHelper.formatTime(date, null);
    }

    public static boolean isDeleteAllView(android.view.View view) {
        if (view == null || view.getTag() != com.navdy.hud.app.framework.glance.GlanceConstants.DELETE_ALL_VIEW_TAG) {
            return false;
        }
        return true;
    }

    public static void showGlancesDeletedToast() {
        android.os.Bundle bundle = new android.os.Bundle();
        bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_TIMEOUT, 1000);
        bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_IMAGE, com.navdy.hud.app.R.drawable.icon_qm_glances_grey);
        bundle.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_2, resources.getString(com.navdy.hud.app.R.string.glances_deleted));
        bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_2_STYLE, com.navdy.hud.app.R.style.Glances_1);
        com.navdy.hud.app.framework.toast.ToastManager toastManager = com.navdy.hud.app.framework.toast.ToastManager.getInstance();
        toastManager.dismissCurrentToast();
        toastManager.clearAllPendingToast();
        toastManager.addToast(new com.navdy.hud.app.framework.toast.ToastManager.ToastParams(DELETE_ALL_GLANCES_TOAST_ID, bundle, null, true, true));
    }

    public static boolean isTrafficNotificationEnabled() {
        return isPackageEnabled(TRAFFIC_PACKAGE);
    }

    public static boolean isFuelNotificationEnabled() {
        return isPackageEnabled(FUEL_PACKAGE);
    }

    public static boolean isPhoneNotificationEnabled() {
        return isPackageEnabled(PHONE_PACKAGE) || isPackageEnabled("com.apple.mobilephone");
    }

    public static boolean isMusicNotificationEnabled() {
        return isPackageEnabled(MUSIC_PACKAGE);
    }

    public static boolean isPackageEnabled(java.lang.String packageName) {
        if (!com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile().getNotificationPreferences().enabled.booleanValue()) {
            return false;
        }
        return java.lang.Boolean.TRUE.equals(com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile().getNotificationSettings().enabled(packageName));
    }

    public static boolean isCalendarApp(com.navdy.hud.app.framework.glance.GlanceApp glanceApp) {
        switch (glanceApp) {
            case GOOGLE_CALENDAR:
            case APPLE_CALENDAR:
            case GENERIC_CALENDAR:
                return true;
            default:
                return false;
        }
    }

    public static java.lang.String getCalendarTime(long time, java.lang.StringBuilder timeUnit, java.lang.StringBuilder ampmMarker, com.navdy.hud.app.common.TimeHelper timeHelper) {
        long minutes = (long) java.lang.Math.round(((float) (time - java.lang.System.currentTimeMillis())) / 60000.0f);
        timeUnit.setLength(0);
        ampmMarker.setLength(0);
        if (minutes >= 60 || minutes < -5) {
            return timeHelper.formatTime12Hour(new java.util.Date(time), ampmMarker, true);
        }
        if (minutes <= 0) {
            return com.navdy.hud.app.framework.glance.GlanceConstants.nowStr;
        }
        java.lang.String str = java.lang.String.valueOf(minutes);
        timeUnit.append(com.navdy.hud.app.framework.glance.GlanceConstants.minute);
        return str;
    }

    public static void initMessageAttributes() {
        android.content.Context context = com.navdy.hud.app.HudApplication.getAppContext();
        android.widget.TextView textView = new android.widget.TextView(context);
        textView.setTextAppearance(context, com.navdy.hud.app.R.style.glance_message_title);
        android.text.StaticLayout layout = new android.text.StaticLayout("RockgOn", textView.getPaint(), com.navdy.hud.app.framework.glance.GlanceConstants.messageWidthBound, android.text.Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);
        int height = layout.getHeight() + com.navdy.hud.app.framework.glance.GlanceConstants.messageTitleMarginTop + com.navdy.hud.app.framework.glance.GlanceConstants.messageTitleMarginBottom;
        sLogger.v("message-title-ht = " + height + " layout=" + layout.getHeight());
        com.navdy.hud.app.framework.glance.GlanceConstants.setMessageTitleHeight(height);
    }

    public static boolean needsScrollLayout(java.lang.String text) {
        boolean needsScroll;
        if (android.text.TextUtils.isEmpty(text)) {
            return false;
        }
        android.content.Context context = com.navdy.hud.app.HudApplication.getAppContext();
        android.widget.TextView textView = new android.widget.TextView(context);
        textView.setTextAppearance(context, com.navdy.hud.app.R.style.glance_message_body);
        float left = ((float) (com.navdy.hud.app.framework.glance.GlanceConstants.messageHeightBound / 2)) - ((float) (new android.text.StaticLayout(text, textView.getPaint(), com.navdy.hud.app.framework.glance.GlanceConstants.messageWidthBound, android.text.Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false).getHeight() / 2));
        int titleHt = com.navdy.hud.app.framework.glance.GlanceConstants.getMessageTitleHeight();
        if (left > ((float) titleHt)) {
            needsScroll = false;
        } else {
            needsScroll = true;
        }
        sLogger.v("message-tittle-calc left[" + left + "] titleHt [" + titleHt + "]");
        return needsScroll;
    }

    public static com.navdy.hud.app.gesture.GestureServiceConnector getGestureService() {
        return gestureServiceConnector;
    }

    public static int getIcon(java.lang.String str) {
        boolean z;
        if (android.text.TextUtils.isEmpty(str)) {
            return -1;
        }
        switch (str.hashCode()) {
            case -30680433:
                if (str.equals("GLANCE_ICON_MESSAGE_SIDE_BLUE")) {
                    z = true;
                    break;
                }
            case -9470533:
                if (str.equals("GLANCE_ICON_NAVDY_MAIN")) {
                    z = false;
                    break;
                }
            default:
                z = true;
                break;
        }
        switch (z) {
            case false:
                return com.navdy.hud.app.R.drawable.icon_user_navdy;
            case true:
                return com.navdy.hud.app.R.drawable.icon_message_blue;
            default:
                return -1;
        }
    }
}
