package com.navdy.hud.app.framework.glance;

public class GlanceHandler {
    private static final java.lang.String EMPTY = "";
    private static final java.text.SimpleDateFormat dateFormat1 = new java.text.SimpleDateFormat("hh:mm a");
    private static final java.text.SimpleDateFormat dateFormat2 = new java.text.SimpleDateFormat("hh:mm");
    private static final java.text.SimpleDateFormat dateFormat3 = new java.text.SimpleDateFormat("MMM d, hh:mm a");
    private static final java.text.SimpleDateFormat dateFormat4 = new java.text.SimpleDateFormat("h");
    private static final java.text.SimpleDateFormat dateFormat5 = new java.text.SimpleDateFormat("h a");
    private static final java.lang.Object lockObj = new java.lang.Object();
    private static final com.navdy.hud.app.framework.glance.GlanceHandler sInstance = new com.navdy.hud.app.framework.glance.GlanceHandler();
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.framework.glance.GlanceHandler.class);
    private com.squareup.otto.Bus bus = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getBus();
    private com.navdy.hud.app.framework.glance.GlanceTracker glanceTracker = new com.navdy.hud.app.framework.glance.GlanceTracker();
    private java.util.HashMap<java.lang.String, java.lang.String> messagesSent = new java.util.HashMap<>();

    class Anon1 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.service.library.events.glances.GlanceEvent val$event;

        Anon1(com.navdy.service.library.events.glances.GlanceEvent glanceEvent) {
            this.val$event = glanceEvent;
        }

        public void run() {
            try {
                com.navdy.hud.app.framework.glance.GlanceHandler.this.handleGlancEventInternal(this.val$event);
            } catch (Throwable t) {
                com.navdy.hud.app.framework.glance.GlanceHandler.sLogger.e("handleGlancEventInternal", t);
            }
        }
    }

    class Anon2 implements com.navdy.hud.app.framework.twilio.TwilioSmsManager.Callback {
        final /* synthetic */ java.lang.String val$message;
        final /* synthetic */ java.lang.String val$name;
        final /* synthetic */ java.lang.String val$number;

        Anon2(java.lang.String str, java.lang.String str2, java.lang.String str3) {
            this.val$number = str;
            this.val$message = str2;
            this.val$name = str3;
        }

        public void result(com.navdy.hud.app.framework.twilio.TwilioSmsManager.ErrorCode code) {
            if (code != com.navdy.hud.app.framework.twilio.TwilioSmsManager.ErrorCode.SUCCESS) {
                com.navdy.hud.app.framework.glance.GlanceHandler.this.sendSmsFailedNotification(this.val$number, this.val$message, this.val$name);
            }
        }
    }

    public static com.navdy.hud.app.framework.glance.GlanceHandler getInstance() {
        return sInstance;
    }

    private GlanceHandler() {
        this.bus.register(this);
    }

    @com.squareup.otto.Subscribe
    public void onGlanceEvent(com.navdy.service.library.events.glances.GlanceEvent event) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.framework.glance.GlanceHandler.Anon1(event), 14);
    }

    /* access modifiers changed from: private */
    public void handleGlancEventInternal(com.navdy.service.library.events.glances.GlanceEvent event) {
        com.navdy.hud.app.framework.glance.GlanceNotification glanceNotification;
        java.lang.String logMsg;
        printGlance(event);
        if (android.text.TextUtils.isEmpty(event.id) || android.text.TextUtils.isEmpty(event.provider) || event.glanceType == null || event.glanceData == null || event.glanceData.size() == 0) {
            sLogger.v("invalid glance event");
            return;
        }
        com.navdy.hud.app.framework.glance.GlanceApp glanceApp = com.navdy.hud.app.framework.glance.GlanceHelper.getGlancesApp(event);
        if (glanceApp == null) {
            glanceApp = com.navdy.hud.app.framework.glance.GlanceApp.GENERIC;
        }
        java.util.Map<java.lang.String, java.lang.String> data = com.navdy.hud.app.framework.glance.GlanceHelper.buildDataMap(event);
        java.util.List<com.navdy.service.library.events.contacts.Contact> contactList = null;
        switch (glanceApp) {
            case SMS:
            case IMESSAGE:
            case GOOGLE_HANGOUT:
                boolean hasNumber = false;
                if (!android.text.TextUtils.isEmpty((java.lang.String) data.get(com.navdy.service.library.events.glances.MessageConstants.MESSAGE_BODY.name()))) {
                    java.lang.String id = com.navdy.hud.app.framework.glance.GlanceHelper.getNumber(glanceApp, data);
                    java.lang.String source = com.navdy.hud.app.framework.glance.GlanceHelper.getFrom(glanceApp, data);
                    if (com.navdy.hud.app.framework.contacts.ContactUtil.isValidNumber(id)) {
                        hasNumber = true;
                        sLogger.v("hasNumber:" + id);
                        if (source == null) {
                            source = id;
                        }
                    } else {
                        id = source;
                        source = null;
                    }
                    if (id != null) {
                        com.navdy.hud.app.framework.recentcall.RecentCall call = new com.navdy.hud.app.framework.recentcall.RecentCall(source, com.navdy.hud.app.framework.recentcall.RecentCall.Category.MESSAGE, id, com.navdy.hud.app.framework.contacts.NumberType.OTHER, new java.util.Date(), com.navdy.hud.app.framework.recentcall.RecentCall.CallType.INCOMING, -1, 0);
                        com.navdy.hud.app.framework.recentcall.RecentCallManager recentCallManager = com.navdy.hud.app.framework.recentcall.RecentCallManager.getInstance();
                        boolean ret = recentCallManager.handleNewCall(call);
                        if (!ret) {
                            contactList = recentCallManager.getContactsFromId(id);
                            sLogger.v("recent call contact list found [" + id + "]");
                        }
                        sLogger.v("recent call id[" + id + "]");
                        if (!hasNumber) {
                            if (!ret) {
                                sLogger.v("no Number yet:" + id);
                                break;
                            } else {
                                sLogger.v("got Number:" + call.number);
                                java.util.ArrayList arrayList = new java.util.ArrayList();
                                for (com.navdy.service.library.events.glances.KeyValue k : event.glanceData) {
                                    if (!android.text.TextUtils.equals(k.key, com.navdy.service.library.events.glances.MessageConstants.MESSAGE_FROM_NUMBER.name())) {
                                        arrayList.add(k);
                                    }
                                }
                                arrayList.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.MessageConstants.MESSAGE_FROM_NUMBER.name(), call.number));
                                event = new com.navdy.service.library.events.glances.GlanceEvent.Builder().id(event.id).actions(event.actions).postTime(event.postTime).glanceType(event.glanceType).provider(event.provider).glanceData(arrayList).build();
                                data = com.navdy.hud.app.framework.glance.GlanceHelper.buildDataMap(event);
                                break;
                            }
                        }
                    }
                } else {
                    sLogger.v("glance message does not exist:" + glanceApp);
                    return;
                }
                break;
            case SLACK:
            case WHATS_APP:
            case FACEBOOK_MESSENGER:
            case GENERIC_MESSAGE:
                if (android.text.TextUtils.isEmpty((java.lang.String) data.get(com.navdy.service.library.events.glances.MessageConstants.MESSAGE_BODY.name()))) {
                    sLogger.v("glance message does not exist:" + glanceApp);
                    return;
                }
                break;
            case FACEBOOK:
            case TWITTER:
            case GENERIC_SOCIAL:
                if (android.text.TextUtils.isEmpty((java.lang.String) data.get(com.navdy.service.library.events.glances.SocialConstants.SOCIAL_MESSAGE.name()))) {
                    sLogger.v("glance message does not exist:" + glanceApp);
                    return;
                }
                break;
            case GENERIC:
                if (android.text.TextUtils.isEmpty((java.lang.String) data.get(com.navdy.service.library.events.glances.GenericConstants.GENERIC_MESSAGE.name()))) {
                    sLogger.v("glance message does not exist:" + glanceApp);
                    return;
                }
                break;
            case GOOGLE_MAIL:
            case GOOGLE_INBOX:
            case APPLE_MAIL:
            case GENERIC_MAIL:
                java.lang.String subject = (java.lang.String) data.get(com.navdy.service.library.events.glances.EmailConstants.EMAIL_SUBJECT.name());
                if (android.text.TextUtils.isEmpty((java.lang.String) data.get(com.navdy.service.library.events.glances.EmailConstants.EMAIL_BODY.name())) && android.text.TextUtils.isEmpty(subject)) {
                    sLogger.v("glance message does not exist:" + glanceApp);
                    return;
                }
        }
        if (com.navdy.hud.app.framework.glance.GlanceHelper.usesMessageLayout(glanceApp)) {
            boolean needsScroll = com.navdy.hud.app.framework.glance.GlanceHelper.needsScrollLayout(com.navdy.hud.app.framework.glance.GlanceHelper.getGlanceMessage(glanceApp, data));
            if (needsScroll) {
                glanceNotification = new com.navdy.hud.app.framework.glance.GlanceNotification(event, glanceApp, com.navdy.hud.app.framework.glance.GlanceViewCache.ViewType.BIG_GLANCE_MESSAGE, data);
            } else {
                glanceNotification = new com.navdy.hud.app.framework.glance.GlanceNotification(event, glanceApp, data);
            }
            logMsg = "posted glance [" + event.id + "] scroll[" + needsScroll + "]";
        } else if (com.navdy.hud.app.framework.glance.GlanceHelper.isCalendarApp(glanceApp)) {
            java.lang.String eventTime = (java.lang.String) data.get(com.navdy.service.library.events.glances.CalendarConstants.CALENDAR_TIME.name());
            long millis = 0;
            if (eventTime != null) {
                try {
                    millis = java.lang.Long.parseLong(eventTime);
                } catch (Throwable t) {
                    sLogger.e(t);
                }
            }
            if (millis == 0) {
                java.lang.String eventTime2 = (java.lang.String) data.get(com.navdy.service.library.events.glances.CalendarConstants.CALENDAR_TIME_STR.name());
                sLogger.v("event-time-str =" + eventTime2);
                if (!android.text.TextUtils.isEmpty(eventTime2)) {
                    millis = extractTime(eventTime2);
                }
                if (millis != 0) {
                    data.put(com.navdy.service.library.events.glances.CalendarConstants.CALENDAR_TIME.name(), java.lang.String.valueOf(millis));
                }
            }
            glanceNotification = new com.navdy.hud.app.framework.glance.GlanceNotification(event, glanceApp, data);
            logMsg = "posted glance [" + event.id + "] time[" + millis + "]";
        } else {
            glanceNotification = new com.navdy.hud.app.framework.glance.GlanceNotification(event, glanceApp, data);
            logMsg = "posted glance [" + event.id + "]";
        }
        long seenAt = this.glanceTracker.isNotificationSeen(event, glanceApp, data);
        if (seenAt > 0) {
            sLogger.v("glance already seen [" + event.id + "] at [" + seenAt + "] now [" + android.os.SystemClock.elapsedRealtime() + "]");
            return;
        }
        if (contactList != null) {
            java.util.ArrayList arrayList2 = new java.util.ArrayList();
            for (com.navdy.service.library.events.contacts.Contact c : contactList) {
                arrayList2.add(new com.navdy.hud.app.framework.contacts.Contact(c));
            }
            glanceNotification.setContactList(arrayList2);
        }
        com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().addNotification(glanceNotification);
        sLogger.v(logMsg);
    }

    private static void printGlance(com.navdy.service.library.events.glances.GlanceEvent event) {
        int size;
        int i = 0;
        if (event == null) {
            sLogger.v("null glance event");
            return;
        }
        com.navdy.service.library.log.Logger logger = sLogger;
        java.lang.StringBuilder append = new java.lang.StringBuilder().append("[glance-event] id[").append(event.id).append("] type[").append(event.glanceType).append("] provider[").append(event.provider).append("] data-size [");
        if (event.glanceData == null) {
            size = 0;
        } else {
            size = event.glanceData.size();
        }
        java.lang.StringBuilder append2 = append.append(size).append("] action-size [");
        if (event.actions != null) {
            i = event.actions.size();
        }
        logger.v(append2.append(i).append("]").toString());
        if (event.glanceData != null && event.glanceData.size() > 0) {
            for (com.navdy.service.library.events.glances.KeyValue keyValue : event.glanceData) {
                sLogger.v("[glance-event] data key[" + keyValue.key + "] val[" + keyValue.value + "]");
            }
        }
        if (event.actions != null && event.actions.size() > 0) {
            for (com.navdy.service.library.events.glances.GlanceEvent.GlanceActions action : event.actions) {
                sLogger.v("[glance-event] action[" + action + "]");
            }
        }
    }

    @com.squareup.otto.Subscribe
    public void onMessage(com.navdy.service.library.events.messaging.SmsMessageResponse event) {
        try {
            if (event.status == com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS) {
                com.navdy.hud.app.analytics.AnalyticsSupport.recordSmsSent(true, com.navdy.hud.app.analytics.AnalyticsSupport.ATTR_PLATFORM_ANDROID, com.navdy.hud.app.framework.glance.GlanceConstants.areMessageCanned());
                return;
            }
            com.navdy.hud.app.analytics.AnalyticsSupport.recordSmsSent(false, com.navdy.hud.app.analytics.AnalyticsSupport.ATTR_PLATFORM_ANDROID, com.navdy.hud.app.framework.glance.GlanceConstants.areMessageCanned());
            java.lang.String msg = removeMessage(event.id);
            if (msg != null) {
                getInstance().sendSmsFailedNotification(event.number, msg, event.name);
            } else {
                sLogger.e("sms message with id:" + event.id + " not found");
            }
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }

    public boolean sendMessage(java.lang.String number, java.lang.String message, java.lang.String name) {
        try {
            if (!com.navdy.hud.app.util.GenericUtil.isClientiOS()) {
                java.lang.String msgId = java.util.UUID.randomUUID().toString();
                addMessage(msgId, message);
                this.bus.post(new com.navdy.hud.app.event.RemoteEvent(new com.navdy.service.library.events.messaging.SmsMessageRequest(number, message, name, msgId)));
                sLogger.v("sms sent");
                return true;
            } else if (com.navdy.hud.app.framework.twilio.TwilioSmsManager.getInstance().sendSms(number, message, new com.navdy.hud.app.framework.glance.GlanceHandler.Anon2(number, message, name)) == com.navdy.hud.app.framework.twilio.TwilioSmsManager.ErrorCode.SUCCESS) {
                sLogger.v("sms-twilio queued");
                return true;
            } else {
                com.navdy.hud.app.analytics.AnalyticsSupport.recordSmsSent(false, com.navdy.hud.app.analytics.AnalyticsSupport.ATTR_PLATFORM_IOS, com.navdy.hud.app.framework.glance.GlanceConstants.areMessageCanned());
                return false;
            }
        } catch (Throwable t) {
            sLogger.e(t);
            return false;
        }
    }

    private long extractTime(java.lang.String str) {
        java.lang.String firstPart;
        java.util.Calendar date;
        long time;
        java.lang.String secondPart = null;
        int index = str.indexOf("\u2013");
        if (index != -1) {
            java.lang.String secondPart2 = str.substring(index + 1).trim();
            java.lang.String firstPart2 = str.substring(0, index).trim();
            secondPart = com.navdy.hud.app.framework.contacts.ContactUtil.sanitizeString(secondPart2);
            firstPart = com.navdy.hud.app.framework.contacts.ContactUtil.sanitizeString(firstPart2);
        } else {
            firstPart = com.navdy.hud.app.framework.contacts.ContactUtil.sanitizeString(str);
        }
        synchronized (lockObj) {
            date = java.util.Calendar.getInstance();
            try {
                java.util.Calendar d = java.util.Calendar.getInstance();
                d.setTime(dateFormat1.parse(firstPart));
                date.set(10, d.get(10));
                date.set(12, d.get(12));
                date.set(13, d.get(13));
                date.set(14, 0);
                date.set(9, d.get(9));
                java.util.Date eventDate = date.getTime();
                sLogger.v("event-time-date = " + eventDate.toString());
                time = eventDate.getTime();
            } catch (Throwable th) {
            }
        }
        return time;
        java.util.Date eventDate2 = date.getTime();
        sLogger.v("event-time-date = " + eventDate2.toString());
        return eventDate2.getTime();
    }

    public void clearState() {
        this.glanceTracker.clearState();
        clearAllMessage();
    }

    public void saveState() {
        this.glanceTracker.saveState();
    }

    public void restoreState() {
        this.glanceTracker.restoreState();
    }

    public void sendSmsFailedNotification(java.lang.String number, java.lang.String message, java.lang.String name) {
        com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().addNotification(new com.navdy.hud.app.framework.message.SmsNotification(com.navdy.hud.app.framework.message.SmsNotification.Mode.Failed, java.util.UUID.randomUUID().toString(), number, message, name));
    }

    public void sendSmsSuccessNotification(java.lang.String id, java.lang.String number, java.lang.String message, java.lang.String name) {
        com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().addNotification(new com.navdy.hud.app.framework.message.SmsNotification(com.navdy.hud.app.framework.message.SmsNotification.Mode.Success, id, number, message, name));
    }

    public void addMessage(java.lang.String id, java.lang.String message) {
        if (id != null && message != null) {
            synchronized (this.messagesSent) {
                this.messagesSent.put(id, message);
            }
        }
    }

    public java.lang.String removeMessage(java.lang.String id) {
        java.lang.String str;
        if (id == null) {
            return null;
        }
        synchronized (this.messagesSent) {
            str = (java.lang.String) this.messagesSent.remove(id);
        }
        return str;
    }

    private void clearAllMessage() {
        synchronized (this.messagesSent) {
            this.messagesSent.clear();
        }
    }
}
