package com.navdy.hud.app.framework.glance;

class GlanceTracker {
    private static final long NOTIFICATION_SEEN_THRESHOLD = java.util.concurrent.TimeUnit.MINUTES.toMillis(60);
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.framework.glance.GlanceTracker.class);
    private java.util.HashMap<com.navdy.hud.app.framework.glance.GlanceApp, java.util.List<com.navdy.hud.app.framework.glance.GlanceTracker.GlanceInfo>> notificationSavedState;
    private java.util.HashMap<com.navdy.hud.app.framework.glance.GlanceApp, java.util.List<com.navdy.hud.app.framework.glance.GlanceTracker.GlanceInfo>> notificationSeen = new java.util.HashMap<>();

    private static class GlanceInfo {
        java.util.Map<java.lang.String, java.lang.String> data;
        com.navdy.service.library.events.glances.GlanceEvent event;
        long time;

        GlanceInfo(long time2, com.navdy.service.library.events.glances.GlanceEvent event2, java.util.Map<java.lang.String, java.lang.String> data2) {
            this.time = time2;
            this.event = event2;
            this.data = data2;
        }
    }

    GlanceTracker() {
    }

    /* access modifiers changed from: 0000 */
    public synchronized long isNotificationSeen(com.navdy.service.library.events.glances.GlanceEvent event, com.navdy.hud.app.framework.glance.GlanceApp app, java.util.Map<java.lang.String, java.lang.String> data) {
        long ret = 0;
        synchronized (this) {
            switch (app) {
                case GOOGLE_CALENDAR:
                case APPLE_CALENDAR:
                case GENERIC_CALENDAR:
                case GOOGLE_MAIL:
                case GOOGLE_INBOX:
                case APPLE_MAIL:
                case GENERIC_MAIL:
                    java.util.List<com.navdy.hud.app.framework.glance.GlanceTracker.GlanceInfo> list = (java.util.List) this.notificationSeen.get(app);
                    if (list != null) {
                        java.util.Iterator<com.navdy.hud.app.framework.glance.GlanceTracker.GlanceInfo> iterator = list.iterator();
                        while (true) {
                            if (!iterator.hasNext()) {
                                list.add(new com.navdy.hud.app.framework.glance.GlanceTracker.GlanceInfo(android.os.SystemClock.elapsedRealtime(), event, data));
                                sLogger.v("new notification [" + app + "]");
                                break;
                            } else {
                                com.navdy.hud.app.framework.glance.GlanceTracker.GlanceInfo glanceInfo = (com.navdy.hud.app.framework.glance.GlanceTracker.GlanceInfo) iterator.next();
                                if (android.os.SystemClock.elapsedRealtime() - glanceInfo.time > NOTIFICATION_SEEN_THRESHOLD) {
                                    iterator.remove();
                                    sLogger.v("expired notification removed [" + app + "]");
                                } else if (isNotificationSame(glanceInfo, app, data)) {
                                    ret = glanceInfo.time;
                                    glanceInfo.time = android.os.SystemClock.elapsedRealtime();
                                    sLogger.v("notification has been seen [" + app + "] time [" + ret + "]");
                                    break;
                                }
                            }
                        }
                    } else {
                        java.util.List<com.navdy.hud.app.framework.glance.GlanceTracker.GlanceInfo> list2 = new java.util.ArrayList<>();
                        list2.add(new com.navdy.hud.app.framework.glance.GlanceTracker.GlanceInfo(android.os.SystemClock.elapsedRealtime(), event, data));
                        this.notificationSeen.put(app, list2);
                        sLogger.v("first notification[" + app + "]");
                        break;
                    }
            }
        }
        return ret;
    }

    /* access modifiers changed from: 0000 */
    public synchronized void clearState() {
        sLogger.v("clearState");
        this.notificationSavedState = null;
    }

    /* access modifiers changed from: 0000 */
    public synchronized void saveState() {
        sLogger.v("saveState");
        this.notificationSavedState = this.notificationSeen;
        this.notificationSeen = new java.util.HashMap<>();
    }

    /* access modifiers changed from: 0000 */
    public synchronized void restoreState() {
        sLogger.v("restoreState");
        this.notificationSeen = this.notificationSavedState;
        this.notificationSavedState = null;
        if (this.notificationSeen == null) {
            this.notificationSeen = new java.util.HashMap<>();
        }
    }

    private boolean isNotificationSame(com.navdy.hud.app.framework.glance.GlanceTracker.GlanceInfo glanceInfo, com.navdy.hud.app.framework.glance.GlanceApp app, java.util.Map<java.lang.String, java.lang.String> data) {
        if (data.size() != glanceInfo.data.size()) {
            return false;
        }
        for (java.util.Map.Entry<java.lang.String, java.lang.String> entry : glanceInfo.data.entrySet()) {
            java.lang.String val = (java.lang.String) data.get(entry.getKey());
            if (val == null) {
                return false;
            }
            if (!android.text.TextUtils.equals(val, (java.lang.CharSequence) entry.getValue())) {
                return false;
            }
        }
        return true;
    }
}
