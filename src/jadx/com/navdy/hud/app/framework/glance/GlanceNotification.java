package com.navdy.hud.app.framework.glance;

public class GlanceNotification implements com.navdy.hud.app.framework.notifications.INotification, com.navdy.hud.app.framework.notifications.IScrollEvent, com.navdy.hud.app.framework.contacts.ContactUtil.IContactCallback {
    private static final float IMAGE_SCALE = 0.5f;
    /* access modifiers changed from: private */
    public static android.os.Handler handler = new android.os.Handler(android.os.Looper.getMainLooper());
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.framework.glance.GlanceNotification.class);
    private boolean alive;
    private int appIcon;
    /* access modifiers changed from: private */
    public android.widget.ImageView audioFeedback;
    private android.view.View bottomScrub;
    private com.squareup.otto.Bus bus;
    private com.navdy.service.library.events.audio.CancelSpeechRequest cancelSpeechRequest;
    private java.util.List<java.lang.String> cannedReplyMessages;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.ui.component.ChoiceLayout2 choiceLayout;
    private com.navdy.hud.app.ui.component.ChoiceLayout2.IListener choiceListener;
    private final int color;
    private com.navdy.hud.app.ui.component.image.ColorImageView colorImageView;
    private java.util.List<com.navdy.hud.app.framework.contacts.Contact> contacts;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.framework.notifications.INotificationController controller;
    /* access modifiers changed from: private */
    public final java.util.Map<java.lang.String, java.lang.String> data;
    /* access modifiers changed from: private */
    public final com.navdy.hud.app.framework.glance.GlanceApp glanceApp;
    private android.view.ViewGroup glanceContainer;
    private final com.navdy.service.library.events.glances.GlanceEvent glanceEvent;
    /* access modifiers changed from: private */
    public android.view.ViewGroup glanceExtendedContainer;
    /* access modifiers changed from: private */
    public boolean hasFuelLevelInfo;
    private final java.lang.String id;
    private boolean initialReplyMode;
    private com.navdy.hud.app.framework.glance.GlanceViewCache.ViewType largeType;
    private com.navdy.hud.app.ui.component.image.InitialsImageView mainImage;
    private android.widget.TextView mainTitle;
    private java.lang.String messageStr;
    /* access modifiers changed from: private */
    public java.lang.String number;
    /* access modifiers changed from: private */
    public boolean onBottom;
    /* access modifiers changed from: private */
    public boolean onTop;
    private com.navdy.hud.app.framework.glance.GlanceNotification.Mode operationMode;
    private boolean photoCheckRequired;
    /* access modifiers changed from: private */
    public final java.util.Date postTime;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.framework.notifications.IProgressUpdate progressUpdate;
    private android.view.ViewGroup replyExitView;
    private android.view.ViewGroup replyMsgView;
    private com.squareup.picasso.Transformation roundTransformation;
    private com.navdy.hud.app.view.ObservableScrollView.IScrollListener scrollListener;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.view.ObservableScrollView scrollView;
    private android.widget.ImageView sideImage;
    private com.navdy.hud.app.framework.glance.GlanceViewCache.ViewType smallType;
    private java.lang.String source;
    private java.lang.StringBuilder stringBuilder1;
    private java.lang.StringBuilder stringBuilder2;
    private android.widget.TextView subTitle;
    /* access modifiers changed from: private */
    public final boolean supportsScroll;
    private android.widget.TextView text1;
    private android.widget.TextView text2;
    private android.widget.TextView text3;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.common.TimeHelper timeHelper;
    /* access modifiers changed from: private */
    public android.view.View topScrub;
    private java.lang.String ttsMessage;
    private boolean ttsSent;
    /* access modifiers changed from: private */
    public java.lang.Runnable updateTimeRunnable;

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            if (com.navdy.hud.app.framework.glance.GlanceNotification.this.controller != null) {
                try {
                    if (com.navdy.hud.app.framework.glance.GlanceNotification.this.glanceExtendedContainer != null) {
                        android.widget.TextView title = (android.widget.TextView) com.navdy.hud.app.framework.glance.GlanceNotification.this.glanceExtendedContainer.findViewById(com.navdy.hud.app.R.id.title);
                        switch (com.navdy.hud.app.framework.glance.GlanceNotification.Anon5.$SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[com.navdy.hud.app.framework.glance.GlanceNotification.this.glanceApp.ordinal()]) {
                            case 1:
                            case 2:
                            case 3:
                                com.navdy.hud.app.framework.glance.GlanceNotification.this.setMainImage();
                                break;
                            case 8:
                                if (com.navdy.hud.app.framework.glance.GlanceNotification.this.hasFuelLevelInfo) {
                                    int fuelLevel = com.navdy.hud.app.obd.ObdManager.getInstance().getFuelLevel();
                                    if (fuelLevel > java.lang.Integer.parseInt((java.lang.String) com.navdy.hud.app.framework.glance.GlanceNotification.this.data.get(com.navdy.service.library.events.glances.FuelConstants.FUEL_LEVEL.name()))) {
                                        com.navdy.hud.app.framework.glance.GlanceNotification.handler.postDelayed(com.navdy.hud.app.framework.glance.GlanceNotification.this.updateTimeRunnable, 30000);
                                        return;
                                    }
                                    com.navdy.hud.app.framework.glance.GlanceNotification.this.data.put(com.navdy.service.library.events.glances.FuelConstants.FUEL_LEVEL.name(), java.lang.String.valueOf(fuelLevel));
                                }
                                com.navdy.hud.app.framework.fuel.FuelRoutingManager fuelRoutingManager = com.navdy.hud.app.framework.fuel.FuelRoutingManager.getInstance();
                                if (!(fuelRoutingManager == null || fuelRoutingManager.getCurrentGasStation() == null)) {
                                    com.navdy.hud.app.framework.glance.GlanceNotification.this.data.put(com.navdy.service.library.events.glances.FuelConstants.GAS_STATION_DISTANCE.name(), java.lang.String.valueOf(((double) java.lang.Math.round((fuelRoutingManager.getCurrentGasStation().getLocation().getCoordinate().distanceTo(com.navdy.hud.app.maps.here.HereMapsManager.getInstance().getLastGeoPosition().getCoordinate()) * 10.0d) / 1609.34d)) / 10.0d));
                                }
                                com.navdy.hud.app.framework.glance.GlanceNotification.this.setTitle();
                                com.navdy.hud.app.framework.glance.GlanceNotification.this.setExpandedContent(com.navdy.hud.app.framework.glance.GlanceNotification.this.glanceExtendedContainer);
                                break;
                            case 9:
                            case 10:
                            case 11:
                            case 12:
                            case 13:
                            case 14:
                            case 15:
                            case 16:
                            case 17:
                            case 18:
                            case 19:
                                title.setText(com.navdy.hud.app.framework.glance.GlanceHelper.getTimeStr(java.lang.System.currentTimeMillis(), com.navdy.hud.app.framework.glance.GlanceNotification.this.postTime, com.navdy.hud.app.framework.glance.GlanceNotification.this.timeHelper));
                                break;
                        }
                        com.navdy.hud.app.framework.glance.GlanceNotification.handler.postDelayed(com.navdy.hud.app.framework.glance.GlanceNotification.this.updateTimeRunnable, 30000);
                    }
                } catch (Throwable t) {
                    com.navdy.hud.app.framework.glance.GlanceNotification.sLogger.e(t);
                } finally {
                    com.navdy.hud.app.framework.glance.GlanceNotification.handler.postDelayed(com.navdy.hud.app.framework.glance.GlanceNotification.this.updateTimeRunnable, 30000);
                }
            }
        }
    }

    class Anon2 implements com.navdy.hud.app.ui.component.ChoiceLayout2.IListener {
        Anon2() {
        }

        public void executeItem(com.navdy.hud.app.ui.component.ChoiceLayout2.Selection selection) {
            if (com.navdy.hud.app.framework.glance.GlanceNotification.this.controller != null) {
                switch (selection.id) {
                    case 1:
                        com.navdy.hud.app.framework.glance.GlanceNotification.this.switchToReplyScreen();
                        return;
                    case 2:
                        com.navdy.hud.app.framework.glance.GlanceNotification.this.switchToMode(com.navdy.hud.app.framework.glance.GlanceNotification.Mode.READ, null);
                        return;
                    case 3:
                        com.navdy.hud.app.framework.glance.GlanceNotification.this.call();
                        return;
                    case 4:
                        com.navdy.hud.app.framework.glance.GlanceNotification.this.switchToMode(com.navdy.hud.app.framework.glance.GlanceNotification.Mode.REPLY, com.navdy.hud.app.framework.glance.GlanceNotification.this.number);
                        return;
                    case 5:
                        if (com.navdy.hud.app.framework.glance.GlanceNotification.this.choiceLayout.getTag(com.navdy.hud.app.R.id.message_secondary_screen) != null) {
                            com.navdy.hud.app.framework.glance.GlanceNotification.this.controller.collapseNotification(false, false);
                            return;
                        }
                        com.navdy.hud.app.framework.glance.GlanceNotification.this.revertChoice();
                        com.navdy.hud.app.framework.glance.GlanceNotification.this.controller.startTimeout(com.navdy.hud.app.framework.glance.GlanceNotification.this.getTimeout());
                        return;
                    case 6:
                        com.navdy.hud.app.framework.glance.GlanceNotification.this.dismissNotification();
                        return;
                    case 7:
                        com.navdy.hud.app.framework.fuel.FuelRoutingManager fuelRoutingManager = com.navdy.hud.app.framework.fuel.FuelRoutingManager.getInstance();
                        if (fuelRoutingManager != null) {
                            fuelRoutingManager.routeToGasStation();
                        }
                        com.navdy.hud.app.framework.glance.GlanceNotification.this.dismissNotification();
                        return;
                    case 8:
                        com.navdy.hud.app.framework.fuel.FuelRoutingManager fuelRoutingManager2 = com.navdy.hud.app.framework.fuel.FuelRoutingManager.getInstance();
                        if (fuelRoutingManager2 != null) {
                            fuelRoutingManager2.dismissGasRoute();
                        }
                        com.navdy.hud.app.framework.glance.GlanceNotification.this.dismissNotification();
                        return;
                    default:
                        return;
                }
            }
        }

        public void itemSelected(com.navdy.hud.app.ui.component.ChoiceLayout2.Selection selection) {
            if (com.navdy.hud.app.framework.glance.GlanceNotification.this.controller != null) {
                com.navdy.hud.app.framework.glance.GlanceNotification.this.controller.resetTimeout();
            }
        }
    }

    class Anon3 implements com.navdy.hud.app.view.ObservableScrollView.IScrollListener {
        Anon3() {
        }

        public void onTop() {
            if (com.navdy.hud.app.framework.glance.GlanceNotification.this.controller != null && com.navdy.hud.app.framework.glance.GlanceNotification.this.supportsScroll && com.navdy.hud.app.framework.glance.GlanceNotification.this.scrollView != null) {
                com.navdy.hud.app.framework.glance.GlanceNotification.this.topScrub.setVisibility(8);
                com.navdy.hud.app.framework.glance.GlanceNotification.this.onTop = true;
                com.navdy.hud.app.framework.glance.GlanceNotification.this.onBottom = false;
                if (com.navdy.hud.app.framework.glance.GlanceNotification.this.progressUpdate != null) {
                    com.navdy.hud.app.framework.glance.GlanceNotification.this.progressUpdate.onPosChange(1);
                }
            }
        }

        public void onBottom() {
            if (com.navdy.hud.app.framework.glance.GlanceNotification.this.controller != null && com.navdy.hud.app.framework.glance.GlanceNotification.this.supportsScroll && com.navdy.hud.app.framework.glance.GlanceNotification.this.scrollView != null) {
                com.navdy.hud.app.framework.glance.GlanceNotification.this.onTop = false;
                com.navdy.hud.app.framework.glance.GlanceNotification.this.onBottom = true;
                if (com.navdy.hud.app.framework.glance.GlanceNotification.this.progressUpdate != null) {
                    com.navdy.hud.app.framework.glance.GlanceNotification.this.progressUpdate.onPosChange(100);
                }
            }
        }

        public void onScroll(int l, int t, int oldl, int oldt) {
            if (com.navdy.hud.app.framework.glance.GlanceNotification.this.controller != null && com.navdy.hud.app.framework.glance.GlanceNotification.this.supportsScroll && com.navdy.hud.app.framework.glance.GlanceNotification.this.scrollView != null) {
                com.navdy.hud.app.framework.glance.GlanceNotification.this.topScrub.setVisibility(0);
                com.navdy.hud.app.framework.glance.GlanceNotification.this.onTop = false;
                com.navdy.hud.app.framework.glance.GlanceNotification.this.onBottom = false;
                if (com.navdy.hud.app.framework.glance.GlanceNotification.this.progressUpdate != null) {
                    com.navdy.hud.app.framework.glance.GlanceNotification.this.progressUpdate.onPosChange((int) ((((double) t) * 100.0d) / ((double) (com.navdy.hud.app.framework.glance.GlanceNotification.this.scrollView.getChildAt(0).getBottom() - com.navdy.hud.app.framework.glance.GlanceNotification.this.scrollView.getHeight()))));
                }
            }
        }
    }

    class Anon4 implements java.lang.Runnable {
        Anon4() {
        }

        public void run() {
            com.navdy.hud.app.framework.glance.GlanceNotification.this.audioFeedback.setVisibility(8);
            com.navdy.hud.app.framework.glance.GlanceNotification.this.audioFeedback.setAlpha(1.0f);
        }
    }

    public enum Mode {
        REPLY,
        READ
    }

    public GlanceNotification(com.navdy.service.library.events.glances.GlanceEvent event, com.navdy.hud.app.framework.glance.GlanceApp app, java.util.Map<java.lang.String, java.lang.String> data2) {
        this(event, app, null, data2);
    }

    public GlanceNotification(com.navdy.service.library.events.glances.GlanceEvent event, com.navdy.hud.app.framework.glance.GlanceApp app, com.navdy.hud.app.framework.glance.GlanceViewCache.ViewType largeType2, java.util.Map<java.lang.String, java.lang.String> eventData) {
        this.stringBuilder1 = new java.lang.StringBuilder();
        this.stringBuilder2 = new java.lang.StringBuilder();
        this.roundTransformation = new com.makeramen.RoundedTransformationBuilder().oval(true).build();
        this.alive = true;
        this.updateTimeRunnable = new com.navdy.hud.app.framework.glance.GlanceNotification.Anon1();
        this.choiceListener = new com.navdy.hud.app.framework.glance.GlanceNotification.Anon2();
        this.scrollListener = new com.navdy.hud.app.framework.glance.GlanceNotification.Anon3();
        this.glanceEvent = event;
        this.glanceApp = app;
        this.id = com.navdy.hud.app.framework.glance.GlanceHelper.getNotificationId(this.glanceEvent);
        this.color = this.glanceApp.getColor();
        this.appIcon = this.glanceApp.getSideIcon();
        if (eventData == null) {
            this.data = com.navdy.hud.app.framework.glance.GlanceHelper.buildDataMap(event);
        } else {
            this.data = eventData;
        }
        switch (app) {
            case FUEL:
                this.hasFuelLevelInfo = this.data.containsKey(com.navdy.service.library.events.glances.FuelConstants.FUEL_LEVEL);
                break;
            case GENERIC:
                java.lang.String iconStr = (java.lang.String) this.data.get(com.navdy.service.library.events.glances.GenericConstants.GENERIC_SIDE_ICON.name());
                if (!android.text.TextUtils.isEmpty(iconStr)) {
                    int nIcon = com.navdy.hud.app.framework.glance.GlanceHelper.getIcon(iconStr);
                    if (nIcon != -1) {
                        this.appIcon = nIcon;
                        break;
                    }
                }
                break;
        }
        this.photoCheckRequired = com.navdy.hud.app.framework.glance.GlanceHelper.isPhotoCheckRequired(this.glanceApp);
        this.postTime = new java.util.Date(event.postTime.longValue());
        this.timeHelper = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getTimeHelper();
        this.smallType = com.navdy.hud.app.framework.glance.GlanceHelper.getSmallViewType(this.glanceApp);
        if (largeType2 == null) {
            this.largeType = com.navdy.hud.app.framework.glance.GlanceHelper.getLargeViewType(this.glanceApp);
        } else {
            this.largeType = largeType2;
        }
        this.ttsMessage = com.navdy.hud.app.framework.glance.GlanceHelper.getTtsMessage(this.glanceApp, this.data, this.stringBuilder1, this.stringBuilder2, this.timeHelper);
        this.messageStr = com.navdy.hud.app.framework.glance.GlanceHelper.getGlanceMessage(this.glanceApp, this.data);
        this.cancelSpeechRequest = new com.navdy.service.library.events.audio.CancelSpeechRequest(this.id);
        this.supportsScroll = com.navdy.hud.app.framework.glance.GlanceViewCache.supportScroll(this.largeType);
        this.bus = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getBus();
    }

    public com.navdy.hud.app.framework.notifications.NotificationType getType() {
        return this.glanceApp.notificationType;
    }

    public java.lang.String getId() {
        return this.id;
    }

    public com.navdy.hud.app.framework.glance.GlanceApp getGlanceApp() {
        return this.glanceApp;
    }

    public android.view.View getView(android.content.Context context) {
        this.glanceContainer = (android.view.ViewGroup) com.navdy.hud.app.framework.glance.GlanceViewCache.getView(this.smallType, context);
        this.mainTitle = (android.widget.TextView) this.glanceContainer.findViewById(com.navdy.hud.app.R.id.mainTitle);
        this.subTitle = (android.widget.TextView) this.glanceContainer.findViewById(com.navdy.hud.app.R.id.subTitle);
        this.sideImage = (android.widget.ImageView) this.glanceContainer.findViewById(com.navdy.hud.app.R.id.sideImage);
        this.choiceLayout = (com.navdy.hud.app.ui.component.ChoiceLayout2) this.glanceContainer.findViewById(com.navdy.hud.app.R.id.choiceLayout);
        this.audioFeedback = (android.widget.ImageView) this.glanceContainer.findViewById(com.navdy.hud.app.R.id.audioFeedback);
        if (com.navdy.hud.app.framework.glance.GlanceHelper.isCalendarApp(this.glanceApp)) {
            this.colorImageView = (com.navdy.hud.app.ui.component.image.ColorImageView) this.glanceContainer.findViewById(com.navdy.hud.app.R.id.mainImage);
            this.text1 = (android.widget.TextView) this.glanceContainer.findViewById(com.navdy.hud.app.R.id.text1);
            this.text2 = (android.widget.TextView) this.glanceContainer.findViewById(com.navdy.hud.app.R.id.text2);
            this.text3 = (android.widget.TextView) this.glanceContainer.findViewById(com.navdy.hud.app.R.id.text3);
        } else {
            this.mainImage = (com.navdy.hud.app.ui.component.image.InitialsImageView) this.glanceContainer.findViewById(com.navdy.hud.app.R.id.mainImage);
        }
        return this.glanceContainer;
    }

    public android.view.View getExpandedView(android.content.Context context, java.lang.Object data2) {
        int current;
        if (this.operationMode == com.navdy.hud.app.framework.glance.GlanceNotification.Mode.READ || this.operationMode == null) {
            this.glanceExtendedContainer = (android.view.ViewGroup) com.navdy.hud.app.framework.glance.GlanceViewCache.getView(this.largeType, context);
            if (this.supportsScroll) {
                this.topScrub = this.glanceExtendedContainer.findViewById(com.navdy.hud.app.R.id.topScrub);
                this.topScrub.setVisibility(8);
                this.bottomScrub = this.glanceExtendedContainer.findViewById(com.navdy.hud.app.R.id.bottomScrub);
                this.bottomScrub.setVisibility(0);
                this.scrollView = (com.navdy.hud.app.view.ObservableScrollView) this.glanceExtendedContainer.findViewById(com.navdy.hud.app.R.id.scrollView);
                this.scrollView.setScrollListener(this.scrollListener);
            }
            setExpandedContent(this.glanceExtendedContainer);
            return this.glanceExtendedContainer;
        }
        com.navdy.hud.app.framework.notifications.NotificationManager notificationManager = com.navdy.hud.app.framework.notifications.NotificationManager.getInstance();
        if (this.initialReplyMode) {
            this.initialReplyMode = false;
            current = 1;
        } else {
            current = ((java.lang.Integer) data2).intValue();
        }
        android.view.View view = notificationManager.getExpandedViewChild();
        if (view != null) {
            removeParent(view);
        }
        return getReplyView(current, context);
    }

    public int getExpandedViewIndicatorColor() {
        return com.navdy.hud.app.framework.glance.GlanceConstants.colorWhite;
    }

    public void onStart(com.navdy.hud.app.framework.notifications.INotificationController controller2) {
        sLogger.v("start called:" + java.lang.System.identityHashCode(this) + " expanded:" + controller2.isExpandedWithStack());
        this.bus.register(this);
        this.controller = controller2;
        if (this.choiceLayout != null) {
            this.choiceLayout.setTag(null);
            this.choiceLayout.setTag(com.navdy.hud.app.R.id.message_prev_choice, null);
            this.choiceLayout.setTag(com.navdy.hud.app.R.id.message_secondary_screen, null);
        }
        handler.postDelayed(this.updateTimeRunnable, 30000);
        updateState();
    }

    public void onUpdate() {
    }

    public void onStop() {
        sLogger.v("stop called:" + java.lang.System.identityHashCode(this));
        handler.removeCallbacks(this.updateTimeRunnable);
        this.bus.unregister(this);
        cancelTts();
        if (this.choiceLayout != null) {
            this.choiceLayout.clear();
        }
        if (this.supportsScroll && this.scrollView != null) {
            this.scrollView.setScrollListener(null);
        }
        this.controller = null;
        if (this.glanceContainer != null) {
            cleanupView(this.smallType, this.glanceContainer);
            this.glanceContainer = null;
        }
        if (this.glanceExtendedContainer != null) {
            cleanupView(this.largeType, this.glanceExtendedContainer);
            this.glanceExtendedContainer = null;
        }
        removeParent(this.replyMsgView);
        this.replyMsgView = null;
        removeParent(this.replyExitView);
        this.replyExitView = null;
        this.operationMode = null;
    }

    public int getTimeout() {
        return com.navdy.hud.app.framework.glance.GlanceConstants.DEFAULT_TIMEOUT;
    }

    public boolean isAlive() {
        return this.alive;
    }

    public boolean isPurgeable() {
        return false;
    }

    public boolean canAddToStackIfCurrentExists() {
        return true;
    }

    public int getColor() {
        return this.color;
    }

    public void onNotificationEvent(com.navdy.hud.app.ui.framework.UIStateManager.Mode mode) {
    }

    public void onExpandedNotificationEvent(com.navdy.hud.app.ui.framework.UIStateManager.Mode mode) {
        if (mode == com.navdy.hud.app.ui.framework.UIStateManager.Mode.COLLAPSE) {
            cancelTts();
            this.operationMode = null;
            if (!(this.choiceLayout.getTag(com.navdy.hud.app.R.id.message_secondary_screen) == null || this.controller == null)) {
                revertChoice();
                this.controller.startTimeout(getTimeout());
            }
            removeParent(this.replyMsgView);
            this.replyMsgView = null;
            removeParent(this.replyExitView);
            this.replyExitView = null;
            this.initialReplyMode = false;
            switch (this.glanceApp) {
                case GOOGLE_MAIL:
                case APPLE_MAIL:
                case GENERIC_MAIL:
                case GOOGLE_INBOX:
                    this.subTitle.setVisibility(0);
                    return;
                default:
                    return;
            }
        } else if (this.operationMode == com.navdy.hud.app.framework.glance.GlanceNotification.Mode.READ || this.operationMode == null) {
            sendTts();
        } else {
            com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().updateExpandedIndicator(getCannedReplyMessages().size() + 1, 1, com.navdy.hud.app.framework.glance.GlanceConstants.colorWhite);
        }
    }

    public void onExpandedNotificationSwitched() {
        if (this.controller != null && this.controller.isExpandedWithStack()) {
            sendTts();
        }
    }

    public android.animation.AnimatorSet getViewSwitchAnimation(boolean viewIn) {
        android.animation.AnimatorSet set = new android.animation.AnimatorSet();
        if (!viewIn) {
            set.playTogether(new android.animation.Animator[]{android.animation.ObjectAnimator.ofFloat(this.mainTitle, android.view.View.ALPHA, new float[]{1.0f, 0.0f}), android.animation.ObjectAnimator.ofFloat(this.subTitle, android.view.View.ALPHA, new float[]{1.0f, 0.0f}), android.animation.ObjectAnimator.ofFloat(this.choiceLayout, android.view.View.ALPHA, new float[]{1.0f, 0.0f})});
        } else {
            set.playTogether(new android.animation.Animator[]{android.animation.ObjectAnimator.ofFloat(this.mainTitle, android.view.View.ALPHA, new float[]{0.0f, 1.0f}), android.animation.ObjectAnimator.ofFloat(this.subTitle, android.view.View.ALPHA, new float[]{0.0f, 1.0f}), android.animation.ObjectAnimator.ofFloat(this.choiceLayout, android.view.View.ALPHA, new float[]{0.0f, 1.0f})});
        }
        return set;
    }

    private void updateState() {
        if (com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getRemoteDeviceInfo() == null) {
            dismissNotification();
            return;
        }
        setTitle();
        setSubTitle();
        setSideImage();
        setMainImage();
        setChoices();
        if (this.controller.isExpandedWithStack()) {
            this.mainTitle.setAlpha(0.0f);
            this.subTitle.setAlpha(0.0f);
            this.choiceLayout.setAlpha(0.0f);
        }
    }

    /* access modifiers changed from: private */
    public void dismissNotification() {
        com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().removeNotification(this.id);
    }

    @com.squareup.otto.Subscribe
    public void onPhotoDownload(com.navdy.hud.app.framework.contacts.PhoneImageDownloader.PhotoDownloadStatus status) {
        if (this.controller != null && this.photoCheckRequired && status.photoType == com.navdy.service.library.events.photo.PhotoType.PHOTO_CONTACT) {
            if (!android.text.TextUtils.equals(status.sourceIdentifier, this.source) && (this.number == null || !android.text.TextUtils.equals(status.sourceIdentifier, this.number))) {
                return;
            }
            if (!status.alreadyDownloaded || this.mainImage.getTag() == null) {
                sLogger.v("photo available");
                com.navdy.hud.app.framework.contacts.ContactUtil.setContactPhoto(this.source, this.number, false, this.mainImage, this.roundTransformation, this);
            }
        }
    }

    @com.squareup.otto.Subscribe
    public void onContactFound(com.navdy.hud.app.framework.recentcall.RecentCallManager.ContactFound event) {
        if (this.controller != null) {
            java.lang.Object tag = this.choiceLayout.getTag();
            if (tag != null && tag == com.navdy.hud.app.framework.glance.GlanceConstants.noNumberandNoReplyBack && event.contact != null && android.text.TextUtils.equals(event.identifier, this.source)) {
                for (com.navdy.service.library.events.contacts.Contact c : event.contact) {
                    if (this.contacts == null) {
                        this.contacts = new java.util.ArrayList();
                    }
                    com.navdy.hud.app.framework.contacts.Contact obj = new com.navdy.hud.app.framework.contacts.Contact(c);
                    obj.setName(this.source);
                    this.contacts.add(obj);
                    if (event.contact.size() == 1) {
                        this.number = c.number;
                    }
                }
                sLogger.v("contact info found:" + event.identifier + " number:" + this.number + " size:" + event.contact.size());
                setSubTitle();
                if (!hasReplyAction()) {
                    this.choiceLayout.setChoices(com.navdy.hud.app.framework.glance.GlanceConstants.numberAndNoReplyBack, 1, this.choiceListener, 0.5f);
                    this.choiceLayout.setTag(com.navdy.hud.app.framework.glance.GlanceConstants.numberAndNoReplyBack);
                } else {
                    this.choiceLayout.setChoices(com.navdy.hud.app.framework.glance.GlanceConstants.numberAndReplyBack_1, 1, this.choiceListener, 0.5f);
                    this.choiceLayout.setTag(com.navdy.hud.app.framework.glance.GlanceConstants.numberAndReplyBack_1);
                }
                if (this.contacts == null || this.contacts.size() <= 1) {
                    com.navdy.hud.app.framework.contacts.ContactUtil.setContactPhoto(this.source, event.identifier, false, this.mainImage, this.roundTransformation, this);
                    return;
                }
                com.navdy.hud.app.framework.contacts.ContactImageHelper contactImageHelper = com.navdy.hud.app.framework.contacts.ContactImageHelper.getInstance();
                this.mainImage.setImage(contactImageHelper.getResourceId(contactImageHelper.getContactImageIndex(this.source)), com.navdy.hud.app.framework.contacts.ContactUtil.getInitials(this.source), com.navdy.hud.app.ui.component.image.InitialsImageView.Style.LARGE);
            }
        }
    }

    public void onClick() {
    }

    public void onTrackHand(float normalized) {
    }

    public boolean onGesture(com.navdy.service.library.events.input.GestureEvent event) {
        return false;
    }

    public boolean onKey(com.navdy.hud.app.manager.InputManager.CustomKeyEvent event) {
        if (this.controller == null) {
            return true;
        }
        if (this.supportsScroll && this.scrollView != null && this.controller.isExpanded()) {
            switch (event) {
                case LEFT:
                    boolean handled = this.scrollView.arrowScroll(33);
                    if (this.onTop) {
                        return false;
                    }
                    return handled;
                case RIGHT:
                    boolean handled2 = this.scrollView.arrowScroll(130);
                    if (this.onBottom) {
                        return false;
                    }
                    return handled2;
            }
        }
        if (this.operationMode == com.navdy.hud.app.framework.glance.GlanceNotification.Mode.REPLY) {
            int current = com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().getExpandedIndicatorCurrentItem();
            switch (event) {
                case LEFT:
                    return false;
                case RIGHT:
                    return false;
                case SELECT:
                    if (current == 0) {
                        this.controller.collapseNotification(false, false);
                    } else {
                        reply((java.lang.String) getCannedReplyMessages().get(current - 1), this.number, this.source);
                    }
                    return true;
                default:
                    return true;
            }
        } else {
            if (0 == 0) {
                switch (event) {
                    case LEFT:
                        this.choiceLayout.moveSelectionLeft();
                        return true;
                    case RIGHT:
                        this.choiceLayout.moveSelectionRight();
                        return true;
                    case SELECT:
                        com.navdy.hud.app.ui.component.ChoiceLayout2.Choice choice = this.choiceLayout.getCurrentSelectedChoice();
                        if (choice != null) {
                            switch (choice.getId()) {
                                case 2:
                                    com.navdy.hud.app.analytics.AnalyticsSupport.recordGlanceAction(com.navdy.hud.app.analytics.AnalyticsSupport.ANALYTICS_EVENT_GLANCE_OPEN_FULL, this, "dial");
                                    break;
                                case 5:
                                    com.navdy.hud.app.analytics.AnalyticsSupport.recordGlanceAction(com.navdy.hud.app.analytics.AnalyticsSupport.ANALYTICS_EVENT_GLANCE_OPEN_MINI, this, "dial");
                                    break;
                                case 6:
                                    com.navdy.hud.app.analytics.AnalyticsSupport.recordGlanceAction(com.navdy.hud.app.analytics.AnalyticsSupport.ANALYTICS_EVENT_GLANCE_DISMISS, this, "dial");
                                    break;
                            }
                        }
                        this.choiceLayout.executeSelectedItem();
                        return true;
                }
            }
            return false;
        }
    }

    public com.navdy.hud.app.manager.InputManager.IInputHandler nextHandler() {
        return null;
    }

    public boolean isContextValid() {
        return this.controller != null;
    }

    /* access modifiers changed from: private */
    public void revertChoice() {
        java.util.List<com.navdy.hud.app.ui.component.ChoiceLayout2.Choice> choices = null;
        int defaultSelection = 0;
        if (this.choiceLayout.getTag(com.navdy.hud.app.R.id.message_secondary_screen) != null) {
            java.lang.Object tag = this.choiceLayout.getTag(com.navdy.hud.app.R.id.message_prev_choice);
            this.choiceLayout.setTag(com.navdy.hud.app.R.id.message_prev_choice, null);
            this.choiceLayout.setTag(com.navdy.hud.app.R.id.message_secondary_screen, null);
            if (tag == com.navdy.hud.app.framework.glance.GlanceConstants.numberAndReplyBack_1) {
                choices = com.navdy.hud.app.framework.glance.GlanceConstants.numberAndReplyBack_1;
                defaultSelection = 1;
            } else if (tag == com.navdy.hud.app.framework.glance.GlanceConstants.numberAndNoReplyBack) {
                choices = com.navdy.hud.app.framework.glance.GlanceConstants.numberAndNoReplyBack;
                defaultSelection = 1;
            } else if (tag == com.navdy.hud.app.framework.glance.GlanceConstants.noMessage) {
                choices = com.navdy.hud.app.framework.glance.GlanceConstants.noMessage;
            } else if (tag == com.navdy.hud.app.framework.glance.GlanceConstants.noNumberandNoReplyBack) {
                choices = com.navdy.hud.app.framework.glance.GlanceConstants.noNumberandNoReplyBack;
            } else {
                this.choiceLayout.setTag(null);
                setChoices();
                return;
            }
        }
        this.choiceLayout.setAlpha(1.0f);
        this.choiceLayout.setVisibility(0);
        this.choiceLayout.setChoices(choices, defaultSelection, this.choiceListener, 0.5f);
        this.choiceLayout.setTag(choices);
    }

    /* access modifiers changed from: private */
    public void setTitle() {
        this.source = com.navdy.hud.app.framework.glance.GlanceHelper.getTitle(this.glanceApp, this.data);
        if (this.number == null) {
            this.number = com.navdy.hud.app.framework.glance.GlanceHelper.getNumber(this.glanceApp, this.data);
        }
        boolean isNumber = false;
        if (this.number != null && android.text.TextUtils.equals(this.source, this.number)) {
            isNumber = true;
        }
        if (isNumber) {
            this.mainTitle.setText(com.navdy.hud.app.util.PhoneUtil.formatPhoneNumber(this.source));
        } else {
            this.mainTitle.setText(this.source);
        }
    }

    private void setSubTitle() {
        java.lang.String text = com.navdy.hud.app.framework.glance.GlanceHelper.getSubTitle(this.glanceApp, this.data);
        if (android.text.TextUtils.isEmpty(text) && this.number != null && !android.text.TextUtils.equals(this.number, this.source)) {
            text = com.navdy.hud.app.util.PhoneUtil.formatPhoneNumber(this.number);
        }
        this.subTitle.setText(text);
    }

    private void setSideImage() {
        this.sideImage.setImageResource(this.appIcon);
    }

    /* access modifiers changed from: private */
    public void setMainImage() {
        int margin;
        if (com.navdy.hud.app.framework.glance.GlanceHelper.isCalendarApp(this.glanceApp)) {
            this.colorImageView.setColor(this.glanceApp.getColor());
            this.text1.setText(com.navdy.hud.app.framework.glance.GlanceConstants.starts);
            long millis = 0;
            java.lang.String str = (java.lang.String) this.data.get(com.navdy.service.library.events.glances.CalendarConstants.CALENDAR_TIME.name());
            if (str != null) {
                try {
                    millis = java.lang.Long.parseLong(str);
                } catch (Throwable t) {
                    sLogger.e(t);
                }
            }
            if (millis > 0) {
                java.lang.String data2 = com.navdy.hud.app.framework.glance.GlanceHelper.getCalendarTime(millis, this.stringBuilder1, this.stringBuilder2, this.timeHelper);
                java.lang.String str2 = this.stringBuilder1.toString();
                if (android.text.TextUtils.isEmpty(str2)) {
                    java.lang.String pmMarker = this.stringBuilder2.toString();
                    if (!android.text.TextUtils.isEmpty(pmMarker)) {
                        margin = com.navdy.hud.app.framework.glance.GlanceConstants.calendarTimeMargin;
                        this.text3.setText("");
                        this.text2.setTextAppearance(com.navdy.hud.app.HudApplication.getAppContext(), com.navdy.hud.app.R.style.small_glance_sign_text_3);
                        android.text.SpannableStringBuilder spannable = new android.text.SpannableStringBuilder();
                        spannable.append(data2);
                        int len = spannable.length();
                        spannable.append(pmMarker);
                        spannable.setSpan(new android.text.style.AbsoluteSizeSpan(com.navdy.hud.app.framework.glance.GlanceConstants.calendarpmMarker), len, spannable.length(), 33);
                        this.text2.setText(spannable);
                    } else {
                        margin = com.navdy.hud.app.framework.glance.GlanceConstants.calendarNowMargin;
                        this.text2.setTextAppearance(com.navdy.hud.app.HudApplication.getAppContext(), com.navdy.hud.app.R.style.small_glance_sign_text_2);
                        this.text2.setText(data2);
                        this.text3.setText("");
                    }
                } else {
                    margin = com.navdy.hud.app.framework.glance.GlanceConstants.calendarNormalMargin;
                    this.text2.setTextAppearance(com.navdy.hud.app.HudApplication.getAppContext(), com.navdy.hud.app.R.style.small_glance_sign_text_1);
                    this.text2.setText(data2);
                    this.text3.setText(str2);
                }
                ((android.view.ViewGroup.MarginLayoutParams) this.text2.getLayoutParams()).topMargin = margin;
            } else {
                this.text1.setText("");
                this.text2.setTextAppearance(com.navdy.hud.app.HudApplication.getAppContext(), com.navdy.hud.app.R.style.small_glance_sign_text_2);
                this.text2.setText(com.navdy.hud.app.framework.glance.GlanceConstants.questionMark);
                this.text3.setText("");
                ((android.view.ViewGroup.MarginLayoutParams) this.text2.getLayoutParams()).topMargin = com.navdy.hud.app.framework.glance.GlanceConstants.calendarNowMargin;
            }
            this.ttsMessage = com.navdy.hud.app.framework.glance.GlanceHelper.getTtsMessage(this.glanceApp, this.data, this.stringBuilder1, this.stringBuilder2, this.timeHelper);
            this.stringBuilder1.setLength(0);
            this.stringBuilder2.setLength(0);
            return;
        }
        int image = this.glanceApp.getMainIcon();
        if (this.glanceApp == com.navdy.hud.app.framework.glance.GlanceApp.GENERIC) {
            java.lang.String iconStr = (java.lang.String) this.data.get(com.navdy.service.library.events.glances.GenericConstants.GENERIC_MAIN_ICON.name());
            if (!android.text.TextUtils.isEmpty(iconStr)) {
                int nIcon = com.navdy.hud.app.framework.glance.GlanceHelper.getIcon(iconStr);
                if (nIcon != -1) {
                    image = nIcon;
                }
            }
        }
        if (image != -1) {
            this.mainImage.setImage(image, null, com.navdy.hud.app.ui.component.image.InitialsImageView.Style.DEFAULT);
        } else if (this.photoCheckRequired) {
            if (this.contacts == null || this.contacts.size() <= 1) {
                com.navdy.hud.app.framework.contacts.ContactUtil.setContactPhoto(this.source, this.number, this.photoCheckRequired, this.mainImage, this.roundTransformation, this);
            } else {
                com.navdy.hud.app.framework.contacts.ContactImageHelper contactImageHelper = com.navdy.hud.app.framework.contacts.ContactImageHelper.getInstance();
                this.mainImage.setImage(contactImageHelper.getResourceId(contactImageHelper.getContactImageIndex(this.source)), com.navdy.hud.app.framework.contacts.ContactUtil.getInitials(this.source), com.navdy.hud.app.ui.component.image.InitialsImageView.Style.LARGE);
            }
            this.mainImage.requestLayout();
        } else if (this.glanceApp.isDefaultIconBasedOnId()) {
            com.navdy.hud.app.framework.contacts.ContactImageHelper contactImageHelper2 = com.navdy.hud.app.framework.contacts.ContactImageHelper.getInstance();
            this.mainImage.setImage(contactImageHelper2.getResourceId(contactImageHelper2.getContactImageIndex(this.source)), com.navdy.hud.app.framework.contacts.ContactUtil.getInitials(this.source), com.navdy.hud.app.ui.component.image.InitialsImageView.Style.LARGE);
        }
    }

    private void setChoices() {
        if (this.controller.isExpandedWithStack()) {
            switchToMode(com.navdy.hud.app.framework.glance.GlanceNotification.Mode.READ, null);
            return;
        }
        java.util.List<com.navdy.hud.app.ui.component.ChoiceLayout2.Choice> choices = null;
        int defaultSelection = 0;
        boolean replyAction = false;
        switch (this.glanceApp) {
            case FUEL:
                if (this.data.get(com.navdy.service.library.events.glances.FuelConstants.NO_ROUTE.name()) == null) {
                    choices = com.navdy.hud.app.framework.glance.GlanceConstants.fuelChoices;
                } else {
                    choices = com.navdy.hud.app.framework.glance.GlanceConstants.fuelChoicesNoRoute;
                }
                defaultSelection = 0;
                break;
            default:
                if (this.choiceLayout.getTag() == null) {
                    if (hasReplyAction()) {
                        replyAction = true;
                    }
                    if (this.number != null || this.contacts != null) {
                        if (!replyAction) {
                            choices = com.navdy.hud.app.framework.glance.GlanceConstants.numberAndNoReplyBack;
                            defaultSelection = 1;
                            break;
                        } else {
                            choices = com.navdy.hud.app.framework.glance.GlanceConstants.numberAndReplyBack_1;
                            defaultSelection = 1;
                            break;
                        }
                    } else if (!com.navdy.hud.app.framework.glance.GlanceHelper.isCalendarApp(this.glanceApp)) {
                        choices = com.navdy.hud.app.framework.glance.GlanceConstants.noNumberandNoReplyBack;
                        break;
                    } else {
                        choices = com.navdy.hud.app.framework.glance.GlanceConstants.calendarOptions;
                        break;
                    }
                }
                break;
        }
        this.choiceLayout.setAlpha(1.0f);
        this.choiceLayout.setVisibility(0);
        this.choiceLayout.setChoices(choices, defaultSelection, this.choiceListener, 0.5f);
        this.choiceLayout.setTag(choices);
    }

    /* access modifiers changed from: private */
    public void call() {
        dismissNotification();
        com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getCallManager().dial(this.number, null, this.source);
    }

    /* access modifiers changed from: private */
    public void switchToMode(com.navdy.hud.app.framework.glance.GlanceNotification.Mode mode, java.lang.String number2) {
        if (this.controller != null) {
            this.controller.stopTimeout(true);
            this.choiceLayout.setTag(com.navdy.hud.app.R.id.message_prev_choice, this.choiceLayout.getTag());
            this.choiceLayout.setTag(com.navdy.hud.app.R.id.message_secondary_screen, java.lang.Integer.valueOf(1));
            if (mode == com.navdy.hud.app.framework.glance.GlanceNotification.Mode.READ) {
                this.operationMode = mode;
                this.choiceLayout.setChoices(com.navdy.hud.app.framework.glance.GlanceConstants.backChoice2, 0, this.choiceListener, 0.5f);
                this.choiceLayout.setTag(com.navdy.hud.app.framework.glance.GlanceConstants.backChoice);
                if (!this.controller.isExpandedWithStack()) {
                    this.controller.expandNotification(true);
                    return;
                }
                return;
            }
            this.operationMode = mode;
            this.choiceLayout.setChoices(null, 0, null, 0.5f);
            this.choiceLayout.setTag(com.navdy.hud.app.framework.glance.GlanceConstants.backChoice);
            if (!this.controller.isExpandedWithStack()) {
                this.initialReplyMode = true;
                this.controller.expandNotification(false);
            }
        }
    }

    private void reply(java.lang.String message, java.lang.String number2, java.lang.String caller) {
        this.alive = false;
        com.navdy.hud.app.framework.glance.GlanceHandler glanceHandler = com.navdy.hud.app.framework.glance.GlanceHandler.getInstance();
        if (glanceHandler.sendMessage(number2, message, caller)) {
            glanceHandler.sendSmsSuccessNotification(this.id, number2, message, caller);
        } else {
            glanceHandler.sendSmsFailedNotification(number2, message, caller);
        }
    }

    public boolean isShowingReadOption() {
        if (this.choiceLayout != null) {
            java.lang.Object tag = this.choiceLayout.getTag();
            if (tag == com.navdy.hud.app.framework.glance.GlanceConstants.noNumberandNoReplyBack || tag == com.navdy.hud.app.framework.glance.GlanceConstants.numberAndNoReplyBack || tag == com.navdy.hud.app.framework.glance.GlanceConstants.numberAndReplyBack_1) {
                return true;
            }
        }
        return false;
    }

    public boolean expandNotification() {
        if (this.choiceLayout.getTag(com.navdy.hud.app.R.id.message_secondary_screen) != null) {
            return false;
        }
        switchToMode(com.navdy.hud.app.framework.glance.GlanceNotification.Mode.READ, null);
        return true;
    }

    public boolean supportScroll() {
        return this.supportsScroll;
    }

    /* access modifiers changed from: private */
    public void setExpandedContent(android.view.View view) {
        if (this.glanceExtendedContainer != null) {
            switch (this.glanceApp) {
                case GOOGLE_CALENDAR:
                case APPLE_CALENDAR:
                case GENERIC_CALENDAR:
                    android.widget.TextView textview = (android.widget.TextView) view.findViewById(com.navdy.hud.app.R.id.text1);
                    java.lang.String s = (java.lang.String) this.data.get(com.navdy.service.library.events.glances.CalendarConstants.CALENDAR_TIME_STR.name());
                    if (!android.text.TextUtils.isEmpty(s)) {
                        textview.setText(s);
                        textview.setVisibility(0);
                    } else {
                        textview.setVisibility(8);
                    }
                    android.widget.TextView textview2 = (android.widget.TextView) view.findViewById(com.navdy.hud.app.R.id.text2);
                    java.lang.String s2 = (java.lang.String) this.data.get(com.navdy.service.library.events.glances.CalendarConstants.CALENDAR_TITLE.name());
                    if (!android.text.TextUtils.isEmpty(s2)) {
                        textview2.setText(s2);
                        textview2.setVisibility(0);
                    } else {
                        textview2.setVisibility(8);
                    }
                    android.view.ViewGroup viewGroup = (android.view.ViewGroup) view.findViewById(com.navdy.hud.app.R.id.locationContainer);
                    java.lang.String s3 = (java.lang.String) this.data.get(com.navdy.service.library.events.glances.CalendarConstants.CALENDAR_LOCATION.name());
                    if (!android.text.TextUtils.isEmpty(s3)) {
                        ((android.widget.TextView) view.findViewById(com.navdy.hud.app.R.id.text3)).setText(s3);
                        viewGroup.setVisibility(0);
                        return;
                    }
                    viewGroup.setVisibility(8);
                    return;
                case GOOGLE_MAIL:
                case APPLE_MAIL:
                case GENERIC_MAIL:
                case GOOGLE_INBOX:
                case GOOGLE_HANGOUT:
                case SLACK:
                case WHATS_APP:
                case FACEBOOK_MESSENGER:
                case FACEBOOK:
                case GENERIC_MESSAGE:
                case TWITTER:
                case GENERIC_SOCIAL:
                case IMESSAGE:
                case SMS:
                case GENERIC:
                    android.widget.TextView title = (android.widget.TextView) view.findViewById(com.navdy.hud.app.R.id.title);
                    android.widget.TextView message = (android.widget.TextView) view.findViewById(com.navdy.hud.app.R.id.message);
                    switch (this.glanceApp) {
                        case GOOGLE_MAIL:
                        case APPLE_MAIL:
                        case GENERIC_MAIL:
                        case GOOGLE_INBOX:
                            title.setTextColor(com.navdy.hud.app.framework.glance.GlanceConstants.colorWhite);
                            this.subTitle.setVisibility(4);
                            java.lang.String subject = (java.lang.String) this.data.get(com.navdy.service.library.events.glances.EmailConstants.EMAIL_SUBJECT.name());
                            if (subject == null) {
                                subject = "";
                            }
                            title.setText(subject);
                            break;
                        default:
                            title.setText(com.navdy.hud.app.framework.glance.GlanceHelper.getTimeStr(java.lang.System.currentTimeMillis(), this.postTime, this.timeHelper));
                            break;
                    }
                    message.setText(this.messageStr);
                    return;
                case FUEL:
                    android.widget.TextView textView1 = (android.widget.TextView) view.findViewById(com.navdy.hud.app.R.id.title1);
                    textView1.setTextAppearance(view.getContext(), com.navdy.hud.app.R.style.glance_title_1);
                    android.widget.TextView textView2 = (android.widget.TextView) view.findViewById(com.navdy.hud.app.R.id.title2);
                    textView2.setTextAppearance(view.getContext(), com.navdy.hud.app.R.style.glance_title_2);
                    textView1.setText((java.lang.CharSequence) this.data.get(com.navdy.service.library.events.glances.FuelConstants.GAS_STATION_NAME.name()));
                    if (this.data.get(com.navdy.service.library.events.glances.FuelConstants.NO_ROUTE.name()) == null) {
                        textView2.setText(view.getContext().getResources().getString(com.navdy.hud.app.R.string.gas_station_detail, new java.lang.Object[]{this.data.get(com.navdy.service.library.events.glances.FuelConstants.GAS_STATION_ADDRESS.name()), this.data.get(com.navdy.service.library.events.glances.FuelConstants.GAS_STATION_DISTANCE.name())}));
                    } else {
                        textView2.setText(view.getContext().getResources().getString(com.navdy.hud.app.R.string.i_cannot_add_gasstation));
                    }
                    textView1.setVisibility(0);
                    textView2.setVisibility(0);
                    return;
                default:
                    return;
            }
        }
    }

    private void cleanupView(com.navdy.hud.app.framework.glance.GlanceViewCache.ViewType viewType, android.view.ViewGroup viewGroup) {
        android.view.ViewGroup parent = (android.view.ViewGroup) viewGroup.getParent();
        if (parent != null) {
            parent.removeView(viewGroup);
        }
        switch (viewType) {
            case SMALL_GLANCE_MESSAGE:
                this.mainTitle.setAlpha(1.0f);
                this.mainTitle.setVisibility(0);
                this.subTitle.setAlpha(1.0f);
                this.subTitle.setVisibility(0);
                this.choiceLayout.setAlpha(1.0f);
                this.choiceLayout.setVisibility(0);
                this.choiceLayout.setTag(null);
                this.choiceLayout.setTag(com.navdy.hud.app.R.id.message_prev_choice, null);
                this.choiceLayout.setTag(com.navdy.hud.app.R.id.message_secondary_screen, null);
                this.mainImage.setImage(0, null, com.navdy.hud.app.ui.component.image.InitialsImageView.Style.DEFAULT);
                this.mainImage.setTag(null);
                this.sideImage.setImageResource(0);
                viewGroup.setAlpha(1.0f);
                break;
            case BIG_GLANCE_MESSAGE:
                viewGroup.setAlpha(1.0f);
                if (this.scrollView != null) {
                    this.scrollView.fullScroll(33);
                    this.onTop = true;
                    this.onBottom = false;
                    break;
                }
                break;
            case BIG_GLANCE_MESSAGE_SINGLE:
                ((android.widget.TextView) viewGroup.findViewById(com.navdy.hud.app.R.id.title)).setTextAppearance(com.navdy.hud.app.HudApplication.getAppContext(), com.navdy.hud.app.R.style.glance_message_title);
                break;
            case BIG_MULTI_TEXT:
            case BIG_CALENDAR:
                break;
            case SMALL_SIGN:
                this.mainTitle.setAlpha(1.0f);
                this.mainTitle.setVisibility(0);
                this.subTitle.setAlpha(1.0f);
                this.subTitle.setVisibility(0);
                this.choiceLayout.setAlpha(1.0f);
                this.choiceLayout.setVisibility(0);
                this.choiceLayout.setTag(null);
                this.choiceLayout.setTag(com.navdy.hud.app.R.id.message_prev_choice, null);
                this.choiceLayout.setTag(com.navdy.hud.app.R.id.message_secondary_screen, null);
                this.sideImage.setImageResource(0);
                viewGroup.setAlpha(1.0f);
                break;
        }
        viewGroup.setAlpha(1.0f);
        com.navdy.hud.app.framework.glance.GlanceViewCache.putView(viewType, viewGroup);
    }

    private void sendTts() {
        if (this.controller != null && this.controller.isTtsOn() && !this.ttsSent) {
            sLogger.v("tts-send [" + this.id + "]");
            this.bus.post(new com.navdy.hud.app.event.LocalSpeechRequest(new com.navdy.service.library.events.audio.SpeechRequest.Builder().words(this.ttsMessage).category(com.navdy.service.library.events.audio.Category.SPEECH_MESSAGE_READ_OUT).id(this.id).sendStatus(java.lang.Boolean.valueOf(true)).build()));
            this.ttsSent = true;
        }
    }

    private void cancelTts() {
        if (this.controller != null && this.controller.isTtsOn() && this.ttsSent) {
            sLogger.v("tts-cancelled [" + this.id + "]");
            this.bus.post(new com.navdy.hud.app.event.RemoteEvent(this.cancelSpeechRequest));
            this.ttsSent = false;
            stopAudioAnimation(false);
        }
    }

    private android.view.ViewGroup getReplyView(int item, android.content.Context context) {
        if (item == 0) {
            if (this.replyExitView == null) {
                this.replyExitView = (android.view.ViewGroup) android.view.LayoutInflater.from(context).inflate(com.navdy.hud.app.R.layout.glance_reply_exit, null, false);
            } else {
                removeParent(this.replyExitView);
            }
            return this.replyExitView;
        }
        if (this.replyMsgView == null) {
            this.replyMsgView = (android.view.ViewGroup) android.view.LayoutInflater.from(context).inflate(com.navdy.hud.app.R.layout.glance_large_message_single, null, false);
        } else {
            removeParent(this.replyMsgView);
        }
        java.util.List<java.lang.String> list = getCannedReplyMessages();
        if (item > list.size()) {
            item = list.size();
        }
        ((android.widget.TextView) this.replyMsgView.findViewById(com.navdy.hud.app.R.id.title)).setText(com.navdy.hud.app.framework.glance.GlanceConstants.message);
        ((android.widget.TextView) this.replyMsgView.findViewById(com.navdy.hud.app.R.id.message)).setText((java.lang.CharSequence) list.get(item - 1));
        return this.replyMsgView;
    }

    private void removeParent(android.view.View view) {
        if (view != null) {
            android.view.ViewGroup parent = (android.view.ViewGroup) view.getParent();
            if (parent != null) {
                parent.removeView(view);
                sLogger.v("reply view removed");
            }
        }
    }

    @com.squareup.otto.Subscribe
    public void onShowToast(com.navdy.hud.app.framework.toast.ToastManager.ShowToast event) {
        if (android.text.TextUtils.equals(event.name, this.id + "message-sent-toast")) {
            sLogger.v("reply toast:animateOutExpandedView");
            com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().collapseExpandedNotification(true, true);
        }
    }

    @com.squareup.otto.Subscribe
    public void onSpeechRequestNotification(com.navdy.service.library.events.audio.SpeechRequestStatus event) {
        if (this.audioFeedback != null && !android.text.TextUtils.isEmpty(this.id) && android.text.TextUtils.equals(this.id, event.id) && event.status != null) {
            switch (event.status) {
                case SPEECH_REQUEST_STARTING:
                    startAudioAnimation();
                    return;
                case SPEECH_REQUEST_STOPPED:
                    stopAudioAnimation(true);
                    return;
                default:
                    return;
            }
        }
    }

    @com.squareup.otto.Subscribe
    public void onConnectionStateChange(com.navdy.service.library.events.connection.ConnectionStateChange event) {
        if (event.state == com.navdy.service.library.events.connection.ConnectionStateChange.ConnectionState.CONNECTION_DISCONNECTED) {
            stopAudioAnimation(true);
        }
    }

    public void setListener(com.navdy.hud.app.framework.notifications.IProgressUpdate callback) {
        this.progressUpdate = callback;
    }

    private void startAudioAnimation() {
        if (this.audioFeedback != null) {
            this.audioFeedback.animate().cancel();
            this.audioFeedback.setAlpha(1.0f);
            this.audioFeedback.setVisibility(0);
            this.audioFeedback.setImageResource(com.navdy.hud.app.R.drawable.audio_loop);
            ((android.graphics.drawable.AnimationDrawable) this.audioFeedback.getDrawable()).start();
        }
    }

    private void stopAudioAnimation(boolean showFadeOutAnimation) {
        if (this.audioFeedback != null && this.audioFeedback.getVisibility() == 0) {
            android.graphics.drawable.AnimationDrawable animationDrawable = (android.graphics.drawable.AnimationDrawable) this.audioFeedback.getDrawable();
            if (animationDrawable.isRunning()) {
                animationDrawable.stop();
                if (showFadeOutAnimation) {
                    this.audioFeedback.animate().alpha(0.0f).setDuration(300).withEndAction(new com.navdy.hud.app.framework.glance.GlanceNotification.Anon4());
                } else {
                    this.audioFeedback.setVisibility(8);
                }
            }
        }
    }

    private boolean hasReplyAction() {
        return this.glanceEvent.actions != null && this.glanceEvent.actions.contains(com.navdy.service.library.events.glances.GlanceEvent.GlanceActions.REPLY);
    }

    private java.util.List<java.lang.String> getCannedReplyMessages() {
        if (this.cannedReplyMessages == null) {
            this.cannedReplyMessages = com.navdy.hud.app.framework.glance.GlanceConstants.getCannedMessages();
        }
        return this.cannedReplyMessages;
    }

    /* access modifiers changed from: private */
    public void switchToReplyScreen() {
        android.os.Bundle args = new android.os.Bundle();
        java.util.ArrayList<android.os.Parcelable> contactList = new java.util.ArrayList<>();
        if (this.contacts == null) {
            contactList.add(new com.navdy.hud.app.framework.contacts.Contact(this.source, this.number, com.navdy.hud.app.framework.contacts.NumberType.OTHER, com.navdy.hud.app.framework.contacts.ContactImageHelper.getInstance().getContactImageIndex(this.number), 0));
        } else {
            contactList.addAll(this.contacts);
        }
        args.putParcelableArrayList(com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.ARG_CONTACTS_LIST, contactList);
        args.putString(com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.ARG_NOTIFICATION_ID, this.id);
        args.putInt(com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.ARG_MENU_MODE, com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.MenuMode.REPLY_PICKER.ordinal());
        com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().removeNotification(this.id, false, com.navdy.service.library.events.ui.Screen.SCREEN_MAIN_MENU, args, null);
    }

    public void setContactList(java.util.List<com.navdy.hud.app.framework.contacts.Contact> list) {
        this.contacts = list;
    }
}
