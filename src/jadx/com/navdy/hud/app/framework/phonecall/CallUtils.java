package com.navdy.hud.app.framework.phonecall;

public class CallUtils {
    private static com.navdy.hud.app.framework.phonecall.CallManager callManager;

    public static synchronized boolean isPhoneCallInProgress() {
        boolean isCallInProgress;
        synchronized (com.navdy.hud.app.framework.phonecall.CallUtils.class) {
            if (callManager == null) {
                callManager = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getCallManager();
            }
            isCallInProgress = callManager.isCallInProgress();
        }
        return isCallInProgress;
    }
}
