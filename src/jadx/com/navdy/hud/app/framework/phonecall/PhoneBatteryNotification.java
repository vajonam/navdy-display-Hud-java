package com.navdy.hud.app.framework.phonecall;

public class PhoneBatteryNotification {
    private static final java.lang.String EMPTY = "";
    private static final java.lang.String ID_BATTERY_EXTREMELY_LOW = "phone-bat-exlow";
    private static final java.lang.String ID_BATTERY_LOW = "phone-bat-low";
    private static final java.lang.String ID_NO_NETWORK = "phone-no-network";
    private static final int PHONE_BATTERY_TIMEOUT = 2000;
    private static final int PHONE_BATTERY_TIMEOUT_EXTREMELY_LOW = 20000;
    private static final int PHONE_NO_NETWORK_TIMEOUT = 2000;
    private static final java.lang.String noNetwork = resources.getString(com.navdy.hud.app.R.string.phone_no_network);
    private static final android.content.res.Resources resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.framework.phonecall.PhoneBatteryNotification.class);
    private static final java.lang.String unknown = resources.getString(com.navdy.hud.app.R.string.question_mark);

    public static void showBatteryToast(com.navdy.service.library.events.callcontrol.PhoneBatteryStatus phoneBatteryStatus) {
        sLogger.v("[PhoneBatteryStatus] status[" + phoneBatteryStatus.status + "] charging[" + phoneBatteryStatus.charging + "] level[" + phoneBatteryStatus.level + "]");
        if (phoneBatteryStatus.status == com.navdy.service.library.events.callcontrol.PhoneBatteryStatus.BatteryStatus.BATTERY_OK || java.lang.Boolean.TRUE.equals(phoneBatteryStatus.charging)) {
            sLogger.v("phone battery is ok/charging");
            dismissAllBatteryToasts();
            return;
        }
        android.os.Bundle bundle = new android.os.Bundle();
        java.lang.String level = java.lang.String.valueOf(phoneBatteryStatus.level != null ? phoneBatteryStatus.level : unknown);
        java.lang.String title = "";
        int icon = 0;
        java.lang.String id = null;
        java.lang.String tts = null;
        com.navdy.hud.app.framework.toast.ToastManager toastManager = com.navdy.hud.app.framework.toast.ToastManager.getInstance();
        switch (phoneBatteryStatus.status) {
            case BATTERY_EXTREMELY_LOW:
                title = resources.getString(com.navdy.hud.app.R.string.phone_ex_low_battery, new java.lang.Object[]{level});
                icon = com.navdy.hud.app.R.drawable.icon_toast_phone_battery_very_low;
                bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_TIMEOUT, PHONE_BATTERY_TIMEOUT_EXTREMELY_LOW);
                id = ID_BATTERY_EXTREMELY_LOW;
                bundle.putBoolean(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_DEFAULT_CHOICE, true);
                tts = com.navdy.hud.app.framework.voice.TTSUtils.TTS_PHONE_BATTERY_VERY_LOW;
                toastManager.dismissCurrentToast(ID_BATTERY_LOW);
                toastManager.clearPendingToast(ID_BATTERY_LOW, ID_BATTERY_EXTREMELY_LOW);
                break;
            case BATTERY_LOW:
                title = resources.getString(com.navdy.hud.app.R.string.phone_low_battery, new java.lang.Object[]{level});
                icon = com.navdy.hud.app.R.drawable.icon_toast_phone_battery_low;
                bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_TIMEOUT, 2000);
                id = ID_BATTERY_LOW;
                tts = com.navdy.hud.app.framework.voice.TTSUtils.TTS_PHONE_BATTERY_LOW;
                toastManager.dismissCurrentToast(ID_BATTERY_EXTREMELY_LOW);
                toastManager.clearPendingToast(ID_BATTERY_LOW, ID_BATTERY_EXTREMELY_LOW);
                break;
        }
        bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_IMAGE, icon);
        bundle.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_2, title);
        bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_2_STYLE, com.navdy.hud.app.R.style.Glances_1);
        bundle.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_TTS, tts);
        if (!android.text.TextUtils.equals(id, toastManager.getCurrentToastId())) {
            com.navdy.hud.app.framework.toast.ToastManager.getInstance().addToast(new com.navdy.hud.app.framework.toast.ToastManager.ToastParams(id, bundle, null, false, false));
        }
    }

    private static void dismissAllBatteryToasts() {
        com.navdy.hud.app.framework.toast.ToastManager toastManager = com.navdy.hud.app.framework.toast.ToastManager.getInstance();
        toastManager.dismissCurrentToast(ID_BATTERY_LOW, ID_BATTERY_EXTREMELY_LOW);
        toastManager.clearPendingToast(ID_BATTERY_LOW, ID_BATTERY_EXTREMELY_LOW);
    }

    public static void dismissAllToasts() {
        dismissAllBatteryToasts();
        com.navdy.hud.app.framework.toast.ToastManager toastManager = com.navdy.hud.app.framework.toast.ToastManager.getInstance();
        toastManager.dismissCurrentToast(ID_NO_NETWORK);
        toastManager.clearPendingToast(ID_NO_NETWORK);
    }
}
