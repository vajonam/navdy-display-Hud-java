package com.navdy.hud.app.framework.phonecall;

public class CallNotification implements com.navdy.hud.app.framework.notifications.INotification, com.navdy.hud.app.framework.contacts.ContactUtil.IContactCallback {
    public static final java.lang.String CALL_NOTIFICATION_TOAST_ID = "incomingcall#toast";
    private static final java.lang.String EMPTY = "";
    private static final int END_NOTIFICATION_TIMEOUT = 3000;
    private static final int FAILED_NOTIFICATION_TIMEOUT = 10000;
    private static final float IMAGE_SCALE = 0.5f;
    private static final int IN_CALL_NOTIFICATION_TIMEOUT = 30000;
    private static final int MISSED_NOTIFICATION_TIMEOUT = 10000;
    private static final int TAG_ACCEPT = 101;
    private static final int TAG_CANCEL = 105;
    private static final int TAG_DISMISS = 107;
    private static final int TAG_END = 106;
    private static final int TAG_IGNORE = 102;
    private static final int TAG_MUTE = 103;
    private static final int TAG_UNMUTE = 104;
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.framework.phonecall.CallNotification.class);
    private final java.lang.String activeCall;
    /* access modifiers changed from: private */
    public java.util.ArrayList<com.navdy.hud.app.ui.component.ChoiceLayout2.Choice> activeCallChoices = new java.util.ArrayList<>(2);
    /* access modifiers changed from: private */
    public java.util.ArrayList<com.navdy.hud.app.ui.component.ChoiceLayout2.Choice> activeCallMutedChoices = new java.util.ArrayList<>(2);
    private java.util.ArrayList<com.navdy.hud.app.ui.component.ChoiceLayout2.Choice> activeCallNoMuteChoices = new java.util.ArrayList<>(1);
    /* access modifiers changed from: private */
    public com.squareup.otto.Bus bus;
    private final java.lang.String callAccept;
    private final java.lang.String callCancel;
    private final java.lang.String callDialing;
    private final java.lang.String callDismiss;
    private final java.lang.String callEnd;
    private final java.lang.String callEnded;
    private final java.lang.String callFailed;
    private final java.lang.String callIgnore;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.framework.phonecall.CallManager callManager;
    private final java.lang.String callMissed;
    private final java.lang.String callMute;
    /* access modifiers changed from: private */
    public final java.lang.String callMuted;
    /* access modifiers changed from: private */
    public android.widget.ImageView callStatusImage;
    private final java.lang.String callUnmute;
    private com.navdy.hud.app.ui.component.image.InitialsImageView callerImage;
    private android.widget.TextView callerName;
    /* access modifiers changed from: private */
    public android.widget.TextView callerStatus;
    private final java.lang.String callerUnknown;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.ui.component.ChoiceLayout2 choiceLayout;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.ui.component.ChoiceLayout2.IListener choiceListener = new com.navdy.hud.app.framework.phonecall.CallNotification.Anon2();
    private android.view.ViewGroup container;
    private int contentWidth;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.framework.notifications.INotificationController controller;
    private java.util.ArrayList<com.navdy.hud.app.ui.component.ChoiceLayout2.Choice> dialingCallChoices = new java.util.ArrayList<>(1);
    private final int dismisColor;
    /* access modifiers changed from: private */
    public java.lang.Runnable durationRunnable = new com.navdy.hud.app.framework.phonecall.CallNotification.Anon1();
    private final int endColor;
    private java.util.ArrayList<com.navdy.hud.app.ui.component.ChoiceLayout2.Choice> endedCallChoices = new java.util.ArrayList<>(1);
    private android.view.ViewGroup extendedContainer;
    /* access modifiers changed from: private */
    public android.widget.TextView extendedTitle;
    private java.util.ArrayList<com.navdy.hud.app.ui.component.ChoiceLayout2.Choice> failedCallChoices = new java.util.ArrayList<>(1);
    /* access modifiers changed from: private */
    public android.os.Handler handler = new android.os.Handler(android.os.Looper.getMainLooper());
    private final java.lang.String incomingCall;
    private java.util.ArrayList<com.navdy.hud.app.ui.component.ChoiceLayout.Choice> incomingCallChoices = new java.util.ArrayList<>(2);
    private java.util.ArrayList<com.navdy.hud.app.ui.component.ChoiceLayout2.Choice> missedCallChoices = new java.util.ArrayList<>(1);
    private final int muteColor;
    private android.content.res.Resources resources;
    /* access modifiers changed from: private */
    public com.squareup.picasso.Transformation roundTransformation = new com.makeramen.RoundedTransformationBuilder().oval(true).build();
    private final int unmuteColor;
    private final int unselectedColor;

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            try {
                com.navdy.hud.app.framework.phonecall.CallNotification.this.setCallDuration(com.navdy.hud.app.framework.phonecall.CallNotification.this.extendedTitle);
            } finally {
                com.navdy.hud.app.framework.phonecall.CallNotification.this.handler.postDelayed(com.navdy.hud.app.framework.phonecall.CallNotification.this.durationRunnable, java.util.concurrent.TimeUnit.MINUTES.toMillis(1));
            }
        }
    }

    class Anon2 implements com.navdy.hud.app.ui.component.ChoiceLayout2.IListener {
        Anon2() {
        }

        public void executeItem(com.navdy.hud.app.ui.component.ChoiceLayout2.Selection selection) {
            switch (selection.id) {
                case 5:
                    if (com.navdy.hud.app.framework.phonecall.CallNotification.this.controller != null && com.navdy.hud.app.framework.phonecall.CallNotification.this.controller.isExpandedWithStack()) {
                        com.navdy.hud.app.framework.phonecall.CallNotification.this.controller.collapseNotification(false, false);
                        return;
                    }
                    return;
                case 103:
                    if (com.navdy.hud.app.framework.phonecall.CallNotification.this.callManager.state != com.navdy.hud.app.framework.phonecall.CallManager.CallNotificationState.IN_PROGRESS) {
                        com.navdy.hud.app.framework.phonecall.CallNotification.this.dismissNotification();
                        return;
                    }
                    com.navdy.hud.app.framework.phonecall.CallNotification.this.callManager.sendCallAction(com.navdy.service.library.events.callcontrol.CallAction.CALL_MUTE, null);
                    com.navdy.hud.app.framework.phonecall.CallNotification.this.callerStatus.setText(com.navdy.hud.app.framework.phonecall.CallNotification.this.callMuted);
                    com.navdy.hud.app.framework.phonecall.CallNotification.this.callStatusImage.setImageResource(com.navdy.hud.app.R.drawable.icon_call_muted);
                    com.navdy.hud.app.framework.phonecall.CallNotification.this.choiceLayout.setChoices(com.navdy.hud.app.framework.phonecall.CallNotification.this.activeCallMutedChoices, 0, com.navdy.hud.app.framework.phonecall.CallNotification.this.choiceListener, 0.5f);
                    com.navdy.hud.app.framework.phonecall.CallNotification.this.choiceLayout.setTag(com.navdy.hud.app.framework.phonecall.CallNotification.this.activeCallMutedChoices);
                    return;
                case 104:
                    if (com.navdy.hud.app.framework.phonecall.CallNotification.this.callManager.state != com.navdy.hud.app.framework.phonecall.CallManager.CallNotificationState.IN_PROGRESS) {
                        com.navdy.hud.app.framework.phonecall.CallNotification.this.dismissNotification();
                        return;
                    }
                    com.navdy.hud.app.framework.phonecall.CallNotification.this.callManager.sendCallAction(com.navdy.service.library.events.callcontrol.CallAction.CALL_UNMUTE, null);
                    com.navdy.hud.app.framework.phonecall.CallNotification.this.callerStatus.setText("");
                    com.navdy.hud.app.framework.phonecall.CallNotification.this.callStatusImage.setImageResource(com.navdy.hud.app.R.drawable.icon_call_green);
                    com.navdy.hud.app.framework.phonecall.CallNotification.this.choiceLayout.setChoices(com.navdy.hud.app.framework.phonecall.CallNotification.this.activeCallChoices, 0, com.navdy.hud.app.framework.phonecall.CallNotification.this.choiceListener, 0.5f);
                    com.navdy.hud.app.framework.phonecall.CallNotification.this.choiceLayout.setTag(com.navdy.hud.app.framework.phonecall.CallNotification.this.activeCallChoices);
                    return;
                case 105:
                    if (com.navdy.hud.app.framework.phonecall.CallNotification.this.callManager.state == com.navdy.hud.app.framework.phonecall.CallManager.CallNotificationState.DIALING) {
                        com.navdy.hud.app.framework.phonecall.CallNotification.this.callManager.sendCallAction(com.navdy.service.library.events.callcontrol.CallAction.CALL_END, null);
                        return;
                    } else if (com.navdy.hud.app.framework.phonecall.CallNotification.this.callManager.state != com.navdy.hud.app.framework.phonecall.CallManager.CallNotificationState.IN_PROGRESS) {
                        com.navdy.hud.app.framework.phonecall.CallNotification.this.dismissNotification();
                        return;
                    } else {
                        return;
                    }
                case 106:
                    if (com.navdy.hud.app.framework.phonecall.CallNotification.this.callManager.state != com.navdy.hud.app.framework.phonecall.CallManager.CallNotificationState.IN_PROGRESS) {
                        com.navdy.hud.app.framework.phonecall.CallNotification.this.dismissNotification();
                        return;
                    } else {
                        com.navdy.hud.app.framework.phonecall.CallNotification.this.callManager.sendCallAction(com.navdy.service.library.events.callcontrol.CallAction.CALL_END, null);
                        return;
                    }
                case com.navdy.hud.app.framework.phonecall.CallNotification.TAG_DISMISS /*107*/:
                    com.navdy.hud.app.framework.phonecall.CallNotification.this.dismissNotification();
                    return;
                default:
                    return;
            }
        }

        public void itemSelected(com.navdy.hud.app.ui.component.ChoiceLayout2.Selection selection) {
        }
    }

    class Anon3 implements com.navdy.hud.app.framework.toast.IToastCallback {
        com.navdy.hud.app.framework.contacts.ContactUtil.IContactCallback cb = new com.navdy.hud.app.framework.phonecall.CallNotification.Anon3.Anon1();
        com.navdy.hud.app.ui.component.image.InitialsImageView toastImage;
        com.navdy.hud.app.view.ToastView toastView;
        final /* synthetic */ java.lang.String val$caller;
        final /* synthetic */ java.lang.String val$number;

        class Anon1 implements com.navdy.hud.app.framework.contacts.ContactUtil.IContactCallback {
            Anon1() {
            }

            public boolean isContextValid() {
                if (com.navdy.hud.app.framework.phonecall.CallNotification.Anon3.this.toastView != null) {
                    return true;
                }
                return false;
            }
        }

        Anon3(java.lang.String str, java.lang.String str2) {
            this.val$caller = str;
            this.val$number = str2;
        }

        public void onStart(com.navdy.hud.app.view.ToastView view) {
            this.toastView = view;
            this.toastImage = view.getMainLayout().screenImage;
            this.toastImage.setVisibility(0);
            com.navdy.hud.app.framework.phonecall.CallNotification.this.bus.register(this);
            com.navdy.hud.app.framework.contacts.ContactUtil.setContactPhoto(this.val$caller, this.val$number, true, this.toastImage, com.navdy.hud.app.framework.phonecall.CallNotification.this.roundTransformation, this.cb);
        }

        public void onStop() {
            com.navdy.hud.app.framework.phonecall.CallNotification.this.bus.unregister(this);
            this.toastView = null;
            this.toastImage = null;
        }

        public boolean onKey(com.navdy.hud.app.manager.InputManager.CustomKeyEvent event) {
            return false;
        }

        public void executeChoiceItem(int pos, int id) {
            switch (id) {
                case 101:
                    if (com.navdy.hud.app.framework.phonecall.CallNotification.this.callManager.state == com.navdy.hud.app.framework.phonecall.CallManager.CallNotificationState.RINGING) {
                        com.navdy.hud.app.framework.phonecall.CallNotification.this.callManager.sendCallAction(com.navdy.service.library.events.callcontrol.CallAction.CALL_ACCEPT, null);
                    }
                    com.navdy.hud.app.framework.toast.ToastManager.getInstance().dismissCurrentToast(com.navdy.hud.app.framework.phonecall.CallNotification.CALL_NOTIFICATION_TOAST_ID);
                    return;
                case 102:
                    if (com.navdy.hud.app.framework.phonecall.CallNotification.this.callManager.state == com.navdy.hud.app.framework.phonecall.CallManager.CallNotificationState.RINGING) {
                        com.navdy.hud.app.framework.phonecall.CallNotification.this.callManager.sendCallAction(com.navdy.service.library.events.callcontrol.CallAction.CALL_REJECT, null);
                    }
                    com.navdy.hud.app.framework.toast.ToastManager.getInstance().dismissCurrentToast(com.navdy.hud.app.framework.phonecall.CallNotification.CALL_NOTIFICATION_TOAST_ID);
                    return;
                default:
                    return;
            }
        }

        @com.squareup.otto.Subscribe
        public void onPhotoDownload(com.navdy.hud.app.framework.contacts.PhoneImageDownloader.PhotoDownloadStatus status) {
            if (this.toastView != null && this.toastImage != null && !status.alreadyDownloaded && status.photoType == com.navdy.service.library.events.photo.PhotoType.PHOTO_CONTACT && com.navdy.hud.app.framework.phonecall.CallNotification.this.callManager.phoneStatus != com.navdy.service.library.events.callcontrol.PhoneStatus.PHONE_IDLE && android.text.TextUtils.equals(this.val$number, status.sourceIdentifier)) {
                com.navdy.hud.app.framework.contacts.ContactUtil.setContactPhoto(this.val$caller, this.val$number, false, this.toastImage, com.navdy.hud.app.framework.phonecall.CallNotification.this.roundTransformation, this.cb);
            }
        }
    }

    CallNotification(com.navdy.hud.app.framework.phonecall.CallManager callManager2, com.squareup.otto.Bus bus2) {
        this.callManager = callManager2;
        this.bus = bus2;
        this.resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        this.callEnded = this.resources.getString(com.navdy.hud.app.R.string.call_ended);
        this.callDialing = this.resources.getString(com.navdy.hud.app.R.string.call_dialing);
        this.callMuted = this.resources.getString(com.navdy.hud.app.R.string.call_muted);
        this.callerUnknown = this.resources.getString(com.navdy.hud.app.R.string.caller_unknown);
        this.callAccept = this.resources.getString(com.navdy.hud.app.R.string.call_accept);
        this.callIgnore = this.resources.getString(com.navdy.hud.app.R.string.call_ignore);
        this.callMute = this.resources.getString(com.navdy.hud.app.R.string.call_mute);
        this.callEnd = this.resources.getString(com.navdy.hud.app.R.string.call_end);
        this.callUnmute = this.resources.getString(com.navdy.hud.app.R.string.call_unmute);
        this.callCancel = this.resources.getString(com.navdy.hud.app.R.string.call_cancel);
        this.callDismiss = this.resources.getString(com.navdy.hud.app.R.string.call_dismiss);
        this.callFailed = this.resources.getString(com.navdy.hud.app.R.string.call_failed);
        this.callMissed = this.resources.getString(com.navdy.hud.app.R.string.call_missed);
        this.incomingCall = this.resources.getString(com.navdy.hud.app.R.string.incoming_call);
        this.activeCall = this.resources.getString(com.navdy.hud.app.R.string.phone_call_duration);
        this.muteColor = this.resources.getColor(com.navdy.hud.app.R.color.call_mute);
        this.unmuteColor = this.resources.getColor(com.navdy.hud.app.R.color.call_unmute);
        this.endColor = this.resources.getColor(com.navdy.hud.app.R.color.call_end);
        this.dismisColor = this.resources.getColor(com.navdy.hud.app.R.color.glance_dismiss);
        this.unselectedColor = -16777216;
        this.contentWidth = (int) this.resources.getDimension(com.navdy.hud.app.R.dimen.toast_app_disconnected_width);
        this.incomingCallChoices.add(new com.navdy.hud.app.ui.component.ChoiceLayout.Choice(this.callAccept, 101));
        this.incomingCallChoices.add(new com.navdy.hud.app.ui.component.ChoiceLayout.Choice(this.callIgnore, 102));
        this.activeCallChoices.add(new com.navdy.hud.app.ui.component.ChoiceLayout2.Choice(103, com.navdy.hud.app.R.drawable.icon_glances_mute, this.muteColor, com.navdy.hud.app.R.drawable.icon_glances_mute, this.unselectedColor, this.callMute, this.muteColor));
        this.activeCallChoices.add(new com.navdy.hud.app.ui.component.ChoiceLayout2.Choice(106, com.navdy.hud.app.R.drawable.icon_glances_endcall, this.endColor, com.navdy.hud.app.R.drawable.icon_glances_endcall, this.unselectedColor, this.callEnd, this.endColor));
        this.activeCallNoMuteChoices.add(new com.navdy.hud.app.ui.component.ChoiceLayout2.Choice(106, com.navdy.hud.app.R.drawable.icon_glances_endcall, this.endColor, com.navdy.hud.app.R.drawable.icon_glances_endcall, this.unselectedColor, this.callEnd, this.endColor));
        this.activeCallMutedChoices.add(new com.navdy.hud.app.ui.component.ChoiceLayout2.Choice(104, com.navdy.hud.app.R.drawable.icon_glances_unmute, this.unmuteColor, com.navdy.hud.app.R.drawable.icon_glances_unmute, this.unselectedColor, this.callUnmute, this.unmuteColor));
        this.activeCallMutedChoices.add(new com.navdy.hud.app.ui.component.ChoiceLayout2.Choice(106, com.navdy.hud.app.R.drawable.icon_glances_endcall, this.endColor, com.navdy.hud.app.R.drawable.icon_glances_endcall, this.unselectedColor, this.callEnd, this.endColor));
        this.dialingCallChoices.add(new com.navdy.hud.app.ui.component.ChoiceLayout2.Choice(105, com.navdy.hud.app.R.drawable.icon_glances_endcall, this.endColor, com.navdy.hud.app.R.drawable.icon_glances_endcall, this.unselectedColor, this.callEnd, this.endColor));
        this.failedCallChoices.add(new com.navdy.hud.app.ui.component.ChoiceLayout2.Choice(TAG_DISMISS, com.navdy.hud.app.R.drawable.icon_glances_dismiss, this.dismisColor, com.navdy.hud.app.R.drawable.icon_glances_dismiss, this.unselectedColor, this.callDismiss, this.dismisColor));
        this.endedCallChoices.add(new com.navdy.hud.app.ui.component.ChoiceLayout2.Choice(TAG_DISMISS, com.navdy.hud.app.R.drawable.icon_glances_dismiss, this.dismisColor, com.navdy.hud.app.R.drawable.icon_glances_dismiss, this.unselectedColor, this.callDismiss, this.dismisColor));
        this.missedCallChoices.add(new com.navdy.hud.app.ui.component.ChoiceLayout2.Choice(TAG_DISMISS, com.navdy.hud.app.R.drawable.icon_glances_dismiss, this.dismisColor, com.navdy.hud.app.R.drawable.icon_glances_dismiss, this.unselectedColor, this.callDismiss, this.dismisColor));
    }

    public com.navdy.hud.app.framework.notifications.NotificationType getType() {
        return com.navdy.hud.app.framework.notifications.NotificationType.PHONE_CALL;
    }

    public java.lang.String getId() {
        return com.navdy.hud.app.framework.notifications.NotificationId.PHONE_CALL_NOTIFICATION_ID;
    }

    public android.view.View getView(android.content.Context context) {
        if (this.container == null) {
            this.container = (android.view.ViewGroup) android.view.LayoutInflater.from(context).inflate(com.navdy.hud.app.R.layout.notification_phonecall, null);
            this.callerName = (android.widget.TextView) this.container.findViewById(com.navdy.hud.app.R.id.callerName);
            this.callerStatus = (android.widget.TextView) this.container.findViewById(com.navdy.hud.app.R.id.callerStatus);
            this.callerImage = (com.navdy.hud.app.ui.component.image.InitialsImageView) this.container.findViewById(com.navdy.hud.app.R.id.callerImage);
            this.callStatusImage = (android.widget.ImageView) this.container.findViewById(com.navdy.hud.app.R.id.callStatusImage);
            this.choiceLayout = (com.navdy.hud.app.ui.component.ChoiceLayout2) this.container.findViewById(com.navdy.hud.app.R.id.choiceLayout);
        }
        return this.container;
    }

    public android.view.View getExpandedView(android.content.Context context, java.lang.Object data) {
        this.extendedContainer = (android.view.ViewGroup) com.navdy.hud.app.framework.glance.GlanceViewCache.getView(com.navdy.hud.app.framework.glance.GlanceViewCache.ViewType.BIG_MULTI_TEXT, context);
        this.extendedTitle = (android.widget.TextView) this.extendedContainer.findViewById(com.navdy.hud.app.R.id.title1);
        this.extendedTitle.setTextAppearance(context, com.navdy.hud.app.R.style.glance_title_1);
        ((android.widget.TextView) this.extendedContainer.findViewById(com.navdy.hud.app.R.id.title2)).setVisibility(8);
        setCallDuration(this.extendedTitle);
        this.extendedTitle.setVisibility(0);
        this.handler.removeCallbacks(this.durationRunnable);
        this.handler.postDelayed(this.durationRunnable, java.util.concurrent.TimeUnit.MINUTES.toMillis(1));
        return this.extendedContainer;
    }

    public int getExpandedViewIndicatorColor() {
        return 0;
    }

    public void onStart(com.navdy.hud.app.framework.notifications.INotificationController controller2) {
        this.controller = controller2;
        this.bus.register(this);
        updateState();
        if (!controller2.isExpandedWithStack()) {
            this.container.setTranslationX(0.0f);
            this.container.setTranslationY(0.0f);
            this.container.setAlpha(1.0f);
            this.callerName.setAlpha(1.0f);
            this.callerStatus.setAlpha(1.0f);
            this.choiceLayout.setAlpha(1.0f);
        }
    }

    public void onUpdate() {
        updateState();
    }

    public void onStop() {
        this.handler.removeCallbacks(this.durationRunnable);
        this.bus.unregister(this);
        this.controller = null;
        if (this.choiceLayout != null) {
            this.choiceLayout.clear();
        }
        if (this.extendedContainer != null) {
            android.view.ViewGroup parent = (android.view.ViewGroup) this.extendedContainer.getParent();
            if (parent != null) {
                parent.removeView(this.extendedContainer);
            }
            com.navdy.hud.app.framework.glance.GlanceViewCache.putView(com.navdy.hud.app.framework.glance.GlanceViewCache.ViewType.BIG_MULTI_TEXT, this.extendedContainer);
            this.extendedContainer = null;
        }
    }

    public int getTimeout() {
        return 0;
    }

    public boolean isAlive() {
        switch (this.callManager.state) {
            case IN_PROGRESS:
            case RINGING:
            case DIALING:
                return true;
            default:
                return false;
        }
    }

    public boolean isPurgeable() {
        return !isAlive();
    }

    public boolean canAddToStackIfCurrentExists() {
        return true;
    }

    public int getColor() {
        return com.navdy.hud.app.framework.glance.GlanceConstants.colorPhoneCall;
    }

    public void onNotificationEvent(com.navdy.hud.app.ui.framework.UIStateManager.Mode mode) {
        if (mode == com.navdy.hud.app.ui.framework.UIStateManager.Mode.EXPAND) {
            com.navdy.hud.app.framework.phonecall.CallManager.CallNotificationState callState = this.callManager.state;
            if (isTimeoutRequired(callState)) {
                if (this.controller != null) {
                    sLogger.v("set timeout:" + callState);
                    this.controller.stopTimeout(true);
                    this.controller.startTimeout(getTimeout(callState));
                }
            } else if (this.controller != null) {
                sLogger.v("reset timeout:" + callState);
                this.controller.stopTimeout(true);
            }
        }
    }

    public void onExpandedNotificationEvent(com.navdy.hud.app.ui.framework.UIStateManager.Mode mode) {
        if (mode == com.navdy.hud.app.ui.framework.UIStateManager.Mode.COLLAPSE && this.controller != null) {
            updateState();
        }
    }

    public void onExpandedNotificationSwitched() {
    }

    public android.animation.AnimatorSet getViewSwitchAnimation(boolean viewIn) {
        android.animation.AnimatorSet set = new android.animation.AnimatorSet();
        if (!viewIn) {
            set.playTogether(new android.animation.Animator[]{android.animation.ObjectAnimator.ofFloat(this.callerName, android.view.View.ALPHA, new float[]{1.0f, 0.0f}), android.animation.ObjectAnimator.ofFloat(this.callerStatus, android.view.View.ALPHA, new float[]{1.0f, 0.0f}), android.animation.ObjectAnimator.ofFloat(this.choiceLayout, android.view.View.ALPHA, new float[]{1.0f, 0.0f})});
        } else {
            set.playTogether(new android.animation.Animator[]{android.animation.ObjectAnimator.ofFloat(this.callerName, android.view.View.ALPHA, new float[]{0.0f, 1.0f}), android.animation.ObjectAnimator.ofFloat(this.callerStatus, android.view.View.ALPHA, new float[]{0.0f, 1.0f}), android.animation.ObjectAnimator.ofFloat(this.choiceLayout, android.view.View.ALPHA, new float[]{0.0f, 1.0f})});
        }
        return set;
    }

    public boolean expandNotification() {
        if (this.controller == null) {
            return false;
        }
        java.lang.Object tag = this.choiceLayout.getTag();
        if (tag != this.activeCallChoices && tag != this.activeCallMutedChoices && tag != this.activeCallNoMuteChoices) {
            return false;
        }
        switchToExpandedMode();
        return true;
    }

    public boolean supportScroll() {
        return false;
    }

    private void switchToExpandedMode() {
        if (this.controller != null) {
            this.controller.stopTimeout(true);
            this.choiceLayout.setChoices(com.navdy.hud.app.framework.glance.GlanceConstants.backChoice2, 0, this.choiceListener, 0.5f);
            this.choiceLayout.setTag(com.navdy.hud.app.framework.glance.GlanceConstants.backChoice2);
            if (!this.controller.isExpandedWithStack()) {
                this.controller.expandNotification(true);
            }
        }
    }

    public void onClick() {
    }

    public void onTrackHand(float normalized) {
    }

    public boolean onGesture(com.navdy.service.library.events.input.GestureEvent event) {
        return false;
    }

    public boolean onKey(com.navdy.hud.app.manager.InputManager.CustomKeyEvent event) {
        boolean handled = false;
        boolean ret = false;
        switch (event) {
            case LEFT:
                handled = this.choiceLayout.moveSelectionLeft();
                ret = true;
                break;
            case RIGHT:
                handled = this.choiceLayout.moveSelectionRight();
                ret = true;
                break;
            case SELECT:
                this.choiceLayout.executeSelectedItem();
                ret = true;
                break;
        }
        if (this.controller != null && handled && isTimeoutRequired(this.callManager.state) && getTimeout(this.callManager.state) > 0) {
            this.controller.resetTimeout();
        }
        return ret;
    }

    public com.navdy.hud.app.manager.InputManager.IInputHandler nextHandler() {
        return null;
    }

    /* access modifiers changed from: 0000 */
    public void updateState() {
        com.navdy.hud.app.framework.phonecall.CallManager.CallNotificationState callState = this.callManager.state;
        sLogger.v("call state:" + callState);
        if (!checkIfCollapseRequired()) {
            switch (callState) {
                case IDLE:
                    if (com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().isCurrentNotificationId(com.navdy.hud.app.framework.notifications.NotificationId.PHONE_CALL_NOTIFICATION_ID)) {
                        dismissNotification();
                        break;
                    }
                    break;
                case MISSED:
                    setCaller();
                    this.callerStatus.setText(this.callMissed);
                    this.choiceLayout.setVisibility(0);
                    this.choiceLayout.setChoices(this.missedCallChoices, 0, this.choiceListener, 0.5f);
                    this.choiceLayout.setTag(this.missedCallChoices);
                    this.callStatusImage.setImageResource(com.navdy.hud.app.R.drawable.icon_badge_callend);
                    this.callStatusImage.setVisibility(0);
                    com.navdy.hud.app.framework.contacts.ContactUtil.setContactPhoto(this.callManager.contact, this.callManager.number, false, this.callerImage, this.roundTransformation, this);
                    break;
                case ENDED:
                    setCaller();
                    this.callerStatus.setText(this.callEnded);
                    this.choiceLayout.setVisibility(0);
                    this.choiceLayout.setChoices(this.endedCallChoices, 0, this.choiceListener, 0.5f);
                    this.choiceLayout.setTag(this.endedCallChoices);
                    this.callStatusImage.setImageResource(com.navdy.hud.app.R.drawable.icon_badge_callend);
                    this.callStatusImage.setVisibility(0);
                    com.navdy.hud.app.framework.contacts.ContactUtil.setContactPhoto(this.callManager.contact, this.callManager.number, false, this.callerImage, this.roundTransformation, this);
                    break;
                case FAILED:
                    setCaller();
                    this.callerStatus.setText(this.callFailed);
                    this.choiceLayout.setVisibility(0);
                    this.choiceLayout.setChoices(this.failedCallChoices, 0, this.choiceListener, 0.5f);
                    this.choiceLayout.setTag(this.failedCallChoices);
                    this.callStatusImage.setImageResource(com.navdy.hud.app.R.drawable.icon_call_red);
                    this.callStatusImage.setVisibility(0);
                    com.navdy.hud.app.framework.contacts.ContactUtil.setContactPhoto(this.callManager.contact, this.callManager.number, false, this.callerImage, this.roundTransformation, this);
                    break;
                case REJECTED:
                    dismissNotification();
                    break;
                case CANCELLED:
                    dismissNotification();
                    break;
                case IN_PROGRESS:
                    if (setCaller()) {
                        this.callerStatus.setText("");
                    } else {
                        java.lang.String number = getCallerNumber();
                        if (com.navdy.hud.app.framework.contacts.ContactUtil.isValidNumber(number)) {
                            this.callerStatus.setText(com.navdy.hud.app.util.PhoneUtil.formatPhoneNumber(number));
                        }
                    }
                    this.callStatusImage.setImageResource(com.navdy.hud.app.R.drawable.icon_badge_call);
                    this.callStatusImage.setVisibility(0);
                    com.navdy.hud.app.framework.contacts.ContactUtil.setContactPhoto(this.callManager.contact, this.callManager.number, false, this.callerImage, this.roundTransformation, this);
                    if (this.controller.isExpandedWithStack()) {
                        this.choiceLayout.setChoices(com.navdy.hud.app.framework.glance.GlanceConstants.backChoice2, 0, this.choiceListener, 0.5f);
                        this.choiceLayout.setTag(com.navdy.hud.app.framework.glance.GlanceConstants.backChoice2);
                    } else {
                        com.navdy.service.library.events.DeviceInfo deviceInfo = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getRemoteDeviceInfo();
                        if (deviceInfo == null || deviceInfo.platform == com.navdy.service.library.events.DeviceInfo.Platform.PLATFORM_Android) {
                            this.choiceLayout.setChoices(this.activeCallNoMuteChoices, 0, this.choiceListener, 0.5f);
                            this.choiceLayout.setTag(this.activeCallNoMuteChoices);
                        } else if (!this.callManager.callMuted) {
                            this.choiceLayout.setChoices(this.activeCallChoices, 0, this.choiceListener, 0.5f);
                            this.choiceLayout.setTag(this.activeCallChoices);
                        } else {
                            this.choiceLayout.setChoices(this.activeCallMutedChoices, 0, this.choiceListener, 0.5f);
                            this.choiceLayout.setTag(this.activeCallMutedChoices);
                        }
                    }
                    this.choiceLayout.setVisibility(0);
                    break;
                case DIALING:
                    setCaller();
                    this.callerStatus.setText(this.callDialing);
                    this.callStatusImage.setVisibility(4);
                    com.navdy.hud.app.framework.contacts.ContactUtil.setContactPhoto(this.callManager.contact, this.callManager.number, true, this.callerImage, this.roundTransformation, this);
                    this.choiceLayout.setChoices(this.dialingCallChoices, 0, this.choiceListener, 0.5f);
                    this.choiceLayout.setTag(this.dialingCallChoices);
                    this.choiceLayout.setVisibility(0);
                    break;
            }
            if (isTimeoutRequired(callState)) {
                if (this.controller != null) {
                    sLogger.v("set timeout:" + callState);
                    this.controller.stopTimeout(true);
                    this.controller.startTimeout(getTimeout(callState));
                }
            } else if (this.controller != null) {
                sLogger.v("reset timeout:" + callState);
                this.controller.stopTimeout(true);
            }
        }
    }

    @com.squareup.otto.Subscribe
    public void onPhotoDownload(com.navdy.hud.app.framework.contacts.PhoneImageDownloader.PhotoDownloadStatus status) {
        if (this.controller != null && !status.alreadyDownloaded && status.photoType == com.navdy.service.library.events.photo.PhotoType.PHOTO_CONTACT && this.callManager.phoneStatus != com.navdy.service.library.events.callcontrol.PhoneStatus.PHONE_IDLE && android.text.TextUtils.equals(this.callManager.number, status.sourceIdentifier)) {
            com.navdy.hud.app.framework.contacts.ContactUtil.setContactPhoto(this.callManager.contact, this.callManager.number, false, this.callerImage, this.roundTransformation, this);
        }
    }

    private boolean setCaller() {
        java.lang.String text = getCaller();
        if (com.navdy.hud.app.framework.contacts.ContactUtil.isValidNumber(text)) {
            this.callerName.setText(com.navdy.hud.app.util.PhoneUtil.formatPhoneNumber(text));
            return true;
        }
        this.callerName.setText(text);
        return false;
    }

    public java.lang.String getCaller() {
        if (!android.text.TextUtils.isEmpty(this.callManager.contact)) {
            return this.callManager.contact;
        }
        if (!android.text.TextUtils.isEmpty(this.callManager.number)) {
            return this.callManager.number;
        }
        return this.callerUnknown;
    }

    private java.lang.String getCallerNumber() {
        return this.callManager.number;
    }

    /* access modifiers changed from: private */
    public void dismissNotification() {
        com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().removeNotification(com.navdy.hud.app.framework.notifications.NotificationId.PHONE_CALL_NOTIFICATION_ID);
        this.callManager.state = com.navdy.hud.app.framework.phonecall.CallManager.CallNotificationState.IDLE;
    }

    private boolean isTimeoutRequired(com.navdy.hud.app.framework.phonecall.CallManager.CallNotificationState state) {
        if (state == null) {
            return false;
        }
        switch (state) {
            case MISSED:
            case ENDED:
            case FAILED:
            case DIALING:
                return true;
            case IN_PROGRESS:
                if (this.controller == null || !this.controller.isExpandedWithStack()) {
                    return true;
                }
                return false;
            default:
                return false;
        }
    }

    private int getTimeout(com.navdy.hud.app.framework.phonecall.CallManager.CallNotificationState state) {
        if (state == null) {
            return 0;
        }
        switch (state) {
            case MISSED:
                return 10000;
            case ENDED:
                return 3000;
            case FAILED:
                return 10000;
            case IN_PROGRESS:
            case DIALING:
                return 30000;
            default:
                return 0;
        }
    }

    public boolean isContextValid() {
        return this.controller != null;
    }

    public void showIncomingCallToast() {
        java.lang.String caller = getCaller();
        java.lang.String n = getCallerNumber();
        if (android.text.TextUtils.isEmpty(n)) {
            n = null;
        }
        java.lang.String number = n;
        android.os.Bundle bundle = new android.os.Bundle();
        bundle.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE, this.incomingCall);
        bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_STYLE, com.navdy.hud.app.R.style.ToastMainTitle);
        bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_SIDE_IMAGE, com.navdy.hud.app.R.drawable.icon_call_green);
        bundle.putParcelableArrayList(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_CHOICE_LIST, this.incomingCallChoices);
        bundle.putBoolean(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_SUPPORTS_GESTURE, true);
        bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_INFO_CONTAINER_MAX_WIDTH, this.contentWidth);
        if (number == null || android.text.TextUtils.equals(caller, number)) {
            if (com.navdy.hud.app.framework.contacts.ContactUtil.isValidNumber(caller)) {
                bundle.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_2, com.navdy.hud.app.util.PhoneUtil.formatPhoneNumber(number));
            } else {
                bundle.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_2, caller);
            }
            bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_2_STYLE, com.navdy.hud.app.R.style.Glances_1);
        } else {
            bundle.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_2, caller);
            bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_2_STYLE, com.navdy.hud.app.R.style.Glances_1);
            bundle.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_3, com.navdy.hud.app.util.PhoneUtil.formatPhoneNumber(number));
            bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_3_STYLE, com.navdy.hud.app.R.style.Toast_1);
        }
        com.navdy.hud.app.framework.toast.IToastCallback callback = new com.navdy.hud.app.framework.phonecall.CallNotification.Anon3(caller, number);
        com.navdy.hud.app.framework.toast.ToastManager toastManager = com.navdy.hud.app.framework.toast.ToastManager.getInstance();
        clearToast();
        toastManager.addToast(new com.navdy.hud.app.framework.toast.ToastManager.ToastParams(CALL_NOTIFICATION_TOAST_ID, bundle, callback, false, true, true));
    }

    public static void clearToast() {
        com.navdy.hud.app.framework.toast.ToastManager toastManager = com.navdy.hud.app.framework.toast.ToastManager.getInstance();
        toastManager.dismissCurrentToast(CALL_NOTIFICATION_TOAST_ID);
        toastManager.clearAllPendingToast();
        toastManager.disableToasts(false);
    }

    /* access modifiers changed from: private */
    public void setCallDuration(android.widget.TextView textView) {
        long start = this.callManager.callStartMs;
        if (start <= 0) {
            textView.setText(this.activeCall);
            return;
        }
        long minute = java.util.concurrent.TimeUnit.MILLISECONDS.toMinutes(android.os.SystemClock.elapsedRealtime() - start);
        if (minute < 1) {
            textView.setText(this.activeCall);
            return;
        }
        textView.setText(this.resources.getString(com.navdy.hud.app.R.string.phone_call_duration_min, new java.lang.Object[]{java.lang.Long.valueOf(minute)}));
    }

    private boolean checkIfCollapseRequired() {
        if (isAlive() || this.controller == null || !this.controller.isExpandedWithStack()) {
            return false;
        }
        sLogger.v("collapse required");
        this.controller.collapseNotification(false, false);
        return true;
    }
}
