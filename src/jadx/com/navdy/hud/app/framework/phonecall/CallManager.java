package com.navdy.hud.app.framework.phonecall;

public class CallManager {
    private static final com.navdy.hud.app.framework.phonecall.CallManager.CallAccepted CALL_ACCEPTED = new com.navdy.hud.app.framework.phonecall.CallManager.CallAccepted();
    private static final com.navdy.hud.app.framework.phonecall.CallManager.CallEnded CALL_ENDED = new com.navdy.hud.app.framework.phonecall.CallManager.CallEnded();
    private static final int NO_RESPONSE_THRESHOLD = 10000;
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.events.callcontrol.PhoneStatusRequest PHONE_STATUS_REQUEST = new com.navdy.service.library.events.callcontrol.PhoneStatusRequest();
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.framework.phonecall.CallManager.class);
    boolean answered = false;
    /* access modifiers changed from: private */
    public com.squareup.otto.Bus bus;
    boolean callMuted;
    private com.navdy.hud.app.framework.phonecall.CallNotification callNotification;
    java.util.Stack<com.navdy.hud.app.framework.phonecall.CallManager.Info> callStack = new java.util.Stack<>();
    long callStartMs;
    java.lang.String callUUID;
    java.lang.String contact = null;
    boolean dialSet;
    long duration;
    private android.os.Handler handler = new android.os.Handler(android.os.Looper.getMainLooper());
    boolean incomingCall = false;
    com.navdy.service.library.events.callcontrol.CallAction lastCallAction;
    private java.lang.Runnable noResponseCheckRunnable = new com.navdy.hud.app.framework.phonecall.CallManager.Anon1();
    java.lang.String normalizedNumber = null;
    java.lang.String number = null;
    com.navdy.service.library.events.callcontrol.PhoneStatus phoneStatus = com.navdy.service.library.events.callcontrol.PhoneStatus.PHONE_IDLE;
    boolean putOnStack = false;
    com.navdy.hud.app.framework.phonecall.CallManager.CallNotificationState state = com.navdy.hud.app.framework.phonecall.CallManager.CallNotificationState.IDLE;
    boolean userCancelledCall;
    boolean userRejectedCall;

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            com.navdy.hud.app.framework.phonecall.CallManager.sLogger.v("no response for call action:" + com.navdy.hud.app.framework.phonecall.CallManager.this.lastCallAction);
            if (com.navdy.hud.app.framework.phonecall.CallManager.this.lastCallAction != null && com.navdy.hud.app.framework.phonecall.CallManager.this.isCallActionResponseRequired(com.navdy.hud.app.framework.phonecall.CallManager.this.lastCallAction)) {
                switch (com.navdy.hud.app.framework.phonecall.CallManager.Anon3.$SwitchMap$com$navdy$service$library$events$callcontrol$CallAction[com.navdy.hud.app.framework.phonecall.CallManager.this.lastCallAction.ordinal()]) {
                    case 1:
                        com.navdy.hud.app.framework.phonecall.CallManager.this.state = com.navdy.hud.app.framework.phonecall.CallManager.CallNotificationState.FAILED;
                        break;
                    case 2:
                        com.navdy.hud.app.framework.phonecall.CallManager.this.state = com.navdy.hud.app.framework.phonecall.CallManager.CallNotificationState.IDLE;
                        break;
                    case 3:
                        com.navdy.hud.app.framework.phonecall.CallManager.this.state = com.navdy.hud.app.framework.phonecall.CallManager.CallNotificationState.FAILED;
                        break;
                }
                com.navdy.hud.app.framework.phonecall.CallManager.this.userRejectedCall = false;
                com.navdy.hud.app.framework.phonecall.CallManager.this.userCancelledCall = false;
                com.navdy.hud.app.framework.phonecall.CallManager.this.dialSet = false;
                if (com.navdy.hud.app.framework.phonecall.CallManager.this.isNotificationAllowed()) {
                    com.navdy.hud.app.framework.phonecall.CallManager.this.sendNotification();
                }
            }
        }
    }

    class Anon2 implements java.lang.Runnable {
        Anon2() {
        }

        public void run() {
            com.navdy.hud.app.framework.phonecall.CallManager.this.bus.post(new com.navdy.hud.app.event.RemoteEvent(com.navdy.hud.app.framework.phonecall.CallManager.PHONE_STATUS_REQUEST));
        }
    }

    public static class CallAccepted {
    }

    public static class CallEnded {
    }

    enum CallNotificationState {
        IDLE,
        RINGING,
        MISSED,
        ENDED,
        FAILED,
        DIALING,
        IN_PROGRESS,
        REJECTED,
        CANCELLED
    }

    private static class Info {
        boolean answered;
        boolean callMuted;
        long callStartMs;
        java.lang.String callUUID;
        java.lang.String contact;
        long duration;
        boolean incomingCall;
        java.lang.String normalizedNumber;
        java.lang.String number;
        boolean putOnStack;
        boolean userCancelledCall;
        boolean userRejectedCall;

        Info(java.lang.String number2, java.lang.String normalizedNumber2, java.lang.String contact2, boolean incomingCall2, boolean answered2, long callStartMs2, java.lang.String callUUID2, long duration2, boolean userRejectedCall2, boolean userCancelledCall2, boolean putOnStack2, boolean callMuted2) {
            this.number = number2;
            this.normalizedNumber = normalizedNumber2;
            this.contact = contact2;
            this.incomingCall = incomingCall2;
            this.answered = answered2;
            this.callStartMs = callStartMs2;
            this.callUUID = callUUID2;
            this.duration = duration2;
            this.userRejectedCall = userRejectedCall2;
            this.userCancelledCall = userCancelledCall2;
            this.putOnStack = putOnStack2;
            this.callMuted = callMuted2;
        }
    }

    public CallManager(com.squareup.otto.Bus bus2, android.content.Context context) {
        this.bus = bus2;
        this.bus.register(this);
        this.callNotification = new com.navdy.hud.app.framework.phonecall.CallNotification(this, bus2);
    }

    @com.squareup.otto.Subscribe
    public void onPhoneEvent(com.navdy.service.library.events.callcontrol.PhoneEvent event) {
        this.handler.removeCallbacks(this.noResponseCheckRunnable);
        this.lastCallAction = null;
        if (event.status == this.phoneStatus) {
            if (this.dialSet && event.status == com.navdy.service.library.events.callcontrol.PhoneStatus.PHONE_DIALING) {
                this.dialSet = false;
            } else if (android.text.TextUtils.equals(com.navdy.hud.app.util.PhoneUtil.normalizeNumber(event.number), this.normalizedNumber)) {
                return;
            }
        }
        sLogger.v("phoneEvent:" + event);
        com.navdy.service.library.events.DeviceInfo deviceInfo = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getRemoteDeviceInfo();
        com.navdy.service.library.events.callcontrol.PhoneStatus lastStatus = this.phoneStatus;
        this.phoneStatus = event.status;
        long nowMs = android.os.SystemClock.elapsedRealtime();
        switch (event.status) {
            case PHONE_IDLE:
                sLogger.v("IDLE: putOnStack=" + this.putOnStack + " lastStatus=" + lastStatus);
                this.callUUID = null;
                this.callMuted = false;
                if (lastStatus == com.navdy.service.library.events.callcontrol.PhoneStatus.PHONE_RINGING || lastStatus == com.navdy.service.library.events.callcontrol.PhoneStatus.PHONE_DIALING) {
                    this.duration = 0;
                    boolean rejected = false;
                    if (this.userRejectedCall) {
                        this.state = com.navdy.hud.app.framework.phonecall.CallManager.CallNotificationState.REJECTED;
                        rejected = true;
                    } else if (this.userCancelledCall) {
                        this.state = com.navdy.hud.app.framework.phonecall.CallManager.CallNotificationState.CANCELLED;
                    } else if (this.answered) {
                        this.state = com.navdy.hud.app.framework.phonecall.CallManager.CallNotificationState.ENDED;
                    } else {
                        this.state = com.navdy.hud.app.framework.phonecall.CallManager.CallNotificationState.MISSED;
                    }
                    com.navdy.hud.app.framework.phonecall.CallNotification.clearToast();
                    this.userRejectedCall = false;
                    this.userCancelledCall = false;
                    if (this.putOnStack) {
                        removeNotification();
                    } else if (!rejected) {
                        com.navdy.hud.app.framework.notifications.INotification notification = com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().getCurrentNotification();
                        if (notification == null || android.text.TextUtils.equals(notification.getId(), com.navdy.hud.app.framework.notifications.NotificationId.PHONE_CALL_NOTIFICATION_ID)) {
                            removeNotification();
                        } else if (isNotificationAllowed()) {
                            sendNotification();
                        }
                    }
                } else if (lastStatus == com.navdy.service.library.events.callcontrol.PhoneStatus.PHONE_OFFHOOK) {
                    if (deviceInfo == null || deviceInfo.platform != com.navdy.service.library.events.DeviceInfo.Platform.PLATFORM_iOS || android.text.TextUtils.isEmpty(event.number) || android.text.TextUtils.equals(com.navdy.hud.app.util.PhoneUtil.normalizeNumber(event.number), this.normalizedNumber)) {
                        this.duration = toSeconds(nowMs - this.callStartMs);
                        this.state = com.navdy.hud.app.framework.phonecall.CallManager.CallNotificationState.ENDED;
                        if (this.userRejectedCall || this.userCancelledCall || this.putOnStack) {
                            removeNotification();
                        } else if (isNotificationAllowed()) {
                            sendNotification();
                        }
                        this.userRejectedCall = false;
                        this.userCancelledCall = false;
                    } else {
                        sLogger.d("Received end for a different call than the current one");
                        return;
                    }
                }
                this.putOnStack = false;
                this.bus.post(CALL_ENDED);
                if (isNotificationAllowed() && this.number != null) {
                    this.bus.post(new com.navdy.service.library.events.callcontrol.CallEvent(java.lang.Boolean.valueOf(this.incomingCall), java.lang.Boolean.valueOf(this.answered), this.number, this.contact, java.lang.Long.valueOf(toSeconds(this.callStartMs)), java.lang.Long.valueOf(this.duration)));
                    break;
                }
                break;
            case PHONE_RINGING:
                if (lastStatus == com.navdy.service.library.events.callcontrol.PhoneStatus.PHONE_OFFHOOK) {
                    sLogger.v("we are in a call");
                    if (deviceInfo == null || deviceInfo.platform != com.navdy.service.library.events.DeviceInfo.Platform.PLATFORM_iOS) {
                        sLogger.w("stack not implemented on android");
                        return;
                    }
                    this.callStack.push(new com.navdy.hud.app.framework.phonecall.CallManager.Info(this.number, this.normalizedNumber, this.contact, this.incomingCall, this.answered, this.callStartMs, this.callUUID, this.duration, this.userRejectedCall, this.userCancelledCall, true, this.callMuted));
                    sLogger.v("stack pushed [" + this.contact + " , " + this.number + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA + this.normalizedNumber + "]");
                }
                this.callStartMs = nowMs;
                this.incomingCall = true;
                this.answered = false;
                this.number = event.number;
                this.normalizedNumber = com.navdy.hud.app.util.PhoneUtil.normalizeNumber(event.number);
                this.contact = event.contact_name;
                this.callMuted = false;
                if (!android.text.TextUtils.isEmpty(event.callUUID)) {
                    this.callUUID = event.callUUID;
                }
                this.state = com.navdy.hud.app.framework.phonecall.CallManager.CallNotificationState.RINGING;
                if (!isNotificationAllowed()) {
                    sLogger.v("incoming phone call disabled");
                    break;
                } else {
                    this.callNotification.showIncomingCallToast();
                    break;
                }
                break;
            case PHONE_DIALING:
                this.callStartMs = nowMs;
                this.incomingCall = false;
                this.answered = false;
                this.number = event.number;
                this.normalizedNumber = com.navdy.hud.app.util.PhoneUtil.normalizeNumber(event.number);
                this.contact = event.contact_name;
                if (!android.text.TextUtils.isEmpty(event.callUUID)) {
                    this.callUUID = event.callUUID;
                }
                this.state = com.navdy.hud.app.framework.phonecall.CallManager.CallNotificationState.DIALING;
                if (isNotificationAllowed()) {
                    sendNotification();
                    break;
                }
                break;
            case PHONE_OFFHOOK:
                com.navdy.hud.app.framework.phonecall.CallNotification.clearToast();
                this.callStartMs = nowMs;
                this.answered = true;
                if (!android.text.TextUtils.isEmpty(event.callUUID)) {
                    this.callUUID = event.callUUID;
                }
                if (android.text.TextUtils.isEmpty(this.normalizedNumber)) {
                    this.normalizedNumber = com.navdy.hud.app.util.PhoneUtil.normalizeNumber(event.number);
                }
                if (android.text.TextUtils.isEmpty(this.contact)) {
                    this.contact = event.contact_name;
                }
                if (android.text.TextUtils.isEmpty(this.number)) {
                    this.number = event.number;
                }
                this.state = com.navdy.hud.app.framework.phonecall.CallManager.CallNotificationState.IN_PROGRESS;
                if (isNotificationAllowed()) {
                    sendNotification();
                    this.bus.post(CALL_ACCEPTED);
                    break;
                }
                break;
            case PHONE_DISCONNECTING:
                if (this.state == com.navdy.hud.app.framework.phonecall.CallManager.CallNotificationState.DIALING) {
                    this.state = com.navdy.hud.app.framework.phonecall.CallManager.CallNotificationState.FAILED;
                    if (isNotificationAllowed()) {
                        sendNotification();
                        break;
                    }
                }
                break;
        }
        sLogger.v("state:" + this.state);
    }

    public void clearCallStack() {
        this.callStack.clear();
    }

    private long toSeconds(long ms) {
        return (500 + ms) / 1000;
    }

    /* access modifiers changed from: 0000 */
    public void sendCallAction(com.navdy.service.library.events.callcontrol.CallAction callAction, java.lang.String number2) {
        try {
            if (this.state == com.navdy.hud.app.framework.phonecall.CallManager.CallNotificationState.RINGING && callAction == com.navdy.service.library.events.callcontrol.CallAction.CALL_REJECT) {
                this.userRejectedCall = true;
            } else if ((this.state == com.navdy.hud.app.framework.phonecall.CallManager.CallNotificationState.DIALING || this.state == com.navdy.hud.app.framework.phonecall.CallManager.CallNotificationState.IN_PROGRESS) && callAction == com.navdy.service.library.events.callcontrol.CallAction.CALL_END) {
                this.userCancelledCall = true;
            }
            switch (callAction) {
                case CALL_MUTE:
                    this.callMuted = true;
                    break;
                case CALL_UNMUTE:
                    this.callMuted = false;
                    break;
            }
            if (isCallActionResponseRequired(callAction)) {
                this.handler.removeCallbacks(this.noResponseCheckRunnable);
                this.handler.postDelayed(this.noResponseCheckRunnable, 10000);
            }
            this.lastCallAction = callAction;
            this.bus.post(new com.navdy.hud.app.event.RemoteEvent(new com.navdy.service.library.events.callcontrol.TelephonyRequest(callAction, number2, this.callUUID)));
            int size = this.callStack.size();
            sLogger.v("call stack size=" + size + " action=" + callAction.name());
            if (size <= 0) {
                return;
            }
            if (callAction == com.navdy.service.library.events.callcontrol.CallAction.CALL_REJECT) {
                restoreInfo((com.navdy.hud.app.framework.phonecall.CallManager.Info) this.callStack.pop());
            } else if (callAction == com.navdy.service.library.events.callcontrol.CallAction.CALL_END) {
                restoreInfo((com.navdy.hud.app.framework.phonecall.CallManager.Info) this.callStack.pop());
            }
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }

    /* access modifiers changed from: 0000 */
    public void restoreInfo(com.navdy.hud.app.framework.phonecall.CallManager.Info info) {
        this.bus.post(new com.navdy.service.library.events.callcontrol.CallEvent(java.lang.Boolean.valueOf(this.incomingCall), java.lang.Boolean.valueOf(this.answered), this.number, this.contact, java.lang.Long.valueOf(toSeconds(this.callStartMs)), java.lang.Long.valueOf(this.duration)));
        this.number = info.number;
        this.normalizedNumber = info.normalizedNumber;
        this.contact = info.contact;
        this.incomingCall = info.incomingCall;
        this.answered = info.answered;
        this.callStartMs = info.callStartMs;
        this.callUUID = info.callUUID;
        this.duration = info.duration;
        this.userRejectedCall = info.userRejectedCall;
        this.userCancelledCall = info.userCancelledCall;
        this.putOnStack = info.putOnStack;
        this.callMuted = info.callMuted;
        sLogger.v("stack restored [" + this.contact + " , " + this.number + " , " + this.normalizedNumber + "]");
        if (!isIos()) {
            this.handler.postDelayed(new com.navdy.hud.app.framework.phonecall.CallManager.Anon2(), 1000);
        } else if (this.answered) {
            this.phoneStatus = com.navdy.service.library.events.callcontrol.PhoneStatus.PHONE_OFFHOOK;
        }
    }

    /* access modifiers changed from: 0000 */
    public void sendNotification() {
        com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().addNotification(this.callNotification);
    }

    /* access modifiers changed from: 0000 */
    public void removeNotification() {
        com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().removeNotification(this.callNotification.getId());
    }

    public com.navdy.service.library.events.callcontrol.PhoneStatus getPhoneStatus() {
        return this.phoneStatus;
    }

    public void dial(java.lang.String number2, java.lang.String callUUID2, java.lang.String contact2) {
        if (number2 == null) {
            sLogger.e("null number passed contact[" + contact2 + "]");
            return;
        }
        this.state = com.navdy.hud.app.framework.phonecall.CallManager.CallNotificationState.DIALING;
        this.phoneStatus = com.navdy.service.library.events.callcontrol.PhoneStatus.PHONE_DIALING;
        this.dialSet = true;
        this.incomingCall = false;
        this.number = number2;
        this.normalizedNumber = com.navdy.hud.app.util.PhoneUtil.normalizeNumber(number2);
        this.contact = contact2;
        sendNotification();
        sendCallAction(com.navdy.service.library.events.callcontrol.CallAction.CALL_DIAL, number2);
    }

    @com.squareup.otto.Subscribe
    public void onDeviceInfoAvailable(com.navdy.hud.app.event.DeviceInfoAvailable deviceInfoAvailable) {
        if (deviceInfoAvailable.deviceInfo != null && deviceInfoAvailable.deviceInfo.platform == com.navdy.service.library.events.DeviceInfo.Platform.PLATFORM_Android) {
            this.bus.post(new com.navdy.hud.app.event.RemoteEvent(new com.navdy.service.library.events.callcontrol.PhoneStatusRequest()));
        }
    }

    private boolean isIos() {
        com.navdy.hud.app.manager.RemoteDeviceManager remoteDeviceManager = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance();
        return com.navdy.service.library.events.DeviceInfo.Platform.PLATFORM_iOS.equals(remoteDeviceManager.isRemoteDeviceConnected() ? remoteDeviceManager.getRemoteDevicePlatform() : null);
    }

    @com.squareup.otto.Subscribe
    public void onPhoneStatusResponse(com.navdy.service.library.events.callcontrol.PhoneStatusResponse response) {
        sLogger.v("onPhoneStatusResponse [" + response.status + "] [" + response.callStatus + "]");
        if (response.status == com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS && response.callStatus != null) {
            if (this.state == com.navdy.hud.app.framework.phonecall.CallManager.CallNotificationState.IDLE || this.state == com.navdy.hud.app.framework.phonecall.CallManager.CallNotificationState.IN_PROGRESS) {
                sLogger.v("onPhoneStatusResponse fwding event");
                onPhoneEvent(response.callStatus);
            }
        }
    }

    @com.squareup.otto.Subscribe
    public void onPhoneBatteryEvent(com.navdy.service.library.events.callcontrol.PhoneBatteryStatus event) {
        sLogger.v("[Battery-phone] status[" + event.status + "] level[" + event.level + "] charging[" + event.charging + "]");
        com.navdy.hud.app.framework.phonecall.PhoneBatteryNotification.showBatteryToast(event);
    }

    /* access modifiers changed from: private */
    public boolean isCallActionResponseRequired(com.navdy.service.library.events.callcontrol.CallAction callAction) {
        if (callAction == null) {
            return false;
        }
        switch (callAction) {
            case CALL_ACCEPT:
            case CALL_END:
            case CALL_DIAL:
                return true;
            default:
                return false;
        }
    }

    public boolean isCallInProgress() {
        if (this.phoneStatus == com.navdy.service.library.events.callcontrol.PhoneStatus.PHONE_OFFHOOK || this.phoneStatus == com.navdy.service.library.events.callcontrol.PhoneStatus.PHONE_DIALING || this.phoneStatus == com.navdy.service.library.events.callcontrol.PhoneStatus.PHONE_RINGING) {
            return true;
        }
        return false;
    }

    public void disconnect() {
        sLogger.v("got disconnect event, setting to IDLE");
        this.phoneStatus = com.navdy.service.library.events.callcontrol.PhoneStatus.PHONE_IDLE;
        this.state = com.navdy.hud.app.framework.phonecall.CallManager.CallNotificationState.IDLE;
        this.incomingCall = false;
        this.number = null;
        this.normalizedNumber = null;
        this.contact = null;
    }

    /* access modifiers changed from: private */
    public boolean isNotificationAllowed() {
        if ((!this.incomingCall || (this.incomingCall && com.navdy.hud.app.framework.glance.GlanceHelper.isPhoneNotificationEnabled())) && com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().isPhoneNotificationsEnabled()) {
            return true;
        }
        return false;
    }

    public int getCurrentCallDuration() {
        if (this.callStartMs <= 0) {
            return -1;
        }
        return (int) ((android.os.SystemClock.elapsedRealtime() - this.callStartMs) / 1000);
    }
}
