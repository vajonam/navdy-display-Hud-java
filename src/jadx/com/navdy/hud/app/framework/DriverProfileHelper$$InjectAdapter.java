package com.navdy.hud.app.framework;

public final class DriverProfileHelper$$InjectAdapter extends dagger.internal.Binding<com.navdy.hud.app.framework.DriverProfileHelper> implements dagger.MembersInjector<com.navdy.hud.app.framework.DriverProfileHelper> {
    private dagger.internal.Binding<com.navdy.hud.app.profile.DriverProfileManager> driverProfileManager;
    private dagger.internal.Binding<android.content.SharedPreferences> globalPreferences;

    public DriverProfileHelper$$InjectAdapter() {
        super(null, "members/com.navdy.hud.app.framework.DriverProfileHelper", false, com.navdy.hud.app.framework.DriverProfileHelper.class);
    }

    public void attach(dagger.internal.Linker linker) {
        this.driverProfileManager = linker.requestBinding("com.navdy.hud.app.profile.DriverProfileManager", com.navdy.hud.app.framework.DriverProfileHelper.class, getClass().getClassLoader());
        this.globalPreferences = linker.requestBinding("android.content.SharedPreferences", com.navdy.hud.app.framework.DriverProfileHelper.class, getClass().getClassLoader());
    }

    public void getDependencies(java.util.Set<dagger.internal.Binding<?>> set, java.util.Set<dagger.internal.Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.driverProfileManager);
        injectMembersBindings.add(this.globalPreferences);
    }

    public void injectMembers(com.navdy.hud.app.framework.DriverProfileHelper object) {
        object.driverProfileManager = (com.navdy.hud.app.profile.DriverProfileManager) this.driverProfileManager.get();
        object.globalPreferences = (android.content.SharedPreferences) this.globalPreferences.get();
    }
}
