package com.navdy.hud.app.framework;

public class DriverProfileHelper {
    private static final com.navdy.hud.app.framework.DriverProfileHelper sInstance = new com.navdy.hud.app.framework.DriverProfileHelper();
    @javax.inject.Inject
    com.navdy.hud.app.profile.DriverProfileManager driverProfileManager;
    @javax.inject.Inject
    android.content.SharedPreferences globalPreferences;

    private DriverProfileHelper() {
        mortar.Mortar.inject(com.navdy.hud.app.HudApplication.getAppContext(), this);
    }

    public static com.navdy.hud.app.framework.DriverProfileHelper getInstance() {
        return sInstance;
    }

    public com.navdy.hud.app.profile.DriverProfile getCurrentProfile() {
        return this.driverProfileManager.getCurrentProfile();
    }

    public com.navdy.hud.app.profile.DriverProfileManager getDriverProfileManager() {
        return this.driverProfileManager;
    }

    public java.util.Locale getCurrentLocale() {
        return this.driverProfileManager.getCurrentLocale();
    }

    public android.content.SharedPreferences getGlobalPreferences() {
        return this.globalPreferences;
    }

    public android.content.SharedPreferences getCurrentProfilePreferences() {
        return getCurrentProfile().isDefaultProfile() ? this.globalPreferences : this.driverProfileManager.getLocalPreferencesForCurrentDriverProfile(com.navdy.hud.app.HudApplication.getAppContext());
    }
}
