package com.navdy.hud.app.framework.toast;

public final class ToastPresenter {
    public static final int ANIMATION_TRANSLATION;
    private static final java.util.List<java.lang.String> DISMISS_CHOICE = new java.util.ArrayList();
    public static final java.lang.String EXTRA_CHOICE_LAYOUT_PADDING = "15";
    public static final java.lang.String EXTRA_CHOICE_LIST = "20";
    public static final java.lang.String EXTRA_DEFAULT_CHOICE = "12";
    public static final java.lang.String EXTRA_INFO_CONTAINER_DEFAULT_MAX_WIDTH = "16_1";
    public static final java.lang.String EXTRA_INFO_CONTAINER_LEFT_PADDING = "16_2";
    public static final java.lang.String EXTRA_INFO_CONTAINER_MAX_WIDTH = "16";
    public static final java.lang.String EXTRA_MAIN_IMAGE = "8";
    public static final java.lang.String EXTRA_MAIN_IMAGE_CACHE_KEY = "9";
    public static final java.lang.String EXTRA_MAIN_IMAGE_INITIALS = "10";
    public static final java.lang.String EXTRA_MAIN_TITLE = "1";
    public static final java.lang.String EXTRA_MAIN_TITLE_1 = "2";
    public static final java.lang.String EXTRA_MAIN_TITLE_1_STYLE = "3";
    public static final java.lang.String EXTRA_MAIN_TITLE_2 = "4";
    public static final java.lang.String EXTRA_MAIN_TITLE_2_STYLE = "5";
    public static final java.lang.String EXTRA_MAIN_TITLE_3 = "6";
    public static final java.lang.String EXTRA_MAIN_TITLE_3_STYLE = "7";
    public static final java.lang.String EXTRA_MAIN_TITLE_STYLE = "1_1";
    public static final java.lang.String EXTRA_NO_START_DELAY = "21";
    public static final java.lang.String EXTRA_SHOW_SCREEN_ID = "18";
    public static final java.lang.String EXTRA_SIDE_IMAGE = "11";
    public static final java.lang.String EXTRA_SUPPORTS_GESTURE = "19";
    public static final java.lang.String EXTRA_TIMEOUT = "13";
    public static final java.lang.String EXTRA_TOAST_ID = "id";
    public static final java.lang.String EXTRA_TTS = "17";
    public static final int TOAST_ANIM_DURATION_IN = 100;
    public static final int TOAST_ANIM_DURATION_OUT = 100;
    public static final int TOAST_ANIM_DURATION_OUT_QUICK = 50;
    public static final int TOAST_START_DELAY = 250;
    private static final com.squareup.otto.Bus bus;
    private static com.navdy.hud.app.ui.component.ChoiceLayout.IListener choiceListener = new com.navdy.hud.app.framework.toast.ToastPresenter.Anon3();
    private static com.navdy.hud.app.framework.toast.ToastManager.ToastInfo currentInfo;
    private static com.navdy.hud.app.ui.component.ChoiceLayout.IListener defaultChoiceListener = new com.navdy.hud.app.framework.toast.ToastPresenter.Anon2();
    public static final int defaultMaxInfoWidth;
    private static com.navdy.hud.app.device.light.LED.Settings gestureLedSettings;
    private static final com.navdy.hud.app.gesture.GestureServiceConnector gestureServiceConnector;
    private static android.os.Handler handler = new android.os.Handler(android.os.Looper.getMainLooper());
    private static java.lang.String id;
    private static int mainSectionBottomPadding = -1;
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.framework.toast.ToastPresenter.class);
    private static volatile java.lang.String screenName;
    private static boolean supportsGesture;
    private static int timeout;
    private static java.lang.Runnable timeoutRunnable = new com.navdy.hud.app.framework.toast.ToastPresenter.Anon1();
    /* access modifiers changed from: private */
    public static com.navdy.hud.app.framework.toast.IToastCallback toastCallback;
    /* access modifiers changed from: private */
    public static com.navdy.hud.app.view.ToastView toastView;

    static class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            if (com.navdy.hud.app.framework.toast.ToastPresenter.toastView != null) {
                com.navdy.hud.app.framework.toast.ToastPresenter.sLogger.v("timeoutRunnable");
                com.navdy.hud.app.framework.toast.ToastPresenter.toastView.animateOut(false);
            }
        }
    }

    static class Anon2 implements com.navdy.hud.app.ui.component.ChoiceLayout.IListener {
        Anon2() {
        }

        public void executeItem(int pos, int id) {
            com.navdy.hud.app.framework.toast.ToastPresenter.dismiss(false);
        }

        public void itemSelected(int pos, int id) {
        }
    }

    static class Anon3 implements com.navdy.hud.app.ui.component.ChoiceLayout.IListener {
        Anon3() {
        }

        public void executeItem(int pos, int id) {
            if (com.navdy.hud.app.framework.toast.ToastPresenter.toastCallback != null) {
                com.navdy.hud.app.framework.toast.ToastPresenter.toastCallback.executeChoiceItem(pos, id);
            }
        }

        public void itemSelected(int pos, int id) {
            com.navdy.hud.app.framework.toast.ToastPresenter.resetTimeout();
        }
    }

    static class Anon4 implements java.lang.Runnable {
        final /* synthetic */ boolean val$quickly;

        Anon4(boolean z) {
            this.val$quickly = z;
        }

        public void run() {
            com.navdy.hud.app.framework.toast.ToastPresenter.dismissInternal(this.val$quickly);
        }
    }

    static class Anon5 implements java.lang.Runnable {
        Anon5() {
        }

        public void run() {
            com.navdy.hud.app.framework.toast.ToastPresenter.clearInternal();
        }
    }

    static {
        android.content.res.Resources resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        DISMISS_CHOICE.add(resources.getString(com.navdy.hud.app.R.string.dismiss));
        ANIMATION_TRANSLATION = (int) resources.getDimension(com.navdy.hud.app.R.dimen.toast_anim_translation);
        defaultMaxInfoWidth = (int) resources.getDimension(com.navdy.hud.app.R.dimen.carousel_main_right_section_width);
        com.navdy.hud.app.manager.RemoteDeviceManager remoteDeviceManager = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance();
        bus = remoteDeviceManager.getBus();
        gestureServiceConnector = remoteDeviceManager.getGestureServiceConnector();
    }

    private ToastPresenter() {
    }

    public static void dismiss() {
        dismiss(false);
    }

    public static void dismiss(boolean quickly) {
        if (com.navdy.hud.app.util.GenericUtil.isMainThread()) {
            dismissInternal(quickly);
        } else {
            handler.post(new com.navdy.hud.app.framework.toast.ToastPresenter.Anon4(quickly));
        }
    }

    /* access modifiers changed from: private */
    public static void dismissInternal(boolean quickly) {
        sLogger.v("dismissInternal");
        handler.removeCallbacks(timeoutRunnable);
        if (toastView != null) {
            toastView.animateOut(quickly);
        }
    }

    static void present(com.navdy.hud.app.view.ToastView view, com.navdy.hud.app.framework.toast.ToastManager.ToastInfo info) {
        if (view == null || info == null) {
            sLogger.e("invalid toast info");
            return;
        }
        currentInfo = info;
        toastView = view;
        if (mainSectionBottomPadding == -1) {
            mainSectionBottomPadding = toastView.getMainLayout().mainSection.getPaddingBottom();
        }
        updateView(view, info);
    }

    public static void clear() {
        if (com.navdy.hud.app.util.GenericUtil.isMainThread()) {
            clearInternal();
        } else {
            handler.post(new com.navdy.hud.app.framework.toast.ToastPresenter.Anon5());
        }
    }

    /* access modifiers changed from: private */
    public static void clearInternal() {
        sLogger.v("clearInternal");
        handler.removeCallbacks(timeoutRunnable);
        if (toastCallback != null) {
            sLogger.v("called onStop():" + id);
            toastCallback.onStop();
            if (supportsGesture) {
                com.navdy.hud.app.device.light.HUDLightUtils.removeSettings(gestureLedSettings);
            }
        }
        bus.post(new com.navdy.hud.app.framework.toast.ToastManager.DismissedToast(id));
        currentInfo = null;
        supportsGesture = false;
        toastCallback = null;
        sLogger.v("toast-cb null");
        toastView = null;
        id = null;
        timeout = 0;
    }

    public static void resetTimeout() {
        if (timeout > 0) {
            handler.removeCallbacks(timeoutRunnable);
            handler.postDelayed(timeoutRunnable, (long) timeout);
            sLogger.v("reset timeout = " + timeout);
        }
    }

    public static com.navdy.hud.app.framework.toast.IToastCallback getCurrentCallback() {
        return toastCallback;
    }

    public static java.lang.String getCurrentId() {
        return id;
    }

    public static boolean hasTimeout() {
        return timeout > 0;
    }

    private static void updateView(com.navdy.hud.app.view.ToastView view, com.navdy.hud.app.framework.toast.ToastManager.ToastInfo info) {
        android.content.Context context = com.navdy.hud.app.HudApplication.getAppContext();
        android.os.Bundle data = info.toastParams.data;
        com.navdy.hud.app.ui.component.ConfirmationLayout layout = view.getConfirmation();
        layout.screenTitle.setTextAppearance(context, com.navdy.hud.app.R.style.mainTitle);
        layout.title1.setTextAppearance(context, com.navdy.hud.app.R.style.title1);
        layout.title2.setTextAppearance(context, com.navdy.hud.app.R.style.title2);
        layout.title3.setTextAppearance(context, com.navdy.hud.app.R.style.title3);
        int padding = data.getInt(EXTRA_CHOICE_LAYOUT_PADDING, 0);
        if (padding > 0) {
            android.widget.RelativeLayout.LayoutParams params = (android.widget.RelativeLayout.LayoutParams) layout.choiceLayout.getLayoutParams();
            params.topMargin = 0;
            params.leftMargin = 0;
            params.rightMargin = 0;
            params.bottomMargin = padding;
        }
        id = data.getString("id");
        toastCallback = com.navdy.hud.app.framework.toast.ToastManager.getInstance().getCallback(id);
        sLogger.v(new java.lang.StringBuilder().append("toast-cb-update :").append(toastCallback).toString() == null ? com.amazonaws.services.s3.internal.Constants.NULL_VERSION_ID : "not null");
        int mainImage = data.getInt(EXTRA_MAIN_IMAGE, 0);
        java.lang.String mainImagePath = data.getString(EXTRA_MAIN_IMAGE_CACHE_KEY);
        java.lang.String initials = data.getString(EXTRA_MAIN_IMAGE_INITIALS);
        timeout = data.getInt(EXTRA_TIMEOUT, 0);
        boolean defaultChoice = data.getBoolean(EXTRA_DEFAULT_CHOICE, false);
        int sideImage = data.getInt(EXTRA_SIDE_IMAGE, 0);
        java.lang.String tts = null;
        screenName = data.getString(EXTRA_SHOW_SCREEN_ID, null);
        supportsGesture = data.getBoolean(EXTRA_SUPPORTS_GESTURE);
        if (supportsGesture && !gestureServiceConnector.isRunning()) {
            sLogger.v("gesture not available, turn off");
            supportsGesture = false;
        }
        java.util.ArrayList<com.navdy.hud.app.ui.component.ChoiceLayout.Choice> choices = data.getParcelableArrayList(EXTRA_CHOICE_LIST);
        com.navdy.hud.app.view.MaxWidthLinearLayout infoContainer = (com.navdy.hud.app.view.MaxWidthLinearLayout) layout.findViewById(com.navdy.hud.app.R.id.infoContainer);
        int maxWidth = 0;
        if (data.containsKey(EXTRA_INFO_CONTAINER_MAX_WIDTH)) {
            int infoContainerWidth = data.getInt(EXTRA_INFO_CONTAINER_MAX_WIDTH);
            maxWidth = infoContainerWidth > 0 ? infoContainerWidth : 0;
        } else if (data.containsKey(EXTRA_INFO_CONTAINER_DEFAULT_MAX_WIDTH)) {
            maxWidth = defaultMaxInfoWidth;
        }
        infoContainer.setMaxWidth(maxWidth);
        ((android.view.ViewGroup.MarginLayoutParams) infoContainer.getLayoutParams()).width = -2;
        if (data.containsKey(EXTRA_INFO_CONTAINER_LEFT_PADDING)) {
            infoContainer.setPadding(data.getInt(EXTRA_INFO_CONTAINER_LEFT_PADDING), infoContainer.getPaddingTop(), infoContainer.getPaddingRight(), infoContainer.getPaddingBottom());
        }
        layout.fluctuatorView.setVisibility(8);
        int lines = 0 + applyTextAndStyle(layout.screenTitle, data, EXTRA_MAIN_TITLE, EXTRA_MAIN_TITLE_STYLE, 1) + applyTextAndStyle(layout.title1, data, EXTRA_MAIN_TITLE_1, EXTRA_MAIN_TITLE_1_STYLE, 1) + applyTextAndStyle(layout.title2, data, EXTRA_MAIN_TITLE_2, EXTRA_MAIN_TITLE_2_STYLE, 1) + applyTextAndStyle(layout.title3, data, EXTRA_MAIN_TITLE_3, EXTRA_MAIN_TITLE_3_STYLE, 3);
        android.view.ViewGroup viewGroup = layout.mainSection;
        if (lines > 4) {
            com.navdy.hud.app.util.ViewUtil.setBottomPadding(layout.mainSection, mainSectionBottomPadding + context.getResources().getDimensionPixelOffset(com.navdy.hud.app.R.dimen.main_section_vertical_padding));
        } else {
            com.navdy.hud.app.util.ViewUtil.setBottomPadding(layout.mainSection, mainSectionBottomPadding);
        }
        if (mainImage != 0) {
            if (initials != null) {
                layout.screenImage.setImage(mainImage, initials, com.navdy.hud.app.ui.component.image.InitialsImageView.Style.LARGE);
                layout.screenImage.setVisibility(0);
            } else {
                layout.screenImage.setImage(mainImage, null, com.navdy.hud.app.ui.component.image.InitialsImageView.Style.DEFAULT);
                layout.screenImage.setVisibility(0);
            }
        } else if (mainImagePath != null) {
            java.io.File file = new java.io.File(mainImagePath);
            android.graphics.Bitmap bitmap = com.navdy.hud.app.util.picasso.PicassoUtil.getBitmapfromCache(file);
            if (bitmap != null) {
                layout.screenImage.setInitials(null, com.navdy.hud.app.ui.component.image.InitialsImageView.Style.DEFAULT);
                layout.screenImage.setImageBitmap(bitmap);
                layout.screenImage.setVisibility(0);
            }
        } else {
            layout.screenImage.setImageResource(0);
            layout.screenImage.setVisibility(8);
        }
        if (sideImage != 0) {
            layout.sideImage.setImageResource(sideImage);
            layout.sideImage.setVisibility(0);
        } else {
            layout.sideImage.setVisibility(8);
        }
        if (defaultChoice) {
            layout.setChoices(DISMISS_CHOICE, 0, defaultChoiceListener);
            layout.choiceLayout.setVisibility(0);
            supportsGesture = false;
        } else if (choices == null) {
            supportsGesture = false;
            layout.choiceLayout.setVisibility(8);
        } else if (!supportsGesture || choices.size() <= 2) {
            layout.setChoicesList(choices, 0, choiceListener);
            layout.choiceLayout.setVisibility(0);
        } else {
            throw new java.lang.RuntimeException("max 2 choice allowed when gesture are on");
        }
        if (supportsGesture) {
            layout.leftSwipe.setVisibility(0);
            layout.rightSwipe.setVisibility(0);
            toastView.gestureOn = true;
        } else {
            layout.leftSwipe.setVisibility(8);
            layout.rightSwipe.setVisibility(8);
            toastView.gestureOn = defaultChoice;
        }
        if (timeout > 0) {
            handler.postDelayed(timeoutRunnable, (long) (timeout + 100));
            sLogger.v("timeout = " + (timeout + 100));
        }
        if (toastCallback != null) {
            sLogger.v("called onStart():" + id);
            toastCallback.onStart(view);
            if (supportsGesture) {
                gestureLedSettings = com.navdy.hud.app.device.light.HUDLightUtils.showGestureDetectionEnabled(com.navdy.hud.app.HudApplication.getAppContext(), com.navdy.hud.app.device.light.LightManager.getInstance(), "Toast" + data.getString(EXTRA_MAIN_TITLE));
            }
        }
        if (0 != 0) {
            if (info.ttsDone) {
                tts = null;
            } else {
                info.ttsDone = true;
            }
        }
        view.animateIn(tts, screenName, id, data.getBoolean(EXTRA_NO_START_DELAY, false));
    }

    private static int applyTextAndStyle(android.widget.TextView view, android.os.Bundle bundle, java.lang.String textAttribute, java.lang.String styleAttribute, int maxLines) {
        return com.navdy.hud.app.util.ViewUtil.applyTextAndStyle(view, bundle.getString(textAttribute), maxLines, bundle.getInt(styleAttribute, -1));
    }

    public static void clearScreenName() {
        screenName = null;
    }

    public static java.lang.String getScreenName() {
        return screenName;
    }
}
