package com.navdy.hud.app.framework.toast;

public interface IToastCallback {
    void executeChoiceItem(int i, int i2);

    boolean onKey(com.navdy.hud.app.manager.InputManager.CustomKeyEvent customKeyEvent);

    void onStart(com.navdy.hud.app.view.ToastView toastView);

    void onStop();
}
