package com.navdy.hud.app.framework.toast;

public class ToastManager {
    private static final com.navdy.hud.app.framework.toast.ToastManager sInstance = new com.navdy.hud.app.framework.toast.ToastManager();
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.framework.toast.ToastManager.class);
    private com.squareup.otto.Bus bus;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.framework.toast.ToastManager.ToastInfo currentToast;
    private android.os.Handler handler = new android.os.Handler(android.os.Looper.getMainLooper());
    /* access modifiers changed from: private */
    public boolean isToastDisplayDisabled = false;
    /* access modifiers changed from: private */
    public boolean isToastScreenDisplayed;
    private java.lang.Runnable nextToast = new com.navdy.hud.app.framework.toast.ToastManager.Anon1();
    /* access modifiers changed from: private */
    public java.util.concurrent.LinkedBlockingDeque<com.navdy.hud.app.framework.toast.ToastManager.ToastInfo> queue = new java.util.concurrent.LinkedBlockingDeque<>();
    private com.navdy.hud.app.ui.framework.UIStateManager uiStateManager;

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            synchronized (com.navdy.hud.app.framework.toast.ToastManager.this.queue) {
                if (com.navdy.hud.app.framework.toast.ToastManager.this.queue.size() == 0) {
                    com.navdy.hud.app.framework.toast.ToastManager.sLogger.v("no pending toast");
                } else if (com.navdy.hud.app.framework.toast.ToastManager.this.isToastScreenDisplayed) {
                    com.navdy.hud.app.framework.toast.ToastManager.sLogger.v("cannot display toast screen still on");
                } else if (com.navdy.hud.app.framework.toast.ToastManager.this.isToastDisplayDisabled) {
                    com.navdy.hud.app.framework.toast.ToastManager.sLogger.v("toasts disabled");
                } else {
                    com.navdy.hud.app.framework.toast.ToastManager.ToastInfo info = (com.navdy.hud.app.framework.toast.ToastManager.ToastInfo) com.navdy.hud.app.framework.toast.ToastManager.this.queue.remove();
                    com.navdy.hud.app.framework.toast.ToastManager.this.currentToast = info;
                    com.navdy.hud.app.framework.toast.ToastManager.this.showToast(info);
                }
            }
        }
    }

    class Anon2 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.hud.app.framework.toast.ToastManager.ToastParams val$toastParams;

        Anon2(com.navdy.hud.app.framework.toast.ToastManager.ToastParams toastParams) {
            this.val$toastParams = toastParams;
        }

        public void run() {
            com.navdy.hud.app.framework.toast.ToastManager.this.addToastInternal(this.val$toastParams);
        }
    }

    class Anon3 implements java.lang.Runnable {
        final /* synthetic */ boolean val$disable;
        final /* synthetic */ java.lang.String val$id;

        Anon3(java.lang.String str, boolean z) {
            this.val$id = str;
            this.val$disable = z;
        }

        public void run() {
            com.navdy.hud.app.framework.toast.ToastManager.this.dismissCurrentToastInternal(this.val$id, this.val$disable);
        }
    }

    class Anon4 implements java.lang.Runnable {
        Anon4() {
        }

        public void run() {
            com.navdy.hud.app.framework.toast.ToastManager.this.clearPendingToastInternal(null);
        }
    }

    class Anon5 implements java.lang.Runnable {
        final /* synthetic */ java.lang.String val$id;

        Anon5(java.lang.String str) {
            this.val$id = str;
        }

        public void run() {
            com.navdy.hud.app.framework.toast.ToastManager.this.clearPendingToastInternal(this.val$id);
        }
    }

    public static class DismissedToast {
        public java.lang.String name;

        public DismissedToast(java.lang.String name2) {
            this.name = name2;
        }
    }

    public static class ShowToast {
        public java.lang.String name;

        public ShowToast(java.lang.String name2) {
            this.name = name2;
        }
    }

    public static class ToastInfo {
        boolean dismissed;
        com.navdy.hud.app.framework.toast.ToastManager.ToastParams toastParams;
        boolean ttsDone;

        ToastInfo(com.navdy.hud.app.framework.toast.ToastManager.ToastParams toastParams2) {
            this.toastParams = toastParams2;
        }
    }

    public static class ToastParams {
        com.navdy.hud.app.framework.toast.IToastCallback cb;
        android.os.Bundle data;
        java.lang.String id;
        boolean lock;
        boolean makeCurrent;
        boolean removeOnDisable;

        public ToastParams(java.lang.String id2, android.os.Bundle data2, com.navdy.hud.app.framework.toast.IToastCallback cb2, boolean removeOnDisable2, boolean makeCurrent2) {
            this(id2, data2, cb2, removeOnDisable2, makeCurrent2, false);
        }

        public ToastParams(java.lang.String id2, android.os.Bundle data2, com.navdy.hud.app.framework.toast.IToastCallback cb2, boolean removeOnDisable2, boolean makeCurrent2, boolean lock2) {
            this.id = id2;
            this.data = data2;
            this.cb = cb2;
            this.removeOnDisable = removeOnDisable2;
            this.makeCurrent = makeCurrent2;
            this.lock = lock2;
        }
    }

    public static com.navdy.hud.app.framework.toast.ToastManager getInstance() {
        return sInstance;
    }

    public void setToastDisplayFlag(boolean b) {
        this.isToastScreenDisplayed = b;
        if (!this.isToastScreenDisplayed) {
            this.currentToast = null;
            this.handler.removeCallbacks(this.nextToast);
            this.handler.post(this.nextToast);
        }
    }

    public boolean isToastDisplayed() {
        return this.isToastScreenDisplayed;
    }

    private ToastManager() {
        com.navdy.hud.app.manager.RemoteDeviceManager remoteDeviceManager = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance();
        this.bus = remoteDeviceManager.getBus();
        this.uiStateManager = remoteDeviceManager.getUiStateManager();
    }

    public void addToast(com.navdy.hud.app.framework.toast.ToastManager.ToastParams toastParams) {
        if (toastParams == null) {
            sLogger.w("invalid toast params");
        } else if (com.navdy.hud.app.util.GenericUtil.isMainThread()) {
            addToastInternal(toastParams);
        } else {
            this.handler.post(new com.navdy.hud.app.framework.toast.ToastManager.Anon2(toastParams));
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:49:?, code lost:
        return;
     */
    public void addToastInternal(com.navdy.hud.app.framework.toast.ToastManager.ToastParams toastParams) {
        if (toastParams.id == null || toastParams.data == null) {
            sLogger.w("invalid toast passed");
        } else if (this.uiStateManager.getRootScreen() == null) {
            sLogger.w("toast [" + toastParams.id + "] not accepted, Main screen not on");
        } else if (isLocked()) {
            sLogger.v("locked, cannot add toast[" + toastParams.id + "]");
        } else {
            toastParams.data.putString("id", toastParams.id);
            com.navdy.hud.app.framework.toast.ToastManager.ToastInfo info = new com.navdy.hud.app.framework.toast.ToastManager.ToastInfo(toastParams);
            synchronized (this.queue) {
                if (toastParams.makeCurrent) {
                    if (this.currentToast == null || this.currentToast.dismissed) {
                        sLogger.v("replace current not reqd");
                        this.queue.addFirst(info);
                        if (!this.isToastScreenDisplayed) {
                            sLogger.v("replace called next:run");
                            this.nextToast.run();
                        }
                    } else {
                        this.queue.addFirst(this.currentToast);
                        sLogger.v("replace current, item to front:" + this.currentToast.toastParams.id);
                        this.queue.addFirst(info);
                        dismissCurrentToast();
                    }
                } else if (this.queue.size() > 0 || this.isToastScreenDisplayed || this.currentToast != null) {
                    sLogger.v("[" + toastParams.id + "] wait for display to be cleared size[" + this.queue.size() + "] current[" + this.currentToast + "] displayed[" + this.isToastScreenDisplayed + "]");
                    this.queue.add(info);
                } else if (this.isToastDisplayDisabled) {
                    sLogger.v("[" + toastParams.id + "] toasts disabled");
                    this.queue.add(info);
                } else {
                    this.currentToast = info;
                    sLogger.v("push toast to display");
                    showToast(info);
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void showToast(com.navdy.hud.app.framework.toast.ToastManager.ToastInfo info) {
        sLogger.v("[show-toast] id[" + info.toastParams.id + "]");
        com.navdy.hud.app.framework.toast.ToastPresenter.present(this.uiStateManager.getToastView(), info);
    }

    public com.navdy.hud.app.framework.toast.IToastCallback getCallback(java.lang.String id) {
        if (id == null || this.currentToast == null || !android.text.TextUtils.equals(id, this.currentToast.toastParams.id)) {
            return null;
        }
        return this.currentToast.toastParams.cb;
    }

    public boolean isCurrentToast(java.lang.String id) {
        if (id == null || this.currentToast == null || !android.text.TextUtils.equals(this.currentToast.toastParams.id, id)) {
            return false;
        }
        return true;
    }

    public java.lang.String getCurrentToastId() {
        if (this.currentToast == null) {
            return null;
        }
        return this.currentToast.toastParams.id;
    }

    public void dismissCurrentToast(java.lang.String... args) {
        for (java.lang.String s : args) {
            dismissCurrentToast(s);
        }
    }

    public void dismissToast(java.lang.String id) {
        dismissCurrentToast(id);
        clearPendingToast(id);
    }

    public void dismissCurrentToast() {
        dismissCurrentToast(getCurrentToastId(), false);
    }

    /* access modifiers changed from: 0000 */
    public void dismissCurrentToast(boolean disable) {
        dismissCurrentToast(getCurrentToastId(), disable);
    }

    public void dismissCurrentToast(java.lang.String id) {
        dismissCurrentToast(id, false);
    }

    private void dismissCurrentToast(java.lang.String id, boolean disable) {
        if (com.navdy.hud.app.util.GenericUtil.isMainThread()) {
            dismissCurrentToastInternal(id, disable);
        } else {
            this.handler.post(new com.navdy.hud.app.framework.toast.ToastManager.Anon3(id, disable));
        }
    }

    /* access modifiers changed from: private */
    public void dismissCurrentToastInternal(java.lang.String id, boolean disable) {
        if (this.currentToast != null) {
            if (id == null || android.text.TextUtils.equals(id, this.currentToast.toastParams.id)) {
                if (!disable || !this.currentToast.toastParams.removeOnDisable) {
                    this.currentToast.dismissed = true;
                    sLogger.v("dismiss current toast[" + id + "] saved");
                } else {
                    this.currentToast = null;
                    sLogger.v("dismiss current toast[" + id + "] removed");
                }
                com.navdy.hud.app.framework.toast.ToastPresenter.dismiss();
                return;
            }
            sLogger.v("current toast[" + this.currentToast.toastParams.id + "] does not match[" + id + "]");
        }
    }

    public void clearPendingToast(java.lang.String... args) {
        for (java.lang.String s : args) {
            clearPendingToast(s);
        }
    }

    public void clearAllPendingToast() {
        if (com.navdy.hud.app.util.GenericUtil.isMainThread()) {
            clearPendingToastInternal(null);
        } else {
            this.handler.post(new com.navdy.hud.app.framework.toast.ToastManager.Anon4());
        }
    }

    public void clearPendingToast(java.lang.String id) {
        if (com.navdy.hud.app.util.GenericUtil.isMainThread()) {
            clearPendingToastInternal(id);
        } else {
            this.handler.post(new com.navdy.hud.app.framework.toast.ToastManager.Anon5(id));
        }
    }

    /* access modifiers changed from: private */
    public void clearPendingToastInternal(java.lang.String id) {
        synchronized (this.queue) {
            if (id == null) {
                sLogger.v("removed-all:" + this.queue.size());
                this.queue.clear();
                this.currentToast = null;
                this.isToastScreenDisplayed = false;
            } else {
                java.util.Iterator<com.navdy.hud.app.framework.toast.ToastManager.ToastInfo> iterator = this.queue.iterator();
                while (iterator.hasNext()) {
                    java.lang.String toastId = ((com.navdy.hud.app.framework.toast.ToastManager.ToastInfo) iterator.next()).toastParams.id;
                    if (android.text.TextUtils.equals(id, toastId)) {
                        sLogger.v("removed:" + toastId);
                        iterator.remove();
                    }
                }
            }
        }
    }

    public void disableToasts(boolean disable) {
        sLogger.v("disableToast [" + disable + "]");
        if (this.isToastDisplayDisabled == disable) {
            sLogger.v("disableToast same state as before");
            return;
        }
        this.isToastDisplayDisabled = disable;
        if (!disable) {
            synchronized (this.queue) {
                if (this.currentToast != null) {
                    showToast(this.currentToast);
                } else {
                    this.handler.post(this.nextToast);
                }
            }
        } else if (isLocked()) {
            sLogger.v("disableToast locked, cannot disable toast");
        } else {
            dismissCurrentToast(true);
        }
    }

    private boolean isLocked() {
        if (this.currentToast != null) {
            return this.currentToast.toastParams.lock;
        }
        return false;
    }
}
