package com.navdy.hud.app.framework.voice;

public class VoiceAssistNotification implements com.navdy.hud.app.framework.notifications.INotification {
    private static final int DEFAULT_VOICE_ASSIST_TIMEOUT = 4000;
    private static final java.lang.String EMPTY = "";
    private static final int SIRI_STARTING_TIMEOUT = 5000;
    private static final int SIRI_TIMEOUT = 200000;
    private static final com.squareup.otto.Bus bus = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getBus();
    private static int gnow_color;
    private static java.lang.String google_now;
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.framework.voice.VoiceAssistNotification.class);
    private static java.lang.String siri;
    private static java.lang.String siriIsActive;
    private static java.lang.String siriIsStarting;
    private static int siri_color;
    /* access modifiers changed from: private */
    public static java.lang.String voice_assist_disabled;
    private static java.lang.String voice_assist_na;
    private com.navdy.hud.app.framework.notifications.INotificationController controller;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.ui.component.FluctuatorAnimatorView fluctuator;
    /* access modifiers changed from: private */
    public android.widget.ImageView image;
    /* access modifiers changed from: private */
    public android.view.ViewGroup layout;
    private android.os.Handler mSiriTimeoutHandler;
    private java.lang.Runnable mSiriTimeoutRunnable;
    private android.widget.TextView status;
    /* access modifiers changed from: private */
    public android.widget.TextView subStatus;
    private boolean waitingAfterTrigger = false;

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            if (com.navdy.hud.app.framework.voice.VoiceAssistNotification.this.layout != null) {
                com.navdy.hud.app.framework.voice.VoiceAssistNotification.sLogger.d("Siri request did not get any response and timed out");
                com.navdy.hud.app.framework.voice.VoiceAssistNotification.this.fluctuator.setVisibility(4);
                com.navdy.hud.app.framework.voice.VoiceAssistNotification.this.fluctuator.stop();
                com.navdy.hud.app.framework.voice.VoiceAssistNotification.this.subStatus.setText(com.navdy.hud.app.framework.voice.VoiceAssistNotification.voice_assist_disabled);
                com.navdy.hud.app.framework.voice.VoiceAssistNotification.this.image.setImageResource(com.navdy.hud.app.R.drawable.icon_siri);
            }
        }
    }

    static {
        android.content.res.Resources resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        google_now = resources.getString(com.navdy.hud.app.R.string.google_now);
        siri = resources.getString(com.navdy.hud.app.R.string.siri);
        voice_assist_na = resources.getString(com.navdy.hud.app.R.string.voice_assist_not_available);
        voice_assist_disabled = resources.getString(com.navdy.hud.app.R.string.voice_assist_disabled);
        siriIsActive = resources.getString(com.navdy.hud.app.R.string.siri_active);
        siriIsStarting = resources.getString(com.navdy.hud.app.R.string.siri_starting);
        gnow_color = resources.getColor(com.navdy.hud.app.R.color.grey_fluctuator);
        siri_color = resources.getColor(17170443);
    }

    public com.navdy.hud.app.framework.notifications.NotificationType getType() {
        return com.navdy.hud.app.framework.notifications.NotificationType.VOICE_ASSIST;
    }

    public java.lang.String getId() {
        return com.navdy.hud.app.framework.notifications.NotificationId.VOICE_ASSIST_NOTIFICATION_ID;
    }

    public android.view.View getExpandedView(android.content.Context context, java.lang.Object data) {
        return null;
    }

    public int getExpandedViewIndicatorColor() {
        return 0;
    }

    public void onStart(com.navdy.hud.app.framework.notifications.INotificationController controller2) {
        if (this.layout != null) {
            this.controller = controller2;
            bus.register(this);
            int color = 0;
            com.navdy.hud.app.manager.RemoteDeviceManager remoteDeviceManager = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance();
            boolean deviceConnected = remoteDeviceManager.isRemoteDeviceConnected();
            com.navdy.service.library.events.DeviceInfo.Platform remoteDevicePlatform = deviceConnected ? remoteDeviceManager.getRemoteDevicePlatform() : null;
            if (!deviceConnected || remoteDevicePlatform == null) {
                this.status.setText(voice_assist_na);
                this.subStatus.setText("");
                this.image.setImageResource(com.navdy.hud.app.R.drawable.icon_voice);
            } else {
                switch (remoteDevicePlatform) {
                    case PLATFORM_Android:
                        this.status.setText(google_now);
                        this.subStatus.setText("");
                        this.image.setImageResource(com.navdy.hud.app.R.drawable.icon_googlenow_off);
                        color = gnow_color;
                        break;
                    case PLATFORM_iOS:
                        color = siri_color;
                        this.status.setText(siri);
                        this.subStatus.setText(siriIsStarting);
                        this.image.setImageResource(com.navdy.hud.app.R.drawable.icon_siri);
                        break;
                }
                sLogger.i("sending VoiceAssistRequest");
                bus.post(new com.navdy.hud.app.event.RemoteEvent(new com.navdy.service.library.events.audio.VoiceAssistRequest(java.lang.Boolean.valueOf(false))));
            }
            this.fluctuator.setFillColor(color);
            this.fluctuator.setStrokeColor(color);
            if (0 != 0) {
                this.fluctuator.setVisibility(0);
                this.fluctuator.start();
                return;
            }
            this.fluctuator.setVisibility(4);
            this.fluctuator.stop();
        }
    }

    public void onStop() {
        this.controller = null;
        bus.unregister(this);
        if (this.layout != null) {
            this.fluctuator.stop();
        }
        if (this.mSiriTimeoutHandler != null) {
            this.mSiriTimeoutHandler.removeCallbacks(this.mSiriTimeoutRunnable);
        }
        com.navdy.hud.app.framework.toast.ToastManager.getInstance().disableToasts(false);
        sLogger.v("startVoiceAssistance: enabled toast");
    }

    public android.view.View getView(android.content.Context context) {
        if (this.layout == null) {
            this.layout = (android.view.ViewGroup) android.view.LayoutInflater.from(context).inflate(com.navdy.hud.app.R.layout.notification_voice_assist, null);
            this.status = (android.widget.TextView) this.layout.findViewById(com.navdy.hud.app.R.id.status);
            this.subStatus = (android.widget.TextView) this.layout.findViewById(com.navdy.hud.app.R.id.subStatus);
            this.fluctuator = (com.navdy.hud.app.ui.component.FluctuatorAnimatorView) this.layout.findViewById(com.navdy.hud.app.R.id.fluctuator);
            this.image = (android.widget.ImageView) this.layout.findViewById(com.navdy.hud.app.R.id.image);
        }
        return this.layout;
    }

    public void onUpdate() {
    }

    public int getTimeout() {
        com.navdy.hud.app.manager.RemoteDeviceManager remoteDeviceManager = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance();
        boolean deviceConnected = remoteDeviceManager.isRemoteDeviceConnected();
        com.navdy.service.library.events.DeviceInfo.Platform remoteDevicePlatform = deviceConnected ? remoteDeviceManager.getRemoteDevicePlatform() : null;
        if (!deviceConnected || remoteDevicePlatform == null) {
            return DEFAULT_VOICE_ASSIST_TIMEOUT;
        }
        switch (remoteDevicePlatform) {
            case PLATFORM_iOS:
                return SIRI_TIMEOUT;
            default:
                return DEFAULT_VOICE_ASSIST_TIMEOUT;
        }
    }

    public boolean isAlive() {
        return false;
    }

    public boolean isPurgeable() {
        return false;
    }

    public boolean canAddToStackIfCurrentExists() {
        return false;
    }

    public int getColor() {
        return 0;
    }

    public void onNotificationEvent(com.navdy.hud.app.ui.framework.UIStateManager.Mode mode) {
    }

    public void onExpandedNotificationEvent(com.navdy.hud.app.ui.framework.UIStateManager.Mode mode) {
    }

    public void onExpandedNotificationSwitched() {
    }

    public android.animation.AnimatorSet getViewSwitchAnimation(boolean viewIn) {
        return null;
    }

    public boolean expandNotification() {
        return false;
    }

    public boolean supportScroll() {
        return false;
    }

    public boolean onKey(com.navdy.hud.app.manager.InputManager.CustomKeyEvent event) {
        switch (event) {
            case LONG_PRESS:
                if (!this.waitingAfterTrigger) {
                    sendSiriKeyEvents();
                }
                return true;
            default:
                return false;
        }
    }

    private void sendSiriKeyEvents() {
        sLogger.i("sending siri key events");
        bus.post(new com.navdy.hud.app.event.RemoteEvent(new com.navdy.service.library.events.input.MediaRemoteKeyEvent(com.navdy.service.library.events.input.MediaRemoteKey.MEDIA_REMOTE_KEY_SIRI, com.navdy.service.library.events.input.KeyEvent.KEY_DOWN)));
        bus.post(new com.navdy.hud.app.event.RemoteEvent(new com.navdy.service.library.events.input.MediaRemoteKeyEvent(com.navdy.service.library.events.input.MediaRemoteKey.MEDIA_REMOTE_KEY_SIRI, com.navdy.service.library.events.input.KeyEvent.KEY_UP)));
    }

    @com.squareup.otto.Subscribe
    public void onVoiceAssistResponse(com.navdy.service.library.events.audio.VoiceAssistResponse response) {
        if (this.controller != null && this.layout != null) {
            if (this.mSiriTimeoutHandler != null) {
                this.mSiriTimeoutHandler.removeCallbacks(this.mSiriTimeoutRunnable);
            }
            com.navdy.hud.app.manager.RemoteDeviceManager remoteDeviceManager = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance();
            boolean deviceConnected = remoteDeviceManager.isRemoteDeviceConnected();
            com.navdy.service.library.events.DeviceInfo.Platform remoteDevicePlatform = deviceConnected ? remoteDeviceManager.getRemoteDevicePlatform() : null;
            java.lang.String title = "";
            java.lang.String subTitle = "";
            boolean startFluctuating = false;
            int icon = 0;
            if (deviceConnected && remoteDevicePlatform != null) {
                sLogger.i("received response " + response.state);
                switch (response.state) {
                    case VOICE_ASSIST_AVAILABLE:
                        if (remoteDevicePlatform == com.navdy.service.library.events.DeviceInfo.Platform.PLATFORM_iOS) {
                            title = siri;
                            subTitle = siriIsStarting;
                            icon = com.navdy.hud.app.R.drawable.icon_siri;
                            this.waitingAfterTrigger = true;
                            sendSiriKeyEvents();
                            if (this.mSiriTimeoutHandler == null) {
                                this.mSiriTimeoutHandler = new android.os.Handler();
                            }
                            this.mSiriTimeoutRunnable = new com.navdy.hud.app.framework.voice.VoiceAssistNotification.Anon1();
                            this.mSiriTimeoutHandler.postDelayed(this.mSiriTimeoutRunnable, 5000);
                            break;
                        }
                        break;
                    case VOICE_ASSIST_NOT_AVAILABLE:
                        if (remoteDevicePlatform == com.navdy.service.library.events.DeviceInfo.Platform.PLATFORM_Android) {
                            title = google_now;
                            subTitle = voice_assist_disabled;
                            icon = com.navdy.hud.app.R.drawable.icon_googlenow_off;
                        } else {
                            title = siri;
                            subTitle = voice_assist_disabled;
                            icon = com.navdy.hud.app.R.drawable.icon_siri;
                        }
                        this.fluctuator.setVisibility(4);
                        this.fluctuator.stop();
                        this.waitingAfterTrigger = false;
                        break;
                    case VOICE_ASSIST_STOPPED:
                        this.fluctuator.setVisibility(4);
                        this.fluctuator.stop();
                        dismissNotification();
                        this.waitingAfterTrigger = false;
                        return;
                    case VOICE_ASSIST_LISTENING:
                        if (remoteDevicePlatform != com.navdy.service.library.events.DeviceInfo.Platform.PLATFORM_Android) {
                            title = siri;
                            subTitle = siriIsActive;
                            icon = com.navdy.hud.app.R.drawable.icon_siri;
                            startFluctuating = true;
                            if (!this.waitingAfterTrigger) {
                                sendSiriKeyEvents();
                                break;
                            } else {
                                this.waitingAfterTrigger = false;
                                break;
                            }
                        } else {
                            title = google_now;
                            icon = com.navdy.hud.app.R.drawable.icon_googlenow_on;
                            startFluctuating = true;
                            break;
                        }
                }
            } else {
                title = voice_assist_na;
                icon = com.navdy.hud.app.R.drawable.icon_voice;
                this.fluctuator.stop();
                this.fluctuator.setVisibility(4);
            }
            this.status.setText(title);
            this.subStatus.setText(subTitle);
            this.image.setImageResource(icon);
            if (startFluctuating) {
                this.fluctuator.setVisibility(0);
                this.fluctuator.start();
                return;
            }
            this.fluctuator.setVisibility(4);
            this.fluctuator.stop();
        }
    }

    private void dismissNotification() {
        com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().removeNotification(com.navdy.hud.app.framework.notifications.NotificationId.VOICE_ASSIST_NOTIFICATION_ID);
    }

    public void onClick() {
    }

    public void onTrackHand(float normalized) {
    }

    public boolean onGesture(com.navdy.service.library.events.input.GestureEvent event) {
        return false;
    }

    public com.navdy.hud.app.manager.InputManager.IInputHandler nextHandler() {
        return null;
    }
}
