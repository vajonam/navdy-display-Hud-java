package com.navdy.hud.app.framework.voice;

public class VoiceSearchHandler {
    private static final java.lang.String searchAgainSubTitle = com.navdy.hud.app.HudApplication.getAppContext().getResources().getString(com.navdy.hud.app.R.string.retry_search);
    /* access modifiers changed from: private */
    public com.squareup.otto.Bus bus;
    private android.content.Context context;
    private android.os.Handler handler = new android.os.Handler(android.os.Looper.getMainLooper());
    private com.navdy.service.library.events.audio.VoiceSearchResponse response;
    private boolean showVoiceSearchTipsToUser = true;
    private com.navdy.hud.app.framework.voice.VoiceSearchHandler.VoiceSearchUserInterface voiceSearchUserInterface;

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            com.navdy.hud.app.framework.voice.VoiceSearchHandler.this.bus.post(new com.navdy.hud.app.event.RemoteEvent(new com.navdy.service.library.events.audio.VoiceSearchRequest(java.lang.Boolean.valueOf(false))));
        }
    }

    class Anon2 implements com.navdy.hud.app.ui.component.destination.IDestinationPicker {
        boolean retrySelected = false;

        Anon2() {
        }

        public boolean onItemClicked(int id, int pos, com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.DestinationPickerState state) {
            switch (id) {
                case com.navdy.hud.app.R.id.retry /*2131624048*/:
                case com.navdy.hud.app.R.id.search_again /*2131624049*/:
                    this.retrySelected = true;
                    if (id == com.navdy.hud.app.R.id.retry) {
                        com.navdy.hud.app.analytics.AnalyticsSupport.recordVoiceSearchListItemSelection(-1);
                        return true;
                    }
                    com.navdy.hud.app.analytics.AnalyticsSupport.recordVoiceSearchListItemSelection(-2);
                    return true;
                default:
                    com.navdy.hud.app.analytics.AnalyticsSupport.recordVoiceSearchListItemSelection(pos);
                    return false;
            }
        }

        public boolean onItemSelected(int id, int pos, com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.DestinationPickerState state) {
            return false;
        }

        public void onDestinationPickerClosed() {
            if (this.retrySelected) {
                com.navdy.hud.app.framework.notifications.NotificationManager notificationManager = com.navdy.hud.app.framework.notifications.NotificationManager.getInstance();
                com.navdy.hud.app.framework.voice.VoiceSearchNotification voiceSearchNotification = (com.navdy.hud.app.framework.voice.VoiceSearchNotification) notificationManager.getNotification(com.navdy.hud.app.framework.notifications.NotificationId.VOICE_SEARCH_NOTIFICATION_ID);
                if (voiceSearchNotification == null) {
                    voiceSearchNotification = new com.navdy.hud.app.framework.voice.VoiceSearchNotification();
                }
                notificationManager.addNotification(voiceSearchNotification);
            }
        }
    }

    public interface VoiceSearchUserInterface {
        void close();

        void onVoiceSearchResponse(com.navdy.service.library.events.audio.VoiceSearchResponse voiceSearchResponse);
    }

    public VoiceSearchHandler(android.content.Context context2, com.squareup.otto.Bus bus2, com.navdy.hud.app.util.FeatureUtil featureUtil) {
        this.bus = bus2;
        this.context = context2;
        this.bus.register(this);
    }

    public void setVoiceSearchUserInterface(com.navdy.hud.app.framework.voice.VoiceSearchHandler.VoiceSearchUserInterface voiceSearchUserInterface2) {
        this.voiceSearchUserInterface = voiceSearchUserInterface2;
    }

    @com.squareup.otto.Subscribe
    public void onDriverProfileChanged(com.navdy.hud.app.event.DriverProfileChanged driverProfileChanged) {
        this.showVoiceSearchTipsToUser = true;
    }

    public void showedVoiceSearchTipsToUser() {
        this.showVoiceSearchTipsToUser = false;
    }

    public boolean shouldShowVoiceSearchTipsToUser() {
        return this.showVoiceSearchTipsToUser;
    }

    @com.squareup.otto.Subscribe
    public void onVoiceSearchResponse(com.navdy.service.library.events.audio.VoiceSearchResponse voiceSearchResponse) {
        if (this.voiceSearchUserInterface != null) {
            this.response = voiceSearchResponse;
            this.voiceSearchUserInterface.onVoiceSearchResponse(voiceSearchResponse);
        }
    }

    public void startVoiceSearch() {
        com.navdy.service.library.events.DeviceInfo.Platform remotePlatform = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getRemoteDevicePlatform();
        if (remotePlatform != null) {
            if (remotePlatform != com.navdy.service.library.events.DeviceInfo.Platform.PLATFORM_iOS || !com.navdy.hud.app.util.RemoteCapabilitiesUtil.supportsVoiceSearchNewIOSPauseBehaviors()) {
                this.bus.post(new com.navdy.hud.app.event.RemoteEvent(new com.navdy.service.library.events.audio.VoiceSearchRequest(java.lang.Boolean.valueOf(false))));
                return;
            }
            com.navdy.hud.app.manager.MusicManager musicManager = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getMusicManager();
            if (musicManager != null) {
                musicManager.softPause();
            }
            this.handler.postDelayed(new com.navdy.hud.app.framework.voice.VoiceSearchHandler.Anon1(), 250);
        }
    }

    public void stopVoiceSearch() {
        if (this.response != null && this.response.state != com.navdy.service.library.events.audio.VoiceSearchResponse.VoiceSearchState.VOICE_SEARCH_SUCCESS) {
            this.bus.post(new com.navdy.hud.app.event.RemoteEvent(new com.navdy.service.library.events.audio.VoiceSearchRequest(java.lang.Boolean.valueOf(true))));
            com.navdy.hud.app.manager.MusicManager musicManager = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getMusicManager();
            if (musicManager != null) {
                musicManager.acceptResumes();
            }
        }
    }

    public void showResults() {
        java.lang.String title;
        if (this.response != null && this.response.results != null) {
            java.util.List<com.navdy.service.library.events.destination.Destination> returnedDestinations = this.response.results;
            android.os.Bundle args = new android.os.Bundle();
            args.putBoolean(com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.PICKER_SHOW_DESTINATION_MAP, true);
            args.putInt(com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.PICKER_DESTINATION_ICON, -1);
            args.putInt(com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.PICKER_INITIAL_SELECTION, 1);
            int defaultColor = android.support.v4.content.ContextCompat.getColor(this.context, com.navdy.hud.app.R.color.route_sel);
            int deselectedColor = android.support.v4.content.ContextCompat.getColor(this.context, com.navdy.hud.app.R.color.icon_bk_color_unselected);
            java.lang.String subTitle = null;
            if (!android.text.TextUtils.isEmpty(this.response.recognizedWords)) {
                title = "\"" + this.response.recognizedWords + "\"";
                subTitle = searchAgainSubTitle;
            } else {
                title = searchAgainSubTitle;
            }
            com.navdy.hud.app.ui.component.destination.DestinationParcelable searchAgain = new com.navdy.hud.app.ui.component.destination.DestinationParcelable(com.navdy.hud.app.R.id.search_again, title, subTitle, false, null, true, null, 0.0d, 0.0d, 0.0d, 0.0d, com.navdy.hud.app.R.drawable.icon_mm_voice_search_2, 0, defaultColor, deselectedColor, com.navdy.hud.app.ui.component.destination.DestinationParcelable.DestinationType.NONE, null);
            java.util.List<com.navdy.hud.app.ui.component.destination.DestinationParcelable> destinations = com.navdy.hud.app.maps.util.DestinationUtil.convert(this.context, returnedDestinations, defaultColor, deselectedColor, true);
            destinations.add(0, searchAgain);
            destinations.add(new com.navdy.hud.app.ui.component.destination.DestinationParcelable(com.navdy.hud.app.R.id.retry, searchAgainSubTitle, null, false, null, false, null, 0.0d, 0.0d, 0.0d, 0.0d, com.navdy.hud.app.R.drawable.icon_mm_voice_search_2, 0, defaultColor, deselectedColor, com.navdy.hud.app.ui.component.destination.DestinationParcelable.DestinationType.NONE, com.navdy.service.library.events.places.PlaceType.PLACE_TYPE_UNKNOWN));
            com.navdy.hud.app.ui.component.destination.DestinationParcelable[] destinationParcelables = new com.navdy.hud.app.ui.component.destination.DestinationParcelable[destinations.size()];
            destinations.toArray(destinationParcelables);
            args.putParcelableArray(com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.PICKER_DESTINATIONS, destinationParcelables);
            com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().removeNotification(com.navdy.hud.app.framework.notifications.NotificationId.VOICE_SEARCH_NOTIFICATION_ID, true, com.navdy.service.library.events.ui.Screen.SCREEN_DESTINATION_PICKER, args, new com.navdy.hud.app.framework.voice.VoiceSearchHandler.Anon2());
        }
    }

    public void go() {
        if (this.response != null && this.response.state == com.navdy.service.library.events.audio.VoiceSearchResponse.VoiceSearchState.VOICE_SEARCH_SUCCESS && this.response.results != null && this.response.results.size() > 0) {
            com.navdy.service.library.events.destination.Destination destination = (com.navdy.service.library.events.destination.Destination) this.response.results.get(0);
            com.navdy.hud.app.framework.destinations.Destination d = com.navdy.hud.app.framework.destinations.DestinationsManager.getInstance().transformToInternalDestination(destination);
            int iconRes = 0;
            com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.PlaceTypeResourceHolder resourceHolder = com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.getPlaceTypeHolder(destination.place_type);
            if (resourceHolder != null) {
                iconRes = resourceHolder.iconRes;
            }
            if (d.destinationIcon == 0 && iconRes != 0) {
                d = new com.navdy.hud.app.framework.destinations.Destination.Builder(d).destinationIcon(iconRes).destinationIconBkColor(android.support.v4.content.ContextCompat.getColor(this.context, com.navdy.hud.app.R.color.icon_bk_color_unselected)).build();
            }
            com.navdy.hud.app.framework.destinations.DestinationsManager.getInstance().requestNavigationWithNavLookup(d);
        }
    }
}
