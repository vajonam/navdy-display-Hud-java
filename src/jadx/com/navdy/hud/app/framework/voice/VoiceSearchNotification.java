package com.navdy.hud.app.framework.voice;

public class VoiceSearchNotification implements com.navdy.hud.app.framework.notifications.INotification, com.navdy.hud.app.ui.component.ChoiceLayout2.IListener, com.navdy.hud.app.framework.voice.VoiceSearchHandler.VoiceSearchUserInterface {
    public static final int ANIMATION_DURATION = 300;
    private static final java.util.HashMap<com.navdy.service.library.events.audio.VoiceSearchResponse.VoiceSearchError, java.lang.Integer> BADGE_ICON_MAPPING = new java.util.HashMap<>();
    private static final java.util.HashMap<com.navdy.service.library.events.audio.VoiceSearchResponse.VoiceSearchError, java.lang.Integer> ERROR_TEXT_MAPPING = new java.util.HashMap<>();
    public static final int INITIALIZATION_DELAY_MILLIS = ((int) java.util.concurrent.TimeUnit.SECONDS.toMillis(30));
    public static final int INPUT_DELAY_MILLIS = ((int) java.util.concurrent.TimeUnit.MINUTES.toMillis(2));
    public static final int RESULT_SELECTION_TIMEOUT = 10000;
    public static final int SEARCH_DELAY_MILLIS = ((int) java.util.concurrent.TimeUnit.MINUTES.toMillis(2));
    private static final java.util.HashMap<com.navdy.hud.app.framework.voice.VoiceSearchNotification.VoiceSearchStateInternal, java.lang.Integer> STATE_NAME_MAPPING = new java.util.HashMap<>();
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.framework.voice.VoiceSearchNotification.class);
    public android.animation.AnimatorSet animatorSet;
    @butterknife.InjectView(2131624150)
    public com.navdy.hud.app.ui.component.ChoiceLayout2 choiceLayout;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.framework.notifications.INotificationController controller;
    /* access modifiers changed from: private */
    public com.navdy.service.library.events.audio.VoiceSearchResponse.VoiceSearchError error;
    private android.os.Handler handler;
    @butterknife.InjectView(2131624307)
    public android.widget.ImageView imageActive;
    @butterknife.InjectView(2131624306)
    public android.widget.ImageView imageInactive;
    @butterknife.InjectView(2131624308)
    public android.widget.ImageView imageProcessing;
    @butterknife.InjectView(2131624309)
    public android.widget.ImageView imageStatus;
    private boolean isListeningOverBluetooth;
    private android.view.ViewGroup layout;
    private android.animation.AnimatorSet listeningFeedbackAnimation;
    @butterknife.InjectView(2131624305)
    public android.view.View listeningFeedbackView;
    private java.util.Locale locale;
    private android.animation.AnimatorSet mainImageAnimation;
    /* access modifiers changed from: private */
    public boolean multipleResultsMode = false;
    /* access modifiers changed from: private */
    public android.animation.AnimatorSet processingImageAnimation;
    /* access modifiers changed from: private */
    public volatile boolean responseFromClientTimedOut;
    private java.util.List<com.navdy.service.library.events.destination.Destination> results;
    @butterknife.InjectView(2131624304)
    public android.widget.TextView resultsCount;
    @butterknife.InjectView(2131624303)
    public android.view.ViewGroup resultsCountContainer;
    private android.animation.AnimatorSet statusImageAnimation;
    @butterknife.InjectView(2131624301)
    public android.widget.TextView subStatus;
    private java.util.List<com.navdy.hud.app.ui.component.ChoiceLayout2.Choice> voiceSearchActiveChoices;
    private java.util.List<com.navdy.hud.app.ui.component.ChoiceLayout2.Choice> voiceSearchFailedChoices;
    private com.navdy.hud.app.framework.voice.VoiceSearchHandler voiceSearchHandler;
    private com.navdy.hud.app.framework.voice.VoiceSearchNotification.VoiceSearchStateInternal voiceSearchState = com.navdy.hud.app.framework.voice.VoiceSearchNotification.VoiceSearchStateInternal.IDLE;
    private java.util.List<com.navdy.hud.app.ui.component.ChoiceLayout2.Choice> voiceSearchSuccessResultsChoices;
    private java.lang.Runnable waitingTimeoutRunnable = new com.navdy.hud.app.framework.voice.VoiceSearchNotification.Anon1();
    private java.util.List<java.lang.String> words;

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            if (com.navdy.hud.app.framework.voice.VoiceSearchNotification.this.controller != null) {
                com.navdy.hud.app.framework.voice.VoiceSearchNotification.this.multipleResultsMode = false;
                com.navdy.hud.app.framework.voice.VoiceSearchNotification.this.error = null;
                com.navdy.hud.app.framework.voice.VoiceSearchNotification.this.responseFromClientTimedOut = true;
                com.navdy.hud.app.framework.voice.VoiceSearchNotification.this.updateUI(com.navdy.hud.app.framework.voice.VoiceSearchNotification.VoiceSearchStateInternal.FAILED);
            }
        }
    }

    class Anon2 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
        Anon2() {
        }

        public void onAnimationEnd(android.animation.Animator animator) {
            com.navdy.hud.app.framework.voice.VoiceSearchNotification.this.imageActive.setAlpha(1.0f);
        }
    }

    class Anon3 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
        Anon3() {
        }

        public void onAnimationEnd(android.animation.Animator animator) {
            com.navdy.hud.app.framework.voice.VoiceSearchNotification.this.imageActive.setAlpha(0.0f);
        }
    }

    class Anon4 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
        private boolean canceled = false;

        Anon4() {
        }

        public void onAnimationStart(android.animation.Animator animator) {
            com.navdy.hud.app.framework.voice.VoiceSearchNotification.this.imageProcessing.setRotation(0.0f);
            this.canceled = false;
        }

        public void onAnimationEnd(android.animation.Animator animator) {
            if (!this.canceled) {
                com.navdy.hud.app.framework.voice.VoiceSearchNotification.this.processingImageAnimation.setStartDelay(33);
                com.navdy.hud.app.framework.voice.VoiceSearchNotification.this.processingImageAnimation.start();
            }
        }

        public void onAnimationCancel(android.animation.Animator animator) {
            this.canceled = true;
        }
    }

    static class Anon5 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
        final /* synthetic */ android.view.View val$view;

        Anon5(android.view.View view) {
            this.val$view = view;
        }

        public void onAnimationStart(android.animation.Animator animation) {
            this.val$view.setVisibility(0);
            super.onAnimationStart(animation);
        }
    }

    static class Anon6 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
        private boolean canceled = false;
        final /* synthetic */ android.animation.AnimatorSet val$animationSet;

        Anon6(android.animation.AnimatorSet animatorSet) {
            this.val$animationSet = animatorSet;
        }

        public void onAnimationStart(android.animation.Animator animator) {
            this.canceled = false;
        }

        public void onAnimationEnd(android.animation.Animator animator) {
            if (!this.canceled) {
                this.val$animationSet.start();
            }
        }

        public void onAnimationCancel(android.animation.Animator animator) {
            this.canceled = true;
        }
    }

    static class SinusoidalInterpolator implements android.view.animation.Interpolator {
        SinusoidalInterpolator() {
        }

        public float getInterpolation(float input) {
            return (float) ((java.lang.Math.sin((((double) input) * 3.141592653589793d) - 1.5707963267948966d) * 0.5d) + 0.5d);
        }
    }

    enum VoiceSearchStateInternal {
        IDLE(-1),
        STARTING(com.navdy.hud.app.R.string.voice_search_starting),
        LISTENING(com.navdy.hud.app.R.string.voice_search_listening),
        SEARCHING(com.navdy.hud.app.R.string.voice_search_searching),
        FAILED(com.navdy.hud.app.R.string.voice_search_failed),
        RESULTS(com.navdy.hud.app.R.string.voice_search_more_results);
        
        private int statusTextResId;

        public int getStatusTextResId() {
            return this.statusTextResId;
        }

        private VoiceSearchStateInternal(int statusTextResId2) {
            this.statusTextResId = statusTextResId2;
        }
    }

    static {
        com.navdy.hud.app.HudApplication.getAppContext().getResources();
        ERROR_TEXT_MAPPING.put(com.navdy.service.library.events.audio.VoiceSearchResponse.VoiceSearchError.FAILED_TO_START, java.lang.Integer.valueOf(com.navdy.hud.app.R.string.voice_search_failed));
        ERROR_TEXT_MAPPING.put(com.navdy.service.library.events.audio.VoiceSearchResponse.VoiceSearchError.OFFLINE, java.lang.Integer.valueOf(com.navdy.hud.app.R.string.voice_search_offline));
        ERROR_TEXT_MAPPING.put(com.navdy.service.library.events.audio.VoiceSearchResponse.VoiceSearchError.NEED_PERMISSION, java.lang.Integer.valueOf(com.navdy.hud.app.R.string.voice_search_need_permission));
        ERROR_TEXT_MAPPING.put(com.navdy.service.library.events.audio.VoiceSearchResponse.VoiceSearchError.NO_WORDS_RECOGNIZED, java.lang.Integer.valueOf(com.navdy.hud.app.R.string.voice_search_failed));
        ERROR_TEXT_MAPPING.put(com.navdy.service.library.events.audio.VoiceSearchResponse.VoiceSearchError.NOT_AVAILABLE, java.lang.Integer.valueOf(com.navdy.hud.app.R.string.voice_search_failed));
        ERROR_TEXT_MAPPING.put(com.navdy.service.library.events.audio.VoiceSearchResponse.VoiceSearchError.NO_RESULTS_FOUND, java.lang.Integer.valueOf(com.navdy.hud.app.R.string.voice_search_no_results_found));
        ERROR_TEXT_MAPPING.put(com.navdy.service.library.events.audio.VoiceSearchResponse.VoiceSearchError.AMBIENT_NOISE, java.lang.Integer.valueOf(com.navdy.hud.app.R.string.voice_search_ambient_noise));
        BADGE_ICON_MAPPING.put(com.navdy.service.library.events.audio.VoiceSearchResponse.VoiceSearchError.FAILED_TO_START, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_badge_alert));
        BADGE_ICON_MAPPING.put(com.navdy.service.library.events.audio.VoiceSearchResponse.VoiceSearchError.OFFLINE, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_badge_alert));
        BADGE_ICON_MAPPING.put(com.navdy.service.library.events.audio.VoiceSearchResponse.VoiceSearchError.NEED_PERMISSION, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_badge_alert));
        BADGE_ICON_MAPPING.put(com.navdy.service.library.events.audio.VoiceSearchResponse.VoiceSearchError.NO_RESULTS_FOUND, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_badge_voice_search));
        BADGE_ICON_MAPPING.put(com.navdy.service.library.events.audio.VoiceSearchResponse.VoiceSearchError.NOT_AVAILABLE, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_badge_alert));
        BADGE_ICON_MAPPING.put(com.navdy.service.library.events.audio.VoiceSearchResponse.VoiceSearchError.AMBIENT_NOISE, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_badge_alert));
        STATE_NAME_MAPPING.put(com.navdy.hud.app.framework.voice.VoiceSearchNotification.VoiceSearchStateInternal.STARTING, java.lang.Integer.valueOf(com.navdy.hud.app.R.string.voice_search_wait));
        STATE_NAME_MAPPING.put(com.navdy.hud.app.framework.voice.VoiceSearchNotification.VoiceSearchStateInternal.LISTENING, java.lang.Integer.valueOf(com.navdy.hud.app.R.string.voice_search_speak_now));
        STATE_NAME_MAPPING.put(com.navdy.hud.app.framework.voice.VoiceSearchNotification.VoiceSearchStateInternal.FAILED, java.lang.Integer.valueOf(com.navdy.hud.app.R.string.voice_search_failed));
        STATE_NAME_MAPPING.put(com.navdy.hud.app.framework.voice.VoiceSearchNotification.VoiceSearchStateInternal.SEARCHING, java.lang.Integer.valueOf(com.navdy.hud.app.R.string.voice_search_searching));
    }

    public com.navdy.hud.app.framework.notifications.NotificationType getType() {
        return com.navdy.hud.app.framework.notifications.NotificationType.VOICE_SEARCH;
    }

    public java.lang.String getId() {
        return com.navdy.hud.app.framework.notifications.NotificationId.VOICE_SEARCH_NOTIFICATION_ID;
    }

    public android.view.View getExpandedView(android.content.Context context, java.lang.Object data) {
        return null;
    }

    public int getExpandedViewIndicatorColor() {
        return 0;
    }

    public void onStart(com.navdy.hud.app.framework.notifications.INotificationController controller2) {
        this.handler = new android.os.Handler();
        if (this.layout != null) {
            this.voiceSearchHandler = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getVoiceSearchHandler();
            this.voiceSearchHandler.setVoiceSearchUserInterface(this);
            this.controller = controller2;
            this.error = null;
            this.isListeningOverBluetooth = false;
            this.words = null;
            this.imageStatus.setImageDrawable(null);
            com.navdy.hud.app.analytics.AnalyticsSupport.recordVoiceSearchEntered();
            startVoiceSearch();
        }
    }

    private void startVoiceSearch() {
        this.handler.removeCallbacks(this.waitingTimeoutRunnable);
        this.responseFromClientTimedOut = false;
        updateUI(com.navdy.hud.app.framework.voice.VoiceSearchNotification.VoiceSearchStateInternal.STARTING);
        this.voiceSearchHandler.startVoiceSearch();
        this.handler.postDelayed(this.waitingTimeoutRunnable, (long) INITIALIZATION_DELAY_MILLIS);
    }

    public void onVoiceSearchResponse(com.navdy.service.library.events.audio.VoiceSearchResponse response) {
        sLogger.d("onVoiceSearchResponse:" + response);
        this.handler.removeCallbacks(this.waitingTimeoutRunnable);
        if (this.responseFromClientTimedOut) {
            sLogger.d("Not processing further response as the client did not respond in time");
            return;
        }
        com.navdy.hud.app.analytics.AnalyticsSupport.recordVoiceSearchResult(response);
        com.navdy.hud.app.manager.MusicManager musicManager = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getMusicManager();
        switch (response.state) {
            case VOICE_SEARCH_STARTING:
                this.multipleResultsMode = false;
                this.error = response.error;
                if (!android.text.TextUtils.isEmpty(response.locale)) {
                    this.locale = com.navdy.hud.app.profile.HudLocale.getLocaleForID(response.locale);
                }
                updateUI(com.navdy.hud.app.framework.voice.VoiceSearchNotification.VoiceSearchStateInternal.STARTING);
                this.handler.postDelayed(this.waitingTimeoutRunnable, (long) INITIALIZATION_DELAY_MILLIS);
                return;
            case VOICE_SEARCH_SEARCHING:
                if (musicManager != null) {
                    musicManager.acceptResumes();
                }
                this.multipleResultsMode = false;
                this.error = response.error;
                updateUI(com.navdy.hud.app.framework.voice.VoiceSearchNotification.VoiceSearchStateInternal.SEARCHING);
                this.handler.postDelayed(this.waitingTimeoutRunnable, (long) SEARCH_DELAY_MILLIS);
                return;
            case VOICE_SEARCH_LISTENING:
                this.multipleResultsMode = false;
                this.isListeningOverBluetooth = ((java.lang.Boolean) com.squareup.wire.Wire.get(response.listeningOverBluetooth, java.lang.Boolean.valueOf(false))).booleanValue();
                this.error = response.error;
                updateUI(com.navdy.hud.app.framework.voice.VoiceSearchNotification.VoiceSearchStateInternal.LISTENING);
                this.handler.postDelayed(this.waitingTimeoutRunnable, (long) INPUT_DELAY_MILLIS);
                return;
            case VOICE_SEARCH_ERROR:
                if (musicManager != null) {
                    musicManager.acceptResumes();
                }
                this.multipleResultsMode = false;
                this.error = response.error;
                sLogger.d("Error " + response.error);
                updateUI(com.navdy.hud.app.framework.voice.VoiceSearchNotification.VoiceSearchStateInternal.FAILED);
                return;
            case VOICE_SEARCH_SUCCESS:
                this.error = response.error;
                if (response.results != null) {
                    boolean startTrip = false;
                    sLogger.v("onVoiceSearchResponse: size=" + response.results.size());
                    if (response.results.size() == 1) {
                        com.navdy.service.library.events.destination.Destination d = (com.navdy.service.library.events.destination.Destination) response.results.get(0);
                        if (d.favorite_type != null) {
                            switch (d.favorite_type) {
                                case FAVORITE_HOME:
                                case FAVORITE_WORK:
                                case FAVORITE_CUSTOM:
                                    startTrip = true;
                                    break;
                                case FAVORITE_NONE:
                                    if (d.last_navigated_to != null && d.last_navigated_to.longValue() > 0) {
                                        startTrip = true;
                                        break;
                                    }
                            }
                        }
                    }
                    if (!startTrip) {
                        sLogger.v("onVoiceSearchResponse: show picker");
                        com.navdy.hud.app.analytics.AnalyticsSupport.recordVoiceSearchAdditionalResultsAction(com.navdy.hud.app.analytics.AnalyticsSupport.VoiceSearchAdditionalResultsAction.ADDITIONAL_RESULTS_LIST);
                        this.voiceSearchHandler.showResults();
                        return;
                    }
                    sLogger.v("onVoiceSearchResponse: start trip");
                    this.multipleResultsMode = false;
                    this.voiceSearchHandler.go();
                    dismissNotification();
                    return;
                }
                this.multipleResultsMode = false;
                updateUI(com.navdy.hud.app.framework.voice.VoiceSearchNotification.VoiceSearchStateInternal.IDLE);
                dismissNotification();
                return;
            default:
                return;
        }
    }

    public void close() {
        dismissNotification();
    }

    /* access modifiers changed from: private */
    public void updateUI(com.navdy.hud.app.framework.voice.VoiceSearchNotification.VoiceSearchStateInternal state) {
        updateUI(state, this.error, this.results);
    }

    private void showHideVoiceSearchTipsIfNeeded(boolean show) {
        sLogger.d("showHideVoiceSearchTipsIfNeeded, show : " + show);
        com.navdy.hud.app.ui.activity.Main main = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getUiStateManager().getRootScreen();
        if (show) {
            sLogger.d("Should show voice search tips :" + this.voiceSearchHandler.shouldShowVoiceSearchTipsToUser());
            if (this.voiceSearchHandler.shouldShowVoiceSearchTipsToUser()) {
                android.view.ViewGroup notificationExtensionHolder = (android.view.ViewGroup) android.view.LayoutInflater.from(com.navdy.hud.app.HudApplication.getAppContext()).inflate(com.navdy.hud.app.R.layout.voice_search_tips_layout, main.getNotificationExtensionViewHolder(), false);
                com.navdy.hud.app.framework.voice.VoiceSearchTipsView voiceSearchTipsView = (com.navdy.hud.app.framework.voice.VoiceSearchTipsView) notificationExtensionHolder.findViewById(com.navdy.hud.app.R.id.voice_search_tips);
                voiceSearchTipsView.setListeningOverBluetooth(this.isListeningOverBluetooth);
                if (this.locale != null) {
                    voiceSearchTipsView.setVoiceSearchLocale(this.locale);
                } else {
                    voiceSearchTipsView.setVoiceSearchLocale(com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile().getLocale());
                }
                main.addNotificationExtensionView(notificationExtensionHolder);
                this.voiceSearchHandler.showedVoiceSearchTipsToUser();
                return;
            }
            return;
        }
        sLogger.d("Remove notification extension");
        main.removeNotificationExtensionView();
    }

    private void updateUI(com.navdy.hud.app.framework.voice.VoiceSearchNotification.VoiceSearchStateInternal newState, com.navdy.service.library.events.audio.VoiceSearchResponse.VoiceSearchError error2, java.util.List<com.navdy.service.library.events.destination.Destination> results2) {
        sLogger.d("updateUI newState :" + newState + " , Error :" + error2);
        boolean stateChanged = this.voiceSearchState != newState;
        if (stateChanged) {
            sLogger.d("State Changed From : " + this.voiceSearchState + " , New state : " + newState);
        }
        this.voiceSearchState = newState;
        android.animation.AnimatorSet animatorSet2 = new android.animation.AnimatorSet();
        android.animation.AnimatorSet.Builder animatorSetBuilder = null;
        if (stateChanged) {
            setChoices(newState);
        }
        switch (newState) {
            case FAILED:
                if (error2 != null && !this.responseFromClientTimedOut) {
                    switch (error2) {
                        case NO_RESULTS_FOUND:
                            this.subStatus.setText("");
                            break;
                        default:
                            java.lang.Integer err = error2 == null ? null : (java.lang.Integer) ERROR_TEXT_MAPPING.get(error2);
                            if (err == null) {
                                err = java.lang.Integer.valueOf(com.navdy.hud.app.R.string.voice_search_failed);
                                sLogger.e(java.lang.String.format("unhandled error response: %s", new java.lang.Object[]{error2}));
                            }
                            this.subStatus.setText(err.intValue());
                            break;
                    }
                } else {
                    this.subStatus.setText(this.voiceSearchState.getStatusTextResId());
                    break;
                }
                break;
            case IDLE:
                if (stateChanged) {
                    this.subStatus.setText("");
                    break;
                }
                break;
            case RESULTS:
                this.subStatus.setText("");
                break;
            default:
                if (stateChanged) {
                    this.subStatus.setText(((java.lang.Integer) STATE_NAME_MAPPING.get(newState)).intValue());
                    break;
                }
                break;
        }
        android.animation.AnimatorSet mainImageAnimationReference = null;
        if (stateChanged) {
            this.resultsCountContainer.setVisibility(4);
            if (this.mainImageAnimation != null) {
                this.mainImageAnimation.removeAllListeners();
                this.mainImageAnimation.cancel();
            }
            this.mainImageAnimation = new android.animation.AnimatorSet();
            mainImageAnimationReference = this.mainImageAnimation;
            switch (newState) {
                case LISTENING:
                    android.animation.ObjectAnimator showActive = android.animation.ObjectAnimator.ofFloat(this.imageActive, "alpha", new float[]{0.0f, 1.0f});
                    showActive.addListener(new com.navdy.hud.app.framework.voice.VoiceSearchNotification.Anon2());
                    mainImageAnimationReference.play(showActive);
                    break;
                default:
                    this.imageInactive.setVisibility(0);
                    if (this.imageActive.getAlpha() > 0.0f) {
                        android.animation.ObjectAnimator hideActive = android.animation.ObjectAnimator.ofFloat(this.imageActive, "alpha", new float[]{1.0f, 0.0f});
                        hideActive.addListener(new com.navdy.hud.app.framework.voice.VoiceSearchNotification.Anon3());
                        mainImageAnimationReference.play(hideActive);
                        break;
                    }
                    break;
            }
            switch (newState) {
                case FAILED:
                    if (error2 == com.navdy.service.library.events.audio.VoiceSearchResponse.VoiceSearchError.NO_RESULTS_FOUND) {
                        this.resultsCountContainer.setVisibility(0);
                        this.imageInactive.setVisibility(4);
                        break;
                    }
                    break;
                case RESULTS:
                    if (results2 != null && results2.size() > 0) {
                        this.resultsCount.setText(java.lang.Integer.toString(results2.size()));
                    }
                    this.resultsCountContainer.setVisibility(0);
                    this.imageInactive.setVisibility(4);
                    break;
            }
        }
        android.animation.Animator processingAnimator = null;
        if (stateChanged) {
            if (this.statusImageAnimation != null) {
                this.statusImageAnimation.removeAllListeners();
                this.statusImageAnimation.cancel();
                this.statusImageAnimation = null;
            }
            this.imageStatus.setVisibility(4);
            if (this.processingImageAnimation != null) {
                this.processingImageAnimation.cancel();
            }
            this.imageProcessing.setVisibility(4);
            this.statusImageAnimation = new android.animation.AnimatorSet();
            switch (newState) {
                case FAILED:
                    if (error2 == null) {
                        this.imageStatus.setImageDrawable(null);
                        break;
                    } else {
                        if (com.navdy.service.library.events.audio.VoiceSearchResponse.VoiceSearchError.NO_WORDS_RECOGNIZED != error2) {
                            java.lang.Integer icon = error2 == null ? null : (java.lang.Integer) BADGE_ICON_MAPPING.get(error2);
                            if (icon == null) {
                                icon = java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_badge_alert);
                                sLogger.e(java.lang.String.format("no icon for error: %s", new java.lang.Object[]{error2}));
                            }
                            this.imageStatus.setImageResource(icon.intValue());
                        } else if (this.isListeningOverBluetooth) {
                            this.imageStatus.setImageResource(com.navdy.hud.app.R.drawable.icon_badge_b_t);
                        } else {
                            this.imageStatus.setImageResource(com.navdy.hud.app.R.drawable.icon_badge_phone);
                        }
                        this.statusImageAnimation.play(getBadgeAnimation(this.imageStatus, true));
                        break;
                    }
                case RESULTS:
                    this.imageStatus.setImageResource(com.navdy.hud.app.R.drawable.icon_badge_voice_search);
                    this.statusImageAnimation.play(getBadgeAnimation(this.imageStatus, true));
                    break;
                case LISTENING:
                    if (this.isListeningOverBluetooth) {
                        this.imageStatus.setImageResource(com.navdy.hud.app.R.drawable.icon_badge_b_t);
                    } else {
                        this.imageStatus.setImageResource(com.navdy.hud.app.R.drawable.icon_badge_phone);
                    }
                    this.statusImageAnimation.play(getBadgeAnimation(this.imageStatus, true));
                    break;
                case STARTING:
                case SEARCHING:
                    this.imageProcessing.setVisibility(0);
                    if (this.processingImageAnimation == null) {
                        this.processingImageAnimation = new android.animation.AnimatorSet();
                        this.processingImageAnimation.addListener(new com.navdy.hud.app.framework.voice.VoiceSearchNotification.Anon4());
                        this.processingImageAnimation.play(getProcessingBadgeAnimation(this.imageProcessing));
                    }
                    processingAnimator = this.processingImageAnimation;
                    break;
            }
        }
        if (mainImageAnimationReference != null) {
            animatorSetBuilder = animatorSet2.play(mainImageAnimationReference);
        }
        if (stateChanged && this.statusImageAnimation != null) {
            if (animatorSetBuilder != null) {
                animatorSetBuilder.before(this.statusImageAnimation);
            } else {
                animatorSet2.play(this.statusImageAnimation);
            }
        }
        if (stateChanged && processingAnimator != null) {
            this.processingImageAnimation.start();
        }
        animatorSet2.setDuration(300);
        animatorSet2.start();
        if (stateChanged) {
            if (this.voiceSearchState == com.navdy.hud.app.framework.voice.VoiceSearchNotification.VoiceSearchStateInternal.LISTENING) {
                this.listeningFeedbackView.setVisibility(0);
                if (this.listeningFeedbackAnimation == null) {
                    this.listeningFeedbackAnimation = getListeningFeedbackDefaultAnimation(this.listeningFeedbackView);
                }
                this.listeningFeedbackAnimation.start();
            } else {
                this.listeningFeedbackView.setVisibility(8);
                if (this.listeningFeedbackAnimation != null && this.listeningFeedbackAnimation.isRunning()) {
                    this.listeningFeedbackAnimation.cancel();
                }
            }
        }
        if (stateChanged && newState == com.navdy.hud.app.framework.voice.VoiceSearchNotification.VoiceSearchStateInternal.FAILED) {
            if (error2 == com.navdy.service.library.events.audio.VoiceSearchResponse.VoiceSearchError.NO_RESULTS_FOUND || error2 == com.navdy.service.library.events.audio.VoiceSearchResponse.VoiceSearchError.NO_WORDS_RECOGNIZED) {
                sLogger.d("VoiceSearch error , error :" + error2 + " , showing voice search");
                showHideVoiceSearchTipsIfNeeded(true);
            }
        }
    }

    public void onStop() {
        this.handler.removeCallbacks(this.waitingTimeoutRunnable);
        this.controller = null;
        this.voiceSearchHandler.stopVoiceSearch();
        this.voiceSearchHandler.setVoiceSearchUserInterface(null);
        if (this.layout != null) {
            if (this.processingImageAnimation != null) {
                this.processingImageAnimation.removeAllListeners();
                this.processingImageAnimation.cancel();
                this.processingImageAnimation = null;
            }
            if (this.listeningFeedbackAnimation != null) {
                this.listeningFeedbackAnimation.removeAllListeners();
                this.listeningFeedbackAnimation.cancel();
                this.listeningFeedbackAnimation = null;
            }
        }
        com.navdy.hud.app.framework.toast.ToastManager.getInstance().disableToasts(false);
        sLogger.v("startVoiceSearch: enabled toast");
    }

    public android.view.View getView(android.content.Context context) {
        if (this.layout == null) {
            this.layout = (android.view.ViewGroup) android.view.LayoutInflater.from(context).inflate(com.navdy.hud.app.R.layout.notification_voice_search, null);
        }
        butterknife.ButterKnife.inject((java.lang.Object) this, (android.view.View) this.layout);
        android.content.res.Resources resources = context.getResources();
        int dismissColor = resources.getColor(com.navdy.hud.app.R.color.glance_dismiss);
        int retryColor = resources.getColor(com.navdy.hud.app.R.color.glance_ok_blue);
        int goColor = resources.getColor(com.navdy.hud.app.R.color.glance_ok_go);
        int showColor = resources.getColor(com.navdy.hud.app.R.color.glance_ok_blue);
        com.navdy.hud.app.ui.component.ChoiceLayout2.Choice dismissChoice = new com.navdy.hud.app.ui.component.ChoiceLayout2.Choice(com.navdy.hud.app.R.id.dismiss, com.navdy.hud.app.R.drawable.icon_glances_dismiss, dismissColor, com.navdy.hud.app.R.drawable.icon_glances_dismiss, -16777216, resources.getString(com.navdy.hud.app.R.string.cancel), dismissColor);
        this.voiceSearchFailedChoices = new java.util.ArrayList();
        this.voiceSearchFailedChoices.add(new com.navdy.hud.app.ui.component.ChoiceLayout2.Choice(com.navdy.hud.app.R.id.retry, com.navdy.hud.app.R.drawable.icon_glances_retry, retryColor, com.navdy.hud.app.R.drawable.icon_glances_retry, -16777216, resources.getString(com.navdy.hud.app.R.string.retry), retryColor));
        this.voiceSearchFailedChoices.add(dismissChoice);
        this.voiceSearchActiveChoices = new java.util.ArrayList();
        this.voiceSearchActiveChoices.add(dismissChoice);
        this.voiceSearchSuccessResultsChoices = new java.util.ArrayList();
        this.voiceSearchSuccessResultsChoices.add(new com.navdy.hud.app.ui.component.ChoiceLayout2.Choice(com.navdy.hud.app.R.id.list, com.navdy.hud.app.R.drawable.icon_glances_read, showColor, com.navdy.hud.app.R.drawable.icon_glances_read, -16777216, resources.getString(com.navdy.hud.app.R.string.view), showColor));
        this.voiceSearchSuccessResultsChoices.add(new com.navdy.hud.app.ui.component.ChoiceLayout2.Choice(com.navdy.hud.app.R.id.go, com.navdy.hud.app.R.drawable.icon_go, goColor, com.navdy.hud.app.R.drawable.icon_go, -16777216, resources.getString(com.navdy.hud.app.R.string.go), goColor));
        this.choiceLayout.setChoices(this.voiceSearchActiveChoices, 0, this);
        this.choiceLayout.setVisibility(0);
        return this.layout;
    }

    private void setChoices(com.navdy.hud.app.framework.voice.VoiceSearchNotification.VoiceSearchStateInternal newState) {
        switch (newState) {
            case FAILED:
                this.choiceLayout.setVisibility(0);
                this.choiceLayout.setChoices(this.voiceSearchFailedChoices, 0, this);
                return;
            case RESULTS:
                if (this.results != null && this.results.size() > 1) {
                    this.choiceLayout.setVisibility(0);
                    this.choiceLayout.setChoices(this.voiceSearchSuccessResultsChoices, 0, this);
                    return;
                }
                return;
            case LISTENING:
            case SEARCHING:
                this.choiceLayout.setVisibility(0);
                this.choiceLayout.setChoices(this.voiceSearchActiveChoices, 0, this);
                return;
            default:
                this.choiceLayout.setVisibility(8);
                return;
        }
    }

    public void onUpdate() {
    }

    public int getTimeout() {
        return 0;
    }

    public boolean isAlive() {
        return false;
    }

    public boolean isPurgeable() {
        return false;
    }

    public boolean canAddToStackIfCurrentExists() {
        return true;
    }

    public int getColor() {
        return 0;
    }

    public void onNotificationEvent(com.navdy.hud.app.ui.framework.UIStateManager.Mode mode) {
    }

    public void onExpandedNotificationEvent(com.navdy.hud.app.ui.framework.UIStateManager.Mode mode) {
    }

    public void onExpandedNotificationSwitched() {
    }

    public android.animation.AnimatorSet getViewSwitchAnimation(boolean viewIn) {
        return null;
    }

    public boolean expandNotification() {
        return false;
    }

    public boolean supportScroll() {
        return false;
    }

    public boolean onKey(com.navdy.hud.app.manager.InputManager.CustomKeyEvent event) {
        switch (event) {
            case LEFT:
                this.choiceLayout.moveSelectionLeft();
                return true;
            case RIGHT:
                this.choiceLayout.moveSelectionRight();
                return true;
            case SELECT:
                this.choiceLayout.executeSelectedItem();
                return true;
            default:
                return false;
        }
    }

    private void dismissNotification() {
        com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().removeNotification(com.navdy.hud.app.framework.notifications.NotificationId.VOICE_SEARCH_NOTIFICATION_ID);
    }

    public void onClick() {
    }

    public void onTrackHand(float normalized) {
    }

    public boolean onGesture(com.navdy.service.library.events.input.GestureEvent event) {
        if (this.multipleResultsMode) {
            switch (event.gesture) {
                case GESTURE_SWIPE_LEFT:
                    this.voiceSearchHandler.showResults();
                    return true;
                case GESTURE_SWIPE_RIGHT:
                    this.voiceSearchHandler.go();
                    dismissNotification();
                    return true;
            }
        }
        return false;
    }

    public com.navdy.hud.app.manager.InputManager.IInputHandler nextHandler() {
        return null;
    }

    public void executeItem(com.navdy.hud.app.ui.component.ChoiceLayout2.Selection selection) {
        switch (selection.id) {
            case com.navdy.hud.app.R.id.dismiss /*2131623947*/:
                com.navdy.hud.app.analytics.AnalyticsSupport.recordRouteSelectionCancelled();
                dismissNotification();
                return;
            case com.navdy.hud.app.R.id.go /*2131623952*/:
                com.navdy.hud.app.analytics.AnalyticsSupport.recordVoiceSearchAdditionalResultsAction(com.navdy.hud.app.analytics.AnalyticsSupport.VoiceSearchAdditionalResultsAction.ADDITIONAL_RESULTS_GO);
                this.voiceSearchHandler.go();
                dismissNotification();
                return;
            case com.navdy.hud.app.R.id.list /*2131623955*/:
                com.navdy.hud.app.analytics.AnalyticsSupport.recordVoiceSearchAdditionalResultsAction(com.navdy.hud.app.analytics.AnalyticsSupport.VoiceSearchAdditionalResultsAction.ADDITIONAL_RESULTS_LIST);
                this.voiceSearchHandler.showResults();
                return;
            case com.navdy.hud.app.R.id.retry /*2131624048*/:
                com.navdy.hud.app.analytics.AnalyticsSupport.recordVoiceSearchRetry();
                showHideVoiceSearchTipsIfNeeded(false);
                startVoiceSearch();
                return;
            default:
                return;
        }
    }

    public void itemSelected(com.navdy.hud.app.ui.component.ChoiceLayout2.Selection selection) {
    }

    public static android.animation.AnimatorSet getBadgeAnimation(android.view.View view, boolean show) {
        view.setAlpha(1.0f);
        android.animation.AnimatorSet animationSet = new android.animation.AnimatorSet();
        animationSet.addListener(new com.navdy.hud.app.framework.voice.VoiceSearchNotification.Anon5(view));
        if (show) {
            view.setTranslationY(-10.0f);
            android.animation.ObjectAnimator translateY = android.animation.ObjectAnimator.ofFloat(view, "translationY", new float[]{-10.0f, 0.0f});
            animationSet.setDuration(200);
            animationSet.play(translateY);
            animationSet.setInterpolator(new android.view.animation.BounceInterpolator());
        } else {
            view.setTranslationY(0.0f);
            android.animation.ObjectAnimator alpha = android.animation.ObjectAnimator.ofFloat(view, "alpha", new float[]{1.0f, 0.0f});
            animationSet.setDuration(200);
            animationSet.play(alpha);
        }
        return animationSet;
    }

    public static android.animation.Animator getProcessingBadgeAnimation(android.view.View view) {
        android.animation.Animator processingAnimator = android.animation.ObjectAnimator.ofFloat(view, android.view.View.ROTATION, new float[]{360.0f});
        processingAnimator.setDuration(300);
        processingAnimator.setInterpolator(new android.view.animation.AccelerateDecelerateInterpolator());
        return processingAnimator;
    }

    public static android.animation.AnimatorSet getListeningFeedbackDefaultAnimation(android.view.View view) {
        android.animation.AnimatorSet animationSet = (android.animation.AnimatorSet) android.animation.AnimatorInflater.loadAnimator(view.getContext(), com.navdy.hud.app.R.animator.listening_feedback_animation);
        animationSet.setTarget(view);
        animationSet.addListener(new com.navdy.hud.app.framework.voice.VoiceSearchNotification.Anon6(animationSet));
        return animationSet;
    }
}
