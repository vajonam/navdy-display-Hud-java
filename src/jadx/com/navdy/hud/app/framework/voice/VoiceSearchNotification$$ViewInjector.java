package com.navdy.hud.app.framework.voice;

public class VoiceSearchNotification$$ViewInjector {
    public static void inject(butterknife.ButterKnife.Finder finder, com.navdy.hud.app.framework.voice.VoiceSearchNotification target, java.lang.Object source) {
        target.subStatus = (android.widget.TextView) finder.findRequiredView(source, com.navdy.hud.app.R.id.subStatus, "field 'subStatus'");
        target.imageInactive = (android.widget.ImageView) finder.findRequiredView(source, com.navdy.hud.app.R.id.image_voice_search_inactive, "field 'imageInactive'");
        target.imageActive = (android.widget.ImageView) finder.findRequiredView(source, com.navdy.hud.app.R.id.image_voice_search_active, "field 'imageActive'");
        target.imageStatus = (android.widget.ImageView) finder.findRequiredView(source, com.navdy.hud.app.R.id.img_status_badge, "field 'imageStatus'");
        target.imageProcessing = (android.widget.ImageView) finder.findRequiredView(source, com.navdy.hud.app.R.id.img_processing_badge, "field 'imageProcessing'");
        target.choiceLayout = (com.navdy.hud.app.ui.component.ChoiceLayout2) finder.findRequiredView(source, com.navdy.hud.app.R.id.choiceLayout, "field 'choiceLayout'");
        target.resultsCountContainer = (android.view.ViewGroup) finder.findRequiredView(source, com.navdy.hud.app.R.id.results_count_container, "field 'resultsCountContainer'");
        target.resultsCount = (android.widget.TextView) finder.findRequiredView(source, com.navdy.hud.app.R.id.results_count, "field 'resultsCount'");
        target.listeningFeedbackView = finder.findRequiredView(source, com.navdy.hud.app.R.id.voice_search_listening_feedback, "field 'listeningFeedbackView'");
    }

    public static void reset(com.navdy.hud.app.framework.voice.VoiceSearchNotification target) {
        target.subStatus = null;
        target.imageInactive = null;
        target.imageActive = null;
        target.imageStatus = null;
        target.imageProcessing = null;
        target.choiceLayout = null;
        target.resultsCountContainer = null;
        target.resultsCount = null;
        target.listeningFeedbackView = null;
    }
}
