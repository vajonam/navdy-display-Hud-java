package com.navdy.hud.app.framework.voice;

public class VoiceSearchTipsView extends android.widget.RelativeLayout implements com.navdy.hud.app.ui.activity.Main.INotificationExtensionView {
    public static final int ANIMATION_DURATION = 600;
    public static final int ANIMATION_INTERVAL = 3000;
    /* access modifiers changed from: private */
    public static int TIPS_COUNT;
    private static android.content.res.TypedArray VOICE_SEARCH_TIPS_ICONS;
    /* access modifiers changed from: private */
    public static java.lang.String[] VOICE_SEARCH_TIPS_TEXTS;
    /* access modifiers changed from: private */
    public android.animation.AnimatorSet animatorSet;
    @butterknife.InjectView(2131624522)
    public android.widget.ImageView audioSourceIcon;
    @butterknife.InjectView(2131624523)
    public android.widget.TextView audioSourceText;
    /* access modifiers changed from: private */
    public int currentTipIndex;
    private android.animation.Animator fadeIn;
    private android.animation.Animator fadeOut;
    /* access modifiers changed from: private */
    public android.animation.Animator fadeOutVoiceSearchInformationView;
    /* access modifiers changed from: private */
    public android.os.Handler handler;
    @butterknife.InjectView(2131624514)
    android.view.ViewGroup holder1;
    @butterknife.InjectView(2131624517)
    android.view.ViewGroup holder2;
    private boolean isListeningOverBluetooth;
    private java.lang.String langauge;
    private java.lang.String languageName;
    @butterknife.InjectView(2131624526)
    public android.widget.TextView localeFullNameText;
    @butterknife.InjectView(2131624525)
    public android.widget.TextView localeTagText;
    /* access modifiers changed from: private */
    public java.lang.Runnable runnable;
    /* access modifiers changed from: private */
    public boolean runningForFirstTime;
    @butterknife.InjectView(2131624515)
    android.widget.ImageView tipsIcon1;
    @butterknife.InjectView(2131624518)
    android.widget.ImageView tipsIcon2;
    @butterknife.InjectView(2131624516)
    android.widget.TextView tipsText1;
    @butterknife.InjectView(2131624519)
    android.widget.TextView tipsText2;
    @butterknife.InjectView(2131624520)
    public android.view.View voiceSearchInformationView;

    class Anon1 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
        Anon1() {
        }

        public void onAnimationEnd(android.animation.Animator animation) {
            super.onAnimationEnd(animation);
            com.navdy.hud.app.framework.voice.VoiceSearchTipsView.this.handler.post(com.navdy.hud.app.framework.voice.VoiceSearchTipsView.this.runnable);
        }
    }

    class Anon2 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
        Anon2() {
        }

        public void onAnimationEnd(android.animation.Animator animation) {
            super.onAnimationEnd(animation);
            com.navdy.hud.app.framework.voice.VoiceSearchTipsView.this.currentTipIndex = (com.navdy.hud.app.framework.voice.VoiceSearchTipsView.this.currentTipIndex + 1) % com.navdy.hud.app.framework.voice.VoiceSearchTipsView.TIPS_COUNT;
            java.lang.String tipText = com.navdy.hud.app.framework.voice.VoiceSearchTipsView.VOICE_SEARCH_TIPS_TEXTS[com.navdy.hud.app.framework.voice.VoiceSearchTipsView.this.currentTipIndex];
            int icon = com.navdy.hud.app.framework.voice.VoiceSearchTipsView.this.getIconIdForIndex(com.navdy.hud.app.framework.voice.VoiceSearchTipsView.this.currentTipIndex);
            com.navdy.hud.app.framework.voice.VoiceSearchTipsView.this.tipsText1.setText(tipText);
            com.navdy.hud.app.framework.voice.VoiceSearchTipsView.this.tipsIcon1.setImageResource(icon);
            com.navdy.hud.app.framework.voice.VoiceSearchTipsView.this.holder1.setAlpha(1.0f);
            com.navdy.hud.app.framework.voice.VoiceSearchTipsView.this.holder2.setAlpha(0.0f);
        }
    }

    class Anon3 implements java.lang.Runnable {
        Anon3() {
        }

        public void run() {
            if (com.navdy.hud.app.framework.voice.VoiceSearchTipsView.this.runningForFirstTime) {
                com.navdy.hud.app.framework.voice.VoiceSearchTipsView.this.currentTipIndex = 0;
                java.lang.String tipText = com.navdy.hud.app.framework.voice.VoiceSearchTipsView.VOICE_SEARCH_TIPS_TEXTS[com.navdy.hud.app.framework.voice.VoiceSearchTipsView.this.currentTipIndex];
                int icon = com.navdy.hud.app.framework.voice.VoiceSearchTipsView.this.getIconIdForIndex(com.navdy.hud.app.framework.voice.VoiceSearchTipsView.this.currentTipIndex);
                com.navdy.hud.app.framework.voice.VoiceSearchTipsView.this.tipsText1.setText(tipText);
                com.navdy.hud.app.framework.voice.VoiceSearchTipsView.this.tipsIcon1.setImageResource(icon);
                com.navdy.hud.app.framework.voice.VoiceSearchTipsView.this.holder1.setAlpha(1.0f);
                com.navdy.hud.app.framework.voice.VoiceSearchTipsView.this.holder2.setAlpha(0.0f);
                com.navdy.hud.app.framework.voice.VoiceSearchTipsView.this.runningForFirstTime = false;
            } else {
                int nextTipIndex = (com.navdy.hud.app.framework.voice.VoiceSearchTipsView.this.currentTipIndex + 1) % com.navdy.hud.app.framework.voice.VoiceSearchTipsView.TIPS_COUNT;
                java.lang.String tipText2 = com.navdy.hud.app.framework.voice.VoiceSearchTipsView.VOICE_SEARCH_TIPS_TEXTS[nextTipIndex];
                int icon2 = com.navdy.hud.app.framework.voice.VoiceSearchTipsView.this.getIconIdForIndex(nextTipIndex);
                com.navdy.hud.app.framework.voice.VoiceSearchTipsView.this.tipsText2.setText(tipText2);
                com.navdy.hud.app.framework.voice.VoiceSearchTipsView.this.tipsIcon2.setImageResource(icon2);
                com.navdy.hud.app.framework.voice.VoiceSearchTipsView.this.animatorSet.start();
            }
            com.navdy.hud.app.framework.voice.VoiceSearchTipsView.this.handler.postDelayed(this, com.navdy.hud.app.device.gps.GpsManager.MINIMUM_DRIVE_TIME);
        }
    }

    class Anon4 implements java.lang.Runnable {
        Anon4() {
        }

        public void run() {
            com.navdy.hud.app.framework.voice.VoiceSearchTipsView.this.fadeOutVoiceSearchInformationView.start();
        }
    }

    static {
        TIPS_COUNT = 0;
        android.content.res.Resources resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        VOICE_SEARCH_TIPS_TEXTS = resources.getStringArray(com.navdy.hud.app.R.array.voice_search_tips);
        TIPS_COUNT = VOICE_SEARCH_TIPS_TEXTS.length;
        VOICE_SEARCH_TIPS_ICONS = resources.obtainTypedArray(com.navdy.hud.app.R.array.voice_search_tips_icons);
    }

    public VoiceSearchTipsView(android.content.Context context) {
        this(context, null);
    }

    public VoiceSearchTipsView(android.content.Context context, android.util.AttributeSet attrs) {
        this(context, attrs, -1);
    }

    public VoiceSearchTipsView(android.content.Context context, android.util.AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.isListeningOverBluetooth = false;
        this.currentTipIndex = 0;
        this.runningForFirstTime = true;
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
        butterknife.ButterKnife.inject((android.view.View) this);
        this.fadeIn = android.animation.AnimatorInflater.loadAnimator(getContext(), 17498112);
        this.fadeOut = android.animation.AnimatorInflater.loadAnimator(getContext(), 17498113);
        this.fadeIn.setTarget(this.holder2);
        this.fadeOut.setTarget(this.holder1);
        this.fadeOutVoiceSearchInformationView = android.animation.AnimatorInflater.loadAnimator(getContext(), 17498113);
        this.fadeOutVoiceSearchInformationView.setTarget(this.voiceSearchInformationView);
        this.fadeOutVoiceSearchInformationView.addListener(new com.navdy.hud.app.framework.voice.VoiceSearchTipsView.Anon1());
        this.animatorSet = new android.animation.AnimatorSet();
        this.animatorSet.addListener(new com.navdy.hud.app.framework.voice.VoiceSearchTipsView.Anon2());
        this.animatorSet.setDuration(600);
        this.animatorSet.playTogether(new android.animation.Animator[]{this.fadeIn, this.fadeOut});
        this.handler = new android.os.Handler();
        this.runnable = new com.navdy.hud.app.framework.voice.VoiceSearchTipsView.Anon3();
    }

    public void setListeningOverBluetooth(boolean isListeningOverBluetooth2) {
        this.isListeningOverBluetooth = isListeningOverBluetooth2;
    }

    public void setVoiceSearchLocale(java.util.Locale locale) {
        this.langauge = locale.getLanguage();
        this.languageName = locale.getDisplayLanguage();
    }

    /* access modifiers changed from: private */
    public int getIconIdForIndex(int index) {
        return VOICE_SEARCH_TIPS_ICONS.getResourceId(index, -1);
    }

    public void onStart() {
        this.currentTipIndex = 0;
        if (this.isListeningOverBluetooth) {
            this.audioSourceIcon.setImageResource(com.navdy.hud.app.R.drawable.icon_badge_bt_tips);
            this.audioSourceText.setText(com.navdy.hud.app.R.string.bluetooth_microphone);
        } else {
            this.audioSourceIcon.setImageResource(com.navdy.hud.app.R.drawable.icon_badge_phone_source);
            this.audioSourceText.setText(com.navdy.hud.app.R.string.phone_microphone);
        }
        this.localeTagText.setText(this.langauge);
        this.localeFullNameText.setText(this.languageName);
        this.handler.postDelayed(new com.navdy.hud.app.framework.voice.VoiceSearchTipsView.Anon4(), com.navdy.hud.app.device.gps.GpsManager.MINIMUM_DRIVE_TIME);
    }

    public void onStop() {
        this.handler.removeCallbacks(this.runnable);
    }
}
