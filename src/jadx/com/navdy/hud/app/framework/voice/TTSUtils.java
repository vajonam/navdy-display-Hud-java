package com.navdy.hud.app.framework.voice;

public class TTSUtils {
    public static final java.lang.String DEBUG_GPS_CALIBRATION_IMU_DONE = "Ublox IMU Calibration Done";
    public static final java.lang.String DEBUG_GPS_CALIBRATION_SENSOR_DONE = "Ublox Sensor Calibration Done";
    public static final java.lang.String DEBUG_GPS_CALIBRATION_SENSOR_NOT_NEEDED = "Ublox Sensor Calibration Not Needed";
    public static final java.lang.String DEBUG_GPS_CALIBRATION_STARTED = "Ublox Calibration started";
    private static final java.lang.String DEBUG_GPS_SWITCH_TOAST_ID = "debug-tts-gps-switch";
    public static final java.lang.String DEBUG_TTS_FASTER_ROUTE_AVAILABLE = "Faster route is available";
    public static final java.lang.String DEBUG_TTS_TRAFFIC_NOT_USED_IN_ROUTE_CALC = "Traffic Not used for Route Calculation";
    public static final java.lang.String DEBUG_TTS_UBLOX_DR_ENDED = "Dead Reckoning has Stopped";
    public static final java.lang.String DEBUG_TTS_UBLOX_DR_STARTED = "Dead Reckoning has Started";
    public static final java.lang.String DEBUG_TTS_UBLOX_HAS_LOCATION_FIX = "Ublox Has Location Fix";
    public static final java.lang.String DEBUG_TTS_USING_GPS_SPEED = "Using GPS RawSpeed";
    public static final java.lang.String DEBUG_TTS_USING_OBD_SPEED = "Using OBD RawSpeed";
    public static final java.lang.String TTS_AUTOMATIC_ZOOM;
    public static final java.lang.String TTS_DIAL_BATTERY_EXTREMELY_LOW;
    public static final java.lang.String TTS_DIAL_BATTERY_LOW;
    public static final java.lang.String TTS_DIAL_BATTERY_VERY_LOW;
    public static final java.lang.String TTS_DIAL_CONNECTED;
    public static final java.lang.String TTS_DIAL_DISCONNECTED;
    public static final java.lang.String TTS_DIAL_FORGOTTEN;
    public static final java.lang.String TTS_FUEL_GAUGE_ADDED;
    public static final java.lang.String TTS_FUEL_RANGE_GAUGE_ADDED;
    public static final java.lang.String TTS_GLANCES_DISABLED;
    public static final java.lang.String TTS_HEADING_GAUGE_ADDED;
    public static final java.lang.String TTS_MANUAL_ZOOM;
    public static final java.lang.String TTS_NAV_STOPPED;
    public static final java.lang.String TTS_PHONE_BATTERY_LOW;
    public static final java.lang.String TTS_PHONE_BATTERY_VERY_LOW;
    public static final java.lang.String TTS_PHONE_OFFLINE;
    public static final java.lang.String TTS_RPM_GAUGE_ADDED;
    public static final java.lang.String TTS_TBT_DISABLED;
    public static final java.lang.String TTS_TBT_ENABLED;
    public static final java.lang.String TTS_TRAFFIC_DISABLED;
    public static final java.lang.String TTS_TRAFFIC_ENABLED;
    private static final com.squareup.otto.Bus bus = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getBus();
    private static final boolean isDebugTTSEnabled = new java.io.File("/sdcard/debug_tts").exists();

    static {
        android.content.res.Resources resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        TTS_GLANCES_DISABLED = resources.getString(com.navdy.hud.app.R.string.glances_disabled);
        TTS_DIAL_DISCONNECTED = resources.getString(com.navdy.hud.app.R.string.tts_dial_disconnected);
        TTS_DIAL_CONNECTED = resources.getString(com.navdy.hud.app.R.string.tts_dial_connected);
        TTS_DIAL_FORGOTTEN = resources.getString(com.navdy.hud.app.R.string.tts_dial_forgotten);
        TTS_TRAFFIC_DISABLED = resources.getString(com.navdy.hud.app.R.string.tts_traffic_disabled);
        TTS_TRAFFIC_ENABLED = resources.getString(com.navdy.hud.app.R.string.tts_traffic_enabled);
        TTS_NAV_STOPPED = resources.getString(com.navdy.hud.app.R.string.tts_nav_stopped);
        TTS_TBT_DISABLED = resources.getString(com.navdy.hud.app.R.string.tts_tbt_disabled);
        TTS_TBT_ENABLED = resources.getString(com.navdy.hud.app.R.string.tts_tbt_enabled);
        TTS_FUEL_RANGE_GAUGE_ADDED = resources.getString(com.navdy.hud.app.R.string.tts_fuel_range_gauge_added);
        TTS_FUEL_GAUGE_ADDED = resources.getString(com.navdy.hud.app.R.string.tts_fuel_gauge_added);
        TTS_RPM_GAUGE_ADDED = resources.getString(com.navdy.hud.app.R.string.tts_rpm_gauge_added);
        TTS_HEADING_GAUGE_ADDED = resources.getString(com.navdy.hud.app.R.string.tts_heading_gauge_added);
        TTS_DIAL_BATTERY_VERY_LOW = resources.getString(com.navdy.hud.app.R.string.tts_dial_battery_very_low);
        TTS_DIAL_BATTERY_LOW = resources.getString(com.navdy.hud.app.R.string.tts_dial_battery_low);
        TTS_DIAL_BATTERY_EXTREMELY_LOW = resources.getString(com.navdy.hud.app.R.string.tts_dial_battery_extremely_low);
        TTS_PHONE_BATTERY_VERY_LOW = resources.getString(com.navdy.hud.app.R.string.tts_phone_battery_very_low);
        TTS_PHONE_BATTERY_LOW = resources.getString(com.navdy.hud.app.R.string.tts_phone_battery_low);
        TTS_PHONE_OFFLINE = resources.getString(com.navdy.hud.app.R.string.phone_no_network);
        TTS_MANUAL_ZOOM = resources.getString(com.navdy.hud.app.R.string.tts_manual_zoom);
        TTS_AUTOMATIC_ZOOM = resources.getString(com.navdy.hud.app.R.string.tts_auto_zoon);
    }

    public static void sendSpeechRequest(java.lang.String str, java.lang.String id) {
        sendSpeechRequest(str, com.navdy.service.library.events.audio.Category.SPEECH_NOTIFICATION, id);
    }

    public static void sendSpeechRequest(java.lang.String str, com.navdy.service.library.events.audio.Category category, java.lang.String id) {
        if (!android.text.TextUtils.isEmpty(str)) {
            bus.post(new com.navdy.hud.app.event.LocalSpeechRequest(new com.navdy.service.library.events.audio.SpeechRequest.Builder().words(str).category(category).id(id).sendStatus(java.lang.Boolean.valueOf(false)).language(java.util.Locale.getDefault().toLanguageTag()).build()));
        }
    }

    public static boolean isDebugTTSEnabled() {
        return isDebugTTSEnabled;
    }

    public static void debugShowNoTrafficToast() {
        if (isDebugTTSEnabled) {
            android.os.Bundle bundle = new android.os.Bundle();
            bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_TIMEOUT, 3000);
            bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_IMAGE, com.navdy.hud.app.R.drawable.icon_open_nav_disable_traffic);
            bundle.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_2, DEBUG_TTS_TRAFFIC_NOT_USED_IN_ROUTE_CALC);
            bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_2_STYLE, com.navdy.hud.app.R.style.Glances_1);
            bundle.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_TTS, DEBUG_TTS_TRAFFIC_NOT_USED_IN_ROUTE_CALC);
            com.navdy.hud.app.framework.toast.ToastManager.getInstance().addToast(new com.navdy.hud.app.framework.toast.ToastManager.ToastParams("debug-tts-no-traffic", bundle, null, true, false));
        }
    }

    public static void debugShowGotUbloxFix() {
        if (isDebugTTSEnabled && com.navdy.hud.app.util.DeviceUtil.isNavdyDevice()) {
            android.os.Bundle bundle = new android.os.Bundle();
            bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_TIMEOUT, 3000);
            bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_IMAGE, com.navdy.hud.app.R.drawable.icon_gpserror);
            bundle.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_2, DEBUG_TTS_UBLOX_HAS_LOCATION_FIX);
            bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_2_STYLE, com.navdy.hud.app.R.style.Glances_1);
            bundle.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_TTS, DEBUG_TTS_UBLOX_HAS_LOCATION_FIX);
            com.navdy.hud.app.framework.toast.ToastManager.getInstance().addToast(new com.navdy.hud.app.framework.toast.ToastManager.ToastParams("debug-tts-ublox-fix", bundle, null, true, false));
        }
    }

    public static void debugShowGpsSwitch(java.lang.String title, java.lang.String info) {
        if (isDebugTTSEnabled && com.navdy.hud.app.util.DeviceUtil.isNavdyDevice()) {
            android.os.Bundle bundle = new android.os.Bundle();
            bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_TIMEOUT, 3000);
            bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_IMAGE, com.navdy.hud.app.R.drawable.icon_gpserror);
            bundle.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_2, title);
            bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_2_STYLE, com.navdy.hud.app.R.style.Glances_1);
            bundle.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_3, info);
            bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_3_STYLE, com.navdy.hud.app.R.style.Glances_2);
            bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_INFO_CONTAINER_MAX_WIDTH, (int) com.navdy.hud.app.HudApplication.getAppContext().getResources().getDimension(com.navdy.hud.app.R.dimen.no_glances_width));
            bundle.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_TTS, title);
            com.navdy.hud.app.framework.toast.ToastManager toastManager = com.navdy.hud.app.framework.toast.ToastManager.getInstance();
            toastManager.dismissCurrentToast(DEBUG_GPS_SWITCH_TOAST_ID);
            toastManager.addToast(new com.navdy.hud.app.framework.toast.ToastManager.ToastParams(DEBUG_GPS_SWITCH_TOAST_ID, bundle, null, true, false));
        }
    }

    public static void debugShowDRStarted(java.lang.String drType) {
        if (isDebugTTSEnabled && com.navdy.hud.app.util.DeviceUtil.isNavdyDevice()) {
            android.os.Bundle bundle = new android.os.Bundle();
            bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_TIMEOUT, 3000);
            bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_IMAGE, com.navdy.hud.app.R.drawable.icon_gpserror);
            bundle.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_1, drType);
            bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_1_STYLE, com.navdy.hud.app.R.style.Glances_1);
            bundle.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_2, DEBUG_TTS_UBLOX_DR_STARTED);
            bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_2_STYLE, com.navdy.hud.app.R.style.Glances_1);
            bundle.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_TTS, DEBUG_TTS_UBLOX_DR_STARTED);
            com.navdy.hud.app.framework.toast.ToastManager.getInstance().addToast(new com.navdy.hud.app.framework.toast.ToastManager.ToastParams("debug-tts-ublox-dr-start", bundle, null, true, false));
        }
    }

    public static void debugShowDREnded() {
        if (isDebugTTSEnabled && com.navdy.hud.app.util.DeviceUtil.isNavdyDevice()) {
            android.os.Bundle bundle = new android.os.Bundle();
            bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_TIMEOUT, 3000);
            bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_IMAGE, com.navdy.hud.app.R.drawable.icon_gpserror);
            bundle.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_2, DEBUG_TTS_UBLOX_DR_ENDED);
            bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_2_STYLE, com.navdy.hud.app.R.style.Glances_1);
            bundle.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_TTS, DEBUG_TTS_UBLOX_DR_ENDED);
            com.navdy.hud.app.framework.toast.ToastManager.getInstance().addToast(new com.navdy.hud.app.framework.toast.ToastManager.ToastParams("debug-tts-ublox-dr-end", bundle, null, true, false));
        }
    }

    public static void debugShowSpeedInput(java.lang.String str) {
        if (isDebugTTSEnabled && com.navdy.hud.app.util.DeviceUtil.isNavdyDevice()) {
            android.os.Bundle bundle = new android.os.Bundle();
            bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_TIMEOUT, 3000);
            bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_IMAGE, com.navdy.hud.app.R.drawable.icon_mm_dash);
            bundle.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_2, str);
            bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_2_STYLE, com.navdy.hud.app.R.style.Glances_1);
            bundle.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_TTS, str);
            com.navdy.hud.app.framework.toast.ToastManager.getInstance().addToast(new com.navdy.hud.app.framework.toast.ToastManager.ToastParams("debug-tts-speed-input", bundle, null, true, false));
        }
    }

    public static void debugShowFasterRouteToast(java.lang.String title, java.lang.String info) {
        if (isDebugTTSEnabled) {
            android.os.Bundle bundle = new android.os.Bundle();
            bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_TIMEOUT, 7000);
            bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_IMAGE, com.navdy.hud.app.R.drawable.icon_open_nav_disable_traffic);
            bundle.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_2, title);
            bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_2_STYLE, com.navdy.hud.app.R.style.Glances_1);
            bundle.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_3, info);
            bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_3_STYLE, com.navdy.hud.app.R.style.Glances_2);
            bundle.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_TTS, DEBUG_TTS_FASTER_ROUTE_AVAILABLE);
            bundle.putBoolean(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_DEFAULT_CHOICE, true);
            com.navdy.hud.app.framework.toast.ToastManager.getInstance().addToast(new com.navdy.hud.app.framework.toast.ToastManager.ToastParams("debug-tts-faster-route", bundle, null, true, false));
        }
    }

    public static void debugShowGpsReset(java.lang.String title) {
        if (isDebugTTSEnabled && com.navdy.hud.app.util.DeviceUtil.isNavdyDevice()) {
            android.os.Bundle bundle = new android.os.Bundle();
            bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_TIMEOUT, 3000);
            bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_IMAGE, com.navdy.hud.app.R.drawable.icon_gpserror);
            bundle.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_2, title);
            bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_2_STYLE, com.navdy.hud.app.R.style.Glances_1);
            bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_INFO_CONTAINER_MAX_WIDTH, (int) com.navdy.hud.app.HudApplication.getAppContext().getResources().getDimension(com.navdy.hud.app.R.dimen.no_glances_width));
            bundle.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_TTS, title);
            com.navdy.hud.app.framework.toast.ToastManager.getInstance().addToast(new com.navdy.hud.app.framework.toast.ToastManager.ToastParams("debug-tts-gps-reset", bundle, null, true, false));
        }
    }

    public static void debugShowGpsCalibrationStarted() {
        if (com.navdy.hud.app.device.gps.GpsUtils.isDebugRawGpsPosEnabled() && com.navdy.hud.app.util.DeviceUtil.isNavdyDevice()) {
            android.os.Bundle bundle = new android.os.Bundle();
            bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_TIMEOUT, 3000);
            bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_IMAGE, com.navdy.hud.app.R.drawable.icon_gpserror);
            bundle.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_1, DEBUG_GPS_CALIBRATION_STARTED);
            bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_1_STYLE, com.navdy.hud.app.R.style.Glances_1);
            com.navdy.hud.app.framework.toast.ToastManager.getInstance().addToast(new com.navdy.hud.app.framework.toast.ToastManager.ToastParams("debug-gps-calibration_start", bundle, null, true, false));
        }
    }

    public static void debugShowGpsImuCalibrationDone() {
        if (com.navdy.hud.app.device.gps.GpsUtils.isDebugRawGpsPosEnabled() && com.navdy.hud.app.util.DeviceUtil.isNavdyDevice()) {
            android.os.Bundle bundle = new android.os.Bundle();
            bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_TIMEOUT, 3000);
            bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_IMAGE, com.navdy.hud.app.R.drawable.icon_gpserror);
            bundle.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_1, DEBUG_GPS_CALIBRATION_IMU_DONE);
            bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_1_STYLE, com.navdy.hud.app.R.style.Glances_1);
            com.navdy.hud.app.framework.toast.ToastManager.getInstance().addToast(new com.navdy.hud.app.framework.toast.ToastManager.ToastParams("debug-gps-calibration_imu", bundle, null, true, false));
        }
    }

    public static void debugShowGpsSensorCalibrationDone() {
        if (com.navdy.hud.app.device.gps.GpsUtils.isDebugRawGpsPosEnabled() && com.navdy.hud.app.util.DeviceUtil.isNavdyDevice()) {
            android.os.Bundle bundle = new android.os.Bundle();
            bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_TIMEOUT, 3000);
            bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_IMAGE, com.navdy.hud.app.R.drawable.icon_gpserror);
            bundle.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_1, DEBUG_GPS_CALIBRATION_SENSOR_DONE);
            bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_1_STYLE, com.navdy.hud.app.R.style.Glances_1);
            com.navdy.hud.app.framework.toast.ToastManager.getInstance().addToast(new com.navdy.hud.app.framework.toast.ToastManager.ToastParams("debug-gps-calibration_sensor", bundle, null, true, false));
        }
    }

    public static void debugShowGpsSensorCalibrationNotNeeded() {
        if (com.navdy.hud.app.device.gps.GpsUtils.isDebugRawGpsPosEnabled() && com.navdy.hud.app.util.DeviceUtil.isNavdyDevice()) {
            android.os.Bundle bundle = new android.os.Bundle();
            bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_TIMEOUT, 3000);
            bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_IMAGE, com.navdy.hud.app.R.drawable.icon_gpserror);
            bundle.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_1, DEBUG_GPS_CALIBRATION_SENSOR_NOT_NEEDED);
            bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_1_STYLE, com.navdy.hud.app.R.style.Glances_1);
            com.navdy.hud.app.framework.toast.ToastManager.getInstance().addToast(new com.navdy.hud.app.framework.toast.ToastManager.ToastParams("debug-gps-calibration_not_needed", bundle, null, true, false));
        }
    }
}
