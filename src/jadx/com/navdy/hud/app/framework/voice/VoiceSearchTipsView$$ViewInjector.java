package com.navdy.hud.app.framework.voice;

public class VoiceSearchTipsView$$ViewInjector {
    public static void inject(butterknife.ButterKnife.Finder finder, com.navdy.hud.app.framework.voice.VoiceSearchTipsView target, java.lang.Object source) {
        target.tipsText1 = (android.widget.TextView) finder.findRequiredView(source, com.navdy.hud.app.R.id.tip_text_1, "field 'tipsText1'");
        target.tipsText2 = (android.widget.TextView) finder.findRequiredView(source, com.navdy.hud.app.R.id.tip_text_2, "field 'tipsText2'");
        target.tipsIcon1 = (android.widget.ImageView) finder.findRequiredView(source, com.navdy.hud.app.R.id.tip_icon_1, "field 'tipsIcon1'");
        target.tipsIcon2 = (android.widget.ImageView) finder.findRequiredView(source, com.navdy.hud.app.R.id.tip_icon_2, "field 'tipsIcon2'");
        target.holder1 = (android.view.ViewGroup) finder.findRequiredView(source, com.navdy.hud.app.R.id.tip_holder_1, "field 'holder1'");
        target.holder2 = (android.view.ViewGroup) finder.findRequiredView(source, com.navdy.hud.app.R.id.tip_holder_2, "field 'holder2'");
        target.voiceSearchInformationView = finder.findRequiredView(source, com.navdy.hud.app.R.id.voice_search_information, "field 'voiceSearchInformationView'");
        target.audioSourceText = (android.widget.TextView) finder.findRequiredView(source, com.navdy.hud.app.R.id.audio_source_text, "field 'audioSourceText'");
        target.audioSourceIcon = (android.widget.ImageView) finder.findRequiredView(source, com.navdy.hud.app.R.id.audio_source_icon, "field 'audioSourceIcon'");
        target.localeTagText = (android.widget.TextView) finder.findRequiredView(source, com.navdy.hud.app.R.id.locale_tag_text, "field 'localeTagText'");
        target.localeFullNameText = (android.widget.TextView) finder.findRequiredView(source, com.navdy.hud.app.R.id.locale_full_text, "field 'localeFullNameText'");
    }

    public static void reset(com.navdy.hud.app.framework.voice.VoiceSearchTipsView target) {
        target.tipsText1 = null;
        target.tipsText2 = null;
        target.tipsIcon1 = null;
        target.tipsIcon2 = null;
        target.holder1 = null;
        target.holder2 = null;
        target.voiceSearchInformationView = null;
        target.audioSourceText = null;
        target.audioSourceIcon = null;
        target.localeTagText = null;
        target.localeFullNameText = null;
    }
}
