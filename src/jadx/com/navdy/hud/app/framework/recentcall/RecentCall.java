package com.navdy.hud.app.framework.recentcall;

public class RecentCall implements java.lang.Comparable<com.navdy.hud.app.framework.recentcall.RecentCall> {
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.framework.recentcall.RecentCall.class);
    public java.util.Date callTime;
    public com.navdy.hud.app.framework.recentcall.RecentCall.CallType callType;
    public com.navdy.hud.app.framework.recentcall.RecentCall.Category category;
    public int defaultImageIndex;
    public java.lang.String firstName;
    public java.lang.String formattedNumber;
    public java.lang.String initials;
    public java.lang.String name;
    public java.lang.String number;
    public com.navdy.hud.app.framework.contacts.NumberType numberType;
    public java.lang.String numberTypeStr;
    public long numericNumber;

    public enum CallType {
        UNNKNOWN(0),
        INCOMING(1),
        OUTGOING(2),
        MISSED(3);
        
        int value;

        private CallType(int value2) {
            this.value = value2;
        }

        public int getValue() {
            return this.value;
        }

        public static com.navdy.hud.app.framework.recentcall.RecentCall.CallType buildFromValue(int n) {
            switch (n) {
                case 1:
                    return INCOMING;
                case 2:
                    return OUTGOING;
                case 3:
                    return MISSED;
                default:
                    return UNNKNOWN;
            }
        }
    }

    public enum Category {
        UNNKNOWN(0),
        PHONE_CALL(1),
        MESSAGE(2);
        
        int value;

        private Category(int value2) {
            this.value = value2;
        }

        public int getValue() {
            return this.value;
        }

        public static com.navdy.hud.app.framework.recentcall.RecentCall.Category buildFromValue(int n) {
            switch (n) {
                case 1:
                    return PHONE_CALL;
                case 2:
                    return MESSAGE;
                default:
                    return UNNKNOWN;
            }
        }
    }

    public RecentCall(java.lang.String name2, com.navdy.hud.app.framework.recentcall.RecentCall.Category category2, java.lang.String number2, com.navdy.hud.app.framework.contacts.NumberType numberType2, java.util.Date callTime2, com.navdy.hud.app.framework.recentcall.RecentCall.CallType callType2, int defaultImageIndex2, long numericNumber2) {
        this.name = name2;
        this.number = number2;
        this.category = category2;
        this.numberType = numberType2;
        this.callTime = callTime2;
        this.callType = callType2;
        this.defaultImageIndex = defaultImageIndex2;
        validateArguments();
    }

    public RecentCall(com.navdy.hud.app.framework.recentcall.RecentCall call) {
        this.name = call.name;
        this.number = call.number;
        this.category = call.category;
        this.numberType = call.numberType;
        this.callTime = call.callTime;
        this.callType = call.callType;
        this.defaultImageIndex = call.defaultImageIndex;
    }

    public int compareTo(com.navdy.hud.app.framework.recentcall.RecentCall another) {
        if (another == null) {
            return 0;
        }
        return this.callTime.compareTo(another.callTime);
    }

    public void validateArguments() {
        if (!android.text.TextUtils.isEmpty(this.name)) {
            this.firstName = com.navdy.hud.app.framework.contacts.ContactUtil.getFirstName(this.name);
            this.initials = com.navdy.hud.app.framework.contacts.ContactUtil.getInitials(this.name);
        }
        this.numberTypeStr = com.navdy.hud.app.framework.contacts.ContactUtil.getPhoneType(this.numberType);
        this.formattedNumber = com.navdy.hud.app.util.PhoneUtil.formatPhoneNumber(this.number);
        if (android.text.TextUtils.isEmpty(this.number)) {
            return;
        }
        if (this.numericNumber > 0) {
            this.numericNumber = this.numericNumber;
            return;
        }
        try {
            this.numericNumber = com.google.i18n.phonenumbers.PhoneNumberUtil.getInstance().parse(this.number, com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentLocale().getCountry()).getNationalNumber();
        } catch (Throwable t) {
            if (sLogger.isLoggable(2)) {
                sLogger.e(t);
            }
        }
    }

    public java.lang.String toString() {
        return "RecentCall{name='" + this.name + '\'' + ", category=" + this.category + ", number='" + this.number + '\'' + ", numberType=" + this.numberType + ", callTime=" + this.callTime + ", callType=" + this.callType + ", defaultImageIndex=" + this.defaultImageIndex + '}';
    }
}
