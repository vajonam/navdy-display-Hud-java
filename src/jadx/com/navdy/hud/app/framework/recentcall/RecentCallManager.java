package com.navdy.hud.app.framework.recentcall;

public class RecentCallManager {
    private static final com.navdy.hud.app.framework.recentcall.RecentCallManager.RecentCallChanged RECENT_CALL_CHANGED = new com.navdy.hud.app.framework.recentcall.RecentCallManager.RecentCallChanged();
    private static final com.navdy.hud.app.framework.recentcall.RecentCallManager sInstance = new com.navdy.hud.app.framework.recentcall.RecentCallManager();
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.framework.recentcall.RecentCallManager.class);
    /* access modifiers changed from: private */
    public com.squareup.otto.Bus bus = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getBus();
    /* access modifiers changed from: private */
    public java.util.HashMap<java.lang.String, com.navdy.hud.app.framework.recentcall.RecentCall> contactRequestMap = new java.util.HashMap<>();
    private java.util.Map<java.lang.Long, com.navdy.hud.app.framework.recentcall.RecentCall> numberMap = new java.util.HashMap();
    private volatile java.util.List<com.navdy.hud.app.framework.recentcall.RecentCall> recentCalls;
    /* access modifiers changed from: private */
    public java.util.HashMap<java.lang.String, java.util.List<com.navdy.service.library.events.contacts.Contact>> tempContactLookupMap = new java.util.HashMap<>();

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            com.navdy.hud.app.framework.recentcall.RecentCallManager.this.load();
        }
    }

    class Anon2 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.service.library.events.contacts.ContactResponse val$event;

        Anon2(com.navdy.service.library.events.contacts.ContactResponse contactResponse) {
            this.val$event = contactResponse;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:60:0x01a4, code lost:
            if (r3 == null) goto L_?;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:62:?, code lost:
            com.navdy.hud.app.framework.recentcall.RecentCallManager.access$Anon100().v("contact found [" + r18.val$event.identifier + "] downloading photo");
            r13 = com.navdy.hud.app.framework.recentcall.RecentCallManager.access$Anon300(r18.this$Anon0);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:63:0x01d4, code lost:
            monitor-enter(r13);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:66:?, code lost:
            com.navdy.hud.app.framework.recentcall.RecentCallManager.access$Anon300(r18.this$Anon0).put(r18.val$event.identifier, r5);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:67:0x01e6, code lost:
            monitor-exit(r13);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:70:?, code lost:
            com.navdy.hud.app.framework.recentcall.RecentCallManager.access$Anon400(r18.this$Anon0).post(new com.navdy.hud.app.framework.recentcall.RecentCallManager.ContactFound(r18.val$event.identifier, r5));
            r9 = com.navdy.hud.app.framework.contacts.PhoneImageDownloader.getInstance();
            r12 = r3.iterator();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:72:0x0209, code lost:
            if (r12.hasNext() == false) goto L_0x0263;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:73:0x020b, code lost:
            r10 = (com.navdy.hud.app.framework.recentcall.RecentCall) r12.next();
            r9.clearPhotoCheckEntry(r10.number, com.navdy.service.library.events.photo.PhotoType.PHOTO_CONTACT);
            r9.submitDownload(r10.number, com.navdy.hud.app.framework.contacts.PhoneImageDownloader.Priority.HIGH, com.navdy.service.library.events.photo.PhotoType.PHOTO_CONTACT, r10.name);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:82:0x0268, code lost:
            if (r3.size() != 1) goto L_?;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:83:0x026a, code lost:
            com.navdy.hud.app.framework.recentcall.RecentCallManager.access$Anon500(r18.this$Anon0, r3);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:97:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:98:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:99:?, code lost:
            return;
         */
        /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
        public void run() {
            java.util.List list;
            java.util.List list2;
            try {
                if (android.text.TextUtils.isEmpty(this.val$event.identifier)) {
                    com.navdy.hud.app.framework.recentcall.RecentCallManager.sLogger.w("invalid contact repsonse");
                    return;
                }
                java.util.List list3 = null;
                java.util.List list4 = null;
                synchronized (com.navdy.hud.app.framework.recentcall.RecentCallManager.this.contactRequestMap) {
                    com.navdy.hud.app.framework.recentcall.RecentCall call = (com.navdy.hud.app.framework.recentcall.RecentCall) com.navdy.hud.app.framework.recentcall.RecentCallManager.this.contactRequestMap.remove(this.val$event.identifier);
                    if (call == null) {
                        com.navdy.hud.app.framework.recentcall.RecentCallManager.sLogger.i("identifer not found[" + this.val$event.identifier + "]");
                    } else if (this.val$event.status != com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS) {
                        com.navdy.hud.app.framework.recentcall.RecentCallManager.sLogger.i("request failed for [" + this.val$event.identifier + "] status[" + this.val$event.status + "]");
                    } else if (this.val$event.contacts == null || this.val$event.contacts.size() == 0) {
                        com.navdy.hud.app.framework.recentcall.RecentCallManager.sLogger.i("no contact returned for [" + this.val$event.identifier + "] status[" + this.val$event.status + "]");
                    } else {
                        com.navdy.hud.app.framework.recentcall.RecentCallManager.sLogger.v("contacts returned [" + this.val$event.contacts.size() + "]");
                        java.util.List list5 = null;
                        java.util.List list6 = null;
                        for (com.navdy.service.library.events.contacts.Contact obj : this.val$event.contacts) {
                            try {
                                if (android.text.TextUtils.isEmpty(obj.number) || !com.navdy.hud.app.framework.contacts.ContactUtil.isValidNumber(obj.number)) {
                                    com.navdy.hud.app.framework.recentcall.RecentCallManager.sLogger.i("no number for  [" + this.val$event.identifier + "] status[" + this.val$event.status + "]");
                                    list = list5;
                                    list2 = list6;
                                } else {
                                    com.navdy.hud.app.framework.recentcall.RecentCall copy = new com.navdy.hud.app.framework.recentcall.RecentCall(call);
                                    if (com.navdy.hud.app.framework.contacts.ContactUtil.isDisplayNameValid(obj.name, obj.number, com.navdy.hud.app.framework.contacts.ContactUtil.getPhoneNumber(obj.number))) {
                                        copy.name = obj.name;
                                    } else {
                                        copy.name = null;
                                    }
                                    copy.number = obj.number;
                                    copy.numberType = com.navdy.hud.app.framework.contacts.ContactUtil.getNumberType(obj.numberType);
                                    copy.validateArguments();
                                    if (list6 == null) {
                                        list2 = new java.util.ArrayList();
                                        try {
                                            list = new java.util.ArrayList();
                                        } catch (Throwable th) {
                                            th = th;
                                            java.util.List list7 = list5;
                                            throw th;
                                        }
                                    } else {
                                        list = list5;
                                        list2 = list6;
                                    }
                                    try {
                                        list2.add(copy);
                                        list.add(obj);
                                    } catch (Throwable th2) {
                                        th = th2;
                                    }
                                }
                                list5 = list;
                                list6 = list2;
                            } catch (Throwable th3) {
                                th = th3;
                                java.util.List list8 = list5;
                                java.util.List list9 = list6;
                            }
                        }
                        list4 = list5;
                        list3 = list6;
                    }
                }
            } catch (Throwable t) {
                com.navdy.hud.app.framework.recentcall.RecentCallManager.sLogger.e(t);
            }
        }
    }

    class Anon3 implements java.lang.Runnable {
        final /* synthetic */ java.util.List val$calls;

        Anon3(java.util.List list) {
            this.val$calls = list;
        }

        public void run() {
            com.navdy.hud.app.storage.db.helper.RecentCallPersistenceHelper.storeRecentCalls(com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile().getProfileName(), this.val$calls, false);
            for (com.navdy.hud.app.framework.recentcall.RecentCall c : this.val$calls) {
                com.navdy.hud.app.framework.recentcall.RecentCallManager.sLogger.v("added contact [" + c.number + "]");
            }
        }
    }

    public static class ContactFound {
        public java.util.List<com.navdy.service.library.events.contacts.Contact> contact;
        public java.lang.String identifier;

        public ContactFound(java.lang.String identifier2, java.util.List<com.navdy.service.library.events.contacts.Contact> contact2) {
            this.identifier = identifier2;
            this.contact = contact2;
        }
    }

    public static class RecentCallChanged {
    }

    public static com.navdy.hud.app.framework.recentcall.RecentCallManager getInstance() {
        return sInstance;
    }

    private RecentCallManager() {
        this.bus.register(this);
    }

    public java.util.List<com.navdy.hud.app.framework.recentcall.RecentCall> getRecentCalls() {
        return this.recentCalls;
    }

    public int getRecentCallsSize() {
        if (this.recentCalls != null) {
            return this.recentCalls.size();
        }
        return 0;
    }

    public void setRecentCalls(java.util.List<com.navdy.hud.app.framework.recentcall.RecentCall> recentCalls2) {
        this.recentCalls = recentCalls2;
        buildMap();
        this.bus.post(RECENT_CALL_CHANGED);
    }

    public void clearRecentCalls() {
        setRecentCalls(null);
    }

    public void buildRecentCalls() {
        synchronized (this.contactRequestMap) {
            this.contactRequestMap.clear();
        }
        if (com.navdy.hud.app.util.GenericUtil.isMainThread()) {
            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.framework.recentcall.RecentCallManager.Anon1(), 1);
        } else {
            load();
        }
    }

    /* access modifiers changed from: private */
    public void load() {
        try {
            java.lang.String id = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile().getProfileName();
            this.recentCalls = com.navdy.hud.app.storage.db.helper.RecentCallPersistenceHelper.getRecentsCalls(id);
            sLogger.v("load recentCall id[" + id + "] calls[" + this.recentCalls.size() + "]");
        } catch (Throwable t) {
            this.recentCalls = null;
            sLogger.e(t);
        } finally {
            buildMap();
            this.bus.post(RECENT_CALL_CHANGED);
        }
    }

    @com.squareup.otto.Subscribe
    public void onCallEvent(com.navdy.service.library.events.callcontrol.CallEvent event) {
        com.navdy.hud.app.framework.recentcall.RecentCall.CallType callType;
        if (android.text.TextUtils.isEmpty(event.number)) {
            sLogger.e("call event does not have a number, " + event.contact_name);
            return;
        }
        if (!event.incoming.booleanValue()) {
            callType = com.navdy.hud.app.framework.recentcall.RecentCall.CallType.OUTGOING;
        } else if (event.answered.booleanValue()) {
            callType = com.navdy.hud.app.framework.recentcall.RecentCall.CallType.INCOMING;
        } else {
            callType = com.navdy.hud.app.framework.recentcall.RecentCall.CallType.MISSED;
        }
        handleNewCall(new com.navdy.hud.app.framework.recentcall.RecentCall(event.contact_name, com.navdy.hud.app.framework.recentcall.RecentCall.Category.PHONE_CALL, event.number, com.navdy.hud.app.framework.contacts.NumberType.OTHER, new java.util.Date(), callType, -1, 0));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x004b, code lost:
        if (android.text.TextUtils.isEmpty(r1) != false) goto L_0x0057;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x004d, code lost:
        r8.number = r1;
        storeContactInfo(r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0057, code lost:
        requestContactInfo(r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:?, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:?, code lost:
        return false;
     */
    public boolean handleNewCall(com.navdy.hud.app.framework.recentcall.RecentCall call) {
        sLogger.v("handle new call:" + call.number);
        if (!com.navdy.hud.app.framework.contacts.ContactUtil.isValidNumber(call.number)) {
            java.lang.String number = null;
            synchronized (this.tempContactLookupMap) {
                java.util.List<com.navdy.service.library.events.contacts.Contact> list = (java.util.List) this.tempContactLookupMap.get(call.number);
                if (list != null) {
                    if (list.size() > 1) {
                        return false;
                    }
                    number = ((com.navdy.service.library.events.contacts.Contact) list.get(0)).number;
                }
            }
        } else {
            storeContactInfo(call);
            return true;
        }
    }

    private void requestContactInfo(com.navdy.hud.app.framework.recentcall.RecentCall call) {
        sLogger.v("number not available for [" + call.number + "]");
        synchronized (this.contactRequestMap) {
            if (this.contactRequestMap.containsKey(call.number)) {
                sLogger.v("contact [" + call.number + "] request already pending");
                return;
            }
            this.contactRequestMap.put(call.number, call);
            sLogger.v("contact [" + call.number + "] request sent");
            this.bus.post(new com.navdy.hud.app.event.RemoteEvent(new com.navdy.service.library.events.contacts.ContactRequest(call.number)));
        }
    }

    @com.squareup.otto.Subscribe
    public void onContactResponse(com.navdy.service.library.events.contacts.ContactResponse event) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.framework.recentcall.RecentCallManager.Anon2(event), 1);
    }

    public void clearContactLookupMap() {
        synchronized (this.tempContactLookupMap) {
            this.tempContactLookupMap.clear();
        }
    }

    public java.util.List<com.navdy.service.library.events.contacts.Contact> getContactsFromId(java.lang.String id) {
        java.util.List<com.navdy.service.library.events.contacts.Contact> list;
        synchronized (this.tempContactLookupMap) {
            list = (java.util.List) this.tempContactLookupMap.get(id);
        }
        return list;
    }

    private void storeContactInfo(com.navdy.hud.app.framework.recentcall.RecentCall call) {
        java.util.List<com.navdy.hud.app.framework.recentcall.RecentCall> list = new java.util.ArrayList<>();
        list.add(call);
        storeContactInfo(list);
    }

    /* access modifiers changed from: private */
    public void storeContactInfo(java.util.List<com.navdy.hud.app.framework.recentcall.RecentCall> calls) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.framework.recentcall.RecentCallManager.Anon3(calls), 1);
    }

    public int getDefaultContactImage(long number) {
        com.navdy.hud.app.framework.recentcall.RecentCall call = (com.navdy.hud.app.framework.recentcall.RecentCall) this.numberMap.get(java.lang.Long.valueOf(number));
        if (call == null) {
            return -1;
        }
        return call.defaultImageIndex;
    }

    private void buildMap() {
        this.numberMap.clear();
        if (this.recentCalls != null && this.recentCalls.size() != 0) {
            for (com.navdy.hud.app.framework.recentcall.RecentCall call : this.recentCalls) {
                this.numberMap.put(java.lang.Long.valueOf(call.numericNumber), call);
            }
        }
    }
}
