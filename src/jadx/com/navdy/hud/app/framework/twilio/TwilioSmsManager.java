package com.navdy.hud.app.framework.twilio;

public class TwilioSmsManager {
    private static final java.lang.String ACCOUNT_SID;
    private static final java.lang.String AUTH_TOKEN;
    private static final int CONNECTION_TIMEOUT = ((int) java.util.concurrent.TimeUnit.SECONDS.toMillis(15));
    /* access modifiers changed from: private */
    public static final java.lang.String CREDENTIALS = okhttp3.Credentials.basic(ACCOUNT_SID, AUTH_TOKEN);
    /* access modifiers changed from: private */
    public static final java.lang.String MESSAGE_SERVICE_ID;
    private static final int READ_TIMEOUT = ((int) java.util.concurrent.TimeUnit.SECONDS.toMillis(30));
    /* access modifiers changed from: private */
    public static final java.lang.String SMS_ENDPOINT = ("https://api.twilio.com/2010-04-01/Accounts/" + ACCOUNT_SID + "/Messages.json");
    private static final int WRITE_TIMEOUT = ((int) java.util.concurrent.TimeUnit.SECONDS.toMillis(30));
    private static com.navdy.hud.app.framework.twilio.TwilioSmsManager instance = new com.navdy.hud.app.framework.twilio.TwilioSmsManager();
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.framework.twilio.TwilioSmsManager.class);
    private final com.navdy.hud.app.profile.DriverProfileManager driverProfileManager = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getDriverProfileManager();
    /* access modifiers changed from: private */
    public final okhttp3.OkHttpClient okHttpClient = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getHttpManager().getClient().newBuilder().readTimeout((long) READ_TIMEOUT, java.util.concurrent.TimeUnit.MILLISECONDS).writeTimeout((long) WRITE_TIMEOUT, java.util.concurrent.TimeUnit.MILLISECONDS).connectTimeout((long) CONNECTION_TIMEOUT, java.util.concurrent.TimeUnit.MILLISECONDS).build();

    class Anon1 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.hud.app.framework.twilio.TwilioSmsManager.Callback val$cb;
        final /* synthetic */ java.lang.String val$message;
        final /* synthetic */ java.lang.String val$number;
        final /* synthetic */ java.lang.String val$profileId;

        Anon1(java.lang.String str, java.lang.String str2, java.lang.String str3, com.navdy.hud.app.framework.twilio.TwilioSmsManager.Callback callback) {
            this.val$message = str;
            this.val$number = str2;
            this.val$profileId = str3;
            this.val$cb = callback;
        }

        public void run() {
            java.lang.String formattedMessage;
            try {
                com.navdy.hud.app.framework.twilio.TwilioSmsManager.sLogger.v("sendSms-start");
                java.lang.String driverName = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile().getFirstName();
                if (!android.text.TextUtils.isEmpty(driverName)) {
                    formattedMessage = com.navdy.hud.app.HudApplication.getAppContext().getString(com.navdy.hud.app.R.string.ios_sms_format, new java.lang.Object[]{driverName, this.val$message});
                } else {
                    formattedMessage = this.val$message;
                }
                okhttp3.Response response = com.navdy.hud.app.framework.twilio.TwilioSmsManager.this.okHttpClient.newCall(new okhttp3.Request.Builder().url(com.navdy.hud.app.framework.twilio.TwilioSmsManager.SMS_ENDPOINT).header(com.amazonaws.http.HttpHeader.AUTHORIZATION, com.navdy.hud.app.framework.twilio.TwilioSmsManager.CREDENTIALS).post(new okhttp3.FormBody.Builder().add("To", com.navdy.hud.app.util.PhoneUtil.convertToE164Format(this.val$number)).add("MessagingServiceSid", com.navdy.hud.app.framework.twilio.TwilioSmsManager.MESSAGE_SERVICE_ID).add("Body", formattedMessage).build()).build()).execute();
                if (!response.isSuccessful()) {
                    com.navdy.hud.app.framework.twilio.TwilioSmsManager.sLogger.e("sendSms-end-err:" + response.code() + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA + response.message());
                    com.navdy.hud.app.framework.twilio.TwilioSmsManager.this.sendResult(com.navdy.hud.app.framework.twilio.TwilioSmsManager.ErrorCode.TWILIO_SERVER_ERROR, this.val$profileId, this.val$cb);
                    return;
                }
                com.navdy.hud.app.framework.twilio.TwilioSmsManager.sLogger.v("sendSms-end-suc:" + response.code());
                com.navdy.hud.app.framework.twilio.TwilioSmsManager.this.sendResult(com.navdy.hud.app.framework.twilio.TwilioSmsManager.ErrorCode.SUCCESS, this.val$profileId, this.val$cb);
            } catch (Throwable t) {
                com.navdy.hud.app.framework.twilio.TwilioSmsManager.sLogger.e("sendSms-end-err", t);
                if (t instanceof java.io.IOException) {
                    com.navdy.hud.app.framework.twilio.TwilioSmsManager.this.sendResult(com.navdy.hud.app.framework.twilio.TwilioSmsManager.ErrorCode.NETWORK_ERROR, this.val$profileId, this.val$cb);
                } else {
                    com.navdy.hud.app.framework.twilio.TwilioSmsManager.this.sendResult(com.navdy.hud.app.framework.twilio.TwilioSmsManager.ErrorCode.INTERNAL_ERROR, this.val$profileId, this.val$cb);
                }
            }
        }
    }

    public interface Callback {
        void result(com.navdy.hud.app.framework.twilio.TwilioSmsManager.ErrorCode errorCode);
    }

    public enum ErrorCode {
        SUCCESS,
        INTERNAL_ERROR,
        NETWORK_ERROR,
        TWILIO_SERVER_ERROR,
        INVALID_PARAMETER
    }

    static {
        android.content.Context context = com.navdy.hud.app.HudApplication.getAppContext();
        ACCOUNT_SID = com.navdy.service.library.util.CredentialUtil.getCredentials(context, "TWILIO_ACCOUNT_SID");
        AUTH_TOKEN = com.navdy.service.library.util.CredentialUtil.getCredentials(context, "TWILIO_AUTH_TOKEN");
        MESSAGE_SERVICE_ID = com.navdy.service.library.util.CredentialUtil.getCredentials(context, "TWILIO_MSG_SERVICE_ID");
    }

    public static com.navdy.hud.app.framework.twilio.TwilioSmsManager getInstance() {
        return instance;
    }

    private TwilioSmsManager() {
    }

    public com.navdy.hud.app.framework.twilio.TwilioSmsManager.ErrorCode sendSms(java.lang.String number, java.lang.String message, com.navdy.hud.app.framework.twilio.TwilioSmsManager.Callback cb) {
        if (number == null || message == null) {
            try {
                return com.navdy.hud.app.framework.twilio.TwilioSmsManager.ErrorCode.INVALID_PARAMETER;
            } catch (Throwable t) {
                sLogger.e("sendSms", t);
                return com.navdy.hud.app.framework.twilio.TwilioSmsManager.ErrorCode.INTERNAL_ERROR;
            }
        } else if (!com.navdy.service.library.util.SystemUtils.isConnectedToNetwork(com.navdy.hud.app.HudApplication.getAppContext())) {
            return com.navdy.hud.app.framework.twilio.TwilioSmsManager.ErrorCode.NETWORK_ERROR;
        } else {
            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.framework.twilio.TwilioSmsManager.Anon1(message, number, this.driverProfileManager.getCurrentProfile().getProfileName(), cb), 23);
            return com.navdy.hud.app.framework.twilio.TwilioSmsManager.ErrorCode.SUCCESS;
        }
    }

    /* access modifiers changed from: private */
    public void sendResult(com.navdy.hud.app.framework.twilio.TwilioSmsManager.ErrorCode errorCode, java.lang.String sendProfileId, com.navdy.hud.app.framework.twilio.TwilioSmsManager.Callback cb) {
        if (cb != null) {
            com.navdy.hud.app.profile.DriverProfile profile = this.driverProfileManager.getCurrentProfile();
            java.lang.String currentProfileName = profile.getProfileName();
            if (profile.isDefaultProfile() || android.text.TextUtils.equals(sendProfileId, currentProfileName)) {
                switch (errorCode) {
                    case SUCCESS:
                        com.navdy.hud.app.analytics.AnalyticsSupport.recordSmsSent(true, com.navdy.hud.app.analytics.AnalyticsSupport.ATTR_PLATFORM_IOS, com.navdy.hud.app.framework.glance.GlanceConstants.areMessageCanned());
                        break;
                    default:
                        com.navdy.hud.app.analytics.AnalyticsSupport.recordSmsSent(false, com.navdy.hud.app.analytics.AnalyticsSupport.ATTR_PLATFORM_IOS, com.navdy.hud.app.framework.glance.GlanceConstants.areMessageCanned());
                        break;
                }
                cb.result(errorCode);
                return;
            }
            sLogger.v("profile changed:" + sendProfileId + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA + currentProfileName);
        }
    }
}
