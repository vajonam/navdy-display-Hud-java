package com.navdy.hud.app.framework.trips;

public final class TripManager$$InjectAdapter extends dagger.internal.Binding<com.navdy.hud.app.framework.trips.TripManager> implements javax.inject.Provider<com.navdy.hud.app.framework.trips.TripManager> {
    private dagger.internal.Binding<com.squareup.otto.Bus> bus;
    private dagger.internal.Binding<android.content.SharedPreferences> preferences;

    public TripManager$$InjectAdapter() {
        super("com.navdy.hud.app.framework.trips.TripManager", "members/com.navdy.hud.app.framework.trips.TripManager", true, com.navdy.hud.app.framework.trips.TripManager.class);
    }

    public void attach(dagger.internal.Linker linker) {
        this.bus = linker.requestBinding("com.squareup.otto.Bus", com.navdy.hud.app.framework.trips.TripManager.class, getClass().getClassLoader());
        this.preferences = linker.requestBinding("android.content.SharedPreferences", com.navdy.hud.app.framework.trips.TripManager.class, getClass().getClassLoader());
    }

    public void getDependencies(java.util.Set<dagger.internal.Binding<?>> getBindings, java.util.Set<dagger.internal.Binding<?>> set) {
        getBindings.add(this.bus);
        getBindings.add(this.preferences);
    }

    public com.navdy.hud.app.framework.trips.TripManager get() {
        return new com.navdy.hud.app.framework.trips.TripManager((com.squareup.otto.Bus) this.bus.get(), (android.content.SharedPreferences) this.preferences.get());
    }
}
