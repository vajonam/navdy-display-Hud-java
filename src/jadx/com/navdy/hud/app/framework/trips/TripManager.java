package com.navdy.hud.app.framework.trips;

@javax.inject.Singleton
public class TripManager {
    public static final int MAX_SPEED_METERS_PER_SECOND_THRESHOLD = 200;
    private static final long MIN_TIME_THRESHOLD = 1000;
    public static final java.lang.String PREFERENCE_TOTAL_DISTANCE_TRAVELLED_WITH_NAVDY = "trip_manager_total_distance_travelled_with_navdy";
    public static final long TOTAL_DISTANCE_PERSIST_INTERVAL = 30000;
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.framework.trips.TripManager.class);
    /* access modifiers changed from: private */
    public final com.squareup.otto.Bus bus;
    /* access modifiers changed from: private */
    public int currentDistanceTraveledInMeters;
    /* access modifiers changed from: private */
    public int currentSequenceNumber;
    /* access modifiers changed from: private */
    public volatile com.navdy.hud.app.framework.trips.TripManager.State currentState;
    /* access modifiers changed from: private */
    public long currentTripNumber;
    private long currentTripStartTime;
    /* access modifiers changed from: private */
    public boolean isClientConnected;
    /* access modifiers changed from: private */
    public com.here.android.mpa.common.GeoCoordinate lastCoords;
    /* access modifiers changed from: private */
    public android.location.Location lastLocation;
    /* access modifiers changed from: private */
    public long lastTotalDistancePersistTime = 0;
    /* access modifiers changed from: private */
    public long lastTotalDistancePersistedValue = 0;
    /* access modifiers changed from: private */
    public long lastTrackingTime;
    /* access modifiers changed from: private */
    public com.navdy.service.library.events.TripUpdate.Builder lastTripUpdateBuilder;
    /* access modifiers changed from: private */
    public java.lang.StringBuilder logBuilder;
    android.content.SharedPreferences preferences;
    /* access modifiers changed from: private */
    public final com.navdy.hud.app.manager.SpeedManager speedManager;
    /* access modifiers changed from: private */
    public int totalDistanceTravelledInMeters;

    class Anon1 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.hud.app.framework.trips.TripManager.TrackingEvent val$event;

        Anon1(com.navdy.hud.app.framework.trips.TripManager.TrackingEvent trackingEvent) {
            this.val$event = trackingEvent;
        }

        public void run() {
            if (com.navdy.hud.app.framework.trips.TripManager.this.currentState == com.navdy.hud.app.framework.trips.TripManager.State.STOPPED) {
                com.navdy.hud.app.framework.trips.TripManager.this.createNewTrip();
                com.navdy.hud.app.framework.trips.TripManager.this.currentState = com.navdy.hud.app.framework.trips.TripManager.State.STARTED;
            }
            com.here.android.mpa.common.GeoCoordinate coords = this.val$event.geoPosition.getCoordinate();
            double latitude = coords.getLatitude();
            double longitude = coords.getLongitude();
            double altitude = coords.getAltitude();
            if (com.navdy.hud.app.maps.util.MapUtils.sanitizeCoords(coords) == null) {
                com.navdy.hud.app.framework.trips.TripManager.sLogger.v("filtering out bad coords: " + latitude + ", " + longitude + ", " + altitude);
                return;
            }
            if (com.navdy.hud.app.framework.trips.TripManager.this.lastCoords != null) {
                long currentTime = android.os.SystemClock.elapsedRealtime();
                long timeElapsed = currentTime - com.navdy.hud.app.framework.trips.TripManager.this.lastTrackingTime;
                if (timeElapsed >= 1000) {
                    com.navdy.hud.app.framework.trips.TripManager.this.lastTrackingTime = currentTime;
                    int dist = (int) this.val$event.geoPosition.getCoordinate().distanceTo(com.navdy.hud.app.framework.trips.TripManager.this.lastCoords);
                    if (((double) dist) / (((double) timeElapsed) / 1000.0d) <= 200.0d) {
                        com.navdy.hud.app.framework.trips.TripManager.this.currentDistanceTraveledInMeters = com.navdy.hud.app.framework.trips.TripManager.this.currentDistanceTraveledInMeters + dist;
                        com.navdy.hud.app.framework.trips.TripManager.this.totalDistanceTravelledInMeters = com.navdy.hud.app.framework.trips.TripManager.this.totalDistanceTravelledInMeters + dist;
                        if (com.navdy.hud.app.framework.trips.TripManager.this.lastTotalDistancePersistTime == 0 || currentTime - com.navdy.hud.app.framework.trips.TripManager.this.lastTotalDistancePersistTime > 30000) {
                            com.navdy.hud.app.framework.trips.TripManager.this.lastTotalDistancePersistTime = currentTime;
                            long newValue = com.navdy.hud.app.framework.trips.TripManager.this.getTotalDistanceTravelledWithNavdy();
                            com.navdy.hud.app.framework.trips.TripManager.this.preferences.edit().putLong(com.navdy.hud.app.framework.trips.TripManager.PREFERENCE_TOTAL_DISTANCE_TRAVELLED_WITH_NAVDY, newValue).apply();
                            com.navdy.hud.app.framework.trips.TripManager.this.lastTotalDistancePersistedValue = (long) com.navdy.hud.app.framework.trips.TripManager.this.totalDistanceTravelledInMeters;
                        }
                    } else {
                        com.navdy.hud.app.framework.trips.TripManager.sLogger.d("Unusual jump between co ordinates, Last lat :" + com.navdy.hud.app.framework.trips.TripManager.this.lastCoords.getLatitude() + ", " + "Lon :" + com.navdy.hud.app.framework.trips.TripManager.this.lastCoords.getLongitude() + ", " + "Current Lat:" + this.val$event.geoPosition.getCoordinate().getLatitude() + ", " + "Lon :" + this.val$event.geoPosition.getCoordinate().getLongitude());
                    }
                } else {
                    return;
                }
            }
            com.navdy.hud.app.framework.trips.TripManager.this.lastCoords = this.val$event.geoPosition.getCoordinate();
            if (com.navdy.hud.app.framework.trips.TripManager.this.isClientConnected) {
                com.navdy.hud.app.framework.trips.TripManager.this.currentSequenceNumber = com.navdy.hud.app.framework.trips.TripManager.this.currentSequenceNumber + 1;
                com.navdy.hud.app.analytics.TelemetrySession telemetrySession = com.navdy.hud.app.analytics.TelemetrySession.INSTANCE;
                com.navdy.service.library.events.TripUpdate.Builder updateBuilder = new com.navdy.service.library.events.TripUpdate.Builder().trip_number(java.lang.Long.valueOf(com.navdy.hud.app.framework.trips.TripManager.this.currentTripNumber)).sequence_number(java.lang.Integer.valueOf(com.navdy.hud.app.framework.trips.TripManager.this.currentSequenceNumber)).timestamp(java.lang.Long.valueOf(java.lang.System.currentTimeMillis())).distance_traveled(java.lang.Integer.valueOf(com.navdy.hud.app.framework.trips.TripManager.this.currentDistanceTraveledInMeters)).current_position(new com.navdy.service.library.events.location.LatLong(java.lang.Double.valueOf(latitude), java.lang.Double.valueOf(longitude))).elevation(java.lang.Double.valueOf(this.val$event.geoPosition.getCoordinate().getAltitude())).bearing(java.lang.Float.valueOf((float) this.val$event.geoPosition.getHeading())).gps_speed(java.lang.Float.valueOf((float) this.val$event.geoPosition.getSpeed())).obd_speed(java.lang.Integer.valueOf(com.navdy.hud.app.framework.trips.TripManager.this.speedManager.getObdSpeed())).hard_acceleration_count(java.lang.Integer.valueOf(telemetrySession.getSessionHardAccelerationCount())).hard_breaking_count(java.lang.Integer.valueOf(telemetrySession.getSessionHardBrakingCount())).high_g_count(java.lang.Integer.valueOf(telemetrySession.getSessionHighGCount())).speeding_ratio(java.lang.Double.valueOf((double) telemetrySession.getSessionSpeedingPercentage())).excessive_speeding_ratio(java.lang.Double.valueOf((double) telemetrySession.getSessionExcessiveSpeedingPercentage())).meters_traveled_since_boot(java.lang.Integer.valueOf((int) telemetrySession.getSessionDistance())).horizontal_accuracy(java.lang.Float.valueOf((this.val$event.geoPosition.getLatitudeAccuracy() + this.val$event.geoPosition.getLongitudeAccuracy()) / 2.0f)).elevation_accuracy(java.lang.Float.valueOf(this.val$event.geoPosition.getAltitudeAccuracy())).last_raw_coordinate(com.navdy.hud.app.framework.trips.TripManager.this.toCoordinate(com.navdy.hud.app.framework.trips.TripManager.this.lastLocation));
                if (com.navdy.hud.app.framework.trips.TripManager.sLogger.isLoggable(2)) {
                    com.navdy.hud.app.framework.trips.TripManager.this.logBuilder.setLength(0);
                    com.navdy.hud.app.framework.trips.TripManager.this.logBuilder.append("TripUpdate: tripNumber=" + com.navdy.hud.app.framework.trips.TripManager.this.currentTripNumber + "; sequenceNumber=" + com.navdy.hud.app.framework.trips.TripManager.this.currentSequenceNumber + "; timestamp=" + java.lang.System.currentTimeMillis() + "; distanceTraveled=" + com.navdy.hud.app.framework.trips.TripManager.this.currentDistanceTraveledInMeters + "; currentPosition=" + latitude + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA + longitude + "; elevation=" + this.val$event.geoPosition.getCoordinate().getAltitude() + "; bearing=" + this.val$event.geoPosition.getHeading() + "; gpsSpeed=" + this.val$event.geoPosition.getSpeed() + "; obdSpeed=" + com.navdy.hud.app.framework.trips.TripManager.this.speedManager.getObdSpeed());
                }
                if (this.val$event.isRouteTracking()) {
                    updateBuilder.chosen_destination_id(this.val$event.chosenDestinationId).estimated_time_remaining(java.lang.Integer.valueOf(this.val$event.estimatedTimeRemaining)).distance_to_destination(java.lang.Integer.valueOf(this.val$event.distanceToDestination));
                    if (com.navdy.hud.app.framework.trips.TripManager.sLogger.isLoggable(2)) {
                        com.navdy.hud.app.framework.trips.TripManager.this.logBuilder.append("; chosenDestinationId=" + this.val$event.chosenDestinationId + "; estimatedTimeRemaining=" + this.val$event.estimatedTimeRemaining + "; distanceToDestination=" + this.val$event.distanceToDestination);
                    }
                }
                if (com.navdy.hud.app.framework.trips.TripManager.sLogger.isLoggable(2)) {
                    com.navdy.hud.app.framework.trips.TripManager.sLogger.v(com.navdy.hud.app.framework.trips.TripManager.this.logBuilder.toString());
                }
                com.navdy.hud.app.framework.trips.TripManager.this.lastTripUpdateBuilder = updateBuilder;
                if (com.navdy.hud.app.framework.trips.TripManager.this.currentState != com.navdy.hud.app.framework.trips.TripManager.State.STOPPED) {
                    com.navdy.hud.app.framework.trips.TripManager.this.bus.post(new com.navdy.hud.app.event.RemoteEvent(com.navdy.hud.app.framework.trips.TripManager.this.lastTripUpdateBuilder.build()));
                }
            }
        }
    }

    class Anon2 implements java.lang.Runnable {
        Anon2() {
        }

        public void run() {
            com.navdy.hud.app.framework.trips.TripManager.this.currentState = com.navdy.hud.app.framework.trips.TripManager.State.STOPPED;
        }
    }

    class Anon3 implements java.lang.Runnable {
        Anon3() {
        }

        public void run() {
            if (com.navdy.hud.app.framework.trips.TripManager.this.lastTripUpdateBuilder != null && com.navdy.hud.app.framework.trips.TripManager.this.lastTripUpdateBuilder.chosen_destination_id != null) {
                com.navdy.hud.app.framework.trips.TripManager.this.lastTripUpdateBuilder.arrived_at_destination_id(com.navdy.hud.app.framework.trips.TripManager.this.lastTripUpdateBuilder.chosen_destination_id);
                if (com.navdy.hud.app.framework.trips.TripManager.this.currentState != com.navdy.hud.app.framework.trips.TripManager.State.STOPPED) {
                    com.navdy.hud.app.framework.trips.TripManager.this.bus.post(new com.navdy.hud.app.event.RemoteEvent(com.navdy.hud.app.framework.trips.TripManager.this.lastTripUpdateBuilder.build()));
                }
            }
        }
    }

    public static class FinishedTripRouteEvent {
    }

    public static class NewTripEvent {
    }

    public enum State {
        STARTED,
        STOPPED
    }

    public static class TrackingEvent {
        /* access modifiers changed from: private */
        public final java.lang.String chosenDestinationId;
        /* access modifiers changed from: private */
        public final int distanceToDestination;
        /* access modifiers changed from: private */
        public final int estimatedTimeRemaining;
        /* access modifiers changed from: private */
        public final com.here.android.mpa.common.GeoPosition geoPosition;

        public TrackingEvent(com.here.android.mpa.common.GeoPosition geoPosition2) {
            this.geoPosition = geoPosition2;
            this.chosenDestinationId = null;
            this.estimatedTimeRemaining = -1;
            this.distanceToDestination = -1;
        }

        public TrackingEvent(com.here.android.mpa.common.GeoPosition geoPosition2, java.lang.String chosenDestinationId2, int estimatedTimeRemaining2, int distanceToDestination2) {
            this.geoPosition = geoPosition2;
            this.chosenDestinationId = chosenDestinationId2;
            this.estimatedTimeRemaining = estimatedTimeRemaining2;
            this.distanceToDestination = distanceToDestination2;
        }

        public boolean isRouteTracking() {
            if (com.navdy.hud.app.framework.trips.TripManager.sLogger.isLoggable(2)) {
                com.navdy.hud.app.framework.trips.TripManager.sLogger.v("[routeTracking] chosenDestinationId=" + this.chosenDestinationId + "; estimatedTimeRemaining=" + this.estimatedTimeRemaining + "; distanceToDestination=" + this.distanceToDestination);
            }
            return this.chosenDestinationId != null && this.estimatedTimeRemaining >= 0 && this.distanceToDestination >= 0;
        }
    }

    @javax.inject.Inject
    public TripManager(com.squareup.otto.Bus bus2, android.content.SharedPreferences preferences2) {
        this.bus = bus2;
        this.speedManager = com.navdy.hud.app.manager.SpeedManager.getInstance();
        this.preferences = preferences2;
        this.currentState = com.navdy.hud.app.framework.trips.TripManager.State.STOPPED;
        this.logBuilder = new java.lang.StringBuilder();
        this.isClientConnected = false;
        this.totalDistanceTravelledInMeters = 0;
    }

    public int getCurrentDistanceTraveled() {
        return this.currentDistanceTraveledInMeters;
    }

    public long getCurrentTripStartTime() {
        return this.currentTripStartTime;
    }

    public int getTotalDistanceTravelled() {
        return this.totalDistanceTravelledInMeters;
    }

    @com.squareup.otto.Subscribe
    public void onLocation(android.location.Location location) {
        this.lastLocation = location;
    }

    @com.squareup.otto.Subscribe
    public void onTrack(com.navdy.hud.app.framework.trips.TripManager.TrackingEvent event) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.framework.trips.TripManager.Anon1(event), 8);
    }

    /* access modifiers changed from: private */
    public com.navdy.service.library.events.location.Coordinate toCoordinate(android.location.Location lastLocation2) {
        if (lastLocation2 == null) {
            return null;
        }
        return new com.navdy.service.library.events.location.Coordinate.Builder().accuracy(java.lang.Float.valueOf(lastLocation2.getAccuracy())).altitude(java.lang.Double.valueOf(lastLocation2.getAltitude())).bearing(java.lang.Float.valueOf(lastLocation2.getBearing())).latitude(java.lang.Double.valueOf(lastLocation2.getLatitude())).longitude(java.lang.Double.valueOf(lastLocation2.getLongitude())).provider(lastLocation2.getProvider()).speed(java.lang.Float.valueOf(lastLocation2.getSpeed())).timestamp(java.lang.Long.valueOf(lastLocation2.getTime())).build();
    }

    @com.squareup.otto.Subscribe
    public void onNewTrip(com.navdy.hud.app.framework.trips.TripManager.NewTripEvent event) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.framework.trips.TripManager.Anon2(), 8);
    }

    @com.squareup.otto.Subscribe
    public void onFinishedTripRouteEvent(com.navdy.hud.app.framework.trips.TripManager.FinishedTripRouteEvent event) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.framework.trips.TripManager.Anon3(), 8);
    }

    @com.squareup.otto.Subscribe
    public void onConnectionStateChange(com.navdy.service.library.events.connection.ConnectionStateChange event) {
        if (event.state == com.navdy.service.library.events.connection.ConnectionStateChange.ConnectionState.CONNECTION_DISCONNECTED) {
            this.isClientConnected = false;
        } else if (event.state == com.navdy.service.library.events.connection.ConnectionStateChange.ConnectionState.CONNECTION_VERIFIED) {
            this.isClientConnected = true;
        }
    }

    /* access modifiers changed from: private */
    public void createNewTrip() {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        this.currentTripNumber = java.lang.System.currentTimeMillis();
        this.currentSequenceNumber = 0;
        this.currentDistanceTraveledInMeters = 0;
        this.currentTripStartTime = java.lang.System.currentTimeMillis();
    }

    public long getTotalDistanceTravelledWithNavdy() {
        return this.preferences.getLong(PREFERENCE_TOTAL_DISTANCE_TRAVELLED_WITH_NAVDY, 0) + (((long) this.totalDistanceTravelledInMeters) - this.lastTotalDistancePersistedValue);
    }
}
