package com.navdy.hud.app.framework.connection;

public class ConnectionNotification {
    private static final int CONNECTED_TIMEOUT = 2000;
    public static final java.lang.String CONNECT_ID = "connection#toast";
    public static final java.lang.String DISCONNECT_ID = "disconnection#toast";
    private static final java.lang.String EMPTY = "";
    private static final int NOT_CONNECTED_TIMEOUT = 30000;
    private static final int TAG_CONNECT = 0;
    private static final int TAG_DISMISS = 1;
    private static final int TAG_DONT_LAUNCH = 3;
    private static final int TAG_LAUNCH_APP = 2;
    private static final java.util.ArrayList<com.navdy.hud.app.ui.component.ChoiceLayout.Choice> appClosedChoices = new java.util.ArrayList<>(2);
    private static final java.util.ArrayList<com.navdy.hud.app.ui.component.ChoiceLayout.Choice> appClosedLaunchFailedChoices = new java.util.ArrayList<>(1);
    private static final int appNotConnectedWidth;
    /* access modifiers changed from: private */
    public static final com.squareup.otto.Bus bus = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getBus();
    private static final java.lang.String connect;
    private static final java.lang.String connectedTitle;
    private static final int contentWidth;
    private static final java.util.ArrayList<com.navdy.hud.app.ui.component.ChoiceLayout.Choice> disconnectedChoices = new java.util.ArrayList<>(2);
    private static final java.lang.String disconnectedTitle;
    private static final java.lang.String dismiss;
    /* access modifiers changed from: private */
    public static android.os.Handler handler = new android.os.Handler(android.os.Looper.getMainLooper());
    private static final java.lang.String iphoneAppClosedLaunchFailedMessage;
    private static final java.lang.String iphoneAppClosedMessage1;
    private static final java.lang.String iphoneAppClosedMessage2;
    private static final java.lang.String iphoneAppClosedTitle;
    private static final java.lang.String launch;
    /* access modifiers changed from: private */
    public static com.squareup.picasso.Transformation roundTransformation = new com.makeramen.RoundedTransformationBuilder().oval(true).build();
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.framework.connection.ConnectionNotification.class);

    static class Anon1 implements com.navdy.hud.app.framework.toast.IToastCallback {
        com.navdy.hud.app.ui.component.image.InitialsImageView driverImage;
        com.navdy.hud.app.view.ToastView toastView;
        final /* synthetic */ java.io.File val$loadImagePath;
        final /* synthetic */ boolean val$loadImageRequired;
        final /* synthetic */ com.navdy.hud.app.profile.DriverProfile val$prevProfile;

        Anon1(boolean z, java.io.File file, com.navdy.hud.app.profile.DriverProfile driverProfile) {
            this.val$loadImageRequired = z;
            this.val$loadImagePath = file;
            this.val$prevProfile = driverProfile;
        }

        public void onStart(com.navdy.hud.app.view.ToastView view) {
            this.toastView = view;
            this.driverImage = view.getMainLayout().screenImage;
            this.driverImage.setTag(com.navdy.hud.app.framework.connection.ConnectionNotification.DISCONNECT_ID);
            if (this.val$loadImageRequired) {
                com.navdy.hud.app.framework.connection.ConnectionNotification.loadDriveImagefromProfile(this.driverImage, com.navdy.hud.app.framework.connection.ConnectionNotification.DISCONNECT_ID, this.val$loadImagePath, this.val$prevProfile);
            }
        }

        public void onStop() {
            this.toastView = null;
            this.driverImage.setTag(null);
            this.driverImage = null;
        }

        public boolean onKey(com.navdy.hud.app.manager.InputManager.CustomKeyEvent event) {
            return false;
        }

        public void executeChoiceItem(int pos, int id) {
            if (this.toastView != null) {
                switch (id) {
                    case 0:
                        this.toastView.dismissToast();
                        android.os.Bundle args = new android.os.Bundle();
                        args.putString(com.navdy.hud.app.screen.WelcomeScreen.ARG_ACTION, com.navdy.hud.app.screen.WelcomeScreen.ACTION_RECONNECT);
                        com.navdy.hud.app.framework.connection.ConnectionNotification.bus.post(new com.navdy.hud.app.event.ShowScreenWithArgs(com.navdy.service.library.events.ui.Screen.SCREEN_WELCOME, args, false));
                        return;
                    case 1:
                        this.toastView.dismissToast();
                        return;
                    case 2:
                        this.toastView.dismissToast();
                        com.navdy.hud.app.framework.connection.ConnectionNotification.bus.post(new com.navdy.hud.app.service.ConnectionHandler.ApplicationLaunchAttempted());
                        com.navdy.hud.app.framework.connection.ConnectionNotification.bus.post(new com.navdy.hud.app.event.RemoteEvent(new com.navdy.service.library.events.input.LaunchAppEvent((java.lang.String) null)));
                        return;
                    case 3:
                        com.navdy.hud.app.framework.connection.ConnectionNotification.bus.post(new com.navdy.hud.app.event.Disconnect());
                        this.toastView.dismissToast();
                        return;
                    default:
                        return;
                }
            }
        }
    }

    static class Anon2 implements com.navdy.hud.app.framework.toast.IToastCallback {
        com.navdy.hud.app.ui.component.image.InitialsImageView driverImage;
        final /* synthetic */ java.io.File val$imagePath;
        final /* synthetic */ com.navdy.hud.app.profile.DriverProfile val$profile;

        Anon2(java.io.File file, com.navdy.hud.app.profile.DriverProfile driverProfile) {
            this.val$imagePath = file;
            this.val$profile = driverProfile;
        }

        public void onStart(com.navdy.hud.app.view.ToastView view) {
            this.driverImage = view.getMainLayout().screenImage;
            this.driverImage.setTag("connection#toast");
            com.navdy.hud.app.framework.connection.ConnectionNotification.loadDriveImagefromProfile(this.driverImage, "connection#toast", this.val$imagePath, this.val$profile);
        }

        public void onStop() {
            this.driverImage.setTag(null);
            this.driverImage = null;
        }

        public boolean onKey(com.navdy.hud.app.manager.InputManager.CustomKeyEvent event) {
            return false;
        }

        public void executeChoiceItem(int pos, int id) {
        }
    }

    static class Anon3 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.hud.app.ui.component.image.InitialsImageView val$driverImage;
        final /* synthetic */ java.io.File val$imagePath;
        final /* synthetic */ com.navdy.hud.app.profile.DriverProfile val$profile;
        final /* synthetic */ java.lang.String val$tag;

        class Anon1 implements java.lang.Runnable {
            Anon1() {
            }

            public void run() {
                if (com.navdy.hud.app.framework.connection.ConnectionNotification.Anon3.this.val$driverImage.getTag() == com.navdy.hud.app.framework.connection.ConnectionNotification.Anon3.this.val$tag) {
                    com.navdy.hud.app.framework.connection.ConnectionNotification.Anon3.this.val$driverImage.setInitials(null, com.navdy.hud.app.ui.component.image.InitialsImageView.Style.DEFAULT);
                    com.navdy.hud.app.util.picasso.PicassoUtil.getInstance().load(com.navdy.hud.app.framework.connection.ConnectionNotification.Anon3.this.val$imagePath).fit().transform(com.navdy.hud.app.framework.connection.ConnectionNotification.roundTransformation).into((android.widget.ImageView) com.navdy.hud.app.framework.connection.ConnectionNotification.Anon3.this.val$driverImage);
                }
            }
        }

        Anon3(java.io.File file, com.navdy.hud.app.ui.component.image.InitialsImageView initialsImageView, java.lang.String str, com.navdy.hud.app.profile.DriverProfile driverProfile) {
            this.val$imagePath = file;
            this.val$driverImage = initialsImageView;
            this.val$tag = str;
            this.val$profile = driverProfile;
        }

        public void run() {
            if (this.val$imagePath.exists()) {
                com.navdy.hud.app.framework.connection.ConnectionNotification.handler.post(new com.navdy.hud.app.framework.connection.ConnectionNotification.Anon3.Anon1());
            } else if (this.val$profile != null) {
                java.io.File f = this.val$profile.getDriverImageFile();
                if (f != null && !f.exists()) {
                    com.navdy.hud.app.framework.connection.ConnectionNotification.sLogger.d("photo not available locally");
                    com.navdy.hud.app.framework.contacts.PhoneImageDownloader.getInstance().submitDownload(com.navdy.hud.app.profile.DriverProfile.DRIVER_PROFILE_IMAGE, com.navdy.hud.app.framework.contacts.PhoneImageDownloader.Priority.NORMAL, com.navdy.service.library.events.photo.PhotoType.PHOTO_DRIVER_PROFILE, null);
                }
            }
        }
    }

    static {
        android.content.res.Resources resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        connectedTitle = resources.getString(com.navdy.hud.app.R.string.phone_connected);
        disconnectedTitle = resources.getString(com.navdy.hud.app.R.string.phone_disconnected);
        iphoneAppClosedTitle = resources.getString(com.navdy.hud.app.R.string.app_disconnected);
        iphoneAppClosedMessage1 = resources.getString(com.navdy.hud.app.R.string.app_disconnected_title);
        iphoneAppClosedMessage2 = resources.getString(com.navdy.hud.app.R.string.app_disconnected_msg);
        iphoneAppClosedLaunchFailedMessage = resources.getString(com.navdy.hud.app.R.string.app_disconnected_msg_launch_failed);
        connect = resources.getString(com.navdy.hud.app.R.string.connect);
        dismiss = resources.getString(com.navdy.hud.app.R.string.dismiss);
        launch = resources.getString(com.navdy.hud.app.R.string.launch);
        disconnectedChoices.add(new com.navdy.hud.app.ui.component.ChoiceLayout.Choice(connect, 0));
        disconnectedChoices.add(new com.navdy.hud.app.ui.component.ChoiceLayout.Choice(dismiss, 1));
        appClosedChoices.add(new com.navdy.hud.app.ui.component.ChoiceLayout.Choice(launch, 2));
        appClosedChoices.add(new com.navdy.hud.app.ui.component.ChoiceLayout.Choice(dismiss, 3));
        appClosedLaunchFailedChoices.add(new com.navdy.hud.app.ui.component.ChoiceLayout.Choice(dismiss, 3));
        contentWidth = (int) resources.getDimension(com.navdy.hud.app.R.dimen.toast_phone_disconnected_width);
        appNotConnectedWidth = (int) resources.getDimension(com.navdy.hud.app.R.dimen.toast_app_disconnected_width);
    }

    public static void showDisconnectedToast(boolean showAppLaunch) {
        com.navdy.hud.app.manager.RemoteDeviceManager remoteDeviceManager = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance();
        com.navdy.hud.app.service.ConnectionHandler connectionHandler = remoteDeviceManager.getConnectionHandler();
        com.navdy.service.library.events.DeviceInfo deviceInfo = remoteDeviceManager.getRemoteDeviceInfo();
        com.navdy.service.library.device.NavdyDeviceId deviceId = null;
        android.os.Bundle bundle = new android.os.Bundle();
        boolean load = false;
        java.io.File path = null;
        bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_TIMEOUT, 30000);
        bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_SIDE_IMAGE, com.navdy.hud.app.R.drawable.icon_type_phone_disconnected);
        bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_STYLE, com.navdy.hud.app.R.style.ToastMainTitle);
        bundle.putBoolean(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_SUPPORTS_GESTURE, true);
        boolean setDisconnectedData = false;
        if (showAppLaunch || (deviceInfo != null && connectionHandler.isAppClosed())) {
            bundle.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE, iphoneAppClosedTitle);
            bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_IMAGE, com.navdy.hud.app.R.drawable.icon_navdyapp);
            bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_INFO_CONTAINER_MAX_WIDTH, appNotConnectedWidth);
            if (!connectionHandler.isAppLaunchAttempted()) {
                sLogger.d("App launch is not attempted yet so showing the app launch prompt");
                bundle.putParcelableArrayList(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_CHOICE_LIST, appClosedChoices);
                bundle.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_3, iphoneAppClosedMessage1);
                bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_3_STYLE, com.navdy.hud.app.R.style.Toast_3);
            } else {
                sLogger.d("App launch has already been attempted but app is not launched yet");
                bundle.putParcelableArrayList(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_CHOICE_LIST, appClosedLaunchFailedChoices);
                bundle.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_3, iphoneAppClosedLaunchFailedMessage);
                bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_3_STYLE, com.navdy.hud.app.R.style.Toast_3);
            }
        } else {
            com.navdy.service.library.events.DeviceInfo lastConnectedDeviceInfo = connectionHandler.getLastConnectedDeviceInfo();
            if (lastConnectedDeviceInfo == null) {
                sLogger.v("last connected device not found");
                com.navdy.service.library.device.connection.ConnectionInfo connectionInfo = com.navdy.service.library.device.RemoteDeviceRegistry.getInstance(com.navdy.hud.app.HudApplication.getAppContext()).getLastPairedDevice();
                if (connectionInfo == null) {
                    sLogger.v("no last paired device found");
                    bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_IMAGE, com.navdy.hud.app.R.drawable.icon_user_numberonly);
                } else {
                    deviceId = connectionInfo.getDeviceId();
                    sLogger.v("last paired device found:" + deviceId);
                }
            } else {
                sLogger.v("last connected device found");
                deviceId = new com.navdy.service.library.device.NavdyDeviceId(lastConnectedDeviceInfo.deviceId);
            }
            bundle.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE, disconnectedTitle);
            bundle.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_2, disconnectedTitle);
            bundle.putParcelableArrayList(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_CHOICE_LIST, disconnectedChoices);
            bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_INFO_CONTAINER_MAX_WIDTH, contentWidth);
            setDisconnectedData = true;
        }
        com.navdy.hud.app.profile.DriverProfile profile = null;
        if (deviceId != null) {
            profile = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getDriverProfileManager().getProfileForId(deviceId);
            java.lang.String deviceName = deviceId.getDeviceName();
            java.lang.String driverName = null;
            if (profile == null) {
                sLogger.v("driver profile not found:" + deviceId);
                bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_IMAGE, com.navdy.hud.app.R.drawable.icon_user_numberonly);
            } else {
                driverName = profile.getDriverName();
                sLogger.v("driver profile found:" + deviceId);
                java.io.File imagePath = profile.getDriverImageFile();
                boolean imageInCache = com.navdy.hud.app.util.picasso.PicassoUtil.isImageAvailableInCache(imagePath);
                java.lang.String name = getName(profile, deviceId);
                if (imageInCache) {
                    bundle.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_IMAGE_CACHE_KEY, imagePath.getAbsolutePath());
                } else {
                    int resId = com.navdy.hud.app.framework.contacts.ContactImageHelper.getInstance().getDriverImageResId(deviceName);
                    java.lang.String initials = com.navdy.hud.app.framework.contacts.ContactUtil.getInitials(name);
                    bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_IMAGE, resId);
                    bundle.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_IMAGE_INITIALS, initials);
                    load = true;
                    path = imagePath;
                }
            }
            if (setDisconnectedData) {
                if (!android.text.TextUtils.isEmpty(driverName)) {
                    bundle.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_2, driverName);
                    bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_2_STYLE, com.navdy.hud.app.R.style.Glances_1);
                    bundle.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_3, deviceName);
                    bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_3_STYLE, com.navdy.hud.app.R.style.Toast_1);
                } else {
                    bundle.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_2, deviceName);
                    bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_2_STYLE, com.navdy.hud.app.R.style.Glances_1);
                }
            }
        }
        com.navdy.hud.app.framework.toast.IToastCallback callback = new com.navdy.hud.app.framework.connection.ConnectionNotification.Anon1(load, path, profile);
        com.navdy.hud.app.framework.toast.ToastManager toastManager = com.navdy.hud.app.framework.toast.ToastManager.getInstance();
        toastManager.dismissCurrentToast(toastManager.getCurrentToastId());
        toastManager.clearAllPendingToast();
        toastManager.addToast(new com.navdy.hud.app.framework.toast.ToastManager.ToastParams(DISCONNECT_ID, bundle, callback, true, true));
    }

    public static void showConnectedToast() {
        com.navdy.hud.app.profile.DriverProfile profile = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile();
        com.navdy.service.library.device.NavdyDeviceId deviceId = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getDeviceId();
        if (deviceId != null) {
            java.io.File imagePath = profile.getDriverImageFile();
            boolean imageInCache = com.navdy.hud.app.util.picasso.PicassoUtil.isImageAvailableInCache(imagePath);
            java.lang.String name = getName(profile, deviceId);
            java.lang.String deviceName = deviceId.getDeviceName();
            android.os.Bundle bundle = new android.os.Bundle();
            bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_TIMEOUT, 2000);
            if (imageInCache) {
                bundle.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_IMAGE_CACHE_KEY, imagePath.getAbsolutePath());
            } else {
                int resId = com.navdy.hud.app.framework.contacts.ContactImageHelper.getInstance().getDriverImageResId(deviceId.getDeviceName());
                java.lang.String initials = com.navdy.hud.app.framework.contacts.ContactUtil.getInitials(name);
                bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_IMAGE, resId);
                bundle.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_IMAGE_INITIALS, initials);
            }
            bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_SIDE_IMAGE, com.navdy.hud.app.R.drawable.icon_alert_phone_connected);
            bundle.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE, connectedTitle);
            bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_STYLE, com.navdy.hud.app.R.style.ToastMainTitle);
            java.lang.String driverName = profile.getDriverName();
            if (!android.text.TextUtils.isEmpty(driverName)) {
                bundle.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_2, driverName);
                bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_2_STYLE, com.navdy.hud.app.R.style.Glances_1);
                bundle.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_3, deviceName);
                bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_3_STYLE, com.navdy.hud.app.R.style.Toast_1);
            } else {
                bundle.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_2, deviceName);
                bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_2_STYLE, com.navdy.hud.app.R.style.Glances_1);
            }
            bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_INFO_CONTAINER_MAX_WIDTH, contentWidth);
            com.navdy.hud.app.framework.toast.IToastCallback callback = null;
            if (!imageInCache) {
                callback = new com.navdy.hud.app.framework.connection.ConnectionNotification.Anon2(imagePath, profile);
            }
            com.navdy.hud.app.framework.toast.ToastManager toastManager = com.navdy.hud.app.framework.toast.ToastManager.getInstance();
            toastManager.dismissCurrentToast(DISCONNECT_ID);
            toastManager.clearPendingToast(DISCONNECT_ID);
            toastManager.addToast(new com.navdy.hud.app.framework.toast.ToastManager.ToastParams("connection#toast", bundle, callback, true, true));
        }
    }

    private static java.lang.String getName(com.navdy.hud.app.profile.DriverProfile profile, com.navdy.service.library.device.NavdyDeviceId deviceId) {
        java.lang.String n = profile.getDriverName();
        if (android.text.TextUtils.isEmpty(n)) {
            n = deviceId.getDeviceName();
        }
        if (n == null) {
            return "";
        }
        return n;
    }

    /* access modifiers changed from: private */
    public static void loadDriveImagefromProfile(com.navdy.hud.app.ui.component.image.InitialsImageView driverImage, java.lang.String tag, java.io.File imagePath, com.navdy.hud.app.profile.DriverProfile profile) {
        android.graphics.Bitmap bitmap = com.navdy.hud.app.util.picasso.PicassoUtil.getBitmapfromCache(imagePath);
        if (bitmap != null) {
            driverImage.setInitials(null, com.navdy.hud.app.ui.component.image.InitialsImageView.Style.DEFAULT);
            driverImage.setImageBitmap(bitmap);
            return;
        }
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.framework.connection.ConnectionNotification.Anon3(imagePath, driverImage, tag, profile), 1);
    }
}
