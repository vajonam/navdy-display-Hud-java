package com.navdy.hud.app.framework.glympse;

public final class GlympseManager {
    private static final java.lang.String GLYMPSE_API_KEY = com.navdy.service.library.util.CredentialUtil.getCredentials(com.navdy.hud.app.HudApplication.getAppContext(), "GLYMPSE_API_KEY");
    private static final java.lang.String GLYMPSE_BASE_URL = "api.glympse.com";
    public static final int GLYMPSE_DURATION = ((int) java.util.concurrent.TimeUnit.HOURS.toMillis(1));
    public static final java.lang.String GLYMPSE_TYPE_SHARE_LOCATION = "Location";
    public static final java.lang.String GLYMPSE_TYPE_SHARE_TRIP = "Trip";
    private static final java.lang.String NAVDY_CONTACT_UUID = "navdy_contact_uuid";
    private static final long NAVDY_GLYMPSE_PARTNER_ID = 1;
    /* access modifiers changed from: private */
    public static final int UPDATE_ETA_INTERVAL = ((int) java.util.concurrent.TimeUnit.MINUTES.toMillis(1));
    /* access modifiers changed from: private */
    public static final android.os.Handler handler = new android.os.Handler(android.os.Looper.getMainLooper());
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.framework.glympse.GlympseManager.class);
    private static final java.lang.String[] messages;
    private java.lang.String currentGlympseUserId;
    /* access modifiers changed from: private */
    public com.glympse.android.api.GGlympse glympse;
    private boolean glympseInitFailed;
    private boolean isTrackingETA;
    private final com.navdy.hud.app.framework.notifications.NotificationManager notificationManager;
    /* access modifiers changed from: private */
    public final java.util.Map<java.lang.String, com.navdy.hud.app.framework.contacts.Contact> ticketsAwaitingReadReceipt;
    private java.lang.Runnable updateETA;

    class Anon1 implements java.lang.Runnable {

        /* renamed from: com.navdy.hud.app.framework.glympse.GlympseManager$Anon1$Anon1 reason: collision with other inner class name */
        class C0010Anon1 implements java.lang.Runnable {

            /* renamed from: com.navdy.hud.app.framework.glympse.GlympseManager$Anon1$Anon1$Anon1 reason: collision with other inner class name */
            class C0011Anon1 implements java.lang.Runnable {
                final /* synthetic */ java.util.Date val$etaDate;
                final /* synthetic */ com.glympse.android.api.GTrack val$track;

                C0011Anon1(java.util.Date date, com.glympse.android.api.GTrack gTrack) {
                    this.val$etaDate = date;
                    this.val$track = gTrack;
                }

                public void run() {
                    long now = java.lang.System.currentTimeMillis();
                    long durationInMillis = this.val$etaDate.getTime() - now;
                    if (durationInMillis < 0) {
                        durationInMillis = 0;
                    }
                    long minutes = durationInMillis / 60000;
                    com.glympse.android.core.GArray<com.glympse.android.api.GTicket> activeTickets = com.navdy.hud.app.framework.glympse.GlympseManager.this.glympse.getHistoryManager().getTickets();
                    int ticketListLength = activeTickets.length();
                    for (int i = 0; i < ticketListLength; i++) {
                        com.glympse.android.api.GTicket ticket = (com.glympse.android.api.GTicket) activeTickets.at(i);
                        if (!ticket.isActive()) {
                            com.navdy.hud.app.framework.glympse.GlympseManager.logger.v("update ETA ticket not active:" + ticket.getId());
                        } else if (ticket.getDestination() == null) {
                            com.navdy.hud.app.framework.glympse.GlympseManager.logger.v("update ETA ticket not for trip:" + ticket.getId());
                        } else {
                            com.navdy.hud.app.framework.glympse.GlympseManager.logger.v("update ETA for ticket with id[" + ticket.getId() + "] durationMillis[" + durationInMillis + "] ETA[" + this.val$etaDate + "] currentTime=" + now + " minutes[" + minutes + "] expireTime[" + new java.util.Date(ticket.getExpireTime()) + "]");
                            ticket.updateEta(durationInMillis);
                            ticket.updateRoute(this.val$track);
                        }
                    }
                }
            }

            C0010Anon1() {
            }

            public void run() {
                if (com.navdy.hud.app.maps.here.HereMapsManager.getInstance().isInitialized()) {
                    com.navdy.hud.app.maps.here.HereNavigationManager hereNavigationManager = com.navdy.hud.app.maps.here.HereNavigationManager.getInstance();
                    com.navdy.hud.app.maps.here.HereNavController navController = hereNavigationManager.getNavController();
                    if (navController.getState() == com.navdy.hud.app.maps.here.HereNavController.State.NAVIGATING) {
                        java.util.Date etaDate = navController.getEta(true, com.here.android.mpa.routing.Route.TrafficPenaltyMode.OPTIMAL);
                        if (com.navdy.hud.app.maps.here.HereMapUtil.isValidEtaDate(etaDate)) {
                            long l1 = android.os.SystemClock.elapsedRealtime();
                            com.glympse.android.api.GTrackBuilder trackBuilder = com.glympse.android.api.GlympseFactory.createTrackBuilder();
                            trackBuilder.setSource(0);
                            trackBuilder.setDistance((int) navController.getDestinationDistance());
                            com.here.android.mpa.routing.Route route = hereNavigationManager.getCurrentRoute();
                            java.lang.StringBuilder builder = new java.lang.StringBuilder();
                            if (route != null) {
                                java.util.List<com.here.android.mpa.routing.Maneuver> maneuverList = route.getManeuvers();
                                if (maneuverList != null) {
                                    for (com.here.android.mpa.routing.Maneuver maneuver : maneuverList) {
                                        com.here.android.mpa.common.GeoCoordinate coordinate = maneuver.getCoordinate();
                                        if (coordinate != null) {
                                            double lat = coordinate.getLatitude();
                                            double lng = coordinate.getLongitude();
                                            trackBuilder.addLocation(com.glympse.android.core.CoreFactory.createLocation(lat, lng));
                                            builder.append("{" + lat + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA + lng + "} ");
                                        }
                                    }
                                }
                            }
                            com.glympse.android.api.GTrack track = trackBuilder.getTrack();
                            com.navdy.hud.app.framework.glympse.GlympseManager.logger.v("update ETA track = " + builder.toString());
                            com.navdy.hud.app.framework.glympse.GlympseManager.logger.v("update ETA time to generate track:" + (android.os.SystemClock.elapsedRealtime() - l1));
                            android.os.Handler access$Anon400 = com.navdy.hud.app.framework.glympse.GlympseManager.handler;
                            com.navdy.hud.app.framework.glympse.GlympseManager.Anon1.C0010Anon1.C0011Anon1 anon1 = new com.navdy.hud.app.framework.glympse.GlympseManager.Anon1.C0010Anon1.C0011Anon1(etaDate, track);
                            access$Anon400.post(anon1);
                        }
                    }
                }
            }
        }

        Anon1() {
        }

        public void run() {
            if (!com.navdy.hud.app.framework.glympse.GlympseManager.getInstance().isSynced()) {
                com.navdy.hud.app.framework.glympse.GlympseManager.logger.v("update ETA: glympse not synched");
                return;
            }
            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.framework.glympse.GlympseManager.Anon1.C0010Anon1(), 1);
            com.navdy.hud.app.framework.glympse.GlympseManager.handler.postDelayed(this, (long) com.navdy.hud.app.framework.glympse.GlympseManager.UPDATE_ETA_INTERVAL);
        }
    }

    class Anon2 implements com.glympse.android.api.GEventListener {
        Anon2() {
        }

        public void eventsOccurred(com.glympse.android.api.GGlympse gGlympse, int listener, int events, java.lang.Object obj) {
            if (com.navdy.hud.app.framework.glympse.GlympseManager.this.hasGlympseEvent(events, 128)) {
                com.navdy.hud.app.framework.glympse.GlympseManager.logger.v("synced with server, setting user profile");
                com.navdy.hud.app.framework.glympse.GlympseManager.this.setUserProfile();
            }
            if (com.navdy.hud.app.framework.glympse.GlympseManager.this.hasGlympseEvent(events, 131072)) {
                com.navdy.hud.app.framework.glympse.GlympseManager.this.addTicketListener((com.glympse.android.api.GTicket) obj);
            }
        }
    }

    class Anon3 implements com.glympse.android.api.GEventListener {
        final /* synthetic */ com.glympse.android.api.GTicket val$ticket;

        Anon3(com.glympse.android.api.GTicket gTicket) {
            this.val$ticket = gTicket;
        }

        public void eventsOccurred(com.glympse.android.api.GGlympse gGlympse, int listener, int events, java.lang.Object obj) {
            com.glympse.android.api.GTicket updatedTicket = (com.glympse.android.api.GTicket) obj;
            if (com.navdy.hud.app.framework.glympse.GlympseManager.this.hasGlympseEvent(events, 8192)) {
                java.lang.String ticketId = updatedTicket.getId();
                java.lang.String contactUuid = updatedTicket.getProperty(1, com.navdy.hud.app.framework.glympse.GlympseManager.NAVDY_CONTACT_UUID).getString();
                com.navdy.hud.app.framework.glympse.GlympseManager.logger.v("ticket added, ticket id is " + ticketId + "; contact uuid is " + contactUuid);
                if (!android.text.TextUtils.isEmpty(contactUuid) && com.navdy.hud.app.framework.glympse.GlympseManager.this.ticketsAwaitingReadReceipt.containsKey(contactUuid)) {
                    com.navdy.hud.app.framework.glympse.GlympseManager.this.addGlympseSentGlance((com.navdy.hud.app.framework.contacts.Contact) com.navdy.hud.app.framework.glympse.GlympseManager.this.ticketsAwaitingReadReceipt.get(contactUuid));
                }
            }
            if (com.navdy.hud.app.framework.glympse.GlympseManager.this.hasGlympseEvent(events, 16384)) {
                java.lang.String ticketId2 = updatedTicket.getId();
                java.lang.String contactUuid2 = updatedTicket.getProperty(1, com.navdy.hud.app.framework.glympse.GlympseManager.NAVDY_CONTACT_UUID).getString();
                com.navdy.hud.app.framework.glympse.GlympseManager.logger.v("ticket updated, ticket id is " + ticketId2 + "; contact uuid is " + contactUuid2);
                if (com.navdy.hud.app.framework.glympse.GlympseManager.this.ticketsAwaitingReadReceipt.containsKey(contactUuid2)) {
                    com.navdy.hud.app.framework.contacts.Contact contact = (com.navdy.hud.app.framework.contacts.Contact) com.navdy.hud.app.framework.glympse.GlympseManager.this.ticketsAwaitingReadReceipt.remove(contactUuid2);
                    com.navdy.hud.app.framework.glympse.GlympseManager.this.addGlympseReadGlance(contact);
                    this.val$ticket.removeListener(this);
                    if (contactUuid2 == null) {
                        contactUuid2 = "";
                    }
                    com.navdy.hud.app.analytics.AnalyticsSupport.recordGlympseViewed(contactUuid2);
                    com.navdy.hud.app.framework.glympse.GlympseManager.logger.v("Glympse viewed ticket id is " + ticketId2 + " contact[" + contact.name + "] number[" + contact.number + "] e164[" + com.navdy.hud.app.util.PhoneUtil.convertToE164Format(contact.number) + "]");
                }
            }
        }
    }

    public enum Error {
        NONE,
        INTERNAL_ERROR,
        NO_INTERNET
    }

    private static class InternalSingleton {
        /* access modifiers changed from: private */
        public static final com.navdy.hud.app.framework.glympse.GlympseManager singleton = new com.navdy.hud.app.framework.glympse.GlympseManager(null);

        private InternalSingleton() {
        }
    }

    /* synthetic */ GlympseManager(com.navdy.hud.app.framework.glympse.GlympseManager.Anon1 x0) {
        this();
    }

    static {
        android.content.res.Resources resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        messages = new java.lang.String[]{resources.getString(com.navdy.hud.app.R.string.send_without_message), resources.getString(com.navdy.hud.app.R.string.on_my_way), resources.getString(com.navdy.hud.app.R.string.i_am_driving), resources.getString(com.navdy.hud.app.R.string.running_late), resources.getString(com.navdy.hud.app.R.string.i_am_here)};
    }

    public static com.navdy.hud.app.framework.glympse.GlympseManager getInstance() {
        return com.navdy.hud.app.framework.glympse.GlympseManager.InternalSingleton.singleton;
    }

    private GlympseManager() {
        this.updateETA = new com.navdy.hud.app.framework.glympse.GlympseManager.Anon1();
        this.notificationManager = com.navdy.hud.app.framework.notifications.NotificationManager.getInstance();
        this.ticketsAwaitingReadReceipt = new java.util.HashMap();
        try {
            logger.v("creating glympse");
            this.glympse = com.glympse.android.api.GlympseFactory.createGlympse(com.navdy.hud.app.HudApplication.getAppContext(), GLYMPSE_BASE_URL, GLYMPSE_API_KEY);
            setUpEventListener();
            this.glympse.setEtaMode(1);
            this.glympse.start();
            logger.v("calling glympse start duration=" + (((long) com.navdy.hud.app.maps.MapSettings.getGlympseDuration()) / java.util.concurrent.TimeUnit.MINUTES.toMillis(1)) + " minutes");
        } catch (Throwable t) {
            logger.e(t);
            this.glympseInitFailed = true;
        }
        com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getBus().register(this);
    }

    public com.navdy.hud.app.framework.glympse.GlympseManager.Error addMessage(com.navdy.hud.app.framework.contacts.Contact contact, java.lang.String message, @android.support.annotation.Nullable java.lang.String destinationLabel, double latitude, double longitude, java.lang.StringBuilder id) {
        if (this.glympseInitFailed) {
            logger.e("glympse init failed, cannot add message, no-op");
            return com.navdy.hud.app.framework.glympse.GlympseManager.Error.INTERNAL_ERROR;
        } else if (!com.navdy.hud.app.framework.network.NetworkStateManager.isConnectedToNetwork(com.navdy.hud.app.HudApplication.getAppContext())) {
            logger.v("addMessage, no connection, no-op");
            return com.navdy.hud.app.framework.glympse.GlympseManager.Error.NO_INTERNET;
        } else {
            com.glympse.android.api.GPlace place = null;
            if (!android.text.TextUtils.isEmpty(destinationLabel) && !(latitude == 0.0d && longitude == 0.0d)) {
                place = com.glympse.android.api.GlympseFactory.createPlace(latitude, longitude, com.glympse.android.core.CoreFactory.createString(destinationLabel));
                startTrackingETA();
            } else {
                stopTrackingETA();
            }
            com.glympse.android.api.GTicket ticket = com.glympse.android.api.GlympseFactory.createTicket(com.navdy.hud.app.maps.MapSettings.getGlympseDuration(), message, place);
            java.lang.String e164Number = com.navdy.hud.app.util.PhoneUtil.convertToE164Format(contact.number);
            ticket.addInvite(com.glympse.android.api.GlympseFactory.createInvite(3, contact.name, e164Number));
            java.lang.String uuid = java.util.UUID.randomUUID().toString();
            id.setLength(0);
            id.append(uuid);
            this.ticketsAwaitingReadReceipt.put(uuid, contact);
            ticket.appendData(1, NAVDY_CONTACT_UUID, com.glympse.android.core.CoreFactory.createPrimitive(uuid));
            this.glympse.sendTicket(ticket);
            logger.v("addMessage, added ticket name[" + contact.name + "] e164Number[" + e164Number + "] number[" + contact.number + "] uuid[" + uuid + "]");
            return com.navdy.hud.app.framework.glympse.GlympseManager.Error.NONE;
        }
    }

    public boolean isSynced() {
        return !this.glympseInitFailed && this.glympse.getHistoryManager().isSynced();
    }

    @com.squareup.otto.Subscribe
    public void onDriverProfileChanged(com.navdy.hud.app.event.DriverProfileChanged event) {
        if (isSynced()) {
            setUserProfile();
        }
    }

    /* access modifiers changed from: private */
    public void setUserProfile() {
        com.navdy.hud.app.profile.DriverProfile driverProfile = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile();
        if (android.text.TextUtils.equals(this.currentGlympseUserId, driverProfile.getProfileName())) {
            logger.v("called setUserProfile with same profile as current, no-op");
            return;
        }
        com.glympse.android.api.GUser user = this.glympse.getUserManager().getSelf();
        if (!driverProfile.isDefaultProfile()) {
            deleteActiveTickets();
            this.ticketsAwaitingReadReceipt.clear();
            logger.v("setting new user profile for Glympse: " + driverProfile.getProfileName());
            if (!android.text.TextUtils.isEmpty(driverProfile.getFirstName())) {
                user.setNickname(driverProfile.getFirstName());
            }
            if (driverProfile.getDriverImage() != null) {
                logger.v("setting photo");
                user.setAvatar(com.glympse.android.core.CoreFactory.createDrawable(driverProfile.getDriverImage()));
            } else {
                logger.v("not setting photo");
                user.setAvatar(null);
            }
            this.currentGlympseUserId = driverProfile.getProfileName();
            return;
        }
        logger.v("driverProfile is default, not setting it for Glympse");
    }

    private void startTrackingETA() {
        if (!this.isTrackingETA) {
            this.isTrackingETA = true;
            handler.post(this.updateETA);
        }
    }

    private void stopTrackingETA() {
        handler.removeCallbacks(this.updateETA);
        this.isTrackingETA = false;
    }

    private void deleteActiveTickets() {
        logger.v("deleteActiveTickets");
        stopTrackingETA();
        com.glympse.android.core.GArray<com.glympse.android.api.GTicket> activeTickets = this.glympse.getHistoryManager().getTickets();
        int ticketListLength = activeTickets.length();
        for (int i = 0; i < ticketListLength; i++) {
            com.glympse.android.api.GTicket ticket = (com.glympse.android.api.GTicket) activeTickets.at(i);
            logger.v("deleteActiveTickets deleting ticket with id " + ticket.getId());
            ticket.deleteTicket();
        }
    }

    public void expireActiveTickets() {
        if (isSynced()) {
            logger.v("expireActiveTickets");
            stopTrackingETA();
            com.glympse.android.core.GArray<com.glympse.android.api.GTicket> activeTickets = this.glympse.getHistoryManager().getTickets();
            int ticketListLength = activeTickets.length();
            for (int i = 0; i < ticketListLength; i++) {
                com.glympse.android.api.GTicket ticket = (com.glympse.android.api.GTicket) activeTickets.at(i);
                if (!ticket.isActive()) {
                    logger.v("expireActiveTickets ticket[" + ticket.getId() + "] not active");
                    return;
                }
                logger.v("expireActiveTickets ticket[ " + ticket.getId() + "] expiryTime[" + new java.util.Date(ticket.getExpireTime()) + "]");
                ticket.expire();
            }
        }
    }

    /* access modifiers changed from: private */
    public boolean hasGlympseEvent(int receivedEvents, int glympseEvent) {
        return (receivedEvents & glympseEvent) != 0;
    }

    private void setUpEventListener() {
        this.glympse.addListener(new com.navdy.hud.app.framework.glympse.GlympseManager.Anon2());
    }

    /* access modifiers changed from: private */
    public void addTicketListener(com.glympse.android.api.GTicket ticket) {
        ticket.addListener(new com.navdy.hud.app.framework.glympse.GlympseManager.Anon3(ticket));
    }

    /* access modifiers changed from: private */
    public void addGlympseSentGlance(com.navdy.hud.app.framework.contacts.Contact contact) {
        this.notificationManager.addNotification(new com.navdy.hud.app.framework.glympse.GlympseNotification(contact, com.navdy.hud.app.framework.glympse.GlympseNotification.Type.SENT));
    }

    /* access modifiers changed from: private */
    public void addGlympseReadGlance(com.navdy.hud.app.framework.contacts.Contact contact) {
        this.notificationManager.addNotification(new com.navdy.hud.app.framework.glympse.GlympseNotification(contact, com.navdy.hud.app.framework.glympse.GlympseNotification.Type.READ));
    }

    public java.lang.String[] getMessages() {
        return messages;
    }
}
