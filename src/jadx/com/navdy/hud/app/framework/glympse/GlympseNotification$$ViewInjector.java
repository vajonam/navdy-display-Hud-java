package com.navdy.hud.app.framework.glympse;

public class GlympseNotification$$ViewInjector {
    public static void inject(butterknife.ButterKnife.Finder finder, com.navdy.hud.app.framework.glympse.GlympseNotification target, java.lang.Object source) {
        target.title = (android.widget.TextView) finder.findRequiredView(source, com.navdy.hud.app.R.id.title, "field 'title'");
        target.subtitle = (android.widget.TextView) finder.findRequiredView(source, com.navdy.hud.app.R.id.subtitle, "field 'subtitle'");
        target.choiceLayout = (com.navdy.hud.app.ui.component.ChoiceLayout2) finder.findRequiredView(source, com.navdy.hud.app.R.id.choice_layout, "field 'choiceLayout'");
        target.notificationIcon = (com.navdy.hud.app.ui.component.image.IconColorImageView) finder.findRequiredView(source, com.navdy.hud.app.R.id.notification_icon, "field 'notificationIcon'");
        target.notificationUserImage = (android.widget.ImageView) finder.findRequiredView(source, com.navdy.hud.app.R.id.notification_user_image, "field 'notificationUserImage'");
        target.badge = (android.widget.ImageView) finder.findRequiredView(source, com.navdy.hud.app.R.id.badge, "field 'badge'");
        target.badgeIcon = (com.navdy.hud.app.ui.component.image.IconColorImageView) finder.findRequiredView(source, com.navdy.hud.app.R.id.badge_icon, "field 'badgeIcon'");
    }

    public static void reset(com.navdy.hud.app.framework.glympse.GlympseNotification target) {
        target.title = null;
        target.subtitle = null;
        target.choiceLayout = null;
        target.notificationIcon = null;
        target.notificationUserImage = null;
        target.badge = null;
        target.badgeIcon = null;
    }
}
