package com.navdy.hud.app.framework.glympse;

public class GlympseNotification implements com.navdy.hud.app.framework.notifications.INotification, com.navdy.hud.app.ui.component.ChoiceLayout2.IListener {
    private static final float NOTIFICATION_ALERT_BADGE_SCALE = 0.95f;
    private static final float NOTIFICATION_BADGE_SCALE = 0.5f;
    private static final float NOTIFICATION_DEFAULT_ICON_SCALE = 1.0f;
    private static final float NOTIFICATION_ICON_SCALE = 1.38f;
    private static final int NOTIFICATION_TIMEOUT = 5000;
    private static final int alertColor = resources.getColor(com.navdy.hud.app.R.color.share_location_trip_alert_color);
    private static final java.util.List<com.navdy.hud.app.ui.component.ChoiceLayout2.Choice> choicesOnlyDismiss = new java.util.ArrayList();
    private static final java.util.List<com.navdy.hud.app.ui.component.ChoiceLayout2.Choice> choicesRetryAndDismiss = new java.util.ArrayList();
    private static final android.os.Handler handler = new android.os.Handler(android.os.Looper.getMainLooper());
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.framework.glympse.GlympseNotification.class);
    private static final android.content.res.Resources resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
    private static final int shareLocationTripColor = resources.getColor(com.navdy.hud.app.R.color.share_location_trip_color);
    @butterknife.InjectView(2131624277)
    android.widget.ImageView badge;
    @butterknife.InjectView(2131624278)
    com.navdy.hud.app.ui.component.image.IconColorImageView badgeIcon;
    @butterknife.InjectView(2131624279)
    com.navdy.hud.app.ui.component.ChoiceLayout2 choiceLayout;
    /* access modifiers changed from: private */
    public final com.navdy.hud.app.framework.contacts.Contact contact;
    private com.navdy.hud.app.framework.notifications.INotificationController controller;
    /* access modifiers changed from: private */
    public final java.lang.String destinationLabel;
    /* access modifiers changed from: private */
    public final double latitude;
    /* access modifiers changed from: private */
    public final double longitude;
    /* access modifiers changed from: private */
    public final java.lang.String message;
    private final java.lang.String notifId;
    @butterknife.InjectView(2131624274)
    com.navdy.hud.app.ui.component.image.IconColorImageView notificationIcon;
    @butterknife.InjectView(2131624275)
    android.widget.ImageView notificationUserImage;
    private boolean retrySendingMessage;
    @butterknife.InjectView(2131624272)
    android.widget.TextView subtitle;
    @butterknife.InjectView(2131624135)
    android.widget.TextView title;
    private final com.navdy.hud.app.framework.glympse.GlympseNotification.Type type;
    private final java.lang.String uuid;

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            boolean hasFailed;
            boolean z;
            boolean z2 = true;
            com.navdy.hud.app.framework.glympse.GlympseNotification.logger.v("Glympse:retry sending message");
            java.lang.StringBuilder builder = new java.lang.StringBuilder();
            if (com.navdy.hud.app.framework.glympse.GlympseManager.getInstance().addMessage(com.navdy.hud.app.framework.glympse.GlympseNotification.this.contact, com.navdy.hud.app.framework.glympse.GlympseNotification.this.message, com.navdy.hud.app.framework.glympse.GlympseNotification.this.destinationLabel, com.navdy.hud.app.framework.glympse.GlympseNotification.this.latitude, com.navdy.hud.app.framework.glympse.GlympseNotification.this.longitude, builder) != com.navdy.hud.app.framework.glympse.GlympseManager.Error.NONE) {
                hasFailed = true;
            } else {
                hasFailed = false;
            }
            java.lang.String shareType = com.navdy.hud.app.framework.glympse.GlympseNotification.this.destinationLabel == null ? "Location" : com.navdy.hud.app.framework.glympse.GlympseManager.GLYMPSE_TYPE_SHARE_TRIP;
            if (hasFailed) {
                com.navdy.hud.app.framework.glympse.GlympseNotification.addGlympseOfflineGlance(com.navdy.hud.app.framework.glympse.GlympseNotification.this.message, com.navdy.hud.app.framework.glympse.GlympseNotification.this.contact, com.navdy.hud.app.framework.glympse.GlympseNotification.this.destinationLabel, com.navdy.hud.app.framework.glympse.GlympseNotification.this.latitude, com.navdy.hud.app.framework.glympse.GlympseNotification.this.longitude);
            }
            com.navdy.service.library.log.Logger access$Anon000 = com.navdy.hud.app.framework.glympse.GlympseNotification.logger;
            java.lang.StringBuilder append = new java.lang.StringBuilder().append("Glympse:retry sucess:");
            if (!hasFailed) {
                z = true;
            } else {
                z = false;
            }
            access$Anon000.v(append.append(z).toString());
            if (hasFailed) {
                z2 = false;
            }
            com.navdy.hud.app.analytics.AnalyticsSupport.recordGlympseSent(z2, shareType, com.navdy.hud.app.framework.glympse.GlympseNotification.this.message, builder.toString());
        }
    }

    public enum Type {
        SENT,
        READ,
        OFFLINE
    }

    static {
        int dismissColor = resources.getColor(com.navdy.hud.app.R.color.glance_dismiss);
        com.navdy.hud.app.ui.component.ChoiceLayout2.Choice dismissChoice = new com.navdy.hud.app.ui.component.ChoiceLayout2.Choice(com.navdy.hud.app.R.id.dismiss, com.navdy.hud.app.R.drawable.icon_glances_dismiss, dismissColor, com.navdy.hud.app.R.drawable.icon_glances_dismiss, -16777216, resources.getString(com.navdy.hud.app.R.string.dismiss), dismissColor);
        choicesOnlyDismiss.add(dismissChoice);
        int retryColor = resources.getColor(com.navdy.hud.app.R.color.glance_ok_blue);
        choicesRetryAndDismiss.add(new com.navdy.hud.app.ui.component.ChoiceLayout2.Choice(com.navdy.hud.app.R.id.retry, com.navdy.hud.app.R.drawable.icon_glances_retry, retryColor, com.navdy.hud.app.R.drawable.icon_glances_retry, -16777216, resources.getString(com.navdy.hud.app.R.string.retry), retryColor));
        choicesRetryAndDismiss.add(dismissChoice);
    }

    GlympseNotification(com.navdy.hud.app.framework.contacts.Contact contact2, com.navdy.hud.app.framework.glympse.GlympseNotification.Type type2) {
        this(contact2, type2, null, null, 0.0d, 0.0d);
    }

    public GlympseNotification(com.navdy.hud.app.framework.contacts.Contact contact2, com.navdy.hud.app.framework.glympse.GlympseNotification.Type type2, java.lang.String message2, java.lang.String destinationLabel2, double latitude2, double longitude2) {
        this.contact = contact2;
        this.type = type2;
        this.message = message2;
        this.destinationLabel = destinationLabel2;
        this.latitude = latitude2;
        this.longitude = longitude2;
        this.uuid = java.util.UUID.randomUUID().toString();
        this.notifId = "navdy#glympse#notif#" + type2.name() + "#" + this.uuid;
    }

    public void onClick() {
    }

    public void onTrackHand(float normalized) {
    }

    public com.navdy.hud.app.framework.notifications.NotificationType getType() {
        return com.navdy.hud.app.framework.notifications.NotificationType.GLYMPSE;
    }

    public java.lang.String getId() {
        return this.notifId;
    }

    public android.view.View getView(android.content.Context context) {
        android.view.View view = android.view.LayoutInflater.from(context).inflate(com.navdy.hud.app.R.layout.notification_glympse, null);
        butterknife.ButterKnife.inject((java.lang.Object) this, view);
        switch (this.type) {
            case SENT:
                setSentUI();
                break;
            case READ:
                setReadUI();
                break;
            case OFFLINE:
                setOfflineUI();
                break;
        }
        return view;
    }

    private void setSentUI() {
        if (!com.navdy.hud.app.maps.here.HereMapsManager.getInstance().isInitialized() || !com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().isNavigationModeOn()) {
            this.title.setText(resources.getString(com.navdy.hud.app.R.string.location_sent));
        } else {
            this.title.setText(resources.getString(com.navdy.hud.app.R.string.trip_sent));
        }
        this.subtitle.setText(resources.getString(com.navdy.hud.app.R.string.to_contact_name, new java.lang.Object[]{getContactName()}));
        this.notificationIcon.setIcon(com.navdy.hud.app.R.drawable.icon_message, shareLocationTripColor, null, NOTIFICATION_ICON_SCALE);
        this.notificationIcon.setVisibility(0);
        this.badge.setImageResource(com.navdy.hud.app.R.drawable.icon_msg_success);
        this.badge.setVisibility(0);
        this.choiceLayout.setChoices(choicesOnlyDismiss, 0, this);
    }

    private void setReadUI() {
        this.title.setText(getContactName());
        if (!com.navdy.hud.app.maps.here.HereMapsManager.getInstance().isInitialized() || !com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().isNavigationModeOn()) {
            this.subtitle.setText(resources.getString(com.navdy.hud.app.R.string.viewed_your_location));
        } else {
            this.subtitle.setText(resources.getString(com.navdy.hud.app.R.string.viewed_your_trip));
        }
        android.graphics.Bitmap bitmap = com.navdy.hud.app.util.picasso.PicassoUtil.getBitmapfromCache(com.navdy.hud.app.framework.contacts.PhoneImageDownloader.getInstance().getImagePath(this.contact.number, com.navdy.service.library.events.photo.PhotoType.PHOTO_CONTACT));
        if (bitmap != null) {
            this.notificationUserImage.setImageBitmap(bitmap);
            this.notificationUserImage.setVisibility(0);
        } else {
            com.navdy.hud.app.framework.contacts.ContactImageHelper contactImageHelper = com.navdy.hud.app.framework.contacts.ContactImageHelper.getInstance();
            this.notificationIcon.setIcon(contactImageHelper.getResourceId(this.contact.defaultImageIndex), contactImageHelper.getResourceColor(this.contact.defaultImageIndex), null, 1.0f);
            this.notificationIcon.setVisibility(0);
        }
        this.badgeIcon.setIcon(com.navdy.hud.app.R.drawable.icon_message, shareLocationTripColor, null, 0.5f);
        this.badgeIcon.setVisibility(0);
        this.choiceLayout.setChoices(choicesOnlyDismiss, 0, this);
    }

    private void setOfflineUI() {
        this.title.setText(resources.getString(com.navdy.hud.app.R.string.sending_failed));
        this.subtitle.setText(resources.getString(com.navdy.hud.app.R.string.offline));
        this.notificationIcon.setIcon(com.navdy.hud.app.R.drawable.icon_message, shareLocationTripColor, null, NOTIFICATION_ICON_SCALE);
        this.notificationIcon.setVisibility(0);
        this.badgeIcon.setIcon(com.navdy.hud.app.R.drawable.icon_msg_alert, alertColor, null, NOTIFICATION_ALERT_BADGE_SCALE);
        this.badgeIcon.setVisibility(0);
        this.choiceLayout.setChoices(choicesRetryAndDismiss, 0, this);
    }

    public android.view.View getExpandedView(android.content.Context context, java.lang.Object data) {
        return null;
    }

    public int getExpandedViewIndicatorColor() {
        return 0;
    }

    public void onStart(com.navdy.hud.app.framework.notifications.INotificationController controller2) {
        this.controller = controller2;
    }

    public void onUpdate() {
    }

    public void onStop() {
        this.controller = null;
        if (this.retrySendingMessage) {
            handler.post(new com.navdy.hud.app.framework.glympse.GlympseNotification.Anon1());
        }
    }

    public int getTimeout() {
        if (this.type == com.navdy.hud.app.framework.glympse.GlympseNotification.Type.OFFLINE) {
            return 0;
        }
        return 5000;
    }

    public boolean isAlive() {
        return false;
    }

    public boolean isPurgeable() {
        return false;
    }

    public boolean canAddToStackIfCurrentExists() {
        return true;
    }

    public int getColor() {
        return 0;
    }

    public void onNotificationEvent(com.navdy.hud.app.ui.framework.UIStateManager.Mode mode) {
    }

    public void onExpandedNotificationEvent(com.navdy.hud.app.ui.framework.UIStateManager.Mode mode) {
    }

    public void onExpandedNotificationSwitched() {
    }

    public android.animation.AnimatorSet getViewSwitchAnimation(boolean viewIn) {
        return null;
    }

    public boolean expandNotification() {
        return false;
    }

    public boolean supportScroll() {
        return false;
    }

    public void executeItem(com.navdy.hud.app.ui.component.ChoiceLayout2.Selection selection) {
        if (selection.id == com.navdy.hud.app.R.id.retry) {
            this.retrySendingMessage = true;
            com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().removeNotification(this.notifId);
        } else if (selection.id == com.navdy.hud.app.R.id.dismiss) {
            com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().removeNotification(this.notifId);
        }
    }

    public void itemSelected(com.navdy.hud.app.ui.component.ChoiceLayout2.Selection selection) {
    }

    public boolean onGesture(com.navdy.service.library.events.input.GestureEvent event) {
        return false;
    }

    public boolean onKey(com.navdy.hud.app.manager.InputManager.CustomKeyEvent event) {
        if (this.controller == null) {
            return false;
        }
        switch (event) {
            case LEFT:
                this.choiceLayout.moveSelectionLeft();
                if (this.type != com.navdy.hud.app.framework.glympse.GlympseNotification.Type.OFFLINE) {
                    this.controller.resetTimeout();
                }
                return true;
            case RIGHT:
                this.choiceLayout.moveSelectionRight();
                if (this.type != com.navdy.hud.app.framework.glympse.GlympseNotification.Type.OFFLINE) {
                    this.controller.resetTimeout();
                }
                return true;
            case SELECT:
                this.choiceLayout.executeSelectedItem();
                return true;
            default:
                return false;
        }
    }

    public com.navdy.hud.app.manager.InputManager.IInputHandler nextHandler() {
        return null;
    }

    /* access modifiers changed from: private */
    public static void addGlympseOfflineGlance(java.lang.String message2, com.navdy.hud.app.framework.contacts.Contact contact2, java.lang.String destinationLabel2, double latitude2, double longitude2) {
        com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().addNotification(new com.navdy.hud.app.framework.glympse.GlympseNotification(contact2, com.navdy.hud.app.framework.glympse.GlympseNotification.Type.OFFLINE, message2, destinationLabel2, latitude2, longitude2));
    }

    private java.lang.String getContactName() {
        if (!android.text.TextUtils.isEmpty(this.contact.name)) {
            return this.contact.name;
        }
        if (!android.text.TextUtils.isEmpty(this.contact.formattedNumber)) {
            return this.contact.formattedNumber;
        }
        return "";
    }
}
