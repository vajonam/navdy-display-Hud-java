package com.navdy.hud.app.framework.places;

public class NearbyPlaceSearchNotification implements com.navdy.hud.app.framework.notifications.INotification, com.navdy.hud.app.ui.component.ChoiceLayout2.IListener {
    private static final float ICON_SCALE = 1.38f;
    private static final int N_OFFLINE_RESULTS = 7;
    private static final long TIMEOUT = 10000;
    private static final java.util.Map<com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification.PlaceTypeSearchState, java.util.List<com.navdy.hud.app.ui.component.ChoiceLayout2.Choice>> choicesMapping = new java.util.HashMap(4);
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification.class);
    private static final java.lang.String searchAgain;
    private static final int searchColor;
    private static final int secondaryColor;
    private final com.squareup.otto.Bus bus = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getBus();
    @butterknife.InjectView(2131624150)
    com.navdy.hud.app.ui.component.ChoiceLayout2 choiceLayout;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.framework.notifications.INotificationController controller;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification.PlaceTypeSearchState currentState;
    /* access modifiers changed from: private */
    public final android.os.Handler handler = new android.os.Handler(android.os.Looper.getMainLooper());
    @butterknife.InjectView(2131624290)
    com.navdy.hud.app.ui.component.image.IconColorImageView iconColorView;
    @butterknife.InjectView(2131624295)
    com.navdy.hud.app.ui.component.image.IconColorImageView iconSide;
    /* access modifiers changed from: private */
    public android.animation.ObjectAnimator loadingAnimator;
    private final com.navdy.hud.app.framework.notifications.NotificationManager notificationManager = com.navdy.hud.app.framework.notifications.NotificationManager.getInstance();
    /* access modifiers changed from: private */
    public final com.navdy.service.library.events.places.PlaceType placeType;
    private final java.lang.String requestId = java.util.UUID.randomUUID().toString();
    @butterknife.InjectView(2131624291)
    android.widget.TextView resultsCount;
    @butterknife.InjectView(2131624292)
    android.widget.TextView resultsLabel;
    private java.util.List<com.navdy.service.library.events.destination.Destination> returnedDestinations;
    private final com.navdy.hud.app.manager.SpeedManager speedManager = com.navdy.hud.app.manager.SpeedManager.getInstance();
    @butterknife.InjectView(2131624294)
    android.widget.ImageView statusBadge;
    @butterknife.InjectView(2131624213)
    android.widget.TextView subTitle;
    /* access modifiers changed from: private */
    public final java.lang.Runnable timeoutForFailure = new com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification.Anon1();
    @butterknife.InjectView(2131624135)
    android.widget.TextView title;

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            if (com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification.this.controller != null) {
                com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification.this.stopLoadingAnimation();
                com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification.this.goToFailedState();
            }
        }
    }

    class Anon2 implements com.navdy.hud.app.ui.component.destination.IDestinationPicker {
        private boolean itemSelected = false;
        private boolean retrySelected = false;

        Anon2() {
        }

        public boolean onItemClicked(int id, int pos, com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.DestinationPickerState state) {
            com.navdy.hud.app.analytics.AnalyticsSupport.recordNearbySearchSelection(com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification.this.placeType, pos);
            this.itemSelected = true;
            if (id != com.navdy.hud.app.R.id.search_again) {
                return false;
            }
            com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification.logger.v("search again");
            this.retrySelected = true;
            return true;
        }

        public boolean onItemSelected(int id, int pos, com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.DestinationPickerState state) {
            return false;
        }

        public void onDestinationPickerClosed() {
            if (!this.itemSelected && !this.retrySelected) {
                com.navdy.hud.app.analytics.AnalyticsSupport.recordNearbySearchResultsClose();
            }
            if (this.retrySelected) {
                com.navdy.hud.app.framework.notifications.NotificationManager notificationManager = com.navdy.hud.app.framework.notifications.NotificationManager.getInstance();
                com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification notif = (com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification) notificationManager.getNotification(com.navdy.hud.app.framework.notifications.NotificationId.PLACE_TYPE_SEARCH_NOTIFICATION_ID);
                if (notif == null) {
                    notif = new com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification(com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification.this.placeType);
                }
                com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification.logger.v("launching notif search again:" + com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification.this.placeType);
                notificationManager.addNotification(notif);
            }
        }
    }

    class Anon3 implements com.navdy.hud.app.maps.here.HerePlacesManager.OnCategoriesSearchListener {

        class Anon1 implements java.lang.Runnable {
            final /* synthetic */ java.util.List val$destinations;

            Anon1(java.util.List list) {
                this.val$destinations = list;
            }

            public void run() {
                if (com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification.this.controller != null) {
                    com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification.this.handler.removeCallbacks(com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification.this.timeoutForFailure);
                    com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification.this.stopLoadingAnimation();
                    if (this.val$destinations.size() > 0) {
                        com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification.this.goToSuccessfulState(this.val$destinations);
                    } else {
                        com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification.this.goToZeroResultsState();
                    }
                }
            }
        }

        class Anon2 implements java.lang.Runnable {
            Anon2() {
            }

            public void run() {
                if (com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification.this.controller != null) {
                    com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification.this.handler.removeCallbacks(com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification.this.timeoutForFailure);
                    com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification.this.stopLoadingAnimation();
                    com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification.this.goToFailedState();
                }
            }
        }

        Anon3() {
        }

        public void onCompleted(java.util.List<com.here.android.mpa.search.Place> places) {
            com.navdy.service.library.events.location.LatLong navigationCoords;
            java.lang.String subtitle;
            if (com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification.this.controller != null) {
                if (com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification.this.currentState == com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification.PlaceTypeSearchState.ERROR) {
                    com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification.logger.w("received response after place type search has already failed, no-op");
                    return;
                }
                com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification.logger.v("performing offline search, returned places: ");
                for (com.here.android.mpa.search.Place place : places) {
                    com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification.logger.v(place.getName());
                }
                java.util.List<com.navdy.service.library.events.destination.Destination> destinations = new java.util.ArrayList<>();
                for (com.here.android.mpa.search.Place place2 : places) {
                    com.here.android.mpa.common.GeoCoordinate coords = place2.getLocation().getCoordinate();
                    com.navdy.service.library.events.location.LatLong displayCoords = new com.navdy.service.library.events.location.LatLong(java.lang.Double.valueOf(coords.getLatitude()), java.lang.Double.valueOf(coords.getLongitude()));
                    java.util.List<com.here.android.mpa.search.NavigationPosition> accessPoints = place2.getLocation().getAccessPoints();
                    if (accessPoints.size() > 0) {
                        com.here.android.mpa.common.GeoCoordinate accessPointCoords = ((com.here.android.mpa.search.NavigationPosition) accessPoints.get(0)).getCoordinate();
                        navigationCoords = new com.navdy.service.library.events.location.LatLong(java.lang.Double.valueOf(accessPointCoords.getLatitude()), java.lang.Double.valueOf(accessPointCoords.getLongitude()));
                    } else {
                        navigationCoords = displayCoords;
                    }
                    com.here.android.mpa.search.Address placeAddress = place2.getLocation().getAddress();
                    if (placeAddress.getHouseNumber() == null || placeAddress.getStreet() == null) {
                        subtitle = placeAddress.toString();
                    } else {
                        subtitle = placeAddress.getHouseNumber() + " " + placeAddress.getStreet();
                    }
                    destinations.add(new com.navdy.service.library.events.destination.Destination.Builder().navigation_position(navigationCoords).display_position(displayCoords).full_address(place2.getLocation().getAddress().toString()).destination_title(place2.getName()).destination_subtitle(subtitle).favorite_type(com.navdy.service.library.events.destination.Destination.FavoriteType.FAVORITE_NONE).identifier(java.util.UUID.randomUUID().toString()).suggestion_type(com.navdy.service.library.events.destination.Destination.SuggestionType.SUGGESTION_NONE).is_recommendation(java.lang.Boolean.valueOf(false)).last_navigated_to(java.lang.Long.valueOf(0)).place_type(com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification.this.placeType).build());
                }
                com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification.this.handler.post(new com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification.Anon3.Anon1(destinations));
            }
        }

        public void onError(com.navdy.hud.app.maps.here.HerePlacesManager.Error error) {
            if (com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification.this.controller != null) {
                com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification.logger.w("error while performing offline search: " + error.name());
                com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification.this.handler.post(new com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification.Anon3.Anon2());
            }
        }
    }

    class Anon4 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
        Anon4() {
        }

        public void onAnimationEnd(android.animation.Animator animation) {
            if (com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification.this.loadingAnimator != null) {
                com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification.this.loadingAnimator.setStartDelay(33);
                com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification.this.loadingAnimator.start();
                return;
            }
            com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification.logger.v("abandon loading animation");
        }
    }

    private enum PlaceTypeSearchState {
        SEARCHING,
        ERROR,
        NO_RESULTS,
        SEARCH_COMPLETE
    }

    static {
        android.content.res.Resources resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        java.lang.String dismiss = resources.getString(com.navdy.hud.app.R.string.dismiss);
        int dismissColor = resources.getColor(com.navdy.hud.app.R.color.glance_dismiss);
        int retryColor = resources.getColor(com.navdy.hud.app.R.color.glance_ok_blue);
        com.navdy.hud.app.ui.component.ChoiceLayout2.Choice dismissChoice = new com.navdy.hud.app.ui.component.ChoiceLayout2.Choice(com.navdy.hud.app.R.id.dismiss, com.navdy.hud.app.R.drawable.icon_glances_dismiss, dismissColor, com.navdy.hud.app.R.drawable.icon_glances_dismiss, -16777216, dismiss, dismissColor);
        com.navdy.hud.app.ui.component.ChoiceLayout2.Choice retryChoice = new com.navdy.hud.app.ui.component.ChoiceLayout2.Choice(com.navdy.hud.app.R.id.retry, com.navdy.hud.app.R.drawable.icon_glances_retry, retryColor, com.navdy.hud.app.R.drawable.icon_glances_retry, -16777216, resources.getString(com.navdy.hud.app.R.string.retry), retryColor);
        java.util.ArrayList arrayList = new java.util.ArrayList();
        java.util.ArrayList arrayList2 = arrayList;
        arrayList2.add(new com.navdy.hud.app.ui.component.ChoiceLayout2.Choice(com.navdy.hud.app.R.id.dismiss, com.navdy.hud.app.R.drawable.icon_glances_dismiss, dismissColor, com.navdy.hud.app.R.drawable.icon_glances_dismiss, -16777216, resources.getString(com.navdy.hud.app.R.string.cancel), dismissColor));
        java.util.ArrayList arrayList3 = new java.util.ArrayList();
        arrayList3.add(retryChoice);
        arrayList3.add(dismissChoice);
        java.util.ArrayList arrayList4 = new java.util.ArrayList();
        arrayList4.add(retryChoice);
        arrayList4.add(dismissChoice);
        int readColor = resources.getColor(com.navdy.hud.app.R.color.glance_ok_blue);
        java.util.ArrayList arrayList5 = new java.util.ArrayList();
        arrayList5.add(new com.navdy.hud.app.ui.component.ChoiceLayout2.Choice(com.navdy.hud.app.R.id.dismiss, com.navdy.hud.app.R.drawable.icon_glances_read, readColor, com.navdy.hud.app.R.drawable.icon_glances_read, -16777216, resources.getString(com.navdy.hud.app.R.string.view), readColor));
        arrayList5.add(dismissChoice);
        secondaryColor = resources.getColor(com.navdy.hud.app.R.color.place_type_search_secondary_color);
        choicesMapping.put(com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification.PlaceTypeSearchState.SEARCHING, arrayList);
        choicesMapping.put(com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification.PlaceTypeSearchState.ERROR, arrayList3);
        choicesMapping.put(com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification.PlaceTypeSearchState.NO_RESULTS, arrayList4);
        choicesMapping.put(com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification.PlaceTypeSearchState.SEARCH_COMPLETE, arrayList5);
        searchColor = resources.getColor(com.navdy.hud.app.R.color.mm_search);
        searchAgain = resources.getString(com.navdy.hud.app.R.string.search_again);
    }

    public NearbyPlaceSearchNotification(com.navdy.service.library.events.places.PlaceType placeType2) {
        this.placeType = placeType2;
    }

    public com.navdy.hud.app.framework.notifications.NotificationType getType() {
        return com.navdy.hud.app.framework.notifications.NotificationType.PLACE_TYPE_SEARCH;
    }

    public java.lang.String getId() {
        return com.navdy.hud.app.framework.notifications.NotificationId.PLACE_TYPE_SEARCH_NOTIFICATION_ID;
    }

    public android.view.View getView(android.content.Context context) {
        android.view.View view = android.view.LayoutInflater.from(context).inflate(com.navdy.hud.app.R.layout.notification_place_type_search, null);
        butterknife.ButterKnife.inject((java.lang.Object) this, view);
        com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.PlaceTypeResourceHolder placeTypeResourceHolder = com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.getPlaceTypeHolder(this.placeType);
        this.title.setText(context.getString(placeTypeResourceHolder.titleRes));
        this.iconColorView.setIcon(placeTypeResourceHolder.iconRes, context.getResources().getColor(placeTypeResourceHolder.colorRes), null, ICON_SCALE);
        this.choiceLayout.setVisibility(0);
        this.choiceLayout.setChoices((java.util.List) choicesMapping.get(com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification.PlaceTypeSearchState.SEARCHING), 0, this);
        return view;
    }

    public android.view.View getExpandedView(android.content.Context context, java.lang.Object data) {
        return null;
    }

    public int getExpandedViewIndicatorColor() {
        return 0;
    }

    public void onStart(com.navdy.hud.app.framework.notifications.INotificationController controller2) {
        this.controller = controller2;
        this.bus.register(this);
        startLoadingAnimation();
        sendRequest();
    }

    public void onUpdate() {
    }

    public void onStop() {
        this.controller = null;
        this.bus.unregister(this);
    }

    public int getTimeout() {
        return 0;
    }

    public boolean isAlive() {
        return false;
    }

    public boolean isPurgeable() {
        return false;
    }

    public boolean canAddToStackIfCurrentExists() {
        return true;
    }

    public int getColor() {
        return 0;
    }

    public void onNotificationEvent(com.navdy.hud.app.ui.framework.UIStateManager.Mode mode) {
    }

    public void onExpandedNotificationEvent(com.navdy.hud.app.ui.framework.UIStateManager.Mode mode) {
    }

    public void onExpandedNotificationSwitched() {
    }

    public android.animation.AnimatorSet getViewSwitchAnimation(boolean viewIn) {
        return null;
    }

    public boolean expandNotification() {
        return false;
    }

    public boolean supportScroll() {
        return false;
    }

    public void onClick() {
    }

    public void onTrackHand(float normalized) {
    }

    public boolean onGesture(com.navdy.service.library.events.input.GestureEvent event) {
        if (this.controller == null || this.currentState != com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification.PlaceTypeSearchState.SEARCH_COMPLETE) {
            return false;
        }
        switch (event.gesture) {
            case GESTURE_SWIPE_LEFT:
                logger.v("show route picker:gesture");
                launchPicker();
                return true;
            default:
                return false;
        }
    }

    public boolean onKey(com.navdy.hud.app.manager.InputManager.CustomKeyEvent event) {
        if (this.controller == null) {
            return false;
        }
        switch (event) {
            case LEFT:
                this.choiceLayout.moveSelectionLeft();
                return true;
            case RIGHT:
                this.choiceLayout.moveSelectionRight();
                return true;
            case SELECT:
                this.choiceLayout.executeSelectedItem();
                return true;
            default:
                return false;
        }
    }

    public com.navdy.hud.app.manager.InputManager.IInputHandler nextHandler() {
        return null;
    }

    private void recordSelection(int pos, int id) {
        switch (this.currentState) {
            case SEARCHING:
                if (id == com.navdy.hud.app.R.id.dismiss) {
                    com.navdy.hud.app.analytics.AnalyticsSupport.recordNearbySearchDismiss();
                    return;
                }
                return;
            case NO_RESULTS:
                com.navdy.hud.app.analytics.AnalyticsSupport.recordNearbySearchNoResult(id == com.navdy.hud.app.R.id.retry);
                return;
            case SEARCH_COMPLETE:
                if (id != com.navdy.hud.app.R.id.dismiss) {
                    return;
                }
                if (pos == 0) {
                    com.navdy.hud.app.analytics.AnalyticsSupport.recordNearbySearchResultsView();
                    return;
                } else {
                    com.navdy.hud.app.analytics.AnalyticsSupport.recordNearbySearchResultsDismiss();
                    return;
                }
            default:
                return;
        }
    }

    public void executeItem(com.navdy.hud.app.ui.component.ChoiceLayout2.Selection selection) {
        recordSelection(selection.pos, selection.id);
        switch (selection.id) {
            case com.navdy.hud.app.R.id.dismiss /*2131623947*/:
                if (this.currentState == com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification.PlaceTypeSearchState.SEARCH_COMPLETE) {
                    if (selection.pos == 0) {
                        launchPicker();
                        return;
                    } else {
                        dismissNotification();
                        return;
                    }
                } else {
                    dismissNotification();
                    return;
                }
            case com.navdy.hud.app.R.id.retry /*2131624048*/:
                startLoadingAnimation();
                sendRequest();
                return;
            default:
                return;
        }
    }

    public void itemSelected(com.navdy.hud.app.ui.component.ChoiceLayout2.Selection selection) {
    }

    @com.squareup.otto.Subscribe
    public void onPlaceTypeSearchResponse(com.navdy.service.library.events.places.PlaceTypeSearchResponse response) {
        logger.v("PlaceTypeSearchResponse:" + response.request_status);
        this.handler.removeCallbacks(this.timeoutForFailure);
        stopLoadingAnimation();
        if (!android.text.TextUtils.equals(response.request_id, this.requestId)) {
            logger.w("received wrong request_id on the PlaceTypeSearchResponse, no-op");
        } else if (this.currentState == com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification.PlaceTypeSearchState.ERROR) {
            logger.w("received response after place type search has already failed, no-op");
        } else {
            switch (response.request_status) {
                case REQUEST_SERVICE_ERROR:
                    goToFailedState();
                    return;
                case REQUEST_NOT_AVAILABLE:
                    goToZeroResultsState();
                    return;
                case REQUEST_SUCCESS:
                    goToSuccessfulState(response.destinations);
                    return;
                default:
                    return;
            }
        }
    }

    private void launchPicker() {
        logger.v("launch picker screen");
        android.content.Context context = com.navdy.hud.app.HudApplication.getAppContext();
        android.content.res.Resources resources = context.getResources();
        android.os.Bundle args = new android.os.Bundle();
        args.putBoolean(com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.PICKER_SHOW_DESTINATION_MAP, true);
        int icon = -1;
        com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.PlaceTypeResourceHolder holder = com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.getPlaceTypeHolder(this.placeType);
        if (holder != null) {
            icon = holder.destinationIcon;
        }
        args.putInt(com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.PICKER_DESTINATION_ICON, icon);
        args.putString(com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.PICKER_LEFT_TITLE, resources.getString(com.navdy.hud.app.R.string.quick_search));
        args.putInt(com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.PICKER_LEFT_ICON, com.navdy.hud.app.R.drawable.icon_mm_search_2);
        args.putInt(com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.PICKER_LEFT_ICON_BKCOLOR, android.support.v4.content.ContextCompat.getColor(context, com.navdy.hud.app.R.color.mm_search));
        args.putInt(com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.PICKER_INITIAL_SELECTION, 1);
        int selectedColor = android.support.v4.content.ContextCompat.getColor(context, holder.colorRes);
        int unselectedColor = android.support.v4.content.ContextCompat.getColor(context, com.navdy.hud.app.R.color.icon_bk_color_unselected);
        com.navdy.hud.app.ui.component.destination.DestinationParcelable searchAgainItem = new com.navdy.hud.app.ui.component.destination.DestinationParcelable(com.navdy.hud.app.R.id.search_again, context.getString(com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.getPlaceTypeHolder(this.placeType).titleRes), searchAgain, false, null, true, null, 0.0d, 0.0d, 0.0d, 0.0d, com.navdy.hud.app.R.drawable.icon_mm_search_2, 0, searchColor, unselectedColor, com.navdy.hud.app.ui.component.destination.DestinationParcelable.DestinationType.NONE, null);
        java.util.List<com.navdy.hud.app.ui.component.destination.DestinationParcelable> destinations = com.navdy.hud.app.maps.util.DestinationUtil.convert(context, this.returnedDestinations, selectedColor, unselectedColor, false);
        destinations.add(0, searchAgainItem);
        com.navdy.hud.app.ui.component.destination.DestinationParcelable[] destinationParcelables = new com.navdy.hud.app.ui.component.destination.DestinationParcelable[destinations.size()];
        destinations.toArray(destinationParcelables);
        args.putParcelableArray(com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.PICKER_DESTINATIONS, destinationParcelables);
        com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().removeNotification(com.navdy.hud.app.framework.notifications.NotificationId.PLACE_TYPE_SEARCH_NOTIFICATION_ID, true, com.navdy.service.library.events.ui.Screen.SCREEN_DESTINATION_PICKER, args, new com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification.Anon2());
    }

    private void sendRequest() {
        this.currentState = com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification.PlaceTypeSearchState.SEARCHING;
        com.navdy.service.library.events.places.PlaceTypeSearchRequest placeTypeSearchRequest = new com.navdy.service.library.events.places.PlaceTypeSearchRequest.Builder().request_id(this.requestId).place_type(this.placeType).build();
        if (!com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().isAppConnected() || !com.navdy.hud.app.framework.network.NetworkStateManager.isConnectedToNetwork(com.navdy.hud.app.HudApplication.getAppContext())) {
            logger.v("performing offline quick search");
            performOfflineSearch();
        } else {
            logger.v("performing online quick search");
            this.bus.post(new com.navdy.hud.app.event.RemoteEvent(placeTypeSearchRequest));
        }
        this.handler.postDelayed(this.timeoutForFailure, TIMEOUT);
    }

    private void performOfflineSearch() {
        com.navdy.hud.app.maps.here.HerePlacesManager.handleCategoriesRequest(getHereCategoryFilter(this.placeType), 7, new com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification.Anon3(), true);
    }

    private com.here.android.mpa.search.CategoryFilter getHereCategoryFilter(com.navdy.service.library.events.places.PlaceType placeType2) {
        com.here.android.mpa.search.CategoryFilter categoryFilter = new com.here.android.mpa.search.CategoryFilter();
        switch (placeType2) {
            case PLACE_TYPE_GAS:
                categoryFilter.add(com.navdy.hud.app.maps.here.HerePlacesManager.HERE_PLACE_TYPE_GAS);
                break;
            case PLACE_TYPE_PARKING:
                categoryFilter.add(com.navdy.hud.app.maps.here.HerePlacesManager.HERE_PLACE_TYPE_PARKING);
                break;
            case PLACE_TYPE_RESTAURANT:
                for (java.lang.String restaurantPlaceType : com.navdy.hud.app.maps.here.HerePlacesManager.HERE_PLACE_TYPE_RESTAURANT) {
                    categoryFilter.add(restaurantPlaceType);
                }
                break;
            case PLACE_TYPE_STORE:
                categoryFilter.add(com.navdy.hud.app.maps.here.HerePlacesManager.HERE_PLACE_TYPE_STORE);
                break;
            case PLACE_TYPE_COFFEE:
                categoryFilter.add(com.navdy.hud.app.maps.here.HerePlacesManager.HERE_PLACE_TYPE_COFFEE);
                break;
            case PLACE_TYPE_ATM:
                categoryFilter.add(com.navdy.hud.app.maps.here.HerePlacesManager.HERE_PLACE_TYPE_ATM);
                break;
            case PLACE_TYPE_HOSPITAL:
                categoryFilter.add(com.navdy.hud.app.maps.here.HerePlacesManager.HERE_PLACE_TYPE_HOSPITAL);
                break;
        }
        return categoryFilter;
    }

    /* access modifiers changed from: private */
    public void goToSuccessfulState(java.util.List<com.navdy.service.library.events.destination.Destination> destinations) {
        android.content.res.Resources resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        this.iconColorView.setIcon(0, secondaryColor, null, ICON_SCALE);
        this.resultsCount.setVisibility(0);
        this.resultsCount.setText(java.lang.String.valueOf(destinations.size()));
        this.resultsLabel.setVisibility(0);
        com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.PlaceTypeResourceHolder holder = com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.getPlaceTypeHolder(this.placeType);
        int badgeRes = holder.iconRes;
        int badgeColor = resources.getColor(holder.colorRes);
        if (badgeRes != 0) {
            this.statusBadge.setVisibility(8);
            this.iconSide.setIcon(badgeRes, badgeColor, null, 0.5f);
            this.iconSide.setVisibility(0);
        }
        this.currentState = com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification.PlaceTypeSearchState.SEARCH_COMPLETE;
        this.choiceLayout.setChoices((java.util.List) choicesMapping.get(this.currentState), 0, this);
        this.returnedDestinations = destinations;
    }

    /* access modifiers changed from: private */
    public void goToZeroResultsState() {
        android.content.res.Resources resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        this.iconColorView.setIcon(0, secondaryColor, null, ICON_SCALE);
        this.resultsCount.setVisibility(0);
        this.resultsCount.setText(resources.getString(com.navdy.hud.app.R.string.zero));
        this.resultsLabel.setVisibility(0);
        com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.PlaceTypeResourceHolder holder = com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.getPlaceTypeHolder(this.placeType);
        int badgeRes = holder.iconRes;
        int badgeColor = resources.getColor(holder.colorRes);
        if (badgeRes != 0) {
            this.statusBadge.setVisibility(8);
            this.iconSide.setIcon(badgeRes, badgeColor, null, 0.5f);
            this.iconSide.setVisibility(0);
        }
        this.currentState = com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification.PlaceTypeSearchState.NO_RESULTS;
        this.choiceLayout.setChoices((java.util.List) choicesMapping.get(this.currentState), 0, this);
    }

    /* access modifiers changed from: private */
    public void goToFailedState() {
        this.iconColorView.setIcon(com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.getPlaceTypeHolder(this.placeType).iconRes, secondaryColor, null, ICON_SCALE);
        this.subTitle.setText(com.navdy.hud.app.R.string.place_type_search_failed);
        this.statusBadge.setImageResource(com.navdy.hud.app.R.drawable.icon_badge_alert);
        this.statusBadge.setVisibility(0);
        this.iconSide.setVisibility(8);
        this.currentState = com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification.PlaceTypeSearchState.ERROR;
        this.choiceLayout.setChoices((java.util.List) choicesMapping.get(this.currentState), 0, this);
    }

    private void dismissNotification() {
        this.notificationManager.removeNotification(com.navdy.hud.app.framework.notifications.NotificationId.PLACE_TYPE_SEARCH_NOTIFICATION_ID);
    }

    private void startLoadingAnimation() {
        if (this.loadingAnimator == null) {
            this.statusBadge.setImageResource(com.navdy.hud.app.R.drawable.loader_circle);
            this.statusBadge.setVisibility(0);
            this.iconSide.setVisibility(8);
            this.loadingAnimator = android.animation.ObjectAnimator.ofFloat(this.statusBadge, android.view.View.ROTATION, new float[]{360.0f});
            this.loadingAnimator.setDuration(500);
            this.loadingAnimator.setInterpolator(new android.view.animation.AccelerateDecelerateInterpolator());
            this.loadingAnimator.addListener(new com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification.Anon4());
        }
        if (!this.loadingAnimator.isRunning()) {
            logger.v("started loading animation");
            this.loadingAnimator.start();
        }
    }

    /* access modifiers changed from: private */
    public void stopLoadingAnimation() {
        if (this.loadingAnimator != null) {
            logger.v("cancelled loading animation");
            this.loadingAnimator.removeAllListeners();
            this.loadingAnimator.cancel();
            this.statusBadge.setRotation(0.0f);
            this.loadingAnimator = null;
        }
    }
}
