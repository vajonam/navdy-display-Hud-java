package com.navdy.hud.app.framework.places;

public class NearbyPlaceSearchNotification$$ViewInjector {
    public static void inject(butterknife.ButterKnife.Finder finder, com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification target, java.lang.Object source) {
        target.title = (android.widget.TextView) finder.findRequiredView(source, com.navdy.hud.app.R.id.title, "field 'title'");
        target.subTitle = (android.widget.TextView) finder.findRequiredView(source, com.navdy.hud.app.R.id.subTitle, "field 'subTitle'");
        target.iconColorView = (com.navdy.hud.app.ui.component.image.IconColorImageView) finder.findRequiredView(source, com.navdy.hud.app.R.id.iconColorView, "field 'iconColorView'");
        target.resultsCount = (android.widget.TextView) finder.findRequiredView(source, com.navdy.hud.app.R.id.resultsCount, "field 'resultsCount'");
        target.resultsLabel = (android.widget.TextView) finder.findRequiredView(source, com.navdy.hud.app.R.id.resultsLabel, "field 'resultsLabel'");
        target.statusBadge = (android.widget.ImageView) finder.findRequiredView(source, com.navdy.hud.app.R.id.iconSpinner, "field 'statusBadge'");
        target.iconSide = (com.navdy.hud.app.ui.component.image.IconColorImageView) finder.findRequiredView(source, com.navdy.hud.app.R.id.iconSide, "field 'iconSide'");
        target.choiceLayout = (com.navdy.hud.app.ui.component.ChoiceLayout2) finder.findRequiredView(source, com.navdy.hud.app.R.id.choiceLayout, "field 'choiceLayout'");
    }

    public static void reset(com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification target) {
        target.title = null;
        target.subTitle = null;
        target.iconColorView = null;
        target.resultsCount = null;
        target.resultsLabel = null;
        target.statusBadge = null;
        target.iconSide = null;
        target.choiceLayout = null;
    }
}
