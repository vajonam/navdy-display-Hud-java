package com.navdy.hud.app.framework.calendar;

public final class CalendarManager$$InjectAdapter extends dagger.internal.Binding<com.navdy.hud.app.framework.calendar.CalendarManager> implements javax.inject.Provider<com.navdy.hud.app.framework.calendar.CalendarManager> {
    private dagger.internal.Binding<com.squareup.otto.Bus> bus;

    public CalendarManager$$InjectAdapter() {
        super("com.navdy.hud.app.framework.calendar.CalendarManager", "members/com.navdy.hud.app.framework.calendar.CalendarManager", false, com.navdy.hud.app.framework.calendar.CalendarManager.class);
    }

    public void attach(dagger.internal.Linker linker) {
        this.bus = linker.requestBinding("com.squareup.otto.Bus", com.navdy.hud.app.framework.calendar.CalendarManager.class, getClass().getClassLoader());
    }

    public void getDependencies(java.util.Set<dagger.internal.Binding<?>> getBindings, java.util.Set<dagger.internal.Binding<?>> set) {
        getBindings.add(this.bus);
    }

    public com.navdy.hud.app.framework.calendar.CalendarManager get() {
        return new com.navdy.hud.app.framework.calendar.CalendarManager((com.squareup.otto.Bus) this.bus.get());
    }
}
