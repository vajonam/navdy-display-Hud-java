package com.navdy.hud.app.framework.calendar;

public class CalendarManager {
    private static final int CALENDAR_EVENTS_LIST_INITIAL_SIZE = 20;
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.framework.calendar.CalendarManager.class);
    public com.squareup.otto.Bus mBus;
    private java.util.List<com.navdy.service.library.events.calendars.CalendarEvent> mCalendarEvents = new java.util.ArrayList(20);
    private com.navdy.service.library.events.calendars.CalendarEventUpdates mLastCalendarEventUpdate;
    private java.lang.String profileName;

    public enum CalendarManagerEvent {
        UPDATED
    }

    @javax.inject.Inject
    public CalendarManager(com.squareup.otto.Bus bus) {
        this.mBus = bus;
        this.mBus.register(this);
    }

    @com.squareup.otto.Subscribe
    public void onCalendarEventsUpdate(com.navdy.service.library.events.calendars.CalendarEventUpdates calendarEventUpdates) {
        sLogger.d("Received Calendar updates :" + calendarEventUpdates);
        this.mLastCalendarEventUpdate = calendarEventUpdates;
        com.navdy.hud.app.profile.DriverProfile currentProfile = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getDriverProfileManager().getCurrentProfile();
        if (currentProfile != null) {
            this.profileName = currentProfile.getProfileName();
        }
        this.mCalendarEvents.clear();
        if (!(calendarEventUpdates == null || calendarEventUpdates.calendar_events == null)) {
            sLogger.d("Calendar events count :" + calendarEventUpdates.calendar_events.size());
            this.mCalendarEvents.addAll(calendarEventUpdates.calendar_events);
        }
        this.mBus.post(com.navdy.hud.app.framework.calendar.CalendarManager.CalendarManagerEvent.UPDATED);
    }

    @com.squareup.otto.Subscribe
    public void onDriverProfileChanged(com.navdy.hud.app.event.DriverProfileChanged driverProfileChanged) {
        if (!com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getDriverProfileManager().getCurrentProfile().isDefaultProfile()) {
            this.mCalendarEvents.clear();
            this.mBus.post(com.navdy.hud.app.framework.calendar.CalendarManager.CalendarManagerEvent.UPDATED);
            return;
        }
        sLogger.d("Default profile loaded, not clearing the calendar events");
    }

    public java.util.List<com.navdy.service.library.events.calendars.CalendarEvent> getCalendarEvents() {
        return this.mCalendarEvents;
    }
}
