package com.navdy.hud.app.framework.notifications;

public interface INotificationController {
    void collapseNotification(boolean z, boolean z2);

    void expandNotification(boolean z);

    android.content.Context getUIContext();

    boolean isExpanded();

    boolean isExpandedWithStack();

    boolean isShowOn();

    boolean isTtsOn();

    void moveNext(boolean z);

    void movePrevious(boolean z);

    void resetTimeout();

    void startTimeout(int i);

    void stopTimeout(boolean z);
}
