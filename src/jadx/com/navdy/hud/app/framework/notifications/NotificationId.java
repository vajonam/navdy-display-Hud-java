package com.navdy.hud.app.framework.notifications;

public final class NotificationId {
    public static final java.lang.String BRIGHTNESS_NOTIFICATION_ID = "navdy#brightness#notif";
    public static final java.lang.String GLYMPSE_NOTIFICATION_ID = "navdy#glympse#notif";
    public static final java.lang.String MUSIC_NOTIFICATION_ID = "navdy#music#notif";
    public static final java.lang.String PHONE_CALL_NOTIFICATION_ID = "navdy#phone#call#notif";
    public static final java.lang.String PLACE_TYPE_SEARCH_NOTIFICATION_ID = "navdy#place#type#search#notif";
    public static final java.lang.String ROUTE_CALC_NOTIFICATION_ID = "navdy#route#calc#notif";
    public static final java.lang.String SMS_NOTIFICATION_ID = "navdy#sms#notif#";
    public static final java.lang.String TRAFFIC_DELAY_NOTIFICATION_ID = "navdy#traffic#delay#notif";
    public static final java.lang.String TRAFFIC_EVENT_NOTIFICATION_ID = "navdy#traffic#event#notif";
    public static final java.lang.String TRAFFIC_JAM_NOTIFICATION_ID = "navdy#traffic#jam#notif";
    public static final java.lang.String TRAFFIC_REROUTE_NOTIFICATION_ID = "navdy#traffic#reroute#notif";
    public static final java.lang.String VOICE_ASSIST_NOTIFICATION_ID = "navdy#voiceassist#notif";
    public static final java.lang.String VOICE_SEARCH_NOTIFICATION_ID = "navdy#voicesearch#notif";
}
