package com.navdy.hud.app.framework.notifications;

public interface INotification extends com.navdy.hud.app.manager.InputManager.IInputHandler, com.navdy.hud.app.gesture.GestureDetector.GestureListener {
    boolean canAddToStackIfCurrentExists();

    boolean expandNotification();

    int getColor();

    android.view.View getExpandedView(android.content.Context context, java.lang.Object obj);

    int getExpandedViewIndicatorColor();

    java.lang.String getId();

    int getTimeout();

    com.navdy.hud.app.framework.notifications.NotificationType getType();

    android.view.View getView(android.content.Context context);

    android.animation.AnimatorSet getViewSwitchAnimation(boolean z);

    boolean isAlive();

    boolean isPurgeable();

    void onExpandedNotificationEvent(com.navdy.hud.app.ui.framework.UIStateManager.Mode mode);

    void onExpandedNotificationSwitched();

    void onNotificationEvent(com.navdy.hud.app.ui.framework.UIStateManager.Mode mode);

    void onStart(com.navdy.hud.app.framework.notifications.INotificationController iNotificationController);

    void onStop();

    void onUpdate();

    boolean supportScroll();
}
