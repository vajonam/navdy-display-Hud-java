package com.navdy.hud.app.framework.notifications;

public final class NotificationManager {
    private static final int EXPAND_VIEW_ANIMATION_DURATION = 250;
    public static final int EXPAND_VIEW_ANIMATION_START_DELAY_DURATION = 250;
    private static final int EXPAND_VIEW_ELEMENT_ANIMATION_DURATION = 100;
    private static final int EXPAND_VIEW_QUICK_DURATION = 0;
    public static final java.lang.String EXTRA_VOICE_SEARCH_NOTIFICATION = "extra_voice_search_notification";
    private static final com.navdy.hud.app.framework.notifications.NotificationManager.NotificationChange NOTIFICATION_CHANGE = new com.navdy.hud.app.framework.notifications.NotificationManager.NotificationChange();
    private static final com.navdy.hud.app.framework.notifications.NotificationManager sInstance = new com.navdy.hud.app.framework.notifications.NotificationManager();
    static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.framework.notifications.NotificationManager.class);
    private java.util.HashSet<java.lang.String> NOTIFS_NOT_ALLOWED_ON_DISCONNECT = new java.util.HashSet<>();
    private java.util.HashSet<java.lang.String> NOTIFS_REMOVED_ON_DISCONNECT = new java.util.HashSet<>();
    int animationTranslation;
    @javax.inject.Inject
    com.squareup.otto.Bus bus;
    private java.util.Comparator<com.navdy.hud.app.framework.notifications.NotificationManager.Info> comparator = new com.navdy.hud.app.framework.notifications.NotificationManager.Anon3();
    /* access modifiers changed from: private */
    public com.navdy.hud.app.framework.notifications.NotificationManager.Info currentNotification;
    /* access modifiers changed from: private */
    public boolean deleteAllGlances;
    private boolean disconnected = true;
    private boolean enable = true;
    private boolean enablePhoneCalls = true;
    private android.view.View expandedNotifCoverView;
    /* access modifiers changed from: private */
    public android.widget.FrameLayout expandedNotifView;
    /* access modifiers changed from: private */
    public boolean expandedWithStack;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.device.light.LED.Settings gestureEnabledSettings;
    private android.os.Handler handler = new android.os.Handler(android.os.Looper.getMainLooper());
    private java.lang.Runnable ignoreScrollRunnable = new com.navdy.hud.app.framework.notifications.NotificationManager.Anon1();
    /* access modifiers changed from: private */
    public volatile boolean isAnimating;
    /* access modifiers changed from: private */
    public volatile boolean isCollapsed;
    /* access modifiers changed from: private */
    public volatile boolean isExpanded;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.framework.notifications.NotificationManager.Info lastStackCurrentNotification;
    /* access modifiers changed from: private */
    public java.lang.Object lockObj = new java.lang.Object();
    private com.navdy.hud.app.ui.activity.Main mainScreen;
    private com.navdy.hud.app.ui.framework.INotificationAnimationListener notifAnimationListener = new com.navdy.hud.app.framework.notifications.NotificationManager.Anon4();
    /* access modifiers changed from: private */
    public com.navdy.hud.app.ui.component.carousel.CarouselIndicator notifIndicator;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.ui.component.carousel.ProgressIndicator notifScrollIndicator;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.view.NotificationView notifView;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.framework.notifications.INotificationController notificationController = new com.navdy.hud.app.framework.notifications.NotificationManager.Anon2();
    private long notificationCounter;
    private java.util.HashMap<java.lang.String, com.navdy.hud.app.framework.notifications.NotificationManager.Info> notificationIdMap = new java.util.HashMap<>();
    /* access modifiers changed from: private */
    public java.util.TreeMap<com.navdy.hud.app.framework.notifications.NotificationManager.Info, java.lang.Object> notificationPriorityMap = new java.util.TreeMap<>(this.comparator);
    private java.util.ArrayList<com.navdy.hud.app.framework.notifications.NotificationManager.Info> notificationSavedList = new java.util.ArrayList<>();
    private com.navdy.service.library.device.NavdyDeviceId notificationSavedListDeviceId;
    /* access modifiers changed from: private */
    public java.util.Queue<com.navdy.hud.app.framework.notifications.NotificationAnimator.OperationInfo> operationQueue = new java.util.LinkedList();
    /* access modifiers changed from: private */
    public boolean operationRunning;
    com.navdy.hud.app.framework.notifications.INotification pendingNotification;
    /* access modifiers changed from: private */
    public com.navdy.service.library.events.ui.Screen pendingScreen;
    /* access modifiers changed from: private */
    public android.os.Bundle pendingScreenArgs;
    /* access modifiers changed from: private */
    public java.lang.Object pendingScreenArgs2;
    /* access modifiers changed from: private */
    public android.os.Bundle pendingShutdownArgs = null;
    private android.content.res.Resources resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
    private com.navdy.hud.app.framework.notifications.IProgressUpdate scrollProgress = new com.navdy.hud.app.framework.notifications.NotificationManager.Anon5();
    /* access modifiers changed from: private */
    public com.navdy.hud.app.framework.notifications.NotificationManager.ScrollState scrollState = com.navdy.hud.app.framework.notifications.NotificationManager.ScrollState.NONE;
    /* access modifiers changed from: private */
    public boolean showOn;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.framework.notifications.NotificationManager.Info stackCurrentNotification;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.framework.notifications.NotificationManager.Info stagedNotification;
    /* access modifiers changed from: private */
    public boolean ttsOn;
    @javax.inject.Inject
    com.navdy.hud.app.ui.framework.UIStateManager uiStateManager;

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            com.navdy.hud.app.framework.notifications.NotificationManager.sLogger.v("limit reached:true");
            com.navdy.hud.app.framework.notifications.NotificationManager.this.scrollState = com.navdy.hud.app.framework.notifications.NotificationManager.ScrollState.LIMIT_REACHED;
        }
    }

    class Anon10 implements java.lang.Runnable {
        final /* synthetic */ java.lang.Runnable val$endAction;
        final /* synthetic */ android.view.View val$large;
        final /* synthetic */ com.navdy.hud.app.framework.notifications.NotificationManager.Info val$notifInfo;
        final /* synthetic */ android.view.View val$notifViewOut;
        final /* synthetic */ android.view.ViewGroup val$notifViewOutParent;

        Anon10(com.navdy.hud.app.framework.notifications.NotificationManager.Info info, android.view.View view, android.view.ViewGroup viewGroup, android.view.View view2, java.lang.Runnable runnable) {
            this.val$notifInfo = info;
            this.val$notifViewOut = view;
            this.val$notifViewOutParent = viewGroup;
            this.val$large = view2;
            this.val$endAction = runnable;
        }

        public void run() {
            if (this.val$notifInfo != null) {
                android.animation.AnimatorSet set = this.val$notifInfo.notification.getViewSwitchAnimation(true);
                if (set != null) {
                    set.setDuration(100);
                    set.start();
                }
            }
            if (this.val$notifViewOut != null && com.navdy.hud.app.framework.glance.GlanceHelper.isDeleteAllView(this.val$notifViewOut)) {
                this.val$notifViewOutParent.removeView(this.val$notifViewOut);
                com.navdy.hud.app.framework.notifications.NotificationManager.sLogger.v("delete all small view removed");
                this.val$notifViewOut.setTag(null);
                com.navdy.hud.app.framework.notifications.NotificationManager.this.cleanupView(this.val$notifViewOut);
            }
            if (this.val$large == null) {
                this.val$endAction.run();
            }
        }
    }

    class Anon11 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.hud.app.framework.notifications.NotificationAnimator.OperationInfo val$operationInfo;

        Anon11(com.navdy.hud.app.framework.notifications.NotificationAnimator.OperationInfo operationInfo) {
            this.val$operationInfo = operationInfo;
        }

        public void run() {
            com.navdy.hud.app.framework.notifications.NotificationManager.this.runOperation(this.val$operationInfo.operation, true, this.val$operationInfo.hideNotification, this.val$operationInfo.quick, this.val$operationInfo.gesture, this.val$operationInfo.id, this.val$operationInfo.collapse);
        }
    }

    class Anon12 implements java.lang.Runnable {
        final /* synthetic */ boolean val$hideNotification;

        Anon12(boolean z) {
            this.val$hideNotification = z;
        }

        public void run() {
            com.navdy.hud.app.framework.notifications.NotificationManager.this.getExpandedNotificationCoverView().setVisibility(8);
            com.navdy.hud.app.framework.notifications.NotificationManager.this.expandedWithStack = false;
            boolean hide = this.val$hideNotification;
            if (com.navdy.hud.app.framework.notifications.NotificationManager.this.stackCurrentNotification != null && com.navdy.hud.app.framework.notifications.NotificationManager.this.stackCurrentNotification != com.navdy.hud.app.framework.notifications.NotificationManager.this.currentNotification) {
                com.navdy.hud.app.framework.notifications.NotificationManager.sLogger.v("stackchange:current and stack are diff");
                if (com.navdy.hud.app.framework.notifications.NotificationManager.this.currentNotification != null && com.navdy.hud.app.framework.notifications.NotificationManager.this.currentNotification.startCalled) {
                    com.navdy.hud.app.framework.notifications.NotificationManager.sLogger.v("stackchange:stopping current:" + com.navdy.hud.app.framework.notifications.NotificationManager.this.currentNotification.notification.getId());
                    com.navdy.hud.app.framework.notifications.NotificationManager.this.currentNotification.startCalled = false;
                    com.navdy.hud.app.framework.notifications.NotificationManager.this.currentNotification.notification.onStop();
                }
                com.navdy.hud.app.framework.notifications.NotificationManager.this.currentNotification = com.navdy.hud.app.framework.notifications.NotificationManager.this.stackCurrentNotification;
                if (com.navdy.hud.app.framework.notifications.NotificationManager.this.currentNotification != null) {
                    com.navdy.hud.app.framework.notifications.NotificationManager.this.notifView.switchNotfication(com.navdy.hud.app.framework.notifications.NotificationManager.this.currentNotification.notification);
                    if (com.navdy.hud.app.framework.notifications.NotificationManager.this.currentNotification.removed) {
                        hide = true;
                        com.navdy.hud.app.framework.notifications.NotificationManager.sLogger.v("stackchange:hide-diff-remove");
                    } else if (!com.navdy.hud.app.framework.notifications.NotificationManager.this.currentNotification.startCalled) {
                        com.navdy.hud.app.framework.notifications.NotificationManager.sLogger.v("stackchange:starting stacked:" + com.navdy.hud.app.framework.notifications.NotificationManager.this.currentNotification.notification.getId());
                        com.navdy.hud.app.framework.notifications.NotificationManager.this.currentNotification.notification.getView(com.navdy.hud.app.framework.notifications.NotificationManager.this.notifView.getContext());
                        com.navdy.hud.app.framework.notifications.NotificationManager.this.currentNotification.startCalled = true;
                        com.navdy.hud.app.framework.notifications.NotificationManager.this.currentNotification.notification.onStart(com.navdy.hud.app.framework.notifications.NotificationManager.this.notificationController);
                    }
                    if (!hide) {
                        com.navdy.hud.app.framework.notifications.NotificationManager.sLogger.v("calling onExpandedNotificationEvent-collapse-");
                        com.navdy.hud.app.framework.notifications.NotificationManager.this.currentNotification.notification.onExpandedNotificationEvent(com.navdy.hud.app.ui.framework.UIStateManager.Mode.COLLAPSE);
                    }
                }
                com.navdy.hud.app.framework.notifications.NotificationManager.this.setNotificationColor();
                com.navdy.hud.app.framework.notifications.NotificationManager.this.notifView.showNextNotificationColor();
            } else if (com.navdy.hud.app.framework.notifications.NotificationManager.this.currentNotification != null) {
                if (com.navdy.hud.app.framework.notifications.NotificationManager.this.currentNotification.removed) {
                    hide = true;
                    com.navdy.hud.app.framework.notifications.NotificationManager.sLogger.v("stackchange:hide-remove");
                } else if (!com.navdy.hud.app.framework.notifications.NotificationManager.this.currentNotification.startCalled && !com.navdy.hud.app.framework.notifications.NotificationManager.this.currentNotification.removed) {
                    com.navdy.hud.app.framework.notifications.NotificationManager.this.currentNotification.notification.getView(com.navdy.hud.app.framework.notifications.NotificationManager.this.notifView.getContext());
                    com.navdy.hud.app.framework.notifications.NotificationManager.this.currentNotification.startCalled = true;
                    com.navdy.hud.app.framework.notifications.NotificationManager.this.currentNotification.notification.onStart(com.navdy.hud.app.framework.notifications.NotificationManager.this.notificationController);
                }
            }
            com.navdy.hud.app.framework.notifications.NotificationManager.this.stackCurrentNotification = null;
            com.navdy.hud.app.framework.notifications.NotificationManager.this.lastStackCurrentNotification = null;
            com.navdy.hud.app.framework.notifications.NotificationManager.this.clearOperationQueue();
            if (hide) {
                com.navdy.hud.app.framework.notifications.NotificationManager.this.hideNotificationInternal();
            } else {
                com.navdy.hud.app.framework.notifications.NotificationManager.this.isAnimating = false;
            }
        }
    }

    class Anon13 implements java.lang.Runnable {
        final /* synthetic */ android.view.View val$child;
        final /* synthetic */ android.view.ViewGroup val$parent;
        final /* synthetic */ java.lang.Runnable val$runnable;

        Anon13(android.view.View view, android.view.ViewGroup viewGroup, java.lang.Runnable runnable) {
            this.val$child = view;
            this.val$parent = viewGroup;
            this.val$runnable = runnable;
        }

        public void run() {
            com.navdy.hud.app.framework.notifications.NotificationManager.this.hideScrollingIndicator();
            if (this.val$child != null) {
                this.val$parent.removeView(this.val$child);
            }
            if (com.navdy.hud.app.framework.glance.GlanceHelper.isDeleteAllView(this.val$child)) {
                this.val$child.setTag(null);
                this.val$child.setAlpha(1.0f);
                com.navdy.hud.app.framework.glance.GlanceViewCache.putView(com.navdy.hud.app.framework.glance.GlanceViewCache.ViewType.BIG_TEXT, this.val$child);
            }
            com.navdy.hud.app.framework.notifications.NotificationAnimator.animateExpandedViewOut(com.navdy.hud.app.framework.notifications.NotificationManager.this.notifView, com.navdy.hud.app.framework.notifications.NotificationManager.this.uiStateManager.getMainPanelWidth() - com.navdy.hud.app.framework.notifications.NotificationManager.this.uiStateManager.getSidePanelWidth(), com.navdy.hud.app.framework.notifications.NotificationManager.this.expandedNotifView, com.navdy.hud.app.framework.notifications.NotificationManager.this.notifIndicator, this.val$runnable, com.navdy.hud.app.framework.notifications.NotificationManager.this.currentNotification, com.navdy.hud.app.framework.notifications.NotificationManager.this.showOn, com.navdy.hud.app.framework.notifications.NotificationManager.this.isExpandedNotificationVisible());
        }
    }

    class Anon14 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
        final /* synthetic */ int val$color;
        final /* synthetic */ android.view.View val$large;
        final /* synthetic */ com.navdy.hud.app.framework.notifications.NotificationManager.Info val$notifInfo;
        final /* synthetic */ boolean val$previous;
        final /* synthetic */ android.view.View val$small;

        Anon14(android.view.View view, android.view.View view2, com.navdy.hud.app.framework.notifications.NotificationManager.Info info, boolean z, int i) {
            this.val$small = view;
            this.val$large = view2;
            this.val$notifInfo = info;
            this.val$previous = z;
            this.val$color = i;
        }

        public void onAnimationEnd(android.animation.Animator animation) {
            com.navdy.hud.app.framework.notifications.NotificationManager.this.viewSwitchAnimation(this.val$small, this.val$large, this.val$notifInfo, this.val$previous, this.val$color);
        }
    }

    class Anon15 implements android.view.ViewTreeObserver.OnGlobalLayoutListener {
        final /* synthetic */ int val$index;
        final /* synthetic */ com.navdy.hud.app.framework.notifications.IScrollEvent val$scroll;

        Anon15(int i, com.navdy.hud.app.framework.notifications.IScrollEvent iScrollEvent) {
            this.val$index = i;
            this.val$scroll = iScrollEvent;
        }

        public void onGlobalLayout() {
            android.graphics.RectF rectF = com.navdy.hud.app.framework.notifications.NotificationManager.this.notifIndicator.getItemPos(this.val$index);
            if (rectF != null) {
                if (rectF.left != 0.0f || rectF.top != 0.0f) {
                    com.navdy.hud.app.framework.notifications.NotificationManager.this.setNotifScrollIndicatorXY(rectF, this.val$scroll);
                } else {
                    return;
                }
            }
            com.navdy.hud.app.framework.notifications.NotificationManager.this.notifIndicator.getViewTreeObserver().removeOnGlobalLayoutListener(this);
        }
    }

    class Anon2 implements com.navdy.hud.app.framework.notifications.INotificationController {

        class Anon1 implements java.lang.Runnable {
            final /* synthetic */ com.navdy.hud.app.ui.component.carousel.CarouselIndicator val$indicator;
            final /* synthetic */ com.navdy.hud.app.framework.notifications.INotification val$notificationObj;

            Anon1(com.navdy.hud.app.framework.notifications.INotification iNotification, com.navdy.hud.app.ui.component.carousel.CarouselIndicator carouselIndicator) {
                this.val$notificationObj = iNotification;
                this.val$indicator = carouselIndicator;
            }

            public void run() {
                com.navdy.hud.app.framework.notifications.NotificationManager.sLogger.v("calling onExpandedNotificationEvent-expand");
                this.val$notificationObj.onExpandedNotificationEvent(com.navdy.hud.app.ui.framework.UIStateManager.Mode.EXPAND);
                this.val$indicator.setVisibility(0);
                com.navdy.hud.app.framework.notifications.NotificationManager.this.displayScrollingIndicator(this.val$indicator.getCurrentItem());
            }
        }

        /* renamed from: com.navdy.hud.app.framework.notifications.NotificationManager$Anon2$Anon2 reason: collision with other inner class name */
        class C0012Anon2 implements java.lang.Runnable {
            final /* synthetic */ android.widget.FrameLayout val$layout;
            final /* synthetic */ android.view.View val$layoutCover;

            C0012Anon2(android.view.View view, android.widget.FrameLayout frameLayout) {
                this.val$layoutCover = view;
                this.val$layout = frameLayout;
            }

            public void run() {
                this.val$layoutCover.setVisibility(0);
                this.val$layout.setVisibility(0);
            }
        }

        class Anon3 implements java.lang.Runnable {
            final /* synthetic */ java.lang.Runnable val$endAction;
            final /* synthetic */ android.view.View val$expandedView;
            final /* synthetic */ android.widget.FrameLayout val$layout;

            Anon3(android.view.View view, android.widget.FrameLayout frameLayout, java.lang.Runnable runnable) {
                this.val$expandedView = view;
                this.val$layout = frameLayout;
                this.val$endAction = runnable;
            }

            public void run() {
                com.navdy.hud.app.framework.notifications.NotificationAnimator.animateExpandedViews(this.val$expandedView, null, this.val$layout, com.navdy.hud.app.framework.notifications.NotificationManager.this.animationTranslation, 250, 0, this.val$endAction);
            }
        }

        Anon2() {
        }

        public void startTimeout(int timeout) {
            com.navdy.hud.app.view.NotificationView notificationView = com.navdy.hud.app.framework.notifications.NotificationManager.this.getNotificationView();
            if (notificationView != null) {
                notificationView.border.startTimeout(timeout);
            }
        }

        public void stopTimeout(boolean force) {
            com.navdy.hud.app.view.NotificationView notificationView = com.navdy.hud.app.framework.notifications.NotificationManager.this.getNotificationView();
            if (notificationView != null) {
                notificationView.border.stopTimeout(force, null);
            }
        }

        public void resetTimeout() {
            com.navdy.hud.app.view.NotificationView notificationView = com.navdy.hud.app.framework.notifications.NotificationManager.this.getNotificationView();
            if (notificationView != null) {
                notificationView.border.resetTimeout();
            }
        }

        public void expandNotification(boolean withStack) {
            int notificationIndex;
            com.navdy.hud.app.framework.notifications.NotificationManager.sLogger.v("expandNotification  running=" + com.navdy.hud.app.framework.notifications.NotificationManager.this.operationRunning + " qsize:" + com.navdy.hud.app.framework.notifications.NotificationManager.this.operationQueue.size());
            com.navdy.hud.app.view.NotificationView notificationView = com.navdy.hud.app.framework.notifications.NotificationManager.this.getNotificationView();
            if (notificationView == null || com.navdy.hud.app.framework.notifications.NotificationManager.this.currentNotification == null) {
                com.navdy.hud.app.framework.notifications.NotificationManager.sLogger.i("cannot display expand notif:no current notif");
            } else if (com.navdy.hud.app.framework.notifications.NotificationManager.this.isAnimating) {
                com.navdy.hud.app.framework.notifications.NotificationManager.sLogger.i("animation in progress, ignore expand");
            } else if (isExpanded()) {
                com.navdy.hud.app.framework.notifications.NotificationManager.sLogger.v("already expanded");
            } else {
                android.widget.FrameLayout layout = com.navdy.hud.app.framework.notifications.NotificationManager.this.getExpandedNotificationView();
                android.view.View layoutCover = com.navdy.hud.app.framework.notifications.NotificationManager.this.getExpandedNotificationCoverView();
                com.navdy.hud.app.ui.component.carousel.CarouselIndicator indicator = com.navdy.hud.app.framework.notifications.NotificationManager.this.getNotificationIndicator();
                com.navdy.hud.app.framework.notifications.NotificationManager.this.scrollState = com.navdy.hud.app.framework.notifications.NotificationManager.ScrollState.NONE;
                if (layout.getChildCount() > 0) {
                    layout.removeAllViews();
                }
                if (withStack) {
                    com.navdy.service.library.events.preferences.NotificationPreferences preferences = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile().getNotificationPreferences();
                    com.navdy.hud.app.framework.notifications.NotificationManager.this.ttsOn = preferences.readAloud.booleanValue();
                    com.navdy.hud.app.framework.notifications.NotificationManager.this.showOn = preferences.showContent.booleanValue();
                    com.navdy.hud.app.framework.notifications.NotificationManager.sLogger.v("expanded with[" + com.navdy.hud.app.framework.notifications.NotificationManager.this.currentNotification.notification.getId() + "] tts=" + com.navdy.hud.app.framework.notifications.NotificationManager.this.ttsOn + " show=" + com.navdy.hud.app.framework.notifications.NotificationManager.this.showOn);
                }
                android.view.View tempExpandedView = null;
                if (!withStack || (withStack && com.navdy.hud.app.framework.notifications.NotificationManager.this.showOn)) {
                    tempExpandedView = com.navdy.hud.app.framework.notifications.NotificationManager.this.currentNotification.notification.getExpandedView(notificationView.getContext(), null);
                    if (tempExpandedView == null) {
                        com.navdy.hud.app.framework.notifications.NotificationManager.sLogger.w("cannot expand, view is null");
                        com.navdy.hud.app.framework.notifications.NotificationManager.this.ttsOn = false;
                        com.navdy.hud.app.framework.notifications.NotificationManager.this.showOn = false;
                        return;
                    }
                }
                android.view.View expandedView = tempExpandedView;
                com.navdy.hud.app.framework.notifications.NotificationManager.this.expandedWithStack = withStack;
                if (withStack) {
                    com.navdy.hud.app.framework.notifications.NotificationManager.this.stackCurrentNotification = com.navdy.hud.app.framework.notifications.NotificationManager.this.currentNotification;
                    com.navdy.hud.app.framework.notifications.NotificationManager.this.lastStackCurrentNotification = null;
                    if (com.navdy.hud.app.framework.notifications.NotificationManager.sLogger.isLoggable(2)) {
                        com.navdy.hud.app.framework.notifications.NotificationManager.this.printPriorityMap();
                    }
                    layout.setTag(java.lang.Boolean.valueOf(true));
                } else {
                    layout.setTag(null);
                    com.navdy.hud.app.framework.notifications.NotificationManager.this.stackCurrentNotification = null;
                    com.navdy.hud.app.framework.notifications.NotificationManager.this.lastStackCurrentNotification = null;
                }
                notificationView.border.stopTimeout(true, null);
                notificationView.hideNextNotificationColor();
                com.navdy.hud.app.framework.notifications.INotification notificationObj = com.navdy.hud.app.framework.notifications.NotificationManager.this.currentNotification.notification;
                if (withStack) {
                    int notificationCount = com.navdy.hud.app.framework.notifications.NotificationManager.this.getNotificationCount();
                    if (com.navdy.hud.app.framework.notifications.NotificationManager.this.isPhoneNotifPresentAndNotAlive()) {
                        notificationCount--;
                    }
                    int notificationIndex2 = com.navdy.hud.app.framework.notifications.NotificationManager.this.getNotificationIndex(com.navdy.hud.app.framework.notifications.NotificationManager.this.currentNotification);
                    com.navdy.hud.app.framework.notifications.NotificationManager.sLogger.v("expandNotification index:" + notificationIndex2);
                    if (notificationIndex2 == -1) {
                        notificationIndex = 0;
                    } else {
                        notificationIndex = notificationIndex2 + 1;
                    }
                    int notificationCount2 = notificationCount + 2;
                    if (notificationIndex >= notificationCount2) {
                        notificationIndex = notificationCount2 - 1;
                    }
                    com.navdy.hud.app.framework.notifications.NotificationManager.this.updateExpandedIndicator(notificationCount2, notificationIndex, com.navdy.hud.app.framework.notifications.NotificationManager.this.currentNotification.notification.getColor());
                }
                java.lang.Runnable endAction = new com.navdy.hud.app.framework.notifications.NotificationManager.Anon2.Anon1(notificationObj, indicator);
                if (!com.navdy.hud.app.framework.notifications.NotificationManager.this.expandedWithStack || (com.navdy.hud.app.framework.notifications.NotificationManager.this.expandedWithStack && com.navdy.hud.app.framework.notifications.NotificationManager.this.showOn)) {
                    notificationView.animate().x(0.0f).withEndAction(new com.navdy.hud.app.framework.notifications.NotificationManager.Anon2.Anon3(expandedView, layout, endAction)).withStartAction(new com.navdy.hud.app.framework.notifications.NotificationManager.Anon2.C0012Anon2(layoutCover, layout)).start();
                } else {
                    endAction.run();
                }
            }
        }

        public void collapseNotification(boolean completely, boolean quickly) {
            com.navdy.hud.app.framework.notifications.NotificationManager.this.collapseNotificationInternal(completely, quickly, false);
        }

        public void moveNext(boolean gesture) {
            com.navdy.hud.app.framework.notifications.NotificationManager.this.moveNext(gesture);
        }

        public void movePrevious(boolean gesture) {
            com.navdy.hud.app.framework.notifications.NotificationManager.this.movePrevious(gesture);
        }

        public boolean isExpandedWithStack() {
            return com.navdy.hud.app.framework.notifications.NotificationManager.this.isExpanded();
        }

        public boolean isExpanded() {
            return com.navdy.hud.app.framework.notifications.NotificationManager.this.isExpandedNotificationVisible();
        }

        public boolean isTtsOn() {
            return com.navdy.hud.app.framework.notifications.NotificationManager.this.ttsOn;
        }

        public boolean isShowOn() {
            return com.navdy.hud.app.framework.notifications.NotificationManager.this.showOn;
        }

        public android.content.Context getUIContext() {
            return com.navdy.hud.app.framework.notifications.NotificationManager.this.getUIContext();
        }
    }

    class Anon3 implements java.util.Comparator<com.navdy.hud.app.framework.notifications.NotificationManager.Info> {
        Anon3() {
        }

        public int compare(com.navdy.hud.app.framework.notifications.NotificationManager.Info lhs, com.navdy.hud.app.framework.notifications.NotificationManager.Info rhs) {
            int diff = lhs.priority - rhs.priority;
            return diff != 0 ? diff : (int) (rhs.uniqueCounter - lhs.uniqueCounter);
        }
    }

    class Anon4 implements com.navdy.hud.app.ui.framework.INotificationAnimationListener {
        Anon4() {
        }

        public void onStart(java.lang.String id, com.navdy.hud.app.framework.notifications.NotificationType type, com.navdy.hud.app.ui.framework.UIStateManager.Mode mode) {
            com.navdy.hud.app.framework.notifications.NotificationManager.sLogger.v("notif-anim-start [" + id + "] type[" + type + "] mode [" + mode + "]");
            com.navdy.hud.app.framework.notifications.NotificationManager.this.isAnimating = true;
            if (mode == com.navdy.hud.app.ui.framework.UIStateManager.Mode.EXPAND) {
                com.navdy.hud.app.framework.notifications.NotificationManager.this.isCollapsed = false;
                com.navdy.hud.app.view.NotificationView notificationView = com.navdy.hud.app.framework.notifications.NotificationManager.this.getNotificationView();
                notificationView.border.stopTimeout(true, null);
                com.navdy.hud.app.framework.notifications.NotificationManager.sLogger.v("notif-anim-start showing notif [" + com.navdy.hud.app.framework.notifications.NotificationManager.this.currentNotification.notification.getId() + "]");
                android.view.View view = com.navdy.hud.app.framework.notifications.NotificationManager.this.currentNotification.notification.getView(notificationView.getContext());
                com.navdy.hud.app.framework.notifications.NotificationManager.this.currentNotification.pushBackDuetoHighPriority = false;
                com.navdy.hud.app.framework.notifications.NotificationManager.this.currentNotification.startCalled = true;
                com.navdy.hud.app.framework.notifications.NotificationManager.this.currentNotification.notification.onStart(com.navdy.hud.app.framework.notifications.NotificationManager.this.notificationController);
                com.navdy.hud.app.framework.notifications.NotificationManager.sLogger.v("current notification [" + com.navdy.hud.app.framework.notifications.NotificationManager.this.currentNotification.notification.getId() + "] called start[" + java.lang.System.identityHashCode(com.navdy.hud.app.framework.notifications.NotificationManager.this.currentNotification.notification) + "]");
                notificationView.addCustomView(com.navdy.hud.app.framework.notifications.NotificationManager.this.currentNotification.notification, view);
                com.navdy.hud.app.framework.notifications.NotificationManager.this.setNotificationColor();
                com.navdy.hud.app.framework.notifications.NotificationManager.this.currentNotification.notification.onNotificationEvent(com.navdy.hud.app.ui.framework.UIStateManager.Mode.EXPAND);
                return;
            }
            com.navdy.hud.app.framework.notifications.NotificationManager.this.isExpanded = false;
            com.navdy.hud.app.device.light.HUDLightUtils.removeSettings(com.navdy.hud.app.framework.notifications.NotificationManager.this.gestureEnabledSettings);
        }

        public void onStop(java.lang.String id, com.navdy.hud.app.framework.notifications.NotificationType type, com.navdy.hud.app.ui.framework.UIStateManager.Mode mode) {
            com.navdy.hud.app.framework.notifications.NotificationManager.sLogger.v("notif-anim-stop [" + id + "] type[" + type + "] mode [" + mode + "]");
            try {
                com.navdy.hud.app.framework.notifications.INotification pending = com.navdy.hud.app.framework.notifications.NotificationManager.this.pendingNotification;
                com.navdy.hud.app.framework.notifications.NotificationManager.this.pendingNotification = null;
                if (mode == com.navdy.hud.app.ui.framework.UIStateManager.Mode.COLLAPSE) {
                    boolean notificationAdded = false;
                    com.navdy.hud.app.framework.notifications.NotificationManager.this.isCollapsed = true;
                    com.navdy.hud.app.view.NotificationView view = com.navdy.hud.app.framework.notifications.NotificationManager.this.getNotificationView();
                    com.navdy.hud.app.framework.notifications.NotificationManager.this.hideNotificationCoverView();
                    if (com.navdy.hud.app.framework.notifications.NotificationManager.this.deleteAllGlances) {
                        com.navdy.hud.app.framework.notifications.NotificationManager.this.deleteAllGlances = false;
                        com.navdy.hud.app.framework.notifications.NotificationManager.sLogger.v("*** user selected delete all glances");
                        view.removeCustomView();
                        synchronized (com.navdy.hud.app.framework.notifications.NotificationManager.this.lockObj) {
                            com.navdy.hud.app.framework.notifications.NotificationManager.this.removeAllNotification();
                        }
                        if (0 == 0) {
                            com.navdy.hud.app.ui.component.homescreen.HomeScreenView homeScreenView = com.navdy.hud.app.framework.notifications.NotificationManager.this.uiStateManager.getHomescreenView();
                            if (homeScreenView.hasModeView()) {
                                com.navdy.hud.app.framework.notifications.NotificationManager.sLogger.v("mode-view: notif onStop show");
                                homeScreenView.animateInModeView();
                            }
                        }
                        return;
                    }
                    com.navdy.hud.app.framework.notifications.NotificationManager.this.cleanupViews(view);
                    if (com.navdy.hud.app.framework.notifications.NotificationManager.this.currentNotification == null) {
                        if (0 == 0) {
                            com.navdy.hud.app.ui.component.homescreen.HomeScreenView homeScreenView2 = com.navdy.hud.app.framework.notifications.NotificationManager.this.uiStateManager.getHomescreenView();
                            if (homeScreenView2.hasModeView()) {
                                com.navdy.hud.app.framework.notifications.NotificationManager.sLogger.v("mode-view: notif onStop show");
                                homeScreenView2.animateInModeView();
                            }
                        }
                        com.navdy.hud.app.framework.notifications.NotificationManager.this.isAnimating = false;
                        return;
                    }
                    com.navdy.hud.app.framework.notifications.NotificationManager.this.currentNotification.notification.onNotificationEvent(com.navdy.hud.app.ui.framework.UIStateManager.Mode.COLLAPSE);
                    java.lang.String notifId = com.navdy.hud.app.framework.notifications.NotificationManager.this.currentNotification.notification.getId();
                    com.navdy.hud.app.framework.notifications.NotificationManager.sLogger.v("notif-anim-start [" + notifId + "]");
                    com.navdy.hud.app.framework.notifications.NotificationManager.this.notificationController.stopTimeout(false);
                    if (!com.navdy.hud.app.framework.notifications.NotificationManager.this.currentNotification.removed && !com.navdy.hud.app.framework.notifications.NotificationManager.this.currentNotification.resurrected && !com.navdy.hud.app.framework.notifications.NotificationManager.this.currentNotification.notification.isAlive()) {
                        com.navdy.hud.app.framework.notifications.NotificationManager.sLogger.v("notif-anim-stop not alive, marked removed");
                        com.navdy.hud.app.framework.notifications.NotificationManager.this.currentNotification.removed = true;
                    }
                    if (com.navdy.hud.app.framework.notifications.NotificationManager.this.currentNotification.removed) {
                        com.navdy.hud.app.framework.notifications.NotificationManager.sLogger.v("notif-anim-stop removed [" + notifId + "]");
                        synchronized (com.navdy.hud.app.framework.notifications.NotificationManager.this.lockObj) {
                            com.navdy.hud.app.framework.notifications.NotificationManager.this.removeNotificationfromStack(com.navdy.hud.app.framework.notifications.NotificationManager.this.currentNotification, false);
                        }
                    } else {
                        com.navdy.hud.app.framework.notifications.NotificationManager.sLogger.v("notif-anim-stop pushed back [" + notifId + "]");
                        if (com.navdy.hud.app.framework.notifications.NotificationManager.this.currentNotification.startCalled) {
                            com.navdy.hud.app.framework.notifications.NotificationManager.this.currentNotification.startCalled = false;
                            com.navdy.hud.app.framework.notifications.NotificationManager.this.currentNotification.notification.onStop();
                        }
                    }
                    try {
                        view.removeCustomView();
                        if (com.navdy.hud.app.framework.notifications.NotificationManager.this.currentNotification.resurrected) {
                            com.navdy.hud.app.framework.notifications.NotificationManager.sLogger.v("notif-anim-stop notif resurrected");
                            com.navdy.hud.app.framework.notifications.NotificationManager.this.currentNotification.resurrected = false;
                            com.navdy.hud.app.framework.notifications.NotificationManager.this.isAnimating = false;
                            com.navdy.hud.app.framework.notifications.NotificationManager.this.showNotification(true);
                            if (1 == 0) {
                                com.navdy.hud.app.ui.component.homescreen.HomeScreenView homeScreenView3 = com.navdy.hud.app.framework.notifications.NotificationManager.this.uiStateManager.getHomescreenView();
                                if (homeScreenView3.hasModeView()) {
                                    com.navdy.hud.app.framework.notifications.NotificationManager.sLogger.v("mode-view: notif onStop show");
                                    homeScreenView3.animateInModeView();
                                }
                            }
                            com.navdy.hud.app.framework.notifications.NotificationManager.this.isAnimating = false;
                            return;
                        }
                        com.navdy.hud.app.framework.notifications.NotificationManager.this.currentNotification = null;
                        if (com.navdy.hud.app.framework.notifications.NotificationManager.this.stagedNotification == null && com.navdy.hud.app.framework.notifications.NotificationManager.this.pendingScreen != null && com.navdy.hud.app.framework.notifications.NotificationManager.this.isScreenHighPriority(com.navdy.hud.app.framework.notifications.NotificationManager.this.pendingScreen)) {
                            com.navdy.hud.app.framework.notifications.NotificationManager.this.bus.post(new com.navdy.hud.app.event.ShowScreenWithArgs(com.navdy.hud.app.framework.notifications.NotificationManager.this.pendingScreen, com.navdy.hud.app.framework.notifications.NotificationManager.this.pendingScreenArgs, com.navdy.hud.app.framework.notifications.NotificationManager.this.pendingScreenArgs2, false));
                            com.navdy.hud.app.framework.notifications.NotificationManager.sLogger.v("launched pending screen hp:" + com.navdy.hud.app.framework.notifications.NotificationManager.this.pendingScreen);
                            com.navdy.hud.app.framework.notifications.NotificationManager.this.pendingScreen = null;
                            com.navdy.hud.app.framework.notifications.NotificationManager.this.pendingScreenArgs = null;
                            com.navdy.hud.app.framework.notifications.NotificationManager.this.pendingScreenArgs2 = null;
                            if (0 == 0) {
                                com.navdy.hud.app.ui.component.homescreen.HomeScreenView homeScreenView4 = com.navdy.hud.app.framework.notifications.NotificationManager.this.uiStateManager.getHomescreenView();
                                if (homeScreenView4.hasModeView()) {
                                    com.navdy.hud.app.framework.notifications.NotificationManager.sLogger.v("mode-view: notif onStop show");
                                    homeScreenView4.animateInModeView();
                                }
                            }
                            com.navdy.hud.app.framework.notifications.NotificationManager.this.isAnimating = false;
                            return;
                        } else if (com.navdy.hud.app.framework.notifications.NotificationManager.this.stagedNotification != null) {
                            com.navdy.hud.app.framework.notifications.NotificationManager.sLogger.v("notif-anim-stop staged-notif [" + com.navdy.hud.app.framework.notifications.NotificationManager.this.stagedNotification.notification.getId() + "]");
                            com.navdy.hud.app.framework.notifications.NotificationManager.this.currentNotification = com.navdy.hud.app.framework.notifications.NotificationManager.this.stagedNotification;
                            com.navdy.hud.app.framework.notifications.NotificationManager.this.stagedNotification = null;
                            com.navdy.hud.app.framework.notifications.NotificationManager.this.isAnimating = false;
                            com.navdy.hud.app.framework.notifications.NotificationManager.this.showNotification();
                            if (1 == 0) {
                                com.navdy.hud.app.ui.component.homescreen.HomeScreenView homeScreenView5 = com.navdy.hud.app.framework.notifications.NotificationManager.this.uiStateManager.getHomescreenView();
                                if (homeScreenView5.hasModeView()) {
                                    com.navdy.hud.app.framework.notifications.NotificationManager.sLogger.v("mode-view: notif onStop show");
                                    homeScreenView5.animateInModeView();
                                }
                            }
                            com.navdy.hud.app.framework.notifications.NotificationManager.this.isAnimating = false;
                            return;
                        } else {
                            com.navdy.hud.app.framework.notifications.NotificationManager.sLogger.v("notif-anim-stop no staged-notif, check if there is pending one");
                            synchronized (com.navdy.hud.app.framework.notifications.NotificationManager.this.lockObj) {
                                if (com.navdy.hud.app.framework.notifications.NotificationManager.this.notificationPriorityMap.size() > 0) {
                                    com.navdy.hud.app.framework.notifications.NotificationManager.Info info = (com.navdy.hud.app.framework.notifications.NotificationManager.Info) com.navdy.hud.app.framework.notifications.NotificationManager.this.notificationPriorityMap.lastKey();
                                    if (info.pushBackDuetoHighPriority) {
                                        com.navdy.hud.app.framework.notifications.NotificationManager.sLogger.v("notif-anim-stop found pending [" + info.notification.getId() + "]");
                                        com.navdy.hud.app.framework.notifications.NotificationManager.this.currentNotification = info;
                                        com.navdy.hud.app.framework.notifications.NotificationManager.this.isAnimating = false;
                                        notificationAdded = true;
                                        com.navdy.hud.app.framework.notifications.NotificationManager.this.showNotification();
                                        pending = null;
                                    }
                                }
                            }
                            if (com.navdy.hud.app.framework.notifications.NotificationManager.this.pendingShutdownArgs != null) {
                                com.navdy.hud.app.framework.notifications.NotificationManager.this.bus.post(new com.navdy.hud.app.event.ShowScreenWithArgs(com.navdy.service.library.events.ui.Screen.SCREEN_SHUTDOWN_CONFIRMATION, com.navdy.hud.app.framework.notifications.NotificationManager.this.pendingShutdownArgs, false));
                                com.navdy.hud.app.framework.notifications.NotificationManager.this.pendingShutdownArgs = null;
                            } else if (pending != null) {
                                com.navdy.hud.app.framework.notifications.NotificationManager.sLogger.v("adding pending notification:" + pending + " current = " + com.navdy.hud.app.framework.notifications.NotificationManager.this.currentNotification);
                                com.navdy.hud.app.framework.notifications.NotificationManager.this.isAnimating = false;
                                com.navdy.hud.app.framework.notifications.NotificationManager.this.addNotification(pending);
                            } else if (com.navdy.hud.app.framework.notifications.NotificationManager.this.pendingScreen != null) {
                                com.navdy.hud.app.framework.notifications.NotificationManager.this.bus.post(new com.navdy.hud.app.event.ShowScreenWithArgs(com.navdy.hud.app.framework.notifications.NotificationManager.this.pendingScreen, com.navdy.hud.app.framework.notifications.NotificationManager.this.pendingScreenArgs, com.navdy.hud.app.framework.notifications.NotificationManager.this.pendingScreenArgs2, false));
                                com.navdy.hud.app.framework.notifications.NotificationManager.sLogger.v("launched pending screen:" + com.navdy.hud.app.framework.notifications.NotificationManager.this.pendingScreen);
                            }
                            com.navdy.hud.app.framework.notifications.NotificationManager.this.pendingScreen = null;
                            com.navdy.hud.app.framework.notifications.NotificationManager.this.pendingScreenArgs = null;
                            com.navdy.hud.app.framework.notifications.NotificationManager.this.pendingScreenArgs2 = null;
                            if (!notificationAdded) {
                                com.navdy.hud.app.ui.component.homescreen.HomeScreenView homeScreenView6 = com.navdy.hud.app.framework.notifications.NotificationManager.this.uiStateManager.getHomescreenView();
                                if (homeScreenView6.hasModeView()) {
                                    com.navdy.hud.app.framework.notifications.NotificationManager.sLogger.v("mode-view: notif onStop show");
                                    homeScreenView6.animateInModeView();
                                }
                            }
                        }
                    } catch (Throwable th) {
                        if (!notificationAdded) {
                            com.navdy.hud.app.ui.component.homescreen.HomeScreenView homeScreenView7 = com.navdy.hud.app.framework.notifications.NotificationManager.this.uiStateManager.getHomescreenView();
                            if (homeScreenView7.hasModeView()) {
                                com.navdy.hud.app.framework.notifications.NotificationManager.sLogger.v("mode-view: notif onStop show");
                                homeScreenView7.animateInModeView();
                            }
                        }
                        throw th;
                    }
                } else {
                    com.navdy.hud.app.framework.notifications.NotificationManager.this.isExpanded = true;
                    if (com.navdy.hud.app.framework.notifications.NotificationManager.this.currentNotification == null || com.navdy.hud.app.framework.notifications.NotificationManager.this.currentNotification.removed || com.navdy.hud.app.framework.notifications.NotificationManager.this.stagedNotification != null) {
                        com.navdy.hud.app.framework.notifications.NotificationManager.sLogger.v("notif-anim-start got removed/changed while it was animating, hide it");
                        com.navdy.hud.app.framework.notifications.NotificationManager.this.isAnimating = false;
                        com.navdy.hud.app.framework.notifications.NotificationManager.this.hideNotification();
                    } else {
                        com.navdy.hud.app.framework.notifications.NotificationManager.this.setNotificationColor();
                        com.navdy.hud.app.framework.notifications.NotificationManager.this.notifView.showNextNotificationColor();
                        com.navdy.hud.app.framework.notifications.NotificationManager.this.notificationController.startTimeout(com.navdy.hud.app.framework.notifications.NotificationManager.this.currentNotification.notification.getTimeout());
                    }
                    com.navdy.hud.app.framework.notifications.NotificationManager.this.gestureEnabledSettings = com.navdy.hud.app.device.light.HUDLightUtils.showGestureDetectionEnabled(com.navdy.hud.app.HudApplication.getAppContext(), com.navdy.hud.app.device.light.LightManager.getInstance(), "Notification");
                }
                com.navdy.hud.app.framework.notifications.NotificationManager.this.isAnimating = false;
            } finally {
                com.navdy.hud.app.framework.notifications.NotificationManager.this.isAnimating = false;
            }
        }
    }

    class Anon5 implements com.navdy.hud.app.framework.notifications.IProgressUpdate {
        Anon5() {
        }

        public void onPosChange(int pos) {
            if (com.navdy.hud.app.framework.notifications.NotificationManager.this.stackCurrentNotification != null && com.navdy.hud.app.framework.notifications.NotificationManager.this.stackCurrentNotification.notification.supportScroll()) {
                if (pos <= 0) {
                    pos = 1;
                } else if (pos > 100) {
                    pos = 100;
                }
                if (com.navdy.hud.app.framework.notifications.NotificationManager.this.notifScrollIndicator.getCurrentItem() != pos) {
                    if (pos == 1 || pos == 100) {
                        com.navdy.hud.app.framework.notifications.NotificationManager.this.startScrollThresholdTimer();
                    }
                    com.navdy.hud.app.framework.notifications.NotificationManager.this.notifScrollIndicator.setCurrentItem(pos);
                }
            }
        }
    }

    class Anon6 implements java.lang.Runnable {
        final /* synthetic */ java.util.EnumSet val$allowedScreen;
        final /* synthetic */ com.navdy.hud.app.framework.notifications.INotification val$notification;

        Anon6(com.navdy.hud.app.framework.notifications.INotification iNotification, java.util.EnumSet enumSet) {
            this.val$notification = iNotification;
            this.val$allowedScreen = enumSet;
        }

        public void run() {
            com.navdy.hud.app.framework.notifications.NotificationManager.this.addNotificationInternal(this.val$notification, this.val$allowedScreen);
        }
    }

    class Anon7 implements java.lang.Runnable {
        final /* synthetic */ java.lang.String val$id;

        Anon7(java.lang.String str) {
            this.val$id = str;
        }

        public void run() {
            com.navdy.hud.app.framework.notifications.NotificationManager.this.removeNotification(this.val$id, true);
        }
    }

    class Anon8 implements java.lang.Runnable {
        Anon8() {
        }

        public void run() {
            com.navdy.hud.app.framework.notifications.NotificationManager.sLogger.v("hideNotification-anim:screen_back");
            com.navdy.hud.app.framework.notifications.NotificationManager.this.bus.post(new com.navdy.hud.app.event.ShowScreenWithArgs(com.navdy.service.library.events.ui.Screen.SCREEN_BACK, null, null, true));
        }
    }

    class Anon9 implements java.lang.Runnable {
        final /* synthetic */ int val$color;
        final /* synthetic */ android.view.View val$expandViewOut;
        final /* synthetic */ int val$item;
        final /* synthetic */ com.navdy.hud.app.framework.notifications.NotificationManager.Info val$notifInfo;

        Anon9(int i, int i2, com.navdy.hud.app.framework.notifications.NotificationManager.Info info, android.view.View view) {
            this.val$item = i;
            this.val$color = i2;
            this.val$notifInfo = info;
            this.val$expandViewOut = view;
        }

        public void run() {
            com.navdy.hud.app.framework.notifications.INotification iNotification;
            synchronized (com.navdy.hud.app.framework.notifications.NotificationManager.this.lockObj) {
                com.navdy.hud.app.framework.notifications.NotificationManager.this.notifIndicator.setCurrentItem(this.val$item, this.val$color);
                if (com.navdy.hud.app.framework.notifications.NotificationManager.this.expandedWithStack) {
                    if (com.navdy.hud.app.framework.notifications.NotificationManager.this.stackCurrentNotification != null && com.navdy.hud.app.framework.notifications.NotificationManager.this.stackCurrentNotification.startCalled) {
                        com.navdy.hud.app.framework.notifications.NotificationManager.this.stackCurrentNotification.startCalled = false;
                        com.navdy.hud.app.framework.notifications.NotificationManager.this.stackCurrentNotification.notification.onStop();
                    }
                    if (this.val$notifInfo == null) {
                        com.navdy.hud.app.framework.notifications.NotificationManager.this.lastStackCurrentNotification = com.navdy.hud.app.framework.notifications.NotificationManager.this.stackCurrentNotification;
                    }
                    com.navdy.hud.app.framework.notifications.NotificationManager.this.stackCurrentNotification = this.val$notifInfo;
                    if (this.val$expandViewOut != null && com.navdy.hud.app.framework.glance.GlanceHelper.isDeleteAllView(this.val$expandViewOut)) {
                        com.navdy.hud.app.framework.notifications.NotificationManager.this.expandedNotifView.removeView(this.val$expandViewOut);
                        com.navdy.hud.app.framework.notifications.NotificationManager.sLogger.v("delete all big view removed");
                        this.val$expandViewOut.setTag(null);
                        com.navdy.hud.app.framework.glance.GlanceViewCache.putView(com.navdy.hud.app.framework.glance.GlanceViewCache.ViewType.BIG_TEXT, this.val$expandViewOut);
                    }
                    if (com.navdy.hud.app.framework.notifications.NotificationManager.sLogger.isLoggable(2)) {
                        com.navdy.hud.app.framework.notifications.NotificationManager.this.printPriorityMap();
                    }
                    if (com.navdy.hud.app.framework.notifications.NotificationManager.this.stackCurrentNotification != null) {
                        com.navdy.hud.app.framework.notifications.NotificationManager.this.stackCurrentNotification.notification.onExpandedNotificationSwitched();
                    }
                    com.navdy.hud.app.framework.notifications.NotificationManager.this.displayScrollingIndicator(this.val$item);
                }
                com.navdy.hud.app.framework.notifications.NotificationManager.this.runQueuedOperation();
            }
            java.lang.String str = com.navdy.hud.app.analytics.AnalyticsSupport.ANALYTICS_EVENT_GLANCE_ADVANCE;
            if (com.navdy.hud.app.framework.notifications.NotificationManager.this.stackCurrentNotification != null) {
                iNotification = com.navdy.hud.app.framework.notifications.NotificationManager.this.stackCurrentNotification.notification;
            } else {
                iNotification = null;
            }
            com.navdy.hud.app.analytics.AnalyticsSupport.recordGlanceAction(str, iNotification, null);
        }
    }

    static class Info {
        com.navdy.hud.app.framework.notifications.INotification notification;
        int priority;
        boolean pushBackDuetoHighPriority;
        boolean removed;
        boolean resurrected;
        boolean startCalled;
        long uniqueCounter;

        Info() {
        }
    }

    public static class NotificationChange {
    }

    private enum ScrollState {
        NONE,
        HANDLED,
        NOT_HANDLED,
        LIMIT_REACHED
    }

    public static com.navdy.hud.app.framework.notifications.NotificationManager getInstance() {
        return sInstance;
    }

    private NotificationManager() {
        mortar.Mortar.inject(com.navdy.hud.app.HudApplication.getAppContext(), this);
        this.uiStateManager.addNotificationAnimationListener(this.notifAnimationListener);
        this.NOTIFS_REMOVED_ON_DISCONNECT.add(com.navdy.hud.app.framework.notifications.NotificationId.PHONE_CALL_NOTIFICATION_ID);
        this.NOTIFS_REMOVED_ON_DISCONNECT.add(com.navdy.hud.app.framework.notifications.NotificationId.MUSIC_NOTIFICATION_ID);
        this.NOTIFS_NOT_ALLOWED_ON_DISCONNECT.add(com.navdy.hud.app.framework.notifications.NotificationId.PHONE_CALL_NOTIFICATION_ID);
        this.NOTIFS_NOT_ALLOWED_ON_DISCONNECT.add(com.navdy.hud.app.framework.notifications.NotificationId.MUSIC_NOTIFICATION_ID);
        this.animationTranslation = (int) com.navdy.hud.app.HudApplication.getAppContext().getResources().getDimension(com.navdy.hud.app.R.dimen.glance_anim_translation);
        this.bus.register(this);
    }

    public void addNotification(com.navdy.hud.app.framework.notifications.INotification notification) {
        addNotification(notification, null);
    }

    public void addNotification(com.navdy.hud.app.framework.notifications.INotification notification, java.util.EnumSet<com.navdy.service.library.events.ui.Screen> allowedScreen) {
        if (notification != null) {
            if (this.uiStateManager.getRootScreen() == null) {
                sLogger.v("notification not accepted, Main screen not on [" + notification.getId() + "]");
            } else if (this.disconnected && this.NOTIFS_NOT_ALLOWED_ON_DISCONNECT.contains(notification.getId())) {
                sLogger.i("notification not allowed during disconnect[" + notification.getId() + "]");
            } else if (android.os.Looper.getMainLooper() != android.os.Looper.myLooper()) {
                this.handler.post(new com.navdy.hud.app.framework.notifications.NotificationManager.Anon6(notification, allowedScreen));
            } else {
                addNotificationInternal(notification, allowedScreen);
            }
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:150:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:154:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:155:?, code lost:
        return;
     */
    public void addNotificationInternal(com.navdy.hud.app.framework.notifications.INotification notification, java.util.EnumSet<com.navdy.service.library.events.ui.Screen> allowedScreen) {
        if (this.mainScreen == null) {
            getNotificationView();
            if (this.mainScreen == null) {
                return;
            }
        }
        java.lang.String id = notification.getId();
        if (android.text.TextUtils.isEmpty(id)) {
            sLogger.w("notification id is null");
            return;
        }
        com.navdy.hud.app.framework.notifications.NotificationType type = notification.getType();
        if (type == null) {
            sLogger.w("notification type is null");
            return;
        }
        sLogger.v("addNotif type[" + notification.getType() + "] id[" + id + "]");
        synchronized (this.lockObj) {
            if (this.currentNotification != null && !isNotificationViewShowing()) {
                sLogger.v("addNotif current-notification marked null:" + (this.currentNotification.notification != null ? this.currentNotification.notification.getType() : "unk"));
                this.currentNotification = null;
            }
            if (this.currentNotification == null || notification.canAddToStackIfCurrentExists()) {
                if (allowedScreen != null) {
                    if (this.currentNotification != null) {
                        sLogger.v("addNotif screen constraint failed, current notification active");
                        return;
                    }
                    com.navdy.hud.app.screen.BaseScreen currentScreen = this.uiStateManager.getCurrentScreen();
                    if (currentScreen == null || !allowedScreen.contains(currentScreen.getScreen())) {
                        sLogger.v("addNotif screen constraint failed, current screen:" + currentScreen);
                        return;
                    } else if (this.mainScreen.isNotificationExpanding() || this.mainScreen.isNotificationCollapsing() || this.mainScreen.isNotificationViewShowing() || this.mainScreen.isScreenAnimating()) {
                        sLogger.v("addNotif screen constraint failed, mainscreen animating");
                        return;
                    }
                }
                if (this.currentNotification == null || !android.text.TextUtils.equals(this.currentNotification.notification.getId(), notification.getId())) {
                    com.navdy.hud.app.framework.notifications.NotificationManager.Info info = (com.navdy.hud.app.framework.notifications.NotificationManager.Info) this.notificationIdMap.get(id);
                    if (info == null) {
                        com.navdy.hud.app.framework.notifications.NotificationManager.Info info2 = new com.navdy.hud.app.framework.notifications.NotificationManager.Info();
                        info2.notification = notification;
                        info2.priority = type.getPriority();
                        long j = this.notificationCounter + 1;
                        this.notificationCounter = j;
                        info2.uniqueCounter = j;
                        this.notificationIdMap.put(id, info2);
                        this.notificationPriorityMap.put(info2, info2);
                        sLogger.v("addNotif added to stack");
                        if (this.currentNotification == null) {
                            sLogger.v("addNotif make current [" + id + "]");
                            this.currentNotification = info2;
                            if (isExpanded()) {
                                this.currentNotification.pushBackDuetoHighPriority = true;
                                hideNotification();
                            } else if (!showNotification()) {
                                sLogger.v("addNotif not making current, disabled");
                                this.currentNotification.pushBackDuetoHighPriority = true;
                                this.currentNotification = null;
                            }
                        } else {
                            boolean highPriority = info2.priority > this.currentNotification.priority;
                            boolean purgeable = this.currentNotification.notification.isPurgeable();
                            if (highPriority || purgeable) {
                                sLogger.v("addNotif high priority[" + highPriority + "] purgeable[" + purgeable + "]");
                                if (this.stagedNotification == null) {
                                    if (!this.currentNotification.notification.isAlive()) {
                                        this.currentNotification.removed = true;
                                        sLogger.v("addNotif current notification not alive anymore [" + this.currentNotification.notification.getType() + "]");
                                    } else {
                                        this.currentNotification.pushBackDuetoHighPriority = true;
                                    }
                                    this.stagedNotification = info2;
                                    if (this.isAnimating) {
                                        sLogger.v("addNotif anim in progress");
                                    } else if (!this.isExpanded) {
                                        if (!this.currentNotification.startCalled) {
                                            sLogger.v("addNotif swapping notif");
                                            this.stagedNotification = null;
                                            this.currentNotification = info2;
                                        } else {
                                            sLogger.v("addNotif showNotif");
                                        }
                                        showNotification();
                                    } else {
                                        sLogger.v("addNotif hideNotif");
                                        hideNotification();
                                    }
                                } else if (android.text.TextUtils.equals(this.stagedNotification.notification.getId(), info2.notification.getId())) {
                                    sLogger.v("addNotif stage notif already added");
                                    return;
                                } else if (info2.priority > this.stagedNotification.priority) {
                                    sLogger.v("addNotif replacing staged notif since it high priority than staged [" + this.stagedNotification.notification.getId() + "]");
                                    this.stagedNotification.pushBackDuetoHighPriority = true;
                                    this.stagedNotification = info2;
                                    if (this.isAnimating) {
                                        sLogger.v("addNotif anim in progress");
                                    } else if (!this.isExpanded) {
                                        sLogger.v("addNotif-1 showNotif");
                                        showNotification();
                                    } else {
                                        sLogger.v("addNotif-1 hideNotif");
                                        hideNotification();
                                        return;
                                    }
                                } else {
                                    sLogger.v("addNotif not replacing staged notif staged [" + this.stagedNotification.notification.getId() + "]");
                                    return;
                                }
                            } else {
                                sLogger.v("addNotif lower priority than current[" + this.currentNotification.notification.getType() + "] update color");
                                info2.pushBackDuetoHighPriority = true;
                                setNotificationColor();
                                if (!this.isAnimating && !this.isExpanded && !isNotificationViewShowing()) {
                                    sLogger.v("add notif lower priority, but view hidden");
                                    showNotification();
                                }
                            }
                        }
                        postNotificationChange();
                        updateExpandedIndicator();
                    } else if (this.currentNotification == null) {
                        sLogger.v("addNotif already exist on stack, making it current");
                        this.currentNotification = info;
                        showNotification();
                    } else {
                        sLogger.v("addNotif already exist on stack, no-op");
                    }
                } else if (this.currentNotification.removed) {
                    sLogger.v("addNotif notif was marked to remove,resurrecting it back");
                    this.currentNotification.removed = false;
                    this.currentNotification.resurrected = true;
                } else if (this.currentNotification.startCalled) {
                    sLogger.v("addNotif already on screen,update");
                    notification.onUpdate();
                } else {
                    sLogger.v("addNotif not on screen yet");
                    if (!this.mainScreen.isNotificationViewShowing()) {
                        showNotification();
                    }
                }
            } else if (!android.text.TextUtils.equals(this.currentNotification.notification.getId(), notification.getId())) {
                sLogger.v("addNotif cannot be added current rule-2");
            } else if (this.currentNotification.startCalled) {
                sLogger.v("addNotif updated");
                this.currentNotification.notification.onUpdate();
            } else {
                sLogger.v("addNotif cannot be added current rule-1");
            }
        }
    }

    public void addPendingNotification(com.navdy.hud.app.framework.notifications.INotification notification) {
        this.pendingNotification = notification;
        sLogger.v("addPendingNotification:" + this.pendingNotification);
    }

    public void removeNotification(java.lang.String id) {
        if (android.os.Looper.getMainLooper() != android.os.Looper.myLooper()) {
            this.handler.post(new com.navdy.hud.app.framework.notifications.NotificationManager.Anon7(id));
        } else {
            removeNotification(id, true);
        }
    }

    public void removeNotification(java.lang.String id, boolean remove) {
        removeNotification(id, remove, null, null, null);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:48:?, code lost:
        return;
     */
    public void removeNotification(java.lang.String id, boolean remove, com.navdy.service.library.events.ui.Screen screen, android.os.Bundle screenArgs, java.lang.Object screenArgs2) {
        if (!android.text.TextUtils.isEmpty(id)) {
            synchronized (this.lockObj) {
                this.pendingScreen = screen;
                this.pendingScreenArgs = screenArgs;
                this.pendingScreenArgs2 = screenArgs2;
                com.navdy.hud.app.framework.notifications.NotificationManager.Info info = (com.navdy.hud.app.framework.notifications.NotificationManager.Info) this.notificationIdMap.get(id);
                if (info != null) {
                    if (!this.notificationController.isExpandedWithStack() || com.navdy.hud.app.framework.notifications.NotificationHelper.isNotificationRemovable(id)) {
                        info.removed = remove;
                        if (info.removed && info.resurrected) {
                            sLogger.v("removeNotification [" + id + "] resurrected:false");
                            info.resurrected = false;
                        }
                        if (!info.removed && !info.notification.isAlive()) {
                            sLogger.v("removeNotification [" + id + "] is not alive");
                            info.removed = true;
                        }
                        if (!info.removed) {
                            sLogger.v("removeNotification [" + id + "] is still alive");
                        }
                        if (this.currentNotification == null || !android.text.TextUtils.equals(this.currentNotification.notification.getId(), info.notification.getId())) {
                            sLogger.v("bkgnd notification being removed");
                            removeNotificationfromStack(info, false);
                            setNotificationColor();
                        } else if (this.isExpanded && !this.isAnimating) {
                            hideNotification();
                        }
                    } else {
                        sLogger.v("removeNotification, handleRemoveNotificationInExpandedMode");
                        info.removed = true;
                        handleRemoveNotificationInExpandedMode(id, true);
                    }
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void removeNotificationfromStack(com.navdy.hud.app.framework.notifications.NotificationManager.Info info, boolean internal) {
        if (info != null) {
            if (this.notificationIdMap.remove(info.notification.getId()) == null) {
                sLogger.v("removeNotification, not found in id map");
            }
            if (this.notificationPriorityMap.remove(info) == null) {
                sLogger.v("removeNotification, not found in priority map");
            }
            if (info.startCalled) {
                info.startCalled = false;
                info.notification.onStop();
            }
            java.lang.String stagedId = null;
            if (this.stagedNotification != null) {
                stagedId = this.stagedNotification.notification.getId();
                if (android.text.TextUtils.equals(this.stagedNotification.notification.getId(), info.notification.getId())) {
                    this.stagedNotification = null;
                    sLogger.v("cancelling staged notification, getting removed");
                }
            }
            postNotificationChange();
            sLogger.v("removed notification from stack [" + info.notification.getId() + "] staged[" + stagedId + "]");
            if (!internal) {
                updateExpandedIndicator();
            }
        }
    }

    public void currentNotificationTimeout() {
        if (this.currentNotification != null) {
            sLogger.v("currentNotificationTimeout");
            com.navdy.hud.app.analytics.AnalyticsSupport.recordGlanceAction(com.navdy.hud.app.analytics.AnalyticsSupport.ANALYTICS_EVENT_GLANCE_DISMISS, this.currentNotification.notification, "timeout");
            removeNotification(this.currentNotification.notification.getId(), false);
        }
    }

    public com.navdy.hud.app.framework.notifications.INotification getCurrentNotification() {
        if (this.currentNotification == null) {
            return null;
        }
        return this.currentNotification.notification;
    }

    public com.navdy.hud.app.framework.notifications.INotification getNotification(java.lang.String id) {
        com.navdy.hud.app.framework.notifications.INotification iNotification = null;
        if (!android.text.TextUtils.isEmpty(id)) {
            synchronized (this.lockObj) {
                com.navdy.hud.app.framework.notifications.NotificationManager.Info info = (com.navdy.hud.app.framework.notifications.NotificationManager.Info) this.notificationIdMap.get(id);
                if (info != null) {
                    iNotification = info.notification;
                }
            }
        }
        return iNotification;
    }

    public int getNotificationCount() {
        int size;
        synchronized (this.lockObj) {
            size = this.notificationIdMap.size();
        }
        return size;
    }

    public boolean isNotificationPresent(java.lang.String id) {
        boolean containsKey;
        synchronized (this.lockObj) {
            containsKey = this.notificationIdMap.containsKey(id);
        }
        return containsKey;
    }

    public boolean showNotification() {
        if (!this.enable) {
            if (!this.enablePhoneCalls || !isCurrentNotificationId(com.navdy.hud.app.framework.notifications.NotificationId.PHONE_CALL_NOTIFICATION_ID)) {
                sLogger.v("notification not enabled don't show notification");
                return false;
            }
            sLogger.v("notification only phone-notif allowed");
        }
        if (this.uiStateManager.isWelcomeScreenOn()) {
            sLogger.v("welcome screen on, don't show notification");
            return false;
        }
        showNotification(false);
        return true;
    }

    /* access modifiers changed from: private */
    public void showNotification(boolean ignoreAnimation) {
        sLogger.v("show notification isExpanded:" + this.isExpanded + " anim:" + this.isAnimating + " ignore:" + ignoreAnimation);
        if (!this.isExpanded && !this.isAnimating) {
            this.bus.post(new com.navdy.hud.app.event.ShowScreenWithArgs(com.navdy.service.library.events.ui.Screen.SCREEN_NOTIFICATION, null, null, ignoreAnimation));
        }
    }

    public void hideNotification() {
        if (this.isExpanded && !this.isAnimating) {
            if (isExpanded()) {
                sLogger.v("hideNotification:expand hide");
                animateOutExpandedView(true, this.currentNotification, false);
                return;
            }
            hideNotificationInternal();
        }
    }

    /* access modifiers changed from: private */
    public void hideNotificationInternal() {
        com.navdy.hud.app.view.NotificationView view = getNotificationView();
        if (view.border.isVisible()) {
            sLogger.v("hideNotification:border visible");
            view.border.stopTimeout(true, new com.navdy.hud.app.framework.notifications.NotificationManager.Anon8());
            return;
        }
        sLogger.v("hideNotification:screen_back");
        this.bus.post(new com.navdy.hud.app.event.ShowScreenWithArgs(com.navdy.service.library.events.ui.Screen.SCREEN_BACK, null, null, true));
    }

    public boolean isCurrentNotificationId(java.lang.String id) {
        if (this.currentNotification != null && android.text.TextUtils.equals(this.currentNotification.notification.getId(), id)) {
            return true;
        }
        return false;
    }

    private com.navdy.hud.app.ui.activity.Main getMainScreen() {
        if (this.mainScreen == null) {
            this.mainScreen = this.uiStateManager.getRootScreen();
        }
        return this.mainScreen;
    }

    /* access modifiers changed from: private */
    public com.navdy.hud.app.view.NotificationView getNotificationView() {
        if (this.notifView != null) {
            return this.notifView;
        }
        if (this.mainScreen == null) {
            getMainScreen();
        }
        if (this.mainScreen != null) {
            this.notifView = this.mainScreen.getNotificationView();
        }
        return this.notifView;
    }

    /* access modifiers changed from: private */
    public android.widget.FrameLayout getExpandedNotificationView() {
        if (this.expandedNotifView != null) {
            return this.expandedNotifView;
        }
        if (this.mainScreen == null) {
            getMainScreen();
        }
        if (this.mainScreen != null) {
            this.expandedNotifView = this.mainScreen.getExpandedNotificationView();
        }
        return this.expandedNotifView;
    }

    /* access modifiers changed from: private */
    public android.view.View getExpandedNotificationCoverView() {
        if (this.expandedNotifCoverView != null) {
            return this.expandedNotifCoverView;
        }
        if (this.mainScreen == null) {
            getMainScreen();
        }
        if (this.mainScreen != null) {
            this.expandedNotifCoverView = this.mainScreen.getExpandedNotificationCoverView();
        }
        return this.expandedNotifCoverView;
    }

    /* access modifiers changed from: private */
    public com.navdy.hud.app.ui.component.carousel.CarouselIndicator getNotificationIndicator() {
        if (this.notifIndicator != null) {
            return this.notifIndicator;
        }
        if (this.mainScreen == null) {
            getMainScreen();
        }
        if (this.mainScreen != null) {
            this.notifIndicator = this.mainScreen.getNotificationIndicator();
            this.notifScrollIndicator = this.mainScreen.getNotificationScrollIndicator();
        }
        return this.notifIndicator;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:38:?, code lost:
        return true;
     */
    public boolean makeNotificationCurrent(boolean makeAllVisible) {
        synchronized (this.lockObj) {
            if (this.currentNotification != null) {
                if (this.notificationPriorityMap.size() == 0) {
                    if (this.currentNotification.startCalled) {
                        this.currentNotification.notification.onStop();
                    }
                    this.currentNotification = null;
                    return false;
                }
                com.navdy.hud.app.framework.notifications.NotificationManager.Info highest = (com.navdy.hud.app.framework.notifications.NotificationManager.Info) this.notificationPriorityMap.lastKey();
                if (!(highest == null || highest == this.currentNotification)) {
                    sLogger.v("mknc:current[" + this.currentNotification.notification.getId() + "] is not highest[" + highest.notification.getId() + "]");
                    if (this.currentNotification.startCalled) {
                        this.currentNotification.startCalled = false;
                        this.currentNotification.notification.onStop();
                        sLogger.v("mknc: stop called");
                    }
                    this.currentNotification = highest;
                }
            } else if (this.notificationPriorityMap.size() <= 0) {
                return false;
            } else {
                if (makeAllVisible) {
                    sLogger.v("make all visible");
                    for (com.navdy.hud.app.framework.notifications.NotificationManager.Info info : this.notificationPriorityMap.keySet()) {
                        info.pushBackDuetoHighPriority = true;
                    }
                }
                this.currentNotification = (com.navdy.hud.app.framework.notifications.NotificationManager.Info) this.notificationPriorityMap.lastKey();
                return true;
            }
        }
    }

    public void setNotificationColor() {
        com.navdy.hud.app.view.NotificationView notificationView = getNotificationView();
        if (notificationView != null) {
            if (this.currentNotification != null) {
                synchronized (this.lockObj) {
                    com.navdy.hud.app.framework.notifications.NotificationManager.Info info = getLowerNotification(this.currentNotification);
                    if (info == null) {
                        notificationView.resetNextNotificationColor();
                    } else {
                        notificationView.setNextNotificationColor(info.notification.getColor());
                    }
                }
                return;
            }
            notificationView.resetNextNotificationColor();
        }
    }

    public int getNotificationColor() {
        int i = -1;
        synchronized (this.lockObj) {
            if (this.notificationPriorityMap.size() > 0) {
                com.navdy.hud.app.framework.notifications.NotificationManager.Info info = (com.navdy.hud.app.framework.notifications.NotificationManager.Info) this.notificationPriorityMap.lastKey();
                if (!info.removed) {
                    i = info.notification.getColor();
                }
            }
        }
        return i;
    }

    public void handleDisconnect(boolean linkLost) {
        if (!this.disconnected) {
            sLogger.v("client disconnected: save stack :" + linkLost);
            this.disconnected = true;
            saveStack();
            if (isExpanded() || isExpandedNotificationVisible()) {
                collapseExpandedNotification(true, true);
            } else {
                collapseNotification();
            }
            com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getCallManager().disconnect();
        }
    }

    public void handleConnect() {
        sLogger.v("client connected: restore stack");
        this.disconnected = false;
        restoreStack();
        this.notificationSavedListDeviceId = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getDeviceId();
    }

    public void showHideToast(boolean connect) {
        com.navdy.hud.app.framework.toast.ToastManager toastManager = com.navdy.hud.app.framework.toast.ToastManager.getInstance();
        if (this.uiStateManager.isWelcomeScreenOn()) {
            sLogger.v("welcome screen on, no disconnect notification");
            hideConnectionToast(connect);
        } else if (!connect) {
            if (!toastManager.isCurrentToast(com.navdy.hud.app.framework.connection.ConnectionNotification.DISCONNECT_ID)) {
                com.navdy.hud.app.framework.connection.ConnectionNotification.showDisconnectedToast(false);
            }
        } else if (!toastManager.isCurrentToast("connection#toast")) {
            com.navdy.hud.app.framework.connection.ConnectionNotification.showConnectedToast();
        }
    }

    public void hideConnectionToast(boolean connected) {
        com.navdy.hud.app.framework.toast.ToastManager toastManager = com.navdy.hud.app.framework.toast.ToastManager.getInstance();
        if (connected) {
            toastManager.dismissCurrentToast(com.navdy.hud.app.framework.connection.ConnectionNotification.DISCONNECT_ID);
        } else {
            toastManager.dismissCurrentToast("connection#toast");
        }
    }

    private void saveStack() {
        synchronized (this.lockObj) {
            com.navdy.hud.app.framework.toast.ToastManager toastManager = com.navdy.hud.app.framework.toast.ToastManager.getInstance();
            toastManager.dismissCurrentToast(com.navdy.hud.app.framework.phonecall.CallNotification.CALL_NOTIFICATION_TOAST_ID);
            toastManager.clearPendingToast(com.navdy.hud.app.framework.phonecall.CallNotification.CALL_NOTIFICATION_TOAST_ID);
            com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getCallManager().clearCallStack();
            this.notificationSavedList.clear();
            int n = this.notificationIdMap.size();
            sLogger.v("saveStack:" + n);
            if (n > 0) {
                java.util.Iterator<java.lang.String> iterator = this.notificationIdMap.keySet().iterator();
                while (iterator.hasNext()) {
                    java.lang.String id = (java.lang.String) iterator.next();
                    com.navdy.hud.app.framework.notifications.NotificationManager.Info info = (com.navdy.hud.app.framework.notifications.NotificationManager.Info) this.notificationIdMap.get(id);
                    if (this.NOTIFS_REMOVED_ON_DISCONNECT.contains(id)) {
                        this.notificationPriorityMap.remove(info);
                        iterator.remove();
                        sLogger.v("saveStack removed:" + id);
                    } else if (com.navdy.hud.app.framework.notifications.NotificationType.GLANCE == info.notification.getType()) {
                        sLogger.v("saveStack saved:" + id);
                        this.notificationSavedList.add(info);
                        iterator.remove();
                        this.notificationPriorityMap.remove(info);
                    } else {
                        sLogger.v("saveStack left:" + id);
                    }
                }
            }
            this.currentNotification = null;
            this.stagedNotification = null;
            com.navdy.hud.app.framework.glance.GlanceHandler.getInstance().saveState();
        }
        this.uiStateManager.enableNotificationColor(true);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x00c1, code lost:
        r9.uiStateManager.enableNotificationColor(true);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:?, code lost:
        return;
     */
    private void restoreStack() {
        synchronized (this.lockObj) {
            int n = this.notificationSavedList.size();
            sLogger.v("restoreStack:" + n);
            if (n > 0) {
                com.navdy.service.library.device.NavdyDeviceId deviceId = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getDeviceId();
                if (deviceId == null) {
                    sLogger.w("restoreStack no device id");
                    this.notificationSavedList.clear();
                } else if (!deviceId.equals(this.notificationSavedListDeviceId)) {
                    sLogger.w("restoreStack device id does not match current[" + deviceId + "] saved[" + this.notificationSavedListDeviceId + "]");
                    this.notificationSavedList.clear();
                    com.navdy.hud.app.framework.glance.GlanceHandler.getInstance().clearState();
                } else {
                    java.util.Iterator it = this.notificationSavedList.iterator();
                    while (it.hasNext()) {
                        com.navdy.hud.app.framework.notifications.NotificationManager.Info info = (com.navdy.hud.app.framework.notifications.NotificationManager.Info) it.next();
                        java.lang.String id = info.notification.getId();
                        sLogger.w("restoreStack restored:" + id);
                        this.notificationIdMap.put(id, info);
                        this.notificationPriorityMap.put(info, info);
                    }
                    com.navdy.hud.app.framework.glance.GlanceHandler.getInstance().restoreState();
                }
            }
        }
    }

    public boolean isExpandedNotificationVisible() {
        android.view.View view = getExpandedNotificationView();
        if (view == null || view.getVisibility() != 0) {
            return false;
        }
        return true;
    }

    public boolean isExpanded() {
        return this.expandedWithStack;
    }

    private void animateOutExpandedView(boolean hideNotification, boolean quick) {
        animateOutExpandedView(hideNotification, this.currentNotification, quick);
    }

    private void animateOutExpandedView(boolean hideNotification, com.navdy.hud.app.framework.notifications.NotificationManager.Info notificationObj, boolean quick) {
        if (isExpanded() || isExpandedNotificationVisible()) {
            runOperation(com.navdy.hud.app.framework.notifications.NotificationAnimator.Operation.COLLAPSE_VIEW, false, hideNotification, quick, false, null, false);
        }
    }

    public boolean collapseNotification() {
        if (this.mainScreen == null) {
            getNotificationView();
            if (this.mainScreen == null) {
                return false;
            }
        }
        if (!this.mainScreen.isNotificationViewShowing() || this.mainScreen.isNotificationCollapsing() || this.mainScreen.isNotificationExpanding()) {
            return false;
        }
        hideNotification();
        return true;
    }

    public boolean expandNotification() {
        if (this.mainScreen == null) {
            getNotificationView();
            if (this.mainScreen == null) {
                return false;
            }
        }
        if (this.mainScreen.isNotificationViewShowing() || this.mainScreen.isNotificationExpanding() || !makeNotificationCurrent(true)) {
            return false;
        }
        showNotification();
        return true;
    }

    public boolean isNotificationViewShowing() {
        if (this.mainScreen == null) {
            getNotificationView();
            if (this.mainScreen == null) {
                return false;
            }
        }
        if (this.mainScreen.isNotificationViewShowing() || this.mainScreen.isNotificationExpanding()) {
            return true;
        }
        return false;
    }

    public java.lang.String getTopNotificationId() {
        java.lang.String str;
        synchronized (this.lockObj) {
            if (this.currentNotification != null) {
                str = this.currentNotification.notification.getId();
            } else if (this.notificationPriorityMap.size() > 0) {
                str = ((com.navdy.hud.app.framework.notifications.NotificationManager.Info) this.notificationPriorityMap.lastKey()).notification.getId();
            } else {
                str = null;
            }
        }
        return str;
    }

    public void collapseExpandedNotification(boolean completely, boolean quickly) {
        this.notificationController.collapseNotification(completely, quickly);
    }

    public android.view.View getExpandedViewChild() {
        android.widget.FrameLayout lyt = getExpandedNotificationView();
        if (lyt == null || lyt.getChildCount() == 0) {
            return null;
        }
        return lyt.getChildAt(0);
    }

    private void updateExpandedIndicator() {
        if (isExpanded() && !this.deleteAllGlances) {
            if (isAnimating()) {
                sLogger.v("updateExpandedIndicator animating no-op");
                return;
            }
            runOperation(com.navdy.hud.app.framework.notifications.NotificationAnimator.Operation.UPDATE_INDICATOR, false, false, false, false, null, false);
        }
    }

    private void updateExpandedIndicatorInternal() {
        android.view.View child;
        android.widget.TextView textView;
        android.view.View view = getExpandedNotificationView();
        if (isExpanded() && view.getTag() != null) {
            int oldCount = this.notifIndicator.getItemCount();
            int currentItem = this.notifIndicator.getCurrentItem();
            boolean onDeleteIndex = isDeleteAllIndex(currentItem, oldCount);
            int currentCount = getNotificationCount();
            if (isPhoneNotifPresentAndNotAlive()) {
                currentCount--;
            }
            int n = currentCount + 2;
            int color = com.navdy.hud.app.framework.glance.GlanceConstants.colorDeleteAll;
            if (currentItem >= n) {
                currentItem = n - 1;
            }
            if (this.stackCurrentNotification != null) {
                currentItem = getNotificationIndex(this.stackCurrentNotification);
                if (currentItem != -1) {
                    color = this.stackCurrentNotification.notification.getColor();
                    currentItem++;
                }
            }
            if (isDeleteAllIndex(currentItem, n)) {
                color = com.navdy.hud.app.framework.glance.GlanceConstants.colorDeleteAll;
            } else if (onDeleteIndex) {
                com.navdy.hud.app.framework.notifications.NotificationManager.Info newInfo = getNotificationByIndex(currentCount - 1);
                if (newInfo == null) {
                    color = com.navdy.hud.app.framework.glance.GlanceConstants.colorDeleteAll;
                    currentItem = n - 1;
                } else {
                    this.lastStackCurrentNotification = newInfo;
                    updateExpandedIndicator(n, n - 1, newInfo.notification.getColor());
                    movePrevInternal(false);
                    return;
                }
            }
            updateExpandedIndicator(n, currentItem, color);
            if (this.showOn) {
                child = getExpandedViewChild();
            } else {
                if (this.notifView == null) {
                    getNotificationView();
                }
                child = this.notifView.getCurrentNotificationViewChild();
            }
            if (com.navdy.hud.app.framework.glance.GlanceHelper.isDeleteAllView(child)) {
                if (this.showOn) {
                    textView = (android.widget.TextView) child.findViewById(com.navdy.hud.app.R.id.textView);
                } else {
                    textView = (android.widget.TextView) child.findViewById(com.navdy.hud.app.R.id.subTitle);
                }
                updateDeleteAllGlanceCount(textView, child);
            }
            runQueuedOperation();
        }
    }

    public void updateExpandedIndicator(int count, int currentItem, int color) {
        this.notifIndicator.setItemCount(count);
        this.notifIndicator.setCurrentItem(currentItem, color);
        displayScrollingIndicator(currentItem);
        sLogger.v("updateExpandedIndicator count =" + count + " currentitem=" + currentItem);
    }

    public int getExpandedIndicatorCurrentItem() {
        return this.notifIndicator.getCurrentItem();
    }

    public int getExpandedIndicatorCount() {
        return this.notifIndicator.getItemCount();
    }

    public android.content.Context getUIContext() {
        return getNotificationView().getContext();
    }

    public boolean handleKey(com.navdy.hud.app.manager.InputManager.CustomKeyEvent event) {
        boolean scrollKey;
        getNotificationView();
        sLogger.v("handleKey: running=" + this.operationRunning + " qsize:" + this.operationQueue.size() + " animating:" + this.isAnimating);
        if (this.isAnimating) {
            sLogger.v("handleKey: animating no-op");
            return false;
        }
        if (!this.operationRunning && isExpanded() && this.stackCurrentNotification != null && this.showOn && this.stackCurrentNotification.notification.supportScroll() && this.stackCurrentNotification.startCalled) {
            if (event == com.navdy.hud.app.manager.InputManager.CustomKeyEvent.LEFT || event == com.navdy.hud.app.manager.InputManager.CustomKeyEvent.RIGHT) {
                scrollKey = true;
            } else {
                scrollKey = false;
            }
            if (scrollKey) {
                if (this.stackCurrentNotification.notification.onKey(event)) {
                    this.scrollState = com.navdy.hud.app.framework.notifications.NotificationManager.ScrollState.HANDLED;
                    this.handler.removeCallbacks(this.ignoreScrollRunnable);
                    return true;
                }
                switch (this.scrollState) {
                    case NOT_HANDLED:
                        return true;
                    case HANDLED:
                        startScrollThresholdTimer();
                        return true;
                }
            }
        }
        this.scrollState = com.navdy.hud.app.framework.notifications.NotificationManager.ScrollState.NONE;
        switch (event) {
            case LEFT:
                movePrevious(false);
                return true;
            case RIGHT:
                moveNext(false);
                return true;
            case SELECT:
                executeItem();
                return true;
            case POWER_BUTTON_LONG_PRESS:
                sLogger.v("power button click: show shutdown");
                this.pendingShutdownArgs = com.navdy.hud.app.event.Shutdown.Reason.POWER_BUTTON.asBundle();
                collapseExpandedNotification(true, true);
                return true;
            default:
                return false;
        }
    }

    public void viewSwitchAnimation(android.view.View small, android.view.View large, com.navdy.hud.app.framework.notifications.NotificationManager.Info notifInfo, boolean previous, int color) {
        int pos;
        int i;
        android.view.View expandViewOut = getExpandedViewChild();
        int pos2 = this.notifIndicator.getCurrentItem();
        if (previous) {
            pos = pos2 - 1;
        } else {
            pos = pos2 + 1;
        }
        int item = pos;
        com.navdy.hud.app.framework.notifications.NotificationManager.Anon9 anon9 = new com.navdy.hud.app.framework.notifications.NotificationManager.Anon9(item, color, notifInfo, expandViewOut);
        if (small != null) {
            android.view.View notifViewOut = this.notifView.getCurrentNotificationViewChild();
            android.view.ViewGroup notifViewOutParent = this.notifView.getNotificationContainer();
            if (previous) {
                i = -this.animationTranslation;
            } else {
                i = this.animationTranslation;
            }
            com.navdy.hud.app.framework.notifications.NotificationAnimator.animateNotifViews(small, notifViewOut, notifViewOutParent, i, 250, 250, new com.navdy.hud.app.framework.notifications.NotificationManager.Anon10(notifInfo, notifViewOut, notifViewOutParent, large, anon9));
        }
        if (large != null) {
            com.navdy.hud.app.framework.notifications.NotificationAnimator.animateExpandedViews(large, expandViewOut, this.expandedNotifView, previous ? -this.animationTranslation : this.animationTranslation, 250, 250, anon9);
        }
        if (isExpanded()) {
            hideScrollingIndicator();
            if (this.stackCurrentNotification != null && this.stackCurrentNotification.notification.supportScroll()) {
                ((com.navdy.hud.app.framework.notifications.IScrollEvent) this.stackCurrentNotification.notification).setListener(null);
            }
        }
        android.animation.AnimatorSet indicatorAnimator = this.notifIndicator.getItemMoveAnimator(item, color);
        if (indicatorAnimator != null) {
            indicatorAnimator.start();
        }
    }

    public void printPriorityMap() {
        synchronized (this.lockObj) {
            if (this.stackCurrentNotification == null) {
                sLogger.v("notif-debug-map stack current = null");
            } else {
                sLogger.v("notif-debug-map stack current = " + this.stackCurrentNotification.notification.getId());
            }
            if (this.currentNotification == null) {
                sLogger.v("notif-debug-map current = null");
            } else {
                sLogger.v("notif-debug-map current = " + this.currentNotification.notification.getId());
            }
            sLogger.v("notif-debug-map-start");
            for (com.navdy.hud.app.framework.notifications.NotificationManager.Info info : this.notificationPriorityMap.keySet()) {
                sLogger.v("notif-debug [" + info.notification.getId() + "] priority[" + info.priority + "] counter [" + info.uniqueCounter + "]");
            }
            sLogger.v("notif-debug-map-stop");
        }
    }

    public boolean isCurrentItemDeleteAll() {
        if (this.notifIndicator == null) {
            return false;
        }
        return isDeleteAllIndex(this.notifIndicator.getCurrentItem(), this.notifIndicator.getItemCount());
    }

    private boolean isDeleteAllIndex(int index, int itemCount) {
        if (index == 0 || index == itemCount - 1) {
            return true;
        }
        return false;
    }

    private void deleteAllGlances() {
        synchronized (this.lockObj) {
            if (this.deleteAllGlances) {
                animateOutExpandedView(true, true);
            }
        }
    }

    /* access modifiers changed from: private */
    public void removeAllNotification() {
        this.expandedWithStack = false;
        synchronized (this.lockObj) {
            if (this.currentNotification != null && this.currentNotification.startCalled && com.navdy.hud.app.framework.notifications.NotificationHelper.isNotificationRemovable(this.currentNotification.notification.getId())) {
                sLogger.v("removeAllNotification calling stop: " + this.currentNotification.notification.getId());
                this.currentNotification.startCalled = false;
                this.currentNotification.notification.onStop();
            }
            int n = this.notificationIdMap.size();
            sLogger.v("removeAllNotification:" + n);
            com.navdy.hud.app.framework.notifications.NotificationManager.Info current = null;
            if (n > 0) {
                java.util.Iterator<java.lang.String> iterator = this.notificationIdMap.keySet().iterator();
                while (iterator.hasNext()) {
                    java.lang.String id = (java.lang.String) iterator.next();
                    com.navdy.hud.app.framework.notifications.NotificationManager.Info info = (com.navdy.hud.app.framework.notifications.NotificationManager.Info) this.notificationIdMap.get(id);
                    if (com.navdy.hud.app.framework.notifications.NotificationHelper.isNotificationRemovable(id)) {
                        this.notificationPriorityMap.remove(info);
                        iterator.remove();
                        sLogger.v("removeAllNotification removed:" + id);
                    } else {
                        current = info;
                        sLogger.v("removeAllNotification left:" + id);
                    }
                }
            }
            this.stackCurrentNotification = null;
            this.lastStackCurrentNotification = null;
            this.currentNotification = current;
            postNotificationChange();
        }
    }

    @com.squareup.otto.Subscribe
    public void onShowToast(com.navdy.hud.app.framework.toast.ToastManager.ShowToast event) {
        if (this.deleteAllGlances && android.text.TextUtils.equals(event.name, com.navdy.hud.app.framework.glance.GlanceHelper.DELETE_ALL_GLANCES_TOAST_ID)) {
            sLogger.v("showToast: called deleteAllGlances");
            deleteAllGlances();
        }
    }

    /* access modifiers changed from: private */
    public int getNotificationIndex(com.navdy.hud.app.framework.notifications.NotificationManager.Info info) {
        synchronized (this.lockObj) {
            int index = -1;
            for (java.lang.Object obj : this.notificationPriorityMap.descendingKeySet()) {
                index++;
                if (obj == info) {
                    return index;
                }
            }
            return -1;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
        return null;
     */
    private com.navdy.hud.app.framework.notifications.NotificationManager.Info getNotificationByIndex(int index) {
        synchronized (this.lockObj) {
            if (index >= 0) {
                if (index < this.notificationPriorityMap.size()) {
                    int i = 0;
                    for (com.navdy.hud.app.framework.notifications.NotificationManager.Info info : this.notificationPriorityMap.descendingKeySet()) {
                        if (i == index) {
                            return info;
                        }
                        i++;
                    }
                    return null;
                }
            }
        }
    }

    private void updateDeleteAllGlanceCount(android.widget.TextView textView, android.view.View container) {
        int count = getNotificationCount() - getNonRemovableNotifCount();
        if (count == 0) {
            textView.setText(com.navdy.hud.app.framework.glance.GlanceConstants.glancesCannotDelete);
            container.setTag(com.navdy.hud.app.R.id.glance_can_delete, java.lang.Integer.valueOf(1));
            return;
        }
        if (this.showOn) {
            textView.setText(this.resources.getString(com.navdy.hud.app.R.string.glances_dismiss_all, new java.lang.Object[]{java.lang.Integer.valueOf(count)}));
        } else {
            textView.setText(this.resources.getString(com.navdy.hud.app.R.string.count, new java.lang.Object[]{java.lang.Integer.valueOf(count)}));
        }
        container.setTag(com.navdy.hud.app.R.id.glance_can_delete, null);
    }

    /* access modifiers changed from: private */
    public void cleanupViews(com.navdy.hud.app.view.NotificationView view) {
        sLogger.v("*** cleaning up expanded view");
        this.expandedWithStack = false;
        android.view.ViewGroup parent = getExpandedNotificationView();
        android.view.View child = getExpandedViewChild();
        if (child != null) {
            parent.removeView(child);
            if (com.navdy.hud.app.framework.glance.GlanceHelper.isDeleteAllView(child)) {
                child.setTag(null);
                child.setAlpha(1.0f);
                com.navdy.hud.app.framework.glance.GlanceViewCache.putView(com.navdy.hud.app.framework.glance.GlanceViewCache.ViewType.BIG_TEXT, child);
            }
            sLogger.v("*** removed expanded view");
        }
        android.view.ViewGroup parentNotif = this.notifView.getNotificationContainer();
        if (parentNotif.getChildCount() > 0) {
            android.view.View childNotif = parentNotif.getChildAt(0);
            parentNotif.removeView(childNotif);
            if (com.navdy.hud.app.framework.glance.GlanceHelper.isDeleteAllView(childNotif)) {
                childNotif.setTag(null);
                cleanupView(childNotif);
            }
            sLogger.v("*** removed notif view");
        }
        if (this.stackCurrentNotification != null && this.stackCurrentNotification.startCalled) {
            this.stackCurrentNotification.startCalled = false;
            this.stackCurrentNotification.notification.onStop();
        }
        this.stackCurrentNotification = null;
        this.lastStackCurrentNotification = null;
        parent.setVisibility(8);
        com.navdy.hud.app.ui.component.carousel.CarouselIndicator indicator = getNotificationIndicator();
        if (indicator != null) {
            indicator.setVisibility(8);
            hideScrollingIndicator();
        }
        setNotificationColor();
        view.showNextNotificationColor();
    }

    private com.navdy.hud.app.framework.notifications.NotificationManager.Info getHigherNotification(com.navdy.hud.app.framework.notifications.NotificationManager.Info info) {
        while (true) {
            com.navdy.hud.app.framework.notifications.NotificationManager.Info higher = (com.navdy.hud.app.framework.notifications.NotificationManager.Info) this.notificationPriorityMap.higherKey(info);
            if (higher == null) {
                return null;
            }
            if (!(higher.notification instanceof com.navdy.hud.app.framework.phonecall.CallNotification) || higher.notification.isAlive()) {
                return higher;
            }
            info = higher;
        }
    }

    private com.navdy.hud.app.framework.notifications.NotificationManager.Info getHighestNotification() {
        com.navdy.hud.app.framework.notifications.NotificationManager.Info info;
        synchronized (this.lockObj) {
            info = (com.navdy.hud.app.framework.notifications.NotificationManager.Info) this.notificationPriorityMap.lastKey();
        }
        return info;
    }

    private com.navdy.hud.app.framework.notifications.NotificationManager.Info getLowestNotification() {
        com.navdy.hud.app.framework.notifications.NotificationManager.Info info;
        synchronized (this.lockObj) {
            info = (com.navdy.hud.app.framework.notifications.NotificationManager.Info) this.notificationPriorityMap.firstKey();
        }
        return info;
    }

    private com.navdy.hud.app.framework.notifications.NotificationManager.Info getLowerNotification(com.navdy.hud.app.framework.notifications.NotificationManager.Info info) {
        while (true) {
            com.navdy.hud.app.framework.notifications.NotificationManager.Info lower = (com.navdy.hud.app.framework.notifications.NotificationManager.Info) this.notificationPriorityMap.lowerKey(info);
            if (lower == null) {
                return null;
            }
            if (!(lower.notification instanceof com.navdy.hud.app.framework.phonecall.CallNotification) || lower.notification.isAlive()) {
                return lower;
            }
            info = lower;
        }
    }

    private com.navdy.hud.app.framework.notifications.NotificationManager.Info getNotificationFromId(java.lang.String str) {
        com.navdy.hud.app.framework.notifications.NotificationManager.Info info;
        synchronized (this.lockObj) {
            info = (com.navdy.hud.app.framework.notifications.NotificationManager.Info) this.notificationIdMap.get(str);
        }
        return info;
    }

    /* access modifiers changed from: private */
    public boolean isPhoneNotifPresentAndNotAlive() {
        com.navdy.hud.app.framework.phonecall.CallNotification callNotification = (com.navdy.hud.app.framework.phonecall.CallNotification) getNotification(com.navdy.hud.app.framework.notifications.NotificationId.PHONE_CALL_NOTIFICATION_ID);
        if (callNotification == null || callNotification.isAlive()) {
            return false;
        }
        return true;
    }

    private int getNonRemovableNotifCount() {
        int count = 0;
        if (getNotification(com.navdy.hud.app.framework.notifications.NotificationId.PHONE_CALL_NOTIFICATION_ID) != null) {
            count = 0 + 1;
        }
        if (getNotification(com.navdy.hud.app.framework.notifications.NotificationId.TRAFFIC_REROUTE_NOTIFICATION_ID) != null) {
            return count + 1;
        }
        return count;
    }

    private boolean isNotifCurrent(java.lang.String id) {
        if (!this.notificationController.isExpandedWithStack() || !this.expandedWithStack) {
            if (this.currentNotification == null || !android.text.TextUtils.equals(id, this.currentNotification.notification.getId()) || !this.currentNotification.startCalled) {
                return false;
            }
            return true;
        } else if (this.stackCurrentNotification == null || !android.text.TextUtils.equals(id, this.stackCurrentNotification.notification.getId()) || !this.stackCurrentNotification.startCalled) {
            return false;
        } else {
            return true;
        }
    }

    public void postNotificationChange() {
        this.bus.post(NOTIFICATION_CHANGE);
    }

    @com.squareup.otto.Subscribe
    public void onCallEnded(com.navdy.hud.app.framework.phonecall.CallManager.CallEnded event) {
        if (this.notificationController.isExpandedWithStack()) {
            handleRemoveNotificationInExpandedMode(com.navdy.hud.app.framework.notifications.NotificationId.PHONE_CALL_NOTIFICATION_ID, false);
        } else {
            sLogger.v("phone-end");
        }
    }

    private void handleRemoveNotificationInExpandedMode(java.lang.String id, boolean collapse) {
        if (isExpanded() && !this.deleteAllGlances) {
            runOperation(com.navdy.hud.app.framework.notifications.NotificationAnimator.Operation.REMOVE_NOTIFICATION, false, false, false, false, id, collapse);
        }
    }

    private void handleRemoveNotificationInExpandedModeInternal(java.lang.String id, boolean collapse) {
        java.lang.String id2;
        java.lang.String id3;
        if (isNotifCurrent(id)) {
            sLogger.v("notif-end: is current:" + id);
            if (collapse) {
                collapseNotificationInternal(false, false, true);
            }
        } else if (getNotificationCount() == 1) {
            sLogger.v("remove-end: no more glance");
            collapseExpandedViewInternal(true, true);
        } else {
            sLogger.v("remove-end: not current, remove:" + id);
            removeNotificationfromStack(getNotificationFromId(id), true);
            if (this.lastStackCurrentNotification != null && android.text.TextUtils.equals(this.lastStackCurrentNotification.notification.getId(), id)) {
                if (this.stackCurrentNotification == null) {
                    this.lastStackCurrentNotification = getLowerNotification(this.lastStackCurrentNotification);
                    com.navdy.service.library.log.Logger logger = sLogger;
                    java.lang.StringBuilder append = new java.lang.StringBuilder().append("after remove notif is:");
                    if (this.lastStackCurrentNotification == null) {
                        id3 = com.amazonaws.services.s3.internal.Constants.NULL_VERSION_ID;
                    } else {
                        id3 = this.lastStackCurrentNotification.notification.getId();
                    }
                    logger.v(append.append(id3).toString());
                } else {
                    this.lastStackCurrentNotification = null;
                }
            }
            if (this.currentNotification != null && android.text.TextUtils.equals(this.currentNotification.notification.getId(), id)) {
                this.currentNotification = this.stackCurrentNotification;
                com.navdy.service.library.log.Logger logger2 = sLogger;
                java.lang.StringBuilder append2 = new java.lang.StringBuilder().append("after remove current notif set:");
                if (this.currentNotification == null) {
                    id2 = com.amazonaws.services.s3.internal.Constants.NULL_VERSION_ID;
                } else {
                    id2 = this.currentNotification.notification.getId();
                }
                logger2.v(append2.append(id2).toString());
            }
            postNotificationChange();
            updateExpandedIndicatorInternal();
        }
    }

    /* access modifiers changed from: private */
    public void collapseNotificationInternal(boolean completely, boolean quickly, boolean internal) {
        sLogger.v("collapseNotificationInternal");
        if (getNotificationView() == null) {
            return;
        }
        if (!isExpanded() && !isExpandedNotificationVisible()) {
            return;
        }
        if (!internal) {
            animateOutExpandedView(completely, this.currentNotification, quickly);
        } else {
            collapseExpandedViewInternal(completely, quickly);
        }
    }

    @com.squareup.otto.Subscribe
    public void onCallAccepted(com.navdy.hud.app.framework.phonecall.CallManager.CallAccepted event) {
        sLogger.v("phone-start");
        updateExpandedIndicator();
    }

    public boolean isNotificationMarkedForRemoval(java.lang.String id) {
        boolean z;
        synchronized (this.lockObj) {
            com.navdy.hud.app.framework.notifications.NotificationManager.Info info = getNotificationFromId(id);
            if (info != null) {
                z = info.removed;
            } else {
                z = false;
            }
        }
        return z;
    }

    /* access modifiers changed from: private */
    public void cleanupView(android.view.View view) {
        com.navdy.hud.app.framework.glance.GlanceViewCache.ViewType viewType = (com.navdy.hud.app.framework.glance.GlanceViewCache.ViewType) view.getTag(com.navdy.hud.app.R.id.glance_view_type);
        view.setTag(com.navdy.hud.app.R.id.glance_view_type, null);
        view.setTag(com.navdy.hud.app.R.id.glance_can_delete, null);
        if (viewType != null) {
            switch (viewType) {
                case SMALL_IMAGE:
                    ((android.widget.ImageView) view.findViewById(com.navdy.hud.app.R.id.imageView)).setImageResource(0);
                    com.navdy.hud.app.framework.glance.GlanceViewCache.putView(com.navdy.hud.app.framework.glance.GlanceViewCache.ViewType.SMALL_IMAGE, view);
                    return;
                case SMALL_GLANCE_MESSAGE:
                    android.widget.TextView mainTitle = (android.widget.TextView) view.findViewById(com.navdy.hud.app.R.id.mainTitle);
                    mainTitle.setAlpha(1.0f);
                    mainTitle.setVisibility(0);
                    android.widget.TextView subTitle = (android.widget.TextView) view.findViewById(com.navdy.hud.app.R.id.subTitle);
                    subTitle.setAlpha(1.0f);
                    subTitle.setVisibility(0);
                    android.view.View choiceLayout = view.findViewById(com.navdy.hud.app.R.id.choiceLayout);
                    if (choiceLayout != null) {
                        choiceLayout.setAlpha(1.0f);
                        choiceLayout.setVisibility(0);
                        choiceLayout.setTag(null);
                        choiceLayout.setTag(com.navdy.hud.app.R.id.message_prev_choice, null);
                        choiceLayout.setTag(com.navdy.hud.app.R.id.message_secondary_screen, null);
                    }
                    com.navdy.hud.app.ui.component.image.InitialsImageView mainImage = (com.navdy.hud.app.ui.component.image.InitialsImageView) view.findViewById(com.navdy.hud.app.R.id.mainImage);
                    mainImage.setImage(0, null, com.navdy.hud.app.ui.component.image.InitialsImageView.Style.DEFAULT);
                    mainImage.setTag(null);
                    ((android.widget.ImageView) view.findViewById(com.navdy.hud.app.R.id.sideImage)).setImageResource(0);
                    view.setAlpha(1.0f);
                    com.navdy.hud.app.framework.glance.GlanceViewCache.putView(com.navdy.hud.app.framework.glance.GlanceViewCache.ViewType.SMALL_GLANCE_MESSAGE, view);
                    return;
                default:
                    return;
            }
        }
    }

    public void setExpandedStack(boolean b) {
        this.expandedWithStack = b;
    }

    public void movePrevious(boolean gesture) {
        if (!isExpanded() && !isExpandedNotificationVisible()) {
            return;
        }
        if (!this.deleteAllGlances) {
            runOperation(com.navdy.hud.app.framework.notifications.NotificationAnimator.Operation.MOVE_PREV, false, false, false, gesture, null, false);
            return;
        }
        sLogger.v("movePrevious: no-op deleteGlances set");
    }

    public void moveNext(boolean gesture) {
        if (!isExpanded() && !isExpandedNotificationVisible()) {
            return;
        }
        if (!this.deleteAllGlances) {
            runOperation(com.navdy.hud.app.framework.notifications.NotificationAnimator.Operation.MOVE_NEXT, false, false, false, gesture, null, false);
            return;
        }
        sLogger.v("moveNext: no-op deleteGlances set");
    }

    public void executeItem() {
        if (!isExpanded() && !isExpandedNotificationVisible()) {
            return;
        }
        if (!this.deleteAllGlances) {
            runOperation(com.navdy.hud.app.framework.notifications.NotificationAnimator.Operation.SELECT, false, false, false, false, null, false);
            return;
        }
        sLogger.v("executeItem: no-op deleteGlances set");
    }

    /* access modifiers changed from: private */
    public void runOperation(com.navdy.hud.app.framework.notifications.NotificationAnimator.Operation operation, boolean ignoreCurrent, boolean hideNotification, boolean quick, boolean gesture, java.lang.String id, boolean collapse) {
        if (ignoreCurrent || !this.operationRunning) {
            sLogger.v("run operation=" + operation);
            this.operationRunning = true;
            switch (operation) {
                case MOVE_NEXT:
                    moveNextInternal(gesture);
                    if (this.deleteAllGlances) {
                        com.navdy.hud.app.analytics.AnalyticsSupport.recordGlanceAction(com.navdy.hud.app.analytics.AnalyticsSupport.ANALYTICS_EVENT_GLANCE_DELETE_ALL, null, gesture ? "swipe" : "dial");
                        return;
                    } else {
                        com.navdy.hud.app.analytics.AnalyticsSupport.setGlanceNavigationSource(gesture ? "swipe" : "dial");
                        return;
                    }
                case MOVE_PREV:
                    movePrevInternal(gesture);
                    com.navdy.hud.app.analytics.AnalyticsSupport.setGlanceNavigationSource(gesture ? "swipe" : "dial");
                    return;
                case SELECT:
                    executeItemInternal();
                    if (this.deleteAllGlances) {
                        com.navdy.hud.app.analytics.AnalyticsSupport.recordGlanceAction(com.navdy.hud.app.analytics.AnalyticsSupport.ANALYTICS_EVENT_GLANCE_DELETE_ALL, null, gesture ? "swipe" : "dial");
                        return;
                    }
                    return;
                case COLLAPSE_VIEW:
                    collapseExpandedViewInternal(hideNotification, quick);
                    return;
                case UPDATE_INDICATOR:
                    updateExpandedIndicatorInternal();
                    return;
                case REMOVE_NOTIFICATION:
                    handleRemoveNotificationInExpandedModeInternal(id, collapse);
                    return;
                default:
                    return;
            }
        } else {
            if (this.operationQueue.size() >= 3) {
                switch (operation) {
                    case MOVE_NEXT:
                    case MOVE_PREV:
                        return;
                }
            }
            this.operationQueue.add(new com.navdy.hud.app.framework.notifications.NotificationAnimator.OperationInfo(operation, hideNotification, quick, gesture, id, collapse));
        }
    }

    /* access modifiers changed from: private */
    public void runQueuedOperation() {
        if (this.operationQueue.size() > 0) {
            this.handler.post(new com.navdy.hud.app.framework.notifications.NotificationManager.Anon11((com.navdy.hud.app.framework.notifications.NotificationAnimator.OperationInfo) this.operationQueue.remove()));
            return;
        }
        sLogger.v("operationRunning=false");
        this.operationRunning = false;
    }

    public void clearOperationQueue() {
        sLogger.v("clearOperationQueue:" + this.operationQueue.size());
        this.operationQueue.clear();
        this.operationRunning = false;
    }

    private void moveNextInternal(boolean gesture) {
        boolean verbose = sLogger.isLoggable(2);
        if (verbose) {
            sLogger.v("moveNextInternal:" + gesture);
        }
        if (isExpanded()) {
            handleExpandedMove(com.navdy.hud.app.manager.InputManager.CustomKeyEvent.RIGHT, gesture);
        } else if (isExpandedNotificationVisible()) {
            int current = this.notifIndicator.getCurrentItem();
            if (current == this.notifIndicator.getItemCount() - 1) {
                runQueuedOperation();
                if (verbose) {
                    sLogger.v("moveNextInternal:cannot go next");
                    return;
                }
                return;
            }
            int current2 = current + 1;
            if (verbose) {
                sLogger.v("moveNextInternal:going to " + current2);
            }
            viewSwitchAnimation(null, this.currentNotification.notification.getExpandedView(this.notifView.getContext(), java.lang.Integer.valueOf(current2)), null, false, this.currentNotification.notification.getExpandedViewIndicatorColor());
        }
    }

    private void movePrevInternal(boolean gesture) {
        boolean verbose = sLogger.isLoggable(2);
        if (verbose) {
            sLogger.v("movePrevInternal:" + gesture);
        }
        if (isExpanded()) {
            handleExpandedMove(com.navdy.hud.app.manager.InputManager.CustomKeyEvent.LEFT, gesture);
        } else if (isExpandedNotificationVisible()) {
            int current = this.notifIndicator.getCurrentItem();
            int itemCount = this.notifIndicator.getItemCount();
            if (current == 0) {
                runQueuedOperation();
                if (verbose) {
                    sLogger.v("movePrevInternal: cannot go prev");
                    return;
                }
                return;
            }
            int current2 = current - 1;
            if (verbose) {
                sLogger.v("movePrevInternal: going to " + current2);
            }
            viewSwitchAnimation(null, this.currentNotification.notification.getExpandedView(this.notifView.getContext(), java.lang.Integer.valueOf(current2)), null, true, com.navdy.hud.app.framework.glance.GlanceConstants.colorWhite);
        }
    }

    private void executeItemInternal() {
        android.view.View view;
        boolean verbose = sLogger.isLoggable(2);
        if (isExpanded()) {
            if (this.stackCurrentNotification != null) {
                if (verbose) {
                    sLogger.v("executeItemInternal: expanded :" + this.stackCurrentNotification);
                }
                clearOperationQueue();
                this.stackCurrentNotification.notification.onKey(com.navdy.hud.app.manager.InputManager.CustomKeyEvent.SELECT);
                return;
            }
            if (verbose) {
                sLogger.v("executeItemInternal: expanded check for delete:");
            }
            if (this.showOn) {
                view = getExpandedViewChild();
            } else {
                view = this.notifView.getCurrentNotificationViewChild();
            }
            if (!com.navdy.hud.app.framework.glance.GlanceHelper.isDeleteAllView(view)) {
                return;
            }
            if (view.getTag(com.navdy.hud.app.R.id.glance_can_delete) != null) {
                sLogger.v("executeItemInternal: no glance to delete");
                runQueuedOperation();
                return;
            }
            if (verbose) {
                sLogger.v("executeItemInternal: delete all");
            }
            this.deleteAllGlances = true;
            clearOperationQueue();
            com.navdy.hud.app.framework.glance.GlanceHelper.showGlancesDeletedToast();
            sLogger.v("executeItemInternal: show delete all toast");
        } else if (isExpandedNotificationVisible()) {
            if (verbose) {
                sLogger.v("executeItemInternal: expandedNoStack :" + this.currentNotification);
            }
            if (this.currentNotification != null) {
                clearOperationQueue();
                this.currentNotification.notification.onKey(com.navdy.hud.app.manager.InputManager.CustomKeyEvent.SELECT);
                return;
            }
            runQueuedOperation();
        }
    }

    private void collapseExpandedViewInternal(boolean hideNotification, boolean quick) {
        sLogger.v("collapseExpandedViewInternal");
        this.isAnimating = true;
        clearOperationQueue();
        java.lang.Runnable runnable = new com.navdy.hud.app.framework.notifications.NotificationManager.Anon12(hideNotification);
        android.view.ViewGroup parent = getExpandedNotificationView();
        android.view.View child = getExpandedViewChild();
        java.lang.Runnable expandOutRunnable = new com.navdy.hud.app.framework.notifications.NotificationManager.Anon13(child, parent, runnable);
        if ((this.showOn || isExpandedNotificationVisible()) && child != null) {
            com.navdy.hud.app.framework.notifications.NotificationAnimator.animateChildViewOut(child, quick ? 0 : 250, expandOutRunnable);
        } else {
            expandOutRunnable.run();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:60:0x0147  */
    private void handleExpandedMove(com.navdy.hud.app.manager.InputManager.CustomKeyEvent event, boolean gesture) {
        android.view.View view;
        com.navdy.hud.app.framework.notifications.NotificationManager.Info info;
        android.view.View view2;
        android.view.View s;
        android.widget.ImageView imageView;
        android.view.View l;
        android.view.View l2;
        int c;
        sLogger.v("handleExpandedMove key:" + event + " gesture:" + gesture);
        com.navdy.hud.app.framework.notifications.NotificationManager.Info info2 = null;
        boolean prev = false;
        boolean deleteAll = false;
        boolean runQueuedOperation = false;
        int currentItem = this.notifIndicator.getCurrentItem();
        int indicatorCount = this.notifIndicator.getItemCount();
        try {
            switch (event) {
                case LEFT:
                    if (currentItem == 0) {
                        runQueuedOperation = true;
                        if (sLogger.isLoggable(2)) {
                            sLogger.v("handleExpandedMove: cannot go up");
                        }
                        if (1 != 0) {
                            runQueuedOperation();
                            return;
                        }
                        return;
                    }
                    if (currentItem == 1) {
                        deleteAll = true;
                    } else {
                        synchronized (this.lockObj) {
                            if (this.stackCurrentNotification != null) {
                                info2 = getHigherNotification(this.stackCurrentNotification);
                                if (info2 == null) {
                                    deleteAll = true;
                                    if (sLogger.isLoggable(2)) {
                                        sLogger.v("handleExpandedMove: cannot go up, no notification left");
                                    }
                                }
                            } else {
                                info2 = this.lastStackCurrentNotification;
                                this.lastStackCurrentNotification = null;
                                if (info2 == null) {
                                    sLogger.v("handleExpandedMove: no prev notification");
                                    if (currentItem == indicatorCount - 1) {
                                        info2 = getLowestNotification();
                                        sLogger.v("handleExpandedMove: got lowest:" + info2);
                                    }
                                }
                            }
                        }
                    }
                    prev = true;
                case RIGHT:
                    if (currentItem == indicatorCount - 1) {
                        if (gesture) {
                            if (this.showOn) {
                                view2 = getExpandedViewChild();
                            } else {
                                view2 = this.notifView.getCurrentNotificationViewChild();
                            }
                            if (view2.getTag(com.navdy.hud.app.R.id.glance_can_delete) != null) {
                                sLogger.v("no glance to delete:right");
                                runQueuedOperation = true;
                            } else {
                                this.deleteAllGlances = true;
                                clearOperationQueue();
                                com.navdy.hud.app.framework.glance.GlanceHelper.showGlancesDeletedToast();
                                if (sLogger.isLoggable(2)) {
                                    sLogger.v("handleExpandedMove: delete all glances");
                                }
                            }
                        } else {
                            if (sLogger.isLoggable(2)) {
                                sLogger.v("handleExpandedMove: cannot go down");
                            }
                            runQueuedOperation = true;
                        }
                        if (runQueuedOperation) {
                            runQueuedOperation();
                            return;
                        }
                        return;
                    } else if (currentItem == indicatorCount - 2) {
                        deleteAll = true;
                    } else {
                        synchronized (this.lockObj) {
                            if (this.stackCurrentNotification != null) {
                                info = getLowerNotification(this.stackCurrentNotification);
                            } else {
                                info = this.lastStackCurrentNotification;
                                this.lastStackCurrentNotification = null;
                                if (info == null) {
                                    sLogger.v("handleExpandedMove: no next notification");
                                    if (currentItem == 0) {
                                        info = getHighestNotification();
                                        sLogger.v("handleExpandedMove: got highest:" + info);
                                    }
                                }
                            }
                        }
                    }
                case SELECT:
                    if (this.stackCurrentNotification != null) {
                        this.stackCurrentNotification.notification.onKey(event);
                        if (0 != 0) {
                            runQueuedOperation();
                            return;
                        }
                        return;
                    }
                    if (this.showOn) {
                        view = getExpandedViewChild();
                    } else {
                        view = this.notifView.getCurrentNotificationViewChild();
                    }
                    if (com.navdy.hud.app.framework.glance.GlanceHelper.isDeleteAllView(view)) {
                        if (view.getTag(com.navdy.hud.app.R.id.glance_can_delete) != null) {
                            sLogger.v("no glance to delete:select");
                            if (1 != 0) {
                                runQueuedOperation();
                                return;
                            }
                            return;
                        }
                        this.deleteAllGlances = true;
                        com.navdy.hud.app.framework.glance.GlanceHelper.showGlancesDeletedToast();
                        if (0 != 0) {
                            runQueuedOperation();
                            return;
                        }
                        return;
                    } else if (0 != 0) {
                        runQueuedOperation();
                        return;
                    } else {
                        return;
                    }
                default:
                    android.content.Context uiContext = this.notifView.getContext();
                    boolean previous = prev;
                    if (info2 != null || deleteAll) {
                        synchronized (this.lockObj) {
                            com.navdy.hud.app.framework.notifications.NotificationManager.Info notifInfo = info2;
                            if (info2 != null) {
                                if (sLogger.isLoggable(2)) {
                                    sLogger.v("handleExpandedMove: going to " + notifInfo.notification.getId());
                                }
                                s = notifInfo.notification.getView(uiContext);
                                if (this.showOn) {
                                    l2 = notifInfo.notification.getExpandedView(uiContext, null);
                                } else {
                                    l2 = null;
                                }
                                notifInfo.startCalled = true;
                                notifInfo.notification.onStart(this.notificationController);
                                c = notifInfo.notification.getColor();
                            } else {
                                if (sLogger.isLoggable(2)) {
                                    sLogger.v("handleExpandedMove: going to delete all");
                                }
                                if (this.showOn) {
                                    s = com.navdy.hud.app.framework.glance.GlanceViewCache.getView(com.navdy.hud.app.framework.glance.GlanceViewCache.ViewType.SMALL_IMAGE, uiContext);
                                    s.setTag(com.navdy.hud.app.R.id.glance_view_type, com.navdy.hud.app.framework.glance.GlanceViewCache.ViewType.SMALL_IMAGE);
                                    imageView = (android.widget.ImageView) s.findViewById(com.navdy.hud.app.R.id.imageView);
                                } else {
                                    s = com.navdy.hud.app.framework.glance.GlanceViewCache.getView(com.navdy.hud.app.framework.glance.GlanceViewCache.ViewType.SMALL_GLANCE_MESSAGE, uiContext);
                                    s.setTag(com.navdy.hud.app.R.id.glance_view_type, com.navdy.hud.app.framework.glance.GlanceViewCache.ViewType.SMALL_GLANCE_MESSAGE);
                                    imageView = (android.widget.ImageView) s.findViewById(com.navdy.hud.app.R.id.mainImage);
                                    android.widget.TextView mainTitle = (android.widget.TextView) s.findViewById(com.navdy.hud.app.R.id.mainTitle);
                                    mainTitle.setText(com.navdy.hud.app.framework.glance.GlanceConstants.glancesDismissAll);
                                    mainTitle.setAlpha(1.0f);
                                    mainTitle.setVisibility(0);
                                    android.widget.TextView subTitle = (android.widget.TextView) s.findViewById(com.navdy.hud.app.R.id.subTitle);
                                    int count = getNotificationCount() - getNonRemovableNotifCount();
                                    if (count == 0) {
                                        s.setTag(com.navdy.hud.app.R.id.glance_can_delete, java.lang.Integer.valueOf(1));
                                    } else {
                                        s.setTag(com.navdy.hud.app.R.id.glance_can_delete, null);
                                    }
                                    subTitle.setText(this.resources.getString(com.navdy.hud.app.R.string.count, new java.lang.Object[]{java.lang.Integer.valueOf(count)}));
                                    subTitle.setAlpha(1.0f);
                                    subTitle.setVisibility(0);
                                    android.view.View choiceLayout = s.findViewById(com.navdy.hud.app.R.id.choiceLayout);
                                    if (choiceLayout != null) {
                                        choiceLayout.setVisibility(4);
                                    }
                                }
                                s.setTag(com.navdy.hud.app.framework.glance.GlanceConstants.DELETE_ALL_VIEW_TAG);
                                imageView.setImageResource(com.navdy.hud.app.R.drawable.icon_dismiss_all_glances);
                                if (this.showOn) {
                                    l = com.navdy.hud.app.framework.glance.GlanceViewCache.getView(com.navdy.hud.app.framework.glance.GlanceViewCache.ViewType.BIG_TEXT, uiContext);
                                    l.setTag(com.navdy.hud.app.framework.glance.GlanceConstants.DELETE_ALL_VIEW_TAG);
                                    updateDeleteAllGlanceCount((android.widget.TextView) l.findViewById(com.navdy.hud.app.R.id.textView), l);
                                } else {
                                    l = null;
                                }
                                c = com.navdy.hud.app.framework.glance.GlanceConstants.colorDeleteAll;
                            }
                            android.view.View small = s;
                            android.view.View large = l2;
                            int color = c;
                            if (this.stackCurrentNotification != null) {
                                android.animation.AnimatorSet set = this.stackCurrentNotification.notification.getViewSwitchAnimation(false);
                                if (set != null) {
                                    set.addListener(new com.navdy.hud.app.framework.notifications.NotificationManager.Anon14(small, large, notifInfo, previous, color));
                                    set.setDuration(100);
                                    set.start();
                                } else {
                                    viewSwitchAnimation(small, large, notifInfo, previous, color);
                                }
                            } else {
                                viewSwitchAnimation(small, large, notifInfo, previous, color);
                            }
                        }
                    } else {
                        if (sLogger.isLoggable(2)) {
                            sLogger.v("handleExpandedMove: no-op");
                        }
                        runQueuedOperation = true;
                    }
                    if (runQueuedOperation) {
                        runQueuedOperation();
                        return;
                    }
                    return;
            }
        } catch (Throwable th) {
            if (runQueuedOperation) {
            }
            throw th;
        }
        if (runQueuedOperation) {
            runQueuedOperation();
        }
        throw th;
    }

    /* access modifiers changed from: private */
    public void displayScrollingIndicator(int index) {
        if (isExpanded() && this.showOn) {
            if (this.stackCurrentNotification == null || !this.stackCurrentNotification.notification.supportScroll()) {
                hideScrollingIndicator();
                return;
            }
            com.navdy.hud.app.framework.notifications.IScrollEvent scroll = (com.navdy.hud.app.framework.notifications.IScrollEvent) this.stackCurrentNotification.notification;
            this.notifScrollIndicator.setBackgroundColor(this.stackCurrentNotification.notification.getColor());
            this.notifIndicator.getViewTreeObserver().addOnGlobalLayoutListener(new com.navdy.hud.app.framework.notifications.NotificationManager.Anon15(index, scroll));
        }
    }

    /* access modifiers changed from: private */
    public void hideScrollingIndicator() {
        if (this.notifScrollIndicator != null) {
            ((android.view.View) this.notifScrollIndicator.getParent()).setVisibility(8);
        }
    }

    /* access modifiers changed from: private */
    public void setNotifScrollIndicatorXY(android.graphics.RectF rectF, com.navdy.hud.app.framework.notifications.IScrollEvent scroll) {
        scroll.setListener(this.scrollProgress);
        this.notifScrollIndicator.setCurrentItem(1);
        android.view.View parent = (android.view.View) this.notifScrollIndicator.getParent();
        parent.setX(this.notifIndicator.getX() + ((float) com.navdy.hud.app.framework.glance.GlanceConstants.scrollingIndicatorLeftPadding));
        parent.setY(((this.notifIndicator.getY() + rectF.top) + ((float) (com.navdy.hud.app.framework.glance.GlanceConstants.scrollingIndicatorCircleSize / 2))) - ((float) (com.navdy.hud.app.framework.glance.GlanceConstants.scrollingIndicatorHeight / 2)));
        parent.setVisibility(0);
    }

    /* access modifiers changed from: private */
    public void startScrollThresholdTimer() {
        this.scrollState = com.navdy.hud.app.framework.notifications.NotificationManager.ScrollState.NOT_HANDLED;
        this.handler.removeCallbacks(this.ignoreScrollRunnable);
        this.handler.postDelayed(this.ignoreScrollRunnable, 750);
    }

    public void enableNotifications(boolean enable2) {
        sLogger.v("notification enabled[" + enable2 + "]");
        this.enable = enable2;
        enablePhoneNotifications(enable2);
    }

    public boolean isNotificationsEnabled() {
        return this.enable;
    }

    public void enablePhoneNotifications(boolean enable2) {
        sLogger.v("notification enabled phone[" + enable2 + "]");
        this.enablePhoneCalls = enable2;
    }

    public boolean isPhoneNotificationsEnabled() {
        return this.enablePhoneCalls;
    }

    public boolean isAnimating() {
        return this.isAnimating;
    }

    @com.squareup.otto.Subscribe
    public void onNotificationPreference(com.navdy.service.library.events.preferences.NotificationPreferences preferences) {
        if (!preferences.enabled.booleanValue()) {
            clearAllGlances();
        }
    }

    @com.squareup.otto.Subscribe
    public void onClearGlances(com.navdy.service.library.events.glances.ClearGlances clearGlances) {
        clearAllGlances();
    }

    private void clearAllGlances() {
        sLogger.v("glances turned off");
        if (isExpanded() || isExpandedNotificationVisible()) {
            sLogger.v("glances turned off: expanded mode");
            this.deleteAllGlances = true;
            clearOperationQueue();
            collapseExpandedNotification(true, true);
        } else if (isNotificationViewShowing()) {
            sLogger.v("glances turned off: notif showing");
            this.deleteAllGlances = true;
            collapseNotification();
        } else {
            sLogger.v("glances turned off: notif not showing");
            synchronized (this.lockObj) {
                removeAllNotification();
            }
        }
    }

    public void hideNotificationCoverView() {
        android.view.View layoutCover = getExpandedNotificationCoverView();
        if (layoutCover != null && layoutCover.getVisibility() == 0) {
            sLogger.v("hideNotificationCoverView");
            layoutCover.setVisibility(8);
        }
    }

    /* access modifiers changed from: private */
    public boolean isScreenHighPriority(com.navdy.service.library.events.ui.Screen screen) {
        switch (screen) {
            case SCREEN_DESTINATION_PICKER:
                return true;
            default:
                return false;
        }
    }
}
