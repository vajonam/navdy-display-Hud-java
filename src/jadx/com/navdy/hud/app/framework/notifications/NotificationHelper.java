package com.navdy.hud.app.framework.notifications;

public class NotificationHelper {
    static com.navdy.service.library.log.Logger sLogger = com.navdy.hud.app.framework.notifications.NotificationManager.sLogger;

    public static boolean isNotificationRemovable(java.lang.String id) {
        char c = 65535;
        switch (id.hashCode()) {
            case -1409926357:
                if (id.equals(com.navdy.hud.app.framework.notifications.NotificationId.PHONE_CALL_NOTIFICATION_ID)) {
                    c = 0;
                    break;
                }
                break;
            case -54194414:
                if (id.equals(com.navdy.hud.app.framework.notifications.NotificationId.TRAFFIC_REROUTE_NOTIFICATION_ID)) {
                    c = 1;
                    break;
                }
                break;
        }
        switch (c) {
            case 0:
            case 1:
                return false;
            default:
                return true;
        }
    }
}
