package com.navdy.hud.app.framework.notifications;

public final class NotificationManager$$InjectAdapter extends dagger.internal.Binding<com.navdy.hud.app.framework.notifications.NotificationManager> implements dagger.MembersInjector<com.navdy.hud.app.framework.notifications.NotificationManager> {
    private dagger.internal.Binding<com.squareup.otto.Bus> bus;
    private dagger.internal.Binding<com.navdy.hud.app.ui.framework.UIStateManager> uiStateManager;

    public NotificationManager$$InjectAdapter() {
        super(null, "members/com.navdy.hud.app.framework.notifications.NotificationManager", false, com.navdy.hud.app.framework.notifications.NotificationManager.class);
    }

    public void attach(dagger.internal.Linker linker) {
        this.uiStateManager = linker.requestBinding("com.navdy.hud.app.ui.framework.UIStateManager", com.navdy.hud.app.framework.notifications.NotificationManager.class, getClass().getClassLoader());
        this.bus = linker.requestBinding("com.squareup.otto.Bus", com.navdy.hud.app.framework.notifications.NotificationManager.class, getClass().getClassLoader());
    }

    public void getDependencies(java.util.Set<dagger.internal.Binding<?>> set, java.util.Set<dagger.internal.Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.uiStateManager);
        injectMembersBindings.add(this.bus);
    }

    public void injectMembers(com.navdy.hud.app.framework.notifications.NotificationManager object) {
        object.uiStateManager = (com.navdy.hud.app.ui.framework.UIStateManager) this.uiStateManager.get();
        object.bus = (com.squareup.otto.Bus) this.bus.get();
    }
}
