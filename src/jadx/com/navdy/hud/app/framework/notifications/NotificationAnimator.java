package com.navdy.hud.app.framework.notifications;

public class NotificationAnimator {
    /* access modifiers changed from: private */
    public static com.navdy.service.library.log.Logger sLogger = com.navdy.hud.app.framework.notifications.NotificationManager.sLogger;

    static class Anon1 implements java.lang.Runnable {
        final /* synthetic */ android.view.View val$view;

        Anon1(android.view.View view) {
            this.val$view = view;
        }

        public void run() {
            this.val$view.setVisibility(0);
        }
    }

    static class Anon2 implements java.lang.Runnable {
        final /* synthetic */ android.view.ViewGroup val$parent;
        final /* synthetic */ android.view.View val$view;

        Anon2(android.view.ViewGroup viewGroup, android.view.View view) {
            this.val$parent = viewGroup;
            this.val$view = view;
        }

        public void run() {
            this.val$parent.removeView(this.val$view);
        }
    }

    static class Anon3 implements java.lang.Runnable {
        final /* synthetic */ android.view.View val$indicator;

        Anon3(android.view.View view) {
            this.val$indicator = view;
        }

        public void run() {
            this.val$indicator.setVisibility(8);
        }
    }

    static class Anon4 implements java.lang.Runnable {
        final /* synthetic */ java.lang.Runnable val$endAction;
        final /* synthetic */ android.view.View val$expandedNotifView;
        final /* synthetic */ com.navdy.hud.app.framework.notifications.NotificationManager.Info val$notificationObj;
        final /* synthetic */ com.navdy.hud.app.view.NotificationView val$notificationView;

        Anon4(android.view.View view, com.navdy.hud.app.framework.notifications.NotificationManager.Info info, com.navdy.hud.app.view.NotificationView notificationView, java.lang.Runnable runnable) {
            this.val$expandedNotifView = view;
            this.val$notificationObj = info;
            this.val$notificationView = notificationView;
            this.val$endAction = runnable;
        }

        public void run() {
            this.val$expandedNotifView.setVisibility(8);
            com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().setExpandedStack(false);
            if (this.val$notificationObj != null && this.val$notificationObj.startCalled) {
                com.navdy.hud.app.framework.notifications.NotificationAnimator.sLogger.v("calling onExpandedNotificationEvent-collapse");
                this.val$notificationObj.notification.onExpandedNotificationEvent(com.navdy.hud.app.ui.framework.UIStateManager.Mode.COLLAPSE);
                com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().setNotificationColor();
                this.val$notificationView.showNextNotificationColor();
            }
            if (this.val$endAction != null) {
                this.val$endAction.run();
            }
        }
    }

    public enum Operation {
        MOVE_PREV,
        MOVE_NEXT,
        SELECT,
        COLLAPSE_VIEW,
        UPDATE_INDICATOR,
        REMOVE_NOTIFICATION
    }

    public static class OperationInfo {
        boolean collapse;
        boolean gesture;
        boolean hideNotification;
        java.lang.String id;
        com.navdy.hud.app.framework.notifications.NotificationAnimator.Operation operation;
        boolean quick;

        OperationInfo(com.navdy.hud.app.framework.notifications.NotificationAnimator.Operation operation2, boolean hideNotification2, boolean quick2, boolean gesture2, java.lang.String id2, boolean collapse2) {
            this.operation = operation2;
            this.hideNotification = hideNotification2;
            this.quick = quick2;
            this.gesture = gesture2;
            this.id = id2;
            this.collapse = collapse2;
        }
    }

    static void animateNotifViews(android.view.View viewIn, android.view.View viewOut, android.view.ViewGroup parent, int translationBy, int duration, int startDelay, java.lang.Runnable endAction) {
        android.view.ViewPropertyAnimator out = null;
        android.view.ViewPropertyAnimator in = null;
        if (viewOut != null) {
            out = animateOut(viewOut, parent, translationBy, duration, 0, null);
        }
        if (viewIn != null) {
            in = animateIn(viewIn, parent, translationBy, duration, startDelay, endAction);
        }
        if (out != null) {
            out.start();
        }
        if (in != null) {
            in.start();
        }
    }

    static void animateExpandedViews(android.view.View viewIn, android.view.View viewOut, android.view.ViewGroup parent, int translationBy, int duration, int startDelay, java.lang.Runnable endAction) {
        android.view.ViewPropertyAnimator out = null;
        android.view.ViewPropertyAnimator in = null;
        if (viewOut != null) {
            out = animateOut(viewOut, parent, translationBy, duration, 0, endAction);
            endAction = null;
        }
        if (viewIn != null) {
            in = animateIn(viewIn, parent, translationBy, duration, startDelay, endAction);
        }
        if (out != null) {
            out.start();
        }
        if (in != null) {
            in.start();
        }
    }

    private static android.view.ViewPropertyAnimator animateIn(android.view.View view, android.view.ViewGroup parent, int translationBy, int duration, int startDelay, java.lang.Runnable endAction) {
        view.setLayoutParams(new android.widget.FrameLayout.LayoutParams(-1, -1));
        view.setTranslationY((float) translationBy);
        view.setAlpha(0.0f);
        parent.addView(view);
        android.view.ViewPropertyAnimator animator = view.animate().alpha(1.0f).translationY(0.0f).setDuration((long) duration).setStartDelay((long) startDelay).withStartAction(new com.navdy.hud.app.framework.notifications.NotificationAnimator.Anon1(view));
        if (endAction != null) {
            animator.withEndAction(endAction);
        }
        return animator;
    }

    private static android.view.ViewPropertyAnimator animateOut(android.view.View view, android.view.ViewGroup parent, int translationBy, int duration, int startDelay, java.lang.Runnable endAction) {
        android.view.ViewPropertyAnimator animator = view.animate().alpha(0.0f).translationY((float) (-translationBy)).setDuration((long) duration).setStartDelay((long) startDelay).withEndAction(new com.navdy.hud.app.framework.notifications.NotificationAnimator.Anon2(parent, view));
        if (endAction != null) {
            animator.withEndAction(endAction);
        }
        return animator;
    }

    static void animateChildViewOut(android.view.View child, int duration, java.lang.Runnable endAction) {
        child.animate().alpha(0.0f).setDuration((long) duration).setStartDelay(0).withEndAction(endAction).start();
    }

    static void animateExpandedViewOut(com.navdy.hud.app.view.NotificationView notificationView, int newX, android.view.View expandedNotifView, android.view.View indicator, java.lang.Runnable endAction, com.navdy.hud.app.framework.notifications.NotificationManager.Info notificationObj, boolean showOn, boolean isExpandNotificationViewVisible) {
        java.lang.Runnable runnable1 = new com.navdy.hud.app.framework.notifications.NotificationAnimator.Anon3(indicator);
        java.lang.Runnable runnable2 = new com.navdy.hud.app.framework.notifications.NotificationAnimator.Anon4(expandedNotifView, notificationObj, notificationView, endAction);
        if (showOn || isExpandNotificationViewVisible) {
            notificationView.animate().x((float) newX).withStartAction(runnable1).withEndAction(runnable2).start();
            return;
        }
        runnable1.run();
        runnable2.run();
    }
}
