package com.navdy.hud.app.framework.destinations;

public class DestinationSuggestionToast {
    public static final java.lang.String DESTINATION_SUGGESTION_TOAST_ID = "connection#toast";
    private static final int TAG_ACCEPT = 1;
    private static final int TAG_IGNORE = 2;
    private static final int TOAST_TIMEOUT = 15000;
    private static final java.lang.String accept;
    private static final java.lang.String ignore;
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.framework.destinations.DestinationSuggestionToast.class);
    private static final java.util.ArrayList<com.navdy.hud.app.ui.component.ChoiceLayout.Choice> suggestionChoices = new java.util.ArrayList<>(2);
    private static final java.lang.String title;

    static class Anon1 implements com.navdy.hud.app.framework.toast.IToastCallback {
        com.navdy.hud.app.view.ToastView toastView;
        final /* synthetic */ com.navdy.service.library.events.destination.Destination val$destination;

        Anon1(com.navdy.service.library.events.destination.Destination destination) {
            this.val$destination = destination;
        }

        public void onStart(com.navdy.hud.app.view.ToastView view) {
            this.toastView = view;
            com.navdy.hud.app.ui.component.ConfirmationLayout layout = view.getConfirmation();
            layout.screenTitle.setTextAppearance(view.getContext(), com.navdy.hud.app.R.style.mainTitle);
            layout.title3.setTextColor(view.getResources().getColor(17170443));
        }

        public void onStop() {
            this.toastView = null;
        }

        public boolean onKey(com.navdy.hud.app.manager.InputManager.CustomKeyEvent event) {
            return false;
        }

        public void executeChoiceItem(int pos, int id) {
            if (this.toastView != null) {
                boolean accepted = false;
                switch (id) {
                    case 1:
                        accepted = true;
                        this.toastView.dismissToast();
                        com.navdy.hud.app.framework.destinations.DestinationsManager.getInstance().goToSuggestedDestination();
                        break;
                    case 2:
                        accepted = false;
                        this.toastView.dismissToast();
                        break;
                }
                try {
                    com.navdy.hud.app.analytics.AnalyticsSupport.recordDestinationSuggestion(accepted, this.val$destination.suggestion_type == com.navdy.service.library.events.destination.Destination.SuggestionType.SUGGESTION_CALENDAR);
                } catch (Throwable t) {
                    com.navdy.hud.app.framework.destinations.DestinationSuggestionToast.sLogger.e("Error while recording the destination suggestion", t);
                }
            }
        }
    }

    static {
        android.content.res.Resources resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        title = resources.getString(com.navdy.hud.app.R.string.suggested_trip);
        accept = resources.getString(com.navdy.hud.app.R.string.accept);
        ignore = resources.getString(com.navdy.hud.app.R.string.ignore);
        suggestionChoices.add(new com.navdy.hud.app.ui.component.ChoiceLayout.Choice(accept, 1));
        suggestionChoices.add(new com.navdy.hud.app.ui.component.ChoiceLayout.Choice(ignore, 2));
    }

    public static void showSuggestion(com.navdy.service.library.events.places.SuggestedDestination suggestion) {
        com.navdy.service.library.events.destination.Destination destination = suggestion.destination;
        int iconRes = com.navdy.hud.app.R.drawable.icon_places_favorite;
        android.os.Bundle bundle = new android.os.Bundle();
        bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_TIMEOUT, 15000);
        if (suggestion.type != null) {
            switch (suggestion.type) {
                case SUGGESTION_CALENDAR:
                    iconRes = com.navdy.hud.app.R.drawable.icon_place_calendar;
                    break;
            }
        }
        if (iconRes == com.navdy.hud.app.R.drawable.icon_places_favorite) {
            switch (destination.favorite_type) {
                case FAVORITE_HOME:
                    iconRes = com.navdy.hud.app.R.drawable.icon_places_home;
                    break;
                case FAVORITE_WORK:
                    iconRes = com.navdy.hud.app.R.drawable.icon_places_work;
                    break;
                case FAVORITE_CUSTOM:
                    iconRes = com.navdy.hud.app.R.drawable.icon_places_favorite;
                    break;
                case FAVORITE_NONE:
                    iconRes = com.navdy.hud.app.R.drawable.icon_mm_suggested_places;
                    break;
            }
        }
        bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_IMAGE, iconRes);
        bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_STYLE, com.navdy.hud.app.R.style.ToastMainTitle);
        bundle.putBoolean(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_SUPPORTS_GESTURE, true);
        bundle.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE, title);
        bundle.putParcelableArrayList(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_CHOICE_LIST, suggestionChoices);
        java.lang.String title2 = !android.text.TextUtils.isEmpty(destination.destination_title) ? destination.destination_title : !android.text.TextUtils.isEmpty(destination.destination_subtitle) ? destination.destination_subtitle : destination.full_address;
        int durationWithTraffic = ((java.lang.Integer) com.squareup.wire.Wire.get(suggestion.duration_traffic, java.lang.Integer.valueOf(-1))).intValue();
        java.lang.String str = "";
        android.content.res.Resources resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        if (resources != null) {
            java.lang.String title3 = durationWithTraffic > 0 ? resources.getString(com.navdy.hud.app.R.string.suggested_eta, new java.lang.Object[]{com.navdy.hud.app.maps.util.RouteUtils.formatEtaMinutes(resources, (int) java.util.concurrent.TimeUnit.SECONDS.toMinutes((long) durationWithTraffic))}) : !android.text.TextUtils.isEmpty(destination.destination_subtitle) ? destination.destination_subtitle : destination.full_address;
            bundle.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_2, title2);
            bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_2_STYLE, com.navdy.hud.app.R.style.Toast_2);
            bundle.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_3, title3);
            bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_3_STYLE, com.navdy.hud.app.R.style.Toast_1);
            bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_INFO_CONTAINER_MAX_WIDTH, resources.getDimensionPixelSize(com.navdy.hud.app.R.dimen.expand_notif_width));
            com.navdy.hud.app.framework.toast.IToastCallback callback = new com.navdy.hud.app.framework.destinations.DestinationSuggestionToast.Anon1(destination);
            boolean makeCurrent = true;
            com.navdy.hud.app.framework.toast.ToastManager toastManager = com.navdy.hud.app.framework.toast.ToastManager.getInstance();
            if (android.text.TextUtils.equals(toastManager.getCurrentToastId(), com.navdy.hud.app.framework.phonecall.CallNotification.CALL_NOTIFICATION_TOAST_ID)) {
                sLogger.v("phone toast active");
                makeCurrent = false;
            } else {
                toastManager.dismissCurrentToast(toastManager.getCurrentToastId());
                toastManager.clearAllPendingToast();
            }
            toastManager.addToast(new com.navdy.hud.app.framework.toast.ToastManager.ToastParams("connection#toast", bundle, callback, true, makeCurrent));
        }
    }

    public static void dismissSuggestionToast() {
        com.navdy.hud.app.framework.toast.ToastManager toastManager = com.navdy.hud.app.framework.toast.ToastManager.getInstance();
        toastManager.clearPendingToast("connection#toast");
        toastManager.dismissCurrentToast("connection#toast");
    }
}
