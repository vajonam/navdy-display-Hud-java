package com.navdy.hud.app.framework.destinations;

public class Destination {
    public final java.util.List<com.navdy.hud.app.framework.contacts.Contact> contacts;
    public final int destinationIcon;
    public final int destinationIconBkColor;
    public final java.lang.String destinationPlaceId;
    public final java.lang.String destinationSubtitle;
    public final java.lang.String destinationTitle;
    public final com.navdy.hud.app.framework.destinations.Destination.DestinationType destinationType;
    public final double displayPositionLatitude;
    public final double displayPositionLongitude;
    public final java.lang.String distanceStr;
    public final com.navdy.hud.app.framework.destinations.Destination.FavoriteDestinationType favoriteDestinationType;
    public final java.lang.String fullAddress;
    public final java.lang.String identifier;
    public final java.lang.String initials;
    public boolean isInitialNumber;
    public final double navigationPositionLatitude;
    public final double navigationPositionLongitude;
    public boolean needShortTitleFont;
    public final java.util.List<com.navdy.hud.app.framework.contacts.Contact> phoneNumbers;
    public final com.navdy.hud.app.framework.destinations.Destination.PlaceCategory placeCategory;
    public final com.navdy.service.library.events.places.PlaceType placeType;
    public final java.lang.String recentTimeLabel;
    public final int recentTimeLabelColor;
    public final boolean recommendation;

    public static class Builder {
        /* access modifiers changed from: private */
        public java.util.List<com.navdy.hud.app.framework.contacts.Contact> contacts;
        /* access modifiers changed from: private */
        public int destinationIcon;
        /* access modifiers changed from: private */
        public int destinationIconBkColor = 0;
        /* access modifiers changed from: private */
        public java.lang.String destinationPlaceId;
        /* access modifiers changed from: private */
        public java.lang.String destinationSubtitle;
        /* access modifiers changed from: private */
        public java.lang.String destinationTitle;
        /* access modifiers changed from: private */
        public com.navdy.hud.app.framework.destinations.Destination.DestinationType destinationType;
        /* access modifiers changed from: private */
        public double displayPositionLatitude;
        /* access modifiers changed from: private */
        public double displayPositionLongitude;
        /* access modifiers changed from: private */
        public java.lang.String distanceStr;
        /* access modifiers changed from: private */
        public com.navdy.hud.app.framework.destinations.Destination.FavoriteDestinationType favoriteDestinationType;
        /* access modifiers changed from: private */
        public java.lang.String fullAddress;
        /* access modifiers changed from: private */
        public java.lang.String identifier;
        /* access modifiers changed from: private */
        public java.lang.String initials;
        /* access modifiers changed from: private */
        public double navigationPositionLatitude;
        /* access modifiers changed from: private */
        public double navigationPositionLongitude;
        /* access modifiers changed from: private */
        public java.util.List<com.navdy.hud.app.framework.contacts.Contact> phoneNumbers;
        /* access modifiers changed from: private */
        public com.navdy.hud.app.framework.destinations.Destination.PlaceCategory placeCategory;
        /* access modifiers changed from: private */
        public com.navdy.service.library.events.places.PlaceType placeType;
        /* access modifiers changed from: private */
        public java.lang.String recentTimeLabel;
        /* access modifiers changed from: private */
        public int recentTimeLabelColor;
        /* access modifiers changed from: private */
        public boolean recommendation;

        public Builder() {
        }

        public Builder(com.navdy.hud.app.framework.destinations.Destination d) {
            this.navigationPositionLatitude = d.navigationPositionLatitude;
            this.navigationPositionLongitude = d.navigationPositionLongitude;
            this.displayPositionLatitude = d.displayPositionLatitude;
            this.displayPositionLongitude = d.displayPositionLongitude;
            this.favoriteDestinationType = d.favoriteDestinationType;
            this.fullAddress = d.fullAddress;
            this.destinationTitle = d.destinationTitle;
            this.destinationSubtitle = d.destinationSubtitle;
            this.identifier = d.identifier;
            this.destinationType = d.destinationType;
            this.initials = d.initials;
            this.placeCategory = d.placeCategory;
            this.recentTimeLabel = d.recentTimeLabel;
            this.recentTimeLabelColor = d.recentTimeLabelColor;
            this.recommendation = d.recommendation;
            this.destinationIcon = d.destinationIcon;
            this.destinationIconBkColor = d.destinationIconBkColor;
            this.destinationPlaceId = d.destinationPlaceId;
            this.distanceStr = d.distanceStr;
            this.phoneNumbers = d.phoneNumbers;
            this.contacts = d.contacts;
        }

        public com.navdy.hud.app.framework.destinations.Destination.Builder navigationPositionLatitude(double navigationPositionLatitude2) {
            this.navigationPositionLatitude = navigationPositionLatitude2;
            return this;
        }

        public com.navdy.hud.app.framework.destinations.Destination.Builder navigationPositionLongitude(double navigationPositionLongitude2) {
            this.navigationPositionLongitude = navigationPositionLongitude2;
            return this;
        }

        public com.navdy.hud.app.framework.destinations.Destination.Builder displayPositionLatitude(double displayPositionLatitude2) {
            this.displayPositionLatitude = displayPositionLatitude2;
            return this;
        }

        public com.navdy.hud.app.framework.destinations.Destination.Builder displayPositionLongitude(double displayPositionLongitude2) {
            this.displayPositionLongitude = displayPositionLongitude2;
            return this;
        }

        public com.navdy.hud.app.framework.destinations.Destination.Builder fullAddress(java.lang.String fullAddress2) {
            this.fullAddress = fullAddress2;
            return this;
        }

        public com.navdy.hud.app.framework.destinations.Destination.Builder destinationTitle(java.lang.String destinationTitle2) {
            this.destinationTitle = destinationTitle2;
            return this;
        }

        public com.navdy.hud.app.framework.destinations.Destination.Builder destinationSubtitle(java.lang.String destinationSubtitle2) {
            this.destinationSubtitle = destinationSubtitle2;
            return this;
        }

        public com.navdy.hud.app.framework.destinations.Destination.Builder identifier(java.lang.String identifier2) {
            this.identifier = identifier2;
            return this;
        }

        public com.navdy.hud.app.framework.destinations.Destination.Builder destinationType(com.navdy.hud.app.framework.destinations.Destination.DestinationType destinationType2) {
            this.destinationType = destinationType2;
            return this;
        }

        public com.navdy.hud.app.framework.destinations.Destination.Builder favoriteDestinationType(com.navdy.hud.app.framework.destinations.Destination.FavoriteDestinationType favoriteDestinationType2) {
            this.favoriteDestinationType = favoriteDestinationType2;
            return this;
        }

        public com.navdy.hud.app.framework.destinations.Destination.Builder initials(java.lang.String initials2) {
            this.initials = initials2;
            return this;
        }

        public com.navdy.hud.app.framework.destinations.Destination.Builder placeCategory(com.navdy.hud.app.framework.destinations.Destination.PlaceCategory placeCategory2) {
            this.placeCategory = placeCategory2;
            return this;
        }

        public com.navdy.hud.app.framework.destinations.Destination.Builder recentTimeLabel(java.lang.String recentTimeLabel2) {
            this.recentTimeLabel = recentTimeLabel2;
            return this;
        }

        public com.navdy.hud.app.framework.destinations.Destination.Builder recentTimeLabelColor(int recentTimeLabelColor2) {
            this.recentTimeLabelColor = recentTimeLabelColor2;
            return this;
        }

        public com.navdy.hud.app.framework.destinations.Destination.Builder recommendation(boolean recommendation2) {
            this.recommendation = recommendation2;
            return this;
        }

        public com.navdy.hud.app.framework.destinations.Destination.Builder destinationIcon(int destinationIcon2) {
            this.destinationIcon = destinationIcon2;
            return this;
        }

        public com.navdy.hud.app.framework.destinations.Destination.Builder destinationIconBkColor(int destinationIconBkColor2) {
            this.destinationIconBkColor = destinationIconBkColor2;
            return this;
        }

        public com.navdy.hud.app.framework.destinations.Destination.Builder destinationPlaceId(java.lang.String placeId) {
            this.destinationPlaceId = placeId;
            return this;
        }

        public com.navdy.hud.app.framework.destinations.Destination.Builder placeType(com.navdy.service.library.events.places.PlaceType placeType2) {
            this.placeType = placeType2;
            return this;
        }

        public com.navdy.hud.app.framework.destinations.Destination.Builder distanceStr(java.lang.String distanceStr2) {
            this.distanceStr = distanceStr2;
            return this;
        }

        public com.navdy.hud.app.framework.destinations.Destination.Builder phoneNumbers(java.util.List<com.navdy.hud.app.framework.contacts.Contact> phoneNumbers2) {
            this.phoneNumbers = phoneNumbers2;
            return this;
        }

        public com.navdy.hud.app.framework.destinations.Destination.Builder contacts(java.util.List<com.navdy.hud.app.framework.contacts.Contact> contacts2) {
            this.contacts = contacts2;
            return this;
        }

        public com.navdy.hud.app.framework.destinations.Destination build() {
            return new com.navdy.hud.app.framework.destinations.Destination(this);
        }
    }

    public enum DestinationType {
        DEFAULT,
        FIND_GAS
    }

    public enum FavoriteDestinationType {
        FAVORITE_NONE(0),
        FAVORITE_HOME(1),
        FAVORITE_WORK(2),
        FAVORITE_CONTACT(3),
        FAVORITE_CALENDAR(4),
        FAVORITE_CUSTOM(10);
        
        public final int value;

        private FavoriteDestinationType(int value2) {
            this.value = value2;
        }

        public static com.navdy.hud.app.framework.destinations.Destination.FavoriteDestinationType buildFromValue(int n) {
            com.navdy.hud.app.framework.destinations.Destination.FavoriteDestinationType[] values;
            for (com.navdy.hud.app.framework.destinations.Destination.FavoriteDestinationType type : values()) {
                if (n == type.value) {
                    return type;
                }
            }
            return FAVORITE_NONE;
        }
    }

    public enum PlaceCategory {
        SUGGESTED,
        RECENT,
        SUGGESTED_RECENT
    }

    private Destination(com.navdy.hud.app.framework.destinations.Destination.Builder builder) {
        this(builder.navigationPositionLatitude, builder.navigationPositionLongitude, builder.displayPositionLatitude, builder.displayPositionLongitude, builder.fullAddress, builder.destinationTitle, builder.destinationSubtitle, builder.identifier, builder.favoriteDestinationType, builder.destinationType, builder.initials, builder.placeCategory, builder.recentTimeLabel, builder.recentTimeLabelColor, builder.recommendation, builder.destinationIcon, builder.destinationIconBkColor, builder.destinationPlaceId, builder.placeType, builder.distanceStr, builder.phoneNumbers, builder.contacts);
    }

    public Destination(double navigationPositionLatitude2, double navigationPositionLongitude2, double displayPositionLatitude2, double displayPositionLongitude2, java.lang.String fullAddress2, java.lang.String destinationTitle2, java.lang.String destinationSubtitle2, java.lang.String identifier2, com.navdy.hud.app.framework.destinations.Destination.FavoriteDestinationType favoriteDestinationType2, com.navdy.hud.app.framework.destinations.Destination.DestinationType destinationType2, java.lang.String initials2, com.navdy.hud.app.framework.destinations.Destination.PlaceCategory placeCategory2, java.lang.String recentTimeLabel2, int recentTimeLabelColor2, boolean recommendation2, int destinationIcon2, int destinationIconBkColor2, java.lang.String destinationPlaceId2, com.navdy.service.library.events.places.PlaceType placeType2, java.lang.String distanceStr2, java.util.List<com.navdy.hud.app.framework.contacts.Contact> phoneNumbers2, java.util.List<com.navdy.hud.app.framework.contacts.Contact> contacts2) {
        this.navigationPositionLatitude = navigationPositionLatitude2;
        this.navigationPositionLongitude = navigationPositionLongitude2;
        this.displayPositionLatitude = displayPositionLatitude2;
        this.displayPositionLongitude = displayPositionLongitude2;
        this.fullAddress = fullAddress2;
        this.destinationTitle = destinationTitle2;
        this.destinationSubtitle = destinationSubtitle2;
        this.identifier = identifier2;
        this.favoriteDestinationType = favoriteDestinationType2;
        this.destinationType = destinationType2;
        this.initials = initials2;
        this.placeCategory = placeCategory2;
        this.recentTimeLabel = recentTimeLabel2;
        this.recentTimeLabelColor = recentTimeLabelColor2;
        this.recommendation = recommendation2;
        this.destinationIcon = destinationIcon2;
        this.destinationIconBkColor = destinationIconBkColor2;
        this.destinationPlaceId = destinationPlaceId2;
        this.placeType = placeType2;
        this.distanceStr = distanceStr2;
        this.phoneNumbers = phoneNumbers2;
        this.contacts = contacts2;
        try {
            java.lang.Integer.parseInt(initials2);
            this.isInitialNumber = true;
        } catch (Throwable th) {
            this.isInitialNumber = false;
        }
    }

    public static com.navdy.hud.app.framework.destinations.Destination getGasDestination() {
        java.lang.String title;
        java.lang.String subtitle;
        android.content.res.Resources resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        if (com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().isNavigationModeOn()) {
            title = resources.getString(com.navdy.hud.app.R.string.suggested_dest_find_gas_on_route_title);
            subtitle = resources.getString(com.navdy.hud.app.R.string.suggested_dest_find_gas_on_route_desc);
        } else {
            title = resources.getString(com.navdy.hud.app.R.string.suggested_dest_find_gas_title);
            subtitle = resources.getString(com.navdy.hud.app.R.string.suggested_dest_find_gas_desc);
        }
        return new com.navdy.hud.app.framework.destinations.Destination.Builder().destinationTitle(title).destinationSubtitle(subtitle).favoriteDestinationType(com.navdy.hud.app.framework.destinations.Destination.FavoriteDestinationType.FAVORITE_NONE).destinationType(com.navdy.hud.app.framework.destinations.Destination.DestinationType.FIND_GAS).build();
    }

    public java.lang.String toString() {
        return "id[" + this.identifier + "] placeid[" + this.destinationPlaceId + "] nav_pos[" + this.navigationPositionLatitude + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA + this.navigationPositionLongitude + "]" + " display_pos[" + this.displayPositionLatitude + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA + this.displayPositionLongitude + "] title[" + this.destinationTitle + "] subTitle[" + this.destinationSubtitle + "] address[" + this.fullAddress + "] place_type[" + this.placeType + "]";
    }
}
