package com.navdy.hud.app.framework.destinations;

public class DestinationsManager {
    private static final com.navdy.hud.app.framework.destinations.DestinationsManager.FavoriteDestinationsChanged FAVORITE_DESTINATIONS_CHANGED = new com.navdy.hud.app.framework.destinations.DestinationsManager.FavoriteDestinationsChanged();
    public static final java.lang.String FAVORITE_DESTINATIONS_FILENAME = "favoriteDestinations.pb";
    private static final char[] INITIALS_ARRAY = new char[4];
    public static final int MAX_DESTINATIONS = 30;
    private static final int MAX_INITIALS = 4;
    private static final com.navdy.hud.app.framework.destinations.DestinationsManager.RecentDestinationsChanged RECENT_DESTINATIONS_CHANGED = new com.navdy.hud.app.framework.destinations.DestinationsManager.RecentDestinationsChanged();
    private static final com.navdy.hud.app.framework.destinations.DestinationsManager.SuggestedDestinationsChanged SUGGESTED_DESTINATIONS_CHANGED = new com.navdy.hud.app.framework.destinations.DestinationsManager.SuggestedDestinationsChanged();
    private static final com.navdy.hud.app.framework.destinations.DestinationsManager sInstance = new com.navdy.hud.app.framework.destinations.DestinationsManager();
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.framework.destinations.DestinationsManager.class);
    private final com.squareup.otto.Bus bus = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getBus();
    private volatile java.util.List<com.navdy.hud.app.framework.destinations.Destination> favoriteDestinations;
    private com.navdy.service.library.events.places.FavoriteDestinationsUpdate lastUpdate;
    private volatile java.util.List<com.navdy.hud.app.framework.destinations.Destination> recentDestinations;
    android.content.res.Resources resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
    /* access modifiers changed from: private */
    public com.navdy.service.library.events.places.SuggestedDestination suggestedDestination;
    private volatile java.util.List<com.navdy.hud.app.framework.destinations.Destination> suggestedDestinations;

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            com.navdy.hud.app.framework.destinations.DestinationsManager.this.requestDestinations();
        }
    }

    class Anon2 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.service.library.events.places.FavoriteDestinationsUpdate val$response;

        Anon2(com.navdy.service.library.events.places.FavoriteDestinationsUpdate favoriteDestinationsUpdate) {
            this.val$response = favoriteDestinationsUpdate;
        }

        public void run() {
            try {
                if (this.val$response.status == com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS) {
                    com.navdy.hud.app.framework.destinations.DestinationsManager.this.parseDestinations(this.val$response);
                } else if (this.val$response.status == com.navdy.service.library.events.RequestStatus.REQUEST_VERSION_IS_CURRENT) {
                    com.navdy.hud.app.framework.destinations.DestinationsManager.sLogger.v("fav-destination response: version" + this.val$response.serial_number + " is current");
                } else {
                    com.navdy.hud.app.framework.destinations.DestinationsManager.sLogger.e("sent fav-destination response error: " + this.val$response.status);
                }
            } catch (Throwable t) {
                com.navdy.hud.app.framework.destinations.DestinationsManager.sLogger.e(t);
            }
        }
    }

    class Anon3 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.service.library.events.destination.RecommendedDestinationsUpdate val$response;

        Anon3(com.navdy.service.library.events.destination.RecommendedDestinationsUpdate recommendedDestinationsUpdate) {
            this.val$response = recommendedDestinationsUpdate;
        }

        public void run() {
            try {
                if (this.val$response.status == com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS) {
                    com.navdy.hud.app.framework.destinations.DestinationsManager.this.parseDestinations(this.val$response);
                } else if (this.val$response.status == com.navdy.service.library.events.RequestStatus.REQUEST_VERSION_IS_CURRENT) {
                    com.navdy.hud.app.framework.destinations.DestinationsManager.sLogger.v("fav-destination response: version" + this.val$response.serial_number + " is current");
                } else {
                    com.navdy.hud.app.framework.destinations.DestinationsManager.sLogger.e("sent fav-destination response error: " + this.val$response.status);
                }
            } catch (Throwable t) {
                com.navdy.hud.app.framework.destinations.DestinationsManager.sLogger.e(t);
            }
        }
    }

    class Anon4 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.service.library.events.places.SuggestedDestination val$suggestedDestination;

        Anon4(com.navdy.service.library.events.places.SuggestedDestination suggestedDestination) {
            this.val$suggestedDestination = suggestedDestination;
        }

        public void run() {
            com.navdy.hud.app.framework.destinations.DestinationsManager.sLogger.v("onDestinationSuggestion:" + this.val$suggestedDestination);
            com.navdy.hud.app.maps.here.HereMapsManager hereMapsManager = com.navdy.hud.app.maps.here.HereMapsManager.getInstance();
            if (!hereMapsManager.isInitialized()) {
                com.navdy.hud.app.framework.destinations.DestinationsManager.sLogger.e("onDestinationSuggestion: Map engine not initialized, cannot process suggestion, at this time");
                com.navdy.hud.app.framework.destinations.DestinationsManager.this.suggestedDestination = this.val$suggestedDestination;
            } else if (com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().isNavigationModeOn()) {
                com.navdy.hud.app.framework.destinations.DestinationsManager.sLogger.e("onDestinationSuggestion: nav mode is on");
            } else if (com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getUiStateManager().getHomescreenView() == null || !com.navdy.hud.app.maps.here.HereRouteManager.isUIShowingRouteCalculation()) {
                com.navdy.hud.app.framework.destinations.DestinationsManager.this.suggestedDestination = this.val$suggestedDestination;
                if (com.navdy.hud.app.maps.here.HereMapUtil.getSavedRouteData(com.navdy.hud.app.framework.destinations.DestinationsManager.sLogger).navigationRouteRequest != null) {
                    com.navdy.hud.app.framework.destinations.DestinationsManager.sLogger.e("onDestinationSuggestion: has saved route data");
                } else if (hereMapsManager.getLocationFixManager().getLastGeoCoordinate() == null) {
                    com.navdy.hud.app.framework.destinations.DestinationsManager.sLogger.e("onDestinationSuggestion: no current position");
                } else {
                    com.navdy.hud.app.framework.destinations.DestinationsManager.this.launchSuggestedDestination();
                }
            } else {
                com.navdy.hud.app.framework.destinations.DestinationsManager.sLogger.e("onDestinationSuggestion: route calc is on");
            }
        }
    }

    class Anon5 implements java.lang.Runnable {
        Anon5() {
        }

        public void run() {
            com.navdy.hud.app.framework.destinations.DestinationsManager.this.load();
        }
    }

    public static class FavoriteDestinationsChanged {
    }

    public static class GasDestination {
        public com.navdy.hud.app.framework.destinations.Destination destination;
        public boolean showFirst;
    }

    public static class RecentDestinationsChanged {
    }

    public static class SuggestedDestinationsChanged {
    }

    public static com.navdy.hud.app.framework.destinations.DestinationsManager getInstance() {
        return sInstance;
    }

    private DestinationsManager() {
        this.bus.register(this);
    }

    public java.util.List<com.navdy.hud.app.framework.destinations.Destination> getFavoriteDestinations() {
        return this.favoriteDestinations;
    }

    private void setFavoriteDestinations(java.util.List<com.navdy.hud.app.framework.destinations.Destination> favoriteDestinations2) {
        if (favoriteDestinations2 != this.favoriteDestinations) {
            this.favoriteDestinations = favoriteDestinations2;
            this.bus.post(FAVORITE_DESTINATIONS_CHANGED);
        }
    }

    public java.util.List<com.navdy.hud.app.framework.destinations.Destination> getSuggestedDestinations() {
        java.util.List<com.navdy.hud.app.framework.destinations.Destination> list = new java.util.ArrayList<>();
        if (this.suggestedDestinations != null) {
            list.addAll(this.suggestedDestinations);
        }
        return list;
    }

    public void setSuggestedDestinations(java.util.List<com.navdy.hud.app.framework.destinations.Destination> suggestedDestinations2) {
        this.suggestedDestinations = suggestedDestinations2;
        this.bus.post(SUGGESTED_DESTINATIONS_CHANGED);
    }

    public java.util.List<com.navdy.hud.app.framework.destinations.Destination> getRecentDestinations() {
        return this.recentDestinations;
    }

    public void setRecentDestinations(java.util.List<com.navdy.hud.app.framework.destinations.Destination> recentDestinations2) {
        this.recentDestinations = recentDestinations2;
        this.bus.post(RECENT_DESTINATIONS_CHANGED);
    }

    @com.squareup.otto.Subscribe
    public void onDeviceSyncRequired(com.navdy.hud.app.service.ConnectionHandler.DeviceSyncEvent event) {
        sLogger.v("syncDestinations");
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.framework.destinations.DestinationsManager.Anon1(), 10);
    }

    @com.squareup.otto.Subscribe
    public void onConnectionStatusChange(com.navdy.service.library.events.connection.ConnectionStateChange event) {
        if (event.state == com.navdy.service.library.events.connection.ConnectionStateChange.ConnectionState.CONNECTION_DISCONNECTED) {
            clearDestinations();
        }
    }

    @com.squareup.otto.Subscribe
    public void onDriverProfileChanged(com.navdy.hud.app.event.DriverProfileChanged event) {
        buildDestinations();
    }

    @com.squareup.otto.Subscribe
    public void onFavoriteDestinationsUpdate(com.navdy.service.library.events.places.FavoriteDestinationsUpdate response) {
        sLogger.v("received FavoriteDestinationsUpdate: " + response);
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.framework.destinations.DestinationsManager.Anon2(response), 1);
    }

    @com.squareup.otto.Subscribe
    public void onRecommendedDestinationsUpdate(com.navdy.service.library.events.destination.RecommendedDestinationsUpdate response) {
        sLogger.v("received RecommendedDestinationsUpdate:" + response);
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.framework.destinations.DestinationsManager.Anon3(response), 1);
    }

    @com.squareup.otto.Subscribe
    public void onDestinationSuggestion(com.navdy.service.library.events.places.SuggestedDestination suggestedDestination2) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.framework.destinations.DestinationsManager.Anon4(suggestedDestination2), 3);
    }

    /* access modifiers changed from: private */
    public void requestDestinations() {
        long favoritesVersion;
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        if (this.lastUpdate != null) {
            favoritesVersion = this.lastUpdate.serial_number.longValue();
        } else {
            favoritesVersion = 0;
        }
        this.bus.post(new com.navdy.hud.app.event.RemoteEvent(new com.navdy.service.library.events.FavoriteDestinationsRequest(java.lang.Long.valueOf(favoritesVersion))));
        this.bus.post(new com.navdy.hud.app.event.RemoteEvent(new com.navdy.service.library.events.destination.RecommendedDestinationsRequest(java.lang.Long.valueOf(0))));
        sLogger.v("sent favorite destinations request with current version=" + favoritesVersion);
        sLogger.v("sent recommended destinations request with current version");
    }

    private void buildDestinations() {
        if (com.navdy.hud.app.util.GenericUtil.isMainThread()) {
            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.framework.destinations.DestinationsManager.Anon5(), 10);
        } else {
            load();
        }
    }

    /* access modifiers changed from: private */
    public void load() {
        try {
            parseDestinations((com.navdy.service.library.events.places.FavoriteDestinationsUpdate) new com.navdy.service.library.events.MessageStore(com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile().getPreferencesDirectory()).readMessage(FAVORITE_DESTINATIONS_FILENAME, com.navdy.service.library.events.places.FavoriteDestinationsUpdate.class));
        } catch (Throwable t) {
            setFavoriteDestinations(null);
            sLogger.e(t);
        }
    }

    private void clearDestinations() {
        this.lastUpdate = null;
        setFavoriteDestinations(null);
        setRecentDestinations(null);
        setSuggestedDestinations(null);
    }

    /* access modifiers changed from: private */
    public void parseDestinations(com.navdy.service.library.events.places.FavoriteDestinationsUpdate response) {
        sLogger.v("parsing FavoriteDestinationsUpdate");
        this.lastUpdate = response;
        java.util.List<com.navdy.hud.app.framework.destinations.Destination> destinations = null;
        if (response == null || response.destinations == null) {
            sLogger.w("fav-destination list returned is null");
        } else {
            new com.navdy.service.library.events.MessageStore(com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile().getPreferencesDirectory()).writeMessage(response, FAVORITE_DESTINATIONS_FILENAME);
            destinations = transform(response.destinations);
        }
        setFavoriteDestinations(destinations);
    }

    /* access modifiers changed from: private */
    public void parseDestinations(com.navdy.service.library.events.destination.RecommendedDestinationsUpdate response) {
        sLogger.v("parsing RecommendedDestinationsUpdate");
        if (response.destinations != null) {
            java.util.List<com.navdy.hud.app.framework.destinations.Destination> destinations = transform(response.destinations);
            java.util.List<com.navdy.hud.app.framework.destinations.Destination> recentDestinations2 = new java.util.ArrayList<>();
            java.util.List<com.navdy.hud.app.framework.destinations.Destination> suggestedDestinations2 = new java.util.ArrayList<>();
            if (destinations.size() > 0) {
                for (com.navdy.hud.app.framework.destinations.Destination destination : destinations) {
                    if (destination.recommendation) {
                        suggestedDestinations2.add(destination);
                    } else {
                        recentDestinations2.add(destination);
                    }
                }
            }
            setRecentDestinations(recentDestinations2);
            setSuggestedDestinations(suggestedDestinations2);
            return;
        }
        sLogger.w("rec-destination list returned is null");
    }

    @android.support.annotation.NonNull
    private java.util.List<com.navdy.hud.app.framework.destinations.Destination> transform(java.util.List<com.navdy.service.library.events.destination.Destination> protoDestinations) {
        java.util.List<com.navdy.hud.app.framework.destinations.Destination> destinations = new java.util.ArrayList<>();
        java.util.Iterator it = protoDestinations.iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            destinations.add(transformToInternalDestination((com.navdy.service.library.events.destination.Destination) it.next()));
            if (destinations.size() == 30) {
                sLogger.v("exceeded max size");
                break;
            }
        }
        return destinations;
    }

    public com.navdy.hud.app.framework.destinations.Destination transformToInternalDestination(com.navdy.service.library.events.destination.Destination d) {
        com.navdy.hud.app.framework.destinations.Destination.FavoriteDestinationType favoriteDestinationType;
        if (d == null) {
            return null;
        }
        boolean suggested = java.lang.Boolean.TRUE.equals(d.is_recommendation);
        int color = -1;
        if (d.suggestion_type == null || d.suggestion_type != com.navdy.service.library.events.destination.Destination.SuggestionType.SUGGESTION_CALENDAR) {
            favoriteDestinationType = com.navdy.hud.app.framework.destinations.Destination.FavoriteDestinationType.buildFromValue(d.favorite_type.getValue());
        } else {
            suggested = true;
            color = this.resources.getColor(com.navdy.hud.app.R.color.suggested_dest_cal);
            favoriteDestinationType = com.navdy.hud.app.framework.destinations.Destination.FavoriteDestinationType.FAVORITE_CALENDAR;
        }
        java.lang.String initials = getInitials(d.destination_title, favoriteDestinationType);
        java.lang.String recentTimeLabel = null;
        com.navdy.hud.app.framework.destinations.Destination.PlaceCategory placeCategory = getPlaceCategory(d);
        if (placeCategory == com.navdy.hud.app.framework.destinations.Destination.PlaceCategory.RECENT) {
            recentTimeLabel = getRecentTimeLabel(d);
        }
        if (suggested) {
            color = getSuggestionLabelColor(d, favoriteDestinationType, placeCategory);
        }
        return new com.navdy.hud.app.framework.destinations.Destination(d.navigation_position != null ? d.navigation_position.latitude.doubleValue() : 0.0d, d.navigation_position != null ? d.navigation_position.longitude.doubleValue() : 0.0d, d.display_position != null ? d.display_position.latitude.doubleValue() : 0.0d, d.display_position != null ? d.display_position.longitude.doubleValue() : 0.0d, d.full_address, d.destination_title, d.destination_subtitle, d.identifier, favoriteDestinationType, com.navdy.hud.app.framework.destinations.Destination.DestinationType.DEFAULT, initials, placeCategory, recentTimeLabel, color, suggested, d.destinationIcon != null ? d.destinationIcon.intValue() : 0, d.destinationIconBkColor != null ? d.destinationIconBkColor.intValue() : 0, d.place_id, d.place_type, d.destinationDistance, com.navdy.hud.app.framework.contacts.ContactUtil.fromPhoneNumbers(d.phoneNumbers), com.navdy.hud.app.framework.contacts.ContactUtil.fromContacts(d.contacts));
    }

    public com.navdy.service.library.events.destination.Destination transformToProtoDestination(com.navdy.hud.app.framework.destinations.Destination d) {
        java.lang.Boolean isRecommended = null;
        com.navdy.service.library.events.destination.Destination.FavoriteType favoriteType = com.navdy.service.library.events.destination.Destination.FavoriteType.FAVORITE_NONE;
        com.navdy.service.library.events.destination.Destination.SuggestionType suggestionType = null;
        if (d.favoriteDestinationType != null) {
            switch (d.favoriteDestinationType) {
                case FAVORITE_CALENDAR:
                    suggestionType = com.navdy.service.library.events.destination.Destination.SuggestionType.SUGGESTION_CALENDAR;
                    break;
                default:
                    switch (d.favoriteDestinationType) {
                        case FAVORITE_HOME:
                            favoriteType = com.navdy.service.library.events.destination.Destination.FavoriteType.FAVORITE_HOME;
                            break;
                        case FAVORITE_WORK:
                            favoriteType = com.navdy.service.library.events.destination.Destination.FavoriteType.FAVORITE_WORK;
                            break;
                        case FAVORITE_CONTACT:
                            favoriteType = com.navdy.service.library.events.destination.Destination.FavoriteType.FAVORITE_CONTACT;
                            break;
                        case FAVORITE_CUSTOM:
                            favoriteType = com.navdy.service.library.events.destination.Destination.FavoriteType.FAVORITE_CUSTOM;
                            break;
                    }
            }
        }
        if (d.placeCategory != null) {
            switch (d.placeCategory) {
                case SUGGESTED:
                    isRecommended = java.lang.Boolean.valueOf(true);
                    break;
                case SUGGESTED_RECENT:
                    isRecommended = java.lang.Boolean.valueOf(true);
                    suggestionType = com.navdy.service.library.events.destination.Destination.SuggestionType.SUGGESTION_RECENT;
                    break;
            }
        }
        com.navdy.service.library.events.places.PlaceType placeType = com.navdy.service.library.events.places.PlaceType.PLACE_TYPE_UNKNOWN;
        if (d.placeType != null) {
            placeType = d.placeType;
        }
        com.navdy.service.library.events.destination.Destination.Builder builder = new com.navdy.service.library.events.destination.Destination.Builder().navigation_position(new com.navdy.service.library.events.location.LatLong(java.lang.Double.valueOf(d.navigationPositionLatitude), java.lang.Double.valueOf(d.navigationPositionLongitude))).display_position(new com.navdy.service.library.events.location.LatLong(java.lang.Double.valueOf(d.displayPositionLatitude), java.lang.Double.valueOf(d.displayPositionLongitude))).full_address(d.fullAddress).destination_title(d.destinationTitle).destination_subtitle(d.destinationSubtitle).identifier(d.identifier).is_recommendation(isRecommended).place_type(placeType).suggestion_type(suggestionType).place_id(d.destinationPlaceId).favorite_type(favoriteType).contacts(com.navdy.hud.app.framework.contacts.ContactUtil.toContacts(d.contacts)).phoneNumbers(com.navdy.hud.app.framework.contacts.ContactUtil.toPhoneNumbers(d.phoneNumbers));
        if (d.destinationIcon != 0) {
            builder.destinationIcon(java.lang.Integer.valueOf(d.destinationIcon));
        }
        if (d.destinationIconBkColor != 0) {
            builder.destinationIconBkColor(java.lang.Integer.valueOf(d.destinationIconBkColor));
        }
        if (d.distanceStr != null) {
            builder.destinationDistance(d.distanceStr);
        }
        return builder.build();
    }

    public void requestNavigation(com.navdy.hud.app.framework.destinations.Destination destination) {
        switch (destination.destinationType) {
            case DEFAULT:
                handleDefaultDestination(destination, false);
                return;
            case FIND_GAS:
                handleFindGasDestination(destination);
                return;
            default:
                return;
        }
    }

    public void requestNavigationWithNavLookup(com.navdy.hud.app.framework.destinations.Destination destination) {
        handleDefaultDestination(destination, true);
    }

    private void handleDefaultDestination(com.navdy.hud.app.framework.destinations.Destination destination, boolean navCoordinateLookup) {
        com.navdy.service.library.events.location.Coordinate navCoordinate;
        sLogger.v("handleDefaultDestination [" + destination.toString() + "] lookup=" + navCoordinateLookup);
        if (navCoordinateLookup) {
            if (!com.navdy.hud.app.util.RemoteCapabilitiesUtil.supportsNavigationCoordinateLookup()) {
                sLogger.v("handleDefaultDestination client does not support nav lookup");
            } else if (destination.navigationPositionLatitude == 0.0d || destination.navigationPositionLongitude == 0.0d) {
                sLogger.v("handleDefaultDestination do nav lookup");
                com.navdy.hud.app.maps.here.HereRouteManager.startNavigationLookup(destination);
                return;
            } else {
                sLogger.v("handleDefaultDestination nav coordinate exists already");
            }
        }
        com.navdy.service.library.events.location.Coordinate displayCoordinate = null;
        if (destination.navigationPositionLatitude == 0.0d && destination.navigationPositionLongitude == 0.0d) {
            navCoordinate = new com.navdy.service.library.events.location.Coordinate.Builder().latitude(java.lang.Double.valueOf(destination.displayPositionLatitude)).longitude(java.lang.Double.valueOf(destination.displayPositionLongitude)).build();
        } else {
            navCoordinate = new com.navdy.service.library.events.location.Coordinate.Builder().latitude(java.lang.Double.valueOf(destination.navigationPositionLatitude)).longitude(java.lang.Double.valueOf(destination.navigationPositionLongitude)).build();
            displayCoordinate = new com.navdy.service.library.events.location.Coordinate.Builder().latitude(java.lang.Double.valueOf(destination.displayPositionLatitude)).longitude(java.lang.Double.valueOf(destination.displayPositionLongitude)).build();
        }
        com.navdy.service.library.events.navigation.NavigationRouteRequest.Builder builder = new com.navdy.service.library.events.navigation.NavigationRouteRequest.Builder().destination(navCoordinate).label(destination.destinationTitle).streetAddress(destination.fullAddress).destination_identifier(destination.identifier).requestDestination(transformToProtoDestination(destination)).originDisplay(java.lang.Boolean.valueOf(true)).geoCodeStreetAddress(java.lang.Boolean.valueOf(false)).destinationType(getDestinationType(destination.favoriteDestinationType)).requestId(java.util.UUID.randomUUID().toString());
        if (displayCoordinate != null) {
            builder.destinationDisplay(displayCoordinate);
        }
        com.navdy.service.library.events.navigation.NavigationRouteRequest request = builder.build();
        sLogger.v("launched navigation route request");
        this.bus.post(new com.navdy.hud.app.event.RemoteEvent(request));
        this.bus.post(request);
    }

    private void handleFindGasDestination(com.navdy.hud.app.framework.destinations.Destination destination) {
        com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().removeNotification(com.navdy.hud.app.framework.fuel.FuelRoutingManager.LOW_FUEL_ID);
        com.navdy.hud.app.framework.fuel.FuelRoutingManager fuelRoutingManager = com.navdy.hud.app.framework.fuel.FuelRoutingManager.getInstance();
        fuelRoutingManager.showFindingGasStationToast();
        fuelRoutingManager.findGasStations(-1, false);
    }

    private com.navdy.service.library.events.destination.Destination.FavoriteType getDestinationType(com.navdy.hud.app.framework.destinations.Destination.FavoriteDestinationType type) {
        switch (type) {
            case FAVORITE_HOME:
                return com.navdy.service.library.events.destination.Destination.FavoriteType.FAVORITE_HOME;
            case FAVORITE_WORK:
                return com.navdy.service.library.events.destination.Destination.FavoriteType.FAVORITE_WORK;
            case FAVORITE_CUSTOM:
                return com.navdy.service.library.events.destination.Destination.FavoriteType.FAVORITE_CUSTOM;
            default:
                return com.navdy.service.library.events.destination.Destination.FavoriteType.FAVORITE_NONE;
        }
    }

    public void goToSuggestedDestination() {
        if (this.suggestedDestination == null) {
            sLogger.e("gotToSuggestedDestination : Suggested destination is null, cannot to navigate");
            return;
        }
        com.navdy.service.library.events.destination.Destination destination = this.suggestedDestination.destination;
        if (destination == null) {
            sLogger.e("gotToSuggestedDestination : Destination in Suggested destination is null, cannot to navigate");
        } else if (com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().isNavigationModeOn()) {
            sLogger.e("gotToSuggestedDestination: Cannot navigate as the user is already navigating");
        } else {
            com.navdy.service.library.events.navigation.NavigationRouteRequest routeRequest = getNavigationRouteRequestForDestination(destination);
            sLogger.v("launched navigation route request");
            this.bus.post(new com.navdy.hud.app.event.RemoteEvent(routeRequest));
            this.bus.post(routeRequest);
        }
    }

    public static java.lang.String getInitials(java.lang.String text, com.navdy.hud.app.framework.destinations.Destination.FavoriteDestinationType favoriteDestinationType) {
        if (favoriteDestinationType != null) {
            switch (favoriteDestinationType) {
                case FAVORITE_NONE:
                case FAVORITE_CUSTOM:
                    return getDestinationInitials(text);
                case FAVORITE_CONTACT:
                    return com.navdy.hud.app.framework.contacts.ContactUtil.getInitials(text);
            }
        }
        return null;
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    private static java.lang.String getDestinationInitials(java.lang.String text) {
        try {
            if (android.text.TextUtils.isEmpty(text)) {
                return null;
            }
            java.lang.String text2 = com.navdy.hud.app.util.GenericUtil.removePunctuation(text).toUpperCase().trim();
            java.lang.String firstWord = null;
            boolean firstNumber = false;
            int index = text2.indexOf(" ");
            if (index >= 0) {
                firstWord = text2.substring(0, index).trim();
            }
            try {
                java.lang.Integer.parseInt(firstWord != null ? firstWord : text2);
                firstNumber = true;
                if (firstWord == null) {
                    firstWord = text2;
                }
            } catch (Throwable th) {
            }
            if (firstNumber) {
                if (firstWord.length() > 4) {
                    return null;
                }
                return firstWord;
            } else if (firstWord == null) {
                return text2.substring(0, 1);
            } else {
                synchronized (INITIALS_ARRAY) {
                    java.util.StringTokenizer tokenizer = new java.util.StringTokenizer(text2, " ");
                    int count = 0;
                    while (tokenizer.hasMoreElements()) {
                        int count2 = count + 1;
                        INITIALS_ARRAY[count] = ((java.lang.String) tokenizer.nextElement()).charAt(0);
                        if (count2 == 4 && tokenizer.hasMoreElements()) {
                            return null;
                        }
                        count = count2;
                    }
                    java.lang.String str = new java.lang.String(INITIALS_ARRAY, 0, count);
                    return str;
                }
            }
        } catch (Throwable t) {
            sLogger.e(t);
            return null;
        }
    }

    private com.navdy.hud.app.framework.destinations.Destination.PlaceCategory getPlaceCategory(com.navdy.service.library.events.destination.Destination d) {
        boolean suggested = false;
        if (java.lang.Boolean.TRUE.equals(d.is_recommendation)) {
            suggested = true;
        }
        boolean recent = false;
        if (d.suggestion_type != null && d.suggestion_type == com.navdy.service.library.events.destination.Destination.SuggestionType.SUGGESTION_RECENT) {
            recent = true;
        }
        if (suggested && recent) {
            return com.navdy.hud.app.framework.destinations.Destination.PlaceCategory.SUGGESTED_RECENT;
        }
        if (suggested) {
            return com.navdy.hud.app.framework.destinations.Destination.PlaceCategory.SUGGESTED;
        }
        if (recent) {
            return com.navdy.hud.app.framework.destinations.Destination.PlaceCategory.RECENT;
        }
        return null;
    }

    private java.lang.String getRecentTimeLabel(com.navdy.service.library.events.destination.Destination d) {
        if (d.suggestion_type == null || d.suggestion_type != com.navdy.service.library.events.destination.Destination.SuggestionType.SUGGESTION_RECENT || d.last_navigated_to == null || d.last_navigated_to.longValue() <= 0) {
            return null;
        }
        return com.navdy.hud.app.util.DateUtil.getDateLabel(new java.util.Date(d.last_navigated_to.longValue()));
    }

    private int getSuggestionLabelColor(com.navdy.service.library.events.destination.Destination d, com.navdy.hud.app.framework.destinations.Destination.FavoriteDestinationType favoriteDestinationType, com.navdy.hud.app.framework.destinations.Destination.PlaceCategory placeCategory) {
        if (!java.lang.Boolean.TRUE.equals(d.is_recommendation)) {
            return -1;
        }
        android.content.res.Resources resources2 = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        if (placeCategory == com.navdy.hud.app.framework.destinations.Destination.PlaceCategory.SUGGESTED_RECENT) {
            return resources2.getColor(com.navdy.hud.app.R.color.suggested_dest_work);
        }
        switch (favoriteDestinationType) {
            case FAVORITE_HOME:
                return resources2.getColor(com.navdy.hud.app.R.color.suggested_dest_home);
            case FAVORITE_WORK:
                return resources2.getColor(com.navdy.hud.app.R.color.suggested_dest_work);
            case FAVORITE_CONTACT:
                return resources2.getColor(com.navdy.hud.app.R.color.suggested_dest_contact);
            case FAVORITE_CALENDAR:
                return resources2.getColor(com.navdy.hud.app.R.color.suggested_dest_cal);
            default:
                return resources2.getColor(com.navdy.hud.app.R.color.suggested_dest_fav);
        }
    }

    public boolean launchSuggestedDestination() {
        if (this.suggestedDestination != null) {
            sLogger.v("launchSuggestedDestination: suggested destination toast");
            com.navdy.hud.app.framework.destinations.DestinationSuggestionToast.showSuggestion(this.suggestedDestination);
            return true;
        }
        sLogger.v("launchSuggestedDestination: no suggested destination available");
        return false;
    }

    public void clearSuggestedDestination() {
        sLogger.v("clearSuggestedDestination");
        com.navdy.hud.app.framework.destinations.DestinationSuggestionToast.dismissSuggestionToast();
        this.suggestedDestination = null;
    }

    public com.navdy.hud.app.framework.destinations.DestinationsManager.GasDestination getGasDestination() {
        boolean addFirst;
        com.navdy.hud.app.framework.destinations.DestinationsManager.GasDestination gasDestinationContainer = null;
        if (com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getFeatureUtil().isFeatureEnabled(com.navdy.hud.app.util.FeatureUtil.Feature.FUEL_ROUTING) && com.navdy.hud.app.framework.fuel.FuelRoutingManager.getInstance().isAvailable()) {
            com.navdy.hud.app.framework.fuel.FuelRoutingManager fuelRoutingManager = com.navdy.hud.app.framework.fuel.FuelRoutingManager.getInstance();
            if (fuelRoutingManager.isBusy()) {
                sLogger.v("fuel route manager is busy");
            } else {
                sLogger.v("fuel route manager is not busy");
                boolean hasFuelPid = false;
                com.navdy.hud.app.obd.ObdManager obdManager = com.navdy.hud.app.obd.ObdManager.getInstance();
                if (obdManager.isConnected() && obdManager.getFuelLevel() != -1) {
                    hasFuelPid = true;
                }
                com.navdy.hud.app.framework.destinations.Destination gasDestination = com.navdy.hud.app.framework.destinations.Destination.getGasDestination();
                if (!hasFuelPid) {
                    addFirst = false;
                } else if (fuelRoutingManager.getFuelGlanceDismissTime() > 0) {
                    addFirst = true;
                } else {
                    addFirst = false;
                }
                gasDestinationContainer = new com.navdy.hud.app.framework.destinations.DestinationsManager.GasDestination();
                gasDestinationContainer.destination = gasDestination;
                if (addFirst) {
                    gasDestinationContainer.showFirst = true;
                } else {
                    gasDestinationContainer.showFirst = false;
                }
            }
        }
        return gasDestinationContainer;
    }

    public static com.navdy.service.library.events.navigation.NavigationRouteRequest getNavigationRouteRequestForDestination(com.navdy.service.library.events.destination.Destination destination) {
        com.navdy.service.library.events.location.Coordinate navCoordinate;
        com.navdy.service.library.events.location.Coordinate displayCoordinate = null;
        if (destination.navigation_position == null || (destination.navigation_position.latitude.doubleValue() == 0.0d && destination.navigation_position.longitude.doubleValue() == 0.0d)) {
            navCoordinate = new com.navdy.service.library.events.location.Coordinate.Builder().latitude(destination.display_position.latitude).longitude(destination.display_position.longitude).build();
        } else {
            navCoordinate = new com.navdy.service.library.events.location.Coordinate.Builder().latitude(destination.display_position.latitude).longitude(destination.display_position.longitude).build();
            displayCoordinate = new com.navdy.service.library.events.location.Coordinate.Builder().latitude(destination.navigation_position.latitude).longitude(destination.navigation_position.longitude).build();
        }
        com.navdy.service.library.events.navigation.NavigationRouteRequest.Builder builder = new com.navdy.service.library.events.navigation.NavigationRouteRequest.Builder().destination(navCoordinate).label(destination.destination_title).streetAddress(destination.full_address).destination_identifier(destination.identifier).originDisplay(java.lang.Boolean.valueOf(true)).geoCodeStreetAddress(java.lang.Boolean.valueOf(false)).autoNavigate(java.lang.Boolean.valueOf(true)).requestId(java.util.UUID.randomUUID().toString());
        if (displayCoordinate != null) {
            builder.destinationDisplay(displayCoordinate);
        }
        return builder.build();
    }
}
