package com.navdy.hud.app.framework.network;

class DnsCache {
    private java.util.HashMap<java.lang.String, java.lang.String> IPtoHostnameMap = new java.util.HashMap<>();

    DnsCache() {
    }

    public synchronized void addEntry(java.lang.String ip, java.lang.String host) {
        this.IPtoHostnameMap.put(ip, host);
    }

    public synchronized java.lang.String getHostnamefromIP(java.lang.String ip) {
        return (java.lang.String) this.IPtoHostnameMap.get(ip);
    }

    public synchronized void clear(java.lang.String ip, java.lang.String host) {
        this.IPtoHostnameMap.clear();
    }
}
