package com.navdy.hud.app.framework.network;

public class NetworkStatCache {
    private com.navdy.hud.app.framework.network.DnsCache dnsCache;
    private java.util.HashMap<java.lang.Integer, com.navdy.hud.app.framework.network.NetworkStatCache.NetworkStatCacheInfo> networkStatMap = new java.util.HashMap<>();
    private java.util.HashMap<java.lang.String, com.navdy.hud.app.framework.network.NetworkStatCache.NetworkStatCacheInfo> networkStatMapBoot = new java.util.HashMap<>();

    public static class NetworkStatCacheInfo {
        public java.lang.String destIP;
        public int rxBytes;
        public int txBytes;

        NetworkStatCacheInfo() {
        }

        NetworkStatCacheInfo(int txBytes2, int rxBytes2, java.lang.String destIP2) {
            this.txBytes = txBytes2;
            this.rxBytes = rxBytes2;
            this.destIP = destIP2;
        }
    }

    NetworkStatCache(com.navdy.hud.app.framework.network.DnsCache dnsCache2) {
        this.dnsCache = dnsCache2;
    }

    public synchronized void addStat(java.lang.String destIP, int txBytes, int rxBytes, int fd) {
        com.navdy.hud.app.framework.network.NetworkStatCache.NetworkStatCacheInfo stat = (com.navdy.hud.app.framework.network.NetworkStatCache.NetworkStatCacheInfo) this.networkStatMap.get(java.lang.Integer.valueOf(fd));
        if (stat == null) {
            this.networkStatMap.put(java.lang.Integer.valueOf(fd), new com.navdy.hud.app.framework.network.NetworkStatCache.NetworkStatCacheInfo(txBytes, rxBytes, destIP));
        } else {
            stat.txBytes += txBytes;
            stat.rxBytes += rxBytes;
        }
        if (destIP != null) {
            com.navdy.hud.app.framework.network.NetworkStatCache.NetworkStatCacheInfo bootstat = (com.navdy.hud.app.framework.network.NetworkStatCache.NetworkStatCacheInfo) this.networkStatMapBoot.get(destIP);
            if (bootstat == null) {
                this.networkStatMapBoot.put(destIP, new com.navdy.hud.app.framework.network.NetworkStatCache.NetworkStatCacheInfo(txBytes, rxBytes, destIP));
            } else {
                bootstat.txBytes += txBytes;
                bootstat.rxBytes += rxBytes;
            }
        }
    }

    public synchronized void clear() {
        this.networkStatMap.clear();
    }

    public synchronized void dump(com.navdy.service.library.log.Logger logger, boolean clearAll) {
        dumpSessionStat(logger, clearAll);
        dumpBootStat(logger);
    }

    private void dumpSessionStat(com.navdy.service.library.log.Logger logger, boolean clearAll) {
        if (this.networkStatMap.size() != 0) {
            logger.v("dump-session total connections:" + this.networkStatMap.size());
            for (java.lang.Integer intValue : this.networkStatMap.keySet()) {
                int fd = intValue.intValue();
                com.navdy.hud.app.framework.network.NetworkStatCache.NetworkStatCacheInfo info = (com.navdy.hud.app.framework.network.NetworkStatCache.NetworkStatCacheInfo) this.networkStatMap.get(java.lang.Integer.valueOf(fd));
                java.lang.String host = this.dnsCache.getHostnamefromIP(info.destIP);
                if (host == null) {
                    host = info.destIP;
                }
                logger.v("dump-session fd[" + fd + "] dest[" + host + "] tx[" + info.txBytes + "] rx[" + info.rxBytes + "]");
            }
            if (clearAll) {
                clear();
            }
        }
    }

    private void dumpBootStat(com.navdy.service.library.log.Logger logger) {
        if (this.networkStatMapBoot.size() != 0) {
            logger.v("dump-boot total endpoints:" + this.networkStatMapBoot.size());
            for (com.navdy.hud.app.framework.network.NetworkStatCache.NetworkStatCacheInfo bootStat : this.networkStatMapBoot.values()) {
                java.lang.String host = this.dnsCache.getHostnamefromIP(bootStat.destIP);
                if (host == null) {
                    host = bootStat.destIP;
                }
                logger.v("dump-boot  dest[" + host + "] tx[" + bootStat.txBytes + "] rx[" + bootStat.rxBytes + "]");
            }
        }
    }

    public synchronized java.util.List<com.navdy.hud.app.framework.network.NetworkStatCache.NetworkStatCacheInfo> getSessionStat() {
        return getStat(this.networkStatMap.values().iterator());
    }

    public synchronized java.util.List<com.navdy.hud.app.framework.network.NetworkStatCache.NetworkStatCacheInfo> getBootStat() {
        return getStat(this.networkStatMapBoot.values().iterator());
    }

    public synchronized java.util.List<com.navdy.hud.app.framework.network.NetworkStatCache.NetworkStatCacheInfo> getStat(java.util.Iterator<com.navdy.hud.app.framework.network.NetworkStatCache.NetworkStatCacheInfo> iterator) {
        java.util.List<com.navdy.hud.app.framework.network.NetworkStatCache.NetworkStatCacheInfo> list;
        list = new java.util.ArrayList<>();
        while (iterator.hasNext()) {
            com.navdy.hud.app.framework.network.NetworkStatCache.NetworkStatCacheInfo info = (com.navdy.hud.app.framework.network.NetworkStatCache.NetworkStatCacheInfo) iterator.next();
            java.lang.String host = this.dnsCache.getHostnamefromIP(info.destIP);
            if (host == null) {
                host = info.destIP;
            }
            list.add(new com.navdy.hud.app.framework.network.NetworkStatCache.NetworkStatCacheInfo(info.txBytes, info.rxBytes, host));
        }
        return list;
    }
}
