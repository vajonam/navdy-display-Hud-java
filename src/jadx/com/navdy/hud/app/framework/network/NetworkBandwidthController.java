package com.navdy.hud.app.framework.network;

public class NetworkBandwidthController {
    private static final int ACTIVE = ((int) java.util.concurrent.TimeUnit.SECONDS.toMillis(15));
    private static final com.navdy.hud.app.framework.network.NetworkBandwidthController.UserBandwidthSettingChanged BW_SETTING_CHANGED = new com.navdy.hud.app.framework.network.NetworkBandwidthController.UserBandwidthSettingChanged();
    private static final java.lang.String DATA = "data";
    private static final int DATA_COLLECTION_INITIAL_INTERVAL = ((int) java.util.concurrent.TimeUnit.SECONDS.toMillis(25));
    /* access modifiers changed from: private */
    public static final int DATA_COLLECTION_INTERVAL = ((int) java.util.concurrent.TimeUnit.MINUTES.toMillis(5));
    /* access modifiers changed from: private */
    public static final int DATA_COLLECTION_INTERVAL_ENG = ((int) java.util.concurrent.TimeUnit.MINUTES.toMillis(1));
    private static final java.lang.String EVENT = "event";
    private static final java.lang.String EVENT_DNS_INFO = "dnslookup";
    private static final java.lang.String EVENT_NETSTAT = "netstat";
    private static final java.lang.String FD = "fd";
    private static final java.lang.String FROM = "from";
    private static final java.lang.String GOOGLE_DNS_IP = "8.8.4.4";
    private static final java.lang.String HERE_ROUTE_END_POINT_PATTERN = "route.hybrid.api.here.com";
    private static final java.lang.String HOST = "host";
    private static final java.lang.String IP = "ip";
    private static final int NETWORK_STAT_INFO_PORT = 23655;
    private static final java.lang.String PERM_DISABLE_NETWORK = "persist.sys.perm_disable_nw";
    private static final java.lang.String RX = "rx";
    private static final java.lang.String TO = "to";
    private static final java.lang.String TX = "tx";
    private static final boolean networkDisabled = com.navdy.hud.app.util.os.SystemProperties.getBoolean(PERM_DISABLE_NETWORK, false);
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger("NetworkBandwidthControl");
    private static final com.navdy.hud.app.framework.network.NetworkBandwidthController singleton = new com.navdy.hud.app.framework.network.NetworkBandwidthController();
    private com.squareup.otto.Bus bus = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getBus();
    /* access modifiers changed from: private */
    public final java.util.HashMap<com.navdy.hud.app.framework.network.NetworkBandwidthController.Component, com.navdy.hud.app.framework.network.NetworkBandwidthController.ComponentInfo> componentInfoMap = new java.util.HashMap<>();
    private java.lang.Runnable dataCollectionRunnable = new com.navdy.hud.app.framework.network.NetworkBandwidthController.Anon2();
    /* access modifiers changed from: private */
    public java.lang.Runnable dataCollectionRunnableBk = new com.navdy.hud.app.framework.network.NetworkBandwidthController.Anon3();
    /* access modifiers changed from: private */
    public com.navdy.hud.app.framework.network.DnsCache dnsCache = new com.navdy.hud.app.framework.network.DnsCache();
    /* access modifiers changed from: private */
    public android.os.Handler handler = new android.os.Handler(android.os.Looper.getMainLooper());
    private boolean limitBandwidthModeOn;
    private java.lang.Runnable networkStatRunnable = new com.navdy.hud.app.framework.network.NetworkBandwidthController.Anon1();
    private java.lang.Thread networkStatThread;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.framework.network.NetworkStatCache statCache = new com.navdy.hud.app.framework.network.NetworkStatCache(this.dnsCache);
    /* access modifiers changed from: private */
    public boolean trafficDataDownloadedOnce;
    /* access modifiers changed from: private */
    public final java.util.HashMap<java.lang.String, com.navdy.hud.app.framework.network.NetworkBandwidthController.Component> urlToComponentMap = new java.util.HashMap<>();

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        /* JADX WARNING: Removed duplicated region for block: B:11:0x0070 A[Catch:{ Throwable -> 0x008d, Throwable -> 0x009e }] */
        /* JADX WARNING: Removed duplicated region for block: B:30:0x00d5 A[Catch:{ Throwable -> 0x008d, Throwable -> 0x009e }] */
        /* JADX WARNING: Removed duplicated region for block: B:41:0x014a A[Catch:{ Throwable -> 0x008d, Throwable -> 0x009e }] */
        public void run() {
            java.net.DatagramSocket socket = null;
            try {
                com.navdy.hud.app.framework.network.NetworkBandwidthController.sLogger.v("networkStat thread enter");
                java.net.DatagramSocket datagramSocket = new java.net.DatagramSocket(com.navdy.hud.app.framework.network.NetworkBandwidthController.NETWORK_STAT_INFO_PORT, java.net.InetAddress.getByName("127.0.0.1"));
                try {
                    datagramSocket.setReceiveBufferSize(16384);
                    byte[] data = new byte[2048];
                    java.net.DatagramPacket datagramPacket = new java.net.DatagramPacket(data, data.length);
                    while (true) {
                        datagramSocket.receive(datagramPacket);
                        java.lang.String str = new java.lang.String(data, 0, datagramPacket.getLength());
                        org.json.JSONObject jSONObject = new org.json.JSONObject(str);
                        java.lang.String event = jSONObject.getString("event");
                        char c = 65535;
                        switch (event.hashCode()) {
                            case 260369379:
                                if (event.equals(com.navdy.hud.app.framework.network.NetworkBandwidthController.EVENT_DNS_INFO)) {
                                    c = 0;
                                }
                            case 1843370353:
                                if (event.equals(com.navdy.hud.app.framework.network.NetworkBandwidthController.EVENT_NETSTAT)) {
                                    c = 1;
                                }
                                switch (c) {
                                    case 0:
                                        java.lang.String host = jSONObject.getString(com.navdy.hud.app.framework.network.NetworkBandwidthController.HOST);
                                        if ("localhost".equals(host)) {
                                            break;
                                        } else {
                                            org.json.JSONArray ips = jSONObject.getJSONArray(com.navdy.hud.app.framework.network.NetworkBandwidthController.IP);
                                            int nIps = ips.length();
                                            if (nIps == 0) {
                                                break;
                                            } else {
                                                for (int i = 0; i < nIps; i++) {
                                                    java.lang.String ip = ips.getString(i);
                                                    com.navdy.hud.app.framework.network.NetworkBandwidthController.this.dnsCache.addEntry(ip, host);
                                                    if (com.navdy.hud.app.framework.network.NetworkBandwidthController.sLogger.isLoggable(2)) {
                                                        com.navdy.hud.app.framework.network.NetworkBandwidthController.sLogger.v("dnslookup " + host + " : " + ip);
                                                    }
                                                }
                                                break;
                                            }
                                        }
                                    case 1:
                                        org.json.JSONArray netStatData = jSONObject.getJSONArray(com.navdy.hud.app.framework.network.NetworkBandwidthController.DATA);
                                        if (netStatData.length() == 0) {
                                            break;
                                        } else {
                                            org.json.JSONObject dataObj = netStatData.getJSONObject(0);
                                            java.lang.String from = dataObj.getString("from");
                                            java.lang.String to = dataObj.getString(com.navdy.hud.app.framework.network.NetworkBandwidthController.TO);
                                            int tx = dataObj.getInt(com.navdy.hud.app.framework.network.NetworkBandwidthController.TX);
                                            int rx = dataObj.getInt(com.navdy.hud.app.framework.network.NetworkBandwidthController.RX);
                                            int fd = dataObj.getInt(com.navdy.hud.app.framework.network.NetworkBandwidthController.FD);
                                            if (com.navdy.hud.app.framework.network.NetworkBandwidthController.sLogger.isLoggable(2)) {
                                                com.navdy.hud.app.framework.network.NetworkBandwidthController.sLogger.v("[" + fd + "] tx[" + tx + "] rx[" + rx + "] from[" + from + "] to[" + to + "]");
                                            }
                                            java.lang.String destIP = to;
                                            if (android.text.TextUtils.isEmpty(destIP)) {
                                                destIP = from;
                                            }
                                            if (com.navdy.hud.app.framework.network.NetworkBandwidthController.GOOGLE_DNS_IP.equals(destIP)) {
                                                break;
                                            } else {
                                                com.navdy.hud.app.framework.network.NetworkBandwidthController.this.statCache.addStat(destIP, tx, rx, fd);
                                                if (tx <= 0 && rx <= 0) {
                                                    break;
                                                } else {
                                                    java.lang.String dnsHost = com.navdy.hud.app.framework.network.NetworkBandwidthController.this.dnsCache.getHostnamefromIP(destIP);
                                                    if (dnsHost == null) {
                                                        break;
                                                    } else {
                                                        com.navdy.hud.app.framework.network.NetworkBandwidthController.Component component = (com.navdy.hud.app.framework.network.NetworkBandwidthController.Component) com.navdy.hud.app.framework.network.NetworkBandwidthController.this.urlToComponentMap.get(dnsHost);
                                                        if (component == null && dnsHost.contains(com.navdy.hud.app.framework.network.NetworkBandwidthController.HERE_ROUTE_END_POINT_PATTERN)) {
                                                            component = com.navdy.hud.app.framework.network.NetworkBandwidthController.Component.HERE_ROUTE;
                                                        }
                                                        if (component != null) {
                                                            if (!com.navdy.hud.app.framework.network.NetworkBandwidthController.this.trafficDataDownloadedOnce && component == com.navdy.hud.app.framework.network.NetworkBandwidthController.Component.HERE_TRAFFIC && rx > 0) {
                                                                com.navdy.hud.app.framework.network.NetworkBandwidthController.sLogger.i("traffic data downloaded once");
                                                                com.navdy.hud.app.framework.network.NetworkBandwidthController.this.trafficDataDownloadedOnce = true;
                                                            }
                                                            com.navdy.hud.app.framework.network.NetworkBandwidthController.ComponentInfo componentInfo = (com.navdy.hud.app.framework.network.NetworkBandwidthController.ComponentInfo) com.navdy.hud.app.framework.network.NetworkBandwidthController.this.componentInfoMap.get(component);
                                                            synchronized (componentInfo) {
                                                                componentInfo.lastActivity = android.os.SystemClock.elapsedRealtime();
                                                            }
                                                            break;
                                                        } else {
                                                            break;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    default:
                                        com.navdy.hud.app.framework.network.NetworkBandwidthController.sLogger.v("invalid command:" + event);
                                        break;
                                }
                        }
                        switch (c) {
                            case 0:
                                break;
                            case 1:
                                break;
                        }
                    }
                } catch (Throwable th) {
                    t = th;
                    socket = datagramSocket;
                }
            } catch (Throwable th2) {
                t = th2;
                com.navdy.hud.app.framework.network.NetworkBandwidthController.sLogger.e(t);
                if (socket != null) {
                    com.navdy.service.library.util.IOUtils.closeStream(socket);
                }
                com.navdy.hud.app.framework.network.NetworkBandwidthController.sLogger.v("networkStat thread exit");
            }
        }
    }

    class Anon2 implements java.lang.Runnable {
        Anon2() {
        }

        public void run() {
            com.navdy.service.library.task.TaskManager.getInstance().execute(com.navdy.hud.app.framework.network.NetworkBandwidthController.this.dataCollectionRunnableBk, 1);
        }
    }

    class Anon3 implements java.lang.Runnable {
        Anon3() {
        }

        public void run() {
            try {
                com.navdy.hud.app.framework.network.NetworkBandwidthController.this.statCache.dump(com.navdy.hud.app.framework.network.NetworkBandwidthController.sLogger, true);
                com.navdy.hud.app.framework.network.NetworkBandwidthController.this.handler.postDelayed(this, com.navdy.hud.app.util.DeviceUtil.isUserBuild() ? (long) com.navdy.hud.app.framework.network.NetworkBandwidthController.DATA_COLLECTION_INTERVAL : (long) com.navdy.hud.app.framework.network.NetworkBandwidthController.DATA_COLLECTION_INTERVAL_ENG);
            } catch (Throwable th) {
                com.navdy.hud.app.framework.network.NetworkBandwidthController.this.handler.postDelayed(this, com.navdy.hud.app.util.DeviceUtil.isUserBuild() ? (long) com.navdy.hud.app.framework.network.NetworkBandwidthController.DATA_COLLECTION_INTERVAL : (long) com.navdy.hud.app.framework.network.NetworkBandwidthController.DATA_COLLECTION_INTERVAL_ENG);
                throw th;
            }
        }
    }

    public enum Component {
        LOCALYTICS,
        HERE_ROUTE,
        HERE_TRAFFIC,
        HERE_MAP_DOWNLOAD,
        HERE_REVERSE_GEO,
        HOCKEY,
        JIRA
    }

    private static class ComponentInfo {
        public long lastActivity;

        private ComponentInfo() {
        }

        /* synthetic */ ComponentInfo(com.navdy.hud.app.framework.network.NetworkBandwidthController.Anon1 x0) {
            this();
        }
    }

    public static class UserBandwidthSettingChanged {
    }

    static {
        if (networkDisabled) {
            sLogger.v("n/w disabled permanently");
        }
    }

    public static com.navdy.hud.app.framework.network.NetworkBandwidthController getInstance() {
        return singleton;
    }

    private NetworkBandwidthController() {
        this.urlToComponentMap.put("v102-62-30-8.route.hybrid.api.here.com", com.navdy.hud.app.framework.network.NetworkBandwidthController.Component.HERE_ROUTE);
        this.urlToComponentMap.put("tpeg.traffic.api.here.com", com.navdy.hud.app.framework.network.NetworkBandwidthController.Component.HERE_TRAFFIC);
        this.urlToComponentMap.put("tpeg.hybrid.api.here.com", com.navdy.hud.app.framework.network.NetworkBandwidthController.Component.HERE_TRAFFIC);
        this.urlToComponentMap.put("download.hybrid.api.here.com", com.navdy.hud.app.framework.network.NetworkBandwidthController.Component.HERE_MAP_DOWNLOAD);
        this.urlToComponentMap.put("reverse.geocoder.api.here.com", com.navdy.hud.app.framework.network.NetworkBandwidthController.Component.HERE_REVERSE_GEO);
        this.urlToComponentMap.put("analytics.localytics.com", com.navdy.hud.app.framework.network.NetworkBandwidthController.Component.LOCALYTICS);
        this.urlToComponentMap.put("profile.localytics.com", com.navdy.hud.app.framework.network.NetworkBandwidthController.Component.LOCALYTICS);
        this.urlToComponentMap.put("sdk.hockeyapp.net", com.navdy.hud.app.framework.network.NetworkBandwidthController.Component.HOCKEY);
        this.urlToComponentMap.put("navdyhud.atlassian.net", com.navdy.hud.app.framework.network.NetworkBandwidthController.Component.JIRA);
        this.componentInfoMap.put(com.navdy.hud.app.framework.network.NetworkBandwidthController.Component.LOCALYTICS, new com.navdy.hud.app.framework.network.NetworkBandwidthController.ComponentInfo(null));
        this.componentInfoMap.put(com.navdy.hud.app.framework.network.NetworkBandwidthController.Component.HERE_ROUTE, new com.navdy.hud.app.framework.network.NetworkBandwidthController.ComponentInfo(null));
        this.componentInfoMap.put(com.navdy.hud.app.framework.network.NetworkBandwidthController.Component.HERE_TRAFFIC, new com.navdy.hud.app.framework.network.NetworkBandwidthController.ComponentInfo(null));
        this.componentInfoMap.put(com.navdy.hud.app.framework.network.NetworkBandwidthController.Component.HERE_MAP_DOWNLOAD, new com.navdy.hud.app.framework.network.NetworkBandwidthController.ComponentInfo(null));
        this.componentInfoMap.put(com.navdy.hud.app.framework.network.NetworkBandwidthController.Component.HERE_REVERSE_GEO, new com.navdy.hud.app.framework.network.NetworkBandwidthController.ComponentInfo(null));
        this.componentInfoMap.put(com.navdy.hud.app.framework.network.NetworkBandwidthController.Component.HOCKEY, new com.navdy.hud.app.framework.network.NetworkBandwidthController.ComponentInfo(null));
        this.componentInfoMap.put(com.navdy.hud.app.framework.network.NetworkBandwidthController.Component.JIRA, new com.navdy.hud.app.framework.network.NetworkBandwidthController.ComponentInfo(null));
        this.networkStatThread = new java.lang.Thread(this.networkStatRunnable);
        this.networkStatThread.setName("hudNetStatThread");
        this.networkStatThread.start();
        sLogger.v("networkStat thread started");
        this.handler.postDelayed(this.dataCollectionRunnable, (long) DATA_COLLECTION_INITIAL_INTERVAL);
        this.limitBandwidthModeOn = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getDriverProfileManager().getCurrentProfile().isLimitBandwidthModeOn();
        this.bus.register(this);
        sLogger.v("registered bus");
    }

    public void netStat() {
        this.handler.removeCallbacks(this.dataCollectionRunnable);
        this.dataCollectionRunnable.run();
    }

    public long getLastActivityTime(com.navdy.hud.app.framework.network.NetworkBandwidthController.Component component) {
        long j;
        com.navdy.hud.app.framework.network.NetworkBandwidthController.ComponentInfo componentInfo = (com.navdy.hud.app.framework.network.NetworkBandwidthController.ComponentInfo) this.componentInfoMap.get(component);
        if (componentInfo == null) {
            return 0;
        }
        synchronized (componentInfo) {
            j = componentInfo.lastActivity;
        }
        return j;
    }

    public boolean isTrafficDataDownloadedOnce() {
        return this.trafficDataDownloadedOnce;
    }

    public boolean isNetworkAccessAllowed(com.navdy.hud.app.framework.network.NetworkBandwidthController.Component component) {
        if (!com.navdy.hud.app.framework.network.NetworkStateManager.isConnectedToNetwork(com.navdy.hud.app.HudApplication.getAppContext())) {
            sLogger.v("n/w access: not connected to network:" + component);
            return false;
        }
        switch (component) {
            case LOCALYTICS:
            case JIRA:
            case HOCKEY:
                if (isComponentActive(com.navdy.hud.app.framework.network.NetworkBandwidthController.Component.HERE_ROUTE)) {
                    sLogger.v("n/w access not allowed:" + component);
                    return false;
                } else if (isComponentActive(com.navdy.hud.app.framework.network.NetworkBandwidthController.Component.HERE_TRAFFIC)) {
                    sLogger.v("n/w access not allowed:" + component);
                    return false;
                } else {
                    sLogger.v("n/w access allowed:" + component);
                    return true;
                }
            default:
                sLogger.v("n/w access allowed:" + component);
                return true;
        }
    }

    private boolean isComponentActive(com.navdy.hud.app.framework.network.NetworkBandwidthController.Component component) {
        long l = getLastActivityTime(component);
        if (l > 0) {
            long diff = android.os.SystemClock.elapsedRealtime() - l;
            if (diff <= ((long) ACTIVE)) {
                sLogger.v("component is ACTIVE:" + component + " diff:" + diff);
                return true;
            }
        }
        return false;
    }

    public java.util.List<com.navdy.hud.app.framework.network.NetworkStatCache.NetworkStatCacheInfo> getSessionStat() {
        return this.statCache.getSessionStat();
    }

    public java.util.List<com.navdy.hud.app.framework.network.NetworkStatCache.NetworkStatCacheInfo> getBootStat() {
        return this.statCache.getBootStat();
    }

    public boolean isNetworkDisabled() {
        return networkDisabled;
    }

    public boolean isLimitBandwidthModeOn() {
        return this.limitBandwidthModeOn;
    }

    @com.squareup.otto.Subscribe
    public void onDriverProfileChanged(com.navdy.hud.app.event.DriverProfileChanged profileChanged) {
        sLogger.v("driver profile changed");
        handleBandwidthPreferenceChange();
    }

    @com.squareup.otto.Subscribe
    public void onDriverProfileUpdated(com.navdy.hud.app.event.DriverProfileUpdated event) {
        sLogger.v("driver profile updated");
        if (event.state == com.navdy.hud.app.event.DriverProfileUpdated.State.UPDATED) {
            handleBandwidthPreferenceChange();
        }
    }

    private void handleBandwidthPreferenceChange() {
        boolean val = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getDriverProfileManager().getCurrentProfile().isLimitBandwidthModeOn();
        if (val == this.limitBandwidthModeOn) {
            sLogger.v("limitbandwidth: no-op current[" + this.limitBandwidthModeOn + "] new [" + val + "]");
            return;
        }
        sLogger.v("limitbandwidth: changed current[" + this.limitBandwidthModeOn + "] new [" + val + "]");
        this.limitBandwidthModeOn = val;
        if (val) {
            sLogger.v("limitbandwidth: on");
        } else {
            sLogger.v("limitbandwidth: off");
        }
        com.navdy.hud.app.maps.here.HereMapsManager.getInstance().handleBandwidthPreferenceChange();
        this.bus.post(BW_SETTING_CHANGED);
    }
}
