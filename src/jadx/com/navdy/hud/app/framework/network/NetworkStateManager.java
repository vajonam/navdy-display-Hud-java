package com.navdy.hud.app.framework.network;

public class NetworkStateManager {
    private static final java.lang.String NAVDY_NETWORK_DOWN_INTENT = "com.navdy.hud.NetworkDown";
    private static final java.lang.String NAVDY_NETWORK_UP_INTENT = "com.navdy.hud.NetworkUp";
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.framework.network.NetworkStateManager.class);
    private static final com.navdy.hud.app.framework.network.NetworkStateManager singleton = new com.navdy.hud.app.framework.network.NetworkStateManager();
    @javax.inject.Inject
    com.squareup.otto.Bus bus;
    /* access modifiers changed from: private */
    public int countDown = 3;
    /* access modifiers changed from: private */
    public android.os.Handler handler = new android.os.Handler(android.os.Looper.getMainLooper());
    /* access modifiers changed from: private */
    public com.navdy.service.library.events.settings.NetworkStateChange lastPhoneNetworkState;
    /* access modifiers changed from: private */
    public java.lang.Runnable networkCheck = new com.navdy.hud.app.framework.network.NetworkStateManager.Anon2();
    /* access modifiers changed from: private */
    public volatile boolean networkStateInitialized;
    /* access modifiers changed from: private */
    public java.lang.Runnable triggerNetworkCheck = new com.navdy.hud.app.framework.network.NetworkStateManager.Anon1();

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            if (!com.navdy.hud.app.framework.network.NetworkStateManager.this.networkStateInitialized) {
                com.navdy.service.library.task.TaskManager.getInstance().execute(com.navdy.hud.app.framework.network.NetworkStateManager.this.networkCheck, 10);
            }
        }
    }

    class Anon2 implements java.lang.Runnable {
        Anon2() {
        }

        /* JADX WARNING: Code restructure failed: missing block: B:16:0x004f, code lost:
            com.navdy.hud.app.framework.network.NetworkStateManager.access$Anon200().v("found dummy network we are up:" + r2[r1].getTypeName() + " count down=" + com.navdy.hud.app.framework.network.NetworkStateManager.access$Anon300(r10.this$Anon0));
            com.navdy.hud.app.framework.network.NetworkStateManager.access$Anon306(r10.this$Anon0);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:17:0x008a, code lost:
            if (com.navdy.hud.app.framework.network.NetworkStateManager.access$Anon300(r10.this$Anon0) <= 0) goto L_0x00a4;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:19:0x0092, code lost:
            if (com.navdy.hud.app.framework.network.NetworkStateManager.access$Anon000(r10.this$Anon0) != false) goto L_?;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:20:0x0094, code lost:
            com.navdy.hud.app.framework.network.NetworkStateManager.access$Anon600(r10.this$Anon0).postDelayed(com.navdy.hud.app.framework.network.NetworkStateManager.access$Anon500(r10.this$Anon0), 1000);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:22:?, code lost:
            com.navdy.hud.app.framework.network.NetworkStateManager.access$Anon200().v("found dummy network starting init");
            com.navdy.hud.app.framework.network.NetworkStateManager.access$Anon002(r10.this$Anon0, true);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:23:0x00bb, code lost:
            if (com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().isRemoteDeviceConnected() == false) goto L_0x013f;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:25:0x00c5, code lost:
            if (com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().isNetworkLinkReady() == false) goto L_0x013f;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:27:0x00cd, code lost:
            if (com.navdy.hud.app.framework.network.NetworkStateManager.access$Anon400(r10.this$Anon0) == null) goto L_0x00dd;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:29:0x00db, code lost:
            if (com.navdy.hud.app.framework.network.NetworkStateManager.access$Anon400(r10.this$Anon0).networkAvailable.booleanValue() != false) goto L_0x0118;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:30:0x00dd, code lost:
            r10.this$Anon0.networkNotAvailable();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:31:0x00e2, code lost:
            r10.this$Anon0.bus.post(new com.navdy.hud.app.framework.network.NetworkStateManager.HudNetworkInitialized());
         */
        /* JADX WARNING: Code restructure failed: missing block: B:39:?, code lost:
            r10.this$Anon0.networkAvailable();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:47:?, code lost:
            r10.this$Anon0.networkNotAvailable();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:58:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:59:?, code lost:
            return;
         */
        public void run() {
            com.navdy.hud.app.framework.network.NetworkStateManager.sLogger.v("checking n/w state");
            android.net.NetworkInfo[] networkInfos = ((android.net.ConnectivityManager) com.navdy.hud.app.HudApplication.getAppContext().getSystemService("connectivity")).getAllNetworkInfo();
            if (networkInfos == null || networkInfos.length == 0) {
                com.navdy.hud.app.framework.network.NetworkStateManager.sLogger.v("no n/w info");
                if (!com.navdy.hud.app.framework.network.NetworkStateManager.this.networkStateInitialized) {
                    com.navdy.hud.app.framework.network.NetworkStateManager.this.handler.postDelayed(com.navdy.hud.app.framework.network.NetworkStateManager.this.triggerNetworkCheck, 1000);
                    return;
                }
                return;
            }
            int i = 0;
            while (true) {
                try {
                    if (i >= networkInfos.length) {
                        break;
                    } else if (networkInfos[i].getType() == 8) {
                        break;
                    } else {
                        i++;
                    }
                } catch (Throwable th) {
                    if (!com.navdy.hud.app.framework.network.NetworkStateManager.this.networkStateInitialized) {
                        com.navdy.hud.app.framework.network.NetworkStateManager.this.handler.postDelayed(com.navdy.hud.app.framework.network.NetworkStateManager.this.triggerNetworkCheck, 1000);
                    }
                    throw th;
                }
            }
            if (!com.navdy.hud.app.framework.network.NetworkStateManager.this.networkStateInitialized) {
                com.navdy.hud.app.framework.network.NetworkStateManager.sLogger.v("network state not initialized");
            }
            if (!com.navdy.hud.app.framework.network.NetworkStateManager.this.networkStateInitialized) {
                com.navdy.hud.app.framework.network.NetworkStateManager.this.handler.postDelayed(com.navdy.hud.app.framework.network.NetworkStateManager.this.triggerNetworkCheck, 1000);
            }
        }
    }

    public static class HudNetworkInitialized {
    }

    static /* synthetic */ int access$Anon306(com.navdy.hud.app.framework.network.NetworkStateManager x0) {
        int i = x0.countDown - 1;
        x0.countDown = i;
        return i;
    }

    public static com.navdy.hud.app.framework.network.NetworkStateManager getInstance() {
        return singleton;
    }

    private NetworkStateManager() {
        mortar.Mortar.inject(com.navdy.hud.app.HudApplication.getAppContext(), this);
        this.bus.register(this);
        if (com.navdy.hud.app.util.DeviceUtil.isNavdyDevice()) {
            this.handler.post(this.triggerNetworkCheck);
            return;
        }
        this.networkStateInitialized = true;
        this.bus.post(new com.navdy.hud.app.framework.network.NetworkStateManager.HudNetworkInitialized());
    }

    @com.squareup.otto.Subscribe
    public void onNetworkStateChange(com.navdy.service.library.events.settings.NetworkStateChange networkStateChange) {
        try {
            this.lastPhoneNetworkState = networkStateChange;
            sLogger.v("NetworkStateChange available[" + networkStateChange.networkAvailable + "] cell[" + networkStateChange.cellNetwork + "] wifi[" + networkStateChange.wifiNetwork + "] reach[" + networkStateChange.reachability + "]");
            if (java.lang.Boolean.TRUE.equals(networkStateChange.networkAvailable)) {
                sLogger.v("DummyNetNetworkFactory:phone n/w avaialble");
                networkAvailable();
                return;
            }
            sLogger.v("DummyNetNetworkFactory:phone n/w lost");
            networkNotAvailable();
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }

    @com.squareup.otto.Subscribe
    public void onConnectionStateChange(com.navdy.service.library.events.connection.ConnectionStateChange event) {
        sLogger.d("ConnectionStateChange - " + event.state);
        switch (event.state) {
            case CONNECTION_DISCONNECTED:
                this.lastPhoneNetworkState = null;
                return;
            default:
                return;
        }
    }

    public com.navdy.service.library.events.settings.NetworkStateChange getLastPhoneNetworkState() {
        return this.lastPhoneNetworkState;
    }

    public void networkAvailable() {
        if (!this.networkStateInitialized) {
            sLogger.v("network not initialized yet");
        } else if (com.navdy.hud.app.framework.network.NetworkBandwidthController.getInstance().isNetworkDisabled()) {
            sLogger.i("network is permanently disabled");
            networkNotAvailable();
        } else {
            com.navdy.hud.app.HudApplication.getAppContext().sendBroadcastAsUser(new android.content.Intent(NAVDY_NETWORK_UP_INTENT), android.os.Process.myUserHandle());
            sLogger.v("DummyNetNetworkFactory:sent:com.navdy.hud.NetworkUp");
        }
    }

    public void networkNotAvailable() {
        if (!this.networkStateInitialized) {
            sLogger.v("network not initialized yet");
            return;
        }
        com.navdy.hud.app.HudApplication.getAppContext().sendBroadcastAsUser(new android.content.Intent(NAVDY_NETWORK_DOWN_INTENT), android.os.Process.myUserHandle());
        sLogger.v("DummyNetNetworkFactory:sent:com.navdy.hud.NetworkDown");
    }

    public static boolean isConnectedToNetwork(android.content.Context context) {
        android.net.NetworkInfo activeNetwork = ((android.net.ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        sLogger.v("setEngineOnlineState active network=" + (activeNetwork != null ? activeNetwork.getTypeName() : " no active network "));
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    public static boolean isConnectedToWifi(android.content.Context context) {
        android.net.NetworkInfo wifiNetworkInfo = ((android.net.ConnectivityManager) context.getSystemService("connectivity")).getNetworkInfo(1);
        if (wifiNetworkInfo == null || !wifiNetworkInfo.isConnected()) {
            return false;
        }
        return true;
    }

    public static boolean isConnectedToCellNetwork(android.content.Context context) {
        android.net.NetworkInfo wifiNetworkInfo = ((android.net.ConnectivityManager) context.getSystemService("connectivity")).getNetworkInfo(0);
        if (wifiNetworkInfo == null || !wifiNetworkInfo.isConnected()) {
            return false;
        }
        return true;
    }

    public boolean isNetworkStateInitialized() {
        return this.networkStateInitialized;
    }
}
