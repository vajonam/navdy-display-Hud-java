package com.navdy.hud.app.framework.network;

public final class NetworkStateManager$$InjectAdapter extends dagger.internal.Binding<com.navdy.hud.app.framework.network.NetworkStateManager> implements dagger.MembersInjector<com.navdy.hud.app.framework.network.NetworkStateManager> {
    private dagger.internal.Binding<com.squareup.otto.Bus> bus;

    public NetworkStateManager$$InjectAdapter() {
        super(null, "members/com.navdy.hud.app.framework.network.NetworkStateManager", false, com.navdy.hud.app.framework.network.NetworkStateManager.class);
    }

    public void attach(dagger.internal.Linker linker) {
        this.bus = linker.requestBinding("com.squareup.otto.Bus", com.navdy.hud.app.framework.network.NetworkStateManager.class, getClass().getClassLoader());
    }

    public void getDependencies(java.util.Set<dagger.internal.Binding<?>> set, java.util.Set<dagger.internal.Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.bus);
    }

    public void injectMembers(com.navdy.hud.app.framework.network.NetworkStateManager object) {
        object.bus = (com.squareup.otto.Bus) this.bus.get();
    }
}
