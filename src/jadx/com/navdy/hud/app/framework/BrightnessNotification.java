package com.navdy.hud.app.framework;

public class BrightnessNotification extends android.database.ContentObserver implements com.navdy.hud.app.framework.notifications.INotification, android.content.SharedPreferences.OnSharedPreferenceChangeListener {
    private static final int DEFAULT_AUTO_BRIGHTNESS_ADJUSTMENT = java.lang.Integer.parseInt("0");
    private static final int DEFAULT_BRIGHTNESS_INT = java.lang.Integer.parseInt("128");
    private static final long DETENT_TIMEOUT = 250;
    private static final android.os.Handler HANDLER = new android.os.Handler();
    private static final int INITIAL_CHANGING_STEP = 1;
    private static final int MAX_BRIGHTNESS = 255;
    private static final int MAX_STEP = 16;
    private static final int NOTIFICATION_TIMEOUT = 10000;
    private static final android.net.Uri SCREEN_BRIGHTNESS_SETTING_URI = android.provider.Settings.System.getUriFor("screen_brightness");
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.framework.BrightnessNotification.class);
    private final boolean AUTO_BRIGHTNESS_ADJ_ENABLED;
    private java.lang.String autoSuffix;
    private int changingStep;
    private com.navdy.hud.app.ui.component.ChoiceLayout2 choiceLayout;
    private android.view.ViewGroup container;
    private com.navdy.hud.app.framework.notifications.INotificationController controller;
    private int currentAdjustmentValue;
    private int currentValue;
    private boolean isAutoBrightnessOn;
    private boolean isLastChangeIncreasing;
    private long lastChangeActionTime;
    private int previousAdjustmentValue;
    private int previousBrightnessValue;
    private com.navdy.hud.app.view.Gauge progressBar;
    private android.content.res.Resources resources;
    private android.content.SharedPreferences sharedPreferences;
    private android.content.SharedPreferences.Editor sharedPreferencesEditor;
    private boolean showAutoBrightnessAdjustmentUI;
    private android.widget.TextView textIndicator;

    class Anon1 implements com.navdy.hud.app.ui.component.ChoiceLayout2.IListener {
        Anon1() {
        }

        public void executeItem(com.navdy.hud.app.ui.component.ChoiceLayout2.Selection selection) {
            com.navdy.hud.app.framework.BrightnessNotification.this.close();
        }

        public void itemSelected(com.navdy.hud.app.ui.component.ChoiceLayout2.Selection selection) {
        }
    }

    public BrightnessNotification() {
        super(new android.os.Handler());
        this.autoSuffix = "";
        this.changingStep = 1;
        this.lastChangeActionTime = -251;
        this.isLastChangeIncreasing = false;
        this.showAutoBrightnessAdjustmentUI = false;
        this.AUTO_BRIGHTNESS_ADJ_ENABLED = true;
        this.sharedPreferences = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getSharedPreferences();
        this.sharedPreferencesEditor = this.sharedPreferences.edit();
        this.isAutoBrightnessOn = getCurrentIsAutoBrightnessOn();
        this.showAutoBrightnessAdjustmentUI = this.isAutoBrightnessOn;
        this.resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        this.autoSuffix = this.resources.getString(com.navdy.hud.app.R.string.brightness_auto_suffix);
    }

    public void onChange(boolean selfChange, android.net.Uri uri) {
        if (!selfChange && SCREEN_BRIGHTNESS_SETTING_URI.equals(uri)) {
            updateState();
        }
    }

    public com.navdy.hud.app.framework.notifications.NotificationType getType() {
        return com.navdy.hud.app.framework.notifications.NotificationType.BRIGHTNESS;
    }

    public java.lang.String getId() {
        return com.navdy.hud.app.framework.notifications.NotificationId.BRIGHTNESS_NOTIFICATION_ID;
    }

    public android.view.View getView(android.content.Context context) {
        if (this.container == null) {
            if (this.showAutoBrightnessAdjustmentUI) {
                this.container = (android.view.ViewGroup) android.view.LayoutInflater.from(context).inflate(com.navdy.hud.app.R.layout.notification_auto_brightness, null);
            } else {
                this.container = (android.view.ViewGroup) android.view.LayoutInflater.from(context).inflate(com.navdy.hud.app.R.layout.notification_brightness, null);
            }
            this.textIndicator = (android.widget.TextView) this.container.findViewById(com.navdy.hud.app.R.id.subTitle);
            this.progressBar = (com.navdy.hud.app.view.Gauge) this.container.findViewById(com.navdy.hud.app.R.id.circle_progress);
            this.progressBar.setAnimated(false);
            showFixedChoices(this.container);
        }
        return this.container;
    }

    public android.view.View getExpandedView(android.content.Context context, java.lang.Object data) {
        return null;
    }

    public int getExpandedViewIndicatorColor() {
        return 0;
    }

    private void showFixedChoices(android.view.ViewGroup container2) {
        this.choiceLayout = (com.navdy.hud.app.ui.component.ChoiceLayout2) container2.findViewById(com.navdy.hud.app.R.id.choiceLayout);
        java.util.List<com.navdy.hud.app.ui.component.ChoiceLayout2.Choice> choices = new java.util.ArrayList<>(3);
        android.content.res.Resources resources2 = container2.getResources();
        java.lang.String dismiss = resources2.getString(com.navdy.hud.app.R.string.call_dismiss);
        int okColor = resources2.getColor(com.navdy.hud.app.R.color.glance_ok_blue);
        choices.add(new com.navdy.hud.app.ui.component.ChoiceLayout2.Choice(2, com.navdy.hud.app.R.drawable.icon_glances_ok, okColor, com.navdy.hud.app.R.drawable.icon_glances_ok, -16777216, dismiss, okColor));
        this.choiceLayout.setChoices(choices, 1, new com.navdy.hud.app.framework.BrightnessNotification.Anon1());
        this.choiceLayout.setVisibility(0);
    }

    public void onStart(com.navdy.hud.app.framework.notifications.INotificationController controller2) {
        com.navdy.hud.app.HudApplication.getAppContext().getContentResolver().registerContentObserver(SCREEN_BRIGHTNESS_SETTING_URI, false, this);
        this.controller = controller2;
        this.sharedPreferences.registerOnSharedPreferenceChangeListener(this);
        this.previousBrightnessValue = getCurrentBrightness();
        this.previousAdjustmentValue = getCurrentAutoBrigthnessAdjustment();
        updateState();
    }

    public void updateState() {
        if (this.controller == null) {
            sLogger.v("brightness notif offscreen");
            return;
        }
        this.currentValue = getCurrentBrightness();
        sLogger.v("Current brightness :" + this.currentValue);
        if (!this.showAutoBrightnessAdjustmentUI) {
            android.widget.TextView textView = this.textIndicator;
            android.content.res.Resources resources2 = this.resources;
            java.lang.Object[] objArr = new java.lang.Object[3];
            objArr[0] = java.lang.Integer.valueOf(this.currentValue);
            objArr[1] = java.lang.Integer.valueOf(255);
            objArr[2] = this.isAutoBrightnessOn ? this.autoSuffix : "";
            textView.setText(resources2.getString(com.navdy.hud.app.R.string.brightness_notification_subtitle, objArr));
            this.progressBar.setValue(this.currentValue);
            return;
        }
        this.currentAdjustmentValue = getCurrentAutoBrigthnessAdjustment();
        this.progressBar.setValue(this.currentAdjustmentValue + 64);
        if (this.currentValue >= 255) {
            this.textIndicator.setText(com.navdy.hud.app.R.string.max);
        } else if (this.currentValue <= 0) {
            this.textIndicator.setText(com.navdy.hud.app.R.string.minimum);
        } else {
            this.textIndicator.setText(null);
        }
    }

    public void onUpdate() {
        if (this.controller != null) {
            this.controller.resetTimeout();
        }
        updateState();
    }

    public void onStop() {
        com.navdy.hud.app.HudApplication.getAppContext().getContentResolver().unregisterContentObserver(this);
        this.sharedPreferences.unregisterOnSharedPreferenceChangeListener(this);
        this.controller = null;
    }

    public int getTimeout() {
        return 10000;
    }

    public boolean isAlive() {
        return false;
    }

    public boolean isPurgeable() {
        return false;
    }

    public boolean canAddToStackIfCurrentExists() {
        return false;
    }

    public int getColor() {
        return 0;
    }

    public void onNotificationEvent(com.navdy.hud.app.ui.framework.UIStateManager.Mode mode) {
    }

    public void onExpandedNotificationEvent(com.navdy.hud.app.ui.framework.UIStateManager.Mode mode) {
    }

    public void onExpandedNotificationSwitched() {
    }

    public android.animation.AnimatorSet getViewSwitchAnimation(boolean viewIn) {
        return null;
    }

    public boolean expandNotification() {
        return false;
    }

    public boolean supportScroll() {
        return false;
    }

    public static void showNotification() {
        com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().addNotification(new com.navdy.hud.app.framework.BrightnessNotification());
    }

    /* access modifiers changed from: private */
    public void close() {
        com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().removeNotification(getId());
        if (this.isAutoBrightnessOn) {
            com.navdy.hud.app.analytics.AnalyticsSupport.recordAutobrightnessAdjustmentChanged(this.previousBrightnessValue, this.previousAdjustmentValue, getCurrentAutoBrigthnessAdjustment());
        } else {
            com.navdy.hud.app.analytics.AnalyticsSupport.recordBrightnessChanged(this.previousBrightnessValue, getCurrentBrightness());
        }
    }

    public void onClick() {
        close();
    }

    public void onTrackHand(float normalized) {
    }

    public boolean onGesture(com.navdy.service.library.events.input.GestureEvent event) {
        return false;
    }

    private void detentChangingStep(boolean isChangeIncreasing) {
        long now = android.os.SystemClock.elapsedRealtime();
        if (isChangeIncreasing != this.isLastChangeIncreasing || this.lastChangeActionTime + 250 < now) {
            this.changingStep = 1;
        } else {
            this.changingStep <<= 1;
            if (this.changingStep > 16) {
                this.changingStep = 16;
            }
        }
        this.isLastChangeIncreasing = isChangeIncreasing;
        this.lastChangeActionTime = now;
    }

    public boolean onKey(com.navdy.hud.app.manager.InputManager.CustomKeyEvent event) {
        if (this.controller == null) {
            return false;
        }
        if (event != com.navdy.hud.app.manager.InputManager.CustomKeyEvent.SELECT) {
            this.controller.resetTimeout();
        }
        switch (event) {
            case SELECT:
                if (this.choiceLayout != null) {
                    this.choiceLayout.executeSelectedItem();
                } else {
                    close();
                }
                return true;
            case LEFT:
                sLogger.v("Decreasing brightness");
                detentChangingStep(false);
                updateBrightness(-this.changingStep);
                return true;
            case RIGHT:
                sLogger.v("Increasing brightness");
                detentChangingStep(true);
                updateBrightness(this.changingStep);
                return true;
            default:
                return false;
        }
    }

    public com.navdy.hud.app.manager.InputManager.IInputHandler nextHandler() {
        return null;
    }

    private boolean getCurrentIsAutoBrightnessOn() {
        return "true".equals(this.sharedPreferences.getString(com.navdy.hud.app.settings.HUDSettings.AUTO_BRIGHTNESS, ""));
    }

    private int getCurrentBrightness() {
        if (this.isAutoBrightnessOn) {
            try {
                return android.provider.Settings.System.getInt(com.navdy.hud.app.HudApplication.getAppContext().getContentResolver(), "screen_brightness");
            } catch (android.provider.Settings.SettingNotFoundException e) {
                sLogger.e("Settings not found exception ", e);
                return DEFAULT_BRIGHTNESS_INT;
            }
        } else {
            try {
                return java.lang.Integer.parseInt(this.sharedPreferences.getString(com.navdy.hud.app.settings.HUDSettings.BRIGHTNESS, "128"));
            } catch (java.lang.NumberFormatException e2) {
                sLogger.e("Cannot parse brightness from the shared preferences", e2);
                return DEFAULT_BRIGHTNESS_INT;
            }
        }
    }

    private int getCurrentAutoBrigthnessAdjustment() {
        try {
            return java.lang.Integer.parseInt(this.sharedPreferences.getString(com.navdy.hud.app.settings.HUDSettings.AUTO_BRIGHTNESS_ADJUSTMENT, "0"));
        } catch (java.lang.NumberFormatException e) {
            sLogger.e("Cannot parse auto brightness adjustment from the shared preferences", e);
            return DEFAULT_AUTO_BRIGHTNESS_ADJUSTMENT;
        }
    }

    public void onSharedPreferenceChanged(android.content.SharedPreferences sharedPreferences2, java.lang.String key) {
        if (key.equals(com.navdy.hud.app.settings.HUDSettings.BRIGHTNESS) || key.equals(com.navdy.hud.app.settings.HUDSettings.AUTO_BRIGHTNESS_ADJUSTMENT)) {
            updateState();
        } else if (key.equals(com.navdy.hud.app.settings.HUDSettings.AUTO_BRIGHTNESS)) {
            boolean newValue = "true".equals(key);
            if (this.isAutoBrightnessOn != newValue) {
                this.isAutoBrightnessOn = newValue;
                updateState();
            }
        }
    }

    private void updateBrightness(int delta) {
        if (delta != 0) {
            if (this.showAutoBrightnessAdjustmentUI) {
                int newValue = this.currentAdjustmentValue + delta;
                if (newValue < -64) {
                    newValue = -64;
                }
                if (newValue > 64) {
                    newValue = 64;
                }
                this.sharedPreferencesEditor.putString(com.navdy.hud.app.settings.HUDSettings.AUTO_BRIGHTNESS_ADJUSTMENT, java.lang.String.valueOf(newValue));
                this.sharedPreferencesEditor.apply();
                return;
            }
            int newValue2 = this.currentValue + delta;
            if (newValue2 < 0) {
                newValue2 = 0;
            }
            if (newValue2 > 255) {
                newValue2 = 255;
            }
            this.sharedPreferencesEditor.putString(com.navdy.hud.app.settings.HUDSettings.BRIGHTNESS, java.lang.String.valueOf(newValue2));
            this.sharedPreferencesEditor.apply();
        }
    }
}
