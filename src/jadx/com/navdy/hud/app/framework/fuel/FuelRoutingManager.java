package com.navdy.hud.app.framework.fuel;

public final class FuelRoutingManager {
    private static final int CANCEL_NOTIFICATION_THRESHOLD = 20;
    private static final int DEFAULT_LOW_FUEL_THRESHOLD = 15;
    public static final java.lang.String FINDING_FAST_STATION_TOAST_ID = "toast#find#gas";
    /* access modifiers changed from: private */
    public static final long FUEL_GLANCE_REDISPLAY_THRESHOLD = java.util.concurrent.TimeUnit.MINUTES.toMillis(TEST_SIMULATION_SPEED);
    public static final com.here.android.mpa.search.CategoryFilter GAS_CATEGORY = new com.here.android.mpa.search.CategoryFilter();
    private static final java.lang.String GAS_STATION_CATEGORY = "petrol-station";
    public static final java.lang.String LOW_FUEL_ID = com.navdy.hud.app.framework.glance.GlanceHelper.getNotificationId(com.navdy.service.library.events.glances.GlanceEvent.GlanceType.GLANCE_TYPE_FUEL, LOW_FUEL_ID_STR);
    private static final java.lang.String LOW_FUEL_ID_STR = "low#fuel#level";
    private static final int N_GAS_STATIONS_REQUESTED = 3;
    private static final int ONE_MINUTE_SECONDS = 60;
    private static final long TEST_SIMULATION_SPEED = 20;
    private static com.navdy.hud.app.framework.fuel.FuelRoutingManager sInstance = new com.navdy.hud.app.framework.fuel.FuelRoutingManager();
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.framework.fuel.FuelRoutingManager.class);
    /* access modifiers changed from: private */
    public boolean available;
    /* access modifiers changed from: private */
    public final com.squareup.otto.Bus bus = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getBus();
    /* access modifiers changed from: private */
    public com.here.android.mpa.search.Place currentGasStation;
    /* access modifiers changed from: private */
    public int currentLowFuelThreshold = 15;
    /* access modifiers changed from: private */
    public com.navdy.service.library.events.navigation.NavigationRouteRequest currentNavigationRouteRequest;
    /* access modifiers changed from: private */
    public java.util.ArrayList<com.navdy.service.library.events.navigation.NavigationRouteResult> currentOutgoingResults;
    /* access modifiers changed from: private */
    public long fuelGlanceDismissTime;
    /* access modifiers changed from: private */
    public com.here.android.mpa.routing.RouteOptions gasRouteOptions;
    /* access modifiers changed from: private */
    public final android.os.Handler handler = new android.os.Handler(android.os.Looper.getMainLooper());
    /* access modifiers changed from: private */
    public final com.navdy.hud.app.maps.here.HereMapsManager hereMapsManager = com.navdy.hud.app.maps.here.HereMapsManager.getInstance();
    /* access modifiers changed from: private */
    public com.navdy.hud.app.maps.here.HereNavigationManager hereNavigationManager;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.maps.here.HereRouteCalculator hereRouteCalculator;
    /* access modifiers changed from: private */
    public boolean isCalculatingFuelRoute;
    /* access modifiers changed from: private */
    public boolean isInTestMode = false;
    /* access modifiers changed from: private */
    public final com.navdy.hud.app.obd.ObdManager obdManager = com.navdy.hud.app.obd.ObdManager.getInstance();

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            com.navdy.hud.app.framework.fuel.FuelRoutingManager.sLogger.v("routeToGasStation");
            if (!com.navdy.hud.app.framework.fuel.FuelRoutingManager.this.available) {
                com.navdy.hud.app.framework.fuel.FuelRoutingManager.sLogger.w("Fuel manager not available");
            } else if (com.navdy.hud.app.framework.fuel.FuelRoutingManager.this.currentOutgoingResults == null || com.navdy.hud.app.framework.fuel.FuelRoutingManager.this.currentOutgoingResults.size() == 0) {
                com.navdy.hud.app.framework.fuel.FuelRoutingManager.sLogger.w("outgoing results not available");
                com.navdy.hud.app.framework.fuel.FuelRoutingManager.this.reset();
            } else if (com.navdy.hud.app.framework.fuel.FuelRoutingManager.this.hereNavigationManager.isNavigationModeOn()) {
                com.navdy.hud.app.framework.fuel.FuelRoutingManager.sLogger.v("navon: saving current non-gas route to storage");
                long simulationSpeed = com.navdy.hud.app.framework.fuel.FuelRoutingManager.this.isInTestMode ? com.navdy.hud.app.framework.fuel.FuelRoutingManager.TEST_SIMULATION_SPEED : -1;
                if (com.navdy.hud.app.framework.fuel.FuelRoutingManager.this.hereNavigationManager.hasArrived()) {
                    com.navdy.hud.app.framework.fuel.FuelRoutingManager.sLogger.v("navon: already arrived, set ignore flag");
                    com.navdy.hud.app.framework.fuel.FuelRoutingManager.this.hereNavigationManager.setIgnoreArrived(true);
                }
                com.here.android.mpa.guidance.NavigationManager.Error navError = com.navdy.hud.app.framework.fuel.FuelRoutingManager.this.hereNavigationManager.addNewRoute(com.navdy.hud.app.framework.fuel.FuelRoutingManager.this.currentOutgoingResults, com.navdy.service.library.events.navigation.NavigationSessionRouteChange.RerouteReason.NAV_SESSION_FUEL_REROUTE, com.navdy.hud.app.framework.fuel.FuelRoutingManager.this.currentNavigationRouteRequest, simulationSpeed);
                if (navError == com.here.android.mpa.guidance.NavigationManager.Error.NONE) {
                    com.navdy.hud.app.framework.fuel.FuelRoutingManager.sLogger.v("navon:gas route navigation started");
                    return;
                }
                com.navdy.hud.app.framework.fuel.FuelRoutingManager.sLogger.e("navon:arrival route navigation started failed:" + navError);
                com.navdy.hud.app.framework.fuel.FuelRoutingManager.this.reset();
                com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().removeNotification(com.navdy.hud.app.framework.fuel.FuelRoutingManager.LOW_FUEL_ID);
            } else {
                com.navdy.service.library.events.navigation.NavigationRouteResult result = (com.navdy.service.library.events.navigation.NavigationRouteResult) com.navdy.hud.app.framework.fuel.FuelRoutingManager.this.currentOutgoingResults.get(0);
                if (com.navdy.hud.app.maps.here.HereRouteCache.getInstance().getRoute(result.routeId) == null) {
                    com.navdy.hud.app.framework.fuel.FuelRoutingManager.sLogger.w("navoff:route not available in cache:" + result.routeId);
                    com.navdy.hud.app.framework.fuel.FuelRoutingManager.this.reset();
                    return;
                }
                com.navdy.hud.app.framework.fuel.FuelRoutingManager.this.hereNavigationManager.handleNavigationSessionRequest(new com.navdy.service.library.events.navigation.NavigationSessionRequest(com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_STARTED, result.label, result.routeId, java.lang.Integer.valueOf(0), java.lang.Boolean.valueOf(true)));
                com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().removeNotification(com.navdy.hud.app.framework.fuel.FuelRoutingManager.LOW_FUEL_ID);
                com.navdy.hud.app.framework.fuel.FuelRoutingManager.sLogger.v("navoff:gas route navigation started");
            }
        }
    }

    class Anon2 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.hud.app.framework.fuel.FuelRoutingManager.OnNearestGasStationCallback val$callback;

        class Anon1 implements com.navdy.hud.app.framework.fuel.FuelRoutingManager.OnRouteToGasStationCallback {
            Anon1() {
            }

            public void onOptimalRouteCalculationComplete(com.navdy.hud.app.framework.fuel.FuelRoutingManager.RouteCacheItem optimal, java.util.ArrayList<com.navdy.service.library.events.navigation.NavigationRouteResult> outgoingResults, com.navdy.service.library.events.navigation.NavigationRouteRequest navigationRouteRequest) {
                com.navdy.hud.app.framework.fuel.FuelRoutingManager.Anon2.this.val$callback.onComplete((com.navdy.service.library.events.navigation.NavigationRouteResult) outgoingResults.get(0));
            }

            public void onError(com.navdy.hud.app.framework.fuel.FuelRoutingManager.OnRouteToGasStationCallback.Error error) {
                com.navdy.hud.app.framework.fuel.FuelRoutingManager.Anon2.this.val$callback.onError(com.navdy.hud.app.framework.fuel.FuelRoutingManager.OnRouteToGasStationCallback.Error.RESPONSE_ERROR);
            }
        }

        Anon2(com.navdy.hud.app.framework.fuel.FuelRoutingManager.OnNearestGasStationCallback onNearestGasStationCallback) {
            this.val$callback = onNearestGasStationCallback;
        }

        public void run() {
            if (!com.navdy.hud.app.framework.fuel.FuelRoutingManager.this.available) {
                com.navdy.hud.app.framework.fuel.FuelRoutingManager.sLogger.w("Fuel manager not available");
            } else if (this.val$callback == null) {
                throw new java.lang.IllegalArgumentException();
            } else if (com.navdy.hud.app.framework.fuel.FuelRoutingManager.this.isCalculatingFuelRoute) {
                this.val$callback.onError(com.navdy.hud.app.framework.fuel.FuelRoutingManager.OnRouteToGasStationCallback.Error.INVALID_STATE);
            } else {
                com.navdy.hud.app.framework.fuel.FuelRoutingManager.this.isCalculatingFuelRoute = true;
                com.navdy.hud.app.framework.fuel.FuelRoutingManager.this.calculateGasStationRoutes(new com.navdy.hud.app.framework.fuel.FuelRoutingManager.Anon2.Anon1());
            }
        }
    }

    class Anon3 implements java.lang.Runnable {
        Anon3() {
        }

        public void run() {
            int fuelLevel = com.navdy.hud.app.framework.fuel.FuelRoutingManager.this.obdManager.getFuelLevel();
            if (!com.navdy.hud.app.framework.fuel.FuelRoutingManager.this.isInTestMode && fuelLevel == -1) {
                com.navdy.hud.app.framework.fuel.FuelRoutingManager.sLogger.d("checkFuelLevel:fuel level is not available from OBD Manager");
                com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().removeNotification(com.navdy.hud.app.framework.fuel.FuelRoutingManager.LOW_FUEL_ID);
            } else if (com.navdy.hud.app.framework.fuel.FuelRoutingManager.this.isCalculatingFuelRoute) {
                com.navdy.hud.app.framework.fuel.FuelRoutingManager.sLogger.d("checkFuelLevel: already calculating fuel route");
            } else if (com.navdy.hud.app.framework.fuel.FuelRoutingManager.this.hereNavigationManager.isOnGasRoute()) {
                com.navdy.hud.app.framework.fuel.FuelRoutingManager.sLogger.d("checkFuelLevel: already on fuel route");
            } else {
                if (com.navdy.hud.app.framework.fuel.FuelRoutingManager.this.fuelGlanceDismissTime > 0) {
                    long elapsed = android.os.SystemClock.elapsedRealtime() - com.navdy.hud.app.framework.fuel.FuelRoutingManager.this.fuelGlanceDismissTime;
                    if (elapsed < com.navdy.hud.app.framework.fuel.FuelRoutingManager.FUEL_GLANCE_REDISPLAY_THRESHOLD) {
                        com.navdy.hud.app.framework.fuel.FuelRoutingManager.sLogger.d("checkFuelLevel: fuel glance dismiss threshold still valid:" + elapsed);
                        return;
                    } else {
                        com.navdy.hud.app.framework.fuel.FuelRoutingManager.sLogger.d("checkFuelLevel: fuel glance dismiss threshold expired:" + elapsed);
                        com.navdy.hud.app.framework.fuel.FuelRoutingManager.this.fuelGlanceDismissTime = 0;
                    }
                }
                com.navdy.hud.app.framework.fuel.FuelRoutingManager.sLogger.i("checkFuelLevel: Fuel level:" + fuelLevel + " threshold:" + com.navdy.hud.app.framework.fuel.FuelRoutingManager.this.currentLowFuelThreshold + " testMode:" + com.navdy.hud.app.framework.fuel.FuelRoutingManager.this.isInTestMode);
                if (com.navdy.hud.app.framework.fuel.FuelRoutingManager.this.isInTestMode || fuelLevel <= com.navdy.hud.app.framework.fuel.FuelRoutingManager.this.currentLowFuelThreshold) {
                    if (!com.navdy.hud.app.framework.fuel.FuelRoutingManager.this.isInTestMode && !com.navdy.hud.app.framework.glance.GlanceHelper.isFuelNotificationEnabled()) {
                        com.navdy.hud.app.framework.fuel.FuelRoutingManager.sLogger.i("checkFuelLevel:fuel notifications are not enabled");
                    } else if (com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().isNotificationPresent(com.navdy.hud.app.framework.fuel.FuelRoutingManager.LOW_FUEL_ID)) {
                        com.navdy.hud.app.framework.fuel.FuelRoutingManager.sLogger.i("checkFuelLevel:low fuel glance already shown to user");
                    } else {
                        com.navdy.hud.app.framework.fuel.FuelRoutingManager.this.findGasStations(fuelLevel, true);
                    }
                } else if (fuelLevel > 20) {
                    com.navdy.hud.app.framework.fuel.FuelRoutingManager.this.setFuelLevelBackToNormal();
                } else if (com.navdy.hud.app.framework.fuel.FuelRoutingManager.sLogger.isLoggable(2)) {
                    com.navdy.hud.app.framework.fuel.FuelRoutingManager.sLogger.i("checkFuelLevel: no-op");
                }
            }
        }
    }

    class Anon4 implements com.navdy.hud.app.maps.here.HerePlacesManager.OnCategoriesSearchListener {
        final /* synthetic */ com.here.android.mpa.common.GeoCoordinate val$gasCalculationCoords;
        final /* synthetic */ com.navdy.hud.app.framework.fuel.FuelRoutingManager.OnRouteToGasStationCallback val$onRouteToGasStationCallback;

        class Anon1 implements java.util.Comparator<com.here.android.mpa.search.Place> {
            Anon1() {
            }

            public int compare(com.here.android.mpa.search.Place lhs, com.here.android.mpa.search.Place rhs) {
                return (int) (com.navdy.hud.app.framework.fuel.FuelRoutingManager.Anon4.this.val$gasCalculationCoords.distanceTo(lhs.getLocation().getCoordinate()) - com.navdy.hud.app.framework.fuel.FuelRoutingManager.Anon4.this.val$gasCalculationCoords.distanceTo(rhs.getLocation().getCoordinate()));
            }
        }

        Anon4(com.navdy.hud.app.framework.fuel.FuelRoutingManager.OnRouteToGasStationCallback onRouteToGasStationCallback, com.here.android.mpa.common.GeoCoordinate geoCoordinate) {
            this.val$onRouteToGasStationCallback = onRouteToGasStationCallback;
            this.val$gasCalculationCoords = geoCoordinate;
        }

        public void onCompleted(java.util.List<com.here.android.mpa.search.Place> places) {
            com.here.android.mpa.common.GeoCoordinate endPoint;
            try {
                com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
                if (!com.navdy.hud.app.framework.fuel.FuelRoutingManager.this.isCalculatingFuelRoute) {
                    this.val$onRouteToGasStationCallback.onError(com.navdy.hud.app.framework.fuel.FuelRoutingManager.OnRouteToGasStationCallback.Error.INVALID_STATE);
                    return;
                }
                if (places.size() > 1) {
                    java.util.Collections.sort(places, new com.navdy.hud.app.framework.fuel.FuelRoutingManager.Anon4.Anon1());
                }
                for (com.here.android.mpa.search.Place p : places) {
                    com.here.android.mpa.search.Location location = p.getLocation();
                    com.here.android.mpa.common.GeoCoordinate gasNavGeo = com.navdy.hud.app.maps.here.HerePlacesManager.getPlaceEntry(p);
                    java.lang.String address = location.getAddress().toString().replace("<br/>", ", ").replace(com.navdy.hud.app.framework.glance.GlanceConstants.NEWLINE, "");
                    com.navdy.hud.app.framework.fuel.FuelRoutingManager.sLogger.v("gas station name [" + p.getName() + "]" + " address [" + address + "]" + " distance [" + ((int) this.val$gasCalculationCoords.distanceTo(gasNavGeo)) + "] meters" + " displayPos [" + location.getCoordinate() + "]" + " navPos [" + gasNavGeo + "]");
                }
                java.util.Queue<com.navdy.hud.app.framework.fuel.FuelRoutingManager.FuelRoutingAction> routingQueue = new java.util.LinkedList<>();
                java.util.List<com.navdy.hud.app.framework.fuel.FuelRoutingManager.RouteCacheItem> routeCache = new java.util.ArrayList<>(3);
                for (com.here.android.mpa.search.Place place : places) {
                    java.util.List<com.here.android.mpa.common.GeoCoordinate> waypoints = new java.util.ArrayList<>();
                    com.here.android.mpa.common.GeoCoordinate placeEntry = com.navdy.hud.app.maps.here.HerePlacesManager.getPlaceEntry(place);
                    if (com.navdy.hud.app.framework.fuel.FuelRoutingManager.this.hereNavigationManager.isNavigationModeOn()) {
                        waypoints.add(placeEntry);
                        endPoint = com.navdy.hud.app.framework.fuel.FuelRoutingManager.this.hereNavigationManager.getCurrentRoute().getDestination();
                    } else {
                        endPoint = placeEntry;
                    }
                    routingQueue.add(new com.navdy.hud.app.framework.fuel.FuelRoutingManager.FuelRoutingAction(places.size(), place, routingQueue, routeCache, this.val$onRouteToGasStationCallback, this.val$gasCalculationCoords, waypoints, endPoint));
                }
                com.navdy.hud.app.framework.fuel.FuelRoutingManager.sLogger.v("posting to handler");
                com.navdy.hud.app.framework.fuel.FuelRoutingManager.this.handler.post((java.lang.Runnable) routingQueue.poll());
            } catch (Throwable t) {
                com.navdy.hud.app.framework.fuel.FuelRoutingManager.sLogger.e("calculateGasStationRoutes", t);
                this.val$onRouteToGasStationCallback.onError(com.navdy.hud.app.framework.fuel.FuelRoutingManager.OnRouteToGasStationCallback.Error.UNKNOWN_ERROR);
            }
        }

        public void onError(com.navdy.hud.app.maps.here.HerePlacesManager.Error error) {
            com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
            com.navdy.hud.app.framework.fuel.FuelRoutingManager.sLogger.e("calculateGasStationRoutes error:" + error);
            this.val$onRouteToGasStationCallback.onError(com.navdy.hud.app.framework.fuel.FuelRoutingManager.OnRouteToGasStationCallback.Error.RESPONSE_ERROR);
        }
    }

    class Anon5 implements com.navdy.hud.app.maps.here.HereRouteCalculator.RouteCalculatorListener {
        final /* synthetic */ com.navdy.hud.app.framework.fuel.FuelRoutingManager.RouteCacheItem val$gasRoute;
        final /* synthetic */ com.navdy.service.library.events.navigation.NavigationRouteRequest val$navigationRouteRequest;
        final /* synthetic */ com.navdy.hud.app.framework.fuel.FuelRoutingManager.OnRouteToGasStationCallback val$onRouteToGasStationCallback;

        Anon5(com.navdy.hud.app.framework.fuel.FuelRoutingManager.OnRouteToGasStationCallback onRouteToGasStationCallback, com.navdy.hud.app.framework.fuel.FuelRoutingManager.RouteCacheItem routeCacheItem, com.navdy.service.library.events.navigation.NavigationRouteRequest navigationRouteRequest) {
            this.val$onRouteToGasStationCallback = onRouteToGasStationCallback;
            this.val$gasRoute = routeCacheItem;
            this.val$navigationRouteRequest = navigationRouteRequest;
        }

        public void preSuccess() {
        }

        public void postSuccess(java.util.ArrayList<com.navdy.service.library.events.navigation.NavigationRouteResult> outgoingResults) {
            this.val$onRouteToGasStationCallback.onOptimalRouteCalculationComplete(this.val$gasRoute, outgoingResults, this.val$navigationRouteRequest);
        }

        public void error(com.here.android.mpa.routing.RoutingError error, java.lang.Throwable t) {
            this.val$onRouteToGasStationCallback.onError(com.navdy.hud.app.framework.fuel.FuelRoutingManager.OnRouteToGasStationCallback.Error.RESPONSE_ERROR);
        }

        public void progress(int progress) {
            if (com.navdy.hud.app.framework.fuel.FuelRoutingManager.sLogger.isLoggable(2)) {
                com.navdy.hud.app.framework.fuel.FuelRoutingManager.sLogger.v("calculating route to optimal gas station progress %: " + progress);
            }
        }
    }

    class Anon6 implements java.lang.Runnable {
        final /* synthetic */ int val$fuelLevel;
        final /* synthetic */ boolean val$postNotificationOnError;

        class Anon1 implements com.navdy.hud.app.framework.fuel.FuelRoutingManager.OnRouteToGasStationCallback {

            /* renamed from: com.navdy.hud.app.framework.fuel.FuelRoutingManager$Anon6$Anon1$Anon1 reason: collision with other inner class name */
            class C0009Anon1 implements java.lang.Runnable {
                final /* synthetic */ com.navdy.service.library.events.navigation.NavigationRouteRequest val$navigationRouteRequest;
                final /* synthetic */ com.navdy.hud.app.framework.fuel.FuelRoutingManager.RouteCacheItem val$optimal;
                final /* synthetic */ java.util.ArrayList val$outgoingResults;

                C0009Anon1(com.navdy.hud.app.framework.fuel.FuelRoutingManager.RouteCacheItem routeCacheItem, java.util.ArrayList arrayList, com.navdy.service.library.events.navigation.NavigationRouteRequest navigationRouteRequest) {
                    this.val$optimal = routeCacheItem;
                    this.val$outgoingResults = arrayList;
                    this.val$navigationRouteRequest = navigationRouteRequest;
                }

                public void run() {
                    com.navdy.hud.app.framework.fuel.FuelRoutingManager.this.currentGasStation = this.val$optimal.gasStation;
                    com.navdy.hud.app.framework.fuel.FuelRoutingManager.this.isCalculatingFuelRoute = false;
                    com.navdy.hud.app.framework.fuel.FuelRoutingManager.this.currentOutgoingResults = this.val$outgoingResults;
                    com.navdy.hud.app.framework.fuel.FuelRoutingManager.this.currentNavigationRouteRequest = this.val$navigationRouteRequest;
                    java.lang.String distanceString = java.lang.String.valueOf(((double) java.lang.Math.round((this.val$optimal.gasStation.getLocation().getCoordinate().distanceTo(com.navdy.hud.app.framework.fuel.FuelRoutingManager.this.hereMapsManager.getLocationFixManager().getLastGeoCoordinate()) * 10.0d) / 1609.34d)) / 10.0d);
                    java.lang.String addressString = this.val$optimal.gasStation.getLocation().getAddress().getText().split("<br/>")[0];
                    java.util.List<com.navdy.service.library.events.glances.KeyValue> data = new java.util.ArrayList<>(1);
                    if (com.navdy.hud.app.framework.fuel.FuelRoutingManager.Anon6.this.val$fuelLevel != -1) {
                        data.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.FuelConstants.FUEL_LEVEL.name(), java.lang.String.valueOf(com.navdy.hud.app.framework.fuel.FuelRoutingManager.this.obdManager.getFuelLevel())));
                    }
                    data.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.FuelConstants.GAS_STATION_NAME.name(), this.val$optimal.gasStation.getName()));
                    data.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.FuelConstants.GAS_STATION_ADDRESS.name(), addressString));
                    data.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.FuelConstants.GAS_STATION_DISTANCE.name(), distanceString));
                    com.navdy.hud.app.framework.fuel.FuelRoutingManager.this.bus.post(new com.navdy.service.library.events.glances.GlanceEvent.Builder().glanceType(com.navdy.service.library.events.glances.GlanceEvent.GlanceType.GLANCE_TYPE_FUEL).id(com.navdy.hud.app.framework.fuel.FuelRoutingManager.LOW_FUEL_ID_STR).postTime(java.lang.Long.valueOf(java.lang.System.currentTimeMillis())).provider(com.navdy.hud.app.framework.glance.GlanceHelper.FUEL_PACKAGE).glanceData(data).build());
                    if (com.navdy.hud.app.framework.fuel.FuelRoutingManager.sLogger.isLoggable(2)) {
                        com.navdy.hud.app.framework.fuel.FuelRoutingManager.sLogger.v("state is now LOW_FUEL, posting low fuel glance");
                    }
                }
            }

            class Anon2 implements java.lang.Runnable {
                final /* synthetic */ com.navdy.hud.app.framework.fuel.FuelRoutingManager.OnRouteToGasStationCallback.Error val$error;

                Anon2(com.navdy.hud.app.framework.fuel.FuelRoutingManager.OnRouteToGasStationCallback.Error error) {
                    this.val$error = error;
                }

                public void run() {
                    com.navdy.hud.app.framework.fuel.FuelRoutingManager.sLogger.w("received an error on calculateGasStationRoutes " + this.val$error.name());
                    com.navdy.hud.app.framework.fuel.FuelRoutingManager.this.isCalculatingFuelRoute = false;
                    if (com.navdy.hud.app.framework.fuel.FuelRoutingManager.Anon6.this.val$postNotificationOnError) {
                        java.util.List<com.navdy.service.library.events.glances.KeyValue> data = new java.util.ArrayList<>(1);
                        data.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.FuelConstants.FUEL_LEVEL.name(), java.lang.String.valueOf(com.navdy.hud.app.framework.fuel.FuelRoutingManager.Anon6.this.val$fuelLevel)));
                        data.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.FuelConstants.NO_ROUTE.name(), ""));
                        com.navdy.hud.app.framework.fuel.FuelRoutingManager.this.bus.post(new com.navdy.service.library.events.glances.GlanceEvent.Builder().glanceType(com.navdy.service.library.events.glances.GlanceEvent.GlanceType.GLANCE_TYPE_FUEL).id(com.navdy.hud.app.framework.fuel.FuelRoutingManager.LOW_FUEL_ID_STR).postTime(java.lang.Long.valueOf(java.lang.System.currentTimeMillis())).provider(com.navdy.hud.app.framework.glance.GlanceHelper.FUEL_PACKAGE).glanceData(data).build());
                        if (com.navdy.hud.app.framework.fuel.FuelRoutingManager.sLogger.isLoggable(2)) {
                            com.navdy.hud.app.framework.fuel.FuelRoutingManager.sLogger.v("posting low fuel glance with no routes");
                        }
                        com.navdy.hud.app.framework.fuel.FuelRoutingManager.this.reset();
                    }
                }
            }

            Anon1() {
            }

            public void onOptimalRouteCalculationComplete(com.navdy.hud.app.framework.fuel.FuelRoutingManager.RouteCacheItem optimal, java.util.ArrayList<com.navdy.service.library.events.navigation.NavigationRouteResult> outgoingResults, com.navdy.service.library.events.navigation.NavigationRouteRequest navigationRouteRequest) {
                com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.framework.fuel.FuelRoutingManager.Anon6.Anon1.C0009Anon1(optimal, outgoingResults, navigationRouteRequest), 21);
            }

            public void onError(com.navdy.hud.app.framework.fuel.FuelRoutingManager.OnRouteToGasStationCallback.Error error) {
                com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.framework.fuel.FuelRoutingManager.Anon6.Anon1.Anon2(error), 21);
            }
        }

        Anon6(int i, boolean z) {
            this.val$fuelLevel = i;
            this.val$postNotificationOnError = z;
        }

        public void run() {
            if (!com.navdy.hud.app.framework.fuel.FuelRoutingManager.this.available) {
                com.navdy.hud.app.framework.fuel.FuelRoutingManager.sLogger.w("Fuel manager not available");
                return;
            }
            com.navdy.hud.app.framework.fuel.FuelRoutingManager.this.isCalculatingFuelRoute = true;
            com.navdy.hud.app.framework.fuel.FuelRoutingManager.this.calculateGasStationRoutes(new com.navdy.hud.app.framework.fuel.FuelRoutingManager.Anon6.Anon1());
        }
    }

    class Anon7 implements java.lang.Runnable {
        Anon7() {
        }

        public void run() {
            com.navdy.hud.app.framework.fuel.FuelRoutingManager.this.checkFuelLevel();
        }
    }

    public static class ClearTestObdLowFuelLevel {
    }

    public static class FuelAddedTestEvent {
    }

    private class FuelRoutingAction implements java.lang.Runnable {
        /* access modifiers changed from: private */
        public int currentRouteCacheSize;
        private final com.here.android.mpa.common.GeoCoordinate endPoint;
        /* access modifiers changed from: private */
        public final com.navdy.hud.app.framework.fuel.FuelRoutingManager.OnRouteToGasStationCallback onRouteToGasStationCallback;
        /* access modifiers changed from: private */
        public final com.here.android.mpa.search.Place place;
        /* access modifiers changed from: private */
        public final java.util.List<com.navdy.hud.app.framework.fuel.FuelRoutingManager.RouteCacheItem> routeCache;
        /* access modifiers changed from: private */
        public final java.util.Queue<com.navdy.hud.app.framework.fuel.FuelRoutingManager.FuelRoutingAction> routingQueue;
        private final com.here.android.mpa.common.GeoCoordinate routingStart;
        private final java.util.List<com.here.android.mpa.common.GeoCoordinate> waypoints;

        class Anon1 implements com.navdy.hud.app.maps.here.HereRouteCalculator.RouteCalculatorListener {
            Anon1() {
            }

            public void preSuccess() {
            }

            public void postSuccess(java.util.ArrayList<com.navdy.service.library.events.navigation.NavigationRouteResult> outgoingResults) {
                if (com.navdy.hud.app.framework.fuel.FuelRoutingManager.sLogger.isLoggable(2)) {
                    com.navdy.hud.app.framework.fuel.FuelRoutingManager.sLogger.v("Calculated route to " + com.navdy.hud.app.framework.fuel.FuelRoutingManager.FuelRoutingAction.this.place.getName() + " with no errors");
                }
                com.navdy.hud.app.framework.fuel.FuelRoutingManager.FuelRoutingAction.this.routeCache.add(new com.navdy.hud.app.framework.fuel.FuelRoutingManager.RouteCacheItem((com.navdy.service.library.events.navigation.NavigationRouteResult) outgoingResults.get(0), com.navdy.hud.app.framework.fuel.FuelRoutingManager.FuelRoutingAction.this.place));
                if (com.navdy.hud.app.framework.fuel.FuelRoutingManager.FuelRoutingAction.this.routeCache.size() == com.navdy.hud.app.framework.fuel.FuelRoutingManager.FuelRoutingAction.this.currentRouteCacheSize) {
                    com.navdy.hud.app.framework.fuel.FuelRoutingManager.this.calculateOptimalGasStation(com.navdy.hud.app.framework.fuel.FuelRoutingManager.FuelRoutingAction.this.routeCache, com.navdy.hud.app.framework.fuel.FuelRoutingManager.FuelRoutingAction.this.onRouteToGasStationCallback);
                } else {
                    com.navdy.hud.app.framework.fuel.FuelRoutingManager.this.handler.post((java.lang.Runnable) com.navdy.hud.app.framework.fuel.FuelRoutingManager.FuelRoutingAction.this.routingQueue.poll());
                }
            }

            public void error(com.here.android.mpa.routing.RoutingError error, java.lang.Throwable t) {
                com.navdy.hud.app.framework.fuel.FuelRoutingManager.sLogger.w("Calculated route to " + com.navdy.hud.app.framework.fuel.FuelRoutingManager.FuelRoutingAction.this.place.getName() + " with error " + error.name());
                com.navdy.hud.app.framework.fuel.FuelRoutingManager.FuelRoutingAction.this.currentRouteCacheSize = com.navdy.hud.app.framework.fuel.FuelRoutingManager.FuelRoutingAction.this.currentRouteCacheSize - 1;
                if (com.navdy.hud.app.framework.fuel.FuelRoutingManager.FuelRoutingAction.this.routeCache.size() == com.navdy.hud.app.framework.fuel.FuelRoutingManager.FuelRoutingAction.this.currentRouteCacheSize) {
                    com.navdy.hud.app.framework.fuel.FuelRoutingManager.this.calculateOptimalGasStation(com.navdy.hud.app.framework.fuel.FuelRoutingManager.FuelRoutingAction.this.routeCache, com.navdy.hud.app.framework.fuel.FuelRoutingManager.FuelRoutingAction.this.onRouteToGasStationCallback);
                } else {
                    com.navdy.hud.app.framework.fuel.FuelRoutingManager.this.handler.post((java.lang.Runnable) com.navdy.hud.app.framework.fuel.FuelRoutingManager.FuelRoutingAction.this.routingQueue.poll());
                }
            }

            public void progress(int progress) {
                if (com.navdy.hud.app.framework.fuel.FuelRoutingManager.sLogger.isLoggable(2)) {
                    com.navdy.hud.app.framework.fuel.FuelRoutingManager.sLogger.v("Calculating route to " + com.navdy.hud.app.framework.fuel.FuelRoutingManager.FuelRoutingAction.this.place.getName() + "; progress %: " + progress);
                }
            }
        }

        public FuelRoutingAction(int placesSize, com.here.android.mpa.search.Place place2, java.util.Queue<com.navdy.hud.app.framework.fuel.FuelRoutingManager.FuelRoutingAction> routingQueue2, java.util.List<com.navdy.hud.app.framework.fuel.FuelRoutingManager.RouteCacheItem> routeCache2, com.navdy.hud.app.framework.fuel.FuelRoutingManager.OnRouteToGasStationCallback onRouteToGasStationCallback2, com.here.android.mpa.common.GeoCoordinate routingStart2, java.util.List<com.here.android.mpa.common.GeoCoordinate> waypoints2, com.here.android.mpa.common.GeoCoordinate endPoint2) {
            this.place = place2;
            this.routingQueue = routingQueue2;
            this.routeCache = routeCache2;
            this.onRouteToGasStationCallback = onRouteToGasStationCallback2;
            this.routingStart = routingStart2;
            this.waypoints = waypoints2;
            this.endPoint = endPoint2;
            this.currentRouteCacheSize = placesSize;
        }

        public void run() {
            if (com.navdy.hud.app.framework.fuel.FuelRoutingManager.this.isCalculatingFuelRoute) {
                com.navdy.hud.app.framework.fuel.FuelRoutingManager.this.hereRouteCalculator.calculateRoute(null, this.routingStart, this.waypoints, this.endPoint, false, new com.navdy.hud.app.framework.fuel.FuelRoutingManager.FuelRoutingAction.Anon1(), 1, com.navdy.hud.app.framework.fuel.FuelRoutingManager.this.gasRouteOptions, false, false, false);
            }
        }
    }

    public interface OnNearestGasStationCallback {
        void onComplete(com.navdy.service.library.events.navigation.NavigationRouteResult navigationRouteResult);

        void onError(com.navdy.hud.app.framework.fuel.FuelRoutingManager.OnRouteToGasStationCallback.Error error);
    }

    public interface OnRouteToGasStationCallback {

        public enum Error {
            BAD_REQUEST,
            RESPONSE_ERROR,
            NO_USER_LOCATION,
            NO_ROUTES,
            UNKNOWN_ERROR,
            INVALID_STATE
        }

        void onError(com.navdy.hud.app.framework.fuel.FuelRoutingManager.OnRouteToGasStationCallback.Error error);

        void onOptimalRouteCalculationComplete(com.navdy.hud.app.framework.fuel.FuelRoutingManager.RouteCacheItem routeCacheItem, java.util.ArrayList<com.navdy.service.library.events.navigation.NavigationRouteResult> arrayList, com.navdy.service.library.events.navigation.NavigationRouteRequest navigationRouteRequest);
    }

    private static class RouteCacheItem {
        com.here.android.mpa.search.Place gasStation;
        com.navdy.service.library.events.navigation.NavigationRouteResult route;

        public RouteCacheItem(com.navdy.service.library.events.navigation.NavigationRouteResult route2, com.here.android.mpa.search.Place gasStation2) {
            this.route = route2;
            this.gasStation = gasStation2;
        }
    }

    public static class TestObdLowFuelLevel {
    }

    static {
        GAS_CATEGORY.add("petrol-station");
    }

    @android.support.annotation.Nullable
    public static com.navdy.hud.app.framework.fuel.FuelRoutingManager getInstance() {
        return sInstance;
    }

    private FuelRoutingManager() {
    }

    @com.squareup.otto.Subscribe
    public void onObdPidChangeEvent(com.navdy.hud.app.obd.ObdManager.ObdPidChangeEvent event) {
        if (event.pid.getId() == 47) {
            checkFuelLevel();
        }
    }

    @com.squareup.otto.Subscribe
    public void onTestObdLowFuelLevel(com.navdy.hud.app.framework.fuel.FuelRoutingManager.TestObdLowFuelLevel event) {
        if (!this.isInTestMode) {
            this.isInTestMode = true;
            checkFuelLevel();
        }
    }

    @com.squareup.otto.Subscribe
    public void onFuelAddedTestEvent(com.navdy.hud.app.framework.fuel.FuelRoutingManager.FuelAddedTestEvent event) {
        setFuelLevelBackToNormal();
    }

    @com.squareup.otto.Subscribe
    public void onClearTestObdLowFuelLevel(com.navdy.hud.app.framework.fuel.FuelRoutingManager.ClearTestObdLowFuelLevel event) {
        reset();
        this.fuelGlanceDismissTime = 0;
    }

    @com.squareup.otto.Subscribe
    public void onNewRouteAdded(com.navdy.hud.app.maps.MapEvents.NewRouteAdded event) {
        if (sLogger.isLoggable(2)) {
            sLogger.v("new route added, resetting gas routes");
        }
        com.navdy.hud.app.framework.notifications.NotificationManager notificationManager = com.navdy.hud.app.framework.notifications.NotificationManager.getInstance();
        if (event.rerouteReason == null && notificationManager.isNotificationPresent(LOW_FUEL_ID)) {
            notificationManager.removeNotification(LOW_FUEL_ID);
        }
    }

    public void routeToGasStation() {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.framework.fuel.FuelRoutingManager.Anon1(), 21);
    }

    public void dismissGasRoute() {
        sLogger.v("dismissGasRoute");
        int i = this.currentLowFuelThreshold / 2;
        this.currentLowFuelThreshold = i;
        reset(i);
        this.fuelGlanceDismissTime = android.os.SystemClock.elapsedRealtime();
    }

    public com.here.android.mpa.search.Place getCurrentGasStation() {
        return this.currentGasStation;
    }

    public void findNearestGasStation(com.navdy.hud.app.framework.fuel.FuelRoutingManager.OnNearestGasStationCallback callback) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.framework.fuel.FuelRoutingManager.Anon2(callback), 21);
    }

    private void reset(int fuelThreshold) {
        this.hereRouteCalculator.cancel();
        if (this.hereNavigationManager.isOnGasRoute()) {
            this.hereNavigationManager.arrived();
        }
        this.currentLowFuelThreshold = fuelThreshold;
        this.isInTestMode = false;
        this.currentOutgoingResults = null;
        this.currentNavigationRouteRequest = null;
        this.currentGasStation = null;
        if (sLogger.isLoggable(2)) {
            sLogger.v("reset, state is now TRACKING");
        }
    }

    public void reset() {
        sLogger.v("reset");
        reset(15);
    }

    /* access modifiers changed from: private */
    public void checkFuelLevel() {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.framework.fuel.FuelRoutingManager.Anon3(), 21);
    }

    /* access modifiers changed from: private */
    public void setFuelLevelBackToNormal() {
        sLogger.i("checkFuelLevel:Fuel level normal");
        reset();
        com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().removeNotification(LOW_FUEL_ID);
        this.fuelGlanceDismissTime = 0;
    }

    /* access modifiers changed from: private */
    public void calculateGasStationRoutes(com.navdy.hud.app.framework.fuel.FuelRoutingManager.OnRouteToGasStationCallback onRouteToGasStationCallback) {
        if (onRouteToGasStationCallback == null) {
            throw new java.lang.IllegalArgumentException();
        }
        com.here.android.mpa.common.GeoCoordinate gasCalculationCoords = getBestInitialGeo();
        if (gasCalculationCoords == null) {
            sLogger.w("No user location while calculating routes to gas station");
            onRouteToGasStationCallback.onError(com.navdy.hud.app.framework.fuel.FuelRoutingManager.OnRouteToGasStationCallback.Error.NO_USER_LOCATION);
            return;
        }
        com.navdy.hud.app.maps.here.HerePlacesManager.handleCategoriesRequest(GAS_CATEGORY, 3, new com.navdy.hud.app.framework.fuel.FuelRoutingManager.Anon4(onRouteToGasStationCallback, gasCalculationCoords));
    }

    private com.here.android.mpa.common.GeoCoordinate getBestInitialGeo() {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        com.here.android.mpa.common.GeoCoordinate bestInitialGeo = this.hereMapsManager.getLocationFixManager().getLastGeoCoordinate();
        com.here.android.mpa.routing.Route route = this.hereNavigationManager.getCurrentRoute();
        if (route == null) {
            return bestInitialGeo;
        }
        com.here.android.mpa.routing.RouteElements routeElements = route.getRouteElementsFromLength((int) this.hereNavigationManager.getNavController().getElapsedDistance());
        if (routeElements == null) {
            return bestInitialGeo;
        }
        java.util.List<com.here.android.mpa.routing.RouteElement> routeElementsList = routeElements.getElements();
        if (routeElementsList == null) {
            return bestInitialGeo;
        }
        com.here.android.mpa.common.GeoCoordinate oneMinuteFromNowGeo = null;
        int timeCount = 0;
        java.util.Iterator it = routeElementsList.iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            com.here.android.mpa.routing.RouteElement routeElement = (com.here.android.mpa.routing.RouteElement) it.next();
            com.here.android.mpa.common.RoadElement roadElement = routeElement.getRoadElement();
            if (roadElement != null && roadElement.getDefaultSpeed() > 0.0f) {
                timeCount = (int) (((long) timeCount) + java.lang.Math.round(roadElement.getGeometryLength() / ((double) roadElement.getDefaultSpeed())));
            }
            if (timeCount > 60) {
                java.util.List<com.here.android.mpa.common.GeoCoordinate> geometry = routeElement.getGeometry();
                if (geometry != null && geometry.size() > 0) {
                    oneMinuteFromNowGeo = (com.here.android.mpa.common.GeoCoordinate) geometry.get(geometry.size() - 1);
                    break;
                }
            }
        }
        if (oneMinuteFromNowGeo != null) {
            return oneMinuteFromNowGeo;
        }
        return bestInitialGeo;
    }

    /* access modifiers changed from: private */
    public void calculateOptimalGasStation(java.util.List<com.navdy.hud.app.framework.fuel.FuelRoutingManager.RouteCacheItem> routeCache, com.navdy.hud.app.framework.fuel.FuelRoutingManager.OnRouteToGasStationCallback onRouteToGasStationCallback) {
        com.navdy.hud.app.framework.fuel.FuelRoutingManager.RouteCacheItem optimal = null;
        int minTta = -1;
        for (com.navdy.hud.app.framework.fuel.FuelRoutingManager.RouteCacheItem routeCacheItem : routeCache) {
            int tta = routeCacheItem.route.duration.intValue();
            int distance = (int) routeCacheItem.gasStation.getLocation().getCoordinate().distanceTo(this.hereMapsManager.getLocationFixManager().getLastGeoCoordinate());
            sLogger.v("evaluating [" + routeCacheItem.gasStation.getName() + "] distance [" + distance + "] tta [" + tta + "]");
            if (minTta < 0 || tta < minTta) {
                minTta = tta;
                optimal = routeCacheItem;
            }
        }
        if (optimal != null) {
            sLogger.v("Optimal route: \n\tName:" + optimal.gasStation.getName() + "\n\tAddress: " + optimal.gasStation.getLocation().getAddress() + "\n\tTTA: " + minTta + " s\n\tGas station distance: " + optimal.gasStation.getLocation().getCoordinate().distanceTo(this.hereMapsManager.getLocationFixManager().getLastGeoCoordinate()) + " m");
            requestOptimalRoute(optimal, onRouteToGasStationCallback);
            return;
        }
        sLogger.w("No routes to any gas stations");
        onRouteToGasStationCallback.onError(com.navdy.hud.app.framework.fuel.FuelRoutingManager.OnRouteToGasStationCallback.Error.NO_ROUTES);
    }

    private void requestOptimalRoute(com.navdy.hud.app.framework.fuel.FuelRoutingManager.RouteCacheItem gasRoute, com.navdy.hud.app.framework.fuel.FuelRoutingManager.OnRouteToGasStationCallback onRouteToGasStationCallback) {
        try {
            com.here.android.mpa.common.GeoCoordinate gasNavGeo = com.navdy.hud.app.maps.here.HerePlacesManager.getPlaceEntry(gasRoute.gasStation);
            com.here.android.mpa.common.GeoCoordinate gasDisplayGeo = gasRoute.gasStation.getLocation().getCoordinate();
            com.navdy.service.library.events.location.Coordinate navigationPosition = new com.navdy.service.library.events.location.Coordinate.Builder().latitude(java.lang.Double.valueOf(gasNavGeo.getLatitude())).longitude(java.lang.Double.valueOf(gasNavGeo.getLongitude())).build();
            com.navdy.service.library.events.location.Coordinate displayPosition = new com.navdy.service.library.events.location.Coordinate.Builder().latitude(java.lang.Double.valueOf(gasDisplayGeo.getLatitude())).longitude(java.lang.Double.valueOf(gasDisplayGeo.getLongitude())).build();
            java.util.ArrayList arrayList = new java.util.ArrayList(1);
            arrayList.add(com.navdy.service.library.events.navigation.NavigationRouteRequest.RouteAttribute.ROUTE_ATTRIBUTE_GAS);
            com.navdy.service.library.events.navigation.NavigationRouteRequest navigationRouteRequest = new com.navdy.service.library.events.navigation.NavigationRouteRequest.Builder().destination(navigationPosition).label(gasRoute.gasStation.getName()).streetAddress(gasRoute.gasStation.getLocation().getAddress().toString()).destination_identifier(gasRoute.gasStation.getId()).originDisplay(java.lang.Boolean.valueOf(true)).geoCodeStreetAddress(java.lang.Boolean.valueOf(false)).destinationType(com.navdy.service.library.events.destination.Destination.FavoriteType.FAVORITE_NONE).requestId(java.util.UUID.randomUUID().toString()).destinationDisplay(displayPosition).routeAttributes(arrayList).build();
            this.hereRouteCalculator.calculateRoute(navigationRouteRequest, this.hereMapsManager.getLocationFixManager().getLastGeoCoordinate(), null, gasNavGeo, true, new com.navdy.hud.app.framework.fuel.FuelRoutingManager.Anon5(onRouteToGasStationCallback, gasRoute, navigationRouteRequest), 1, this.gasRouteOptions, true, true, false);
        } catch (Throwable t) {
            sLogger.e("requestOptimalRoute", t);
            onRouteToGasStationCallback.onError(com.navdy.hud.app.framework.fuel.FuelRoutingManager.OnRouteToGasStationCallback.Error.UNKNOWN_ERROR);
        }
    }

    public void findGasStations(int fuelLevel, boolean postNotificationOnError) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.framework.fuel.FuelRoutingManager.Anon6(fuelLevel, postNotificationOnError), 21);
    }

    public void showFindingGasStationToast() {
        android.content.res.Resources resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        com.navdy.hud.app.framework.toast.ToastManager toastManager = com.navdy.hud.app.framework.toast.ToastManager.getInstance();
        toastManager.dismissCurrentToast(FINDING_FAST_STATION_TOAST_ID);
        toastManager.clearPendingToast(FINDING_FAST_STATION_TOAST_ID);
        android.os.Bundle bundle = new android.os.Bundle();
        bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_TIMEOUT, com.loopj.android.http.AsyncHttpClient.DEFAULT_RETRY_SLEEP_TIME_MILLIS);
        bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_IMAGE, com.navdy.hud.app.R.drawable.icon_glance_fuel_low);
        bundle.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_2, resources.getString(com.navdy.hud.app.R.string.search_gas_station));
        bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_2_STYLE, com.navdy.hud.app.R.style.Glances_1);
        toastManager.addToast(new com.navdy.hud.app.framework.toast.ToastManager.ToastParams(FINDING_FAST_STATION_TOAST_ID, bundle, null, true, false, false));
    }

    public void markAvailable() {
        if (!this.available) {
            this.available = true;
            this.hereNavigationManager = com.navdy.hud.app.maps.here.HereNavigationManager.getInstance();
            this.hereRouteCalculator = new com.navdy.hud.app.maps.here.HereRouteCalculator(sLogger, false);
            this.gasRouteOptions = new com.here.android.mpa.routing.RouteOptions();
            this.gasRouteOptions.setRouteCount(1);
            this.gasRouteOptions.setTransportMode(com.here.android.mpa.routing.RouteOptions.TransportMode.CAR);
            this.gasRouteOptions.setRouteType(com.here.android.mpa.routing.RouteOptions.Type.FASTEST);
            this.bus.register(this);
            this.handler.postDelayed(new com.navdy.hud.app.framework.fuel.FuelRoutingManager.Anon7(), 20000);
            sLogger.v("mark available");
        }
    }

    public boolean isAvailable() {
        return this.available;
    }

    public boolean isBusy() {
        return this.isCalculatingFuelRoute || this.hereNavigationManager.isOnGasRoute() || com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().isNotificationPresent(LOW_FUEL_ID);
    }

    public long getFuelGlanceDismissTime() {
        return this.fuelGlanceDismissTime;
    }
}
