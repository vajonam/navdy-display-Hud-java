package com.navdy.hud.app.framework.message;

public class SmsNotification$$ViewInjector {
    public static void inject(butterknife.ButterKnife.Finder finder, com.navdy.hud.app.framework.message.SmsNotification target, java.lang.Object source) {
        target.title = (android.widget.TextView) finder.findRequiredView(source, com.navdy.hud.app.R.id.title, "field 'title'");
        target.subtitle = (android.widget.TextView) finder.findRequiredView(source, com.navdy.hud.app.R.id.subtitle, "field 'subtitle'");
        target.choiceLayout = (com.navdy.hud.app.ui.component.ChoiceLayout2) finder.findRequiredView(source, com.navdy.hud.app.R.id.choice_layout, "field 'choiceLayout'");
        target.notificationUserImage = (com.navdy.hud.app.ui.component.image.InitialsImageView) finder.findRequiredView(source, com.navdy.hud.app.R.id.notification_user_image, "field 'notificationUserImage'");
        target.sideImage = (android.widget.ImageView) finder.findRequiredView(source, com.navdy.hud.app.R.id.badge, "field 'sideImage'");
    }

    public static void reset(com.navdy.hud.app.framework.message.SmsNotification target) {
        target.title = null;
        target.subtitle = null;
        target.choiceLayout = null;
        target.notificationUserImage = null;
        target.sideImage = null;
    }
}
