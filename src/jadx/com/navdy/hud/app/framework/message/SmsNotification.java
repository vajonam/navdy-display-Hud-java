package com.navdy.hud.app.framework.message;

public class SmsNotification implements com.navdy.hud.app.framework.notifications.INotification, com.navdy.hud.app.ui.component.ChoiceLayout2.IListener, com.navdy.hud.app.framework.contacts.ContactUtil.IContactCallback {
    private static final int NOTIFICATION_TIMEOUT = ((int) java.util.concurrent.TimeUnit.SECONDS.toMillis(5));
    private static final int TIMEOUT = ((int) java.util.concurrent.TimeUnit.SECONDS.toMillis(10));
    private static final java.util.List<com.navdy.hud.app.ui.component.ChoiceLayout2.Choice> choicesDismiss = new java.util.ArrayList();
    private static final java.util.List<com.navdy.hud.app.ui.component.ChoiceLayout2.Choice> choicesRetryAndDismiss = new java.util.ArrayList();
    private static final android.os.Handler handler = new android.os.Handler(android.os.Looper.getMainLooper());
    private static final android.content.res.Resources resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.framework.message.SmsNotification.class);
    @butterknife.InjectView(2131624279)
    com.navdy.hud.app.ui.component.ChoiceLayout2 choiceLayout;
    private com.navdy.hud.app.framework.notifications.INotificationController controller;
    /* access modifiers changed from: private */
    public final java.lang.String message;
    private final com.navdy.hud.app.framework.message.SmsNotification.Mode mode;
    /* access modifiers changed from: private */
    public final java.lang.String name;
    /* access modifiers changed from: private */
    public final java.lang.String notifId;
    @butterknife.InjectView(2131624275)
    com.navdy.hud.app.ui.component.image.InitialsImageView notificationUserImage;
    /* access modifiers changed from: private */
    public final java.lang.String number;
    /* access modifiers changed from: private */
    public final java.lang.String rawNumber;
    private boolean retrySendingMessage;
    private com.squareup.picasso.Transformation roundTransformation = new com.makeramen.RoundedTransformationBuilder().oval(true).build();
    @butterknife.InjectView(2131624277)
    android.widget.ImageView sideImage;
    @butterknife.InjectView(2131624272)
    android.widget.TextView subtitle;
    @butterknife.InjectView(2131624135)
    android.widget.TextView title;

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            com.navdy.hud.app.framework.glance.GlanceHandler glanceHandler = com.navdy.hud.app.framework.glance.GlanceHandler.getInstance();
            if (!glanceHandler.sendMessage(com.navdy.hud.app.framework.message.SmsNotification.this.rawNumber, com.navdy.hud.app.framework.message.SmsNotification.this.message, com.navdy.hud.app.framework.message.SmsNotification.this.name)) {
                com.navdy.hud.app.framework.message.SmsNotification.sLogger.v("retry message not sent");
                glanceHandler.sendSmsFailedNotification(com.navdy.hud.app.framework.message.SmsNotification.this.number, com.navdy.hud.app.framework.message.SmsNotification.this.message, com.navdy.hud.app.framework.message.SmsNotification.this.name);
                return;
            }
            com.navdy.hud.app.framework.message.SmsNotification.sLogger.v("retry message sent");
            glanceHandler.sendSmsSuccessNotification(com.navdy.hud.app.framework.message.SmsNotification.this.notifId, com.navdy.hud.app.framework.message.SmsNotification.this.number, com.navdy.hud.app.framework.message.SmsNotification.this.message, com.navdy.hud.app.framework.message.SmsNotification.this.name);
        }
    }

    public enum Mode {
        Success,
        Failed
    }

    static {
        java.lang.String dismiss = resources.getString(com.navdy.hud.app.R.string.dismiss);
        int dismissColor = resources.getColor(com.navdy.hud.app.R.color.glance_dismiss);
        int retryColor = resources.getColor(com.navdy.hud.app.R.color.glance_ok_blue);
        choicesRetryAndDismiss.add(new com.navdy.hud.app.ui.component.ChoiceLayout2.Choice(com.navdy.hud.app.R.id.retry, com.navdy.hud.app.R.drawable.icon_glances_retry, retryColor, com.navdy.hud.app.R.drawable.icon_glances_retry, -16777216, resources.getString(com.navdy.hud.app.R.string.retry), retryColor));
        choicesRetryAndDismiss.add(new com.navdy.hud.app.ui.component.ChoiceLayout2.Choice(com.navdy.hud.app.R.id.dismiss, com.navdy.hud.app.R.drawable.icon_glances_dismiss, dismissColor, com.navdy.hud.app.R.drawable.icon_glances_dismiss, -16777216, dismiss, dismissColor));
        choicesDismiss.add(new com.navdy.hud.app.ui.component.ChoiceLayout2.Choice(com.navdy.hud.app.R.id.dismiss, com.navdy.hud.app.R.drawable.icon_glances_dismiss, dismissColor, com.navdy.hud.app.R.drawable.icon_glances_dismiss, -16777216, dismiss, dismissColor));
    }

    public SmsNotification(com.navdy.hud.app.framework.message.SmsNotification.Mode mode2, java.lang.String id, java.lang.String number2, java.lang.String message2, java.lang.String name2) {
        this.mode = mode2;
        this.notifId = com.navdy.hud.app.framework.notifications.NotificationId.SMS_NOTIFICATION_ID + id;
        this.number = com.navdy.hud.app.util.PhoneUtil.formatPhoneNumber(number2);
        this.rawNumber = com.navdy.hud.app.util.PhoneUtil.convertToE164Format(number2);
        this.message = message2;
        this.name = name2;
    }

    public com.navdy.hud.app.framework.notifications.NotificationType getType() {
        return com.navdy.hud.app.framework.notifications.NotificationType.SMS;
    }

    public java.lang.String getId() {
        return this.notifId;
    }

    public android.view.View getView(android.content.Context context) {
        android.view.View view = android.view.LayoutInflater.from(context).inflate(com.navdy.hud.app.R.layout.notification_sms, null);
        butterknife.ButterKnife.inject((java.lang.Object) this, view);
        return view;
    }

    private void setUI() {
        if (!android.text.TextUtils.isEmpty(this.name)) {
            this.subtitle.setText(this.name);
        } else if (!android.text.TextUtils.isEmpty(this.number)) {
            this.subtitle.setText(com.navdy.hud.app.util.PhoneUtil.formatPhoneNumber(this.number));
        } else {
            this.subtitle.setText("");
        }
        com.navdy.hud.app.framework.contacts.ContactUtil.setContactPhoto(this.name, this.number, false, this.notificationUserImage, this.roundTransformation, this);
        switch (this.mode) {
            case Failed:
                this.title.setText(resources.getString(com.navdy.hud.app.R.string.reply_failed));
                this.subtitle.setText(getContactName());
                this.sideImage.setImageResource(com.navdy.hud.app.R.drawable.icon_msg_failed);
                this.choiceLayout.setChoices(choicesRetryAndDismiss, 0, this);
                return;
            case Success:
                this.title.setText(resources.getString(com.navdy.hud.app.R.string.reply_sent));
                this.subtitle.setText(getContactName());
                this.sideImage.setImageResource(com.navdy.hud.app.R.drawable.icon_msg_success);
                this.choiceLayout.setChoices(choicesDismiss, 0, this);
                return;
            default:
                return;
        }
    }

    public void onClick() {
    }

    public void onTrackHand(float normalized) {
    }

    public android.view.View getExpandedView(android.content.Context context, java.lang.Object data) {
        return null;
    }

    public int getExpandedViewIndicatorColor() {
        return 0;
    }

    public void onStart(com.navdy.hud.app.framework.notifications.INotificationController controller2) {
        this.controller = controller2;
        setUI();
    }

    public void onUpdate() {
    }

    public void onStop() {
        this.controller = null;
        if (this.retrySendingMessage) {
            handler.post(new com.navdy.hud.app.framework.message.SmsNotification.Anon1());
        }
    }

    public int getTimeout() {
        switch (this.mode) {
            case Failed:
                return TIMEOUT;
            default:
                return NOTIFICATION_TIMEOUT;
        }
    }

    public boolean isAlive() {
        return false;
    }

    public boolean isPurgeable() {
        return false;
    }

    public boolean canAddToStackIfCurrentExists() {
        return true;
    }

    public int getColor() {
        return 0;
    }

    public void onNotificationEvent(com.navdy.hud.app.ui.framework.UIStateManager.Mode mode2) {
    }

    public void onExpandedNotificationEvent(com.navdy.hud.app.ui.framework.UIStateManager.Mode mode2) {
    }

    public void onExpandedNotificationSwitched() {
    }

    public android.animation.AnimatorSet getViewSwitchAnimation(boolean viewIn) {
        return null;
    }

    public boolean expandNotification() {
        return false;
    }

    public boolean supportScroll() {
        return false;
    }

    public void executeItem(com.navdy.hud.app.ui.component.ChoiceLayout2.Selection selection) {
        switch (selection.id) {
            case com.navdy.hud.app.R.id.dismiss /*2131623947*/:
                com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().removeNotification(this.notifId);
                return;
            case com.navdy.hud.app.R.id.retry /*2131624048*/:
                this.retrySendingMessage = true;
                com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().removeNotification(this.notifId);
                return;
            default:
                return;
        }
    }

    public void itemSelected(com.navdy.hud.app.ui.component.ChoiceLayout2.Selection selection) {
    }

    public boolean onGesture(com.navdy.service.library.events.input.GestureEvent event) {
        return false;
    }

    public boolean onKey(com.navdy.hud.app.manager.InputManager.CustomKeyEvent event) {
        if (this.controller == null) {
            return false;
        }
        switch (event) {
            case LEFT:
                this.choiceLayout.moveSelectionLeft();
                this.controller.resetTimeout();
                return true;
            case RIGHT:
                this.choiceLayout.moveSelectionRight();
                this.controller.resetTimeout();
                return true;
            case SELECT:
                this.choiceLayout.executeSelectedItem();
                return true;
            default:
                return false;
        }
    }

    public com.navdy.hud.app.manager.InputManager.IInputHandler nextHandler() {
        return null;
    }

    public boolean isContextValid() {
        return this.controller != null;
    }

    private java.lang.String getContactName() {
        if (!android.text.TextUtils.isEmpty(this.name)) {
            return this.name;
        }
        if (!android.text.TextUtils.isEmpty(this.number)) {
            return this.number;
        }
        return "";
    }
}
