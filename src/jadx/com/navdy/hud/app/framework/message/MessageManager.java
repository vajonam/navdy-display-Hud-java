package com.navdy.hud.app.framework.message;

public class MessageManager {
    private static final java.lang.String EMPTY = "";
    private static final com.navdy.hud.app.framework.message.MessageManager sInstance = new com.navdy.hud.app.framework.message.MessageManager();
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.framework.message.MessageManager.class);
    private com.squareup.otto.Bus bus = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getBus();

    public static com.navdy.hud.app.framework.message.MessageManager getInstance() {
        return sInstance;
    }

    private MessageManager() {
        this.bus.register(this);
    }

    @com.squareup.otto.Subscribe
    public void onMessage(com.navdy.service.library.events.notification.NotificationEvent event) {
        sLogger.v("old notification --> convert to glance");
        if (event.sourceIdentifier == null) {
            sLogger.w("Ignoring notification with null sourceIdentifier" + event);
            return;
        }
        com.navdy.hud.app.framework.recentcall.RecentCall call = null;
        if (event.category == com.navdy.service.library.events.notification.NotificationCategory.CATEGORY_SOCIAL) {
            call = new com.navdy.hud.app.framework.recentcall.RecentCall(null, com.navdy.hud.app.framework.recentcall.RecentCall.Category.MESSAGE, event.sourceIdentifier, com.navdy.hud.app.framework.contacts.NumberType.OTHER, new java.util.Date(), com.navdy.hud.app.framework.recentcall.RecentCall.CallType.INCOMING, -1, 0);
            com.navdy.hud.app.framework.recentcall.RecentCallManager.getInstance().handleNewCall(call);
        }
        java.lang.String from = android.text.TextUtils.isEmpty(event.title) ? event.sourceIdentifier : event.title;
        java.lang.String message = event.message;
        java.lang.String number = null;
        boolean cannotReplyBack = java.lang.Boolean.TRUE.equals(event.cannotReplyBack);
        boolean validNumber = false;
        if (com.navdy.hud.app.framework.contacts.ContactUtil.isValidNumber(event.sourceIdentifier)) {
            number = event.sourceIdentifier;
            validNumber = true;
        } else if (call != null && com.navdy.hud.app.framework.contacts.ContactUtil.isValidNumber(call.number)) {
            number = call.number;
            validNumber = true;
        }
        if (!validNumber) {
            cannotReplyBack = true;
        }
        java.lang.String id = com.navdy.hud.app.framework.glance.GlanceHelper.getId();
        java.util.List<com.navdy.service.library.events.glances.KeyValue> data = new java.util.ArrayList<>();
        java.util.List<com.navdy.service.library.events.glances.GlanceEvent.GlanceActions> action = null;
        data.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.MessageConstants.MESSAGE_FROM.name(), from));
        if (number != null) {
            data.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.MessageConstants.MESSAGE_FROM_NUMBER.name(), number));
            if (!cannotReplyBack) {
                action = new java.util.ArrayList<>(1);
                action.add(com.navdy.service.library.events.glances.GlanceEvent.GlanceActions.REPLY);
                data.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.MessageConstants.MESSAGE_IS_SMS.name(), from));
            }
        }
        data.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.MessageConstants.MESSAGE_BODY.name(), message));
        com.navdy.service.library.events.glances.GlanceEvent.Builder builder = new com.navdy.service.library.events.glances.GlanceEvent.Builder().glanceType(com.navdy.service.library.events.glances.GlanceEvent.GlanceType.GLANCE_TYPE_MESSAGE).provider(com.navdy.hud.app.framework.glance.GlanceHelper.SMS_PACKAGE).id(id).postTime(java.lang.Long.valueOf(java.lang.System.currentTimeMillis())).glanceData(data);
        if (action != null) {
            builder.actions(action);
        }
        this.bus.post(builder.build());
    }
}
