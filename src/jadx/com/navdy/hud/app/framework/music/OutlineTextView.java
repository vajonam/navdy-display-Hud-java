package com.navdy.hud.app.framework.music;

public class OutlineTextView extends android.widget.TextView {
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.framework.music.OutlineTextView.class);
    java.lang.reflect.Field currentTextColorField;

    public OutlineTextView(android.content.Context context) {
        this(context, null);
    }

    public OutlineTextView(android.content.Context context, android.util.AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public OutlineTextView(android.content.Context context, android.util.AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        try {
            this.currentTextColorField = android.widget.TextView.class.getDeclaredField("mCurTextColor");
            this.currentTextColorField.setAccessible(true);
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }

    public void onDraw(android.graphics.Canvas canvas) {
        int restoreColor = getCurrentTextColor();
        android.text.TextPaint paint = getPaint();
        paint.setStyle(android.graphics.Paint.Style.STROKE);
        paint.setStrokeJoin(android.graphics.Paint.Join.MITER);
        paint.setStrokeMiter(1.0f);
        setCurrentTextColor(java.lang.Integer.valueOf(-16777216));
        paint.setStrokeWidth(4.0f);
        super.onDraw(canvas);
        paint.setStyle(android.graphics.Paint.Style.FILL);
        setCurrentTextColor(java.lang.Integer.valueOf(restoreColor));
        super.onDraw(canvas);
    }

    private void setCurrentTextColor(java.lang.Integer color) {
        try {
            if (this.currentTextColorField != null) {
                this.currentTextColorField.set(this, color);
            }
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }
}
