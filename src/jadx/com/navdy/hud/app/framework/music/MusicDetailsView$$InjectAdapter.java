package com.navdy.hud.app.framework.music;

public final class MusicDetailsView$$InjectAdapter extends dagger.internal.Binding<com.navdy.hud.app.framework.music.MusicDetailsView> implements dagger.MembersInjector<com.navdy.hud.app.framework.music.MusicDetailsView> {
    private dagger.internal.Binding<com.navdy.hud.app.framework.music.MusicDetailsScreen.Presenter> presenter;

    public MusicDetailsView$$InjectAdapter() {
        super(null, "members/com.navdy.hud.app.framework.music.MusicDetailsView", false, com.navdy.hud.app.framework.music.MusicDetailsView.class);
    }

    public void attach(dagger.internal.Linker linker) {
        this.presenter = linker.requestBinding("com.navdy.hud.app.framework.music.MusicDetailsScreen$Presenter", com.navdy.hud.app.framework.music.MusicDetailsView.class, getClass().getClassLoader());
    }

    public void getDependencies(java.util.Set<dagger.internal.Binding<?>> set, java.util.Set<dagger.internal.Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.presenter);
    }

    public void injectMembers(com.navdy.hud.app.framework.music.MusicDetailsView object) {
        object.presenter = (com.navdy.hud.app.framework.music.MusicDetailsScreen.Presenter) this.presenter.get();
    }
}
