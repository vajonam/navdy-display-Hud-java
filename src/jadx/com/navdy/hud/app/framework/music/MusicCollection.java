package com.navdy.hud.app.framework.music;

public class MusicCollection {
    public java.util.List<com.navdy.service.library.events.audio.MusicCollectionInfo> collections = new java.util.ArrayList();
    public com.navdy.service.library.events.audio.MusicCollectionInfo musicCollectionInfo;
    public java.util.List<com.navdy.service.library.events.audio.MusicTrackInfo> tracks = new java.util.ArrayList();
}
