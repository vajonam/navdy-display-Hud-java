package com.navdy.hud.app.framework.music;

@flow.Layout(2130903136)
public class MusicDetailsScreen extends com.navdy.hud.app.screen.BaseScreen {
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.framework.music.MusicDetailsScreen.class);

    @dagger.Module(addsTo = com.navdy.hud.app.ui.activity.Main.Module.class, injects = {com.navdy.hud.app.framework.music.MusicDetailsView.class})
    public class Module {
        public Module() {
        }
    }

    @javax.inject.Singleton
    public static class Presenter extends com.navdy.hud.app.ui.framework.BasePresenter<com.navdy.hud.app.framework.music.MusicDetailsView> {
        private static final int MUSIC_CONTROL_TRAY_POS = 0;
        private static final int music_selected_color;
        private static final int music_unselected_color;
        private static final java.lang.String[] subTitles = {"Churches Your Vibe, Your Tribe Vol. 1", "Old Classics, Vol 2", "woof woof"};
        private static final com.navdy.hud.app.ui.component.vlist.VerticalList.Model thumbsDown = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.music_thumbsdown, com.navdy.hud.app.R.drawable.icon_thumbs_dn, music_selected_color, music_unselected_color, music_selected_color, "Dislike", null);
        private static final com.navdy.hud.app.ui.component.vlist.VerticalList.Model thumbsUp = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.music_thumbsup, com.navdy.hud.app.R.drawable.icon_thumbs_up, music_selected_color, music_unselected_color, music_selected_color, "Like", null);
        private static final java.lang.String[] titles = {"The Mother We Both Share", "Tomorrow, Tomorrow, I love you", "Who Let the Dogs out"};
        private boolean animateIn;
        @javax.inject.Inject
        com.squareup.otto.Bus bus;
        private boolean closed;
        private int currentCounter;
        private int currentTitleSelection;
        private java.util.List<com.navdy.hud.app.ui.component.vlist.VerticalList.Model> data;
        private boolean handledSelection;
        private boolean keyPressed;
        private boolean registered;
        private int thumbPos;

        class Anon1 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
            Anon1() {
            }

            public void onAnimationEnd(android.animation.Animator animation) {
                com.navdy.hud.app.framework.music.MusicDetailsScreen.sLogger.v("post back");
                com.navdy.hud.app.framework.music.MusicDetailsScreen.Presenter.this.bus.post(new com.navdy.service.library.events.ui.ShowScreen.Builder().screen(com.navdy.service.library.events.ui.Screen.SCREEN_BACK).build());
            }
        }

        static {
            android.content.res.Resources resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
            music_selected_color = resources.getColor(com.navdy.hud.app.R.color.music_details_sel);
            music_unselected_color = resources.getColor(com.navdy.hud.app.R.color.icon_bk_color_unselected);
        }

        public void onLoad(android.os.Bundle savedInstanceState) {
            this.currentCounter = 1;
            this.currentTitleSelection = 0;
            this.animateIn = false;
            this.keyPressed = false;
            reset(null);
            this.bus.register(this);
            this.registered = true;
            updateView();
            super.onLoad(savedInstanceState);
        }

        public void onUnload() {
            if (this.registered) {
                this.registered = false;
                this.bus.unregister(this);
            }
            reset(null);
            super.onUnload();
        }

        /* access modifiers changed from: 0000 */
        public void reset(com.navdy.hud.app.framework.music.MusicDetailsView view) {
            this.closed = false;
            this.handledSelection = false;
            if (view != null) {
                view.vmenuComponent.unlock();
            }
        }

        /* access modifiers changed from: 0000 */
        public void resetSelectedItem() {
            com.navdy.hud.app.framework.music.MusicDetailsScreen.sLogger.v("resetSelectedItem");
            this.handledSelection = false;
            if (!this.animateIn) {
                this.animateIn = true;
                com.navdy.hud.app.framework.music.MusicDetailsView view = (com.navdy.hud.app.framework.music.MusicDetailsView) getView();
                if (view != null) {
                    view.vmenuComponent.animateIn(null);
                }
            }
        }

        /* access modifiers changed from: protected */
        public void updateView() {
            com.navdy.hud.app.framework.music.MusicDetailsView view = (com.navdy.hud.app.framework.music.MusicDetailsView) getView();
            if (view != null) {
                init(view);
            }
        }

        /* access modifiers changed from: 0000 */
        public void init(com.navdy.hud.app.framework.music.MusicDetailsView view) {
            view.vmenuComponent.setOverrideDefaultKeyEvents(com.navdy.hud.app.manager.InputManager.CustomKeyEvent.LONG_PRESS, true);
            java.util.ArrayList arrayList = new java.util.ArrayList();
            arrayList.add(com.navdy.hud.app.ui.component.vlist.viewholder.TitleSubtitleViewHolder.buildModel(titles[this.currentTitleSelection], subTitles[this.currentTitleSelection]));
            this.currentTitleSelection++;
            int[] iconSelectedColor = {music_selected_color, music_selected_color, music_selected_color};
            arrayList.add(com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder.buildModel(com.navdy.hud.app.R.id.music_controls, new int[]{com.navdy.hud.app.R.drawable.icon_prev_music, com.navdy.hud.app.R.drawable.icon_play_music, com.navdy.hud.app.R.drawable.icon_next_music}, new int[]{com.navdy.hud.app.R.id.music_rewind, com.navdy.hud.app.R.id.music_play, com.navdy.hud.app.R.id.music_forward}, iconSelectedColor, new int[]{music_unselected_color, music_unselected_color, music_unselected_color}, iconSelectedColor, 1, false));
            int id = 2 + 1;
            arrayList.add(com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(2, com.navdy.hud.app.R.drawable.icon_playlist, music_selected_color, music_unselected_color, music_selected_color, "Playlist/Album name", null));
            int id2 = id + 1;
            arrayList.add(com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(id, com.navdy.hud.app.R.drawable.icon_heart, music_selected_color, music_unselected_color, music_selected_color, "Love", null));
            this.thumbPos = arrayList.size();
            arrayList.add(thumbsUp);
            int id3 = id2 + 1;
            arrayList.add(com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(id2, com.navdy.hud.app.R.drawable.icon_shuffle, music_selected_color, music_unselected_color, music_selected_color, "Shuffle", null));
            int id4 = id3 + 1;
            arrayList.add(com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(id3, com.navdy.hud.app.R.drawable.icon_stations, music_selected_color, music_unselected_color, music_selected_color, "Stations", null));
            int i = id4 + 1;
            arrayList.add(com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(id4, com.navdy.hud.app.R.drawable.icon_bookmark, music_selected_color, music_unselected_color, music_selected_color, "Bookmark", null));
            this.data = arrayList;
            view.vmenuComponent.updateView(this.data, 0, false);
        }

        /* access modifiers changed from: 0000 */
        public void close() {
            if (this.closed) {
                com.navdy.hud.app.framework.music.MusicDetailsScreen.sLogger.v("already closed");
                return;
            }
            this.closed = true;
            com.navdy.hud.app.framework.music.MusicDetailsScreen.sLogger.v("close");
            com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("exit");
            com.navdy.hud.app.framework.music.MusicDetailsView view = (com.navdy.hud.app.framework.music.MusicDetailsView) getView();
            if (view != null) {
                view.vmenuComponent.animateOut(new com.navdy.hud.app.framework.music.MusicDetailsScreen.Presenter.Anon1());
            }
        }

        /* access modifiers changed from: 0000 */
        public boolean selectItem(com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState selection) {
            com.navdy.hud.app.framework.music.MusicDetailsScreen.sLogger.v("select id:" + selection.id + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA + selection.pos + " subid=" + selection.subId);
            if (this.handledSelection) {
                com.navdy.hud.app.framework.music.MusicDetailsScreen.sLogger.v("already handled [" + selection.id + "], " + selection.pos);
                return true;
            }
            com.navdy.hud.app.framework.music.MusicDetailsView view = (com.navdy.hud.app.framework.music.MusicDetailsView) getView();
            if (view != null) {
                switch (selection.id) {
                    case com.navdy.hud.app.R.id.music_controls /*2131624025*/:
                        switch (selection.subId) {
                            case com.navdy.hud.app.R.id.music_forward /*2131624026*/:
                                reset(view);
                                break;
                            case com.navdy.hud.app.R.id.music_pause /*2131624027*/:
                                reset(view);
                                break;
                            case com.navdy.hud.app.R.id.music_play /*2131624028*/:
                                reset(view);
                                if (this.currentTitleSelection == titles.length) {
                                    this.currentTitleSelection = 0;
                                }
                                com.navdy.hud.app.ui.component.vlist.VerticalList.Model titleModel = com.navdy.hud.app.ui.component.vlist.viewholder.TitleSubtitleViewHolder.buildModel(titles[this.currentTitleSelection], subTitles[this.currentTitleSelection]);
                                this.currentTitleSelection++;
                                view.vmenuComponent.verticalList.refreshData(selection.pos, titleModel);
                                break;
                            case com.navdy.hud.app.R.id.music_rewind /*2131624029*/:
                                reset(view);
                                break;
                        }
                    case com.navdy.hud.app.R.id.music_thumbsdown /*2131624030*/:
                        com.navdy.hud.app.framework.music.MusicDetailsScreen.sLogger.v("thumbsup");
                        reset(view);
                        view.vmenuComponent.verticalList.refreshData(this.thumbPos, thumbsUp);
                        break;
                    case com.navdy.hud.app.R.id.music_thumbsup /*2131624031*/:
                        com.navdy.hud.app.framework.music.MusicDetailsScreen.sLogger.v("thumbsdown");
                        reset(view);
                        view.vmenuComponent.verticalList.refreshData(this.thumbPos, thumbsDown);
                        break;
                    default:
                        close();
                        break;
                }
            }
            return this.handledSelection;
        }

        /* access modifiers changed from: 0000 */
        public boolean isClosed() {
            return this.closed;
        }

        /* access modifiers changed from: 0000 */
        public boolean isItemClickable(com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState selection) {
            return true;
        }

        @com.squareup.otto.Subscribe
        public void onConnectionStateChange(com.navdy.service.library.events.connection.ConnectionStateChange event) {
            switch (event.state) {
                case CONNECTION_DISCONNECTED:
                    com.navdy.hud.app.framework.music.MusicDetailsScreen.sLogger.v("disconnected");
                    close();
                    return;
                default:
                    return;
            }
        }

        @com.squareup.otto.Subscribe
        public void onKeyEvent(android.view.KeyEvent event) {
            boolean fwd;
            if (com.navdy.hud.app.manager.InputManager.isCenterKey(event.getKeyCode())) {
                com.navdy.hud.app.framework.music.MusicDetailsView view = (com.navdy.hud.app.framework.music.MusicDetailsView) getView();
                if (view != null && !view.vmenuComponent.isCloseMenuVisible() && view.vmenuComponent.verticalList.getCurrentPosition() == 0) {
                    com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder vh = view.vmenuComponent.verticalList.getCurrentViewHolder();
                    if (vh != null && (vh instanceof com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder)) {
                        com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder iconOptionsViewHolder = (com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder) vh;
                        int selection = iconOptionsViewHolder.getCurrentSelection();
                        if (selection != 1) {
                            if (selection == 2) {
                                fwd = true;
                            } else {
                                fwd = false;
                            }
                            if (event.getAction() == 0) {
                                if (!this.keyPressed) {
                                    this.keyPressed = true;
                                    iconOptionsViewHolder.selectionDown();
                                }
                                java.lang.String text = java.lang.String.valueOf(this.currentCounter);
                                if (fwd) {
                                    this.currentCounter++;
                                } else if (this.currentCounter > 1) {
                                    this.currentCounter--;
                                }
                                view.counter.setText(text);
                            } else if (event.getAction() == 1 && this.keyPressed) {
                                this.keyPressed = false;
                                iconOptionsViewHolder.selectionUp();
                                reset(view);
                            }
                        }
                    }
                }
            }
        }
    }

    public java.lang.String getMortarScopeName() {
        return getClass().getName();
    }

    public java.lang.Object getDaggerModule() {
        return new com.navdy.hud.app.framework.music.MusicDetailsScreen.Module();
    }

    public com.navdy.service.library.events.ui.Screen getScreen() {
        return com.navdy.service.library.events.ui.Screen.SCREEN_MUSIC_DETAILS;
    }

    public int getAnimationIn(flow.Flow.Direction direction) {
        return -1;
    }

    public int getAnimationOut(flow.Flow.Direction direction) {
        return -1;
    }
}
