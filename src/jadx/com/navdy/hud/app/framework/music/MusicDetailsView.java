package com.navdy.hud.app.framework.music;

public class MusicDetailsView extends android.widget.RelativeLayout implements com.navdy.hud.app.manager.InputManager.IInputHandler {
    private static final int musicRightContainerLeftMargin;
    private static final int musicRightContainerSize;
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.framework.music.MusicDetailsView.class);
    android.widget.ImageView albumArt;
    android.view.ViewGroup albumArtContainer;
    private com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.Callback callback;
    android.widget.TextView counter;
    @javax.inject.Inject
    public com.navdy.hud.app.framework.music.MusicDetailsScreen.Presenter presenter;
    com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent vmenuComponent;

    class Anon1 implements com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.Callback {
        Anon1() {
        }

        public void select(com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState selection) {
            com.navdy.hud.app.framework.music.MusicDetailsView.this.presenter.selectItem(selection);
        }

        public void onLoad() {
            com.navdy.hud.app.framework.music.MusicDetailsView.sLogger.v("onLoad");
            com.navdy.hud.app.framework.music.MusicDetailsView.this.presenter.resetSelectedItem();
        }

        public boolean isItemClickable(com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState selection) {
            return com.navdy.hud.app.framework.music.MusicDetailsView.this.presenter.isItemClickable(selection);
        }

        public void onBindToView(com.navdy.hud.app.ui.component.vlist.VerticalList.Model model, android.view.View view, int pos, com.navdy.hud.app.ui.component.vlist.VerticalList.ModelState state) {
        }

        public void onItemSelected(com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState selection) {
        }

        public void onScrollIdle() {
        }

        public void onFastScrollStart() {
        }

        public void onFastScrollEnd() {
        }

        public void showToolTip() {
        }

        public void close() {
            com.navdy.hud.app.framework.music.MusicDetailsView.this.presenter.close();
        }

        public boolean isClosed() {
            return com.navdy.hud.app.framework.music.MusicDetailsView.this.presenter.isClosed();
        }
    }

    static {
        android.content.res.Resources resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        musicRightContainerSize = resources.getDimensionPixelSize(com.navdy.hud.app.R.dimen.music_details_rightC_w);
        musicRightContainerLeftMargin = resources.getDimensionPixelSize(com.navdy.hud.app.R.dimen.music_details_rightC_left_margin);
    }

    public MusicDetailsView(android.content.Context context) {
        this(context, null);
    }

    public MusicDetailsView(android.content.Context context, android.util.AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MusicDetailsView(android.content.Context context, android.util.AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.callback = new com.navdy.hud.app.framework.music.MusicDetailsView.Anon1();
        if (!isInEditMode()) {
            mortar.Mortar.inject(context, this);
        }
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
        butterknife.ButterKnife.inject((android.view.View) this);
        this.vmenuComponent = new com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent(this, this.callback, true);
        this.vmenuComponent.leftContainer.setAlpha(0.0f);
        this.vmenuComponent.rightContainer.setAlpha(0.0f);
        android.view.ViewGroup.MarginLayoutParams layoutParams = (android.view.ViewGroup.MarginLayoutParams) this.vmenuComponent.rightContainer.getLayoutParams();
        layoutParams.width = musicRightContainerSize;
        layoutParams.leftMargin = musicRightContainerLeftMargin;
        android.view.LayoutInflater layoutInflater = android.view.LayoutInflater.from(getContext());
        this.vmenuComponent.leftContainer.removeAllViews();
        this.albumArtContainer = (android.view.ViewGroup) layoutInflater.inflate(com.navdy.hud.app.R.layout.music_details_album_art, this, false);
        this.albumArt = (android.widget.ImageView) this.albumArtContainer.findViewById(com.navdy.hud.app.R.id.albumArt);
        this.counter = (android.widget.TextView) this.albumArtContainer.findViewById(com.navdy.hud.app.R.id.counter);
        this.albumArt.setImageDrawable(new android.graphics.drawable.ColorDrawable(-12303292));
        this.vmenuComponent.leftContainer.addView(this.albumArtContainer);
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.presenter != null) {
            this.presenter.takeView(this);
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.vmenuComponent.clear();
        if (this.presenter != null) {
            this.presenter.dropView(this);
        }
    }

    public boolean onGesture(com.navdy.service.library.events.input.GestureEvent event) {
        return this.vmenuComponent.handleGesture(event);
    }

    public boolean onKey(com.navdy.hud.app.manager.InputManager.CustomKeyEvent event) {
        return this.vmenuComponent.handleKey(event);
    }

    public com.navdy.hud.app.manager.InputManager.IInputHandler nextHandler() {
        return com.navdy.hud.app.manager.InputManager.nextContainingHandler(this);
    }

    /* access modifiers changed from: 0000 */
    public void performSelectionAnimation(java.lang.Runnable endAction, int delay) {
        this.vmenuComponent.performSelectionAnimation(endAction, delay);
    }
}
