package com.navdy.hud.app.framework.music;

public class MusicNotification implements com.navdy.hud.app.framework.notifications.INotification, com.navdy.hud.app.manager.MusicManager.MusicUpdateListener {
    private static final float ARTWORK_ALPHA_NOT_PLAYING = 0.5f;
    private static final float ARTWORK_ALPHA_PLAYING = 1.0f;
    private static final java.lang.String EMPTY = "";
    private static final int MUSIC_TIMEOUT = 5000;
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.framework.music.MusicNotification.class);
    /* access modifiers changed from: private */
    public final com.navdy.service.library.util.ScalingUtilities.ScalingLogic FIT = com.navdy.service.library.util.ScalingUtilities.ScalingLogic.FIT;
    private com.squareup.otto.Bus bus;
    private com.navdy.hud.app.framework.music.MediaControllerLayout choiceLayout;
    private android.view.ViewGroup container;
    private com.navdy.hud.app.framework.notifications.INotificationController controller;
    private android.os.Handler handler;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.framework.music.AlbumArtImageView image;
    private java.util.concurrent.atomic.AtomicBoolean isKeyPressedDown = new java.util.concurrent.atomic.AtomicBoolean(false);
    private java.lang.String lastEventIdentifier;
    private com.navdy.hud.app.manager.MusicManager musicManager;
    private android.widget.ProgressBar progressBar;
    private android.widget.TextView subTitle;
    private android.widget.TextView title;
    private com.navdy.service.library.events.audio.MusicTrackInfo trackInfo;

    class Anon1 implements java.lang.Runnable {
        final /* synthetic */ boolean val$animate;
        final /* synthetic */ okio.ByteString val$photo;

        Anon1(okio.ByteString byteString, boolean z) {
            this.val$photo = byteString;
            this.val$animate = z;
        }

        public void run() {
            com.navdy.hud.app.framework.music.MusicNotification.sLogger.d("PhotoUpdate runnable " + this.val$photo);
            byte[] byteArray = this.val$photo.toByteArray();
            if (byteArray == null || byteArray.length <= 0) {
                com.navdy.hud.app.framework.music.MusicNotification.sLogger.i("Received photo has null or empty byte array");
                com.navdy.hud.app.framework.music.MusicNotification.this.setDefaultImage(true);
                return;
            }
            int size = com.navdy.hud.app.HudApplication.getAppContext().getResources().getDimensionPixelSize(com.navdy.hud.app.R.dimen.music_notification_image_size);
            android.graphics.Bitmap artwork = null;
            android.graphics.Bitmap scaled = null;
            try {
                artwork = com.navdy.service.library.util.ScalingUtilities.decodeByteArray(byteArray, size, size, com.navdy.hud.app.framework.music.MusicNotification.this.FIT);
                scaled = com.navdy.service.library.util.ScalingUtilities.createScaledBitmap(artwork, size, size, com.navdy.hud.app.framework.music.MusicNotification.this.FIT);
                com.navdy.hud.app.framework.music.MusicNotification.this.setImage(scaled, this.val$animate);
                if (artwork != null && artwork != scaled) {
                    artwork.recycle();
                }
            } catch (java.lang.Exception e) {
                com.navdy.hud.app.framework.music.MusicNotification.sLogger.e("Error updating the artwork received ", e);
                com.navdy.hud.app.framework.music.MusicNotification.this.setDefaultImage(this.val$animate);
                if (artwork != null && artwork != scaled) {
                    artwork.recycle();
                }
            } catch (Throwable th) {
                if (!(artwork == null || artwork == scaled)) {
                    artwork.recycle();
                }
                throw th;
            }
        }
    }

    class Anon2 implements java.lang.Runnable {
        final /* synthetic */ boolean val$animate;

        Anon2(boolean z) {
            this.val$animate = z;
        }

        public void run() {
            com.navdy.hud.app.framework.music.MusicNotification.this.image.setArtworkBitmap(null, this.val$animate);
        }
    }

    class Anon3 implements java.lang.Runnable {
        final /* synthetic */ boolean val$animate;
        final /* synthetic */ android.graphics.Bitmap val$artwork;

        Anon3(android.graphics.Bitmap bitmap, boolean z) {
            this.val$artwork = bitmap;
            this.val$animate = z;
        }

        public void run() {
            com.navdy.hud.app.framework.music.MusicNotification.this.image.setArtworkBitmap(this.val$artwork, this.val$animate);
        }
    }

    public MusicNotification(com.navdy.hud.app.manager.MusicManager musicManager2, com.squareup.otto.Bus bus2) {
        this.musicManager = musicManager2;
        this.bus = bus2;
        this.handler = new android.os.Handler();
    }

    public com.navdy.hud.app.framework.notifications.NotificationType getType() {
        return com.navdy.hud.app.framework.notifications.NotificationType.MUSIC;
    }

    public java.lang.String getId() {
        return com.navdy.hud.app.framework.notifications.NotificationId.MUSIC_NOTIFICATION_ID;
    }

    public android.view.View getView(android.content.Context context) {
        if (this.container == null) {
            this.container = (android.view.ViewGroup) android.view.LayoutInflater.from(context).inflate(com.navdy.hud.app.R.layout.notification_music, null);
            this.title = (android.widget.TextView) this.container.findViewById(com.navdy.hud.app.R.id.title);
            this.subTitle = (android.widget.TextView) this.container.findViewById(com.navdy.hud.app.R.id.subTitle);
            this.image = (com.navdy.hud.app.framework.music.AlbumArtImageView) this.container.findViewById(com.navdy.hud.app.R.id.image);
            this.progressBar = (android.widget.ProgressBar) this.container.findViewById(com.navdy.hud.app.R.id.music_progress);
            this.choiceLayout = (com.navdy.hud.app.framework.music.MediaControllerLayout) this.container.findViewById(com.navdy.hud.app.R.id.choiceLayout);
        }
        return this.container;
    }

    public android.view.View getExpandedView(android.content.Context context, java.lang.Object data) {
        return null;
    }

    public int getExpandedViewIndicatorColor() {
        return 0;
    }

    public void onStart(com.navdy.hud.app.framework.notifications.INotificationController controller2) {
        this.controller = controller2;
        this.bus.register(this);
        this.musicManager.addMusicUpdateListener(this);
        updateProgressBar(this.musicManager.getCurrentPosition());
    }

    public void onUpdate() {
        onTrackUpdated(this.musicManager.getCurrentTrack(), this.musicManager.getCurrentControls(), false);
        updateProgressBar(this.musicManager.getCurrentPosition());
    }

    public void onStop() {
        this.musicManager.removeMusicUpdateListener(this);
        this.bus.unregister(this);
        this.controller = null;
    }

    public int getTimeout() {
        return 5000;
    }

    public boolean isAlive() {
        return false;
    }

    public boolean isPurgeable() {
        return false;
    }

    public boolean canAddToStackIfCurrentExists() {
        return false;
    }

    public int getColor() {
        return 0;
    }

    public void onNotificationEvent(com.navdy.hud.app.ui.framework.UIStateManager.Mode mode) {
    }

    public void onExpandedNotificationEvent(com.navdy.hud.app.ui.framework.UIStateManager.Mode mode) {
    }

    public void onExpandedNotificationSwitched() {
    }

    public android.animation.AnimatorSet getViewSwitchAnimation(boolean viewIn) {
        return null;
    }

    public boolean expandNotification() {
        return false;
    }

    public boolean supportScroll() {
        return false;
    }

    public boolean onKey(com.navdy.hud.app.manager.InputManager.CustomKeyEvent event) {
        if (this.controller == null) {
            return false;
        }
        if (this.choiceLayout != null) {
            switch (event) {
                case LEFT:
                    this.controller.resetTimeout();
                    this.choiceLayout.moveSelectionLeft();
                    return true;
                case RIGHT:
                    this.controller.resetTimeout();
                    this.choiceLayout.moveSelectionRight();
                    return true;
                case SELECT:
                    this.controller.resetTimeout();
                    com.navdy.hud.app.ui.component.ChoiceLayout.Choice choice = this.choiceLayout.getSelectedItem();
                    if (choice == null) {
                        sLogger.w("Choice layout selected item is null, returning");
                        return false;
                    }
                    com.navdy.hud.app.manager.MusicManager.MediaControl action = com.navdy.hud.app.manager.MusicManager.CONTROLS[choice.id];
                    switch (action) {
                        case PLAY:
                        case PAUSE:
                            if (this.trackInfo != null) {
                                this.musicManager.executeMediaControl(action, this.isKeyPressedDown.get());
                            }
                            this.choiceLayout.executeSelectedItem(true);
                            return true;
                        case MUSIC_MENU:
                            this.choiceLayout.executeSelectedItem(true);
                            android.os.Bundle args = new android.os.Bundle();
                            java.lang.String path = this.musicManager.getMusicMenuPath();
                            if (path == null) {
                                path = com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.SLASH + com.navdy.hud.app.ui.component.mainmenu.IMenu.Menu.MUSIC.name();
                            }
                            args.putString(com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.ARG_MENU_PATH, path);
                            com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().removeNotification(getId(), true, com.navdy.service.library.events.ui.Screen.SCREEN_MAIN_MENU, args, null);
                            return true;
                        default:
                            return false;
                    }
                case LONG_PRESS:
                    this.controller.resetTimeout();
                    return true;
            }
        }
        return false;
    }

    @com.squareup.otto.Subscribe
    public void onKeyEvent(android.view.KeyEvent event) {
        if (this.controller != null && this.choiceLayout != null && com.navdy.hud.app.manager.InputManager.isCenterKey(event.getKeyCode())) {
            com.navdy.hud.app.ui.component.ChoiceLayout.Choice selectedItem = this.choiceLayout.getSelectedItem();
            if (selectedItem == null) {
                sLogger.w("Choice layout selected item is null, returning");
                return;
            }
            com.navdy.hud.app.manager.MusicManager.MediaControl action = com.navdy.hud.app.manager.MusicManager.CONTROLS[selectedItem.id];
            if (action == com.navdy.hud.app.manager.MusicManager.MediaControl.NEXT || action == com.navdy.hud.app.manager.MusicManager.MediaControl.PREVIOUS) {
                this.controller.resetTimeout();
                if (this.trackInfo != null) {
                    this.musicManager.handleKeyEvent(event, action, this.isKeyPressedDown.get());
                }
                if (event.getAction() == 0 && this.isKeyPressedDown.compareAndSet(false, true)) {
                    this.choiceLayout.keyDownSelectedItem();
                } else if (event.getAction() == 1 && this.isKeyPressedDown.compareAndSet(true, false)) {
                    this.choiceLayout.keyUpSelectedItem();
                }
            }
        }
    }

    public void updateProgressBar(int value) {
        if (this.controller != null) {
            this.progressBar.setProgress(value);
        }
    }

    public void onTrackUpdated(@android.support.annotation.NonNull com.navdy.service.library.events.audio.MusicTrackInfo trackInfo2, java.util.Set<com.navdy.hud.app.manager.MusicManager.MediaControl> mediaControls, boolean willOpenNotification) {
        this.trackInfo = trackInfo2;
        if (this.controller != null) {
            java.lang.String songIdentifier = com.navdy.service.library.util.MusicDataUtils.songIdentifierFromTrackInfo(trackInfo2);
            if (this.lastEventIdentifier != null && !this.lastEventIdentifier.equals(songIdentifier)) {
                this.controller.resetTimeout();
            }
            this.lastEventIdentifier = songIdentifier;
            if (trackInfo2.name != null) {
                this.subTitle.setText(trackInfo2.name);
            } else {
                this.subTitle.setText("");
            }
            if (trackInfo2.author != null) {
                this.title.setText(trackInfo2.author);
            } else {
                this.title.setText("");
            }
            if (!(trackInfo2.duration == null || trackInfo2.currentPosition == null)) {
                this.progressBar.setMax(trackInfo2.duration.intValue());
                this.progressBar.setSecondaryProgress(trackInfo2.duration.intValue());
            }
            if (com.navdy.hud.app.manager.MusicManager.tryingToPlay(trackInfo2.playbackState)) {
                this.image.setAlpha(1.0f);
            } else {
                this.image.setAlpha(0.5f);
            }
            this.choiceLayout.updateControls(mediaControls);
        }
    }

    public void onAlbumArtUpdate(@android.support.annotation.Nullable okio.ByteString photo, boolean animate) {
        if (photo == null) {
            sLogger.v("ByteString image to set in notification is null");
            setDefaultImage(animate);
            return;
        }
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.framework.music.MusicNotification.Anon1(photo, animate), 1);
    }

    /* access modifiers changed from: private */
    public void setDefaultImage(boolean animate) {
        this.handler.post(new com.navdy.hud.app.framework.music.MusicNotification.Anon2(animate));
    }

    /* access modifiers changed from: private */
    public void setImage(android.graphics.Bitmap artwork, boolean animate) {
        this.handler.post(new com.navdy.hud.app.framework.music.MusicNotification.Anon3(artwork, animate));
    }

    public void onClick() {
    }

    public void onTrackHand(float normalized) {
    }

    public boolean onGesture(com.navdy.service.library.events.input.GestureEvent event) {
        return false;
    }

    public com.navdy.hud.app.manager.InputManager.IInputHandler nextHandler() {
        return null;
    }
}
