package com.navdy.hud.app.framework.music;

public class MediaControllerLayout extends com.navdy.hud.app.ui.component.ChoiceLayout {
    public static final java.util.HashMap<com.navdy.hud.app.manager.MusicManager.MediaControl, java.lang.Integer> MEDIA_CONTROL_ICON_MAPPING = new java.util.HashMap<>();
    private java.util.Set<com.navdy.hud.app.manager.MusicManager.MediaControl> availableMediaControls;

    static {
        MEDIA_CONTROL_ICON_MAPPING.put(com.navdy.hud.app.manager.MusicManager.MediaControl.PLAY, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_play_sm_2));
        MEDIA_CONTROL_ICON_MAPPING.put(com.navdy.hud.app.manager.MusicManager.MediaControl.PAUSE, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_pause_sm_2));
        MEDIA_CONTROL_ICON_MAPPING.put(com.navdy.hud.app.manager.MusicManager.MediaControl.PREVIOUS, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_prev_sm_2));
        MEDIA_CONTROL_ICON_MAPPING.put(com.navdy.hud.app.manager.MusicManager.MediaControl.NEXT, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_next_sm_2));
        MEDIA_CONTROL_ICON_MAPPING.put(com.navdy.hud.app.manager.MusicManager.MediaControl.MUSIC_MENU, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_music_sm_2));
        MEDIA_CONTROL_ICON_MAPPING.put(com.navdy.hud.app.manager.MusicManager.MediaControl.MUSIC_MENU_DEEP, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_music_queue_sm));
    }

    public MediaControllerLayout(android.content.Context context) {
        super(context);
    }

    public MediaControllerLayout(android.content.Context context, android.util.AttributeSet attrs) {
        super(context, attrs);
    }

    public MediaControllerLayout(android.content.Context context, android.util.AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void updateControls(java.util.Set<com.navdy.hud.app.manager.MusicManager.MediaControl> controls) {
        int i = 0;
        boolean changed = false;
        if (controls == null || controls.size() == 0) {
            this.availableMediaControls = controls;
            setVisibility(4);
        } else if (this.availableMediaControls == null || this.availableMediaControls.size() != controls.size()) {
            this.availableMediaControls = controls;
            java.util.List<com.navdy.hud.app.ui.component.ChoiceLayout.Choice> choices = getChoicesForControls();
            setChoices(com.navdy.hud.app.ui.component.ChoiceLayout.Mode.ICON, choices, getDefaultSelection(choices), null);
            setHighlightVisibility(0);
            setVisibility(0);
        } else {
            com.navdy.hud.app.manager.MusicManager.MediaControl[] mediaControlArr = com.navdy.hud.app.manager.MusicManager.CONTROLS;
            int length = mediaControlArr.length;
            while (true) {
                if (i >= length) {
                    break;
                }
                com.navdy.hud.app.manager.MusicManager.MediaControl control = mediaControlArr[i];
                if (controls.contains(control) != this.availableMediaControls.contains(control)) {
                    changed = true;
                    this.availableMediaControls = controls;
                    break;
                }
                i++;
            }
            java.util.List<com.navdy.hud.app.ui.component.ChoiceLayout.Choice> choices2 = getChoicesForControls();
            if (changed) {
                updateChoices(choices2);
                return;
            }
            int selected = getSelectedItemIndex();
            if (com.navdy.hud.app.ui.component.UISettings.isMusicBrowsingEnabled() && selected == 0) {
                setSelectedItem(getDefaultSelection(choices2));
            }
        }
    }

    private int getDefaultSelection(java.util.List<com.navdy.hud.app.ui.component.ChoiceLayout.Choice> choices) {
        int selection = 0;
        for (com.navdy.hud.app.ui.component.ChoiceLayout.Choice choice : choices) {
            if (choice.id == com.navdy.hud.app.manager.MusicManager.MediaControl.MUSIC_MENU.ordinal() || choice.id == com.navdy.hud.app.manager.MusicManager.MediaControl.PREVIOUS.ordinal()) {
                selection++;
            }
        }
        return selection;
    }

    private java.util.List<com.navdy.hud.app.ui.component.ChoiceLayout.Choice> getChoicesForControls() {
        int iconRes;
        java.util.List<com.navdy.hud.app.ui.component.ChoiceLayout.Choice> choices = new java.util.ArrayList<>();
        if (com.navdy.hud.app.ui.component.UISettings.isMusicBrowsingEnabled()) {
            if (com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getMusicManager().getMusicMenuPath() != null) {
                iconRes = ((java.lang.Integer) MEDIA_CONTROL_ICON_MAPPING.get(com.navdy.hud.app.manager.MusicManager.MediaControl.MUSIC_MENU_DEEP)).intValue();
            } else {
                iconRes = ((java.lang.Integer) MEDIA_CONTROL_ICON_MAPPING.get(com.navdy.hud.app.manager.MusicManager.MediaControl.MUSIC_MENU)).intValue();
            }
            choices.add(new com.navdy.hud.app.ui.component.ChoiceLayout.Choice(new com.navdy.hud.app.ui.component.ChoiceLayout.Icon(iconRes, iconRes), com.navdy.hud.app.manager.MusicManager.MediaControl.MUSIC_MENU.ordinal()));
        }
        if (this.availableMediaControls.contains(com.navdy.hud.app.manager.MusicManager.MediaControl.PREVIOUS)) {
            int iconRes2 = ((java.lang.Integer) MEDIA_CONTROL_ICON_MAPPING.get(com.navdy.hud.app.manager.MusicManager.MediaControl.PREVIOUS)).intValue();
            choices.add(new com.navdy.hud.app.ui.component.ChoiceLayout.Choice(new com.navdy.hud.app.ui.component.ChoiceLayout.Icon(iconRes2, iconRes2), com.navdy.hud.app.manager.MusicManager.MediaControl.PREVIOUS.ordinal()));
        }
        com.navdy.hud.app.manager.MusicManager.MediaControl control = this.availableMediaControls.contains(com.navdy.hud.app.manager.MusicManager.MediaControl.PLAY) ? com.navdy.hud.app.manager.MusicManager.MediaControl.PLAY : com.navdy.hud.app.manager.MusicManager.MediaControl.PAUSE;
        int iconRes3 = ((java.lang.Integer) MEDIA_CONTROL_ICON_MAPPING.get(control)).intValue();
        choices.add(new com.navdy.hud.app.ui.component.ChoiceLayout.Choice(new com.navdy.hud.app.ui.component.ChoiceLayout.Icon(iconRes3, iconRes3), control.ordinal()));
        if (this.availableMediaControls.contains(com.navdy.hud.app.manager.MusicManager.MediaControl.NEXT)) {
            int iconRes4 = ((java.lang.Integer) MEDIA_CONTROL_ICON_MAPPING.get(com.navdy.hud.app.manager.MusicManager.MediaControl.NEXT)).intValue();
            choices.add(new com.navdy.hud.app.ui.component.ChoiceLayout.Choice(new com.navdy.hud.app.ui.component.ChoiceLayout.Icon(iconRes4, iconRes4), com.navdy.hud.app.manager.MusicManager.MediaControl.NEXT.ordinal()));
        }
        return choices;
    }

    private void updateChoices(java.util.List<com.navdy.hud.app.ui.component.ChoiceLayout.Choice> choices) {
        this.choices = choices;
        for (int i = 0; i < this.choices.size(); i++) {
            ((android.widget.ImageView) this.choiceContainer.getChildAt(i)).setImageResource(((com.navdy.hud.app.ui.component.ChoiceLayout.Choice) this.choices.get(i)).icon.resIdSelected);
        }
    }

    /* access modifiers changed from: protected */
    public boolean isHighlightPersistent() {
        return true;
    }
}
