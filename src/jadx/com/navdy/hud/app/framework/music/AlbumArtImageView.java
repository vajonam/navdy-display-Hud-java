package com.navdy.hud.app.framework.music;

public class AlbumArtImageView extends android.widget.ImageView {
    private static final int ARTWORK_TRANSITION_DURATION = 1000;
    private static com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.framework.music.AlbumArtImageView.class);
    /* access modifiers changed from: private */
    public final java.lang.Object artworkLock;
    private android.graphics.drawable.BitmapDrawable defaultArtwork;
    private android.os.Handler handler;
    /* access modifiers changed from: private */
    public java.util.concurrent.atomic.AtomicBoolean isAnimatingArtwork;
    private boolean mask;
    private android.graphics.Bitmap nextArtwork;
    /* access modifiers changed from: private */
    public java.lang.String nextArtworkHash;
    private android.graphics.Paint paint;
    private android.graphics.RadialGradient radialGradient;

    class Anon1 implements java.lang.Runnable {
        final /* synthetic */ java.lang.String val$artworkHash;
        final /* synthetic */ android.graphics.drawable.Drawable[] val$drawables;

        Anon1(android.graphics.drawable.Drawable[] drawableArr, java.lang.String str) {
            this.val$drawables = drawableArr;
            this.val$artworkHash = str;
        }

        public void run() {
            boolean sameArtwork;
            com.navdy.hud.app.framework.music.AlbumArtImageView.this.setImageDrawable(new android.graphics.drawable.LayerDrawable(new android.graphics.drawable.Drawable[]{this.val$drawables[1]}));
            synchronized (com.navdy.hud.app.framework.music.AlbumArtImageView.this.artworkLock) {
                sameArtwork = android.text.TextUtils.equals(this.val$artworkHash, com.navdy.hud.app.framework.music.AlbumArtImageView.this.nextArtworkHash);
            }
            if (sameArtwork) {
                com.navdy.hud.app.framework.music.AlbumArtImageView.this.isAnimatingArtwork.set(false);
            } else {
                com.navdy.hud.app.framework.music.AlbumArtImageView.this.animateArtwork();
            }
        }
    }

    public AlbumArtImageView(android.content.Context context) {
        this(context, null);
    }

    public AlbumArtImageView(android.content.Context context, android.util.AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AlbumArtImageView(android.content.Context context, android.util.AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.isAnimatingArtwork = new java.util.concurrent.atomic.AtomicBoolean(false);
        this.artworkLock = new java.lang.Object();
        this.handler = new android.os.Handler(android.os.Looper.getMainLooper());
        init(attrs);
    }

    private void init(android.util.AttributeSet attrs) {
        int defaultArtworkResource = com.navdy.hud.app.R.drawable.icon_mm_music_control_blank;
        if (attrs != null) {
            android.content.res.TypedArray a = getContext().obtainStyledAttributes(attrs, com.navdy.hud.app.R.styleable.AlbumArtImageView);
            this.mask = a.getBoolean(0, false);
            defaultArtworkResource = a.getResourceId(1, com.navdy.hud.app.R.drawable.icon_mm_music_control_blank);
            a.recycle();
        }
        android.graphics.drawable.BitmapDrawable drawable = (android.graphics.drawable.BitmapDrawable) getResources().getDrawable(defaultArtworkResource);
        if (this.mask) {
            this.paint = new android.graphics.Paint();
            int[] colors = getResources().getIntArray(com.navdy.hud.app.R.array.smart_dash_music_gauge_gradient);
            android.content.res.TypedArray a2 = getContext().obtainStyledAttributes(attrs, new int[]{16842996});
            float radius = (float) (a2.getDimensionPixelSize(0, 130) / 2);
            a2.recycle();
            this.radialGradient = new android.graphics.RadialGradient(radius, radius, radius, colors, null, android.graphics.Shader.TileMode.CLAMP);
            this.defaultArtwork = new android.graphics.drawable.BitmapDrawable(getResources(), addMask(drawable.getBitmap()));
        } else {
            this.defaultArtwork = drawable;
        }
        setImageDrawable(new android.graphics.drawable.LayerDrawable(new android.graphics.drawable.Drawable[]{this.defaultArtwork}));
    }

    public void setArtworkBitmap(@android.support.annotation.Nullable android.graphics.Bitmap bitmap, boolean animate) {
        java.lang.String hash = com.navdy.service.library.util.IOUtils.hashForBitmap(bitmap);
        if (android.text.TextUtils.equals(this.nextArtworkHash, hash)) {
            logger.d("Already set this artwork, ignoring");
            return;
        }
        synchronized (this) {
            this.nextArtworkHash = hash;
            if (this.mask) {
                bitmap = addMask(bitmap);
            }
            this.nextArtwork = bitmap;
        }
        if (!animate) {
            drawImmediately();
        } else if (!this.isAnimatingArtwork.compareAndSet(false, true)) {
            logger.d("Already animating");
        } else {
            animateArtwork();
        }
    }

    /* access modifiers changed from: private */
    public void animateArtwork() {
        java.lang.String artworkHash;
        android.graphics.Bitmap artwork;
        synchronized (this.artworkLock) {
            artworkHash = this.nextArtworkHash;
            artwork = this.nextArtwork;
        }
        android.graphics.drawable.Drawable[] drawables = new android.graphics.drawable.Drawable[2];
        android.graphics.drawable.Drawable currentDrawable = getDrawable();
        if (currentDrawable == null) {
            drawables[0] = new android.graphics.drawable.ColorDrawable(0);
        } else {
            drawables[0] = ((android.graphics.drawable.LayerDrawable) currentDrawable).getDrawable(0);
        }
        if (artwork != null) {
            drawables[1] = new android.graphics.drawable.BitmapDrawable(getContext().getResources(), artwork);
        } else {
            logger.i("Bitmap image is null");
            drawables[1] = this.defaultArtwork;
        }
        android.graphics.drawable.TransitionDrawable drawable = new android.graphics.drawable.TransitionDrawable(drawables);
        setImageDrawable(drawable);
        drawable.setCrossFadeEnabled(true);
        drawable.startTransition(1000);
        this.handler.postDelayed(new com.navdy.hud.app.framework.music.AlbumArtImageView.Anon1(drawables, artworkHash), 1000);
    }

    private void drawImmediately() {
        this.isAnimatingArtwork.set(false);
        this.handler.removeCallbacksAndMessages(null);
        android.graphics.drawable.Drawable[] layers = new android.graphics.drawable.Drawable[1];
        if (this.nextArtwork != null) {
            layers[0] = new android.graphics.drawable.BitmapDrawable(getContext().getResources(), this.nextArtwork);
        } else {
            logger.i("Bitmap image is null");
            layers[0] = this.defaultArtwork;
        }
        setImageDrawable(new android.graphics.drawable.LayerDrawable(layers));
    }

    @android.support.annotation.Nullable
    private android.graphics.Bitmap addMask(@android.support.annotation.Nullable android.graphics.Bitmap src) {
        if (src == null) {
            return null;
        }
        int w = src.getWidth();
        int h = src.getHeight();
        android.graphics.Bitmap bitmap = android.graphics.Bitmap.createBitmap(w, h, android.graphics.Bitmap.Config.ARGB_8888);
        android.graphics.Canvas canvas = new android.graphics.Canvas(bitmap);
        canvas.drawBitmap(src, 0.0f, 0.0f, null);
        this.paint.setShader(this.radialGradient);
        this.paint.setXfermode(new android.graphics.PorterDuffXfermode(android.graphics.PorterDuff.Mode.MULTIPLY));
        canvas.drawRect(0.0f, 0.0f, (float) w, (float) h, this.paint);
        return bitmap;
    }
}
