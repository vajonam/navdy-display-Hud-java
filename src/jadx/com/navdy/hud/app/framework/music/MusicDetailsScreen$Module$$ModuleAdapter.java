package com.navdy.hud.app.framework.music;

public final class MusicDetailsScreen$Module$$ModuleAdapter extends dagger.internal.ModuleAdapter<com.navdy.hud.app.framework.music.MusicDetailsScreen.Module> {
    private static final java.lang.Class<?>[] INCLUDES = new java.lang.Class[0];
    private static final java.lang.String[] INJECTS = {"members/com.navdy.hud.app.framework.music.MusicDetailsView"};
    private static final java.lang.Class<?>[] STATIC_INJECTIONS = new java.lang.Class[0];

    public MusicDetailsScreen$Module$$ModuleAdapter() {
        super(com.navdy.hud.app.framework.music.MusicDetailsScreen.Module.class, INJECTS, STATIC_INJECTIONS, false, INCLUDES, true, false);
    }
}
