package com.navdy.hud.app.framework.contacts;

public class Contact implements android.os.Parcelable {
    public static final android.os.Parcelable.Creator<com.navdy.hud.app.framework.contacts.Contact> CREATOR = new com.navdy.hud.app.framework.contacts.Contact.Anon1();
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.framework.contacts.Contact.class);
    public int defaultImageIndex;
    public java.lang.String firstName;
    public java.lang.String formattedNumber;
    public java.lang.String initials;
    public java.lang.String name;
    public java.lang.String number;
    public com.navdy.hud.app.framework.contacts.NumberType numberType;
    public java.lang.String numberTypeStr;
    public long numericNumber;

    static class Anon1 implements android.os.Parcelable.Creator<com.navdy.hud.app.framework.contacts.Contact> {
        Anon1() {
        }

        public com.navdy.hud.app.framework.contacts.Contact createFromParcel(android.os.Parcel in) {
            return new com.navdy.hud.app.framework.contacts.Contact(in);
        }

        public com.navdy.hud.app.framework.contacts.Contact[] newArray(int size) {
            return new com.navdy.hud.app.framework.contacts.Contact[size];
        }
    }

    public Contact(java.lang.String name2, java.lang.String number2, com.navdy.hud.app.framework.contacts.NumberType numberType2, int defaultImageIndex2, long numericNumber2) {
        this.name = name2;
        this.number = number2;
        this.numberType = numberType2;
        this.defaultImageIndex = defaultImageIndex2;
        this.numericNumber = numericNumber2;
        init(numericNumber2);
    }

    public Contact(com.navdy.service.library.events.contacts.PhoneNumber phoneNumber) {
        this.number = phoneNumber.number;
        this.numberType = com.navdy.hud.app.framework.contacts.ContactUtil.getNumberType(phoneNumber.numberType);
        this.defaultImageIndex = com.navdy.hud.app.framework.contacts.ContactImageHelper.getInstance().getContactImageIndex(this.number);
        this.numberTypeStr = phoneNumber.customType;
        init(0);
    }

    public Contact(com.navdy.service.library.events.contacts.Contact contact) {
        this.name = contact.name;
        this.number = contact.number;
        this.numberType = com.navdy.hud.app.framework.contacts.ContactUtil.getNumberType(contact.numberType);
        this.defaultImageIndex = com.navdy.hud.app.framework.contacts.ContactImageHelper.getInstance().getContactImageIndex(this.number);
        this.numberTypeStr = contact.label;
        init(0);
    }

    public Contact(android.os.Parcel in) {
        this.name = in.readString();
        this.number = in.readString();
        int i = in.readInt();
        if (i != -1) {
            this.numberType = com.navdy.hud.app.framework.contacts.NumberType.values()[i];
        }
        this.defaultImageIndex = in.readInt();
        this.numericNumber = in.readLong();
        this.firstName = in.readString();
        this.initials = in.readString();
        this.numberTypeStr = in.readString();
        this.formattedNumber = in.readString();
    }

    private void init(long numericNumber2) {
        if (!android.text.TextUtils.isEmpty(this.name)) {
            this.firstName = com.navdy.hud.app.framework.contacts.ContactUtil.getFirstName(this.name);
            this.initials = com.navdy.hud.app.framework.contacts.ContactUtil.getInitials(this.name);
        }
        if (this.numberTypeStr == null) {
            this.numberTypeStr = com.navdy.hud.app.framework.contacts.ContactUtil.getPhoneType(this.numberType);
        }
        this.formattedNumber = com.navdy.hud.app.util.PhoneUtil.formatPhoneNumber(this.number);
        java.util.Locale currentLocale = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentLocale();
        if (android.text.TextUtils.isEmpty(this.number)) {
            return;
        }
        if (numericNumber2 > 0) {
            this.numericNumber = numericNumber2;
            return;
        }
        try {
            this.numericNumber = com.google.i18n.phonenumbers.PhoneNumberUtil.getInstance().parse(this.number, currentLocale.getCountry()).getNationalNumber();
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }

    public void setName(java.lang.String name2) {
        this.name = name2;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.number);
        dest.writeInt(this.numberType != null ? this.numberType.ordinal() : -1);
        dest.writeInt(this.defaultImageIndex);
        dest.writeLong(this.numericNumber);
        dest.writeString(this.firstName);
        dest.writeString(this.initials);
        dest.writeString(this.numberTypeStr);
        dest.writeString(this.formattedNumber);
    }
}
