package com.navdy.hud.app.framework.contacts;

public class ContactUtil {
    private static final java.lang.String EMPTY = "";
    private static java.lang.String HOME_STR = null;
    private static java.util.HashSet<java.lang.Character> MARKER_CHARS = new java.util.HashSet<>();
    private static java.lang.String MOBILE_STR = null;
    public static final java.lang.String SPACE = " ";
    private static final char SPACE_CHAR = ' ';
    private static java.lang.String WORK_STR;
    private static java.lang.StringBuilder builder = new java.lang.StringBuilder();
    /* access modifiers changed from: private */
    public static android.os.Handler handler = new android.os.Handler(android.os.Looper.getMainLooper());

    static class Anon1 implements java.lang.Runnable {
        final /* synthetic */ java.lang.String val$cName;
        final /* synthetic */ com.navdy.hud.app.framework.contacts.ContactUtil.IContactCallback val$callback;
        final /* synthetic */ java.io.File val$imagePath;
        final /* synthetic */ com.navdy.hud.app.ui.component.image.InitialsImageView val$imageView;
        final /* synthetic */ java.lang.String val$number;
        final /* synthetic */ com.navdy.hud.app.framework.contacts.PhoneImageDownloader val$phoneImageDownloader;
        final /* synthetic */ com.squareup.picasso.Transformation val$transformation;
        final /* synthetic */ boolean val$triggerPhotoDownload;

        /* renamed from: com.navdy.hud.app.framework.contacts.ContactUtil$Anon1$Anon1 reason: collision with other inner class name */
        class C0008Anon1 implements java.lang.Runnable {
            final /* synthetic */ boolean val$imageExists;

            C0008Anon1(boolean z) {
                this.val$imageExists = z;
            }

            public void run() {
                com.navdy.hud.app.framework.contacts.ContactUtil.setContactPhoto(com.navdy.hud.app.framework.contacts.ContactUtil.Anon1.this.val$cName, com.navdy.hud.app.framework.contacts.ContactUtil.Anon1.this.val$number, com.navdy.hud.app.framework.contacts.ContactUtil.Anon1.this.val$triggerPhotoDownload, com.navdy.hud.app.framework.contacts.ContactUtil.Anon1.this.val$imageView, com.navdy.hud.app.framework.contacts.ContactUtil.Anon1.this.val$transformation, com.navdy.hud.app.framework.contacts.ContactUtil.Anon1.this.val$callback, com.navdy.hud.app.framework.contacts.ContactUtil.Anon1.this.val$imagePath, this.val$imageExists, com.navdy.hud.app.framework.contacts.ContactUtil.Anon1.this.val$phoneImageDownloader, null);
            }
        }

        Anon1(com.navdy.hud.app.framework.contacts.ContactUtil.IContactCallback iContactCallback, java.io.File file, java.lang.String str, java.lang.String str2, boolean z, com.navdy.hud.app.ui.component.image.InitialsImageView initialsImageView, com.squareup.picasso.Transformation transformation, com.navdy.hud.app.framework.contacts.PhoneImageDownloader phoneImageDownloader) {
            this.val$callback = iContactCallback;
            this.val$imagePath = file;
            this.val$cName = str;
            this.val$number = str2;
            this.val$triggerPhotoDownload = z;
            this.val$imageView = initialsImageView;
            this.val$transformation = transformation;
            this.val$phoneImageDownloader = phoneImageDownloader;
        }

        public void run() {
            if (this.val$callback.isContextValid()) {
                com.navdy.hud.app.framework.contacts.ContactUtil.handler.post(new com.navdy.hud.app.framework.contacts.ContactUtil.Anon1.C0008Anon1(this.val$imagePath.exists()));
            }
        }
    }

    static class Anon2 implements com.squareup.picasso.Callback {
        final /* synthetic */ com.navdy.hud.app.ui.component.image.InitialsImageView val$imageView;
        final /* synthetic */ java.io.File val$path;

        Anon2(com.navdy.hud.app.ui.component.image.InitialsImageView initialsImageView, java.io.File file) {
            this.val$imageView = initialsImageView;
            this.val$path = file;
        }

        public void onSuccess() {
            this.val$imageView.setTag(this.val$path.getAbsolutePath());
        }

        public void onError() {
        }
    }

    public interface IContactCallback {
        boolean isContextValid();
    }

    static {
        android.content.res.Resources resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        WORK_STR = resources.getString(com.navdy.hud.app.R.string.work);
        MOBILE_STR = resources.getString(com.navdy.hud.app.R.string.mobile);
        HOME_STR = resources.getString(com.navdy.hud.app.R.string.home);
        MARKER_CHARS.add(java.lang.Character.valueOf(8206));
        MARKER_CHARS.add(java.lang.Character.valueOf(8234));
        MARKER_CHARS.add(java.lang.Character.valueOf(8236));
        MARKER_CHARS.add(java.lang.Character.valueOf(8206));
    }

    public static java.lang.String getFirstName(java.lang.String name) {
        if (android.text.TextUtils.isEmpty(name)) {
            return "";
        }
        java.lang.String name2 = name.trim();
        int index = name2.indexOf(" ");
        if (index > 0) {
            return name2.substring(0, index);
        }
        return name2;
    }

    public static synchronized java.lang.String getInitials(java.lang.String name) {
        java.lang.String str;
        synchronized (com.navdy.hud.app.framework.contacts.ContactUtil.class) {
            if (android.text.TextUtils.isEmpty(name)) {
                str = "";
            } else {
                java.lang.String name2 = com.navdy.hud.app.util.GenericUtil.removePunctuation(name).trim();
                int index = name2.indexOf(" ");
                if (index > 0) {
                    java.lang.String first = name2.substring(0, index).trim().replaceAll("[\\u202A]", "");
                    java.lang.String last = name2.substring(index + 1).trim().replaceAll("[\\u202A]", "");
                    builder.setLength(0);
                    if (!android.text.TextUtils.isEmpty(first)) {
                        builder.append(first.charAt(0));
                    }
                    if (!android.text.TextUtils.isEmpty(last)) {
                        builder.append(last.charAt(0));
                    }
                    str = builder.toString().toUpperCase();
                } else if (name2.length() > 0) {
                    str = java.lang.String.valueOf(name2.charAt(0)).toUpperCase();
                } else {
                    str = "";
                }
            }
        }
        return str;
    }

    public static java.lang.String getPhoneType(com.navdy.hud.app.framework.contacts.NumberType numberType) {
        if (numberType == null) {
            return null;
        }
        switch (numberType) {
            case WORK_MOBILE:
            case WORK:
                return WORK_STR;
            case MOBILE:
                return MOBILE_STR;
            case HOME:
                return HOME_STR;
            default:
                return null;
        }
    }

    public static boolean isValidNumber(java.lang.String number) {
        try {
            if (android.text.TextUtils.isEmpty(number)) {
                return false;
            }
            com.google.i18n.phonenumbers.Phonenumber.PhoneNumber parse = com.google.i18n.phonenumbers.PhoneNumberUtil.getInstance().parse(number, com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentLocale().getCountry());
            return true;
        } catch (Throwable th) {
            return false;
        }
    }

    public static boolean isDisplayNameValid(java.lang.String displayName, java.lang.String phoneNumber, long nPhoneNum) {
        if (android.text.TextUtils.isEmpty(displayName) && android.text.TextUtils.isEmpty(phoneNumber)) {
            return false;
        }
        if (!android.text.TextUtils.isEmpty(displayName) && android.text.TextUtils.isEmpty(phoneNumber)) {
            return true;
        }
        if (phoneNumber.equalsIgnoreCase(displayName)) {
            return false;
        }
        try {
            if (com.google.i18n.phonenumbers.PhoneNumberUtil.getInstance().parse(displayName, com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentLocale().getCountry()).getNationalNumber() == nPhoneNum) {
                return false;
            }
        } catch (Throwable th) {
        }
        return true;
    }

    public static long getPhoneNumber(java.lang.String number) {
        try {
            return com.google.i18n.phonenumbers.PhoneNumberUtil.getInstance().parse(number, com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentLocale().getCountry()).getNationalNumber();
        } catch (Throwable th) {
            return -1;
        }
    }

    public static boolean isImageAvailable(java.lang.String contactName, java.lang.String number) {
        if ((!android.text.TextUtils.isEmpty(contactName) || !android.text.TextUtils.isEmpty(number)) && !android.text.TextUtils.isEmpty(number)) {
            return true;
        }
        return false;
    }

    public static void setContactPhoto(java.lang.String contactName, java.lang.String number, boolean triggerPhotoDownload, com.navdy.hud.app.ui.component.image.InitialsImageView imageView, com.squareup.picasso.Transformation transformation, com.navdy.hud.app.framework.contacts.ContactUtil.IContactCallback callback) {
        imageView.setTag(null);
        if (android.text.TextUtils.isEmpty(contactName) && android.text.TextUtils.isEmpty(number)) {
            imageView.setImage(com.navdy.hud.app.R.drawable.icon_user_numberonly, null, com.navdy.hud.app.ui.component.image.InitialsImageView.Style.DEFAULT);
        } else if (android.text.TextUtils.isEmpty(number)) {
            imageView.setImage(com.navdy.hud.app.R.drawable.icon_user_numberonly, null, com.navdy.hud.app.ui.component.image.InitialsImageView.Style.DEFAULT);
        } else {
            if (!isDisplayNameValid(contactName, number, getPhoneNumber(number))) {
                contactName = null;
            }
            com.navdy.hud.app.framework.contacts.PhoneImageDownloader phoneImageDownloader = com.navdy.hud.app.framework.contacts.PhoneImageDownloader.getInstance();
            java.io.File imagePath = phoneImageDownloader.getImagePath(number, com.navdy.service.library.events.photo.PhotoType.PHOTO_CONTACT);
            android.graphics.Bitmap bitmap = com.navdy.hud.app.util.picasso.PicassoUtil.getBitmapfromCache(imagePath);
            if (bitmap != null) {
                imageView.setInitials(null, com.navdy.hud.app.ui.component.image.InitialsImageView.Style.DEFAULT);
                imageView.setImageBitmap(bitmap);
                setContactPhoto(contactName, number, triggerPhotoDownload, imageView, transformation, callback, imagePath, false, phoneImageDownloader, bitmap);
                return;
            }
            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.framework.contacts.ContactUtil.Anon1(callback, imagePath, contactName, number, triggerPhotoDownload, imageView, transformation, phoneImageDownloader), 1);
        }
    }

    /* access modifiers changed from: private */
    public static void setContactPhoto(java.lang.String contactName, java.lang.String number, boolean triggerPhotoDownload, com.navdy.hud.app.ui.component.image.InitialsImageView imageView, com.squareup.picasso.Transformation transformation, com.navdy.hud.app.framework.contacts.ContactUtil.IContactCallback callback, java.io.File imagePath, boolean imageExists, com.navdy.hud.app.framework.contacts.PhoneImageDownloader phoneImageDownloader, android.graphics.Bitmap bitmap) {
        if (bitmap == null) {
            if (callback.isContextValid()) {
                if (imageExists) {
                    updateInitials(com.navdy.hud.app.ui.component.image.InitialsImageView.Style.DEFAULT, null, imageView);
                    setImage(imagePath, imageView, transformation);
                } else if (contactName != null) {
                    com.navdy.hud.app.framework.contacts.ContactImageHelper contactImageHelper = com.navdy.hud.app.framework.contacts.ContactImageHelper.getInstance();
                    imageView.setImage(contactImageHelper.getResourceId(contactImageHelper.getContactImageIndex(number)), getInitials(contactName), com.navdy.hud.app.ui.component.image.InitialsImageView.Style.LARGE);
                } else {
                    imageView.setImage(com.navdy.hud.app.R.drawable.icon_user_numberonly, null, com.navdy.hud.app.ui.component.image.InitialsImageView.Style.DEFAULT);
                }
            } else {
                return;
            }
        }
        if (triggerPhotoDownload) {
            phoneImageDownloader.clearPhotoCheckEntry(number, com.navdy.service.library.events.photo.PhotoType.PHOTO_CONTACT);
            phoneImageDownloader.submitDownload(number, com.navdy.hud.app.framework.contacts.PhoneImageDownloader.Priority.HIGH, com.navdy.service.library.events.photo.PhotoType.PHOTO_CONTACT, contactName);
        }
    }

    private static void updateInitials(com.navdy.hud.app.ui.component.image.InitialsImageView.Style style, java.lang.String initials, com.navdy.hud.app.ui.component.image.InitialsImageView imageView) {
        imageView.setInitials(initials, style);
    }

    private static void setImage(java.io.File path, com.navdy.hud.app.ui.component.image.InitialsImageView imageView, com.squareup.picasso.Transformation transformation) {
        com.navdy.hud.app.util.picasso.PicassoUtil.getInstance().load(path).fit().transform(transformation).into(imageView, new com.navdy.hud.app.framework.contacts.ContactUtil.Anon2(imageView, path));
    }

    public static java.lang.String sanitizeString(java.lang.String str) {
        if (str == null) {
            return str;
        }
        boolean foundMarker = false;
        int len = str.length();
        char[] chrs = str.toCharArray();
        for (int i = 0; i < chrs.length; i++) {
            if (MARKER_CHARS.contains(java.lang.Character.valueOf(chrs[i]))) {
                foundMarker = true;
                chrs[i] = ' ';
            }
        }
        if (!foundMarker) {
            return str.trim();
        }
        return new java.lang.String(chrs, 0, len).trim();
    }

    public static com.navdy.hud.app.framework.contacts.NumberType getNumberType(com.navdy.service.library.events.contacts.PhoneNumberType numberType) {
        if (numberType == null) {
            return null;
        }
        switch (numberType) {
            case PHONE_NUMBER_HOME:
                return com.navdy.hud.app.framework.contacts.NumberType.HOME;
            case PHONE_NUMBER_MOBILE:
                return com.navdy.hud.app.framework.contacts.NumberType.MOBILE;
            case PHONE_NUMBER_WORK:
                return com.navdy.hud.app.framework.contacts.NumberType.WORK;
            default:
                return com.navdy.hud.app.framework.contacts.NumberType.OTHER;
        }
    }

    public static java.util.List<com.navdy.hud.app.framework.contacts.Contact> fromPhoneNumbers(java.util.List<com.navdy.service.library.events.contacts.PhoneNumber> phoneNumbers) {
        if (phoneNumbers == null) {
            return null;
        }
        java.util.List<com.navdy.hud.app.framework.contacts.Contact> result = new java.util.ArrayList<>(phoneNumbers.size());
        for (com.navdy.service.library.events.contacts.PhoneNumber phoneNumber : phoneNumbers) {
            result.add(new com.navdy.hud.app.framework.contacts.Contact(phoneNumber));
        }
        return result;
    }

    public static java.util.List<com.navdy.hud.app.framework.contacts.Contact> fromContacts(java.util.List<com.navdy.service.library.events.contacts.Contact> contacts) {
        if (contacts == null) {
            return null;
        }
        java.util.List<com.navdy.hud.app.framework.contacts.Contact> result = new java.util.ArrayList<>(contacts.size());
        for (com.navdy.service.library.events.contacts.Contact contact : contacts) {
            result.add(new com.navdy.hud.app.framework.contacts.Contact(contact));
        }
        return result;
    }

    public static com.navdy.service.library.events.contacts.PhoneNumberType convertNumberType(com.navdy.hud.app.framework.contacts.NumberType numberType) {
        if (numberType == null) {
            return null;
        }
        switch (numberType) {
            case WORK_MOBILE:
            case WORK:
                return com.navdy.service.library.events.contacts.PhoneNumberType.PHONE_NUMBER_WORK;
            case MOBILE:
                return com.navdy.service.library.events.contacts.PhoneNumberType.PHONE_NUMBER_MOBILE;
            case HOME:
                return com.navdy.service.library.events.contacts.PhoneNumberType.PHONE_NUMBER_HOME;
            default:
                return com.navdy.service.library.events.contacts.PhoneNumberType.PHONE_NUMBER_OTHER;
        }
    }

    public static java.util.List<com.navdy.service.library.events.contacts.PhoneNumber> toPhoneNumbers(java.util.List<com.navdy.hud.app.framework.contacts.Contact> contacts) {
        java.lang.String str;
        if (contacts == null) {
            return null;
        }
        java.util.ArrayList arrayList = new java.util.ArrayList(contacts.size());
        for (com.navdy.hud.app.framework.contacts.Contact c : contacts) {
            com.navdy.service.library.events.contacts.PhoneNumber.Builder numberType = new com.navdy.service.library.events.contacts.PhoneNumber.Builder().number(c.number).numberType(convertNumberType(c.numberType));
            if (c.numberType == com.navdy.hud.app.framework.contacts.NumberType.OTHER) {
                str = c.numberTypeStr;
            } else {
                str = null;
            }
            arrayList.add(numberType.customType(str).build());
        }
        return arrayList;
    }

    public static java.util.List<com.navdy.service.library.events.contacts.Contact> toContacts(java.util.List<com.navdy.hud.app.framework.contacts.Contact> contacts) {
        java.lang.String str;
        if (contacts == null) {
            return null;
        }
        java.util.ArrayList arrayList = new java.util.ArrayList(contacts.size());
        for (com.navdy.hud.app.framework.contacts.Contact c : contacts) {
            com.navdy.service.library.events.contacts.Contact.Builder numberType = new com.navdy.service.library.events.contacts.Contact.Builder().name(c.name).number(c.number).numberType(convertNumberType(c.numberType));
            if (c.numberType == com.navdy.hud.app.framework.contacts.NumberType.OTHER) {
                str = c.numberTypeStr;
            } else {
                str = null;
            }
            arrayList.add(numberType.label(str).build());
        }
        return arrayList;
    }
}
