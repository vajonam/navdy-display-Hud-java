package com.navdy.hud.app.framework.contacts;

public class PhoneImageDownloader {
    private static final java.lang.String MD5_EXTENSION = ".md5";
    private static final boolean VERBOSE = com.navdy.hud.app.BuildConfig.DEBUG;
    private static final com.navdy.hud.app.framework.contacts.PhoneImageDownloader sInstance = new com.navdy.hud.app.framework.contacts.PhoneImageDownloader();
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.framework.contacts.PhoneImageDownloader.class);
    /* access modifiers changed from: private */
    public com.squareup.otto.Bus bus = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getBus();
    private java.util.HashSet<java.lang.String> contactPhotoChecked = new java.util.HashSet<>();
    private java.util.concurrent.PriorityBlockingQueue<com.navdy.hud.app.framework.contacts.PhoneImageDownloader.Info> contactQueue = new java.util.concurrent.PriorityBlockingQueue<>(10, this.priorityComparator);
    /* access modifiers changed from: private */
    public android.content.Context context = com.navdy.hud.app.HudApplication.getAppContext();
    /* access modifiers changed from: private */
    public volatile java.lang.String currentDriverProfileId;
    private boolean downloading;
    private java.lang.Object lockObj = new java.lang.Object();
    private java.util.Comparator<com.navdy.hud.app.framework.contacts.PhoneImageDownloader.Info> priorityComparator = new com.navdy.hud.app.framework.contacts.PhoneImageDownloader.Anon1();

    class Anon1 implements java.util.Comparator<com.navdy.hud.app.framework.contacts.PhoneImageDownloader.Info> {
        Anon1() {
        }

        public int compare(com.navdy.hud.app.framework.contacts.PhoneImageDownloader.Info lhs, com.navdy.hud.app.framework.contacts.PhoneImageDownloader.Info rhs) {
            if (!(lhs instanceof com.navdy.hud.app.framework.contacts.PhoneImageDownloader.Info) || !(rhs instanceof com.navdy.hud.app.framework.contacts.PhoneImageDownloader.Info)) {
                return 0;
            }
            return lhs.priority - rhs.priority;
        }
    }

    class Anon2 implements java.lang.Runnable {
        Anon2() {
        }

        public void run() {
            com.navdy.hud.app.framework.contacts.PhoneImageDownloader.this.download();
        }
    }

    class Anon3 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.service.library.events.photo.PhotoResponse val$event;

        Anon3(com.navdy.service.library.events.photo.PhotoResponse photoResponse) {
            this.val$event = photoResponse;
        }

        public void run() {
            com.navdy.hud.app.framework.contacts.PhoneImageDownloader.sLogger.v("storing photo");
            java.io.FileOutputStream fos = null;
            java.io.FileOutputStream fos_hash = null;
            try {
                byte[] byteArray = this.val$event.photo.toByteArray();
                android.graphics.Bitmap image = android.graphics.BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
                java.lang.String filename = com.navdy.hud.app.framework.contacts.PhoneImageDownloader.this.normalizedFilename(this.val$event.identifier, this.val$event.photoType);
                if (image != null) {
                    com.navdy.hud.app.profile.DriverProfile driverProfile = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile();
                    if (android.text.TextUtils.equals(driverProfile.getProfileName(), com.navdy.hud.app.framework.contacts.PhoneImageDownloader.this.currentDriverProfileId)) {
                        java.io.File f = new java.io.File(com.navdy.hud.app.framework.contacts.PhoneImageDownloader.this.getPhotoPath(driverProfile, this.val$event.photoType), filename);
                        java.io.FileOutputStream fos2 = new java.io.FileOutputStream(f);
                        try {
                            fos2.write(byteArray);
                            java.lang.String hash = com.navdy.service.library.util.IOUtils.hashForBytes(byteArray);
                            java.io.FileOutputStream fos_hash2 = new java.io.FileOutputStream(new java.io.File(com.navdy.hud.app.framework.contacts.PhoneImageDownloader.this.getPhotoPath(driverProfile, this.val$event.photoType), filename + com.navdy.hud.app.framework.contacts.PhoneImageDownloader.MD5_EXTENSION));
                            try {
                                fos_hash2.write(hash.getBytes());
                                com.navdy.hud.app.framework.contacts.PhoneImageDownloader.sLogger.v("photo saved[" + this.val$event.photoType + "] id[" + this.val$event.identifier + "]");
                                com.navdy.hud.app.util.picasso.PicassoUtil.getInstance().invalidate(f);
                                com.navdy.hud.app.framework.contacts.PhoneImageDownloader.this.bus.post(new com.navdy.hud.app.framework.contacts.PhoneImageDownloader.PhotoDownloadStatus(this.val$event.identifier, this.val$event.photoType, true, false));
                                fos_hash = fos_hash2;
                                fos = fos2;
                            } catch (Throwable th) {
                                th = th;
                                fos_hash = fos_hash2;
                                fos = fos2;
                                com.navdy.service.library.util.IOUtils.fileSync(fos);
                                com.navdy.service.library.util.IOUtils.closeStream(fos);
                                com.navdy.service.library.util.IOUtils.fileSync(fos_hash);
                                com.navdy.service.library.util.IOUtils.closeStream(fos_hash);
                                com.navdy.hud.app.framework.contacts.PhoneImageDownloader.this.invokeDownload();
                                throw th;
                            }
                        } catch (Throwable th2) {
                            th = th2;
                            fos = fos2;
                            com.navdy.service.library.util.IOUtils.fileSync(fos);
                            com.navdy.service.library.util.IOUtils.closeStream(fos);
                            com.navdy.service.library.util.IOUtils.fileSync(fos_hash);
                            com.navdy.service.library.util.IOUtils.closeStream(fos_hash);
                            com.navdy.hud.app.framework.contacts.PhoneImageDownloader.this.invokeDownload();
                            throw th;
                        }
                    } else {
                        com.navdy.hud.app.framework.contacts.PhoneImageDownloader.sLogger.w("photo cannot save [" + this.val$event.photoType + "] id[" + this.val$event.identifier + "] profile changed from [" + com.navdy.hud.app.framework.contacts.PhoneImageDownloader.this.currentDriverProfileId + "] to [" + driverProfile.getProfileName() + "]");
                    }
                } else {
                    com.navdy.hud.app.framework.contacts.PhoneImageDownloader.this.bus.post(new com.navdy.hud.app.framework.contacts.PhoneImageDownloader.PhotoDownloadStatus(this.val$event.identifier, this.val$event.photoType, false, false));
                }
                com.navdy.service.library.util.IOUtils.fileSync(fos);
                com.navdy.service.library.util.IOUtils.closeStream(fos);
                com.navdy.service.library.util.IOUtils.fileSync(fos_hash);
                com.navdy.service.library.util.IOUtils.closeStream(fos_hash);
                com.navdy.hud.app.framework.contacts.PhoneImageDownloader.this.invokeDownload();
            } catch (Throwable th3) {
                t = th3;
            }
        }
    }

    class Anon4 implements java.lang.Runnable {
        final /* synthetic */ java.io.File val$file;
        final /* synthetic */ java.lang.String val$id;
        final /* synthetic */ com.navdy.service.library.events.photo.PhotoType val$photoType;

        Anon4(java.io.File file, java.lang.String str, com.navdy.service.library.events.photo.PhotoType photoType) {
            this.val$file = file;
            this.val$id = str;
            this.val$photoType = photoType;
        }

        public void run() {
            if (com.navdy.service.library.util.IOUtils.deleteFile(com.navdy.hud.app.framework.contacts.PhoneImageDownloader.this.context, this.val$file.getAbsolutePath())) {
                com.navdy.hud.app.framework.contacts.PhoneImageDownloader.sLogger.v("removed [" + this.val$id + "]");
            }
            com.navdy.hud.app.framework.contacts.PhoneImageDownloader.this.bus.post(new com.navdy.hud.app.framework.contacts.PhoneImageDownloader.PhotoDownloadStatus(this.val$id, this.val$photoType, false, false));
        }
    }

    private static class Info {
        java.lang.String displayName;
        java.lang.String fileName;
        com.navdy.service.library.events.photo.PhotoType photoType;
        int priority;
        java.lang.String sourceIdentifier;

        Info(java.lang.String fileName2, com.navdy.hud.app.framework.contacts.PhoneImageDownloader.Priority priority2, com.navdy.service.library.events.photo.PhotoType photoType2, java.lang.String sourceIdentifier2, java.lang.String displayName2) {
            this.fileName = fileName2;
            this.priority = priority2.getValue();
            this.photoType = photoType2;
            this.sourceIdentifier = sourceIdentifier2;
            this.displayName = displayName2;
        }
    }

    public static class PhotoDownloadStatus {
        public boolean alreadyDownloaded;
        public com.navdy.service.library.events.photo.PhotoType photoType;
        public java.lang.String sourceIdentifier;
        public boolean success;

        PhotoDownloadStatus(java.lang.String sourceIdentifier2, com.navdy.service.library.events.photo.PhotoType photoType2, boolean success2, boolean alreadyDownloaded2) {
            this.sourceIdentifier = sourceIdentifier2;
            this.photoType = photoType2;
            this.success = success2;
            this.alreadyDownloaded = alreadyDownloaded2;
        }
    }

    public enum Priority {
        NORMAL(1),
        HIGH(2);
        
        private final int val;

        private Priority(int val2) {
            this.val = val2;
        }

        public int getValue() {
            return this.val;
        }
    }

    public static final com.navdy.hud.app.framework.contacts.PhoneImageDownloader getInstance() {
        return sInstance;
    }

    private PhoneImageDownloader() {
        this.bus.register(this);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:28:?, code lost:
        return;
     */
    public void submitDownload(java.lang.String id, com.navdy.hud.app.framework.contacts.PhoneImageDownloader.Priority priority, com.navdy.service.library.events.photo.PhotoType photoType, java.lang.String displayName) {
        if (android.text.TextUtils.isEmpty(id)) {
            sLogger.w("invalid id passed");
            return;
        }
        java.lang.String filename = normalizedFilename(id, photoType);
        synchronized (this.lockObj) {
            if (photoType != com.navdy.service.library.events.photo.PhotoType.PHOTO_CONTACT || !this.contactPhotoChecked.contains(filename)) {
                sLogger.v("submitting photo request name[" + filename + "] type [" + photoType + "] displayName[" + displayName + "]");
                this.contactQueue.add(new com.navdy.hud.app.framework.contacts.PhoneImageDownloader.Info(filename, priority, photoType, id, displayName));
                if (!this.downloading) {
                    this.downloading = true;
                    invokeDownload();
                } else if (VERBOSE) {
                }
            } else {
                sLogger.v("contact photo request has already been processed name[" + filename + "]");
            }
        }
    }

    /* access modifiers changed from: private */
    public void invokeDownload() {
        this.currentDriverProfileId = null;
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.framework.contacts.PhoneImageDownloader.Anon2(), 7);
    }

    public java.lang.String hashFor(java.lang.String id, com.navdy.service.library.events.photo.PhotoType photoType) {
        java.lang.String hash = null;
        java.lang.String fileName = normalizedFilename(id, photoType);
        java.io.File imageDir = getPhotoPath(com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile(), photoType);
        java.io.File path = new java.io.File(imageDir, fileName);
        if (!path.exists() || path.isDirectory()) {
            return hash;
        }
        java.io.File md5 = new java.io.File(imageDir, fileName + MD5_EXTENSION);
        if (!md5.exists()) {
            sLogger.w("md5 does not exist, delete file, " + fileName);
            com.navdy.service.library.util.IOUtils.deleteFile(this.context, path.getAbsolutePath());
            return hash;
        }
        try {
            return com.navdy.service.library.util.IOUtils.convertFileToString(md5.getAbsolutePath());
        } catch (Throwable t) {
            sLogger.e("error reading md5[" + fileName + "]", t);
            com.navdy.service.library.util.IOUtils.deleteFile(this.context, path.getAbsolutePath());
            com.navdy.service.library.util.IOUtils.deleteFile(this.context, md5.getAbsolutePath());
            return hash;
        }
    }

    /* access modifiers changed from: private */
    public void download() {
        try {
            synchronized (this.lockObj) {
                if (this.contactQueue.size() == 0) {
                    sLogger.v("no request pending");
                    this.downloading = false;
                    return;
                }
                com.navdy.hud.app.framework.contacts.PhoneImageDownloader.Info info = (com.navdy.hud.app.framework.contacts.PhoneImageDownloader.Info) this.contactQueue.remove();
                if (info.priority == com.navdy.hud.app.framework.contacts.PhoneImageDownloader.Priority.NORMAL.getValue()) {
                    com.navdy.hud.app.util.GenericUtil.sleep(500);
                }
                this.currentDriverProfileId = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile().getProfileName();
                java.lang.String hash = hashFor(info.sourceIdentifier, info.photoType);
                if (hash == null) {
                    sLogger.v("sending fresh download request file [" + info.fileName + "] id[" + info.sourceIdentifier + "]");
                } else {
                    sLogger.v("checking for update file [" + info.fileName + "] id[" + info.sourceIdentifier + "]");
                }
                this.bus.post(new com.navdy.hud.app.event.RemoteEvent(new com.navdy.service.library.events.photo.PhotoRequest(info.sourceIdentifier, hash, info.photoType, info.displayName)));
            }
        } catch (Throwable t) {
            sLogger.e(t);
            invokeDownload();
        }
    }

    private void purgeQueue() {
        sLogger.i("purging queue");
        this.currentDriverProfileId = null;
        synchronized (this.lockObj) {
            this.contactQueue.clear();
            this.downloading = false;
        }
    }

    @com.squareup.otto.Subscribe
    public void onConnectionStatusChange(com.navdy.service.library.events.connection.ConnectionStateChange event) {
        switch (event.state) {
            case CONNECTION_DISCONNECTED:
                purgeQueue();
                return;
            default:
                return;
        }
    }

    @com.squareup.otto.Subscribe
    public void onDriverProfileChanged(com.navdy.hud.app.event.DriverProfileChanged event) {
        purgeQueue();
    }

    @com.squareup.otto.Subscribe
    public void onPhotoResponse(com.navdy.service.library.events.photo.PhotoResponse event) {
        sLogger.v("got photoResponse:" + event.status + " type[" + event.photoType + "] id[" + event.identifier + "]");
        if (this.currentDriverProfileId == null) {
            sLogger.w("currentDriverProfile is null");
            invokeDownload();
            return;
        }
        java.lang.String filename = normalizedFilename(event.identifier, event.photoType);
        if (event.status == com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS) {
            this.contactPhotoChecked.add(filename);
            storePhoto(event);
            return;
        }
        if (event.status == com.navdy.service.library.events.RequestStatus.REQUEST_NOT_AVAILABLE) {
            this.contactPhotoChecked.add(filename);
            removeImage(event.identifier, event.photoType);
            sLogger.v("photo not available type[" + event.photoType + "] id[" + event.identifier + "]");
        } else {
            sLogger.v("photo not available type[" + event.photoType + "] id[" + event.identifier + "] error[" + event.status + "]");
        }
        invokeDownload();
    }

    private void storePhoto(com.navdy.service.library.events.photo.PhotoResponse event) {
        if (event.photo == null) {
            sLogger.v("photo is upto-date type[" + event.photoType + "] id[" + event.identifier + "]");
            this.bus.post(new com.navdy.hud.app.framework.contacts.PhoneImageDownloader.PhotoDownloadStatus(event.identifier, event.photoType, true, true));
            invokeDownload();
            return;
        }
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.framework.contacts.PhoneImageDownloader.Anon3(event), 7);
    }

    /* access modifiers changed from: private */
    public java.io.File getPhotoPath(com.navdy.hud.app.profile.DriverProfile profile, com.navdy.service.library.events.photo.PhotoType photoType) {
        switch (photoType) {
            case PHOTO_ALBUM_ART:
                return profile.getMusicImageDir();
            case PHOTO_CONTACT:
                return profile.getContactsImageDir();
            case PHOTO_DRIVER_PROFILE:
                return profile.getPreferencesDirectory();
            default:
                return null;
        }
    }

    /* access modifiers changed from: private */
    public java.lang.String normalizedFilename(java.lang.String id, com.navdy.service.library.events.photo.PhotoType type) {
        if (type == com.navdy.service.library.events.photo.PhotoType.PHOTO_DRIVER_PROFILE) {
            return com.navdy.hud.app.profile.DriverProfile.DRIVER_PROFILE_IMAGE;
        }
        return com.navdy.hud.app.util.GenericUtil.normalizeToFilename(id);
    }

    public java.io.File getImagePath(java.lang.String id, com.navdy.service.library.events.photo.PhotoType type) {
        return getImagePath(id, "", type);
    }

    public java.io.File getImagePath(java.lang.String id, java.lang.String suffix, com.navdy.service.library.events.photo.PhotoType type) {
        java.lang.String filename = normalizedFilename(id, type);
        java.io.File f = getPhotoPath(com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile(), type);
        if (f == null) {
            return null;
        }
        return new java.io.File(f, filename + suffix);
    }

    public void clearPhotoCheckEntry(java.lang.String id, com.navdy.service.library.events.photo.PhotoType type) {
        java.lang.String filename = normalizedFilename(id, type);
        synchronized (this.lockObj) {
            if (this.contactPhotoChecked.remove(filename)) {
                sLogger.v("photo check entry removed [" + filename + "]");
            }
        }
    }

    public void clearAllPhotoCheckEntries() {
        synchronized (this.lockObj) {
            this.contactPhotoChecked.clear();
            sLogger.v("cleared all photo check entries");
        }
    }

    private void removeImage(java.lang.String id, com.navdy.service.library.events.photo.PhotoType photoType) {
        if (id == null || photoType == null) {
            sLogger.i("null param");
            return;
        }
        java.io.File file = getImagePath(id, photoType);
        com.navdy.hud.app.util.picasso.PicassoUtil.getInstance().invalidate(file);
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.framework.contacts.PhoneImageDownloader.Anon4(file, id, photoType), 7);
    }
}
