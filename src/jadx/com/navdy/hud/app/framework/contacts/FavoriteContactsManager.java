package com.navdy.hud.app.framework.contacts;

public class FavoriteContactsManager {
    private static final com.navdy.hud.app.framework.contacts.FavoriteContactsManager.FavoriteContactsChanged FAVORITE_CONTACTS_CHANGED = new com.navdy.hud.app.framework.contacts.FavoriteContactsManager.FavoriteContactsChanged(null);
    private static final int MAX_FAV_CONTACTS = 30;
    private static final com.navdy.hud.app.framework.contacts.FavoriteContactsManager sInstance = new com.navdy.hud.app.framework.contacts.FavoriteContactsManager();
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.framework.contacts.FavoriteContactsManager.class);
    private com.squareup.otto.Bus bus = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getBus();
    private volatile java.util.List<com.navdy.hud.app.framework.contacts.Contact> favContacts;
    /* access modifiers changed from: private */
    public boolean fullSync = true;
    /* access modifiers changed from: private */
    public boolean isAllowedToReceiveContacts = false;

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            com.navdy.hud.app.framework.contacts.FavoriteContactsManager.this.load();
        }
    }

    class Anon2 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.service.library.events.contacts.FavoriteContactsResponse val$response;

        Anon2(com.navdy.service.library.events.contacts.FavoriteContactsResponse favoriteContactsResponse) {
            this.val$response = favoriteContactsResponse;
        }

        public void run() {
            try {
                if (this.val$response.status == com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS) {
                    com.navdy.hud.app.framework.contacts.FavoriteContactsManager.this.isAllowedToReceiveContacts = true;
                    if (this.val$response.contacts != null) {
                        com.navdy.hud.app.framework.contacts.FavoriteContactsManager.sLogger.v("onFavoriteContactResponse size[" + this.val$response.contacts.size() + "]");
                        java.util.HashSet<java.lang.String> seenNumbers = new java.util.HashSet<>();
                        java.util.List<com.navdy.hud.app.framework.contacts.Contact> list = new java.util.ArrayList<>();
                        java.util.Iterator it = this.val$response.contacts.iterator();
                        while (true) {
                            if (!it.hasNext()) {
                                break;
                            }
                            com.navdy.service.library.events.contacts.Contact c = (com.navdy.service.library.events.contacts.Contact) it.next();
                            if (android.text.TextUtils.isEmpty(c.number)) {
                                com.navdy.hud.app.framework.contacts.FavoriteContactsManager.sLogger.v("null number");
                            } else if (seenNumbers.contains(c.number)) {
                                com.navdy.hud.app.framework.contacts.FavoriteContactsManager.sLogger.v("number already seen:" + c.number);
                            } else {
                                com.navdy.hud.app.framework.contacts.NumberType numberType = com.navdy.hud.app.framework.contacts.ContactUtil.getNumberType(c.numberType);
                                java.lang.String displayName = c.name;
                                if (!android.text.TextUtils.isEmpty(c.name) && !com.navdy.hud.app.framework.contacts.ContactUtil.isDisplayNameValid(c.name, c.number, com.navdy.hud.app.framework.contacts.ContactUtil.getPhoneNumber(c.number))) {
                                    displayName = null;
                                }
                                list.add(new com.navdy.hud.app.framework.contacts.Contact(displayName, c.number, numberType, com.navdy.hud.app.framework.contacts.ContactImageHelper.getInstance().getContactImageIndex(c.number), 0));
                                seenNumbers.add(c.number);
                                if (list.size() == 30) {
                                    com.navdy.hud.app.framework.contacts.FavoriteContactsManager.sLogger.v("exceeded max size");
                                    break;
                                }
                            }
                        }
                        com.navdy.hud.app.framework.contacts.FavoriteContactsManager.sLogger.v("favorite contact size[" + list.size() + "]");
                        com.navdy.hud.app.storage.db.helper.FavoriteContactsPersistenceHelper.storeFavoriteContacts(com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile().getProfileName(), list, com.navdy.hud.app.framework.contacts.FavoriteContactsManager.this.fullSync);
                        return;
                    }
                    com.navdy.hud.app.framework.contacts.FavoriteContactsManager.sLogger.w("fav-contact list returned is null");
                } else if (this.val$response.status == com.navdy.service.library.events.RequestStatus.REQUEST_NOT_AVAILABLE) {
                    com.navdy.hud.app.framework.contacts.FavoriteContactsManager.this.isAllowedToReceiveContacts = false;
                } else {
                    com.navdy.hud.app.framework.contacts.FavoriteContactsManager.sLogger.e("sent fav-contact response error:" + this.val$response.status);
                }
            } catch (Throwable t) {
                com.navdy.hud.app.framework.contacts.FavoriteContactsManager.sLogger.e(t);
            }
        }
    }

    private static class FavoriteContactsChanged {
        private FavoriteContactsChanged() {
        }

        /* synthetic */ FavoriteContactsChanged(com.navdy.hud.app.framework.contacts.FavoriteContactsManager.Anon1 x0) {
            this();
        }
    }

    public boolean isAllowedToReceiveContacts() {
        return this.isAllowedToReceiveContacts;
    }

    public static com.navdy.hud.app.framework.contacts.FavoriteContactsManager getInstance() {
        return sInstance;
    }

    private FavoriteContactsManager() {
        this.bus.register(this);
    }

    public java.util.List<com.navdy.hud.app.framework.contacts.Contact> getFavoriteContacts() {
        return this.favContacts;
    }

    public void setFavoriteContacts(java.util.List<com.navdy.hud.app.framework.contacts.Contact> favContacts2) {
        this.favContacts = favContacts2;
        this.bus.post(FAVORITE_CONTACTS_CHANGED);
    }

    public void clearFavoriteContacts() {
        setFavoriteContacts(null);
    }

    public void buildFavoriteContacts() {
        if (com.navdy.hud.app.util.GenericUtil.isMainThread()) {
            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.framework.contacts.FavoriteContactsManager.Anon1(), 1);
        } else {
            load();
        }
    }

    public void syncFavoriteContacts(boolean fullSync2) {
        try {
            this.fullSync = fullSync2;
            this.bus.post(new com.navdy.hud.app.event.RemoteEvent(new com.navdy.service.library.events.contacts.FavoriteContactsRequest(java.lang.Integer.valueOf(30))));
            sLogger.v("sent fav-contact request");
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }

    @com.squareup.otto.Subscribe
    public void onFavoriteContactResponse(com.navdy.service.library.events.contacts.FavoriteContactsResponse response) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.framework.contacts.FavoriteContactsManager.Anon2(response), 1);
    }

    /* access modifiers changed from: private */
    public void load() {
        try {
            java.lang.String id = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile().getProfileName();
            this.favContacts = com.navdy.hud.app.storage.db.helper.FavoriteContactsPersistenceHelper.getFavoriteContacts(id);
            sLogger.v("load favcontacts id[" + id + "] number of contacts[" + this.favContacts.size() + "]");
        } catch (Throwable t) {
            this.favContacts = null;
            sLogger.e(t);
        } finally {
            this.bus.post(FAVORITE_CONTACTS_CHANGED);
        }
    }
}
