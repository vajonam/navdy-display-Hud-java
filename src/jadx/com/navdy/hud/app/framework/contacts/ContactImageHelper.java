package com.navdy.hud.app.framework.contacts;

public class ContactImageHelper {
    public static final int DEFAULT_IMAGE_INDEX = -1;
    public static int NO_CONTACT_COLOR = 0;
    public static final int NO_CONTACT_IMAGE = 2130838046;
    private static final java.util.Map<java.lang.Integer, java.lang.Integer> sContactColorMap = new java.util.HashMap();
    private static final java.util.Map<java.lang.Integer, java.lang.Integer> sContactImageMap = new java.util.HashMap();
    private static final com.navdy.hud.app.framework.contacts.ContactImageHelper sInstance = new com.navdy.hud.app.framework.contacts.ContactImageHelper();

    static {
        android.content.res.Resources resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        NO_CONTACT_COLOR = resources.getColor(com.navdy.hud.app.R.color.grey_4a);
        sContactColorMap.put(java.lang.Integer.valueOf(0), java.lang.Integer.valueOf(resources.getColor(com.navdy.hud.app.R.color.icon_user_bg_0)));
        int counter = 0 + 1;
        sContactImageMap.put(java.lang.Integer.valueOf(0), java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_user_bg_0));
        sContactColorMap.put(java.lang.Integer.valueOf(counter), java.lang.Integer.valueOf(resources.getColor(com.navdy.hud.app.R.color.icon_user_bg_1)));
        int counter2 = counter + 1;
        sContactImageMap.put(java.lang.Integer.valueOf(counter), java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_user_bg_1));
        sContactColorMap.put(java.lang.Integer.valueOf(counter2), java.lang.Integer.valueOf(resources.getColor(com.navdy.hud.app.R.color.icon_user_bg_2)));
        int counter3 = counter2 + 1;
        sContactImageMap.put(java.lang.Integer.valueOf(counter2), java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_user_bg_2));
        sContactColorMap.put(java.lang.Integer.valueOf(counter3), java.lang.Integer.valueOf(resources.getColor(com.navdy.hud.app.R.color.icon_user_bg_3)));
        int counter4 = counter3 + 1;
        sContactImageMap.put(java.lang.Integer.valueOf(counter3), java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_user_bg_3));
        sContactColorMap.put(java.lang.Integer.valueOf(counter4), java.lang.Integer.valueOf(resources.getColor(com.navdy.hud.app.R.color.icon_user_bg_4)));
        int counter5 = counter4 + 1;
        sContactImageMap.put(java.lang.Integer.valueOf(counter4), java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_user_bg_4));
        sContactColorMap.put(java.lang.Integer.valueOf(counter5), java.lang.Integer.valueOf(resources.getColor(com.navdy.hud.app.R.color.icon_user_bg_5)));
        int counter6 = counter5 + 1;
        sContactImageMap.put(java.lang.Integer.valueOf(counter5), java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_user_bg_5));
        sContactColorMap.put(java.lang.Integer.valueOf(counter6), java.lang.Integer.valueOf(resources.getColor(com.navdy.hud.app.R.color.icon_user_bg_6)));
        int counter7 = counter6 + 1;
        sContactImageMap.put(java.lang.Integer.valueOf(counter6), java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_user_bg_6));
        sContactColorMap.put(java.lang.Integer.valueOf(counter7), java.lang.Integer.valueOf(resources.getColor(com.navdy.hud.app.R.color.icon_user_bg_7)));
        int counter8 = counter7 + 1;
        sContactImageMap.put(java.lang.Integer.valueOf(counter7), java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_user_bg_7));
        sContactColorMap.put(java.lang.Integer.valueOf(counter8), java.lang.Integer.valueOf(resources.getColor(com.navdy.hud.app.R.color.icon_user_bg_8)));
        int i = counter8 + 1;
        sContactImageMap.put(java.lang.Integer.valueOf(counter8), java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_user_bg_8));
    }

    public static com.navdy.hud.app.framework.contacts.ContactImageHelper getInstance() {
        return sInstance;
    }

    public int getContactImageIndex(java.lang.String contactId) {
        if (contactId == null) {
            return -1;
        }
        java.lang.String contactId2 = com.navdy.hud.app.util.GenericUtil.normalizeToFilename(contactId);
        if (com.navdy.hud.app.framework.contacts.ContactUtil.isValidNumber(contactId2)) {
            contactId2 = com.navdy.hud.app.util.PhoneUtil.convertToE164Format(contactId2);
        }
        return java.lang.Math.abs(contactId2.hashCode() % sContactImageMap.size());
    }

    public int getResourceId(int index) {
        if (index < 0 || index >= sContactImageMap.size()) {
            return com.navdy.hud.app.R.drawable.icon_user_numberonly;
        }
        return ((java.lang.Integer) sContactImageMap.get(java.lang.Integer.valueOf(index))).intValue();
    }

    public int getResourceColor(int index) {
        if (index < 0 || index >= sContactColorMap.size()) {
            return NO_CONTACT_COLOR;
        }
        return ((java.lang.Integer) sContactColorMap.get(java.lang.Integer.valueOf(index))).intValue();
    }

    public int getDriverImageResId(java.lang.String deviceName) {
        return getResourceId(java.lang.Math.abs(com.navdy.hud.app.util.GenericUtil.normalizeToFilename(deviceName).hashCode() % sContactImageMap.size()));
    }
}
