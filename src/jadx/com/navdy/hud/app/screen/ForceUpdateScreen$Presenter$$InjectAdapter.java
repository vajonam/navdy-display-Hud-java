package com.navdy.hud.app.screen;

public final class ForceUpdateScreen$Presenter$$InjectAdapter extends dagger.internal.Binding<com.navdy.hud.app.screen.ForceUpdateScreen.Presenter> implements javax.inject.Provider<com.navdy.hud.app.screen.ForceUpdateScreen.Presenter>, dagger.MembersInjector<com.navdy.hud.app.screen.ForceUpdateScreen.Presenter> {
    private dagger.internal.Binding<com.navdy.hud.app.gesture.GestureServiceConnector> gestureServiceConnector;
    private dagger.internal.Binding<com.squareup.otto.Bus> mBus;
    private dagger.internal.Binding<android.content.SharedPreferences> mPreferences;
    private dagger.internal.Binding<com.navdy.hud.app.screen.ForceUpdateScreen> mScreen;
    private dagger.internal.Binding<com.navdy.hud.app.ui.framework.BasePresenter> supertype;
    private dagger.internal.Binding<com.navdy.hud.app.ui.framework.UIStateManager> uiStateManager;

    public ForceUpdateScreen$Presenter$$InjectAdapter() {
        super("com.navdy.hud.app.screen.ForceUpdateScreen$Presenter", "members/com.navdy.hud.app.screen.ForceUpdateScreen$Presenter", false, com.navdy.hud.app.screen.ForceUpdateScreen.Presenter.class);
    }

    public void attach(dagger.internal.Linker linker) {
        this.mBus = linker.requestBinding("com.squareup.otto.Bus", com.navdy.hud.app.screen.ForceUpdateScreen.Presenter.class, getClass().getClassLoader());
        this.mPreferences = linker.requestBinding("android.content.SharedPreferences", com.navdy.hud.app.screen.ForceUpdateScreen.Presenter.class, getClass().getClassLoader());
        this.mScreen = linker.requestBinding("com.navdy.hud.app.screen.ForceUpdateScreen", com.navdy.hud.app.screen.ForceUpdateScreen.Presenter.class, getClass().getClassLoader());
        this.gestureServiceConnector = linker.requestBinding("com.navdy.hud.app.gesture.GestureServiceConnector", com.navdy.hud.app.screen.ForceUpdateScreen.Presenter.class, getClass().getClassLoader());
        this.uiStateManager = linker.requestBinding("com.navdy.hud.app.ui.framework.UIStateManager", com.navdy.hud.app.screen.ForceUpdateScreen.Presenter.class, getClass().getClassLoader());
        dagger.internal.Linker linker2 = linker;
        this.supertype = linker2.requestBinding("members/com.navdy.hud.app.ui.framework.BasePresenter", com.navdy.hud.app.screen.ForceUpdateScreen.Presenter.class, getClass().getClassLoader(), false, true);
    }

    public void getDependencies(java.util.Set<dagger.internal.Binding<?>> set, java.util.Set<dagger.internal.Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.mBus);
        injectMembersBindings.add(this.mPreferences);
        injectMembersBindings.add(this.mScreen);
        injectMembersBindings.add(this.gestureServiceConnector);
        injectMembersBindings.add(this.uiStateManager);
        injectMembersBindings.add(this.supertype);
    }

    public com.navdy.hud.app.screen.ForceUpdateScreen.Presenter get() {
        com.navdy.hud.app.screen.ForceUpdateScreen.Presenter result = new com.navdy.hud.app.screen.ForceUpdateScreen.Presenter();
        injectMembers(result);
        return result;
    }

    public void injectMembers(com.navdy.hud.app.screen.ForceUpdateScreen.Presenter object) {
        object.mBus = (com.squareup.otto.Bus) this.mBus.get();
        object.mPreferences = (android.content.SharedPreferences) this.mPreferences.get();
        object.mScreen = (com.navdy.hud.app.screen.ForceUpdateScreen) this.mScreen.get();
        object.gestureServiceConnector = (com.navdy.hud.app.gesture.GestureServiceConnector) this.gestureServiceConnector.get();
        object.uiStateManager = (com.navdy.hud.app.ui.framework.UIStateManager) this.uiStateManager.get();
        this.supertype.injectMembers(object);
    }
}
