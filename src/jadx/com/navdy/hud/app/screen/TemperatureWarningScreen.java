package com.navdy.hud.app.screen;

@flow.Layout(2130903169)
public class TemperatureWarningScreen extends com.navdy.hud.app.screen.BaseScreen {

    @dagger.Module(addsTo = com.navdy.hud.app.ui.activity.Main.Module.class, injects = {com.navdy.hud.app.view.TemperatureWarningView.class})
    public class Module {
        public Module() {
        }
    }

    @javax.inject.Singleton
    public static class Presenter extends com.navdy.hud.app.ui.framework.BasePresenter<com.navdy.hud.app.view.TemperatureWarningView> {
        @javax.inject.Inject
        com.squareup.otto.Bus mBus;

        public void finish() {
            this.mBus.post(new com.navdy.service.library.events.ui.ShowScreen.Builder().screen(com.navdy.service.library.events.ui.Screen.SCREEN_BACK).build());
        }
    }

    public java.lang.String getMortarScopeName() {
        return com.navdy.hud.app.screen.TemperatureWarningScreen.class.getName();
    }

    public java.lang.Object getDaggerModule() {
        return new com.navdy.hud.app.screen.TemperatureWarningScreen.Module();
    }

    public com.navdy.service.library.events.ui.Screen getScreen() {
        return com.navdy.service.library.events.ui.Screen.SCREEN_TEMPERATURE_WARNING;
    }
}
