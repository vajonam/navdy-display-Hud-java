package com.navdy.hud.app.screen;

@flow.Layout(2130903066)
public class ForceUpdateScreen extends com.navdy.hud.app.screen.BaseScreen {
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.screen.ForceUpdateScreen.class);

    @dagger.Module(addsTo = com.navdy.hud.app.ui.activity.Main.Module.class, injects = {com.navdy.hud.app.view.ForceUpdateView.class})
    public class Module {
        public Module() {
        }

        /* access modifiers changed from: 0000 */
        @dagger.Provides
        public com.navdy.hud.app.screen.ForceUpdateScreen provideScreen() {
            return com.navdy.hud.app.screen.ForceUpdateScreen.this;
        }
    }

    public static class Presenter extends com.navdy.hud.app.ui.framework.BasePresenter<com.navdy.hud.app.view.ForceUpdateView> {
        @javax.inject.Inject
        com.navdy.hud.app.gesture.GestureServiceConnector gestureServiceConnector;
        @javax.inject.Inject
        com.squareup.otto.Bus mBus;
        @javax.inject.Inject
        android.content.SharedPreferences mPreferences;
        @javax.inject.Inject
        com.navdy.hud.app.screen.ForceUpdateScreen mScreen;
        @javax.inject.Inject
        com.navdy.hud.app.ui.framework.UIStateManager uiStateManager;

        public void onLoad(android.os.Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);
            this.uiStateManager.enableSystemTray(false);
            this.uiStateManager.enableNotificationColor(false);
            com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().enableNotifications(false);
        }

        /* access modifiers changed from: protected */
        public void onUnload() {
            this.uiStateManager.enableSystemTray(true);
            this.uiStateManager.enableNotificationColor(true);
            com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().enableNotifications(true);
            super.onUnload();
        }

        public void dismiss() {
            this.mBus.post(new com.navdy.service.library.events.ui.ShowScreen.Builder().screen(this.uiStateManager.getDefaultMainActiveScreen()).build());
        }

        public void install() {
            android.content.Intent intent = new android.content.Intent(com.navdy.hud.app.HudApplication.getAppContext(), com.navdy.hud.app.util.OTAUpdateService.class);
            intent.putExtra("COMMAND", com.navdy.hud.app.util.OTAUpdateService.COMMAND_INSTALL_UPDATE);
            com.navdy.hud.app.HudApplication.getAppContext().startService(intent);
        }

        public void shutDown() {
            this.mBus.post(new com.navdy.hud.app.event.Shutdown(com.navdy.hud.app.event.Shutdown.Reason.FORCED_UPDATE));
        }

        public boolean isSoftwareUpdatePending() {
            return com.navdy.hud.app.util.OTAUpdateService.isUpdateAvailable();
        }

        public com.squareup.otto.Bus getBus() {
            return this.mBus;
        }
    }

    public java.lang.String getMortarScopeName() {
        return com.navdy.hud.app.screen.ForceUpdateScreen.class.getName();
    }

    public java.lang.Object getDaggerModule() {
        return new com.navdy.hud.app.screen.ForceUpdateScreen.Module();
    }

    public com.navdy.service.library.events.ui.Screen getScreen() {
        return com.navdy.service.library.events.ui.Screen.SCREEN_FORCE_UPDATE;
    }
}
