package com.navdy.hud.app.screen;

@flow.Layout(2130903113)
public class DialUpdateConfirmationScreen extends com.navdy.hud.app.screen.BaseScreen {
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.screen.DialUpdateConfirmationScreen.class);

    @dagger.Module(addsTo = com.navdy.hud.app.ui.activity.Main.Module.class, injects = {com.navdy.hud.app.view.DialUpdateConfirmationView.class})
    public class Module {
        public Module() {
        }
    }

    @javax.inject.Singleton
    public static class Presenter extends com.navdy.hud.app.ui.framework.BasePresenter<com.navdy.hud.app.view.DialUpdateConfirmationView> {
        private boolean isReminder;
        @javax.inject.Inject
        com.squareup.otto.Bus mBus;

        public void onLoad(android.os.Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);
            if (savedInstanceState != null) {
                this.isReminder = savedInstanceState.getBoolean(com.navdy.hud.app.manager.UpdateReminderManager.EXTRA_UPDATE_REMINDER, false);
                return;
            }
            com.navdy.hud.app.screen.DialUpdateConfirmationScreen.sLogger.e("null bundle passed to onLoad()");
            this.isReminder = false;
        }

        public void install() {
            android.os.Bundle args = new android.os.Bundle();
            args.putInt(com.navdy.hud.app.screen.DialUpdateProgressScreen.EXTRA_PROGRESS_CAUSE, 1);
            this.mBus.post(new com.navdy.hud.app.event.ShowScreenWithArgs(com.navdy.service.library.events.ui.Screen.SCREEN_DIAL_UPDATE_PROGRESS, args, false));
        }

        public void finish() {
            this.mBus.post(new com.navdy.service.library.events.ui.ShowScreen.Builder().screen(com.navdy.service.library.events.ui.Screen.SCREEN_BACK).build());
        }

        public boolean isReminder() {
            return this.isReminder;
        }
    }

    public java.lang.String getMortarScopeName() {
        return com.navdy.hud.app.screen.DialUpdateConfirmationScreen.class.getName();
    }

    public java.lang.Object getDaggerModule() {
        return new com.navdy.hud.app.screen.DialUpdateConfirmationScreen.Module();
    }

    public com.navdy.service.library.events.ui.Screen getScreen() {
        return com.navdy.service.library.events.ui.Screen.SCREEN_DIAL_UPDATE_CONFIRMATION;
    }
}
