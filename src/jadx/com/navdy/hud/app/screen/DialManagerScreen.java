package com.navdy.hud.app.screen;

@flow.Layout(2130903112)
public class DialManagerScreen extends com.navdy.hud.app.screen.BaseScreen {
    /* access modifiers changed from: private */
    public static com.navdy.hud.app.screen.DialManagerScreen.Presenter presenter;
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.screen.DialManagerScreen.class);

    @dagger.Module(addsTo = com.navdy.hud.app.ui.activity.Main.Module.class, injects = {com.navdy.hud.app.view.DialManagerView.class})
    public class Module {
        public Module() {
        }
    }

    @javax.inject.Singleton
    public static class Presenter extends com.navdy.hud.app.ui.framework.BasePresenter<com.navdy.hud.app.view.DialManagerView> {
        @javax.inject.Inject
        com.squareup.otto.Bus bus;
        private com.navdy.hud.app.device.dial.DialManager dialManager;
        @javax.inject.Inject
        com.navdy.hud.app.gesture.GestureServiceConnector gestureServiceConnector;
        private android.os.Handler handler = new android.os.Handler(android.os.Looper.getMainLooper());
        private java.util.concurrent.atomic.AtomicBoolean isLedTurnedBlue = new java.util.concurrent.atomic.AtomicBoolean(false);
        private boolean scanningMode;
        @javax.inject.Inject
        com.navdy.hud.app.ui.framework.UIStateManager uiStateManager;

        public com.squareup.otto.Bus getBus() {
            return this.bus;
        }

        public void onLoad(android.os.Bundle savedInstanceState) {
            com.navdy.hud.app.screen.DialManagerScreen.sLogger.v("onLoad");
            com.navdy.hud.app.screen.DialManagerScreen.presenter = this;
            this.dialManager = com.navdy.hud.app.device.dial.DialManager.getInstance();
            this.uiStateManager.enableSystemTray(false);
            this.uiStateManager.enableNotificationColor(false);
            com.navdy.hud.app.screen.DialManagerScreen.sLogger.v("systemtray:invisible");
            java.lang.String dialName = null;
            if (savedInstanceState != null) {
                dialName = savedInstanceState.getString(com.navdy.hud.app.device.dial.DialConstants.OTA_DIAL_NAME_KEY, null);
            }
            updateView(dialName);
            super.onLoad(savedInstanceState);
        }

        /* access modifiers changed from: protected */
        public void onUnload() {
            com.navdy.hud.app.screen.DialManagerScreen.sLogger.v("onUnload");
            com.navdy.hud.app.screen.DialManagerScreen.presenter = null;
            showPairingLed(false);
            this.uiStateManager.enableSystemTray(true);
            this.uiStateManager.enableNotificationColor(true);
            com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().enableNotifications(true);
            com.navdy.hud.app.screen.DialManagerScreen.sLogger.v("systemtray:visible");
            super.onUnload();
        }

        public void updateView(java.lang.String dialName) {
            com.navdy.hud.app.view.DialManagerView view = (com.navdy.hud.app.view.DialManagerView) getView();
            if (view == null) {
                return;
            }
            if (dialName != null) {
                com.navdy.hud.app.screen.DialManagerScreen.sLogger.v("show searching for dial view");
                this.scanningMode = true;
                com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().enableNotifications(false);
                view.showDialSearching(dialName);
                showPairingLed(true);
            } else if (this.dialManager.isDialConnected()) {
                com.navdy.hud.app.screen.DialManagerScreen.sLogger.v("show dial connected view");
                view.showDialConnected();
            } else {
                com.navdy.hud.app.screen.DialManagerScreen.sLogger.v("show dial pairing view");
                this.scanningMode = true;
                com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().enableNotifications(false);
                view.showDialPairing();
                showPairingLed(true);
            }
        }

        public void dismiss() {
            exitScreen();
        }

        public void showPairingLed(boolean show) {
            if (show) {
                if (this.isLedTurnedBlue.compareAndSet(false, true)) {
                    com.navdy.hud.app.device.light.HUDLightUtils.showPairing(com.navdy.hud.app.HudApplication.getAppContext(), com.navdy.hud.app.device.light.LightManager.getInstance(), true);
                }
            } else if (this.isLedTurnedBlue.compareAndSet(true, false)) {
                com.navdy.hud.app.device.light.HUDLightUtils.showPairing(com.navdy.hud.app.HudApplication.getAppContext(), com.navdy.hud.app.device.light.LightManager.getInstance(), false);
            }
        }

        public android.os.Handler getHandler() {
            return this.handler;
        }

        private void exitScreen() {
            if (!com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().isRemoteDeviceConnected()) {
                com.navdy.hud.app.screen.DialManagerScreen.sLogger.v("go to welcome screen");
                android.os.Bundle args = new android.os.Bundle();
                args.putString(com.navdy.hud.app.screen.WelcomeScreen.ARG_ACTION, com.navdy.hud.app.screen.WelcomeScreen.ACTION_RECONNECT);
                this.bus.post(new com.navdy.hud.app.event.ShowScreenWithArgs(com.navdy.service.library.events.ui.Screen.SCREEN_WELCOME, args, false));
                return;
            }
            com.navdy.hud.app.screen.DialManagerScreen.sLogger.v("go to default screen");
            this.bus.post(new com.navdy.service.library.events.ui.ShowScreen.Builder().screen(com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getUiStateManager().getDefaultMainActiveScreen()).build());
        }

        public boolean isScanningMode() {
            return this.scanningMode;
        }
    }

    public com.navdy.service.library.events.ui.Screen getScreen() {
        return com.navdy.service.library.events.ui.Screen.SCREEN_DIAL_PAIRING;
    }

    public java.lang.String getMortarScopeName() {
        return getClass().getName();
    }

    public java.lang.Object getDaggerModule() {
        return new com.navdy.hud.app.screen.DialManagerScreen.Module();
    }

    public int getAnimationIn(flow.Flow.Direction direction) {
        return -1;
    }

    public int getAnimationOut(flow.Flow.Direction direction) {
        return -1;
    }

    public boolean isScanningMode() {
        if (presenter != null) {
            return presenter.isScanningMode();
        }
        return false;
    }
}
