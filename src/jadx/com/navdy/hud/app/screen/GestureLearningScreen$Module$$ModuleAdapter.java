package com.navdy.hud.app.screen;

public final class GestureLearningScreen$Module$$ModuleAdapter extends dagger.internal.ModuleAdapter<com.navdy.hud.app.screen.GestureLearningScreen.Module> {
    private static final java.lang.Class<?>[] INCLUDES = new java.lang.Class[0];
    private static final java.lang.String[] INJECTS = {"members/com.navdy.hud.app.view.LearnGestureScreenLayout", "members/com.navdy.hud.app.view.GestureLearningView", "members/com.navdy.hud.app.view.ScrollableTextPresenterLayout", "members/com.navdy.hud.app.view.GestureVideoCaptureView"};
    private static final java.lang.Class<?>[] STATIC_INJECTIONS = new java.lang.Class[0];

    public GestureLearningScreen$Module$$ModuleAdapter() {
        super(com.navdy.hud.app.screen.GestureLearningScreen.Module.class, INJECTS, STATIC_INJECTIONS, false, INCLUDES, true, false);
    }
}
