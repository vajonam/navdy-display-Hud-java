package com.navdy.hud.app.screen;

public interface IDashboardGaugesManager {

    public enum GaugePositioning {
        LEFT,
        RIGHT
    }

    int getOptionIconResourceForGauge(int i);

    java.lang.String getOptionLabelForGauge(int i);

    java.util.List<java.lang.Integer> getSmartDashGauges();

    com.navdy.hud.app.view.DashboardWidgetPresenter getWidgetPresenterForGauge(int i, boolean z);

    boolean isGaugeWorking(int i);

    void showGauge(int i, com.navdy.hud.app.view.DashboardWidgetView dashboardWidgetView);

    void updateGauge(int i, java.lang.Object obj);

    void updateUserPreference(com.navdy.hud.app.screen.IDashboardGaugesManager.GaugePositioning gaugePositioning, int i);
}
