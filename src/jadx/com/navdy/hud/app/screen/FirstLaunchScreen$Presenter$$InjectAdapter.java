package com.navdy.hud.app.screen;

public final class FirstLaunchScreen$Presenter$$InjectAdapter extends dagger.internal.Binding<com.navdy.hud.app.screen.FirstLaunchScreen.Presenter> implements javax.inject.Provider<com.navdy.hud.app.screen.FirstLaunchScreen.Presenter>, dagger.MembersInjector<com.navdy.hud.app.screen.FirstLaunchScreen.Presenter> {
    private dagger.internal.Binding<com.squareup.otto.Bus> bus;
    private dagger.internal.Binding<com.navdy.hud.app.device.PowerManager> powerManager;
    private dagger.internal.Binding<com.navdy.hud.app.ui.framework.BasePresenter> supertype;
    private dagger.internal.Binding<com.navdy.hud.app.ui.framework.UIStateManager> uiStateManager;

    public FirstLaunchScreen$Presenter$$InjectAdapter() {
        super("com.navdy.hud.app.screen.FirstLaunchScreen$Presenter", "members/com.navdy.hud.app.screen.FirstLaunchScreen$Presenter", true, com.navdy.hud.app.screen.FirstLaunchScreen.Presenter.class);
    }

    public void attach(dagger.internal.Linker linker) {
        this.bus = linker.requestBinding("com.squareup.otto.Bus", com.navdy.hud.app.screen.FirstLaunchScreen.Presenter.class, getClass().getClassLoader());
        this.uiStateManager = linker.requestBinding("com.navdy.hud.app.ui.framework.UIStateManager", com.navdy.hud.app.screen.FirstLaunchScreen.Presenter.class, getClass().getClassLoader());
        this.powerManager = linker.requestBinding("com.navdy.hud.app.device.PowerManager", com.navdy.hud.app.screen.FirstLaunchScreen.Presenter.class, getClass().getClassLoader());
        dagger.internal.Linker linker2 = linker;
        this.supertype = linker2.requestBinding("members/com.navdy.hud.app.ui.framework.BasePresenter", com.navdy.hud.app.screen.FirstLaunchScreen.Presenter.class, getClass().getClassLoader(), false, true);
    }

    public void getDependencies(java.util.Set<dagger.internal.Binding<?>> set, java.util.Set<dagger.internal.Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.bus);
        injectMembersBindings.add(this.uiStateManager);
        injectMembersBindings.add(this.powerManager);
        injectMembersBindings.add(this.supertype);
    }

    public com.navdy.hud.app.screen.FirstLaunchScreen.Presenter get() {
        com.navdy.hud.app.screen.FirstLaunchScreen.Presenter result = new com.navdy.hud.app.screen.FirstLaunchScreen.Presenter();
        injectMembers(result);
        return result;
    }

    public void injectMembers(com.navdy.hud.app.screen.FirstLaunchScreen.Presenter object) {
        object.bus = (com.squareup.otto.Bus) this.bus.get();
        object.uiStateManager = (com.navdy.hud.app.ui.framework.UIStateManager) this.uiStateManager.get();
        object.powerManager = (com.navdy.hud.app.device.PowerManager) this.powerManager.get();
        this.supertype.injectMembers(object);
    }
}
