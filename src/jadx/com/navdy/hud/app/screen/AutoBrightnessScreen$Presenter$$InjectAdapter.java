package com.navdy.hud.app.screen;

public final class AutoBrightnessScreen$Presenter$$InjectAdapter extends dagger.internal.Binding<com.navdy.hud.app.screen.AutoBrightnessScreen.Presenter> implements javax.inject.Provider<com.navdy.hud.app.screen.AutoBrightnessScreen.Presenter>, dagger.MembersInjector<com.navdy.hud.app.screen.AutoBrightnessScreen.Presenter> {
    private dagger.internal.Binding<com.squareup.otto.Bus> bus;
    private dagger.internal.Binding<android.content.SharedPreferences> sharedPreferences;
    private dagger.internal.Binding<com.navdy.hud.app.ui.framework.BasePresenter> supertype;

    public AutoBrightnessScreen$Presenter$$InjectAdapter() {
        super("com.navdy.hud.app.screen.AutoBrightnessScreen$Presenter", "members/com.navdy.hud.app.screen.AutoBrightnessScreen$Presenter", true, com.navdy.hud.app.screen.AutoBrightnessScreen.Presenter.class);
    }

    public void attach(dagger.internal.Linker linker) {
        this.bus = linker.requestBinding("com.squareup.otto.Bus", com.navdy.hud.app.screen.AutoBrightnessScreen.Presenter.class, getClass().getClassLoader());
        this.sharedPreferences = linker.requestBinding("android.content.SharedPreferences", com.navdy.hud.app.screen.AutoBrightnessScreen.Presenter.class, getClass().getClassLoader());
        dagger.internal.Linker linker2 = linker;
        this.supertype = linker2.requestBinding("members/com.navdy.hud.app.ui.framework.BasePresenter", com.navdy.hud.app.screen.AutoBrightnessScreen.Presenter.class, getClass().getClassLoader(), false, true);
    }

    public void getDependencies(java.util.Set<dagger.internal.Binding<?>> set, java.util.Set<dagger.internal.Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.bus);
        injectMembersBindings.add(this.sharedPreferences);
        injectMembersBindings.add(this.supertype);
    }

    public com.navdy.hud.app.screen.AutoBrightnessScreen.Presenter get() {
        com.navdy.hud.app.screen.AutoBrightnessScreen.Presenter result = new com.navdy.hud.app.screen.AutoBrightnessScreen.Presenter();
        injectMembers(result);
        return result;
    }

    public void injectMembers(com.navdy.hud.app.screen.AutoBrightnessScreen.Presenter object) {
        object.bus = (com.squareup.otto.Bus) this.bus.get();
        object.sharedPreferences = (android.content.SharedPreferences) this.sharedPreferences.get();
        this.supertype.injectMembers(object);
    }
}
