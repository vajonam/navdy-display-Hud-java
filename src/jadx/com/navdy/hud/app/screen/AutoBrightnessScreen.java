package com.navdy.hud.app.screen;

@flow.Layout(2130903109)
public class AutoBrightnessScreen extends com.navdy.hud.app.screen.BaseScreen {
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.screen.AutoBrightnessScreen.class);

    @dagger.Module(addsTo = com.navdy.hud.app.ui.activity.Main.Module.class, injects = {com.navdy.hud.app.view.AutoBrightnessView.class})
    public class Module {
        public Module() {
        }
    }

    @javax.inject.Singleton
    public static class Presenter extends com.navdy.hud.app.ui.framework.BasePresenter<com.navdy.hud.app.view.AutoBrightnessView> {
        private static final int POS_ACTION = 0;
        private com.navdy.hud.app.ui.component.ChoiceLayout.IListener autoBrightnessOffListener = new com.navdy.hud.app.screen.AutoBrightnessScreen.Presenter.Anon2();
        private com.navdy.hud.app.ui.component.ChoiceLayout.IListener autoBrightnessOnListener = new com.navdy.hud.app.screen.AutoBrightnessScreen.Presenter.Anon1();
        @javax.inject.Inject
        com.squareup.otto.Bus bus;
        private android.content.res.Resources resources;
        @javax.inject.Inject
        android.content.SharedPreferences sharedPreferences;
        private com.navdy.hud.app.view.AutoBrightnessView view;

        class Anon1 implements com.navdy.hud.app.ui.component.ChoiceLayout.IListener {
            Anon1() {
            }

            public void executeItem(int pos, int id) {
                if (pos == 0) {
                    com.navdy.hud.app.screen.AutoBrightnessScreen.Presenter.this.setAutoBrightness(false);
                }
                com.navdy.hud.app.screen.AutoBrightnessScreen.Presenter.this.bus.post(new com.navdy.service.library.events.ui.ShowScreen.Builder().screen(com.navdy.service.library.events.ui.Screen.SCREEN_BACK).build());
            }

            public void itemSelected(int pos, int id) {
            }
        }

        class Anon2 implements com.navdy.hud.app.ui.component.ChoiceLayout.IListener {
            Anon2() {
            }

            public void executeItem(int pos, int id) {
                if (pos == 0) {
                    com.navdy.hud.app.screen.AutoBrightnessScreen.Presenter.this.setAutoBrightness(true);
                }
                com.navdy.hud.app.screen.AutoBrightnessScreen.Presenter.this.bus.post(new com.navdy.service.library.events.ui.ShowScreen.Builder().screen(com.navdy.service.library.events.ui.Screen.SCREEN_BACK).build());
            }

            public void itemSelected(int pos, int id) {
            }
        }

        /* access modifiers changed from: private */
        public void setAutoBrightness(boolean enabled) {
            android.content.SharedPreferences.Editor editor = this.sharedPreferences.edit();
            editor.putString(com.navdy.hud.app.settings.HUDSettings.AUTO_BRIGHTNESS, enabled ? "true" : "false");
            editor.apply();
            com.navdy.hud.app.util.os.SystemProperties.set("persist.sys.autobrightness", enabled ? "enabled" : "disabled");
        }

        public void onLoad(android.os.Bundle savedInstanceState) {
            this.resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
            this.bus.register(this);
            updateView();
            super.onLoad(savedInstanceState);
        }

        /* access modifiers changed from: protected */
        public void updateView() {
            com.navdy.hud.app.view.AutoBrightnessView view2 = (com.navdy.hud.app.view.AutoBrightnessView) getView();
            if (view2 != null) {
                init(view2);
            }
        }

        private void init(com.navdy.hud.app.view.AutoBrightnessView view2) {
            if ("true".equals(this.sharedPreferences.getString(com.navdy.hud.app.settings.HUDSettings.AUTO_BRIGHTNESS, ""))) {
                java.util.List<com.navdy.hud.app.ui.component.ChoiceLayout.Choice> autoBrightnessOnChoices = getChoicesOn();
                view2.setTitle(this.resources.getString(com.navdy.hud.app.R.string.auto_brightness_on_title));
                view2.setStatusLabel(this.resources.getString(com.navdy.hud.app.R.string.auto_brightness_on_status_label));
                view2.setIcon(this.resources.getDrawable(com.navdy.hud.app.R.drawable.icon_mm_brightness_copy));
                view2.setChoices(autoBrightnessOnChoices, this.autoBrightnessOnListener);
            } else {
                java.util.List<com.navdy.hud.app.ui.component.ChoiceLayout.Choice> autoBrightnessOnChoices2 = getChoicesOff();
                view2.setTitle(this.resources.getString(com.navdy.hud.app.R.string.auto_brightness_off_title));
                view2.setStatusLabel(this.resources.getString(com.navdy.hud.app.R.string.auto_brightness_off_status_label));
                view2.setIcon(this.resources.getDrawable(com.navdy.hud.app.R.drawable.icon_mm_brightness_disabled));
                view2.setChoices(autoBrightnessOnChoices2, this.autoBrightnessOffListener);
            }
            this.view = view2;
        }

        private java.util.List<com.navdy.hud.app.ui.component.ChoiceLayout.Choice> getChoicesOn() {
            java.util.List<com.navdy.hud.app.ui.component.ChoiceLayout.Choice> choices = new java.util.ArrayList<>();
            java.lang.String choiceDisable = this.resources.getString(com.navdy.hud.app.R.string.auto_brightness_choice_disable);
            java.lang.String choiceCancel = this.resources.getString(com.navdy.hud.app.R.string.auto_brightness_choice_cancel);
            choices.add(new com.navdy.hud.app.ui.component.ChoiceLayout.Choice(choiceDisable, 0));
            choices.add(new com.navdy.hud.app.ui.component.ChoiceLayout.Choice(choiceCancel, 0));
            return choices;
        }

        private java.util.List<com.navdy.hud.app.ui.component.ChoiceLayout.Choice> getChoicesOff() {
            java.util.List<com.navdy.hud.app.ui.component.ChoiceLayout.Choice> choices = new java.util.ArrayList<>();
            java.lang.String choiceEnable = this.resources.getString(com.navdy.hud.app.R.string.auto_brightness_choice_enable);
            java.lang.String choiceCancel = this.resources.getString(com.navdy.hud.app.R.string.auto_brightness_choice_cancel);
            choices.add(new com.navdy.hud.app.ui.component.ChoiceLayout.Choice(choiceEnable, 0));
            choices.add(new com.navdy.hud.app.ui.component.ChoiceLayout.Choice(choiceCancel, 0));
            return choices;
        }

        public boolean handleKey(com.navdy.hud.app.manager.InputManager.CustomKeyEvent event) {
            switch (event) {
                case LEFT:
                    this.view.moveSelectionLeft();
                    break;
                case RIGHT:
                    this.view.moveSelectionRight();
                    break;
                case SELECT:
                    this.view.executeSelectedItem();
                    break;
            }
            return true;
        }
    }

    public com.navdy.service.library.events.ui.Screen getScreen() {
        return com.navdy.service.library.events.ui.Screen.SCREEN_AUTO_BRIGHTNESS;
    }

    public java.lang.String getMortarScopeName() {
        return getClass().getName();
    }

    public java.lang.Object getDaggerModule() {
        return new com.navdy.hud.app.screen.AutoBrightnessScreen.Module();
    }
}
