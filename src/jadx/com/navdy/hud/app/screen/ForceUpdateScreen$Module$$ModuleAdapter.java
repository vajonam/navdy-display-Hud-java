package com.navdy.hud.app.screen;

public final class ForceUpdateScreen$Module$$ModuleAdapter extends dagger.internal.ModuleAdapter<com.navdy.hud.app.screen.ForceUpdateScreen.Module> {
    private static final java.lang.Class<?>[] INCLUDES = new java.lang.Class[0];
    private static final java.lang.String[] INJECTS = {"members/com.navdy.hud.app.view.ForceUpdateView"};
    private static final java.lang.Class<?>[] STATIC_INJECTIONS = new java.lang.Class[0];

    /* compiled from: ForceUpdateScreen$Module$$ModuleAdapter */
    public static final class ProvideScreenProvidesAdapter extends dagger.internal.ProvidesBinding<com.navdy.hud.app.screen.ForceUpdateScreen> implements javax.inject.Provider<com.navdy.hud.app.screen.ForceUpdateScreen> {
        private final com.navdy.hud.app.screen.ForceUpdateScreen.Module module;

        public ProvideScreenProvidesAdapter(com.navdy.hud.app.screen.ForceUpdateScreen.Module module2) {
            super("com.navdy.hud.app.screen.ForceUpdateScreen", false, "com.navdy.hud.app.screen.ForceUpdateScreen.Module", "provideScreen");
            this.module = module2;
            setLibrary(false);
        }

        public com.navdy.hud.app.screen.ForceUpdateScreen get() {
            return this.module.provideScreen();
        }
    }

    public ForceUpdateScreen$Module$$ModuleAdapter() {
        super(com.navdy.hud.app.screen.ForceUpdateScreen.Module.class, INJECTS, STATIC_INJECTIONS, false, INCLUDES, true, false);
    }

    public void getBindings(dagger.internal.BindingsGroup bindings, com.navdy.hud.app.screen.ForceUpdateScreen.Module module) {
        bindings.contributeProvidesBinding("com.navdy.hud.app.screen.ForceUpdateScreen", new com.navdy.hud.app.screen.ForceUpdateScreen$Module$$ModuleAdapter.ProvideScreenProvidesAdapter(module));
    }
}
