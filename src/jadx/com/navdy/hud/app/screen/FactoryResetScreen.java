package com.navdy.hud.app.screen;

@flow.Layout(2130903115)
public class FactoryResetScreen extends com.navdy.hud.app.screen.BaseScreen {
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.screen.FactoryResetScreen.class);

    @dagger.Module(addsTo = com.navdy.hud.app.ui.activity.Main.Module.class, injects = {com.navdy.hud.app.view.FactoryResetView.class})
    public class Module {
        public Module() {
        }
    }

    @javax.inject.Singleton
    public static class Presenter extends com.navdy.hud.app.ui.framework.BasePresenter<com.navdy.hud.app.view.FactoryResetView> {
        private static final int CANCEL_POSITION = 1;
        private static final java.util.List<java.lang.String> CONFIRMATION_CHOICES = new java.util.ArrayList();
        private static final int FACTORY_RESET_POSITION = 0;
        @javax.inject.Inject
        com.squareup.otto.Bus bus;
        private com.navdy.hud.app.ui.component.ChoiceLayout.IListener confirmationListener = new com.navdy.hud.app.screen.FactoryResetScreen.Presenter.Anon1();
        private com.navdy.hud.app.ui.component.ConfirmationLayout factoryResetConfirmation;
        private android.content.res.Resources resources;

        class Anon1 implements com.navdy.hud.app.ui.component.ChoiceLayout.IListener {

            /* renamed from: com.navdy.hud.app.screen.FactoryResetScreen$Presenter$Anon1$Anon1 reason: collision with other inner class name */
            class C0031Anon1 implements java.lang.Runnable {
                C0031Anon1() {
                }

                public void run() {
                    try {
                        android.os.RecoverySystem.rebootWipeUserData(com.navdy.hud.app.HudApplication.getAppContext());
                    } catch (java.lang.Exception e) {
                        com.navdy.hud.app.screen.FactoryResetScreen.sLogger.e("Failed to do factory reset", e);
                        com.navdy.hud.app.screen.FactoryResetScreen.Presenter.this.bus.post(new com.navdy.service.library.events.ui.ShowScreen.Builder().screen(com.navdy.service.library.events.ui.Screen.SCREEN_BACK).build());
                    }
                }
            }

            Anon1() {
            }

            public void executeItem(int pos, int id) {
                switch (pos) {
                    case 0:
                        com.navdy.hud.app.analytics.AnalyticsSupport.recordShutdown(com.navdy.hud.app.event.Shutdown.Reason.FACTORY_RESET, false);
                        com.navdy.hud.app.device.dial.DialManager.getInstance().requestDialForgetKeys();
                        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.screen.FactoryResetScreen.Presenter.Anon1.C0031Anon1(), 1);
                        return;
                    case 1:
                        com.navdy.hud.app.screen.FactoryResetScreen.Presenter.this.bus.post(new com.navdy.service.library.events.ui.ShowScreen.Builder().screen(com.navdy.service.library.events.ui.Screen.SCREEN_BACK).build());
                        return;
                    default:
                        return;
                }
            }

            public void itemSelected(int pos, int id) {
            }
        }

        static {
            CONFIRMATION_CHOICES.add(com.navdy.hud.app.HudApplication.getAppContext().getResources().getString(com.navdy.hud.app.R.string.factory_reset_go));
            CONFIRMATION_CHOICES.add(com.navdy.hud.app.HudApplication.getAppContext().getResources().getString(com.navdy.hud.app.R.string.factory_reset_cancel));
        }

        public void onLoad(android.os.Bundle savedInstanceState) {
            this.resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
            updateView();
            super.onLoad(savedInstanceState);
        }

        public boolean handleKey(com.navdy.hud.app.manager.InputManager.CustomKeyEvent event) {
            return this.factoryResetConfirmation.handleKey(event);
        }

        /* access modifiers changed from: protected */
        public void updateView() {
            com.navdy.hud.app.view.FactoryResetView view = (com.navdy.hud.app.view.FactoryResetView) getView();
            if (view != null) {
                this.factoryResetConfirmation = view.getConfirmation();
                this.factoryResetConfirmation.screenTitle.setText(this.resources.getString(com.navdy.hud.app.R.string.factory_reset_screen_title));
                this.factoryResetConfirmation.title1.setVisibility(8);
                this.factoryResetConfirmation.title2.setVisibility(8);
                this.factoryResetConfirmation.title3.setSingleLine(false);
                this.factoryResetConfirmation.title3.setText(this.resources.getString(com.navdy.hud.app.R.string.factory_reset_description));
                this.factoryResetConfirmation.screenImage.setImageDrawable(this.resources.getDrawable(com.navdy.hud.app.R.drawable.icon_settings_factory_reset));
                this.factoryResetConfirmation.setChoices(CONFIRMATION_CHOICES, 1, this.confirmationListener);
            }
        }
    }

    public com.navdy.service.library.events.ui.Screen getScreen() {
        return com.navdy.service.library.events.ui.Screen.SCREEN_FACTORY_RESET;
    }

    public java.lang.String getMortarScopeName() {
        return getClass().getName();
    }

    public java.lang.Object getDaggerModule() {
        return new com.navdy.hud.app.screen.FactoryResetScreen.Module();
    }
}
