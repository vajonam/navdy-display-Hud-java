package com.navdy.hud.app.screen;

@flow.Layout(2130903114)
public class DialUpdateProgressScreen extends com.navdy.hud.app.screen.BaseScreen {
    private static final java.lang.String DIAL_UPDATE_ERROR_TOAST_ID = "dial-fw-update-err";
    public static final java.lang.String EXTRA_PROGRESS_CAUSE = "PROGRESS_CAUSE";
    public static final int POST_OTA_PAIRING_DELAY = 6000;
    public static final int POST_OTA_SHUTDOWN_DELAY = 30000;
    public static final int SETTINGS_SCREEN = 1;
    public static final int SHUTDOWN_SCREEN = 2;
    /* access modifiers changed from: private */
    public static android.os.Handler handler = new android.os.Handler(android.os.Looper.getMainLooper());
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.screen.DialUpdateProgressScreen.class);
    public static boolean updateStarted = false;

    @dagger.Module(addsTo = com.navdy.hud.app.ui.activity.Main.Module.class, injects = {com.navdy.hud.app.view.DialUpdateProgressView.class})
    public class Module {
        public Module() {
        }

        /* access modifiers changed from: 0000 */
        @dagger.Provides
        public com.navdy.hud.app.screen.DialUpdateProgressScreen provideScreen() {
            return com.navdy.hud.app.screen.DialUpdateProgressScreen.this;
        }
    }

    @javax.inject.Singleton
    public static class Presenter extends com.navdy.hud.app.ui.framework.BasePresenter<com.navdy.hud.app.view.DialUpdateProgressView> {
        private int cause;
        @javax.inject.Inject
        com.squareup.otto.Bus mBus;
        @javax.inject.Inject
        android.content.SharedPreferences mPreferences;
        @javax.inject.Inject
        com.navdy.hud.app.screen.DialUpdateProgressScreen mScreen;

        class Anon1 implements java.lang.Runnable {
            Anon1() {
            }

            public void run() {
                com.navdy.hud.app.screen.DialUpdateProgressScreen.Presenter.this.mBus.post(new com.navdy.hud.app.event.Shutdown(com.navdy.hud.app.event.Shutdown.Reason.DIAL_OTA));
            }
        }

        class Anon2 implements java.lang.Runnable {
            Anon2() {
            }

            public void run() {
                android.os.Bundle args = new android.os.Bundle();
                args.putString(com.navdy.hud.app.device.dial.DialConstants.OTA_DIAL_NAME_KEY, com.navdy.hud.app.device.dial.DialManager.getInstance().getDialFirmwareUpdater().getDialName());
                com.navdy.hud.app.screen.DialUpdateProgressScreen.Presenter.this.mBus.post(new com.navdy.hud.app.event.ShowScreenWithArgs(com.navdy.service.library.events.ui.Screen.SCREEN_DIAL_PAIRING, args, false));
            }
        }

        class Anon3 implements com.navdy.hud.app.device.dial.DialFirmwareUpdater.UpdateProgressListener {

            class Anon1 implements java.lang.Runnable {
                final /* synthetic */ int val$percentage;

                Anon1(int i) {
                    this.val$percentage = i;
                }

                public void run() {
                    com.navdy.hud.app.view.DialUpdateProgressView view = (com.navdy.hud.app.view.DialUpdateProgressView) com.navdy.hud.app.screen.DialUpdateProgressScreen.Presenter.this.getView();
                    if (view != null) {
                        view.setProgress(this.val$percentage);
                    }
                }
            }

            class Anon2 implements java.lang.Runnable {
                final /* synthetic */ com.navdy.hud.app.device.dial.DialFirmwareUpdater.Error val$error;

                Anon2(com.navdy.hud.app.device.dial.DialFirmwareUpdater.Error error) {
                    this.val$error = error;
                }

                public void run() {
                    com.navdy.hud.app.screen.DialUpdateProgressScreen.Presenter.this.finish(this.val$error);
                }
            }

            Anon3() {
            }

            public void onProgress(int percentage) {
                com.navdy.hud.app.screen.DialUpdateProgressScreen.handler.post(new com.navdy.hud.app.screen.DialUpdateProgressScreen.Presenter.Anon3.Anon1(percentage));
            }

            public void onFinished(com.navdy.hud.app.device.dial.DialFirmwareUpdater.Error error, java.lang.String detail) {
                com.navdy.hud.app.screen.DialUpdateProgressScreen.sLogger.v("onFinished:" + error + " , " + detail);
                com.navdy.hud.app.screen.DialUpdateProgressScreen.handler.post(new com.navdy.hud.app.screen.DialUpdateProgressScreen.Presenter.Anon3.Anon2(error));
            }
        }

        public void onLoad(android.os.Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);
            if (this.mScreen != null) {
                android.os.Bundle args = this.mScreen.arguments;
                if (args != null) {
                    this.cause = args.getInt(com.navdy.hud.app.screen.DialUpdateProgressScreen.EXTRA_PROGRESS_CAUSE, 2);
                }
            }
        }

        public void finish(com.navdy.hud.app.device.dial.DialFirmwareUpdater.Error error) {
            com.navdy.hud.app.screen.DialUpdateProgressScreen.updateStarted = false;
            switch (this.cause) {
                case 2:
                    com.navdy.hud.app.screen.DialUpdateProgressScreen.handler.postDelayed(new com.navdy.hud.app.screen.DialUpdateProgressScreen.Presenter.Anon1(), 30000);
                    break;
            }
            com.navdy.hud.app.service.ShutdownMonitor.getInstance().disableScreenDim(false);
            if (error != com.navdy.hud.app.device.dial.DialFirmwareUpdater.Error.NONE) {
                com.navdy.hud.app.framework.toast.ToastManager.getInstance().disableToasts(false);
                com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().enableNotifications(true);
                this.mBus.post(new com.navdy.service.library.events.ui.ShowScreen.Builder().screen(com.navdy.service.library.events.ui.Screen.SCREEN_BACK).build());
                cancelUpdate();
                showDialUpdateErrorToast(error);
                return;
            }
            com.navdy.hud.app.screen.DialUpdateProgressScreen.handler.postDelayed(new com.navdy.hud.app.screen.DialUpdateProgressScreen.Presenter.Anon2(), 6000);
        }

        public com.navdy.hud.app.device.dial.DialFirmwareUpdater.Versions getVersions() {
            return com.navdy.hud.app.device.dial.DialManager.getInstance().getDialFirmwareUpdater().getVersions();
        }

        public void startUpdate() {
            if (!com.navdy.hud.app.screen.DialUpdateProgressScreen.updateStarted) {
                com.navdy.hud.app.screen.DialUpdateProgressScreen.updateStarted = true;
                com.navdy.hud.app.service.ShutdownMonitor.getInstance().disableScreenDim(true);
                com.navdy.hud.app.device.dial.DialManager.getInstance().getDialFirmwareUpdater().startUpdate(new com.navdy.hud.app.screen.DialUpdateProgressScreen.Presenter.Anon3());
            }
        }

        public void cancelUpdate() {
            com.navdy.hud.app.device.dial.DialManager.getInstance().getDialFirmwareUpdater().cancelUpdate();
        }

        private void showDialUpdateErrorToast(com.navdy.hud.app.device.dial.DialFirmwareUpdater.Error error) {
            android.content.res.Resources resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
            android.os.Bundle bundle = new android.os.Bundle();
            bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_TIMEOUT, 10000);
            bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_IMAGE, com.navdy.hud.app.R.drawable.icon_dial_update);
            bundle.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE, resources.getString(com.navdy.hud.app.R.string.dial_update_err));
            bundle.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_2, error.name());
            bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_2_STYLE, com.navdy.hud.app.R.style.Glances_1);
            bundle.putBoolean(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_DEFAULT_CHOICE, true);
            com.navdy.hud.app.framework.toast.ToastManager toastManager = com.navdy.hud.app.framework.toast.ToastManager.getInstance();
            toastManager.dismissCurrentToast(com.navdy.hud.app.screen.DialUpdateProgressScreen.DIAL_UPDATE_ERROR_TOAST_ID);
            toastManager.addToast(new com.navdy.hud.app.framework.toast.ToastManager.ToastParams(com.navdy.hud.app.screen.DialUpdateProgressScreen.DIAL_UPDATE_ERROR_TOAST_ID, bundle, null, true, false));
        }
    }

    public java.lang.String getMortarScopeName() {
        return com.navdy.hud.app.screen.DialUpdateProgressScreen.class.getName();
    }

    public java.lang.Object getDaggerModule() {
        return new com.navdy.hud.app.screen.DialUpdateProgressScreen.Module();
    }

    public com.navdy.service.library.events.ui.Screen getScreen() {
        return com.navdy.service.library.events.ui.Screen.SCREEN_DIAL_UPDATE_PROGRESS;
    }
}
