package com.navdy.hud.app.screen;

public final class AutoBrightnessScreen$Module$$ModuleAdapter extends dagger.internal.ModuleAdapter<com.navdy.hud.app.screen.AutoBrightnessScreen.Module> {
    private static final java.lang.Class<?>[] INCLUDES = new java.lang.Class[0];
    private static final java.lang.String[] INJECTS = {"members/com.navdy.hud.app.view.AutoBrightnessView"};
    private static final java.lang.Class<?>[] STATIC_INJECTIONS = new java.lang.Class[0];

    public AutoBrightnessScreen$Module$$ModuleAdapter() {
        super(com.navdy.hud.app.screen.AutoBrightnessScreen.Module.class, INJECTS, STATIC_INJECTIONS, false, INCLUDES, true, false);
    }
}
