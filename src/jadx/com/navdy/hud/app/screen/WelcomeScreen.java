package com.navdy.hud.app.screen;

@flow.Layout(2130903139)
public class WelcomeScreen extends com.navdy.hud.app.screen.BaseScreen {
    public static java.lang.String ACTION_RECONNECT = "reconnect";
    public static java.lang.String ACTION_SWITCH_PHONE = "switch";
    private static final int APP_LAUNCH_TIMEOUT = 12000;
    public static java.lang.String ARG_ACTION = "action";
    private static final int CONNECTION_TIMEOUT = 5000;
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.screen.WelcomeScreen.class);

    public static class DeviceMetadata {
        public final com.navdy.service.library.device.NavdyDeviceId deviceId;
        public final com.navdy.hud.app.profile.DriverProfile driverProfile;

        public DeviceMetadata(com.navdy.service.library.device.NavdyDeviceId deviceId2, com.navdy.hud.app.profile.DriverProfile driverProfile2) {
            this.deviceId = deviceId2;
            this.driverProfile = driverProfile2;
        }

        public java.lang.String toString() {
            return "DeviceMetadata{deviceId=" + this.deviceId + ", driverProfile=" + this.driverProfile + '}';
        }
    }

    @dagger.Module(addsTo = com.navdy.hud.app.ui.activity.Main.Module.class, injects = {com.navdy.hud.app.view.WelcomeView.class})
    public class Module {
        public Module() {
        }
    }

    @javax.inject.Singleton
    public static class Presenter extends com.navdy.hud.app.ui.framework.BasePresenter<com.navdy.hud.app.view.WelcomeView> {
        private static final int MESSAGE_TIMEOUT = 2500;
        public static final int MISSING_DEVICES_OFFSET = 100;
        /* access modifiers changed from: private */
        public boolean appConnected;
        private java.lang.Runnable appLaunchTimeout = new com.navdy.hud.app.screen.WelcomeScreen.Presenter.Anon2();
        /* access modifiers changed from: private */
        public boolean appLaunched;
        private java.lang.Runnable appWaitTimeout = new com.navdy.hud.app.screen.WelcomeScreen.Presenter.Anon3();
        @javax.inject.Inject
        com.squareup.otto.Bus bus;
        /* access modifiers changed from: private */
        public boolean connected;
        /* access modifiers changed from: private */
        public com.navdy.service.library.device.NavdyDeviceId connectingDevice;
        @javax.inject.Inject
        com.navdy.hud.app.service.ConnectionHandler connectionHandler;
        private int currentItem;
        private java.lang.Runnable deviceConnectTimeout = new com.navdy.hud.app.screen.WelcomeScreen.Presenter.Anon1();
        /* access modifiers changed from: private */
        public com.navdy.service.library.device.RemoteDeviceRegistry deviceRegistry = com.navdy.service.library.device.RemoteDeviceRegistry.getInstance(com.navdy.hud.app.HudApplication.getAppContext());
        private boolean dirty = false;
        private com.navdy.service.library.device.NavdyDeviceId failedConnection;
        private boolean firstUpdate;
        @javax.inject.Inject
        com.navdy.hud.app.gesture.GestureServiceConnector gestureServiceConnector;
        private android.os.Handler handler = new android.os.Handler();
        private boolean ledUpdated = false;
        private java.util.concurrent.atomic.AtomicBoolean mGreetingPending = new java.util.concurrent.atomic.AtomicBoolean(false);
        @javax.inject.Inject
        com.navdy.hud.app.manager.PairingManager pairingManager;
        @javax.inject.Inject
        com.navdy.hud.app.profile.DriverProfileManager profileManager;
        /* access modifiers changed from: private */
        public boolean searching = false;
        /* access modifiers changed from: private */
        public com.navdy.hud.app.screen.WelcomeScreen.State state = com.navdy.hud.app.screen.WelcomeScreen.State.UNKNOWN;
        private java.lang.Runnable stateTimeout = new com.navdy.hud.app.screen.WelcomeScreen.Presenter.Anon4();
        @javax.inject.Inject
        com.navdy.hud.app.ui.framework.UIStateManager uiStateManager;

        class Anon1 implements java.lang.Runnable {
            Anon1() {
            }

            public void run() {
                com.navdy.hud.app.screen.WelcomeScreen.logger.i("checking connection timeout - connected:" + com.navdy.hud.app.screen.WelcomeScreen.Presenter.this.connected);
                if (!com.navdy.hud.app.screen.WelcomeScreen.Presenter.this.connected) {
                    com.navdy.hud.app.screen.WelcomeScreen.Presenter.this.setState(com.navdy.hud.app.screen.WelcomeScreen.State.CONNECTION_FAILED);
                    com.navdy.hud.app.screen.WelcomeScreen.Presenter.this.updateView();
                }
            }
        }

        class Anon2 implements java.lang.Runnable {
            Anon2() {
            }

            public void run() {
                com.navdy.hud.app.screen.WelcomeScreen.logger.v("appLaunch: " + com.navdy.hud.app.screen.WelcomeScreen.Presenter.this.appConnected);
                if (!com.navdy.hud.app.screen.WelcomeScreen.Presenter.this.appConnected && com.navdy.hud.app.ui.activity.Main.mProtocolStatus == com.navdy.hud.app.ui.activity.Main.ProtocolStatus.PROTOCOL_VALID) {
                    com.navdy.hud.app.screen.WelcomeScreen.logger.v("showing app dis-connected toast");
                    com.navdy.hud.app.framework.connection.ConnectionNotification.showDisconnectedToast(true);
                    com.navdy.hud.app.screen.WelcomeScreen.Presenter.this.appLaunched = true;
                }
            }
        }

        class Anon3 implements java.lang.Runnable {
            Anon3() {
            }

            public void run() {
                if (com.navdy.hud.app.screen.WelcomeScreen.Presenter.this.connected && !com.navdy.hud.app.screen.WelcomeScreen.Presenter.this.appConnected) {
                    com.navdy.hud.app.framework.connection.ConnectionNotification.showDisconnectedToast(true);
                }
            }
        }

        class Anon4 implements java.lang.Runnable {
            Anon4() {
            }

            public void run() {
                com.navdy.hud.app.view.WelcomeView view = (com.navdy.hud.app.view.WelcomeView) com.navdy.hud.app.screen.WelcomeScreen.Presenter.this.getView();
                com.navdy.hud.app.screen.WelcomeScreen.logger.i("State timeout fired - current state:" + com.navdy.hud.app.screen.WelcomeScreen.Presenter.this.state + " current view:" + view);
                if (view != null) {
                    switch (com.navdy.hud.app.screen.WelcomeScreen.Presenter.this.state) {
                        case WELCOME:
                            com.navdy.hud.app.screen.WelcomeScreen.logger.i("Welcome timed out - going back");
                            com.navdy.hud.app.screen.WelcomeScreen.Presenter.this.connectingDevice = null;
                            com.navdy.hud.app.screen.WelcomeScreen.Presenter.this.finish();
                            return;
                        case CONNECTION_FAILED:
                            com.navdy.hud.app.screen.WelcomeScreen.Presenter.this.setState(com.navdy.hud.app.screen.WelcomeScreen.State.PICKING);
                            com.navdy.hud.app.screen.WelcomeScreen.Presenter.this.updateView();
                            return;
                        default:
                            return;
                    }
                }
            }
        }

        class Anon5 implements java.lang.Runnable {
            Anon5() {
            }

            public void run() {
                com.navdy.hud.app.screen.WelcomeScreen.Presenter.this.deviceRegistry.refresh();
            }
        }

        class Anon6 implements java.lang.Runnable {
            Anon6() {
            }

            public void run() {
                com.navdy.hud.app.view.WelcomeView view = (com.navdy.hud.app.view.WelcomeView) com.navdy.hud.app.screen.WelcomeScreen.Presenter.this.getView();
                if (view != null) {
                    view.setSearching(com.navdy.hud.app.screen.WelcomeScreen.Presenter.this.searching);
                }
            }
        }

        class Anon7 implements java.lang.Runnable {
            final /* synthetic */ com.navdy.service.library.device.NavdyDeviceId val$deviceId;

            Anon7(com.navdy.service.library.device.NavdyDeviceId navdyDeviceId) {
                this.val$deviceId = navdyDeviceId;
            }

            public void run() {
                com.navdy.hud.app.screen.WelcomeScreen.Presenter.this.connectionHandler.connectToDevice(this.val$deviceId);
            }
        }

        public void onLoad(android.os.Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);
            this.uiStateManager.enableNotificationColor(false);
            java.lang.String action = null;
            if (savedInstanceState != null) {
                action = savedInstanceState.getString(com.navdy.hud.app.screen.WelcomeScreen.ARG_ACTION);
            }
            com.navdy.hud.app.screen.WelcomeScreen.logger.i("onLoad - action:" + action);
            this.firstUpdate = true;
            this.bus.register(this);
            this.currentItem = 0;
            this.dirty = true;
            if (!com.navdy.hud.app.screen.WelcomeScreen.ACTION_SWITCH_PHONE.equals(action)) {
                if (!com.navdy.hud.app.screen.WelcomeScreen.ACTION_RECONNECT.equals(action) && this.state != com.navdy.hud.app.screen.WelcomeScreen.State.UNKNOWN) {
                    switch (this.state) {
                        case WELCOME:
                        case CONNECTING:
                        case LAUNCHING_APP:
                        case CONNECTION_FAILED:
                            if (this.connectingDevice != null) {
                                this.handler.removeCallbacks(this.stateTimeout);
                                this.handler.postDelayed(this.stateTimeout, 2500);
                                break;
                            } else {
                                finish();
                                return;
                            }
                        case SEARCHING:
                            startSearchIfNeeded();
                            break;
                    }
                } else if (hasPaired()) {
                    setSearching(true);
                    setState(com.navdy.hud.app.screen.WelcomeScreen.State.SEARCHING);
                    startSearchIfNeeded();
                } else {
                    setState(com.navdy.hud.app.screen.WelcomeScreen.State.DOWNLOAD_APP);
                }
            } else {
                this.connectingDevice = null;
                if (hasPaired()) {
                    setState(com.navdy.hud.app.screen.WelcomeScreen.State.PICKING);
                } else {
                    setState(com.navdy.hud.app.screen.WelcomeScreen.State.DOWNLOAD_APP);
                }
            }
            updateView();
        }

        /* access modifiers changed from: protected */
        public void onUnload() {
            updatePairingStatusOnLED(false);
            this.uiStateManager.enableNotificationColor(true);
        }

        private void startSearchIfNeeded() {
            if (this.state == com.navdy.hud.app.screen.WelcomeScreen.State.SEARCHING && this.connectionHandler.serviceConnected() && android.bluetooth.BluetoothAdapter.getDefaultAdapter().isEnabled()) {
                this.connectionHandler.searchForDevices();
            }
        }

        private boolean hasPaired() {
            return this.deviceRegistry.hasPaired();
        }

        /* access modifiers changed from: private */
        public void setState(com.navdy.hud.app.screen.WelcomeScreen.State state2) {
            if (this.state != state2) {
                this.state = state2;
                this.pairingManager.setAutoPairing(false);
                clearCallbacks();
                this.dirty = true;
                com.navdy.hud.app.screen.WelcomeScreen.logger.i("switching to state:" + state2);
                switch (state2) {
                    case WELCOME:
                        this.mGreetingPending.set(true);
                        this.handler.removeCallbacks(this.stateTimeout);
                        this.handler.postDelayed(this.stateTimeout, 2500);
                        break;
                    case CONNECTING:
                        this.handler.postDelayed(this.deviceConnectTimeout, 5000);
                        break;
                    case LAUNCHING_APP:
                        this.handler.postDelayed(this.appLaunchTimeout, 12000);
                        break;
                    case CONNECTION_FAILED:
                        this.handler.removeCallbacks(this.stateTimeout);
                        this.handler.postDelayed(this.stateTimeout, 2500);
                        break;
                    case SEARCHING:
                    case PICKING:
                        if (this.connectingDevice != null) {
                            this.failedConnection = this.connectingDevice;
                            this.connectingDevice = null;
                            break;
                        }
                        break;
                    case DOWNLOAD_APP:
                        this.connectionHandler.stopSearch();
                        this.bus.post(new com.navdy.hud.app.event.Disconnect());
                        this.pairingManager.setAutoPairing(true);
                        break;
                }
                updateViewState();
            }
        }

        private void clearCallbacks() {
            this.handler.removeCallbacks(this.deviceConnectTimeout);
            this.handler.removeCallbacks(this.appLaunchTimeout);
            this.handler.removeCallbacks(this.appWaitTimeout);
        }

        private void clearLaunchNotification() {
            this.appLaunched = false;
            clearCallbacks();
            com.navdy.hud.app.framework.toast.ToastManager toastManager = com.navdy.hud.app.framework.toast.ToastManager.getInstance();
            toastManager.dismissCurrentToast(com.navdy.hud.app.framework.connection.ConnectionNotification.DISCONNECT_ID);
            toastManager.dismissCurrentToast("connection#toast");
        }

        @com.squareup.otto.Subscribe
        public void onDriverProfileUpdated(com.navdy.hud.app.event.DriverProfileUpdated event) {
            com.navdy.hud.app.profile.DriverProfile profile = this.profileManager.getCurrentProfile();
            if (this.state == com.navdy.hud.app.screen.WelcomeScreen.State.WELCOME) {
                com.navdy.hud.app.screen.WelcomeScreen.logger.v("sayWelcome profile[" + profile.getProfileName() + "] FN[" + profile.getFirstName() + "]");
                updateView();
                sayWelcome(profile);
                if (event.state == com.navdy.hud.app.event.DriverProfileUpdated.State.UPDATED) {
                    this.handler.removeCallbacks(this.stateTimeout);
                    this.handler.postDelayed(this.stateTimeout, 2500);
                }
            }
        }

        @com.squareup.otto.Subscribe
        public void onDisconnect(com.navdy.hud.app.event.Disconnect disconnect) {
            this.appLaunched = false;
            clearCallbacks();
            if (this.state != com.navdy.hud.app.screen.WelcomeScreen.State.DOWNLOAD_APP) {
                finish();
            }
        }

        private void sayWelcome(com.navdy.hud.app.profile.DriverProfile profile) {
            if (this.mGreetingPending.compareAndSet(true, false)) {
                com.navdy.hud.app.framework.voice.TTSUtils.sendSpeechRequest(com.navdy.hud.app.HudApplication.getAppContext().getResources().getString(com.navdy.hud.app.R.string.welcome_welcome_driver, new java.lang.Object[]{profile.getFirstName()}), com.navdy.service.library.events.audio.Category.SPEECH_WELCOME_MESSAGE, null);
            }
        }

        private void updateViewState() {
            com.navdy.hud.app.view.WelcomeView view = (com.navdy.hud.app.view.WelcomeView) getView();
            if (view != null) {
                int viewState = -1;
                switch (this.state) {
                    case WELCOME:
                    case CONNECTING:
                    case LAUNCHING_APP:
                    case SEARCHING:
                        viewState = 3;
                        break;
                    case DOWNLOAD_APP:
                        viewState = 1;
                        break;
                    case PICKING:
                        viewState = 2;
                        break;
                }
                if (viewState != -1) {
                    view.setState(viewState);
                }
            }
        }

        public void cancel() {
            this.connectionHandler.stopSearch();
            finish();
        }

        public void finish() {
            com.navdy.hud.app.screen.BaseScreen screen = this.uiStateManager.getCurrentScreen();
            if (screen == null || screen.getScreen() == com.navdy.service.library.events.ui.Screen.SCREEN_WELCOME) {
                com.navdy.hud.app.screen.WelcomeScreen.logger.v("finish: switching: " + screen.getScreen());
                this.bus.post(new com.navdy.service.library.events.ui.ShowScreen.Builder().screen(this.uiStateManager.getDefaultMainActiveScreen()).build());
                return;
            }
            com.navdy.hud.app.screen.WelcomeScreen.logger.v("finish: not switching: " + screen.getScreen());
        }

        @com.squareup.otto.Subscribe
        public void onConnectionStateChange(com.navdy.service.library.events.connection.ConnectionStateChange stateChange) {
            com.navdy.service.library.device.NavdyDeviceId remoteDeviceId = null;
            try {
                remoteDeviceId = new com.navdy.service.library.device.NavdyDeviceId(stateChange.remoteDeviceId);
            } catch (java.lang.Exception e) {
            }
            com.navdy.hud.app.screen.WelcomeScreen.logger.v("connection state change:" + stateChange.state + " for: " + remoteDeviceId);
            switch (stateChange.state) {
                case CONNECTION_LINK_LOST:
                    this.connected = false;
                    if (remoteDeviceId != null) {
                        if (remoteDeviceId.equals(this.connectingDevice)) {
                            clearLaunchNotification();
                            setState(com.navdy.hud.app.screen.WelcomeScreen.State.PICKING);
                            break;
                        }
                    } else {
                        startSearchIfNeeded();
                        break;
                    }
                    break;
                case CONNECTION_CONNECTED:
                    this.connectingDevice = remoteDeviceId;
                    break;
                case CONNECTION_VERIFIED:
                    this.appConnected = true;
                    this.connectingDevice = remoteDeviceId;
                    setState(com.navdy.hud.app.screen.WelcomeScreen.State.WELCOME);
                    break;
                case CONNECTION_DISCONNECTED:
                    if (!(remoteDeviceId == null || this.connectingDevice == null || !this.connectingDevice.equals(remoteDeviceId))) {
                        setState(com.navdy.hud.app.screen.WelcomeScreen.State.PICKING);
                        break;
                    }
                case CONNECTION_LINK_ESTABLISHED:
                    this.connectingDevice = remoteDeviceId;
                    this.connected = true;
                    this.appConnected = false;
                    updatePairingStatusOnLED(false);
                    setDevice(remoteDeviceId);
                    setState(com.navdy.hud.app.screen.WelcomeScreen.State.LAUNCHING_APP);
                    break;
                default:
                    return;
            }
            updateView();
        }

        @com.squareup.otto.Subscribe
        public void onConnectionStatus(com.navdy.service.library.events.connection.ConnectionStatus event) {
            com.navdy.service.library.device.NavdyDeviceId remoteDeviceId = null;
            try {
                remoteDeviceId = new com.navdy.service.library.device.NavdyDeviceId(event.remoteDeviceId);
            } catch (java.lang.Exception e) {
            }
            com.navdy.hud.app.screen.WelcomeScreen.logger.i("ConnectionStatus: " + event);
            switch (event.status) {
                case CONNECTION_SEARCH_STARTED:
                    setSearching(true);
                    updatePairingStatusOnLED(true);
                    break;
                case CONNECTION_SEARCH_FINISHED:
                    setSearching(false);
                    updatePairingStatusOnLED(false);
                    if (this.state == com.navdy.hud.app.screen.WelcomeScreen.State.SEARCHING) {
                        this.connectionHandler.stopSearch();
                        finish();
                        break;
                    }
                    break;
                case CONNECTION_FOUND:
                    if (this.state == com.navdy.hud.app.screen.WelcomeScreen.State.SEARCHING) {
                        selectDevice(remoteDeviceId);
                        break;
                    }
                    break;
                case CONNECTION_LOST:
                    break;
                case CONNECTION_PAIRED_DEVICES_CHANGED:
                    com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.screen.WelcomeScreen.Presenter.Anon5(), 1);
                    return;
                default:
                    return;
            }
            updateView();
        }

        @com.squareup.otto.Subscribe
        public void onDismissToast(com.navdy.hud.app.framework.toast.ToastManager.DismissedToast event) {
            if (this.appLaunched && android.text.TextUtils.equals(com.navdy.hud.app.framework.connection.ConnectionNotification.DISCONNECT_ID, event.name)) {
                com.navdy.hud.app.screen.WelcomeScreen.logger.v("launching app timeout");
                this.handler.removeCallbacks(this.appLaunchTimeout);
                this.handler.removeCallbacks(this.appWaitTimeout);
                this.handler.postDelayed(this.appWaitTimeout, 5000);
            }
        }

        @com.squareup.otto.Subscribe
        public void onPhotoDownload(com.navdy.hud.app.framework.contacts.PhoneImageDownloader.PhotoDownloadStatus event) {
            if (event.photoType == com.navdy.service.library.events.photo.PhotoType.PHOTO_DRIVER_PROFILE && !event.alreadyDownloaded) {
                com.navdy.hud.app.view.WelcomeView view = (com.navdy.hud.app.view.WelcomeView) getView();
                if (view != null) {
                    view.carousel.reload();
                }
            }
        }

        private void setSearching(boolean searching2) {
            if (this.searching != searching2) {
                this.searching = searching2;
                this.dirty = true;
            }
        }

        private void setDevice(com.navdy.service.library.device.NavdyDeviceId device) {
            this.connectingDevice = device;
            this.failedConnection = null;
            this.dirty = true;
            this.currentItem = 0;
        }

        /* access modifiers changed from: protected */
        public void updateView() {
            int i = 1;
            com.navdy.hud.app.view.WelcomeView view = (com.navdy.hud.app.view.WelcomeView) getView();
            if (view != null) {
                updateViewState();
                if (this.dirty) {
                    if (this.firstUpdate) {
                        this.firstUpdate = false;
                        this.handler.postDelayed(new com.navdy.hud.app.screen.WelcomeScreen.Presenter.Anon6(), 1000);
                    } else {
                        view.setSearching(this.searching);
                    }
                    this.dirty = false;
                    java.util.List<com.navdy.hud.app.ui.component.carousel.Carousel.Model> list = new java.util.ArrayList<>();
                    switch (this.state) {
                        case WELCOME:
                            list.add(buildDriverModel(this.connectingDevice, com.navdy.hud.app.R.id.welcome_menu_connecting, com.navdy.hud.app.R.string.welcome_welcome));
                            break;
                        case CONNECTING:
                        case LAUNCHING_APP:
                            list.add(buildDriverModel(this.connectingDevice, com.navdy.hud.app.R.id.welcome_menu_connecting, com.navdy.hud.app.R.string.welcome_connecting));
                            break;
                        case CONNECTION_FAILED:
                            list.add(buildDriverModel(this.connectingDevice, com.navdy.hud.app.R.id.welcome_menu_connecting, com.navdy.hud.app.R.string.welcome_cant_connect, com.navdy.hud.app.R.string.welcome_bluetooth_disabled));
                            break;
                        case SEARCHING:
                            list.add(buildSearchingModel());
                            break;
                        case PICKING:
                            list.add(buildAddDriverModel(true));
                            int i2 = 1;
                            java.util.List<com.navdy.service.library.device.NavdyDeviceId> devices = knownDevices();
                            com.navdy.service.library.device.NavdyDeviceId connectedDevice = this.connectionHandler.getConnectedDevice();
                            if (devices.size() <= 0) {
                                i = 0;
                            }
                            this.currentItem = i;
                            for (com.navdy.service.library.device.NavdyDeviceId deviceId : devices) {
                                int title = com.navdy.hud.app.R.string.welcome_who_is_driving;
                                int message = 0;
                                if (deviceId.equals(this.failedConnection)) {
                                    this.currentItem = i2;
                                    title = com.navdy.hud.app.R.string.welcome_cant_connect;
                                    message = com.navdy.hud.app.R.string.welcome_bluetooth_disabled;
                                } else if (deviceId.equals(connectedDevice)) {
                                    title = com.navdy.hud.app.R.string.welcome_current_driver;
                                }
                                list.add(buildDriverModel(deviceId, i2, title, message));
                                i2++;
                            }
                            break;
                    }
                    view.carousel.setModel(list, this.currentItem, false);
                }
            }
        }

        private java.util.List<com.navdy.service.library.device.NavdyDeviceId> knownDevices() {
            java.util.List<com.navdy.service.library.device.connection.ConnectionInfo> infos = com.navdy.service.library.device.RemoteDeviceRegistry.getInstance(com.navdy.hud.app.HudApplication.getAppContext()).getPairedConnections();
            java.util.List<com.navdy.service.library.device.NavdyDeviceId> result = new java.util.ArrayList<>();
            for (com.navdy.service.library.device.connection.ConnectionInfo info : infos) {
                result.add(info.getDeviceId());
            }
            return result;
        }

        private com.navdy.hud.app.ui.component.carousel.Carousel.Model buildDriverModel(com.navdy.service.library.device.NavdyDeviceId deviceId, int i) {
            return buildDriverModel(deviceId, i, 0, 0);
        }

        private com.navdy.hud.app.ui.component.carousel.Carousel.Model buildDriverModel(com.navdy.service.library.device.NavdyDeviceId deviceId, int i, int titleResId) {
            return buildDriverModel(deviceId, i, titleResId, 0);
        }

        private com.navdy.hud.app.ui.component.carousel.Carousel.Model buildDriverModel(com.navdy.service.library.device.NavdyDeviceId deviceId, int i, int titleResId, int messageResId) {
            java.lang.String profileName;
            android.content.res.Resources resources = ((com.navdy.hud.app.view.WelcomeView) getView()).getResources();
            com.navdy.hud.app.ui.component.carousel.Carousel.Model driver = new com.navdy.hud.app.ui.component.carousel.Carousel.Model();
            driver.id = i;
            java.lang.String deviceName = deviceId.getDeviceName();
            int resId = com.navdy.hud.app.framework.contacts.ContactImageHelper.getInstance().getDriverImageResId(deviceName);
            driver.smallImageRes = resId;
            driver.largeImageRes = resId;
            com.navdy.hud.app.screen.WelcomeScreen.DeviceMetadata metaData = new com.navdy.hud.app.screen.WelcomeScreen.DeviceMetadata(deviceId, this.profileManager.getProfileForId(deviceId));
            driver.extras = metaData;
            driver.infoMap = new java.util.HashMap<>(4);
            driver.infoMap.put(java.lang.Integer.valueOf(com.navdy.hud.app.R.id.title), resources.getString(titleResId));
            if (metaData.driverProfile == null || android.text.TextUtils.isEmpty(metaData.driverProfile.getDriverName())) {
                profileName = deviceName;
            } else {
                profileName = metaData.driverProfile.getDriverName();
            }
            driver.infoMap.put(java.lang.Integer.valueOf(com.navdy.hud.app.R.id.subTitle), profileName);
            if (messageResId != 0) {
                driver.infoMap.put(java.lang.Integer.valueOf(com.navdy.hud.app.R.id.message), resources.getString(messageResId));
            }
            return driver;
        }

        private com.navdy.hud.app.ui.component.carousel.Carousel.Model buildSearchingModel() {
            android.content.res.Resources resources = ((com.navdy.hud.app.view.WelcomeView) getView()).getResources();
            com.navdy.hud.app.ui.component.carousel.Carousel.Model searching2 = new com.navdy.hud.app.ui.component.carousel.Carousel.Model();
            searching2.id = com.navdy.hud.app.R.id.welcome_menu_searching;
            searching2.smallImageRes = com.navdy.hud.app.R.drawable.icon_bluetooth_connecting;
            searching2.largeImageRes = com.navdy.hud.app.R.drawable.icon_bluetooth_connecting;
            searching2.infoMap = new java.util.HashMap<>(1);
            searching2.infoMap.put(java.lang.Integer.valueOf(com.navdy.hud.app.R.id.title), resources.getString(com.navdy.hud.app.R.string.welcome_looking_for_drivers));
            searching2.infoMap.put(java.lang.Integer.valueOf(com.navdy.hud.app.R.id.subTitle), "");
            return searching2;
        }

        private com.navdy.hud.app.ui.component.carousel.Carousel.Model buildAddDriverModel(boolean driversFound) {
            android.content.res.Resources resources = ((com.navdy.hud.app.view.WelcomeView) getView()).getResources();
            int title = driversFound ? com.navdy.hud.app.R.string.welcome_who_is_driving : com.navdy.hud.app.R.string.welcome_no_drivers_found;
            com.navdy.hud.app.ui.component.carousel.Carousel.Model addDriver = new com.navdy.hud.app.ui.component.carousel.Carousel.Model();
            addDriver.id = com.navdy.hud.app.R.id.welcome_menu_add_driver;
            addDriver.smallImageRes = com.navdy.hud.app.R.drawable.icon_add_driver;
            addDriver.largeImageRes = com.navdy.hud.app.R.drawable.icon_add_driver;
            addDriver.infoMap = new java.util.HashMap<>(1);
            addDriver.infoMap.put(java.lang.Integer.valueOf(com.navdy.hud.app.R.id.title), resources.getString(title));
            addDriver.infoMap.put(java.lang.Integer.valueOf(com.navdy.hud.app.R.id.subTitle), resources.getString(com.navdy.hud.app.R.string.welcome_add_driver));
            return addDriver;
        }

        public void onCurrentItemChanged(int pos, int id) {
            com.navdy.hud.app.screen.WelcomeScreen.logger.i("onCurrentItemChanged id:" + id + " pos:" + pos);
            this.currentItem = pos;
        }

        private void selectDevice(com.navdy.service.library.device.NavdyDeviceId deviceId) {
            com.navdy.hud.app.screen.WelcomeScreen.logger.i("Trying to select " + deviceId);
            com.navdy.service.library.device.RemoteDevice currentDevice = this.connectionHandler.getRemoteDevice();
            if (currentDevice == null || !currentDevice.getDeviceId().equals(deviceId)) {
                setState(com.navdy.hud.app.screen.WelcomeScreen.State.CONNECTING);
                this.handler.post(new com.navdy.hud.app.screen.WelcomeScreen.Presenter.Anon7(deviceId));
            } else {
                setState(com.navdy.hud.app.screen.WelcomeScreen.State.WELCOME);
            }
            setDevice(deviceId);
            updateView();
        }

        public void executeItem(int id, int pos) {
            com.navdy.hud.app.screen.WelcomeScreen.logger.i("execute: id:" + id + " pos:" + pos);
            if (id != com.navdy.hud.app.R.id.welcome_menu_searching && id != com.navdy.hud.app.R.id.welcome_menu_connecting) {
                if (id == com.navdy.hud.app.R.id.welcome_menu_add_driver) {
                    setState(com.navdy.hud.app.screen.WelcomeScreen.State.DOWNLOAD_APP);
                    return;
                }
                com.navdy.hud.app.view.WelcomeView view = (com.navdy.hud.app.view.WelcomeView) getView();
                if (view != null) {
                    try {
                        selectDevice(((com.navdy.hud.app.screen.WelcomeScreen.DeviceMetadata) view.carousel.getModel(pos).extras).deviceId);
                    } catch (java.lang.Exception e) {
                        com.navdy.hud.app.screen.WelcomeScreen.logger.e("Failed to select device at pos " + pos);
                    }
                }
            }
        }

        private void updatePairingStatusOnLED(boolean pairing) {
            if (!this.ledUpdated && pairing) {
                com.navdy.hud.app.device.light.HUDLightUtils.showPairing(com.navdy.hud.app.HudApplication.getAppContext(), com.navdy.hud.app.device.light.LightManager.getInstance(), true);
                this.ledUpdated = true;
            } else if (this.ledUpdated) {
                com.navdy.hud.app.device.light.HUDLightUtils.showPairing(com.navdy.hud.app.HudApplication.getAppContext(), com.navdy.hud.app.device.light.LightManager.getInstance(), false);
                this.ledUpdated = false;
            }
        }
    }

    enum State {
        UNKNOWN,
        DOWNLOAD_APP,
        SEARCHING,
        PICKING,
        CONNECTING,
        LAUNCHING_APP,
        CONNECTION_FAILED,
        WELCOME
    }

    public java.lang.String getMortarScopeName() {
        return getClass().getName();
    }

    public java.lang.Object getDaggerModule() {
        return new com.navdy.hud.app.screen.WelcomeScreen.Module();
    }

    public com.navdy.service.library.events.ui.Screen getScreen() {
        return com.navdy.service.library.events.ui.Screen.SCREEN_WELCOME;
    }

    public int getAnimationIn(flow.Flow.Direction direction) {
        return 17432576;
    }

    public int getAnimationOut(flow.Flow.Direction direction) {
        return 17432577;
    }
}
