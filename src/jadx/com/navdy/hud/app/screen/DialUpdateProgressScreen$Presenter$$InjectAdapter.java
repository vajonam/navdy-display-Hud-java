package com.navdy.hud.app.screen;

public final class DialUpdateProgressScreen$Presenter$$InjectAdapter extends dagger.internal.Binding<com.navdy.hud.app.screen.DialUpdateProgressScreen.Presenter> implements javax.inject.Provider<com.navdy.hud.app.screen.DialUpdateProgressScreen.Presenter>, dagger.MembersInjector<com.navdy.hud.app.screen.DialUpdateProgressScreen.Presenter> {
    private dagger.internal.Binding<com.squareup.otto.Bus> mBus;
    private dagger.internal.Binding<android.content.SharedPreferences> mPreferences;
    private dagger.internal.Binding<com.navdy.hud.app.screen.DialUpdateProgressScreen> mScreen;
    private dagger.internal.Binding<com.navdy.hud.app.ui.framework.BasePresenter> supertype;

    public DialUpdateProgressScreen$Presenter$$InjectAdapter() {
        super("com.navdy.hud.app.screen.DialUpdateProgressScreen$Presenter", "members/com.navdy.hud.app.screen.DialUpdateProgressScreen$Presenter", true, com.navdy.hud.app.screen.DialUpdateProgressScreen.Presenter.class);
    }

    public void attach(dagger.internal.Linker linker) {
        this.mBus = linker.requestBinding("com.squareup.otto.Bus", com.navdy.hud.app.screen.DialUpdateProgressScreen.Presenter.class, getClass().getClassLoader());
        this.mScreen = linker.requestBinding("com.navdy.hud.app.screen.DialUpdateProgressScreen", com.navdy.hud.app.screen.DialUpdateProgressScreen.Presenter.class, getClass().getClassLoader());
        this.mPreferences = linker.requestBinding("android.content.SharedPreferences", com.navdy.hud.app.screen.DialUpdateProgressScreen.Presenter.class, getClass().getClassLoader());
        dagger.internal.Linker linker2 = linker;
        this.supertype = linker2.requestBinding("members/com.navdy.hud.app.ui.framework.BasePresenter", com.navdy.hud.app.screen.DialUpdateProgressScreen.Presenter.class, getClass().getClassLoader(), false, true);
    }

    public void getDependencies(java.util.Set<dagger.internal.Binding<?>> set, java.util.Set<dagger.internal.Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.mBus);
        injectMembersBindings.add(this.mScreen);
        injectMembersBindings.add(this.mPreferences);
        injectMembersBindings.add(this.supertype);
    }

    public com.navdy.hud.app.screen.DialUpdateProgressScreen.Presenter get() {
        com.navdy.hud.app.screen.DialUpdateProgressScreen.Presenter result = new com.navdy.hud.app.screen.DialUpdateProgressScreen.Presenter();
        injectMembers(result);
        return result;
    }

    public void injectMembers(com.navdy.hud.app.screen.DialUpdateProgressScreen.Presenter object) {
        object.mBus = (com.squareup.otto.Bus) this.mBus.get();
        object.mScreen = (com.navdy.hud.app.screen.DialUpdateProgressScreen) this.mScreen.get();
        object.mPreferences = (android.content.SharedPreferences) this.mPreferences.get();
        this.supertype.injectMembers(object);
    }
}
