package com.navdy.hud.app.screen;

@flow.Layout(2130903133)
public class GestureLearningScreen extends com.navdy.hud.app.screen.BaseScreen {
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.screen.GestureLearningScreen.class);

    @dagger.Module(addsTo = com.navdy.hud.app.ui.activity.Main.Module.class, injects = {com.navdy.hud.app.view.LearnGestureScreenLayout.class, com.navdy.hud.app.view.GestureLearningView.class, com.navdy.hud.app.view.ScrollableTextPresenterLayout.class, com.navdy.hud.app.view.GestureVideoCaptureView.class})
    public class Module {
        public Module() {
        }
    }

    @javax.inject.Singleton
    public static class Presenter extends com.navdy.hud.app.ui.framework.BasePresenter<com.navdy.hud.app.view.LearnGestureScreenLayout> {
        @javax.inject.Inject
        com.navdy.hud.app.gesture.GestureServiceConnector gestureServiceConnector;
        @javax.inject.Inject
        com.squareup.otto.Bus mBus;
        @javax.inject.Inject
        android.content.SharedPreferences mPreferences;
        android.os.Bundle tipsBundle;
        @javax.inject.Inject
        com.navdy.hud.app.ui.framework.UIStateManager uiStateManager;

        public void onLoad(android.os.Bundle savedInstanceState) {
            com.navdy.hud.app.screen.GestureLearningScreen.sLogger.v("onLoad");
            super.onLoad(savedInstanceState);
            this.uiStateManager.enableSystemTray(false);
            com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().enableNotifications(false);
            com.navdy.hud.app.framework.toast.ToastManager.getInstance().disableToasts(true);
        }

        /* access modifiers changed from: protected */
        public void onUnload() {
            com.navdy.hud.app.screen.GestureLearningScreen.sLogger.v("onUnload");
            this.uiStateManager.enableSystemTray(true);
            com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().enableNotifications(true);
            com.navdy.hud.app.framework.toast.ToastManager.getInstance().disableToasts(false);
            super.onUnload();
        }

        public void showTips() {
            com.navdy.hud.app.view.LearnGestureScreenLayout view = (com.navdy.hud.app.view.LearnGestureScreenLayout) getView();
            if (view != null) {
                view.showTips();
            }
        }

        public void hideTips() {
            com.navdy.hud.app.view.LearnGestureScreenLayout view = (com.navdy.hud.app.view.LearnGestureScreenLayout) getView();
            if (view != null) {
                view.hideTips();
            }
        }

        public void showCameraSensorBlocked() {
            com.navdy.hud.app.view.LearnGestureScreenLayout view = (com.navdy.hud.app.view.LearnGestureScreenLayout) getView();
            if (view != null) {
                view.showSensorBlocked();
            }
        }

        public void hideCameraSensorBlocked() {
            com.navdy.hud.app.view.LearnGestureScreenLayout view = (com.navdy.hud.app.view.LearnGestureScreenLayout) getView();
            if (view != null) {
                view.hideSensorBlocked();
            }
        }

        public void showCaptureView() {
            com.navdy.hud.app.view.LearnGestureScreenLayout view = (com.navdy.hud.app.view.LearnGestureScreenLayout) getView();
            if (view != null) {
                view.showCaptureGestureVideosView();
            }
        }

        public void hideCaptureView() {
            com.navdy.hud.app.view.LearnGestureScreenLayout view = (com.navdy.hud.app.view.LearnGestureScreenLayout) getView();
            if (view != null) {
                view.hideCaptureGestureVideosView();
            }
        }

        public void finish() {
            this.mBus.post(new com.navdy.service.library.events.ui.ShowScreen.Builder().screen(com.navdy.service.library.events.ui.Screen.SCREEN_BACK).build());
        }
    }

    public com.navdy.service.library.events.ui.Screen getScreen() {
        return com.navdy.service.library.events.ui.Screen.SCREEN_GESTURE_LEARNING;
    }

    public java.lang.String getMortarScopeName() {
        return com.navdy.hud.app.screen.GestureLearningScreen.class.getSimpleName();
    }

    public java.lang.Object getDaggerModule() {
        return new com.navdy.hud.app.screen.GestureLearningScreen.Module();
    }
}
