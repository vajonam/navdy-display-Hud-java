package com.navdy.hud.app.screen;

@flow.Layout(2130903138)
public class OSUpdateConfirmationScreen extends com.navdy.hud.app.screen.BaseScreen {
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.screen.OSUpdateConfirmationScreen.class);

    @dagger.Module(addsTo = com.navdy.hud.app.ui.activity.Main.Module.class, injects = {com.navdy.hud.app.view.UpdateConfirmationView.class})
    public class Module {
        public Module() {
        }
    }

    @javax.inject.Singleton
    public static class Presenter extends com.navdy.hud.app.ui.framework.BasePresenter<com.navdy.hud.app.view.UpdateConfirmationView> {
        private boolean isReminder;
        @javax.inject.Inject
        com.squareup.otto.Bus mBus;
        @javax.inject.Inject
        android.content.SharedPreferences mPreferences;

        public void onLoad(android.os.Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);
            if (savedInstanceState != null) {
                this.isReminder = savedInstanceState.getBoolean(com.navdy.hud.app.manager.UpdateReminderManager.EXTRA_UPDATE_REMINDER, false);
                return;
            }
            com.navdy.hud.app.screen.OSUpdateConfirmationScreen.sLogger.e("null bundle passed to onLoad()");
            this.isReminder = false;
        }

        public void install() {
            android.content.Intent intent = new android.content.Intent(com.navdy.hud.app.HudApplication.getAppContext(), com.navdy.hud.app.util.OTAUpdateService.class);
            intent.putExtra("COMMAND", com.navdy.hud.app.util.OTAUpdateService.COMMAND_INSTALL_UPDATE);
            com.navdy.hud.app.HudApplication.getAppContext().startService(intent);
        }

        public void doNotPrompt() {
            android.content.Intent intent = new android.content.Intent(com.navdy.hud.app.HudApplication.getAppContext(), com.navdy.hud.app.util.OTAUpdateService.class);
            intent.putExtra("COMMAND", com.navdy.hud.app.util.OTAUpdateService.COMMAND_DO_NOT_PROMPT);
            com.navdy.hud.app.HudApplication.getAppContext().startService(intent);
        }

        public void finish() {
            this.mBus.post(new com.navdy.service.library.events.ui.ShowScreen.Builder().screen(com.navdy.service.library.events.ui.Screen.SCREEN_BACK).build());
        }

        public java.lang.String getUpdateVersion() {
            return com.navdy.hud.app.util.OTAUpdateService.getSWUpdateVersion();
        }

        public java.lang.String getCurrentVersion() {
            return com.navdy.hud.app.util.OTAUpdateService.getCurrentVersion();
        }

        public boolean isReminder() {
            return this.isReminder;
        }
    }

    public java.lang.String getMortarScopeName() {
        return com.navdy.hud.app.screen.OSUpdateConfirmationScreen.class.getName();
    }

    public java.lang.Object getDaggerModule() {
        return new com.navdy.hud.app.screen.OSUpdateConfirmationScreen.Module();
    }

    public com.navdy.service.library.events.ui.Screen getScreen() {
        return com.navdy.service.library.events.ui.Screen.SCREEN_OTA_CONFIRMATION;
    }
}
