package com.navdy.hud.app.screen;

public final class DialUpdateConfirmationScreen$Presenter$$InjectAdapter extends dagger.internal.Binding<com.navdy.hud.app.screen.DialUpdateConfirmationScreen.Presenter> implements javax.inject.Provider<com.navdy.hud.app.screen.DialUpdateConfirmationScreen.Presenter>, dagger.MembersInjector<com.navdy.hud.app.screen.DialUpdateConfirmationScreen.Presenter> {
    private dagger.internal.Binding<com.squareup.otto.Bus> mBus;
    private dagger.internal.Binding<com.navdy.hud.app.ui.framework.BasePresenter> supertype;

    public DialUpdateConfirmationScreen$Presenter$$InjectAdapter() {
        super("com.navdy.hud.app.screen.DialUpdateConfirmationScreen$Presenter", "members/com.navdy.hud.app.screen.DialUpdateConfirmationScreen$Presenter", true, com.navdy.hud.app.screen.DialUpdateConfirmationScreen.Presenter.class);
    }

    public void attach(dagger.internal.Linker linker) {
        this.mBus = linker.requestBinding("com.squareup.otto.Bus", com.navdy.hud.app.screen.DialUpdateConfirmationScreen.Presenter.class, getClass().getClassLoader());
        dagger.internal.Linker linker2 = linker;
        this.supertype = linker2.requestBinding("members/com.navdy.hud.app.ui.framework.BasePresenter", com.navdy.hud.app.screen.DialUpdateConfirmationScreen.Presenter.class, getClass().getClassLoader(), false, true);
    }

    public void getDependencies(java.util.Set<dagger.internal.Binding<?>> set, java.util.Set<dagger.internal.Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.mBus);
        injectMembersBindings.add(this.supertype);
    }

    public com.navdy.hud.app.screen.DialUpdateConfirmationScreen.Presenter get() {
        com.navdy.hud.app.screen.DialUpdateConfirmationScreen.Presenter result = new com.navdy.hud.app.screen.DialUpdateConfirmationScreen.Presenter();
        injectMembers(result);
        return result;
    }

    public void injectMembers(com.navdy.hud.app.screen.DialUpdateConfirmationScreen.Presenter object) {
        object.mBus = (com.squareup.otto.Bus) this.mBus.get();
        this.supertype.injectMembers(object);
    }
}
