package com.navdy.hud.app.screen;

public final class OSUpdateConfirmationScreen$Module$$ModuleAdapter extends dagger.internal.ModuleAdapter<com.navdy.hud.app.screen.OSUpdateConfirmationScreen.Module> {
    private static final java.lang.Class<?>[] INCLUDES = new java.lang.Class[0];
    private static final java.lang.String[] INJECTS = {"members/com.navdy.hud.app.view.UpdateConfirmationView"};
    private static final java.lang.Class<?>[] STATIC_INJECTIONS = new java.lang.Class[0];

    public OSUpdateConfirmationScreen$Module$$ModuleAdapter() {
        super(com.navdy.hud.app.screen.OSUpdateConfirmationScreen.Module.class, INJECTS, STATIC_INJECTIONS, false, INCLUDES, true, false);
    }
}
