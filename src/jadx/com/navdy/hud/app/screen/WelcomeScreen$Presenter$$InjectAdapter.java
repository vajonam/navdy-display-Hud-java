package com.navdy.hud.app.screen;

public final class WelcomeScreen$Presenter$$InjectAdapter extends dagger.internal.Binding<com.navdy.hud.app.screen.WelcomeScreen.Presenter> implements javax.inject.Provider<com.navdy.hud.app.screen.WelcomeScreen.Presenter>, dagger.MembersInjector<com.navdy.hud.app.screen.WelcomeScreen.Presenter> {
    private dagger.internal.Binding<com.squareup.otto.Bus> bus;
    private dagger.internal.Binding<com.navdy.hud.app.service.ConnectionHandler> connectionHandler;
    private dagger.internal.Binding<com.navdy.hud.app.gesture.GestureServiceConnector> gestureServiceConnector;
    private dagger.internal.Binding<com.navdy.hud.app.manager.PairingManager> pairingManager;
    private dagger.internal.Binding<com.navdy.hud.app.profile.DriverProfileManager> profileManager;
    private dagger.internal.Binding<com.navdy.hud.app.ui.framework.BasePresenter> supertype;
    private dagger.internal.Binding<com.navdy.hud.app.ui.framework.UIStateManager> uiStateManager;

    public WelcomeScreen$Presenter$$InjectAdapter() {
        super("com.navdy.hud.app.screen.WelcomeScreen$Presenter", "members/com.navdy.hud.app.screen.WelcomeScreen$Presenter", true, com.navdy.hud.app.screen.WelcomeScreen.Presenter.class);
    }

    public void attach(dagger.internal.Linker linker) {
        this.bus = linker.requestBinding("com.squareup.otto.Bus", com.navdy.hud.app.screen.WelcomeScreen.Presenter.class, getClass().getClassLoader());
        this.profileManager = linker.requestBinding("com.navdy.hud.app.profile.DriverProfileManager", com.navdy.hud.app.screen.WelcomeScreen.Presenter.class, getClass().getClassLoader());
        this.connectionHandler = linker.requestBinding("com.navdy.hud.app.service.ConnectionHandler", com.navdy.hud.app.screen.WelcomeScreen.Presenter.class, getClass().getClassLoader());
        this.uiStateManager = linker.requestBinding("com.navdy.hud.app.ui.framework.UIStateManager", com.navdy.hud.app.screen.WelcomeScreen.Presenter.class, getClass().getClassLoader());
        this.pairingManager = linker.requestBinding("com.navdy.hud.app.manager.PairingManager", com.navdy.hud.app.screen.WelcomeScreen.Presenter.class, getClass().getClassLoader());
        this.gestureServiceConnector = linker.requestBinding("com.navdy.hud.app.gesture.GestureServiceConnector", com.navdy.hud.app.screen.WelcomeScreen.Presenter.class, getClass().getClassLoader());
        dagger.internal.Linker linker2 = linker;
        this.supertype = linker2.requestBinding("members/com.navdy.hud.app.ui.framework.BasePresenter", com.navdy.hud.app.screen.WelcomeScreen.Presenter.class, getClass().getClassLoader(), false, true);
    }

    public void getDependencies(java.util.Set<dagger.internal.Binding<?>> set, java.util.Set<dagger.internal.Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.bus);
        injectMembersBindings.add(this.profileManager);
        injectMembersBindings.add(this.connectionHandler);
        injectMembersBindings.add(this.uiStateManager);
        injectMembersBindings.add(this.pairingManager);
        injectMembersBindings.add(this.gestureServiceConnector);
        injectMembersBindings.add(this.supertype);
    }

    public com.navdy.hud.app.screen.WelcomeScreen.Presenter get() {
        com.navdy.hud.app.screen.WelcomeScreen.Presenter result = new com.navdy.hud.app.screen.WelcomeScreen.Presenter();
        injectMembers(result);
        return result;
    }

    public void injectMembers(com.navdy.hud.app.screen.WelcomeScreen.Presenter object) {
        object.bus = (com.squareup.otto.Bus) this.bus.get();
        object.profileManager = (com.navdy.hud.app.profile.DriverProfileManager) this.profileManager.get();
        object.connectionHandler = (com.navdy.hud.app.service.ConnectionHandler) this.connectionHandler.get();
        object.uiStateManager = (com.navdy.hud.app.ui.framework.UIStateManager) this.uiStateManager.get();
        object.pairingManager = (com.navdy.hud.app.manager.PairingManager) this.pairingManager.get();
        object.gestureServiceConnector = (com.navdy.hud.app.gesture.GestureServiceConnector) this.gestureServiceConnector.get();
        this.supertype.injectMembers(object);
    }
}
