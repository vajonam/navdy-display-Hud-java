package com.navdy.hud.app.screen;

public final class DialUpdateProgressScreen$Module$$ModuleAdapter extends dagger.internal.ModuleAdapter<com.navdy.hud.app.screen.DialUpdateProgressScreen.Module> {
    private static final java.lang.Class<?>[] INCLUDES = new java.lang.Class[0];
    private static final java.lang.String[] INJECTS = {"members/com.navdy.hud.app.view.DialUpdateProgressView"};
    private static final java.lang.Class<?>[] STATIC_INJECTIONS = new java.lang.Class[0];

    /* compiled from: DialUpdateProgressScreen$Module$$ModuleAdapter */
    public static final class ProvideScreenProvidesAdapter extends dagger.internal.ProvidesBinding<com.navdy.hud.app.screen.DialUpdateProgressScreen> implements javax.inject.Provider<com.navdy.hud.app.screen.DialUpdateProgressScreen> {
        private final com.navdy.hud.app.screen.DialUpdateProgressScreen.Module module;

        public ProvideScreenProvidesAdapter(com.navdy.hud.app.screen.DialUpdateProgressScreen.Module module2) {
            super("com.navdy.hud.app.screen.DialUpdateProgressScreen", false, "com.navdy.hud.app.screen.DialUpdateProgressScreen.Module", "provideScreen");
            this.module = module2;
            setLibrary(false);
        }

        public com.navdy.hud.app.screen.DialUpdateProgressScreen get() {
            return this.module.provideScreen();
        }
    }

    public DialUpdateProgressScreen$Module$$ModuleAdapter() {
        super(com.navdy.hud.app.screen.DialUpdateProgressScreen.Module.class, INJECTS, STATIC_INJECTIONS, false, INCLUDES, true, false);
    }

    public void getBindings(dagger.internal.BindingsGroup bindings, com.navdy.hud.app.screen.DialUpdateProgressScreen.Module module) {
        bindings.contributeProvidesBinding("com.navdy.hud.app.screen.DialUpdateProgressScreen", new com.navdy.hud.app.screen.DialUpdateProgressScreen$Module$$ModuleAdapter.ProvideScreenProvidesAdapter(module));
    }
}
