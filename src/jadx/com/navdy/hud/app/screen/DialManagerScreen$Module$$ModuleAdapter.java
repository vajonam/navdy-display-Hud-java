package com.navdy.hud.app.screen;

public final class DialManagerScreen$Module$$ModuleAdapter extends dagger.internal.ModuleAdapter<com.navdy.hud.app.screen.DialManagerScreen.Module> {
    private static final java.lang.Class<?>[] INCLUDES = new java.lang.Class[0];
    private static final java.lang.String[] INJECTS = {"members/com.navdy.hud.app.view.DialManagerView"};
    private static final java.lang.Class<?>[] STATIC_INJECTIONS = new java.lang.Class[0];

    public DialManagerScreen$Module$$ModuleAdapter() {
        super(com.navdy.hud.app.screen.DialManagerScreen.Module.class, INJECTS, STATIC_INJECTIONS, false, INCLUDES, true, false);
    }
}
