package com.navdy.hud.app.screen;

public abstract class BaseScreen implements mortar.Blueprint {
    public static final int ANIMATION_NONE = -1;
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.screen.BaseScreen.class);
    protected android.os.Bundle arguments;
    /* access modifiers changed from: protected */
    public java.lang.Object arguments2;

    public abstract com.navdy.service.library.events.ui.Screen getScreen();

    public int getAnimationIn(flow.Flow.Direction direction) {
        return direction == flow.Flow.Direction.FORWARD ? com.navdy.hud.app.R.anim.slide_in_down : com.navdy.hud.app.R.anim.slide_in_up;
    }

    public int getAnimationOut(flow.Flow.Direction direction) {
        return direction == flow.Flow.Direction.FORWARD ? com.navdy.hud.app.R.anim.slide_out_down : com.navdy.hud.app.R.anim.slide_out_up;
    }

    public android.os.Bundle getArguments() {
        return this.arguments;
    }

    public java.lang.Object getArguments2() {
        return this.arguments2;
    }

    public void setArguments(android.os.Bundle bundle, java.lang.Object arg) {
        this.arguments = bundle;
        this.arguments2 = arg;
        updateInstanceState();
    }

    public void onAnimationInStart() {
    }

    public void onAnimationInEnd() {
    }

    public void onAnimationOutStart() {
    }

    public void onAnimationOutEnd() {
    }

    private void updateInstanceState() {
        mortar.MortarScope scope = mortar.Mortar.getScope(com.navdy.hud.app.HudApplication.getAppContext()).findChild(com.navdy.hud.app.ui.activity.Main.class.getName());
        if (scope != null) {
            mortar.MortarScope screenScope = scope.requireChild(this);
            android.os.Bundle latestState = getInstanceState(screenScope);
            if (latestState == null) {
                latestState = new android.os.Bundle();
                setInstanceState(screenScope, latestState);
            }
            latestState.putBundle(getMortarScopeName() + "$Presenter", this.arguments);
        }
    }

    private android.os.Bundle getInstanceState(mortar.MortarScope scope) {
        try {
            java.lang.reflect.Field field = scope.getClass().getDeclaredField("latestSavedInstanceState");
            field.setAccessible(true);
            return (android.os.Bundle) field.get(scope);
        } catch (Throwable e) {
            sLogger.e("failed to get screen instance state", e);
            return null;
        }
    }

    private void setInstanceState(mortar.MortarScope scope, android.os.Bundle bundle) {
        try {
            java.lang.reflect.Field field = scope.getClass().getDeclaredField("latestSavedInstanceState");
            field.setAccessible(true);
            field.set(scope, bundle);
        } catch (Throwable e) {
            sLogger.e("failed to set screen instance state", e);
        }
    }
}
