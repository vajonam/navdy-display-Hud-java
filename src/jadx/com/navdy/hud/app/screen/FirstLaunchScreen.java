package com.navdy.hud.app.screen;

@flow.Layout(2130903116)
public class FirstLaunchScreen extends com.navdy.hud.app.screen.BaseScreen {
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.screen.FirstLaunchScreen.class);

    private static class MediaServerUp {
        private MediaServerUp() {
        }
    }

    @dagger.Module(addsTo = com.navdy.hud.app.ui.activity.Main.Module.class, injects = {com.navdy.hud.app.view.FirstLaunchView.class})
    public class Module {
        public Module() {
        }
    }

    @javax.inject.Singleton
    public static class Presenter extends com.navdy.hud.app.ui.framework.BasePresenter<com.navdy.hud.app.view.FirstLaunchView> {
        @javax.inject.Inject
        com.squareup.otto.Bus bus;
        private com.navdy.hud.app.device.dial.DialManager dialManager;
        @javax.inject.Inject
        com.navdy.hud.app.device.PowerManager powerManager;
        private boolean registered;
        private boolean stateChecked;
        @javax.inject.Inject
        com.navdy.hud.app.ui.framework.UIStateManager uiStateManager;

        class Anon1 implements java.lang.Runnable {
            Anon1() {
            }

            public void run() {
                com.navdy.hud.app.screen.FirstLaunchScreen.Presenter.this.checkState();
            }
        }

        class Anon2 implements java.lang.Runnable {
            Anon2() {
            }

            /* JADX WARNING: Removed duplicated region for block: B:16:0x0064 A[SYNTHETIC, Splitter:B:16:0x0064] */
            /* JADX WARNING: Removed duplicated region for block: B:27:0x0067 A[SYNTHETIC] */
            public void run() {
                int counter = 1;
                while (true) {
                    android.media.MediaPlayer mediaPlayer = null;
                    try {
                        long l1 = android.os.SystemClock.elapsedRealtime();
                        int counter2 = counter + 1;
                        try {
                            com.navdy.hud.app.screen.FirstLaunchScreen.sLogger.v("creating media player counter=" + counter);
                            android.media.MediaPlayer mediaPlayer2 = new android.media.MediaPlayer();
                            try {
                                mediaPlayer2.release();
                                mediaPlayer = null;
                                com.navdy.hud.app.screen.FirstLaunchScreen.sLogger.v("created media player time=" + (android.os.SystemClock.elapsedRealtime() - l1));
                                com.navdy.hud.app.screen.FirstLaunchScreen.Presenter.this.bus.post(new com.navdy.hud.app.screen.FirstLaunchScreen.MediaServerUp());
                                break;
                            } catch (Throwable th) {
                                t = th;
                                mediaPlayer = mediaPlayer2;
                                counter = counter2;
                                com.navdy.hud.app.screen.FirstLaunchScreen.sLogger.e("checkForMediaService", t);
                                if (mediaPlayer == null) {
                                    try {
                                        mediaPlayer.release();
                                    } catch (Throwable t1) {
                                        com.navdy.hud.app.screen.FirstLaunchScreen.sLogger.e(t1);
                                    }
                                }
                                com.navdy.hud.app.util.GenericUtil.sleep(1000);
                            }
                        } catch (Throwable th2) {
                            t = th2;
                            counter = counter2;
                        }
                    } catch (Throwable th3) {
                        t = th3;
                        com.navdy.hud.app.screen.FirstLaunchScreen.sLogger.e("checkForMediaService", t);
                        if (mediaPlayer == null) {
                        }
                        com.navdy.hud.app.util.GenericUtil.sleep(1000);
                    }
                    com.navdy.hud.app.util.GenericUtil.sleep(1000);
                }
            }
        }

        public void onLoad(android.os.Bundle savedInstanceState) {
            com.navdy.hud.app.screen.FirstLaunchScreen.sLogger.v("onLoad");
            this.bus.register(this);
            this.registered = true;
            this.dialManager = com.navdy.hud.app.device.dial.DialManager.getInstance();
            if (this.dialManager.isInitialized()) {
                com.navdy.hud.app.screen.FirstLaunchScreen.sLogger.v("already inititalized");
                checkState();
            } else {
                com.navdy.hud.app.screen.FirstLaunchScreen.sLogger.v("show view");
            }
            this.uiStateManager.enableSystemTray(false);
            this.uiStateManager.enableNotificationColor(false);
            com.navdy.hud.app.screen.FirstLaunchScreen.sLogger.v("systemtray:invisible");
        }

        /* access modifiers changed from: protected */
        public void onUnload() {
            com.navdy.hud.app.screen.FirstLaunchScreen.sLogger.v("onUnload");
            if (this.registered) {
                this.bus.unregister(this);
            }
            super.onUnload();
            this.uiStateManager.enableSystemTray(true);
            this.uiStateManager.enableNotificationColor(true);
            com.navdy.hud.app.screen.FirstLaunchScreen.sLogger.v("systemtray:visible");
        }

        @com.squareup.otto.Subscribe
        public void onDialManagerInitEvent(com.navdy.hud.app.device.dial.DialConstants.DialManagerInitEvent event) {
            com.navdy.hud.app.screen.FirstLaunchScreen.sLogger.v("got dialmanager init event");
            if (this.registered) {
                com.navdy.hud.app.view.FirstLaunchView view = (com.navdy.hud.app.view.FirstLaunchView) getView();
                if (view != null) {
                    view.firstLaunchLogo.animate().translationY((float) com.navdy.hud.app.framework.toast.ToastPresenter.ANIMATION_TRANSLATION).alpha(0.0f).withEndAction(new com.navdy.hud.app.screen.FirstLaunchScreen.Presenter.Anon1()).start();
                } else {
                    checkState();
                }
            } else {
                checkState();
            }
        }

        @com.squareup.otto.Subscribe
        public void onMediaServerUp(com.navdy.hud.app.screen.FirstLaunchScreen.MediaServerUp event) {
            com.navdy.hud.app.screen.FirstLaunchScreen.sLogger.v("media service is up, stop boot-animation");
            launchDialPairing();
        }

        /* access modifiers changed from: private */
        public void checkState() {
            if (this.stateChecked) {
                com.navdy.hud.app.screen.FirstLaunchScreen.sLogger.v("state already checked");
                return;
            }
            this.stateChecked = true;
            if (this.powerManager.inQuietMode()) {
                com.navdy.hud.app.util.os.SystemProperties.set("service.bootanim.exit", com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE);
                return;
            }
            int n = this.dialManager.getBondedDialCount();
            if (n > 0) {
                com.navdy.hud.app.screen.FirstLaunchScreen.sLogger.v("bonded dial count=" + n + " , go to next screen, stop boot animation");
                com.navdy.hud.app.util.os.SystemProperties.set("service.bootanim.exit", com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE);
                exitScreen();
                return;
            }
            com.navdy.hud.app.screen.FirstLaunchScreen.sLogger.v("checkForMediaService");
            checkForMediaService();
        }

        private void launchDialPairing() {
            com.navdy.hud.app.util.os.SystemProperties.set("service.bootanim.exit", com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE);
            com.navdy.hud.app.screen.FirstLaunchScreen.sLogger.v("bonded dial count=" + this.dialManager.getBondedDialCount() + " , go to dial pairing screen");
            this.bus.post(new com.navdy.service.library.events.ui.ShowScreen.Builder().screen(com.navdy.service.library.events.ui.Screen.SCREEN_DIAL_PAIRING).build());
        }

        private void exitScreen() {
            if (!com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().isRemoteDeviceConnected()) {
                com.navdy.hud.app.screen.FirstLaunchScreen.sLogger.v("go to welcome screen");
                android.os.Bundle args = new android.os.Bundle();
                args.putString(com.navdy.hud.app.screen.WelcomeScreen.ARG_ACTION, com.navdy.hud.app.screen.WelcomeScreen.ACTION_RECONNECT);
                this.bus.post(new com.navdy.hud.app.event.ShowScreenWithArgs(com.navdy.service.library.events.ui.Screen.SCREEN_WELCOME, args, false));
                return;
            }
            com.navdy.hud.app.screen.FirstLaunchScreen.sLogger.v("go to default screen");
            this.bus.post(new com.navdy.service.library.events.ui.ShowScreen.Builder().screen(com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getUiStateManager().getDefaultMainActiveScreen()).build());
        }

        private void checkForMediaService() {
            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.screen.FirstLaunchScreen.Presenter.Anon2(), 1);
        }
    }

    public com.navdy.service.library.events.ui.Screen getScreen() {
        return com.navdy.service.library.events.ui.Screen.SCREEN_FIRST_LAUNCH;
    }

    public java.lang.String getMortarScopeName() {
        return getClass().getName();
    }

    public java.lang.Object getDaggerModule() {
        return new com.navdy.hud.app.screen.FirstLaunchScreen.Module();
    }

    public int getAnimationIn(flow.Flow.Direction direction) {
        return -1;
    }

    public int getAnimationOut(flow.Flow.Direction direction) {
        return -1;
    }
}
