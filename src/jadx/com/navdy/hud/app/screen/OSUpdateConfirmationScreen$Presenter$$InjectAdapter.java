package com.navdy.hud.app.screen;

public final class OSUpdateConfirmationScreen$Presenter$$InjectAdapter extends dagger.internal.Binding<com.navdy.hud.app.screen.OSUpdateConfirmationScreen.Presenter> implements javax.inject.Provider<com.navdy.hud.app.screen.OSUpdateConfirmationScreen.Presenter>, dagger.MembersInjector<com.navdy.hud.app.screen.OSUpdateConfirmationScreen.Presenter> {
    private dagger.internal.Binding<com.squareup.otto.Bus> mBus;
    private dagger.internal.Binding<android.content.SharedPreferences> mPreferences;
    private dagger.internal.Binding<com.navdy.hud.app.ui.framework.BasePresenter> supertype;

    public OSUpdateConfirmationScreen$Presenter$$InjectAdapter() {
        super("com.navdy.hud.app.screen.OSUpdateConfirmationScreen$Presenter", "members/com.navdy.hud.app.screen.OSUpdateConfirmationScreen$Presenter", true, com.navdy.hud.app.screen.OSUpdateConfirmationScreen.Presenter.class);
    }

    public void attach(dagger.internal.Linker linker) {
        this.mBus = linker.requestBinding("com.squareup.otto.Bus", com.navdy.hud.app.screen.OSUpdateConfirmationScreen.Presenter.class, getClass().getClassLoader());
        this.mPreferences = linker.requestBinding("android.content.SharedPreferences", com.navdy.hud.app.screen.OSUpdateConfirmationScreen.Presenter.class, getClass().getClassLoader());
        dagger.internal.Linker linker2 = linker;
        this.supertype = linker2.requestBinding("members/com.navdy.hud.app.ui.framework.BasePresenter", com.navdy.hud.app.screen.OSUpdateConfirmationScreen.Presenter.class, getClass().getClassLoader(), false, true);
    }

    public void getDependencies(java.util.Set<dagger.internal.Binding<?>> set, java.util.Set<dagger.internal.Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.mBus);
        injectMembersBindings.add(this.mPreferences);
        injectMembersBindings.add(this.supertype);
    }

    public com.navdy.hud.app.screen.OSUpdateConfirmationScreen.Presenter get() {
        com.navdy.hud.app.screen.OSUpdateConfirmationScreen.Presenter result = new com.navdy.hud.app.screen.OSUpdateConfirmationScreen.Presenter();
        injectMembers(result);
        return result;
    }

    public void injectMembers(com.navdy.hud.app.screen.OSUpdateConfirmationScreen.Presenter object) {
        object.mBus = (com.squareup.otto.Bus) this.mBus.get();
        object.mPreferences = (android.content.SharedPreferences) this.mPreferences.get();
        this.supertype.injectMembers(object);
    }
}
