package com.navdy.hud.app.screen;

@flow.Layout(2130903144)
public class ShutDownScreen extends com.navdy.hud.app.screen.BaseScreen {
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.screen.ShutDownScreen.class);

    @dagger.Module(addsTo = com.navdy.hud.app.ui.activity.Main.Module.class, injects = {com.navdy.hud.app.view.ShutDownConfirmationView.class})
    public class Module {
        public Module() {
        }
    }

    @javax.inject.Singleton
    public static class Presenter extends com.navdy.hud.app.ui.framework.BasePresenter<com.navdy.hud.app.view.ShutDownConfirmationView> {
        @javax.inject.Inject
        com.navdy.hud.app.gesture.GestureServiceConnector gestureServiceConnector;
        @javax.inject.Inject
        com.squareup.otto.Bus mBus;
        @javax.inject.Inject
        android.content.SharedPreferences mPreferences;
        com.navdy.hud.app.obd.ObdManager obdManager;
        com.navdy.hud.app.event.Shutdown.Reason shutDownCause;
        @javax.inject.Inject
        com.navdy.hud.app.ui.framework.UIStateManager uiStateManager;

        public void onLoad(android.os.Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);
            this.obdManager = com.navdy.hud.app.obd.ObdManager.getInstance();
            this.mBus.register(this);
            if (savedInstanceState != null) {
                try {
                    java.lang.String reason = savedInstanceState.getString(com.navdy.hud.app.event.Shutdown.EXTRA_SHUTDOWN_CAUSE);
                    if (reason != null) {
                        this.shutDownCause = com.navdy.hud.app.event.Shutdown.Reason.valueOf(reason);
                    }
                } catch (java.lang.Exception e) {
                    com.navdy.hud.app.screen.ShutDownScreen.sLogger.e("failed to parse shutdown reason " + e.getMessage());
                    if (this.shutDownCause == null) {
                        com.navdy.hud.app.screen.ShutDownScreen.sLogger.e("defaulting to inactivity reason");
                        this.shutDownCause = com.navdy.hud.app.event.Shutdown.Reason.INACTIVITY;
                    }
                } catch (Throwable th) {
                    if (this.shutDownCause == null) {
                        com.navdy.hud.app.screen.ShutDownScreen.sLogger.e("defaulting to inactivity reason");
                        this.shutDownCause = com.navdy.hud.app.event.Shutdown.Reason.INACTIVITY;
                    }
                    throw th;
                }
            }
            if (this.shutDownCause == null) {
                com.navdy.hud.app.screen.ShutDownScreen.sLogger.e("defaulting to inactivity reason");
                this.shutDownCause = com.navdy.hud.app.event.Shutdown.Reason.INACTIVITY;
            }
            this.uiStateManager.enableSystemTray(false);
            this.uiStateManager.enableNotificationColor(false);
            com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().enableNotifications(false);
            com.navdy.hud.app.service.ShutdownMonitor.getInstance().disableInactivityShutdown(true);
        }

        /* access modifiers changed from: protected */
        public void onUnload() {
            com.navdy.hud.app.screen.ShutDownScreen.sLogger.v("onUnload");
            this.uiStateManager.enableSystemTray(true);
            this.uiStateManager.enableNotificationColor(true);
            com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().enableNotifications(true);
            com.navdy.hud.app.service.ShutdownMonitor.getInstance().disableInactivityShutdown(false);
            this.mBus.unregister(this);
            super.onUnload();
        }

        public void installAndShutDown() {
            java.lang.String command = com.navdy.hud.app.util.OTAUpdateService.COMMAND_INSTALL_UPDATE_SHUTDOWN;
            if (this.obdManager.getConnectionType() == com.navdy.hud.app.obd.ObdManager.ConnectionType.OBD && this.obdManager.getBatteryVoltage() < 13.100000381469727d) {
                command = com.navdy.hud.app.util.OTAUpdateService.COMMAND_INSTALL_UPDATE_REBOOT_QUIET;
            }
            android.content.Intent intent = new android.content.Intent(com.navdy.hud.app.HudApplication.getAppContext(), com.navdy.hud.app.util.OTAUpdateService.class);
            intent.putExtra("COMMAND", command);
            com.navdy.hud.app.HudApplication.getAppContext().startService(intent);
        }

        public void shutDown() {
            com.navdy.hud.app.event.Shutdown event = new com.navdy.hud.app.event.Shutdown(this.shutDownCause, com.navdy.hud.app.event.Shutdown.State.CONFIRMED);
            com.navdy.hud.app.screen.ShutDownScreen.sLogger.i("Posting " + event);
            this.mBus.post(event);
        }

        public java.lang.String getUpdateVersion() {
            return com.navdy.hud.app.util.OTAUpdateService.getSWUpdateVersion();
        }

        public java.lang.String getCurrentVersion() {
            return com.navdy.hud.app.util.OTAUpdateService.getCurrentVersion();
        }

        @com.squareup.otto.Subscribe
        public void onDriving(com.navdy.hud.app.event.DrivingStateChange event) {
            if (event.driving && getShutDownCause() == com.navdy.hud.app.event.Shutdown.Reason.INACTIVITY) {
                finish();
            }
        }

        public void finish() {
            this.mBus.post(new com.navdy.service.library.events.ui.ShowScreen.Builder().screen(com.navdy.service.library.events.ui.Screen.SCREEN_BACK).build());
            com.navdy.hud.app.event.Shutdown event = new com.navdy.hud.app.event.Shutdown(this.shutDownCause, com.navdy.hud.app.event.Shutdown.State.CANCELED);
            com.navdy.hud.app.screen.ShutDownScreen.sLogger.i("Posting " + event);
            this.mBus.post(event);
        }

        public boolean isSoftwareUpdatePending() {
            return com.navdy.hud.app.util.OTAUpdateService.isUpdateAvailable();
        }

        public boolean isDialFirmwareUpdatePending() {
            com.navdy.hud.app.device.dial.DialManager dialManager = com.navdy.hud.app.device.dial.DialManager.getInstance();
            return dialManager.isDialConnected() && dialManager.getDialFirmwareUpdater().getVersions().isUpdateAvailable();
        }

        public void installDialUpdateAndShutDown() {
            android.os.Bundle args = new android.os.Bundle();
            args.putInt(com.navdy.hud.app.screen.DialUpdateProgressScreen.EXTRA_PROGRESS_CAUSE, 2);
            this.mBus.post(new com.navdy.hud.app.event.ShowScreenWithArgs(com.navdy.service.library.events.ui.Screen.SCREEN_DIAL_UPDATE_PROGRESS, args, false));
        }

        public com.navdy.hud.app.event.Shutdown.Reason getShutDownCause() {
            return this.shutDownCause;
        }
    }

    public java.lang.String getMortarScopeName() {
        return com.navdy.hud.app.screen.ShutDownScreen.class.getName();
    }

    public java.lang.Object getDaggerModule() {
        return new com.navdy.hud.app.screen.ShutDownScreen.Module();
    }

    public com.navdy.service.library.events.ui.Screen getScreen() {
        return com.navdy.service.library.events.ui.Screen.SCREEN_SHUTDOWN_CONFIRMATION;
    }
}
