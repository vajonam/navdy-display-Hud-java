package com.navdy.hud.app.service;

public class GestureVideosSyncService extends com.navdy.hud.app.service.S3FileUploadService {
    private static final int MAX_GESTURE_VIDEOS_TO_UPLOAD = 100;
    public static final java.lang.String NAVDY_GESTURE_VIDEOS_BUCKET = "navdy-gesture-videos";
    private static java.util.concurrent.atomic.AtomicBoolean isUploading = new java.util.concurrent.atomic.AtomicBoolean(false);
    private static boolean mIsInitialized = false;
    private static com.navdy.hud.app.service.S3FileUploadService.Request sCurrentRequest;
    private static java.lang.String sGestureVideosFolder;
    private static final com.navdy.hud.app.service.S3FileUploadService.UploadQueue sGestureVideosUploadQueue = new com.navdy.hud.app.service.S3FileUploadService.UploadQueue();
    private static com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.service.GestureVideosSyncService.class);
    @javax.inject.Inject
    com.squareup.otto.Bus bus;

    public void onCreate() {
        mortar.Mortar.inject(com.navdy.hud.app.HudApplication.getAppContext(), this);
        super.onCreate();
    }

    public GestureVideosSyncService() {
        super("GESTURE_VIDEOS_SYNC");
    }

    /* access modifiers changed from: protected */
    public void initialize() {
        initializeIfNecessary();
    }

    /* access modifiers changed from: protected */
    public java.util.concurrent.atomic.AtomicBoolean getIsUploading() {
        return isUploading;
    }

    /* access modifiers changed from: protected */
    public com.navdy.hud.app.service.S3FileUploadService.Request getCurrentRequest() {
        return sCurrentRequest;
    }

    /* access modifiers changed from: protected */
    public void setCurrentRequest(com.navdy.hud.app.service.S3FileUploadService.Request currentRequest) {
        sCurrentRequest = currentRequest;
    }

    /* access modifiers changed from: protected */
    public com.navdy.hud.app.service.S3FileUploadService.UploadQueue getUploadQueue() {
        return sGestureVideosUploadQueue;
    }

    /* access modifiers changed from: protected */
    public java.lang.String getKeyPrefix(java.io.File file) {
        return "archives" + java.io.File.separator + file.getParentFile().getName();
    }

    /* access modifiers changed from: protected */
    public java.lang.String getAWSBucket() {
        return NAVDY_GESTURE_VIDEOS_BUCKET;
    }

    public static void addGestureVideoToUploadQueue(java.io.File file, java.lang.String userTag) {
        initializeIfNecessary(file);
        synchronized (sGestureVideosUploadQueue) {
            sLogger.d("Add gesture video to upload " + file.getName());
            sGestureVideosUploadQueue.add(new com.navdy.hud.app.service.S3FileUploadService.Request(file, userTag));
            sLogger.d("Queue size : " + sGestureVideosUploadQueue.size());
            if (!isUploading.get() && sGestureVideosUploadQueue.size() > 100) {
                sLogger.d("Number of videos to upload exceeded ");
                sGestureVideosUploadQueue.pop();
            }
        }
    }

    private static void initializeIfNecessary() {
        initializeIfNecessary(null);
    }

    private static void initializeIfNecessary(java.io.File fileToIgnore) {
        synchronized (sGestureVideosUploadQueue) {
            if (!mIsInitialized) {
                sLogger.d("Not initialized , initializing now");
                if (!mIsInitialized) {
                    sGestureVideosFolder = com.navdy.hud.app.storage.PathManager.getInstance().getGestureVideosSyncFolder();
                    java.io.File[] directories = new java.io.File(sGestureVideosFolder).listFiles();
                    java.util.ArrayList<java.io.File> filesToPopulate = new java.util.ArrayList<>();
                    for (java.io.File sessionDirectory : directories) {
                        sLogger.d("Session directory :" + sessionDirectory);
                        if (sessionDirectory.isDirectory()) {
                            java.io.File[] gestureVideoFiles = sessionDirectory.listFiles();
                            if (gestureVideoFiles.length == 0) {
                                sLogger.d("Remove empty directory:" + sessionDirectory);
                                com.navdy.service.library.util.IOUtils.deleteDirectory(com.navdy.hud.app.HudApplication.getAppContext(), sessionDirectory);
                            }
                            for (java.io.File gestureVideoFile : gestureVideoFiles) {
                                sLogger.d("Gesture video :" + gestureVideoFile);
                                if (gestureVideoFile.isFile()) {
                                    if (fileToIgnore != null) {
                                        try {
                                            if (gestureVideoFile.getCanonicalPath().equals(fileToIgnore.getCanonicalPath())) {
                                                continue;
                                            }
                                        } catch (java.io.IOException e) {
                                        }
                                    }
                                    filesToPopulate.add(gestureVideoFile);
                                }
                            }
                            continue;
                        }
                    }
                    sLogger.d("Number of Gesture videos :" + filesToPopulate.size());
                    populateFilesQueue(filesToPopulate, sGestureVideosUploadQueue, 100);
                    mIsInitialized = true;
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void uploadFinished(boolean succeeded, java.lang.String path, java.lang.String userTag) {
        this.bus.post(new com.navdy.hud.app.service.S3FileUploadService.UploadFinished(succeeded, path, userTag));
    }

    public void sync() {
        syncNow();
    }

    public static void syncNow() {
        sLogger.d("synNow");
        android.content.Intent intent = new android.content.Intent(com.navdy.hud.app.HudApplication.getAppContext(), com.navdy.hud.app.service.GestureVideosSyncService.class);
        intent.setAction(com.navdy.hud.app.service.S3FileUploadService.ACTION_SYNC);
        com.navdy.hud.app.HudApplication.getAppContext().startService(intent);
    }

    public void reSchedule() {
        sLogger.d("reschedule");
        scheduleWithDelay(10000);
    }

    public boolean canCompleteRequest(com.navdy.hud.app.service.S3FileUploadService.Request request) {
        return false;
    }

    /* access modifiers changed from: protected */
    public com.navdy.service.library.log.Logger getLogger() {
        return sLogger;
    }

    public static void scheduleWithDelay(long delay) {
        android.content.Intent intent = new android.content.Intent(com.navdy.hud.app.HudApplication.getAppContext(), com.navdy.hud.app.service.GestureVideosSyncService.class);
        intent.setAction(com.navdy.hud.app.service.S3FileUploadService.ACTION_SYNC);
        ((android.app.AlarmManager) com.navdy.hud.app.HudApplication.getAppContext().getSystemService("alarm")).setExact(3, android.os.SystemClock.elapsedRealtime() + delay, android.app.PendingIntent.getService(com.navdy.hud.app.HudApplication.getAppContext(), 123, intent, 268435456));
    }
}
