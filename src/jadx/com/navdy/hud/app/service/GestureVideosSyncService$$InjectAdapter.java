package com.navdy.hud.app.service;

public final class GestureVideosSyncService$$InjectAdapter extends dagger.internal.Binding<com.navdy.hud.app.service.GestureVideosSyncService> implements javax.inject.Provider<com.navdy.hud.app.service.GestureVideosSyncService>, dagger.MembersInjector<com.navdy.hud.app.service.GestureVideosSyncService> {
    private dagger.internal.Binding<com.squareup.otto.Bus> bus;
    private dagger.internal.Binding<com.navdy.hud.app.service.S3FileUploadService> supertype;

    public GestureVideosSyncService$$InjectAdapter() {
        super("com.navdy.hud.app.service.GestureVideosSyncService", "members/com.navdy.hud.app.service.GestureVideosSyncService", false, com.navdy.hud.app.service.GestureVideosSyncService.class);
    }

    public void attach(dagger.internal.Linker linker) {
        this.bus = linker.requestBinding("com.squareup.otto.Bus", com.navdy.hud.app.service.GestureVideosSyncService.class, getClass().getClassLoader());
        dagger.internal.Linker linker2 = linker;
        this.supertype = linker2.requestBinding("members/com.navdy.hud.app.service.S3FileUploadService", com.navdy.hud.app.service.GestureVideosSyncService.class, getClass().getClassLoader(), false, true);
    }

    public void getDependencies(java.util.Set<dagger.internal.Binding<?>> set, java.util.Set<dagger.internal.Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.bus);
        injectMembersBindings.add(this.supertype);
    }

    public com.navdy.hud.app.service.GestureVideosSyncService get() {
        com.navdy.hud.app.service.GestureVideosSyncService result = new com.navdy.hud.app.service.GestureVideosSyncService();
        injectMembers(result);
        return result;
    }

    public void injectMembers(com.navdy.hud.app.service.GestureVideosSyncService object) {
        object.bus = (com.squareup.otto.Bus) this.bus.get();
        this.supertype.injectMembers(object);
    }
}
