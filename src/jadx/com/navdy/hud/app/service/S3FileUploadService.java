package com.navdy.hud.app.service;

public abstract class S3FileUploadService extends android.app.IntentService {
    public static final java.lang.String ACTION_SYNC = "SYNC";
    private static final java.lang.String AWS_ACCOUNT_ID = "AWS_ACCESS_KEY_ID";
    private static final java.lang.String AWS_SECRET = "AWS_SECRET_ACCESS_KEY";
    public static final int RETRY_DELAY = 10000;
    public static final int S3_CONNECTION_TIMEOUT = 15000;
    public static final int S3_UPLOAD_TIMEOUT = 300000;
    protected com.navdy.service.library.log.Logger logger = getLogger();
    private com.amazonaws.services.s3.AmazonS3Client s3Client;
    /* access modifiers changed from: private */
    public com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility transferUtility;

    class Anon1 implements com.amazonaws.mobileconnectors.s3.transferutility.TransferListener {
        final /* synthetic */ java.io.File val$file;
        final /* synthetic */ java.util.concurrent.atomic.AtomicBoolean val$isUploading;
        final /* synthetic */ com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver val$observer;

        Anon1(java.util.concurrent.atomic.AtomicBoolean atomicBoolean, java.io.File file, com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver transferObserver) {
            this.val$isUploading = atomicBoolean;
            this.val$file = file;
            this.val$observer = transferObserver;
        }

        public void onStateChanged(int id, com.amazonaws.mobileconnectors.s3.transferutility.TransferState state) {
            com.navdy.hud.app.service.S3FileUploadService.this.logger.d("State changed : " + state);
            com.navdy.hud.app.service.S3FileUploadService.Request currentRequest = com.navdy.hud.app.service.S3FileUploadService.this.getCurrentRequest();
            boolean shouldPutBack = true;
            boolean shouldReschedule = true;
            switch (com.navdy.hud.app.service.S3FileUploadService.Anon2.$SwitchMap$com$amazonaws$mobileconnectors$s3$transferutility$TransferState[state.ordinal()]) {
                case 1:
                    com.navdy.hud.app.service.S3FileUploadService.this.logger.d("Upload completed");
                    shouldPutBack = false;
                    if (currentRequest != null) {
                        com.navdy.service.library.util.IOUtils.deleteFile(com.navdy.hud.app.HudApplication.getAppContext(), currentRequest.file.getAbsolutePath());
                        this.val$isUploading.set(false);
                        com.navdy.hud.app.service.S3FileUploadService.this.uploadFinished(true, this.val$file.getPath(), currentRequest.userTag);
                        com.navdy.hud.app.service.S3FileUploadService.this.setCurrentRequest(null);
                    }
                    com.navdy.hud.app.service.S3FileUploadService.this.sync();
                    break;
                case 2:
                    com.navdy.hud.app.service.S3FileUploadService.this.logger.d("Upload canceled");
                    break;
                case 3:
                    com.navdy.hud.app.service.S3FileUploadService.this.logger.d("Waiting for network");
                    com.navdy.hud.app.service.S3FileUploadService.this.transferUtility.cancel(id);
                    shouldReschedule = false;
                    break;
                case 4:
                    com.navdy.hud.app.service.S3FileUploadService.this.logger.d("In progress " + this.val$observer.getBytesTotal() + " bytes");
                    shouldPutBack = false;
                    break;
                case 5:
                    com.navdy.hud.app.service.S3FileUploadService.this.logger.d("Failed");
                    break;
            }
            if (shouldPutBack) {
                com.navdy.hud.app.service.S3FileUploadService.UploadQueue gestureUploadQueue = com.navdy.hud.app.service.S3FileUploadService.this.getUploadQueue();
                synchronized (gestureUploadQueue) {
                    if (currentRequest != null) {
                        gestureUploadQueue.add(currentRequest);
                        com.navdy.hud.app.service.S3FileUploadService.this.setCurrentRequest(null);
                    } else {
                        com.navdy.hud.app.service.S3FileUploadService.this.logger.d("request object in upload callback is null, network anomaly?");
                    }
                }
                this.val$isUploading.compareAndSet(true, false);
                if (shouldReschedule) {
                    com.navdy.hud.app.service.S3FileUploadService.this.reSchedule();
                }
            }
        }

        public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
        }

        public void onError(int id, java.lang.Exception ex) {
            com.navdy.hud.app.service.S3FileUploadService.this.logger.e("onError , while uploading, id " + id, ex);
            com.navdy.hud.app.service.S3FileUploadService.Request currentRequest = com.navdy.hud.app.service.S3FileUploadService.this.getCurrentRequest();
            com.navdy.hud.app.service.S3FileUploadService.UploadQueue gestureUploadQueue = com.navdy.hud.app.service.S3FileUploadService.this.getUploadQueue();
            synchronized (gestureUploadQueue) {
                if (currentRequest != null) {
                    gestureUploadQueue.add(currentRequest);
                    com.navdy.hud.app.service.S3FileUploadService.this.setCurrentRequest(null);
                }
            }
            this.val$isUploading.compareAndSet(true, false);
            com.navdy.hud.app.service.S3FileUploadService.this.reSchedule();
        }
    }

    static /* synthetic */ class Anon2 {
        static final /* synthetic */ int[] $SwitchMap$com$amazonaws$mobileconnectors$s3$transferutility$TransferState = new int[com.amazonaws.mobileconnectors.s3.transferutility.TransferState.values().length];

        static {
            try {
                $SwitchMap$com$amazonaws$mobileconnectors$s3$transferutility$TransferState[com.amazonaws.mobileconnectors.s3.transferutility.TransferState.COMPLETED.ordinal()] = 1;
            } catch (java.lang.NoSuchFieldError e) {
            }
            try {
                $SwitchMap$com$amazonaws$mobileconnectors$s3$transferutility$TransferState[com.amazonaws.mobileconnectors.s3.transferutility.TransferState.CANCELED.ordinal()] = 2;
            } catch (java.lang.NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$com$amazonaws$mobileconnectors$s3$transferutility$TransferState[com.amazonaws.mobileconnectors.s3.transferutility.TransferState.WAITING_FOR_NETWORK.ordinal()] = 3;
            } catch (java.lang.NoSuchFieldError e3) {
            }
            try {
                $SwitchMap$com$amazonaws$mobileconnectors$s3$transferutility$TransferState[com.amazonaws.mobileconnectors.s3.transferutility.TransferState.IN_PROGRESS.ordinal()] = 4;
            } catch (java.lang.NoSuchFieldError e4) {
            }
            try {
                $SwitchMap$com$amazonaws$mobileconnectors$s3$transferutility$TransferState[com.amazonaws.mobileconnectors.s3.transferutility.TransferState.FAILED.ordinal()] = 5;
            } catch (java.lang.NoSuchFieldError e5) {
            }
        }
    }

    protected static class Request {
        public final java.io.File file;
        public final java.lang.String userTag;

        public Request(java.io.File file2, java.lang.String userTag2) {
            this.file = file2;
            this.userTag = userTag2;
        }
    }

    public static class RequestTimeComparator implements java.util.Comparator<com.navdy.hud.app.service.S3FileUploadService.Request> {
        public int compare(com.navdy.hud.app.service.S3FileUploadService.Request a, com.navdy.hud.app.service.S3FileUploadService.Request b) {
            return java.lang.Long.compare(b.file.lastModified(), a.file.lastModified());
        }
    }

    public static class UploadFinished {
        public final java.lang.String filePath;
        public final boolean succeeded;
        public final java.lang.String userTag;

        UploadFinished(boolean succeeded2, java.lang.String filePath2, java.lang.String userTag2) {
            this.succeeded = succeeded2;
            this.filePath = filePath2;
            this.userTag = userTag2;
        }
    }

    public static class UploadQueue {
        private final java.util.PriorityQueue<com.navdy.hud.app.service.S3FileUploadService.Request> internalQueue = new java.util.PriorityQueue<>(15, new com.navdy.hud.app.service.S3FileUploadService.RequestTimeComparator());

        public com.navdy.hud.app.service.S3FileUploadService.Request pop() {
            return (com.navdy.hud.app.service.S3FileUploadService.Request) this.internalQueue.poll();
        }

        public com.navdy.hud.app.service.S3FileUploadService.Request peek() {
            return (com.navdy.hud.app.service.S3FileUploadService.Request) this.internalQueue.peek();
        }

        public void add(com.navdy.hud.app.service.S3FileUploadService.Request request) {
            this.internalQueue.add(request);
        }

        public int size() {
            return this.internalQueue.size();
        }
    }

    public abstract boolean canCompleteRequest(com.navdy.hud.app.service.S3FileUploadService.Request request);

    /* access modifiers changed from: protected */
    public abstract java.lang.String getAWSBucket();

    /* access modifiers changed from: protected */
    public abstract com.navdy.hud.app.service.S3FileUploadService.Request getCurrentRequest();

    /* access modifiers changed from: protected */
    public abstract java.util.concurrent.atomic.AtomicBoolean getIsUploading();

    /* access modifiers changed from: protected */
    public abstract java.lang.String getKeyPrefix(java.io.File file);

    /* access modifiers changed from: protected */
    public abstract com.navdy.service.library.log.Logger getLogger();

    /* access modifiers changed from: protected */
    public abstract com.navdy.hud.app.service.S3FileUploadService.UploadQueue getUploadQueue();

    /* access modifiers changed from: protected */
    public abstract void initialize();

    public abstract void reSchedule();

    /* access modifiers changed from: protected */
    public abstract void setCurrentRequest(com.navdy.hud.app.service.S3FileUploadService.Request request);

    public abstract void sync();

    /* access modifiers changed from: protected */
    public abstract void uploadFinished(boolean z, java.lang.String str, java.lang.String str2);

    public S3FileUploadService(java.lang.String name) {
        super(name);
    }

    public com.amazonaws.services.s3.AmazonS3Client createS3Client() {
        com.amazonaws.auth.BasicAWSCredentials credentials = new com.amazonaws.auth.BasicAWSCredentials(com.navdy.service.library.util.CredentialUtil.getCredentials(com.navdy.hud.app.HudApplication.getAppContext(), "AWS_ACCESS_KEY_ID"), com.navdy.service.library.util.CredentialUtil.getCredentials(com.navdy.hud.app.HudApplication.getAppContext(), "AWS_SECRET_ACCESS_KEY"));
        com.amazonaws.ClientConfiguration config = new com.amazonaws.ClientConfiguration();
        config.setConnectionTimeout(15000);
        config.setSocketTimeout(300000);
        return new com.amazonaws.services.s3.AmazonS3Client((com.amazonaws.auth.AWSCredentials) credentials, config);
    }

    public void onCreate() {
        super.onCreate();
        if (this.s3Client == null) {
            this.s3Client = createS3Client();
            this.transferUtility = new com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility(this.s3Client, com.navdy.hud.app.HudApplication.getAppContext());
        }
    }

    public void onDestroy() {
        super.onDestroy();
    }

    public static void populateFilesQueue(java.util.ArrayList<java.io.File> files, com.navdy.hud.app.service.S3FileUploadService.UploadQueue filesQueue, int maxSize) {
        if (files != null) {
            java.util.Iterator it = files.iterator();
            while (it.hasNext()) {
                java.io.File file = (java.io.File) it.next();
                if (file.isFile()) {
                    filesQueue.add(new com.navdy.hud.app.service.S3FileUploadService.Request(file, null));
                    if (filesQueue.size() == maxSize) {
                        filesQueue.pop();
                    }
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:49:?, code lost:
        return;
     */
    public void onHandleIntent(android.content.Intent intent) {
        if (ACTION_SYNC.equals(intent != null ? intent.getAction() : "")) {
            boolean connectedNetwork = com.navdy.service.library.util.SystemUtils.isConnectedToNetwork(com.navdy.hud.app.HudApplication.getAppContext());
            this.logger.d("Performing sync , connected to network ? : " + connectedNetwork);
            if (connectedNetwork) {
                initialize();
                com.navdy.hud.app.service.S3FileUploadService.UploadQueue gestureUploadQueue = getUploadQueue();
                synchronized (gestureUploadQueue) {
                    if (gestureUploadQueue.size() > 0) {
                        java.util.concurrent.atomic.AtomicBoolean isUploading = getIsUploading();
                        if (isUploading.compareAndSet(false, true)) {
                            com.navdy.hud.app.service.S3FileUploadService.Request currentRequest = null;
                            do {
                                if (currentRequest != null) {
                                    this.logger.e("File to upload " + currentRequest.file + ", Does not exist anymore");
                                }
                                if (gestureUploadQueue.size() > 0) {
                                    currentRequest = gestureUploadQueue.pop();
                                } else {
                                    isUploading.set(false);
                                    return;
                                }
                            } while (!currentRequest.file.exists());
                            setCurrentRequest(currentRequest);
                            java.io.File file = currentRequest.file;
                            java.lang.String prefix = getKeyPrefix(file);
                            java.lang.String key = !android.text.TextUtils.isEmpty(prefix) ? prefix + java.io.File.separator + file.getName() : file.getName();
                            this.logger.d("Trying to upload : " + file.getName() + ", Under :" + prefix + ", Key :" + key);
                            try {
                                com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver observer = this.transferUtility.upload(getAWSBucket(), key, file);
                                this.logger.d("Transfer id " + observer.getId());
                                observer.setTransferListener(new com.navdy.hud.app.service.S3FileUploadService.Anon1(isUploading, file, observer));
                            } catch (java.lang.IllegalArgumentException e) {
                                e.printStackTrace();
                                isUploading.set(false);
                                uploadFinished(false, file.getPath(), currentRequest.userTag);
                                sync();
                            }
                        }
                    } else {
                        this.logger.d("Nothing to upload");
                    }
                }
            }
        }
    }
}
