package com.navdy.hud.app.service;

public class ConnectionServiceAnalyticsSupport {

    public static class IAPListenerReceiver implements com.navdy.hud.mfi.IAPListener {
        public void onCoprocessorStatusCheckFailed(java.lang.String desc, java.lang.String status, java.lang.String errorCode) {
            com.navdy.hud.app.service.ConnectionServiceAnalyticsSupport.sendEvent(com.navdy.hud.app.analytics.AnalyticsSupport.ANALYTICS_EVENT_IAP_FAILURE, true, "Description", desc, "Status", status, "Error_Code", errorCode);
        }

        public void onDeviceAuthenticationSuccess(int retries) {
            if (retries > 0) {
                com.navdy.hud.app.service.ConnectionServiceAnalyticsSupport.sendEvent(com.navdy.hud.app.analytics.AnalyticsSupport.ANALYTICS_EVENT_IAP_RETRY_SUCCESS, true, "Retries", java.lang.Integer.toString(retries));
            }
        }
    }

    public static void recordGpsAccuracy(java.lang.String minimum, java.lang.String maximum, java.lang.String average) {
        sendEvent(com.navdy.hud.app.analytics.AnalyticsSupport.ANALYTICS_EVENT_GPS_ACCURACY_STATISTICS, com.navdy.hud.app.device.gps.GpsConstants.GPS_EVENT_ACCURACY_MIN, minimum, com.navdy.hud.app.device.gps.GpsConstants.GPS_EVENT_ACCURACY_MAX, maximum, com.navdy.hud.app.device.gps.GpsConstants.GPS_EVENT_ACCURACY_AVERAGE, average);
    }

    public static void recordGpsAcquireLocation(java.lang.String time, java.lang.String accuracy) {
        sendEvent(com.navdy.hud.app.analytics.AnalyticsSupport.ANALYTICS_EVENT_GPS_ACQUIRE_LOCATION, com.navdy.hud.app.device.gps.GpsConstants.GPS_EVENT_TIME, time, com.navdy.hud.app.device.gps.GpsConstants.GPS_EVENT_ACCURACY, accuracy);
    }

    public static void recordGpsAttemptAcquireLocation() {
        sendEvent(com.navdy.hud.app.analytics.AnalyticsSupport.ANALYTICS_EVENT_GPS_ATTEMPT_ACQUIRE_LOCATION, new java.lang.String[0]);
    }

    public static void recordGpsLostLocation(java.lang.String time) {
        sendEvent(com.navdy.hud.app.analytics.AnalyticsSupport.ANALYTICS_EVENT_GPS_LOST_LOCATION, com.navdy.hud.app.device.gps.GpsConstants.GPS_EVENT_TIME, time);
    }

    public static void recordNewDevice() {
        sendEvent(com.navdy.hud.app.analytics.AnalyticsSupport.ANALYTICS_EVENT_NEW_DEVICE, new java.lang.String[0]);
    }

    private static void sendEvent(java.lang.String name, java.lang.String... args) {
        sendEvent(name, false, args);
    }

    /* access modifiers changed from: private */
    public static void sendEvent(java.lang.String name, boolean includeDeviceInfo, java.lang.String... args) {
        android.content.Intent intent = new android.content.Intent(com.navdy.hud.app.analytics.AnalyticsSupport.ANALYTICS_INTENT);
        intent.putExtra(com.navdy.hud.app.analytics.AnalyticsSupport.ANALYTICS_INTENT_EXTRA_TAG_NAME, name);
        intent.putExtra(com.navdy.hud.app.analytics.AnalyticsSupport.ANALYTICS_INTENT_EXTRA_ARGUMENTS, args);
        if (includeDeviceInfo) {
            intent.putExtra(com.navdy.hud.app.analytics.AnalyticsSupport.ANALYTICS_INTENT_EXTRA_INCLUDE_REMOTE_DEVICE_INFO, true);
        }
        com.navdy.hud.app.HudApplication.getAppContext().sendBroadcastAsUser(intent, android.os.Process.myUserHandle());
    }
}
