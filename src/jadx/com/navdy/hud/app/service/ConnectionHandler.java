package com.navdy.hud.app.service;

public class ConnectionHandler {
    static com.navdy.service.library.events.Capabilities CAPABILITIES = new com.navdy.service.library.events.Capabilities.Builder().compactUi(java.lang.Boolean.valueOf(true)).localMusicBrowser(java.lang.Boolean.valueOf(true)).voiceSearch(java.lang.Boolean.valueOf(true)).voiceSearchNewIOSPauseBehaviors(java.lang.Boolean.valueOf(true)).musicArtworkCache(java.lang.Boolean.valueOf(true)).searchResultList(java.lang.Boolean.valueOf(true)).cannedResponseToSms(java.lang.Boolean.valueOf(com.navdy.hud.app.ui.component.UISettings.supportsIosSms())).customDialLongPress(java.lang.Boolean.valueOf(true)).build();
    private static final com.navdy.hud.app.service.ConnectionHandler.DeviceSyncEvent DEVICE_FULL_SYNC_EVENT = new com.navdy.hud.app.service.ConnectionHandler.DeviceSyncEvent(1);
    private static final com.navdy.hud.app.service.ConnectionHandler.DeviceSyncEvent DEVICE_MINIMAL_SYNC_EVENT = new com.navdy.hud.app.service.ConnectionHandler.DeviceSyncEvent(0);
    public static final int IOS_APP_LAUNCH_TIMEOUT = 5000;
    private static java.util.List<com.navdy.service.library.events.LegacyCapability> LEGACY_CAPABILITIES = new java.util.ArrayList();
    private static final int LINK_LOST_TIME_OUT_AFTER_DISCONNECTED = 1000;
    private static final int REDOWNLOAD_THRESHOLD = 30000;
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.service.ConnectionHandler.class);
    private com.squareup.otto.Bus bus;
    protected android.content.Context context;
    protected boolean disconnecting;
    private volatile boolean forceFullUpdate = false;
    private volatile boolean isNetworkLinkReady = false;
    private long lastConnectTime;
    private java.lang.String lastConnectedDeviceId;
    protected com.navdy.service.library.events.DeviceInfo lastConnectedDeviceInfo;
    private long lastDisconnectTime;
    private long lastLinkTime;
    private boolean mAppClosed = true;
    private boolean mAppLaunchAttempted = false;
    private java.lang.Runnable mAppLaunchAttemptedRunnable;
    protected com.navdy.hud.app.service.CustomNotificationServiceHandler mCustomNotificationServiceHandler;
    protected java.lang.Runnable mDisconnectRunnable;
    protected com.navdy.hud.app.profile.DriverProfileManager mDriverProfileManager;
    protected android.os.Handler mHandler;
    private com.navdy.hud.app.device.PowerManager mPowerManager;
    protected java.lang.Runnable mReconnectTimedOutRunnable;
    protected com.navdy.hud.app.service.RemoteDeviceProxy mRemoteDevice;
    protected com.navdy.hud.app.service.ConnectionServiceProxy proxy;
    private com.navdy.hud.app.common.TimeHelper timeHelper;
    private com.navdy.hud.app.ui.framework.UIStateManager uiStateManager;

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            com.navdy.hud.app.service.ConnectionHandler.this.handleDisconnectWithoutLinkLoss();
        }
    }

    class Anon2 implements java.lang.Runnable {
        Anon2() {
        }

        public void run() {
            com.navdy.hud.app.service.ConnectionHandler.this.sendConnectionNotification();
        }
    }

    class Anon3 implements java.lang.Runnable {
        Anon3() {
        }

        public void run() {
            com.navdy.hud.app.service.ConnectionHandler.sLogger.d("No response after the timeout after App launch attempt");
            com.navdy.hud.app.service.ConnectionHandler.this.sendConnectionNotification();
        }
    }

    public static class ApplicationLaunchAttempted {
    }

    public static class DeviceSyncEvent {
        public static final int FULL = 1;
        public static final int MINIMAL = 0;
        public final int amount;

        public DeviceSyncEvent(int amount2) {
            this.amount = amount2;
        }
    }

    static {
        LEGACY_CAPABILITIES.add(com.navdy.service.library.events.LegacyCapability.CAPABILITY_VOICE_SEARCH);
        LEGACY_CAPABILITIES.add(com.navdy.service.library.events.LegacyCapability.CAPABILITY_COMPACT_UI);
        LEGACY_CAPABILITIES.add(com.navdy.service.library.events.LegacyCapability.CAPABILITY_LOCAL_MUSIC_BROWSER);
    }

    public ConnectionHandler(android.content.Context context2, com.navdy.hud.app.service.ConnectionServiceProxy proxy2, com.navdy.hud.app.device.PowerManager powerManager, com.navdy.hud.app.profile.DriverProfileManager driverProfileManager, com.navdy.hud.app.ui.framework.UIStateManager uiStateManager2, com.navdy.hud.app.common.TimeHelper timeHelper2) {
        this.context = context2;
        this.proxy = proxy2;
        this.mPowerManager = powerManager;
        this.bus = this.proxy.getBus();
        this.bus.register(this);
        this.mCustomNotificationServiceHandler = new com.navdy.hud.app.service.CustomNotificationServiceHandler(this.bus);
        this.mCustomNotificationServiceHandler.start();
        this.mDriverProfileManager = driverProfileManager;
        this.uiStateManager = uiStateManager2;
        this.timeHelper = timeHelper2;
        sLogger.i("Creating connectionHandler:" + this);
        this.mHandler = new android.os.Handler(android.os.Looper.getMainLooper());
        this.mDisconnectRunnable = new com.navdy.hud.app.service.ConnectionHandler.Anon1();
        this.mReconnectTimedOutRunnable = new com.navdy.hud.app.service.ConnectionHandler.Anon2();
        this.mAppLaunchAttemptedRunnable = new com.navdy.hud.app.service.ConnectionHandler.Anon3();
    }

    public boolean serviceConnected() {
        return this.proxy.connected();
    }

    public void connect() {
        this.proxy.connect();
    }

    public void shutdown() {
        disconnect();
        this.proxy.disconnect();
    }

    public com.navdy.service.library.device.RemoteDevice getRemoteDevice() {
        return this.mRemoteDevice;
    }

    public com.navdy.service.library.device.NavdyDeviceId getConnectedDevice() {
        if (this.mRemoteDevice != null) {
            return this.mRemoteDevice.getDeviceId();
        }
        return null;
    }

    public void connectToDevice(com.navdy.service.library.device.NavdyDeviceId deviceId) {
        sendLocalMessage(new com.navdy.service.library.events.connection.ConnectionRequest(com.navdy.service.library.events.connection.ConnectionRequest.Action.CONNECTION_SELECT, deviceId != null ? deviceId.toString() : null));
    }

    public void searchForDevices() {
        sendLocalMessage(new com.navdy.service.library.events.connection.ConnectionRequest(com.navdy.service.library.events.connection.ConnectionRequest.Action.CONNECTION_START_SEARCH, (java.lang.String) null));
    }

    public void stopSearch() {
        sendLocalMessage(new com.navdy.service.library.events.connection.ConnectionRequest(com.navdy.service.library.events.connection.ConnectionRequest.Action.CONNECTION_STOP_SEARCH, (java.lang.String) null));
    }

    public void sendLocalMessage(com.squareup.wire.Message message) {
        try {
            this.proxy.postRemoteEvent(com.navdy.service.library.device.NavdyDeviceId.getThisDevice(this.context), com.navdy.service.library.events.NavdyEventUtil.eventFromMessage(message));
        } catch (java.lang.Exception e) {
            sLogger.e("Failed to send local event", e);
        }
    }

    public void disconnect() {
        if (this.mRemoteDevice != null) {
            this.lastConnectedDeviceInfo = null;
            this.disconnecting = true;
            connectToDevice(null);
        }
    }

    @com.squareup.otto.Subscribe
    public void onDisconnect(com.navdy.hud.app.event.Disconnect event) {
        disconnect();
    }

    @com.squareup.otto.Subscribe
    public void onShutdown(com.navdy.hud.app.event.Shutdown event) {
        if (event.state == com.navdy.hud.app.event.Shutdown.State.SHUTTING_DOWN) {
            shutdown();
        }
    }

    @com.squareup.otto.Subscribe
    public void onIncrementalOTAFailureDetected(com.navdy.hud.app.util.OTAUpdateService.IncrementalOTAFailureDetected event) {
        sLogger.d("Incremental OTA failure detected, indicate the remote device to force full update");
        this.forceFullUpdate = true;
    }

    @com.squareup.otto.Subscribe
    public void onLinkPropertiesChanged(com.navdy.service.library.events.connection.LinkPropertiesChanged linkPropertiesChanged) {
        if (linkPropertiesChanged != null && linkPropertiesChanged.bandwidthLevel != null && this.mRemoteDevice != null) {
            this.mRemoteDevice.setLinkBandwidthLevel(linkPropertiesChanged.bandwidthLevel.intValue());
        }
    }

    @com.squareup.otto.Subscribe
    public void onConnectionStateChange(com.navdy.service.library.events.connection.ConnectionStateChange event) {
        sLogger.d("ConnectionStateChange - " + event.state);
        java.lang.String remoteDeviceId = event.remoteDeviceId;
        com.navdy.service.library.device.NavdyDeviceId deviceId = android.text.TextUtils.isEmpty(remoteDeviceId) ? com.navdy.service.library.device.NavdyDeviceId.UNKNOWN_ID : new com.navdy.service.library.device.NavdyDeviceId(remoteDeviceId);
        this.mAppLaunchAttempted = false;
        this.mHandler.removeCallbacks(this.mAppLaunchAttemptedRunnable);
        switch (event.state) {
            case CONNECTION_LINK_ESTABLISHED:
                this.mAppClosed = true;
                onLinkEstablished(deviceId);
                return;
            case CONNECTION_CONNECTED:
                onConnect(deviceId);
                this.mAppClosed = false;
                return;
            case CONNECTION_DISCONNECTED:
                this.lastDisconnectTime = android.os.SystemClock.elapsedRealtime();
                this.mAppClosed = true;
                onDisconnect(deviceId);
                return;
            case CONNECTION_LINK_LOST:
                this.lastDisconnectTime = android.os.SystemClock.elapsedRealtime();
                this.mAppClosed = true;
                onLinkLost(deviceId);
                return;
            default:
                return;
        }
    }

    @com.squareup.otto.Subscribe
    public void onApplicationLaunchAttempted(com.navdy.hud.app.service.ConnectionHandler.ApplicationLaunchAttempted applicationLaunchAttempted) {
        sLogger.d("Attempting to launch the App, Will wait and see");
        if (this.mAppClosed) {
            this.mAppLaunchAttempted = true;
            this.mHandler.removeCallbacks(this.mAppLaunchAttemptedRunnable);
            this.mHandler.postDelayed(this.mAppLaunchAttemptedRunnable, 5000);
        }
    }

    private void onLinkEstablished(com.navdy.service.library.device.NavdyDeviceId connectingDevice) {
        this.lastLinkTime = android.os.SystemClock.elapsedRealtime();
        this.mPowerManager.wakeUp(com.navdy.hud.app.analytics.AnalyticsSupport.WakeupReason.PHONE);
        if (this.mRemoteDevice != null && com.navdy.hud.app.BuildConfig.DEBUG) {
            sLogger.w(java.lang.String.format("Seeing a connect for %s while connected to %s!", new java.lang.Object[]{connectingDevice.getDisplayName(), this.mRemoteDevice.getDeviceId().getDisplayName()}));
        }
        this.mRemoteDevice = new com.navdy.hud.app.service.RemoteDeviceProxy(this.proxy, this.context, connectingDevice);
        this.mHandler.removeCallbacks(this.mReconnectTimedOutRunnable);
    }

    private void onConnect(com.navdy.service.library.device.NavdyDeviceId connectingDevice) {
        this.lastConnectTime = android.os.SystemClock.elapsedRealtime();
        if (this.mRemoteDevice == null) {
            this.mRemoteDevice = new com.navdy.hud.app.service.RemoteDeviceProxy(this.proxy, this.context, connectingDevice);
        }
        this.mDriverProfileManager.loadProfileForId(connectingDevice);
        com.navdy.service.library.device.NavdyDeviceId displayDeviceId = com.navdy.service.library.device.NavdyDeviceId.getThisDevice(this.context);
        com.navdy.service.library.events.DeviceInfo deviceInfo = new com.navdy.service.library.events.DeviceInfo.Builder().deviceId(displayDeviceId.toString()).clientVersion(com.navdy.hud.app.BuildConfig.VERSION_NAME).protocolVersion(com.navdy.service.library.Version.PROTOCOL_VERSION.toString()).deviceName(displayDeviceId.getDeviceName()).systemVersion(getSystemVersion()).model(android.os.Build.MODEL).deviceUuid(android.os.Build.SERIAL).systemApiLevel(java.lang.Integer.valueOf(android.os.Build.VERSION.SDK_INT)).kernelVersion(java.lang.System.getProperty("os.version")).platform(com.navdy.service.library.events.DeviceInfo.Platform.PLATFORM_Android).buildType(android.os.Build.TYPE).deviceMake("Navdy").forceFullUpdate(java.lang.Boolean.valueOf(this.forceFullUpdate)).legacyCapabilities(LEGACY_CAPABILITIES).capabilities(CAPABILITIES).build();
        sLogger.d("onConnect - sending device info: " + deviceInfo);
        this.mRemoteDevice.postEvent((com.squareup.wire.Message) deviceInfo);
    }

    @com.squareup.otto.Subscribe
    public void onNetworkLinkReady(com.navdy.service.library.events.connection.NetworkLinkReady networkLinkReady) {
        sLogger.v("DummyNetNetworkFactory:onConnect");
        this.isNetworkLinkReady = true;
        com.navdy.hud.app.framework.network.NetworkStateManager.getInstance().networkAvailable();
        com.navdy.hud.app.service.GestureVideosSyncService.scheduleWithDelay(30000);
        com.navdy.hud.app.service.ObdCANBusDataUploadService.scheduleWithDelay(45000);
    }

    private java.lang.String getSystemVersion() {
        if (!java.lang.String.valueOf(com.navdy.hud.app.BuildConfig.VERSION_CODE).equals(android.os.Build.VERSION.INCREMENTAL)) {
            return com.navdy.hud.app.BuildConfig.VERSION_NAME;
        }
        return android.os.Build.VERSION.INCREMENTAL;
    }

    private synchronized void onDisconnect(com.navdy.service.library.device.NavdyDeviceId navdyDeviceId) {
        sLogger.v("onDisconnect:" + navdyDeviceId);
        this.isNetworkLinkReady = false;
        com.navdy.hud.app.framework.network.NetworkStateManager.getInstance().networkNotAvailable();
        this.mDriverProfileManager.setCurrentProfile(null);
        this.mHandler.postDelayed(this.mDisconnectRunnable, 1000);
    }

    private synchronized void onLinkLost(com.navdy.service.library.device.NavdyDeviceId deviceId) {
        boolean wasConnected = true;
        synchronized (this) {
            sLogger.v("onLinkLost:" + deviceId);
            this.isNetworkLinkReady = false;
            this.mHandler.removeCallbacks(this.mDisconnectRunnable);
            this.mHandler.removeCallbacks(this.mReconnectTimedOutRunnable);
            if (this.mRemoteDevice == null) {
                wasConnected = false;
            }
            this.mRemoteDevice = null;
            com.navdy.hud.app.util.picasso.PicassoUtil.clearCache();
            if (wasConnected) {
                com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().handleDisconnect(true);
                if (!this.disconnecting) {
                    this.mHandler.postDelayed(this.mReconnectTimedOutRunnable, com.glympse.android.lib.StaticConfig.PERMISSION_CHECK_INTERVAL);
                }
            }
            this.disconnecting = false;
        }
    }

    /* access modifiers changed from: private */
    public void handleDisconnectWithoutLinkLoss() {
        com.navdy.service.library.events.DeviceInfo.Platform remoteDevicePlatform = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getRemoteDevicePlatform();
        if (remoteDevicePlatform != null && remoteDevicePlatform.equals(com.navdy.service.library.events.DeviceInfo.Platform.PLATFORM_iOS)) {
            sendConnectionNotification();
        }
    }

    @com.squareup.otto.Subscribe
    public void onDeviceInfo(com.navdy.service.library.events.DeviceInfo deviceInfo) {
        sLogger.i("Received deviceInfo:" + deviceInfo);
        if (this.mRemoteDevice != null) {
            boolean triggerDownload = true;
            if (this.lastConnectedDeviceId != null) {
                long time = android.os.SystemClock.elapsedRealtime() - this.lastDisconnectTime;
                if (time < 30000) {
                    sLogger.v("threshold met=" + time);
                    if (android.text.TextUtils.equals(this.lastConnectedDeviceId, deviceInfo.deviceId)) {
                        triggerDownload = false;
                        sLogger.v("same device connected:" + deviceInfo.deviceId);
                    } else {
                        sLogger.v("different device connected last[" + this.lastConnectedDeviceId + "] current[" + deviceInfo.deviceId + "]");
                        this.lastConnectedDeviceId = deviceInfo.deviceId;
                    }
                } else {
                    sLogger.v("threshold not met=" + time);
                    this.lastConnectedDeviceId = deviceInfo.deviceId;
                }
            } else {
                sLogger.v("no last device");
                this.lastConnectedDeviceId = deviceInfo.deviceId;
            }
            java.lang.String currentDeviceId = this.mRemoteDevice.getDeviceId().toString();
            com.navdy.service.library.events.DeviceInfo deviceInfo2 = new com.navdy.service.library.events.DeviceInfo.Builder(deviceInfo).deviceId(currentDeviceId).build();
            boolean newDevice = !currentDeviceId.equals(this.lastConnectedDeviceInfo != null ? this.lastConnectedDeviceInfo.deviceId : null);
            this.lastConnectedDeviceInfo = deviceInfo2;
            this.mRemoteDevice.setDeviceInfo(deviceInfo2);
            com.navdy.hud.app.analytics.AnalyticsSupport.recordDeviceConnect(deviceInfo2, this.lastConnectTime, this.lastLinkTime);
            this.bus.post(new com.navdy.hud.app.event.DeviceInfoAvailable(deviceInfo2));
            if (triggerDownload) {
                sLogger.v("trigger device sync");
                this.bus.post(DEVICE_FULL_SYNC_EVENT);
            } else if (deviceInfo2.platform == com.navdy.service.library.events.DeviceInfo.Platform.PLATFORM_iOS) {
                sLogger.v("triggering minimal sync");
                this.bus.post(DEVICE_MINIMAL_SYNC_EVENT);
            } else {
                sLogger.v("don't trigger device sync");
            }
            if (newDevice) {
                sendConnectionNotification();
                return;
            }
            com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().handleConnect();
            com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().hideConnectionToast(true);
            long elapsedTime = android.os.SystemClock.elapsedRealtime() - this.lastDisconnectTime;
            sLogger.i("Tracking a reconnect that took " + elapsedTime + "ms");
            com.navdy.hud.app.analytics.AnalyticsSupport.recordPhoneDisconnect(true, java.lang.String.valueOf(elapsedTime));
        }
    }

    @com.squareup.otto.Subscribe
    public void onRemoteEvent(com.navdy.hud.app.event.RemoteEvent event) {
        if (this.mRemoteDevice != null) {
            this.mRemoteDevice.postEvent(event.getMessage());
        }
    }

    @com.squareup.otto.Subscribe
    public void onLocalSpeechRequest(com.navdy.hud.app.event.LocalSpeechRequest event) {
        if (!com.navdy.hud.app.framework.phonecall.CallUtils.isPhoneCallInProgress()) {
            if (sLogger.isLoggable(2)) {
                sLogger.v("[tts-outbound] [" + event.speechRequest.category.name() + "] [" + event.speechRequest.words + "]");
            }
            onRemoteEvent(new com.navdy.hud.app.event.RemoteEvent(event.speechRequest));
        }
    }

    @com.squareup.otto.Subscribe
    public void onDateTimeConfiguration(com.navdy.service.library.events.settings.DateTimeConfiguration configurationEvent) {
        sLogger.i("Received timestamp from the client: " + configurationEvent.timestamp + " " + configurationEvent.timezone);
        try {
            if (com.navdy.hud.app.util.DeviceUtil.isNavdyDevice()) {
                android.app.AlarmManager am = (android.app.AlarmManager) this.context.getSystemService("alarm");
                sLogger.v("setting date timezone[" + configurationEvent.timezone + "] time[" + configurationEvent.timestamp + "]");
                if (configurationEvent.timezone == null) {
                    sLogger.w("timezone not provided");
                } else if (java.util.TimeZone.getDefault().getID().equalsIgnoreCase(configurationEvent.timezone)) {
                    sLogger.v("timezone already set to " + configurationEvent.timezone);
                } else {
                    java.util.TimeZone timeZone = java.util.TimeZone.getTimeZone(configurationEvent.timezone);
                    if (!timeZone.getID().equalsIgnoreCase(configurationEvent.timezone)) {
                        sLogger.e("timezone not found on HUD:" + configurationEvent.timezone);
                    } else {
                        sLogger.v("timezone found on HUD:" + timeZone.getID());
                        am.setTimeZone(configurationEvent.timezone);
                    }
                }
                sLogger.v("setting time [" + configurationEvent.timestamp + "]");
                am.setTime(configurationEvent.timestamp.longValue());
                sLogger.v("set time [" + configurationEvent.timestamp + "]");
                this.bus.post(com.navdy.hud.app.common.TimeHelper.DATE_TIME_AVAILABLE_EVENT);
                sLogger.v("post date complete");
            }
            com.navdy.hud.app.profile.DriverProfileManager driverProfileManager = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getDriverProfileManager();
            driverProfileManager.getSessionPreferences().setClockConfiguration(configurationEvent);
            driverProfileManager.updateLocalPreferences(new com.navdy.service.library.events.preferences.LocalPreferences.Builder(driverProfileManager.getCurrentProfile().getLocalPreferences()).clockFormat(configurationEvent.format).build());
        } catch (Throwable t) {
            sLogger.e("Cannot update time on device", t);
        }
    }

    @com.squareup.otto.Subscribe
    public void onDriverProfileUpdated(com.navdy.hud.app.event.DriverProfileChanged profileChanged) {
        com.navdy.hud.app.profile.DriverProfile profile = this.mDriverProfileManager.getCurrentProfile();
        requestIAPUpdateBasedOnPreference(profile != null ? profile.getNotificationPreferences() : null);
    }

    @com.squareup.otto.Subscribe
    public void onNotificationPreference(com.navdy.service.library.events.preferences.NotificationPreferences preferences) {
        requestIAPUpdateBasedOnPreference(preferences);
    }

    @com.squareup.otto.Subscribe
    public void onWakeup(com.navdy.hud.app.event.Wakeup event) {
        if (com.navdy.hud.app.device.dial.DialManager.getInstance().getBondedDialCount() == 0) {
            sLogger.v("go to dial pairing screen");
            this.bus.post(new com.navdy.service.library.events.ui.ShowScreen.Builder().screen(com.navdy.service.library.events.ui.Screen.SCREEN_DIAL_PAIRING).build());
        } else if (this.mRemoteDevice == null) {
            sLogger.v("go to welcome screen");
            android.os.Bundle args = new android.os.Bundle();
            args.putString(com.navdy.hud.app.screen.WelcomeScreen.ARG_ACTION, com.navdy.hud.app.screen.WelcomeScreen.ACTION_RECONNECT);
            this.bus.post(new com.navdy.hud.app.event.ShowScreenWithArgs(com.navdy.service.library.events.ui.Screen.SCREEN_WELCOME, args, false));
        } else {
            this.bus.post(new com.navdy.service.library.events.ui.ShowScreen.Builder().screen(this.uiStateManager.getDefaultMainActiveScreen()).build());
        }
    }

    private void requestIAPUpdateBasedOnPreference(com.navdy.service.library.events.preferences.NotificationPreferences preferences) {
        if (preferences == null) {
            return;
        }
        if (java.lang.Boolean.TRUE.equals(preferences.enabled)) {
            this.bus.post(new com.navdy.hud.app.event.RemoteEvent(new com.navdy.service.library.events.callcontrol.CallStateUpdateRequest(java.lang.Boolean.valueOf(true))));
            this.bus.post(new com.navdy.hud.app.event.RemoteEvent(new com.navdy.service.library.events.audio.NowPlayingUpdateRequest(java.lang.Boolean.valueOf(true))));
            return;
        }
        this.bus.post(new com.navdy.hud.app.event.RemoteEvent(new com.navdy.service.library.events.callcontrol.CallStateUpdateRequest(java.lang.Boolean.valueOf(false))));
        this.bus.post(new com.navdy.hud.app.event.RemoteEvent(new com.navdy.service.library.events.audio.NowPlayingUpdateRequest(java.lang.Boolean.valueOf(false))));
    }

    public void sendConnectionNotification() {
        com.navdy.hud.app.framework.notifications.NotificationManager notificationManager = com.navdy.hud.app.framework.notifications.NotificationManager.getInstance();
        if (!com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().isRemoteDeviceConnected()) {
            notificationManager.handleDisconnect(true);
            notificationManager.showHideToast(false);
        } else if (isAppClosed()) {
            notificationManager.handleDisconnect(false);
            notificationManager.showHideToast(false);
        } else {
            notificationManager.handleConnect();
            notificationManager.showHideToast(true);
        }
    }

    public com.navdy.service.library.events.DeviceInfo getLastConnectedDeviceInfo() {
        return this.lastConnectedDeviceInfo;
    }

    public boolean isAppClosed() {
        return this.mAppClosed;
    }

    public boolean isNetworkLinkReady() {
        return this.isNetworkLinkReady;
    }

    public com.navdy.service.library.events.debug.StartDriveRecordingEvent getDriverRecordingEvent() {
        if (this.proxy != null) {
            return this.proxy.getDriverRecordingEvent();
        }
        return null;
    }

    public boolean isAppLaunchAttempted() {
        return this.mAppLaunchAttempted;
    }
}
