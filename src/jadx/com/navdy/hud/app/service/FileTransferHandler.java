package com.navdy.hud.app.service;

public class FileTransferHandler {
    public static final java.lang.String ACTION_OTA_DOWNLOAD = "com.navdy.hud.app.service.OTA_DOWNLOAD";
    public static final java.lang.String OTA_DOWLOADING_PROPERTY = "navdy.ota.downloading";
    static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.service.FileTransferHandler.class);
    private boolean closed;
    private android.content.Context context;
    com.navdy.service.library.file.IFileTransferAuthority fileTransferAuthority = com.navdy.hud.app.storage.PathManager.getInstance();
    com.navdy.service.library.file.FileTransferSessionManager fileTransferManager;
    /* access modifiers changed from: private */
    public final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(getClass());
    com.navdy.service.library.device.RemoteDevice remoteDevice;

    class Anon1 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.service.library.events.file.FileTransferRequest val$request;

        Anon1(com.navdy.service.library.events.file.FileTransferRequest fileTransferRequest) {
            this.val$request = fileTransferRequest;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
            com.navdy.hud.app.service.FileTransferHandler.sLogger.e("Error sending the chunk because data was null");
         */
        public void run() {
            com.navdy.service.library.events.file.FileTransferData data;
            if (com.navdy.hud.app.service.FileTransferHandler.this.remoteDevice.getLinkBandwidthLevel() <= 0) {
                com.navdy.hud.app.service.FileTransferHandler.sLogger.d("Link bandwidth is low");
                if (this.val$request != null) {
                    com.navdy.hud.app.service.FileTransferHandler.sLogger.d("FileTransferRequest (pull) : rejecting file transfer request to avoid link traffic");
                    com.navdy.hud.app.service.FileTransferHandler.this.remoteDevice.postEvent((com.squareup.wire.Message) new com.navdy.service.library.events.file.FileTransferResponse.Builder().success(java.lang.Boolean.valueOf(false)).destinationFileName(this.val$request.destinationFileName).fileType(this.val$request.fileType).error(com.navdy.service.library.events.file.FileTransferError.FILE_TRANSFER_HOST_BUSY).build());
                    return;
                }
                return;
            }
            com.navdy.service.library.events.file.FileTransferResponse fileTransferResponse = com.navdy.hud.app.service.FileTransferHandler.this.fileTransferManager.handleFileTransferRequest(this.val$request);
            com.navdy.hud.app.service.FileTransferHandler.this.remoteDevice.postEvent((com.squareup.wire.Message) fileTransferResponse);
            if (fileTransferResponse.success.booleanValue()) {
                int mTransferId = fileTransferResponse.transferId.intValue();
                try {
                    if (!fileTransferResponse.supportsAcks.booleanValue()) {
                        while (true) {
                            data = com.navdy.hud.app.service.FileTransferHandler.this.sendNextChunk(mTransferId);
                            if (data == null) {
                                break;
                            }
                            java.lang.Thread.sleep(1000);
                            if (data != null) {
                                if (data.lastChunk.booleanValue()) {
                                    break;
                                }
                            } else {
                                break;
                            }
                        }
                        if (data != null && data.lastChunk.booleanValue()) {
                            com.navdy.hud.app.service.FileTransferHandler.this.fileTransferAuthority.onFileSent(this.val$request.fileType);
                            return;
                        }
                        return;
                    }
                    if (com.navdy.hud.app.service.FileTransferHandler.this.sendNextChunk(mTransferId) != null) {
                        com.navdy.hud.app.service.FileTransferHandler.sLogger.d("Send the first chunk");
                    } else {
                        com.navdy.hud.app.service.FileTransferHandler.sLogger.e("Error sending the first chunk because data was null");
                    }
                    com.navdy.hud.app.service.FileTransferHandler.sLogger.d("Flow control enabled, waiting for the FileTransferStatus");
                } catch (Throwable t) {
                    com.navdy.hud.app.service.FileTransferHandler.sLogger.d("Exception while reading the data and sending it across", t);
                }
            }
        }
    }

    class Anon2 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.service.library.events.file.FileTransferRequest val$request;

        Anon2(com.navdy.service.library.events.file.FileTransferRequest fileTransferRequest) {
            this.val$request = fileTransferRequest;
        }

        public void run() {
            com.navdy.hud.app.service.FileTransferHandler.this.logger.v("onFileTransferRequest");
            if (com.navdy.hud.app.service.FileTransferHandler.this.remoteDevice.getLinkBandwidthLevel() <= 0) {
                com.navdy.hud.app.service.FileTransferHandler.sLogger.d("Link bandwidth is low");
                if (this.val$request != null) {
                    com.navdy.hud.app.service.FileTransferHandler.sLogger.d("FileTransferRequest (PUSH) : rejecting file transfer request to avoid link traffic");
                    com.navdy.hud.app.service.FileTransferHandler.this.remoteDevice.postEvent((com.squareup.wire.Message) new com.navdy.service.library.events.file.FileTransferResponse.Builder().success(java.lang.Boolean.valueOf(false)).destinationFileName(this.val$request.destinationFileName).fileType(this.val$request.fileType).error(com.navdy.service.library.events.file.FileTransferError.FILE_TRANSFER_HOST_BUSY).build());
                    return;
                }
                return;
            }
            com.navdy.hud.app.service.FileTransferHandler.this.remoteDevice.postEvent((com.squareup.wire.Message) com.navdy.hud.app.service.FileTransferHandler.this.fileTransferManager.handleFileTransferRequest(this.val$request));
        }
    }

    class Anon3 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.service.library.events.file.FileTransferData val$request;

        Anon3(com.navdy.service.library.events.file.FileTransferData fileTransferData) {
            this.val$request = fileTransferData;
        }

        public void run() {
            boolean z = false;
            com.navdy.hud.app.service.FileTransferHandler.this.logger.v("onFileTransferData");
            if (com.navdy.hud.app.service.FileTransferHandler.this.remoteDevice.getLinkBandwidthLevel() <= 0) {
                com.navdy.hud.app.service.FileTransferHandler.this.remoteDevice.postEvent((com.squareup.wire.Message) new com.navdy.service.library.events.file.FileTransferStatus.Builder().success(java.lang.Boolean.valueOf(false)).transferComplete(java.lang.Boolean.valueOf(false)).error(com.navdy.service.library.events.file.FileTransferError.FILE_TRANSFER_HOST_BUSY).build());
                return;
            }
            java.lang.String absolutePath = com.navdy.hud.app.service.FileTransferHandler.this.fileTransferManager.absolutePathForTransferId(this.val$request.transferId.intValue());
            com.navdy.service.library.events.file.FileTransferStatus fileTransferStatus = com.navdy.hud.app.service.FileTransferHandler.this.fileTransferManager.handleFileTransferData(this.val$request);
            com.navdy.hud.app.service.FileTransferHandler.this.remoteDevice.postEvent((com.squareup.wire.Message) fileTransferStatus);
            com.navdy.hud.app.service.FileTransferHandler fileTransferHandler = com.navdy.hud.app.service.FileTransferHandler.this;
            if (fileTransferStatus.success.booleanValue() && !fileTransferStatus.transferComplete.booleanValue()) {
                z = true;
            }
            fileTransferHandler.setDownloadingProperty(z);
            if (absolutePath != null && !absolutePath.equals("") && fileTransferStatus.transferComplete.booleanValue()) {
                com.navdy.hud.app.service.FileTransferHandler.this.onFileTransferDone(absolutePath);
            }
        }
    }

    class Anon4 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.service.library.events.file.FileTransferStatus val$fileTransferStatus;

        Anon4(com.navdy.service.library.events.file.FileTransferStatus fileTransferStatus) {
            this.val$fileTransferStatus = fileTransferStatus;
        }

        public void run() {
            if (this.val$fileTransferStatus == null) {
                return;
            }
            if (!this.val$fileTransferStatus.success.booleanValue() || !this.val$fileTransferStatus.transferComplete.booleanValue()) {
                try {
                    if (com.navdy.hud.app.service.FileTransferHandler.this.remoteDevice.getLinkBandwidthLevel() <= 0) {
                        com.navdy.hud.app.service.FileTransferHandler.sLogger.d("Link bandwidth is low, Not sending the next chunk. Ending the file transfer");
                        com.navdy.hud.app.service.FileTransferHandler.this.fileTransferManager.endFileTransferSession(this.val$fileTransferStatus.transferId.intValue(), true);
                    } else if (!com.navdy.hud.app.service.FileTransferHandler.this.fileTransferManager.handleFileTransferStatus(this.val$fileTransferStatus)) {
                        com.navdy.hud.app.service.FileTransferHandler.this.fileTransferManager.endFileTransferSession(this.val$fileTransferStatus.transferId.intValue(), true);
                    } else if (com.navdy.hud.app.service.FileTransferHandler.this.sendNextChunk(this.val$fileTransferStatus.transferId.intValue()) != null) {
                        com.navdy.hud.app.service.FileTransferHandler.sLogger.d("Sent chunk");
                    } else {
                        com.navdy.hud.app.service.FileTransferHandler.sLogger.e("Failed to get the data for the transfer session");
                    }
                } catch (Throwable throwable) {
                    com.navdy.hud.app.service.FileTransferHandler.sLogger.d("Exception while reading the data and sending it across", throwable);
                }
            } else {
                com.navdy.hud.app.service.FileTransferHandler.sLogger.d("File Transfer complete " + this.val$fileTransferStatus.transferId);
                com.navdy.service.library.events.file.FileType fileType = com.navdy.hud.app.service.FileTransferHandler.this.fileTransferManager.getFileType(this.val$fileTransferStatus.transferId.intValue());
                com.navdy.hud.app.service.FileTransferHandler.this.fileTransferManager.endFileTransferSession(this.val$fileTransferStatus.transferId.intValue(), true);
                if (fileType != null) {
                    com.navdy.hud.app.service.FileTransferHandler.this.fileTransferAuthority.onFileSent(fileType);
                } else {
                    com.navdy.hud.app.service.FileTransferHandler.sLogger.e("Cannot find FileType associated with transfer ID :" + this.val$fileTransferStatus.transferId);
                }
            }
        }
    }

    class Anon5 implements java.lang.Runnable {
        final /* synthetic */ java.lang.String val$absolutePath;

        Anon5(java.lang.String str) {
            this.val$absolutePath = str;
        }

        public void run() {
            com.navdy.hud.app.service.FileTransferHandler.this.logger.v("onFileTransferDone");
            java.io.File file = new java.io.File(this.val$absolutePath);
            if (!file.exists() || !file.isFile() || !file.canRead()) {
                com.navdy.hud.app.service.FileTransferHandler.this.logger.e("Cannot read from downloaded OTA file " + file.getAbsolutePath());
                return;
            }
            android.content.Intent intent = new android.content.Intent(com.navdy.hud.app.service.FileTransferHandler.ACTION_OTA_DOWNLOAD);
            intent.putExtra("path", file.getAbsolutePath());
            com.navdy.hud.app.HudApplication.getAppContext().sendBroadcastAsUser(intent, android.os.Process.myUserHandle());
        }
    }

    public FileTransferHandler(android.content.Context context2) {
        this.fileTransferManager = new com.navdy.service.library.file.FileTransferSessionManager(context2, this.fileTransferAuthority);
        this.context = context2;
    }

    public void onFileTransferRequest(com.navdy.service.library.events.file.FileTransferRequest request) {
        if (!this.closed) {
            if (request == null || request.fileType == null || request.fileType != com.navdy.service.library.events.file.FileType.FILE_TYPE_LOGS) {
                com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.service.FileTransferHandler.Anon2(request), 5);
            } else {
                com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.service.FileTransferHandler.Anon1(request), 5);
            }
        }
    }

    /* access modifiers changed from: private */
    public com.navdy.service.library.events.file.FileTransferData sendNextChunk(int transferId) throws java.lang.Throwable {
        com.navdy.service.library.events.file.FileTransferData data = this.fileTransferManager.getNextChunk(transferId);
        if (data != null) {
            this.remoteDevice.postEvent((com.squareup.wire.Message) data);
        }
        return data;
    }

    /* access modifiers changed from: private */
    public void setDownloadingProperty(boolean downloading) {
        int time = 0;
        if (downloading) {
            time = (int) (android.os.SystemClock.elapsedRealtime() / 1000);
        }
        com.navdy.hud.app.util.os.SystemProperties.set(OTA_DOWLOADING_PROPERTY, java.lang.String.format("%d", new java.lang.Object[]{java.lang.Integer.valueOf(time)}));
    }

    public void onFileTransferData(com.navdy.service.library.events.file.FileTransferData request) {
        if (!this.closed) {
            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.service.FileTransferHandler.Anon3(request), 5);
        }
    }

    public void onFileTransferStatus(com.navdy.service.library.events.file.FileTransferStatus fileTransferStatus) {
        if (!this.closed) {
            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.service.FileTransferHandler.Anon4(fileTransferStatus), 5);
        }
    }

    /* access modifiers changed from: private */
    public synchronized void onFileTransferDone(java.lang.String absolutePath) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.service.FileTransferHandler.Anon5(absolutePath), 5);
    }

    public void onDeviceConnected(com.navdy.service.library.device.RemoteDevice device) {
        this.remoteDevice = device;
    }

    public void onDeviceDisconnected() {
        this.remoteDevice = null;
    }

    public void close() {
        if (!this.closed) {
            this.closed = true;
            if (this.fileTransferManager != null) {
                this.fileTransferManager.stop();
            }
        }
    }
}
