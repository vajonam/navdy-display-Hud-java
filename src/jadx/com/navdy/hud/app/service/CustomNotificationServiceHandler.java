package com.navdy.hud.app.service;

public class CustomNotificationServiceHandler {
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.service.CustomNotificationServiceHandler.class);
    com.squareup.otto.Bus bus;

    class Anon1 implements com.navdy.hud.app.maps.here.HerePlacesManager.OnCategoriesSearchListener {
        final /* synthetic */ long val$t1;

        Anon1(long j) {
            this.val$t1 = j;
        }

        public void onCompleted(java.util.List<com.here.android.mpa.search.Place> places) {
            try {
                com.navdy.hud.app.service.CustomNotificationServiceHandler.sLogger.v("FIND_GAS_STATION: found gas station (" + places.size() + ") time to find [" + (android.os.SystemClock.elapsedRealtime() - this.val$t1) + "]");
                int counter = 1;
                com.here.android.mpa.common.GeoCoordinate currentPos = com.navdy.hud.app.maps.here.HereMapsManager.getInstance().getRouteStartPoint();
                if (currentPos == null) {
                    currentPos = com.navdy.hud.app.maps.here.HereMapsManager.getInstance().getLocationFixManager().getLastGeoCoordinate();
                } else {
                    com.navdy.hud.app.service.CustomNotificationServiceHandler.sLogger.v("using debug start point:" + currentPos);
                }
                if (currentPos == null) {
                    com.navdy.hud.app.service.CustomNotificationServiceHandler.sLogger.e("FIND_GAS_STATION no current position");
                    return;
                }
                for (com.here.android.mpa.search.Place place : places) {
                    com.here.android.mpa.search.Location location = place.getLocation();
                    com.here.android.mpa.common.GeoCoordinate gasNavGeo = com.navdy.hud.app.maps.here.HerePlacesManager.getPlaceEntry(place);
                    java.lang.String address = location.getAddress().toString().replace("<br/>", ", ").replace(com.navdy.hud.app.framework.glance.GlanceConstants.NEWLINE, "");
                    com.navdy.hud.app.service.CustomNotificationServiceHandler.sLogger.v("FIND_GAS_STATION [" + counter + "]" + " name [" + place.getName() + "]" + " address [" + address + "]" + " distance [" + ((int) currentPos.distanceTo(gasNavGeo)) + "] meters" + " displayPos [" + location.getCoordinate() + "]" + " navPos [" + gasNavGeo + "]");
                    counter++;
                }
            } catch (Throwable t) {
                com.navdy.hud.app.service.CustomNotificationServiceHandler.sLogger.e("FIND_GAS_STATION", t);
            }
        }

        public void onError(com.navdy.hud.app.maps.here.HerePlacesManager.Error error) {
            com.navdy.hud.app.service.CustomNotificationServiceHandler.sLogger.v("FIND_GAS_STATION: could not find gas station:" + error + " time to error [" + (android.os.SystemClock.elapsedRealtime() - this.val$t1) + "]");
        }
    }

    class Anon2 implements com.navdy.hud.app.framework.fuel.FuelRoutingManager.OnNearestGasStationCallback {
        final /* synthetic */ long val$t1;

        Anon2(long j) {
            this.val$t1 = j;
        }

        public void onComplete(com.navdy.service.library.events.navigation.NavigationRouteResult result) {
            com.navdy.hud.app.service.CustomNotificationServiceHandler.sLogger.v("FIND_ROUTE_TO_CLOSEST_GAS_STATION: success: time  [" + (android.os.SystemClock.elapsedRealtime() - this.val$t1) + "]");
            com.navdy.hud.app.service.CustomNotificationServiceHandler.sLogger.v("FIND_ROUTE_TO_CLOSEST_GAS_STATION route id[" + result.routeId + "] label[" + result.label + "] via[" + result.via + "] address[" + (result.address != null ? result.address.replace(com.navdy.hud.app.framework.glance.GlanceConstants.NEWLINE, "") : com.amazonaws.services.s3.internal.Constants.NULL_VERSION_ID) + "] length[" + result.length + "] duration[" + result.duration_traffic + "] freeFlowDuration[" + result.duration + "]");
        }

        public void onError(com.navdy.hud.app.framework.fuel.FuelRoutingManager.OnRouteToGasStationCallback.Error error) {
            com.navdy.hud.app.service.CustomNotificationServiceHandler.sLogger.v("FIND_ROUTE_TO_CLOSEST_GAS_STATION: failed:" + error + " time to error [" + (android.os.SystemClock.elapsedRealtime() - this.val$t1) + "]");
        }
    }

    public CustomNotificationServiceHandler(com.squareup.otto.Bus bus2) {
        this.bus = bus2;
        bus2.register(this);
    }

    public void start() {
    }

    @com.squareup.otto.Subscribe
    public void onShowCustomNotification(com.navdy.service.library.events.notification.ShowCustomNotification event) {
        sLogger.v("onShowCustomNotification: " + event.id);
        com.navdy.hud.app.framework.notifications.NotificationManager instance = com.navdy.hud.app.framework.notifications.NotificationManager.getInstance();
        java.lang.String str = event.id;
        char c = 65535;
        switch (str.hashCode()) {
            case -2039247981:
                if (str.equals("DIAL_BATTERY_LOW")) {
                    c = 14;
                    break;
                }
                break;
            case -1827661852:
                if (str.equals("DIAL_FORGOTTEN_MULTIPLE")) {
                    c = 13;
                    break;
                }
                break;
            case -1814512122:
                if (str.equals("CLEAR_OBD_LOW_FUEL_LEVEL")) {
                    c = ')';
                    break;
                }
                break;
            case -1802230586:
                if (str.equals("CLEAR_TRAFFIC_INCIDENT")) {
                    c = 26;
                    break;
                }
                break;
            case -1735435934:
                if (str.equals("CLEAR_TRAFFIC_REROUTE")) {
                    c = 22;
                    break;
                }
                break;
            case -1614526796:
                if (str.equals("OBD_LOW_FUEL_LEVEL")) {
                    c = '(';
                    break;
                }
                break;
            case -1527103628:
                if (str.equals("TRAFFIC_REROUTE")) {
                    c = 21;
                    break;
                }
                break;
            case -1481455415:
                if (str.equals("CLEAR_ALL_TOAST_AND_CURRENT")) {
                    c = kotlin.text.Typography.quote;
                    break;
                }
                break;
            case -1472431602:
                if (str.equals("LOW_FUEL_LEVEL_NOCHECK")) {
                    c = kotlin.text.Typography.amp;
                    break;
                }
                break;
            case -1338282068:
                if (str.equals("DIAL_BATTERY_VERY_LOW")) {
                    c = 16;
                    break;
                }
                break;
            case -1284131530:
                if (str.equals("GOOGLE_CALENDAR_1")) {
                    c = '#';
                    break;
                }
                break;
            case -1251325556:
                if (str.equals("PHONE_DISCONNECTED")) {
                    c = 29;
                    break;
                }
                break;
            case -1221193815:
                if (str.equals("GENERIC_1")) {
                    c = 0;
                    break;
                }
                break;
            case -1221193814:
                if (str.equals("GENERIC_2")) {
                    c = 1;
                    break;
                }
                break;
            case -1163820707:
                if (str.equals("INCOMING_PHONE_CALL_323")) {
                    c = 2;
                    break;
                }
                break;
            case -930941457:
                if (str.equals("CLEAR_TRAFFIC_DELAY")) {
                    c = 28;
                    break;
                }
                break;
            case -767590304:
                if (str.equals("FIND_ROUTE_TO_CLOSEST_GAS_STATION")) {
                    c = ',';
                    break;
                }
                break;
            case -638027464:
                if (str.equals("PHONE_CONNECTED")) {
                    c = 30;
                    break;
                }
                break;
            case -406264442:
                if (str.equals("LOW_FUEL_LEVEL")) {
                    c = '%';
                    break;
                }
                break;
            case -342876771:
                if (str.equals("DIAL_BATTERY_OK")) {
                    c = 17;
                    break;
                }
                break;
            case -63686527:
                if (str.equals("TRAFFIC_DELAY")) {
                    c = 27;
                    break;
                }
                break;
            case -11797496:
                if (str.equals("FIND_GAS_STATION")) {
                    c = '+';
                    break;
                }
                break;
            case -5849545:
                if (str.equals("CLEAR_ALL_TOAST")) {
                    c = '!';
                    break;
                }
                break;
            case 43099391:
                if (str.equals("PHONE_BATTERY_OK")) {
                    c = 6;
                    break;
                }
                break;
            case 60514076:
                if (str.equals("DIAL_FORGOTTEN_SINGLE")) {
                    c = 12;
                    break;
                }
                break;
            case 73725445:
                if (str.equals("MUSIC")) {
                    c = 18;
                    break;
                }
                break;
            case 83953026:
                if (str.equals("CLEAR_TRAFFIC_JAM")) {
                    c = 24;
                    break;
                }
                break;
            case 204001803:
                if (str.equals("PHONE_BATTERY_EXTREMELY_LOW")) {
                    c = 5;
                    break;
                }
                break;
            case 333419062:
                if (str.equals("VOICE_ASSIST")) {
                    c = 19;
                    break;
                }
                break;
            case 361103604:
                if (str.equals("TRAFFIC_INCIDENT")) {
                    c = 25;
                    break;
                }
                break;
            case 518374548:
                if (str.equals("TRAFFIC_JAM")) {
                    c = 23;
                    break;
                }
                break;
            case 531351226:
                if (str.equals("FUEL_ADDED_TEST")) {
                    c = '*';
                    break;
                }
                break;
            case 634551002:
                if (str.equals("TEXT_NOTIFICAION_WITH_NO_REPLY_510")) {
                    c = 8;
                    break;
                }
                break;
            case 993806900:
                if (str.equals("LOW_FUEL_LEVEL_CLEAR")) {
                    c = '\'';
                    break;
                }
                break;
            case 998768680:
                if (str.equals("END_PHONE_CALL_323")) {
                    c = 3;
                    break;
                }
                break;
            case 1033071277:
                if (str.equals("DIAL_BATTERY_EXTREMELY_LOW")) {
                    c = 15;
                    break;
                }
                break;
            case 1133254737:
                if (str.equals("BRIGHTNESS")) {
                    c = 20;
                    break;
                }
                break;
            case 1225163157:
                if (str.equals("APPLE_CALENDAR_1")) {
                    c = kotlin.text.Typography.dollar;
                    break;
                }
                break;
            case 1314336394:
                if (str.equals("PHONE_APP_DISCONNECTED")) {
                    c = 31;
                    break;
                }
                break;
            case 1336078449:
                if (str.equals("PHONE_BATTERY_LOW")) {
                    c = 4;
                    break;
                }
                break;
            case 1427731674:
                if (str.equals("DIAL_CONNECTED")) {
                    c = 10;
                    break;
                }
                break;
            case 1456922895:
                if (str.equals("CLEAR_CURRENT_TOAST")) {
                    c = com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.SPACE_CHAR;
                    break;
                }
                break;
            case 1483755310:
                if (str.equals("TEXT_NOTIFICAION_WITH_REPLY_408")) {
                    c = 7;
                    break;
                }
                break;
            case 1483760395:
                if (str.equals("TEXT_NOTIFICAION_WITH_REPLY_999")) {
                    c = 9;
                    break;
                }
                break;
            case 1487737514:
                if (str.equals("DIAL_DISCONNECTED")) {
                    c = 11;
                    break;
                }
                break;
        }
        switch (c) {
            case 0:
                java.util.ArrayList arrayList = new java.util.ArrayList();
                arrayList.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.GenericConstants.GENERIC_TITLE.name(), "John Doe"));
                arrayList.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.GenericConstants.GENERIC_MESSAGE.name(), "Message Test"));
                arrayList.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.GenericConstants.GENERIC_MAIN_ICON.name(), com.navdy.service.library.events.glances.GlanceIconConstants.GLANCE_ICON_NAVDY_MAIN.name()));
                arrayList.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.GenericConstants.GENERIC_SIDE_ICON.name(), com.navdy.service.library.events.glances.GlanceIconConstants.GLANCE_ICON_MESSAGE_SIDE_BLUE.name()));
                this.bus.post(new com.navdy.service.library.events.glances.GlanceEvent.Builder().glanceType(com.navdy.service.library.events.glances.GlanceEvent.GlanceType.GLANCE_TYPE_GENERIC).id(java.util.UUID.randomUUID().toString()).postTime(java.lang.Long.valueOf(java.lang.System.currentTimeMillis())).provider("blah").glanceData(arrayList).build());
                return;
            case 1:
                java.util.ArrayList arrayList2 = new java.util.ArrayList();
                arrayList2.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.GenericConstants.GENERIC_TITLE.name(), "Jone Doe"));
                arrayList2.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.GenericConstants.GENERIC_MESSAGE.name(), "Message Test"));
                this.bus.post(new com.navdy.service.library.events.glances.GlanceEvent.Builder().glanceType(com.navdy.service.library.events.glances.GlanceEvent.GlanceType.GLANCE_TYPE_GENERIC).id(java.util.UUID.randomUUID().toString()).postTime(java.lang.Long.valueOf(java.lang.System.currentTimeMillis())).provider("blah").glanceData(arrayList2).build());
                return;
            case 2:
                this.bus.post(new com.navdy.service.library.events.callcontrol.PhoneEvent.Builder().number("323-222-1111").status(com.navdy.service.library.events.callcontrol.PhoneStatus.PHONE_RINGING).contact_name("Al Jazeera Al Bin Al Salaad").build());
                return;
            case 3:
                this.bus.post(new com.navdy.service.library.events.callcontrol.PhoneEvent.Builder().number("323-222-1111").status(com.navdy.service.library.events.callcontrol.PhoneStatus.PHONE_IDLE).contact_name("Al Jazeera").build());
                return;
            case 4:
                this.bus.post(new com.navdy.service.library.events.callcontrol.PhoneBatteryStatus(com.navdy.service.library.events.callcontrol.PhoneBatteryStatus.BatteryStatus.BATTERY_LOW, java.lang.Integer.valueOf(12), java.lang.Boolean.valueOf(false)));
                return;
            case 5:
                this.bus.post(new com.navdy.service.library.events.callcontrol.PhoneBatteryStatus(com.navdy.service.library.events.callcontrol.PhoneBatteryStatus.BatteryStatus.BATTERY_EXTREMELY_LOW, java.lang.Integer.valueOf(4), java.lang.Boolean.valueOf(false)));
                return;
            case 6:
                this.bus.post(new com.navdy.service.library.events.callcontrol.PhoneBatteryStatus(com.navdy.service.library.events.callcontrol.PhoneBatteryStatus.BatteryStatus.BATTERY_OK, java.lang.Integer.valueOf(36), java.lang.Boolean.valueOf(false)));
                return;
            case 7:
                java.util.List<com.navdy.service.library.events.glances.GlanceEvent.GlanceActions> actionsList = new java.util.ArrayList<>(1);
                actionsList.add(com.navdy.service.library.events.glances.GlanceEvent.GlanceActions.REPLY);
                java.util.ArrayList arrayList3 = new java.util.ArrayList();
                arrayList3.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.MessageConstants.MESSAGE_FROM.name(), "John Doe"));
                arrayList3.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.MessageConstants.MESSAGE_FROM_NUMBER.name(), "4081111111"));
                arrayList3.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.MessageConstants.MESSAGE_BODY.name(), "Hi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing"));
                arrayList3.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.MessageConstants.MESSAGE_IS_SMS.name(), ""));
                this.bus.post(new com.navdy.service.library.events.glances.GlanceEvent.Builder().glanceType(com.navdy.service.library.events.glances.GlanceEvent.GlanceType.GLANCE_TYPE_MESSAGE).id(java.util.UUID.randomUUID().toString()).postTime(java.lang.Long.valueOf(java.lang.System.currentTimeMillis())).provider(com.navdy.hud.app.framework.glance.GlanceHelper.SMS_PACKAGE).actions(actionsList).glanceData(arrayList3).build());
                return;
            case 8:
                java.util.ArrayList arrayList4 = new java.util.ArrayList();
                arrayList4.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.MessageConstants.MESSAGE_FROM.name(), "Santa Singh"));
                arrayList4.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.MessageConstants.MESSAGE_FROM_NUMBER.name(), "999-999-0999"));
                arrayList4.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.MessageConstants.MESSAGE_BODY.name(), "Hi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doinghow are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doinghow are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing"));
                arrayList4.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.MessageConstants.MESSAGE_IS_SMS.name(), ""));
                this.bus.post(new com.navdy.service.library.events.glances.GlanceEvent.Builder().glanceType(com.navdy.service.library.events.glances.GlanceEvent.GlanceType.GLANCE_TYPE_MESSAGE).id(java.util.UUID.randomUUID().toString()).postTime(java.lang.Long.valueOf(java.lang.System.currentTimeMillis())).provider(com.navdy.hud.app.framework.glance.GlanceHelper.SMS_PACKAGE).glanceData(arrayList4).build());
                return;
            case 9:
                java.util.List<com.navdy.service.library.events.glances.GlanceEvent.GlanceActions> actionsList2 = new java.util.ArrayList<>(1);
                actionsList2.add(com.navdy.service.library.events.glances.GlanceEvent.GlanceActions.REPLY);
                java.util.ArrayList arrayList5 = new java.util.ArrayList();
                arrayList5.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.MessageConstants.MESSAGE_FROM.name(), "Barack Obama"));
                arrayList5.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.MessageConstants.MESSAGE_FROM_NUMBER.name(), "9999999999"));
                arrayList5.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.MessageConstants.MESSAGE_BODY.name(), "Hi how are u doing"));
                arrayList5.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.MessageConstants.MESSAGE_IS_SMS.name(), ""));
                this.bus.post(new com.navdy.service.library.events.glances.GlanceEvent.Builder().glanceType(com.navdy.service.library.events.glances.GlanceEvent.GlanceType.GLANCE_TYPE_MESSAGE).id(java.util.UUID.randomUUID().toString()).postTime(java.lang.Long.valueOf(java.lang.System.currentTimeMillis())).provider(com.navdy.hud.app.framework.glance.GlanceHelper.SMS_PACKAGE).actions(actionsList2).glanceData(arrayList5).build());
                return;
            case 10:
                com.navdy.hud.app.device.dial.DialNotification.showConnectedToast();
                return;
            case 11:
                com.navdy.hud.app.device.dial.DialNotification.showDisconnectedToast("Navdy Dial (test)");
                return;
            case 12:
                com.navdy.hud.app.device.dial.DialNotification.showForgottenToast(false, "Navdy Dial (test)");
                return;
            case 13:
                com.navdy.hud.app.device.dial.DialNotification.showForgottenToast(true, "Navdy Dial (AAAA), Navdy Dial (BBBB)");
                return;
            case 14:
                com.navdy.hud.app.device.dial.DialNotification.showLowBatteryToast();
                return;
            case 15:
                com.navdy.hud.app.device.dial.DialNotification.showExtremelyLowBatteryToast();
                return;
            case 16:
                com.navdy.hud.app.device.dial.DialNotification.showVeryLowBatteryToast();
                return;
            case 17:
                com.navdy.hud.app.device.dial.DialNotification.dismissAllBatteryToasts();
                return;
            case 18:
                this.bus.post(new com.navdy.service.library.events.ui.ShowScreen.Builder().screen(com.navdy.service.library.events.ui.Screen.SCREEN_MUSIC).build());
                return;
            case 19:
                this.bus.post(new com.navdy.service.library.events.ui.ShowScreen.Builder().screen(com.navdy.service.library.events.ui.Screen.SCREEN_VOICE_CONTROL).build());
                return;
            case 20:
                this.bus.post(new com.navdy.service.library.events.ui.ShowScreen.Builder().screen(com.navdy.service.library.events.ui.Screen.SCREEN_BRIGHTNESS).build());
                return;
            case 21:
                this.bus.post(new com.navdy.hud.app.maps.MapEvents.TrafficRerouteEvent("Heaven", "Heaven, Hell", 1500, java.lang.System.currentTimeMillis() + 300000, 10000, 0));
                return;
            case 22:
                this.bus.post(new com.navdy.hud.app.maps.MapEvents.TrafficRerouteDismissEvent());
                return;
            case 23:
                this.bus.post(new com.navdy.hud.app.maps.MapEvents.TrafficJamProgressEvent(com.amazonaws.auth.STSAssumeRoleSessionCredentialsProvider.DEFAULT_DURATION_SECONDS));
                return;
            case 24:
                this.bus.post(new com.navdy.hud.app.maps.MapEvents.TrafficJamDismissEvent());
                return;
            case 25:
                this.bus.post(new com.navdy.hud.app.maps.MapEvents.LiveTrafficEvent(com.navdy.hud.app.maps.MapEvents.LiveTrafficEvent.Type.INCIDENT, com.navdy.hud.app.maps.MapEvents.LiveTrafficEvent.Severity.HIGH));
                return;
            case 26:
                this.bus.post(new com.navdy.hud.app.maps.MapEvents.LiveTrafficDismissEvent());
                return;
            case 27:
                this.bus.post(new com.navdy.hud.app.maps.MapEvents.TrafficDelayEvent(420));
                return;
            case 28:
                this.bus.post(new com.navdy.hud.app.maps.MapEvents.TrafficDelayDismissEvent());
                return;
            case 29:
                com.navdy.hud.app.framework.connection.ConnectionNotification.showDisconnectedToast(false);
                return;
            case 30:
                com.navdy.hud.app.framework.connection.ConnectionNotification.showConnectedToast();
                return;
            case 31:
                com.navdy.hud.app.framework.connection.ConnectionNotification.showDisconnectedToast(true);
                return;
            case ' ':
                com.navdy.hud.app.framework.toast.ToastManager.getInstance().dismissCurrentToast();
                return;
            case '!':
                com.navdy.hud.app.framework.toast.ToastManager.getInstance().clearAllPendingToast();
                return;
            case '\"':
                com.navdy.hud.app.framework.toast.ToastManager.getInstance().clearAllPendingToast();
                com.navdy.hud.app.framework.toast.ToastManager.getInstance().dismissCurrentToast();
                return;
            case '#':
                long meetingTime = java.lang.System.currentTimeMillis() + java.util.concurrent.TimeUnit.MINUTES.toMillis(10);
                java.util.ArrayList arrayList6 = new java.util.ArrayList();
                arrayList6.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.CalendarConstants.CALENDAR_TITLE.name(), "Meeting with Obama"));
                arrayList6.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.CalendarConstants.CALENDAR_TIME_STR.name(), "\u200e\u202a12:00 \u2013 12:10 PM\u202c\u200e"));
                arrayList6.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.CalendarConstants.CALENDAR_LOCATION.name(), "575 7th Street, San Francisco, CA 94103"));
                this.bus.post(new com.navdy.service.library.events.glances.GlanceEvent.Builder().glanceType(com.navdy.service.library.events.glances.GlanceEvent.GlanceType.GLANCE_TYPE_CALENDAR).id(java.util.UUID.randomUUID().toString()).postTime(java.lang.Long.valueOf(meetingTime)).provider("com.google.android.calendar").glanceData(arrayList6).build());
                return;
            case '$':
                long meetingTime2 = java.lang.System.currentTimeMillis();
                java.util.ArrayList arrayList7 = new java.util.ArrayList();
                arrayList7.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.CalendarConstants.CALENDAR_TITLE.name(), "Huddle"));
                arrayList7.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.CalendarConstants.CALENDAR_TIME.name(), java.lang.String.valueOf(meetingTime2)));
                arrayList7.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.CalendarConstants.CALENDAR_TIME_STR.name(), "1:30 pm - 1:45 pm"));
                this.bus.post(new com.navdy.service.library.events.glances.GlanceEvent.Builder().glanceType(com.navdy.service.library.events.glances.GlanceEvent.GlanceType.GLANCE_TYPE_CALENDAR).id(java.util.UUID.randomUUID().toString()).postTime(java.lang.Long.valueOf(meetingTime2)).provider(com.navdy.hud.app.profile.NotificationSettings.APP_ID_CALENDAR).glanceData(arrayList7).build());
                return;
            case '%':
                if (com.navdy.hud.app.framework.glance.GlanceHelper.isFuelNotificationEnabled()) {
                    java.util.ArrayList arrayList8 = new java.util.ArrayList();
                    arrayList8.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.FuelConstants.FUEL_LEVEL.name(), "14"));
                    this.bus.post(new com.navdy.service.library.events.glances.GlanceEvent.Builder().glanceType(com.navdy.service.library.events.glances.GlanceEvent.GlanceType.GLANCE_TYPE_FUEL).id(java.util.UUID.randomUUID().toString()).postTime(java.lang.Long.valueOf(java.lang.System.currentTimeMillis())).provider(com.navdy.hud.app.framework.glance.GlanceHelper.FUEL_PACKAGE).glanceData(arrayList8).build());
                    return;
                }
                return;
            case '&':
                java.util.ArrayList arrayList9 = new java.util.ArrayList();
                arrayList9.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.FuelConstants.FUEL_LEVEL.name(), "14"));
                this.bus.post(new com.navdy.service.library.events.glances.GlanceEvent.Builder().glanceType(com.navdy.service.library.events.glances.GlanceEvent.GlanceType.GLANCE_TYPE_FUEL).id(java.util.UUID.randomUUID().toString()).postTime(java.lang.Long.valueOf(java.lang.System.currentTimeMillis())).provider(com.navdy.hud.app.framework.glance.GlanceHelper.FUEL_PACKAGE).glanceData(arrayList9).build());
                return;
            case '\'':
                com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().removeNotification(com.navdy.hud.app.framework.fuel.FuelRoutingManager.LOW_FUEL_ID);
                return;
            case '(':
                this.bus.post(new com.navdy.hud.app.framework.fuel.FuelRoutingManager.TestObdLowFuelLevel());
                return;
            case ')':
                this.bus.post(new com.navdy.hud.app.framework.fuel.FuelRoutingManager.ClearTestObdLowFuelLevel());
                return;
            case '*':
                this.bus.post(new com.navdy.hud.app.framework.fuel.FuelRoutingManager.FuelAddedTestEvent());
                return;
            case '+':
                try {
                    if (!com.navdy.hud.app.maps.here.HereMapsManager.getInstance().isInitialized()) {
                        sLogger.v("FIND_GAS_STATION: here maps not initialized");
                        return;
                    }
                    com.navdy.hud.app.maps.here.HerePlacesManager.handleCategoriesRequest(com.navdy.hud.app.framework.fuel.FuelRoutingManager.GAS_CATEGORY, 3, new com.navdy.hud.app.service.CustomNotificationServiceHandler.Anon1(android.os.SystemClock.elapsedRealtime()));
                    return;
                } catch (Throwable t) {
                    sLogger.e("FIND_GAS_STATION", t);
                    return;
                }
            case ',':
                try {
                    if (!com.navdy.hud.app.maps.here.HereMapsManager.getInstance().isInitialized()) {
                        sLogger.v("FIND_ROUTE_TO_CLOSEST_GAS_STATION: here maps not initialized");
                        return;
                    }
                    com.navdy.hud.app.framework.fuel.FuelRoutingManager.getInstance().findNearestGasStation(new com.navdy.hud.app.service.CustomNotificationServiceHandler.Anon2(android.os.SystemClock.elapsedRealtime()));
                    return;
                } catch (Throwable t2) {
                    sLogger.e("FIND_ROUTE_TO_CLOSEST_GAS_STATION", t2);
                    return;
                }
            default:
                return;
        }
    }
}
