package com.navdy.hud.app.service;

class DeviceSearch implements com.navdy.service.library.device.RemoteDevice.Listener {
    private static final int SEARCH_TIMEOUT = 15000;
    private java.util.Deque<com.navdy.service.library.device.RemoteDevice> connectedDevices = new java.util.ArrayDeque();
    private java.util.Deque<com.navdy.service.library.device.RemoteDevice> connectingDevices = new java.util.ArrayDeque();
    /* access modifiers changed from: private */
    public com.navdy.hud.app.service.DeviceSearch.EventSink eventSink;
    private final android.os.Handler handler = new android.os.Handler();
    protected final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(getClass());
    private java.util.Deque<com.navdy.service.library.device.RemoteDevice> remoteDevices = new java.util.ArrayDeque();
    private long startTime = android.os.SystemClock.elapsedRealtime();
    private boolean started = false;

    class Anon1 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.service.library.device.RemoteDevice val$remoteDevice;

        Anon1(com.navdy.service.library.device.RemoteDevice remoteDevice) {
            this.val$remoteDevice = remoteDevice;
        }

        public void run() {
            this.val$remoteDevice.disconnect();
        }
    }

    class Anon2 implements java.lang.Runnable {
        final /* synthetic */ com.squareup.wire.Message val$message;

        Anon2(com.squareup.wire.Message message) {
            this.val$message = message;
        }

        public void run() {
            com.navdy.hud.app.service.DeviceSearch.this.eventSink.onEvent(this.val$message);
        }
    }

    public interface EventSink {
        void onEvent(com.squareup.wire.Message message);
    }

    public DeviceSearch(android.content.Context context, com.navdy.hud.app.service.DeviceSearch.EventSink eventSink2, boolean parseEvents) {
        this.eventSink = eventSink2;
        java.util.List<com.navdy.service.library.device.connection.ConnectionInfo> infos = com.navdy.service.library.device.RemoteDeviceRegistry.getInstance(context).getPairedConnections();
        for (int i = 0; i < infos.size(); i++) {
            com.navdy.service.library.device.RemoteDevice device = new com.navdy.service.library.device.RemoteDevice(context, (com.navdy.service.library.device.connection.ConnectionInfo) infos.get(i), parseEvents);
            device.addListener(this);
            this.remoteDevices.add(device);
        }
    }

    public void start() {
        if (!this.started) {
            this.started = true;
            sendEvent(new com.navdy.service.library.events.connection.ConnectionStatus(com.navdy.service.library.events.connection.ConnectionStatus.Status.CONNECTION_SEARCH_STARTED, (java.lang.String) null));
        }
    }

    public void stop() {
        if (this.started) {
            this.started = false;
            sendEvent(new com.navdy.service.library.events.connection.ConnectionStatus(com.navdy.service.library.events.connection.ConnectionStatus.Status.CONNECTION_SEARCH_FINISHED, (java.lang.String) null));
        }
    }

    public synchronized boolean next() {
        boolean z = false;
        synchronized (this) {
            if (android.os.SystemClock.elapsedRealtime() - this.startTime > com.glympse.android.lib.StaticConfig.PERMISSION_CHECK_INTERVAL) {
                stop();
            } else if (this.remoteDevices.size() > 0) {
                start();
                com.navdy.service.library.device.RemoteDevice device = (com.navdy.service.library.device.RemoteDevice) this.remoteDevices.removeFirst();
                if (device.connect()) {
                    this.connectingDevices.add(device);
                } else {
                    this.remoteDevices.addLast(device);
                }
                z = true;
            } else if (this.connectingDevices.size() > 0) {
                z = true;
            } else {
                stop();
            }
        }
        return z;
    }

    public synchronized com.navdy.service.library.device.RemoteDevice select(com.navdy.service.library.device.NavdyDeviceId deviceId) {
        com.navdy.service.library.device.RemoteDevice missingDevice;
        java.lang.String bluetoothAddress = deviceId.getBluetoothAddress();
        missingDevice = findDevice(bluetoothAddress, this.remoteDevices);
        com.navdy.service.library.device.RemoteDevice connectingDevice = findDevice(bluetoothAddress, this.connectingDevices);
        com.navdy.service.library.device.RemoteDevice connectedDevice = findDevice(bluetoothAddress, this.connectedDevices);
        if (missingDevice != null) {
            this.remoteDevices.remove(missingDevice);
            missingDevice.removeListener(this);
        } else if (connectingDevice != null) {
            this.connectingDevices.remove(connectingDevice);
            connectingDevice.removeListener(this);
            missingDevice = connectingDevice;
        } else if (connectedDevice != null) {
            this.connectedDevices.remove(connectedDevice);
            connectedDevice.removeListener(this);
            missingDevice = connectedDevice;
        } else {
            missingDevice = null;
        }
        return missingDevice;
    }

    public synchronized void close() {
        stop();
        closeAll(this.connectingDevices);
        closeAll(this.connectedDevices);
    }

    private void closeAll(java.util.Deque<com.navdy.service.library.device.RemoteDevice> queue) {
        for (com.navdy.service.library.device.RemoteDevice device : queue) {
            device.removeListener(this);
            queueDisconnect(device);
        }
    }

    private com.navdy.service.library.device.RemoteDevice findDevice(java.lang.String bluetoothAddress, java.util.Deque<com.navdy.service.library.device.RemoteDevice> queue) {
        for (com.navdy.service.library.device.RemoteDevice remoteDevice : queue) {
            if (remoteDevice.getDeviceId().getBluetoothAddress().equals(bluetoothAddress)) {
                return remoteDevice;
            }
        }
        return null;
    }

    private com.navdy.service.library.device.RemoteDevice findDevice(android.bluetooth.BluetoothDevice device, java.util.Deque<com.navdy.service.library.device.RemoteDevice> queue) {
        return findDevice(device.getAddress(), queue);
    }

    private void queueDisconnect(com.navdy.service.library.device.RemoteDevice remoteDevice) {
        if (remoteDevice != null) {
            this.handler.post(new com.navdy.hud.app.service.DeviceSearch.Anon1(remoteDevice));
        }
    }

    public synchronized void handleDisconnect(android.bluetooth.BluetoothDevice device) {
        queueDisconnect(findDevice(device, this.connectedDevices));
    }

    public synchronized boolean forgetDevice(android.bluetooth.BluetoothDevice device) {
        boolean z = true;
        synchronized (this) {
            this.logger.i("DeviceSearch forgetting:" + device);
            com.navdy.service.library.device.RemoteDevice remoteDevice = findDevice(device, this.connectingDevices);
            if (remoteDevice != null) {
                this.logger.i("DeviceSearch removing device from connecting devices");
                this.connectingDevices.remove(remoteDevice);
                queueDisconnect(remoteDevice);
            } else {
                com.navdy.service.library.device.RemoteDevice remoteDevice2 = findDevice(device, this.remoteDevices);
                if (remoteDevice2 != null) {
                    this.logger.i("DeviceSearch removing device from remote devices");
                    this.remoteDevices.remove(remoteDevice2);
                } else {
                    z = false;
                }
            }
        }
        return z;
    }

    public void onDeviceConnecting(com.navdy.service.library.device.RemoteDevice device) {
    }

    public synchronized void onDeviceConnected(com.navdy.service.library.device.RemoteDevice device) {
        this.logger.i("DeviceSearch connected:" + device.getDeviceId());
        this.connectingDevices.remove(device);
        this.connectedDevices.add(device);
        sendEvent(new com.navdy.service.library.events.connection.ConnectionStatus(com.navdy.service.library.events.connection.ConnectionStatus.Status.CONNECTION_FOUND, device.getDeviceId().toString()));
    }

    public synchronized void onDeviceConnectFailure(com.navdy.service.library.device.RemoteDevice device, com.navdy.service.library.device.connection.Connection.ConnectionFailureCause cause) {
        this.logger.i("DeviceSearch connect failure:" + device.getDeviceId());
        if (this.connectingDevices.remove(device)) {
            this.remoteDevices.addLast(device);
        }
    }

    public synchronized void onDeviceDisconnected(com.navdy.service.library.device.RemoteDevice device, com.navdy.service.library.device.connection.Connection.DisconnectCause cause) {
        this.logger.i("DeviceSearch disconnected:" + device.getDeviceId());
        this.connectedDevices.remove(device);
        this.remoteDevices.addLast(device);
        sendEvent(new com.navdy.service.library.events.connection.ConnectionStatus(com.navdy.service.library.events.connection.ConnectionStatus.Status.CONNECTION_LOST, device.getDeviceId().toString()));
    }

    public void onNavdyEventReceived(com.navdy.service.library.device.RemoteDevice device, byte[] event) {
    }

    public void onNavdyEventReceived(com.navdy.service.library.device.RemoteDevice device, com.navdy.service.library.events.NavdyEvent event) {
    }

    private void sendEvent(com.squareup.wire.Message message) {
        if (this.eventSink != null) {
            this.handler.post(new com.navdy.hud.app.service.DeviceSearch.Anon2(message));
        }
    }
}
