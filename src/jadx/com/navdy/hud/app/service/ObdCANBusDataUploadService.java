package com.navdy.hud.app.service;

public class ObdCANBusDataUploadService extends com.navdy.hud.app.service.S3FileUploadService {
    private static final int MAX_FILES_TO_UPLOAD = 5;
    public static final java.lang.String NAVDY_GESTURE_VIDEOS_BUCKET = "navdy-obd-data";
    private static java.util.concurrent.atomic.AtomicBoolean isUploading = new java.util.concurrent.atomic.AtomicBoolean(false);
    private static boolean mIsInitialized = false;
    private static com.navdy.hud.app.service.S3FileUploadService.Request sCurrentRequest;
    private static com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.service.ObdCANBusDataUploadService.class);
    private static java.lang.String sObdDatFilesFolder;
    private static final com.navdy.hud.app.service.S3FileUploadService.UploadQueue sObdDataFilesUploadQueue = new com.navdy.hud.app.service.S3FileUploadService.UploadQueue();

    public ObdCANBusDataUploadService() {
        super(com.navdy.hud.app.service.ObdCANBusDataUploadService.class.getName());
    }

    public void onCreate() {
        mortar.Mortar.inject(com.navdy.hud.app.HudApplication.getAppContext(), this);
        super.onCreate();
    }

    /* access modifiers changed from: protected */
    public void initialize() {
        initializeIfNecessary();
    }

    /* access modifiers changed from: protected */
    public java.util.concurrent.atomic.AtomicBoolean getIsUploading() {
        return isUploading;
    }

    /* access modifiers changed from: protected */
    public com.navdy.hud.app.service.S3FileUploadService.Request getCurrentRequest() {
        return sCurrentRequest;
    }

    /* access modifiers changed from: protected */
    public void setCurrentRequest(com.navdy.hud.app.service.S3FileUploadService.Request currentRequest) {
        sCurrentRequest = currentRequest;
    }

    /* access modifiers changed from: protected */
    public com.navdy.hud.app.service.S3FileUploadService.UploadQueue getUploadQueue() {
        return sObdDataFilesUploadQueue;
    }

    /* access modifiers changed from: protected */
    public java.lang.String getKeyPrefix(java.io.File file) {
        return "CANBus";
    }

    /* access modifiers changed from: protected */
    public java.lang.String getAWSBucket() {
        return NAVDY_GESTURE_VIDEOS_BUCKET;
    }

    public static void populateFilesQueue(java.util.ArrayList<java.io.File> files, com.navdy.hud.app.service.S3FileUploadService.UploadQueue filesQueue, int maxSize) {
        if (files != null) {
            java.util.Iterator it = files.iterator();
            while (it.hasNext()) {
                java.io.File file = (java.io.File) it.next();
                if (file.isFile()) {
                    filesQueue.add(new com.navdy.hud.app.service.S3FileUploadService.Request(file, null));
                    if (filesQueue.size() == maxSize) {
                        filesQueue.pop();
                    }
                }
            }
        }
    }

    public static void addObdDataFileToQueue(java.io.File file) {
        initializeIfNecessary(file);
        synchronized (sObdDataFilesUploadQueue) {
            sObdDataFilesUploadQueue.add(new com.navdy.hud.app.service.S3FileUploadService.Request(file, ""));
            sLogger.d("Queue size : " + sObdDataFilesUploadQueue.size());
            if (!isUploading.get()) {
                while (sObdDataFilesUploadQueue.size() > 5) {
                    sObdDataFilesUploadQueue.pop();
                }
            }
        }
    }

    private static void initializeIfNecessary() {
        initializeIfNecessary(null);
    }

    private static void initializeIfNecessary(java.io.File fileToIgnore) {
        synchronized (sObdDataFilesUploadQueue) {
            if (!mIsInitialized) {
                sLogger.d("Not initialized , initializing now");
                if (!mIsInitialized) {
                    sObdDatFilesFolder = com.navdy.hud.app.obd.ObdCanBusRecordingPolicy.FILES_TO_UPLOAD_DIRECTORY;
                    java.io.File[] files = new java.io.File(sObdDatFilesFolder).listFiles();
                    java.util.ArrayList<java.io.File> filesToPopulate = new java.util.ArrayList<>();
                    for (java.io.File obdCanBusDataBundle : files) {
                        if (obdCanBusDataBundle.isFile() && obdCanBusDataBundle.getName().endsWith(".zip")) {
                            sLogger.d("ObdCanBus Data bundle :" + obdCanBusDataBundle);
                            filesToPopulate.add(obdCanBusDataBundle);
                        }
                    }
                    populateFilesQueue(filesToPopulate, sObdDataFilesUploadQueue, 5);
                    mIsInitialized = true;
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void uploadFinished(boolean succeeded, java.lang.String path, java.lang.String userTag) {
        if (succeeded) {
            com.navdy.hud.app.obd.ObdManager.getInstance().getObdCanBusRecordingPolicy().onFileUploaded(new java.io.File(path));
        } else {
            sLogger.e("File upload failed " + path);
        }
    }

    public void sync() {
        syncNow();
    }

    public static void syncNow() {
        sLogger.d("synNow");
        android.content.Intent intent = new android.content.Intent(com.navdy.hud.app.HudApplication.getAppContext(), com.navdy.hud.app.service.ObdCANBusDataUploadService.class);
        intent.setAction(com.navdy.hud.app.service.S3FileUploadService.ACTION_SYNC);
        com.navdy.hud.app.HudApplication.getAppContext().startService(intent);
    }

    public void reSchedule() {
        sLogger.d("reschedule");
        scheduleWithDelay(10000);
    }

    public boolean canCompleteRequest(com.navdy.hud.app.service.S3FileUploadService.Request request) {
        return true;
    }

    /* access modifiers changed from: protected */
    public com.navdy.service.library.log.Logger getLogger() {
        return sLogger;
    }

    public static void scheduleWithDelay(long delay) {
        android.content.Intent intent = new android.content.Intent(com.navdy.hud.app.HudApplication.getAppContext(), com.navdy.hud.app.service.ObdCANBusDataUploadService.class);
        intent.setAction(com.navdy.hud.app.service.S3FileUploadService.ACTION_SYNC);
        ((android.app.AlarmManager) com.navdy.hud.app.HudApplication.getAppContext().getSystemService("alarm")).setExact(3, android.os.SystemClock.elapsedRealtime() + delay, android.app.PendingIntent.getService(com.navdy.hud.app.HudApplication.getAppContext(), 123, intent, 268435456));
    }
}
