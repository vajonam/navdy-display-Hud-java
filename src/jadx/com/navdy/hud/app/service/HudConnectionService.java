package com.navdy.hud.app.service;

public class HudConnectionService extends com.navdy.service.library.device.connection.ConnectionService {
    public static final java.lang.String ACTION_DEVICE_FORCE_RECONNECT = "com.navdy.hud.app.force_reconnect";
    private static final java.lang.String APP_TERMINATION_RECONNECT_DELAY_MS = "persist.sys.app_reconnect_ms";
    private static final java.lang.String AUTO_DRIVERECORD_PROP = "persist.sys.driverecord.auto";
    public static final java.lang.String EXTRA_HUD_FORCE_RECONNECT_REASON = "force_reconnect_reason";
    private static final int PROXY_ENTRY_LOCAL_PORT = 3000;
    public static final java.lang.String REASON_CONNECTION_DISCONNECTED = "CONNECTION_DISCONNECTED";
    private android.content.BroadcastReceiver bluetoothReceiver = new com.navdy.hud.app.service.HudConnectionService.Anon3();
    private android.content.BroadcastReceiver debugReceiver;
    private com.navdy.hud.app.service.HudConnectionService.ReconnectRunnable deviceReconnectRunnable = new com.navdy.hud.app.service.HudConnectionService.ReconnectRunnable();
    private com.navdy.hud.app.service.FileTransferHandler fileTransferHandler;
    private final com.navdy.hud.app.device.gps.GpsManager gpsManager = com.navdy.hud.app.device.gps.GpsManager.getInstance();
    private com.navdy.service.library.device.link.LinkManager.LinkFactory iAPLinkFactory = new com.navdy.hud.app.service.HudConnectionService.Anon1();
    private boolean isSimulatingGpsCoordinates;
    private boolean needAutoSearch = true;
    private final com.navdy.hud.app.debug.RouteRecorder routeRecorder = com.navdy.hud.app.debug.RouteRecorder.getInstance();
    /* access modifiers changed from: private */
    public com.navdy.hud.app.service.DeviceSearch search;

    class Anon1 implements com.navdy.service.library.device.link.LinkManager.LinkFactory {
        Anon1() {
        }

        public com.navdy.service.library.device.link.Link build(com.navdy.service.library.device.connection.ConnectionInfo connectionInfo) {
            return new com.navdy.hud.device.connection.iAP2Link(new com.navdy.hud.app.service.ConnectionServiceAnalyticsSupport.IAPListenerReceiver(), com.navdy.hud.app.service.HudConnectionService.this.getApplicationContext());
        }
    }

    class Anon2 extends android.content.BroadcastReceiver {
        Anon2() {
        }

        public void onReceive(android.content.Context context, android.content.Intent intent) {
            com.navdy.hud.app.service.HudConnectionService.this.setBandwidthLevel(intent.getIntExtra(com.navdy.service.library.device.connection.ConnectionService.EXTRA_BANDWIDTH_LEVEL, 1));
        }
    }

    class Anon3 extends android.content.BroadcastReceiver {
        private android.bluetooth.BluetoothDevice cancelledDevice;
        private long cancelledTime;

        Anon3() {
        }

        public void onReceive(android.content.Context context, android.content.Intent intent) {
            java.lang.String action = intent.getAction();
            android.bluetooth.BluetoothDevice device = (android.bluetooth.BluetoothDevice) intent.getParcelableExtra("android.bluetooth.device.extra.DEVICE");
            if (action.equals("android.bluetooth.device.action.BOND_STATE_CHANGED")) {
                int bondState = intent.getIntExtra("android.bluetooth.device.extra.BOND_STATE", Integer.MIN_VALUE);
                com.navdy.hud.app.service.HudConnectionService.this.logger.d("Bond state change - device:" + device + " state:" + bondState);
                if (bondState != 11) {
                    return;
                }
                if ((com.navdy.hud.app.service.HudConnectionService.this.search != null && com.navdy.hud.app.service.HudConnectionService.this.search.forgetDevice(device)) || (com.navdy.hud.app.service.HudConnectionService.this.mRemoteDevice != null && com.navdy.hud.app.service.HudConnectionService.this.mRemoteDevice.getDeviceId().getBluetoothAddress().equals(device.getAddress()))) {
                    com.navdy.hud.app.service.HudConnectionService.this.logger.i("Unexpected bonding with known device - removing bond");
                    com.navdy.hud.app.service.HudConnectionService.this.forgetPairedDevice(device);
                    com.navdy.hud.app.util.BluetoothUtil.cancelBondProcess(device);
                    com.navdy.hud.app.util.BluetoothUtil.removeBond(device);
                    this.cancelledDevice = device;
                    this.cancelledTime = android.os.SystemClock.elapsedRealtime();
                    com.navdy.hud.app.service.HudConnectionService.this.serviceHandler.removeCallbacks(com.navdy.hud.app.service.HudConnectionService.this.reconnectRunnable);
                    com.navdy.hud.app.service.HudConnectionService.this.setActiveDevice(null);
                }
            } else if (action.equals("android.bluetooth.device.action.PAIRING_REQUEST")) {
                if (device.equals(this.cancelledDevice) && android.os.SystemClock.elapsedRealtime() - this.cancelledTime < 250) {
                    com.navdy.hud.app.service.HudConnectionService.this.logger.d("Aborting pairing request for cancelled bonding");
                    abortBroadcast();
                }
                this.cancelledDevice = null;
            } else if (action.equals("android.bluetooth.device.action.ACL_DISCONNECTED") && com.navdy.hud.app.service.HudConnectionService.this.search != null) {
                com.navdy.hud.app.service.HudConnectionService.this.search.handleDisconnect(device);
            }
        }
    }

    class Anon4 implements com.navdy.hud.app.service.DeviceSearch.EventSink {
        Anon4() {
        }

        public void onEvent(com.squareup.wire.Message message) {
            if ((message instanceof com.navdy.service.library.events.connection.ConnectionStatus) && ((com.navdy.service.library.events.connection.ConnectionStatus) message).status == com.navdy.service.library.events.connection.ConnectionStatus.Status.CONNECTION_SEARCH_FINISHED && com.navdy.hud.app.service.HudConnectionService.this.state == com.navdy.service.library.device.connection.ConnectionService.State.SEARCHING) {
                if (com.navdy.hud.app.service.HudConnectionService.this.search != null) {
                    com.navdy.hud.app.service.HudConnectionService.this.search.close();
                    com.navdy.hud.app.service.HudConnectionService.this.search = null;
                }
                com.navdy.hud.app.service.HudConnectionService.this.setState(com.navdy.service.library.device.connection.ConnectionService.State.IDLE);
            }
            com.navdy.hud.app.service.HudConnectionService.this.forwardEventLocally(message);
        }
    }

    class Anon5 implements java.lang.Runnable {
        Anon5() {
        }

        public void run() {
            com.navdy.hud.app.service.HudConnectionService.this.logger.i("Ending search");
            if (com.navdy.hud.app.service.HudConnectionService.this.search != null) {
                com.navdy.hud.app.service.HudConnectionService.this.search.close();
                com.navdy.hud.app.service.HudConnectionService.this.search = null;
            }
            com.navdy.hud.app.service.HudConnectionService.this.setState(com.navdy.service.library.device.connection.ConnectionService.State.IDLE);
        }
    }

    class Anon6 implements com.navdy.service.library.network.SocketFactory {
        Anon6() {
        }

        public com.navdy.service.library.network.SocketAdapter build() throws java.io.IOException {
            if (com.navdy.hud.device.connection.iAP2Link.proxyEASession != null) {
                return new com.navdy.hud.device.connection.EASessionSocketAdapter(com.navdy.hud.device.connection.iAP2Link.proxyEASession);
            }
            if (com.navdy.hud.app.service.HudConnectionService.this.mRemoteDevice != null) {
                return new com.navdy.service.library.network.BTSocketFactory(com.navdy.hud.app.service.HudConnectionService.this.mRemoteDevice.getDeviceId().getBluetoothAddress(), com.navdy.service.library.device.connection.ConnectionService.NAVDY_PROXY_TUNNEL_UUID).build();
            }
            throw new java.io.IOException("can't create proxy tunnel because HUD is not connected to remote device");
        }
    }

    public class ReconnectRunnable implements java.lang.Runnable {
        private java.lang.String reason;

        public ReconnectRunnable() {
        }

        public void run() {
            com.navdy.hud.app.service.HudConnectionService.this.reconnect(this.reason);
        }

        public void setReason(java.lang.String reason2) {
            this.reason = reason2;
        }
    }

    public void onCreate() {
        super.onCreate();
        com.navdy.service.library.device.link.LinkManager.registerFactory(com.navdy.service.library.device.connection.ConnectionType.BT_IAP2_LINK, this.iAPLinkFactory);
        com.navdy.service.library.device.link.LinkManager.registerFactory(com.navdy.service.library.device.connection.ConnectionType.EA_PROTOBUF, this.iAPLinkFactory);
        this.gpsManager.setConnectionService(this);
        this.routeRecorder.setConnectionService(this);
        this.serverMode = true;
        this.inProcess = false;
        com.navdy.service.library.util.NetworkActivityTracker.getInstance().start();
        this.fileTransferHandler = new com.navdy.hud.app.service.FileTransferHandler(getApplicationContext());
        if (!com.navdy.hud.app.util.DeviceUtil.isUserBuild()) {
            android.content.IntentFilter intentFilter = new android.content.IntentFilter();
            intentFilter.addAction(com.navdy.service.library.device.connection.ConnectionService.ACTION_LINK_BANDWIDTH_LEVEL_CHANGED);
            intentFilter.addCategory(com.navdy.service.library.device.connection.ConnectionService.CATEGORY_NAVDY_LINK);
            this.debugReceiver = new com.navdy.hud.app.service.HudConnectionService.Anon2();
            registerReceiver(this.debugReceiver, intentFilter);
        }
    }

    public void onDestroy() {
        super.onDestroy();
        this.routeRecorder.stopRecording(false);
        this.gpsManager.shutdown();
        unregisterReceiver(this.bluetoothReceiver);
        if (this.debugReceiver != null) {
            unregisterReceiver(this.debugReceiver);
        }
    }

    public boolean isPromiscuous() {
        return com.navdy.hud.app.device.PowerManager.isAwake();
    }

    /* access modifiers changed from: protected */
    public com.navdy.service.library.device.discovery.RemoteDeviceBroadcaster[] getRemoteDeviceBroadcasters() {
        if (com.navdy.hud.app.util.DeviceUtil.isNavdyDevice()) {
            return new com.navdy.service.library.device.discovery.RemoteDeviceBroadcaster[]{new com.navdy.service.library.device.discovery.BTDeviceBroadcaster()};
        }
        return new com.navdy.service.library.device.discovery.RemoteDeviceBroadcaster[]{new com.navdy.service.library.device.discovery.BTDeviceBroadcaster(), new com.navdy.service.library.device.discovery.TCPRemoteDeviceBroadcaster(getApplicationContext())};
    }

    /* access modifiers changed from: protected */
    public com.navdy.service.library.device.connection.ConnectionListener[] getConnectionListeners(android.content.Context context) {
        if (com.navdy.hud.app.util.DeviceUtil.isNavdyDevice()) {
            return new com.navdy.service.library.device.connection.ConnectionListener[]{new com.navdy.service.library.device.connection.AcceptorListener(context, new com.navdy.service.library.network.BTSocketAcceptor("Navdy", NAVDY_PROTO_SERVICE_UUID), com.navdy.service.library.device.connection.ConnectionType.BT_PROTOBUF), new com.navdy.service.library.device.connection.AcceptorListener(context, new com.navdy.service.library.network.BTSocketAcceptor(com.navdy.service.library.device.connection.ConnectionService.NAVDY_IAP_NAME, ACCESSORY_IAP2), com.navdy.service.library.device.connection.ConnectionType.BT_IAP2_LINK)};
        }
        return new com.navdy.service.library.device.connection.ConnectionListener[]{new com.navdy.service.library.device.connection.AcceptorListener(context, new com.navdy.service.library.network.TCPSocketAcceptor(com.navdy.service.library.device.connection.TCPConnectionInfo.SERVICE_PORT), com.navdy.service.library.device.connection.ConnectionType.TCP_PROTOBUF), new com.navdy.service.library.device.connection.AcceptorListener(context, new com.navdy.service.library.network.BTSocketAcceptor("Navdy", NAVDY_PROTO_SERVICE_UUID), com.navdy.service.library.device.connection.ConnectionType.BT_PROTOBUF)};
    }

    /* access modifiers changed from: protected */
    public void exitState(com.navdy.service.library.device.connection.ConnectionService.State state) {
        super.exitState(state);
        switch (state) {
            case START:
                android.content.IntentFilter filter = new android.content.IntentFilter("android.bluetooth.device.action.PAIRING_REQUEST");
                filter.setPriority(1);
                filter.addAction("android.bluetooth.device.action.ACL_DISCONNECTED");
                filter.addAction("android.bluetooth.device.action.BOND_STATE_CHANGED");
                registerReceiver(this.bluetoothReceiver, filter);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void enterState(com.navdy.service.library.device.connection.ConnectionService.State state) {
        switch (state) {
            case IDLE:
                if (this.search != null) {
                    setState(com.navdy.service.library.device.connection.ConnectionService.State.SEARCHING);
                    break;
                }
                break;
            case SEARCHING:
                if (isPromiscuous()) {
                    startBroadcasters();
                }
                startListeners();
                if (this.search == null) {
                    this.search = new com.navdy.hud.app.service.DeviceSearch(this, new com.navdy.hud.app.service.HudConnectionService.Anon4(), this.inProcess);
                    this.search.next();
                }
                if (this.mRemoteDevice != null && (this.mRemoteDevice.isConnected() || this.mRemoteDevice.isConnecting())) {
                    setState(com.navdy.service.library.device.connection.ConnectionService.State.DISCONNECTING);
                    break;
                }
            case CONNECTED:
                this.needAutoSearch = false;
                if (this.search != null) {
                    this.search.close();
                    this.search = null;
                }
                if (this.mRemoteDevice != null && this.deviceRegistry.findDevice(this.mRemoteDevice.getDeviceId()) == null) {
                    com.navdy.hud.app.service.ConnectionServiceAnalyticsSupport.recordNewDevice();
                    break;
                }
            case CONNECTING:
            case RECONNECTING:
                this.needAutoSearch = false;
                break;
        }
        super.enterState(state);
    }

    public boolean needAutoSearch() {
        return this.needAutoSearch && com.navdy.hud.app.device.PowerManager.isAwake();
    }

    /* access modifiers changed from: protected */
    public void heartBeat() {
        switch (this.state) {
            case SEARCHING:
                this.search.next();
                break;
        }
        super.heartBeat();
    }

    /* access modifiers changed from: protected */
    public void forgetPairedDevice(android.bluetooth.BluetoothDevice device) {
        if (this.search != null) {
            this.search.forgetDevice(device);
        }
        super.forgetPairedDevice(device);
    }

    public void onDeviceConnected(com.navdy.service.library.device.RemoteDevice device) {
        super.onDeviceConnected(device);
        this.fileTransferHandler.onDeviceConnected(device);
        startProxyService();
    }

    /* access modifiers changed from: protected */
    public void handleDeviceDisconnect(com.navdy.service.library.device.RemoteDevice device, com.navdy.service.library.device.connection.Connection.DisconnectCause cause) {
        super.handleDeviceDisconnect(device, cause);
        this.logger.d("HUDConnectionService: handleDeviceDisconnect " + device + ", Cause :" + cause);
        if (this.mRemoteDevice == device && cause == com.navdy.service.library.device.connection.Connection.DisconnectCause.ABORTED && this.serverMode) {
            this.logger.d("HUDConnectionService: Reporting the non fatal crash, Bluetooth disconnected");
            com.navdy.hud.app.util.CrashReportService.dumpCrashReportAsync(com.navdy.hud.app.util.CrashReportService.CrashType.BLUETOOTH_DISCONNECTED);
        }
    }

    public void onDeviceDisconnected(com.navdy.service.library.device.RemoteDevice device, com.navdy.service.library.device.connection.Connection.DisconnectCause cause) {
        super.onDeviceDisconnected(device, cause);
        this.fileTransferHandler.onDeviceDisconnected();
    }

    /* access modifiers changed from: protected */
    public boolean processEvent(byte[] eventData, com.navdy.service.library.events.NavdyEvent.MessageType messageType) {
        switch (messageType) {
            case Coordinate:
                try {
                    if (this.isSimulatingGpsCoordinates) {
                        return true;
                    }
                    this.gpsManager.feedLocation((com.navdy.service.library.events.location.Coordinate) com.navdy.service.library.events.NavdyEventUtil.messageFromEvent((com.navdy.service.library.events.NavdyEvent) this.mWire.parseFrom(eventData, com.navdy.service.library.events.NavdyEvent.class)));
                    return true;
                } catch (Throwable t) {
                    this.logger.e(t);
                    return true;
                }
            case StartDriveRecordingEvent:
                return parseStartDriveRecordingEvent(eventData);
            case StopDriveRecordingEvent:
                this.routeRecorder.stopRecording(false);
                this.logger.v("onStopDriveRecording");
                return true;
            case DriveRecordingsRequest:
                this.routeRecorder.requestRecordings();
                this.logger.v("onDriveRecordingsRequest");
                return true;
            case StartDrivePlaybackEvent:
                parseStartDrivePlaybackEvent(eventData);
                return false;
            case StopDrivePlaybackEvent:
                this.routeRecorder.stopPlayback();
                this.logger.v("onStopDrivePlayback");
                return false;
            case FileTransferRequest:
                try {
                    this.fileTransferHandler.onFileTransferRequest((com.navdy.service.library.events.file.FileTransferRequest) com.navdy.service.library.events.NavdyEventUtil.messageFromEvent((com.navdy.service.library.events.NavdyEvent) this.mWire.parseFrom(eventData, com.navdy.service.library.events.NavdyEvent.class)));
                    return true;
                } catch (Throwable t2) {
                    this.logger.e(t2);
                    return true;
                }
            case FileTransferStatus:
                try {
                    this.fileTransferHandler.onFileTransferStatus((com.navdy.service.library.events.file.FileTransferStatus) com.navdy.service.library.events.NavdyEventUtil.messageFromEvent((com.navdy.service.library.events.NavdyEvent) this.mWire.parseFrom(eventData, com.navdy.service.library.events.NavdyEvent.class)));
                    return true;
                } catch (Throwable t3) {
                    this.logger.e(t3);
                    return true;
                }
            case FileTransferData:
                try {
                    this.fileTransferHandler.onFileTransferData((com.navdy.service.library.events.file.FileTransferData) com.navdy.service.library.events.NavdyEventUtil.messageFromEvent((com.navdy.service.library.events.NavdyEvent) this.mWire.parseFrom(eventData, com.navdy.service.library.events.NavdyEvent.class)));
                    return true;
                } catch (Throwable t4) {
                    this.logger.e(t4);
                    return true;
                }
            case ConnectionStateChange:
                try {
                    com.navdy.service.library.events.connection.ConnectionStateChange message = (com.navdy.service.library.events.connection.ConnectionStateChange) com.navdy.service.library.events.NavdyEventUtil.messageFromEvent((com.navdy.service.library.events.NavdyEvent) this.mWire.parseFrom(eventData, com.navdy.service.library.events.NavdyEvent.class));
                    switch (message.state) {
                        case CONNECTION_DISCONNECTED:
                            int delay = com.navdy.hud.app.util.os.SystemProperties.getInt(APP_TERMINATION_RECONNECT_DELAY_MS, 1000);
                            this.deviceReconnectRunnable.setReason(REASON_CONNECTION_DISCONNECTED);
                            this.serviceHandler.postDelayed(this.deviceReconnectRunnable, (long) delay);
                            break;
                        case CONNECTION_CONNECTED:
                        case CONNECTION_LINK_LOST:
                            this.serviceHandler.removeCallbacks(this.deviceReconnectRunnable);
                            break;
                    }
                } catch (Throwable t5) {
                    this.logger.e(t5);
                }
                return false;
            case AudioStatus:
                try {
                    com.navdy.service.library.events.audio.AudioStatus message2 = (com.navdy.service.library.events.audio.AudioStatus) com.navdy.service.library.events.NavdyEventUtil.messageFromEvent((com.navdy.service.library.events.NavdyEvent) this.mWire.parseFrom(eventData, com.navdy.service.library.events.NavdyEvent.class));
                    if (message2 != null && message2.profileType == com.navdy.service.library.events.audio.AudioStatus.ProfileType.AUDIO_PROFILE_HFP) {
                        this.logger.d("AudioStatus for HFP received");
                        if (!((java.lang.Boolean) com.squareup.wire.Wire.get(message2.isConnected, java.lang.Boolean.valueOf(false))).booleanValue()) {
                            this.logger.d("AudioStatus: HFP disconnected");
                            setBandwidthLevel(1);
                            break;
                        } else {
                            this.logger.d("AudioStatus: HFP connected");
                            if (!((java.lang.Boolean) com.squareup.wire.Wire.get(message2.hasSCOConnection, java.lang.Boolean.valueOf(false))).booleanValue()) {
                                this.logger.d("AudioStatus: does not have SCO connection");
                                setBandwidthLevel(1);
                                break;
                            } else {
                                this.logger.d("AudioStatus: has SCO connection");
                                setBandwidthLevel(0);
                                break;
                            }
                        }
                    }
                } catch (Throwable t6) {
                    this.logger.e(t6);
                    break;
                }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean processLocalEvent(byte[] eventData, com.navdy.service.library.events.NavdyEvent.MessageType messageType) {
        com.navdy.service.library.events.connection.ConnectionRequest request;
        if (messageType == com.navdy.service.library.events.NavdyEvent.MessageType.ConnectionRequest) {
            try {
                request = (com.navdy.service.library.events.connection.ConnectionRequest) ((com.navdy.service.library.events.NavdyEvent) this.mWire.parseFrom(eventData, com.navdy.service.library.events.NavdyEvent.class)).getExtension(com.navdy.service.library.events.Ext_NavdyEvent.connectionRequest);
            } catch (Throwable t) {
                this.logger.e(t);
            }
            if (request == null) {
                return true;
            }
            switch (request.action) {
                case CONNECTION_SELECT:
                    if (request.remoteDeviceId != null) {
                        selectDevice(new com.navdy.service.library.device.NavdyDeviceId(request.remoteDeviceId));
                        return true;
                    }
                    setActiveDevice(null);
                    return true;
                case CONNECTION_START_SEARCH:
                    setState(com.navdy.service.library.device.connection.ConnectionService.State.SEARCHING);
                    return true;
                case CONNECTION_STOP_SEARCH:
                    this.serviceHandler.post(new com.navdy.hud.app.service.HudConnectionService.Anon5());
                    return true;
                default:
                    return true;
            }
            this.logger.e(t);
            return false;
        } else if (messageType == com.navdy.service.library.events.NavdyEvent.MessageType.StartDrivePlaybackEvent) {
            parseStartDrivePlaybackEvent(eventData);
            forwardEventLocally(eventData);
            return true;
        } else if (messageType == com.navdy.service.library.events.NavdyEvent.MessageType.StopDrivePlaybackEvent) {
            this.routeRecorder.stopPlayback();
            forwardEventLocally(eventData);
            this.logger.v("onStopDrivePlayback");
            return true;
        } else if (messageType == com.navdy.service.library.events.NavdyEvent.MessageType.StartDriveRecordingEvent) {
            return parseStartDriveRecordingEvent(eventData);
        } else {
            if (messageType == com.navdy.service.library.events.NavdyEvent.MessageType.StopDriveRecordingEvent) {
                this.routeRecorder.stopRecording(false);
                forwardEventLocally(eventData);
            }
            return false;
        }
    }

    private boolean parseStartDrivePlaybackEvent(byte[] eventData) {
        boolean z = false;
        try {
            com.navdy.service.library.events.debug.StartDrivePlaybackEvent startDrivePlaybackEvent = (com.navdy.service.library.events.debug.StartDrivePlaybackEvent) com.navdy.service.library.events.NavdyEventUtil.messageFromEvent((com.navdy.service.library.events.NavdyEvent) this.mWire.parseFrom(eventData, com.navdy.service.library.events.NavdyEvent.class));
            com.navdy.hud.app.debug.RouteRecorder routeRecorder2 = this.routeRecorder;
            java.lang.String str = startDrivePlaybackEvent.label;
            if (startDrivePlaybackEvent.playSecondaryLocation != null) {
                z = startDrivePlaybackEvent.playSecondaryLocation.booleanValue();
            }
            routeRecorder2.startPlayback(str, z, false);
            this.logger.v("onStartDrivePlayback, name=" + startDrivePlaybackEvent.label);
        } catch (Throwable t) {
            this.logger.e(t);
        }
        return true;
    }

    private boolean parseStartDriveRecordingEvent(byte[] eventData) {
        try {
            com.navdy.service.library.events.debug.StartDriveRecordingEvent startDriveRecordingEvent = (com.navdy.service.library.events.debug.StartDriveRecordingEvent) com.navdy.service.library.events.NavdyEventUtil.messageFromEvent((com.navdy.service.library.events.NavdyEvent) this.mWire.parseFrom(eventData, com.navdy.service.library.events.NavdyEvent.class));
            if (startDriveRecordingEvent != null) {
                java.lang.String fileName = this.routeRecorder.startRecording(startDriveRecordingEvent.label, false);
                if (!android.text.TextUtils.isEmpty(fileName)) {
                    forwardEventLocally((com.squareup.wire.Message) new com.navdy.service.library.events.debug.StartDriveRecordingEvent(fileName));
                    this.logger.v("onStartDriveRecording, name=" + startDriveRecordingEvent.label);
                } else {
                    this.logger.e("startDriveRecording event fileName is empty");
                }
            } else {
                this.logger.e("startDriveRecording event is null");
            }
        } catch (Throwable t) {
            this.logger.e(t);
        }
        return true;
    }

    private void selectDevice(com.navdy.service.library.device.NavdyDeviceId deviceId) {
        if (this.search != null) {
            this.logger.i("Connecting to " + deviceId);
            com.navdy.service.library.device.RemoteDevice device = this.search.select(deviceId);
            if (device != null) {
                this.logger.i("Found corresponding remote device:" + device);
                setActiveDevice(device);
                return;
            }
            return;
        }
        com.navdy.service.library.device.connection.ConnectionInfo info = this.deviceRegistry.findDevice(deviceId);
        if (info != null) {
            this.logger.i("Found corresponding connection info:" + info);
            connect(info);
        }
    }

    /* access modifiers changed from: protected */
    public com.navdy.service.library.device.connection.tunnel.Tunnel createProxyService() throws java.io.IOException {
        return new com.navdy.service.library.device.connection.tunnel.Tunnel(new com.navdy.service.library.network.TCPSocketAcceptor(3000, true), new com.navdy.hud.app.service.HudConnectionService.Anon6());
    }

    public void setSimulatingGpsCoordinates(boolean isSimulatingGpsCoordinates2) {
        this.isSimulatingGpsCoordinates = isSimulatingGpsCoordinates2;
    }

    /* access modifiers changed from: protected */
    public void sendEventsOnLocalConnect() {
        this.logger.v("sendEventsOnLocalConnect");
        com.navdy.hud.app.debug.RouteRecorder routeRecorder2 = com.navdy.hud.app.debug.RouteRecorder.getInstance();
        if (routeRecorder2.isRecording()) {
            java.lang.String label = routeRecorder2.getLabel();
            if (label != null) {
                forwardEventLocally((com.squareup.wire.Message) new com.navdy.service.library.events.debug.StartDriveRecordingEvent(label));
                this.logger.v("sendEventsOnLocalConnect: send recording event:" + label);
                return;
            }
            this.logger.v("sendEventsOnLocalConnect: invalid label");
        }
    }

    public synchronized void reconnect(java.lang.String reason) {
        broadcastReconnectingIntent(reason);
        super.reconnect(reason);
    }

    public com.navdy.service.library.events.DeviceInfo.Platform getDevicePlatform() {
        try {
            if (this.mRemoteDevice == null || !this.mRemoteDevice.isConnected()) {
                return null;
            }
            return this.mRemoteDevice.getDeviceInfo().platform;
        } catch (Throwable th) {
            return null;
        }
    }

    public void broadcastReconnectingIntent(java.lang.String reason) {
        android.content.Intent intent = new android.content.Intent(ACTION_DEVICE_FORCE_RECONNECT);
        intent.putExtra(EXTRA_HUD_FORCE_RECONNECT_REASON, reason);
        sendBroadcast(intent);
    }

    public boolean reconnectAfterDeadConnection() {
        return true;
    }
}
