package com.navdy.hud.app.service;

public class RemoteDeviceProxy extends com.navdy.service.library.device.RemoteDevice {
    private volatile int bandwidthLevel = 1;
    private boolean connected;
    protected com.navdy.hud.app.service.ConnectionServiceProxy proxy;

    class Anon1 implements com.navdy.service.library.device.RemoteDevice.EventDispatcher {
        final /* synthetic */ com.navdy.service.library.events.NavdyEvent val$event;

        Anon1(com.navdy.service.library.events.NavdyEvent navdyEvent) {
            this.val$event = navdyEvent;
        }

        public void dispatchEvent(com.navdy.service.library.device.RemoteDevice source, com.navdy.service.library.device.RemoteDevice.Listener listener) {
            listener.onNavdyEventReceived(source, this.val$event);
        }
    }

    public RemoteDeviceProxy(com.navdy.hud.app.service.ConnectionServiceProxy proxy2, android.content.Context context, com.navdy.service.library.device.NavdyDeviceId deviceId) {
        super(context, deviceId, true);
        this.proxy = proxy2;
        this.proxy.getBus().register(this);
        this.bandwidthLevel = 1;
        this.connected = true;
    }

    @com.squareup.otto.Subscribe
    public void onConnectionStateChange(com.navdy.service.library.events.connection.ConnectionStateChange event) {
        switch (event.state) {
            case CONNECTION_CONNECTED:
                this.connected = true;
                dispatchConnectEvent();
                return;
            case CONNECTION_DISCONNECTED:
                this.connected = false;
                dispatchDisconnectEvent(com.navdy.service.library.device.connection.Connection.DisconnectCause.NORMAL);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void dispatchNavdyEvent(com.navdy.service.library.events.NavdyEvent event) {
        dispatchToListeners(new com.navdy.hud.app.service.RemoteDeviceProxy.Anon1(event));
    }

    @com.squareup.otto.Subscribe
    public void onNavdyEvent(com.navdy.service.library.events.NavdyEvent event) {
        dispatchNavdyEvent(event);
    }

    public boolean postEvent(com.navdy.service.library.events.NavdyEvent event) {
        this.proxy.postRemoteEvent(getDeviceId(), event);
        return true;
    }

    public boolean postEvent(com.navdy.service.library.events.NavdyEvent event, com.navdy.service.library.device.RemoteDevice.PostEventHandler postEventHandler) {
        if (postEventHandler != null) {
            notSupported();
            return false;
        }
        this.proxy.postRemoteEvent(getDeviceId(), event);
        return true;
    }

    public boolean connect() {
        notSupported();
        return false;
    }

    public boolean disconnect() {
        notSupported();
        return false;
    }

    public boolean isConnected() {
        return this.connected;
    }

    private void notSupported() {
        throw new java.lang.UnsupportedOperationException("Can't call this method on remoteDeviceProxy");
    }

    public void setLinkBandwidthLevel(int bandwidthLevel2) {
        this.bandwidthLevel = bandwidthLevel2;
    }

    public int getLinkBandwidthLevel() {
        return this.bandwidthLevel;
    }
}
