package com.navdy.hud.app.service;

public class ConnectionServiceProxy {
    private static final boolean VERBOSE = true;
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.service.ConnectionServiceProxy.class);
    protected com.squareup.otto.Bus bus;
    private com.navdy.service.library.events.debug.StartDriveRecordingEvent driveRecordingEvent;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.IEventListener.Stub eventListener = new com.navdy.hud.app.service.ConnectionServiceProxy.Anon3();
    protected android.content.Context mContext;
    protected com.navdy.hud.app.IEventSource mEventSource;
    protected com.squareup.wire.Wire mWire;
    private android.content.ServiceConnection serviceConnection = new com.navdy.hud.app.service.ConnectionServiceProxy.Anon2();

    class Anon1 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.service.library.device.NavdyDeviceId val$deviceId;
        final /* synthetic */ com.navdy.service.library.events.NavdyEvent val$event;

        Anon1(com.navdy.service.library.events.NavdyEvent navdyEvent, com.navdy.service.library.device.NavdyDeviceId navdyDeviceId) {
            this.val$event = navdyEvent;
            this.val$deviceId = navdyDeviceId;
        }

        public void run() {
            if (com.navdy.hud.app.service.ConnectionServiceProxy.this.mEventSource != null) {
                try {
                    long l1 = android.os.SystemClock.elapsedRealtime();
                    byte[] eventData = this.val$event.toByteArray();
                    long l2 = android.os.SystemClock.elapsedRealtime();
                    int len = eventData.length;
                    if (com.navdy.hud.app.service.ConnectionServiceProxy.sLogger.isLoggable(2)) {
                        com.navdy.hud.app.service.ConnectionServiceProxy.sLogger.v("NAVDY-PACKET [H2P-Outgoing-Event] " + this.val$event);
                    }
                    long time = l2 - l1;
                    if (time >= 100) {
                        com.navdy.hud.app.service.ConnectionServiceProxy.sLogger.v("NAVDY-PACKET [H2P-Outgoing-Event]" + this.val$event.type.name() + " took: " + time + " len:" + len);
                    }
                    com.navdy.hud.app.service.ConnectionServiceProxy.this.mEventSource.postRemoteEvent(this.val$deviceId.toString(), eventData);
                } catch (android.os.DeadObjectException ex) {
                    com.navdy.hud.app.service.ConnectionServiceProxy.sLogger.e("Exception posting remote event, server died", ex);
                } catch (android.os.TransactionTooLargeException e) {
                    com.navdy.hud.app.service.ConnectionServiceProxy.sLogger.e("Exception posting remote event, large payload type[" + this.val$event.type + "] size[" + -1 + "]", e);
                } catch (Throwable t) {
                    com.navdy.hud.app.service.ConnectionServiceProxy.sLogger.e("Exception posting remote event", t);
                }
            } else {
                com.navdy.hud.app.service.ConnectionServiceProxy.sLogger.e("Failed to send event - service connection broken" + this.val$event);
            }
        }
    }

    class Anon2 implements android.content.ServiceConnection {
        Anon2() {
        }

        public void onServiceConnected(android.content.ComponentName name, android.os.IBinder service) {
            com.navdy.hud.app.service.ConnectionServiceProxy.sLogger.i("Connected to IEventSource service");
            com.navdy.hud.app.service.ConnectionServiceProxy.this.mEventSource = com.navdy.hud.app.IEventSource.Stub.asInterface(service);
            try {
                com.navdy.hud.app.service.ConnectionServiceProxy.this.mEventSource.addEventListener(com.navdy.hud.app.service.ConnectionServiceProxy.this.eventListener);
            } catch (android.os.RemoteException e) {
                com.navdy.hud.app.service.ConnectionServiceProxy.sLogger.e("Failed to connect event listener", e);
            }
            com.navdy.hud.app.service.ConnectionServiceProxy.sLogger.d("Starting connection service state machine");
            com.navdy.hud.app.service.ConnectionServiceProxy.this.mContext.startService(new android.content.Intent(com.navdy.hud.app.service.ConnectionServiceProxy.this.mContext, com.navdy.hud.app.service.HudConnectionService.class));
            com.navdy.hud.app.service.ConnectionServiceProxy.this.bus.post(new com.navdy.hud.app.event.InitEvents.ConnectionServiceStarted());
        }

        public void onServiceDisconnected(android.content.ComponentName name) {
            com.navdy.hud.app.service.ConnectionServiceProxy.sLogger.i("Disconnected from IEventSource service");
            com.navdy.hud.app.service.ConnectionServiceProxy.this.bus.post(new com.navdy.service.library.events.connection.ConnectionStateChange("", com.navdy.service.library.events.connection.ConnectionStateChange.ConnectionState.CONNECTION_DISCONNECTED));
            com.navdy.hud.app.service.ConnectionServiceProxy.this.mEventSource = null;
        }
    }

    class Anon3 extends com.navdy.hud.app.IEventListener.Stub {
        Anon3() {
        }

        public void onEvent(byte[] bytes) throws android.os.RemoteException {
            com.navdy.hud.app.service.ConnectionServiceProxy.this.postEvent(bytes);
        }
    }

    public ConnectionServiceProxy(android.content.Context context, com.squareup.otto.Bus bus2) {
        this.mContext = context;
        this.bus = bus2;
        bus2.register(this);
        this.mWire = new com.squareup.wire.Wire((java.lang.Class<?>[]) new java.lang.Class[]{com.navdy.service.library.events.Ext_NavdyEvent.class});
        sLogger.i("Creating connection service proxy:" + this);
    }

    public void connect() {
        if (this.mEventSource == null) {
            sLogger.d("Starting connection service");
            android.content.Intent intent = new android.content.Intent(this.mContext, com.navdy.hud.app.service.HudConnectionService.class);
            this.mContext.startService(intent);
            sLogger.d("Binding to connection service");
            intent.setAction(com.navdy.hud.app.IEventSource.class.getName());
            this.mContext.bindService(intent, this.serviceConnection, 0);
        }
    }

    public void disconnect() {
        if (this.mEventSource != null) {
            try {
                this.mEventSource.removeEventListener(this.eventListener);
            } catch (android.os.RemoteException e) {
                sLogger.e("Failed to remove event listener", e);
            }
            this.mContext.unbindService(this.serviceConnection);
            this.mContext.stopService(new android.content.Intent(this.mContext, com.navdy.hud.app.service.HudConnectionService.class));
            this.mEventSource = null;
        }
    }

    public boolean connected() {
        return this.mEventSource != null;
    }

    public com.squareup.otto.Bus getBus() {
        return this.bus;
    }

    @com.squareup.otto.Subscribe
    public void onEvent(com.navdy.service.library.events.NavdyEvent event) {
        com.squareup.wire.Message message = com.navdy.service.library.events.NavdyEventUtil.messageFromEvent(event);
        if (message != null) {
            if (sLogger.isLoggable(2)) {
                sLogger.d("Received message: " + message);
            }
            this.bus.post(message);
            switch (event.type) {
                case StartDriveRecordingEvent:
                    this.driveRecordingEvent = (com.navdy.service.library.events.debug.StartDriveRecordingEvent) message;
                    return;
                case StopDriveRecordingEvent:
                    this.driveRecordingEvent = null;
                    return;
                default:
                    return;
            }
        }
    }

    public void postEvent(byte[] bytes) {
        int eventTypeIndex;
        if (this.bus != null) {
            try {
                this.bus.post((com.navdy.service.library.events.NavdyEvent) this.mWire.parseFrom(bytes, com.navdy.service.library.events.NavdyEvent.class));
                return;
            } catch (Throwable th) {
            }
        } else {
            return;
        }
        sLogger.e("Ignoring invalid navdy event[" + eventTypeIndex + "]", e);
    }

    public void postRemoteEvent(com.navdy.service.library.device.NavdyDeviceId deviceId, com.navdy.service.library.events.NavdyEvent event) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.service.ConnectionServiceProxy.Anon1(event, deviceId), 11);
    }

    public com.navdy.service.library.events.debug.StartDriveRecordingEvent getDriverRecordingEvent() {
        return this.driveRecordingEvent;
    }
}
