package com.navdy.hud.app.service;

public class ShutdownMonitor implements java.lang.Runnable {
    private static final long ACCELERATED_TIMEOUT = java.util.concurrent.TimeUnit.SECONDS.toMillis(30);
    private static final long ACTIVE_USE_TIMEOUT = java.util.concurrent.TimeUnit.MINUTES.toMillis(3);
    private static final long BOOT_MODE_TIMEOUT = java.util.concurrent.TimeUnit.MINUTES.toMillis(7);
    private static final float MOVEMENT_THRESHOLD = 60.0f;
    private static final long OBD_ACTIVE_TIMEOUT = java.util.concurrent.TimeUnit.MINUTES.toMillis(20);
    private static final long OBD_DISCONNECT_DELAY = java.util.concurrent.TimeUnit.SECONDS.toMillis(30);
    /* access modifiers changed from: private */
    public static final long OBD_DISCONNECT_TIMEOUT = java.util.concurrent.TimeUnit.SECONDS.toMillis(5);
    private static final long PENDING_SHUTDOWN_TIMEOUT = java.util.concurrent.TimeUnit.MINUTES.toMillis(5);
    /* access modifiers changed from: private */
    public static final long POWER_DISCONECT_SMOOTHING_TIMEOUT = java.util.concurrent.TimeUnit.SECONDS.toMillis(3);
    /* access modifiers changed from: private */
    public static long REPORT_INTERVAL = java.util.concurrent.TimeUnit.MINUTES.toMillis(1);
    /* access modifiers changed from: private */
    public static final long SAMPLING_INTERVAL = java.util.concurrent.TimeUnit.SECONDS.toMillis(15);
    private static final java.lang.String SHUTDOWND_REQUEST_CLICK = "powerclick";
    private static final java.lang.String SHUTDOWND_REQUEST_DOUBLE_CLICK = "powerdoubleclick";
    private static final java.lang.String SHUTDOWND_REQUEST_LONG_PRESS = "powerlongpress";
    private static final java.lang.String SHUTDOWND_REQUEST_POWER_STATE = "powerstate";
    private static java.lang.String SHUTDOWN_OVERRIDE_SETTING = "persist.sys.noautoshutdown";
    private static final java.lang.String SOCKET_NAME = "shutdownd";
    private static java.lang.String TEMPORARY_SHUTDOWN_OVERRIDE_SETTING = "sys.powerctl.noautoshutdown";
    private static com.navdy.hud.app.service.ShutdownMonitor sInstance = null;
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.service.ShutdownMonitor.class);
    @javax.inject.Inject
    com.squareup.otto.Bus bus;
    private java.lang.String lastWakeReason;
    /* access modifiers changed from: private */
    public long mAccelerateTime = 0;
    private long mDialLastActivityTimeMsecs = 0;
    private com.navdy.hud.app.device.dial.DialManager mDialManager;
    /* access modifiers changed from: private */
    public boolean mDriving = false;
    /* access modifiers changed from: private */
    public android.os.Handler mHandler;
    private boolean mInactivityShutdownDisabled = false;
    @javax.inject.Inject
    com.navdy.hud.app.manager.InputManager mInputManager;
    private boolean mIsDialConnected = false;
    private long mLastInputEventTimeMsecs = 0;
    /* access modifiers changed from: private */
    public long mLastMovementReport = 0;
    private long mLastMovementTimeMsecs = 0;
    private android.location.LocationListener mLocationListener = new com.navdy.hud.app.service.ShutdownMonitor.Anon2();
    /* access modifiers changed from: private */
    public boolean mMainPowerOn;
    private com.navdy.hud.app.service.ShutdownMonitor.MonitorState mMonitorState = com.navdy.hud.app.service.ShutdownMonitor.MonitorState.STATE_BOOT;
    /* access modifiers changed from: private */
    public android.util.ArrayMap<java.lang.String, android.location.Location> mMovementBases = new android.util.ArrayMap<>();
    /* access modifiers changed from: private */
    public long mObdDisconnectTimeMsecs = 0;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.obd.ObdManager mObdManager;
    /* access modifiers changed from: private */
    public long mRemoteDeviceConnectTimeMsecs = 0;
    private boolean mScreenDimmingDisabled = false;
    private android.net.LocalSocket mSocket;
    private java.io.InputStream mSocketInputStream;
    private java.io.OutputStream mSocketOutputStream;
    private long mStateChangeTime = 0;
    /* access modifiers changed from: private */
    public boolean mUsbPowerOn;
    private double maxVoltage = 0.0d;
    private final com.navdy.hud.app.service.ShutdownMonitor.NotificationReceiver notifyReceiver = new com.navdy.hud.app.service.ShutdownMonitor.NotificationReceiver(this, null);
    /* access modifiers changed from: private */
    public java.lang.Runnable powerLossRunnable = null;
    @javax.inject.Inject
    com.navdy.hud.app.device.PowerManager powerManager;

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            com.navdy.hud.app.service.ShutdownMonitor.this.establishSocketConnection();
            new com.navdy.hud.app.service.ShutdownMonitor.MessageReceiver().start();
            com.navdy.hud.app.service.ShutdownMonitor.this.mHandler.postDelayed(com.navdy.hud.app.service.ShutdownMonitor.this, com.navdy.hud.app.service.ShutdownMonitor.SAMPLING_INTERVAL);
        }
    }

    class Anon2 implements android.location.LocationListener {

        class Anon1 implements java.lang.Runnable {
            final /* synthetic */ android.location.Location val$copy;

            Anon1(android.location.Location location) {
                this.val$copy = location;
            }

            public void run() {
                java.lang.String provider = this.val$copy.getProvider();
                if (provider == null) {
                    provider = "unknown";
                }
                android.location.Location movementBase = (android.location.Location) com.navdy.hud.app.service.ShutdownMonitor.this.mMovementBases.get(provider);
                if (movementBase == null) {
                    com.navdy.hud.app.service.ShutdownMonitor.this.mMovementBases.put(provider, this.val$copy);
                    com.navdy.hud.app.service.ShutdownMonitor.this.onMovement();
                } else if (movementBase.distanceTo(this.val$copy) > 60.0f) {
                    com.navdy.hud.app.service.ShutdownMonitor.this.mMovementBases.put(provider, this.val$copy);
                    com.navdy.hud.app.service.ShutdownMonitor.this.onMovement();
                    long curTime = com.navdy.hud.app.service.ShutdownMonitor.this.getCurrentTimeMsecs();
                    if (com.navdy.hud.app.service.ShutdownMonitor.this.mLastMovementReport == 0 || curTime - com.navdy.hud.app.service.ShutdownMonitor.this.mLastMovementReport >= com.navdy.hud.app.service.ShutdownMonitor.REPORT_INTERVAL) {
                        com.navdy.hud.app.service.ShutdownMonitor.this.mLastMovementReport = curTime;
                        com.navdy.hud.app.service.ShutdownMonitor.sLogger.i("Moved significant distance - based on " + provider + " provider");
                    }
                }
            }
        }

        Anon2() {
        }

        public void onLocationChanged(android.location.Location location) {
            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.service.ShutdownMonitor.Anon2.Anon1(new android.location.Location(location)), 1);
        }

        public void onStatusChanged(java.lang.String var1, int var2, android.os.Bundle var3) {
        }

        public void onProviderEnabled(java.lang.String var1) {
        }

        public void onProviderDisabled(java.lang.String var1) {
        }
    }

    class Anon3 implements java.lang.Runnable {
        Anon3() {
        }

        public void run() {
            com.navdy.hud.app.service.ShutdownMonitor.this.updateStats();
            com.navdy.hud.app.service.ShutdownMonitor.this.mHandler.postDelayed(com.navdy.hud.app.service.ShutdownMonitor.this, com.navdy.hud.app.service.ShutdownMonitor.this.evaluateState());
        }
    }

    private class MessageReceiver extends java.lang.Thread {

        class Anon1 implements java.lang.Runnable {
            Anon1() {
            }

            public void run() {
                if (!com.navdy.hud.app.service.ShutdownMonitor.this.mMainPowerOn && !com.navdy.hud.app.service.ShutdownMonitor.this.mUsbPowerOn) {
                    com.navdy.hud.app.service.ShutdownMonitor.this.bus.post(new com.navdy.hud.app.event.Shutdown(com.navdy.hud.app.event.Shutdown.Reason.POWER_LOSS));
                }
            }
        }

        public MessageReceiver() {
            super("ShutdownMonitor MessageReceiver thread");
        }

        public void run() {
            byte[] buf = new byte[100];
            java.nio.charset.Charset cs = java.nio.charset.Charset.forName("US-ASCII");
            int retries = 0;
            java.lang.String prevData = "";
            while (true) {
                int readCount = -1;
                try {
                    java.io.InputStream is = com.navdy.hud.app.service.ShutdownMonitor.this.getSocketInputStream();
                    if (is != null) {
                        retries = 0;
                        readCount = is.read(buf);
                        if (readCount < 0) {
                            com.navdy.hud.app.service.ShutdownMonitor.sLogger.e("end of file reading shutdownd socket");
                            com.navdy.hud.app.service.ShutdownMonitor.this.closeSocketConnection();
                        }
                    }
                } catch (java.lang.Exception e) {
                    com.navdy.hud.app.service.ShutdownMonitor.sLogger.e("exception reading shutdownd socket", e);
                    com.navdy.hud.app.service.ShutdownMonitor.this.closeSocketConnection();
                }
                if (readCount < 0) {
                    retries++;
                    if (retries > 3) {
                        com.navdy.hud.app.service.ShutdownMonitor.sLogger.e("MessageReceiver thread terminating");
                        return;
                    }
                    try {
                        java.lang.Thread.sleep((long) (retries * 10000));
                    } catch (java.lang.InterruptedException e2) {
                    }
                } else {
                    try {
                        java.lang.StringBuilder append = new java.lang.StringBuilder().append(prevData);
                        java.lang.String str = new java.lang.String(buf, 0, readCount, cs);
                        java.lang.String data = append.append(str).toString();
                        while (data.contains(com.navdy.hud.app.framework.glance.GlanceConstants.NEWLINE)) {
                            java.lang.String[] split = data.split(com.navdy.hud.app.framework.glance.GlanceConstants.NEWLINE, 2);
                            java.lang.String command = split[0];
                            data = split.length > 1 ? split[1] : "";
                            com.navdy.hud.app.service.ShutdownMonitor.sLogger.v("got command[" + command + "]");
                            if (com.navdy.hud.app.service.ShutdownMonitor.SHUTDOWND_REQUEST_CLICK.equals(command)) {
                                com.navdy.hud.app.service.ShutdownMonitor.this.mInputManager.injectKey(com.navdy.hud.app.manager.InputManager.CustomKeyEvent.POWER_BUTTON_CLICK);
                            } else if (com.navdy.hud.app.service.ShutdownMonitor.SHUTDOWND_REQUEST_DOUBLE_CLICK.equals(command)) {
                                com.navdy.hud.app.service.ShutdownMonitor.this.mInputManager.injectKey(com.navdy.hud.app.manager.InputManager.CustomKeyEvent.POWER_BUTTON_DOUBLE_CLICK);
                            } else if (com.navdy.hud.app.service.ShutdownMonitor.SHUTDOWND_REQUEST_LONG_PRESS.equals(command)) {
                                com.navdy.hud.app.service.ShutdownMonitor.this.mInputManager.injectKey(com.navdy.hud.app.manager.InputManager.CustomKeyEvent.POWER_BUTTON_LONG_PRESS);
                            } else if (command.startsWith(com.navdy.hud.app.service.ShutdownMonitor.SHUTDOWND_REQUEST_POWER_STATE)) {
                                java.lang.String state = command.substring(com.navdy.hud.app.service.ShutdownMonitor.SHUTDOWND_REQUEST_POWER_STATE.length() + 1).trim();
                                com.navdy.hud.app.service.ShutdownMonitor.sLogger.d("State : " + state);
                                boolean usbOn = state.charAt(0) != '0';
                                boolean mainOn = state.charAt(1) != '0';
                                com.navdy.hud.app.service.ShutdownMonitor.sLogger.d("USB on : " + usbOn + " , Main on : " + mainOn);
                                if (usbOn || mainOn) {
                                    if (!com.navdy.hud.app.service.ShutdownMonitor.this.mMainPowerOn) {
                                        if (!com.navdy.hud.app.service.ShutdownMonitor.this.mUsbPowerOn && com.navdy.hud.app.service.ShutdownMonitor.this.powerLossRunnable != null) {
                                            com.navdy.hud.app.service.ShutdownMonitor.this.mHandler.removeCallbacks(com.navdy.hud.app.service.ShutdownMonitor.this.powerLossRunnable);
                                            com.navdy.hud.app.service.ShutdownMonitor.this.powerLossRunnable = null;
                                        }
                                        com.navdy.hud.app.service.ShutdownMonitor.this.mObdManager.updateListener();
                                    }
                                } else if (com.navdy.hud.app.service.ShutdownMonitor.this.mMainPowerOn || com.navdy.hud.app.service.ShutdownMonitor.this.mUsbPowerOn) {
                                    com.navdy.hud.app.service.ShutdownMonitor shutdownMonitor = com.navdy.hud.app.service.ShutdownMonitor.this;
                                    com.navdy.hud.app.service.ShutdownMonitor.MessageReceiver.Anon1 anon1 = new com.navdy.hud.app.service.ShutdownMonitor.MessageReceiver.Anon1();
                                    shutdownMonitor.powerLossRunnable = anon1;
                                    com.navdy.hud.app.service.ShutdownMonitor.this.mHandler.postDelayed(com.navdy.hud.app.service.ShutdownMonitor.this.powerLossRunnable, com.navdy.hud.app.service.ShutdownMonitor.POWER_DISCONECT_SMOOTHING_TIMEOUT);
                                }
                                com.navdy.hud.app.service.ShutdownMonitor.this.mMainPowerOn = mainOn;
                                com.navdy.hud.app.service.ShutdownMonitor.this.mUsbPowerOn = usbOn;
                            } else {
                                com.navdy.hud.app.service.ShutdownMonitor.sLogger.e("invalid command from shutdownd socket: " + command);
                            }
                        }
                        prevData = data;
                    } catch (java.lang.Exception e3) {
                        com.navdy.hud.app.service.ShutdownMonitor.sLogger.e("exception while processing data from shutdownd", e3);
                        prevData = "";
                    }
                }
            }
        }
    }

    private enum MonitorState {
        STATE_BOOT,
        STATE_OBD_ACTIVE,
        STATE_ACTIVE_USE,
        STATE_PENDING_SHUTDOWN,
        STATE_SHUTDOWN_PROMPT,
        STATE_QUIET_MODE
    }

    private class NotificationReceiver {

        class Anon1 implements java.lang.Runnable {
            final /* synthetic */ com.navdy.hud.app.event.Shutdown val$event;

            Anon1(com.navdy.hud.app.event.Shutdown shutdown) {
                this.val$event = shutdown;
            }

            public void run() {
                com.navdy.hud.app.service.ShutdownMonitor.NotificationReceiver.this.triggerShutdown(this.val$event);
            }
        }

        private class ShutdownRunnable implements java.lang.Runnable {
            private final com.navdy.hud.app.event.Shutdown event;

            ShutdownRunnable(com.navdy.hud.app.event.Shutdown r) {
                this.event = r;
            }

            public void run() {
                boolean fullShutdown;
                boolean usingCLA;
                boolean charging;
                com.navdy.hud.app.event.Shutdown.Reason reason = this.event.reason;
                com.navdy.hud.app.HudApplication.getApplication().setShutdownReason(reason);
                if (!com.navdy.hud.app.service.ShutdownMonitor.this.powerManager.quietModeEnabled()) {
                    fullShutdown = true;
                } else {
                    fullShutdown = false;
                }
                double batteryVoltage = com.navdy.hud.app.service.ShutdownMonitor.this.mObdManager.getBatteryVoltage();
                boolean autoOnEnabled = com.navdy.hud.app.service.ShutdownMonitor.this.mObdManager.getObdDeviceConfigurationManager().isAutoOnEnabled();
                if (com.navdy.hud.app.service.ShutdownMonitor.this.mObdManager.getConnectionType() == com.navdy.hud.app.obd.ObdManager.ConnectionType.POWER_ONLY) {
                    usingCLA = true;
                } else {
                    usingCLA = false;
                }
                if (batteryVoltage >= 13.100000381469727d) {
                    charging = true;
                } else {
                    charging = false;
                }
                switch (reason) {
                    case POWER_LOSS:
                    case CRITICAL_VOLTAGE:
                    case LOW_VOLTAGE:
                    case TIMEOUT:
                    case HIGH_TEMPERATURE:
                        fullShutdown = true;
                        break;
                }
                com.navdy.hud.app.service.ShutdownMonitor.sLogger.i("Shutting down, auto-on:" + autoOnEnabled + " voltage:" + batteryVoltage + " usingCLA:" + usingCLA + " charging:" + charging);
                if (!autoOnEnabled || usingCLA || charging) {
                    fullShutdown = true;
                } else {
                    if (com.navdy.hud.app.service.ShutdownMonitor.this.mObdManager.isSleeping()) {
                        com.navdy.hud.app.service.ShutdownMonitor.this.mObdManager.wakeup();
                        com.navdy.hud.app.util.GenericUtil.sleep(1000);
                    }
                    com.navdy.hud.app.service.ShutdownMonitor.this.mObdManager.sleep(fullShutdown);
                }
                com.navdy.hud.app.service.ShutdownMonitor.this.powerManager.androidShutdown(reason, fullShutdown);
            }
        }

        private NotificationReceiver() {
        }

        /* synthetic */ NotificationReceiver(com.navdy.hud.app.service.ShutdownMonitor x0, com.navdy.hud.app.service.ShutdownMonitor.Anon1 x1) {
            this();
        }

        /* access modifiers changed from: private */
        public void triggerShutdown(com.navdy.hud.app.event.Shutdown event) {
            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.service.ShutdownMonitor.NotificationReceiver.ShutdownRunnable(event), 1);
        }

        @com.squareup.otto.Subscribe
        public void onShutdown(com.navdy.hud.app.event.Shutdown event) {
            switch (event.state) {
                case CANCELED:
                    com.navdy.hud.app.service.ShutdownMonitor.this.enterActiveUseState();
                    return;
                case CONFIRMED:
                    com.navdy.hud.app.service.ShutdownMonitor.this.mHandler.post(new com.navdy.hud.app.service.ShutdownMonitor.NotificationReceiver.Anon1(event));
                    return;
                default:
                    return;
            }
        }

        @com.squareup.otto.Subscribe
        public void onWakeup(com.navdy.hud.app.event.Wakeup event) {
            com.navdy.hud.app.service.ShutdownMonitor.this.enterBootState();
        }

        @com.squareup.otto.Subscribe
        public void ObdStateChangeEvent(com.navdy.hud.app.obd.ObdManager.ObdConnectionStatusEvent event) {
            if (event.connected) {
                com.navdy.hud.app.service.ShutdownMonitor.this.mObdDisconnectTimeMsecs = 0;
                com.navdy.hud.app.service.ShutdownMonitor.this.mAccelerateTime = 0;
                return;
            }
            com.navdy.hud.app.service.ShutdownMonitor.this.mObdDisconnectTimeMsecs = com.navdy.hud.app.service.ShutdownMonitor.this.getCurrentTimeMsecs();
            com.navdy.hud.app.service.ShutdownMonitor.this.mHandler.removeCallbacks(com.navdy.hud.app.service.ShutdownMonitor.this);
            com.navdy.hud.app.service.ShutdownMonitor.this.mHandler.postDelayed(com.navdy.hud.app.service.ShutdownMonitor.this, com.navdy.hud.app.service.ShutdownMonitor.OBD_DISCONNECT_TIMEOUT);
        }

        @com.squareup.otto.Subscribe
        public void onConnectionStateChange(com.navdy.service.library.events.connection.ConnectionStateChange event) {
            if (event.state == com.navdy.service.library.events.connection.ConnectionStateChange.ConnectionState.CONNECTION_VERIFIED) {
                com.navdy.hud.app.service.ShutdownMonitor.this.mRemoteDeviceConnectTimeMsecs = com.navdy.hud.app.service.ShutdownMonitor.this.getCurrentTimeMsecs();
                com.navdy.hud.app.service.ShutdownMonitor.this.mAccelerateTime = 0;
                com.navdy.hud.app.service.ShutdownMonitor.this.onWakeEvent();
            } else if (event.state == com.navdy.service.library.events.connection.ConnectionStateChange.ConnectionState.CONNECTION_DISCONNECTED) {
                com.navdy.hud.app.service.ShutdownMonitor.this.mRemoteDeviceConnectTimeMsecs = 0;
            }
        }

        @com.squareup.otto.Subscribe
        public void onAccelerateShutdown(com.navdy.service.library.events.hudcontrol.AccelerateShutdown event) {
            com.navdy.hud.app.service.ShutdownMonitor.sLogger.v(java.lang.String.format("received AccelerateShutdown, reason = %s", new java.lang.Object[]{event.reason}));
            com.navdy.hud.app.service.ShutdownMonitor.this.mAccelerateTime = com.navdy.hud.app.service.ShutdownMonitor.this.getCurrentTimeMsecs();
        }

        @com.squareup.otto.Subscribe
        public void onDrivingStateChange(com.navdy.hud.app.event.DrivingStateChange event) {
            com.navdy.hud.app.service.ShutdownMonitor.this.mDriving = event.driving;
            if (com.navdy.hud.app.service.ShutdownMonitor.this.mDriving) {
                com.navdy.hud.app.service.ShutdownMonitor.this.onMovement();
            }
        }
    }

    public static synchronized com.navdy.hud.app.service.ShutdownMonitor getInstance() {
        com.navdy.hud.app.service.ShutdownMonitor shutdownMonitor;
        synchronized (com.navdy.hud.app.service.ShutdownMonitor.class) {
            if (sInstance == null) {
                sInstance = new com.navdy.hud.app.service.ShutdownMonitor();
                sInstance.init();
            }
            shutdownMonitor = sInstance;
        }
        return shutdownMonitor;
    }

    /* access modifiers changed from: private */
    public long getCurrentTimeMsecs() {
        return android.os.SystemClock.elapsedRealtime();
    }

    /* access modifiers changed from: private */
    public synchronized void establishSocketConnection() {
        this.mSocket = new android.net.LocalSocket();
        try {
            this.mSocket.connect(new android.net.LocalSocketAddress(SOCKET_NAME, android.net.LocalSocketAddress.Namespace.RESERVED));
            this.mSocketOutputStream = this.mSocket.getOutputStream();
            this.mSocketInputStream = this.mSocket.getInputStream();
        } catch (java.lang.Exception e) {
            sLogger.e("exception while attempting to connect to shutdownd socket", e);
            closeSocketConnection();
        }
        return;
    }

    /* access modifiers changed from: private */
    public synchronized void closeSocketConnection() {
        com.navdy.service.library.util.IOUtils.closeStream(this.mSocketInputStream);
        com.navdy.service.library.util.IOUtils.closeStream(this.mSocketOutputStream);
        com.navdy.service.library.util.IOUtils.closeStream(this.mSocket);
        this.mSocket = null;
        this.mSocketInputStream = null;
        this.mSocketOutputStream = null;
    }

    private synchronized java.io.OutputStream getSocketOutputStream() {
        if (this.mSocketOutputStream == null) {
            establishSocketConnection();
        }
        return this.mSocketOutputStream;
    }

    /* access modifiers changed from: private */
    public synchronized java.io.InputStream getSocketInputStream() {
        if (this.mSocketInputStream == null) {
            establishSocketConnection();
        }
        return this.mSocketInputStream;
    }

    private ShutdownMonitor() {
        if (!com.navdy.hud.app.util.DeviceUtil.isNavdyDevice()) {
            sLogger.w("not a Navdy device, ShutdownMonitor will not run");
            return;
        }
        long curTimeMsecs = getCurrentTimeMsecs();
        this.mHandler = new android.os.Handler(android.os.Looper.getMainLooper());
        this.mDialManager = com.navdy.hud.app.device.dial.DialManager.getInstance();
        this.mObdManager = com.navdy.hud.app.obd.ObdManager.getInstance();
        this.mLastInputEventTimeMsecs = curTimeMsecs;
        this.mDialLastActivityTimeMsecs = curTimeMsecs;
        this.mObdDisconnectTimeMsecs = 0;
        this.mLastMovementTimeMsecs = 0;
        this.mRemoteDeviceConnectTimeMsecs = 0;
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.service.ShutdownMonitor.Anon1(), 1);
    }

    private boolean canDoShutdown() {
        boolean shutdownDisabled;
        if (com.navdy.hud.app.util.os.SystemProperties.getBoolean(SHUTDOWN_OVERRIDE_SETTING, false) || com.navdy.hud.app.util.os.SystemProperties.getBoolean(TEMPORARY_SHUTDOWN_OVERRIDE_SETTING, false) || this.mUsbPowerOn) {
            shutdownDisabled = true;
        } else {
            shutdownDisabled = false;
        }
        if (!shutdownDisabled) {
            return true;
        }
        return false;
    }

    private void init() {
        mortar.Mortar.inject(com.navdy.hud.app.HudApplication.getAppContext(), this);
        android.os.Looper looper = android.os.Looper.getMainLooper();
        android.location.LocationManager locationManager = (android.location.LocationManager) com.navdy.hud.app.HudApplication.getAppContext().getSystemService("location");
        try {
            locationManager.requestLocationUpdates("gps", 0, 0.0f, this.mLocationListener, looper);
            locationManager.requestLocationUpdates("network", 0, 0.0f, this.mLocationListener, looper);
        } catch (java.lang.IllegalArgumentException e) {
            sLogger.e("failed to register with GPS or Network provider");
        }
        this.bus.register(this.notifyReceiver);
        if (this.powerManager.inQuietMode()) {
            enterQuiteModeState();
        } else {
            enterBootState();
        }
    }

    /* access modifiers changed from: private */
    public void onMovement() {
        this.mLastMovementTimeMsecs = getCurrentTimeMsecs();
        this.mAccelerateTime = 0;
        onWakeEvent();
    }

    /* access modifiers changed from: private */
    public void onWakeEvent() {
        if (this.mMonitorState == com.navdy.hud.app.service.ShutdownMonitor.MonitorState.STATE_SHUTDOWN_PROMPT || this.mMonitorState == com.navdy.hud.app.service.ShutdownMonitor.MonitorState.STATE_PENDING_SHUTDOWN) {
            enterActiveUseState();
        }
    }

    public void recordInputEvent() {
        this.mLastInputEventTimeMsecs = getCurrentTimeMsecs();
        if (this.mMonitorState == com.navdy.hud.app.service.ShutdownMonitor.MonitorState.STATE_PENDING_SHUTDOWN) {
            enterActiveUseState();
        }
    }

    /* access modifiers changed from: private */
    public void updateStats() {
        long curTimeMsecs = getCurrentTimeMsecs();
        boolean connected = this.mDialManager.isDialConnected();
        if (connected && !this.mIsDialConnected) {
            this.mDialLastActivityTimeMsecs = curTimeMsecs;
        }
        this.mIsDialConnected = connected;
    }

    private long checkObdConnection() {
        if (this.mObdDisconnectTimeMsecs == 0) {
            return 0;
        }
        return getCurrentTimeMsecs() - this.mObdDisconnectTimeMsecs;
    }

    private void setMonitorState(com.navdy.hud.app.service.ShutdownMonitor.MonitorState state) {
        switch (this.mMonitorState) {
            case STATE_PENDING_SHUTDOWN:
                leavePendingShutdownState();
                break;
            case STATE_SHUTDOWN_PROMPT:
                leaveShutdownPromptState();
                break;
        }
        sLogger.v(java.lang.String.format("setting monitor state to %s", new java.lang.Object[]{state}));
        this.mMonitorState = state;
        this.mStateChangeTime = getCurrentTimeMsecs();
    }

    /* access modifiers changed from: private */
    public void enterBootState() {
        setMonitorState(com.navdy.hud.app.service.ShutdownMonitor.MonitorState.STATE_BOOT);
    }

    private void enterObdActiveState() {
        setMonitorState(com.navdy.hud.app.service.ShutdownMonitor.MonitorState.STATE_OBD_ACTIVE);
    }

    /* access modifiers changed from: private */
    public void enterActiveUseState() {
        setMonitorState(com.navdy.hud.app.service.ShutdownMonitor.MonitorState.STATE_ACTIVE_USE);
    }

    private void enterPendingShutdownState() {
        if (canDoShutdown()) {
            com.navdy.hud.app.device.light.LED.writeToSysfs("0", "/sys/dlpc/led_enable");
            setMonitorState(com.navdy.hud.app.service.ShutdownMonitor.MonitorState.STATE_PENDING_SHUTDOWN);
            return;
        }
        sLogger.v("pending shutdown state disabled by property setting");
    }

    private void leavePendingShutdownState() {
        com.navdy.hud.app.device.light.LED.writeToSysfs(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE, "/sys/dlpc/led_enable");
    }

    private void enterShutdownPromptState(com.navdy.hud.app.event.Shutdown.Reason reason) {
        this.bus.post(new com.navdy.hud.app.event.Shutdown(reason));
        setMonitorState(com.navdy.hud.app.service.ShutdownMonitor.MonitorState.STATE_SHUTDOWN_PROMPT);
    }

    private void enterShutdownPromptStateIfNotDisabled(com.navdy.hud.app.event.Shutdown.Reason reason) {
        boolean shutdownAllowed = canDoShutdown();
        if (this.mInactivityShutdownDisabled || !shutdownAllowed) {
            sLogger.v(java.lang.String.format("shutdown prompt state disabled, shutdownAllowed = %b, mInactivityShutdownDisabled = %b, mUsbPowerOn = %b", new java.lang.Object[]{java.lang.Boolean.valueOf(shutdownAllowed), java.lang.Boolean.valueOf(this.mInactivityShutdownDisabled), java.lang.Boolean.valueOf(this.mUsbPowerOn)}));
            return;
        }
        enterShutdownPromptState(reason);
    }

    private void leaveShutdownPromptState() {
        this.mAccelerateTime = 0;
    }

    private void enterQuiteModeState() {
        setMonitorState(com.navdy.hud.app.service.ShutdownMonitor.MonitorState.STATE_QUIET_MODE);
    }

    public void run() {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.service.ShutdownMonitor.Anon3(), 10);
    }

    public void disableInactivityShutdown(boolean disable) {
        this.mInactivityShutdownDisabled = disable;
    }

    public void disableScreenDim(boolean disable) {
        this.mScreenDimmingDisabled = disable;
        recordInputEvent();
        com.navdy.service.library.log.Logger logger = sLogger;
        java.lang.String str = "screen dimming %s";
        java.lang.Object[] objArr = new java.lang.Object[1];
        objArr[0] = disable ? "disabled" : "enabled";
        logger.v(java.lang.String.format(str, objArr));
    }

    /* access modifiers changed from: private */
    public long evaluateState() {
        long curTime = getCurrentTimeMsecs();
        long nextTimeout = SAMPLING_INTERVAL;
        double curVoltage = this.mObdManager.getBatteryVoltage();
        if (curVoltage > this.maxVoltage) {
            this.maxVoltage = curVoltage;
        }
        switch (this.mMonitorState) {
            case STATE_PENDING_SHUTDOWN:
                long wakeEventTime = lastWakeEventTime();
                if (this.mObdManager.isConnected()) {
                    enterObdActiveState();
                    return nextTimeout;
                } else if (wakeEventTime > this.mStateChangeTime) {
                    sLogger.i("Wake reason:" + this.lastWakeReason);
                    enterActiveUseState();
                    return nextTimeout;
                } else if (this.mAccelerateTime == 0 && curTime - wakeEventTime < PENDING_SHUTDOWN_TIMEOUT) {
                    return nextTimeout;
                } else {
                    enterShutdownPromptStateIfNotDisabled(com.navdy.hud.app.event.Shutdown.Reason.INACTIVITY);
                    return nextTimeout;
                }
            case STATE_BOOT:
                if (this.mObdManager.isConnected()) {
                    enterObdActiveState();
                    return nextTimeout;
                } else if (curTime - this.mStateChangeTime <= BOOT_MODE_TIMEOUT) {
                    return nextTimeout;
                } else {
                    enterActiveUseState();
                    return nextTimeout;
                }
            case STATE_OBD_ACTIVE:
                long obdDiscMsec = checkObdConnection();
                if (obdDiscMsec >= OBD_DISCONNECT_TIMEOUT) {
                    sLogger.i(java.lang.String.format("OBD disconnected, driving = %b, battery voltage = %5.1f, max voltage = %5.1f", new java.lang.Object[]{java.lang.Boolean.valueOf(this.mDriving), java.lang.Double.valueOf(curVoltage), java.lang.Double.valueOf(this.maxVoltage)}));
                    if (!this.mDriving && curVoltage != -1.0d && curVoltage <= 12.899999618530273d) {
                        enterShutdownPromptStateIfNotDisabled(com.navdy.hud.app.event.Shutdown.Reason.ENGINE_OFF);
                        return nextTimeout;
                    } else if (obdDiscMsec < OBD_DISCONNECT_DELAY) {
                        return java.lang.Math.min(java.util.concurrent.TimeUnit.SECONDS.toMillis(5), OBD_DISCONNECT_DELAY - obdDiscMsec);
                    } else {
                        if (this.mDriving) {
                            enterActiveUseState();
                            return nextTimeout;
                        }
                        enterPendingShutdownState();
                        return nextTimeout;
                    }
                } else if (curTime - lastWakeEventTime() < OBD_ACTIVE_TIMEOUT || this.mDriving) {
                    return nextTimeout;
                } else {
                    enterShutdownPromptStateIfNotDisabled(com.navdy.hud.app.event.Shutdown.Reason.ENGINE_OFF);
                    return nextTimeout;
                }
            case STATE_ACTIVE_USE:
                if (this.mObdManager.isConnected()) {
                    enterObdActiveState();
                    return nextTimeout;
                } else if (this.mAccelerateTime != 0) {
                    long wt = lastWakeEventTime();
                    if (curTime - wt < ACCELERATED_TIMEOUT || this.mDriving) {
                        return (ACCELERATED_TIMEOUT + wt) - curTime;
                    }
                    enterShutdownPromptStateIfNotDisabled(com.navdy.hud.app.event.Shutdown.Reason.ACCELERATE_SHUTDOWN);
                    return nextTimeout;
                } else if (this.mScreenDimmingDisabled || curTime - lastWakeEventTime() <= ACTIVE_USE_TIMEOUT || this.mDriving) {
                    return nextTimeout;
                } else {
                    sLogger.i("Active use timeout - last wake event was " + this.lastWakeReason);
                    enterPendingShutdownState();
                    return nextTimeout;
                }
            default:
                return nextTimeout;
        }
    }

    private long lastWakeEventTime() {
        long lastDownloadTime;
        updateStats();
        long currentTime = getCurrentTimeMsecs();
        long lastWakeEventTime = this.mStateChangeTime;
        this.lastWakeReason = "stateChange";
        if (this.mDialLastActivityTimeMsecs > lastWakeEventTime) {
            lastWakeEventTime = this.mDialLastActivityTimeMsecs;
            this.lastWakeReason = "dial";
        }
        if (this.mLastInputEventTimeMsecs > lastWakeEventTime) {
            lastWakeEventTime = this.mLastInputEventTimeMsecs;
            this.lastWakeReason = "input";
        }
        if (this.mRemoteDeviceConnectTimeMsecs > lastWakeEventTime) {
            lastWakeEventTime = this.mRemoteDeviceConnectTimeMsecs;
            this.lastWakeReason = com.navdy.hud.app.device.gps.GpsConstants.USING_PHONE_LOCATION;
        }
        if (this.mLastMovementTimeMsecs > lastWakeEventTime) {
            lastWakeEventTime = this.mLastMovementTimeMsecs;
            this.lastWakeReason = "movement";
        }
        com.navdy.hud.app.debug.DriveRecorder odr = this.mObdManager.getDriveRecorder();
        if (odr != null && odr.isDemoPlaying()) {
            lastWakeEventTime = currentTime;
            this.lastWakeReason = "recording";
        }
        java.lang.String downloading = com.navdy.hud.app.util.os.SystemProperties.get(com.navdy.hud.app.service.FileTransferHandler.OTA_DOWLOADING_PROPERTY, "0");
        try {
            lastDownloadTime = ((long) java.lang.Integer.parseInt(downloading)) * 1000;
        } catch (java.lang.NumberFormatException e) {
            sLogger.e(java.lang.String.format("%s property has invalid value: %s", new java.lang.Object[]{com.navdy.hud.app.service.FileTransferHandler.OTA_DOWLOADING_PROPERTY, downloading}));
            lastDownloadTime = 0;
        }
        if (lastDownloadTime <= lastWakeEventTime) {
            return lastWakeEventTime;
        }
        long lastWakeEventTime2 = lastDownloadTime;
        this.lastWakeReason = "ota";
        return lastWakeEventTime2;
    }
}
