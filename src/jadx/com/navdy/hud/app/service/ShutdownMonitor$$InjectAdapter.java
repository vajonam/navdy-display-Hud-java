package com.navdy.hud.app.service;

public final class ShutdownMonitor$$InjectAdapter extends dagger.internal.Binding<com.navdy.hud.app.service.ShutdownMonitor> implements dagger.MembersInjector<com.navdy.hud.app.service.ShutdownMonitor> {
    private dagger.internal.Binding<com.squareup.otto.Bus> bus;
    private dagger.internal.Binding<com.navdy.hud.app.manager.InputManager> mInputManager;
    private dagger.internal.Binding<com.navdy.hud.app.device.PowerManager> powerManager;

    public ShutdownMonitor$$InjectAdapter() {
        super(null, "members/com.navdy.hud.app.service.ShutdownMonitor", false, com.navdy.hud.app.service.ShutdownMonitor.class);
    }

    public void attach(dagger.internal.Linker linker) {
        this.bus = linker.requestBinding("com.squareup.otto.Bus", com.navdy.hud.app.service.ShutdownMonitor.class, getClass().getClassLoader());
        this.powerManager = linker.requestBinding("com.navdy.hud.app.device.PowerManager", com.navdy.hud.app.service.ShutdownMonitor.class, getClass().getClassLoader());
        this.mInputManager = linker.requestBinding("com.navdy.hud.app.manager.InputManager", com.navdy.hud.app.service.ShutdownMonitor.class, getClass().getClassLoader());
    }

    public void getDependencies(java.util.Set<dagger.internal.Binding<?>> set, java.util.Set<dagger.internal.Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.bus);
        injectMembersBindings.add(this.powerManager);
        injectMembersBindings.add(this.mInputManager);
    }

    public void injectMembers(com.navdy.hud.app.service.ShutdownMonitor object) {
        object.bus = (com.squareup.otto.Bus) this.bus.get();
        object.powerManager = (com.navdy.hud.app.device.PowerManager) this.powerManager.get();
        object.mInputManager = (com.navdy.hud.app.manager.InputManager) this.mInputManager.get();
    }
}
