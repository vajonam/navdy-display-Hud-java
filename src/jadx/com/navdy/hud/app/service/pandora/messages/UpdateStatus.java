package com.navdy.hud.app.service.pandora.messages;

public class UpdateStatus extends com.navdy.hud.app.service.pandora.messages.BaseIncomingMessage {
    private static int MESSAGE_LENGTH = 2;
    private static java.util.Map<java.lang.Byte, com.navdy.hud.app.service.pandora.messages.UpdateStatus> SINGLETONS_MAP = new com.navdy.hud.app.service.pandora.messages.UpdateStatus.Anon1();
    public com.navdy.hud.app.service.pandora.messages.UpdateStatus.Value value;

    static class Anon1 extends java.util.HashMap<java.lang.Byte, com.navdy.hud.app.service.pandora.messages.UpdateStatus> {
        Anon1() {
            put(java.lang.Byte.valueOf(1), new com.navdy.hud.app.service.pandora.messages.UpdateStatus(com.navdy.hud.app.service.pandora.messages.UpdateStatus.Value.PLAYING, null));
            put(java.lang.Byte.valueOf(2), new com.navdy.hud.app.service.pandora.messages.UpdateStatus(com.navdy.hud.app.service.pandora.messages.UpdateStatus.Value.PAUSED, null));
            put(java.lang.Byte.valueOf(3), new com.navdy.hud.app.service.pandora.messages.UpdateStatus(com.navdy.hud.app.service.pandora.messages.UpdateStatus.Value.INCOMPATIBLE_API_VERSION, null));
            put(java.lang.Byte.valueOf(4), new com.navdy.hud.app.service.pandora.messages.UpdateStatus(com.navdy.hud.app.service.pandora.messages.UpdateStatus.Value.UNKNOWN_ERROR, null));
            put(java.lang.Byte.valueOf(5), new com.navdy.hud.app.service.pandora.messages.UpdateStatus(com.navdy.hud.app.service.pandora.messages.UpdateStatus.Value.NO_STATIONS, null));
            put(java.lang.Byte.valueOf(6), new com.navdy.hud.app.service.pandora.messages.UpdateStatus(com.navdy.hud.app.service.pandora.messages.UpdateStatus.Value.NO_STATION_ACTIVE, null));
            put(java.lang.Byte.valueOf(7), new com.navdy.hud.app.service.pandora.messages.UpdateStatus(com.navdy.hud.app.service.pandora.messages.UpdateStatus.Value.INSUFFICIENT_CONNECTIVITY, null));
            put(java.lang.Byte.valueOf(8), new com.navdy.hud.app.service.pandora.messages.UpdateStatus(com.navdy.hud.app.service.pandora.messages.UpdateStatus.Value.LISTENING_RESTRICTIONS, null));
            put(java.lang.Byte.valueOf(9), new com.navdy.hud.app.service.pandora.messages.UpdateStatus(com.navdy.hud.app.service.pandora.messages.UpdateStatus.Value.INVALID_LOGIN, null));
        }
    }

    public enum Value {
        PLAYING,
        PAUSED,
        INCOMPATIBLE_API_VERSION,
        UNKNOWN_ERROR,
        NO_STATIONS,
        NO_STATION_ACTIVE,
        INSUFFICIENT_CONNECTIVITY,
        LISTENING_RESTRICTIONS,
        INVALID_LOGIN
    }

    /* synthetic */ UpdateStatus(com.navdy.hud.app.service.pandora.messages.UpdateStatus.Value x0, com.navdy.hud.app.service.pandora.messages.UpdateStatus.Anon1 x1) {
        this(x0);
    }

    private UpdateStatus(com.navdy.hud.app.service.pandora.messages.UpdateStatus.Value value2) {
        this.value = value2;
    }

    protected static com.navdy.hud.app.service.pandora.messages.BaseIncomingMessage innerBuildFromPayload(byte[] payload) throws com.navdy.hud.app.service.pandora.exceptions.MessageWrongWayException, com.navdy.hud.app.service.pandora.exceptions.CorruptedPayloadException {
        if (payload.length != MESSAGE_LENGTH) {
            throw new com.navdy.hud.app.service.pandora.exceptions.CorruptedPayloadException();
        }
        com.navdy.hud.app.service.pandora.messages.UpdateStatus result = (com.navdy.hud.app.service.pandora.messages.UpdateStatus) SINGLETONS_MAP.get(java.lang.Byte.valueOf(payload[1]));
        if (result != null) {
            return result;
        }
        throw new com.navdy.hud.app.service.pandora.exceptions.CorruptedPayloadException();
    }

    public java.lang.String toString() {
        return "Update Status: " + this.value;
    }
}
