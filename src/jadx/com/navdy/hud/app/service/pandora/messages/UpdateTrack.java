package com.navdy.hud.app.service.pandora.messages;

public class UpdateTrack extends com.navdy.hud.app.service.pandora.messages.BaseIncomingOneIntMessage {
    public UpdateTrack(int value) {
        super(value);
    }

    protected static com.navdy.hud.app.service.pandora.messages.BaseIncomingMessage innerBuildFromPayload(byte[] payload) throws com.navdy.hud.app.service.pandora.exceptions.MessageWrongWayException, com.navdy.hud.app.service.pandora.exceptions.CorruptedPayloadException {
        return new com.navdy.hud.app.service.pandora.messages.UpdateTrack(parseIntValue(payload));
    }

    public java.lang.String toString() {
        return "New track with token: " + this.value;
    }
}
