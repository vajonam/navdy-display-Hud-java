package com.navdy.hud.app.service.pandora.messages;

public class SessionStart extends com.navdy.hud.app.service.pandora.messages.BaseOutgoingConstantMessage {
    private static final int ACCESSORY_ID_FIELD_LENGTH = 8;
    public static final com.navdy.hud.app.service.pandora.messages.SessionStart INSTANCE = new com.navdy.hud.app.service.pandora.messages.SessionStart();

    public /* bridge */ /* synthetic */ byte[] buildPayload() throws java.io.IOException, com.navdy.hud.app.service.pandora.exceptions.StringOverflowException, com.navdy.hud.app.service.pandora.exceptions.UnexpectedEndOfStringException, com.navdy.hud.app.service.pandora.exceptions.MessageWrongWayException {
        return super.buildPayload();
    }

    private SessionStart() {
    }

    public java.io.ByteArrayOutputStream putThis(java.io.ByteArrayOutputStream os) throws java.io.IOException, com.navdy.hud.app.service.pandora.exceptions.StringOverflowException, com.navdy.hud.app.service.pandora.exceptions.UnexpectedEndOfStringException {
        putByte(os, 0);
        putShort(os, 3);
        putFixedLengthASCIIString(os, 8, com.navdy.hud.app.service.pandora.messages.BaseMessage.ACCESSORY_ID);
        putShort(os, 100);
        putByte(os, 1);
        putByte(os, 2);
        putShort(os, 0);
        return os;
    }

    public java.lang.String toString() {
        return "Session Start";
    }
}
