package com.navdy.hud.app.service.pandora.messages;

public class SessionTerminate extends com.navdy.hud.app.service.pandora.messages.BaseOutgoingEmptyMessage {
    public static final com.navdy.hud.app.service.pandora.messages.SessionTerminate INSTANCE = new com.navdy.hud.app.service.pandora.messages.SessionTerminate();

    public /* bridge */ /* synthetic */ byte[] buildPayload() throws java.io.IOException, com.navdy.hud.app.service.pandora.exceptions.StringOverflowException, com.navdy.hud.app.service.pandora.exceptions.UnexpectedEndOfStringException, com.navdy.hud.app.service.pandora.exceptions.MessageWrongWayException {
        return super.buildPayload();
    }

    private SessionTerminate() {
    }

    /* access modifiers changed from: protected */
    public byte getMessageType() {
        return 5;
    }

    public java.lang.String toString() {
        return "Session Terminate";
    }
}
