package com.navdy.hud.app.service.pandora.messages;

public class UpdateTrackElapsed extends com.navdy.hud.app.service.pandora.messages.BaseIncomingMessage {
    public short elapsed;
    public int trackToken;

    protected static com.navdy.hud.app.service.pandora.messages.BaseIncomingMessage innerBuildFromPayload(byte[] payload) throws com.navdy.hud.app.service.pandora.exceptions.MessageWrongWayException, com.navdy.hud.app.service.pandora.exceptions.CorruptedPayloadException {
        java.nio.ByteBuffer buffer = java.nio.ByteBuffer.wrap(payload);
        buffer.get();
        com.navdy.hud.app.service.pandora.messages.UpdateTrackElapsed result = new com.navdy.hud.app.service.pandora.messages.UpdateTrackElapsed();
        result.trackToken = buffer.getInt();
        result.elapsed = buffer.getShort();
        return result;
    }

    public java.lang.String toString() {
        return "Got track's elapsed time update - seconds: " + this.elapsed;
    }
}
