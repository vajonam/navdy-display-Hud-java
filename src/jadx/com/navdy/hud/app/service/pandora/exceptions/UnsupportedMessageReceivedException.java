package com.navdy.hud.app.service.pandora.exceptions;

public class UnsupportedMessageReceivedException extends java.lang.Exception {
    public UnsupportedMessageReceivedException(byte msgType) {
        super("Message received from Pandora is not supported: " + java.lang.String.format("%02X", new java.lang.Object[]{java.lang.Byte.valueOf(msgType)}));
    }
}
