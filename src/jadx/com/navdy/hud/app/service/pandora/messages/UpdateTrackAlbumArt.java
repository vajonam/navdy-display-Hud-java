package com.navdy.hud.app.service.pandora.messages;

public class UpdateTrackAlbumArt extends com.navdy.hud.app.service.pandora.messages.BaseIncomingMessage {
    private static int MESSAGE_LENGTH = 9;
    public int imageLength;
    public int trackToken;

    protected static com.navdy.hud.app.service.pandora.messages.BaseIncomingMessage innerBuildFromPayload(byte[] payload) throws com.navdy.hud.app.service.pandora.exceptions.MessageWrongWayException, com.navdy.hud.app.service.pandora.exceptions.CorruptedPayloadException {
        if (payload.length != MESSAGE_LENGTH) {
            throw new com.navdy.hud.app.service.pandora.exceptions.CorruptedPayloadException();
        }
        java.nio.ByteBuffer buffer = java.nio.ByteBuffer.wrap(payload);
        buffer.get();
        com.navdy.hud.app.service.pandora.messages.UpdateTrackAlbumArt result = new com.navdy.hud.app.service.pandora.messages.UpdateTrackAlbumArt();
        result.trackToken = buffer.getInt();
        result.imageLength = buffer.getInt();
        return result;
    }

    public java.lang.String toString() {
        return "Artwork image loaded on client for track: " + this.trackToken;
    }
}
