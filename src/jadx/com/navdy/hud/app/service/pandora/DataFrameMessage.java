package com.navdy.hud.app.service.pandora;

public class DataFrameMessage extends com.navdy.hud.app.service.pandora.FrameMessage {
    public DataFrameMessage(boolean isSequence0, byte[] payload) throws java.lang.IllegalArgumentException {
        super(isSequence0, payload);
        if (payload.length == 0) {
            throw new java.lang.IllegalArgumentException("Empty payload in data frame is not allowed");
        }
    }

    public byte[] buildFrame() {
        byte b = 0;
        int length = this.payload.length;
        java.nio.ByteBuffer put = java.nio.ByteBuffer.allocate(length + 6).put(0);
        if (!this.isSequence0) {
            b = 1;
        }
        return buildFrameFromCRCPart(put.put(b).putInt(length).put(this.payload));
    }

    protected static com.navdy.hud.app.service.pandora.DataFrameMessage parseDataFrame(byte[] dataFrame) throws java.lang.IllegalArgumentException {
        boolean isSequence0;
        if (dataFrame[0] == 126 && dataFrame[dataFrame.length - 1] == 124 && dataFrame[1] == 0) {
            java.nio.ByteBuffer buffer = unescapeBytes(dataFrame);
            buffer.position(2);
            byte sequence = buffer.get();
            if (sequence == 0) {
                isSequence0 = true;
            } else if (sequence == 1) {
                isSequence0 = false;
            } else {
                throw new java.lang.IllegalArgumentException("Unknown message frame sequence: " + java.lang.String.format("%02X", new java.lang.Object[]{java.lang.Byte.valueOf(sequence)}));
            }
            int length = buffer.getInt();
            if (length == 0) {
                throw new java.lang.IllegalArgumentException("Data frame has 0 as payload's length");
            }
            byte[] payload = new byte[length];
            buffer.get(payload);
            byte[] crc = new byte[2];
            buffer.get(crc);
            if (buffer.remaining() != 1) {
                throw new java.lang.IllegalArgumentException("Corrupted message frame");
            }
            byte[] crcData = new byte[(length + 6)];
            buffer.position(1);
            buffer.get(crcData);
            if (com.navdy.hud.app.service.pandora.CRC16CCITT.checkCRC(crcData, crc)) {
                return new com.navdy.hud.app.service.pandora.DataFrameMessage(isSequence0, payload);
            }
            throw new java.lang.IllegalArgumentException("CRC fail");
        }
        throw new java.lang.IllegalArgumentException("Data frame start/stop/type bytes not in place");
    }
}
