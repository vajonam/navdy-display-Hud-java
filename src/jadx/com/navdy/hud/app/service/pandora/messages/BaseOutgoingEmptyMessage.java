package com.navdy.hud.app.service.pandora.messages;

abstract class BaseOutgoingEmptyMessage extends com.navdy.hud.app.service.pandora.messages.BaseOutgoingConstantMessage {
    /* access modifiers changed from: protected */
    public abstract byte getMessageType();

    BaseOutgoingEmptyMessage() {
    }

    /* access modifiers changed from: protected */
    public java.io.ByteArrayOutputStream putThis(java.io.ByteArrayOutputStream os) throws java.io.IOException, com.navdy.hud.app.service.pandora.exceptions.StringOverflowException, com.navdy.hud.app.service.pandora.exceptions.UnexpectedEndOfStringException {
        putByte(os, getMessageType());
        return os;
    }
}
