package com.navdy.hud.app.service.pandora.messages;

public class ReturnTrackAlbumArtSegment extends com.navdy.hud.app.service.pandora.messages.BaseIncomingMessage {
    public byte[] data;
    public byte segmentIndex;
    public byte totalSegments;
    public int trackToken;

    protected static com.navdy.hud.app.service.pandora.messages.BaseIncomingMessage innerBuildFromPayload(byte[] payload) throws com.navdy.hud.app.service.pandora.exceptions.MessageWrongWayException, com.navdy.hud.app.service.pandora.exceptions.CorruptedPayloadException {
        java.nio.ByteBuffer buffer = java.nio.ByteBuffer.wrap(payload);
        buffer.get();
        com.navdy.hud.app.service.pandora.messages.ReturnTrackAlbumArtSegment result = new com.navdy.hud.app.service.pandora.messages.ReturnTrackAlbumArtSegment();
        result.trackToken = buffer.getInt();
        result.segmentIndex = buffer.get();
        result.totalSegments = buffer.get();
        result.data = new byte[buffer.remaining()];
        buffer.get(result.data);
        return result;
    }

    public java.lang.String toString() {
        return "Got artwork's segment " + (this.segmentIndex + 1) + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.SLASH + this.totalSegments + " for track with token " + this.trackToken;
    }
}
