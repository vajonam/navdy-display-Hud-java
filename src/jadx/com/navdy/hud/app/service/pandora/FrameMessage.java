package com.navdy.hud.app.service.pandora;

abstract class FrameMessage {
    protected static final byte[] ACK_PAYLOAD = new byte[0];
    protected static final int ADDITIONAL_BYTES_LENGTH = 6;
    protected static final int CRC_BYTES_LENGTH = 2;
    protected static final int FRAME_TYPE_POSITION = 1;
    protected static final byte PNDR_FRAME_END = 124;
    private static final byte PNDR_FRAME_ESCAPE = 125;
    private static final java.util.Map<java.lang.Byte, java.lang.Byte> PNDR_FRAME_ESCAPE_MAPPING = new com.navdy.hud.app.service.pandora.FrameMessage.Anon1();
    protected static final byte PNDR_FRAME_SEQUENCE_0 = 0;
    protected static final byte PNDR_FRAME_SEQUENCE_1 = 1;
    protected static final byte PNDR_FRAME_START = 126;
    protected static final byte PNDR_FRAME_TYPE_ACK = 1;
    protected static final byte PNDR_FRAME_TYPE_DATA = 0;
    private static final java.util.Map<java.lang.Byte, java.lang.Byte> PNDR_FRAME_UNESCAPE_MAPPING = new java.util.HashMap(PNDR_FRAME_ESCAPE_MAPPING.size());
    protected boolean isSequence0;
    protected byte[] payload;

    static class Anon1 extends java.util.HashMap<java.lang.Byte, java.lang.Byte> {
        Anon1() {
            put(java.lang.Byte.valueOf(com.navdy.hud.app.service.pandora.FrameMessage.PNDR_FRAME_START), new java.lang.Byte(94));
            put(java.lang.Byte.valueOf(com.navdy.hud.app.service.pandora.FrameMessage.PNDR_FRAME_END), new java.lang.Byte(92));
            put(java.lang.Byte.valueOf(com.navdy.hud.app.service.pandora.FrameMessage.PNDR_FRAME_ESCAPE), new java.lang.Byte(93));
        }
    }

    public abstract byte[] buildFrame();

    static {
        for (java.util.Map.Entry<java.lang.Byte, java.lang.Byte> entry : PNDR_FRAME_ESCAPE_MAPPING.entrySet()) {
            PNDR_FRAME_UNESCAPE_MAPPING.put(entry.getValue(), entry.getKey());
        }
    }

    protected FrameMessage(boolean isSequence02, byte[] payload2) {
        this.isSequence0 = isSequence02;
        this.payload = payload2;
    }

    protected static byte[] escapeBytes(java.nio.ByteBuffer in) {
        java.io.ByteArrayOutputStream byteStream = new java.io.ByteArrayOutputStream();
        for (int i = 0; i < in.limit(); i++) {
            byte b = in.get(i);
            java.lang.Byte escapedB = (java.lang.Byte) PNDR_FRAME_ESCAPE_MAPPING.get(java.lang.Byte.valueOf(b));
            if (escapedB != null) {
                byteStream.write(125);
                byteStream.write(escapedB.byteValue());
            } else {
                byteStream.write(b);
            }
        }
        return byteStream.toByteArray();
    }

    protected static java.nio.ByteBuffer unescapeBytes(byte[] in) throws java.lang.IllegalArgumentException {
        java.io.ByteArrayOutputStream byteStream = new java.io.ByteArrayOutputStream();
        int length = in.length;
        int i = 0;
        while (i < length) {
            byte b = in[i];
            if (b == 125) {
                i++;
                try {
                    b = ((java.lang.Byte) PNDR_FRAME_UNESCAPE_MAPPING.get(java.lang.Byte.valueOf(in[i]))).byteValue();
                } catch (java.lang.ArrayIndexOutOfBoundsException e) {
                    throw new java.lang.IllegalArgumentException("Escape byte not followed by a byte");
                } catch (java.lang.NullPointerException e2) {
                    throw new java.lang.IllegalArgumentException("Escape byte not followed by a proper byte");
                }
            }
            byteStream.write(b);
            i++;
        }
        return java.nio.ByteBuffer.wrap(byteStream.toByteArray());
    }

    protected static byte[] buildFrameFromCRCPart(java.nio.ByteBuffer bufferToCrc) {
        java.nio.ByteBuffer crc = com.navdy.hud.app.service.pandora.CRC16CCITT.calculate(bufferToCrc);
        byte[] escapedDataBytes = escapeBytes(bufferToCrc);
        byte[] escapedCRCBytes = escapeBytes(crc);
        return java.nio.ByteBuffer.allocate(escapedDataBytes.length + escapedCRCBytes.length + 2).put(PNDR_FRAME_START).put(escapedDataBytes).put(escapedCRCBytes).put(PNDR_FRAME_END).array();
    }

    public static com.navdy.hud.app.service.pandora.FrameMessage parseFrame(byte[] frame) throws java.lang.IllegalArgumentException {
        byte type = frame[1];
        if (type == 1) {
            return com.navdy.hud.app.service.pandora.AckFrameMessage.parseAckFrame(frame);
        }
        if (type == 0) {
            return com.navdy.hud.app.service.pandora.DataFrameMessage.parseDataFrame(frame);
        }
        throw new java.lang.IllegalArgumentException("Unknown message frame type: " + java.lang.String.format("%02X", new java.lang.Object[]{java.lang.Byte.valueOf(type)}));
    }
}
