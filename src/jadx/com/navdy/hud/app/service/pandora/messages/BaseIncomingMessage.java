package com.navdy.hud.app.service.pandora.messages;

public abstract class BaseIncomingMessage extends com.navdy.hud.app.service.pandora.messages.BaseMessage {
    protected static boolean getBoolean(java.nio.ByteBuffer buffer) {
        return buffer.get() != 0;
    }

    protected static java.lang.String getFixedLengthASCIIString(java.nio.ByteBuffer buffer, int fixedLength) {
        java.io.ByteArrayOutputStream byteArrayOS = null;
        for (int i = 0; i < fixedLength; i++) {
            byte b = buffer.get();
            if (b == 0 && byteArrayOS != null) {
                break;
            }
            if (b != 0) {
                if (byteArrayOS == null) {
                    byteArrayOS = new java.io.ByteArrayOutputStream();
                }
                byteArrayOS.write(b);
            }
        }
        java.lang.String result = "";
        if (byteArrayOS == null) {
            return result;
        }
        java.lang.String result2 = new java.lang.String(byteArrayOS.toByteArray(), FIXED_LENGTH_STRING_ENCODING);
        com.navdy.service.library.util.IOUtils.closeStream(byteArrayOS);
        return result2;
    }

    protected static java.lang.String getString(java.nio.ByteBuffer buffer) throws com.navdy.hud.app.service.pandora.exceptions.UnterminatedStringException, com.navdy.hud.app.service.pandora.exceptions.StringOverflowException {
        java.io.ByteArrayOutputStream byteArrayOS = new java.io.ByteArrayOutputStream();
        int currentLength = 0;
        byte b = buffer.get();
        while (b != 0) {
            if (b < 0) {
                throw new com.navdy.hud.app.service.pandora.exceptions.UnterminatedStringException();
            }
            byteArrayOS.write(b);
            currentLength++;
            if (currentLength > 247) {
                throw new com.navdy.hud.app.service.pandora.exceptions.StringOverflowException();
            }
            b = buffer.get();
        }
        return new java.lang.String(byteArrayOS.toByteArray(), STRING_ENCODING);
    }

    protected static com.navdy.hud.app.service.pandora.messages.BaseIncomingMessage innerBuildFromPayload(byte[] payload) throws com.navdy.hud.app.service.pandora.exceptions.MessageWrongWayException, com.navdy.hud.app.service.pandora.exceptions.CorruptedPayloadException {
        throw new com.navdy.hud.app.service.pandora.exceptions.MessageWrongWayException();
    }

    public static com.navdy.hud.app.service.pandora.messages.BaseIncomingMessage buildFromPayload(byte[] payload) throws com.navdy.hud.app.service.pandora.exceptions.MessageWrongWayException, com.navdy.hud.app.service.pandora.exceptions.CorruptedPayloadException, com.navdy.hud.app.service.pandora.exceptions.UnsupportedMessageReceivedException {
        switch (payload[0]) {
            case -127:
                return com.navdy.hud.app.service.pandora.messages.UpdateStatus.innerBuildFromPayload(payload);
            case -112:
                return com.navdy.hud.app.service.pandora.messages.UpdateTrack.innerBuildFromPayload(payload);
            case -107:
                return com.navdy.hud.app.service.pandora.messages.ReturnTrackAlbumArtSegment.innerBuildFromPayload(payload);
            case -106:
                return com.navdy.hud.app.service.pandora.messages.UpdateTrackAlbumArt.innerBuildFromPayload(payload);
            case -105:
                return com.navdy.hud.app.service.pandora.messages.UpdateTrackElapsed.innerBuildFromPayload(payload);
            case -99:
                return com.navdy.hud.app.service.pandora.messages.ReturnTrackInfoExtended.innerBuildFromPayload(payload);
            case -98:
                return com.navdy.hud.app.service.pandora.messages.UpdateTrackCompleted.innerBuildFromPayload(payload);
            case -70:
                return com.navdy.hud.app.service.pandora.messages.UpdateStationActive.innerBuildFromPayload(payload);
            default:
                throw new com.navdy.hud.app.service.pandora.exceptions.UnsupportedMessageReceivedException(payload[0]);
        }
    }
}
