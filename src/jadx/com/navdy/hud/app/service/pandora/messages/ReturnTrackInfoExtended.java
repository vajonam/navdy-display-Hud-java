package com.navdy.hud.app.service.pandora.messages;

public class ReturnTrackInfoExtended extends com.navdy.hud.app.service.pandora.messages.BaseIncomingMessage {
    public java.lang.String album;
    public int albumArtLength;
    public java.lang.String artist;
    public short duration;
    public short elapsed;
    public byte identityFlags;
    public byte permissionFlags;
    public byte rating;
    public java.lang.String title;
    public int trackToken;

    protected static com.navdy.hud.app.service.pandora.messages.BaseIncomingMessage innerBuildFromPayload(byte[] payload) throws com.navdy.hud.app.service.pandora.exceptions.MessageWrongWayException, com.navdy.hud.app.service.pandora.exceptions.CorruptedPayloadException {
        java.nio.ByteBuffer buffer = java.nio.ByteBuffer.wrap(payload);
        buffer.get();
        com.navdy.hud.app.service.pandora.messages.ReturnTrackInfoExtended result = new com.navdy.hud.app.service.pandora.messages.ReturnTrackInfoExtended();
        result.trackToken = buffer.getInt();
        result.albumArtLength = buffer.getInt();
        result.duration = buffer.getShort();
        result.elapsed = buffer.getShort();
        result.rating = buffer.get();
        result.permissionFlags = buffer.get();
        result.identityFlags = buffer.get();
        try {
            result.title = getString(buffer);
            result.artist = getString(buffer);
            result.album = getString(buffer);
            return result;
        } catch (com.navdy.hud.app.service.pandora.exceptions.StringOverflowException | com.navdy.hud.app.service.pandora.exceptions.UnterminatedStringException e) {
            throw new com.navdy.hud.app.service.pandora.exceptions.CorruptedPayloadException();
        }
    }

    public java.lang.String toString() {
        return "Got extended track's info for track with token: " + this.trackToken;
    }
}
