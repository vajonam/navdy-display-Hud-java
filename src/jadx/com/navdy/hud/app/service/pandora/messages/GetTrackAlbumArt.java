package com.navdy.hud.app.service.pandora.messages;

public class GetTrackAlbumArt extends com.navdy.hud.app.service.pandora.messages.BaseOutgoingConstantMessage {
    public static final com.navdy.hud.app.service.pandora.messages.GetTrackAlbumArt INSTANCE = new com.navdy.hud.app.service.pandora.messages.GetTrackAlbumArt();

    public /* bridge */ /* synthetic */ byte[] buildPayload() throws java.io.IOException, com.navdy.hud.app.service.pandora.exceptions.StringOverflowException, com.navdy.hud.app.service.pandora.exceptions.UnexpectedEndOfStringException, com.navdy.hud.app.service.pandora.exceptions.MessageWrongWayException {
        return super.buildPayload();
    }

    private GetTrackAlbumArt() {
    }

    /* access modifiers changed from: protected */
    public java.io.ByteArrayOutputStream putThis(java.io.ByteArrayOutputStream os) throws java.io.IOException, com.navdy.hud.app.service.pandora.exceptions.StringOverflowException, com.navdy.hud.app.service.pandora.exceptions.UnexpectedEndOfStringException, com.navdy.hud.app.service.pandora.exceptions.MessageWrongWayException {
        putByte(os, com.navdy.hud.app.service.pandora.messages.BaseMessage.PNDR_GET_TRACK_ALBUM_ART);
        putInt(os, com.navdy.hud.app.service.pandora.messages.BaseMessage.MAX_ARTWORK_PAYLOAD_SIZE);
        return os;
    }

    public java.lang.String toString() {
        return "Requesting album artwork";
    }
}
