package com.navdy.hud.app.service.pandora;

public class PandoraManager implements java.io.Closeable {
    private static final int ACK_WAITING_TIME = 750;
    private static final java.util.concurrent.atomic.AtomicInteger CONNECT_THREAD_COUNTER = new java.util.concurrent.atomic.AtomicInteger(1);
    private static final com.navdy.service.library.events.audio.MusicTrackInfo.Builder INITIAL_TRACK_INFO_BUILDER = new com.navdy.service.library.events.audio.MusicTrackInfo.Builder().playbackState(com.navdy.service.library.events.audio.MusicPlaybackState.PLAYBACK_NONE).dataSource(com.navdy.service.library.events.audio.MusicDataSource.MUSIC_SOURCE_PANDORA_API);
    private static final int MAX_SEND_MESSAGE_RETRIES = 10;
    /* access modifiers changed from: private */
    public static final java.util.UUID PANDORA_UUID = java.util.UUID.fromString("453994D5-D58B-96F9-6616-B37F586BA2EC");
    /* access modifiers changed from: private */
    public static final java.util.concurrent.atomic.AtomicInteger READ_THREAD_COUNTER = new java.util.concurrent.atomic.AtomicInteger(1);
    private static final android.os.Handler handler = new android.os.Handler();
    static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.service.pandora.PandoraManager.class);
    private java.lang.Runnable activeAckMessageTimeoutCounter = null;
    private java.lang.String adTitle = "";
    private com.squareup.otto.Bus bus;
    private java.io.ByteArrayOutputStream currentArtwork = null;
    private com.navdy.service.library.events.audio.MusicTrackInfo currentTrack = INITIAL_TRACK_INFO_BUILDER.build();
    private byte expectedArtworkSegmentIndex = 0;
    /* access modifiers changed from: private */
    public java.util.concurrent.atomic.AtomicBoolean isConnectingInProgress = new java.util.concurrent.atomic.AtomicBoolean(false);
    private boolean isCurrentSequence0 = true;
    private java.util.concurrent.ConcurrentLinkedQueue<com.navdy.hud.app.service.pandora.messages.BaseOutgoingMessage> msgQueue = new java.util.concurrent.ConcurrentLinkedQueue<>();
    /* access modifiers changed from: private */
    public java.lang.String remoteDeviceId = null;
    private android.bluetooth.BluetoothSocket socket = null;

    class Anon1 implements java.lang.Runnable {
        final /* synthetic */ byte[] val$frame;
        final /* synthetic */ int val$newValueLeftAttempts;

        Anon1(byte[] bArr, int i) {
            this.val$frame = bArr;
            this.val$newValueLeftAttempts = i;
        }

        public void run() {
            com.navdy.hud.app.service.pandora.PandoraManager.this.sendDataMessage(this.val$frame, this.val$newValueLeftAttempts);
        }
    }

    class ConnectRunnable implements java.lang.Runnable {
        private static final int MAX_CONNECTING_RETRIES = 10;
        private static final int PAUSE_BETWEEN_CONNECTING_RETRIES = 2000;
        private boolean isStartMusicOn = false;

        public ConnectRunnable(boolean isStartMusicOn2) {
            this.isStartMusicOn = isStartMusicOn2;
        }

        private void safeSleepPause() {
            try {
                java.lang.Thread.sleep(2000);
            } catch (java.lang.InterruptedException e) {
                com.navdy.hud.app.service.pandora.PandoraManager.sLogger.e("Interrupted while pausing between Pandora connection retries");
            }
        }

        public void run() {
            android.bluetooth.BluetoothSocket tempSocket;
            int retry;
            try {
                if (com.navdy.hud.app.service.pandora.PandoraManager.this.isConnected()) {
                    com.navdy.hud.app.service.pandora.PandoraManager.this.terminateAndClose();
                }
                android.bluetooth.BluetoothDevice device = android.bluetooth.BluetoothAdapter.getDefaultAdapter().getRemoteDevice(new com.navdy.service.library.device.NavdyDeviceId(com.navdy.hud.app.service.pandora.PandoraManager.this.remoteDeviceId).getBluetoothAddress());
                tempSocket = null;
                retry = 0;
                while (retry < 10) {
                    tempSocket = device.createRfcommSocketToServiceRecord(com.navdy.hud.app.service.pandora.PandoraManager.PANDORA_UUID);
                    tempSocket.connect();
                }
                com.navdy.hud.app.service.pandora.PandoraManager.this.setSocket(tempSocket);
                if (com.navdy.hud.app.service.pandora.PandoraManager.this.isConnected()) {
                    com.navdy.hud.app.service.pandora.PandoraManager.this.sendOrQueueMessage(com.navdy.hud.app.service.pandora.messages.SessionStart.INSTANCE);
                    com.navdy.hud.app.service.pandora.PandoraManager.this.sendOrQueueMessage(com.navdy.hud.app.service.pandora.messages.GetTrackInfoExtended.INSTANCE);
                    com.navdy.hud.app.service.pandora.PandoraManager.this.sendOrQueueMessage(com.navdy.hud.app.service.pandora.messages.SetTrackElapsedPolling.ENABLED);
                    if (this.isStartMusicOn) {
                        com.navdy.hud.app.service.pandora.PandoraManager.this.sendOrQueueMessage(com.navdy.hud.app.service.pandora.messages.EventTrackPlay.INSTANCE);
                    }
                    java.lang.Thread thread = new java.lang.Thread(new com.navdy.hud.app.service.pandora.ReadMessagesThread(com.navdy.hud.app.service.pandora.PandoraManager.this));
                    thread.setName("Pandora-Read-" + com.navdy.hud.app.service.pandora.PandoraManager.READ_THREAD_COUNTER.getAndIncrement());
                    thread.start();
                }
                com.navdy.hud.app.service.pandora.PandoraManager.this.isConnectingInProgress.set(false);
            } catch (java.io.IOException e) {
                com.navdy.hud.app.service.pandora.PandoraManager.sLogger.e("Cannot connect to Pandora on remote device: " + com.navdy.hud.app.service.pandora.PandoraManager.this.remoteDeviceId);
                com.navdy.service.library.util.IOUtils.closeObject(tempSocket);
                safeSleepPause();
                retry++;
            } catch (Throwable t) {
                com.navdy.hud.app.service.pandora.PandoraManager.sLogger.e(java.lang.Thread.currentThread().getName(), t);
            }
            com.navdy.hud.app.service.pandora.PandoraManager.sLogger.v("exiting thread:" + java.lang.Thread.currentThread().getName());
        }
    }

    public PandoraManager(com.squareup.otto.Bus bus2, android.content.res.Resources resources) {
        this.bus = bus2;
        this.adTitle = resources.getString(com.navdy.hud.app.R.string.music_pandora_ad_title);
    }

    private void sendByteArray(byte[] frame) throws java.io.IOException {
        java.io.OutputStream os = getSocket().getOutputStream();
        os.write(frame);
        os.flush();
    }

    /* access modifiers changed from: private */
    public void sendDataMessage(byte[] frame, int leftAttempts) {
        if (leftAttempts <= 0) {
            sLogger.e("Max number of retries for the message achieved - closing connection");
            close();
            return;
        }
        int newValueLeftAttempts = leftAttempts - 1;
        try {
            sendByteArray(frame);
            this.activeAckMessageTimeoutCounter = new com.navdy.hud.app.service.pandora.PandoraManager.Anon1(frame, newValueLeftAttempts);
            handler.postDelayed(this.activeAckMessageTimeoutCounter, 750);
        } catch (java.io.IOException e) {
            sLogger.e("Cannot send message frame through the socket - disconnecting", e);
            close();
        }
    }

    public void sendOrQueueMessage(com.navdy.hud.app.service.pandora.messages.BaseOutgoingMessage msg) {
        this.msgQueue.add(msg);
        if (this.activeAckMessageTimeoutCounter == null) {
            sendNextMessage();
        }
    }

    private void sendNextMessage() {
        try {
            com.navdy.hud.app.service.pandora.messages.BaseOutgoingMessage msg = (com.navdy.hud.app.service.pandora.messages.BaseOutgoingMessage) this.msgQueue.poll();
            if (msg == null) {
                sLogger.v("Queue empty: no new message waiting to be sent");
                return;
            }
            sLogger.i("Sending message to Pandora: " + msg.toString());
            sendDataMessage(new com.navdy.hud.app.service.pandora.DataFrameMessage(this.isCurrentSequence0, msg.buildPayload()).buildFrame(), 10);
        } catch (java.lang.Exception e) {
            sLogger.e("Cannot send data message - disconnecting", e);
            close();
        }
    }

    private void sendAckMessage(boolean isAckSequence0) {
        byte[] ackMsgFrame;
        if (isAckSequence0) {
            ackMsgFrame = com.navdy.hud.app.service.pandora.AckFrameMessage.ACK_WITH_SEQUENCE_0.buildFrame();
        } else {
            ackMsgFrame = com.navdy.hud.app.service.pandora.AckFrameMessage.ACK_WITH_SEQUENCE_1.buildFrame();
        }
        try {
            sendByteArray(ackMsgFrame);
            if (sLogger.isLoggable(2)) {
                sLogger.i("Ack message sent to Pandora - is sequence 0: " + isAckSequence0);
            }
        } catch (java.io.IOException e) {
            sLogger.e("Cannot send ACK message frame through the socket - disconnecting", e);
            close();
        }
    }

    /* access modifiers changed from: protected */
    public void onFrameReceived(byte[] frame) {
        try {
            com.navdy.hud.app.service.pandora.FrameMessage msg = com.navdy.hud.app.service.pandora.FrameMessage.parseFrame(frame);
            if (msg instanceof com.navdy.hud.app.service.pandora.AckFrameMessage) {
                if (sLogger.isLoggable(2)) {
                    sLogger.i("Ack message sent to Pandora - is sequence 0: " + msg.isSequence0);
                }
                if (this.activeAckMessageTimeoutCounter == null || this.isCurrentSequence0 == msg.isSequence0) {
                    sLogger.e("Invalid ACK message received - ignoring it");
                    return;
                }
                handler.removeCallbacks(this.activeAckMessageTimeoutCounter);
                this.isCurrentSequence0 = msg.isSequence0;
                this.activeAckMessageTimeoutCounter = null;
                sendNextMessage();
            } else if (msg instanceof com.navdy.hud.app.service.pandora.DataFrameMessage) {
                sendAckMessage(!msg.isSequence0);
                com.navdy.hud.app.service.pandora.messages.BaseIncomingMessage parsedMsg = null;
                try {
                    parsedMsg = com.navdy.hud.app.service.pandora.messages.BaseIncomingMessage.buildFromPayload(msg.payload);
                    if (sLogger.isLoggable(2)) {
                        sLogger.i("Received (and ACKed) message from Pandora: " + parsedMsg.toString());
                    }
                } catch (com.navdy.hud.app.service.pandora.exceptions.CorruptedPayloadException | com.navdy.hud.app.service.pandora.exceptions.MessageWrongWayException | com.navdy.hud.app.service.pandora.exceptions.UnsupportedMessageReceivedException e) {
                    sLogger.w(e.getMessage() + " - ignoring the message (already ACKed)");
                }
                if (parsedMsg != null) {
                    onMessageReceived(parsedMsg);
                }
            } else {
                sLogger.e("Unsupported message type");
            }
        } catch (java.lang.IllegalArgumentException e2) {
            sLogger.e("Cannot parse received message frame", e2);
        }
    }

    private boolean isFlagInField(byte flag, byte flagsField) {
        return (flagsField & flag) != 0;
    }

    private void onMessageReceived(com.navdy.hud.app.service.pandora.messages.BaseIncomingMessage msg) {
        if (msg instanceof com.navdy.hud.app.service.pandora.messages.UpdateStatus) {
            onUpdateStatus((com.navdy.hud.app.service.pandora.messages.UpdateStatus) msg);
        } else if (msg instanceof com.navdy.hud.app.service.pandora.messages.UpdateTrack) {
            onUpdateTrack((com.navdy.hud.app.service.pandora.messages.UpdateTrack) msg);
        } else if (msg instanceof com.navdy.hud.app.service.pandora.messages.UpdateTrackCompleted) {
            onUpdateTrackCompleted((com.navdy.hud.app.service.pandora.messages.UpdateTrackCompleted) msg);
        } else if (msg instanceof com.navdy.hud.app.service.pandora.messages.ReturnTrackInfoExtended) {
            onReturnTrackInfoExtended((com.navdy.hud.app.service.pandora.messages.ReturnTrackInfoExtended) msg);
        } else if (msg instanceof com.navdy.hud.app.service.pandora.messages.UpdateTrackAlbumArt) {
            onUpdateTrackAlbumArt((com.navdy.hud.app.service.pandora.messages.UpdateTrackAlbumArt) msg);
        } else if (msg instanceof com.navdy.hud.app.service.pandora.messages.ReturnTrackAlbumArtSegment) {
            onReturnTrackAlbumArtSegment((com.navdy.hud.app.service.pandora.messages.ReturnTrackAlbumArtSegment) msg);
        } else if (msg instanceof com.navdy.hud.app.service.pandora.messages.UpdateTrackElapsed) {
            onUpdateTrackElapsed((com.navdy.hud.app.service.pandora.messages.UpdateTrackElapsed) msg);
        } else {
            sLogger.w("Received unsupported message from Pandora - ignoring: " + msg.toString());
        }
    }

    private void onUpdateStatus(com.navdy.hud.app.service.pandora.messages.UpdateStatus msg) {
        com.navdy.service.library.events.audio.MusicTrackInfo.Builder resultBuilder = new com.navdy.service.library.events.audio.MusicTrackInfo.Builder(this.currentTrack);
        switch (msg.value) {
            case PLAYING:
                resultBuilder.playbackState(com.navdy.service.library.events.audio.MusicPlaybackState.PLAYBACK_PLAYING);
                break;
            case PAUSED:
                resultBuilder.playbackState(com.navdy.service.library.events.audio.MusicPlaybackState.PLAYBACK_PAUSED);
                break;
            default:
                resultBuilder.playbackState(com.navdy.service.library.events.audio.MusicPlaybackState.PLAYBACK_NONE);
                break;
        }
        setCurrentTrack(resultBuilder.build());
        if (resultBuilder.playbackState == com.navdy.service.library.events.audio.MusicPlaybackState.PLAYBACK_NONE) {
            sLogger.w("Receive bad status from Pandora: " + msg.value);
            terminateAndClose();
        }
    }

    private void onUpdateTrackCompleted(com.navdy.hud.app.service.pandora.messages.UpdateTrackCompleted msg) {
        resetCurrentArtwork();
    }

    private void onUpdateTrack(com.navdy.hud.app.service.pandora.messages.UpdateTrack msg) {
        if (msg.value != 0) {
            try {
                sendOrQueueMessage(com.navdy.hud.app.service.pandora.messages.GetTrackInfoExtended.INSTANCE);
            } catch (java.lang.Exception e) {
                sLogger.e("Cannot send request message for track's info - disconnecting", e);
                terminateAndClose();
            }
        }
    }

    private void onReturnTrackInfoExtended(com.navdy.hud.app.service.pandora.messages.ReturnTrackInfoExtended msg) {
        java.lang.String title = msg.title;
        if (android.text.TextUtils.isEmpty(title) && android.text.TextUtils.isEmpty(msg.album) && android.text.TextUtils.isEmpty(msg.artist) && msg.duration > 0) {
            title = this.adTitle;
            resetCurrentArtwork();
        } else if (msg.albumArtLength > 0) {
            requestArtwork();
        }
        setCurrentTrack(new com.navdy.service.library.events.audio.MusicTrackInfo.Builder(this.currentTrack).index(java.lang.Long.valueOf((long) msg.trackToken)).name(title).album(msg.album).author(msg.artist).duration(java.lang.Integer.valueOf(msg.duration * 1000)).currentPosition(java.lang.Integer.valueOf(secondsToMilliseconds(msg.elapsed))).isPreviousAllowed(java.lang.Boolean.valueOf(false)).isNextAllowed(java.lang.Boolean.valueOf(isFlagInField(2, msg.permissionFlags))).isOnlineStream(java.lang.Boolean.valueOf(true)).build());
    }

    private void onUpdateTrackAlbumArt(com.navdy.hud.app.service.pandora.messages.UpdateTrackAlbumArt msg) {
        if (this.currentTrack != null && this.currentTrack.index != null) {
            if (this.currentTrack.index.longValue() != ((long) msg.trackToken)) {
                sLogger.w("Received update for artwork on the wrong song - ignoring");
            } else {
                requestArtwork();
            }
        }
    }

    private void onReturnTrackAlbumArtSegment(com.navdy.hud.app.service.pandora.messages.ReturnTrackAlbumArtSegment msg) {
        if (this.currentTrack != null && this.currentTrack.index != null) {
            if (((long) msg.trackToken) == this.currentTrack.index.longValue() && msg.segmentIndex == this.expectedArtworkSegmentIndex) {
                if (this.currentArtwork == null) {
                    if (msg.segmentIndex != 0) {
                        sLogger.e("Wrong initial artwork's segment sent - ignoring it");
                        return;
                    }
                    this.currentArtwork = new java.io.ByteArrayOutputStream();
                }
                try {
                    this.currentArtwork.write(msg.data);
                    this.currentArtwork.flush();
                    this.expectedArtworkSegmentIndex = (byte) (msg.segmentIndex + 1);
                    if (this.expectedArtworkSegmentIndex == msg.totalSegments) {
                        this.bus.post(new com.navdy.service.library.events.photo.PhotoUpdate.Builder().identifier(java.lang.String.valueOf(msg.trackToken)).photo(okio.ByteString.of(this.currentArtwork.toByteArray())).photoType(com.navdy.service.library.events.photo.PhotoType.PHOTO_ALBUM_ART).build());
                        resetCurrentArtwork();
                    }
                } catch (java.io.IOException e) {
                    sLogger.e("Exception while processing artwork chunk: " + msg.segmentIndex);
                    resetCurrentArtwork();
                }
            } else {
                sLogger.e("Wrong artwork's segment sent - ignoring it");
            }
        }
    }

    private void onUpdateTrackElapsed(com.navdy.hud.app.service.pandora.messages.UpdateTrackElapsed msg) {
        if (this.currentTrack != null && this.currentTrack.index != null) {
            if (((long) msg.trackToken) != this.currentTrack.index.longValue()) {
                sLogger.e("Elapsed time received for wrong song - ignoring");
                return;
            }
            int elapsed = secondsToMilliseconds(msg.elapsed);
            if (elapsed != this.currentTrack.currentPosition.intValue()) {
                setCurrentTrack(new com.navdy.service.library.events.audio.MusicTrackInfo.Builder(this.currentTrack).currentPosition(java.lang.Integer.valueOf(elapsed)).build());
            }
        }
    }

    private int secondsToMilliseconds(short seconds) {
        return seconds * 1000;
    }

    private void setCurrentTrack(com.navdy.service.library.events.audio.MusicTrackInfo currentTrack2) {
        if (!this.currentTrack.equals(currentTrack2)) {
            this.currentTrack = currentTrack2;
            this.bus.post(this.currentTrack);
        }
    }

    private void resetCurrentArtwork() {
        com.navdy.service.library.util.IOUtils.closeStream(this.currentArtwork);
        this.currentArtwork = null;
        this.expectedArtworkSegmentIndex = 0;
    }

    private void requestArtwork() {
        resetCurrentArtwork();
        try {
            sendOrQueueMessage(com.navdy.hud.app.service.pandora.messages.GetTrackAlbumArt.INSTANCE);
        } catch (java.lang.Exception e) {
            sLogger.e("Exception while requesting pre-loaded artwork - ignoring", e);
        }
    }

    /* access modifiers changed from: protected */
    public void terminateAndClose() {
        sendOrQueueMessage(com.navdy.hud.app.service.pandora.messages.SessionTerminate.INSTANCE);
        close();
    }

    public void close() {
        if (this.activeAckMessageTimeoutCounter != null) {
            handler.removeCallbacks(this.activeAckMessageTimeoutCounter);
            this.activeAckMessageTimeoutCounter = null;
        }
        resetCurrentArtwork();
        setCurrentTrack(INITIAL_TRACK_INFO_BUILDER.build());
        com.navdy.service.library.util.IOUtils.closeStream(this.socket);
        setSocket(null);
        this.msgQueue.clear();
    }

    @com.squareup.otto.Subscribe
    public void onDeviceConnectionStateChange(com.navdy.service.library.events.connection.ConnectionStateChange connectionStateChange) {
        switch (connectionStateChange.state) {
            case CONNECTION_VERIFIED:
                this.remoteDeviceId = connectionStateChange.remoteDeviceId;
                return;
            case CONNECTION_DISCONNECTED:
                this.remoteDeviceId = null;
                close();
                return;
            default:
                return;
        }
    }

    @com.squareup.otto.Subscribe
    public void onNotificationsStatusUpdate(com.navdy.service.library.events.notification.NotificationsStatusUpdate event) {
        if (com.navdy.service.library.events.notification.ServiceType.SERVICE_PANDORA.equals(event.service)) {
            if (this.remoteDeviceId == null) {
                sLogger.e("Received Pandora app state message while remote device ID is unknown");
                return;
            }
            if (!com.navdy.service.library.events.DeviceInfo.Platform.PLATFORM_Android.equals(com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getRemoteDevicePlatform())) {
                sLogger.e("Pandora is supported only for Android clients for now");
                return;
            }
            boolean isClientsPandoraRunning = com.navdy.service.library.events.notification.NotificationsState.NOTIFICATIONS_ENABLED.equals(event.state);
            sLogger.d("Received Pandora's running status: " + isClientsPandoraRunning);
            if (isClientsPandoraRunning) {
                if (isConnected()) {
                    sLogger.i("Pandora is already connected - restarting connection");
                    terminateAndClose();
                }
                startConnecting(false);
                return;
            }
            terminateAndClose();
        }
    }

    public void startAndPlay() {
        if (!isConnected()) {
            startConnecting(true);
        } else {
            sendOrQueueMessage(com.navdy.hud.app.service.pandora.messages.EventTrackPlay.INSTANCE);
        }
    }

    private void startConnecting(boolean isStartMusicOn) {
        if (this.isConnectingInProgress.compareAndSet(false, true)) {
            java.lang.Thread thread = new java.lang.Thread(new com.navdy.hud.app.service.pandora.PandoraManager.ConnectRunnable(isStartMusicOn));
            thread.setName("Pandora-Connect-" + CONNECT_THREAD_COUNTER.getAndIncrement());
            thread.start();
            return;
        }
        sLogger.w("Connecting thread is already running - ignoring the request");
    }

    public synchronized android.bluetooth.BluetoothSocket getSocket() {
        return this.socket;
    }

    /* access modifiers changed from: private */
    public synchronized void setSocket(android.bluetooth.BluetoothSocket newSocket) {
        if (this.socket != newSocket) {
            if (newSocket == null) {
                com.navdy.service.library.util.IOUtils.closeStream(this.socket);
            }
            this.socket = newSocket;
        }
    }

    /* access modifiers changed from: private */
    public boolean isConnected() {
        android.bluetooth.BluetoothSocket tempSocket = getSocket();
        return tempSocket != null && tempSocket.isConnected();
    }
}
