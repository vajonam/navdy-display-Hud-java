package com.navdy.hud.app.service.pandora;

public class ReadMessagesThread implements java.lang.Runnable {
    private static final com.navdy.service.library.log.Logger sLogger = com.navdy.hud.app.service.pandora.PandoraManager.sLogger;
    private com.navdy.hud.app.service.pandora.PandoraManager parentManager;

    public ReadMessagesThread(com.navdy.hud.app.service.pandora.PandoraManager parentManager2) {
        this.parentManager = parentManager2;
    }

    public void run() {
        java.io.InputStream is = null;
        java.io.ByteArrayOutputStream buffer = null;
        try {
            android.bluetooth.BluetoothSocket socket = this.parentManager.getSocket();
            is = socket.getInputStream();
            int b = is.read();
            java.io.ByteArrayOutputStream buffer2 = null;
            while (b >= 0 && socket != null) {
                if (buffer2 != null) {
                    try {
                        buffer2.write(b);
                        if (b == 124) {
                            this.parentManager.onFrameReceived(buffer2.toByteArray());
                            buffer = null;
                        }
                        buffer = buffer2;
                    } catch (Throwable th) {
                        th = th;
                        buffer = buffer2;
                        com.navdy.service.library.util.IOUtils.closeStream(buffer);
                        com.navdy.service.library.util.IOUtils.closeStream(is);
                        this.parentManager.terminateAndClose();
                        throw th;
                    }
                } else if (b == 126) {
                    buffer = new java.io.ByteArrayOutputStream();
                    buffer.write(b);
                } else {
                    sLogger.e("Unexpected byte received - skipping: " + java.lang.String.format("%02X", new java.lang.Object[]{java.lang.Integer.valueOf(b)}));
                    buffer = buffer2;
                }
                b = is.read();
                buffer2 = buffer;
            }
            sLogger.w("End of input read from the stream");
            com.navdy.service.library.util.IOUtils.closeStream(buffer2);
            com.navdy.service.library.util.IOUtils.closeStream(is);
            this.parentManager.terminateAndClose();
            java.io.ByteArrayOutputStream byteArrayOutputStream = buffer2;
        } catch (Throwable th2) {
            e = th2;
        }
        sLogger.v("exiting thread:" + java.lang.Thread.currentThread().getName());
    }
}
