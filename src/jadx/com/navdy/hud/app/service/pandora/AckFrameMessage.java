package com.navdy.hud.app.service.pandora;

public class AckFrameMessage extends com.navdy.hud.app.service.pandora.FrameMessage {
    private static final java.nio.ByteBuffer ACK_FRAME_BASE = java.nio.ByteBuffer.allocate(6).put(1).put(0).putInt(0);
    public static final com.navdy.hud.app.service.pandora.AckFrameMessage ACK_WITH_SEQUENCE_0 = new com.navdy.hud.app.service.pandora.AckFrameMessage(true);
    private static final byte[] ACK_WITH_SEQUENCE_0_FRAME = buildFrameFromCRCPart(ACK_FRAME_BASE);
    public static final com.navdy.hud.app.service.pandora.AckFrameMessage ACK_WITH_SEQUENCE_1 = new com.navdy.hud.app.service.pandora.AckFrameMessage(false);
    private static final byte[] ACK_WITH_SEQUENCE_1_FRAME = buildFrameFromCRCPart(ACK_FRAME_BASE.put(1, 1));

    private AckFrameMessage(boolean isSequence0) {
        super(isSequence0, ACK_PAYLOAD);
    }

    public byte[] buildFrame() throws java.lang.IllegalArgumentException {
        return this.isSequence0 ? ACK_WITH_SEQUENCE_0_FRAME : ACK_WITH_SEQUENCE_1_FRAME;
    }

    protected static com.navdy.hud.app.service.pandora.AckFrameMessage parseAckFrame(byte[] ackFrame) throws java.lang.IllegalArgumentException {
        if (java.util.Arrays.equals(ackFrame, ACK_WITH_SEQUENCE_0_FRAME)) {
            return ACK_WITH_SEQUENCE_0;
        }
        if (java.util.Arrays.equals(ackFrame, ACK_WITH_SEQUENCE_1_FRAME)) {
            return ACK_WITH_SEQUENCE_1;
        }
        throw new java.lang.IllegalArgumentException("Not a valid ACK message frame");
    }
}
