package com.navdy.hud.app.service.pandora.messages;

abstract class BaseOutgoingConstantMessage extends com.navdy.hud.app.service.pandora.messages.BaseOutgoingMessage {
    private byte[] preBuiltPayload = null;

    BaseOutgoingConstantMessage() {
    }

    public byte[] buildPayload() throws java.io.IOException, com.navdy.hud.app.service.pandora.exceptions.StringOverflowException, com.navdy.hud.app.service.pandora.exceptions.UnexpectedEndOfStringException, com.navdy.hud.app.service.pandora.exceptions.MessageWrongWayException {
        if (this.preBuiltPayload == null) {
            this.preBuiltPayload = super.buildPayload();
        }
        return this.preBuiltPayload;
    }
}
