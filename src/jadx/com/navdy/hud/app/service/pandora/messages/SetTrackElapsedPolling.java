package com.navdy.hud.app.service.pandora.messages;

public class SetTrackElapsedPolling extends com.navdy.hud.app.service.pandora.messages.BaseOutgoingConstantMessage {
    public static final com.navdy.hud.app.service.pandora.messages.SetTrackElapsedPolling DISABLED = new com.navdy.hud.app.service.pandora.messages.SetTrackElapsedPolling(false);
    public static final com.navdy.hud.app.service.pandora.messages.SetTrackElapsedPolling ENABLED = new com.navdy.hud.app.service.pandora.messages.SetTrackElapsedPolling(true);
    public boolean isOn;

    public /* bridge */ /* synthetic */ byte[] buildPayload() throws java.io.IOException, com.navdy.hud.app.service.pandora.exceptions.StringOverflowException, com.navdy.hud.app.service.pandora.exceptions.UnexpectedEndOfStringException, com.navdy.hud.app.service.pandora.exceptions.MessageWrongWayException {
        return super.buildPayload();
    }

    private SetTrackElapsedPolling(boolean isOn2) {
        this.isOn = isOn2;
    }

    /* access modifiers changed from: protected */
    public java.io.ByteArrayOutputStream putThis(java.io.ByteArrayOutputStream os) throws java.io.IOException, com.navdy.hud.app.service.pandora.exceptions.StringOverflowException, com.navdy.hud.app.service.pandora.exceptions.UnexpectedEndOfStringException, com.navdy.hud.app.service.pandora.exceptions.MessageWrongWayException {
        putByte(os, com.navdy.hud.app.service.pandora.messages.BaseMessage.PNDR_SET_TRACK_ELAPSED_POLLING);
        putBoolean(os, this.isOn);
        return os;
    }

    public java.lang.String toString() {
        return "Setting track elapsed polling to: " + this.isOn;
    }
}
