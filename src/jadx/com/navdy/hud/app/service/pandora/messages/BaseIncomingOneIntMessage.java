package com.navdy.hud.app.service.pandora.messages;

public class BaseIncomingOneIntMessage extends com.navdy.hud.app.service.pandora.messages.BaseIncomingMessage {
    private static int MESSAGE_LENGTH = 5;
    private static com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.service.pandora.messages.BaseIncomingOneIntMessage.class);
    public int value;

    public BaseIncomingOneIntMessage(int value2) {
        this.value = value2;
    }

    protected static int parseIntValue(byte[] payload) throws com.navdy.hud.app.service.pandora.exceptions.MessageWrongWayException, com.navdy.hud.app.service.pandora.exceptions.CorruptedPayloadException {
        if (payload.length == MESSAGE_LENGTH) {
            return java.nio.ByteBuffer.wrap(payload).getInt(1);
        }
        throw new com.navdy.hud.app.service.pandora.exceptions.CorruptedPayloadException();
    }

    public java.lang.String toString() {
        sLogger.w("toString not overwritten in BaseIncomingOneIntMessage class");
        return "One int message received with value: " + this.value;
    }
}
