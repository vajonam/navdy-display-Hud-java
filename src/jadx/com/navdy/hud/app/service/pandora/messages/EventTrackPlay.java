package com.navdy.hud.app.service.pandora.messages;

public class EventTrackPlay extends com.navdy.hud.app.service.pandora.messages.BaseOutgoingEmptyMessage {
    public static final com.navdy.hud.app.service.pandora.messages.EventTrackPlay INSTANCE = new com.navdy.hud.app.service.pandora.messages.EventTrackPlay();

    public /* bridge */ /* synthetic */ byte[] buildPayload() throws java.io.IOException, com.navdy.hud.app.service.pandora.exceptions.StringOverflowException, com.navdy.hud.app.service.pandora.exceptions.UnexpectedEndOfStringException, com.navdy.hud.app.service.pandora.exceptions.MessageWrongWayException {
        return super.buildPayload();
    }

    private EventTrackPlay() {
    }

    /* access modifiers changed from: protected */
    public byte getMessageType() {
        return com.navdy.hud.app.service.pandora.messages.BaseMessage.PNDR_EVENT_TRACK_PLAY;
    }

    public java.lang.String toString() {
        return "Play music action";
    }
}
