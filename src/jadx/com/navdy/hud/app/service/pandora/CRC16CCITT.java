package com.navdy.hud.app.service.pandora;

class CRC16CCITT {
    private static final int CRC_INITIAL_VALUE = 65535;

    CRC16CCITT() {
    }

    public static java.nio.ByteBuffer calculate(java.nio.ByteBuffer buffer) {
        int crc = 65535;
        for (int i = 0; i < buffer.limit(); i++) {
            int crc2 = (((crc >>> 8) | (crc << 8)) & 65535) ^ (buffer.get(i) & 255);
            int crc3 = crc2 ^ ((crc2 & 255) >> 4);
            int crc4 = crc3 ^ ((crc3 << 12) & 65535);
            crc = crc4 ^ (((crc4 & 255) << 5) & 65535);
        }
        return java.nio.ByteBuffer.allocate(2).putShort((short) crc);
    }

    public static boolean checkCRC(byte[] data, byte[] crc) {
        return java.util.Arrays.equals(calculate(java.nio.ByteBuffer.wrap(data)).array(), crc);
    }
}
