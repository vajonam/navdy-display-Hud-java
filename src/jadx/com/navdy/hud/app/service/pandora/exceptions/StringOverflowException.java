package com.navdy.hud.app.service.pandora.exceptions;

public class StringOverflowException extends java.lang.Exception {
    public StringOverflowException() {
        super("Maximum byte length for String value reached");
    }
}
