package com.navdy.hud.app.service.pandora.messages;

public class UpdateStationActive extends com.navdy.hud.app.service.pandora.messages.BaseIncomingOneIntMessage {
    public UpdateStationActive(int value) {
        super(value);
    }

    protected static com.navdy.hud.app.service.pandora.messages.BaseIncomingMessage innerBuildFromPayload(byte[] payload) throws com.navdy.hud.app.service.pandora.exceptions.MessageWrongWayException, com.navdy.hud.app.service.pandora.exceptions.CorruptedPayloadException {
        return new com.navdy.hud.app.service.pandora.messages.UpdateStationActive(parseIntValue(payload));
    }

    public java.lang.String toString() {
        return "New active station with token: " + this.value;
    }
}
