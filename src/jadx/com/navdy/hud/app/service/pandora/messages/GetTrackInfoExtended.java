package com.navdy.hud.app.service.pandora.messages;

public class GetTrackInfoExtended extends com.navdy.hud.app.service.pandora.messages.BaseOutgoingEmptyMessage {
    public static final com.navdy.hud.app.service.pandora.messages.GetTrackInfoExtended INSTANCE = new com.navdy.hud.app.service.pandora.messages.GetTrackInfoExtended();

    public /* bridge */ /* synthetic */ byte[] buildPayload() throws java.io.IOException, com.navdy.hud.app.service.pandora.exceptions.StringOverflowException, com.navdy.hud.app.service.pandora.exceptions.UnexpectedEndOfStringException, com.navdy.hud.app.service.pandora.exceptions.MessageWrongWayException {
        return super.buildPayload();
    }

    private GetTrackInfoExtended() {
    }

    /* access modifiers changed from: protected */
    public byte getMessageType() {
        return com.navdy.hud.app.service.pandora.messages.BaseMessage.PNDR_GET_TRACK_INFO_EXTENDED;
    }

    public java.lang.String toString() {
        return "Getting extended track's info";
    }
}
