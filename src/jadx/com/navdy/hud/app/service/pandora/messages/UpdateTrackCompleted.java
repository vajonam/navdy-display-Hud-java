package com.navdy.hud.app.service.pandora.messages;

public class UpdateTrackCompleted extends com.navdy.hud.app.service.pandora.messages.BaseIncomingOneIntMessage {
    public UpdateTrackCompleted(int value) {
        super(value);
    }

    protected static com.navdy.hud.app.service.pandora.messages.BaseIncomingMessage innerBuildFromPayload(byte[] payload) throws com.navdy.hud.app.service.pandora.exceptions.MessageWrongWayException, com.navdy.hud.app.service.pandora.exceptions.CorruptedPayloadException {
        return new com.navdy.hud.app.service.pandora.messages.UpdateTrackCompleted(parseIntValue(payload));
    }

    public java.lang.String toString() {
        return "Track ended - token: " + this.value;
    }
}
