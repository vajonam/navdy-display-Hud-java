package com.navdy.hud.app.service.pandora.messages;

public abstract class BaseOutgoingMessage extends com.navdy.hud.app.service.pandora.messages.BaseMessage {
    protected static java.io.ByteArrayOutputStream putBoolean(java.io.ByteArrayOutputStream outStream, boolean value) throws java.io.IOException, com.navdy.hud.app.service.pandora.exceptions.StringOverflowException {
        outStream.write(value ? 1 : 0);
        return outStream;
    }

    protected static java.io.ByteArrayOutputStream putByte(java.io.ByteArrayOutputStream outStream, byte value) throws java.io.IOException, com.navdy.hud.app.service.pandora.exceptions.StringOverflowException {
        outStream.write(value);
        return outStream;
    }

    protected static java.io.ByteArrayOutputStream putShort(java.io.ByteArrayOutputStream outStream, short value) throws java.io.IOException, com.navdy.hud.app.service.pandora.exceptions.StringOverflowException {
        outStream.write(java.nio.ByteBuffer.allocate(2).putShort(value).array());
        return outStream;
    }

    protected static java.io.ByteArrayOutputStream putInt(java.io.ByteArrayOutputStream outStream, int value) throws java.io.IOException, com.navdy.hud.app.service.pandora.exceptions.StringOverflowException {
        outStream.write(java.nio.ByteBuffer.allocate(4).putInt(value).array());
        return outStream;
    }

    protected static java.io.ByteArrayOutputStream putFixedLengthASCIIString(java.io.ByteArrayOutputStream outStream, int fixedLength, java.lang.String value) throws java.io.IOException, com.navdy.hud.app.service.pandora.exceptions.StringOverflowException {
        byte[] bytes = value.getBytes(FIXED_LENGTH_STRING_ENCODING);
        if (bytes.length > fixedLength) {
            throw new com.navdy.hud.app.service.pandora.exceptions.StringOverflowException();
        }
        outStream.write(bytes);
        for (int i = bytes.length; i < fixedLength; i++) {
            outStream.write(0);
        }
        return outStream;
    }

    protected static java.io.ByteArrayOutputStream putString(java.io.ByteArrayOutputStream outStream, java.lang.String value) throws java.io.IOException, com.navdy.hud.app.service.pandora.exceptions.StringOverflowException, com.navdy.hud.app.service.pandora.exceptions.UnexpectedEndOfStringException {
        if (value.length() > 247) {
            throw new com.navdy.hud.app.service.pandora.exceptions.StringOverflowException();
        } else if (value.indexOf(0) >= 0) {
            throw new com.navdy.hud.app.service.pandora.exceptions.UnexpectedEndOfStringException();
        } else {
            outStream.write(value.getBytes(STRING_ENCODING));
            outStream.write(0);
            return outStream;
        }
    }

    /* access modifiers changed from: protected */
    public java.io.ByteArrayOutputStream putThis(java.io.ByteArrayOutputStream os) throws java.io.IOException, com.navdy.hud.app.service.pandora.exceptions.StringOverflowException, com.navdy.hud.app.service.pandora.exceptions.UnexpectedEndOfStringException, com.navdy.hud.app.service.pandora.exceptions.MessageWrongWayException {
        throw new com.navdy.hud.app.service.pandora.exceptions.MessageWrongWayException();
    }

    public byte[] buildPayload() throws java.io.IOException, com.navdy.hud.app.service.pandora.exceptions.StringOverflowException, com.navdy.hud.app.service.pandora.exceptions.UnexpectedEndOfStringException, com.navdy.hud.app.service.pandora.exceptions.MessageWrongWayException {
        java.io.ByteArrayOutputStream os = new java.io.ByteArrayOutputStream();
        try {
            putThis(os);
            return os.toByteArray();
        } finally {
            com.navdy.service.library.util.IOUtils.closeStream(os);
        }
    }
}
