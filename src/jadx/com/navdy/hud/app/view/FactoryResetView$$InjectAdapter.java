package com.navdy.hud.app.view;

public final class FactoryResetView$$InjectAdapter extends dagger.internal.Binding<com.navdy.hud.app.view.FactoryResetView> implements dagger.MembersInjector<com.navdy.hud.app.view.FactoryResetView> {
    private dagger.internal.Binding<com.navdy.hud.app.screen.FactoryResetScreen.Presenter> presenter;

    public FactoryResetView$$InjectAdapter() {
        super(null, "members/com.navdy.hud.app.view.FactoryResetView", false, com.navdy.hud.app.view.FactoryResetView.class);
    }

    public void attach(dagger.internal.Linker linker) {
        this.presenter = linker.requestBinding("com.navdy.hud.app.screen.FactoryResetScreen$Presenter", com.navdy.hud.app.view.FactoryResetView.class, getClass().getClassLoader());
    }

    public void getDependencies(java.util.Set<dagger.internal.Binding<?>> set, java.util.Set<dagger.internal.Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.presenter);
    }

    public void injectMembers(com.navdy.hud.app.view.FactoryResetView object) {
        object.presenter = (com.navdy.hud.app.screen.FactoryResetScreen.Presenter) this.presenter.get();
    }
}
