package com.navdy.hud.app.view;

public class ETAGaugePresenter extends com.navdy.hud.app.view.DashboardWidgetPresenter {
    static final /* synthetic */ boolean $assertionsDisabled;
    /* access modifiers changed from: private */
    public static final long TRIP_INFO_UPDATE_INTERVAL = java.util.concurrent.TimeUnit.MINUTES.toMillis(1);
    @butterknife.InjectView(2131624177)
    android.widget.TextView etaAmPmText;
    private com.navdy.hud.app.view.drawable.ETAProgressDrawable etaProgressDrawable;
    @butterknife.InjectView(2131624176)
    android.widget.TextView etaText;
    private java.lang.String etaTripGaugeName;
    @butterknife.InjectView(2131624167)
    android.view.ViewGroup etaView;
    private int farMargin;
    private java.lang.String feetLabel;
    private int fontSize18;
    private int fontSize30;
    private int gravity;
    private int greyColor;
    private boolean isNavigationActive;
    private java.lang.String kiloMetersLabel;
    private java.lang.String[] labelShort;
    private java.lang.String[] labels;
    /* access modifiers changed from: private */
    public android.os.Handler mHandler;
    /* access modifiers changed from: private */
    public java.lang.Runnable mTripInfoUpdateRunnable;
    private com.navdy.hud.app.framework.trips.TripManager mTripManager = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getTripManager();
    private com.navdy.hud.app.maps.MapEvents.ManeuverDisplay maneuverDisplay;
    private java.lang.String metersLabel;
    private java.lang.String milesLabel;
    private int nearMargin;
    @butterknife.InjectView(2131624174)
    android.widget.TextView remainingDistanceText;
    @butterknife.InjectView(2131624175)
    android.widget.TextView remainingDistanceUnitText;
    @butterknife.InjectView(2131624180)
    android.widget.TextView tripDistanceView;
    private int tripGaugeIconMargin;
    private int tripGaugeLongMargin;
    private int tripGaugeShortMargin;
    @butterknife.InjectView(2131624181)
    android.widget.ImageView tripIconView;
    @butterknife.InjectView(2131624178)
    android.support.constraint.ConstraintLayout tripOpenMapView;
    @butterknife.InjectView(2131624179)
    android.widget.TextView tripTimeView;
    @butterknife.InjectView(2131624169)
    android.widget.TextView ttaText1;
    @butterknife.InjectView(2131624170)
    android.widget.TextView ttaText2;
    @butterknife.InjectView(2131624171)
    android.widget.TextView ttaText3;
    @butterknife.InjectView(2131624172)
    android.widget.TextView ttaText4;

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            com.navdy.hud.app.view.ETAGaugePresenter.this.mHandler.postDelayed(com.navdy.hud.app.view.ETAGaugePresenter.this.mTripInfoUpdateRunnable, com.navdy.hud.app.view.ETAGaugePresenter.TRIP_INFO_UPDATE_INTERVAL);
            com.navdy.hud.app.view.ETAGaugePresenter.this.reDraw();
        }
    }

    static {
        boolean z;
        if (!com.navdy.hud.app.view.ETAGaugePresenter.class.desiredAssertionStatus()) {
            z = true;
        } else {
            z = false;
        }
        $assertionsDisabled = z;
    }

    public ETAGaugePresenter(android.content.Context context) {
        android.content.res.Resources resources = context.getResources();
        this.labels = resources.getStringArray(com.navdy.hud.app.R.array.time_labels);
        this.labelShort = resources.getStringArray(com.navdy.hud.app.R.array.time_labels_short);
        this.metersLabel = context.getResources().getString(com.navdy.hud.app.R.string.unit_meters);
        this.kiloMetersLabel = context.getResources().getString(com.navdy.hud.app.R.string.unit_kilometers);
        this.feetLabel = context.getResources().getString(com.navdy.hud.app.R.string.unit_feet);
        this.milesLabel = context.getResources().getString(com.navdy.hud.app.R.string.unit_miles);
        this.farMargin = resources.getDimensionPixelSize(com.navdy.hud.app.R.dimen.far_margin);
        this.nearMargin = resources.getDimensionPixelSize(com.navdy.hud.app.R.dimen.near_margin);
        this.etaTripGaugeName = context.getResources().getString(com.navdy.hud.app.R.string.widget_trip_eta);
        this.tripGaugeShortMargin = resources.getDimensionPixelSize(com.navdy.hud.app.R.dimen.trip_gauge_short_margin);
        this.tripGaugeLongMargin = resources.getDimensionPixelSize(com.navdy.hud.app.R.dimen.trip_gauge_long_margin);
        this.tripGaugeIconMargin = resources.getDimensionPixelSize(com.navdy.hud.app.R.dimen.trip_gauge_icon_margin);
        this.greyColor = android.support.v4.content.ContextCompat.getColor(context, com.navdy.hud.app.R.color.grey_ababab);
        this.fontSize18 = resources.getDimensionPixelSize(com.navdy.hud.app.R.dimen.active_trip_18);
        this.fontSize30 = resources.getDimensionPixelSize(com.navdy.hud.app.R.dimen.size_30);
        this.mHandler = new android.os.Handler();
        this.mTripInfoUpdateRunnable = new com.navdy.hud.app.view.ETAGaugePresenter.Anon1();
        this.etaProgressDrawable = new com.navdy.hud.app.view.drawable.ETAProgressDrawable(context);
        this.etaProgressDrawable.setMinValue(0.0f);
        this.etaProgressDrawable.setMaxGaugeValue(100.0f);
    }

    private boolean parseSeconds(long timeInSeconds, int[] splitTime, byte[] startEnd) {
        splitTime[3] = (int) java.util.concurrent.TimeUnit.SECONDS.toDays(timeInSeconds);
        splitTime[2] = (int) (java.util.concurrent.TimeUnit.SECONDS.toHours(timeInSeconds) - ((long) (splitTime[3] * 24)));
        splitTime[1] = (int) (java.util.concurrent.TimeUnit.SECONDS.toMinutes(timeInSeconds) - (java.util.concurrent.TimeUnit.SECONDS.toHours(timeInSeconds) * 60));
        splitTime[0] = (int) (timeInSeconds - (java.util.concurrent.TimeUnit.SECONDS.toMinutes(timeInSeconds) * 60));
        startEnd[0] = 0;
        startEnd[1] = 0;
        for (byte i = 3; i > 0; i = (byte) (i - 1)) {
            if (splitTime[i] > 0) {
                startEnd[1] = i;
                if (startEnd[0] == 0) {
                    startEnd[0] = i;
                }
            } else if (startEnd[0] != 0) {
                break;
            }
        }
        if (!$assertionsDisabled && startEnd[0] != startEnd[1] && startEnd[0] != startEnd[1] + 1) {
            throw new java.lang.AssertionError();
        } else if (startEnd[0] > startEnd[1]) {
            return true;
        } else {
            return false;
        }
    }

    private void setTTAText(long timeInMilliSeconds) {
        int[] splitTime = new int[4];
        byte[] startEnd = new byte[2];
        boolean singleCharacter = parseSeconds(java.lang.Math.max(60, timeInMilliSeconds / 1000), splitTime, startEnd);
        byte start = startEnd[0];
        byte end = startEnd[1];
        if (singleCharacter) {
            this.ttaText1.setText(java.lang.Integer.toString(splitTime[start]));
            this.ttaText2.setText(java.lang.Character.toString(this.labels[start].charAt(0)));
            this.ttaText3.setVisibility(0);
            this.ttaText4.setVisibility(0);
            this.ttaText3.setText(java.lang.Integer.toString(splitTime[end]));
            this.ttaText4.setText(java.lang.Character.toString(this.labels[end].charAt(0)));
            return;
        }
        this.ttaText1.setText(java.lang.Integer.toString(splitTime[start]));
        this.ttaText2.setText(this.labels[start]);
        this.ttaText3.setVisibility(8);
        this.ttaText4.setVisibility(8);
    }

    private void setTripTimeText(long timeInMilliSeconds) {
        android.text.SpannableStringBuilder stringBuilder;
        long timeInSeconds = timeInMilliSeconds / 1000;
        if (timeInSeconds == 0) {
            this.tripDistanceView.setVisibility(8);
            this.tripTimeView.setText(getTime(java.lang.String.valueOf(0), this.labelShort[0]));
            return;
        }
        int[] splitTime = new int[4];
        byte[] startEnd = new byte[2];
        boolean singleCharacter = parseSeconds(java.lang.Math.max(60, timeInSeconds), splitTime, startEnd);
        byte start = startEnd[0];
        byte end = startEnd[1];
        if (singleCharacter) {
            stringBuilder = getTime(java.lang.Integer.toString(splitTime[start]), java.lang.String.valueOf(this.labelShort[start].charAt(0)), java.lang.Integer.toString(splitTime[end]), java.lang.String.valueOf(this.labelShort[end].charAt(0)));
        } else {
            stringBuilder = getTime(java.lang.Integer.toString(splitTime[start]), java.lang.String.valueOf(this.labels[start]));
        }
        this.tripTimeView.setText(stringBuilder);
    }

    public void setView(com.navdy.hud.app.view.DashboardWidgetView dashboardWidgetView, android.os.Bundle arguments) {
        if (dashboardWidgetView != null) {
            this.gravity = arguments != null ? arguments.getInt(com.navdy.hud.app.view.DashboardWidgetPresenter.EXTRA_GRAVITY, 0) : 0;
            dashboardWidgetView.setContentView((int) com.navdy.hud.app.R.layout.eta_gauge_layout);
            butterknife.ButterKnife.inject((java.lang.Object) this, (android.view.View) dashboardWidgetView);
            android.view.ViewGroup.MarginLayoutParams marginParams = (android.view.ViewGroup.MarginLayoutParams) this.etaView.getLayoutParams();
            android.support.constraint.ConstraintSet constraintSet = new android.support.constraint.ConstraintSet();
            constraintSet.clone(this.tripOpenMapView);
            int parentId = this.tripOpenMapView.getId();
            int iconViewId = this.tripIconView.getId();
            switch (this.gravity) {
                case 0:
                    marginParams.leftMargin = this.nearMargin;
                    marginParams.rightMargin = this.farMargin;
                    constraintSet.connect(iconViewId, 6, parentId, 6, 0);
                    constraintSet.connect(this.tripTimeView.getId(), 1, iconViewId, 2, this.tripGaugeIconMargin);
                    break;
                case 2:
                    marginParams.leftMargin = this.farMargin;
                    marginParams.rightMargin = this.nearMargin;
                    constraintSet.connect(iconViewId, 7, parentId, 7, 0);
                    constraintSet.connect(this.tripTimeView.getId(), 2, iconViewId, 1, this.tripGaugeIconMargin);
                    break;
            }
            constraintSet.applyTo(this.tripOpenMapView);
            this.etaView.setLayoutParams(marginParams);
            this.isNavigationActive = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getUiStateManager().isNavigationActive();
            com.navdy.hud.app.maps.here.HereMapsManager.getInstance().postManeuverDisplay();
            super.setView(dashboardWidgetView, arguments);
            reDraw();
            return;
        }
        super.setView(dashboardWidgetView, arguments);
        reDraw();
    }

    /* access modifiers changed from: protected */
    public boolean isRegisteringToBusRequired() {
        return true;
    }

    public android.graphics.drawable.Drawable getDrawable() {
        return this.etaProgressDrawable;
    }

    /* access modifiers changed from: protected */
    public void updateGauge() {
        if (!this.isNavigationActive || this.maneuverDisplay == null || this.maneuverDisplay.etaDate == null || this.maneuverDisplay.totalDistanceUnit == null || this.maneuverDisplay.totalDistanceRemainingUnit == null) {
            this.tripOpenMapView.setVisibility(0);
            long tripTime = this.mTripManager.getCurrentTripStartTime() > 0 ? java.lang.System.currentTimeMillis() - this.mTripManager.getCurrentTripStartTime() : 0;
            long distanceTravelledInMeters = (long) this.mTripManager.getCurrentDistanceTraveled();
            com.navdy.hud.app.manager.SpeedManager speedManager = com.navdy.hud.app.manager.SpeedManager.getInstance();
            com.navdy.hud.app.maps.util.DistanceConverter.Distance distance = new com.navdy.hud.app.maps.util.DistanceConverter.Distance();
            com.navdy.hud.app.maps.util.DistanceConverter.convertToDistance(speedManager.getSpeedUnit(), (float) distanceTravelledInMeters, distance);
            java.lang.String unitLabel = "";
            switch (distance.unit) {
                case DISTANCE_METERS:
                    unitLabel = this.metersLabel;
                    break;
                case DISTANCE_KMS:
                    unitLabel = this.kiloMetersLabel;
                    break;
                case DISTANCE_MILES:
                    unitLabel = this.milesLabel;
                    break;
                case DISTANCE_FEET:
                    unitLabel = this.feetLabel;
                    break;
            }
            this.tripDistanceView.setText(getDistance(java.lang.Float.toString(distance.value), unitLabel));
            this.tripDistanceView.setVisibility(0);
            setTripTimeText(tripTime);
            this.mHandler.removeCallbacks(this.mTripInfoUpdateRunnable);
            this.mHandler.postDelayed(this.mTripInfoUpdateRunnable, TRIP_INFO_UPDATE_INTERVAL);
            this.etaView.setVisibility(8);
            return;
        }
        this.mHandler.removeCallbacks(this.mTripInfoUpdateRunnable);
        this.tripOpenMapView.setVisibility(8);
        this.etaView.setVisibility(0);
        long currentTime = java.lang.System.currentTimeMillis();
        long etaTime = this.maneuverDisplay.etaDate.getTime();
        setTTAText(etaTime > currentTime ? etaTime - currentTime : 0);
        this.etaText.setText(this.maneuverDisplay.eta);
        this.etaAmPmText.setText(this.maneuverDisplay.etaAmPm);
        float totalDistanceInMeters = com.navdy.hud.app.maps.util.DistanceConverter.convertToMeters(this.maneuverDisplay.totalDistance, this.maneuverDisplay.totalDistanceUnit);
        float coveredDistanceInMeters = totalDistanceInMeters - com.navdy.hud.app.maps.util.DistanceConverter.convertToMeters(this.maneuverDisplay.totalDistanceRemaining, this.maneuverDisplay.totalDistanceRemainingUnit);
        this.etaProgressDrawable.setGaugeValue((float) ((totalDistanceInMeters <= 0.0f || totalDistanceInMeters <= coveredDistanceInMeters) ? 0 : (int) ((coveredDistanceInMeters / totalDistanceInMeters) * 100.0f)));
        this.remainingDistanceText.setText(java.lang.Float.toString(this.maneuverDisplay.totalDistanceRemaining));
        switch (this.maneuverDisplay.totalDistanceRemainingUnit) {
            case DISTANCE_METERS:
                this.remainingDistanceUnitText.setText(this.metersLabel);
                return;
            case DISTANCE_KMS:
                this.remainingDistanceUnitText.setText(this.kiloMetersLabel);
                return;
            case DISTANCE_MILES:
                this.remainingDistanceUnitText.setText(this.milesLabel);
                return;
            case DISTANCE_FEET:
                this.remainingDistanceUnitText.setText(this.feetLabel);
                return;
            default:
                return;
        }
    }

    public java.lang.String getWidgetIdentifier() {
        return com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.ETA_GAUGE_ID;
    }

    public java.lang.String getWidgetName() {
        return this.etaTripGaugeName;
    }

    @com.squareup.otto.Subscribe
    public void onNavigationModeChange(com.navdy.hud.app.maps.NavigationMode navigationMode) {
        this.isNavigationActive = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getUiStateManager().isNavigationActive();
        this.maneuverDisplay = null;
        reDraw();
    }

    @com.squareup.otto.Subscribe
    public void onManeuverDisplay(com.navdy.hud.app.maps.MapEvents.ManeuverDisplay maneuverDisplay2) {
        if (!maneuverDisplay2.isEmpty()) {
            com.navdy.hud.app.ui.framework.UIStateManager uiStateManager = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getUiStateManager();
            this.maneuverDisplay = maneuverDisplay2;
            if (this.maneuverDisplay != null) {
                this.isNavigationActive = this.maneuverDisplay.isNavigating();
            } else {
                this.isNavigationActive = uiStateManager.isNavigationActive();
            }
            reDraw();
        }
    }

    private android.text.SpannableStringBuilder getTime(java.lang.String... data) {
        android.text.SpannableStringBuilder stringBuilder = new android.text.SpannableStringBuilder();
        if (this.gravity == 0) {
            addPadding(stringBuilder);
        }
        int i = 0;
        while (i < data.length) {
            if (i != 0) {
                stringBuilder.append(" ");
            }
            java.lang.String val = data[i];
            java.lang.String unit = data[i + 1];
            i += 2;
            int start = stringBuilder.length();
            stringBuilder.append(val);
            int len = stringBuilder.length();
            stringBuilder.setSpan(new android.text.style.AbsoluteSizeSpan(this.fontSize30), start, len, 34);
            stringBuilder.setSpan(new android.text.style.ForegroundColorSpan(-1), start, len, 34);
            stringBuilder.append(" ");
            int start2 = stringBuilder.length();
            stringBuilder.append(unit);
            int len2 = stringBuilder.length();
            stringBuilder.setSpan(new android.text.style.AbsoluteSizeSpan(this.fontSize18), start2, len2, 34);
            stringBuilder.setSpan(new android.text.style.ForegroundColorSpan(this.greyColor), start2, len2, 34);
        }
        if (this.gravity == 2) {
            addPadding(stringBuilder);
        }
        return stringBuilder;
    }

    private android.text.SpannableStringBuilder getDistance(java.lang.String val, java.lang.String unit) {
        android.text.SpannableStringBuilder stringBuilder = new android.text.SpannableStringBuilder();
        if (this.gravity == 0) {
            addPadding(stringBuilder);
        }
        stringBuilder.append(val);
        int len = stringBuilder.length();
        stringBuilder.setSpan(new android.text.style.AbsoluteSizeSpan(this.fontSize18), 0, len, 34);
        stringBuilder.setSpan(new android.text.style.ForegroundColorSpan(-1), 0, len, 34);
        stringBuilder.append(" ");
        int start = stringBuilder.length();
        stringBuilder.append(unit);
        int len2 = stringBuilder.length();
        stringBuilder.setSpan(new android.text.style.AbsoluteSizeSpan(this.fontSize18), start, len2, 34);
        stringBuilder.setSpan(new android.text.style.ForegroundColorSpan(this.greyColor), start, len2, 34);
        if (this.gravity == 2) {
            addPadding(stringBuilder);
        }
        return stringBuilder;
    }

    private void addPadding(android.text.SpannableStringBuilder stringBuilder) {
        stringBuilder.append(" ");
        stringBuilder.append(" ");
        stringBuilder.append(" ");
    }
}
