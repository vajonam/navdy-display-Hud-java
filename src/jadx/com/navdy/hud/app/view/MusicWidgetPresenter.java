package com.navdy.hud.app.view;

public class MusicWidgetPresenter extends com.navdy.hud.app.view.DashboardWidgetPresenter implements com.navdy.hud.app.manager.MusicManager.MusicUpdateListener {
    private static final float ARTWORK_ALPHA_NOT_PLAYING = 0.5f;
    private static final float ARTWORK_ALPHA_PLAYING = 1.0f;
    /* access modifiers changed from: private */
    public static com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.view.MusicWidgetPresenter.class);
    /* access modifiers changed from: private */
    public android.graphics.Bitmap albumArt;
    private boolean animateNextArtworkTransition;
    private com.navdy.hud.app.framework.music.OutlineTextView artistText;
    private android.os.Handler handler = new android.os.Handler();
    private com.navdy.hud.app.framework.music.AlbumArtImageView image;
    private java.lang.CharSequence lastAlbumArtHash;
    private java.lang.String musicGaugeName;
    @javax.inject.Inject
    com.navdy.hud.app.manager.MusicManager musicManager;
    private com.navdy.hud.app.framework.music.OutlineTextView stateText;
    private com.navdy.hud.app.framework.music.OutlineTextView titleText;
    private com.navdy.service.library.events.audio.MusicTrackInfo trackInfo = null;

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            com.navdy.hud.app.view.MusicWidgetPresenter.this.reDraw();
        }
    }

    class Anon2 implements java.lang.Runnable {
        final /* synthetic */ okio.ByteString val$photo;

        Anon2(okio.ByteString byteString) {
            this.val$photo = byteString;
        }

        public void run() {
            com.navdy.hud.app.view.MusicWidgetPresenter.logger.d("PhotoUpdate runnable " + this.val$photo);
            byte[] byteArray = this.val$photo.toByteArray();
            if (byteArray == null || byteArray.length <= 0) {
                com.navdy.hud.app.view.MusicWidgetPresenter.logger.i("Received photo has null or empty byte array");
                com.navdy.hud.app.view.MusicWidgetPresenter.this.resetArtwork();
                com.navdy.hud.app.view.MusicWidgetPresenter.this.redrawOnMainThread();
                return;
            }
            int size = com.navdy.hud.app.HudApplication.getAppContext().getResources().getDimensionPixelSize(com.navdy.hud.app.R.dimen.music_gauge_album_art_size);
            android.graphics.Bitmap artwork = null;
            try {
                artwork = com.navdy.service.library.util.ScalingUtilities.decodeByteArray(byteArray, size, size, com.navdy.service.library.util.ScalingUtilities.ScalingLogic.FIT);
                com.navdy.hud.app.view.MusicWidgetPresenter.this.albumArt = com.navdy.service.library.util.ScalingUtilities.createScaledBitmap(artwork, size, size, com.navdy.service.library.util.ScalingUtilities.ScalingLogic.FIT);
                com.navdy.hud.app.view.MusicWidgetPresenter.this.redrawOnMainThread();
                if (artwork != null && artwork != com.navdy.hud.app.view.MusicWidgetPresenter.this.albumArt) {
                    artwork.recycle();
                }
            } catch (java.lang.Exception e) {
                com.navdy.hud.app.view.MusicWidgetPresenter.logger.e("Error updating the art work received ", e);
                com.navdy.hud.app.view.MusicWidgetPresenter.this.resetArtwork();
                com.navdy.hud.app.view.MusicWidgetPresenter.this.redrawOnMainThread();
                if (artwork != null && artwork != com.navdy.hud.app.view.MusicWidgetPresenter.this.albumArt) {
                    artwork.recycle();
                }
            } catch (Throwable th) {
                if (!(artwork == null || artwork == com.navdy.hud.app.view.MusicWidgetPresenter.this.albumArt)) {
                    artwork.recycle();
                }
                throw th;
            }
        }
    }

    class Anon3 implements java.lang.Runnable {
        Anon3() {
        }

        public void run() {
            com.navdy.hud.app.view.MusicWidgetPresenter.this.reDraw();
        }
    }

    public MusicWidgetPresenter(android.content.Context context) {
        this.musicGaugeName = context.getResources().getString(com.navdy.hud.app.R.string.widget_music);
        mortar.Mortar.inject(com.navdy.hud.app.HudApplication.getAppContext(), this);
    }

    public void setView(com.navdy.hud.app.view.DashboardWidgetView dashboardWidgetView, android.os.Bundle arguments) {
        logger.d("setView " + dashboardWidgetView);
        if (dashboardWidgetView != null) {
            dashboardWidgetView.setContentView((int) com.navdy.hud.app.R.layout.music_widget);
            this.titleText = (com.navdy.hud.app.framework.music.OutlineTextView) dashboardWidgetView.findViewById(com.navdy.hud.app.R.id.txt_name);
            this.artistText = (com.navdy.hud.app.framework.music.OutlineTextView) dashboardWidgetView.findViewById(com.navdy.hud.app.R.id.txt_author);
            this.stateText = (com.navdy.hud.app.framework.music.OutlineTextView) dashboardWidgetView.findViewById(com.navdy.hud.app.R.id.txt_player_state);
            this.image = (com.navdy.hud.app.framework.music.AlbumArtImageView) dashboardWidgetView.findViewById(com.navdy.hud.app.R.id.img_album_art);
        }
        super.setView(dashboardWidgetView);
    }

    public android.graphics.drawable.Drawable getDrawable() {
        return null;
    }

    /* access modifiers changed from: protected */
    public void updateGauge() {
        this.image.setArtworkBitmap(this.albumArt, this.animateNextArtworkTransition);
        if (this.trackInfo == null || this.trackInfo == com.navdy.hud.app.manager.MusicManager.EMPTY_TRACK) {
            this.titleText.setText("");
            this.artistText.setText("");
            this.stateText.setVisibility(4);
            return;
        }
        if (com.navdy.hud.app.manager.MusicManager.tryingToPlay(this.trackInfo.playbackState)) {
            this.image.setAlpha(1.0f);
            this.stateText.setVisibility(4);
        } else {
            this.image.setAlpha(0.5f);
            this.stateText.setVisibility(0);
        }
        updateMetadata();
    }

    private void updateMetadata() {
        java.lang.CharSequence currentTitle = this.titleText.getText();
        if (this.trackInfo.name == null || !this.trackInfo.name.equals(currentTitle)) {
            this.titleText.setText(this.trackInfo.name);
        }
        java.lang.CharSequence currentArtist = this.artistText.getText();
        if (this.trackInfo.author == null || !this.trackInfo.author.equals(currentArtist)) {
            this.artistText.setText(this.trackInfo.author);
        }
    }

    public java.lang.String getWidgetIdentifier() {
        logger.d("getWidgetIdentifier");
        return com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.MUSIC_WIDGET_ID;
    }

    public java.lang.String getWidgetName() {
        return this.musicGaugeName;
    }

    public void onTrackUpdated(com.navdy.service.library.events.audio.MusicTrackInfo trackInfo2, java.util.Set<com.navdy.hud.app.manager.MusicManager.MediaControl> set, boolean willOpenNotification) {
        this.trackInfo = trackInfo2;
        if (willOpenNotification) {
            logger.d("onTrackUpdated, delayed!");
            this.handler.postDelayed(new com.navdy.hud.app.view.MusicWidgetPresenter.Anon1(), 250);
            return;
        }
        logger.d("onTrackUpdated (immediate)");
        reDraw();
    }

    public void onAlbumArtUpdate(@android.support.annotation.Nullable okio.ByteString photo, boolean animate) {
        logger.d("onAlbumArtUpdate " + photo + ", " + animate);
        this.animateNextArtworkTransition = animate;
        if (photo == null) {
            logger.v("ByteString image to set in notification is null");
            resetArtwork();
            reDraw();
            return;
        }
        java.lang.String hash = photo.md5().hex();
        if (!android.text.TextUtils.equals(this.lastAlbumArtHash, hash) || this.albumArt == null) {
            this.lastAlbumArtHash = hash;
            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.view.MusicWidgetPresenter.Anon2(photo), 1);
            return;
        }
        logger.d("Already have this artwork, ignoring");
    }

    /* access modifiers changed from: private */
    public void resetArtwork() {
        this.albumArt = null;
        this.lastAlbumArtHash = null;
    }

    /* access modifiers changed from: private */
    public void redrawOnMainThread() {
        this.handler.post(new com.navdy.hud.app.view.MusicWidgetPresenter.Anon3());
    }

    public void setWidgetVisibleToUser(boolean b) {
        logger.d("setWidgetVisibleToUser " + b);
        if (b) {
            logger.v("setWidgetVisibleToUser: add music update listener");
            this.musicManager.addMusicUpdateListener(this);
            this.trackInfo = this.musicManager.getCurrentTrack();
        } else {
            logger.v("setWidgetVisibleToUser: remove music update listener");
            this.musicManager.removeMusicUpdateListener(this);
        }
        super.setWidgetVisibleToUser(b);
    }
}
