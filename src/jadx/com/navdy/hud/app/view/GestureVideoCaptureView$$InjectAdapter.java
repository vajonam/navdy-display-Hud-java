package com.navdy.hud.app.view;

public final class GestureVideoCaptureView$$InjectAdapter extends dagger.internal.Binding<com.navdy.hud.app.view.GestureVideoCaptureView> implements dagger.MembersInjector<com.navdy.hud.app.view.GestureVideoCaptureView> {
    private dagger.internal.Binding<com.squareup.otto.Bus> bus;
    private dagger.internal.Binding<com.navdy.hud.app.gesture.GestureServiceConnector> gestureService;
    private dagger.internal.Binding<com.navdy.hud.app.screen.GestureLearningScreen.Presenter> mPresenter;

    public GestureVideoCaptureView$$InjectAdapter() {
        super(null, "members/com.navdy.hud.app.view.GestureVideoCaptureView", false, com.navdy.hud.app.view.GestureVideoCaptureView.class);
    }

    public void attach(dagger.internal.Linker linker) {
        this.mPresenter = linker.requestBinding("com.navdy.hud.app.screen.GestureLearningScreen$Presenter", com.navdy.hud.app.view.GestureVideoCaptureView.class, getClass().getClassLoader());
        this.gestureService = linker.requestBinding("com.navdy.hud.app.gesture.GestureServiceConnector", com.navdy.hud.app.view.GestureVideoCaptureView.class, getClass().getClassLoader());
        this.bus = linker.requestBinding("com.squareup.otto.Bus", com.navdy.hud.app.view.GestureVideoCaptureView.class, getClass().getClassLoader());
    }

    public void getDependencies(java.util.Set<dagger.internal.Binding<?>> set, java.util.Set<dagger.internal.Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.mPresenter);
        injectMembersBindings.add(this.gestureService);
        injectMembersBindings.add(this.bus);
    }

    public void injectMembers(com.navdy.hud.app.view.GestureVideoCaptureView object) {
        object.mPresenter = (com.navdy.hud.app.screen.GestureLearningScreen.Presenter) this.mPresenter.get();
        object.gestureService = (com.navdy.hud.app.gesture.GestureServiceConnector) this.gestureService.get();
        object.bus = (com.squareup.otto.Bus) this.bus.get();
    }
}
