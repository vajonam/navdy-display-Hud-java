package com.navdy.hud.app.view;

public class CompassDrawable extends com.navdy.hud.app.view.drawable.GaugeDrawable {
    private static final double SEGMENT_ANGLE = 22.5d;
    private static final int TEXT_PADDING = 6;
    private static final int TOTAL_SEGMENTS = 16;
    private static java.lang.String[] headings;
    private static int majorTickHeight = 10;
    private static int minorTickHeight = 5;
    private static int[] widths;
    private android.graphics.drawable.Drawable mIndicatorDrawable;
    private int mIndicatorHeight;
    private int mIndicatorTopMargin;
    private int mIndicatorWidth;
    private android.graphics.Path mPath;
    private int mStripHeight;
    private int mStripTopMargin;
    private int mStripWidth;
    private android.graphics.Paint mTextPaint = new android.graphics.Paint();

    public CompassDrawable(android.content.Context context) {
        super(context, 0);
        android.content.res.Resources resources = context.getResources();
        this.mIndicatorDrawable = resources.getDrawable(com.navdy.hud.app.R.drawable.icon_gauge_compass_heading_indicator);
        this.mIndicatorWidth = resources.getDimensionPixelSize(com.navdy.hud.app.R.dimen.compass_indicator_width);
        this.mIndicatorHeight = resources.getDimensionPixelSize(com.navdy.hud.app.R.dimen.compass_indicator_height);
        this.mIndicatorTopMargin = resources.getDimensionPixelSize(com.navdy.hud.app.R.dimen.compass_indicator_top_margin);
        this.mStripWidth = resources.getDimensionPixelSize(com.navdy.hud.app.R.dimen.compass_strip_width);
        this.mStripTopMargin = resources.getDimensionPixelSize(com.navdy.hud.app.R.dimen.compass_strip_margin_top);
        this.mStripHeight = resources.getDimensionPixelSize(com.navdy.hud.app.R.dimen.compass_strip_height);
        this.mPaint.setAntiAlias(true);
        this.mPaint.setStyle(android.graphics.Paint.Style.FILL);
        this.mPaint.setColor(resources.getColor(com.navdy.hud.app.R.color.compass_frame_color));
        this.mTextPaint.setStrokeWidth(0.0f);
        this.mTextPaint.setColor(-1);
        this.mTextPaint.setAntiAlias(false);
        this.mTextPaint.setTextAlign(android.graphics.Paint.Align.LEFT);
        this.mTextPaint.setTypeface(android.graphics.Typeface.create("sans-serif", 1));
        this.mTextPaint.setTextSize((float) resources.getDimensionPixelSize(com.navdy.hud.app.R.dimen.compass_text_height));
        this.mPath = new android.graphics.Path();
        android.graphics.Rect measuringRect = new android.graphics.Rect();
        if (headings == null) {
            majorTickHeight = resources.getDimensionPixelSize(com.navdy.hud.app.R.dimen.compass_major_tick_height);
            minorTickHeight = resources.getDimensionPixelSize(com.navdy.hud.app.R.dimen.compass_minor_tick_height);
            headings = resources.getStringArray(com.navdy.hud.app.R.array.compass_points);
            widths = new int[headings.length];
            for (int i = 0; i < widths.length; i++) {
                this.mTextPaint.getTextBounds(headings[i], 0, headings[i].length(), measuringRect);
                widths[i] = measuringRect.width();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onBoundsChange(android.graphics.Rect bounds) {
        super.onBoundsChange(bounds);
        this.mPath.reset();
        this.mPath.addOval(0.0f, 0.0f, (float) bounds.width(), (float) bounds.height(), android.graphics.Path.Direction.CW);
    }

    private void drawCompassPoints(android.graphics.Canvas canvas, float heading, int left, int top, int right, int bottom, boolean showHeadings) {
        int width = right - left;
        float center = (float) ((width / 2) + left);
        int segmentVisualSize = this.mStripWidth / 16;
        int segmentsToDraw = ((int) java.lang.Math.ceil((double) (width / segmentVisualSize))) + 1;
        float segment = (heading % 360.0f) / 22.5f;
        int tick = (int) segment;
        float tickOffset = segment - ((float) tick);
        int rightTick = segmentsToDraw / 2;
        for (int i = (-segmentsToDraw) / 2; i < rightTick; i++) {
            int x = (int) (((double) (((((float) i) + tickOffset) * ((float) segmentVisualSize)) + center)) + 0.5d);
            int tickIndex = ((tick - i) + 16) % 16;
            boolean majorTick = (tickIndex & 1) == 0;
            int height = majorTick ? majorTickHeight : minorTickHeight;
            this.mTextPaint.setAntiAlias(false);
            canvas.drawLine((float) x, (float) (bottom - height), (float) x, (float) bottom, this.mTextPaint);
            if (majorTick && showHeadings) {
                int headingIndex = tickIndex / 2;
                java.lang.String text = headings[headingIndex];
                this.mTextPaint.setAntiAlias(true);
                canvas.drawText(text, (float) (x - (widths[headingIndex] / 2)), (float) ((bottom - height) - 6), this.mTextPaint);
            }
        }
    }

    public void draw(android.graphics.Canvas canvas) {
        super.draw(canvas);
        android.graphics.Rect bounds = getBounds();
        int width = bounds.width();
        int height = bounds.height();
        canvas.clipRect(bounds);
        canvas.drawArc(0.0f, 0.0f, (float) width, (float) height, 0.0f, 360.0f, true, this.mPaint);
        int top = bounds.top + this.mIndicatorTopMargin;
        this.mIndicatorDrawable.setBounds((bounds.left + (width / 2)) - (this.mIndicatorWidth / 2), top, bounds.left + (width / 2) + (this.mIndicatorWidth / 2), top + this.mIndicatorHeight);
        this.mIndicatorDrawable.draw(canvas);
        canvas.clipPath(this.mPath);
        drawCompassPoints(canvas, this.mValue, 0, this.mStripTopMargin, width, this.mStripTopMargin + this.mStripHeight, this.mValue != 0.0f);
    }
}
