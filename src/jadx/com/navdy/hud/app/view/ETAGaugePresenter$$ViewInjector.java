package com.navdy.hud.app.view;

public class ETAGaugePresenter$$ViewInjector {
    public static void inject(butterknife.ButterKnife.Finder finder, com.navdy.hud.app.view.ETAGaugePresenter target, java.lang.Object source) {
        target.ttaText1 = (android.widget.TextView) finder.findRequiredView(source, com.navdy.hud.app.R.id.txt_tta1, "field 'ttaText1'");
        target.ttaText2 = (android.widget.TextView) finder.findRequiredView(source, com.navdy.hud.app.R.id.txt_tta2, "field 'ttaText2'");
        target.ttaText3 = (android.widget.TextView) finder.findRequiredView(source, com.navdy.hud.app.R.id.txt_tta3, "field 'ttaText3'");
        target.ttaText4 = (android.widget.TextView) finder.findRequiredView(source, com.navdy.hud.app.R.id.txt_tta4, "field 'ttaText4'");
        target.remainingDistanceText = (android.widget.TextView) finder.findRequiredView(source, com.navdy.hud.app.R.id.txt_remaining_distance, "field 'remainingDistanceText'");
        target.remainingDistanceUnitText = (android.widget.TextView) finder.findRequiredView(source, com.navdy.hud.app.R.id.txt_remaining_distance_unit, "field 'remainingDistanceUnitText'");
        target.etaText = (android.widget.TextView) finder.findRequiredView(source, com.navdy.hud.app.R.id.txt_eta, "field 'etaText'");
        target.etaAmPmText = (android.widget.TextView) finder.findRequiredView(source, com.navdy.hud.app.R.id.txt_eta_am_pm, "field 'etaAmPmText'");
        target.etaView = (android.view.ViewGroup) finder.findRequiredView(source, com.navdy.hud.app.R.id.eta_view, "field 'etaView'");
        target.tripOpenMapView = (android.support.constraint.ConstraintLayout) finder.findRequiredView(source, com.navdy.hud.app.R.id.tripOpenMap, "field 'tripOpenMapView'");
        target.tripDistanceView = (android.widget.TextView) finder.findRequiredView(source, com.navdy.hud.app.R.id.tripDistance, "field 'tripDistanceView'");
        target.tripTimeView = (android.widget.TextView) finder.findRequiredView(source, com.navdy.hud.app.R.id.tripTime, "field 'tripTimeView'");
        target.tripIconView = (android.widget.ImageView) finder.findRequiredView(source, com.navdy.hud.app.R.id.tripIcon, "field 'tripIconView'");
    }

    public static void reset(com.navdy.hud.app.view.ETAGaugePresenter target) {
        target.ttaText1 = null;
        target.ttaText2 = null;
        target.ttaText3 = null;
        target.ttaText4 = null;
        target.remainingDistanceText = null;
        target.remainingDistanceUnitText = null;
        target.etaText = null;
        target.etaAmPmText = null;
        target.etaView = null;
        target.tripOpenMapView = null;
        target.tripDistanceView = null;
        target.tripTimeView = null;
        target.tripIconView = null;
    }
}
