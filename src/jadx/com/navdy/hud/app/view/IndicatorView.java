package com.navdy.hud.app.view;

public class IndicatorView extends android.view.View {
    private android.graphics.Paint indicatorPaint;
    private android.graphics.Path indicatorPath;
    private float mCurveRadius;
    private int mIndicatorHeight;
    private com.navdy.hud.app.util.CustomDimension mIndicatorHeightAttribute;
    private float mIndicatorStrokeWidth;
    private int mIndicatorWidth;
    private com.navdy.hud.app.util.CustomDimension mIndicatorWidthAttribute;
    private float mValue;

    public IndicatorView(android.content.Context context) {
        this(context, null);
    }

    public IndicatorView(android.content.Context context, android.util.AttributeSet attrs) {
        super(context, attrs);
        initFromAttributes(context, attrs);
    }

    public static void drawIndicatorPath(android.graphics.Path path, float left, float bottom, float curveRadius, float indicatorWidth, float indicatorHeight, float width) {
        drawIndicatorPath(path, 0.5f, left, bottom, curveRadius, indicatorWidth, indicatorHeight, width);
    }

    public static void drawIndicatorPath(android.graphics.Path path, float value, float left, float bottom, float curveRadius, float indicatorWidth, float indicatorHeight, float width) {
        path.moveTo(left, bottom);
        float right = left + width;
        double angle = java.lang.Math.atan((double) (indicatorHeight / (indicatorWidth / 2.0f)));
        float rise = (float) (java.lang.Math.sin(angle) * ((double) curveRadius));
        float run = (float) (java.lang.Math.cos(angle) * ((double) curveRadius));
        float indicatorRadius = indicatorWidth / 2.0f;
        float middle = left + indicatorRadius + ((width - indicatorWidth) * value);
        path.lineTo((middle - indicatorRadius) - curveRadius, bottom);
        path.quadTo(middle - indicatorRadius, bottom, (middle - indicatorRadius) + run, bottom - rise);
        path.lineTo(middle, bottom - indicatorHeight);
        path.lineTo((middle + indicatorRadius) - run, bottom - rise);
        path.quadTo(middle + indicatorRadius, bottom, middle + indicatorRadius + curveRadius, bottom);
        path.lineTo(right, bottom);
    }

    private void initFromAttributes(android.content.Context context, android.util.AttributeSet attrs) {
        android.content.res.TypedArray a = context.getTheme().obtainStyledAttributes(attrs, com.navdy.hud.app.R.styleable.Indicator, 0, 0);
        try {
            this.mIndicatorWidthAttribute = com.navdy.hud.app.util.CustomDimension.getDimension(this, a, 0, 0.0f);
            this.mIndicatorHeightAttribute = com.navdy.hud.app.util.CustomDimension.getDimension(this, a, 1, 0.0f);
            this.mCurveRadius = a.getDimension(2, 10.0f);
            this.mIndicatorStrokeWidth = a.getDimension(3, 4.0f);
            this.mValue = a.getFloat(4, 0.5f);
        } finally {
            a.recycle();
        }
    }

    public void evaluateDimensions(int w, int h) {
        this.mIndicatorHeight = (int) this.mIndicatorHeightAttribute.getSize(this, (float) w, 0.0f);
        this.mIndicatorWidth = (int) this.mIndicatorWidthAttribute.getSize(this, (float) w, 0.0f);
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        evaluateDimensions(w, h);
        initDrawingTools();
    }

    private void initDrawingTools() {
        this.indicatorPath = new android.graphics.Path();
        this.indicatorPaint = new android.graphics.Paint();
        this.indicatorPaint.setColor(-1);
        this.indicatorPaint.setAntiAlias(true);
        this.indicatorPaint.setStyle(android.graphics.Paint.Style.STROKE);
        this.indicatorPaint.setStrokeWidth(this.mIndicatorStrokeWidth);
        int[] colors = {0, -1, -1, 0};
        float[] positions = {0.0f, 0.45f, 0.55f, 1.0f};
        int left = getPaddingLeft();
        this.indicatorPaint.setShader(new android.graphics.LinearGradient((float) left, 0.0f, (float) (getWidth() - getPaddingRight()), 0.0f, colors, positions, android.graphics.Shader.TileMode.CLAMP));
        drawIndicatorPath(this.indicatorPath, this.mValue, (float) left, ((float) getHeight()) - this.mIndicatorStrokeWidth, this.mCurveRadius, (float) this.mIndicatorWidth, (float) this.mIndicatorHeight, (float) ((getWidth() - getPaddingRight()) - getPaddingLeft()));
    }

    public int getIndicatorHeight() {
        return this.mIndicatorHeight;
    }

    public int getIndicatorWidth() {
        return this.mIndicatorWidth;
    }

    public com.navdy.hud.app.util.CustomDimension getIndicatorWidthAttribute() {
        return this.mIndicatorWidthAttribute;
    }

    public float getCurveRadius() {
        return this.mCurveRadius;
    }

    public float getStrokeWidth() {
        return this.mIndicatorStrokeWidth;
    }

    public void setValue(float value) {
        if (value != this.mValue) {
            this.mValue = value;
            initDrawingTools();
            invalidate();
        }
    }

    public float getValue() {
        return this.mValue;
    }

    /* access modifiers changed from: protected */
    public void onDraw(android.graphics.Canvas canvas) {
        canvas.drawPath(this.indicatorPath, this.indicatorPaint);
    }
}
