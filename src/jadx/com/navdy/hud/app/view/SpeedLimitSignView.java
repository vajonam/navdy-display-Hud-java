package com.navdy.hud.app.view;

public class SpeedLimitSignView extends android.widget.FrameLayout {
    @butterknife.InjectView(2131624420)
    protected android.view.ViewGroup euSpeedLimitSignView;
    private int speedLimit;
    private android.widget.TextView speedLimitTextView;
    private com.navdy.hud.app.manager.SpeedManager.SpeedUnit speedLimitUnit;
    @butterknife.InjectView(2131624418)
    protected android.view.ViewGroup usSpeedLimitSignView;

    public SpeedLimitSignView(android.content.Context context) {
        this(context, null);
    }

    public SpeedLimitSignView(android.content.Context context, android.util.AttributeSet attrs) {
        this(context, attrs, -1);
    }

    public SpeedLimitSignView(android.content.Context context, android.util.AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, -1);
    }

    public SpeedLimitSignView(android.content.Context context, android.util.AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.speedLimitUnit = null;
        inflate(getContext(), com.navdy.hud.app.R.layout.speed_sign_eu, this);
        inflate(getContext(), com.navdy.hud.app.R.layout.speed_limit_sign_us, this);
        butterknife.ButterKnife.inject((java.lang.Object) this, (android.view.View) this);
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
        setSpeedLimitUnit(this.speedLimitUnit);
    }

    public void setSpeedLimitUnit(com.navdy.hud.app.manager.SpeedManager.SpeedUnit speedLimitUnit2) {
        if (this.speedLimitUnit != speedLimitUnit2) {
            this.speedLimitUnit = speedLimitUnit2;
            android.view.ViewGroup speedLimitView = null;
            switch (this.speedLimitUnit) {
                case MILES_PER_HOUR:
                    speedLimitView = this.usSpeedLimitSignView;
                    this.euSpeedLimitSignView.setVisibility(8);
                    break;
                case KILOMETERS_PER_HOUR:
                    speedLimitView = this.euSpeedLimitSignView;
                    this.usSpeedLimitSignView.setVisibility(8);
                    break;
            }
            speedLimitView.setVisibility(0);
            this.speedLimitTextView = (android.widget.TextView) speedLimitView.findViewById(com.navdy.hud.app.R.id.txt_speed_limit);
            setSpeedLimit(this.speedLimit);
        }
    }

    public void setSpeedLimit(int speedLimit2) {
        if (this.speedLimit != speedLimit2) {
            this.speedLimit = speedLimit2;
            if (this.speedLimitTextView != null) {
                this.speedLimitTextView.setText(java.lang.Integer.toString(speedLimit2));
            }
        }
    }

    public com.navdy.hud.app.manager.SpeedManager.SpeedUnit getSpeedLimitUnit() {
        return this.speedLimitUnit;
    }
}
