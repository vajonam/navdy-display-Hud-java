package com.navdy.hud.app.view;

public class AutoBrightnessView extends android.widget.FrameLayout implements com.navdy.hud.app.manager.InputManager.IInputHandler {
    @butterknife.InjectView(2131624150)
    com.navdy.hud.app.ui.component.ChoiceLayout autoBrightnessChoices;
    @butterknife.InjectView(2131624147)
    android.widget.ImageView autoBrightnessIcon;
    @butterknife.InjectView(2131624144)
    android.widget.TextView autoBrightnessTitle;
    @butterknife.InjectView(2131624077)
    android.widget.TextView brightnessTitle;
    @butterknife.InjectView(2131624078)
    android.widget.TextView brightnessTitleDesc;
    @butterknife.InjectView(2131624131)
    android.widget.LinearLayout infoContainer;
    @javax.inject.Inject
    com.navdy.hud.app.screen.AutoBrightnessScreen.Presenter presenter;
    java.lang.String text;
    @butterknife.InjectView(2131624076)
    android.widget.TextView title1;

    public AutoBrightnessView(android.content.Context context) {
        this(context, null);
    }

    public AutoBrightnessView(android.content.Context context, android.util.AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AutoBrightnessView(android.content.Context context, android.util.AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if (!isInEditMode()) {
            mortar.Mortar.inject(context, this);
        }
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
        butterknife.ButterKnife.inject((android.view.View) this);
        android.content.res.Resources resources = getResources();
        int width = (int) resources.getDimension(com.navdy.hud.app.R.dimen.auto_brightness_section_width);
        ((android.view.ViewGroup.MarginLayoutParams) this.infoContainer.getLayoutParams()).width = width;
        this.infoContainer.setMinimumWidth(width);
        ((android.view.ViewGroup.MarginLayoutParams) this.autoBrightnessChoices.getLayoutParams()).bottomMargin = (int) resources.getDimension(com.navdy.hud.app.R.dimen.auto_brightness_choice_margin_bottom);
        this.autoBrightnessTitle.setTypeface(android.graphics.Typeface.create("sans-serif-medium", 0));
        this.title1.setVisibility(8);
        this.autoBrightnessTitle.setTypeface(android.graphics.Typeface.create("sans-serif-light", 0));
        this.autoBrightnessTitle.setTypeface(android.graphics.Typeface.create("sans-serif", 0));
        this.brightnessTitleDesc.setSingleLine(false);
        this.brightnessTitleDesc.setMaxLines(3);
        this.brightnessTitleDesc.setText(resources.getString(com.navdy.hud.app.R.string.auto_brightness_description_text));
        this.text = resources.getString(com.navdy.hud.app.R.string.auto_brightness_status_label_text);
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.presenter != null) {
            this.presenter.takeView(this);
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.presenter != null) {
            this.presenter.dropView(this);
        }
    }

    public boolean onGesture(com.navdy.service.library.events.input.GestureEvent event) {
        return false;
    }

    public boolean onKey(com.navdy.hud.app.manager.InputManager.CustomKeyEvent event) {
        return this.presenter.handleKey(event);
    }

    public com.navdy.hud.app.manager.InputManager.IInputHandler nextHandler() {
        return com.navdy.hud.app.manager.InputManager.nextContainingHandler(this);
    }

    public void setTitle(java.lang.String title) {
        this.autoBrightnessTitle.setText(title);
    }

    public void setStatusLabel(java.lang.String statusLabel) {
        android.text.SpannableStringBuilder builder = new android.text.SpannableStringBuilder();
        builder.append(this.text);
        builder.setSpan(new android.text.style.TypefaceSpan("sans-serif-light"), 0, builder.length(), 33);
        int start = builder.length();
        builder.append(statusLabel);
        builder.setSpan(new android.text.style.StyleSpan(1), start, builder.length(), 33);
        this.brightnessTitle.setText(builder);
    }

    public void setIcon(android.graphics.drawable.Drawable drawable) {
        this.autoBrightnessIcon.setImageDrawable(drawable);
    }

    public void setChoices(java.util.List<com.navdy.hud.app.ui.component.ChoiceLayout.Choice> choicesList, com.navdy.hud.app.ui.component.ChoiceLayout.IListener choicesListener) {
        this.autoBrightnessChoices.setChoices(com.navdy.hud.app.ui.component.ChoiceLayout.Mode.LABEL, choicesList, 0, choicesListener);
    }

    public void moveSelectionLeft() {
        this.autoBrightnessChoices.moveSelectionLeft();
    }

    public void moveSelectionRight() {
        this.autoBrightnessChoices.moveSelectionRight();
    }

    public void executeSelectedItem() {
        this.autoBrightnessChoices.executeSelectedItem(true);
    }
}
