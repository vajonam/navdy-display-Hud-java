package com.navdy.hud.app.view;

public class FactoryResetView$$ViewInjector {
    public static void inject(butterknife.ButterKnife.Finder finder, com.navdy.hud.app.view.FactoryResetView target, java.lang.Object source) {
        target.factoryResetConfirmation = (com.navdy.hud.app.ui.component.ConfirmationLayout) finder.findRequiredView(source, com.navdy.hud.app.R.id.factory_reset_confirmation, "field 'factoryResetConfirmation'");
    }

    public static void reset(com.navdy.hud.app.view.FactoryResetView target) {
        target.factoryResetConfirmation = null;
    }
}
