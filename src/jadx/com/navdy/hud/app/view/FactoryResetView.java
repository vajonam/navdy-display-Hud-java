package com.navdy.hud.app.view;

public class FactoryResetView extends android.widget.RelativeLayout implements com.navdy.hud.app.manager.InputManager.IInputHandler {
    @butterknife.InjectView(2131624322)
    com.navdy.hud.app.ui.component.ConfirmationLayout factoryResetConfirmation;
    @javax.inject.Inject
    com.navdy.hud.app.screen.FactoryResetScreen.Presenter presenter;

    public FactoryResetView(android.content.Context context) {
        this(context, null);
    }

    public FactoryResetView(android.content.Context context, android.util.AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public FactoryResetView(android.content.Context context, android.util.AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if (!isInEditMode()) {
            mortar.Mortar.inject(context, this);
        }
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
        butterknife.ButterKnife.inject((android.view.View) this);
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.presenter != null) {
            this.presenter.takeView(this);
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.presenter != null) {
            this.presenter.dropView(this);
        }
    }

    public boolean onGesture(com.navdy.service.library.events.input.GestureEvent event) {
        return false;
    }

    public boolean onKey(com.navdy.hud.app.manager.InputManager.CustomKeyEvent event) {
        return this.presenter.handleKey(event);
    }

    public com.navdy.hud.app.manager.InputManager.IInputHandler nextHandler() {
        return com.navdy.hud.app.manager.InputManager.nextContainingHandler(this);
    }

    public com.navdy.hud.app.ui.component.ConfirmationLayout getConfirmation() {
        return this.factoryResetConfirmation;
    }
}
