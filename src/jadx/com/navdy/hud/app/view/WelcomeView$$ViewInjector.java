package com.navdy.hud.app.view;

public class WelcomeView$$ViewInjector {
    public static void inject(butterknife.ButterKnife.Finder finder, com.navdy.hud.app.view.WelcomeView target, java.lang.Object source) {
        target.titleView = (android.widget.TextView) finder.findRequiredView(source, com.navdy.hud.app.R.id.title, "field 'titleView'");
        target.subTitleView = (android.widget.TextView) finder.findRequiredView(source, com.navdy.hud.app.R.id.subTitle, "field 'subTitleView'");
        target.messageView = (android.widget.TextView) finder.findRequiredView(source, com.navdy.hud.app.R.id.message, "field 'messageView'");
        target.animatorView = (com.navdy.hud.app.ui.component.FluctuatorAnimatorView) finder.findRequiredView(source, com.navdy.hud.app.R.id.animatorView, "field 'animatorView'");
        target.carousel = (com.navdy.hud.app.ui.component.carousel.CarouselLayout) finder.findRequiredView(source, com.navdy.hud.app.R.id.carousel, "field 'carousel'");
        target.rootContainer = finder.findRequiredView(source, com.navdy.hud.app.R.id.rootContainer, "field 'rootContainer'");
        target.downloadAppContainer = (android.widget.RelativeLayout) finder.findRequiredView(source, com.navdy.hud.app.R.id.download_app, "field 'downloadAppContainer'");
        target.downloadAppTextView1 = (android.widget.TextView) finder.findRequiredView(source, com.navdy.hud.app.R.id.download_text_1, "field 'downloadAppTextView1'");
        target.downloadAppTextView2 = (android.widget.TextView) finder.findRequiredView(source, com.navdy.hud.app.R.id.download_text_2, "field 'downloadAppTextView2'");
        target.appStoreImage1 = (android.widget.ImageView) finder.findRequiredView(source, com.navdy.hud.app.R.id.app_store_img_1, "field 'appStoreImage1'");
        target.appStoreImage2 = (android.widget.ImageView) finder.findRequiredView(source, com.navdy.hud.app.R.id.app_store_img_2, "field 'appStoreImage2'");
        target.playStoreImage1 = (android.widget.ImageView) finder.findRequiredView(source, com.navdy.hud.app.R.id.play_store_img_1, "field 'playStoreImage1'");
        target.playStoreImage2 = (android.widget.ImageView) finder.findRequiredView(source, com.navdy.hud.app.R.id.play_store_img_2, "field 'playStoreImage2'");
        target.choiceLayout = (com.navdy.hud.app.ui.component.ChoiceLayout) finder.findRequiredView(source, com.navdy.hud.app.R.id.choiceLayout, "field 'choiceLayout'");
        target.leftDot = (android.widget.ImageView) finder.findRequiredView(source, com.navdy.hud.app.R.id.leftDot, "field 'leftDot'");
        target.rightDot = (android.widget.ImageView) finder.findRequiredView(source, com.navdy.hud.app.R.id.rightDot, "field 'rightDot'");
    }

    public static void reset(com.navdy.hud.app.view.WelcomeView target) {
        target.titleView = null;
        target.subTitleView = null;
        target.messageView = null;
        target.animatorView = null;
        target.carousel = null;
        target.rootContainer = null;
        target.downloadAppContainer = null;
        target.downloadAppTextView1 = null;
        target.downloadAppTextView2 = null;
        target.appStoreImage1 = null;
        target.appStoreImage2 = null;
        target.playStoreImage1 = null;
        target.playStoreImage2 = null;
        target.choiceLayout = null;
        target.leftDot = null;
        target.rightDot = null;
    }
}
