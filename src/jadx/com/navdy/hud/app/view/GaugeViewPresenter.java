package com.navdy.hud.app.view;

public abstract class GaugeViewPresenter extends com.navdy.hud.app.view.DashboardWidgetPresenter {
    protected com.navdy.hud.app.view.GaugeView mGaugeView;

    public void setView(com.navdy.hud.app.view.DashboardWidgetView dashboardWidgetView) {
        super.setView(dashboardWidgetView);
        if (dashboardWidgetView == null) {
            this.mGaugeView = null;
        } else if (!(dashboardWidgetView instanceof com.navdy.hud.app.view.GaugeView)) {
            throw new java.lang.IllegalArgumentException("The view has to be of type GaugeView");
        } else {
            this.mGaugeView = (com.navdy.hud.app.view.GaugeView) dashboardWidgetView;
        }
    }
}
