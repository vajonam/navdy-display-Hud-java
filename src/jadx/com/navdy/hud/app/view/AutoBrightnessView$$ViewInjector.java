package com.navdy.hud.app.view;

public class AutoBrightnessView$$ViewInjector {
    public static void inject(butterknife.ButterKnife.Finder finder, com.navdy.hud.app.view.AutoBrightnessView target, java.lang.Object source) {
        target.autoBrightnessTitle = (android.widget.TextView) finder.findRequiredView(source, com.navdy.hud.app.R.id.mainTitle, "field 'autoBrightnessTitle'");
        target.autoBrightnessIcon = (android.widget.ImageView) finder.findRequiredView(source, com.navdy.hud.app.R.id.image, "field 'autoBrightnessIcon'");
        target.title1 = (android.widget.TextView) finder.findRequiredView(source, com.navdy.hud.app.R.id.title1, "field 'title1'");
        target.brightnessTitle = (android.widget.TextView) finder.findRequiredView(source, com.navdy.hud.app.R.id.title2, "field 'brightnessTitle'");
        target.brightnessTitleDesc = (android.widget.TextView) finder.findRequiredView(source, com.navdy.hud.app.R.id.title3, "field 'brightnessTitleDesc'");
        target.infoContainer = (android.widget.LinearLayout) finder.findRequiredView(source, com.navdy.hud.app.R.id.infoContainer, "field 'infoContainer'");
        target.autoBrightnessChoices = (com.navdy.hud.app.ui.component.ChoiceLayout) finder.findRequiredView(source, com.navdy.hud.app.R.id.choiceLayout, "field 'autoBrightnessChoices'");
    }

    public static void reset(com.navdy.hud.app.view.AutoBrightnessView target) {
        target.autoBrightnessTitle = null;
        target.autoBrightnessIcon = null;
        target.title1 = null;
        target.brightnessTitle = null;
        target.brightnessTitleDesc = null;
        target.infoContainer = null;
        target.autoBrightnessChoices = null;
    }
}
