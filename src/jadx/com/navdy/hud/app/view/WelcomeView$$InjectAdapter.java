package com.navdy.hud.app.view;

public final class WelcomeView$$InjectAdapter extends dagger.internal.Binding<com.navdy.hud.app.view.WelcomeView> implements dagger.MembersInjector<com.navdy.hud.app.view.WelcomeView> {
    private dagger.internal.Binding<com.navdy.hud.app.screen.WelcomeScreen.Presenter> presenter;

    public WelcomeView$$InjectAdapter() {
        super(null, "members/com.navdy.hud.app.view.WelcomeView", false, com.navdy.hud.app.view.WelcomeView.class);
    }

    public void attach(dagger.internal.Linker linker) {
        this.presenter = linker.requestBinding("com.navdy.hud.app.screen.WelcomeScreen$Presenter", com.navdy.hud.app.view.WelcomeView.class, getClass().getClassLoader());
    }

    public void getDependencies(java.util.Set<dagger.internal.Binding<?>> set, java.util.Set<dagger.internal.Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.presenter);
    }

    public void injectMembers(com.navdy.hud.app.view.WelcomeView object) {
        object.presenter = (com.navdy.hud.app.screen.WelcomeScreen.Presenter) this.presenter.get();
    }
}
