package com.navdy.hud.app.view;

public final class AutoBrightnessView$$InjectAdapter extends dagger.internal.Binding<com.navdy.hud.app.view.AutoBrightnessView> implements dagger.MembersInjector<com.navdy.hud.app.view.AutoBrightnessView> {
    private dagger.internal.Binding<com.navdy.hud.app.screen.AutoBrightnessScreen.Presenter> presenter;

    public AutoBrightnessView$$InjectAdapter() {
        super(null, "members/com.navdy.hud.app.view.AutoBrightnessView", false, com.navdy.hud.app.view.AutoBrightnessView.class);
    }

    public void attach(dagger.internal.Linker linker) {
        this.presenter = linker.requestBinding("com.navdy.hud.app.screen.AutoBrightnessScreen$Presenter", com.navdy.hud.app.view.AutoBrightnessView.class, getClass().getClassLoader());
    }

    public void getDependencies(java.util.Set<dagger.internal.Binding<?>> set, java.util.Set<dagger.internal.Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.presenter);
    }

    public void injectMembers(com.navdy.hud.app.view.AutoBrightnessView object) {
        object.presenter = (com.navdy.hud.app.screen.AutoBrightnessScreen.Presenter) this.presenter.get();
    }
}
