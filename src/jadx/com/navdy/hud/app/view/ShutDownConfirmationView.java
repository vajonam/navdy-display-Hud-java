package com.navdy.hud.app.view;

public class ShutDownConfirmationView extends android.widget.RelativeLayout implements com.navdy.hud.app.ui.component.ChoiceLayout.IListener, com.navdy.hud.app.manager.InputManager.IInputHandler {
    public static final int DIAL_OTA = 2;
    public static final int INACTIVITY = 0;
    private static final int INACTIVITY_SHUTDOWN_TIMEOUT = 60000;
    private static final int INSTALL_UPDATE_AND_SHUTDOWN_TIMEOUT = 30000;
    public static final int OTA = 1;
    private static final int SHUTDOWN_MESSAGE_MAX_WIDTH = 360;
    private static final int SHUTDOWN_RESET_TIMEOUT = 30000;
    private static final int SHUTDOWN_TIMEOUT = 5000;
    private static final int TAG_DO_NOT_SHUT_DOWN = 2;
    private static final int TAG_INSTALL_AND_SHUT_DOWN = 1;
    private static final int TAG_INSTALL_DIAL_UPDATE = 3;
    private static final int TAG_SHUT_DOWN = 0;
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.view.ShutDownConfirmationView.class);
    private boolean gesturesEnabled;
    private android.os.Handler handler;
    private int initialState;
    @butterknife.InjectView(2131624150)
    com.navdy.hud.app.ui.component.ChoiceLayout mChoiceLayout;
    @butterknife.InjectView(2131624147)
    android.widget.ImageView mIcon;
    @butterknife.InjectView(2131624078)
    android.widget.TextView mInfoText;
    boolean mIsUpdateAvailable;
    @butterknife.InjectView(2131624151)
    android.widget.ImageView mLefttSwipe;
    @butterknife.InjectView(2131624077)
    android.widget.TextView mMainTitleText;
    @javax.inject.Inject
    com.navdy.hud.app.screen.ShutDownScreen.Presenter mPresenter;
    @butterknife.InjectView(2131624152)
    android.widget.ImageView mRightSwipe;
    @butterknife.InjectView(2131624144)
    android.widget.TextView mScreenTitleText;
    android.animation.ValueAnimator mSecondsAnimator;
    @butterknife.InjectView(2131624079)
    android.widget.TextView mSummaryText;
    @butterknife.InjectView(2131624076)
    android.widget.TextView mTextView1;
    private com.navdy.hud.app.event.Shutdown.Reason shutDownCause;
    private int swipeLeftAction;
    private int swipeRightAction;
    private java.lang.Runnable timeout;
    java.lang.String timerMessage;
    private java.lang.String updateTargetVersion;

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            com.navdy.hud.app.view.ShutDownConfirmationView.this.performAction(1);
        }
    }

    class Anon2 implements android.animation.ValueAnimator.AnimatorUpdateListener {
        Anon2() {
        }

        public void onAnimationUpdate(android.animation.ValueAnimator animation) {
            int value = ((java.lang.Integer) animation.getAnimatedValue()).intValue();
            com.navdy.hud.app.view.ShutDownConfirmationView.this.mInfoText.setText(com.navdy.hud.app.view.ShutDownConfirmationView.this.getResources().getString(com.navdy.hud.app.R.string.update_installation_will_begin_in, new java.lang.Object[]{java.lang.Integer.valueOf(value)}));
        }
    }

    class Anon3 implements java.lang.Runnable {
        Anon3() {
        }

        public void run() {
            com.navdy.hud.app.view.ShutDownConfirmationView.this.performAction(3);
        }
    }

    class Anon4 implements android.animation.ValueAnimator.AnimatorUpdateListener {
        Anon4() {
        }

        public void onAnimationUpdate(android.animation.ValueAnimator animation) {
            int value = ((java.lang.Integer) animation.getAnimatedValue()).intValue();
            com.navdy.hud.app.view.ShutDownConfirmationView.this.mInfoText.setText(com.navdy.hud.app.view.ShutDownConfirmationView.this.getResources().getString(com.navdy.hud.app.R.string.dial_update_installation_will_begin_in, new java.lang.Object[]{java.lang.Integer.valueOf(value)}));
        }
    }

    class Anon5 implements java.lang.Runnable {
        Anon5() {
        }

        public void run() {
            com.navdy.hud.app.view.ShutDownConfirmationView.this.performAction(0);
        }
    }

    class Anon6 implements android.animation.ValueAnimator.AnimatorUpdateListener {
        Anon6() {
        }

        public void onAnimationUpdate(android.animation.ValueAnimator animation) {
            int value = ((java.lang.Integer) animation.getAnimatedValue()).intValue();
            com.navdy.hud.app.view.ShutDownConfirmationView.this.mSummaryText.setText(com.navdy.hud.app.view.ShutDownConfirmationView.this.getResources().getString(com.navdy.hud.app.R.string.powering_off, new java.lang.Object[]{java.lang.Integer.valueOf(value)}));
        }
    }

    public ShutDownConfirmationView(android.content.Context context) {
        this(context, null);
    }

    public ShutDownConfirmationView(android.content.Context context, android.util.AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ShutDownConfirmationView(android.content.Context context, android.util.AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mIsUpdateAvailable = false;
        this.handler = new android.os.Handler();
        this.gesturesEnabled = false;
        this.swipeLeftAction = -1;
        this.swipeRightAction = -1;
        this.timerMessage = null;
        this.updateTargetVersion = "1.3.2884";
        this.shutDownCause = com.navdy.hud.app.event.Shutdown.Reason.INACTIVITY;
        this.initialState = 0;
        if (!isInEditMode()) {
            mortar.Mortar.inject(context, this);
        }
        initFromAttributes(context, attrs);
    }

    private void initFromAttributes(android.content.Context context, android.util.AttributeSet attrs) {
        android.content.res.TypedArray a = context.getTheme().obtainStyledAttributes(attrs, com.navdy.hud.app.R.styleable.ShutdownScreen, 0, 0);
        try {
            this.initialState = a.getInt(0, 0);
        } finally {
            a.recycle();
        }
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
        butterknife.ButterKnife.inject((android.view.View) this);
        this.mTextView1.setVisibility(8);
        android.widget.LinearLayout linearLayout = (android.widget.LinearLayout) findViewById(com.navdy.hud.app.R.id.infoContainer);
        android.view.ViewGroup.LayoutParams params = linearLayout.getLayoutParams();
        ((com.navdy.hud.app.view.MaxWidthLinearLayout) linearLayout).setMaxWidth(SHUTDOWN_MESSAGE_MAX_WIDTH);
        params.width = -2;
        linearLayout.setLayoutParams(params);
        java.lang.String updateFromVersion = "1.2.2872";
        if (this.mPresenter != null) {
            this.mPresenter.takeView(this);
            com.navdy.hud.app.framework.toast.ToastManager.getInstance().disableToasts(true);
            com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().enableNotifications(false);
            if (this.mPresenter.isSoftwareUpdatePending()) {
                this.initialState = 1;
                this.updateTargetVersion = this.mPresenter.getUpdateVersion();
                updateFromVersion = this.mPresenter.getCurrentVersion();
            } else if (this.mPresenter.isDialFirmwareUpdatePending()) {
                this.initialState = 2;
                com.navdy.hud.app.device.dial.DialFirmwareUpdater.Versions versions = com.navdy.hud.app.device.dial.DialManager.getInstance().getDialFirmwareUpdater().getVersions();
                this.updateTargetVersion = "1.0." + versions.local.incrementalVersion;
                updateFromVersion = "1.0." + versions.dial.incrementalVersion;
            } else {
                this.shutDownCause = this.mPresenter.getShutDownCause();
                this.initialState = 0;
            }
        }
        this.gesturesEnabled = false;
        this.swipeLeftAction = -1;
        this.swipeRightAction = -1;
        if (this.initialState == 1) {
            this.mIsUpdateAvailable = true;
            this.mScreenTitleText.setVisibility(8);
            this.mMainTitleText.setText(com.navdy.hud.app.R.string.update_available);
            this.timerMessage = getResources().getString(com.navdy.hud.app.R.string.update_installation_will_begin_in, new java.lang.Object[]{java.lang.Integer.valueOf(30)});
            this.mInfoText.setVisibility(0);
            this.mInfoText.setText(this.timerMessage);
            java.lang.String updateSummary = java.lang.String.format(getResources().getString(com.navdy.hud.app.R.string.update_navdy_will_upgrade_from_to), new java.lang.Object[]{this.updateTargetVersion, updateFromVersion});
            this.mSummaryText.setVisibility(0);
            this.mSummaryText.setText(updateSummary);
            this.mIcon.setImageResource(com.navdy.hud.app.R.drawable.icon_software_update);
            java.util.List<com.navdy.hud.app.ui.component.ChoiceLayout.Choice> list = new java.util.ArrayList<>();
            list.add(new com.navdy.hud.app.ui.component.ChoiceLayout.Choice(getContext().getString(com.navdy.hud.app.R.string.install), 1));
            list.add(new com.navdy.hud.app.ui.component.ChoiceLayout.Choice(getContext().getString(com.navdy.hud.app.R.string.shutdown), 0));
            list.add(new com.navdy.hud.app.ui.component.ChoiceLayout.Choice(getContext().getString(com.navdy.hud.app.R.string.dont_shutdown), 2));
            this.mChoiceLayout.setChoices(com.navdy.hud.app.ui.component.ChoiceLayout.Mode.LABEL, list, 0, this);
            this.swipeLeftAction = 1;
            this.swipeRightAction = 2;
            this.gesturesEnabled = true;
            this.mRightSwipe.setVisibility(0);
            this.mLefttSwipe.setVisibility(0);
            startCountDown();
        } else if (this.initialState == 2) {
            this.mIsUpdateAvailable = true;
            this.mScreenTitleText.setText("");
            this.mMainTitleText.setText(com.navdy.hud.app.R.string.dial_update_available);
            this.timerMessage = getResources().getString(com.navdy.hud.app.R.string.dial_update_installation_will_begin_in, new java.lang.Object[]{java.lang.Integer.valueOf(30)});
            this.mInfoText.setVisibility(0);
            this.mInfoText.setText(this.timerMessage);
            this.mSummaryText.setText(java.lang.String.format(getResources().getString(com.navdy.hud.app.R.string.dial_update_will_upgrade_to_from), new java.lang.Object[]{this.updateTargetVersion, updateFromVersion}));
            this.mSummaryText.setVisibility(0);
            this.mIcon.setImageResource(com.navdy.hud.app.R.drawable.icon_dial_update);
            java.util.List<com.navdy.hud.app.ui.component.ChoiceLayout.Choice> list2 = new java.util.ArrayList<>();
            list2.add(new com.navdy.hud.app.ui.component.ChoiceLayout.Choice(getContext().getString(com.navdy.hud.app.R.string.install), 3));
            list2.add(new com.navdy.hud.app.ui.component.ChoiceLayout.Choice(getContext().getString(com.navdy.hud.app.R.string.shutdown), 0));
            list2.add(new com.navdy.hud.app.ui.component.ChoiceLayout.Choice(getContext().getString(com.navdy.hud.app.R.string.dont_shutdown), 2));
            this.mChoiceLayout.setChoices(com.navdy.hud.app.ui.component.ChoiceLayout.Mode.LABEL, list2, 0, this);
            this.swipeLeftAction = 3;
            this.swipeRightAction = 2;
            this.gesturesEnabled = true;
            this.mRightSwipe.setVisibility(0);
            this.mLefttSwipe.setVisibility(0);
            startCountDown();
        } else {
            this.mIsUpdateAvailable = false;
            this.mScreenTitleText.setText(null);
            this.mMainTitleText.setText(com.navdy.hud.app.R.string.shutting_down);
            this.mInfoText.setVisibility(0);
            setShutdownCause(this.shutDownCause);
            this.mSummaryText.setText(getResources().getString(com.navdy.hud.app.R.string.powering_off, new java.lang.Object[]{java.lang.Integer.valueOf(5)}));
            this.mSummaryText.setVisibility(0);
            this.mIcon.setImageResource(com.navdy.hud.app.R.drawable.icon_power);
            java.util.List<com.navdy.hud.app.ui.component.ChoiceLayout.Choice> list3 = new java.util.ArrayList<>();
            list3.add(new com.navdy.hud.app.ui.component.ChoiceLayout.Choice(getContext().getString(com.navdy.hud.app.R.string.shutdown), 0));
            list3.add(new com.navdy.hud.app.ui.component.ChoiceLayout.Choice(getContext().getString(com.navdy.hud.app.R.string.dont_shutdown), 2));
            this.mChoiceLayout.setChoices(com.navdy.hud.app.ui.component.ChoiceLayout.Mode.LABEL, list3, 0, this);
            sLogger.v("shutdown cause is " + this.shutDownCause + " time is " + getTimeout(this.shutDownCause));
            this.mRightSwipe.setVisibility(8);
            this.mLefttSwipe.setVisibility(8);
            startCountDown();
        }
        com.navdy.hud.app.util.ViewUtil.autosize(this.mMainTitleText, 2, (int) SHUTDOWN_MESSAGE_MAX_WIDTH, (int) com.navdy.hud.app.R.array.title_sizes);
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        cancelTimeout();
        if (this.mPresenter != null) {
            this.mPresenter.dropView(this);
            com.navdy.hud.app.framework.toast.ToastManager.getInstance().disableToasts(false);
            com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().enableNotifications(true);
        }
    }

    public boolean onGesture(com.navdy.service.library.events.input.GestureEvent event) {
        if (this.gesturesEnabled && event.gesture != null) {
            switch (event.gesture) {
                case GESTURE_SWIPE_LEFT:
                    if (this.swipeLeftAction != -1) {
                        executeItem(0, this.swipeLeftAction);
                        return true;
                    }
                    break;
                case GESTURE_SWIPE_RIGHT:
                    if (this.swipeRightAction != -1) {
                        executeItem(0, this.swipeRightAction);
                        return true;
                    }
                    break;
            }
        }
        return false;
    }

    public boolean onKey(com.navdy.hud.app.manager.InputManager.CustomKeyEvent event) {
        if (this.mChoiceLayout == null || this.mChoiceLayout.getVisibility() != 0) {
            return false;
        }
        switch (event) {
            case LEFT:
                startCountDown();
                this.mChoiceLayout.moveSelectionLeft();
                return true;
            case RIGHT:
                startCountDown();
                this.mChoiceLayout.moveSelectionRight();
                return true;
            case SELECT:
                cancelTimeout();
                this.mChoiceLayout.executeSelectedItem(true);
                return true;
            case POWER_BUTTON_DOUBLE_CLICK:
                sLogger.v("power btn dlb-click");
                cancelTimeout();
                this.mPresenter.finish();
                return true;
            case POWER_BUTTON_CLICK:
                sLogger.v("power btn click");
                cancelTimeout();
                this.mPresenter.finish();
                return true;
            default:
                return true;
        }
    }

    public com.navdy.hud.app.manager.InputManager.IInputHandler nextHandler() {
        return null;
    }

    public void executeItem(int pos, int id) {
        performAction(id);
    }

    public void itemSelected(int pos, int id) {
    }

    /* access modifiers changed from: private */
    public void performAction(int tag) {
        switch (tag) {
            case 0:
                recordUpdateSelection(false);
                this.mPresenter.shutDown();
                return;
            case 1:
                if (this.mPresenter.isSoftwareUpdatePending()) {
                    recordUpdateSelection(true);
                    this.mPresenter.installAndShutDown();
                    return;
                }
                return;
            case 2:
                recordUpdateSelection(false);
                this.mPresenter.finish();
                return;
            case 3:
                if (this.mPresenter.isDialFirmwareUpdatePending()) {
                    recordUpdateSelection(true);
                    this.mScreenTitleText.setText(com.navdy.hud.app.R.string.dial_update_started);
                    this.mMainTitleText.setText("");
                    this.mInfoText.setVisibility(0);
                    java.util.List<com.navdy.hud.app.ui.component.ChoiceLayout.Choice> list = new java.util.ArrayList<>();
                    list.add(new com.navdy.hud.app.ui.component.ChoiceLayout.Choice(getContext().getString(com.navdy.hud.app.R.string.shutdown), 0));
                    this.mChoiceLayout.setChoices(com.navdy.hud.app.ui.component.ChoiceLayout.Mode.LABEL, list, 0, this);
                    this.mPresenter.installDialUpdateAndShutDown();
                    return;
                }
                return;
            default:
                return;
        }
    }

    private void recordUpdateSelection(boolean accepted) {
        if (this.mIsUpdateAvailable) {
            com.navdy.hud.app.analytics.AnalyticsSupport.recordUpdatePrompt(accepted, !this.mPresenter.isSoftwareUpdatePending(), this.updateTargetVersion);
        }
    }

    public void setShutdownCause(com.navdy.hud.app.event.Shutdown.Reason cause) {
        sLogger.d("Update is " + (this.mIsUpdateAvailable ? "" : "not ") + "available, shut down cause:" + cause);
        if (!this.mIsUpdateAvailable) {
            switch (cause) {
                case ENGINE_OFF:
                case ACCELERATE_SHUTDOWN:
                case INACTIVITY:
                    this.mInfoText.setVisibility(0);
                    this.mInfoText.setText(getResources().getString(com.navdy.hud.app.R.string.navdy_inactivity));
                    return;
                default:
                    this.mInfoText.setVisibility(8);
                    return;
            }
        } else {
            this.mInfoText.setVisibility(0);
        }
    }

    private void startCountDown() {
        sLogger.v("startCountDown");
        if (this.timeout != null) {
            this.handler.removeCallbacks(this.timeout);
        }
        if (this.mSecondsAnimator != null) {
            this.mSecondsAnimator.removeAllUpdateListeners();
            this.mSecondsAnimator.removeAllListeners();
            this.mSecondsAnimator.cancel();
        }
        if (this.initialState == 1) {
            if (this.timeout == null) {
                this.timeout = new com.navdy.hud.app.view.ShutDownConfirmationView.Anon1();
            }
            this.mSecondsAnimator = android.animation.ValueAnimator.ofInt(new int[]{30, 0});
            this.mSecondsAnimator.setDuration(30000);
            this.mSecondsAnimator.setInterpolator(new android.view.animation.LinearInterpolator());
            this.mSecondsAnimator.addUpdateListener(new com.navdy.hud.app.view.ShutDownConfirmationView.Anon2());
            this.handler.postDelayed(this.timeout, 30000);
            this.mSecondsAnimator.start();
        } else if (this.initialState == 2) {
            if (this.timeout == null) {
                this.timeout = new com.navdy.hud.app.view.ShutDownConfirmationView.Anon3();
            }
            this.mSecondsAnimator = android.animation.ValueAnimator.ofInt(new int[]{30, 0});
            this.mSecondsAnimator.setDuration(30000);
            this.mSecondsAnimator.setInterpolator(new android.view.animation.LinearInterpolator());
            this.mSecondsAnimator.addUpdateListener(new com.navdy.hud.app.view.ShutDownConfirmationView.Anon4());
            this.handler.postDelayed(this.timeout, 30000);
            this.mSecondsAnimator.start();
        } else {
            int timeVal = 30000;
            if (this.mSecondsAnimator == null) {
                timeVal = getTimeout(this.shutDownCause);
            }
            if (this.timeout == null) {
                this.timeout = new com.navdy.hud.app.view.ShutDownConfirmationView.Anon5();
            }
            this.mSecondsAnimator = android.animation.ValueAnimator.ofInt(new int[]{timeVal / 1000, 0});
            this.mSecondsAnimator.setDuration((long) timeVal);
            this.mSecondsAnimator.setInterpolator(new android.view.animation.LinearInterpolator());
            this.mSecondsAnimator.addUpdateListener(new com.navdy.hud.app.view.ShutDownConfirmationView.Anon6());
            this.handler.postDelayed(this.timeout, (long) timeVal);
            this.mSecondsAnimator.start();
        }
    }

    private int getTimeout(com.navdy.hud.app.event.Shutdown.Reason reason) {
        switch (reason) {
            case INACTIVITY:
                return 60000;
            default:
                return 5000;
        }
    }

    private void cancelTimeout() {
        sLogger.v("cancelTimeout");
        if (this.timeout != null) {
            this.handler.removeCallbacks(this.timeout);
        }
        if (this.mSecondsAnimator != null) {
            this.mSecondsAnimator.removeAllUpdateListeners();
            this.mSecondsAnimator.removeAllListeners();
            this.mSecondsAnimator.cancel();
        }
    }
}
