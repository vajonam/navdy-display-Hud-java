package com.navdy.hud.app.view;

public class CalendarWidgetPresenter extends com.navdy.hud.app.view.DashboardWidgetPresenter {
    private static final long MILLISECONDS_IN_A_DAY = 86400000;
    private static final int REFRESH_INTERVAL = ((int) java.util.concurrent.TimeUnit.MINUTES.toMillis(5));
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.view.CalendarWidgetPresenter.class);
    private java.lang.String calendarGaugeName;
    com.navdy.hud.app.framework.calendar.CalendarManager calendarManager;
    private android.content.Context context;
    private java.lang.String formattedText;
    private android.os.Handler handler;
    private boolean nextEventExists;
    private int paddingLeft;
    private java.lang.Runnable refreshRunnable;
    private com.navdy.hud.app.common.TimeHelper timeHelper = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getTimeHelper();
    private java.lang.String title;

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            com.navdy.hud.app.view.CalendarWidgetPresenter.this.refreshCalendarEvent();
        }
    }

    public CalendarWidgetPresenter(android.content.Context context2) {
        this.context = context2;
        this.paddingLeft = context2.getResources().getDimensionPixelSize(com.navdy.hud.app.R.dimen.calendar_widget_left_padding);
        this.calendarGaugeName = context2.getResources().getString(com.navdy.hud.app.R.string.widget_calendar);
        this.calendarManager = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getCalendarManager();
        this.handler = new android.os.Handler();
        this.refreshRunnable = new com.navdy.hud.app.view.CalendarWidgetPresenter.Anon1();
    }

    public void setView(com.navdy.hud.app.view.DashboardWidgetView dashboardWidgetView, android.os.Bundle arguments) {
        int paddingLeft2;
        if (dashboardWidgetView != null) {
            int gravity = arguments != null ? arguments.getInt(com.navdy.hud.app.view.DashboardWidgetPresenter.EXTRA_GRAVITY, 0) : 0;
            dashboardWidgetView.setContentView((int) com.navdy.hud.app.R.layout.calendar_widget);
            if (gravity == 2) {
                paddingLeft2 = this.paddingLeft;
            } else {
                paddingLeft2 = 0;
            }
            dashboardWidgetView.getChildAt(0).setPadding(paddingLeft2, 0, 0, 0);
            this.handler.removeCallbacks(this.refreshRunnable);
            refreshCalendarEvent();
        } else {
            this.handler.removeCallbacks(this.refreshRunnable);
        }
        super.setView(dashboardWidgetView, arguments);
    }

    /* access modifiers changed from: protected */
    public boolean isRegisteringToBusRequired() {
        return true;
    }

    public void refreshCalendarEvent() {
        java.util.List<com.navdy.service.library.events.calendars.CalendarEvent> calendarEvents = this.calendarManager.getCalendarEvents();
        com.navdy.service.library.events.calendars.CalendarEvent nextEvent = null;
        long currentTime = java.lang.System.currentTimeMillis();
        java.util.TimeZone timeZone = this.timeHelper.getTimeZone();
        java.util.Calendar currentTimeCalendar = java.util.Calendar.getInstance(timeZone);
        currentTimeCalendar.setTime(new java.util.Date(currentTime));
        if (calendarEvents != null) {
            java.util.Iterator it = calendarEvents.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                com.navdy.service.library.events.calendars.CalendarEvent event = (com.navdy.service.library.events.calendars.CalendarEvent) it.next();
                sLogger.d("Event :" + event.display_name + ", From : " + event.start_time + ", To : " + event.end_time + ", All day : " + event.all_day);
                long eventEndTime = ((java.lang.Long) com.squareup.wire.Wire.get(event.end_time, java.lang.Long.valueOf(0))).longValue();
                long eventStartTime = ((java.lang.Long) com.squareup.wire.Wire.get(event.start_time, java.lang.Long.valueOf(0))).longValue();
                java.util.Calendar eventStartTimeCalendar = java.util.Calendar.getInstance(timeZone);
                eventStartTimeCalendar.setTime(new java.util.Date(eventStartTime));
                if (eventEndTime > currentTime && currentTimeCalendar.get(1) == eventStartTimeCalendar.get(1) && currentTimeCalendar.get(6) == eventStartTimeCalendar.get(6)) {
                    if (!event.all_day.booleanValue()) {
                        nextEvent = event;
                        break;
                    } else if (nextEvent == null) {
                        nextEvent = event;
                    }
                }
            }
        }
        if (nextEvent != null) {
            sLogger.d("Next event shown : " + nextEvent.display_name);
            this.nextEventExists = true;
            this.formattedText = formatTime(nextEvent.start_time.longValue());
            this.title = nextEvent.display_name;
        } else {
            sLogger.d("No event shown");
            this.nextEventExists = false;
            this.formattedText = "";
            this.title = "";
        }
        this.handler.removeCallbacks(this.refreshRunnable);
        this.handler.postDelayed(this.refreshRunnable, (long) REFRESH_INTERVAL);
        reDraw();
    }

    private java.lang.String formatTime(long time) {
        java.lang.StringBuilder amPmMarker = new java.lang.StringBuilder();
        java.lang.String formattedTime = this.timeHelper.formatTime(new java.util.Date(time), amPmMarker);
        return amPmMarker.length() > 0 ? formattedTime + " " + amPmMarker.toString() : formattedTime;
    }

    public android.graphics.drawable.Drawable getDrawable() {
        return null;
    }

    /* access modifiers changed from: protected */
    public void updateGauge() {
        if (this.mWidgetView != null) {
            android.widget.TextView dateTextView = (android.widget.TextView) this.mWidgetView.findViewById(com.navdy.hud.app.R.id.txt_unit);
            android.widget.TextView titleTextView = (android.widget.TextView) this.mWidgetView.findViewById(com.navdy.hud.app.R.id.txt_value);
            if (this.nextEventExists) {
                dateTextView.setText(this.formattedText);
                titleTextView.setText(this.title);
                return;
            }
            dateTextView.setText(com.navdy.hud.app.R.string.today);
            titleTextView.setText(com.navdy.hud.app.R.string.no_more_events);
        }
    }

    public java.lang.String getWidgetIdentifier() {
        return com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.CALENDAR_WIDGET_ID;
    }

    public java.lang.String getWidgetName() {
        return this.calendarGaugeName;
    }

    @com.squareup.otto.Subscribe
    public void onClockChanged(com.navdy.hud.app.common.TimeHelper.UpdateClock updateClock) {
        refreshCalendarEvent();
    }

    @com.squareup.otto.Subscribe
    public void onCalendarManagerEvent(com.navdy.hud.app.framework.calendar.CalendarManager.CalendarManagerEvent event) {
        switch (event) {
            case UPDATED:
                refreshCalendarEvent();
                return;
            default:
                return;
        }
    }
}
