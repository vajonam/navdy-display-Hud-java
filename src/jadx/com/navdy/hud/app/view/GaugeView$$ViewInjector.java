package com.navdy.hud.app.view;

public class GaugeView$$ViewInjector {
    public static void inject(butterknife.ButterKnife.Finder finder, com.navdy.hud.app.view.GaugeView target, java.lang.Object source) {
        com.navdy.hud.app.view.DashboardWidgetView$$ViewInjector.inject(finder, target, source);
        target.mTvValue = (android.widget.TextView) finder.findRequiredView(source, com.navdy.hud.app.R.id.txt_value, "field 'mTvValue'");
        target.mTvUnit = (android.widget.TextView) finder.findOptionalView(source, com.navdy.hud.app.R.id.txt_unit);
    }

    public static void reset(com.navdy.hud.app.view.GaugeView target) {
        com.navdy.hud.app.view.DashboardWidgetView$$ViewInjector.reset(target);
        target.mTvValue = null;
        target.mTvUnit = null;
    }
}
