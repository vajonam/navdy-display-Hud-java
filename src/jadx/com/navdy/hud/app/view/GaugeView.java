package com.navdy.hud.app.view;

public class GaugeView extends com.navdy.hud.app.view.DashboardWidgetView {
    @butterknife.InjectView(2131624129)
    @butterknife.Optional
    android.widget.TextView mTvUnit;
    @butterknife.InjectView(2131624130)
    android.widget.TextView mTvValue;

    public GaugeView(android.content.Context context) {
        this(context, null);
    }

    public GaugeView(android.content.Context context, android.util.AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public GaugeView(android.content.Context context, android.util.AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        android.content.res.TypedArray customAttributes = context.obtainStyledAttributes(attrs, com.navdy.hud.app.R.styleable.GaugeView, defStyleAttr, 0);
        if (customAttributes != null) {
            setValueTextSize(customAttributes.getDimensionPixelSize(0, 0));
            customAttributes.recycle();
        }
    }

    public void setValueTextSize(int pixelSize) {
        this.mTvValue.setTextSize(0, (float) pixelSize);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    public void setValueText(java.lang.CharSequence text) {
        if (this.mTvValue != null) {
            this.mTvValue.setText(text);
        }
    }

    public void setUnitText(java.lang.CharSequence text) {
        if (this.mTvUnit != null) {
            this.mTvUnit.setText(text);
        }
    }

    public android.widget.TextView getUnitTextView() {
        return this.mTvUnit;
    }

    public android.widget.TextView getValueTextView() {
        return this.mTvValue;
    }

    public void clear() {
        super.clear();
        this.mTvUnit.setText(null);
        this.mTvValue.setText(null);
    }

    public android.widget.TextView getValueView() {
        return this.mTvValue;
    }

    public android.widget.TextView getUnitView() {
        return this.mTvUnit;
    }
}
