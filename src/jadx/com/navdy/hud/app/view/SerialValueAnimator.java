package com.navdy.hud.app.view;

public class SerialValueAnimator {
    private com.navdy.hud.app.ui.framework.DefaultAnimationListener animationListener = new com.navdy.hud.app.view.SerialValueAnimator.Anon1();
    /* access modifiers changed from: private */
    public com.navdy.hud.app.view.SerialValueAnimator.SerialValueAnimatorAdapter mAdapter;
    /* access modifiers changed from: private */
    public boolean mAnimationRunning;
    /* access modifiers changed from: private */
    public android.animation.ValueAnimator mAnimator;
    private int mDuration;
    /* access modifiers changed from: private */
    public java.util.Queue<java.lang.Float> mValueQueue = new java.util.LinkedList();

    class Anon1 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
        Anon1() {
        }

        public void onAnimationEnd(android.animation.Animator animation) {
            if (animation instanceof android.animation.ValueAnimator) {
                com.navdy.hud.app.view.SerialValueAnimator.this.mAdapter.animationComplete(((java.lang.Float) ((android.animation.ValueAnimator) animation).getAnimatedValue()).floatValue());
            }
            if (com.navdy.hud.app.view.SerialValueAnimator.this.mValueQueue.size() == 0) {
                com.navdy.hud.app.view.SerialValueAnimator.this.mAnimationRunning = false;
                com.navdy.hud.app.view.SerialValueAnimator.this.mAnimator = null;
                return;
            }
            com.navdy.hud.app.view.SerialValueAnimator.this.animate(((java.lang.Float) com.navdy.hud.app.view.SerialValueAnimator.this.mValueQueue.remove()).floatValue());
        }
    }

    class Anon2 implements android.animation.ValueAnimator.AnimatorUpdateListener {
        Anon2() {
        }

        public void onAnimationUpdate(android.animation.ValueAnimator animation) {
            com.navdy.hud.app.view.SerialValueAnimator.this.mAdapter.setValue(((java.lang.Float) animation.getAnimatedValue()).floatValue());
        }
    }

    interface SerialValueAnimatorAdapter {
        void animationComplete(float f);

        float getValue();

        void setValue(float f);
    }

    public SerialValueAnimator(com.navdy.hud.app.view.SerialValueAnimator.SerialValueAnimatorAdapter adapter, int animationDuration) {
        this.mAdapter = adapter;
        this.mDuration = animationDuration;
        if (adapter == null) {
            throw new java.lang.IllegalArgumentException("SerialValueAnimator cannot run without the adapter");
        }
    }

    public void setValue(float val) {
        if (this.mAdapter.getValue() == val) {
            this.mAdapter.setValue(val);
            this.mAdapter.animationComplete(val);
        } else if (this.mAnimationRunning) {
            this.mValueQueue.add(java.lang.Float.valueOf(val));
        } else {
            this.mAnimationRunning = true;
            animate(val);
        }
    }

    /* access modifiers changed from: private */
    public void animate(float value) {
        this.mAnimator = android.animation.ValueAnimator.ofFloat(new float[]{this.mAdapter.getValue(), value});
        this.mAnimator.setDuration((long) this.mDuration);
        this.mAnimator.setInterpolator(new android.view.animation.LinearInterpolator());
        this.mAnimator.addUpdateListener(new com.navdy.hud.app.view.SerialValueAnimator.Anon2());
        this.mAnimator.addListener(this.animationListener);
        this.mAnimator.start();
    }

    public void release() {
        this.mValueQueue.clear();
        if (this.mAnimator != null) {
            this.mAnimator.removeAllListeners();
            this.mAnimator.removeAllUpdateListeners();
            this.mAnimator.cancel();
        }
        this.mAnimator = null;
        this.mAnimationRunning = false;
    }

    public void setDuration(int duration) {
        this.mDuration = duration;
    }
}
