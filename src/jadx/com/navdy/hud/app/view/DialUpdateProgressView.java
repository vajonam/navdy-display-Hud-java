package com.navdy.hud.app.view;

public class DialUpdateProgressView extends android.widget.RelativeLayout implements com.navdy.hud.app.manager.InputManager.IInputHandler {
    private static final long UPDATE_TIMEOUT = 480000;
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.view.DialUpdateProgressView.class);
    private android.os.Handler handler = new android.os.Handler();
    @javax.inject.Inject
    com.navdy.hud.app.screen.DialUpdateProgressScreen.Presenter mPresenter;
    @butterknife.InjectView(2131624321)
    android.widget.ProgressBar mProgress;
    private java.lang.Runnable timeout = new com.navdy.hud.app.view.DialUpdateProgressView.Anon1();

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            com.navdy.hud.app.view.DialUpdateProgressView.sLogger.v("timedout");
            com.navdy.hud.app.view.DialUpdateProgressView.this.mPresenter.finish(com.navdy.hud.app.device.dial.DialFirmwareUpdater.Error.TIMEDOUT);
        }
    }

    public DialUpdateProgressView(android.content.Context context) {
        super(context, null);
        if (!isInEditMode()) {
            mortar.Mortar.inject(context, this);
        }
    }

    public DialUpdateProgressView(android.content.Context context, android.util.AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) {
            mortar.Mortar.inject(context, this);
        }
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
        butterknife.ButterKnife.inject((android.view.View) this);
        if (this.mPresenter != null) {
            this.mPresenter.takeView(this);
            com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().enableNotifications(false);
            com.navdy.hud.app.framework.toast.ToastManager.getInstance().disableToasts(true);
            this.mPresenter.startUpdate();
            this.handler.removeCallbacks(this.timeout);
            this.handler.postDelayed(this.timeout, UPDATE_TIMEOUT);
        }
        setProgress(0);
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        com.navdy.hud.app.framework.toast.ToastManager.getInstance().disableToasts(false);
        com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().enableNotifications(true);
        if (this.mPresenter != null) {
            this.handler.removeCallbacks(this.timeout);
            this.mPresenter.dropView(this);
        }
    }

    public void setProgress(int percentage) {
        this.mProgress.setProgress(percentage);
    }

    public boolean onGesture(com.navdy.service.library.events.input.GestureEvent event) {
        return false;
    }

    public boolean onKey(com.navdy.hud.app.manager.InputManager.CustomKeyEvent event) {
        return true;
    }

    public com.navdy.hud.app.manager.InputManager.IInputHandler nextHandler() {
        return null;
    }
}
