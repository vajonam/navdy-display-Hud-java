package com.navdy.hud.app.view;

public class SpeedLimitSignPresenter$$ViewInjector {
    public static void inject(butterknife.ButterKnife.Finder finder, com.navdy.hud.app.view.SpeedLimitSignPresenter target, java.lang.Object source) {
        target.speedLimitSignView = (com.navdy.hud.app.view.SpeedLimitSignView) finder.findRequiredView(source, com.navdy.hud.app.R.id.speed_limit_sign, "field 'speedLimitSignView'");
        target.speedLimitUnavailableText = (android.widget.TextView) finder.findRequiredView(source, com.navdy.hud.app.R.id.txt_speed_limit_unavailable, "field 'speedLimitUnavailableText'");
    }

    public static void reset(com.navdy.hud.app.view.SpeedLimitSignPresenter target) {
        target.speedLimitSignView = null;
        target.speedLimitUnavailableText = null;
    }
}
