package com.navdy.hud.app.view;

public final class DialUpdateProgressView$$InjectAdapter extends dagger.internal.Binding<com.navdy.hud.app.view.DialUpdateProgressView> implements dagger.MembersInjector<com.navdy.hud.app.view.DialUpdateProgressView> {
    private dagger.internal.Binding<com.navdy.hud.app.screen.DialUpdateProgressScreen.Presenter> mPresenter;

    public DialUpdateProgressView$$InjectAdapter() {
        super(null, "members/com.navdy.hud.app.view.DialUpdateProgressView", false, com.navdy.hud.app.view.DialUpdateProgressView.class);
    }

    public void attach(dagger.internal.Linker linker) {
        this.mPresenter = linker.requestBinding("com.navdy.hud.app.screen.DialUpdateProgressScreen$Presenter", com.navdy.hud.app.view.DialUpdateProgressView.class, getClass().getClassLoader());
    }

    public void getDependencies(java.util.Set<dagger.internal.Binding<?>> set, java.util.Set<dagger.internal.Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.mPresenter);
    }

    public void injectMembers(com.navdy.hud.app.view.DialUpdateProgressView object) {
        object.mPresenter = (com.navdy.hud.app.screen.DialUpdateProgressScreen.Presenter) this.mPresenter.get();
    }
}
