package com.navdy.hud.app.view;

public final class LearnGestureScreenLayout$$InjectAdapter extends dagger.internal.Binding<com.navdy.hud.app.view.LearnGestureScreenLayout> implements dagger.MembersInjector<com.navdy.hud.app.view.LearnGestureScreenLayout> {
    private dagger.internal.Binding<com.navdy.hud.app.screen.GestureLearningScreen.Presenter> mPresenter;

    public LearnGestureScreenLayout$$InjectAdapter() {
        super(null, "members/com.navdy.hud.app.view.LearnGestureScreenLayout", false, com.navdy.hud.app.view.LearnGestureScreenLayout.class);
    }

    public void attach(dagger.internal.Linker linker) {
        this.mPresenter = linker.requestBinding("com.navdy.hud.app.screen.GestureLearningScreen$Presenter", com.navdy.hud.app.view.LearnGestureScreenLayout.class, getClass().getClassLoader());
    }

    public void getDependencies(java.util.Set<dagger.internal.Binding<?>> set, java.util.Set<dagger.internal.Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.mPresenter);
    }

    public void injectMembers(com.navdy.hud.app.view.LearnGestureScreenLayout object) {
        object.mPresenter = (com.navdy.hud.app.screen.GestureLearningScreen.Presenter) this.mPresenter.get();
    }
}
