package com.navdy.hud.app.view;

public class TemperatureWarningView$$ViewInjector {
    public static void inject(butterknife.ButterKnife.Finder finder, com.navdy.hud.app.view.TemperatureWarningView target, java.lang.Object source) {
        target.mScreenTitleText = (android.widget.TextView) finder.findRequiredView(source, com.navdy.hud.app.R.id.mainTitle, "field 'mScreenTitleText'");
        target.mTitle1 = (android.widget.TextView) finder.findRequiredView(source, com.navdy.hud.app.R.id.title1, "field 'mTitle1'");
        target.mMainTitleText = (android.widget.TextView) finder.findRequiredView(source, com.navdy.hud.app.R.id.title2, "field 'mMainTitleText'");
        target.mWarningMessage = (android.widget.TextView) finder.findRequiredView(source, com.navdy.hud.app.R.id.title3, "field 'mWarningMessage'");
        target.mChoiceLayout = (com.navdy.hud.app.ui.component.ChoiceLayout) finder.findRequiredView(source, com.navdy.hud.app.R.id.choiceLayout, "field 'mChoiceLayout'");
        target.mRightSwipe = (android.widget.ImageView) finder.findRequiredView(source, com.navdy.hud.app.R.id.rightSwipe, "field 'mRightSwipe'");
        target.mIcon = (android.widget.ImageView) finder.findRequiredView(source, com.navdy.hud.app.R.id.image, "field 'mIcon'");
    }

    public static void reset(com.navdy.hud.app.view.TemperatureWarningView target) {
        target.mScreenTitleText = null;
        target.mTitle1 = null;
        target.mMainTitleText = null;
        target.mWarningMessage = null;
        target.mChoiceLayout = null;
        target.mRightSwipe = null;
        target.mIcon = null;
    }
}
