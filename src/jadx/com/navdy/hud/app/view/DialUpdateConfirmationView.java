package com.navdy.hud.app.view;

public class DialUpdateConfirmationView extends android.widget.RelativeLayout implements com.navdy.hud.app.ui.component.ChoiceLayout.IListener, com.navdy.hud.app.manager.InputManager.IInputHandler {
    private static final int UPDATE_MESSAGE_MAX_WIDTH = 380;
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.view.DialUpdateConfirmationView.class);
    private boolean isReminder;
    @butterknife.InjectView(2131624150)
    com.navdy.hud.app.ui.component.ChoiceLayout mChoiceLayout;
    @butterknife.InjectView(2131624147)
    android.widget.ImageView mIcon;
    @butterknife.InjectView(2131624078)
    android.widget.TextView mInfoText;
    @butterknife.InjectView(2131624151)
    android.widget.ImageView mLefttSwipe;
    @butterknife.InjectView(2131624077)
    android.widget.TextView mMainTitleText;
    @javax.inject.Inject
    com.navdy.hud.app.screen.DialUpdateConfirmationScreen.Presenter mPresenter;
    @butterknife.InjectView(2131624152)
    android.widget.ImageView mRightSwipe;
    @butterknife.InjectView(2131624144)
    android.widget.TextView mScreenTitleText;
    @butterknife.InjectView(2131624079)
    android.widget.TextView mSummaryText;
    @butterknife.InjectView(2131624145)
    android.widget.RelativeLayout mainSection;
    private java.lang.String updateTargetVersion;

    public DialUpdateConfirmationView(android.content.Context context) {
        this(context, null, 0);
    }

    public DialUpdateConfirmationView(android.content.Context context, android.util.AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public DialUpdateConfirmationView(android.content.Context context, android.util.AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.updateTargetVersion = "1.0.60";
        this.isReminder = false;
        if (!isInEditMode()) {
            mortar.Mortar.inject(context, this);
        }
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
        java.lang.String updateFromVersion = "1.0.1";
        butterknife.ButterKnife.inject((android.view.View) this);
        if (this.mPresenter != null) {
            this.mPresenter.takeView(this);
            this.isReminder = this.mPresenter.isReminder();
            com.navdy.hud.app.framework.toast.ToastManager.getInstance().disableToasts(true);
            com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().enableNotifications(false);
            com.navdy.hud.app.device.dial.DialFirmwareUpdater.Versions versions = com.navdy.hud.app.device.dial.DialManager.getInstance().getDialFirmwareUpdater().getVersions();
            this.updateTargetVersion = "1.0." + versions.local.incrementalVersion;
            updateFromVersion = "1.0." + versions.dial.incrementalVersion;
        }
        this.mScreenTitleText.setVisibility(8);
        findViewById(com.navdy.hud.app.R.id.title1).setVisibility(8);
        this.mMainTitleText.setText(com.navdy.hud.app.R.string.dial_update_ready_to_install);
        com.navdy.hud.app.util.ViewUtil.adjustPadding(this.mainSection, 0, 0, 0, 10);
        com.navdy.hud.app.util.ViewUtil.autosize(this.mMainTitleText, 2, (int) UPDATE_MESSAGE_MAX_WIDTH, (int) com.navdy.hud.app.R.array.title_sizes);
        this.mMainTitleText.setVisibility(0);
        this.mIcon.setImageResource(com.navdy.hud.app.R.drawable.icon_dial_update);
        this.mInfoText.setText(com.navdy.hud.app.R.string.dial_update_installation_will_take);
        this.mInfoText.setVisibility(0);
        ((com.navdy.hud.app.view.MaxWidthLinearLayout) findViewById(com.navdy.hud.app.R.id.infoContainer)).setMaxWidth(UPDATE_MESSAGE_MAX_WIDTH);
        android.content.res.Resources res = getContext().getResources();
        this.mSummaryText.setText(java.lang.String.format(res.getString(com.navdy.hud.app.R.string.dial_update_will_upgrade_to_from), new java.lang.Object[]{this.updateTargetVersion, updateFromVersion}));
        this.mSummaryText.setVisibility(0);
        java.util.List<com.navdy.hud.app.ui.component.ChoiceLayout.Choice> list = new java.util.ArrayList<>();
        if (this.isReminder) {
            list.add(new com.navdy.hud.app.ui.component.ChoiceLayout.Choice(res.getString(com.navdy.hud.app.R.string.install_now), 0));
            list.add(new com.navdy.hud.app.ui.component.ChoiceLayout.Choice(res.getString(com.navdy.hud.app.R.string.install_later), 1));
        } else {
            list.add(new com.navdy.hud.app.ui.component.ChoiceLayout.Choice(res.getString(com.navdy.hud.app.R.string.install), 0));
            list.add(new com.navdy.hud.app.ui.component.ChoiceLayout.Choice(res.getString(com.navdy.hud.app.R.string.cancel), 1));
        }
        this.mChoiceLayout.setChoices(com.navdy.hud.app.ui.component.ChoiceLayout.Mode.LABEL, list, 0, this);
        this.mRightSwipe.setVisibility(0);
        this.mLefttSwipe.setVisibility(0);
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        com.navdy.hud.app.screen.BaseScreen screen = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getUiStateManager().getCurrentScreen();
        boolean enableNotif = true;
        if (screen != null && screen.getScreen() == com.navdy.service.library.events.ui.Screen.SCREEN_DIAL_UPDATE_PROGRESS) {
            enableNotif = false;
        }
        sLogger.v("enableNotif:" + enableNotif + " screen[" + (screen == null ? "none" : screen.getScreen()) + "]");
        if (enableNotif) {
            com.navdy.hud.app.framework.toast.ToastManager.getInstance().disableToasts(false);
            com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().enableNotifications(true);
        }
        if (this.mPresenter != null) {
            this.mPresenter.dropView(this);
        }
    }

    public void executeItem(int pos, int id) {
        switch (pos) {
            case 0:
                this.mPresenter.install();
                if (this.isReminder) {
                    com.navdy.hud.app.analytics.AnalyticsSupport.recordUpdatePrompt(true, true, this.updateTargetVersion);
                    return;
                }
                return;
            case 1:
                this.mPresenter.finish();
                if (this.isReminder) {
                    com.navdy.hud.app.analytics.AnalyticsSupport.recordUpdatePrompt(false, true, this.updateTargetVersion);
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void itemSelected(int pos, int id) {
    }

    public boolean onGesture(com.navdy.service.library.events.input.GestureEvent event) {
        if (event.gesture != null) {
            switch (event.gesture) {
                case GESTURE_SWIPE_LEFT:
                    executeItem(0, 0);
                    return true;
                case GESTURE_SWIPE_RIGHT:
                    executeItem(1, 0);
                    return true;
            }
        }
        return false;
    }

    public boolean onKey(com.navdy.hud.app.manager.InputManager.CustomKeyEvent event) {
        if (this.mChoiceLayout == null || this.mChoiceLayout.getVisibility() != 0) {
            return false;
        }
        switch (event) {
            case LEFT:
                this.mChoiceLayout.moveSelectionLeft();
                return true;
            case RIGHT:
                this.mChoiceLayout.moveSelectionRight();
                return true;
            case SELECT:
                this.mChoiceLayout.executeSelectedItem(true);
                return true;
            default:
                return true;
        }
    }

    public com.navdy.hud.app.manager.InputManager.IInputHandler nextHandler() {
        return null;
    }
}
