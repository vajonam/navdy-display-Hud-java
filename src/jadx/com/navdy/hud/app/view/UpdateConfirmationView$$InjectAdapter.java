package com.navdy.hud.app.view;

public final class UpdateConfirmationView$$InjectAdapter extends dagger.internal.Binding<com.navdy.hud.app.view.UpdateConfirmationView> implements dagger.MembersInjector<com.navdy.hud.app.view.UpdateConfirmationView> {
    private dagger.internal.Binding<com.navdy.hud.app.screen.OSUpdateConfirmationScreen.Presenter> mPresenter;

    public UpdateConfirmationView$$InjectAdapter() {
        super(null, "members/com.navdy.hud.app.view.UpdateConfirmationView", false, com.navdy.hud.app.view.UpdateConfirmationView.class);
    }

    public void attach(dagger.internal.Linker linker) {
        this.mPresenter = linker.requestBinding("com.navdy.hud.app.screen.OSUpdateConfirmationScreen$Presenter", com.navdy.hud.app.view.UpdateConfirmationView.class, getClass().getClassLoader());
    }

    public void getDependencies(java.util.Set<dagger.internal.Binding<?>> set, java.util.Set<dagger.internal.Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.mPresenter);
    }

    public void injectMembers(com.navdy.hud.app.view.UpdateConfirmationView object) {
        object.mPresenter = (com.navdy.hud.app.screen.OSUpdateConfirmationScreen.Presenter) this.mPresenter.get();
    }
}
