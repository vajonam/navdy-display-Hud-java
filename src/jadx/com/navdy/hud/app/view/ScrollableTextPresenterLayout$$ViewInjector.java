package com.navdy.hud.app.view;

public class ScrollableTextPresenterLayout$$ViewInjector {
    public static void inject(butterknife.ButterKnife.Finder finder, com.navdy.hud.app.view.ScrollableTextPresenterLayout target, java.lang.Object source) {
        target.mMainTitleText = (android.widget.TextView) finder.findRequiredView(source, com.navdy.hud.app.R.id.mainTitle, "field 'mMainTitleText'");
        target.mMainImageView = (android.widget.ImageView) finder.findRequiredView(source, com.navdy.hud.app.R.id.mainImage, "field 'mMainImageView'");
        target.mObservableScrollView = (com.navdy.hud.app.view.ObservableScrollView) finder.findRequiredView(source, com.navdy.hud.app.R.id.scrollView, "field 'mObservableScrollView'");
        target.mTitleText = (android.widget.TextView) finder.findRequiredView(source, com.navdy.hud.app.R.id.title, "field 'mTitleText'");
        target.mMessageText = (android.widget.TextView) finder.findRequiredView(source, com.navdy.hud.app.R.id.message, "field 'mMessageText'");
        target.topScrub = finder.findRequiredView(source, com.navdy.hud.app.R.id.topScrub, "field 'topScrub'");
        target.bottomScrub = finder.findRequiredView(source, com.navdy.hud.app.R.id.bottomScrub, "field 'bottomScrub'");
        target.mNotificationIndicator = (com.navdy.hud.app.ui.component.carousel.CarouselIndicator) finder.findRequiredView(source, com.navdy.hud.app.R.id.notifIndicator, "field 'mNotificationIndicator'");
        target.mNotificationScrollIndicator = (com.navdy.hud.app.ui.component.carousel.ProgressIndicator) finder.findRequiredView(source, com.navdy.hud.app.R.id.notifScrollIndicator, "field 'mNotificationScrollIndicator'");
        target.mChoiceLayout = (com.navdy.hud.app.ui.component.ChoiceLayout) finder.findRequiredView(source, com.navdy.hud.app.R.id.choiceLayout, "field 'mChoiceLayout'");
    }

    public static void reset(com.navdy.hud.app.view.ScrollableTextPresenterLayout target) {
        target.mMainTitleText = null;
        target.mMainImageView = null;
        target.mObservableScrollView = null;
        target.mTitleText = null;
        target.mMessageText = null;
        target.topScrub = null;
        target.bottomScrub = null;
        target.mNotificationIndicator = null;
        target.mNotificationScrollIndicator = null;
        target.mChoiceLayout = null;
    }
}
