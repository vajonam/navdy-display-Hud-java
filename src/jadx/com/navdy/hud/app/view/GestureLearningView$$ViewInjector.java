package com.navdy.hud.app.view;

public class GestureLearningView$$ViewInjector {
    public static void inject(butterknife.ButterKnife.Finder finder, com.navdy.hud.app.view.GestureLearningView target, java.lang.Object source) {
        target.mChoiceLayout = (com.navdy.hud.app.ui.component.ChoiceLayout) finder.findRequiredView(source, com.navdy.hud.app.R.id.choiceLayout, "field 'mChoiceLayout'");
        target.mTipsScroller = (android.widget.LinearLayout) finder.findRequiredView(source, com.navdy.hud.app.R.id.tips_scroller, "field 'mTipsScroller'");
        target.mTipsTextView1 = (android.widget.TextView) finder.findRequiredView(source, com.navdy.hud.app.R.id.tipsText1, "field 'mTipsTextView1'");
        target.mTipsTextView2 = (android.widget.TextView) finder.findRequiredView(source, com.navdy.hud.app.R.id.tipsText2, "field 'mTipsTextView2'");
        target.mCenterImage = (android.widget.ImageView) finder.findRequiredView(source, com.navdy.hud.app.R.id.center_image, "field 'mCenterImage'");
        target.mGestureProgressIndicator = finder.findRequiredView(source, com.navdy.hud.app.R.id.gesture_progress_indicator, "field 'mGestureProgressIndicator'");
        target.mLeftSwipeLayout = (android.widget.LinearLayout) finder.findRequiredView(source, com.navdy.hud.app.R.id.lyt_left_swipe, "field 'mLeftSwipeLayout'");
        target.mRightSwipeLayout = (android.widget.LinearLayout) finder.findRequiredView(source, com.navdy.hud.app.R.id.lyt_right_swipe, "field 'mRightSwipeLayout'");
    }

    public static void reset(com.navdy.hud.app.view.GestureLearningView target) {
        target.mChoiceLayout = null;
        target.mTipsScroller = null;
        target.mTipsTextView1 = null;
        target.mTipsTextView2 = null;
        target.mCenterImage = null;
        target.mGestureProgressIndicator = null;
        target.mLeftSwipeLayout = null;
        target.mRightSwipeLayout = null;
    }
}
