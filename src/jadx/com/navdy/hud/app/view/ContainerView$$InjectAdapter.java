package com.navdy.hud.app.view;

public final class ContainerView$$InjectAdapter extends dagger.internal.Binding<com.navdy.hud.app.view.ContainerView> implements dagger.MembersInjector<com.navdy.hud.app.view.ContainerView> {
    private dagger.internal.Binding<com.navdy.hud.app.ui.framework.UIStateManager> uiStateManager;

    public ContainerView$$InjectAdapter() {
        super(null, "members/com.navdy.hud.app.view.ContainerView", false, com.navdy.hud.app.view.ContainerView.class);
    }

    public void attach(dagger.internal.Linker linker) {
        this.uiStateManager = linker.requestBinding("com.navdy.hud.app.ui.framework.UIStateManager", com.navdy.hud.app.view.ContainerView.class, getClass().getClassLoader());
    }

    public void getDependencies(java.util.Set<dagger.internal.Binding<?>> set, java.util.Set<dagger.internal.Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.uiStateManager);
    }

    public void injectMembers(com.navdy.hud.app.view.ContainerView object) {
        object.uiStateManager = (com.navdy.hud.app.ui.framework.UIStateManager) this.uiStateManager.get();
    }
}
