package com.navdy.hud.app.view;

public class EmptyGaugePresenter extends com.navdy.hud.app.view.DashboardWidgetPresenter {
    public static final int ALPHA_ANIMATION_DURATION = 2000;
    public static final int ANIMATION_DELAY = 1000;
    private java.lang.String emptyGaugeName;
    private java.lang.Runnable mAnimationRunnable;
    @butterknife.InjectView(2131624165)
    android.widget.TextView mEmptyGaugeText;
    private android.os.Handler mHandler = new android.os.Handler();
    private boolean mPlayedAnimation = false;
    /* access modifiers changed from: private */
    public android.animation.ValueAnimator mTextAlphaAnimator = new android.animation.ValueAnimator();

    class Anon1 implements android.animation.ValueAnimator.AnimatorUpdateListener {
        Anon1() {
        }

        public void onAnimationUpdate(android.animation.ValueAnimator animation) {
            float alphaValue = ((java.lang.Float) animation.getAnimatedValue()).floatValue();
            if (com.navdy.hud.app.view.EmptyGaugePresenter.this.mEmptyGaugeText != null) {
                com.navdy.hud.app.view.EmptyGaugePresenter.this.mEmptyGaugeText.setAlpha(alphaValue);
            }
        }
    }

    class Anon2 implements java.lang.Runnable {
        Anon2() {
        }

        public void run() {
            com.navdy.hud.app.view.EmptyGaugePresenter.this.mTextAlphaAnimator.start();
        }
    }

    public EmptyGaugePresenter(android.content.Context context) {
        this.mTextAlphaAnimator.setFloatValues(new float[]{1.0f, 0.0f});
        this.mTextAlphaAnimator.setDuration(2000);
        this.emptyGaugeName = context.getString(com.navdy.hud.app.R.string.widget_empty);
        this.mTextAlphaAnimator.addUpdateListener(new com.navdy.hud.app.view.EmptyGaugePresenter.Anon1());
        this.mAnimationRunnable = new com.navdy.hud.app.view.EmptyGaugePresenter.Anon2();
    }

    public void setView(com.navdy.hud.app.view.DashboardWidgetView dashboardWidgetView, android.os.Bundle arguments) {
        if (dashboardWidgetView != null) {
            dashboardWidgetView.setContentView((int) com.navdy.hud.app.R.layout.empty_gauge_layout);
            butterknife.ButterKnife.inject((java.lang.Object) this, (android.view.View) dashboardWidgetView);
            if (arguments != null) {
                if (!arguments.getBoolean(com.navdy.hud.app.view.DashboardWidgetPresenter.EXTRA_IS_ACTIVE, false)) {
                    this.mPlayedAnimation = false;
                    this.mTextAlphaAnimator.end();
                    this.mHandler.removeCallbacks(this.mAnimationRunnable);
                    this.mEmptyGaugeText.setAlpha(1.0f);
                } else if (!this.mPlayedAnimation) {
                    this.mPlayedAnimation = true;
                    this.mTextAlphaAnimator.end();
                    this.mHandler.removeCallbacks(this.mAnimationRunnable);
                    this.mEmptyGaugeText.setAlpha(1.0f);
                    this.mHandler.postDelayed(this.mAnimationRunnable, 1000);
                }
            }
            super.setView(dashboardWidgetView, arguments);
            return;
        }
        this.mPlayedAnimation = false;
        super.setView(dashboardWidgetView, arguments);
    }

    public android.graphics.drawable.Drawable getDrawable() {
        return null;
    }

    /* access modifiers changed from: protected */
    public void updateGauge() {
    }

    public java.lang.String getWidgetIdentifier() {
        return com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.EMPTY_WIDGET_ID;
    }

    public java.lang.String getWidgetName() {
        return this.emptyGaugeName;
    }
}
