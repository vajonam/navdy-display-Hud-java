package com.navdy.hud.app.view;

public class LearnGestureScreenLayout extends android.widget.FrameLayout implements com.navdy.hud.app.manager.InputManager.IInputHandler, com.navdy.hud.app.ui.component.ChoiceLayout.IListener {
    public static final int TAG_BACK = 0;
    public static final int TAG_TIPS = 1;
    @butterknife.InjectView(2131624398)
    android.view.ViewGroup mCameraBlockedMessage;
    com.navdy.hud.app.ui.component.ChoiceLayout mCameraSensorBlockedChoiceLayout;
    private com.navdy.hud.app.view.LearnGestureScreenLayout.Mode mCurrentMode;
    @butterknife.InjectView(2131624396)
    com.navdy.hud.app.view.GestureLearningView mGestureLearningView;
    @javax.inject.Inject
    com.navdy.hud.app.screen.GestureLearningScreen.Presenter mPresenter;
    @butterknife.InjectView(2131624397)
    com.navdy.hud.app.view.ScrollableTextPresenterLayout mScrollableTextPresenter;
    @butterknife.InjectView(2131624399)
    com.navdy.hud.app.view.GestureVideoCaptureView mVideoCaptureInstructionsLayout;

    enum Mode {
        GESTURE,
        SENSOR_BLOCKED,
        TIPS,
        CAPTURE
    }

    public LearnGestureScreenLayout(android.content.Context context) {
        this(context, null);
    }

    public LearnGestureScreenLayout(android.content.Context context, android.util.AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public LearnGestureScreenLayout(android.content.Context context, android.util.AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mCurrentMode = com.navdy.hud.app.view.LearnGestureScreenLayout.Mode.GESTURE;
        if (!isInEditMode()) {
            mortar.Mortar.inject(context, this);
        }
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
        butterknife.ButterKnife.inject((android.view.View) this);
        android.widget.ImageView sideImageView = (android.widget.ImageView) this.mCameraBlockedMessage.findViewById(com.navdy.hud.app.R.id.sideImage);
        sideImageView.setVisibility(0);
        sideImageView.setImageResource(com.navdy.hud.app.R.drawable.icon_alert_sensor_blocked);
        android.widget.ImageView mainImageView = (android.widget.ImageView) this.mCameraBlockedMessage.findViewById(com.navdy.hud.app.R.id.image);
        mainImageView.setVisibility(0);
        mainImageView.setImageResource(com.navdy.hud.app.R.drawable.icon_settings_learning_to_gesture_gray);
        ((android.widget.TextView) this.mCameraBlockedMessage.findViewById(com.navdy.hud.app.R.id.title1)).setText(com.navdy.hud.app.R.string.sensor_is_blocked);
        android.widget.TextView secondaryText = (android.widget.TextView) this.mCameraBlockedMessage.findViewById(com.navdy.hud.app.R.id.title3);
        ((com.navdy.hud.app.view.MaxWidthLinearLayout) this.mCameraBlockedMessage.findViewById(com.navdy.hud.app.R.id.infoContainer)).setMaxWidth(getResources().getDimensionPixelSize(com.navdy.hud.app.R.dimen.gesture_sensor_blocked_max_width));
        secondaryText.setText(com.navdy.hud.app.R.string.sensor_is_blocked_message);
        this.mCameraBlockedMessage.findViewById(com.navdy.hud.app.R.id.title2).setVisibility(8);
        this.mCameraBlockedMessage.findViewById(com.navdy.hud.app.R.id.title4).setVisibility(8);
        secondaryText.setSingleLine(false);
        secondaryText.setTextSize(1, 17.0f);
        java.util.List<com.navdy.hud.app.ui.component.ChoiceLayout.Choice> list = new java.util.ArrayList<>();
        list.add(new com.navdy.hud.app.ui.component.ChoiceLayout.Choice(getContext().getString(com.navdy.hud.app.R.string.back), 0));
        list.add(new com.navdy.hud.app.ui.component.ChoiceLayout.Choice(getContext().getString(com.navdy.hud.app.R.string.tips), 1));
        this.mCameraSensorBlockedChoiceLayout = (com.navdy.hud.app.ui.component.ChoiceLayout) this.mCameraBlockedMessage.findViewById(com.navdy.hud.app.R.id.choiceLayout);
        this.mCameraSensorBlockedChoiceLayout.setChoices(com.navdy.hud.app.ui.component.ChoiceLayout.Mode.LABEL, list, 0, this);
        this.mCameraSensorBlockedChoiceLayout.setHighlightPersistent(true);
        java.lang.String[] array = getResources().getStringArray(com.navdy.hud.app.R.array.gesture_tips);
        java.lang.CharSequence[] charSequences = new java.lang.CharSequence[array.length];
        for (int i = 0; i < array.length; i++) {
            charSequences[i] = array[i];
        }
        this.mScrollableTextPresenter.setTextContents(charSequences);
        if (this.mPresenter != null) {
            this.mPresenter.takeView(this);
        }
        this.mGestureLearningView.setVisibility(0);
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.mPresenter != null) {
            this.mPresenter.dropView(this);
        }
    }

    public boolean onGesture(com.navdy.service.library.events.input.GestureEvent event) {
        return false;
    }

    public boolean onKey(com.navdy.hud.app.manager.InputManager.CustomKeyEvent event) {
        if (this.mCurrentMode != com.navdy.hud.app.view.LearnGestureScreenLayout.Mode.SENSOR_BLOCKED) {
            return false;
        }
        switch (event) {
            case LEFT:
                this.mCameraSensorBlockedChoiceLayout.moveSelectionLeft();
                return true;
            case RIGHT:
                this.mCameraSensorBlockedChoiceLayout.moveSelectionRight();
                return true;
            case SELECT:
                this.mCameraSensorBlockedChoiceLayout.executeSelectedItem(true);
                return true;
            default:
                return true;
        }
    }

    public com.navdy.hud.app.manager.InputManager.IInputHandler nextHandler() {
        switch (this.mCurrentMode) {
            case GESTURE:
                return this.mGestureLearningView;
            case TIPS:
                return this.mScrollableTextPresenter;
            case CAPTURE:
                return this.mVideoCaptureInstructionsLayout;
            default:
                return null;
        }
    }

    public void showTips() {
        this.mGestureLearningView.setVisibility(8);
        this.mCameraBlockedMessage.setVisibility(8);
        this.mVideoCaptureInstructionsLayout.setVisibility(8);
        this.mScrollableTextPresenter.setVisibility(0);
        this.mCurrentMode = com.navdy.hud.app.view.LearnGestureScreenLayout.Mode.TIPS;
    }

    public void hideTips() {
        this.mGestureLearningView.setVisibility(0);
        this.mVideoCaptureInstructionsLayout.setVisibility(8);
        this.mCameraBlockedMessage.setVisibility(8);
        this.mScrollableTextPresenter.setVisibility(8);
        this.mCurrentMode = com.navdy.hud.app.view.LearnGestureScreenLayout.Mode.GESTURE;
    }

    public void showSensorBlocked() {
        this.mGestureLearningView.setVisibility(8);
        this.mVideoCaptureInstructionsLayout.setVisibility(8);
        this.mCameraBlockedMessage.setVisibility(0);
        this.mScrollableTextPresenter.setVisibility(8);
        this.mCurrentMode = com.navdy.hud.app.view.LearnGestureScreenLayout.Mode.SENSOR_BLOCKED;
    }

    public void hideSensorBlocked() {
        this.mGestureLearningView.setVisibility(0);
        this.mCameraBlockedMessage.setVisibility(8);
        this.mVideoCaptureInstructionsLayout.setVisibility(8);
        this.mScrollableTextPresenter.setVisibility(8);
        this.mCurrentMode = com.navdy.hud.app.view.LearnGestureScreenLayout.Mode.GESTURE;
    }

    public void showCaptureGestureVideosView() {
        this.mGestureLearningView.setVisibility(8);
        this.mCameraBlockedMessage.setVisibility(8);
        this.mVideoCaptureInstructionsLayout.setVisibility(0);
        this.mScrollableTextPresenter.setVisibility(8);
        this.mCurrentMode = com.navdy.hud.app.view.LearnGestureScreenLayout.Mode.CAPTURE;
    }

    public void hideCaptureGestureVideosView() {
        this.mGestureLearningView.setVisibility(0);
        this.mCameraBlockedMessage.setVisibility(8);
        this.mVideoCaptureInstructionsLayout.setVisibility(8);
        this.mScrollableTextPresenter.setVisibility(8);
        this.mCurrentMode = com.navdy.hud.app.view.LearnGestureScreenLayout.Mode.GESTURE;
    }

    public void executeItem(int pos, int id) {
        switch (id) {
            case 0:
                this.mPresenter.hideCameraSensorBlocked();
                return;
            case 1:
                this.mPresenter.showTips();
                return;
            default:
                return;
        }
    }

    public void itemSelected(int pos, int id) {
    }
}
