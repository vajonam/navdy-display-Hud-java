package com.navdy.hud.app.view;

public abstract class DashboardWidgetPresenter {
    public static final java.lang.String EXTRA_GRAVITY = "EXTRA_GRAVITY";
    public static final java.lang.String EXTRA_IS_ACTIVE = "EXTRA_IS_ACTIVE";
    public static final int GRAVITY_CENTER = 1;
    public static final int GRAVITY_LEFT = 0;
    public static final int GRAVITY_RIGHT = 2;
    protected boolean isDashActive;
    protected boolean isRegistered = false;
    protected boolean isWidgetVisibleToUser;
    protected com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(getClass());
    protected android.view.View mCustomView;
    protected com.navdy.hud.app.view.DashboardWidgetView mWidgetView;

    public abstract android.graphics.drawable.Drawable getDrawable();

    public abstract java.lang.String getWidgetIdentifier();

    public abstract java.lang.String getWidgetName();

    /* access modifiers changed from: protected */
    public abstract void updateGauge();

    public void setView(com.navdy.hud.app.view.DashboardWidgetView dashboardWidgetView) {
        this.mWidgetView = dashboardWidgetView;
        if (this.mWidgetView != null) {
            this.mCustomView = dashboardWidgetView.getCustomView();
            if (this.mCustomView != null) {
                this.mCustomView.setBackground(getDrawable());
            }
            if (isRegisteringToBusRequired() && !this.isRegistered) {
                try {
                    registerToBus();
                    this.isRegistered = true;
                } catch (java.lang.RuntimeException re) {
                    this.logger.e("Runtime exception registering to the bus ", re);
                }
            }
        } else if (this.isRegistered) {
            try {
                unregisterToBus();
                this.isRegistered = false;
            } catch (java.lang.RuntimeException re2) {
                this.logger.e("Runtime exception unregistering from the bus ", re2);
            }
        }
        reDraw();
    }

    /* access modifiers changed from: protected */
    public boolean isRegisteringToBusRequired() {
        return false;
    }

    /* access modifiers changed from: protected */
    public void registerToBus() {
        com.navdy.hud.app.HudApplication.getApplication().getBus().register(this);
    }

    /* access modifiers changed from: protected */
    public void unregisterToBus() {
        com.navdy.hud.app.HudApplication.getApplication().getBus().unregister(this);
    }

    public void setView(com.navdy.hud.app.view.DashboardWidgetView dashboardWidgetView, android.os.Bundle arguments) {
        setView(dashboardWidgetView);
    }

    public void reDraw() {
        if (canDraw()) {
            updateGauge();
            if (this.mCustomView != null) {
                this.mCustomView.invalidate();
            }
        }
    }

    public boolean isWidgetActive() {
        return this.mWidgetView != null;
    }

    public com.navdy.hud.app.view.DashboardWidgetView getWidgetView() {
        return this.mWidgetView;
    }

    public void onPause() {
        this.isDashActive = false;
        if (canDraw()) {
            this.logger.v("widget::onPause:");
        }
    }

    public void onResume() {
        this.isDashActive = true;
        if (canDraw()) {
            this.logger.v("widget::onResume:");
            reDraw();
        }
    }

    public void setWidgetVisibleToUser(boolean b) {
        this.isWidgetVisibleToUser = b;
        if (canDraw()) {
            this.logger.v("widget::visible:reDraw");
            reDraw();
        }
    }

    public boolean canDraw() {
        if (!this.isDashActive || !this.isWidgetVisibleToUser || this.mWidgetView == null) {
            return false;
        }
        return true;
    }
}
