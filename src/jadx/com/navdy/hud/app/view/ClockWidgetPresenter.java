package com.navdy.hud.app.view;

public class ClockWidgetPresenter extends com.navdy.hud.app.view.DashboardWidgetPresenter {
    private static final int TIME_UPDATE_INTERVAL = 30000;
    private java.lang.StringBuilder amPmMarker = new java.lang.StringBuilder();
    private com.navdy.hud.app.view.drawable.AnalogClockDrawable analogClockDrawable;
    private java.lang.String analogClockWidgetName;
    private com.squareup.otto.Bus bus;
    private java.lang.String dateText;
    private int dayOfTheMonth;
    private int dayOfTheWeek;
    private java.lang.String dayText;
    private java.lang.String digitalClock2WidgetName;
    private java.lang.String digitalClockWidgetName;
    private com.navdy.hud.app.view.ClockWidgetPresenter.ClockType mClockType;
    private android.content.Context mContext;
    /* access modifiers changed from: private */
    public android.os.Handler mHandler = new android.os.Handler(android.os.Looper.getMainLooper());
    private com.navdy.hud.app.view.drawable.SmallAnalogClockDrawable smallAnalogClockDrawable;
    private com.navdy.hud.app.common.TimeHelper timeHelper;
    private int timeHour;
    private int timeMinute;
    private int timeSeconds;
    private java.lang.String timeText;
    /* access modifiers changed from: private */
    public java.lang.Runnable updateTimeRunnable;

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            com.navdy.hud.app.view.ClockWidgetPresenter.this.updateTime();
            com.navdy.hud.app.view.ClockWidgetPresenter.this.mHandler.postDelayed(com.navdy.hud.app.view.ClockWidgetPresenter.this.updateTimeRunnable, (long) com.navdy.hud.app.view.ClockWidgetPresenter.this.nextUpdateInterval());
        }
    }

    public enum ClockType {
        ANALOG,
        DIGITAL1,
        DIGITAL2
    }

    /* access modifiers changed from: private */
    public int nextUpdateInterval() {
        int intervalSecs = 30;
        int nextSecs = (this.timeSeconds + 30) % 60;
        if (nextSecs >= 45) {
            intervalSecs = 30 + (60 - nextSecs);
        } else if (nextSecs <= 15) {
            intervalSecs = 30 - nextSecs;
        }
        return intervalSecs * 1000;
    }

    public ClockWidgetPresenter(android.content.Context context, com.navdy.hud.app.view.ClockWidgetPresenter.ClockType clockType) {
        this.mClockType = clockType;
        this.mContext = context;
        this.timeHelper = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getTimeHelper();
        this.bus = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getBus();
        this.analogClockWidgetName = context.getResources().getString(com.navdy.hud.app.R.string.widget_clock_analog);
        this.digitalClockWidgetName = context.getResources().getString(com.navdy.hud.app.R.string.widget_clock_digital);
        this.digitalClock2WidgetName = context.getResources().getString(com.navdy.hud.app.R.string.widget_clock_time_and_date);
        this.updateTimeRunnable = new com.navdy.hud.app.view.ClockWidgetPresenter.Anon1();
        switch (clockType) {
            case ANALOG:
                this.analogClockDrawable = new com.navdy.hud.app.view.drawable.AnalogClockDrawable(context);
                return;
            case DIGITAL1:
                this.smallAnalogClockDrawable = new com.navdy.hud.app.view.drawable.SmallAnalogClockDrawable(context);
                return;
            default:
                return;
        }
    }

    @com.squareup.otto.Subscribe
    public void onUpdateClock(com.navdy.hud.app.common.TimeHelper.UpdateClock event) {
        updateTime();
    }

    /* access modifiers changed from: private */
    public void updateTime() {
        java.util.Calendar cal = java.util.Calendar.getInstance();
        java.util.Date currentTime = new java.util.Date();
        cal.setTime(currentTime);
        this.timeMinute = cal.get(12);
        this.timeHour = cal.get(11);
        this.timeSeconds = cal.get(13);
        this.dayOfTheMonth = cal.get(5);
        this.dayOfTheWeek = cal.get(7);
        switch (this.mClockType) {
            case DIGITAL1:
                break;
            case DIGITAL2:
                this.dateText = this.timeHelper.getDate();
                this.dayText = this.timeHelper.getDay();
                this.dateText = this.dateText.toUpperCase();
                this.dayText = this.dayText.toUpperCase();
                break;
        }
        this.timeText = this.timeHelper.formatTime(currentTime, this.amPmMarker);
        reDraw();
    }

    public void setView(com.navdy.hud.app.view.DashboardWidgetView dashboardWidgetView, android.os.Bundle arguments) {
        if (dashboardWidgetView != null) {
            switch (this.mClockType) {
                case ANALOG:
                    dashboardWidgetView.setContentView((int) com.navdy.hud.app.R.layout.smart_dash_widget_circular_content_layout);
                    break;
                case DIGITAL1:
                    dashboardWidgetView.setContentView((int) com.navdy.hud.app.R.layout.digital_clock1_layout);
                    break;
                case DIGITAL2:
                    dashboardWidgetView.setContentView((int) com.navdy.hud.app.R.layout.digital_clock2_layout);
                    break;
            }
            super.setView(dashboardWidgetView, arguments);
            this.mHandler.removeCallbacks(this.updateTimeRunnable);
            updateTime();
            this.mHandler.postDelayed(this.updateTimeRunnable, (long) nextUpdateInterval());
            return;
        }
        this.mHandler.removeCallbacks(this.updateTimeRunnable);
        super.setView(dashboardWidgetView, arguments);
    }

    /* access modifiers changed from: protected */
    public boolean isRegisteringToBusRequired() {
        return true;
    }

    public android.graphics.drawable.Drawable getDrawable() {
        switch (this.mClockType) {
            case ANALOG:
                return this.analogClockDrawable;
            case DIGITAL1:
                return this.smallAnalogClockDrawable;
            default:
                return null;
        }
    }

    /* access modifiers changed from: protected */
    public void updateGauge() {
        if (this.mWidgetView != null) {
            switch (this.mClockType) {
                case ANALOG:
                    this.analogClockDrawable.setTime(this.dayOfTheMonth, this.timeHour, this.timeMinute, 0);
                    return;
                case DIGITAL1:
                    this.smallAnalogClockDrawable.setTime(this.timeHour, this.timeMinute, this.timeSeconds);
                    android.widget.TextView timeTextView = (android.widget.TextView) this.mWidgetView.findViewById(com.navdy.hud.app.R.id.txt_value);
                    ((android.widget.TextView) this.mWidgetView.findViewById(com.navdy.hud.app.R.id.txt_unit)).setText(this.amPmMarker.toString());
                    timeTextView.setText(this.timeText);
                    return;
                case DIGITAL2:
                    android.widget.TextView pmTextView = (android.widget.TextView) this.mWidgetView.findViewById(com.navdy.hud.app.R.id.txt_unit);
                    ((android.widget.TextView) this.mWidgetView.findViewById(com.navdy.hud.app.R.id.txt_value)).setText(this.timeText);
                    pmTextView.setText(this.amPmMarker.toString());
                    ((android.widget.TextView) this.mWidgetView.findViewById(com.navdy.hud.app.R.id.txt_date)).setText(this.dateText);
                    ((android.widget.TextView) this.mWidgetView.findViewById(com.navdy.hud.app.R.id.txt_day)).setText(this.dayText);
                    return;
                default:
                    return;
            }
        }
    }

    public java.lang.String getWidgetIdentifier() {
        switch (this.mClockType) {
            case ANALOG:
                return com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.ANALOG_CLOCK_WIDGET_ID;
            case DIGITAL1:
                return com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.DIGITAL_CLOCK_WIDGET_ID;
            case DIGITAL2:
                return com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.DIGITAL_CLOCK_2_WIDGET_ID;
            default:
                return null;
        }
    }

    public java.lang.String getWidgetName() {
        switch (this.mClockType) {
            case ANALOG:
                return this.analogClockWidgetName;
            case DIGITAL1:
                return this.digitalClockWidgetName;
            case DIGITAL2:
                return this.digitalClock2WidgetName;
            default:
                return null;
        }
    }
}
