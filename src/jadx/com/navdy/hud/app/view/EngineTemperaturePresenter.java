package com.navdy.hud.app.view;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000v\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0006\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u0000 R2\u00020\u0001:\u0001RB\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\n\u0010C\u001a\u0004\u0018\u00010DH\u0016J\b\u0010E\u001a\u00020\u0018H\u0016J\b\u0010F\u001a\u00020\u0018H\u0016J\b\u0010G\u001a\u000206H\u0014J\u0010\u0010H\u001a\u00020I2\u0006\u0010J\u001a\u00020KH\u0007J\u001c\u0010L\u001a\u00020I2\b\u0010M\u001a\u0004\u0018\u00010N2\b\u0010O\u001a\u0004\u0018\u00010PH\u0016J\b\u0010Q\u001a\u00020IH\u0014R\u001a\u0010\u0005\u001a\u00020\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\nR\u001a\u0010\u000b\u001a\u00020\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\f\u0010\b\"\u0004\b\r\u0010\nR\u001a\u0010\u000e\u001a\u00020\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000f\u0010\b\"\u0004\b\u0010\u0010\nR\u001e\u0010\u0011\u001a\u00020\u00128\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0013\u0010\u0014\"\u0004\b\u0015\u0010\u0016R\u001a\u0010\u0017\u001a\u00020\u0018X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0019\u0010\u001a\"\u0004\b\u001b\u0010\u001cR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u001eR\u001e\u0010\u001f\u001a\u00020 8\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b!\u0010\"\"\u0004\b#\u0010$R$\u0010'\u001a\u00020&2\u0006\u0010%\u001a\u00020&@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b(\u0010)\"\u0004\b*\u0010+R\u001c\u0010,\u001a\u0004\u0018\u00010-X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b.\u0010/\"\u0004\b0\u00101R\u001a\u00102\u001a\u00020\u0018X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b3\u0010\u001a\"\u0004\b4\u0010\u001cR\u000e\u00105\u001a\u000206X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u00107\u001a\u000206X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u00108\u001a\u000209X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b:\u0010;\"\u0004\b<\u0010=R\u001a\u0010>\u001a\u000209X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b?\u0010;\"\u0004\b@\u0010=R\u0011\u0010A\u001a\u00020\u0018\u00a2\u0006\b\n\u0000\u001a\u0004\bB\u0010\u001a\u00a8\u0006S"}, d2 = {"Lcom/navdy/hud/app/view/EngineTemperaturePresenter;", "Lcom/navdy/hud/app/view/DashboardWidgetPresenter;", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "STATE_COLD", "", "getSTATE_COLD", "()I", "setSTATE_COLD", "(I)V", "STATE_HOT", "getSTATE_HOT", "setSTATE_HOT", "STATE_NORMAL", "getSTATE_NORMAL", "setSTATE_NORMAL", "bus", "Lcom/squareup/otto/Bus;", "getBus", "()Lcom/squareup/otto/Bus;", "setBus", "(Lcom/squareup/otto/Bus;)V", "celsiusLabel", "", "getCelsiusLabel", "()Ljava/lang/String;", "setCelsiusLabel", "(Ljava/lang/String;)V", "getContext", "()Landroid/content/Context;", "driverProfileManager", "Lcom/navdy/hud/app/profile/DriverProfileManager;", "getDriverProfileManager", "()Lcom/navdy/hud/app/profile/DriverProfileManager;", "setDriverProfileManager", "(Lcom/navdy/hud/app/profile/DriverProfileManager;)V", "value", "", "engineTemperature", "getEngineTemperature", "()D", "setEngineTemperature", "(D)V", "engineTemperatureDrawable", "Lcom/navdy/hud/app/view/drawable/EngineTemperatureDrawable;", "getEngineTemperatureDrawable", "()Lcom/navdy/hud/app/view/drawable/EngineTemperatureDrawable;", "setEngineTemperatureDrawable", "(Lcom/navdy/hud/app/view/drawable/EngineTemperatureDrawable;)V", "fahrenheitLabel", "getFahrenheitLabel", "setFahrenheitLabel", "mLeftOriented", "", "registered", "tempText", "Landroid/widget/TextView;", "getTempText", "()Landroid/widget/TextView;", "setTempText", "(Landroid/widget/TextView;)V", "tempUnitText", "getTempUnitText", "setTempUnitText", "widgetNameString", "getWidgetNameString", "getDrawable", "Landroid/graphics/drawable/Drawable;", "getWidgetIdentifier", "getWidgetName", "isRegisteringToBusRequired", "onSpeedUnitChanged", "", "speedUnitChangedEvent", "Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnitChanged;", "setView", "dashboardWidgetView", "Lcom/navdy/hud/app/view/DashboardWidgetView;", "arguments", "Landroid/os/Bundle;", "updateGauge", "Companion", "app_hudDebug"}, k = 1, mv = {1, 1, 6})
/* compiled from: EngineTemperaturePresenter.kt */
public final class EngineTemperaturePresenter extends com.navdy.hud.app.view.DashboardWidgetPresenter {
    public static final com.navdy.hud.app.view.EngineTemperaturePresenter.Companion Companion = new com.navdy.hud.app.view.EngineTemperaturePresenter.Companion(null);
    /* access modifiers changed from: private */
    public static final double TEMPERATURE_GAUGE_COLD_THRESHOLD = TEMPERATURE_GAUGE_COLD_THRESHOLD;
    /* access modifiers changed from: private */
    public static final double TEMPERATURE_GAUGE_HOT_THRESHOLD = TEMPERATURE_GAUGE_HOT_THRESHOLD;
    /* access modifiers changed from: private */
    public static final double TEMPERATURE_GAUGE_LOWER_BOUND_CELSIUS = TEMPERATURE_GAUGE_LOWER_BOUND_CELSIUS;
    /* access modifiers changed from: private */
    public static final double TEMPERATURE_GAUGE_MID_POINT_CELSIUS = TEMPERATURE_GAUGE_MID_POINT_CELSIUS;
    /* access modifiers changed from: private */
    public static final double TEMPERATURE_GAUGE_UPPER_BOUND_CELSIUS = TEMPERATURE_GAUGE_UPPER_BOUND_CELSIUS;
    private int STATE_COLD = 1;
    private int STATE_HOT = 2;
    private int STATE_NORMAL;
    @javax.inject.Inject
    @org.jetbrains.annotations.NotNull
    public com.squareup.otto.Bus bus;
    @org.jetbrains.annotations.NotNull
    private java.lang.String celsiusLabel = "";
    @org.jetbrains.annotations.NotNull
    private final android.content.Context context;
    @javax.inject.Inject
    @org.jetbrains.annotations.NotNull
    public com.navdy.hud.app.profile.DriverProfileManager driverProfileManager;
    private double engineTemperature;
    @org.jetbrains.annotations.Nullable
    private com.navdy.hud.app.view.drawable.EngineTemperatureDrawable engineTemperatureDrawable = new com.navdy.hud.app.view.drawable.EngineTemperatureDrawable(this.context, com.navdy.hud.app.R.array.smart_dash_engine_temperature_state_colors);
    @org.jetbrains.annotations.NotNull
    private java.lang.String fahrenheitLabel = "";
    private boolean mLeftOriented = true;
    private boolean registered;
    @org.jetbrains.annotations.NotNull
    public android.widget.TextView tempText;
    @org.jetbrains.annotations.NotNull
    public android.widget.TextView tempUnitText;
    @org.jetbrains.annotations.NotNull
    private final java.lang.String widgetNameString;

    @kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0006\n\u0002\b\u000b\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\u00020\u0004X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u0014\u0010\u0007\u001a\u00020\u0004X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\b\u0010\u0006R\u0014\u0010\t\u001a\u00020\u0004X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u0006R\u0014\u0010\u000b\u001a\u00020\u0004X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\f\u0010\u0006R\u0014\u0010\r\u001a\u00020\u0004X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u0006\u00a8\u0006\u000f"}, d2 = {"Lcom/navdy/hud/app/view/EngineTemperaturePresenter$Companion;", "", "()V", "TEMPERATURE_GAUGE_COLD_THRESHOLD", "", "getTEMPERATURE_GAUGE_COLD_THRESHOLD", "()D", "TEMPERATURE_GAUGE_HOT_THRESHOLD", "getTEMPERATURE_GAUGE_HOT_THRESHOLD", "TEMPERATURE_GAUGE_LOWER_BOUND_CELSIUS", "getTEMPERATURE_GAUGE_LOWER_BOUND_CELSIUS", "TEMPERATURE_GAUGE_MID_POINT_CELSIUS", "getTEMPERATURE_GAUGE_MID_POINT_CELSIUS", "TEMPERATURE_GAUGE_UPPER_BOUND_CELSIUS", "getTEMPERATURE_GAUGE_UPPER_BOUND_CELSIUS", "app_hudDebug"}, k = 1, mv = {1, 1, 6})
    /* compiled from: EngineTemperaturePresenter.kt */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(kotlin.jvm.internal.DefaultConstructorMarker $constructor_marker) {
            this();
        }

        public final double getTEMPERATURE_GAUGE_LOWER_BOUND_CELSIUS() {
            return com.navdy.hud.app.view.EngineTemperaturePresenter.TEMPERATURE_GAUGE_LOWER_BOUND_CELSIUS;
        }

        public final double getTEMPERATURE_GAUGE_UPPER_BOUND_CELSIUS() {
            return com.navdy.hud.app.view.EngineTemperaturePresenter.TEMPERATURE_GAUGE_UPPER_BOUND_CELSIUS;
        }

        public final double getTEMPERATURE_GAUGE_MID_POINT_CELSIUS() {
            return com.navdy.hud.app.view.EngineTemperaturePresenter.TEMPERATURE_GAUGE_MID_POINT_CELSIUS;
        }

        public final double getTEMPERATURE_GAUGE_COLD_THRESHOLD() {
            return com.navdy.hud.app.view.EngineTemperaturePresenter.TEMPERATURE_GAUGE_COLD_THRESHOLD;
        }

        public final double getTEMPERATURE_GAUGE_HOT_THRESHOLD() {
            return com.navdy.hud.app.view.EngineTemperaturePresenter.TEMPERATURE_GAUGE_HOT_THRESHOLD;
        }
    }

    public EngineTemperaturePresenter(@org.jetbrains.annotations.NotNull android.content.Context context2) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(context2, "context");
        this.context = context2;
        com.navdy.hud.app.view.drawable.EngineTemperatureDrawable engineTemperatureDrawable2 = this.engineTemperatureDrawable;
        if (engineTemperatureDrawable2 != null) {
            engineTemperatureDrawable2.setMinValue((float) Companion.getTEMPERATURE_GAUGE_LOWER_BOUND_CELSIUS());
        }
        com.navdy.hud.app.view.drawable.EngineTemperatureDrawable engineTemperatureDrawable3 = this.engineTemperatureDrawable;
        if (engineTemperatureDrawable3 != null) {
            engineTemperatureDrawable3.setMaxGaugeValue((float) Companion.getTEMPERATURE_GAUGE_UPPER_BOUND_CELSIUS());
        }
        android.content.res.Resources resources = this.context.getResources();
        java.lang.String string = resources.getString(com.navdy.hud.app.R.string.temperature_unit_fahrenheit);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(string, "resources.getString(R.st\u2026perature_unit_fahrenheit)");
        this.fahrenheitLabel = string;
        java.lang.String string2 = resources.getString(com.navdy.hud.app.R.string.temperature_unit_celsius);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(string2, "resources.getString(R.st\u2026temperature_unit_celsius)");
        this.celsiusLabel = string2;
        mortar.Mortar.inject(com.navdy.hud.app.HudApplication.getAppContext(), this);
        java.lang.String string3 = resources.getString(com.navdy.hud.app.R.string.widget_engine_temperature);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(string3, "resources.getString(R.st\u2026idget_engine_temperature)");
        this.widgetNameString = string3;
    }

    @org.jetbrains.annotations.NotNull
    public final android.content.Context getContext() {
        return this.context;
    }

    @org.jetbrains.annotations.Nullable
    public final com.navdy.hud.app.view.drawable.EngineTemperatureDrawable getEngineTemperatureDrawable() {
        return this.engineTemperatureDrawable;
    }

    public final void setEngineTemperatureDrawable(@org.jetbrains.annotations.Nullable com.navdy.hud.app.view.drawable.EngineTemperatureDrawable engineTemperatureDrawable2) {
        this.engineTemperatureDrawable = engineTemperatureDrawable2;
    }

    public final int getSTATE_NORMAL() {
        return this.STATE_NORMAL;
    }

    public final void setSTATE_NORMAL(int i) {
        this.STATE_NORMAL = i;
    }

    public final int getSTATE_COLD() {
        return this.STATE_COLD;
    }

    public final void setSTATE_COLD(int i) {
        this.STATE_COLD = i;
    }

    public final int getSTATE_HOT() {
        return this.STATE_HOT;
    }

    public final void setSTATE_HOT(int i) {
        this.STATE_HOT = i;
    }

    @org.jetbrains.annotations.NotNull
    public final android.widget.TextView getTempText() {
        android.widget.TextView textView = this.tempText;
        if (textView == null) {
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException("tempText");
        }
        return textView;
    }

    public final void setTempText(@org.jetbrains.annotations.NotNull android.widget.TextView textView) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(textView, "<set-?>");
        this.tempText = textView;
    }

    @org.jetbrains.annotations.NotNull
    public final android.widget.TextView getTempUnitText() {
        android.widget.TextView textView = this.tempUnitText;
        if (textView == null) {
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException("tempUnitText");
        }
        return textView;
    }

    public final void setTempUnitText(@org.jetbrains.annotations.NotNull android.widget.TextView textView) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(textView, "<set-?>");
        this.tempUnitText = textView;
    }

    public final double getEngineTemperature() {
        return this.engineTemperature;
    }

    public final void setEngineTemperature(double value) {
        this.engineTemperature = value;
        reDraw();
    }

    @org.jetbrains.annotations.NotNull
    public final com.navdy.hud.app.profile.DriverProfileManager getDriverProfileManager() {
        com.navdy.hud.app.profile.DriverProfileManager driverProfileManager2 = this.driverProfileManager;
        if (driverProfileManager2 == null) {
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException("driverProfileManager");
        }
        return driverProfileManager2;
    }

    public final void setDriverProfileManager(@org.jetbrains.annotations.NotNull com.navdy.hud.app.profile.DriverProfileManager driverProfileManager2) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(driverProfileManager2, "<set-?>");
        this.driverProfileManager = driverProfileManager2;
    }

    @org.jetbrains.annotations.NotNull
    public final com.squareup.otto.Bus getBus() {
        com.squareup.otto.Bus bus2 = this.bus;
        if (bus2 == null) {
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException("bus");
        }
        return bus2;
    }

    public final void setBus(@org.jetbrains.annotations.NotNull com.squareup.otto.Bus bus2) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(bus2, "<set-?>");
        this.bus = bus2;
    }

    @org.jetbrains.annotations.NotNull
    public final java.lang.String getFahrenheitLabel() {
        return this.fahrenheitLabel;
    }

    public final void setFahrenheitLabel(@org.jetbrains.annotations.NotNull java.lang.String str) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(str, "<set-?>");
        this.fahrenheitLabel = str;
    }

    @org.jetbrains.annotations.NotNull
    public final java.lang.String getCelsiusLabel() {
        return this.celsiusLabel;
    }

    public final void setCelsiusLabel(@org.jetbrains.annotations.NotNull java.lang.String str) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(str, "<set-?>");
        this.celsiusLabel = str;
    }

    @org.jetbrains.annotations.NotNull
    public final java.lang.String getWidgetNameString() {
        return this.widgetNameString;
    }

    @org.jetbrains.annotations.Nullable
    public android.graphics.drawable.Drawable getDrawable() {
        return this.engineTemperatureDrawable;
    }

    public void setView(@org.jetbrains.annotations.Nullable com.navdy.hud.app.view.DashboardWidgetView dashboardWidgetView, @org.jetbrains.annotations.Nullable android.os.Bundle arguments) {
        int layoutResourceId = com.navdy.hud.app.R.layout.engine_temp_gauge_left;
        if (arguments != null) {
            switch (arguments.getInt(com.navdy.hud.app.view.DashboardWidgetPresenter.EXTRA_GRAVITY)) {
                case 0:
                    layoutResourceId = com.navdy.hud.app.R.layout.engine_temp_gauge_left;
                    this.mLeftOriented = true;
                    break;
                case 2:
                    layoutResourceId = com.navdy.hud.app.R.layout.engine_temp_gauge_right;
                    this.mLeftOriented = false;
                    break;
            }
        }
        if (dashboardWidgetView != null) {
            dashboardWidgetView.setContentView(layoutResourceId);
            android.view.View findViewById = dashboardWidgetView.findViewById(com.navdy.hud.app.R.id.txt_value);
            if (findViewById == null) {
                throw new kotlin.TypeCastException("null cannot be cast to non-null type android.widget.TextView");
            }
            this.tempText = (android.widget.TextView) findViewById;
            android.view.View findViewById2 = dashboardWidgetView.findViewById(com.navdy.hud.app.R.id.txt_unit);
            if (findViewById2 == null) {
                throw new kotlin.TypeCastException("null cannot be cast to non-null type android.widget.TextView");
            }
            this.tempUnitText = (android.widget.TextView) findViewById2;
        }
        super.setView(dashboardWidgetView, arguments);
        reDraw();
    }

    /* access modifiers changed from: protected */
    public boolean isRegisteringToBusRequired() {
        return true;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0019, code lost:
        if (r7 != null) goto L_0x001b;
     */
    public void updateGauge() {
        com.navdy.service.library.events.preferences.DriverProfilePreferences.UnitSystem unitSystem;
        java.lang.CharSequence charSequence;
        java.lang.CharSequence valueOf;
        if (this.mWidgetView != null) {
            com.navdy.hud.app.profile.DriverProfileManager driverProfileManager2 = this.driverProfileManager;
            if (driverProfileManager2 == null) {
                kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException("driverProfileManager");
            }
            if (driverProfileManager2 != null) {
                com.navdy.hud.app.profile.DriverProfile currentProfile = driverProfileManager2.getCurrentProfile();
                if (currentProfile != null) {
                    unitSystem = currentProfile.getUnitSystem();
                }
            }
            unitSystem = com.navdy.service.library.events.preferences.DriverProfilePreferences.UnitSystem.UNIT_SYSTEM_METRIC;
            com.navdy.hud.app.view.drawable.EngineTemperatureDrawable engineTemperatureDrawable2 = this.engineTemperatureDrawable;
            if (engineTemperatureDrawable2 != null) {
                engineTemperatureDrawable2.setGaugeValue((float) com.navdy.hud.app.ExtensionsKt.clamp(this.engineTemperature, Companion.getTEMPERATURE_GAUGE_COLD_THRESHOLD(), Companion.getTEMPERATURE_GAUGE_UPPER_BOUND_CELSIUS()));
            }
            com.navdy.hud.app.view.drawable.EngineTemperatureDrawable engineTemperatureDrawable3 = this.engineTemperatureDrawable;
            if (engineTemperatureDrawable3 != null) {
                engineTemperatureDrawable3.setLeftOriented(this.mLeftOriented);
            }
            int state = this.STATE_NORMAL;
            if (this.engineTemperature <= Companion.getTEMPERATURE_GAUGE_COLD_THRESHOLD()) {
                state = this.STATE_COLD;
            } else if (this.engineTemperature > Companion.getTEMPERATURE_GAUGE_HOT_THRESHOLD()) {
                state = this.STATE_HOT;
            }
            com.navdy.hud.app.view.drawable.EngineTemperatureDrawable engineTemperatureDrawable4 = this.engineTemperatureDrawable;
            if (engineTemperatureDrawable4 != null) {
                engineTemperatureDrawable4.setState(state);
            }
            android.widget.TextView textView = this.tempText;
            if (textView == null) {
                kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException("tempText");
            }
            if (textView != null) {
                switch (unitSystem) {
                    case UNIT_SYSTEM_METRIC:
                        valueOf = java.lang.String.valueOf((int) this.engineTemperature);
                        break;
                    case UNIT_SYSTEM_IMPERIAL:
                        valueOf = java.lang.String.valueOf((int) com.navdy.hud.app.ExtensionsKt.celsiusToFahrenheit(this.engineTemperature));
                        break;
                    default:
                        throw new kotlin.NoWhenBranchMatchedException();
                }
                textView.setText(valueOf);
            }
            android.widget.TextView textView2 = this.tempUnitText;
            if (textView2 == null) {
                kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException("tempUnitText");
            }
            if (textView2 != null) {
                switch (unitSystem) {
                    case UNIT_SYSTEM_METRIC:
                        charSequence = this.celsiusLabel;
                        break;
                    case UNIT_SYSTEM_IMPERIAL:
                        charSequence = this.fahrenheitLabel;
                        break;
                    default:
                        throw new kotlin.NoWhenBranchMatchedException();
                }
                textView2.setText(charSequence);
            }
        }
    }

    @com.squareup.otto.Subscribe
    public final void onSpeedUnitChanged(@org.jetbrains.annotations.NotNull com.navdy.hud.app.manager.SpeedManager.SpeedUnitChanged speedUnitChangedEvent) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(speedUnitChangedEvent, "speedUnitChangedEvent");
        reDraw();
    }

    @org.jetbrains.annotations.NotNull
    public java.lang.String getWidgetIdentifier() {
        return com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.ENGINE_TEMPERATURE_GAUGE_ID;
    }

    @org.jetbrains.annotations.NotNull
    public java.lang.String getWidgetName() {
        return this.widgetNameString;
    }
}
