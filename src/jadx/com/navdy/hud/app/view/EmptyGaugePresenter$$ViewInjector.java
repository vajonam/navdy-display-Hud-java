package com.navdy.hud.app.view;

public class EmptyGaugePresenter$$ViewInjector {
    public static void inject(butterknife.ButterKnife.Finder finder, com.navdy.hud.app.view.EmptyGaugePresenter target, java.lang.Object source) {
        target.mEmptyGaugeText = (android.widget.TextView) finder.findRequiredView(source, com.navdy.hud.app.R.id.txt_empty_gauge, "field 'mEmptyGaugeText'");
    }

    public static void reset(com.navdy.hud.app.view.EmptyGaugePresenter target) {
        target.mEmptyGaugeText = null;
    }
}
