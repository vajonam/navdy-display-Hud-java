package com.navdy.hud.app.view;

public class DashboardWidgetView$$ViewInjector {
    public static void inject(butterknife.ButterKnife.Finder finder, com.navdy.hud.app.view.DashboardWidgetView target, java.lang.Object source) {
        target.mCustomView = finder.findOptionalView(source, com.navdy.hud.app.R.id.custom_drawable);
    }

    public static void reset(com.navdy.hud.app.view.DashboardWidgetView target) {
        target.mCustomView = null;
    }
}
