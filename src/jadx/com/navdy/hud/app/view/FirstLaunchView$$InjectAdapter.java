package com.navdy.hud.app.view;

public final class FirstLaunchView$$InjectAdapter extends dagger.internal.Binding<com.navdy.hud.app.view.FirstLaunchView> implements dagger.MembersInjector<com.navdy.hud.app.view.FirstLaunchView> {
    private dagger.internal.Binding<com.navdy.hud.app.screen.FirstLaunchScreen.Presenter> presenter;

    public FirstLaunchView$$InjectAdapter() {
        super(null, "members/com.navdy.hud.app.view.FirstLaunchView", false, com.navdy.hud.app.view.FirstLaunchView.class);
    }

    public void attach(dagger.internal.Linker linker) {
        this.presenter = linker.requestBinding("com.navdy.hud.app.screen.FirstLaunchScreen$Presenter", com.navdy.hud.app.view.FirstLaunchView.class, getClass().getClassLoader());
    }

    public void getDependencies(java.util.Set<dagger.internal.Binding<?>> set, java.util.Set<dagger.internal.Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.presenter);
    }

    public void injectMembers(com.navdy.hud.app.view.FirstLaunchView object) {
        object.presenter = (com.navdy.hud.app.screen.FirstLaunchScreen.Presenter) this.presenter.get();
    }
}
