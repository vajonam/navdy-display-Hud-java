package com.navdy.hud.app.view;

public class DialUpdateProgressView$$ViewInjector {
    public static void inject(butterknife.ButterKnife.Finder finder, com.navdy.hud.app.view.DialUpdateProgressView target, java.lang.Object source) {
        target.mProgress = (android.widget.ProgressBar) finder.findRequiredView(source, com.navdy.hud.app.R.id.progress, "field 'mProgress'");
    }

    public static void reset(com.navdy.hud.app.view.DialUpdateProgressView target) {
        target.mProgress = null;
    }
}
