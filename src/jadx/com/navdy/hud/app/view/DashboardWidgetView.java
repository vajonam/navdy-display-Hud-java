package com.navdy.hud.app.view;

public class DashboardWidgetView extends android.widget.RelativeLayout {
    @butterknife.InjectView(2131624159)
    @butterknife.Optional
    protected android.view.View mCustomView;
    private int mLayout;

    public DashboardWidgetView(android.content.Context context) {
        this(context, null);
    }

    public DashboardWidgetView(android.content.Context context, android.util.AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public DashboardWidgetView(android.content.Context context, android.util.AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        android.content.res.TypedArray customAttributes = context.obtainStyledAttributes(attrs, com.navdy.hud.app.R.styleable.GaugeView, defStyleAttr, 0);
        if (customAttributes != null) {
            setContentView(customAttributes.getResourceId(1, com.navdy.hud.app.R.layout.small_gauge_view));
            butterknife.ButterKnife.inject((android.view.View) this);
            customAttributes.recycle();
        }
    }

    public boolean setContentView(android.view.View view) {
        this.mLayout = -1;
        removeAllViews();
        if (view == null) {
            return false;
        }
        addView(view);
        return true;
    }

    public boolean setContentView(int layoutResId) {
        if (layoutResId <= 0 || layoutResId == this.mLayout) {
            return false;
        }
        this.mLayout = layoutResId;
        removeAllViews();
        android.view.LayoutInflater.from(getContext()).inflate(layoutResId, this);
        butterknife.ButterKnife.inject((android.view.View) this);
        return true;
    }

    public android.view.View getCustomView() {
        return this.mCustomView;
    }

    public void clear() {
        if (this.mCustomView != null) {
            this.mCustomView.setBackground(null);
        }
    }
}
