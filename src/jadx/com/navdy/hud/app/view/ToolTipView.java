package com.navdy.hud.app.view;

public class ToolTipView extends android.widget.LinearLayout {
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.view.ToolTipView.class);
    private int iconWidth;
    private int margin;
    private int maxIcons;
    private int maxTextWidth;
    private int minTextWidth;
    private int padding;
    private int sideMargin;
    @butterknife.InjectView(2131624483)
    android.view.ViewGroup toolTipContainer;
    @butterknife.InjectView(2131624485)
    android.view.View toolTipScrim;
    @butterknife.InjectView(2131624484)
    android.widget.TextView toolTipTextView;
    @butterknife.InjectView(2131624486)
    android.view.View toolTipTriangle;
    private int totalWidth;
    private int triangleWidth;
    private android.widget.TextView widthCalcTextView;

    public ToolTipView(android.content.Context context) {
        this(context, null);
    }

    public ToolTipView(android.content.Context context, android.util.AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ToolTipView(android.content.Context context, android.util.AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        android.content.Context context = getContext();
        this.widthCalcTextView = new android.widget.TextView(context);
        this.widthCalcTextView.setTextAppearance(context, com.navdy.hud.app.R.style.tool_tip);
        android.content.res.Resources resources = context.getResources();
        this.maxTextWidth = resources.getDimensionPixelSize(com.navdy.hud.app.R.dimen.tool_tip_max_text_width);
        this.minTextWidth = resources.getDimensionPixelSize(com.navdy.hud.app.R.dimen.tool_tip_min_text_width);
        this.padding = resources.getDimensionPixelSize(com.navdy.hud.app.R.dimen.tool_tip_padding);
        this.sideMargin = resources.getDimensionPixelSize(com.navdy.hud.app.R.dimen.tool_tip_margin);
        this.triangleWidth = resources.getDimensionPixelSize(com.navdy.hud.app.R.dimen.tool_tip_triangle_width);
        setOrientation(1);
        android.view.LayoutInflater.from(context).inflate(com.navdy.hud.app.R.layout.tooltip_lyt, this, true);
        butterknife.ButterKnife.inject((android.view.View) this);
    }

    public void setParams(int iconWidth2, int margin2, int maxIcons2) {
        this.iconWidth = iconWidth2;
        this.margin = margin2;
        this.maxIcons = maxIcons2;
        this.totalWidth = (iconWidth2 * maxIcons2) + ((maxIcons2 - 1) * margin2);
    }

    public void show(int posIndex, java.lang.String text) {
        int textWidth;
        int x;
        android.text.TextPaint textPaint = this.widthCalcTextView.getPaint();
        boolean scrimOn = false;
        if (new android.text.StaticLayout(text, textPaint, this.maxTextWidth, android.text.Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false).getLineCount() == 1) {
            int width = (int) textPaint.measureText(text);
            if (width < this.minTextWidth) {
                textWidth = this.minTextWidth;
            } else {
                textWidth = width;
            }
        } else {
            textWidth = this.maxTextWidth;
            scrimOn = true;
        }
        int textWidth2 = textWidth + (this.padding * 2);
        ((android.view.ViewGroup.MarginLayoutParams) getLayoutParams()).width = textWidth2;
        int triangleMiddlePos = (this.iconWidth * posIndex) + (this.margin * posIndex) + (this.iconWidth / 2);
        int triangleStartPos = triangleMiddlePos - (this.triangleWidth / 2);
        switch (posIndex) {
            case 0:
            case 1:
                if (textWidth2 / 2 <= triangleMiddlePos - this.sideMargin) {
                    x = triangleMiddlePos - (textWidth2 / 2);
                    break;
                } else {
                    x = this.sideMargin;
                    break;
                }
            case 2:
            case 3:
                int distanceRight = (this.totalWidth - triangleMiddlePos) - this.sideMargin;
                if (textWidth2 / 2 <= distanceRight) {
                    x = triangleMiddlePos - (textWidth2 / 2);
                    break;
                } else {
                    x = triangleMiddlePos - (textWidth2 - distanceRight);
                    break;
                }
            default:
                x = 0;
                break;
        }
        ((android.view.ViewGroup.MarginLayoutParams) this.toolTipTriangle.getLayoutParams()).leftMargin = triangleStartPos - x;
        this.toolTipTextView.setText(text);
        this.toolTipTextView.requestLayout();
        if (scrimOn) {
            this.toolTipScrim.setVisibility(0);
        } else {
            this.toolTipScrim.setVisibility(8);
        }
        setX((float) x);
        requestLayout();
        setVisibility(0);
    }

    public void hide() {
        setVisibility(8);
    }
}
