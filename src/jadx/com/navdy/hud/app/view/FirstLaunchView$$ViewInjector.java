package com.navdy.hud.app.view;

public class FirstLaunchView$$ViewInjector {
    public static void inject(butterknife.ButterKnife.Finder finder, com.navdy.hud.app.view.FirstLaunchView target, java.lang.Object source) {
        target.firstLaunchLogo = (android.widget.ImageView) finder.findRequiredView(source, com.navdy.hud.app.R.id.firstLaunchLogo, "field 'firstLaunchLogo'");
    }

    public static void reset(com.navdy.hud.app.view.FirstLaunchView target) {
        target.firstLaunchLogo = null;
    }
}
