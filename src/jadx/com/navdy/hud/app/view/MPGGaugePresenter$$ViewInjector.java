package com.navdy.hud.app.view;

public class MPGGaugePresenter$$ViewInjector {
    public static void inject(butterknife.ButterKnife.Finder finder, com.navdy.hud.app.view.MPGGaugePresenter target, java.lang.Object source) {
        target.mFuelConsumptionUnit = (android.widget.TextView) finder.findRequiredView(source, com.navdy.hud.app.R.id.txt_unit, "field 'mFuelConsumptionUnit'");
        target.mFuelConsumptionValue = (android.widget.TextView) finder.findRequiredView(source, com.navdy.hud.app.R.id.txt_value, "field 'mFuelConsumptionValue'");
    }

    public static void reset(com.navdy.hud.app.view.MPGGaugePresenter target) {
        target.mFuelConsumptionUnit = null;
        target.mFuelConsumptionValue = null;
    }
}
