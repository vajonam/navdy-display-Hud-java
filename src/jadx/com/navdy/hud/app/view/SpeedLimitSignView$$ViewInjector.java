package com.navdy.hud.app.view;

public class SpeedLimitSignView$$ViewInjector {
    public static void inject(butterknife.ButterKnife.Finder finder, com.navdy.hud.app.view.SpeedLimitSignView target, java.lang.Object source) {
        target.usSpeedLimitSignView = (android.view.ViewGroup) finder.findRequiredView(source, com.navdy.hud.app.R.id.speed_limit_sign_us, "field 'usSpeedLimitSignView'");
        target.euSpeedLimitSignView = (android.view.ViewGroup) finder.findRequiredView(source, com.navdy.hud.app.R.id.speed_limit_sign_eu, "field 'euSpeedLimitSignView'");
    }

    public static void reset(com.navdy.hud.app.view.SpeedLimitSignView target) {
        target.usSpeedLimitSignView = null;
        target.euSpeedLimitSignView = null;
    }
}
