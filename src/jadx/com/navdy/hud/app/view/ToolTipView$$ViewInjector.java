package com.navdy.hud.app.view;

public class ToolTipView$$ViewInjector {
    public static void inject(butterknife.ButterKnife.Finder finder, com.navdy.hud.app.view.ToolTipView target, java.lang.Object source) {
        target.toolTipContainer = (android.view.ViewGroup) finder.findRequiredView(source, com.navdy.hud.app.R.id.tooltip_container, "field 'toolTipContainer'");
        target.toolTipTextView = (android.widget.TextView) finder.findRequiredView(source, com.navdy.hud.app.R.id.tooltip_text, "field 'toolTipTextView'");
        target.toolTipScrim = finder.findRequiredView(source, com.navdy.hud.app.R.id.tooltip_scrim, "field 'toolTipScrim'");
        target.toolTipTriangle = finder.findRequiredView(source, com.navdy.hud.app.R.id.tooltip_triangle, "field 'toolTipTriangle'");
    }

    public static void reset(com.navdy.hud.app.view.ToolTipView target) {
        target.toolTipContainer = null;
        target.toolTipTextView = null;
        target.toolTipScrim = null;
        target.toolTipTriangle = null;
    }
}
