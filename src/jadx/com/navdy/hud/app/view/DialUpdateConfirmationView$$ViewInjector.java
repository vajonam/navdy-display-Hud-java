package com.navdy.hud.app.view;

public class DialUpdateConfirmationView$$ViewInjector {
    public static void inject(butterknife.ButterKnife.Finder finder, com.navdy.hud.app.view.DialUpdateConfirmationView target, java.lang.Object source) {
        target.mScreenTitleText = (android.widget.TextView) finder.findRequiredView(source, com.navdy.hud.app.R.id.mainTitle, "field 'mScreenTitleText'");
        target.mMainTitleText = (android.widget.TextView) finder.findRequiredView(source, com.navdy.hud.app.R.id.title2, "field 'mMainTitleText'");
        target.mInfoText = (android.widget.TextView) finder.findRequiredView(source, com.navdy.hud.app.R.id.title3, "field 'mInfoText'");
        target.mSummaryText = (android.widget.TextView) finder.findRequiredView(source, com.navdy.hud.app.R.id.title4, "field 'mSummaryText'");
        target.mChoiceLayout = (com.navdy.hud.app.ui.component.ChoiceLayout) finder.findRequiredView(source, com.navdy.hud.app.R.id.choiceLayout, "field 'mChoiceLayout'");
        target.mIcon = (android.widget.ImageView) finder.findRequiredView(source, com.navdy.hud.app.R.id.image, "field 'mIcon'");
        target.mRightSwipe = (android.widget.ImageView) finder.findRequiredView(source, com.navdy.hud.app.R.id.rightSwipe, "field 'mRightSwipe'");
        target.mLefttSwipe = (android.widget.ImageView) finder.findRequiredView(source, com.navdy.hud.app.R.id.leftSwipe, "field 'mLefttSwipe'");
        target.mainSection = (android.widget.RelativeLayout) finder.findRequiredView(source, com.navdy.hud.app.R.id.mainSection, "field 'mainSection'");
    }

    public static void reset(com.navdy.hud.app.view.DialUpdateConfirmationView target) {
        target.mScreenTitleText = null;
        target.mMainTitleText = null;
        target.mInfoText = null;
        target.mSummaryText = null;
        target.mChoiceLayout = null;
        target.mIcon = null;
        target.mRightSwipe = null;
        target.mLefttSwipe = null;
        target.mainSection = null;
    }
}
