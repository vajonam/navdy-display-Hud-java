package com.navdy.hud.app.view.drawable;

public class SpeedoMeterGaugeDrawable2 extends com.navdy.hud.app.view.drawable.GaugeDrawable {
    private static final float GAUGE_START_ANGLE = 146.2f;
    private static final float SPEED_LIMIT_MARKER_ANGLE_WIDTH = 4.0f;
    public static final int TEXT_MARGIN = 15;
    private int mGaugeMargin;
    private int mOuterRingColor;
    private int mSpeedLimit;
    private int mSpeedLimitMarkerColor;
    private int mSpeedLimitTextColor;
    private int mSpeedLimitTextSize;
    private int mValueRingWidth;
    public int speedLimitTextAlpha = 0;

    public SpeedoMeterGaugeDrawable2(android.content.Context context, int backgroundResource, int stateColorsResId, int valueRingWidth) {
        super(context, backgroundResource, stateColorsResId);
        this.mValueRingWidth = context.getResources().getDimensionPixelSize(valueRingWidth);
        this.mGaugeMargin = context.getResources().getDimensionPixelSize(com.navdy.hud.app.R.dimen.speedometer_gauge_margin);
        this.mSpeedLimitTextColor = context.getResources().getColor(17170443);
        this.mOuterRingColor = this.mColorTable[3];
        this.mSpeedLimitMarkerColor = this.mColorTable[4];
        this.mSpeedLimitTextSize = context.getResources().getDimensionPixelSize(com.navdy.hud.app.R.dimen.speedometer_gauge_speed_limit_text_size);
    }

    public void setSpeedLimit(int mSpeedLimit2) {
        this.mSpeedLimit = mSpeedLimit2;
    }

    public void setState(int index) {
        super.setState(index);
        switch (index) {
            case 0:
                this.mSpeedLimitMarkerColor = this.mColorTable[1];
                return;
            case 1:
                this.mSpeedLimitMarkerColor = this.mColorTable[4];
                return;
            case 2:
                this.mSpeedLimitMarkerColor = this.mColorTable[4];
                return;
            default:
                return;
        }
    }

    public void draw(android.graphics.Canvas canvas) {
        super.draw(canvas);
        android.graphics.Rect bounds = getBounds();
        int innerRingInset = this.mValueRingWidth / 2;
        android.graphics.RectF oval = new android.graphics.RectF((float) (bounds.left + innerRingInset), (float) (bounds.top + innerRingInset), (float) (bounds.right - innerRingInset), (float) ((bounds.top + bounds.width()) - innerRingInset));
        oval.inset((float) this.mGaugeMargin, (float) this.mGaugeMargin);
        this.mPaint.setStrokeWidth((float) this.mValueRingWidth);
        this.mPaint.setColor(this.mOuterRingColor);
        this.mPaint.setStyle(android.graphics.Paint.Style.STROKE);
        canvas.drawArc(oval, GAUGE_START_ANGLE, 247.6f, false, this.mPaint);
        this.mPaint.setStrokeWidth((float) this.mValueRingWidth);
        this.mPaint.setStyle(android.graphics.Paint.Style.STROKE);
        this.mPaint.setColor(this.mDefaultColor);
        canvas.drawArc(oval, GAUGE_START_ANGLE, (this.mValue / this.mMaxValue) * 247.6f, false, this.mPaint);
        if (this.mSpeedLimit > 0) {
            this.mPaint.setColor(this.mSpeedLimitMarkerColor);
            float speedLimitMarkerAngle = ((GAUGE_START_ANGLE + ((((float) this.mSpeedLimit) / this.mMaxValue) * 247.6f)) - 2.0f) % 360.0f;
            canvas.drawArc(oval, speedLimitMarkerAngle, SPEED_LIMIT_MARKER_ANGLE_WIDTH, false, this.mPaint);
            android.graphics.RectF rectF = new android.graphics.RectF(bounds);
            rectF.inset(15.0f, 15.0f);
            int radius = (int) (rectF.width() / 2.0f);
            int alpha = this.mPaint.getAlpha();
            this.mPaint.setStyle(android.graphics.Paint.Style.FILL);
            this.mPaint.setColor(this.mSpeedLimitTextColor);
            this.mPaint.setTextSize((float) this.mSpeedLimitTextSize);
            this.mPaint.setAlpha(this.speedLimitTextAlpha);
            this.mPaint.setTypeface(android.graphics.Typeface.create(android.graphics.Typeface.DEFAULT, 1));
            java.lang.String text = java.lang.Integer.toString(this.mSpeedLimit);
            android.graphics.Rect textBounds = new android.graphics.Rect();
            this.mPaint.getTextBounds(text, 0, text.length(), textBounds);
            int y = (int) (((double) rectF.centerY()) + (((double) radius) * java.lang.Math.sin(java.lang.Math.toRadians((double) speedLimitMarkerAngle))));
            canvas.drawText(text, (float) (((int) (((double) rectF.centerX()) + (((double) radius) * java.lang.Math.cos(java.lang.Math.toRadians((double) speedLimitMarkerAngle))))) - (textBounds.width() / 2)), (float) ((textBounds.height() / 2) + y), this.mPaint);
            this.mPaint.setAlpha(alpha);
        }
    }
}
