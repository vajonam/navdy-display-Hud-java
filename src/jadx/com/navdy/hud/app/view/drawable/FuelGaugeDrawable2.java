package com.navdy.hud.app.view.drawable;

public class FuelGaugeDrawable2 extends com.navdy.hud.app.view.drawable.GaugeDrawable {
    private static final int TICKS_COUNT = 4;
    private static final int TICK_ANGLE_WIDTH = 2;
    private int mBackgroundColor;
    private int mFuelGaugeWidth;
    private boolean mLeftOriented = true;

    public FuelGaugeDrawable2(android.content.Context context, int stateColorTable) {
        super(context, 0, stateColorTable);
        this.mFuelGaugeWidth = context.getResources().getDimensionPixelSize(com.navdy.hud.app.R.dimen.fuel_gauge_width);
        this.mBackgroundColor = this.mColorTable[3];
    }

    public void setLeftOriented(boolean leftOriented) {
        this.mLeftOriented = leftOriented;
    }

    public void draw(android.graphics.Canvas canvas) {
        android.graphics.RectF fuelGaugeArcBounds;
        super.draw(canvas);
        android.graphics.Rect bounds = getBounds();
        if (this.mLeftOriented) {
            fuelGaugeArcBounds = new android.graphics.RectF((float) bounds.left, (float) bounds.top, (float) (bounds.right + bounds.width()), (float) bounds.bottom);
        } else {
            fuelGaugeArcBounds = new android.graphics.RectF((float) (bounds.left - bounds.width()), (float) bounds.top, (float) bounds.right, (float) bounds.bottom);
        }
        fuelGaugeArcBounds.inset((float) ((this.mFuelGaugeWidth / 2) + 1), (float) ((this.mFuelGaugeWidth / 2) + 1));
        this.mPaint.setStyle(android.graphics.Paint.Style.STROKE);
        this.mPaint.setStrokeWidth((float) this.mFuelGaugeWidth);
        this.mPaint.setColor(this.mBackgroundColor);
        int startAngle = this.mLeftOriented ? 90 : 270;
        int i = startAngle + com.navdy.hud.app.bluetooth.obex.ResponseCodes.OBEX_HTTP_NOT_MODIFIED;
        canvas.drawArc(fuelGaugeArcBounds, (float) startAngle, (float) com.navdy.hud.app.bluetooth.obex.ResponseCodes.OBEX_HTTP_NOT_MODIFIED, false, this.mPaint);
        this.mPaint.setColor(this.mDefaultColor);
        int valueSweepAngle = (int) ((this.mValue / this.mMaxValue) * ((float) com.navdy.hud.app.bluetooth.obex.ResponseCodes.OBEX_HTTP_NOT_MODIFIED));
        canvas.drawArc(fuelGaugeArcBounds, (float) (this.mLeftOriented ? 90 : 450 - valueSweepAngle), (float) valueSweepAngle, false, this.mPaint);
        int i2 = com.navdy.hud.app.bluetooth.obex.ResponseCodes.OBEX_HTTP_NOT_MODIFIED / 4;
        this.mPaint.setColor(-16777216);
        for (int i3 = 1; i3 < 4; i3++) {
            canvas.drawArc(fuelGaugeArcBounds, (float) (((i3 * 45) + startAngle) % 360), 2.0f, false, this.mPaint);
        }
    }
}
