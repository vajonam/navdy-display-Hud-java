package com.navdy.hud.app.view.drawable;

public class SmallAnalogClockDrawable extends com.navdy.hud.app.view.drawable.CustomDrawable {
    private int frameColor;
    private int frameStrokeWidth;
    private int hour;
    private int hourHandColor;
    private int hourHandStrokeWidth;
    private int minute;
    private int minuteHandColor = -1;
    private int minuteHandStrokeWidth;
    private int seconds;

    public SmallAnalogClockDrawable(android.content.Context context) {
        this.frameColor = context.getResources().getColor(com.navdy.hud.app.R.color.small_analog_clock_frame_color);
        this.frameStrokeWidth = context.getResources().getDimensionPixelSize(com.navdy.hud.app.R.dimen.small_analog_clock_frame_stroke_width);
        this.hourHandColor = context.getResources().getColor(com.navdy.hud.app.R.color.cyan);
        this.hourHandStrokeWidth = context.getResources().getDimensionPixelSize(com.navdy.hud.app.R.dimen.small_analog_clock_hour_hand_width);
        this.minuteHandStrokeWidth = context.getResources().getDimensionPixelSize(com.navdy.hud.app.R.dimen.small_analog_clock_minute_hand_width);
    }

    public void setTime(int hour2, int minute2, int seconds2) {
        this.hour = hour2;
        this.minute = minute2;
        this.seconds = seconds2;
    }

    public void draw(android.graphics.Canvas canvas) {
        super.draw(canvas);
        android.graphics.Rect bounds = getBounds();
        new android.graphics.RectF(bounds);
        this.mPaint.setAntiAlias(true);
        this.mPaint.setStyle(android.graphics.Paint.Style.STROKE);
        this.mPaint.setStrokeWidth((float) this.frameStrokeWidth);
        this.mPaint.setColor(this.frameColor);
        android.graphics.RectF frameBounds = new android.graphics.RectF(bounds);
        frameBounds.inset(((float) (this.frameStrokeWidth / 2)) + 0.5f, ((float) (this.frameStrokeWidth / 2)) + 0.5f);
        canvas.drawArc(frameBounds, 0.0f, 360.0f, true, this.mPaint);
        this.mPaint.setStyle(android.graphics.Paint.Style.STROKE);
        this.mPaint.setColor(this.hourHandColor);
        this.mPaint.setStrokeCap(android.graphics.Paint.Cap.ROUND);
        this.mPaint.setStrokeWidth((float) this.hourHandStrokeWidth);
        float hourAngle = com.navdy.hud.app.util.DateUtil.getClockAngleForHour(this.hour, this.minute);
        float radius = (float) ((bounds.width() / 2) - 7);
        android.graphics.Canvas canvas2 = canvas;
        canvas2.drawLine((float) bounds.centerX(), (float) bounds.centerY(), (float) ((int) (((double) bounds.centerX()) + (((double) radius) * java.lang.Math.cos(java.lang.Math.toRadians((double) hourAngle))))), (float) ((int) (((double) bounds.centerY()) + (((double) radius) * java.lang.Math.sin(java.lang.Math.toRadians((double) hourAngle))))), this.mPaint);
        this.mPaint.setStyle(android.graphics.Paint.Style.STROKE);
        this.mPaint.setColor(this.minuteHandColor);
        this.mPaint.setStrokeCap(android.graphics.Paint.Cap.ROUND);
        this.mPaint.setStrokeWidth((float) this.minuteHandStrokeWidth);
        float minuteAngle = com.navdy.hud.app.util.DateUtil.getClockAngleForMinutes(this.minute);
        float radius2 = (float) ((bounds.width() / 2) - 6);
        android.graphics.Canvas canvas3 = canvas;
        canvas3.drawLine((float) bounds.centerX(), (float) bounds.centerY(), (float) ((int) (((double) bounds.centerX()) + (((double) radius2) * java.lang.Math.cos(java.lang.Math.toRadians((double) minuteAngle))))), (float) ((int) (((double) bounds.centerY()) + (((double) radius2) * java.lang.Math.sin(java.lang.Math.toRadians((double) minuteAngle))))), this.mPaint);
    }
}
