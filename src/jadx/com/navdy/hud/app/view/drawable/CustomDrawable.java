package com.navdy.hud.app.view.drawable;

public class CustomDrawable extends android.graphics.drawable.Drawable {
    protected android.graphics.Paint mPaint = new android.graphics.Paint();

    public void draw(android.graphics.Canvas canvas) {
    }

    public void setAlpha(int alpha) {
        this.mPaint.setAlpha(alpha);
    }

    public void setColorFilter(android.graphics.ColorFilter cf) {
        this.mPaint.setColorFilter(cf);
    }

    public int getOpacity() {
        return -3;
    }
}
