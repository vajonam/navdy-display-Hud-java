package com.navdy.hud.app.view.drawable;

public class ETAProgressDrawable extends com.navdy.hud.app.view.drawable.GaugeDrawable {
    private int backgroundColor;
    private int foreGroundColor;
    private android.graphics.drawable.Drawable progressDrawable;
    private int verticalMargin;

    public ETAProgressDrawable(android.content.Context context) {
        super(context, 0);
        android.content.res.Resources res = context.getResources();
        this.backgroundColor = res.getColor(com.navdy.hud.app.R.color.cyan);
        this.foreGroundColor = res.getColor(com.navdy.hud.app.R.color.grey_4a);
        this.progressDrawable = res.getDrawable(com.navdy.hud.app.R.drawable.trip_progress_point_indicator);
        this.verticalMargin = res.getDimensionPixelSize(com.navdy.hud.app.R.dimen.eta_progress_vertical_margin);
    }

    public void draw(android.graphics.Canvas canvas) {
        super.draw(canvas);
        android.graphics.Rect bounds = getBounds();
        float height = (float) bounds.height();
        float width = (float) bounds.width();
        android.graphics.Rect progressBounds = new android.graphics.Rect(bounds);
        progressBounds.inset(0, this.verticalMargin);
        this.mPaint.setColor(this.backgroundColor);
        this.mPaint.setStyle(android.graphics.Paint.Style.FILL);
        canvas.drawRect((float) progressBounds.left, (float) progressBounds.top, (float) progressBounds.right, (float) progressBounds.bottom, this.mPaint);
        int widthFraction = (int) (width * (this.mValue / (this.mMaxValue - this.mMinValue)));
        this.mPaint.setColor(this.foreGroundColor);
        canvas.drawRect((float) progressBounds.left, (float) progressBounds.top, (float) (progressBounds.left + widthFraction), (float) progressBounds.bottom, this.mPaint);
        float imageWidth = height;
        int imageLeft = (int) java.lang.Math.min((float) (progressBounds.left + widthFraction), width - imageWidth);
        this.progressDrawable.setBounds(imageLeft, bounds.top, (int) (((float) imageLeft) + imageWidth), bounds.bottom);
        this.progressDrawable.draw(canvas);
    }
}
