package com.navdy.hud.app.view.drawable;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0017\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0012\u0010\u000b\u001a\u00020\f2\b\u0010\r\u001a\u0004\u0018\u00010\u000eH\u0016J\u000e\u0010\u000f\u001a\u00020\f2\u0006\u0010\u0010\u001a\u00020\nR\u000e\u0010\u0007\u001a\u00020\u0005X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0005X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0011"}, d2 = {"Lcom/navdy/hud/app/view/drawable/EngineTemperatureDrawable;", "Lcom/navdy/hud/app/view/drawable/GaugeDrawable;", "context", "Landroid/content/Context;", "stateColorsResId", "", "(Landroid/content/Context;I)V", "mBackgroundColor", "mFuelGaugeWidth", "mLeftOriented", "", "draw", "", "canvas", "Landroid/graphics/Canvas;", "setLeftOriented", "leftOriented", "app_hudDebug"}, k = 1, mv = {1, 1, 6})
/* compiled from: EngineTemperatureDrawable.kt */
public final class EngineTemperatureDrawable extends com.navdy.hud.app.view.drawable.GaugeDrawable {
    private int mBackgroundColor;
    private int mFuelGaugeWidth;
    private boolean mLeftOriented = true;

    public EngineTemperatureDrawable(@org.jetbrains.annotations.NotNull android.content.Context context, int stateColorsResId) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(context, "context");
        super(context, 0, stateColorsResId);
        this.mFuelGaugeWidth = context.getResources().getDimensionPixelSize(com.navdy.hud.app.R.dimen.fuel_gauge_width);
        this.mBackgroundColor = this.mColorTable[3];
    }

    public final void setLeftOriented(boolean leftOriented) {
        this.mLeftOriented = leftOriented;
    }

    public void draw(@org.jetbrains.annotations.Nullable android.graphics.Canvas canvas) {
        android.graphics.RectF fuelGaugeArcBounds;
        int valueStartAngle = 90;
        super.draw(canvas);
        android.graphics.Rect bounds = getBounds();
        if (this.mLeftOriented) {
            fuelGaugeArcBounds = new android.graphics.RectF((float) bounds.left, (float) bounds.top, (float) (bounds.right + bounds.width()), (float) bounds.bottom);
        } else {
            fuelGaugeArcBounds = new android.graphics.RectF((float) (bounds.left - bounds.width()), (float) bounds.top, (float) bounds.right, (float) bounds.bottom);
        }
        fuelGaugeArcBounds.inset((float) ((this.mFuelGaugeWidth / 2) + 1), (float) ((this.mFuelGaugeWidth / 2) + 1));
        this.mPaint.setStyle(android.graphics.Paint.Style.STROKE);
        this.mPaint.setStrokeWidth((float) this.mFuelGaugeWidth);
        this.mPaint.setColor(this.mBackgroundColor);
        int startAngle = this.mLeftOriented ? 90 : 270;
        int i = startAngle + com.navdy.hud.app.bluetooth.obex.ResponseCodes.OBEX_HTTP_NOT_MODIFIED;
        if (canvas != null) {
            canvas.drawArc(fuelGaugeArcBounds, (float) startAngle, (float) com.navdy.hud.app.bluetooth.obex.ResponseCodes.OBEX_HTTP_NOT_MODIFIED, false, this.mPaint);
        }
        this.mPaint.setColor(this.mDefaultColor);
        int valueSweepAngle = (int) (((this.mValue - this.mMinValue) / (this.mMaxValue - this.mMinValue)) * ((float) com.navdy.hud.app.bluetooth.obex.ResponseCodes.OBEX_HTTP_NOT_MODIFIED));
        if (!this.mLeftOriented) {
            valueStartAngle = 450 - valueSweepAngle;
        }
        boolean z = this.mLeftOriented;
        if (z) {
            if (canvas != null) {
                canvas.drawArc(fuelGaugeArcBounds, (float) valueStartAngle, ((float) valueSweepAngle) - 5.0f, false, this.mPaint);
            }
        } else if (!z && canvas != null) {
            canvas.drawArc(fuelGaugeArcBounds, ((float) valueStartAngle) + 5.0f, (float) valueSweepAngle, false, this.mPaint);
        }
        this.mPaint.setColor(-16777216);
        boolean z2 = this.mLeftOriented;
        if (z2) {
            if (canvas != null) {
                canvas.drawArc(fuelGaugeArcBounds, ((float) valueSweepAngle) + ((float) valueStartAngle), -5.0f, false, this.mPaint);
            }
        } else if (!z2 && canvas != null) {
            canvas.drawArc(fuelGaugeArcBounds, (float) valueStartAngle, 5.0f, false, this.mPaint);
        }
    }
}
