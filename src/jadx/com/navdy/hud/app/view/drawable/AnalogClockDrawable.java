package com.navdy.hud.app.view.drawable;

public class AnalogClockDrawable extends com.navdy.hud.app.view.drawable.CustomDrawable {
    private int centerPointWidth;
    private int dateTextMargin;
    private int dateTextSize;
    private int dayOfMonth;
    private int frameColor;
    private int hour;
    private int hourHandColor;
    private float hourHandLengthFraction = 0.75f;
    private int hourHandStrokeWidth;
    private int minute;
    private int minuteHandColor;
    private float minuteHandLengthFraction = 0.8f;
    private int minuteHandStrokeWidth;
    private int seconds;

    public AnalogClockDrawable(android.content.Context context) {
        this.frameColor = context.getResources().getColor(com.navdy.hud.app.R.color.analog_clock_frame_color);
        this.hourHandColor = context.getResources().getColor(com.navdy.hud.app.R.color.cyan);
        this.hourHandStrokeWidth = context.getResources().getDimensionPixelSize(com.navdy.hud.app.R.dimen.analog_clock_hour_hand_width);
        this.minuteHandStrokeWidth = context.getResources().getDimensionPixelSize(com.navdy.hud.app.R.dimen.analog_clock_minute_hand_width);
        this.centerPointWidth = context.getResources().getDimensionPixelSize(com.navdy.hud.app.R.dimen.analog_clock_center_point_width);
        this.minuteHandColor = -1;
        this.dateTextSize = context.getResources().getDimensionPixelSize(com.navdy.hud.app.R.dimen.analog_clock_date_text_size);
        this.dateTextMargin = context.getResources().getDimensionPixelSize(com.navdy.hud.app.R.dimen.analog_clock_date_text_margin);
    }

    public void setTime(int day, int hour2, int minute2, int seconds2) {
        this.dayOfMonth = day;
        this.hour = hour2;
        this.minute = minute2;
        this.seconds = seconds2;
    }

    public void draw(android.graphics.Canvas canvas) {
        super.draw(canvas);
        android.graphics.Rect bounds = getBounds();
        android.graphics.RectF boundsRectF = new android.graphics.RectF(bounds);
        this.mPaint.setAntiAlias(true);
        this.mPaint.setStyle(android.graphics.Paint.Style.FILL);
        this.mPaint.setColor(this.frameColor);
        canvas.drawArc(boundsRectF, 0.0f, 360.0f, true, this.mPaint);
        this.mPaint.setStyle(android.graphics.Paint.Style.STROKE);
        this.mPaint.setColor(this.hourHandColor);
        this.mPaint.setStrokeCap(android.graphics.Paint.Cap.ROUND);
        this.mPaint.setStrokeWidth((float) this.hourHandStrokeWidth);
        this.mPaint.setShadowLayer(10.0f, 10.0f, 10.0f, -16777216);
        float hourAngle = com.navdy.hud.app.util.DateUtil.getClockAngleForHour(this.hour, this.minute);
        float radius = (float) ((bounds.width() / 2) - 15);
        android.graphics.Canvas canvas2 = canvas;
        canvas2.drawLine((float) bounds.centerX(), (float) bounds.centerY(), (float) ((int) (((double) bounds.centerX()) + (((double) radius) * java.lang.Math.cos(java.lang.Math.toRadians((double) hourAngle))))), (float) ((int) (((double) bounds.centerY()) + (((double) radius) * java.lang.Math.sin(java.lang.Math.toRadians((double) hourAngle))))), this.mPaint);
        this.mPaint.setShadowLayer(0.0f, 0.0f, 0.0f, this.mPaint.getColor());
        this.mPaint.setStyle(android.graphics.Paint.Style.STROKE);
        this.mPaint.setColor(this.minuteHandColor);
        this.mPaint.setStrokeCap(android.graphics.Paint.Cap.ROUND);
        this.mPaint.setStrokeWidth((float) this.minuteHandStrokeWidth);
        float minuteAngle = com.navdy.hud.app.util.DateUtil.getClockAngleForMinutes(this.minute);
        float radius2 = (float) ((bounds.width() / 2) - 10);
        android.graphics.Canvas canvas3 = canvas;
        canvas3.drawLine((float) bounds.centerX(), (float) bounds.centerY(), (float) ((int) (((double) bounds.centerX()) + (((double) radius2) * java.lang.Math.cos(java.lang.Math.toRadians((double) minuteAngle))))), (float) ((int) (((double) bounds.centerY()) + (((double) radius2) * java.lang.Math.sin(java.lang.Math.toRadians((double) minuteAngle))))), this.mPaint);
        this.mPaint.setStyle(android.graphics.Paint.Style.FILL);
        this.mPaint.setStrokeWidth(1.0f);
        this.mPaint.setColor(this.minuteHandColor);
        canvas.drawArc((float) (bounds.centerX() - this.centerPointWidth), (float) (bounds.centerY() - this.centerPointWidth), (float) (bounds.centerX() + this.centerPointWidth), (float) (bounds.centerY() + this.centerPointWidth), 0.0f, 360.0f, true, this.mPaint);
        float radius3 = (float) ((bounds.width() / 2) - this.dateTextMargin);
        this.mPaint.setStyle(android.graphics.Paint.Style.FILL);
        this.mPaint.setTypeface(android.graphics.Typeface.create(android.graphics.Typeface.DEFAULT, 1));
        this.mPaint.setTextSize((float) this.dateTextSize);
        java.lang.String text = java.lang.Integer.toString(this.dayOfMonth);
        android.graphics.Rect rect = new android.graphics.Rect();
        this.mPaint.getTextBounds(text, 0, text.length(), rect);
        int hour12Format = this.hour % 12;
        if ((hour12Format < 7 || hour12Format >= 11) && (this.minute < 35 || this.minute >= 55)) {
            canvas.drawText(text, (float) ((int) (((double) bounds.centerX()) + (((double) radius3) * java.lang.Math.cos(java.lang.Math.toRadians((double) 180.0f))))), (float) ((rect.height() / 2) + ((int) (((double) bounds.centerY()) + (((double) radius3) * java.lang.Math.sin(java.lang.Math.toRadians((double) 180.0f)))))), this.mPaint);
        } else if ((hour12Format < 1 || hour12Format >= 4) && (this.minute < 5 || this.minute >= 20)) {
            canvas.drawText(text, (float) (((int) (((double) bounds.centerX()) + (((double) radius3) * java.lang.Math.cos(java.lang.Math.toRadians((double) 0.0f))))) - rect.width()), (float) ((rect.height() / 2) + ((int) (((double) bounds.centerY()) + (((double) radius3) * java.lang.Math.sin(java.lang.Math.toRadians((double) 0.0f)))))), this.mPaint);
        } else if (hour12Format >= 4 && hour12Format < 7) {
        } else {
            if (this.minute < 20 || this.minute >= 35) {
                canvas.drawText(text, (float) (((int) (((double) bounds.centerX()) + (((double) radius3) * java.lang.Math.cos(java.lang.Math.toRadians((double) 90.0f))))) - (rect.width() / 2)), (float) ((int) (((double) bounds.centerY()) + (((double) radius3) * java.lang.Math.sin(java.lang.Math.toRadians((double) 90.0f))))), this.mPaint);
            }
        }
    }
}
