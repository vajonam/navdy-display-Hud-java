package com.navdy.hud.app.view;

public final class NotificationView$$InjectAdapter extends dagger.internal.Binding<com.navdy.hud.app.view.NotificationView> implements dagger.MembersInjector<com.navdy.hud.app.view.NotificationView> {
    private dagger.internal.Binding<com.navdy.hud.app.presenter.NotificationPresenter> presenter;

    public NotificationView$$InjectAdapter() {
        super(null, "members/com.navdy.hud.app.view.NotificationView", false, com.navdy.hud.app.view.NotificationView.class);
    }

    public void attach(dagger.internal.Linker linker) {
        this.presenter = linker.requestBinding("com.navdy.hud.app.presenter.NotificationPresenter", com.navdy.hud.app.view.NotificationView.class, getClass().getClassLoader());
    }

    public void getDependencies(java.util.Set<dagger.internal.Binding<?>> set, java.util.Set<dagger.internal.Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.presenter);
    }

    public void injectMembers(com.navdy.hud.app.view.NotificationView object) {
        object.presenter = (com.navdy.hud.app.presenter.NotificationPresenter) this.presenter.get();
    }
}
