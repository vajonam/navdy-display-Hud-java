package com.navdy.hud.app.view;

public class ForceUpdateView$$ViewInjector {
    public static void inject(butterknife.ButterKnife.Finder finder, com.navdy.hud.app.view.ForceUpdateView target, java.lang.Object source) {
        target.mScreenTitleText = (android.widget.TextView) finder.findRequiredView(source, com.navdy.hud.app.R.id.mainTitle, "field 'mScreenTitleText'");
        target.mTextView1 = (android.widget.TextView) finder.findRequiredView(source, com.navdy.hud.app.R.id.title1, "field 'mTextView1'");
        target.mTextView2 = (android.widget.TextView) finder.findRequiredView(source, com.navdy.hud.app.R.id.title2, "field 'mTextView2'");
        target.mChoiceLayout = (com.navdy.hud.app.ui.component.ChoiceLayout) finder.findRequiredView(source, com.navdy.hud.app.R.id.choiceLayout, "field 'mChoiceLayout'");
        target.mTextView3 = (android.widget.TextView) finder.findRequiredView(source, com.navdy.hud.app.R.id.title3, "field 'mTextView3'");
        target.mIcon = (android.widget.ImageView) finder.findRequiredView(source, com.navdy.hud.app.R.id.image, "field 'mIcon'");
    }

    public static void reset(com.navdy.hud.app.view.ForceUpdateView target) {
        target.mScreenTitleText = null;
        target.mTextView1 = null;
        target.mTextView2 = null;
        target.mChoiceLayout = null;
        target.mTextView3 = null;
        target.mIcon = null;
    }
}
