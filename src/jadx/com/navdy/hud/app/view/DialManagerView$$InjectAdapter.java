package com.navdy.hud.app.view;

public final class DialManagerView$$InjectAdapter extends dagger.internal.Binding<com.navdy.hud.app.view.DialManagerView> implements dagger.MembersInjector<com.navdy.hud.app.view.DialManagerView> {
    private dagger.internal.Binding<com.navdy.hud.app.screen.DialManagerScreen.Presenter> presenter;
    private dagger.internal.Binding<android.content.SharedPreferences> sharedPreferences;

    public DialManagerView$$InjectAdapter() {
        super(null, "members/com.navdy.hud.app.view.DialManagerView", false, com.navdy.hud.app.view.DialManagerView.class);
    }

    public void attach(dagger.internal.Linker linker) {
        this.presenter = linker.requestBinding("com.navdy.hud.app.screen.DialManagerScreen$Presenter", com.navdy.hud.app.view.DialManagerView.class, getClass().getClassLoader());
        this.sharedPreferences = linker.requestBinding("android.content.SharedPreferences", com.navdy.hud.app.view.DialManagerView.class, getClass().getClassLoader());
    }

    public void getDependencies(java.util.Set<dagger.internal.Binding<?>> set, java.util.Set<dagger.internal.Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.presenter);
        injectMembersBindings.add(this.sharedPreferences);
    }

    public void injectMembers(com.navdy.hud.app.view.DialManagerView object) {
        object.presenter = (com.navdy.hud.app.screen.DialManagerScreen.Presenter) this.presenter.get();
        object.sharedPreferences = (android.content.SharedPreferences) this.sharedPreferences.get();
    }
}
