package com.navdy.hud.app.view;

public class ObservableScrollView extends android.widget.ScrollView {
    private android.view.View child;
    private com.navdy.hud.app.view.ObservableScrollView.IScrollListener listener;

    public interface IScrollListener {
        void onBottom();

        void onScroll(int i, int i2, int i3, int i4);

        void onTop();
    }

    public ObservableScrollView(android.content.Context context) {
        super(context);
    }

    public ObservableScrollView(android.content.Context context, android.util.AttributeSet attrs) {
        super(context, attrs);
    }

    public ObservableScrollView(android.content.Context context, android.util.AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setScrollListener(com.navdy.hud.app.view.ObservableScrollView.IScrollListener listener2) {
        this.listener = listener2;
    }

    /* access modifiers changed from: protected */
    public void onScrollChanged(int l, int t, int oldl, int oldt) {
        if (this.child == null) {
            this.child = getChildAt(0);
        }
        if (this.child == null) {
            super.onScrollChanged(l, t, oldl, oldt);
        } else if (t <= 0) {
            if (this.listener != null) {
                this.listener.onTop();
            }
            super.onScrollChanged(l, t, oldl, oldt);
        } else if (this.child.getBottom() - (getHeight() + getScrollY()) == 0) {
            if (this.listener != null) {
                this.listener.onBottom();
            }
            super.onScrollChanged(l, t, oldl, oldt);
        } else {
            if (this.listener != null) {
                this.listener.onScroll(l, t, oldl, oldt);
            }
            super.onScrollChanged(l, t, oldl, oldt);
        }
    }
}
