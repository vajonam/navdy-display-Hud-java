package com.navdy.hud.app.view;

public class LearnGestureScreenLayout$$ViewInjector {
    public static void inject(butterknife.ButterKnife.Finder finder, com.navdy.hud.app.view.LearnGestureScreenLayout target, java.lang.Object source) {
        target.mGestureLearningView = (com.navdy.hud.app.view.GestureLearningView) finder.findRequiredView(source, com.navdy.hud.app.R.id.gesture_learning_view, "field 'mGestureLearningView'");
        target.mScrollableTextPresenter = (com.navdy.hud.app.view.ScrollableTextPresenterLayout) finder.findRequiredView(source, com.navdy.hud.app.R.id.scrollable_text_presenter, "field 'mScrollableTextPresenter'");
        target.mCameraBlockedMessage = (android.view.ViewGroup) finder.findRequiredView(source, com.navdy.hud.app.R.id.sensor_blocked_message, "field 'mCameraBlockedMessage'");
        target.mVideoCaptureInstructionsLayout = (com.navdy.hud.app.view.GestureVideoCaptureView) finder.findRequiredView(source, com.navdy.hud.app.R.id.capture_instructions_lyt, "field 'mVideoCaptureInstructionsLayout'");
    }

    public static void reset(com.navdy.hud.app.view.LearnGestureScreenLayout target) {
        target.mGestureLearningView = null;
        target.mScrollableTextPresenter = null;
        target.mCameraBlockedMessage = null;
        target.mVideoCaptureInstructionsLayout = null;
    }
}
