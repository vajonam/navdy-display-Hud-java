package com.navdy.hud.app.view;

public class HTMLTextView extends android.widget.TextView {
    public HTMLTextView(android.content.Context context) {
        super(context);
    }

    public HTMLTextView(android.content.Context context, android.util.AttributeSet attrs) {
        super(context, attrs);
    }

    public HTMLTextView(android.content.Context context, android.util.AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void setText(java.lang.CharSequence text, android.widget.TextView.BufferType type) {
        super.setText(android.text.Html.fromHtml(text.toString()), type);
    }
}
