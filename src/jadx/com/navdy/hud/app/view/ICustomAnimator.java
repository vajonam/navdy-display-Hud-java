package com.navdy.hud.app.view;

public interface ICustomAnimator {
    android.animation.Animator getCustomAnimator(com.navdy.hud.app.view.MainView.CustomAnimationMode customAnimationMode);
}
