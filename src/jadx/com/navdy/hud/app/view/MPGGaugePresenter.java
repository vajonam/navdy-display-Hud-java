package com.navdy.hud.app.view;

public class MPGGaugePresenter extends com.navdy.hud.app.view.DashboardWidgetPresenter {
    private static final int GAUGE_UPDATE_INTERVAL = 1000;
    private static final long MPG_AVERAGE_TIME_WINDOW = java.util.concurrent.TimeUnit.MINUTES.toMillis(5);
    private android.content.Context mContext;
    @butterknife.InjectView(2131624129)
    android.widget.TextView mFuelConsumptionUnit;
    @butterknife.InjectView(2131624130)
    android.widget.TextView mFuelConsumptionValue;
    /* access modifiers changed from: private */
    public android.os.Handler mHandler = new android.os.Handler();
    private java.lang.String mKmplLabel;
    private double mMpg;
    private java.lang.String mMpgLabel;
    /* access modifiers changed from: private */
    public java.lang.Runnable mRunnable = new com.navdy.hud.app.view.MPGGaugePresenter.Anon1();

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            com.navdy.hud.app.view.MPGGaugePresenter.this.reDraw();
            com.navdy.hud.app.view.MPGGaugePresenter.this.mHandler.postDelayed(com.navdy.hud.app.view.MPGGaugePresenter.this.mRunnable, 1000);
        }
    }

    public MPGGaugePresenter(android.content.Context context) {
        this.mContext = context;
        android.content.res.Resources res = context.getResources();
        this.mMpgLabel = res.getString(com.navdy.hud.app.R.string.mpg);
        this.mKmplLabel = res.getString(com.navdy.hud.app.R.string.kmpl);
    }

    public void setView(com.navdy.hud.app.view.DashboardWidgetView dashboardWidgetView, android.os.Bundle arguments) {
        if (dashboardWidgetView != null) {
            dashboardWidgetView.setContentView((int) com.navdy.hud.app.R.layout.fuel_consumption_gauge);
            butterknife.ButterKnife.inject((java.lang.Object) this, (android.view.View) dashboardWidgetView);
            this.mHandler.removeCallbacks(this.mRunnable);
            this.mHandler.post(this.mRunnable);
        } else {
            this.mHandler.removeCallbacks(this.mRunnable);
        }
        super.setView(dashboardWidgetView, arguments);
    }

    public void setCurrentMPG(double mpgValue) {
        this.mMpg = mpgValue;
        reDraw();
    }

    public android.graphics.drawable.Drawable getDrawable() {
        return null;
    }

    /* access modifiers changed from: protected */
    public void updateGauge() {
        if (this.mWidgetView != null) {
            long runningAverageValueRounded = 0;
            switch (com.navdy.hud.app.manager.SpeedManager.getInstance().getSpeedUnit()) {
                case KILOMETERS_PER_HOUR:
                    runningAverageValueRounded = java.lang.Math.round(com.navdy.hud.app.util.ConversionUtil.convertLpHundredKmToKMPL(this.mMpg));
                    this.mFuelConsumptionUnit.setText(this.mKmplLabel);
                    break;
                case MILES_PER_HOUR:
                    runningAverageValueRounded = java.lang.Math.round(com.navdy.hud.app.util.ConversionUtil.convertLpHundredKmToMPG(this.mMpg));
                    this.mFuelConsumptionUnit.setText(this.mMpgLabel);
                    break;
            }
            if (runningAverageValueRounded > 0) {
                this.mFuelConsumptionValue.setText(java.lang.Long.toString(runningAverageValueRounded));
            } else {
                this.mFuelConsumptionValue.setText("- -");
            }
        }
    }

    public java.lang.String getWidgetIdentifier() {
        return com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.MPG_AVG_WIDGET_ID;
    }

    public java.lang.String getWidgetName() {
        switch (com.navdy.hud.app.manager.SpeedManager.getInstance().getSpeedUnit()) {
            case KILOMETERS_PER_HOUR:
                return this.mKmplLabel;
            case MILES_PER_HOUR:
                return this.mMpgLabel;
            default:
                return null;
        }
    }
}
