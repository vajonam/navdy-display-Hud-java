package com.navdy.hud.app.view;

public class CompassPresenter extends com.navdy.hud.app.view.DashboardWidgetPresenter implements com.navdy.hud.app.view.SerialValueAnimator.SerialValueAnimatorAdapter {
    private com.navdy.hud.app.view.SerialValueAnimator animator = new com.navdy.hud.app.view.SerialValueAnimator(this, 50);
    private java.lang.String compassGaugeName;
    com.navdy.hud.app.view.CompassDrawable mCompassDrawable;
    float mHeadingAngle;

    public CompassPresenter(android.content.Context context) {
        this.mCompassDrawable = new com.navdy.hud.app.view.CompassDrawable(context);
        this.mCompassDrawable.setMaxGaugeValue(360.0f);
        this.mCompassDrawable.setMinValue(0.0f);
        this.compassGaugeName = context.getResources().getString(com.navdy.hud.app.R.string.widget_compass);
    }

    public void setView(com.navdy.hud.app.view.DashboardWidgetView dashboardWidgetView) {
        if (dashboardWidgetView != null) {
            dashboardWidgetView.setContentView((int) com.navdy.hud.app.R.layout.smart_dash_widget_circular_content_layout);
        }
        super.setView(dashboardWidgetView);
    }

    public android.graphics.drawable.Drawable getDrawable() {
        return this.mCompassDrawable;
    }

    public void setHeadingAngle(double angle) {
        if (!this.isDashActive || !this.isWidgetVisibleToUser || this.mWidgetView == null) {
            this.mHeadingAngle = (float) angle;
        } else if (angle == 0.0d) {
            this.mHeadingAngle = 0.0f;
            reDraw();
        } else {
            this.animator.setValue((float) (angle % 360.0d));
        }
    }

    /* access modifiers changed from: protected */
    public void updateGauge() {
        this.mCompassDrawable.setGaugeValue(this.mHeadingAngle);
    }

    public java.lang.String getWidgetIdentifier() {
        return com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.COMPASS_WIDGET_ID;
    }

    public java.lang.String getWidgetName() {
        return this.compassGaugeName;
    }

    public float getValue() {
        return this.mHeadingAngle;
    }

    public void setValue(float newValue) {
        this.mHeadingAngle = newValue;
        reDraw();
    }

    public void animationComplete(float newValue) {
    }
}
