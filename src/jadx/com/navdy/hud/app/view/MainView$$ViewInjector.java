package com.navdy.hud.app.view;

public class MainView$$ViewInjector {
    public static void inject(butterknife.ButterKnife.Finder finder, com.navdy.hud.app.view.MainView target, java.lang.Object source) {
        target.containerView = (com.navdy.hud.app.view.ContainerView) finder.findRequiredView(source, com.navdy.hud.app.R.id.container, "field 'containerView'");
        target.notificationView = (com.navdy.hud.app.view.NotificationView) finder.findRequiredView(source, com.navdy.hud.app.R.id.notification, "field 'notificationView'");
        target.notifIndicator = (com.navdy.hud.app.ui.component.carousel.CarouselIndicator) finder.findRequiredView(source, com.navdy.hud.app.R.id.notifIndicator, "field 'notifIndicator'");
        target.notifScrollIndicator = (com.navdy.hud.app.ui.component.carousel.ProgressIndicator) finder.findRequiredView(source, com.navdy.hud.app.R.id.notifScrollIndicator, "field 'notifScrollIndicator'");
        target.expandedNotificationView = (android.widget.FrameLayout) finder.findRequiredView(source, com.navdy.hud.app.R.id.expandedNotifView, "field 'expandedNotificationView'");
        target.notificationExtensionView = (android.widget.FrameLayout) finder.findRequiredView(source, com.navdy.hud.app.R.id.notificationExtensionView, "field 'notificationExtensionView'");
        target.mainLowerView = (com.navdy.hud.app.ui.component.main.MainLowerView) finder.findRequiredView(source, com.navdy.hud.app.R.id.mainLowerView, "field 'mainLowerView'");
        target.expandedNotificationCoverView = finder.findRequiredView(source, com.navdy.hud.app.R.id.expandedNotifCoverView, "field 'expandedNotificationCoverView'");
        target.splitterView = (android.widget.FrameLayout) finder.findRequiredView(source, com.navdy.hud.app.R.id.splitter, "field 'splitterView'");
        target.notificationColorView = (com.navdy.hud.app.ui.component.image.ColorImageView) finder.findRequiredView(source, com.navdy.hud.app.R.id.notificationColorView, "field 'notificationColorView'");
        target.screenContainer = (android.widget.FrameLayout) finder.findRequiredView(source, com.navdy.hud.app.R.id.screenContainer, "field 'screenContainer'");
        target.systemTray = (com.navdy.hud.app.ui.component.SystemTrayView) finder.findRequiredView(source, com.navdy.hud.app.R.id.systemTray, "field 'systemTray'");
        target.toastView = (com.navdy.hud.app.view.ToastView) finder.findRequiredView(source, com.navdy.hud.app.R.id.toastView, "field 'toastView'");
    }

    public static void reset(com.navdy.hud.app.view.MainView target) {
        target.containerView = null;
        target.notificationView = null;
        target.notifIndicator = null;
        target.notifScrollIndicator = null;
        target.expandedNotificationView = null;
        target.notificationExtensionView = null;
        target.mainLowerView = null;
        target.expandedNotificationCoverView = null;
        target.splitterView = null;
        target.notificationColorView = null;
        target.screenContainer = null;
        target.systemTray = null;
        target.toastView = null;
    }
}
