package com.navdy.hud.app.view;

public final class DialUpdateConfirmationView$$InjectAdapter extends dagger.internal.Binding<com.navdy.hud.app.view.DialUpdateConfirmationView> implements dagger.MembersInjector<com.navdy.hud.app.view.DialUpdateConfirmationView> {
    private dagger.internal.Binding<com.navdy.hud.app.screen.DialUpdateConfirmationScreen.Presenter> mPresenter;

    public DialUpdateConfirmationView$$InjectAdapter() {
        super(null, "members/com.navdy.hud.app.view.DialUpdateConfirmationView", false, com.navdy.hud.app.view.DialUpdateConfirmationView.class);
    }

    public void attach(dagger.internal.Linker linker) {
        this.mPresenter = linker.requestBinding("com.navdy.hud.app.screen.DialUpdateConfirmationScreen$Presenter", com.navdy.hud.app.view.DialUpdateConfirmationView.class, getClass().getClassLoader());
    }

    public void getDependencies(java.util.Set<dagger.internal.Binding<?>> set, java.util.Set<dagger.internal.Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.mPresenter);
    }

    public void injectMembers(com.navdy.hud.app.view.DialUpdateConfirmationView object) {
        object.mPresenter = (com.navdy.hud.app.screen.DialUpdateConfirmationScreen.Presenter) this.mPresenter.get();
    }
}
