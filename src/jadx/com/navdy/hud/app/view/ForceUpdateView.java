package com.navdy.hud.app.view;

public class ForceUpdateView extends android.widget.RelativeLayout implements com.navdy.hud.app.ui.component.ChoiceLayout.IListener, com.navdy.hud.app.manager.InputManager.IInputHandler {
    private static final int TAG_DISMISS = 2;
    private static final int TAG_INSTALL = 1;
    private static final int TAG_SHUT_DOWN = 0;
    private static final int UPDATE_MESSAGE_MAX_WIDTH = 350;
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.view.ForceUpdateView.class);
    @butterknife.InjectView(2131624150)
    com.navdy.hud.app.ui.component.ChoiceLayout mChoiceLayout;
    @butterknife.InjectView(2131624147)
    android.widget.ImageView mIcon;
    @javax.inject.Inject
    com.navdy.hud.app.screen.ForceUpdateScreen.Presenter mPresenter;
    @butterknife.InjectView(2131624144)
    android.widget.TextView mScreenTitleText;
    @butterknife.InjectView(2131624076)
    android.widget.TextView mTextView1;
    @butterknife.InjectView(2131624077)
    android.widget.TextView mTextView2;
    @butterknife.InjectView(2131624078)
    android.widget.TextView mTextView3;
    private boolean waitingForUpdate = false;

    public void executeItem(int pos, int id) {
        performAction(id);
    }

    public void itemSelected(int pos, int id) {
    }

    public boolean onGesture(com.navdy.service.library.events.input.GestureEvent event) {
        return false;
    }

    public boolean onKey(com.navdy.hud.app.manager.InputManager.CustomKeyEvent event) {
        if (this.mChoiceLayout == null || this.mChoiceLayout.getVisibility() != 0) {
            return false;
        }
        switch (event) {
            case LEFT:
                this.mChoiceLayout.moveSelectionLeft();
                return true;
            case RIGHT:
                this.mChoiceLayout.moveSelectionRight();
                return true;
            case SELECT:
                this.mChoiceLayout.executeSelectedItem(true);
                return true;
            default:
                return true;
        }
    }

    public com.navdy.hud.app.manager.InputManager.IInputHandler nextHandler() {
        return null;
    }

    public ForceUpdateView(android.content.Context context) {
        super(context);
        if (!isInEditMode()) {
            mortar.Mortar.inject(context, this);
        }
    }

    public ForceUpdateView(android.content.Context context, android.util.AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) {
            mortar.Mortar.inject(context, this);
        }
    }

    public ForceUpdateView(android.content.Context context, android.util.AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if (!isInEditMode()) {
            mortar.Mortar.inject(context, this);
        }
    }

    @com.squareup.otto.Subscribe
    public void onUpdateReady(com.navdy.hud.app.util.OTAUpdateService.UpdateVerified event) {
        if (this.waitingForUpdate) {
            java.util.List<com.navdy.hud.app.ui.component.ChoiceLayout.Choice> list = new java.util.ArrayList<>();
            list.add(new com.navdy.hud.app.ui.component.ChoiceLayout.Choice(getContext().getString(com.navdy.hud.app.R.string.install), 1));
            list.add(new com.navdy.hud.app.ui.component.ChoiceLayout.Choice(getContext().getString(com.navdy.hud.app.R.string.shutdown), 0));
            this.mChoiceLayout.setChoices(com.navdy.hud.app.ui.component.ChoiceLayout.Mode.LABEL, list, 0, this);
            this.mTextView3.setText(com.navdy.hud.app.R.string.hud_update_install);
            this.waitingForUpdate = false;
        }
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
        boolean updateAvailable = this.mPresenter.isSoftwareUpdatePending();
        java.util.List<com.navdy.hud.app.ui.component.ChoiceLayout.Choice> list = new java.util.ArrayList<>();
        butterknife.ButterKnife.inject((android.view.View) this);
        this.mPresenter.takeView(this);
        this.mPresenter.getBus().register(this);
        ((com.navdy.hud.app.view.MaxWidthLinearLayout) findViewById(com.navdy.hud.app.R.id.infoContainer)).setMaxWidth(UPDATE_MESSAGE_MAX_WIDTH);
        this.mIcon.setImageResource(com.navdy.hud.app.R.drawable.icon_software_update);
        this.mTextView1.setVisibility(8);
        this.mTextView2.setVisibility(8);
        this.mTextView3.setSingleLine(false);
        this.mTextView3.setVisibility(0);
        if (com.navdy.hud.app.ui.activity.Main.mProtocolStatus == com.navdy.hud.app.ui.activity.Main.ProtocolStatus.PROTOCOL_REMOTE_NEEDS_UPDATE) {
            this.mScreenTitleText.setText(com.navdy.hud.app.R.string.title_app_update_required);
            this.mTextView3.setText(com.navdy.hud.app.R.string.phone_update_required);
            list.add(new com.navdy.hud.app.ui.component.ChoiceLayout.Choice(getContext().getString(com.navdy.hud.app.R.string.dismiss), 2));
        } else {
            this.mScreenTitleText.setText(com.navdy.hud.app.R.string.title_display_update_required);
            if (!updateAvailable) {
                this.mTextView3.setText(com.navdy.hud.app.R.string.hud_update_download);
                this.waitingForUpdate = true;
            } else {
                this.mTextView3.setText(com.navdy.hud.app.R.string.hud_update_install);
                list.add(new com.navdy.hud.app.ui.component.ChoiceLayout.Choice(getContext().getString(com.navdy.hud.app.R.string.install), 1));
            }
            list.add(new com.navdy.hud.app.ui.component.ChoiceLayout.Choice(getContext().getString(com.navdy.hud.app.R.string.shutdown), 0));
        }
        this.mChoiceLayout.setChoices(com.navdy.hud.app.ui.component.ChoiceLayout.Mode.LABEL, list, 0, this);
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.mPresenter.getBus().unregister(this);
    }

    private void performAction(int tag) {
        switch (tag) {
            case 0:
                this.mPresenter.shutDown();
                return;
            case 1:
                this.mPresenter.install();
                return;
            case 2:
                this.mPresenter.dismiss();
                return;
            default:
                sLogger.e("unknown action received: " + tag);
                return;
        }
    }
}
