package com.navdy.hud.app.view;

public final class ShutDownConfirmationView$$InjectAdapter extends dagger.internal.Binding<com.navdy.hud.app.view.ShutDownConfirmationView> implements dagger.MembersInjector<com.navdy.hud.app.view.ShutDownConfirmationView> {
    private dagger.internal.Binding<com.navdy.hud.app.screen.ShutDownScreen.Presenter> mPresenter;

    public ShutDownConfirmationView$$InjectAdapter() {
        super(null, "members/com.navdy.hud.app.view.ShutDownConfirmationView", false, com.navdy.hud.app.view.ShutDownConfirmationView.class);
    }

    public void attach(dagger.internal.Linker linker) {
        this.mPresenter = linker.requestBinding("com.navdy.hud.app.screen.ShutDownScreen$Presenter", com.navdy.hud.app.view.ShutDownConfirmationView.class, getClass().getClassLoader());
    }

    public void getDependencies(java.util.Set<dagger.internal.Binding<?>> set, java.util.Set<dagger.internal.Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.mPresenter);
    }

    public void injectMembers(com.navdy.hud.app.view.ShutDownConfirmationView object) {
        object.mPresenter = (com.navdy.hud.app.screen.ShutDownScreen.Presenter) this.mPresenter.get();
    }
}
