package com.navdy.hud.app.view;

public class MainView extends android.widget.FrameLayout implements com.navdy.hud.app.manager.InputManager.IInputHandler {
    private static final java.lang.String COMPACT_MODE_SCALE_PROPERTY = "persist.sys.compact.scale";
    private static final float DEFAULT_COMPACT_MODE_SCALE = 0.87f;
    private static final int SHOW_OPTION_LAG = 350;
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.view.MainView.class);
    private com.navdy.hud.app.ui.framework.AnimationQueue animationQueue;
    /* access modifiers changed from: private */
    public int basePaddingTop;
    private android.content.SharedPreferences.OnSharedPreferenceChangeListener brightnessControl;
    @butterknife.InjectView(2131624246)
    com.navdy.hud.app.view.ContainerView containerView;
    @butterknife.InjectView(2131624250)
    android.view.View expandedNotificationCoverView;
    @butterknife.InjectView(2131624251)
    android.widget.FrameLayout expandedNotificationView;
    private com.navdy.service.library.events.preferences.DriverProfilePreferences.DisplayFormat format;
    /* access modifiers changed from: private */
    public android.os.Handler handler;
    /* access modifiers changed from: private */
    public boolean isNotificationCollapsing;
    /* access modifiers changed from: private */
    public boolean isNotificationExpanding;
    /* access modifiers changed from: private */
    public boolean isScreenAnimating;
    @butterknife.InjectView(2131624253)
    com.navdy.hud.app.ui.component.main.MainLowerView mainLowerView;
    /* access modifiers changed from: private */
    public int mainPanelWidth;
    @butterknife.InjectView(2131624255)
    com.navdy.hud.app.ui.component.carousel.CarouselIndicator notifIndicator;
    @butterknife.InjectView(2131624256)
    com.navdy.hud.app.ui.component.carousel.ProgressIndicator notifScrollIndicator;
    private com.navdy.hud.app.ui.framework.INotificationAnimationListener notificationAnimationListener;
    @butterknife.InjectView(2131624248)
    com.navdy.hud.app.ui.component.image.ColorImageView notificationColorView;
    @butterknife.InjectView(2131624252)
    android.widget.FrameLayout notificationExtensionView;
    @butterknife.InjectView(2131624254)
    com.navdy.hud.app.view.NotificationView notificationView;
    @javax.inject.Inject
    android.content.SharedPreferences preferences;
    @javax.inject.Inject
    com.navdy.hud.app.ui.activity.Main.Presenter presenter;
    /* access modifiers changed from: private */
    public int rightPanelStart;
    private com.navdy.hud.app.ui.framework.IScreenAnimationListener screenAnimationListener;
    @butterknife.InjectView(2131624247)
    android.widget.FrameLayout screenContainer;
    /* access modifiers changed from: private */
    public boolean showingNotificationExtension;
    /* access modifiers changed from: private */
    public int sidePanelWidth;
    @butterknife.InjectView(2131624245)
    android.widget.FrameLayout splitterView;
    @butterknife.InjectView(2131624249)
    com.navdy.hud.app.ui.component.SystemTrayView systemTray;
    @butterknife.InjectView(2131624257)
    com.navdy.hud.app.view.ToastView toastView;
    @javax.inject.Inject
    com.navdy.hud.app.ui.framework.UIStateManager uiStateManager;
    /* access modifiers changed from: private */
    public int verticalOffset;

    public enum AnimationMode {
        MOVE_LEFT_SHRINK,
        RIGHT_EXPAND
    }

    class Anon1 implements com.navdy.hud.app.ui.framework.INotificationAnimationListener {
        Anon1() {
        }

        public void onStart(java.lang.String id, com.navdy.hud.app.framework.notifications.NotificationType type, com.navdy.hud.app.ui.framework.UIStateManager.Mode mode) {
            if (mode == com.navdy.hud.app.ui.framework.UIStateManager.Mode.EXPAND) {
                com.navdy.hud.app.view.MainView.this.isNotificationExpanding = true;
            } else {
                com.navdy.hud.app.view.MainView.this.isNotificationCollapsing = true;
            }
        }

        public void onStop(java.lang.String id, com.navdy.hud.app.framework.notifications.NotificationType type, com.navdy.hud.app.ui.framework.UIStateManager.Mode mode) {
            if (mode == com.navdy.hud.app.ui.framework.UIStateManager.Mode.EXPAND) {
                com.navdy.hud.app.view.MainView.this.isNotificationExpanding = false;
            } else {
                com.navdy.hud.app.view.MainView.this.isNotificationCollapsing = false;
            }
        }
    }

    class Anon2 implements com.navdy.hud.app.ui.framework.IScreenAnimationListener {
        Anon2() {
        }

        public void onStart(com.navdy.hud.app.screen.BaseScreen in, com.navdy.hud.app.screen.BaseScreen out) {
            com.navdy.hud.app.view.MainView.this.isScreenAnimating = true;
        }

        public void onStop(com.navdy.hud.app.screen.BaseScreen in, com.navdy.hud.app.screen.BaseScreen out) {
            com.navdy.hud.app.view.MainView.this.isScreenAnimating = false;
        }
    }

    class Anon3 implements android.view.ViewTreeObserver.OnGlobalLayoutListener {

        class Anon1 implements java.lang.Runnable {
            Anon1() {
            }

            public void run() {
                com.navdy.hud.app.view.MainView.this.notificationView.setX((float) com.navdy.hud.app.view.MainView.this.mainPanelWidth);
                com.navdy.hud.app.view.MainView.sLogger.v("notifView x: " + com.navdy.hud.app.view.MainView.this.mainPanelWidth);
            }
        }

        Anon3() {
        }

        public void onGlobalLayout() {
            com.navdy.hud.app.view.MainView.this.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            android.content.res.Resources resources = com.navdy.hud.app.view.MainView.this.getResources();
            int measuredHt = (int) resources.getDimension(com.navdy.hud.app.R.dimen.dashboard_height);
            com.navdy.hud.app.view.MainView.this.basePaddingTop = (measuredHt / 2) - (((int) com.navdy.hud.app.view.MainView.this.getResources().getDimension(com.navdy.hud.app.R.dimen.dashboard_box_height)) / 2);
            ((android.widget.FrameLayout.LayoutParams) com.navdy.hud.app.view.MainView.this.splitterView.getLayoutParams()).topMargin = com.navdy.hud.app.view.MainView.this.basePaddingTop + com.navdy.hud.app.view.MainView.this.verticalOffset;
            com.navdy.hud.app.view.MainView.this.mainPanelWidth = (int) resources.getDimension(com.navdy.hud.app.R.dimen.dashboard_width);
            if (com.navdy.hud.app.util.DeviceUtil.isNavdyDevice()) {
                com.navdy.hud.app.view.MainView.this.sidePanelWidth = (int) resources.getDimension(com.navdy.hud.app.R.dimen.side_panel_width);
            } else {
                com.navdy.hud.app.view.MainView.this.sidePanelWidth = (int) (((float) com.navdy.hud.app.view.MainView.this.mainPanelWidth) * 0.33f);
            }
            com.navdy.hud.app.view.MainView.this.uiStateManager.setMainPanelWidth(com.navdy.hud.app.view.MainView.this.mainPanelWidth);
            com.navdy.hud.app.view.MainView.this.uiStateManager.setSidePanelWidth(com.navdy.hud.app.view.MainView.this.sidePanelWidth);
            com.navdy.hud.app.view.MainView.sLogger.v("side panel width=" + com.navdy.hud.app.view.MainView.this.sidePanelWidth);
            com.navdy.hud.app.view.MainView.this.rightPanelStart = com.navdy.hud.app.view.MainView.this.mainPanelWidth - com.navdy.hud.app.view.MainView.this.sidePanelWidth;
            android.util.DisplayMetrics metrics = com.navdy.hud.app.view.MainView.this.getResources().getDisplayMetrics();
            com.navdy.hud.app.view.MainView.sLogger.v("Density:" + metrics.density + ", dpi:" + metrics.densityDpi + ", width:" + metrics.widthPixels + ", height:" + metrics.heightPixels);
            com.navdy.hud.app.view.MainView.sLogger.v("onGlobalLayout mainPanelWidth = " + com.navdy.hud.app.view.MainView.this.mainPanelWidth + " sidePanelWidth = " + com.navdy.hud.app.view.MainView.this.sidePanelWidth + " rightPanelStart = " + com.navdy.hud.app.view.MainView.this.rightPanelStart + " paddingTop = " + com.navdy.hud.app.view.MainView.this.basePaddingTop + " mh =" + measuredHt);
            com.navdy.hud.app.view.MainView.this.containerView.setX(0.0f);
            com.navdy.hud.app.view.MainView.sLogger.v("containerView x: 0");
            ((android.widget.FrameLayout.LayoutParams) com.navdy.hud.app.view.MainView.this.containerView.getLayoutParams()).width = com.navdy.hud.app.view.MainView.this.mainPanelWidth;
            ((android.widget.FrameLayout.LayoutParams) com.navdy.hud.app.view.MainView.this.notificationView.getLayoutParams()).width = com.navdy.hud.app.view.MainView.this.sidePanelWidth;
            com.navdy.hud.app.view.MainView.this.notificationView.setX((float) com.navdy.hud.app.view.MainView.this.mainPanelWidth);
            com.navdy.hud.app.view.MainView.sLogger.v("notifView x: " + com.navdy.hud.app.view.MainView.this.mainPanelWidth);
            com.navdy.hud.app.view.MainView.this.expandedNotificationView.setX((float) com.navdy.hud.app.view.MainView.this.sidePanelWidth);
            com.navdy.hud.app.view.MainView.sLogger.v("expand notifView x: " + com.navdy.hud.app.view.MainView.this.sidePanelWidth);
            int expandX = com.navdy.hud.app.view.MainView.this.sidePanelWidth + ((int) resources.getDimension(com.navdy.hud.app.R.dimen.expand_notif_width)) + ((int) resources.getDimension(com.navdy.hud.app.R.dimen.expand_notif_indicator_left_margin));
            com.navdy.hud.app.view.MainView.this.notifIndicator.setOrientation(com.navdy.hud.app.ui.component.carousel.CarouselIndicator.Orientation.VERTICAL);
            com.navdy.hud.app.view.MainView.this.notifIndicator.setX((float) expandX);
            com.navdy.hud.app.view.MainView.sLogger.v("notif indicator x: " + expandX);
            com.navdy.hud.app.view.MainView.this.notifScrollIndicator.setOrientation(com.navdy.hud.app.ui.component.carousel.CarouselIndicator.Orientation.VERTICAL);
            com.navdy.hud.app.view.MainView.this.notifScrollIndicator.setProperties(com.navdy.hud.app.framework.glance.GlanceConstants.scrollingRoundSize, com.navdy.hud.app.framework.glance.GlanceConstants.scrollingIndicatorProgressSize, 0, 0, com.navdy.hud.app.framework.glance.GlanceConstants.colorWhite, com.navdy.hud.app.framework.glance.GlanceConstants.colorWhite, true, com.navdy.hud.app.framework.glance.GlanceConstants.scrollingIndicatorPadding, -1, -1);
            com.navdy.hud.app.view.MainView.this.notifScrollIndicator.setItemCount(100);
            com.navdy.hud.app.view.MainView.this.handler.postDelayed(new com.navdy.hud.app.view.MainView.Anon3.Anon1(), 2000);
            com.navdy.hud.app.view.MainView.this.presenter.createScreens();
            com.navdy.hud.app.view.MainView.this.presenter.setInputFocus();
        }
    }

    class Anon4 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
        final /* synthetic */ java.lang.String val$notifId;
        final /* synthetic */ com.navdy.hud.app.framework.notifications.NotificationType val$type;

        Anon4(java.lang.String str, com.navdy.hud.app.framework.notifications.NotificationType notificationType) {
            this.val$notifId = str;
            this.val$type = notificationType;
        }

        public void onAnimationStart(android.animation.Animator animation) {
            com.navdy.hud.app.view.MainView.this.uiStateManager.postNotificationAnimationEvent(true, this.val$notifId, this.val$type, com.navdy.hud.app.ui.framework.UIStateManager.Mode.EXPAND);
        }

        public void onAnimationEnd(android.animation.Animator animation) {
            com.navdy.hud.app.view.MainView.this.uiStateManager.postNotificationAnimationEvent(false, this.val$notifId, this.val$type, com.navdy.hud.app.ui.framework.UIStateManager.Mode.EXPAND);
        }
    }

    class Anon5 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
        Anon5() {
        }

        public void onAnimationStart(android.animation.Animator animation) {
            com.navdy.hud.app.view.MainView.this.postNotificationCollapseEvent(true);
        }

        public void onAnimationEnd(android.animation.Animator animation) {
            com.navdy.hud.app.view.MainView.this.postNotificationCollapseEvent(false);
        }
    }

    class Anon6 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
        final /* synthetic */ android.view.ViewGroup val$notificationExtensionView;
        final /* synthetic */ android.view.View val$view;

        Anon6(android.view.ViewGroup viewGroup, android.view.View view) {
            this.val$notificationExtensionView = viewGroup;
            this.val$view = view;
        }

        public void onAnimationStart(android.animation.Animator animation) {
            super.onAnimationStart(animation);
            this.val$notificationExtensionView.setVisibility(0);
            com.navdy.hud.app.view.MainView.this.showingNotificationExtension = true;
        }

        public void onAnimationEnd(android.animation.Animator animation) {
            super.onAnimationEnd(animation);
            if (this.val$view instanceof com.navdy.hud.app.ui.activity.Main.INotificationExtensionView) {
                ((com.navdy.hud.app.ui.activity.Main.INotificationExtensionView) this.val$view).onStart();
            }
        }
    }

    class Anon7 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
        final /* synthetic */ android.view.ViewGroup val$notificationExtension;

        Anon7(android.view.ViewGroup viewGroup) {
            this.val$notificationExtension = viewGroup;
        }

        public void onAnimationEnd(android.animation.Animator animation) {
            super.onAnimationEnd(animation);
            android.view.View view = this.val$notificationExtension.getChildAt(0);
            if (view instanceof com.navdy.hud.app.ui.activity.Main.INotificationExtensionView) {
                ((com.navdy.hud.app.ui.activity.Main.INotificationExtensionView) view).onStop();
            }
            this.val$notificationExtension.setVisibility(8);
            this.val$notificationExtension.removeAllViews();
        }
    }

    public enum CustomAnimationMode {
        SHRINK_LEFT,
        EXPAND,
        SHRINK_MODE
    }

    public MainView(android.content.Context context) {
        this(context, null);
    }

    public MainView(android.content.Context context, android.util.AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MainView(android.content.Context context, android.util.AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.basePaddingTop = -1;
        this.animationQueue = new com.navdy.hud.app.ui.framework.AnimationQueue();
        this.handler = new android.os.Handler();
        this.notificationAnimationListener = new com.navdy.hud.app.view.MainView.Anon1();
        this.screenAnimationListener = new com.navdy.hud.app.view.MainView.Anon2();
        if (!isInEditMode()) {
            mortar.Mortar.inject(context, this);
        }
        initLayout();
    }

    private void initLayout() {
        getViewTreeObserver().addOnGlobalLayoutListener(new com.navdy.hud.app.view.MainView.Anon3());
    }

    public void setDisplayFormat(com.navdy.service.library.events.preferences.DriverProfilePreferences.DisplayFormat format2) {
        if (format2 != this.format) {
            this.format = format2;
            float scale = format2 == com.navdy.service.library.events.preferences.DriverProfilePreferences.DisplayFormat.DISPLAY_FORMAT_COMPACT ? compactScale() : 1.0f;
            setScaleX(scale);
            setScaleY(scale);
        }
    }

    private float compactScale() {
        return com.navdy.hud.app.util.os.SystemProperties.getFloat(COMPACT_MODE_SCALE_PROPERTY, DEFAULT_COMPACT_MODE_SCALE);
    }

    public int getBasePaddingTop() {
        return this.basePaddingTop;
    }

    public int getVerticalOffset() {
        return this.verticalOffset;
    }

    public void setVerticalOffset(int offset) {
        if (offset != this.verticalOffset) {
            this.verticalOffset = offset;
            if (this.basePaddingTop != -1) {
                int newPadding = this.basePaddingTop + this.verticalOffset;
                if (newPadding >= 0) {
                    android.widget.FrameLayout boxView = getBoxView();
                    android.widget.FrameLayout.LayoutParams params = (android.widget.FrameLayout.LayoutParams) boxView.getLayoutParams();
                    params.topMargin = newPadding;
                    boxView.setLayoutParams(params);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
        butterknife.ButterKnife.inject((android.view.View) this);
        if (!isInEditMode()) {
            if (!com.navdy.hud.app.util.DeviceUtil.isNavdyDevice()) {
                addDebugMarkers(getContext());
            }
            this.uiStateManager.setToastView(this.toastView);
            this.uiStateManager.addScreenAnimationListener(this.screenAnimationListener);
            this.uiStateManager.addNotificationAnimationListener(this.notificationAnimationListener);
        }
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.presenter != null) {
            this.presenter.takeView(this);
        }
        if (this.preferences == null) {
            return;
        }
        if (com.navdy.hud.app.settings.HUDSettings.USE_ADAPTIVE_AUTOBRIGHTNESS) {
            this.brightnessControl = new com.navdy.hud.app.settings.AdaptiveBrightnessControl(getContext(), com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getBus(), this.preferences, com.navdy.hud.app.settings.HUDSettings.BRIGHTNESS, com.navdy.hud.app.settings.HUDSettings.AUTO_BRIGHTNESS, com.navdy.hud.app.settings.HUDSettings.AUTO_BRIGHTNESS_ADJUSTMENT, com.navdy.hud.app.settings.HUDSettings.LED_BRIGHTNESS);
        } else {
            this.brightnessControl = new com.navdy.hud.app.settings.BrightnessControl(getContext(), com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getBus(), this.preferences, com.navdy.hud.app.settings.HUDSettings.BRIGHTNESS, com.navdy.hud.app.settings.HUDSettings.AUTO_BRIGHTNESS, com.navdy.hud.app.settings.HUDSettings.AUTO_BRIGHTNESS_ADJUSTMENT, com.navdy.hud.app.settings.HUDSettings.LED_BRIGHTNESS);
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.presenter != null) {
            this.presenter.dropView(this);
        }
        if (this.preferences != null) {
            this.preferences.unregisterOnSharedPreferenceChangeListener(this.brightnessControl);
            this.brightnessControl = null;
        }
    }

    public com.navdy.hud.app.view.ContainerView getContainerView() {
        return this.containerView;
    }

    public com.navdy.hud.app.view.NotificationView getNotificationView() {
        return this.notificationView;
    }

    public android.widget.FrameLayout getExpandedNotificationView() {
        return this.expandedNotificationView;
    }

    public android.widget.FrameLayout getNotificationExtensionView() {
        return this.notificationExtensionView;
    }

    public android.view.View getExpandedNotificationCoverView() {
        return this.expandedNotificationCoverView;
    }

    public com.navdy.hud.app.ui.component.carousel.CarouselIndicator getNotificationIndicator() {
        return this.notifIndicator;
    }

    public com.navdy.hud.app.ui.component.carousel.ProgressIndicator getNotificationScrollIndicator() {
        return this.notifScrollIndicator;
    }

    public android.widget.FrameLayout getBoxView() {
        return this.splitterView;
    }

    public boolean isMainUIShrunk() {
        return (this.isNotificationExpanding || isNotificationViewShowing()) && !this.isNotificationCollapsing;
    }

    public boolean isScreenAnimating() {
        return this.isScreenAnimating;
    }

    public boolean isNotificationViewShowing() {
        return this.notificationView.getX() < ((float) this.mainPanelWidth);
    }

    public boolean isExpandedNotificationViewShowing() {
        return this.expandedNotificationView.getVisibility() == 0;
    }

    public boolean isNotificationExpanding() {
        return this.isNotificationExpanding;
    }

    public boolean isNotificationCollapsing() {
        return this.isNotificationCollapsing;
    }

    private android.animation.ObjectAnimator getXPositionAnimator(android.view.ViewGroup viewGroup, int xPos) {
        return android.animation.ObjectAnimator.ofFloat(viewGroup, "x", new float[]{(float) xPos});
    }

    public boolean onGesture(com.navdy.service.library.events.input.GestureEvent event) {
        return false;
    }

    public boolean onKey(com.navdy.hud.app.manager.InputManager.CustomKeyEvent event) {
        return false;
    }

    public com.navdy.hud.app.manager.InputManager.IInputHandler nextHandler() {
        return this.presenter;
    }

    private void addDebugMarkers(android.content.Context context) {
        int color = getResources().getColor(17170443);
        android.util.DisplayMetrics displayMetrics = new android.util.DisplayMetrics();
        ((android.view.WindowManager) com.navdy.hud.app.HudApplication.getAppContext().getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
        int dip = (int) android.util.TypedValue.applyDimension(1, 1.0f, displayMetrics);
        android.widget.FrameLayout.LayoutParams params = new android.widget.FrameLayout.LayoutParams(-1, dip);
        params.gravity = 48;
        android.view.View view = new android.view.View(context);
        view.setBackgroundColor(color);
        this.splitterView.addView(view, params);
        android.widget.FrameLayout.LayoutParams params2 = new android.widget.FrameLayout.LayoutParams(-1, dip);
        params2.gravity = 80;
        android.view.View view2 = new android.view.View(context);
        view2.setBackgroundColor(color);
        this.splitterView.addView(view2, params2);
    }

    public void showNotification(java.lang.String notifId, com.navdy.hud.app.framework.notifications.NotificationType type) {
        android.animation.AnimatorSet set = new android.animation.AnimatorSet();
        android.animation.AnimatorSet set2 = new android.animation.AnimatorSet();
        set2.playTogether(new android.animation.Animator[]{rightExpand(), middleAnimate(com.navdy.hud.app.view.MainView.AnimationMode.MOVE_LEFT_SHRINK)});
        if (this.notificationView.getX() < ((float) this.mainPanelWidth)) {
            this.uiStateManager.postNotificationAnimationEvent(true, notifId, type, com.navdy.hud.app.ui.framework.UIStateManager.Mode.EXPAND);
            this.uiStateManager.postNotificationAnimationEvent(false, notifId, type, com.navdy.hud.app.ui.framework.UIStateManager.Mode.EXPAND);
            return;
        }
        set.play(set2);
        com.navdy.hud.app.screen.BaseScreen currentScreen = this.uiStateManager.getCurrentScreen();
        set.addListener(new com.navdy.hud.app.view.MainView.Anon4(notifId, type));
        this.animationQueue.startAnimation(set);
    }

    public void dismissNotification() {
        android.animation.AnimatorSet set = new android.animation.AnimatorSet();
        sLogger.d("dismissNotification");
        if (this.notificationView.getX() < ((float) this.mainPanelWidth)) {
            android.animation.AnimatorSet.Builder builder = set.play(rightCollapse()).with(middleAnimate(com.navdy.hud.app.view.MainView.AnimationMode.RIGHT_EXPAND));
            if (this.showingNotificationExtension) {
                sLogger.d("hiding notification extension along with notification");
                builder.with(getRemoveNotificationExtensionAnimator());
            }
        }
        set.addListener(new com.navdy.hud.app.view.MainView.Anon5());
        this.animationQueue.startAnimation(set);
    }

    public void addNotificationExtensionView(android.view.View view) {
        sLogger.d("addNotificationExtensionView");
        android.view.ViewGroup notificationExtensionView2 = getNotificationExtensionView();
        notificationExtensionView2.addView(view);
        android.animation.Animator objectAnimator = android.animation.AnimatorInflater.loadAnimator(notificationExtensionView2.getContext(), 17498112);
        objectAnimator.setTarget(notificationExtensionView2);
        objectAnimator.addListener(new com.navdy.hud.app.view.MainView.Anon6(notificationExtensionView2, view));
        objectAnimator.start();
    }

    public void removeNotificationExtensionView() {
        sLogger.d("addNotificationExtensionView, showing extension ? : " + this.showingNotificationExtension);
        android.view.ViewGroup notificationExtension = getNotificationExtensionView();
        if (notificationExtension == null) {
            return;
        }
        if (this.showingNotificationExtension) {
            getRemoveNotificationExtensionAnimator().start();
            return;
        }
        this.notificationExtensionView.setVisibility(8);
        notificationExtension.removeAllViews();
    }

    private android.animation.Animator getRemoveNotificationExtensionAnimator() {
        android.view.ViewGroup notificationExtension = getNotificationExtensionView();
        android.animation.Animator objectAnimator = android.animation.AnimatorInflater.loadAnimator(notificationExtension.getContext(), 17498113);
        objectAnimator.setTarget(notificationExtension);
        objectAnimator.addListener(new com.navdy.hud.app.view.MainView.Anon7(notificationExtension));
        return objectAnimator;
    }

    private android.animation.Animator rightCollapse() {
        return getXPositionAnimator(this.notificationView, this.mainPanelWidth);
    }

    private android.animation.ObjectAnimator rightExpand() {
        return getXPositionAnimator(this.notificationView, this.rightPanelStart);
    }

    private android.animation.Animator middleAnimate(com.navdy.hud.app.view.MainView.AnimationMode animationMode) {
        android.animation.Animator customAnimator = null;
        com.navdy.hud.app.view.MainView.CustomAnimationMode customAnimationMode = null;
        if (animationMode == com.navdy.hud.app.view.MainView.AnimationMode.MOVE_LEFT_SHRINK) {
            customAnimationMode = com.navdy.hud.app.view.MainView.CustomAnimationMode.SHRINK_LEFT;
        } else if (animationMode == com.navdy.hud.app.view.MainView.AnimationMode.RIGHT_EXPAND) {
            customAnimationMode = com.navdy.hud.app.view.MainView.CustomAnimationMode.EXPAND;
        }
        if (customAnimationMode != null) {
            customAnimator = this.containerView.getCustomContainerAnimator(customAnimationMode);
        }
        android.animation.AnimatorSet set = new android.animation.AnimatorSet();
        switch (animationMode) {
            case MOVE_LEFT_SHRINK:
                android.animation.Animator xAnimator = getXPositionAnimator(this.containerView, -this.sidePanelWidth);
                if (customAnimator != null) {
                    set.playTogether(new android.animation.Animator[]{xAnimator, customAnimator});
                    return set;
                }
                set.playTogether(new android.animation.Animator[]{xAnimator});
                return set;
            case RIGHT_EXPAND:
                android.animation.Animator xAnimator2 = getXPositionAnimator(this.containerView, 0);
                if (customAnimator != null) {
                    set.playTogether(new android.animation.Animator[]{xAnimator2, customAnimator});
                    return set;
                }
                set.play(xAnimator2);
                return set;
            default:
                return null;
        }
    }

    public void setNotificationColor(int color) {
        this.notificationColorView.setColor(color);
    }

    public void setNotificationColorVisibility(int visibility) {
        this.notificationColorView.setVisibility(visibility);
    }

    public int getNotificationColorVisibility() {
        return this.notificationColorView.getVisibility();
    }

    public com.navdy.hud.app.ui.component.SystemTrayView getSystemTray() {
        return this.systemTray;
    }

    public com.navdy.hud.app.view.ToastView getToastView() {
        return this.toastView;
    }

    /* access modifiers changed from: private */
    public void postNotificationCollapseEvent(boolean starting) {
        java.lang.String s = null;
        com.navdy.hud.app.framework.notifications.NotificationType t = null;
        com.navdy.hud.app.framework.notifications.INotification notification = com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().getCurrentNotification();
        if (notification != null) {
            s = notification.getId();
            t = notification.getType();
        }
        this.uiStateManager.postNotificationAnimationEvent(starting, s, t, com.navdy.hud.app.ui.framework.UIStateManager.Mode.COLLAPSE);
    }

    public void injectMainLowerView(android.view.View view) {
        this.mainLowerView.injectView(view);
    }

    public void ejectMainLowerView() {
        this.mainLowerView.ejectView();
    }

    public boolean isMainLowerViewVisible() {
        return this.mainLowerView.getVisibility() == 0;
    }
}
