package com.navdy.hud.app.view;

public class SpeedometerGaugePresenter2 extends com.navdy.hud.app.view.GaugeViewPresenter implements com.navdy.hud.app.view.SerialValueAnimator.SerialValueAnimatorAdapter {
    private static final boolean ALWAYS_SHOW_SPEED_LIMIT = true;
    public static final java.lang.String ID = "SPEEDO_METER_WIDGET_2";
    private static final int MAX_ANIMATION_DURATION = 100;
    public static final int MAX_SPEED = 158;
    private static final int MIN_ANIMATION_DURATION = 20;
    public static final int RESAMPLE_INTERVAL = 1000;
    public static final int SAFE = 0;
    private static final int SPEED_LIMIT_ANIMATION_DELAY = 500;
    private static final int SPEED_LIMIT_ANIMATION_DURATION = 1000;
    public static final int SPEED_LIMIT_EXCEEDED = 1;
    public static final int SPEED_LIMIT_TRESHOLD_EXCEEDED = 2;
    private int animationDuration = 100;
    private android.os.Handler handler = new android.os.Handler();
    private final java.lang.String kmhString;
    private int lastSpeed = Integer.MIN_VALUE;
    private long lastSpeedSampleArrivalTime = 0;
    private com.navdy.hud.app.view.SerialValueAnimator mSerialValueAnimator;
    private int mSpeed;
    private int mSpeedLimit = 0;
    private com.navdy.hud.app.view.drawable.SpeedoMeterGaugeDrawable2 mSpeedoMeterGaugeDrawable;
    private final java.lang.String mphString;
    /* access modifiers changed from: private */
    public boolean showingSpeedLimitAnimation;
    /* access modifiers changed from: private */
    public int speedLimitAlpha;
    private java.lang.Runnable speedLimitFadeOutAnimationRunnable;
    /* access modifiers changed from: private */
    public android.animation.ValueAnimator speedLimitFadeOutAnimator;
    private boolean speedLimitMarkerNeedsTobeRemoved = false;
    private com.navdy.hud.app.manager.SpeedManager speedManager = com.navdy.hud.app.manager.SpeedManager.getInstance();
    private final java.lang.String speedoMeterWidgetName;
    private int textSize2Chars;
    private int textSize3Chars;
    private boolean updateText = true;

    class Anon1 implements java.lang.Runnable {

        /* renamed from: com.navdy.hud.app.view.SpeedometerGaugePresenter2$Anon1$Anon1 reason: collision with other inner class name */
        class C0037Anon1 implements android.animation.ValueAnimator.AnimatorUpdateListener {
            C0037Anon1() {
            }

            public void onAnimationUpdate(android.animation.ValueAnimator animation) {
                com.navdy.hud.app.view.SpeedometerGaugePresenter2.this.speedLimitAlpha = ((java.lang.Integer) animation.getAnimatedValue()).intValue();
                com.navdy.hud.app.view.SpeedometerGaugePresenter2.this.reDraw();
            }
        }

        class Anon2 implements android.animation.Animator.AnimatorListener {
            Anon2() {
            }

            public void onAnimationStart(android.animation.Animator animation) {
            }

            public void onAnimationEnd(android.animation.Animator animation) {
                com.navdy.hud.app.view.SpeedometerGaugePresenter2.this.speedLimitAlpha = 0;
                com.navdy.hud.app.view.SpeedometerGaugePresenter2.this.showingSpeedLimitAnimation = false;
                com.navdy.hud.app.view.SpeedometerGaugePresenter2.this.reDraw();
            }

            public void onAnimationCancel(android.animation.Animator animation) {
            }

            public void onAnimationRepeat(android.animation.Animator animation) {
            }
        }

        Anon1() {
        }

        public void run() {
            if (com.navdy.hud.app.view.SpeedometerGaugePresenter2.this.speedLimitFadeOutAnimator.isRunning()) {
                com.navdy.hud.app.view.SpeedometerGaugePresenter2.this.speedLimitFadeOutAnimator.cancel();
            }
            com.navdy.hud.app.view.SpeedometerGaugePresenter2.this.speedLimitFadeOutAnimator.setIntValues(new int[]{255, 0});
            com.navdy.hud.app.view.SpeedometerGaugePresenter2.this.speedLimitFadeOutAnimator.setDuration(1000);
            com.navdy.hud.app.view.SpeedometerGaugePresenter2.this.speedLimitFadeOutAnimator.addUpdateListener(new com.navdy.hud.app.view.SpeedometerGaugePresenter2.Anon1.C0037Anon1());
            com.navdy.hud.app.view.SpeedometerGaugePresenter2.this.speedLimitFadeOutAnimator.addListener(new com.navdy.hud.app.view.SpeedometerGaugePresenter2.Anon1.Anon2());
            com.navdy.hud.app.view.SpeedometerGaugePresenter2.this.speedLimitFadeOutAnimator.start();
        }
    }

    public SpeedometerGaugePresenter2(android.content.Context context) {
        this.mSpeedoMeterGaugeDrawable = new com.navdy.hud.app.view.drawable.SpeedoMeterGaugeDrawable2(context, 0, com.navdy.hud.app.R.array.smart_dash_speedo_meter_color_table, com.navdy.hud.app.R.dimen.speedo_meter_guage_inner_bar_width);
        this.mSpeedoMeterGaugeDrawable.setMaxGaugeValue(158.0f);
        this.mSerialValueAnimator = new com.navdy.hud.app.view.SerialValueAnimator(this, 100);
        this.speedLimitFadeOutAnimator = new android.animation.ValueAnimator();
        this.speedLimitFadeOutAnimationRunnable = new com.navdy.hud.app.view.SpeedometerGaugePresenter2.Anon1();
        android.content.res.Resources resources = context.getResources();
        this.mphString = resources.getString(com.navdy.hud.app.R.string.mph);
        this.kmhString = resources.getString(com.navdy.hud.app.R.string.kilometers_per_hour);
        this.textSize2Chars = resources.getDimensionPixelSize(com.navdy.hud.app.R.dimen.tachometer_guage_speed_text_size);
        this.textSize3Chars = resources.getDimensionPixelSize(com.navdy.hud.app.R.dimen.tachometer_guage_speed_text_size_2);
        this.speedoMeterWidgetName = resources.getString(com.navdy.hud.app.R.string.gauge_speedo_meter);
    }

    public void setSpeed(int speed) {
        int speedVal;
        if (speed != -1) {
            long now = android.os.SystemClock.elapsedRealtime();
            long newTimeDifferenceBetweenSamples = now - this.lastSpeedSampleArrivalTime;
            this.lastSpeedSampleArrivalTime = now;
            if (newTimeDifferenceBetweenSamples > 1000) {
                this.animationDuration = 100;
            } else {
                this.animationDuration = (int) java.lang.Math.max(20, java.lang.Math.min((long) this.animationDuration, newTimeDifferenceBetweenSamples));
            }
            if (this.lastSpeed != speed) {
                this.lastSpeed = speed;
                if (speed > 158) {
                    speedVal = MAX_SPEED;
                } else {
                    speedVal = speed;
                }
                this.mSerialValueAnimator.setDuration(this.animationDuration);
                this.mSerialValueAnimator.setValue((float) speedVal);
            }
        }
    }

    public void updateGauge() {
        java.lang.String unitText;
        int speedLimitThreshold;
        if (this.mGaugeView != null) {
            android.content.res.Resources res = this.mGaugeView.getResources();
            this.mSpeedoMeterGaugeDrawable.setGaugeValue((float) this.mSpeed);
            this.mSpeedoMeterGaugeDrawable.setSpeedLimit(this.mSpeedLimit);
            com.navdy.hud.app.manager.SpeedManager.SpeedUnit speedUnit = this.speedManager.getSpeedUnit();
            java.lang.String str = this.mphString;
            switch (speedUnit) {
                case KILOMETERS_PER_HOUR:
                    unitText = this.kmhString;
                    speedLimitThreshold = 13;
                    break;
                default:
                    speedLimitThreshold = 8;
                    unitText = this.mphString;
                    break;
            }
            if (this.updateText) {
                this.mGaugeView.getValueTextView().setTextSize(2, this.mSpeed >= 100 ? (float) this.textSize3Chars : (float) this.textSize2Chars);
                this.mGaugeView.setValueText(java.lang.Integer.toString(this.mSpeed));
            }
            int state = 0;
            if (this.mSpeedLimit == 0) {
                state = 0;
            } else {
                if (this.mSpeed > this.mSpeedLimit) {
                    state = 1;
                }
                if (this.mSpeed > this.mSpeedLimit + speedLimitThreshold) {
                    state = 2;
                }
            }
            int colorRes = 17170443;
            boolean showSpeedLimitLabel = false;
            switch (state) {
                case 0:
                    colorRes = 17170443;
                    break;
                case 1:
                    colorRes = com.navdy.hud.app.R.color.cyan;
                    showSpeedLimitLabel = true;
                    break;
                case 2:
                    colorRes = com.navdy.hud.app.R.color.speed_very_fast_warning_color;
                    showSpeedLimitLabel = true;
                    break;
            }
            this.mGaugeView.getUnitTextView().setTextColor(res.getColor(colorRes));
            this.mSpeedoMeterGaugeDrawable.setState(state);
            this.mSpeedoMeterGaugeDrawable.speedLimitTextAlpha = this.speedLimitAlpha;
            if (showSpeedLimitLabel) {
                this.mGaugeView.setUnitText(res.getString(com.navdy.hud.app.R.string.speed_limit_with_unit, new java.lang.Object[]{java.lang.Integer.valueOf(this.mSpeedLimit), unitText}));
            } else {
                this.mGaugeView.setUnitText(unitText);
            }
        }
    }

    public java.lang.String getWidgetIdentifier() {
        return ID;
    }

    public java.lang.String getWidgetName() {
        return this.speedoMeterWidgetName;
    }

    public void setSpeedLimit(int speedLimit) {
        if (speedLimit != this.mSpeedLimit) {
            this.mSpeedLimit = speedLimit;
            showSpeedLimitText();
            if (this.mSpeed < this.mSpeedLimit) {
                showSpeedLimitTextAndFadeOut();
            }
            reDraw();
        }
    }

    private void showSpeedLimitText() {
        this.handler.removeCallbacks(this.speedLimitFadeOutAnimationRunnable);
        this.speedLimitAlpha = 255;
    }

    private void showSpeedLimitTextAndFadeOut() {
        this.speedLimitAlpha = 255;
    }

    public void setView(com.navdy.hud.app.view.DashboardWidgetView dashboardWidgetView) {
        if (dashboardWidgetView != null) {
            dashboardWidgetView.setContentView((int) com.navdy.hud.app.R.layout.speedo_meter_gauge_2);
        }
        super.setView(dashboardWidgetView);
        if (this.mGaugeView != null) {
            if (android.os.Build.VERSION.SDK_INT >= 21) {
                android.util.TypedValue outValue = new android.util.TypedValue();
                this.mGaugeView.getResources().getValue(com.navdy.hud.app.R.dimen.speedometer_text_letter_spacing, outValue, true);
                this.mGaugeView.getValueTextView().setLetterSpacing(outValue.getFloat());
            }
            reDraw();
        }
    }

    public android.graphics.drawable.Drawable getDrawable() {
        return this.mSpeedoMeterGaugeDrawable;
    }

    public float getValue() {
        return (float) this.mSpeed;
    }

    public void setValue(float newValue) {
        int newSpeed = (int) newValue;
        if (newSpeed != this.mSpeed) {
            this.mSpeed = newSpeed;
            this.updateText = false;
            setSpeedValueInternal((float) this.mSpeed);
        }
    }

    private void setSpeedValueInternal(float speed) {
        if (this.mSpeed >= this.mSpeedLimit) {
            showSpeedLimitText();
        } else if (!this.showingSpeedLimitAnimation && this.speedLimitMarkerNeedsTobeRemoved) {
            showSpeedLimitTextAndFadeOut();
        }
        reDraw();
    }

    public void animationComplete(float newValue) {
        this.updateText = true;
        setSpeedValueInternal(newValue);
    }
}
