package com.navdy.hud.app.view;

public class GForcePresenter extends com.navdy.hud.app.view.DashboardWidgetPresenter {
    com.navdy.hud.app.view.GForceDrawable drawable;
    private java.lang.String gMeterGaugeName;
    private boolean registered;
    private float xAccel;
    private float yAccel;
    private float zAccel;

    public GForcePresenter(android.content.Context context) {
        this.drawable = new com.navdy.hud.app.view.GForceDrawable(context);
        this.gMeterGaugeName = context.getResources().getString(com.navdy.hud.app.R.string.widget_g_meter);
    }

    public void setView(com.navdy.hud.app.view.DashboardWidgetView dashboardWidgetView) {
        if (dashboardWidgetView != null) {
            dashboardWidgetView.setContentView((int) com.navdy.hud.app.R.layout.smart_dash_widget_circular_content_layout);
        }
        super.setView(dashboardWidgetView);
    }

    /* access modifiers changed from: protected */
    public boolean isRegisteringToBusRequired() {
        return true;
    }

    @com.squareup.otto.Subscribe
    public void onCalibratedGForceData(com.navdy.hud.app.device.gps.CalibratedGForceData calibratedGForceData) {
        this.xAccel = calibratedGForceData.getXAccel();
        this.yAccel = calibratedGForceData.getYAccel();
        this.zAccel = calibratedGForceData.getZAccel();
        reDraw();
    }

    public android.graphics.drawable.Drawable getDrawable() {
        return this.drawable;
    }

    /* access modifiers changed from: protected */
    public void updateGauge() {
        this.drawable.setAcceleration(this.xAccel, this.yAccel, this.zAccel);
    }

    public java.lang.String getWidgetIdentifier() {
        return com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.GFORCE_WIDGET_ID;
    }

    public java.lang.String getWidgetName() {
        return this.gMeterGaugeName;
    }
}
