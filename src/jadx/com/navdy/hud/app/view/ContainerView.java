package com.navdy.hud.app.view;

public class ContainerView extends android.widget.FrameLayout implements com.navdy.hud.app.util.CanShowScreen<mortar.Blueprint>, com.navdy.hud.app.util.Pauseable {
    private com.navdy.hud.app.util.ScreenConductor<mortar.Blueprint> screenMaestro;
    @javax.inject.Inject
    com.navdy.hud.app.ui.framework.UIStateManager uiStateManager;

    public ContainerView(android.content.Context context, android.util.AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) {
            mortar.Mortar.inject(context, this);
        }
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
        this.screenMaestro = new com.navdy.hud.app.util.ScreenConductor<>(getContext(), (android.view.ViewGroup) findViewById(com.navdy.hud.app.R.id.screenContainer), this.uiStateManager);
    }

    public void showScreen(mortar.Blueprint screen, flow.Flow.Direction direction, int animIn, int animOut) {
        this.screenMaestro.showScreen(screen, direction, animIn, animOut);
    }

    public void onPause() {
        android.view.View view = getCurrentView();
        if (view instanceof com.navdy.hud.app.util.Pauseable) {
            ((com.navdy.hud.app.util.Pauseable) view).onPause();
        }
    }

    public void onResume() {
        android.view.View view = getCurrentView();
        if (view instanceof com.navdy.hud.app.util.Pauseable) {
            ((com.navdy.hud.app.util.Pauseable) view).onResume();
        }
    }

    public android.view.View getCurrentView() {
        return this.screenMaestro.getChildView();
    }

    public android.animation.Animator getCustomContainerAnimator(com.navdy.hud.app.view.MainView.CustomAnimationMode mode) {
        android.view.View view = getCurrentView();
        if (view == null || !(view instanceof com.navdy.hud.app.view.ICustomAnimator)) {
            return null;
        }
        return ((com.navdy.hud.app.view.ICustomAnimator) view).getCustomAnimator(mode);
    }

    public void createScreens() {
        this.screenMaestro.createScreens();
    }
}
