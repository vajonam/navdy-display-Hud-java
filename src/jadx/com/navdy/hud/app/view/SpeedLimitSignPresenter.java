package com.navdy.hud.app.view;

public class SpeedLimitSignPresenter extends com.navdy.hud.app.view.DashboardWidgetPresenter {
    private int speedLimit;
    @butterknife.InjectView(2131624416)
    protected com.navdy.hud.app.view.SpeedLimitSignView speedLimitSignView;
    private java.lang.String speedLimitSignWidgetName;
    @butterknife.InjectView(2131624417)
    protected android.widget.TextView speedLimitUnavailableText;
    private com.navdy.hud.app.manager.SpeedManager speedManager = com.navdy.hud.app.manager.SpeedManager.getInstance();

    public SpeedLimitSignPresenter(android.content.Context context) {
        this.speedLimitSignWidgetName = context.getResources().getString(com.navdy.hud.app.R.string.widget_speed_limit);
    }

    public void setSpeedLimit(int speedLimit2) {
        this.speedLimit = speedLimit2;
        reDraw();
    }

    public void setView(com.navdy.hud.app.view.DashboardWidgetView dashboardWidgetView, android.os.Bundle arguments) {
        if (dashboardWidgetView != null) {
            dashboardWidgetView.setContentView((int) com.navdy.hud.app.R.layout.speed_limit_sign_gauge);
            butterknife.ButterKnife.inject((java.lang.Object) this, (android.view.View) dashboardWidgetView);
        }
        super.setView(dashboardWidgetView, arguments);
    }

    /* access modifiers changed from: protected */
    public boolean isRegisteringToBusRequired() {
        return true;
    }

    public android.graphics.drawable.Drawable getDrawable() {
        return null;
    }

    /* access modifiers changed from: protected */
    public void updateGauge() {
        if (this.speedLimit > 0) {
            this.speedLimitUnavailableText.setVisibility(8);
            this.speedLimitSignView.setVisibility(0);
            this.speedLimitSignView.setSpeedLimitUnit(this.speedManager.getSpeedUnit());
            this.speedLimitSignView.setSpeedLimit(this.speedLimit);
            return;
        }
        this.speedLimitUnavailableText.setVisibility(0);
        this.speedLimitSignView.setVisibility(8);
    }

    public java.lang.String getWidgetIdentifier() {
        return com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.SPEED_LIMIT_SIGN_GAUGE_ID;
    }

    public java.lang.String getWidgetName() {
        return this.speedLimitSignWidgetName;
    }

    @com.squareup.otto.Subscribe
    public void onSpeedUnitChanged(com.navdy.hud.app.manager.SpeedManager.SpeedUnitChanged unitChanged) {
        reDraw();
    }
}
