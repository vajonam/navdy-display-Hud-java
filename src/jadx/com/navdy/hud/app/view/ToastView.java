package com.navdy.hud.app.view;

public class ToastView extends android.widget.FrameLayout implements com.navdy.hud.app.manager.InputManager.IInputHandler {
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.view.ToastView.class);
    /* access modifiers changed from: private */
    public com.squareup.otto.Bus bus;
    public boolean gestureOn;
    com.navdy.hud.app.manager.InputManager inputManager;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.ui.activity.Main mainScreen;
    @butterknife.InjectView(2131624127)
    com.navdy.hud.app.ui.component.ConfirmationLayout mainView;
    /* access modifiers changed from: private */
    public volatile boolean sendShowEvent;

    class Anon1 implements java.lang.Runnable {
        final /* synthetic */ java.lang.String val$id;
        final /* synthetic */ java.lang.String val$screenName;
        final /* synthetic */ java.lang.String val$tts;

        Anon1(java.lang.String str, java.lang.String str2, java.lang.String str3) {
            this.val$tts = str;
            this.val$id = str2;
            this.val$screenName = str3;
        }

        public void run() {
            if (this.val$tts != null) {
                com.navdy.hud.app.framework.voice.TTSUtils.sendSpeechRequest(this.val$tts, this.val$id);
            }
            if (this.val$screenName != null) {
                try {
                    com.navdy.hud.app.view.ToastView.sLogger.v("in dismiss screen:" + this.val$screenName);
                    com.navdy.hud.app.view.ToastView.this.bus.post(new com.navdy.service.library.events.ui.ShowScreen.Builder().screen(com.navdy.service.library.events.ui.Screen.valueOf(this.val$screenName)).build());
                    com.navdy.hud.app.framework.toast.ToastPresenter.clearScreenName();
                } catch (Throwable t) {
                    com.navdy.hud.app.view.ToastView.sLogger.e(t);
                }
            }
            com.navdy.hud.app.view.ToastView.this.bus.post(new com.navdy.hud.app.framework.toast.ToastManager.ShowToast(com.navdy.hud.app.framework.toast.ToastPresenter.getCurrentId()));
            com.navdy.hud.app.view.ToastView.this.sendShowEvent = true;
        }
    }

    class Anon2 implements java.lang.Runnable {
        final /* synthetic */ java.lang.String val$screenName;

        Anon2(java.lang.String str) {
            this.val$screenName = str;
        }

        public void run() {
            com.navdy.hud.app.view.ToastView.sLogger.v("toast:animationIn :" + this.val$screenName);
            com.navdy.hud.app.view.ToastView.sLogger.v("[focus] toastView");
            com.navdy.hud.app.view.ToastView.this.inputManager.setFocus(com.navdy.hud.app.view.ToastView.this);
            com.navdy.hud.app.view.ToastView.this.setVisibility(0);
            com.navdy.hud.app.framework.toast.ToastManager.getInstance().setToastDisplayFlag(true);
        }
    }

    class Anon3 implements java.lang.Runnable {
        Anon3() {
        }

        public void run() {
            com.navdy.hud.app.view.ToastView.sLogger.v("toast:animationOut");
            if (com.navdy.hud.app.view.ToastView.this.mainScreen == null) {
                com.navdy.hud.app.view.ToastView.this.mainScreen = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getUiStateManager().getRootScreen();
            }
            if (com.navdy.hud.app.view.ToastView.this.mainScreen != null) {
                com.navdy.hud.app.view.ToastView.this.inputManager.setFocus(null);
                com.navdy.hud.app.view.ToastView.this.mainScreen.setInputFocus();
            }
            com.navdy.hud.app.view.ToastView.this.setVisibility(8);
            com.navdy.hud.app.view.ToastView.this.revertToOriginal();
            com.navdy.hud.app.framework.toast.ToastPresenter.clear();
            com.navdy.hud.app.framework.toast.ToastManager.getInstance().setToastDisplayFlag(false);
        }
    }

    class Anon4 implements java.lang.Runnable {
        Anon4() {
        }

        public void run() {
            java.lang.String screenName = com.navdy.hud.app.framework.toast.ToastPresenter.getScreenName();
            if (screenName != null) {
                try {
                    com.navdy.hud.app.view.ToastView.sLogger.v("out dismiss screen:" + screenName);
                    com.navdy.hud.app.view.ToastView.this.bus.post(new com.navdy.service.library.events.ui.ShowScreen.Builder().screen(com.navdy.service.library.events.ui.Screen.valueOf(screenName)).build());
                    com.navdy.hud.app.framework.toast.ToastPresenter.clearScreenName();
                } catch (Throwable t) {
                    com.navdy.hud.app.view.ToastView.sLogger.e(t);
                }
            }
        }
    }

    public ToastView(android.content.Context context) {
        this(context, null);
    }

    public ToastView(android.content.Context context, android.util.AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ToastView(android.content.Context context, android.util.AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        com.navdy.hud.app.manager.RemoteDeviceManager remoteDeviceManager = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance();
        this.inputManager = remoteDeviceManager.getInputManager();
        this.bus = remoteDeviceManager.getBus();
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
        butterknife.ButterKnife.inject((android.view.View) this);
    }

    public com.navdy.hud.app.ui.component.ConfirmationLayout getMainLayout() {
        return this.mainView;
    }

    public boolean onGesture(com.navdy.service.library.events.input.GestureEvent event) {
        if (this.gestureOn && event.gesture != null) {
            com.navdy.hud.app.framework.toast.IToastCallback callback = com.navdy.hud.app.framework.toast.ToastPresenter.getCurrentCallback();
            if (callback == null) {
                return false;
            }
            java.util.List<com.navdy.hud.app.ui.component.ChoiceLayout.Choice> choice = this.mainView.choiceLayout.getChoices();
            if (choice == null || choice.size() == 0) {
                return false;
            }
            switch (event.gesture) {
                case GESTURE_SWIPE_LEFT:
                    callback.executeChoiceItem(0, ((com.navdy.hud.app.ui.component.ChoiceLayout.Choice) choice.get(0)).id);
                    return true;
                case GESTURE_SWIPE_RIGHT:
                    if (choice.size() == 1) {
                        callback.executeChoiceItem(0, ((com.navdy.hud.app.ui.component.ChoiceLayout.Choice) choice.get(0)).id);
                    } else {
                        callback.executeChoiceItem(1, ((com.navdy.hud.app.ui.component.ChoiceLayout.Choice) choice.get(1)).id);
                    }
                    return true;
            }
        }
        return false;
    }

    public boolean onKey(com.navdy.hud.app.manager.InputManager.CustomKeyEvent event) {
        com.navdy.hud.app.framework.toast.IToastCallback cb = com.navdy.hud.app.framework.toast.ToastPresenter.getCurrentCallback();
        if (cb != null && cb.onKey(event)) {
            return true;
        }
        switch (event) {
            case LEFT:
                this.mainView.choiceLayout.moveSelectionLeft();
                return true;
            case RIGHT:
                this.mainView.choiceLayout.moveSelectionRight();
                return true;
            case SELECT:
                if (!com.navdy.hud.app.framework.toast.ToastPresenter.hasTimeout() || this.mainView.choiceLayout.getVisibility() == 0) {
                    this.mainView.choiceLayout.executeSelectedItem(true);
                    return true;
                }
                dismissToast();
                return true;
            case POWER_BUTTON_LONG_PRESS:
            case LONG_PRESS:
                return false;
            default:
                return true;
        }
    }

    public com.navdy.hud.app.manager.InputManager.IInputHandler nextHandler() {
        return null;
    }

    public com.navdy.hud.app.ui.component.ConfirmationLayout getConfirmation() {
        return this.mainView;
    }

    public void dismissToast() {
        if (com.navdy.hud.app.framework.toast.ToastManager.getInstance().isToastDisplayed()) {
            animateOut(false);
        }
    }

    public void animateIn(java.lang.String tts, java.lang.String screenName, java.lang.String id, boolean noStartDelay) {
        this.sendShowEvent = false;
        this.mainView.setTranslationY((float) com.navdy.hud.app.framework.toast.ToastPresenter.ANIMATION_TRANSLATION);
        this.mainView.setAlpha(0.0f);
        this.mainView.animate().alpha(1.0f).translationY(0.0f).setDuration(noStartDelay ? 0 : 100).setStartDelay(250).withStartAction(new com.navdy.hud.app.view.ToastView.Anon2(screenName)).withEndAction(new com.navdy.hud.app.view.ToastView.Anon1(tts, id, screenName)).start();
    }

    public void animateOut(boolean quickly) {
        int duration;
        if (quickly) {
            duration = 50;
        } else {
            duration = 100;
        }
        if (!this.sendShowEvent) {
            this.bus.post(new com.navdy.hud.app.framework.toast.ToastManager.ShowToast(com.navdy.hud.app.framework.toast.ToastPresenter.getCurrentId()));
            this.sendShowEvent = true;
        }
        this.mainView.animate().alpha(0.0f).translationY((float) com.navdy.hud.app.framework.toast.ToastPresenter.ANIMATION_TRANSLATION).setDuration((long) duration).withStartAction(new com.navdy.hud.app.view.ToastView.Anon4()).withEndAction(new com.navdy.hud.app.view.ToastView.Anon3()).start();
    }

    public void revertToOriginal() {
        this.mainView.fluctuatorView.stop();
        this.gestureOn = false;
    }

    public com.navdy.hud.app.ui.component.ConfirmationLayout getView() {
        return this.mainView;
    }
}
