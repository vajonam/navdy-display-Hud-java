package com.navdy.hud.app.view;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000|\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u001d\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\bJ\n\u0010P\u001a\u0004\u0018\u00010QH\u0016J\b\u0010R\u001a\u00020\u001cH\u0016J\b\u0010S\u001a\u00020\u001cH\u0016J\b\u0010T\u001a\u00020\u0007H\u0014J\u0010\u0010U\u001a\u00020V2\u0006\u0010'\u001a\u00020&H\u0007J\b\u0010W\u001a\u00020VH\u0016J\u001c\u0010X\u001a\u00020V2\b\u0010Y\u001a\u0004\u0018\u00010Z2\b\u0010[\u001a\u0004\u0018\u00010\\H\u0016J\u0010\u0010]\u001a\u00020V2\u0006\u0010^\u001a\u00020\u0007H\u0016J\u0006\u0010_\u001a\u00020VJ\u0006\u0010`\u001a\u00020VJ\b\u0010a\u001a\u00020VH\u0014R\u0014\u0010\t\u001a\u00020\nX\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u001a\u0010\r\u001a\u00020\u000eX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000f\u0010\u0010\"\u0004\b\u0011\u0010\u0012R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014R\u001a\u0010\u0015\u001a\u00020\u0016X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0017\u0010\u0018\"\u0004\b\u0019\u0010\u001aR\u0011\u0010\u001b\u001a\u00020\u001c\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u001eR\u001a\u0010\u001f\u001a\u00020 X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b!\u0010\"\"\u0004\b#\u0010$R$\u0010'\u001a\u00020&2\u0006\u0010%\u001a\u00020&@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b(\u0010)\"\u0004\b*\u0010+R\u0011\u0010,\u001a\u00020-\u00a2\u0006\b\n\u0000\u001a\u0004\b.\u0010/R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b0\u00101R\u001e\u00102\u001a\u00020\u00072\u0006\u0010%\u001a\u00020\u0007@BX\u0082\u000e\u00a2\u0006\b\n\u0000\"\u0004\b3\u00104R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\b\n\u0000\u001a\u0004\b5\u00106R\u001a\u00107\u001a\u00020 X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b8\u0010\"\"\u0004\b9\u0010$R\u0011\u0010:\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b;\u00101R\u0011\u0010<\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b=\u00101R\u0011\u0010>\u001a\u00020\u001c\u00a2\u0006\b\n\u0000\u001a\u0004\b?\u0010\u001eR\u0011\u0010@\u001a\u00020\u001c\u00a2\u0006\b\n\u0000\u001a\u0004\bA\u0010\u001eR\u0011\u0010B\u001a\u00020\u001c\u00a2\u0006\b\n\u0000\u001a\u0004\bC\u0010\u001eR\u0011\u0010D\u001a\u00020\u001c\u00a2\u0006\b\n\u0000\u001a\u0004\bE\u0010\u001eR\u0011\u0010F\u001a\u00020\u001c\u00a2\u0006\b\n\u0000\u001a\u0004\bG\u0010\u001eR\u0011\u0010H\u001a\u00020\u001c\u00a2\u0006\b\n\u0000\u001a\u0004\bI\u0010\u001eR\u001a\u0010J\u001a\u00020KX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\bL\u0010M\"\u0004\bN\u0010O\u00a8\u0006b"}, d2 = {"Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;", "Lcom/navdy/hud/app/view/DashboardWidgetPresenter;", "context", "Landroid/content/Context;", "layoutId", "", "showScoreMeter", "", "(Landroid/content/Context;IZ)V", "CLEAR_WARNING_TIME_MS", "", "getCLEAR_WARNING_TIME_MS", "()J", "clearWarning", "Ljava/lang/Runnable;", "getClearWarning", "()Ljava/lang/Runnable;", "setClearWarning", "(Ljava/lang/Runnable;)V", "getContext", "()Landroid/content/Context;", "driveScoreGaugeDrawable", "Lcom/navdy/hud/app/view/drawable/DriveScoreGaugeDrawable;", "getDriveScoreGaugeDrawable", "()Lcom/navdy/hud/app/view/drawable/DriveScoreGaugeDrawable;", "setDriveScoreGaugeDrawable", "(Lcom/navdy/hud/app/view/drawable/DriveScoreGaugeDrawable;)V", "driveScoreGaugeName", "", "getDriveScoreGaugeName", "()Ljava/lang/String;", "driveScoreText", "Landroid/widget/TextView;", "getDriveScoreText", "()Landroid/widget/TextView;", "setDriveScoreText", "(Landroid/widget/TextView;)V", "value", "Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;", "driveScoreUpdated", "getDriveScoreUpdated", "()Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;", "setDriveScoreUpdated", "(Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;)V", "handler", "Landroid/os/Handler;", "getHandler", "()Landroid/os/Handler;", "getLayoutId", "()I", "newEvent", "setNewEvent", "(Z)V", "getShowScoreMeter", "()Z", "summaryText", "getSummaryText", "setSummaryText", "summaryTextColorNormal", "getSummaryTextColorNormal", "summaryTextColorWarning", "getSummaryTextColorWarning", "summaryTextDriveScore", "getSummaryTextDriveScore", "summaryTextExcessiveSpeeding", "getSummaryTextExcessiveSpeeding", "summaryTextHardAcceleration", "getSummaryTextHardAcceleration", "summaryTextHardBraking", "getSummaryTextHardBraking", "summaryTextHighG", "getSummaryTextHighG", "summaryTextSpeeding", "getSummaryTextSpeeding", "warningImage", "Landroid/widget/ImageView;", "getWarningImage", "()Landroid/widget/ImageView;", "setWarningImage", "(Landroid/widget/ImageView;)V", "getDrawable", "Landroid/graphics/drawable/Drawable;", "getWidgetIdentifier", "getWidgetName", "isRegisteringToBusRequired", "onDriveScoreUpdated", "", "onResume", "setView", "dashboardWidgetView", "Lcom/navdy/hud/app/view/DashboardWidgetView;", "arguments", "Landroid/os/Bundle;", "setWidgetVisibleToUser", "b", "showHighG", "showSpeeding", "updateGauge", "app_hudDebug"}, k = 1, mv = {1, 1, 6})
/* compiled from: DriveScoreGaugePresenter.kt */
public final class DriveScoreGaugePresenter extends com.navdy.hud.app.view.DashboardWidgetPresenter {
    private final long CLEAR_WARNING_TIME_MS = 2000;
    @org.jetbrains.annotations.NotNull
    private java.lang.Runnable clearWarning;
    @org.jetbrains.annotations.NotNull
    private final android.content.Context context;
    @org.jetbrains.annotations.NotNull
    private com.navdy.hud.app.view.drawable.DriveScoreGaugeDrawable driveScoreGaugeDrawable = new com.navdy.hud.app.view.drawable.DriveScoreGaugeDrawable(this.context, com.navdy.hud.app.R.array.drive_state_colors);
    @org.jetbrains.annotations.NotNull
    private final java.lang.String driveScoreGaugeName;
    @org.jetbrains.annotations.NotNull
    public android.widget.TextView driveScoreText;
    @org.jetbrains.annotations.NotNull
    private com.navdy.hud.app.analytics.TelemetryDataManager.DriveScoreUpdated driveScoreUpdated;
    @org.jetbrains.annotations.NotNull
    private final android.os.Handler handler;
    private final int layoutId;
    /* access modifiers changed from: private */
    public boolean newEvent;
    private final boolean showScoreMeter;
    @org.jetbrains.annotations.NotNull
    public android.widget.TextView summaryText;
    private final int summaryTextColorNormal;
    private final int summaryTextColorWarning;
    @org.jetbrains.annotations.NotNull
    private final java.lang.String summaryTextDriveScore;
    @org.jetbrains.annotations.NotNull
    private final java.lang.String summaryTextExcessiveSpeeding;
    @org.jetbrains.annotations.NotNull
    private final java.lang.String summaryTextHardAcceleration;
    @org.jetbrains.annotations.NotNull
    private final java.lang.String summaryTextHardBraking;
    @org.jetbrains.annotations.NotNull
    private final java.lang.String summaryTextHighG;
    @org.jetbrains.annotations.NotNull
    private final java.lang.String summaryTextSpeeding;
    @org.jetbrains.annotations.NotNull
    public android.widget.ImageView warningImage;

    public DriveScoreGaugePresenter(@org.jetbrains.annotations.NotNull android.content.Context context2, int layoutId2, boolean showScoreMeter2) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(context2, "context");
        this.context = context2;
        this.layoutId = layoutId2;
        this.showScoreMeter = showScoreMeter2;
        com.navdy.hud.app.view.drawable.DriveScoreGaugeDrawable driveScoreGaugeDrawable2 = this.driveScoreGaugeDrawable;
        if (driveScoreGaugeDrawable2 != null) {
            driveScoreGaugeDrawable2.setMinValue(0.0f);
        }
        com.navdy.hud.app.view.drawable.DriveScoreGaugeDrawable driveScoreGaugeDrawable3 = this.driveScoreGaugeDrawable;
        if (driveScoreGaugeDrawable3 != null) {
            driveScoreGaugeDrawable3.setMaxGaugeValue(100.0f);
        }
        this.handler = new android.os.Handler(android.os.Looper.getMainLooper());
        android.content.res.Resources resources = this.context.getResources();
        java.lang.String string = resources.getString(com.navdy.hud.app.R.string.drive_score);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(string, "resources.getString(R.string.drive_score)");
        this.summaryTextDriveScore = string;
        java.lang.String string2 = resources.getString(com.navdy.hud.app.R.string.hard_accel);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(string2, "resources.getString(R.string.hard_accel)");
        this.summaryTextHardAcceleration = string2;
        java.lang.String string3 = resources.getString(com.navdy.hud.app.R.string.hard_braking);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(string3, "resources.getString(R.string.hard_braking)");
        this.summaryTextHardBraking = string3;
        java.lang.String string4 = resources.getString(com.navdy.hud.app.R.string.speeding);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(string4, "resources.getString(R.string.speeding)");
        this.summaryTextSpeeding = string4;
        java.lang.String string5 = resources.getString(com.navdy.hud.app.R.string.widget_drive_score);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(string5, "resources.getString(R.string.widget_drive_score)");
        this.driveScoreGaugeName = string5;
        java.lang.String string6 = resources.getString(com.navdy.hud.app.R.string.excessive_speeding);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(string6, "resources.getString(R.string.excessive_speeding)");
        this.summaryTextExcessiveSpeeding = string6;
        java.lang.String string7 = resources.getString(com.navdy.hud.app.R.string.high_g);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(string7, "resources.getString(R.string.high_g)");
        this.summaryTextHighG = string7;
        this.summaryTextColorNormal = this.showScoreMeter ? resources.getColor(17170443) : resources.getColor(com.navdy.hud.app.R.color.grey_fluctuator);
        this.summaryTextColorWarning = resources.getColor(com.navdy.hud.app.R.color.driving_score_warning_color);
        this.clearWarning = new com.navdy.hud.app.view.DriveScoreGaugePresenter$clearWarning$Anon1(this);
        this.driveScoreUpdated = new com.navdy.hud.app.analytics.TelemetryDataManager.DriveScoreUpdated(com.navdy.hud.app.analytics.TelemetrySession.InterestingEvent.NONE, false, false, false, 100);
    }

    @org.jetbrains.annotations.NotNull
    public final android.content.Context getContext() {
        return this.context;
    }

    public final int getLayoutId() {
        return this.layoutId;
    }

    public final boolean getShowScoreMeter() {
        return this.showScoreMeter;
    }

    public final long getCLEAR_WARNING_TIME_MS() {
        return this.CLEAR_WARNING_TIME_MS;
    }

    @org.jetbrains.annotations.NotNull
    public final com.navdy.hud.app.view.drawable.DriveScoreGaugeDrawable getDriveScoreGaugeDrawable() {
        return this.driveScoreGaugeDrawable;
    }

    public final void setDriveScoreGaugeDrawable(@org.jetbrains.annotations.NotNull com.navdy.hud.app.view.drawable.DriveScoreGaugeDrawable driveScoreGaugeDrawable2) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(driveScoreGaugeDrawable2, "<set-?>");
        this.driveScoreGaugeDrawable = driveScoreGaugeDrawable2;
    }

    @org.jetbrains.annotations.NotNull
    public final android.widget.TextView getDriveScoreText() {
        android.widget.TextView textView = this.driveScoreText;
        if (textView == null) {
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException("driveScoreText");
        }
        return textView;
    }

    public final void setDriveScoreText(@org.jetbrains.annotations.NotNull android.widget.TextView textView) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(textView, "<set-?>");
        this.driveScoreText = textView;
    }

    @org.jetbrains.annotations.NotNull
    public final android.widget.TextView getSummaryText() {
        android.widget.TextView textView = this.summaryText;
        if (textView == null) {
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException("summaryText");
        }
        return textView;
    }

    public final void setSummaryText(@org.jetbrains.annotations.NotNull android.widget.TextView textView) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(textView, "<set-?>");
        this.summaryText = textView;
    }

    @org.jetbrains.annotations.NotNull
    public final android.widget.ImageView getWarningImage() {
        android.widget.ImageView imageView = this.warningImage;
        if (imageView == null) {
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException("warningImage");
        }
        return imageView;
    }

    public final void setWarningImage(@org.jetbrains.annotations.NotNull android.widget.ImageView imageView) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(imageView, "<set-?>");
        this.warningImage = imageView;
    }

    @org.jetbrains.annotations.NotNull
    public final android.os.Handler getHandler() {
        return this.handler;
    }

    @org.jetbrains.annotations.NotNull
    public final java.lang.String getSummaryTextDriveScore() {
        return this.summaryTextDriveScore;
    }

    @org.jetbrains.annotations.NotNull
    public final java.lang.String getSummaryTextHardAcceleration() {
        return this.summaryTextHardAcceleration;
    }

    @org.jetbrains.annotations.NotNull
    public final java.lang.String getSummaryTextHardBraking() {
        return this.summaryTextHardBraking;
    }

    @org.jetbrains.annotations.NotNull
    public final java.lang.String getSummaryTextHighG() {
        return this.summaryTextHighG;
    }

    @org.jetbrains.annotations.NotNull
    public final java.lang.String getDriveScoreGaugeName() {
        return this.driveScoreGaugeName;
    }

    @org.jetbrains.annotations.NotNull
    public final java.lang.String getSummaryTextSpeeding() {
        return this.summaryTextSpeeding;
    }

    @org.jetbrains.annotations.NotNull
    public final java.lang.String getSummaryTextExcessiveSpeeding() {
        return this.summaryTextExcessiveSpeeding;
    }

    public final int getSummaryTextColorNormal() {
        return this.summaryTextColorNormal;
    }

    public final int getSummaryTextColorWarning() {
        return this.summaryTextColorWarning;
    }

    @org.jetbrains.annotations.NotNull
    public final java.lang.Runnable getClearWarning() {
        return this.clearWarning;
    }

    public final void setClearWarning(@org.jetbrains.annotations.NotNull java.lang.Runnable runnable) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(runnable, "<set-?>");
        this.clearWarning = runnable;
    }

    @org.jetbrains.annotations.NotNull
    public final com.navdy.hud.app.analytics.TelemetryDataManager.DriveScoreUpdated getDriveScoreUpdated() {
        return this.driveScoreUpdated;
    }

    public final void setDriveScoreUpdated(@org.jetbrains.annotations.NotNull com.navdy.hud.app.analytics.TelemetryDataManager.DriveScoreUpdated value) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(value, com.glympse.android.hal.NotificationListener.INTENT_EXTRA_VALUE);
        this.driveScoreUpdated = value;
        if (!kotlin.jvm.internal.Intrinsics.areEqual((java.lang.Object) value.getInterestingEvent(), (java.lang.Object) com.navdy.hud.app.analytics.TelemetrySession.InterestingEvent.NONE)) {
            setNewEvent(true);
        }
        reDraw();
    }

    /* access modifiers changed from: private */
    public final void setNewEvent(boolean value) {
        this.newEvent = value;
    }

    @com.squareup.otto.Subscribe
    public final void onDriveScoreUpdated(@org.jetbrains.annotations.NotNull com.navdy.hud.app.analytics.TelemetryDataManager.DriveScoreUpdated driveScoreUpdated2) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(driveScoreUpdated2, "driveScoreUpdated");
        setDriveScoreUpdated(driveScoreUpdated2);
    }

    /* access modifiers changed from: protected */
    public boolean isRegisteringToBusRequired() {
        return true;
    }

    public void setView(@org.jetbrains.annotations.Nullable com.navdy.hud.app.view.DashboardWidgetView dashboardWidgetView, @org.jetbrains.annotations.Nullable android.os.Bundle arguments) {
        int layoutResourceId = this.layoutId;
        if (dashboardWidgetView != null) {
            setDriveScoreUpdated(com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getTelemetryDataManager().getDriveScoreUpdatedEvent());
            setNewEvent(false);
            dashboardWidgetView.setContentView(layoutResourceId);
            android.view.View findViewById = dashboardWidgetView.findViewById(com.navdy.hud.app.R.id.txt_value);
            if (findViewById == null) {
                throw new kotlin.TypeCastException("null cannot be cast to non-null type android.widget.TextView");
            }
            this.driveScoreText = (android.widget.TextView) findViewById;
            android.view.View findViewById2 = dashboardWidgetView.findViewById(com.navdy.hud.app.R.id.txt_summary);
            if (findViewById2 == null) {
                throw new kotlin.TypeCastException("null cannot be cast to non-null type android.widget.TextView");
            }
            this.summaryText = (android.widget.TextView) findViewById2;
            android.view.View findViewById3 = dashboardWidgetView.findViewById(com.navdy.hud.app.R.id.img_warning);
            if (findViewById3 == null) {
                throw new kotlin.TypeCastException("null cannot be cast to non-null type android.widget.ImageView");
            }
            this.warningImage = (android.widget.ImageView) findViewById3;
        }
        super.setView(dashboardWidgetView, arguments);
    }

    @org.jetbrains.annotations.Nullable
    public android.graphics.drawable.Drawable getDrawable() {
        if (this.showScoreMeter) {
            return this.driveScoreGaugeDrawable;
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public void updateGauge() {
        this.logger.d("Update Drive score " + this.driveScoreUpdated);
        if (this.mWidgetView != null) {
            if (this.showScoreMeter) {
                this.driveScoreGaugeDrawable.setGaugeValue((float) this.driveScoreUpdated.getDriveScore());
            }
            if (this.newEvent) {
                if (this.showScoreMeter) {
                    this.driveScoreGaugeDrawable.setState(1);
                }
                android.widget.TextView textView = this.driveScoreText;
                if (textView == null) {
                    kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException("driveScoreText");
                }
                textView.setVisibility(4);
                android.widget.ImageView imageView = this.warningImage;
                if (imageView == null) {
                    kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException("warningImage");
                }
                imageView.setVisibility(0);
                android.widget.TextView textView2 = this.summaryText;
                if (textView2 == null) {
                    kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException("summaryText");
                }
                textView2.setTextColor(this.summaryTextColorWarning);
                if (kotlin.jvm.internal.Intrinsics.areEqual((java.lang.Object) this.driveScoreUpdated.getInterestingEvent(), (java.lang.Object) com.navdy.hud.app.analytics.TelemetrySession.InterestingEvent.HARD_ACCELERATION)) {
                    android.widget.ImageView imageView2 = this.warningImage;
                    if (imageView2 == null) {
                        kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException("warningImage");
                    }
                    imageView2.setImageResource(com.navdy.hud.app.R.drawable.accel);
                    android.widget.TextView textView3 = this.summaryText;
                    if (textView3 == null) {
                        kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException("summaryText");
                    }
                    textView3.setText(this.summaryTextHardAcceleration);
                } else if (kotlin.jvm.internal.Intrinsics.areEqual((java.lang.Object) this.driveScoreUpdated.getInterestingEvent(), (java.lang.Object) com.navdy.hud.app.analytics.TelemetrySession.InterestingEvent.HARD_BRAKING)) {
                    android.widget.ImageView imageView3 = this.warningImage;
                    if (imageView3 == null) {
                        kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException("warningImage");
                    }
                    imageView3.setImageResource(com.navdy.hud.app.R.drawable.brake);
                    android.widget.TextView textView4 = this.summaryText;
                    if (textView4 == null) {
                        kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException("summaryText");
                    }
                    textView4.setText(this.summaryTextHardBraking);
                } else if (this.driveScoreUpdated.isDoingHighGManeuver()) {
                    showHighG();
                } else if (this.driveScoreUpdated.isSpeeding()) {
                    showSpeeding();
                }
                this.handler.removeCallbacks(this.clearWarning);
                this.handler.postDelayed(this.clearWarning, this.CLEAR_WARNING_TIME_MS);
            } else if (this.driveScoreUpdated.isDoingHighGManeuver()) {
                showHighG();
            } else if (this.driveScoreUpdated.isSpeeding()) {
                showSpeeding();
            } else {
                this.handler.removeCallbacks(this.clearWarning);
                if (this.showScoreMeter) {
                    this.driveScoreGaugeDrawable.setState(0);
                }
                android.widget.TextView textView5 = this.driveScoreText;
                if (textView5 == null) {
                    kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException("driveScoreText");
                }
                textView5.setVisibility(0);
                android.widget.TextView textView6 = this.driveScoreText;
                if (textView6 == null) {
                    kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException("driveScoreText");
                }
                textView6.setText(java.lang.String.valueOf(this.driveScoreUpdated.getDriveScore()));
                android.widget.TextView textView7 = this.summaryText;
                if (textView7 == null) {
                    kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException("summaryText");
                }
                textView7.setTextColor(this.summaryTextColorNormal);
                android.widget.TextView textView8 = this.summaryText;
                if (textView8 == null) {
                    kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException("summaryText");
                }
                textView8.setText(this.summaryTextDriveScore);
                android.widget.ImageView imageView4 = this.warningImage;
                if (imageView4 == null) {
                    kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException("warningImage");
                }
                imageView4.setVisibility(4);
            }
        }
    }

    public final void showHighG() {
        if (this.showScoreMeter) {
            this.driveScoreGaugeDrawable.setState(1);
        }
        android.widget.TextView textView = this.driveScoreText;
        if (textView == null) {
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException("driveScoreText");
        }
        textView.setVisibility(4);
        android.widget.ImageView imageView = this.warningImage;
        if (imageView == null) {
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException("warningImage");
        }
        imageView.setVisibility(0);
        android.widget.TextView textView2 = this.summaryText;
        if (textView2 == null) {
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException("summaryText");
        }
        textView2.setTextColor(this.summaryTextColorWarning);
        android.widget.ImageView imageView2 = this.warningImage;
        if (imageView2 == null) {
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException("warningImage");
        }
        imageView2.setImageResource(com.navdy.hud.app.R.drawable.high_g);
        android.widget.TextView textView3 = this.summaryText;
        if (textView3 == null) {
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException("summaryText");
        }
        textView3.setText(this.summaryTextHighG);
    }

    public final void showSpeeding() {
        if (this.showScoreMeter) {
            this.driveScoreGaugeDrawable.setState(1);
        }
        android.widget.TextView textView = this.driveScoreText;
        if (textView == null) {
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException("driveScoreText");
        }
        textView.setVisibility(4);
        android.widget.ImageView imageView = this.warningImage;
        if (imageView == null) {
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException("warningImage");
        }
        imageView.setVisibility(0);
        android.widget.TextView textView2 = this.summaryText;
        if (textView2 == null) {
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException("summaryText");
        }
        textView2.setTextColor(this.summaryTextColorWarning);
        if (this.driveScoreUpdated.isExcessiveSpeeding()) {
            android.widget.ImageView imageView2 = this.warningImage;
            if (imageView2 == null) {
                kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException("warningImage");
            }
            imageView2.setImageResource(com.navdy.hud.app.R.drawable.excessive_speed);
            android.widget.TextView textView3 = this.summaryText;
            if (textView3 == null) {
                kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException("summaryText");
            }
            textView3.setText(this.summaryTextExcessiveSpeeding);
            return;
        }
        android.widget.ImageView imageView3 = this.warningImage;
        if (imageView3 == null) {
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException("warningImage");
        }
        imageView3.setImageResource(com.navdy.hud.app.R.drawable.speed);
        android.widget.TextView textView4 = this.summaryText;
        if (textView4 == null) {
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException("summaryText");
        }
        textView4.setText(this.summaryTextSpeeding);
    }

    public void onResume() {
        setNewEvent(false);
        super.onResume();
    }

    public void setWidgetVisibleToUser(boolean b) {
        setNewEvent(false);
        super.setWidgetVisibleToUser(b);
    }

    @org.jetbrains.annotations.NotNull
    public java.lang.String getWidgetIdentifier() {
        return com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.DRIVE_SCORE_GAUGE_ID;
    }

    @org.jetbrains.annotations.NotNull
    public java.lang.String getWidgetName() {
        return this.driveScoreGaugeName;
    }
}
