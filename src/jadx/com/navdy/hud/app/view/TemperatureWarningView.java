package com.navdy.hud.app.view;

public class TemperatureWarningView extends android.widget.RelativeLayout implements com.navdy.hud.app.ui.component.ChoiceLayout.IListener, com.navdy.hud.app.manager.InputManager.IInputHandler {
    private static final int WARNING_MESSAGE_MAX_WIDTH = 320;
    private static long lastDismissedTime = -1;
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.view.TemperatureWarningView.class);
    @butterknife.InjectView(2131624150)
    com.navdy.hud.app.ui.component.ChoiceLayout mChoiceLayout;
    @butterknife.InjectView(2131624147)
    android.widget.ImageView mIcon;
    @butterknife.InjectView(2131624077)
    android.widget.TextView mMainTitleText;
    @javax.inject.Inject
    com.navdy.hud.app.screen.TemperatureWarningScreen.Presenter mPresenter;
    @butterknife.InjectView(2131624152)
    android.widget.ImageView mRightSwipe;
    @butterknife.InjectView(2131624144)
    android.widget.TextView mScreenTitleText;
    @butterknife.InjectView(2131624076)
    android.widget.TextView mTitle1;
    @butterknife.InjectView(2131624078)
    android.widget.TextView mWarningMessage;

    public TemperatureWarningView(android.content.Context context) {
        super(context);
        if (!isInEditMode()) {
            mortar.Mortar.inject(context, this);
        }
    }

    public TemperatureWarningView(android.content.Context context, android.util.AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) {
            mortar.Mortar.inject(context, this);
        }
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
        butterknife.ButterKnife.inject((android.view.View) this);
        com.navdy.hud.app.framework.toast.ToastManager.getInstance().disableToasts(true);
        this.mScreenTitleText.setText(com.navdy.hud.app.R.string.temperature_title);
        this.mMainTitleText.setText(com.navdy.hud.app.R.string.navdy_too_hot);
        this.mIcon.setImageResource(com.navdy.hud.app.R.drawable.icon_warning_temperature);
        ((com.navdy.hud.app.view.MaxWidthLinearLayout) findViewById(com.navdy.hud.app.R.id.infoContainer)).setMaxWidth(320);
        this.mTitle1.setVisibility(8);
        this.mWarningMessage.setVisibility(0);
        this.mWarningMessage.setText(com.navdy.hud.app.R.string.avoid_overheating);
        this.mWarningMessage.setSingleLine(false);
        java.util.List<com.navdy.hud.app.ui.component.ChoiceLayout.Choice> list = new java.util.ArrayList<>();
        list.add(new com.navdy.hud.app.ui.component.ChoiceLayout.Choice(getContext().getString(com.navdy.hud.app.R.string.dismiss), 0));
        this.mChoiceLayout.setChoices(com.navdy.hud.app.ui.component.ChoiceLayout.Mode.LABEL, list, 0, this);
        this.mRightSwipe.setVisibility(0);
        lastDismissedTime = 0;
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        com.navdy.hud.app.framework.toast.ToastManager.getInstance().disableToasts(false);
        lastDismissedTime = android.os.SystemClock.elapsedRealtime();
    }

    public void executeItem(int pos, int id) {
        this.mPresenter.finish();
    }

    public void itemSelected(int pos, int id) {
    }

    public boolean onGesture(com.navdy.service.library.events.input.GestureEvent event) {
        if (this.mRightSwipe == null || this.mRightSwipe.getVisibility() != 0 || event.gesture != com.navdy.service.library.events.input.Gesture.GESTURE_SWIPE_RIGHT) {
            return false;
        }
        this.mPresenter.finish();
        return true;
    }

    public boolean onKey(com.navdy.hud.app.manager.InputManager.CustomKeyEvent event) {
        if (this.mChoiceLayout == null || this.mChoiceLayout.getVisibility() != 0 || event != com.navdy.hud.app.manager.InputManager.CustomKeyEvent.SELECT) {
            return false;
        }
        this.mChoiceLayout.executeSelectedItem(true);
        return true;
    }

    public com.navdy.hud.app.manager.InputManager.IInputHandler nextHandler() {
        return null;
    }

    public static long getLastDismissedTime() {
        return lastDismissedTime;
    }
}
