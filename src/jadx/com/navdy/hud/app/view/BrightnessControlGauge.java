package com.navdy.hud.app.view;

public class BrightnessControlGauge extends com.navdy.hud.app.view.Gauge {
    private static final int PIVOT_ANGLE = 5;
    private static final int THUMB_ANGLE = 10;
    private int mPivotIndicatorColor;
    private android.graphics.Paint mPivotIndicatorPaint;
    private int mPivotValue;
    private int mThumbColor;
    private android.graphics.Paint mThumbPaint;

    public BrightnessControlGauge(android.content.Context context) {
        this(context, null);
    }

    public BrightnessControlGauge(android.content.Context context, android.util.AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public BrightnessControlGauge(android.content.Context context, android.util.AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        android.content.res.Resources resources = getResources();
        this.mPivotValue = context.getTheme().obtainStyledAttributes(attrs, com.navdy.hud.app.R.styleable.BrightnessControlGauge, 0, 0).getInteger(0, 0);
        this.mThumbColor = resources.getColor(com.navdy.hud.app.R.color.thumb_color);
        this.mPivotIndicatorColor = resources.getColor(com.navdy.hud.app.R.color.pivot_indicator_color);
    }

    /* access modifiers changed from: protected */
    public void initDrawingTools() {
        super.initDrawingTools();
        this.mPivotIndicatorPaint = new android.graphics.Paint();
        this.mPivotIndicatorPaint.setStrokeWidth((float) this.mBackgroundThickness);
        this.mPivotIndicatorPaint.setAntiAlias(true);
        this.mPivotIndicatorPaint.setStrokeCap(android.graphics.Paint.Cap.BUTT);
        this.mPivotIndicatorPaint.setStyle(android.graphics.Paint.Style.STROKE);
        this.mThumbPaint = new android.graphics.Paint();
        this.mThumbPaint.setColor(this.mThumbColor);
        this.mThumbPaint.setStrokeWidth((float) this.mThickness);
        this.mThumbPaint.setAntiAlias(true);
        this.mThumbPaint.setStrokeCap(android.graphics.Paint.Cap.BUTT);
        this.mThumbPaint.setStyle(android.graphics.Paint.Style.STROKE);
    }

    /* access modifiers changed from: protected */
    public void drawGauge(android.graphics.Canvas canvas) {
        float width = (float) getWidth();
        float height = (float) getHeight();
        float radius = getRadius();
        float startAngle = deltaToAngle(this.mPivotValue - this.mMinValue) - (5.0f / 2.0f);
        float rectLeft = (width / 2.0f) - radius;
        float rectTop = (height / 2.0f) - radius;
        float rectRight = (width / 2.0f) + radius;
        float rectBottom = (height / 2.0f) + radius;
        android.graphics.RectF rect = new android.graphics.RectF();
        rect.set(rectLeft, rectTop, rectRight, rectBottom);
        canvas.drawArc(rect, ((float) this.mStartAngle) + startAngle, 5.0f, false, this.mPivotIndicatorPaint);
        if (this.mValue > this.mPivotValue) {
            drawIndicator(canvas, this.mPivotValue, this.mValue);
        } else if (this.mValue < this.mPivotValue) {
            drawIndicator(canvas, this.mValue, this.mPivotValue);
        }
        android.graphics.Canvas canvas2 = canvas;
        canvas2.drawArc(rect, ((float) this.mStartAngle) + (deltaToAngle(this.mValue - this.mMinValue) - (10.0f / 2.0f)), 10.0f, false, this.mThumbPaint);
    }
}
