package com.navdy.hud.app.view;

public class TachometerGaugePresenter extends com.navdy.hud.app.view.GaugeViewPresenter implements com.navdy.hud.app.view.SerialValueAnimator.SerialValueAnimatorAdapter {
    public static final java.lang.String ID = "TACHOMETER_GAUGE_IDENTIFIER";
    private static final int MAX_MARKER_ANIMATION_DELAY = 500;
    private static final int MAX_MARKER_ANIMATION_DURATION = 1000;
    private static final int MAX_RPM_VALUE = 8000;
    private static final int RPM_DIFFERENCE = 100;
    private static final int RPM_WARNING_THRESHOLD = 7000;
    public static final int VALUE_ANIMATION_DURATION = 200;
    int animatedRpm;
    private java.lang.Runnable animationRunnable;
    private boolean fullMode;
    private android.os.Handler handler = new android.os.Handler();
    private final java.lang.String kmhString;
    private int lastRPM = Integer.MIN_VALUE;
    com.navdy.hud.app.view.drawable.TachometerGaugeDrawable mTachometerGaugeDrawable;
    /* access modifiers changed from: private */
    public int maxMarkerAlpha;
    /* access modifiers changed from: private */
    public int maxRpmValue;
    private final java.lang.String mphString;
    int rpm;
    private com.navdy.hud.app.view.SerialValueAnimator serialValueAnimator = new com.navdy.hud.app.view.SerialValueAnimator(this, 200);
    /* access modifiers changed from: private */
    public boolean showingMaxMarkerAnimation;
    int speed;
    private int speedFastWarningColor;
    int speedLimit;
    private com.navdy.hud.app.manager.SpeedManager speedManager = com.navdy.hud.app.manager.SpeedManager.getInstance();
    private int speedSafeColor;
    private int speedVeryFastWarningColor;
    /* access modifiers changed from: private */
    public android.animation.ValueAnimator valueAnimator;
    private java.lang.String widgetName;

    class Anon1 implements java.lang.Runnable {

        /* renamed from: com.navdy.hud.app.view.TachometerGaugePresenter$Anon1$Anon1 reason: collision with other inner class name */
        class C0038Anon1 implements android.animation.ValueAnimator.AnimatorUpdateListener {
            C0038Anon1() {
            }

            public void onAnimationUpdate(android.animation.ValueAnimator animation) {
                com.navdy.hud.app.view.TachometerGaugePresenter.this.maxMarkerAlpha = ((java.lang.Integer) animation.getAnimatedValue()).intValue();
                com.navdy.hud.app.view.TachometerGaugePresenter.this.reDraw();
            }
        }

        class Anon2 implements android.animation.Animator.AnimatorListener {
            Anon2() {
            }

            public void onAnimationStart(android.animation.Animator animation) {
            }

            public void onAnimationEnd(android.animation.Animator animation) {
                com.navdy.hud.app.view.TachometerGaugePresenter.this.maxMarkerAlpha = 0;
                com.navdy.hud.app.view.TachometerGaugePresenter.this.showingMaxMarkerAnimation = false;
                com.navdy.hud.app.view.TachometerGaugePresenter.this.maxRpmValue = 0;
                com.navdy.hud.app.view.TachometerGaugePresenter.this.reDraw();
            }

            public void onAnimationCancel(android.animation.Animator animation) {
            }

            public void onAnimationRepeat(android.animation.Animator animation) {
            }
        }

        Anon1() {
        }

        public void run() {
            com.navdy.hud.app.view.TachometerGaugePresenter.this.valueAnimator = new android.animation.ValueAnimator();
            com.navdy.hud.app.view.TachometerGaugePresenter.this.valueAnimator.setIntValues(new int[]{255, 0});
            com.navdy.hud.app.view.TachometerGaugePresenter.this.valueAnimator.setDuration(1000);
            com.navdy.hud.app.view.TachometerGaugePresenter.this.valueAnimator.addUpdateListener(new com.navdy.hud.app.view.TachometerGaugePresenter.Anon1.C0038Anon1());
            com.navdy.hud.app.view.TachometerGaugePresenter.this.valueAnimator.addListener(new com.navdy.hud.app.view.TachometerGaugePresenter.Anon1.Anon2());
            com.navdy.hud.app.view.TachometerGaugePresenter.this.valueAnimator.start();
        }
    }

    public TachometerGaugePresenter(android.content.Context context) {
        this.mTachometerGaugeDrawable = new com.navdy.hud.app.view.drawable.TachometerGaugeDrawable(context, 0);
        this.mTachometerGaugeDrawable.setMaxGaugeValue(8000.0f);
        this.mTachometerGaugeDrawable.setRPMWarningLevel(RPM_WARNING_THRESHOLD);
        this.speedSafeColor = context.getResources().getColor(17170443);
        this.speedFastWarningColor = context.getResources().getColor(com.navdy.hud.app.R.color.cyan);
        this.speedVeryFastWarningColor = context.getResources().getColor(com.navdy.hud.app.R.color.speed_very_fast_warning_color);
        this.widgetName = context.getResources().getString(com.navdy.hud.app.R.string.gauge_tacho_meter);
        android.content.res.Resources resources = context.getResources();
        this.mphString = resources.getString(com.navdy.hud.app.R.string.mph);
        this.kmhString = resources.getString(com.navdy.hud.app.R.string.kilometers_per_hour);
        this.animationRunnable = new com.navdy.hud.app.view.TachometerGaugePresenter.Anon1();
    }

    public void setRPM(int rpm2) {
        if (this.isDashActive && this.lastRPM != rpm2 && java.lang.Math.abs(this.lastRPM - rpm2) >= 100) {
            this.lastRPM = rpm2;
            this.serialValueAnimator.setValue((float) rpm2);
        }
    }

    public void setSpeed(int speed2) {
        if (speed2 != -1 && this.speed != speed2) {
            this.speed = speed2;
            if (this.isDashActive) {
                reDraw();
            }
        }
    }

    public void setSpeedLimit(int speedLimit2) {
        if (this.speedLimit != speedLimit2) {
            this.speedLimit = speedLimit2;
            if (this.isDashActive) {
                reDraw();
            }
        }
    }

    public android.graphics.drawable.Drawable getDrawable() {
        return this.mTachometerGaugeDrawable;
    }

    /* access modifiers changed from: protected */
    public void updateGauge() {
        java.lang.String unitText;
        int speedLimitThreshold;
        if (this.mGaugeView != null) {
            this.mTachometerGaugeDrawable.setGaugeValue((float) this.rpm);
            this.mTachometerGaugeDrawable.setMaxMarkerValue(this.maxRpmValue);
            this.mTachometerGaugeDrawable.setMaxMarkerAlpha(this.maxMarkerAlpha);
            this.mGaugeView.setValueText(java.lang.Integer.toString(this.speed));
            switch (this.speedManager.getSpeedUnit()) {
                case KILOMETERS_PER_HOUR:
                    unitText = this.kmhString;
                    speedLimitThreshold = 13;
                    break;
                default:
                    unitText = this.mphString;
                    speedLimitThreshold = 8;
                    break;
            }
            if (this.speedLimit <= 0) {
                this.mGaugeView.setUnitText(unitText);
                this.mGaugeView.setValueText(java.lang.Integer.toString(this.speed));
                this.mGaugeView.getUnitTextView().setTextColor(this.speedSafeColor);
            } else if (this.speed >= this.speedLimit + speedLimitThreshold) {
                this.mGaugeView.setUnitText(this.mGaugeView.getResources().getString(com.navdy.hud.app.R.string.speed_limit_with_unit, new java.lang.Object[]{java.lang.Integer.valueOf(this.speedLimit), unitText}));
                this.mGaugeView.getUnitTextView().setTextColor(this.speedVeryFastWarningColor);
            } else if (this.speed >= this.speedLimit) {
                this.mGaugeView.setUnitText(this.mGaugeView.getResources().getString(com.navdy.hud.app.R.string.speed_limit_with_unit, new java.lang.Object[]{java.lang.Integer.valueOf(this.speedLimit), unitText}));
                this.mGaugeView.getUnitTextView().setTextColor(this.speedFastWarningColor);
            } else {
                this.mGaugeView.getUnitTextView().setTextColor(this.speedSafeColor);
                this.mGaugeView.setUnitText(unitText);
            }
        }
    }

    public java.lang.String getWidgetIdentifier() {
        return ID;
    }

    public java.lang.String getWidgetName() {
        return null;
    }

    public void setView(com.navdy.hud.app.view.DashboardWidgetView dashboardWidgetView) {
        if (dashboardWidgetView != null) {
            dashboardWidgetView.setContentView((int) com.navdy.hud.app.R.layout.tachometer_gauge);
        }
        super.setView(dashboardWidgetView);
        this.mGaugeView = (com.navdy.hud.app.view.GaugeView) dashboardWidgetView;
        if (this.mGaugeView != null && android.os.Build.VERSION.SDK_INT >= 21) {
            android.util.TypedValue outValue = new android.util.TypedValue();
            this.mGaugeView.getResources().getValue(com.navdy.hud.app.R.dimen.speedometer_text_letter_spacing, outValue, true);
            this.mGaugeView.getValueTextView().setLetterSpacing(outValue.getFloat());
        }
    }

    public float getValue() {
        return (float) this.rpm;
    }

    public void setValue(float newValue) {
        this.rpm = (int) newValue;
        if (this.rpm > this.maxRpmValue) {
            this.maxRpmValue = java.lang.Math.min(MAX_RPM_VALUE, this.rpm);
            this.handler.removeCallbacks(this.animationRunnable);
            this.showingMaxMarkerAnimation = false;
            this.maxMarkerAlpha = 0;
            if (this.valueAnimator != null && this.valueAnimator.isRunning()) {
                this.valueAnimator.cancel();
            }
        }
        if (this.rpm < this.maxRpmValue && !this.showingMaxMarkerAnimation) {
            this.maxMarkerAlpha = 255;
            this.showingMaxMarkerAnimation = true;
            this.handler.removeCallbacks(this.animationRunnable);
            this.handler.postDelayed(this.animationRunnable, 500);
        }
        reDraw();
    }

    public void animationComplete(float newValue) {
    }
}
