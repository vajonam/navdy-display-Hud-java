package com.navdy.hud.app.view;

public class NotificationView$$ViewInjector {
    public static void inject(butterknife.ButterKnife.Finder finder, com.navdy.hud.app.view.NotificationView target, java.lang.Object source) {
        target.customNotificationContainer = (android.widget.FrameLayout) finder.findRequiredView(source, com.navdy.hud.app.R.id.customNotificationContainer, "field 'customNotificationContainer'");
        target.border = (com.navdy.hud.app.ui.component.ShrinkingBorderView) finder.findRequiredView(source, com.navdy.hud.app.R.id.border, "field 'border'");
        target.nextNotificationColorView = (com.navdy.hud.app.ui.component.image.ColorImageView) finder.findRequiredView(source, com.navdy.hud.app.R.id.nextNotificationColorView, "field 'nextNotificationColorView'");
    }

    public static void reset(com.navdy.hud.app.view.NotificationView target) {
        target.customNotificationContainer = null;
        target.border = null;
        target.nextNotificationColorView = null;
    }
}
