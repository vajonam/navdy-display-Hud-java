package com.navdy.hud.app.view;

public class DialManagerView$$ViewInjector {
    public static void inject(butterknife.ButterKnife.Finder finder, com.navdy.hud.app.view.DialManagerView target, java.lang.Object source) {
        target.videoContainer = (android.view.ViewGroup) finder.findRequiredView(source, com.navdy.hud.app.R.id.videoContainer, "field 'videoContainer'");
        target.videoView = (android.widget.VideoView) finder.findRequiredView(source, com.navdy.hud.app.R.id.videoView, "field 'videoView'");
        target.rePairTextView = (android.widget.TextView) finder.findRequiredView(source, com.navdy.hud.app.R.id.repair_text, "field 'rePairTextView'");
        target.connectedView = (com.navdy.hud.app.ui.component.ConfirmationLayout) finder.findRequiredView(source, com.navdy.hud.app.R.id.connectedView, "field 'connectedView'");
    }

    public static void reset(com.navdy.hud.app.view.DialManagerView target) {
        target.videoContainer = null;
        target.videoView = null;
        target.rePairTextView = null;
        target.connectedView = null;
    }
}
