package com.navdy.hud.app.view;

public class FuelGaugePresenter2$$ViewInjector {
    public static void inject(butterknife.ButterKnife.Finder finder, com.navdy.hud.app.view.FuelGaugePresenter2 target, java.lang.Object source) {
        target.lowFuelIndicatorLeft = (android.widget.ImageView) finder.findRequiredView(source, com.navdy.hud.app.R.id.low_fuel_indicator_left, "field 'lowFuelIndicatorLeft'");
        target.lowFuelIndicatorRight = (android.widget.ImageView) finder.findRequiredView(source, com.navdy.hud.app.R.id.low_fuel_indicator_right, "field 'lowFuelIndicatorRight'");
        target.fuelTankIndicatorLeft = (android.widget.ImageView) finder.findRequiredView(source, com.navdy.hud.app.R.id.fuel_tank_side_indicator_left, "field 'fuelTankIndicatorLeft'");
        target.fuelTankIndicatorRight = (android.widget.ImageView) finder.findRequiredView(source, com.navdy.hud.app.R.id.fuel_tank_side_indicator_right, "field 'fuelTankIndicatorRight'");
        target.fuelTypeIndicator = (android.widget.ImageView) finder.findRequiredView(source, com.navdy.hud.app.R.id.fuel_type_icon, "field 'fuelTypeIndicator'");
        target.rangeText = (android.widget.TextView) finder.findRequiredView(source, com.navdy.hud.app.R.id.txt_value, "field 'rangeText'");
        target.rangeUnitText = (android.widget.TextView) finder.findRequiredView(source, com.navdy.hud.app.R.id.txt_unit, "field 'rangeUnitText'");
    }

    public static void reset(com.navdy.hud.app.view.FuelGaugePresenter2 target) {
        target.lowFuelIndicatorLeft = null;
        target.lowFuelIndicatorRight = null;
        target.fuelTankIndicatorLeft = null;
        target.fuelTankIndicatorRight = null;
        target.fuelTypeIndicator = null;
        target.rangeText = null;
        target.rangeUnitText = null;
    }
}
