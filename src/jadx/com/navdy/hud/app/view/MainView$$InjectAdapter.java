package com.navdy.hud.app.view;

public final class MainView$$InjectAdapter extends dagger.internal.Binding<com.navdy.hud.app.view.MainView> implements dagger.MembersInjector<com.navdy.hud.app.view.MainView> {
    private dagger.internal.Binding<android.content.SharedPreferences> preferences;
    private dagger.internal.Binding<com.navdy.hud.app.ui.activity.Main.Presenter> presenter;
    private dagger.internal.Binding<com.navdy.hud.app.ui.framework.UIStateManager> uiStateManager;

    public MainView$$InjectAdapter() {
        super(null, "members/com.navdy.hud.app.view.MainView", false, com.navdy.hud.app.view.MainView.class);
    }

    public void attach(dagger.internal.Linker linker) {
        this.presenter = linker.requestBinding("com.navdy.hud.app.ui.activity.Main$Presenter", com.navdy.hud.app.view.MainView.class, getClass().getClassLoader());
        this.uiStateManager = linker.requestBinding("com.navdy.hud.app.ui.framework.UIStateManager", com.navdy.hud.app.view.MainView.class, getClass().getClassLoader());
        this.preferences = linker.requestBinding("android.content.SharedPreferences", com.navdy.hud.app.view.MainView.class, getClass().getClassLoader());
    }

    public void getDependencies(java.util.Set<dagger.internal.Binding<?>> set, java.util.Set<dagger.internal.Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.presenter);
        injectMembersBindings.add(this.uiStateManager);
        injectMembersBindings.add(this.preferences);
    }

    public void injectMembers(com.navdy.hud.app.view.MainView object) {
        object.presenter = (com.navdy.hud.app.ui.activity.Main.Presenter) this.presenter.get();
        object.uiStateManager = (com.navdy.hud.app.ui.framework.UIStateManager) this.uiStateManager.get();
        object.preferences = (android.content.SharedPreferences) this.preferences.get();
    }
}
