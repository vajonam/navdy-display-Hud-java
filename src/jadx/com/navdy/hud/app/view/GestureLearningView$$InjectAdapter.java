package com.navdy.hud.app.view;

public final class GestureLearningView$$InjectAdapter extends dagger.internal.Binding<com.navdy.hud.app.view.GestureLearningView> implements dagger.MembersInjector<com.navdy.hud.app.view.GestureLearningView> {
    private dagger.internal.Binding<com.squareup.otto.Bus> mBus;
    private dagger.internal.Binding<com.navdy.hud.app.screen.GestureLearningScreen.Presenter> mPresenter;

    public GestureLearningView$$InjectAdapter() {
        super(null, "members/com.navdy.hud.app.view.GestureLearningView", false, com.navdy.hud.app.view.GestureLearningView.class);
    }

    public void attach(dagger.internal.Linker linker) {
        this.mPresenter = linker.requestBinding("com.navdy.hud.app.screen.GestureLearningScreen$Presenter", com.navdy.hud.app.view.GestureLearningView.class, getClass().getClassLoader());
        this.mBus = linker.requestBinding("com.squareup.otto.Bus", com.navdy.hud.app.view.GestureLearningView.class, getClass().getClassLoader());
    }

    public void getDependencies(java.util.Set<dagger.internal.Binding<?>> set, java.util.Set<dagger.internal.Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.mPresenter);
        injectMembersBindings.add(this.mBus);
    }

    public void injectMembers(com.navdy.hud.app.view.GestureLearningView object) {
        object.mPresenter = (com.navdy.hud.app.screen.GestureLearningScreen.Presenter) this.mPresenter.get();
        object.mBus = (com.squareup.otto.Bus) this.mBus.get();
    }
}
