package com.navdy.hud.app.view;

public class ScrollableTextPresenterLayout extends android.widget.RelativeLayout implements com.navdy.hud.app.ui.component.ChoiceLayout.IListener, com.navdy.hud.app.view.ObservableScrollView.IScrollListener, com.navdy.hud.app.manager.InputManager.IInputHandler {
    public static final int TAG_BACK = 0;
    @butterknife.InjectView(2131624208)
    android.view.View bottomScrub;
    @butterknife.InjectView(2131624150)
    com.navdy.hud.app.ui.component.ChoiceLayout mChoiceLayout;
    boolean mContentNeedsScrolling;
    private boolean mContentReachedBottom;
    private boolean mContentReachedTop;
    private int mCount;
    private int mCurrentItem;
    @butterknife.InjectView(2131624214)
    android.widget.ImageView mMainImageView;
    @butterknife.InjectView(2131624144)
    android.widget.TextView mMainTitleText;
    @butterknife.InjectView(2131624206)
    android.widget.TextView mMessageText;
    @butterknife.InjectView(2131624255)
    com.navdy.hud.app.ui.component.carousel.CarouselIndicator mNotificationIndicator;
    @butterknife.InjectView(2131624256)
    com.navdy.hud.app.ui.component.carousel.ProgressIndicator mNotificationScrollIndicator;
    @butterknife.InjectView(2131624205)
    com.navdy.hud.app.view.ObservableScrollView mObservableScrollView;
    @javax.inject.Inject
    com.navdy.hud.app.screen.GestureLearningScreen.Presenter mPresenter;
    java.util.ArrayList<java.lang.CharSequence> mTextContents;
    @butterknife.InjectView(2131624135)
    android.widget.TextView mTitleText;
    private int scrollColor;
    @butterknife.InjectView(2131624207)
    android.view.View topScrub;

    class Anon1 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
        Anon1() {
        }

        public void onAnimationEnd(android.animation.Animator animation) {
            super.onAnimationEnd(animation);
            com.navdy.hud.app.view.ScrollableTextPresenterLayout.this.displayScrollIndicator(false);
        }
    }

    class Anon2 implements android.view.ViewTreeObserver.OnGlobalLayoutListener {
        Anon2() {
        }

        public void onGlobalLayout() {
            com.navdy.hud.app.view.ScrollableTextPresenterLayout.this.layoutScrollIndicator();
            com.navdy.hud.app.view.ScrollableTextPresenterLayout.this.mNotificationIndicator.getViewTreeObserver().removeOnGlobalLayoutListener(this);
        }
    }

    public ScrollableTextPresenterLayout(android.content.Context context) {
        this(context, null);
    }

    public ScrollableTextPresenterLayout(android.content.Context context, android.util.AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ScrollableTextPresenterLayout(android.content.Context context, android.util.AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mTextContents = new java.util.ArrayList<>();
        this.mContentNeedsScrolling = false;
        if (!isInEditMode()) {
            mortar.Mortar.inject(context, this);
        }
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
        butterknife.ButterKnife.inject((android.view.View) this);
        this.mChoiceLayout.setHighlightPersistent(true);
        this.scrollColor = getResources().getColor(com.navdy.hud.app.R.color.scroll_option_color);
        this.mObservableScrollView.setScrollListener(this);
        java.util.List<com.navdy.hud.app.ui.component.ChoiceLayout.Choice> list = new java.util.ArrayList<>();
        list.add(new com.navdy.hud.app.ui.component.ChoiceLayout.Choice(getContext().getString(com.navdy.hud.app.R.string.back), 0));
        this.mChoiceLayout.setChoices(com.navdy.hud.app.ui.component.ChoiceLayout.Mode.LABEL, list, 0, this);
        this.mMainImageView.setImageResource(com.navdy.hud.app.R.drawable.icon_settings_learning_to_gesture);
        this.mMainTitleText.setText(com.navdy.hud.app.R.string.gesture_tips_title);
        this.mNotificationScrollIndicator.setOrientation(com.navdy.hud.app.ui.component.carousel.CarouselIndicator.Orientation.VERTICAL);
        this.mNotificationScrollIndicator.setProperties(com.navdy.hud.app.framework.glance.GlanceConstants.scrollingRoundSize, com.navdy.hud.app.framework.glance.GlanceConstants.scrollingIndicatorProgressSize, 0, 0, com.navdy.hud.app.framework.glance.GlanceConstants.colorWhite, com.navdy.hud.app.framework.glance.GlanceConstants.colorWhite, true, com.navdy.hud.app.framework.glance.GlanceConstants.scrollingIndicatorPadding, -1, -1);
        this.mNotificationScrollIndicator.setItemCount(100);
        this.mNotificationIndicator.setOrientation(com.navdy.hud.app.ui.component.carousel.CarouselIndicator.Orientation.VERTICAL);
        this.mNotificationScrollIndicator.setBackgroundColor(this.scrollColor);
    }

    public void setVisibility(int visibility) {
        super.setVisibility(visibility);
        if (visibility == 0) {
            setCurrentItem(this.mCurrentItem, false, false);
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
    }

    public void setTextContents(java.lang.CharSequence[] textContents) {
        this.mTextContents.clear();
        for (java.lang.CharSequence text : textContents) {
            this.mTextContents.add(text);
        }
        this.mCount = this.mTextContents.size();
        this.mNotificationIndicator.setItemCount(this.mCount);
        setCurrentItem(0, false, false);
    }

    private void setCurrentItem(int item, boolean animate, boolean previous) {
        if (item >= 0 && item < this.mCount) {
            this.mCurrentItem = item;
            java.lang.CharSequence text = (java.lang.CharSequence) this.mTextContents.get(this.mCurrentItem);
            this.mContentNeedsScrolling = com.navdy.hud.app.framework.glance.GlanceHelper.needsScrollLayout(text.toString());
            this.mObservableScrollView.fullScroll(33);
            this.mMessageText.setText(text);
            this.mMessageText.setVisibility(0);
            this.topScrub.setVisibility(8);
            this.bottomScrub.setVisibility(0);
            this.mContentReachedTop = true;
            this.mContentReachedBottom = false;
            if (animate) {
                android.animation.AnimatorSet animatorSet = this.mNotificationIndicator.getItemMoveAnimator(this.mCurrentItem, this.scrollColor);
                if (animatorSet != null) {
                    animatorSet.addListener(new com.navdy.hud.app.view.ScrollableTextPresenterLayout.Anon1());
                    animatorSet.start();
                    return;
                }
                displayScrollIndicator(false);
                return;
            }
            displayScrollIndicator(true);
        }
    }

    /* access modifiers changed from: private */
    public void displayScrollIndicator(boolean initial) {
        if (this.mCount > 0) {
            this.mNotificationIndicator.setVisibility(0);
        }
        this.mNotificationIndicator.setCurrentItem(this.mCurrentItem, this.scrollColor);
        if (!this.mContentNeedsScrolling) {
            ((android.view.View) this.mNotificationScrollIndicator.getParent()).setVisibility(8);
        } else if (initial) {
            this.mNotificationIndicator.getViewTreeObserver().addOnGlobalLayoutListener(new com.navdy.hud.app.view.ScrollableTextPresenterLayout.Anon2());
        } else {
            layoutScrollIndicator();
        }
    }

    /* access modifiers changed from: private */
    public void layoutScrollIndicator() {
        android.graphics.RectF rectF = this.mNotificationIndicator.getItemPos(this.mCurrentItem);
        if (rectF == null) {
            return;
        }
        if (rectF.left != 0.0f || rectF.top != 0.0f) {
            this.mNotificationScrollIndicator.setCurrentItem(1);
            android.view.View parent = (android.view.View) this.mNotificationScrollIndicator.getParent();
            parent.setX(this.mNotificationIndicator.getX() + ((float) com.navdy.hud.app.framework.glance.GlanceConstants.scrollingIndicatorLeftPadding));
            parent.setY(((this.mNotificationIndicator.getY() + rectF.top) + ((float) (com.navdy.hud.app.framework.glance.GlanceConstants.scrollingIndicatorCircleSize / 2))) - ((float) (com.navdy.hud.app.framework.glance.GlanceConstants.scrollingIndicatorHeight / 2)));
            parent.setVisibility(0);
        }
    }

    public void onTop() {
        if (this.mContentNeedsScrolling) {
            this.topScrub.setVisibility(8);
            this.mContentReachedTop = true;
            this.mContentReachedBottom = false;
            onPosChange(1);
        }
    }

    public void onBottom() {
        if (this.mContentNeedsScrolling) {
            this.mContentReachedTop = false;
            this.mContentReachedBottom = true;
            onPosChange(100);
        }
    }

    public void onScroll(int l, int t, int oldl, int oldt) {
        if (this.mContentNeedsScrolling) {
            this.topScrub.setVisibility(0);
            this.mContentReachedTop = false;
            this.mContentReachedBottom = false;
            onPosChange((int) ((((double) t) * 100.0d) / ((double) (this.mObservableScrollView.getChildAt(0).getBottom() - this.mObservableScrollView.getHeight()))));
        }
    }

    public void onPosChange(int pos) {
        if (pos <= 0) {
            pos = 1;
        } else if (pos > 100) {
            pos = 100;
        }
        if (this.mNotificationScrollIndicator.getCurrentItem() != pos) {
            if (pos == 1 || pos == 100) {
            }
            this.mNotificationScrollIndicator.setCurrentItem(pos);
        }
    }

    public boolean onGesture(com.navdy.service.library.events.input.GestureEvent event) {
        return false;
    }

    public boolean onKey(com.navdy.hud.app.manager.InputManager.CustomKeyEvent event) {
        switch (event) {
            case LEFT:
                handleLeft();
                return true;
            case RIGHT:
                handleRight();
                return true;
            case SELECT:
                this.mChoiceLayout.executeSelectedItem(true);
                return true;
            default:
                return false;
        }
    }

    private void handleLeft() {
        boolean handled = false;
        if (this.mContentNeedsScrolling) {
            handled = this.mObservableScrollView.arrowScroll(33) && !this.mContentReachedTop;
        }
        if (!handled && this.mCurrentItem > 0) {
            setCurrentItem(this.mCurrentItem - 1, true, true);
        }
    }

    private void handleRight() {
        boolean handled = false;
        if (this.mContentNeedsScrolling) {
            handled = this.mObservableScrollView.arrowScroll(130) && !this.mContentReachedBottom;
        }
        if (!handled && this.mCurrentItem < this.mCount - 1) {
            setCurrentItem(this.mCurrentItem + 1, true, false);
        }
    }

    public com.navdy.hud.app.manager.InputManager.IInputHandler nextHandler() {
        return null;
    }

    public void executeItem(int pos, int id) {
        switch (id) {
            case 0:
                this.mPresenter.hideTips();
                return;
            default:
                return;
        }
    }

    public void itemSelected(int pos, int id) {
    }
}
