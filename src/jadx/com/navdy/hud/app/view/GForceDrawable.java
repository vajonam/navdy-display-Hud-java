package com.navdy.hud.app.view;

public class GForceDrawable extends com.navdy.hud.app.view.drawable.CustomDrawable {
    private static final int BACKGROUND = 0;
    private static final int DANGER = 4;
    private static final int EXTREME = 5;
    private static final int HIGHLIGHT = 2;
    public static final float HIGH_G = 1.05f;
    private static final int LOW = 1;
    public static final float LOW_G = 0.45f;
    public static final float MAX_ACCEL = 2.0f;
    public static final float MEDIUM_G = 0.75f;
    public static final float NO_G = 0.15f;
    private static final int WARNING = 3;
    private static android.graphics.Typeface typeface;
    private int[] colors;
    protected com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(getClass());
    private android.graphics.Paint mTextPaint;
    private int textColor;
    private float xAccel;
    private float yAccel;
    private float zAccel;

    public GForceDrawable(android.content.Context context) {
        this.mPaint.setColor(-1);
        this.mPaint.setStrokeWidth(1.0f);
        if (typeface == null) {
            typeface = android.graphics.Typeface.create("sans-serif-medium", 0);
        }
        this.mTextPaint = new android.graphics.Paint();
        this.mTextPaint.setTextAlign(android.graphics.Paint.Align.CENTER);
        android.content.res.Resources resources = context.getResources();
        this.colors = resources.getIntArray(com.navdy.hud.app.R.array.gforce_colors);
        this.textColor = resources.getColor(com.navdy.hud.app.R.color.gforce_text);
    }

    public void setAcceleration(float xAccel2, float yAccel2, float zAccel2) {
        this.xAccel = xAccel2;
        this.yAccel = yAccel2;
        this.zAccel = zAccel2;
    }

    /* access modifiers changed from: protected */
    public void onBoundsChange(android.graphics.Rect bounds) {
        super.onBoundsChange(bounds);
    }

    public void draw(android.graphics.Canvas canvas) {
        super.draw(canvas);
        android.graphics.Rect bounds = getBounds();
        int width = bounds.width() - 10;
        float centerY = (float) ((bounds.height() - 10) / 2);
        float centerX = (float) (width / 2);
        float x = clamp(this.xAccel, -2.0f, 2.0f);
        float y = clamp(this.yAccel, -2.0f, 2.0f);
        float clamp = clamp(this.zAccel, -2.0f, 2.0f);
        if (0 != 0) {
            int subTicks = java.lang.Math.min(10, java.lang.Math.max(1, (width / 4) / 4));
            int ticks = 2 * subTicks;
            for (int i = -ticks; i <= ticks; i++) {
                float tickX = ((float) 5) + ((1.0f + (((float) i) / ((float) ticks))) * centerX);
                int tickHeight = i % subTicks == 0 ? 10 : 5;
                canvas.drawLine(tickX, (((float) 5) + centerY) - ((float) (tickHeight / 2)), tickX, ((float) 5) + centerY + ((float) (tickHeight / 2)), this.mPaint);
                float tickY = ((float) 5) + ((1.0f + (((float) i) / ((float) ticks))) * centerY);
                canvas.drawLine((((float) 5) + centerX) - ((float) (tickHeight / 2)), tickY, ((float) 5) + centerX + ((float) (tickHeight / 2)), tickY, this.mPaint);
            }
            canvas.drawCircle(((float) 5) + centerX + ((x * centerX) / 2.0f), (((float) 5) + centerY) - ((y * centerY) / 2.0f), 4.0f + (java.lang.Math.abs(this.zAccel) * 3.0f), this.mPaint);
            return;
        }
        float g = java.lang.Math.max(java.lang.Math.abs(x), java.lang.Math.abs(y));
        this.mPaint.setColor(this.colors[0]);
        canvas.drawCircle(((float) 5) + centerX, ((float) 5) + centerY, 50.0f, this.mPaint);
        float centerAngle = (float) ((java.lang.Math.atan((double) ((-y) / x)) * 180.0d) / 3.141592653589793d);
        if (x < 0.0f) {
            centerAngle += 180.0f;
        }
        int color = this.colors[1];
        if (g > 0.15f) {
            float sweepAngle = 20.0f + ((70.0f * (clamp(g, 0.15f, 0.75f) - 0.15f)) / 0.6f);
            float startAngle = centerAngle - (sweepAngle / 2.0f);
            if (g < 0.45f) {
                color = this.colors[2];
            } else if (g < 0.75f) {
                color = this.colors[3];
            } else if (g < 1.05f) {
                color = this.colors[4];
            } else {
                color = this.colors[5];
            }
            this.mPaint.setColor(color);
            canvas.drawArc(new android.graphics.RectF(50.0f - 50.0f, 50.0f - 50.0f, 50.0f + 50.0f, 50.0f + 50.0f), startAngle, sweepAngle, true, this.mPaint);
        }
        this.mPaint.setColor(-16777216);
        canvas.drawCircle(((float) 5) + centerX, ((float) 5) + centerY, 36.0f, this.mPaint);
        this.mTextPaint.setColor(this.textColor);
        this.mTextPaint.setTextSize(16.0f);
        this.mTextPaint.setTypeface(android.graphics.Typeface.SANS_SERIF);
        canvas.drawText("G", ((float) 5) + centerX, (((float) 5) + centerY) - 18.0f, this.mTextPaint);
        canvas.drawText("-", ((float) 5) + centerX, ((float) 5) + centerY + 28.0f, this.mTextPaint);
        this.mTextPaint.setColor(color);
        this.mTextPaint.setTextSize(28.0f);
        this.mTextPaint.setTypeface(typeface);
        canvas.drawText((this.xAccel > 0.0f ? 1 : (this.xAccel == 0.0f ? 0 : -1)) == 0 && (this.yAccel > 0.0f ? 1 : (this.yAccel == 0.0f ? 0 : -1)) == 0 && (this.zAccel > 0.0f ? 1 : (this.zAccel == 0.0f ? 0 : -1)) == 0 ? "-" : java.lang.String.format(java.util.Locale.US, "%01.2f", new java.lang.Object[]{java.lang.Float.valueOf(g)}), ((float) 5) + centerX, ((float) 5) + centerY + 10.0f, this.mTextPaint);
    }

    private float clamp(float val, float min, float max) {
        if (val < min) {
            return min;
        }
        if (val > max) {
            return max;
        }
        return val;
    }
}
