package com.navdy.hud.app.view;

public final class TemperatureWarningView$$InjectAdapter extends dagger.internal.Binding<com.navdy.hud.app.view.TemperatureWarningView> implements dagger.MembersInjector<com.navdy.hud.app.view.TemperatureWarningView> {
    private dagger.internal.Binding<com.navdy.hud.app.screen.TemperatureWarningScreen.Presenter> mPresenter;

    public TemperatureWarningView$$InjectAdapter() {
        super(null, "members/com.navdy.hud.app.view.TemperatureWarningView", false, com.navdy.hud.app.view.TemperatureWarningView.class);
    }

    public void attach(dagger.internal.Linker linker) {
        this.mPresenter = linker.requestBinding("com.navdy.hud.app.screen.TemperatureWarningScreen$Presenter", com.navdy.hud.app.view.TemperatureWarningView.class, getClass().getClassLoader());
    }

    public void getDependencies(java.util.Set<dagger.internal.Binding<?>> set, java.util.Set<dagger.internal.Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.mPresenter);
    }

    public void injectMembers(com.navdy.hud.app.view.TemperatureWarningView object) {
        object.mPresenter = (com.navdy.hud.app.screen.TemperatureWarningScreen.Presenter) this.mPresenter.get();
    }
}
