package com.navdy.hud.app.view;

public abstract class ChoicesView extends android.widget.RelativeLayout implements com.navdy.hud.app.manager.InputManager.IInputHandler, com.navdy.hud.app.gesture.GestureDetector.GestureListener {
    protected boolean animateItemOnExecution;
    protected boolean animateItemOnSelection;
    protected com.navdy.hud.app.gesture.GestureDetector detector;
    protected android.view.animation.Animation executeAnimation;
    protected int lastTouchX;
    protected int lastTouchY;
    private android.view.animation.Animation.AnimationListener listener;
    protected com.navdy.service.library.log.Logger logger;
    protected int selectedItem;
    protected boolean wrapAround;

    class Anon1 implements android.view.View.OnTouchListener {
        Anon1() {
        }

        public boolean onTouch(android.view.View v, android.view.MotionEvent event) {
            if (event.getAction() == 1) {
                com.navdy.hud.app.view.ChoicesView.this.lastTouchX = (int) event.getX();
                com.navdy.hud.app.view.ChoicesView.this.lastTouchY = (int) event.getY();
            }
            return false;
        }
    }

    class Anon2 implements android.view.View.OnClickListener {
        Anon2() {
        }

        public void onClick(android.view.View v) {
            android.graphics.Rect hitRect = new android.graphics.Rect();
            int[] myPos = new int[2];
            int[] parentPos = new int[2];
            com.navdy.hud.app.view.ChoicesView.this.getLocationInWindow(myPos);
            int i = 0;
            while (i < com.navdy.hud.app.view.ChoicesView.this.getItemCount()) {
                android.view.View childView = com.navdy.hud.app.view.ChoicesView.this.getItemAt(i);
                ((android.view.View) childView.getParent()).getLocationInWindow(parentPos);
                int touchX = com.navdy.hud.app.view.ChoicesView.this.lastTouchX - (parentPos[0] - myPos[0]);
                int touchY = com.navdy.hud.app.view.ChoicesView.this.lastTouchY - (parentPos[1] - myPos[1]);
                childView.getHitRect(hitRect);
                if (!hitRect.contains(touchX, touchY)) {
                    i++;
                } else if (i == com.navdy.hud.app.view.ChoicesView.this.getSelectedItem()) {
                    com.navdy.hud.app.view.ChoicesView.this.executeSelectedItem(com.navdy.hud.app.view.ChoicesView.this.animateItemOnExecution);
                    return;
                } else {
                    com.navdy.hud.app.view.ChoicesView.this.setSelectedItem(i);
                    return;
                }
            }
        }
    }

    class Anon3 implements android.view.animation.Animation.AnimationListener {
        Anon3() {
        }

        public void onAnimationStart(android.view.animation.Animation animation) {
        }

        public void onAnimationEnd(android.view.animation.Animation animation) {
            com.navdy.hud.app.view.ChoicesView.this.onExecuteItem(com.navdy.hud.app.view.ChoicesView.this.selectedItem);
        }

        public void onAnimationRepeat(android.view.animation.Animation animation) {
        }
    }

    /* access modifiers changed from: protected */
    public abstract void animateDown();

    /* access modifiers changed from: protected */
    public abstract void animateUp();

    public abstract android.view.View getItemAt(int i);

    public abstract int getItemCount();

    public abstract float getValue();

    public abstract void onExecuteItem(int i);

    public abstract void setValue(float f);

    public ChoicesView(android.content.Context context) {
        this(context, null);
    }

    public ChoicesView(android.content.Context context, android.util.AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ChoicesView(android.content.Context context, android.util.AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.logger = new com.navdy.service.library.log.Logger(getClass());
        this.selectedItem = -1;
        this.wrapAround = false;
        this.animateItemOnExecution = true;
        this.animateItemOnSelection = true;
        this.listener = new com.navdy.hud.app.view.ChoicesView.Anon3();
        this.detector = new com.navdy.hud.app.gesture.GestureDetector(this);
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        setOnTouchListener(new com.navdy.hud.app.view.ChoicesView.Anon1());
        setOnClickListener(new com.navdy.hud.app.view.ChoicesView.Anon2());
    }

    /* access modifiers changed from: protected */
    public void onDeselectItem(android.view.View choice, int newSelectionIndex) {
    }

    /* access modifiers changed from: protected */
    public void onSelectItem(android.view.View choice, int oldSelectionIndex) {
    }

    public int getSelectedItem() {
        return this.selectedItem;
    }

    public void setSelectedItem(int index) {
        if (index != this.selectedItem) {
            this.logger.d("selecting option - " + (index < 0 ? "none" : java.lang.String.valueOf(index + 1)));
            android.view.View oldView = null;
            android.view.View newView = null;
            if (this.selectedItem != -1) {
                oldView = getItemAt(this.selectedItem);
            }
            if (index != -1) {
                newView = getItemAt(index);
            }
            if (oldView != null) {
                onDeselectItem(oldView, index);
            }
            int oldIndex = this.selectedItem;
            this.selectedItem = index;
            if (newView != null) {
                onSelectItem(newView, oldIndex);
            }
            if (index != -1) {
                setValue(valueForIndex(index));
            }
        }
    }

    public int indexForValue(float value) {
        int itemCount = getItemCount();
        if (itemCount <= 1) {
            return 0;
        }
        return (int) ((((float) (itemCount - 1)) * value) + 0.5f);
    }

    public float valueForIndex(int index) {
        int itemCount = getItemCount();
        if (itemCount <= 1) {
            return 0.0f;
        }
        return ((float) index) / ((float) (itemCount - 1));
    }

    public void executeSelectedItem(boolean animated) {
        if (this.selectedItem != -1 && getItemCount() > 0) {
            this.logger.d("USER_STUDY executing option " + (this.selectedItem + 1));
            if (animated) {
                if (this.executeAnimation == null) {
                    this.executeAnimation = android.view.animation.AnimationUtils.loadAnimation(getContext(), com.navdy.hud.app.R.anim.press_button);
                    this.executeAnimation.setAnimationListener(this.listener);
                }
                getItemAt(this.selectedItem).startAnimation(this.executeAnimation);
                return;
            }
            onExecuteItem(this.selectedItem);
        }
    }

    public void onClick() {
        executeSelectedItem(this.animateItemOnExecution);
    }

    public void onTrackHand(float position) {
        setValue(position);
    }

    public boolean onGesture(com.navdy.service.library.events.input.GestureEvent event) {
        this.detector.onGesture(event);
        switch (event.gesture) {
            case GESTURE_FINGER_UP:
            case GESTURE_HAND_UP:
                animateUp();
                break;
            case GESTURE_FINGER_DOWN:
            case GESTURE_HAND_DOWN:
                animateDown();
                setSelectedItem(-1);
                break;
            case GESTURE_SWIPE_LEFT:
                if (this.wrapAround || this.selectedItem != 0) {
                    moveSelectionLeft();
                    return true;
                }
                buildBounceAnim(0.0f, valueForIndex(0)).start();
                return true;
            case GESTURE_SWIPE_RIGHT:
                if (this.wrapAround || this.selectedItem + 1 != getItemCount()) {
                    moveSelectionRight();
                    return true;
                }
                buildBounceAnim(1.0f, valueForIndex(this.selectedItem)).start();
                return true;
        }
        return false;
    }

    public boolean onKey(com.navdy.hud.app.manager.InputManager.CustomKeyEvent event) {
        if (this.selectedItem == -1) {
            animateUp();
            return true;
        }
        switch (event) {
            case LEFT:
                moveSelectionLeft();
                return true;
            case RIGHT:
                moveSelectionRight();
                return true;
            case SELECT:
                executeSelectedItem(true);
                return true;
            default:
                return false;
        }
    }

    private void moveSelectionLeft() {
        int itemCount = getItemCount();
        if (this.selectedItem > 0 && itemCount > 0) {
            setSelectedItem(((this.selectedItem - 1) + itemCount) % itemCount);
        }
    }

    private void moveSelectionRight() {
        if (this.selectedItem != -1) {
            int itemCount = getItemCount();
            if (this.selectedItem + 1 < itemCount) {
                setSelectedItem((this.selectedItem + 1) % itemCount);
            }
        }
    }

    private android.animation.Animator buildBounceAnim(float edgeValue, float backToValue) {
        android.animation.AnimatorSet s = new android.animation.AnimatorSet();
        s.play(android.animation.ObjectAnimator.ofFloat(this, com.glympse.android.hal.NotificationListener.INTENT_EXTRA_VALUE, new float[]{edgeValue})).before(android.animation.ObjectAnimator.ofFloat(this, com.glympse.android.hal.NotificationListener.INTENT_EXTRA_VALUE, new float[]{backToValue}));
        return s;
    }

    public com.navdy.hud.app.manager.InputManager.IInputHandler nextHandler() {
        return com.navdy.hud.app.manager.InputManager.nextContainingHandler(this);
    }
}
