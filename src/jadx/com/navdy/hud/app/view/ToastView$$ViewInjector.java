package com.navdy.hud.app.view;

public class ToastView$$ViewInjector {
    public static void inject(butterknife.ButterKnife.Finder finder, com.navdy.hud.app.view.ToastView target, java.lang.Object source) {
        target.mainView = (com.navdy.hud.app.ui.component.ConfirmationLayout) finder.findRequiredView(source, com.navdy.hud.app.R.id.mainView, "field 'mainView'");
    }

    public static void reset(com.navdy.hud.app.view.ToastView target) {
        target.mainView = null;
    }
}
