package com.navdy.hud.app.view;

public class GestureVideoCaptureView extends android.widget.RelativeLayout implements com.navdy.hud.app.ui.component.ChoiceLayout.IListener, com.navdy.hud.app.manager.InputManager.IInputHandler {
    public static final int TAG_EXIT = 0;
    public static final int TAG_NEXT = 1;
    private static final long UPLOAD_TIMEOUT = 300000;
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.view.GestureVideoCaptureView.class);
    @javax.inject.Inject
    com.squareup.otto.Bus bus;
    private java.util.List<com.navdy.hud.app.ui.component.ChoiceLayout.Choice> emptyChoices;
    private java.util.List<com.navdy.hud.app.ui.component.ChoiceLayout.Choice> exitChoices;
    @javax.inject.Inject
    com.navdy.hud.app.gesture.GestureServiceConnector gestureService;
    @butterknife.InjectView(2131624150)
    com.navdy.hud.app.ui.component.ChoiceLayout mChoiceLayout;
    @javax.inject.Inject
    com.navdy.hud.app.screen.GestureLearningScreen.Presenter mPresenter;
    private android.os.Handler mainHandler;
    @butterknife.InjectView(2131624147)
    android.widget.ImageView mainImageView;
    @butterknife.InjectView(2131624076)
    android.widget.TextView mainText;
    private java.util.List<com.navdy.hud.app.ui.component.ChoiceLayout.Choice> nextAndExitChoices;
    private com.navdy.hud.app.view.GestureVideoCaptureView.State prevState;
    private com.navdy.hud.app.view.GestureVideoCaptureView.RecordingSavingContext recordingToSave;
    private java.util.Set<java.lang.String> recordingsToUpload;
    private boolean registered;
    @butterknife.InjectView(2131624078)
    android.widget.TextView secondaryText;
    private java.util.concurrent.ExecutorService serialExecutor;
    /* access modifiers changed from: private */
    public java.lang.String sessionName;
    @butterknife.InjectView(2131624149)
    android.widget.ImageView sideImageView;
    private com.navdy.hud.app.view.GestureVideoCaptureView.State state;
    private java.lang.Runnable uploadTimeoutRunnable;

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            com.navdy.hud.app.view.GestureVideoCaptureView.this.setState(com.navdy.hud.app.view.GestureVideoCaptureView.State.ALL_DONE);
        }
    }

    class Anon2 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.hud.app.gesture.GestureServiceConnector.RecordingSaved val$recordingSaved;
        final /* synthetic */ java.lang.String val$sessionPath;
        final /* synthetic */ java.lang.String val$userTag;
        final /* synthetic */ java.lang.String val$videoArchivePath;

        Anon2(java.lang.String str, com.navdy.hud.app.gesture.GestureServiceConnector.RecordingSaved recordingSaved, java.lang.String str2, java.lang.String str3) {
            this.val$sessionPath = str;
            this.val$recordingSaved = recordingSaved;
            this.val$videoArchivePath = str2;
            this.val$userTag = str3;
        }

        public void run() {
            java.io.File sessionDirectory = new java.io.File(this.val$sessionPath);
            if (!sessionDirectory.exists()) {
                sessionDirectory.mkdirs();
            }
            com.navdy.service.library.util.IOUtils.compressFilesToZip(com.navdy.hud.app.view.GestureVideoCaptureView.this.getContext(), new java.io.File(this.val$recordingSaved.path).listFiles(), this.val$videoArchivePath);
            com.navdy.hud.app.view.GestureVideoCaptureView.sLogger.d("Compressed file : " + this.val$videoArchivePath);
            com.navdy.hud.app.service.GestureVideosSyncService.addGestureVideoToUploadQueue(new java.io.File(this.val$videoArchivePath), this.val$userTag);
            com.navdy.hud.app.service.GestureVideosSyncService.syncNow();
        }
    }

    private static class ChoiceInfo {
        public final int tag;
        public final int text;

        public ChoiceInfo(int text2, int tag2) {
            this.text = text2;
            this.tag = tag2;
        }
    }

    private class RecordingSavingContext {
        public final java.lang.String sessionName;
        public final com.navdy.hud.app.view.GestureVideoCaptureView.State state;

        public RecordingSavingContext(java.lang.String sessionName2, com.navdy.hud.app.view.GestureVideoCaptureView.State state2) {
            this.sessionName = sessionName2;
            this.state = state2;
        }
    }

    enum State {
        NOT_PAIRED(com.navdy.hud.app.R.string.please_pair_phone, com.navdy.hud.app.R.string.please_pair_phone_detail, null),
        SWIPE_LEFT(com.navdy.hud.app.R.string.swipe_left, 0, "swipe-left"),
        SWIPE_RIGHT(com.navdy.hud.app.R.string.swipe_right, 0, "swipe-right"),
        DOUBLE_TAP_1(com.navdy.hud.app.R.string.double_tap_1, com.navdy.hud.app.R.string.double_tap_detail_1, "tap"),
        DOUBLE_TAP_2(com.navdy.hud.app.R.string.double_tap_2, com.navdy.hud.app.R.string.double_tap_detail_2, "tap2"),
        SAVING(0, com.navdy.hud.app.R.string.saving_recording, null),
        UPLOADING(com.navdy.hud.app.R.string.uploading, com.navdy.hud.app.R.string.uploading_detail, null),
        ALL_DONE(com.navdy.hud.app.R.string.uploading_success, 0, null),
        UPLOADING_FAILURE(com.navdy.hud.app.R.string.upload_failure, com.navdy.hud.app.R.string.upload_failure_detail, null);
        
        int detailText;
        java.lang.String filename;
        int mainText;

        private State(int mainText2, int detailText2, java.lang.String filename2) {
            this.mainText = mainText2;
            this.detailText = detailText2;
            this.filename = filename2;
        }
    }

    private class UploadTimeoutRunnable implements java.lang.Runnable {
        private final java.lang.String sessionName;

        UploadTimeoutRunnable(java.lang.String sessionName2) {
            this.sessionName = sessionName2;
        }

        public void run() {
            com.navdy.hud.app.view.GestureVideoCaptureView.sLogger.d("Upload has timed out.");
            if (this.sessionName.equals(com.navdy.hud.app.view.GestureVideoCaptureView.this.sessionName)) {
                com.navdy.hud.app.view.GestureVideoCaptureView.this.setState(com.navdy.hud.app.view.GestureVideoCaptureView.State.UPLOADING_FAILURE);
            }
        }
    }

    public GestureVideoCaptureView(android.content.Context context) {
        this(context, null);
    }

    public GestureVideoCaptureView(android.content.Context context, android.util.AttributeSet attrs) {
        this(context, attrs, -1);
    }

    public GestureVideoCaptureView(android.content.Context context, android.util.AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, -1);
    }

    public GestureVideoCaptureView(android.content.Context context, android.util.AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.registered = false;
        this.recordingsToUpload = new java.util.HashSet();
        this.emptyChoices = java.util.Collections.EMPTY_LIST;
        this.mainHandler = new android.os.Handler(android.os.Looper.getMainLooper());
        if (!isInEditMode()) {
            mortar.Mortar.inject(context, this);
        }
        this.serialExecutor = java.util.concurrent.Executors.newSingleThreadExecutor();
        this.nextAndExitChoices = new java.util.ArrayList();
        this.nextAndExitChoices.add(new com.navdy.hud.app.ui.component.ChoiceLayout.Choice(context.getString(com.navdy.hud.app.R.string.next_recording_step), 1));
        this.nextAndExitChoices.add(new com.navdy.hud.app.ui.component.ChoiceLayout.Choice(context.getString(com.navdy.hud.app.R.string.exit_recording), 0));
        this.exitChoices = new java.util.ArrayList();
        this.exitChoices.add(new com.navdy.hud.app.ui.component.ChoiceLayout.Choice(context.getString(com.navdy.hud.app.R.string.exit_recording), 0));
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
        butterknife.ButterKnife.inject((android.view.View) this);
        this.sideImageView.setVisibility(0);
        this.mainImageView.setVisibility(0);
        this.mainImageView.setImageResource(com.navdy.hud.app.R.drawable.icon_settings_learning_to_gesture_gray);
        findViewById(com.navdy.hud.app.R.id.title2).setVisibility(8);
        findViewById(com.navdy.hud.app.R.id.title4).setVisibility(8);
        this.secondaryText.setSingleLine(false);
        this.secondaryText.setTextSize(1, 17.0f);
        this.mChoiceLayout.setHighlightPersistent(true);
    }

    private void createSession() {
        this.gestureService.setCalibrationEnabled(false);
        com.navdy.hud.app.profile.DriverProfile driverProfile = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile();
        if (driverProfile.isDefaultProfile()) {
            setState(com.navdy.hud.app.view.GestureVideoCaptureView.State.NOT_PAIRED);
            return;
        }
        long time = java.lang.System.currentTimeMillis();
        this.sessionName = driverProfile.getDriverEmail() + "-" + new java.text.SimpleDateFormat("yyyy-MM-dd-HH-mm-ss").format(new java.util.Date(time));
        setState(com.navdy.hud.app.view.GestureVideoCaptureView.State.SWIPE_LEFT);
    }

    private void resetSession() {
        this.gestureService.setCalibrationEnabled(true);
        this.recordingToSave = null;
        this.recordingsToUpload.clear();
        if (this.uploadTimeoutRunnable != null) {
            this.mainHandler.removeCallbacks(this.uploadTimeoutRunnable);
            this.uploadTimeoutRunnable = null;
        }
    }

    /* access modifiers changed from: private */
    public void setState(com.navdy.hud.app.view.GestureVideoCaptureView.State newState) {
        if (this.state != null) {
            switch (this.state) {
                case SWIPE_LEFT:
                case SWIPE_RIGHT:
                case DOUBLE_TAP_1:
                case DOUBLE_TAP_2:
                    stopRecording();
                    break;
            }
        }
        this.prevState = this.state;
        this.state = newState;
        if (this.state == com.navdy.hud.app.view.GestureVideoCaptureView.State.SAVING) {
            this.secondaryText.setText(this.state.detailText);
        } else {
            this.mainText.setText(this.state.mainText);
            if (this.state.detailText != 0) {
                this.secondaryText.setText(this.state.detailText);
            } else {
                this.secondaryText.setText("");
            }
        }
        this.mChoiceLayout.setChoices(com.navdy.hud.app.ui.component.ChoiceLayout.Mode.LABEL, choicesForState(this.state), 0, this);
        switch (this.state) {
            case SWIPE_LEFT:
            case SWIPE_RIGHT:
            case DOUBLE_TAP_1:
            case DOUBLE_TAP_2:
                startRecording();
                return;
            case UPLOADING:
                if (this.recordingsToUpload.isEmpty()) {
                    this.mainHandler.post(new com.navdy.hud.app.view.GestureVideoCaptureView.Anon1());
                    return;
                }
                this.uploadTimeoutRunnable = new com.navdy.hud.app.view.GestureVideoCaptureView.UploadTimeoutRunnable(this.sessionName);
                this.mainHandler.postDelayed(this.uploadTimeoutRunnable, UPLOAD_TIMEOUT);
                return;
            default:
                return;
        }
    }

    private java.util.List<com.navdy.hud.app.ui.component.ChoiceLayout.Choice> choicesForState(com.navdy.hud.app.view.GestureVideoCaptureView.State state2) {
        switch (state2) {
            case SWIPE_LEFT:
            case SWIPE_RIGHT:
            case DOUBLE_TAP_1:
            case DOUBLE_TAP_2:
                return this.nextAndExitChoices;
            case UPLOADING:
            case ALL_DONE:
            case UPLOADING_FAILURE:
            case NOT_PAIRED:
                return this.exitChoices;
            default:
                return this.emptyChoices;
        }
    }

    private com.navdy.hud.app.view.GestureVideoCaptureView.State nextState() {
        switch (this.state) {
            case SWIPE_LEFT:
            case SWIPE_RIGHT:
            case DOUBLE_TAP_1:
            case DOUBLE_TAP_2:
                return com.navdy.hud.app.view.GestureVideoCaptureView.State.SAVING;
            case SAVING:
                switch (this.prevState) {
                    case SWIPE_LEFT:
                        return com.navdy.hud.app.view.GestureVideoCaptureView.State.SWIPE_RIGHT;
                    case SWIPE_RIGHT:
                        return com.navdy.hud.app.view.GestureVideoCaptureView.State.DOUBLE_TAP_1;
                    case DOUBLE_TAP_1:
                        return com.navdy.hud.app.view.GestureVideoCaptureView.State.DOUBLE_TAP_2;
                    case DOUBLE_TAP_2:
                        return com.navdy.hud.app.view.GestureVideoCaptureView.State.UPLOADING;
                    default:
                        return null;
                }
            default:
                return null;
        }
    }

    public void setVisibility(int visibility) {
        super.setVisibility(visibility);
        if (visibility == 0) {
            this.gestureService.setRecordMode(true);
            createSession();
            if (!this.registered) {
                this.bus.register(this);
                this.registered = true;
                return;
            }
            return;
        }
        this.gestureService.stopRecordingVideo();
        resetSession();
        this.gestureService.setRecordMode(false);
        if (this.registered) {
            this.bus.unregister(this);
            this.registered = false;
        }
    }

    public void executeItem(int pos, int id) {
        switch (id) {
            case 0:
                this.mPresenter.hideCaptureView();
                return;
            case 1:
                setState(nextState());
                return;
            default:
                return;
        }
    }

    @com.squareup.otto.Subscribe
    public void onRecordingSaved(com.navdy.hud.app.gesture.GestureServiceConnector.RecordingSaved recordingSaved) {
        sLogger.d("Recording saved");
        com.navdy.hud.app.view.GestureVideoCaptureView.RecordingSavingContext context = this.recordingToSave;
        if (this.recordingToSave == null) {
            sLogger.d("Ignore recording " + recordingSaved.path + " due to missing context.");
        } else if (!this.recordingToSave.sessionName.equals(this.sessionName)) {
            sLogger.d("Recording session " + this.recordingToSave.sessionName + " doesn't match current session " + this.sessionName);
            sLogger.d("Ignore recording " + recordingSaved.path);
        } else {
            this.recordingToSave = null;
            setState(nextState());
            java.lang.String sessionPath = com.navdy.hud.app.storage.PathManager.getInstance().getGestureVideosSyncFolder() + java.io.File.separator + context.sessionName;
            sLogger.d("Session Path : " + sessionPath);
            sLogger.d("Video archive name to be saved : " + context.state.filename);
            java.lang.String videoArchivePath = sessionPath + java.io.File.separator + context.state.filename + ".zip";
            this.recordingsToUpload.add(videoArchivePath);
            this.serialExecutor.submit(new com.navdy.hud.app.view.GestureVideoCaptureView.Anon2(sessionPath, recordingSaved, videoArchivePath, this.sessionName));
        }
    }

    @com.squareup.otto.Subscribe
    public void onUploadFinished(com.navdy.hud.app.service.S3FileUploadService.UploadFinished result) {
        if (result.userTag != null && result.userTag.equals(this.sessionName)) {
            if (!result.succeeded) {
                sLogger.d("Failed to upload: " + result.filePath);
                setState(com.navdy.hud.app.view.GestureVideoCaptureView.State.UPLOADING_FAILURE);
                return;
            }
            this.recordingsToUpload.remove(result.filePath);
            sLogger.d("Remaining files to be uploaded: " + this.recordingsToUpload.size());
            if (this.recordingsToUpload.isEmpty() && this.state == com.navdy.hud.app.view.GestureVideoCaptureView.State.UPLOADING) {
                if (this.uploadTimeoutRunnable != null) {
                    this.mainHandler.removeCallbacks(this.uploadTimeoutRunnable);
                    this.uploadTimeoutRunnable = null;
                }
                setState(com.navdy.hud.app.view.GestureVideoCaptureView.State.ALL_DONE);
            }
        }
    }

    private void stopRecording() {
        sLogger.d("Stop recording for " + this.state.filename);
        this.gestureService.stopRecordingVideo();
    }

    private void startRecording() {
        sLogger.d("Start recording");
        this.recordingToSave = new com.navdy.hud.app.view.GestureVideoCaptureView.RecordingSavingContext(this.sessionName, this.state);
        this.gestureService.startRecordingVideo();
    }

    public void itemSelected(int pos, int id) {
    }

    public boolean onGesture(com.navdy.service.library.events.input.GestureEvent event) {
        return false;
    }

    public boolean onKey(com.navdy.hud.app.manager.InputManager.CustomKeyEvent event) {
        switch (event) {
            case LEFT:
                this.mChoiceLayout.moveSelectionLeft();
                break;
            case RIGHT:
                this.mChoiceLayout.moveSelectionRight();
                break;
            case SELECT:
                this.mChoiceLayout.executeSelectedItem(true);
                break;
        }
        return false;
    }

    public com.navdy.hud.app.manager.InputManager.IInputHandler nextHandler() {
        return null;
    }
}
