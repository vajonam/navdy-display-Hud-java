package com.navdy.hud.app.view;

public final class ForceUpdateView$$InjectAdapter extends dagger.internal.Binding<com.navdy.hud.app.view.ForceUpdateView> implements dagger.MembersInjector<com.navdy.hud.app.view.ForceUpdateView> {
    private dagger.internal.Binding<com.navdy.hud.app.screen.ForceUpdateScreen.Presenter> mPresenter;

    public ForceUpdateView$$InjectAdapter() {
        super(null, "members/com.navdy.hud.app.view.ForceUpdateView", false, com.navdy.hud.app.view.ForceUpdateView.class);
    }

    public void attach(dagger.internal.Linker linker) {
        this.mPresenter = linker.requestBinding("com.navdy.hud.app.screen.ForceUpdateScreen$Presenter", com.navdy.hud.app.view.ForceUpdateView.class, getClass().getClassLoader());
    }

    public void getDependencies(java.util.Set<dagger.internal.Binding<?>> set, java.util.Set<dagger.internal.Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.mPresenter);
    }

    public void injectMembers(com.navdy.hud.app.view.ForceUpdateView object) {
        object.mPresenter = (com.navdy.hud.app.screen.ForceUpdateScreen.Presenter) this.mPresenter.get();
    }
}
