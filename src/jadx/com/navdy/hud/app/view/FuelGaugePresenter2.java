package com.navdy.hud.app.view;

public class FuelGaugePresenter2 extends com.navdy.hud.app.view.DashboardWidgetPresenter {
    public static final int LOW_FUEL_WARNING_LIMIT = 25;
    public static final int VERY_LOW_FUEL_WARNING_LIMIT = 13;
    com.navdy.hud.app.view.drawable.FuelGaugeDrawable2 fuelGaugeDrawable2;
    private java.lang.String fuelGaugeName;
    @butterknife.InjectView(2131624183)
    android.widget.ImageView fuelTankIndicatorLeft;
    @butterknife.InjectView(2131624185)
    android.widget.ImageView fuelTankIndicatorRight;
    @butterknife.InjectView(2131624184)
    android.widget.ImageView fuelTypeIndicator;
    @butterknife.InjectView(2131624182)
    android.widget.ImageView lowFuelIndicatorLeft;
    @butterknife.InjectView(2131624186)
    android.widget.ImageView lowFuelIndicatorRight;
    private android.content.Context mContext;
    private int mFuelLevel;
    private int mFuelRange;
    private java.lang.String mFuelRangeUnit = "";
    private boolean mLeftOriented = true;
    private com.navdy.hud.app.view.FuelGaugePresenter2.FuelTankSide mTanksSide = com.navdy.hud.app.view.FuelGaugePresenter2.FuelTankSide.UNKNOWN;
    @butterknife.InjectView(2131624130)
    android.widget.TextView rangeText;
    @butterknife.InjectView(2131624129)
    android.widget.TextView rangeUnitText;

    public enum FuelTankSide {
        UNKNOWN,
        LEFT,
        RIGHT
    }

    public FuelGaugePresenter2(android.content.Context context) {
        this.mContext = context;
        this.fuelGaugeDrawable2 = new com.navdy.hud.app.view.drawable.FuelGaugeDrawable2(context, com.navdy.hud.app.R.array.smart_dash_fuel_gauge2_state_colors);
        this.fuelGaugeDrawable2.setMinValue(0.0f);
        this.fuelGaugeDrawable2.setMaxGaugeValue(100.0f);
        this.fuelGaugeName = context.getResources().getString(com.navdy.hud.app.R.string.widget_fuel);
    }

    public void setFuelLevel(int mFuelLevel2) {
        this.mFuelLevel = mFuelLevel2;
        reDraw();
    }

    public void setFuelTankSide(com.navdy.hud.app.view.FuelGaugePresenter2.FuelTankSide tankSide) {
        this.mTanksSide = tankSide;
    }

    public void setFuelRange(int value) {
        this.mFuelRange = value;
    }

    public void setFuelRangeUnit(java.lang.String unitText) {
        this.mFuelRangeUnit = unitText;
    }

    public void setView(com.navdy.hud.app.view.DashboardWidgetView dashboardWidgetView, android.os.Bundle arguments) {
        int layoutResourceId = com.navdy.hud.app.R.layout.fuel_gauge_left;
        if (arguments != null) {
            switch (arguments.getInt(com.navdy.hud.app.view.DashboardWidgetPresenter.EXTRA_GRAVITY)) {
                case 0:
                    layoutResourceId = com.navdy.hud.app.R.layout.fuel_gauge_left;
                    this.mLeftOriented = true;
                    break;
                case 2:
                    layoutResourceId = com.navdy.hud.app.R.layout.fuel_gauge_right;
                    this.mLeftOriented = false;
                    break;
            }
        }
        if (dashboardWidgetView != null) {
            dashboardWidgetView.setContentView(layoutResourceId);
            butterknife.ButterKnife.inject((java.lang.Object) this, (android.view.View) dashboardWidgetView);
        }
        super.setView(dashboardWidgetView, arguments);
        reDraw();
    }

    public android.graphics.drawable.Drawable getDrawable() {
        return this.fuelGaugeDrawable2;
    }

    /* access modifiers changed from: protected */
    public void updateGauge() {
        boolean veryLowFuel;
        int i = 1;
        if (this.mWidgetView != null) {
            boolean lowFuel = this.mFuelLevel <= 25;
            if (this.mFuelLevel <= 13) {
                veryLowFuel = true;
            } else {
                veryLowFuel = false;
            }
            this.fuelGaugeDrawable2.setGaugeValue((float) this.mFuelLevel);
            this.fuelGaugeDrawable2.setLeftOriented(this.mLeftOriented);
            com.navdy.hud.app.view.drawable.FuelGaugeDrawable2 fuelGaugeDrawable22 = this.fuelGaugeDrawable2;
            if (!lowFuel) {
                i = 0;
            } else if (veryLowFuel) {
                i = 2;
            }
            fuelGaugeDrawable22.setState(i);
            this.lowFuelIndicatorLeft.setVisibility(8);
            this.lowFuelIndicatorRight.setVisibility(8);
            switch (this.mTanksSide) {
                case UNKNOWN:
                    this.fuelTankIndicatorLeft.setVisibility(8);
                    this.fuelTankIndicatorRight.setVisibility(8);
                    if (veryLowFuel) {
                        this.lowFuelIndicatorLeft.setVisibility(0);
                        break;
                    }
                    break;
                case LEFT:
                    this.fuelTankIndicatorLeft.setVisibility(0);
                    this.fuelTankIndicatorRight.setVisibility(8);
                    if (veryLowFuel) {
                        this.lowFuelIndicatorRight.setVisibility(0);
                        break;
                    }
                    break;
                case RIGHT:
                    this.fuelTankIndicatorLeft.setVisibility(8);
                    this.fuelTankIndicatorRight.setVisibility(0);
                    if (veryLowFuel) {
                        this.lowFuelIndicatorLeft.setVisibility(0);
                        break;
                    }
                    break;
            }
            if (this.mFuelRange > 0) {
                this.rangeText.setText(java.lang.Integer.toString(this.mFuelRange));
            } else {
                this.rangeText.setText("");
            }
            this.rangeUnitText.setText(this.mFuelRangeUnit);
        }
    }

    public java.lang.String getWidgetIdentifier() {
        return com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.FUEL_GAUGE_ID;
    }

    public java.lang.String getWidgetName() {
        return this.fuelGaugeName;
    }
}
