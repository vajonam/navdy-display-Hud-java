package com.navdy.hud.app.view;

public class FirstLaunchView extends android.widget.RelativeLayout implements com.navdy.hud.app.manager.InputManager.IInputHandler {
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.view.FirstLaunchView.class);
    @butterknife.InjectView(2131624324)
    public android.widget.ImageView firstLaunchLogo;
    @javax.inject.Inject
    com.navdy.hud.app.screen.FirstLaunchScreen.Presenter presenter;

    public FirstLaunchView(android.content.Context context) {
        this(context, null);
    }

    public FirstLaunchView(android.content.Context context, android.util.AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public FirstLaunchView(android.content.Context context, android.util.AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if (!isInEditMode()) {
            mortar.Mortar.inject(context, this);
        }
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
        butterknife.ButterKnife.inject((android.view.View) this);
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        sLogger.v("onAttachToWindow");
        super.onAttachedToWindow();
        com.navdy.hud.app.framework.toast.ToastManager.getInstance().disableToasts(true);
        if (this.presenter != null) {
            this.presenter.takeView(this);
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        sLogger.v("onDetachToWindow");
        com.navdy.hud.app.framework.toast.ToastManager.getInstance().disableToasts(false);
        super.onDetachedFromWindow();
        if (this.presenter != null) {
            this.presenter.dropView(this);
        }
    }

    public boolean onGesture(com.navdy.service.library.events.input.GestureEvent event) {
        return true;
    }

    public boolean onKey(com.navdy.hud.app.manager.InputManager.CustomKeyEvent event) {
        return true;
    }

    public com.navdy.hud.app.manager.InputManager.IInputHandler nextHandler() {
        return null;
    }
}
