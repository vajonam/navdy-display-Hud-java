package com.navdy.hud.app.view;

public class FontTextView extends android.widget.TextView {
    private static java.util.HashMap<java.lang.String, android.graphics.Typeface> mCachedTypeFaces = new java.util.HashMap<>();

    public FontTextView(android.content.Context context) {
        this(context, null);
    }

    public FontTextView(android.content.Context context, android.util.AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public FontTextView(android.content.Context context, android.util.AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        android.content.res.TypedArray customAttributes = context.obtainStyledAttributes(attrs, com.navdy.hud.app.R.styleable.FontTextView, defStyleAttr, 0);
        if (customAttributes != null) {
            java.lang.String fontFileName = customAttributes.getString(0);
            if (!android.text.TextUtils.isEmpty(fontFileName)) {
                setTypeface(createFromAsset(context.getAssets(), fontFileName));
            }
            customAttributes.recycle();
        }
    }

    public static android.graphics.Typeface createFromAsset(android.content.res.AssetManager assetManager, java.lang.String path) {
        if (mCachedTypeFaces.containsKey(path)) {
            return (android.graphics.Typeface) mCachedTypeFaces.get(path);
        }
        android.graphics.Typeface typeFace = android.graphics.Typeface.createFromAsset(assetManager, path);
        if (typeFace != null) {
            mCachedTypeFaces.put(path, typeFace);
        }
        return typeFace;
    }
}
