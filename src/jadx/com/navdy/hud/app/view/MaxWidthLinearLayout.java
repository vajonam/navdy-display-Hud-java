package com.navdy.hud.app.view;

public class MaxWidthLinearLayout extends android.widget.LinearLayout {
    private int maxWidth;

    public MaxWidthLinearLayout(android.content.Context context) {
        super(context);
        this.maxWidth = 0;
    }

    public MaxWidthLinearLayout(android.content.Context context, android.util.AttributeSet attrs) {
        super(context, attrs);
        android.content.res.TypedArray a = getContext().obtainStyledAttributes(attrs, com.navdy.hud.app.R.styleable.MaxWidthLinearLayout);
        this.maxWidth = a.getDimensionPixelSize(0, Integer.MAX_VALUE);
        a.recycle();
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int measuredWidth = android.view.View.MeasureSpec.getSize(widthMeasureSpec);
        if (this.maxWidth > 0 && this.maxWidth < measuredWidth) {
            widthMeasureSpec = android.view.View.MeasureSpec.makeMeasureSpec(this.maxWidth, android.view.View.MeasureSpec.getMode(widthMeasureSpec));
        }
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    public void setMaxWidth(int n) {
        this.maxWidth = n;
        invalidate();
    }

    public int getMaxWidth() {
        return this.maxWidth;
    }
}
