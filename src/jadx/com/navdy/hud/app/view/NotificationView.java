package com.navdy.hud.app.view;

public class NotificationView extends android.widget.RelativeLayout implements com.navdy.hud.app.manager.InputManager.IInputHandler, com.navdy.hud.app.gesture.GestureDetector.GestureListener {
    private static final int CLICK_COUNT = 2;
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.view.NotificationView.class);
    @butterknife.InjectView(2131624270)
    public com.navdy.hud.app.ui.component.ShrinkingBorderView border;
    private com.navdy.hud.app.ui.component.ShrinkingBorderView.IListener borderListener;
    private com.navdy.hud.app.gesture.MultipleClickGestureDetector clickGestureDetector;
    private com.navdy.hud.app.gesture.MultipleClickGestureDetector.IMultipleClickKeyGesture clickGestureListener;
    @butterknife.InjectView(2131624269)
    android.widget.FrameLayout customNotificationContainer;
    private int defaultColor;
    /* access modifiers changed from: private */
    public boolean initialized;
    @butterknife.InjectView(2131624268)
    public com.navdy.hud.app.ui.component.image.ColorImageView nextNotificationColorView;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.framework.notifications.INotification notification;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.framework.notifications.NotificationManager notificationManager;
    @javax.inject.Inject
    com.navdy.hud.app.presenter.NotificationPresenter presenter;

    class Anon1 implements com.navdy.hud.app.gesture.MultipleClickGestureDetector.IMultipleClickKeyGesture {
        Anon1() {
        }

        public void onMultipleClick(int count) {
            com.navdy.hud.app.view.NotificationView.this.takeNotificationAction(false);
        }

        public boolean onGesture(com.navdy.service.library.events.input.GestureEvent event) {
            return false;
        }

        public boolean onKey(com.navdy.hud.app.manager.InputManager.CustomKeyEvent event) {
            if (com.navdy.hud.app.view.NotificationView.this.notification == null) {
                return false;
            }
            if (com.navdy.hud.app.view.NotificationView.this.notificationManager.isExpanded() || com.navdy.hud.app.view.NotificationView.this.notificationManager.isExpandedNotificationVisible()) {
                return com.navdy.hud.app.view.NotificationView.this.notificationManager.handleKey(event);
            }
            return com.navdy.hud.app.view.NotificationView.this.notification.onKey(event);
        }

        public com.navdy.hud.app.manager.InputManager.IInputHandler nextHandler() {
            return null;
        }
    }

    class Anon2 implements com.navdy.hud.app.ui.component.ShrinkingBorderView.IListener {
        Anon2() {
        }

        public void timeout() {
            if (com.navdy.hud.app.view.NotificationView.this.initialized) {
                com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().currentNotificationTimeout();
            }
        }
    }

    public NotificationView(android.content.Context context) {
        this(context, null);
    }

    public NotificationView(android.content.Context context, android.util.AttributeSet attrs) {
        super(context, attrs);
        this.clickGestureListener = new com.navdy.hud.app.view.NotificationView.Anon1();
        this.borderListener = new com.navdy.hud.app.view.NotificationView.Anon2();
        if (!isInEditMode()) {
            mortar.Mortar.inject(context, this);
            this.notificationManager = com.navdy.hud.app.framework.notifications.NotificationManager.getInstance();
            this.clickGestureDetector = new com.navdy.hud.app.gesture.MultipleClickGestureDetector(2, this.clickGestureListener);
        }
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.defaultColor = getContext().getResources().getColor(17170444);
        if (this.presenter != null) {
            this.presenter.takeView(this);
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.presenter != null) {
            this.presenter.dropView(this);
        }
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
        butterknife.ButterKnife.inject((android.view.View) this);
        this.border.setListener(this.borderListener);
        resetNextNotificationColor();
    }

    public void onClick() {
    }

    public void onTrackHand(float normalized) {
    }

    public boolean onGesture(com.navdy.service.library.events.input.GestureEvent event) {
        if (!this.initialized || this.notification == null || event == null || event.gesture == null) {
            return false;
        }
        if (this.notificationManager.isAnimating()) {
            sLogger.v("ignore gesture, animation in progress");
            return true;
        }
        switch (event.gesture) {
            case GESTURE_SWIPE_LEFT:
                if (!this.notificationManager.isExpanded() && !this.notificationManager.isExpandedNotificationVisible()) {
                    if (!this.notification.onGesture(event)) {
                        com.navdy.hud.app.analytics.AnalyticsSupport.recordGlanceAction(com.navdy.hud.app.analytics.AnalyticsSupport.ANALYTICS_EVENT_GLANCE_OPEN_FULL, this.notification, "swipe");
                        sLogger.v("gesture expanded:" + this.notification.expandNotification());
                        break;
                    } else {
                        sLogger.v("gesture handled by notif:" + this.notification.getType());
                        break;
                    }
                } else if (this.notificationManager.isExpanded() || this.notificationManager.isExpandedNotificationVisible()) {
                    this.notificationManager.moveNext(true);
                    break;
                }
                break;
            case GESTURE_SWIPE_RIGHT:
                boolean handled = false;
                if (this.notification != null && !this.notificationManager.isExpanded() && !this.notificationManager.isExpandedNotificationVisible()) {
                    handled = this.notification.onGesture(event);
                }
                if (handled) {
                    sLogger.v("gesture handled by notif:" + this.notification.getType());
                    break;
                } else {
                    takeNotificationAction(true);
                    break;
                }
        }
        return true;
    }

    public boolean onKey(com.navdy.hud.app.manager.InputManager.CustomKeyEvent event) {
        if (!this.initialized) {
            return false;
        }
        return this.clickGestureDetector.onKey(event);
    }

    public com.navdy.hud.app.manager.InputManager.IInputHandler nextHandler() {
        return null;
    }

    public void clearNotification() {
        this.customNotificationContainer.removeAllViews();
    }

    public void addCustomView(com.navdy.hud.app.framework.notifications.INotification notification2, android.view.View view) {
        removeCustomView();
        this.customNotificationContainer.addView(view, new android.widget.FrameLayout.LayoutParams(-1, -1));
        this.notification = notification2;
        this.initialized = true;
    }

    public void removeCustomView() {
        this.customNotificationContainer.removeAllViews();
        this.border.stopTimeout(false, null);
        this.initialized = false;
    }

    public void switchNotfication(com.navdy.hud.app.framework.notifications.INotification notification2) {
        this.notification = notification2;
    }

    public void setNextNotificationColor(int color) {
        this.nextNotificationColorView.setColor(color);
    }

    public void resetNextNotificationColor() {
        this.nextNotificationColorView.setColor(this.defaultColor);
    }

    public void showNextNotificationColor() {
        this.nextNotificationColorView.setVisibility(0);
    }

    public void hideNextNotificationColor() {
        this.nextNotificationColorView.setVisibility(4);
    }

    /* access modifiers changed from: private */
    public void takeNotificationAction(boolean quickly) {
        if (this.notification == null) {
            return;
        }
        if (this.notificationManager.isExpanded() || this.notificationManager.isExpandedNotificationVisible()) {
            boolean completely = false;
            if (this.notificationManager.isCurrentItemDeleteAll()) {
                com.navdy.hud.app.analytics.AnalyticsSupport.recordGlanceAction(com.navdy.hud.app.analytics.AnalyticsSupport.ANALYTICS_EVENT_GLANCE_DISMISS, null, quickly ? "swipe" : "doubleclick");
                completely = true;
                quickly = true;
            } else {
                com.navdy.hud.app.analytics.AnalyticsSupport.recordGlanceAction(com.navdy.hud.app.analytics.AnalyticsSupport.ANALYTICS_EVENT_GLANCE_OPEN_MINI, this.notification, quickly ? "swipe" : "doubleclick");
            }
            this.notificationManager.collapseExpandedNotification(completely, quickly);
            return;
        }
        com.navdy.hud.app.analytics.AnalyticsSupport.recordGlanceAction(com.navdy.hud.app.analytics.AnalyticsSupport.ANALYTICS_EVENT_GLANCE_DISMISS, this.notification, quickly ? "swipe" : "doubleclick");
        this.notificationManager.collapseNotification();
    }

    public android.view.View getCurrentNotificationViewChild() {
        if (this.customNotificationContainer.getChildCount() > 0) {
            return this.customNotificationContainer.getChildAt(0);
        }
        return null;
    }

    public android.view.ViewGroup getNotificationContainer() {
        return this.customNotificationContainer;
    }
}
