package com.navdy.hud.app.view;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\b\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\b\u0002"}, d2 = {"<anonymous>", "", "run"}, k = 3, mv = {1, 1, 6})
/* compiled from: DriveScoreGaugePresenter.kt */
final class DriveScoreGaugePresenter$clearWarning$Anon1 implements java.lang.Runnable {
    final /* synthetic */ com.navdy.hud.app.view.DriveScoreGaugePresenter this$Anon0;

    DriveScoreGaugePresenter$clearWarning$Anon1(com.navdy.hud.app.view.DriveScoreGaugePresenter driveScoreGaugePresenter) {
        this.this$Anon0 = driveScoreGaugePresenter;
    }

    public final void run() {
        this.this$Anon0.setNewEvent(false);
        this.this$Anon0.reDraw();
    }
}
