package com.navdy.hud.app.view;

public class WelcomeView extends android.widget.FrameLayout implements com.navdy.hud.app.manager.InputManager.IInputHandler {
    /* access modifiers changed from: private */
    public static final int DOWNLOAD_APP_STAY_INTERVAL = (((int) java.util.concurrent.TimeUnit.SECONDS.toMillis(5)) - DOWNLOAD_APP_TRANSITION_INTERVAL);
    /* access modifiers changed from: private */
    public static final int DOWNLOAD_APP_TRANSITION_INTERVAL = ((int) java.util.concurrent.TimeUnit.MILLISECONDS.toMillis(200));
    public static final int STATE_DOWNLOAD_APP = 1;
    public static final int STATE_PICKER = 2;
    public static final int STATE_UNKNOWN = -1;
    public static final int STATE_WELCOME = 3;
    /* access modifiers changed from: private */
    public static int[] appStoreIcons;
    private static int downloadColor1;
    private static int downloadColor2;
    private static java.lang.String[] downloadText1;
    private static java.lang.String[] downloadText2;
    /* access modifiers changed from: private */
    public static int[] playStoreIcons;
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.view.WelcomeView.class);
    /* access modifiers changed from: private */
    public java.lang.Runnable animationRunnable;
    @butterknife.InjectView(2131624410)
    public com.navdy.hud.app.ui.component.FluctuatorAnimatorView animatorView;
    @butterknife.InjectView(2131624406)
    public android.widget.ImageView appStoreImage1;
    @butterknife.InjectView(2131624407)
    public android.widget.ImageView appStoreImage2;
    @butterknife.InjectView(2131624413)
    public com.navdy.hud.app.ui.component.carousel.CarouselLayout carousel;
    @butterknife.InjectView(2131624150)
    public com.navdy.hud.app.ui.component.ChoiceLayout choiceLayout;
    /* access modifiers changed from: private */
    public int currentDownloadIndex;
    /* access modifiers changed from: private */
    public int currentDownloadView;
    @butterknife.InjectView(2131624116)
    public android.widget.RelativeLayout downloadAppContainer;
    @butterknife.InjectView(2131624403)
    public android.widget.TextView downloadAppTextView1;
    @butterknife.InjectView(2131624404)
    public android.widget.TextView downloadAppTextView2;
    /* access modifiers changed from: private */
    public android.os.Handler handler;
    private int initialState;
    @butterknife.InjectView(2131624411)
    public android.widget.ImageView leftDot;
    public java.util.List<com.navdy.hud.app.ui.component.carousel.Carousel.Model> list;
    @butterknife.InjectView(2131624206)
    public android.widget.TextView messageView;
    @butterknife.InjectView(2131624408)
    public android.widget.ImageView playStoreImage1;
    @butterknife.InjectView(2131624409)
    public android.widget.ImageView playStoreImage2;
    @javax.inject.Inject
    public com.navdy.hud.app.screen.WelcomeScreen.Presenter presenter;
    @butterknife.InjectView(2131624412)
    public android.widget.ImageView rightDot;
    @butterknife.InjectView(2131624271)
    public android.view.View rootContainer;
    com.squareup.picasso.Transformation roundTransformation;
    private boolean searching;
    private int state;
    @butterknife.InjectView(2131624213)
    public android.widget.TextView subTitleView;
    @butterknife.InjectView(2131624135)
    public android.widget.TextView titleView;

    class Anon1 implements java.lang.Runnable {

        /* renamed from: com.navdy.hud.app.view.WelcomeView$Anon1$Anon1 reason: collision with other inner class name */
        class C0039Anon1 implements java.lang.Runnable {
            final /* synthetic */ android.widget.ImageView val$appView;
            final /* synthetic */ android.widget.ImageView val$playView;
            final /* synthetic */ android.widget.TextView val$txtView;

            C0039Anon1(android.widget.TextView textView, android.widget.ImageView imageView, android.widget.ImageView imageView2) {
                this.val$txtView = textView;
                this.val$appView = imageView;
                this.val$playView = imageView2;
            }

            public void run() {
                com.navdy.hud.app.view.WelcomeView.this.currentDownloadIndex = com.navdy.hud.app.view.WelcomeView.this.currentDownloadIndex + 1;
                if (com.navdy.hud.app.view.WelcomeView.this.currentDownloadIndex == com.navdy.hud.app.view.WelcomeView.appStoreIcons.length) {
                    com.navdy.hud.app.view.WelcomeView.this.currentDownloadIndex = 0;
                }
                com.navdy.hud.app.view.WelcomeView.this.handler.removeCallbacks(com.navdy.hud.app.view.WelcomeView.this.animationRunnable);
                com.navdy.hud.app.view.WelcomeView.this.handler.postDelayed(com.navdy.hud.app.view.WelcomeView.this.animationRunnable, (long) com.navdy.hud.app.view.WelcomeView.DOWNLOAD_APP_STAY_INTERVAL);
                com.navdy.hud.app.view.WelcomeView.this.setDownloadText(this.val$txtView, com.navdy.hud.app.view.WelcomeView.this.currentDownloadIndex);
                com.navdy.hud.app.view.WelcomeView.this.setDownloadImage(this.val$appView, com.navdy.hud.app.view.WelcomeView.appStoreIcons[com.navdy.hud.app.view.WelcomeView.this.currentDownloadIndex]);
                com.navdy.hud.app.view.WelcomeView.this.setDownloadImage(this.val$playView, com.navdy.hud.app.view.WelcomeView.playStoreIcons[com.navdy.hud.app.view.WelcomeView.this.currentDownloadIndex]);
            }
        }

        Anon1() {
        }

        public void run() {
            if (com.navdy.hud.app.view.WelcomeView.this.currentDownloadIndex != -1) {
                android.widget.TextView current = null;
                android.widget.TextView next = null;
                android.widget.ImageView appStoreCurrent = null;
                android.widget.ImageView appStoreNext = null;
                android.widget.ImageView playStoreCurrent = null;
                android.widget.ImageView playStoreNext = null;
                switch (com.navdy.hud.app.view.WelcomeView.this.currentDownloadView) {
                    case 0:
                        current = com.navdy.hud.app.view.WelcomeView.this.downloadAppTextView1;
                        next = com.navdy.hud.app.view.WelcomeView.this.downloadAppTextView2;
                        appStoreCurrent = com.navdy.hud.app.view.WelcomeView.this.appStoreImage1;
                        appStoreNext = com.navdy.hud.app.view.WelcomeView.this.appStoreImage2;
                        playStoreCurrent = com.navdy.hud.app.view.WelcomeView.this.playStoreImage1;
                        playStoreNext = com.navdy.hud.app.view.WelcomeView.this.playStoreImage2;
                        com.navdy.hud.app.view.WelcomeView.this.currentDownloadView = 1;
                        break;
                    case 1:
                        current = com.navdy.hud.app.view.WelcomeView.this.downloadAppTextView2;
                        next = com.navdy.hud.app.view.WelcomeView.this.downloadAppTextView1;
                        appStoreCurrent = com.navdy.hud.app.view.WelcomeView.this.appStoreImage2;
                        appStoreNext = com.navdy.hud.app.view.WelcomeView.this.appStoreImage1;
                        playStoreCurrent = com.navdy.hud.app.view.WelcomeView.this.playStoreImage2;
                        playStoreNext = com.navdy.hud.app.view.WelcomeView.this.playStoreImage1;
                        com.navdy.hud.app.view.WelcomeView.this.currentDownloadView = 0;
                        break;
                }
                android.widget.TextView txtView = current;
                android.widget.ImageView appView = appStoreCurrent;
                android.widget.ImageView playView = playStoreCurrent;
                current.animate().alpha(0.0f).setDuration((long) com.navdy.hud.app.view.WelcomeView.DOWNLOAD_APP_TRANSITION_INTERVAL);
                next.animate().alpha(1.0f).setDuration((long) com.navdy.hud.app.view.WelcomeView.DOWNLOAD_APP_TRANSITION_INTERVAL).withEndAction(new com.navdy.hud.app.view.WelcomeView.Anon1.C0039Anon1(txtView, appView, playView));
                appStoreCurrent.animate().alpha(0.0f).setDuration((long) com.navdy.hud.app.view.WelcomeView.DOWNLOAD_APP_TRANSITION_INTERVAL);
                appStoreNext.animate().alpha(1.0f).setDuration((long) com.navdy.hud.app.view.WelcomeView.DOWNLOAD_APP_TRANSITION_INTERVAL);
                playStoreCurrent.animate().alpha(0.0f).setDuration((long) com.navdy.hud.app.view.WelcomeView.DOWNLOAD_APP_TRANSITION_INTERVAL);
                playStoreNext.animate().alpha(1.0f).setDuration((long) com.navdy.hud.app.view.WelcomeView.DOWNLOAD_APP_TRANSITION_INTERVAL);
            }
        }
    }

    class Anon2 implements com.navdy.hud.app.ui.component.ChoiceLayout.IListener {
        Anon2() {
        }

        public void executeItem(int pos, int id) {
            com.navdy.hud.app.view.WelcomeView.this.presenter.cancel();
        }

        public void itemSelected(int pos, int id) {
        }
    }

    class Anon3 implements com.navdy.hud.app.ui.component.carousel.Carousel.ViewProcessor {

        class Anon1 implements java.lang.Runnable {
            final /* synthetic */ com.navdy.service.library.device.NavdyDeviceId val$deviceId;
            final /* synthetic */ java.io.File val$imagePath;
            final /* synthetic */ java.lang.String val$initials;
            final /* synthetic */ com.navdy.hud.app.ui.component.image.InitialsImageView val$initialsImageView;
            final /* synthetic */ android.widget.ImageView val$view;

            /* renamed from: com.navdy.hud.app.view.WelcomeView$Anon3$Anon1$Anon1 reason: collision with other inner class name */
            class C0040Anon1 implements java.lang.Runnable {
                final /* synthetic */ boolean val$imageExists;

                C0040Anon1(boolean z) {
                    this.val$imageExists = z;
                }

                public void run() {
                    if (com.navdy.hud.app.view.WelcomeView.Anon3.Anon1.this.val$view.getTag() == com.navdy.hud.app.view.WelcomeView.Anon3.Anon1.this.val$deviceId) {
                        if (!this.val$imageExists) {
                            com.navdy.hud.app.view.WelcomeView.Anon3.Anon1.this.val$initialsImageView.setInitials(com.navdy.hud.app.view.WelcomeView.Anon3.Anon1.this.val$initials, com.navdy.hud.app.ui.component.image.InitialsImageView.Style.LARGE);
                        } else {
                            com.navdy.hud.app.view.WelcomeView.this.setImage(com.navdy.hud.app.view.WelcomeView.Anon3.Anon1.this.val$imagePath, com.navdy.hud.app.view.WelcomeView.Anon3.Anon1.this.val$initialsImageView, null);
                        }
                    }
                }
            }

            Anon1(android.widget.ImageView imageView, com.navdy.service.library.device.NavdyDeviceId navdyDeviceId, java.io.File file, com.navdy.hud.app.ui.component.image.InitialsImageView initialsImageView, java.lang.String str) {
                this.val$view = imageView;
                this.val$deviceId = navdyDeviceId;
                this.val$imagePath = file;
                this.val$initialsImageView = initialsImageView;
                this.val$initials = str;
            }

            public void run() {
                if (this.val$view.getTag() == this.val$deviceId) {
                    com.navdy.hud.app.view.WelcomeView.this.handler.post(new com.navdy.hud.app.view.WelcomeView.Anon3.Anon1.C0040Anon1(this.val$imagePath.exists()));
                }
            }
        }

        Anon3() {
        }

        public void processSmallImageView(com.navdy.hud.app.ui.component.carousel.Carousel.Model model, android.view.View view, int pos, int width, int height) {
            com.navdy.hud.app.ui.component.image.CrossFadeImageView crossFadeImageView = (com.navdy.hud.app.ui.component.image.CrossFadeImageView) view;
            updateInitials(false, model, (com.navdy.hud.app.ui.component.image.InitialsImageView) crossFadeImageView.getSmall());
            updateInitials(true, model, (com.navdy.hud.app.ui.component.image.InitialsImageView) crossFadeImageView.getBig());
        }

        public void processLargeImageView(com.navdy.hud.app.ui.component.carousel.Carousel.Model model, android.view.View view, int pos, int width, int height) {
            com.navdy.hud.app.ui.component.image.CrossFadeImageView crossFadeImageView = (com.navdy.hud.app.ui.component.image.CrossFadeImageView) view;
            updateInitials(false, model, (com.navdy.hud.app.ui.component.image.InitialsImageView) crossFadeImageView.getSmall());
            updateInitials(true, model, (com.navdy.hud.app.ui.component.image.InitialsImageView) crossFadeImageView.getBig());
        }

        private void updateInitials(boolean largeImage, com.navdy.hud.app.ui.component.carousel.Carousel.Model model, android.widget.ImageView view) {
            com.navdy.hud.app.ui.component.image.InitialsImageView initialsImageView = (com.navdy.hud.app.ui.component.image.InitialsImageView) view;
            if (model.id == com.navdy.hud.app.R.id.welcome_menu_add_driver || model.id == com.navdy.hud.app.R.id.welcome_menu_searching) {
                initialsImageView.setInitials(null, com.navdy.hud.app.ui.component.image.InitialsImageView.Style.LARGE);
                return;
            }
            com.navdy.hud.app.screen.WelcomeScreen.DeviceMetadata device = (com.navdy.hud.app.screen.WelcomeScreen.DeviceMetadata) model.extras;
            view.setAlpha((!(model.id >= 100) || largeImage) ? 1.0f : 0.5f);
            com.navdy.service.library.device.NavdyDeviceId deviceId = device.deviceId;
            view.setTag(deviceId);
            java.util.Map<java.lang.Integer, java.lang.String> infoMap = model.infoMap;
            java.lang.String initials = com.navdy.hud.app.framework.contacts.ContactUtil.getInitials(infoMap != null ? (java.lang.String) infoMap.get(java.lang.Integer.valueOf(com.navdy.hud.app.R.id.subTitle)) : "");
            initialsImageView.setInitials(null, com.navdy.hud.app.ui.component.image.InitialsImageView.Style.DEFAULT);
            java.io.File imagePath = device.driverProfile.getDriverImageFile();
            android.graphics.Bitmap bitmap = com.navdy.hud.app.util.picasso.PicassoUtil.getBitmapfromCache(imagePath);
            if (bitmap != null) {
                initialsImageView.setImageBitmap(bitmap);
            } else {
                com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.view.WelcomeView.Anon3.Anon1(view, deviceId, imagePath, initialsImageView, initials), 1);
            }
        }

        public void processInfoView(com.navdy.hud.app.ui.component.carousel.Carousel.Model model, android.view.View view, int pos) {
        }
    }

    class Anon4 implements com.navdy.hud.app.ui.component.carousel.Carousel.Listener {
        Anon4() {
        }

        public void onCurrentItemChanging(int pos, int newPos, int id) {
            int i = 0;
            if (newPos > pos) {
                android.widget.ImageView imageView = com.navdy.hud.app.view.WelcomeView.this.rightDot;
                if (newPos + 2 >= com.navdy.hud.app.view.WelcomeView.this.carousel.getCount()) {
                    i = 8;
                }
                imageView.setVisibility(i);
            } else {
                android.widget.ImageView imageView2 = com.navdy.hud.app.view.WelcomeView.this.leftDot;
                if (newPos <= 1) {
                    i = 8;
                }
                imageView2.setVisibility(i);
            }
            com.navdy.hud.app.view.WelcomeView.this.updateInfo(com.navdy.hud.app.view.WelcomeView.this.carousel.getModel(newPos));
        }

        public void onCurrentItemChanged(int pos, int id) {
            int i;
            int i2 = 0;
            android.widget.ImageView imageView = com.navdy.hud.app.view.WelcomeView.this.leftDot;
            if (pos > 1) {
                i = 0;
            } else {
                i = 8;
            }
            imageView.setVisibility(i);
            android.widget.ImageView imageView2 = com.navdy.hud.app.view.WelcomeView.this.rightDot;
            if (pos + 2 >= com.navdy.hud.app.view.WelcomeView.this.carousel.getCount()) {
                i2 = 8;
            }
            imageView2.setVisibility(i2);
            com.navdy.hud.app.ui.component.carousel.Carousel.Model model = com.navdy.hud.app.view.WelcomeView.this.carousel.getModel(pos);
            com.navdy.hud.app.view.WelcomeView.this.updateInfo(model);
            if (com.navdy.hud.app.view.WelcomeView.this.presenter != null) {
                com.navdy.hud.app.view.WelcomeView.this.presenter.onCurrentItemChanged(pos, model.id);
            }
        }

        public void onExecuteItem(int id, int pos) {
            if (com.navdy.hud.app.view.WelcomeView.this.presenter != null) {
                com.navdy.hud.app.view.WelcomeView.this.presenter.executeItem(id, pos);
            }
        }

        public void onExit() {
        }
    }

    public WelcomeView(android.content.Context context) {
        this(context, null);
    }

    public WelcomeView(android.content.Context context, android.util.AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public WelcomeView(android.content.Context context, android.util.AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.state = -1;
        this.currentDownloadIndex = -1;
        this.currentDownloadView = -1;
        this.list = new java.util.ArrayList();
        this.roundTransformation = new com.makeramen.RoundedTransformationBuilder().oval(true).build();
        this.handler = new android.os.Handler(android.os.Looper.getMainLooper());
        this.animationRunnable = new com.navdy.hud.app.view.WelcomeView.Anon1();
        if (!isInEditMode()) {
            mortar.Mortar.inject(context, this);
        }
        initFromAttributes(context, attrs);
    }

    private void initFromAttributes(android.content.Context context, android.util.AttributeSet attrs) {
        android.content.res.TypedArray a = context.getTheme().obtainStyledAttributes(attrs, com.navdy.hud.app.R.styleable.WelcomeView, 0, 0);
        try {
            this.initialState = a.getInt(0, 1);
        } finally {
            a.recycle();
        }
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
        butterknife.ButterKnife.inject((android.view.View) this);
        setState(this.initialState);
        initializeCarousel();
        java.util.List<com.navdy.hud.app.ui.component.ChoiceLayout.Choice> choices = new java.util.ArrayList<>(1);
        choices.add(new com.navdy.hud.app.ui.component.ChoiceLayout.Choice(getResources().getString(com.navdy.hud.app.R.string.welcome_cancel), 0));
        this.choiceLayout.setChoices(com.navdy.hud.app.ui.component.ChoiceLayout.Mode.LABEL, choices, 0, new com.navdy.hud.app.view.WelcomeView.Anon2());
    }

    public void setState(int state2) {
        if (this.state != state2) {
            this.state = state2;
            this.rootContainer.setVisibility(8);
            stopDownloadAppAnimation();
            this.leftDot.setVisibility(8);
            this.rightDot.setVisibility(8);
            switch (state2) {
                case 1:
                    startDownloadAppAnimation();
                    return;
                case 2:
                case 3:
                    this.rootContainer.setVisibility(0);
                    return;
                default:
                    return;
            }
        }
    }

    private void initializeCarousel() {
        com.navdy.hud.app.ui.component.carousel.Carousel.InitParams initParams = new com.navdy.hud.app.ui.component.carousel.Carousel.InitParams();
        initParams.model = this.list;
        initParams.rootContainer = this.rootContainer;
        initParams.viewProcessor = new com.navdy.hud.app.view.WelcomeView.Anon3();
        initParams.infoLayoutResourceId = com.navdy.hud.app.R.layout.carousel_view_menu;
        initParams.imageLytResourceId = com.navdy.hud.app.R.layout.crossfade_image_lyt;
        initParams.carouselIndicator = null;
        initParams.animator = new com.navdy.hud.app.ui.component.carousel.ShrinkAnimator();
        this.carousel.init(initParams);
        this.carousel.setListener(new com.navdy.hud.app.view.WelcomeView.Anon4());
    }

    /* access modifiers changed from: private */
    public void updateInfo(com.navdy.hud.app.ui.component.carousel.Carousel.Model model) {
        if (model != null && model.infoMap != null) {
            java.lang.String title = (java.lang.String) model.infoMap.get(java.lang.Integer.valueOf(com.navdy.hud.app.R.id.title));
            java.lang.String subTitle = (java.lang.String) model.infoMap.get(java.lang.Integer.valueOf(com.navdy.hud.app.R.id.subTitle));
            java.lang.String message = (java.lang.String) model.infoMap.get(java.lang.Integer.valueOf(com.navdy.hud.app.R.id.message));
            android.widget.TextView textView = this.titleView;
            if (title == null) {
                title = "";
            }
            textView.setText(title);
            android.widget.TextView textView2 = this.subTitleView;
            if (subTitle == null) {
                subTitle = "";
            }
            textView2.setText(subTitle);
            android.widget.TextView textView3 = this.messageView;
            if (message == null) {
                message = "";
            }
            textView3.setText(message);
        }
    }

    /* access modifiers changed from: private */
    public void setImage(java.io.File path, android.widget.ImageView imageView, com.squareup.picasso.Callback callback) {
        com.navdy.hud.app.util.picasso.PicassoUtil.getInstance().load(path).noPlaceholder().fit().transform(this.roundTransformation).noFade().into(imageView, callback);
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.presenter != null) {
            this.presenter.takeView(this);
        }
        if (this.searching) {
            this.animatorView.start();
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.searching = false;
        this.animatorView.stop();
        stopDownloadAppAnimation();
        if (this.presenter != null) {
            this.presenter.dropView(this);
        }
    }

    public void setSearching(boolean searching2) {
        if (searching2 != this.searching) {
            this.searching = searching2;
            if (!searching2) {
                this.animatorView.stop();
                this.animatorView.setVisibility(8);
                return;
            }
            this.animatorView.setVisibility(0);
            this.animatorView.start();
        }
    }

    public boolean onGesture(com.navdy.service.library.events.input.GestureEvent event) {
        return false;
    }

    public boolean onKey(com.navdy.hud.app.manager.InputManager.CustomKeyEvent event) {
        if (this.state != 1) {
            return this.carousel.onKey(event);
        }
        switch (event) {
            case LEFT:
                this.choiceLayout.moveSelectionLeft();
                return true;
            case RIGHT:
                this.choiceLayout.moveSelectionRight();
                return true;
            case SELECT:
                this.choiceLayout.executeSelectedItem(true);
                return true;
            default:
                return false;
        }
    }

    public com.navdy.hud.app.manager.InputManager.IInputHandler nextHandler() {
        return com.navdy.hud.app.manager.InputManager.nextContainingHandler(this);
    }

    private void startDownloadAppAnimation() {
        sLogger.v("startDownloadAppAnimation");
        android.content.res.Resources resources = getContext().getResources();
        if (downloadText1 == null) {
            downloadText1 = resources.getStringArray(com.navdy.hud.app.R.array.download_app_1);
            downloadText2 = resources.getStringArray(com.navdy.hud.app.R.array.download_app_2);
            downloadColor1 = resources.getColor(com.navdy.hud.app.R.color.download_app_1);
            downloadColor2 = resources.getColor(com.navdy.hud.app.R.color.download_app_2);
            android.content.res.TypedArray typedArray = getResources().obtainTypedArray(com.navdy.hud.app.R.array.download_app_icon_app_store);
            appStoreIcons = new int[downloadText1.length];
            for (int i = 0; i < appStoreIcons.length; i++) {
                appStoreIcons[i] = typedArray.getResourceId(i, 0);
            }
            typedArray.recycle();
            android.content.res.TypedArray typedArray2 = getResources().obtainTypedArray(com.navdy.hud.app.R.array.download_app_icon_play_store);
            playStoreIcons = new int[downloadText1.length];
            for (int i2 = 0; i2 < playStoreIcons.length; i2++) {
                playStoreIcons[i2] = typedArray2.getResourceId(i2, 0);
            }
            typedArray2.recycle();
        }
        this.currentDownloadIndex = 0;
        this.currentDownloadView = 0;
        int index1 = this.currentDownloadIndex;
        int index2 = this.currentDownloadIndex + 1;
        setDownloadText(this.downloadAppTextView1, index1);
        setDownloadText(this.downloadAppTextView2, index2);
        setDownloadImage(this.appStoreImage1, appStoreIcons[index1]);
        setDownloadImage(this.appStoreImage2, appStoreIcons[index2]);
        setDownloadImage(this.playStoreImage1, playStoreIcons[index1]);
        setDownloadImage(this.playStoreImage2, playStoreIcons[index2]);
        this.downloadAppTextView1.setAlpha(1.0f);
        this.downloadAppTextView2.setAlpha(0.0f);
        this.appStoreImage1.setAlpha(1.0f);
        this.appStoreImage2.setAlpha(0.0f);
        this.playStoreImage1.setAlpha(1.0f);
        this.playStoreImage2.setAlpha(0.0f);
        this.downloadAppContainer.setVisibility(0);
        this.currentDownloadIndex++;
        this.handler.removeCallbacks(this.animationRunnable);
        this.handler.postDelayed(this.animationRunnable, (long) DOWNLOAD_APP_STAY_INTERVAL);
    }

    /* access modifiers changed from: private */
    public void setDownloadText(android.widget.TextView textView, int index) {
        android.text.SpannableStringBuilder builder = new android.text.SpannableStringBuilder();
        builder.append(downloadText1[index]);
        builder.setSpan(new android.text.style.ForegroundColorSpan(downloadColor1), 0, builder.length(), 33);
        builder.append(" ");
        int start = builder.length();
        builder.append(downloadText2[index]);
        builder.setSpan(new android.text.style.ForegroundColorSpan(downloadColor2), start, builder.length(), 33);
        builder.setSpan(new android.text.style.StyleSpan(1), start, builder.length(), 33);
        textView.setText(builder);
    }

    /* access modifiers changed from: private */
    public void setDownloadImage(android.widget.ImageView image, int resId) {
        image.setImageResource(resId);
    }

    private void stopDownloadAppAnimation() {
        sLogger.v("stopDownloadAppAnimation");
        this.handler.removeCallbacks(this.animationRunnable);
        this.downloadAppTextView1.animate().cancel();
        this.downloadAppTextView2.animate().cancel();
        this.appStoreImage1.animate().cancel();
        this.appStoreImage2.animate().cancel();
        this.playStoreImage1.animate().cancel();
        this.playStoreImage2.animate().cancel();
        this.downloadAppContainer.setVisibility(8);
        this.currentDownloadIndex = -1;
    }
}
