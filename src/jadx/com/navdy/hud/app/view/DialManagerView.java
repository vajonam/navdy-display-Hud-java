package com.navdy.hud.app.view;

public class DialManagerView extends android.widget.RelativeLayout implements com.navdy.hud.app.manager.InputManager.IInputHandler {
    private static final int CONNECTED_TIMEOUT = 30000;
    public static final int DIAL_CONNECTED = 5;
    public static final int DIAL_CONNECTING = 4;
    public static final int DIAL_CONNECTION_FAILED = 6;
    public static final int DIAL_PAIRED = 3;
    public static final int DIAL_PAIRING = 1;
    public static final int DIAL_SEARCHING = 2;
    private static final java.lang.String EMPTY = "";
    private static final long POST_OTA_TIMEOUT = java.util.concurrent.TimeUnit.SECONDS.toMillis(15);
    private static final int POS_DISMISS = 0;
    private static final int RESTART_INTERVAL = 2000;
    private static android.net.Uri firstLaunchVideoUri;
    private static android.net.Uri repairVideoUri;
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.view.DialManagerView.class);
    private int batteryLevel;
    /* access modifiers changed from: private */
    public int completeCount;
    private com.navdy.hud.app.ui.component.ChoiceLayout.IListener connectedChoiceListener;
    private java.lang.Runnable connectedTimeoutRunnable;
    @butterknife.InjectView(2131624317)
    public com.navdy.hud.app.ui.component.ConfirmationLayout connectedView;
    private java.lang.String dialName;
    private boolean firstTime;
    private boolean foundDial;
    /* access modifiers changed from: private */
    public android.os.Handler handler;
    private int incrementalVersion;
    private int initialState;
    @javax.inject.Inject
    com.navdy.hud.app.screen.DialManagerScreen.Presenter presenter;
    @butterknife.InjectView(2131624316)
    public android.widget.TextView rePairTextView;
    private boolean registered;
    private java.lang.Runnable restartScanRunnable;
    @javax.inject.Inject
    android.content.SharedPreferences sharedPreferences;
    /* access modifiers changed from: private */
    public android.animation.ObjectAnimator spinner;
    private int state;
    @butterknife.InjectView(2131624314)
    public android.view.ViewGroup videoContainer;
    private android.net.Uri videoUri;
    @butterknife.InjectView(2131624315)
    public android.widget.VideoView videoView;
    private boolean videoViewInitialized;

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            com.navdy.hud.app.view.DialManagerView.sLogger.v("connected timeout");
            com.navdy.hud.app.view.DialManagerView.this.presenter.dismiss();
        }
    }

    class Anon2 implements com.navdy.hud.app.ui.component.ChoiceLayout.IListener {
        Anon2() {
        }

        public void executeItem(int pos, int id) {
            if (pos == 0) {
                com.navdy.hud.app.view.DialManagerView.this.presenter.dismiss();
            }
        }

        public void itemSelected(int pos, int id) {
        }
    }

    class Anon3 implements java.lang.Runnable {
        Anon3() {
        }

        public void run() {
            com.navdy.hud.app.view.DialManagerView.this.handler.removeCallbacks(this);
            com.navdy.hud.app.view.DialManagerView.this.showDialPairing();
        }
    }

    class Anon4 implements com.navdy.hud.app.device.dial.DialManagerHelper.IDialForgotten {

        class Anon1 implements java.lang.Runnable {
            Anon1() {
            }

            public void run() {
                com.navdy.hud.app.view.DialManagerView.this.presenter.updateView(null);
            }
        }

        Anon4() {
        }

        public void onForgotten() {
            com.navdy.hud.app.view.DialManagerView.sLogger.v("all forgotten");
            com.navdy.hud.app.view.DialManagerView.this.presenter.getHandler().post(new com.navdy.hud.app.view.DialManagerView.Anon4.Anon1());
        }
    }

    class Anon5 implements android.media.MediaPlayer.OnPreparedListener {
        Anon5() {
        }

        public void onPrepared(android.media.MediaPlayer mp) {
            mp.setLooping(true);
            mp.start();
            com.navdy.hud.app.view.DialManagerView.sLogger.v("Duration= " + mp.getDuration());
        }
    }

    class Anon6 implements android.media.MediaPlayer.OnCompletionListener {
        Anon6() {
        }

        public void onCompletion(android.media.MediaPlayer mp) {
            com.navdy.hud.app.view.DialManagerView.access$Anon204(com.navdy.hud.app.view.DialManagerView.this);
            com.navdy.hud.app.view.DialManagerView.sLogger.v("completed:" + com.navdy.hud.app.view.DialManagerView.this.completeCount);
        }
    }

    class Anon7 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
        Anon7() {
        }

        public void onAnimationEnd(android.animation.Animator animation) {
            if (com.navdy.hud.app.view.DialManagerView.this.spinner != null) {
                com.navdy.hud.app.view.DialManagerView.this.spinner.setStartDelay(33);
                com.navdy.hud.app.view.DialManagerView.this.spinner.start();
            }
        }
    }

    class Anon8 implements java.lang.Runnable {
        Anon8() {
        }

        public void run() {
            if (com.navdy.hud.app.view.DialManagerView.this.presenter == null || !com.navdy.hud.app.view.DialManagerView.this.presenter.isActive()) {
                com.navdy.hud.app.view.DialManagerView.sLogger.v("showPairingSuccessUI:hide not active");
            } else {
                com.navdy.hud.app.view.DialManagerView.this.presenter.dismiss();
            }
        }
    }

    static /* synthetic */ int access$Anon204(com.navdy.hud.app.view.DialManagerView x0) {
        int i = x0.completeCount + 1;
        x0.completeCount = i;
        return i;
    }

    public DialManagerView(android.content.Context context) {
        this(context, null);
    }

    public DialManagerView(android.content.Context context, android.util.AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public DialManagerView(android.content.Context context, android.util.AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.initialState = 0;
        this.state = 0;
        this.dialName = "Navdy Dial";
        this.handler = new android.os.Handler(android.os.Looper.getMainLooper());
        this.connectedTimeoutRunnable = new com.navdy.hud.app.view.DialManagerView.Anon1();
        this.connectedChoiceListener = new com.navdy.hud.app.view.DialManagerView.Anon2();
        this.restartScanRunnable = new com.navdy.hud.app.view.DialManagerView.Anon3();
        if (!isInEditMode()) {
            mortar.Mortar.inject(context, this);
            if (firstLaunchVideoUri == null) {
                java.lang.String packageName = com.navdy.hud.app.HudApplication.getAppContext().getPackageName();
                firstLaunchVideoUri = android.net.Uri.parse("android.resource://" + packageName + "/raw/dial_pairing_flow");
                repairVideoUri = android.net.Uri.parse("android.resource://" + packageName + "/raw/dial_repairing_flow");
            }
        } else {
            com.navdy.hud.app.HudApplication.setContext(context);
        }
        initFromAttributes(context, attrs);
    }

    private void initFromAttributes(android.content.Context context, android.util.AttributeSet attrs) {
        android.content.res.TypedArray a = context.getTheme().obtainStyledAttributes(attrs, com.navdy.hud.app.R.styleable.DialManagerScreen, 0, 0);
        try {
            this.initialState = a.getInt(0, 1);
            this.batteryLevel = a.getInt(1, -1);
        } finally {
            a.recycle();
        }
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
        butterknife.ButterKnife.inject((android.view.View) this);
        ((com.navdy.hud.app.view.MaxWidthLinearLayout) findViewById(com.navdy.hud.app.R.id.infoContainer)).setMaxWidth(getContext().getResources().getDimensionPixelOffset(com.navdy.hud.app.R.dimen.dial_message_max_width));
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        sLogger.v("onAttachToWindow");
        super.onAttachedToWindow();
        if (this.presenter != null) {
            com.navdy.hud.app.framework.toast.ToastManager.getInstance().disableToasts(true);
            com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().enableNotifications(false);
            this.presenter.takeView(this);
            this.presenter.getBus().register(this);
            this.registered = true;
            return;
        }
        setState(this.initialState);
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        sLogger.v("onDetachToWindow");
        clearConnectedTimeout();
        if (this.videoContainer.getVisibility() == 0) {
            sLogger.v("stop dial scan");
            com.navdy.hud.app.device.dial.DialManager.getInstance().stopScan(false);
            stopVideo();
        }
        com.navdy.hud.app.framework.toast.ToastManager.getInstance().disableToasts(false);
        com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().enableNotifications(true);
        super.onDetachedFromWindow();
        if (this.presenter != null) {
            this.presenter.dropView(this);
            if (this.registered) {
                this.presenter.getBus().unregister(this);
            }
        }
        stopSpinner();
        if (!isInfoViewVisible()) {
            com.navdy.hud.app.device.dial.DialManagerHelper.sendLocalyticsEvent(this.presenter.getHandler(), this.firstTime, this.foundDial, 10000, true);
        }
    }

    public boolean onGesture(com.navdy.service.library.events.input.GestureEvent event) {
        return false;
    }

    public boolean onKey(com.navdy.hud.app.manager.InputManager.CustomKeyEvent event) {
        switch (event) {
            case POWER_BUTTON_CLICK:
            case POWER_BUTTON_DOUBLE_CLICK:
                clearConnectedTimeout();
                this.presenter.dismiss();
                break;
            case LEFT:
                if (isInfoViewVisible()) {
                    resetConnectedTimeout();
                    this.connectedView.choiceLayout.moveSelectionLeft();
                    break;
                }
                break;
            case RIGHT:
                if (isInfoViewVisible()) {
                    resetConnectedTimeout();
                    this.connectedView.choiceLayout.moveSelectionRight();
                    break;
                }
                break;
            case SELECT:
                if (isInfoViewVisible()) {
                    clearConnectedTimeout();
                    this.connectedView.choiceLayout.executeSelectedItem(true);
                    break;
                }
                break;
            case POWER_BUTTON_LONG_PRESS:
                clearConnectedTimeout();
                sLogger.v("pwrbtn long press in connected mode");
                com.navdy.hud.app.device.dial.DialManager.getInstance().forgetAllDials(true, new com.navdy.hud.app.view.DialManagerView.Anon4());
                break;
        }
        return true;
    }

    public com.navdy.hud.app.manager.InputManager.IInputHandler nextHandler() {
        return null;
    }

    private void setState(int state2) {
        if (this.state != state2) {
            sLogger.i("Switching to state:" + state2);
        }
        this.state = state2;
        switch (this.state) {
            case 1:
                showDialPairingState();
                return;
            case 2:
                showDialSearchingState();
                return;
            case 3:
                showPairedState();
                return;
            case 4:
                setConnectingUI();
                return;
            case 5:
                showDialConnectedState();
                return;
            case 6:
                showConnectionFailedState();
                return;
            default:
                return;
        }
    }

    public void showDialPairing() {
        setState(1);
    }

    public void showDialConnected() {
        this.dialName = com.navdy.hud.app.device.dial.DialNotification.getDialName();
        this.batteryLevel = com.navdy.hud.app.device.dial.DialManager.getInstance().getLastKnownBatteryLevel();
        this.incrementalVersion = com.navdy.hud.app.device.dial.DialManager.getInstance().getDialFirmwareUpdater().getVersions().dial.incrementalVersion;
        setState(5);
    }

    public void showDialSearching(java.lang.String dialName2) {
        this.dialName = dialName2;
        setState(2);
    }

    private void showDialPairingState() {
        this.firstTime = com.navdy.hud.app.device.dial.DialManager.getInstance().getBondedDialCount() == 0;
        this.connectedView.setVisibility(8);
        resetConnectedView();
        if (!this.videoViewInitialized) {
            this.videoView.setOnPreparedListener(new com.navdy.hud.app.view.DialManagerView.Anon5());
            this.videoView.setOnCompletionListener(new com.navdy.hud.app.view.DialManagerView.Anon6());
            this.videoViewInitialized = true;
        }
        this.videoContainer.setVisibility(0);
        if (this.sharedPreferences == null || !this.sharedPreferences.getBoolean(com.navdy.hud.app.device.dial.DialConstants.VIDEO_SHOWN_PREF, false)) {
            setVideoURI(firstLaunchVideoUri);
            this.rePairTextView.setVisibility(8);
        } else {
            setVideoURI(repairVideoUri);
            this.rePairTextView.setText(getContext().getString(com.navdy.hud.app.R.string.navdy_dial_repair_text));
            this.rePairTextView.setVisibility(0);
        }
        sLogger.v("starting scanning");
        com.navdy.hud.app.device.dial.DialManager.getInstance().startScan();
    }

    private void setVideoURI(android.net.Uri uri) {
        if (uri != null) {
            try {
                if (!uri.equals(this.videoUri)) {
                    this.videoView.setVideoURI(uri);
                    this.videoUri = uri;
                }
            } catch (Throwable t) {
                sLogger.e(t);
            }
        }
        if (uri == null) {
            this.videoView.stopPlayback();
        }
        this.videoUri = uri;
    }

    private void resetConnectedView() {
        this.connectedView.title1.setVisibility(8);
        this.connectedView.title1.setTextAppearance(getContext(), com.navdy.hud.app.R.style.title1);
        this.connectedView.title2.setTextAppearance(getContext(), com.navdy.hud.app.R.style.title2);
        this.connectedView.title3.setVisibility(8);
        this.connectedView.title4.setVisibility(8);
        this.connectedView.sideImage.setVisibility(8);
        ((android.view.ViewGroup.MarginLayoutParams) this.connectedView.choiceLayout.getLayoutParams()).bottomMargin = (int) getResources().getDimension(com.navdy.hud.app.R.dimen.dial_pairing_status_margin_bottom);
        this.connectedView.choiceLayout.setChoices(com.navdy.hud.app.ui.component.ChoiceLayout.Mode.LABEL, java.util.Collections.EMPTY_LIST, 0, this.connectedChoiceListener);
    }

    private void showDialConnectedState() {
        java.lang.String fw;
        stopVideo();
        android.content.Context context = getContext();
        android.content.res.Resources resources = context.getResources();
        this.connectedView.screenImage.setImage(com.navdy.hud.app.R.drawable.icon_dial_2, null, com.navdy.hud.app.ui.component.image.InitialsImageView.Style.DEFAULT);
        this.connectedView.sideImage.setImageResource(com.navdy.hud.app.R.drawable.icon_sm_success);
        this.connectedView.sideImage.setVisibility(0);
        this.connectedView.titleContainer.setVisibility(0);
        this.connectedView.title1.setTextAppearance(context, com.navdy.hud.app.R.style.title2);
        this.connectedView.title1.setText(resources.getString(com.navdy.hud.app.R.string.dial_paired_text));
        this.connectedView.title2.setTextAppearance(context, com.navdy.hud.app.R.style.title1);
        if (this.dialName != null) {
            this.connectedView.title2.setText(this.dialName);
        } else {
            this.connectedView.title2.setVisibility(8);
        }
        switch (com.navdy.hud.app.device.dial.DialManager.getBatteryLevelCategory(this.batteryLevel)) {
            case LOW_BATTERY:
                android.text.SpannableStringBuilder ss1 = new android.text.SpannableStringBuilder("  " + resources.getString(com.navdy.hud.app.R.string.dial_battery_level_low, new java.lang.Object[]{com.navdy.hud.app.device.dial.DialNotification.getDialBatteryLevel()}));
                ss1.setSpan(new android.text.style.ImageSpan(context, com.navdy.hud.app.R.drawable.icon_dial_batterylow_2_copy, 1), 0, 1, 33);
                this.connectedView.title3.setText(ss1);
                break;
            case VERY_LOW_BATTERY:
                android.text.SpannableStringBuilder ss2 = new android.text.SpannableStringBuilder("  " + resources.getString(com.navdy.hud.app.R.string.dial_battery_level_very_low, new java.lang.Object[]{com.navdy.hud.app.device.dial.DialNotification.getDialBatteryLevel()}));
                ss2.setSpan(new android.text.style.ImageSpan(context, com.navdy.hud.app.R.drawable.icon_dial_batterylow_copy, 1), 0, 1, 33);
                this.connectedView.title3.setText(ss2);
                break;
            case EXTREMELY_LOW_BATTERY:
                android.text.SpannableStringBuilder ss3 = new android.text.SpannableStringBuilder("  " + resources.getString(com.navdy.hud.app.R.string.dial_battery_level_extremely_low, new java.lang.Object[]{com.navdy.hud.app.device.dial.DialNotification.getDialBatteryLevel()}));
                ss3.setSpan(new android.text.style.ImageSpan(context, com.navdy.hud.app.R.drawable.icon_dial_batterylow_copy, 1), 0, 1, 33);
                this.connectedView.title3.setText(ss3);
                break;
            default:
                this.connectedView.title3.setText(resources.getString(com.navdy.hud.app.R.string.dial_battery_level_ok));
                break;
        }
        int incremental = this.incrementalVersion;
        if (incremental == -1) {
            fw = "";
        } else {
            fw = "1.0." + incremental;
        }
        this.connectedView.title4.setText(resources.getString(com.navdy.hud.app.R.string.dial_firmware_rev, new java.lang.Object[]{fw}));
        this.connectedView.title4.setVisibility(0);
        ((android.view.ViewGroup.MarginLayoutParams) this.connectedView.choiceLayout.getLayoutParams()).bottomMargin = (int) resources.getDimension(com.navdy.hud.app.R.dimen.dial_connected_status_margin_bottom);
        java.util.List<com.navdy.hud.app.ui.component.ChoiceLayout.Choice> choiceList = new java.util.ArrayList<>(2);
        choiceList.add(new com.navdy.hud.app.ui.component.ChoiceLayout.Choice(resources.getString(com.navdy.hud.app.R.string.dial_paired_choice_dismiss), 0));
        this.connectedView.choiceLayout.setChoices(com.navdy.hud.app.ui.component.ChoiceLayout.Mode.LABEL, choiceList, 0, this.connectedChoiceListener);
        this.connectedView.setVisibility(0);
        if (this.presenter != null) {
            android.os.Handler handler2 = this.presenter.getHandler();
            handler2.removeCallbacks(this.connectedTimeoutRunnable);
            handler2.postDelayed(this.connectedTimeoutRunnable, 30000);
        }
    }

    private void showDialSearchingState() {
        this.firstTime = false;
        resetConnectedView();
        setConnectingUI();
        sLogger.v("starting scanning");
        com.navdy.hud.app.device.dial.DialManager.getInstance().startScan();
        this.handler.postDelayed(this.restartScanRunnable, POST_OTA_TIMEOUT);
    }

    private void stopVideo() {
        if (this.videoView.isPlaying()) {
            setVideoURI(null);
            sLogger.v("stopped video");
        } else {
            sLogger.v("video not playing");
        }
        this.videoContainer.setVisibility(8);
    }

    @com.squareup.otto.Subscribe
    public void onShowToast(com.navdy.hud.app.framework.toast.ToastManager.ShowToast event) {
        sLogger.v("got toast:" + event.name);
        if (android.text.TextUtils.equals(event.name, com.navdy.hud.app.device.dial.DialNotification.DIAL_CONNECT_ID) && this.presenter != null) {
            this.presenter.dismiss();
        }
    }

    @com.squareup.otto.Subscribe
    public void onDialConnectionStatus(com.navdy.hud.app.device.dial.DialConstants.DialConnectionStatus event) {
        if (!isInfoViewVisible()) {
            sLogger.v("onDialConnectionStatus:" + event.status + " name:" + event.dialName);
            if (this.foundDial) {
                sLogger.v("already found dial");
                return;
            }
            com.navdy.hud.app.device.dial.DialManager dialManager = com.navdy.hud.app.device.dial.DialManager.getInstance();
            this.handler.removeCallbacks(this.restartScanRunnable);
            stopSpinner();
            switch (event.status) {
                case CONNECTED:
                    sLogger.v("dial connected:" + event.dialName);
                    this.foundDial = true;
                    dialManager.stopScan(false);
                    com.navdy.hud.app.framework.toast.ToastManager toastManager = com.navdy.hud.app.framework.toast.ToastManager.getInstance();
                    toastManager.dismissCurrentToast();
                    toastManager.clearAllPendingToast();
                    toastManager.disableToasts(false);
                    sLogger.v("show connected state");
                    showPairingSuccessUI(event.dialName);
                    return;
                case CONNECTING:
                    sLogger.v("dial connnecting:" + event.dialName);
                    stopVideo();
                    setConnectingUI();
                    return;
                case DISCONNECTED:
                    sLogger.v("dial connection disconnected:" + event.dialName);
                    this.handler.post(this.restartScanRunnable);
                    return;
                case CONNECTION_FAILED:
                    sLogger.v("dial connection failed:" + event.dialName);
                    setState(6);
                    this.handler.postDelayed(this.restartScanRunnable, 2000);
                    return;
                case NO_DIAL_FOUND:
                    sLogger.v("no dials found");
                    showDialPairing();
                    return;
                default:
                    return;
            }
        }
    }

    private void resetConnectedTimeout() {
        android.os.Handler handler2 = this.presenter.getHandler();
        handler2.removeCallbacks(this.connectedTimeoutRunnable);
        handler2.postDelayed(this.connectedTimeoutRunnable, 30000);
    }

    private void clearConnectedTimeout() {
        this.presenter.getHandler().removeCallbacks(this.connectedTimeoutRunnable);
    }

    private boolean isInfoViewVisible() {
        return this.connectedView.getVisibility() == 0 && this.connectedView.title4.getVisibility() == 0;
    }

    private void startSpinner() {
        if (this.spinner == null) {
            this.spinner = android.animation.ObjectAnimator.ofFloat(this.connectedView.sideImage, android.view.View.ROTATION, new float[]{360.0f});
            this.spinner.setDuration(500);
            this.spinner.setInterpolator(new android.view.animation.AccelerateDecelerateInterpolator());
            this.spinner.addListener(new com.navdy.hud.app.view.DialManagerView.Anon7());
        }
        if (!this.spinner.isRunning()) {
            this.spinner.start();
        }
    }

    private void stopSpinner() {
        if (this.spinner != null) {
            this.spinner.removeAllListeners();
            this.spinner.cancel();
            this.connectedView.sideImage.setRotation(0.0f);
            this.spinner = null;
        }
    }

    private void setConnectingUI() {
        stopVideo();
        this.connectedView.screenImage.setImage(com.navdy.hud.app.R.drawable.icon_dial_2, null, com.navdy.hud.app.ui.component.image.InitialsImageView.Style.DEFAULT);
        this.connectedView.screenImage.setVisibility(0);
        this.connectedView.mainSection.setLayoutTransition(new android.animation.LayoutTransition());
        this.connectedView.mainSection.removeView(this.connectedView.titleContainer);
        this.connectedView.sideImage.setImageResource(com.navdy.hud.app.R.drawable.icon_sm_spinner);
        this.connectedView.sideImage.setVisibility(0);
        startSpinner();
        this.connectedView.setVisibility(0);
    }

    private void showConnectionFailedState() {
        stopVideo();
        this.connectedView.screenImage.setImage(com.navdy.hud.app.R.drawable.icon_dial_2, null, com.navdy.hud.app.ui.component.image.InitialsImageView.Style.DEFAULT);
        this.connectedView.screenImage.setVisibility(0);
        this.connectedView.sideImage.setImageResource(com.navdy.hud.app.R.drawable.icon_connection_failed);
        this.connectedView.sideImage.setVisibility(0);
        this.connectedView.setVisibility(0);
    }

    private void showPairingSuccessUI(java.lang.String dialName2) {
        this.dialName = dialName2;
        setState(3);
        com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getBus().post(new com.navdy.hud.app.event.LocalSpeechRequest(new com.navdy.service.library.events.audio.SpeechRequest.Builder().words(com.navdy.hud.app.framework.voice.TTSUtils.TTS_DIAL_CONNECTED).language(java.util.Locale.getDefault().toLanguageTag()).category(com.navdy.service.library.events.audio.Category.SPEECH_NOTIFICATION).build()));
        this.handler.postDelayed(new com.navdy.hud.app.view.DialManagerView.Anon8(), 5000);
    }

    private void showPairedState() {
        sLogger.v("showPairingSuccessUI=" + this.dialName);
        android.content.Context context = getContext();
        stopVideo();
        android.content.res.Resources resources = context.getResources();
        this.connectedView.mainSection.removeView(this.connectedView.titleContainer);
        this.connectedView.mainSection.setLayoutTransition(new android.animation.LayoutTransition());
        this.connectedView.screenImage.setImage(com.navdy.hud.app.R.drawable.icon_dial_2, null, com.navdy.hud.app.ui.component.image.InitialsImageView.Style.DEFAULT);
        this.connectedView.screenImage.setVisibility(0);
        this.connectedView.sideImage.setImageResource(com.navdy.hud.app.R.drawable.icon_sm_success);
        this.connectedView.sideImage.setVisibility(0);
        this.connectedView.title1.setVisibility(8);
        this.connectedView.title2.setTextAppearance(context, com.navdy.hud.app.R.style.Glances_1_bold);
        this.connectedView.title2.setText(resources.getString(com.navdy.hud.app.R.string.navdy_dial));
        this.dialName = com.navdy.hud.app.device.dial.DialNotification.getDialAddressPart(this.dialName);
        if (!android.text.TextUtils.isEmpty(this.dialName)) {
            this.connectedView.title3.setText(resources.getString(com.navdy.hud.app.R.string.navdy_dial_name, new java.lang.Object[]{this.dialName}));
            this.connectedView.title3.setVisibility(0);
        } else {
            this.connectedView.title3.setVisibility(8);
        }
        this.connectedView.titleContainer.setVisibility(0);
        this.connectedView.mainSection.addView(this.connectedView.titleContainer);
        this.connectedView.setVisibility(0);
    }
}
