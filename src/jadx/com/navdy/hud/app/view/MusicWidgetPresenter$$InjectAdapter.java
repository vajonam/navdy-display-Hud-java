package com.navdy.hud.app.view;

public final class MusicWidgetPresenter$$InjectAdapter extends dagger.internal.Binding<com.navdy.hud.app.view.MusicWidgetPresenter> implements dagger.MembersInjector<com.navdy.hud.app.view.MusicWidgetPresenter> {
    private dagger.internal.Binding<com.navdy.hud.app.manager.MusicManager> musicManager;
    private dagger.internal.Binding<com.navdy.hud.app.view.DashboardWidgetPresenter> supertype;

    public MusicWidgetPresenter$$InjectAdapter() {
        super(null, "members/com.navdy.hud.app.view.MusicWidgetPresenter", false, com.navdy.hud.app.view.MusicWidgetPresenter.class);
    }

    public void attach(dagger.internal.Linker linker) {
        this.musicManager = linker.requestBinding("com.navdy.hud.app.manager.MusicManager", com.navdy.hud.app.view.MusicWidgetPresenter.class, getClass().getClassLoader());
        dagger.internal.Linker linker2 = linker;
        this.supertype = linker2.requestBinding("members/com.navdy.hud.app.view.DashboardWidgetPresenter", com.navdy.hud.app.view.MusicWidgetPresenter.class, getClass().getClassLoader(), false, true);
    }

    public void getDependencies(java.util.Set<dagger.internal.Binding<?>> set, java.util.Set<dagger.internal.Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.musicManager);
        injectMembersBindings.add(this.supertype);
    }

    public void injectMembers(com.navdy.hud.app.view.MusicWidgetPresenter object) {
        object.musicManager = (com.navdy.hud.app.manager.MusicManager) this.musicManager.get();
        this.supertype.injectMembers(object);
    }
}
