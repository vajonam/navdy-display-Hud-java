package com.navdy.hud.app.view;

public class GestureLearningView extends android.widget.RelativeLayout implements com.navdy.hud.app.ui.component.ChoiceLayout.IListener, com.navdy.hud.app.manager.InputManager.IInputHandler {
    public static final int MOVE_IN_DURATION = 600;
    public static final int MOVE_OUT_DURATION = 250;
    private static final int PROGRESS_INDICATOR_HIDE_INTERVAL = 100;
    private static final int SCROLL_DURATION = 1000;
    private static final int SCROLL_INTERVAL = 4000;
    private static final int SENSOR_BLOCKED_MESSAGE_DELAY = 30000;
    private static final java.lang.String SMOOTHING_COEFF_KEY = "persist.sys.gesture_smooth";
    public static final int TAG_CAPTURE = 2;
    public static final int TAG_DONE = 0;
    public static final int TAG_TIPS = 1;
    /* access modifiers changed from: private */
    public java.lang.String[] TIPS;
    @javax.inject.Inject
    com.squareup.otto.Bus mBus;
    @butterknife.InjectView(2131624192)
    android.widget.ImageView mCenterImage;
    @butterknife.InjectView(2131624150)
    com.navdy.hud.app.ui.component.ChoiceLayout mChoiceLayout;
    /* access modifiers changed from: private */
    public int mCurrentSmallTipIndex;
    /* access modifiers changed from: private */
    public float mGestureIndicatorHalfWidth;
    /* access modifiers changed from: private */
    public float mGestureIndicatorProgressSpan;
    @butterknife.InjectView(2131624187)
    android.view.View mGestureProgressIndicator;
    /* access modifiers changed from: private */
    public float mHandIconMargin;
    /* access modifiers changed from: private */
    public android.os.Handler mHandler;
    private android.animation.AnimatorSet mLeftGestureAnimation;
    @butterknife.InjectView(2131624193)
    android.widget.LinearLayout mLeftSwipeLayout;
    private final float mLowPassCoeff;
    /* access modifiers changed from: private */
    public float mNeutralX;
    @javax.inject.Inject
    com.navdy.hud.app.screen.GestureLearningScreen.Presenter mPresenter;
    private java.lang.Runnable mProgressIndicatorRunnable;
    private android.animation.AnimatorSet mRightGestureAnimation;
    @butterknife.InjectView(2131624197)
    android.widget.LinearLayout mRightSwipeLayout;
    /* access modifiers changed from: private */
    public android.animation.ValueAnimator mScrollAnimator;
    private java.lang.Runnable mShowSensorBlockedMessageRunnable;
    private float mSmoothedProgress;
    /* access modifiers changed from: private */
    public boolean mStopAnimation;
    @butterknife.InjectView(2131624189)
    android.widget.LinearLayout mTipsScroller;
    int mTipsScrollerHeight;
    /* access modifiers changed from: private */
    public java.lang.Runnable mTipsScrollerRunnable;
    @butterknife.InjectView(2131624190)
    android.widget.TextView mTipsTextView1;
    @butterknife.InjectView(2131624191)
    android.widget.TextView mTipsTextView2;

    class Anon1 implements android.view.ViewTreeObserver.OnGlobalLayoutListener {
        Anon1() {
        }

        public void onGlobalLayout() {
            com.navdy.hud.app.view.GestureLearningView.this.mNeutralX = (float) (com.navdy.hud.app.view.GestureLearningView.this.getWidth() / 2);
            com.navdy.hud.app.view.GestureLearningView.this.mGestureProgressIndicator.setY((com.navdy.hud.app.view.GestureLearningView.this.mCenterImage.getY() + ((float) (com.navdy.hud.app.view.GestureLearningView.this.mCenterImage.getHeight() / 2))) - ((float) (com.navdy.hud.app.view.GestureLearningView.this.mGestureProgressIndicator.getHeight() / 2)));
            com.navdy.hud.app.view.GestureLearningView.this.mGestureIndicatorProgressSpan = (com.navdy.hud.app.view.GestureLearningView.this.mHandIconMargin + ((float) (com.navdy.hud.app.view.GestureLearningView.this.mCenterImage.getWidth() / 2))) - ((float) (com.navdy.hud.app.view.GestureLearningView.this.mGestureProgressIndicator.getWidth() / 2));
            com.navdy.hud.app.view.GestureLearningView.this.mGestureIndicatorHalfWidth = (float) (com.navdy.hud.app.view.GestureLearningView.this.mGestureProgressIndicator.getWidth() / 2);
            com.navdy.hud.app.view.GestureLearningView.this.getViewTreeObserver().removeOnGlobalLayoutListener(this);
        }
    }

    class Anon10 implements android.animation.Animator.AnimatorListener {
        final /* synthetic */ android.widget.TextView val$directionText;
        final /* synthetic */ android.widget.ImageView val$overLay;

        Anon10(android.widget.ImageView imageView, android.widget.TextView textView) {
            this.val$overLay = imageView;
            this.val$directionText = textView;
        }

        public void onAnimationStart(android.animation.Animator animation) {
            this.val$overLay.setAlpha(1.0f);
            this.val$directionText.setAlpha(1.0f);
        }

        public void onAnimationEnd(android.animation.Animator animation) {
            this.val$overLay.setAlpha(0.0f);
            this.val$directionText.setAlpha(0.0f);
        }

        public void onAnimationCancel(android.animation.Animator animation) {
        }

        public void onAnimationRepeat(android.animation.Animator animation) {
        }
    }

    class Anon2 implements android.animation.ValueAnimator.AnimatorUpdateListener {
        Anon2() {
        }

        public void onAnimationUpdate(android.animation.ValueAnimator animation) {
            android.view.ViewGroup.MarginLayoutParams marginLayoutParams = (android.view.ViewGroup.MarginLayoutParams) com.navdy.hud.app.view.GestureLearningView.this.mTipsScroller.getLayoutParams();
            marginLayoutParams.topMargin = ((java.lang.Integer) animation.getAnimatedValue()).intValue();
            com.navdy.hud.app.view.GestureLearningView.this.mTipsScroller.setLayoutParams(marginLayoutParams);
        }
    }

    class Anon3 implements android.animation.Animator.AnimatorListener {
        Anon3() {
        }

        public void onAnimationStart(android.animation.Animator animation) {
        }

        public void onAnimationEnd(android.animation.Animator animation) {
            com.navdy.hud.app.view.GestureLearningView.this.mCurrentSmallTipIndex = (com.navdy.hud.app.view.GestureLearningView.this.mCurrentSmallTipIndex + 1) % com.navdy.hud.app.view.GestureLearningView.this.TIPS.length;
            java.lang.String tipText1 = com.navdy.hud.app.view.GestureLearningView.this.TIPS[com.navdy.hud.app.view.GestureLearningView.this.mCurrentSmallTipIndex];
            java.lang.String tipText2 = com.navdy.hud.app.view.GestureLearningView.this.TIPS[(com.navdy.hud.app.view.GestureLearningView.this.mCurrentSmallTipIndex + 1) % com.navdy.hud.app.view.GestureLearningView.this.TIPS.length];
            com.navdy.hud.app.view.GestureLearningView.this.mTipsTextView1.setText(tipText1);
            android.view.ViewGroup.MarginLayoutParams marginLayoutParams = (android.view.ViewGroup.MarginLayoutParams) com.navdy.hud.app.view.GestureLearningView.this.mTipsScroller.getLayoutParams();
            marginLayoutParams.topMargin = 0;
            com.navdy.hud.app.view.GestureLearningView.this.mTipsScroller.setLayoutParams(marginLayoutParams);
            com.navdy.hud.app.view.GestureLearningView.this.mTipsTextView2.setText(tipText2);
            if (!com.navdy.hud.app.view.GestureLearningView.this.mStopAnimation) {
                com.navdy.hud.app.view.GestureLearningView.this.mHandler.postDelayed(com.navdy.hud.app.view.GestureLearningView.this.mTipsScrollerRunnable, 4000);
            }
        }

        public void onAnimationCancel(android.animation.Animator animation) {
        }

        public void onAnimationRepeat(android.animation.Animator animation) {
        }
    }

    class Anon4 implements java.lang.Runnable {
        Anon4() {
        }

        public void run() {
            com.navdy.hud.app.view.GestureLearningView.this.mScrollAnimator.start();
        }
    }

    class Anon5 implements java.lang.Runnable {
        Anon5() {
        }

        public void run() {
            com.navdy.hud.app.view.GestureLearningView.this.mGestureProgressIndicator.setVisibility(4);
        }
    }

    class Anon6 implements java.lang.Runnable {
        Anon6() {
        }

        public void run() {
            com.navdy.hud.app.view.GestureLearningView.this.mPresenter.showCameraSensorBlocked();
        }
    }

    class Anon7 implements android.animation.ValueAnimator.AnimatorUpdateListener {
        final /* synthetic */ boolean val$rightSide;
        final /* synthetic */ android.view.ViewGroup val$view;

        Anon7(android.view.ViewGroup viewGroup, boolean z) {
            this.val$view = viewGroup;
            this.val$rightSide = z;
        }

        public void onAnimationUpdate(android.animation.ValueAnimator animation) {
            android.view.ViewGroup.MarginLayoutParams marginLayoutParams = (android.view.ViewGroup.MarginLayoutParams) this.val$view.getLayoutParams();
            int animatedValue = ((java.lang.Integer) animation.getAnimatedValue()).intValue();
            if (this.val$rightSide) {
                marginLayoutParams.leftMargin = animatedValue;
            } else {
                marginLayoutParams.rightMargin = animatedValue;
            }
            this.val$view.setLayoutParams(marginLayoutParams);
        }
    }

    class Anon8 implements android.animation.ValueAnimator.AnimatorUpdateListener {
        final /* synthetic */ boolean val$rightSide;
        final /* synthetic */ android.view.ViewGroup val$view;

        Anon8(android.view.ViewGroup viewGroup, boolean z) {
            this.val$view = viewGroup;
            this.val$rightSide = z;
        }

        public void onAnimationUpdate(android.animation.ValueAnimator animation) {
            android.view.ViewGroup.MarginLayoutParams marginLayoutParams = (android.view.ViewGroup.MarginLayoutParams) this.val$view.getLayoutParams();
            int animatedValue = ((java.lang.Integer) animation.getAnimatedValue()).intValue();
            if (this.val$rightSide) {
                marginLayoutParams.leftMargin = animatedValue;
            } else {
                marginLayoutParams.rightMargin = animatedValue;
            }
            this.val$view.setLayoutParams(marginLayoutParams);
        }
    }

    class Anon9 implements android.animation.ValueAnimator.AnimatorUpdateListener {
        final /* synthetic */ android.widget.TextView val$directionText;
        final /* synthetic */ android.widget.ImageView val$overLay;

        Anon9(android.widget.TextView textView, android.widget.ImageView imageView) {
            this.val$directionText = textView;
            this.val$overLay = imageView;
        }

        public void onAnimationUpdate(android.animation.ValueAnimator animation) {
            this.val$directionText.setAlpha(((java.lang.Float) animation.getAnimatedValue()).floatValue());
            this.val$overLay.setAlpha(((java.lang.Float) animation.getAnimatedValue()).floatValue());
        }
    }

    public GestureLearningView(android.content.Context context) {
        this(context, null);
    }

    public GestureLearningView(android.content.Context context, android.util.AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public GestureLearningView(android.content.Context context, android.util.AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mCurrentSmallTipIndex = 0;
        this.mStopAnimation = false;
        this.mLowPassCoeff = java.lang.Float.parseFloat(com.navdy.hud.app.util.os.SystemProperties.get(SMOOTHING_COEFF_KEY, "1.0"));
        this.mSmoothedProgress = 0.0f;
        if (!isInEditMode()) {
            mortar.Mortar.inject(context, this);
        }
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
        butterknife.ButterKnife.inject((android.view.View) this);
        this.mChoiceLayout.setHighlightPersistent(true);
        android.content.res.Resources resources = getResources();
        this.mTipsScrollerHeight = resources.getDimensionPixelSize(com.navdy.hud.app.R.dimen.gesture_learning_tips_text_scroll_height);
        this.mHandIconMargin = resources.getDimension(com.navdy.hud.app.R.dimen.gesture_icon_margin_from_center_image);
        this.mLeftGestureAnimation = prepareGestureAnimation(false, this.mLeftSwipeLayout);
        this.mRightGestureAnimation = prepareGestureAnimation(true, this.mRightSwipeLayout);
        this.TIPS = resources.getStringArray(com.navdy.hud.app.R.array.gesture_small_tips);
        this.mHandler = new android.os.Handler();
        this.mScrollAnimator = new android.animation.ValueAnimator();
        this.mScrollAnimator.setIntValues(new int[]{0, -this.mTipsScrollerHeight});
        this.mScrollAnimator.setDuration(1000);
        getViewTreeObserver().addOnGlobalLayoutListener(new com.navdy.hud.app.view.GestureLearningView.Anon1());
        this.mScrollAnimator.addUpdateListener(new com.navdy.hud.app.view.GestureLearningView.Anon2());
        this.mScrollAnimator.addListener(new com.navdy.hud.app.view.GestureLearningView.Anon3());
        java.lang.String tipText1 = this.TIPS[this.mCurrentSmallTipIndex];
        java.lang.String tipText2 = this.TIPS[(this.mCurrentSmallTipIndex + 1) % this.TIPS.length];
        this.mTipsTextView1.setText(tipText1);
        this.mTipsTextView2.setText(tipText2);
        this.mTipsScrollerRunnable = new com.navdy.hud.app.view.GestureLearningView.Anon4();
        this.mProgressIndicatorRunnable = new com.navdy.hud.app.view.GestureLearningView.Anon5();
        this.mShowSensorBlockedMessageRunnable = new com.navdy.hud.app.view.GestureLearningView.Anon6();
        this.mHandler.postDelayed(this.mTipsScrollerRunnable, 4000);
        java.util.List<com.navdy.hud.app.ui.component.ChoiceLayout.Choice> list = new java.util.ArrayList<>();
        list.add(new com.navdy.hud.app.ui.component.ChoiceLayout.Choice(getContext().getString(com.navdy.hud.app.R.string.done), 0));
        list.add(new com.navdy.hud.app.ui.component.ChoiceLayout.Choice(getContext().getString(com.navdy.hud.app.R.string.tips), 1));
        if (com.navdy.hud.app.BuildConfig.DEBUG) {
            list.add(new com.navdy.hud.app.ui.component.ChoiceLayout.Choice(getContext().getString(com.navdy.hud.app.R.string.record), 2));
        }
        this.mChoiceLayout.setChoices(com.navdy.hud.app.ui.component.ChoiceLayout.Mode.LABEL, list, 0, this);
        if (!isInEditMode()) {
            this.mBus.register(this);
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.mBus.unregister(this);
        this.mStopAnimation = true;
        this.mScrollAnimator.cancel();
        this.mHandler.removeCallbacks(this.mTipsScrollerRunnable);
        this.mHandler.removeCallbacks(this.mShowSensorBlockedMessageRunnable);
    }

    public boolean onGesture(com.navdy.service.library.events.input.GestureEvent event) {
        this.mHandler.removeCallbacks(this.mShowSensorBlockedMessageRunnable);
        switch (event.gesture) {
            case GESTURE_SWIPE_LEFT:
                this.mLeftGestureAnimation.start();
                break;
            case GESTURE_SWIPE_RIGHT:
                this.mRightGestureAnimation.start();
                break;
        }
        return true;
    }

    public boolean onKey(com.navdy.hud.app.manager.InputManager.CustomKeyEvent event) {
        switch (event) {
            case LEFT:
                this.mChoiceLayout.moveSelectionLeft();
                break;
            case RIGHT:
                this.mChoiceLayout.moveSelectionRight();
                break;
            case SELECT:
                this.mChoiceLayout.executeSelectedItem(true);
                break;
        }
        return false;
    }

    public com.navdy.hud.app.manager.InputManager.IInputHandler nextHandler() {
        return null;
    }

    public void executeItem(int pos, int id) {
        switch (id) {
            case 0:
                this.mPresenter.finish();
                return;
            case 1:
                this.mPresenter.showTips();
                return;
            case 2:
                this.mPresenter.showCaptureView();
                return;
            default:
                return;
        }
    }

    public void itemSelected(int pos, int id) {
    }

    private android.animation.AnimatorSet prepareGestureAnimation(boolean rightSide, android.view.ViewGroup view) {
        android.widget.TextView directionText;
        android.widget.ImageView overLay;
        if (rightSide) {
            directionText = (android.widget.TextView) view.findViewById(com.navdy.hud.app.R.id.txt_right);
            overLay = (android.widget.ImageView) view.findViewById(com.navdy.hud.app.R.id.img_right_hand_overlay);
        } else {
            directionText = (android.widget.TextView) view.findViewById(com.navdy.hud.app.R.id.txt_left);
            overLay = (android.widget.ImageView) view.findViewById(com.navdy.hud.app.R.id.img_left_hand_overlay);
        }
        android.animation.AnimatorSet animatorSet = new android.animation.AnimatorSet();
        android.animation.ValueAnimator moveOutAnimator = new android.animation.ValueAnimator();
        moveOutAnimator.setDuration(250);
        int marginDistance = getResources().getDimensionPixelSize(com.navdy.hud.app.R.dimen.gesture_icon_animate_distance);
        moveOutAnimator.setIntValues(new int[]{(int) this.mHandIconMargin, ((int) this.mHandIconMargin) + marginDistance});
        moveOutAnimator.addUpdateListener(new com.navdy.hud.app.view.GestureLearningView.Anon7(view, rightSide));
        android.animation.ValueAnimator moveRightAnimator = new android.animation.ValueAnimator();
        moveRightAnimator.setDuration(600);
        moveRightAnimator.setIntValues(new int[]{((int) this.mHandIconMargin) + marginDistance, (int) this.mHandIconMargin});
        moveRightAnimator.addUpdateListener(new com.navdy.hud.app.view.GestureLearningView.Anon8(view, rightSide));
        android.animation.ValueAnimator alphaAnimator = new android.animation.ValueAnimator();
        alphaAnimator.setDuration(600);
        alphaAnimator.setFloatValues(new float[]{1.0f, 0.0f});
        alphaAnimator.addUpdateListener(new com.navdy.hud.app.view.GestureLearningView.Anon9(directionText, overLay));
        animatorSet.play(moveOutAnimator).before(alphaAnimator).before(moveRightAnimator);
        animatorSet.addListener(new com.navdy.hud.app.view.GestureLearningView.Anon10(overLay, directionText));
        return animatorSet;
    }

    public void setVisibility(int visibility) {
        super.setVisibility(visibility);
        if (visibility == 8) {
            this.mHandler.removeCallbacks(this.mShowSensorBlockedMessageRunnable);
        } else if (visibility == 0) {
            this.mHandler.removeCallbacks(this.mShowSensorBlockedMessageRunnable);
            this.mHandler.postDelayed(this.mShowSensorBlockedMessageRunnable, 30000);
        }
    }

    @com.squareup.otto.Subscribe
    public void onGestureProgress(com.navdy.hud.app.gesture.GestureServiceConnector.GestureProgress gestureProgress) {
        this.mHandler.removeCallbacks(this.mProgressIndicatorRunnable);
        float progress = gestureProgress.progress;
        if (gestureProgress.direction == com.navdy.hud.app.gesture.GestureServiceConnector.GestureDirection.LEFT) {
            progress *= -1.0f;
        }
        float newSmoothedProgress = (this.mLowPassCoeff * progress) + (this.mSmoothedProgress * (1.0f - this.mLowPassCoeff));
        if (newSmoothedProgress != this.mSmoothedProgress) {
            this.mGestureProgressIndicator.setVisibility(0);
            this.mSmoothedProgress = newSmoothedProgress;
            this.mGestureProgressIndicator.setX((this.mNeutralX + (this.mGestureIndicatorProgressSpan * this.mSmoothedProgress)) - this.mGestureIndicatorHalfWidth);
        }
        this.mHandler.postDelayed(this.mProgressIndicatorRunnable, 100);
    }
}
