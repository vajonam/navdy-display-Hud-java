package com.navdy.hud.app.view;

public class UpdateConfirmationView extends android.widget.RelativeLayout implements com.navdy.hud.app.ui.component.ChoiceLayout.IListener, com.navdy.hud.app.manager.InputManager.IInputHandler {
    private static final long CONFIRMATION_TIMEOUT = 30000;
    private static final int UPDATE_MESSAGE_MAX_WIDTH = 360;
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.view.UpdateConfirmationView.class);
    private android.os.Handler handler;
    private boolean isReminder;
    @butterknife.InjectView(2131624150)
    com.navdy.hud.app.ui.component.ChoiceLayout mChoiceLayout;
    @butterknife.InjectView(2131624147)
    android.widget.ImageView mIcon;
    @butterknife.InjectView(2131624078)
    android.widget.TextView mInfoText;
    @butterknife.InjectView(2131624151)
    android.widget.ImageView mLefttSwipe;
    @butterknife.InjectView(2131624077)
    android.widget.TextView mMainTitleText;
    @javax.inject.Inject
    com.navdy.hud.app.screen.OSUpdateConfirmationScreen.Presenter mPresenter;
    @butterknife.InjectView(2131624152)
    android.widget.ImageView mRightSwipe;
    @butterknife.InjectView(2131624144)
    android.widget.TextView mScreenTitleText;
    @butterknife.InjectView(2131624079)
    android.widget.TextView mVersionText;
    @butterknife.InjectView(2131624145)
    android.widget.RelativeLayout mainSection;
    private java.lang.Runnable timeout;
    private java.lang.String updateVersion;

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            com.navdy.hud.app.view.UpdateConfirmationView.sLogger.v("timedout");
            com.navdy.hud.app.view.UpdateConfirmationView.this.mPresenter.finish();
        }
    }

    public UpdateConfirmationView(android.content.Context context) {
        this(context, null, 0);
    }

    public UpdateConfirmationView(android.content.Context context, android.util.AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public UpdateConfirmationView(android.content.Context context, android.util.AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.handler = new android.os.Handler();
        this.timeout = new com.navdy.hud.app.view.UpdateConfirmationView.Anon1();
        this.updateVersion = "1.3.2887";
        this.isReminder = false;
        if (!isInEditMode()) {
            mortar.Mortar.inject(context, this);
        }
    }

    /* access modifiers changed from: protected */
    @android.annotation.SuppressLint({"StringFormatMatches"})
    public void onFinishInflate() {
        super.onFinishInflate();
        butterknife.ButterKnife.inject((android.view.View) this);
        android.content.res.Resources res = getContext().getResources();
        java.lang.String currentVersion = "1.2.2884";
        if (this.mPresenter != null) {
            this.mPresenter.takeView(this);
            com.navdy.hud.app.framework.toast.ToastManager.getInstance().disableToasts(true);
            com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().enableNotifications(false);
            this.updateVersion = this.mPresenter.getUpdateVersion();
            currentVersion = this.mPresenter.getCurrentVersion();
            this.isReminder = this.mPresenter.isReminder();
        }
        this.mVersionText.setText(java.lang.String.format(res.getString(com.navdy.hud.app.R.string.update_navdy_will_upgrade_from_to), new java.lang.Object[]{this.updateVersion, currentVersion}));
        this.mVersionText.setVisibility(0);
        this.mScreenTitleText.setVisibility(8);
        findViewById(com.navdy.hud.app.R.id.title1).setVisibility(8);
        this.mMainTitleText.setText(com.navdy.hud.app.R.string.update_ready_to_install);
        com.navdy.hud.app.util.ViewUtil.adjustPadding(this.mainSection, 0, 0, 0, 10);
        com.navdy.hud.app.util.ViewUtil.autosize(this.mMainTitleText, 2, (int) UPDATE_MESSAGE_MAX_WIDTH, (int) com.navdy.hud.app.R.array.title_sizes);
        this.mMainTitleText.setVisibility(0);
        this.mInfoText.setText(com.navdy.hud.app.R.string.ota_update_installation_will_take);
        this.mInfoText.setVisibility(0);
        this.mIcon.setImageResource(com.navdy.hud.app.R.drawable.icon_software_update);
        ((com.navdy.hud.app.view.MaxWidthLinearLayout) findViewById(com.navdy.hud.app.R.id.infoContainer)).setMaxWidth(UPDATE_MESSAGE_MAX_WIDTH);
        java.util.List<com.navdy.hud.app.ui.component.ChoiceLayout.Choice> list = new java.util.ArrayList<>();
        if (this.isReminder) {
            list.add(new com.navdy.hud.app.ui.component.ChoiceLayout.Choice(res.getString(com.navdy.hud.app.R.string.install_now), 0));
            list.add(new com.navdy.hud.app.ui.component.ChoiceLayout.Choice(res.getString(com.navdy.hud.app.R.string.install_later), 1));
        } else {
            list.add(new com.navdy.hud.app.ui.component.ChoiceLayout.Choice(res.getString(com.navdy.hud.app.R.string.install), 0));
            list.add(new com.navdy.hud.app.ui.component.ChoiceLayout.Choice(res.getString(com.navdy.hud.app.R.string.cancel), 1));
        }
        this.mChoiceLayout.setChoices(com.navdy.hud.app.ui.component.ChoiceLayout.Mode.LABEL, list, 1, this);
        this.mRightSwipe.setVisibility(0);
        this.mLefttSwipe.setVisibility(0);
        this.handler.removeCallbacks(this.timeout);
        this.handler.postDelayed(this.timeout, 30000);
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        this.handler.removeCallbacks(this.timeout);
        super.onDetachedFromWindow();
        com.navdy.hud.app.framework.toast.ToastManager.getInstance().disableToasts(false);
        com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().enableNotifications(true);
        if (this.mPresenter != null) {
            this.mPresenter.dropView(this);
        }
    }

    public void executeItem(int pos, int id) {
        this.handler.removeCallbacks(this.timeout);
        switch (pos) {
            case 0:
                this.mPresenter.install();
                this.mPresenter.finish();
                if (this.isReminder) {
                    com.navdy.hud.app.analytics.AnalyticsSupport.recordUpdatePrompt(true, false, this.updateVersion);
                    return;
                }
                return;
            case 1:
                this.mPresenter.finish();
                if (this.isReminder) {
                    com.navdy.hud.app.analytics.AnalyticsSupport.recordUpdatePrompt(false, false, this.updateVersion);
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void itemSelected(int pos, int id) {
    }

    public boolean onGesture(com.navdy.service.library.events.input.GestureEvent event) {
        if (event.gesture != null) {
            switch (event.gesture) {
                case GESTURE_SWIPE_LEFT:
                    executeItem(0, 0);
                    return true;
                case GESTURE_SWIPE_RIGHT:
                    executeItem(1, 0);
                    return true;
            }
        }
        return false;
    }

    public boolean onKey(com.navdy.hud.app.manager.InputManager.CustomKeyEvent event) {
        this.handler.removeCallbacks(this.timeout);
        if (this.mChoiceLayout == null || this.mChoiceLayout.getVisibility() != 0) {
            return false;
        }
        switch (event) {
            case LEFT:
                this.mChoiceLayout.moveSelectionLeft();
                return true;
            case RIGHT:
                this.mChoiceLayout.moveSelectionRight();
                return true;
            case SELECT:
                this.mChoiceLayout.executeSelectedItem(true);
                return true;
            default:
                return true;
        }
    }

    public com.navdy.hud.app.manager.InputManager.IInputHandler nextHandler() {
        return null;
    }
}
