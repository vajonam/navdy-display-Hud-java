package com.navdy.hud.app.view;

public class GestureVideoCaptureView$$ViewInjector {
    public static void inject(butterknife.ButterKnife.Finder finder, com.navdy.hud.app.view.GestureVideoCaptureView target, java.lang.Object source) {
        target.sideImageView = (android.widget.ImageView) finder.findRequiredView(source, com.navdy.hud.app.R.id.sideImage, "field 'sideImageView'");
        target.mainImageView = (android.widget.ImageView) finder.findRequiredView(source, com.navdy.hud.app.R.id.image, "field 'mainImageView'");
        target.mainText = (android.widget.TextView) finder.findRequiredView(source, com.navdy.hud.app.R.id.title1, "field 'mainText'");
        target.secondaryText = (android.widget.TextView) finder.findRequiredView(source, com.navdy.hud.app.R.id.title3, "field 'secondaryText'");
        target.mChoiceLayout = (com.navdy.hud.app.ui.component.ChoiceLayout) finder.findRequiredView(source, com.navdy.hud.app.R.id.choiceLayout, "field 'mChoiceLayout'");
    }

    public static void reset(com.navdy.hud.app.view.GestureVideoCaptureView target) {
        target.sideImageView = null;
        target.mainImageView = null;
        target.mainText = null;
        target.secondaryText = null;
        target.mChoiceLayout = null;
    }
}
