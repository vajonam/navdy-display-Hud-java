package com.navdy.hud.app.view;

public class MapMask extends android.view.View {
    private static final java.lang.String TAG = com.navdy.hud.app.view.MapMask.class.getSimpleName();
    private int bottom;
    private android.graphics.Paint fadePaint;
    private android.graphics.Path fadePath;
    private int left;
    private int mHorizontalRadius;
    private com.navdy.hud.app.util.CustomDimension mHorizontalRadiusAttribute;
    private com.navdy.hud.app.view.IndicatorView mIndicator;
    private int mMaskColor;
    private int mVerticalRadius;
    private com.navdy.hud.app.util.CustomDimension mVerticalRadiusAttribute;
    private android.graphics.Paint maskPaint;
    private android.graphics.Path maskPath;
    private int right;
    private int top;

    public MapMask(android.content.Context context) {
        this(context, null);
    }

    public MapMask(android.content.Context context, android.util.AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MapMask(android.content.Context context, android.util.AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initFromAttributes(context, attrs);
        this.mIndicator = new com.navdy.hud.app.view.IndicatorView(context, attrs);
        initDrawingTools();
    }

    private void initFromAttributes(android.content.Context context, android.util.AttributeSet attrs) {
        android.content.res.TypedArray a = context.getTheme().obtainStyledAttributes(attrs, com.navdy.hud.app.R.styleable.MapMask, 0, 0);
        try {
            this.mHorizontalRadiusAttribute = com.navdy.hud.app.util.CustomDimension.getDimension(this, a, 0, 0.0f);
            this.mVerticalRadiusAttribute = com.navdy.hud.app.util.CustomDimension.getDimension(this, a, 1, 0.0f);
            this.mMaskColor = a.getColor(2, -16777216);
        } finally {
            a.recycle();
        }
    }

    private void evaluateDimensions(int w, int h) {
        this.mHorizontalRadius = (int) this.mHorizontalRadiusAttribute.getSize(this, (float) w, 0.0f);
        this.mVerticalRadius = (int) this.mVerticalRadiusAttribute.getSize(this, (float) h, 0.0f);
        this.mIndicator.evaluateDimensions(this.mHorizontalRadius * 2, h);
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        evaluateDimensions(w, h);
        initDrawingTools();
    }

    public void layout(int l, int t, int r, int b) {
        super.layout(l, t, r, b);
        this.mIndicator.layout(this.left, 0, this.right, this.mIndicator.getIndicatorHeight());
    }

    private void initDrawingTools() {
        this.maskPath = new android.graphics.Path();
        this.maskPaint = new android.graphics.Paint();
        this.maskPaint.setColor(this.mMaskColor);
        this.maskPaint.setAntiAlias(true);
        this.maskPaint.setStyle(android.graphics.Paint.Style.FILL_AND_STROKE);
        int width = getWidth();
        int height = getHeight();
        this.maskPath.moveTo(0.0f, 0.0f);
        this.maskPath.lineTo(0.0f, (float) height);
        this.maskPath.lineTo((float) width, (float) height);
        this.maskPath.lineTo((float) width, 0.0f);
        this.maskPath.close();
        this.top = (height - this.mVerticalRadius) / 2;
        this.bottom = this.top + this.mVerticalRadius;
        int ovalBottom = this.bottom + this.mVerticalRadius;
        this.left = (width - (this.mHorizontalRadius * 2)) / 2;
        this.right = this.left + (this.mHorizontalRadius * 2);
        this.maskPath.moveTo((float) this.left, (float) this.bottom);
        android.graphics.RectF oval = new android.graphics.RectF((float) this.left, (float) this.top, (float) this.right, (float) ovalBottom);
        this.maskPath.arcTo(oval, 180.0f, 180.0f, false);
        com.navdy.hud.app.view.IndicatorView.drawIndicatorPath(this.maskPath, (float) this.left, (float) this.bottom, this.mIndicator.getCurveRadius(), (float) this.mIndicator.getIndicatorWidth(), (float) this.mIndicator.getIndicatorHeight(), (float) (this.mHorizontalRadius * 2));
        this.maskPath.setFillType(android.graphics.Path.FillType.EVEN_ODD);
        float strokeWidth = this.mIndicator.getStrokeWidth();
        float radius = (float) this.mHorizontalRadius;
        if (radius < 60.0f) {
            radius = 60.0f;
        }
        this.fadePaint = new android.graphics.Paint();
        this.fadePaint.setStrokeWidth(60.0f);
        this.fadePaint.setAntiAlias(true);
        this.fadePaint.setStrokeCap(android.graphics.Paint.Cap.BUTT);
        this.fadePaint.setStyle(android.graphics.Paint.Style.STROKE);
        this.fadePaint.setShader(new android.graphics.RadialGradient(((float) this.left) + radius, ((float) this.bottom) + strokeWidth, radius, new int[]{0, 0, -16777216}, new float[]{0.0f, (radius - 60.0f) / radius, 1.0f}, android.graphics.Shader.TileMode.CLAMP));
        this.fadePath = new android.graphics.Path();
        this.fadePath.moveTo((float) this.left, (float) this.bottom);
        oval.inset((60.0f / 2.0f) - strokeWidth, (60.0f / 2.0f) - strokeWidth);
        oval.top += strokeWidth;
        oval.bottom += strokeWidth;
        this.fadePath.arcTo(oval, 180.0f, 180.0f, false);
    }

    public android.graphics.PointF getIndicatorPoint() {
        return new android.graphics.PointF((float) (this.left + this.mHorizontalRadius), (float) (this.bottom - this.mIndicator.getIndicatorHeight()));
    }

    /* access modifiers changed from: protected */
    public void onDraw(android.graphics.Canvas canvas) {
        canvas.drawPath(this.maskPath, this.maskPaint);
        int yOffset = this.bottom - this.mIndicator.getIndicatorHeight();
        canvas.translate((float) this.left, (float) yOffset);
        this.mIndicator.draw(canvas);
        canvas.translate((float) (-this.left), (float) (-yOffset));
        canvas.drawPath(this.fadePath, this.fadePaint);
    }
}
