package com.navdy.hud.app.view;

public class HighlightView extends android.view.View {
    private android.graphics.Paint paint;
    private int radius;
    private float strokeWidth;

    public HighlightView(android.content.Context context) {
        this(context, null);
    }

    public HighlightView(android.content.Context context, android.util.AttributeSet attrs) {
        super(context, attrs);
        initFromAttributes(context, attrs);
        this.paint = new android.graphics.Paint();
        this.paint.setStrokeWidth(this.strokeWidth);
        this.paint.setAntiAlias(true);
        this.paint.setColor(getResources().getColor(com.navdy.hud.app.R.color.hud_cyan));
    }

    private void initFromAttributes(android.content.Context context, android.util.AttributeSet attrs) {
        android.content.res.TypedArray a = context.getTheme().obtainStyledAttributes(attrs, com.navdy.hud.app.R.styleable.Indicator, 0, 0);
        this.strokeWidth = a.getDimension(3, (float) ((int) getResources().getDimension(com.navdy.hud.app.R.dimen.default_highlight_stroke_width)));
        a.recycle();
    }

    public void setStrokeWidth(float strokeWidth2) {
        this.strokeWidth = strokeWidth2;
    }

    public void setPaintStyle(android.graphics.Paint.Style paintStyle) {
        if (paintStyle == android.graphics.Paint.Style.FILL_AND_STROKE) {
            throw new java.lang.IllegalArgumentException();
        }
        this.paint.setStyle(paintStyle);
    }

    public void setRadius(int radius2) {
        this.radius = radius2;
    }

    /* access modifiers changed from: protected */
    public void onDraw(android.graphics.Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawCircle((float) (getWidth() / 2), (float) (getHeight() / 2), (float) this.radius, this.paint);
    }
}
