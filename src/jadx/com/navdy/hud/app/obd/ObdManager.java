package com.navdy.hud.app.obd;

public final class ObdManager {
    public static final float BATTERY_CHARGING_VOLTAGE = 13.1f;
    public static final double BATTERY_NO_OBD = -1.0d;
    private static final java.lang.String BLACKLIST_MODIFICATION_TIME = "blacklist_modification_time";
    public static final double CRITICAL_BATTERY_VOLTAGE = ((double) com.navdy.hud.app.util.os.SystemProperties.getFloat(CRITICAL_VOLTAGE_OVERRIDE_PROPERTY, DEFAULT_CRITICAL_BATTERY_VOLTAGE));
    private static final java.lang.String CRITICAL_VOLTAGE_OVERRIDE_PROPERTY = "persist.sys.voltage.crit";
    private static final float DEFAULT_CRITICAL_BATTERY_VOLTAGE = 12.0f;
    private static final long DEFAULT_LOW_VOLTAGE_SHUTOFF_DURATION = java.util.concurrent.TimeUnit.MINUTES.toMillis(15);
    private static final java.lang.String DEFAULT_SET = "default_set";
    private static final int DISTANCE_UPDATE_INTERVAL = 60000;
    private static final int ENGINE_TEMP_UPDATE_INTERVAL = 2000;
    public static final java.lang.String FIRMWARE_VERSION = "4.2.1";
    public static final java.lang.String FIRST_ODOMETER_READING_KM = "first_odometer_reading";
    private static final int FLUSH_INTERVAL_MS = 30000;
    private static final int FUEL_UPDATE_INTERVAL = 15000;
    private static final int GENERIC_UPDATE_INTERVAL = 1000;
    public static final java.lang.String LAST_ODOMETER_READING_KM = "last_odometer_reading";
    /* access modifiers changed from: private */
    public static final int LOW_BATTERY_COUNT_THRESHOLD = ((int) java.lang.Math.ceil(((double) LOW_VOLTAGE_SHUTOFF_DURATION) / 2000.0d));
    public static final double LOW_BATTERY_VOLTAGE = ((double) com.navdy.hud.app.util.os.SystemProperties.getFloat(LOW_VOLTAGE_OVERRIDE_PROPERTY, 12.2f));
    public static final int LOW_FUEL_LEVEL = 10;
    private static final java.lang.String LOW_VOLTAGE_DURATION_OVERRIDE_PROPERTY = "persist.sys.voltage.duration";
    private static final java.lang.String LOW_VOLTAGE_OVERRIDE_PROPERTY = "persist.sys.voltage.low";
    private static final long LOW_VOLTAGE_SHUTOFF_DURATION = com.navdy.hud.app.util.os.SystemProperties.getLong(LOW_VOLTAGE_DURATION_OVERRIDE_PROPERTY, DEFAULT_LOW_VOLTAGE_SHUTOFF_DURATION);
    public static final long MINIMUM_TIME_BETWEEN_MILEAGE_REPORTING = java.util.concurrent.TimeUnit.MINUTES.toMillis(10);
    private static final int MPG_UPDATE_INTERVAL = 1000;
    public static final int NOT_AVAILABLE = -1;
    private static final com.navdy.hud.app.obd.ObdManager.ObdConnectionStatusEvent OBD_CONNECTED = new com.navdy.hud.app.obd.ObdManager.ObdConnectionStatusEvent(true);
    private static final com.navdy.hud.app.obd.ObdManager.ObdConnectionStatusEvent OBD_NOT_CONNECTED = new com.navdy.hud.app.obd.ObdManager.ObdConnectionStatusEvent(false);
    private static final int RPM_UPDATE_INTERVAL = 250;
    public static final int SAFE_TO_SLEEP_BATTERY_OBSERVED_COUNT_THRESHOLD = 5;
    private static final java.lang.String SCAN_SETTING = "scan_setting";
    private static final int SLOW_RPM_UPDATE_INTERVAL = 2500;
    private static final int SPEED_UPDATE_INTERVAL = 100;
    private static final int STATE_CONNECTED = 2;
    private static final int STATE_CONNECTING = 1;
    private static final int STATE_DISCONNECTED = 0;
    private static final int STATE_IDLE = 4;
    private static final int STATE_SLEEPING = 5;
    public static final java.lang.String TELEMETRY_TAG = "Telemetry";
    public static final java.lang.String TOTAL_DISTANCE_TRAVELLED_WITH_NAVDY_METERS = "total_distance_travelled_with_navdy";
    public static final int VALID_FUEL_DATA_WAIT_TIME = 45000;
    public static final java.lang.String VEHICLE_VIN = "vehicle_vin";
    public static final int VOLTAGE_REPORT_INTERVAL = 2000;
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.obd.ObdManager.class);
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sTelemetryLogger = new com.navdy.service.library.log.Logger(TELEMETRY_TAG);
    private static final com.navdy.hud.app.obd.ObdManager singleton = new com.navdy.hud.app.obd.ObdManager();
    /* access modifiers changed from: private */
    public double batteryVoltage = -1.0d;
    @javax.inject.Inject
    com.squareup.otto.Bus bus;
    private com.navdy.obd.ICanBusMonitoringListener canBusMonitoringListener = new com.navdy.hud.app.obd.ObdManager.Anon7();
    /* access modifiers changed from: private */
    public com.navdy.hud.app.obd.CarServiceConnector carServiceConnector;
    /* access modifiers changed from: private */
    public java.lang.Runnable checkVoltage = new com.navdy.hud.app.obd.ObdManager.Anon5();
    private com.navdy.hud.app.obd.ObdManager.ConnectionType connectionType = com.navdy.hud.app.obd.ObdManager.ConnectionType.POWER_ONLY;
    private com.navdy.hud.app.debug.DriveRecorder driveRecorder;
    @javax.inject.Inject
    com.navdy.hud.app.profile.DriverProfileManager driverProfileManager;
    /* access modifiers changed from: private */
    public java.util.List<com.navdy.obd.ECU> ecus;
    /* access modifiers changed from: private */
    public boolean firstScan;
    com.navdy.hud.app.obd.ObdManager.PidCheck fuelPidCheck;
    private com.navdy.obd.ScanSchedule fullScan;
    /* access modifiers changed from: private */
    public android.os.Handler handler;
    private boolean isCheckEngineLightOn = false;
    /* access modifiers changed from: private */
    public int lowBatteryCount;
    /* access modifiers changed from: private */
    public double maxBatteryVoltage = -1.0d;
    private long mileageEventLastReportTime = 0;
    /* access modifiers changed from: private */
    public double minBatteryVoltage = -1.0d;
    private com.navdy.obd.ScanSchedule minimalScan;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.obd.ObdCanBusRecordingPolicy obdCanBusRecordingPolicy;
    private java.lang.String obdChipFirmwareVersion;
    private volatile boolean obdConnected;
    private com.navdy.hud.app.obd.ObdDeviceConfigurationManager obdDeviceConfigurationManager;
    /* access modifiers changed from: private */
    public java.util.HashMap<java.lang.Integer, java.lang.Double> obdPidsCache = new java.util.HashMap<>();
    private boolean odoMeterReadingValidated = false;
    /* access modifiers changed from: private */
    public java.lang.Runnable periodicCheckRunnable = new com.navdy.hud.app.obd.ObdManager.Anon4();
    private com.navdy.obd.IPidListener pidListener = new com.navdy.hud.app.obd.ObdManager.Anon6();
    @javax.inject.Inject
    com.navdy.hud.app.device.PowerManager powerManager;
    /* access modifiers changed from: private */
    public java.lang.String protocol;
    /* access modifiers changed from: private */
    public com.navdy.obd.IPidListener recordingPidListener;
    /* access modifiers changed from: private */
    public int safeToSleepBatteryLevelObservedCount = 0;
    private com.navdy.obd.ScanSchedule schedule;
    @javax.inject.Inject
    android.content.SharedPreferences sharedPreferences;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.manager.SpeedManager speedManager = com.navdy.hud.app.manager.SpeedManager.getInstance();
    /* access modifiers changed from: private */
    public com.navdy.obd.PidSet supportedPidSet = new com.navdy.obd.PidSet();
    @javax.inject.Inject
    public com.navdy.hud.app.analytics.TelemetryDataManager telemetryDataManager;
    private long totalDistanceInCurrentSessionPersisted = 0;
    @javax.inject.Inject
    com.navdy.hud.app.framework.trips.TripManager tripManager;
    private java.util.List<java.lang.String> troubleCodes;
    /* access modifiers changed from: private */
    public java.lang.String vin;

    class Anon1 extends com.navdy.hud.app.obd.ObdManager.PidCheck {
        Anon1(int pid, android.os.Handler handler) {
            super(pid, handler);
        }

        public boolean isPidValueValid(double value) {
            return value > 0.0d;
        }

        public void invalidatePid(int pid) {
            com.navdy.hud.app.obd.ObdManager.this.obdPidsCache.remove(java.lang.Integer.valueOf(47));
            if (com.navdy.hud.app.obd.ObdManager.this.supportedPidSet != null) {
                com.navdy.hud.app.obd.ObdManager.this.supportedPidSet.remove(47);
                com.navdy.hud.app.obd.ObdManager.this.bus.post(new com.navdy.hud.app.obd.ObdManager.ObdSupportedPidsChangedEvent());
            }
            com.navdy.hud.app.analytics.AnalyticsSupport.recordIncorrectFuelData();
        }

        public long getWaitForValidDataTimeout() {
            return 45000;
        }
    }

    class Anon2 implements java.lang.Runnable {

        class Anon1 implements java.lang.Runnable {
            Anon1() {
            }

            public void run() {
                com.navdy.service.library.log.Logger.flush();
            }
        }

        Anon2() {
        }

        public void run() {
            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.obd.ObdManager.Anon2.Anon1(), 6);
            com.navdy.hud.app.obd.ObdManager.this.handler.postDelayed(this, 30000);
        }
    }

    class Anon3 implements java.lang.Runnable {
        Anon3() {
        }

        public void run() {
            com.navdy.hud.app.obd.ObdManager.this.updateListener();
        }
    }

    class Anon4 implements java.lang.Runnable {
        Anon4() {
        }

        public void run() {
            com.navdy.service.library.task.TaskManager.getInstance().execute(com.navdy.hud.app.obd.ObdManager.this.checkVoltage, 1);
            com.navdy.hud.app.obd.ObdManager.this.handler.postDelayed(this, 2000);
        }
    }

    class Anon5 implements java.lang.Runnable {
        Anon5() {
        }

        public void run() {
            double prevVoltage = com.navdy.hud.app.obd.ObdManager.this.batteryVoltage;
            com.navdy.hud.app.obd.ObdManager.this.batteryVoltage = com.navdy.hud.app.obd.ObdManager.this.getBatteryVoltage();
            if (com.navdy.hud.app.obd.ObdManager.this.batteryVoltage != -1.0d && prevVoltage == -1.0d) {
                com.navdy.hud.app.obd.ObdManager.this.minBatteryVoltage = com.navdy.hud.app.obd.ObdManager.this.batteryVoltage;
                com.navdy.hud.app.analytics.AnalyticsSupport.recordStartingBatteryVoltage(com.navdy.hud.app.obd.ObdManager.this.batteryVoltage);
            }
            if (com.navdy.hud.app.obd.ObdManager.this.batteryVoltage == 0.0d || com.navdy.hud.app.obd.ObdManager.this.batteryVoltage == -1.0d) {
                if (com.navdy.hud.app.obd.ObdManager.this.lowBatteryCount != 0) {
                    com.navdy.hud.app.obd.ObdManager.sLogger.d("Battery level data is not available");
                }
                com.navdy.hud.app.obd.ObdManager.this.lowBatteryCount = 0;
                com.navdy.hud.app.obd.ObdManager.this.safeToSleepBatteryLevelObservedCount = 0;
                return;
            }
            if (com.navdy.hud.app.obd.ObdManager.this.batteryVoltage < com.navdy.hud.app.obd.ObdManager.this.minBatteryVoltage) {
                com.navdy.hud.app.obd.ObdManager.this.minBatteryVoltage = com.navdy.hud.app.obd.ObdManager.this.batteryVoltage;
            }
            if (com.navdy.hud.app.obd.ObdManager.this.batteryVoltage > com.navdy.hud.app.obd.ObdManager.this.maxBatteryVoltage) {
                com.navdy.hud.app.obd.ObdManager.this.maxBatteryVoltage = com.navdy.hud.app.obd.ObdManager.this.batteryVoltage;
            }
            double lowBatteryThreshold = com.navdy.hud.app.obd.ObdManager.this.powerManager.inQuietMode() ? com.navdy.hud.app.obd.ObdManager.LOW_BATTERY_VOLTAGE : com.navdy.hud.app.obd.ObdManager.CRITICAL_BATTERY_VOLTAGE;
            if (com.navdy.hud.app.obd.ObdManager.this.batteryVoltage >= 13.100000381469727d && com.navdy.hud.app.obd.ObdManager.this.powerManager.inQuietMode()) {
                com.navdy.hud.app.obd.ObdManager.sLogger.d("Battery seems to be charging, waking up");
                com.navdy.hud.app.obd.ObdManager.this.powerManager.wakeUp(com.navdy.hud.app.analytics.AnalyticsSupport.WakeupReason.VOLTAGE_SPIKE);
                com.navdy.hud.app.obd.ObdManager.this.safeToSleepBatteryLevelObservedCount = 0;
            } else if (com.navdy.hud.app.obd.ObdManager.this.batteryVoltage < lowBatteryThreshold) {
                com.navdy.hud.app.obd.ObdManager.this.safeToSleepBatteryLevelObservedCount = 0;
                com.navdy.hud.app.obd.ObdManager.sLogger.d("Battery voltage (LOW) : " + com.navdy.hud.app.obd.ObdManager.this.batteryVoltage);
                com.navdy.hud.app.obd.ObdManager.this.lowBatteryCount = com.navdy.hud.app.obd.ObdManager.this.lowBatteryCount + 1;
                if (com.navdy.hud.app.obd.ObdManager.this.lowBatteryCount > com.navdy.hud.app.obd.ObdManager.LOW_BATTERY_COUNT_THRESHOLD) {
                    com.navdy.hud.app.obd.ObdManager.sLogger.d("Battery Voltage is too low, shutting down");
                    try {
                        com.navdy.hud.app.event.Shutdown.Reason reason = com.navdy.hud.app.obd.ObdManager.this.powerManager.inQuietMode() ? com.navdy.hud.app.event.Shutdown.Reason.LOW_VOLTAGE : com.navdy.hud.app.event.Shutdown.Reason.CRITICAL_VOLTAGE;
                        com.navdy.hud.app.obd.ObdManager.this.sleep(true);
                        com.navdy.hud.app.obd.ObdManager.this.bus.post(new com.navdy.hud.app.event.Shutdown(reason));
                    } catch (java.lang.Exception e) {
                        com.navdy.hud.app.obd.ObdManager.sLogger.e("error invoking ShutdownMonitor.androidShutdown(): " + e);
                    }
                }
            } else {
                com.navdy.hud.app.obd.ObdManager.this.lowBatteryCount = 0;
                if (com.navdy.hud.app.obd.ObdManager.this.powerManager.inQuietMode()) {
                    com.navdy.hud.app.obd.ObdManager.sLogger.d("In Quiet mode , Battery voltage : " + com.navdy.hud.app.obd.ObdManager.this.batteryVoltage);
                    com.navdy.hud.app.obd.ObdManager.this.safeToSleepBatteryLevelObservedCount = com.navdy.hud.app.obd.ObdManager.this.safeToSleepBatteryLevelObservedCount + 1;
                    if (com.navdy.hud.app.obd.ObdManager.this.safeToSleepBatteryLevelObservedCount >= 5) {
                        com.navdy.hud.app.obd.ObdManager.sLogger.d("Safe to put obd chip to sleep, invoking sleep");
                        com.navdy.hud.app.obd.ObdManager.this.safeToSleepBatteryLevelObservedCount = 0;
                        com.navdy.hud.app.obd.ObdManager.this.handler.removeCallbacks(com.navdy.hud.app.obd.ObdManager.this.periodicCheckRunnable);
                        com.navdy.hud.app.obd.ObdManager.this.sleep(false);
                    }
                }
            }
        }
    }

    class Anon6 extends com.navdy.obd.IPidListener.Stub {
        Anon6() {
        }

        public synchronized void pidsRead(java.util.List<com.navdy.obd.Pid> pidsRead, java.util.List<com.navdy.obd.Pid> pidsChanged) throws android.os.RemoteException {
            if (com.navdy.hud.app.obd.ObdManager.this.firstScan && pidsChanged != null) {
                com.navdy.hud.app.obd.ObdManager.this.firstScan = false;
                try {
                    com.navdy.obd.ICarService api = com.navdy.hud.app.obd.ObdManager.this.carServiceConnector.getCarApi();
                    com.navdy.hud.app.obd.ObdManager.this.vin = api.getVIN();
                    com.navdy.hud.app.obd.ObdManager.this.onVinRead(com.navdy.hud.app.obd.ObdManager.this.vin);
                    com.navdy.hud.app.obd.ObdManager.this.ecus = api.getEcus();
                    com.navdy.hud.app.obd.ObdManager.this.protocol = api.getProtocol();
                    java.util.List<com.navdy.obd.Pid> supportedPids = com.navdy.hud.app.obd.ObdManager.this.carServiceConnector.getCarApi().getSupportedPids();
                    com.navdy.hud.app.obd.ObdManager.sLogger.d("First scan, got the supported PIDS");
                    if (supportedPids != null) {
                        com.navdy.hud.app.obd.ObdManager.sLogger.d("Supported PIDS size :" + supportedPids.size());
                    } else {
                        com.navdy.hud.app.obd.ObdManager.sLogger.d("First scan, got the supported PIDS , list null");
                    }
                    if (supportedPids != null) {
                        com.navdy.obd.PidSet pidSet = new com.navdy.obd.PidSet(supportedPids);
                        if (!pidSet.equals(com.navdy.hud.app.obd.ObdManager.this.supportedPidSet)) {
                            com.navdy.hud.app.obd.ObdManager.sTelemetryLogger.v("SUPPORTED VIN: " + (com.navdy.hud.app.obd.ObdManager.this.vin != null ? com.navdy.hud.app.obd.ObdManager.this.vin : "") + ", " + "FUEL: " + pidSet.contains(47) + ", " + "SPEED: " + pidSet.contains(13) + ", " + "RPM: " + pidSet.contains(12) + ", " + "DIST: " + pidSet.contains(49) + ", " + "MAF: " + pidSet.contains(16) + ", " + "IFC(LPKKM): " + supportedPids.contains(java.lang.Integer.valueOf(256)));
                            com.navdy.hud.app.obd.ObdManager.this.supportedPidSet = pidSet;
                            com.navdy.hud.app.obd.ObdManager.this.fuelPidCheck.reset();
                            com.navdy.hud.app.obd.ObdManager.this.bus.post(new com.navdy.hud.app.obd.ObdManager.ObdSupportedPidsChangedEvent());
                            com.navdy.hud.app.obd.ObdManager.sLogger.v("pid-changed event sent");
                        } else {
                            com.navdy.hud.app.obd.ObdManager.sLogger.v("pid-changed event not sent");
                        }
                    }
                } catch (Throwable t) {
                    com.navdy.hud.app.obd.ObdManager.sLogger.e(t);
                }
            }
            if (pidsChanged == null) {
                com.navdy.hud.app.obd.ObdManager.this.bus.post(new com.navdy.hud.app.obd.ObdManager.ObdPidReadEvent(pidsRead));
            } else {
                for (com.navdy.obd.Pid pid : pidsChanged) {
                    switch (pid.getId()) {
                        case 13:
                            com.navdy.hud.app.obd.ObdManager.this.speedManager.setObdSpeed((int) pid.getValue(), pid.getTimeStamp());
                            break;
                        case 47:
                            com.navdy.hud.app.obd.ObdManager.this.fuelPidCheck.checkPid((double) ((int) pid.getValue()));
                            if (com.navdy.hud.app.obd.ObdManager.this.fuelPidCheck.hasIncorrectData()) {
                                com.navdy.hud.app.obd.ObdManager.this.obdPidsCache.remove(java.lang.Integer.valueOf(47));
                                break;
                            } else {
                                com.navdy.hud.app.obd.ObdManager.this.obdPidsCache.put(java.lang.Integer.valueOf(pid.getId()), java.lang.Double.valueOf(pid.getValue()));
                                break;
                            }
                        case 49:
                            com.navdy.hud.app.obd.ObdManager.this.obdPidsCache.put(java.lang.Integer.valueOf(pid.getId()), java.lang.Double.valueOf(pid.getValue()));
                            com.navdy.hud.app.obd.ObdManager.this.updateCumulativeVehicleDistanceTravelled((long) pid.getValue());
                            break;
                        default:
                            com.navdy.hud.app.obd.ObdManager.this.obdPidsCache.put(java.lang.Integer.valueOf(pid.getId()), java.lang.Double.valueOf(pid.getValue()));
                            break;
                    }
                    com.navdy.hud.app.obd.ObdManager.this.bus.post(new com.navdy.hud.app.obd.ObdManager.ObdPidChangeEvent(pid));
                }
                if (!(com.navdy.hud.app.obd.ObdManager.this.recordingPidListener == null || pidsChanged == null)) {
                    com.navdy.hud.app.obd.ObdManager.this.recordingPidListener.pidsRead(pidsRead, pidsChanged);
                }
                com.navdy.hud.app.obd.ObdManager.this.bus.post(new com.navdy.hud.app.obd.ObdManager.ObdPidReadEvent(pidsRead));
            }
            return;
        }

        public void pidsChanged(java.util.List<com.navdy.obd.Pid> list) throws android.os.RemoteException {
        }

        public void onConnectionStateChange(int newState) throws android.os.RemoteException {
            com.navdy.hud.app.obd.ObdManager.this.updateConnectionState(newState);
        }
    }

    class Anon7 extends com.navdy.obd.ICanBusMonitoringListener.Stub {
        Anon7() {
        }

        public void onNewDataAvailable(java.lang.String fileName) throws android.os.RemoteException {
            com.navdy.hud.app.obd.ObdManager.sLogger.d("onNewDataAvailable : " + fileName);
            com.navdy.hud.app.obd.ObdManager.this.obdCanBusRecordingPolicy.onNewDataAvailable(fileName);
        }

        public void onCanBusMonitoringError(java.lang.String errorMessage) throws android.os.RemoteException {
            com.navdy.hud.app.obd.ObdManager.sLogger.d("onCanBusMonitoringError, Error : " + errorMessage);
            com.navdy.hud.app.obd.ObdManager.this.obdCanBusRecordingPolicy.onCanBusMonitoringFailed();
        }

        public void onCanBusMonitorSuccess(int averageAmountOfData) throws android.os.RemoteException {
            com.navdy.hud.app.obd.ObdManager.sLogger.d("onCanBusMonitorSuccess , Average amount of data : " + averageAmountOfData);
            com.navdy.hud.app.obd.ObdManager.this.obdCanBusRecordingPolicy.onCanBusMonitorSuccess(averageAmountOfData);
        }

        public int getGpsSpeed() throws android.os.RemoteException {
            return java.lang.Math.round(com.navdy.hud.app.obd.ObdManager.this.speedManager.getGpsSpeedInMetersPerSecond());
        }

        public double getLatitude() throws android.os.RemoteException {
            if (com.navdy.hud.app.maps.here.HereMapsManager.getInstance().isInitialized()) {
                com.here.android.mpa.common.GeoCoordinate c = com.navdy.hud.app.maps.here.HereMapsManager.getInstance().getLocationFixManager().getLastGeoCoordinate();
                if (c != null) {
                    return c.getLatitude();
                }
            }
            return -1.0d;
        }

        public double getLongitude() throws android.os.RemoteException {
            if (com.navdy.hud.app.maps.here.HereMapsManager.getInstance().isInitialized()) {
                com.here.android.mpa.common.GeoCoordinate c = com.navdy.hud.app.maps.here.HereMapsManager.getInstance().getLocationFixManager().getLastGeoCoordinate();
                if (c != null) {
                    return c.getLongitude();
                }
            }
            return -1.0d;
        }

        public boolean isMonitoringLimitReached() throws android.os.RemoteException {
            return com.navdy.hud.app.obd.ObdManager.this.obdCanBusRecordingPolicy.isCanBusMonitoringLimitReached();
        }

        public java.lang.String getMake() throws android.os.RemoteException {
            if (com.navdy.hud.app.obd.ObdManager.this.driverProfileManager != null) {
                com.navdy.hud.app.profile.DriverProfile profile = com.navdy.hud.app.obd.ObdManager.this.driverProfileManager.getCurrentProfile();
                if (profile != null) {
                    return profile.getCarMake();
                }
            }
            return "";
        }

        public java.lang.String getModel() throws android.os.RemoteException {
            if (com.navdy.hud.app.obd.ObdManager.this.driverProfileManager != null) {
                com.navdy.hud.app.profile.DriverProfile profile = com.navdy.hud.app.obd.ObdManager.this.driverProfileManager.getCurrentProfile();
                if (profile != null) {
                    return profile.getCarModel();
                }
            }
            return "";
        }

        public java.lang.String getYear() throws android.os.RemoteException {
            if (com.navdy.hud.app.obd.ObdManager.this.driverProfileManager != null) {
                com.navdy.hud.app.profile.DriverProfile profile = com.navdy.hud.app.obd.ObdManager.this.driverProfileManager.getCurrentProfile();
                if (profile != null) {
                    return profile.getCarYear();
                }
            }
            return "";
        }
    }

    public enum ConnectionType {
        POWER_ONLY,
        OBD
    }

    public enum Firmware {
        OLD_320(com.navdy.hud.app.R.raw.update3_2_0),
        NEW_421(com.navdy.hud.app.R.raw.update4_2_1);
        
        public int resource;

        private Firmware(int resource2) {
            this.resource = resource2;
        }
    }

    public static class ObdConnectionStatusEvent {
        public boolean connected;

        ObdConnectionStatusEvent(boolean connected2) {
            this.connected = connected2;
        }
    }

    public static class ObdPidChangeEvent {
        public com.navdy.obd.Pid pid;

        ObdPidChangeEvent(com.navdy.obd.Pid pid2) {
            this.pid = pid2;
        }
    }

    public static class ObdPidReadEvent {
        public com.navdy.obd.PidSet readPids;

        public ObdPidReadEvent(java.util.List<com.navdy.obd.Pid> readPids2) {
            this.readPids = new com.navdy.obd.PidSet(readPids2);
        }
    }

    public static class ObdSupportedPidsChangedEvent {
    }

    class PidCheck {
        private android.os.Handler handler;
        volatile boolean hasIncorrectData = false;
        private java.lang.Runnable invalidPidRunnable;
        int pid;
        volatile boolean waitingForValidData = false;

        class Anon1 implements java.lang.Runnable {
            final /* synthetic */ int val$pid;
            final /* synthetic */ com.navdy.hud.app.obd.ObdManager val$this$Anon0;

            Anon1(com.navdy.hud.app.obd.ObdManager obdManager, int i) {
                this.val$this$Anon0 = obdManager;
                this.val$pid = i;
            }

            public void run() {
                if (!com.navdy.hud.app.obd.ObdManager.PidCheck.this.hasIncorrectData) {
                    com.navdy.hud.app.obd.ObdManager.PidCheck.this.waitingForValidData = false;
                    com.navdy.hud.app.obd.ObdManager.PidCheck.this.hasIncorrectData = true;
                    com.navdy.hud.app.obd.ObdManager.PidCheck.this.invalidatePid(this.val$pid);
                }
            }
        }

        public PidCheck(int pid2, android.os.Handler handler2) {
            this.pid = pid2;
            this.handler = handler2;
            this.invalidPidRunnable = new com.navdy.hud.app.obd.ObdManager.PidCheck.Anon1(com.navdy.hud.app.obd.ObdManager.this, pid2);
        }

        public boolean isPidValueValid(double value) {
            return true;
        }

        public void invalidatePid(int pid2) {
        }

        public boolean hasIncorrectData() {
            return this.hasIncorrectData;
        }

        public boolean isWaitingForValidData() {
            return this.waitingForValidData;
        }

        public long getWaitForValidDataTimeout() {
            return 0;
        }

        public void reset() {
            this.hasIncorrectData = false;
            this.handler.removeCallbacks(this.invalidPidRunnable);
            this.waitingForValidData = false;
        }

        public void checkPid(double value) {
            if (isPidValueValid(value)) {
                reset();
            } else if (this.hasIncorrectData) {
            } else {
                if (!this.waitingForValidData) {
                    this.handler.removeCallbacks(this.invalidPidRunnable);
                    this.handler.postDelayed(this.invalidPidRunnable, getWaitForValidDataTimeout());
                    this.waitingForValidData = true;
                    return;
                }
                com.navdy.hud.app.obd.ObdManager.sLogger.d("Already waiting for valid PID data, PID : " + this.pid + " ,  Value : " + value);
            }
        }
    }

    public static com.navdy.hud.app.obd.ObdManager getInstance() {
        return singleton;
    }

    public com.navdy.hud.app.obd.ObdManager.ConnectionType getConnectionType() {
        return this.connectionType;
    }

    private ObdManager() {
        mortar.Mortar.inject(com.navdy.hud.app.HudApplication.getAppContext(), this);
        this.obdCanBusRecordingPolicy = new com.navdy.hud.app.obd.ObdCanBusRecordingPolicy(com.navdy.hud.app.HudApplication.getAppContext(), this.sharedPreferences, this, this.tripManager);
        this.carServiceConnector = new com.navdy.hud.app.obd.CarServiceConnector(com.navdy.hud.app.HudApplication.getAppContext());
        this.obdDeviceConfigurationManager = new com.navdy.hud.app.obd.ObdDeviceConfigurationManager(this.carServiceConnector);
        this.minimalScan = new com.navdy.obd.ScanSchedule();
        this.minimalScan.addPid(13, 100);
        this.minimalScan.addPid(47, 15000);
        this.minimalScan.addPid(49, 60000);
        this.fullScan = new com.navdy.obd.ScanSchedule(this.minimalScan);
        this.fullScan.addPid(12, 250);
        this.fullScan.addPid(16, 1000);
        this.fullScan.addPid(256, 1000);
        this.fullScan.addPid(5, 2000);
        this.fullScan.addPid(com.navdy.obd.Pids.ENGINE_OIL_PRESSURE, 1000);
        this.fullScan.addPid(com.navdy.obd.Pids.ENGINE_TRIP_FUEL, 1000);
        this.fullScan.addPid(com.navdy.obd.Pids.TOTAL_VEHICLE_DISTANCE, 1000);
        this.minimalScan.addPid(12, SLOW_RPM_UPDATE_INTERVAL);
        this.bus.register(this);
        this.bus.register(this.obdDeviceConfigurationManager);
        this.bus.register(this.obdCanBusRecordingPolicy);
        sTelemetryLogger.v("SESSION started");
        this.handler = new android.os.Handler(android.os.Looper.getMainLooper());
        this.fuelPidCheck = new com.navdy.hud.app.obd.ObdManager.Anon1(47, this.handler);
        this.handler.post(new com.navdy.hud.app.obd.ObdManager.Anon2());
    }

    public java.lang.String getObdChipFirmwareVersion() {
        return this.obdChipFirmwareVersion;
    }

    /* access modifiers changed from: 0000 */
    public void serviceConnected() {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.obd.ObdManager.Anon3(), 6);
    }

    public void updateListener() {
        com.navdy.obd.ICarService carService = this.carServiceConnector.getCarApi();
        if (carService != null) {
            try {
                carService.setCANBusMonitoringListener(this.canBusMonitoringListener);
            } catch (android.os.RemoteException e) {
                sLogger.e("RemoteException ", e);
            }
            try {
                this.firstScan = true;
                int connectionState = carService.getConnectionState();
                switch (connectionState) {
                    case 2:
                    case 4:
                        this.obdChipFirmwareVersion = carService.getObdChipFirmwareVersion();
                        sLogger.d("Obd chip firmware version " + this.obdChipFirmwareVersion + ", Connection State :" + connectionState);
                        break;
                    case 5:
                        sLogger.d("ObdService is in Sleeping state, waking it up");
                        carService.wakeup();
                        break;
                }
                updateConnectionState(carService.getConnectionState());
                monitorBatteryVoltage();
                sLogger.i("adding listener for " + this.schedule);
                if (this.schedule != null) {
                    carService.updateScan(this.schedule, this.pidListener);
                }
                if (this.obdCanBusRecordingPolicy.isCanBusMonitoringNeeded()) {
                    sLogger.d("Start CAN bus monitoring");
                    carService.startCanBusMonitoring();
                    return;
                }
                sLogger.d("stop CAN bus monitoring");
                carService.stopCanBusMonitoring();
            } catch (Throwable t) {
                sLogger.e(t);
            }
        }
    }

    public com.navdy.obd.ICarService getCarService() {
        if (this.carServiceConnector != null) {
            return this.carServiceConnector.getCarApi();
        }
        return null;
    }

    @com.squareup.otto.Subscribe
    public void onWakeUp(com.navdy.hud.app.event.Wakeup wakeup) {
        sLogger.d("onWakeUp");
        updateListener();
    }

    public void monitorBatteryVoltage() {
        sLogger.i("monitorBatteryVoltage()");
        this.handler.removeCallbacks(this.periodicCheckRunnable);
        this.handler.postDelayed(this.periodicCheckRunnable, 4000);
    }

    private void setConnected(boolean connected) {
        if (this.obdConnected != connected) {
            if (this.driveRecorder != null) {
                this.driveRecorder.setRealObdConnected(connected);
            }
            this.fuelPidCheck.reset();
            this.obdDeviceConfigurationManager.setConnectionState(connected);
            this.obdConnected = connected;
            if (connected) {
                try {
                    com.navdy.obd.ICarService api = this.carServiceConnector.getCarApi();
                    this.vin = api.getVIN();
                    onVinRead(this.vin);
                    this.ecus = api.getEcus();
                    this.protocol = api.getProtocol();
                    this.isCheckEngineLightOn = api.isCheckEngineLightOn();
                    this.troubleCodes = api.getTroubleCodes();
                    this.obdChipFirmwareVersion = api.getObdChipFirmwareVersion();
                    sLogger.d("Connected, Obd chip firmware version " + this.obdChipFirmwareVersion);
                } catch (Throwable t) {
                    sLogger.d("Failed to read vin/ecu/protocol after connecting", t);
                }
            } else {
                this.firstScan = true;
                this.vin = null;
                this.ecus = null;
                this.protocol = null;
                this.speedManager.setObdSpeed(-1, 0);
                if (this.supportedPidSet != null) {
                    this.supportedPidSet.clear();
                }
                this.obdPidsCache.clear();
            }
            this.bus.post(connected ? OBD_CONNECTED : OBD_NOT_CONNECTED);
            this.obdCanBusRecordingPolicy.onObdConnectionStateChanged(connected);
        }
    }

    /* access modifiers changed from: 0000 */
    public void serviceDisconnected() {
        setConnected(false);
    }

    public boolean isConnected() {
        return this.obdConnected;
    }

    public boolean isSleeping() {
        com.navdy.obd.ICarService carService = this.carServiceConnector.getCarApi();
        if (carService == null) {
            return false;
        }
        try {
            if (carService.getConnectionState() == 5) {
                return true;
            }
            return false;
        } catch (android.os.RemoteException e) {
            return false;
        }
    }

    /* access modifiers changed from: private */
    public synchronized void onVinRead(java.lang.String vin2) {
        android.content.SharedPreferences sharedPreferences2 = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getSharedPreferences();
        java.lang.String savedVin = sharedPreferences2.getString(VEHICLE_VIN, null);
        if (!android.text.TextUtils.equals(vin2, savedVin)) {
            sLogger.d("Vin changed , Old :" + savedVin + ", New Vin :" + vin2);
            if (android.text.TextUtils.isEmpty(vin2)) {
                sharedPreferences2.edit().remove(VEHICLE_VIN).apply();
            } else {
                sharedPreferences2.edit().putString(VEHICLE_VIN, vin2).apply();
            }
            resetCumulativeMileageData();
        } else {
            sLogger.d("Vin has not changed , Vin " + vin2);
        }
    }

    private void resetCumulativeMileageData() {
        android.content.SharedPreferences preferences = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getSharedPreferences();
        sLogger.d("Resetting the cumulative mileage statistics");
        preferences.edit().putLong(FIRST_ODOMETER_READING_KM, 0).putLong(LAST_ODOMETER_READING_KM, 0).putLong(TOTAL_DISTANCE_TRAVELLED_WITH_NAVDY_METERS, 0).apply();
        this.totalDistanceInCurrentSessionPersisted = 0;
        this.odoMeterReadingValidated = false;
    }

    private void reportMileageEvent() {
        long currentTime = android.os.SystemClock.elapsedRealtime();
        if (this.mileageEventLastReportTime == 0 || currentTime - this.mileageEventLastReportTime > MINIMUM_TIME_BETWEEN_MILEAGE_REPORTING) {
            android.content.SharedPreferences preferences = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getSharedPreferences();
            long firstOdoMeterReadingKms = preferences.getLong(FIRST_ODOMETER_READING_KM, 0);
            long lastOdoMeterReadingKms = preferences.getLong(LAST_ODOMETER_READING_KM, 0);
            double totalDistanceVehicleTravelledInKms = (double) (lastOdoMeterReadingKms - firstOdoMeterReadingKms);
            sLogger.d("Initial reading (KM) : " + firstOdoMeterReadingKms + ", Current reading (KM) : " + lastOdoMeterReadingKms + " , Total (KM) : " + totalDistanceVehicleTravelledInKms);
            double totalDistanceTravelledWithNavdyInMeters = (double) preferences.getLong(TOTAL_DISTANCE_TRAVELLED_WITH_NAVDY_METERS, 0);
            sLogger.d("Distance travelled with Navdy(Meters) : " + totalDistanceTravelledWithNavdyInMeters);
            double totalDistanceTravelledWithNavdyKms = totalDistanceTravelledWithNavdyInMeters / 1000.0d;
            if (totalDistanceVehicleTravelledInKms > 0.0d && totalDistanceTravelledWithNavdyKms > 0.0d) {
                double usageRate = totalDistanceVehicleTravelledInKms / totalDistanceVehicleTravelledInKms;
                sLogger.d("Reporting the Navdy Mileage Vehicle (KMs) : " + totalDistanceVehicleTravelledInKms + " , Navdy (KMs) : " + totalDistanceVehicleTravelledInKms + " , Navdy Usage rate " + usageRate);
                com.navdy.hud.app.analytics.AnalyticsSupport.recordNavdyMileageEvent(totalDistanceVehicleTravelledInKms, totalDistanceTravelledWithNavdyKms, usageRate);
                this.mileageEventLastReportTime = currentTime;
            }
        }
    }

    /* access modifiers changed from: private */
    public synchronized void updateCumulativeVehicleDistanceTravelled(long odoMeterReadingInKms) {
        if (odoMeterReadingInKms > 0) {
            sLogger.d("New odometer reading (KMs) : " + odoMeterReadingInKms);
            android.content.SharedPreferences preferences = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getSharedPreferences();
            if (!this.odoMeterReadingValidated) {
                sLogger.d("Validating the Cumulative mileage data");
                double savedOdoMeterReadingKms = (double) preferences.getLong(LAST_ODOMETER_READING_KM, 0);
                if (savedOdoMeterReadingKms == 0.0d) {
                    sLogger.d("No saved odo meter reading, starting fresh");
                    resetCumulativeMileageData();
                    preferences.edit().putLong(FIRST_ODOMETER_READING_KM, odoMeterReadingInKms).apply();
                } else if (((double) odoMeterReadingInKms) < savedOdoMeterReadingKms) {
                    sLogger.d("Odometer reading mismatch with saved reading, resetting, Saved :" + savedOdoMeterReadingKms + ", New : " + odoMeterReadingInKms);
                    resetCumulativeMileageData();
                    preferences.edit().putLong(FIRST_ODOMETER_READING_KM, odoMeterReadingInKms).apply();
                }
                this.odoMeterReadingValidated = true;
            }
            preferences.edit().putLong(LAST_ODOMETER_READING_KM, odoMeterReadingInKms).apply();
            com.navdy.hud.app.framework.trips.TripManager tripManager2 = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getTripManager();
            long savedDistanceTravelledWithNavdyMeters = preferences.getLong(TOTAL_DISTANCE_TRAVELLED_WITH_NAVDY_METERS, 0);
            long totalDistanceTravelledMeters = (long) tripManager2.getTotalDistanceTravelled();
            sLogger.d("Saving total distance travelled in current session, before :" + savedDistanceTravelledWithNavdyMeters + ", Current Trip mileage : " + tripManager2.getTotalDistanceTravelled());
            long newDistanceAfterLastSaveMeters = totalDistanceTravelledMeters - this.totalDistanceInCurrentSessionPersisted;
            if (newDistanceAfterLastSaveMeters > 0) {
                preferences.edit().putLong(TOTAL_DISTANCE_TRAVELLED_WITH_NAVDY_METERS, savedDistanceTravelledWithNavdyMeters + newDistanceAfterLastSaveMeters).apply();
                this.totalDistanceInCurrentSessionPersisted = totalDistanceTravelledMeters;
                reportMileageEvent();
            }
        }
    }

    /* access modifiers changed from: private */
    public void updateConnectionState(int newState) {
        if (newState == 2) {
            setConnected(true);
        } else {
            setConnected(false);
        }
        if (this.connectionType == com.navdy.hud.app.obd.ObdManager.ConnectionType.POWER_ONLY && newState > 1) {
            this.connectionType = com.navdy.hud.app.obd.ObdManager.ConnectionType.OBD;
        }
    }

    private void setScanSchedule(com.navdy.obd.ScanSchedule schedule2) {
        if (schedule2 != this.schedule) {
            this.schedule = schedule2;
            updateListener();
        }
    }

    @com.squareup.otto.Subscribe
    public void onDrivingStateChange(com.navdy.hud.app.event.DrivingStateChange event) {
        if (event.driving && !isConnected()) {
            com.navdy.obd.ICarService api = this.carServiceConnector.getCarApi();
            if (api != null) {
                try {
                    sLogger.i("Driving started - triggering obd scan");
                    api.rescan();
                } catch (android.os.RemoteException e) {
                    sLogger.e("Failed to trigger OBD rescan");
                }
            }
        }
    }

    public void enableInstantaneousMode(boolean enable) {
        this.obdCanBusRecordingPolicy.onInstantaneousModeChanged(enable);
        if (enable) {
            setScanSchedule(this.fullScan);
        } else if (com.navdy.hud.app.debug.DriveRecorder.isAutoRecordingEnabled()) {
            setScanSchedule(this.fullScan);
        } else {
            setScanSchedule(this.minimalScan);
        }
    }

    public void enableObdScanning(boolean enable) {
        com.navdy.obd.ICarService carService = this.carServiceConnector.getCarApi();
        if (carService != null) {
            try {
                sLogger.i("setting obd scanning enabled:" + enable);
                carService.setObdPidsScanningEnabled(enable);
            } catch (android.os.RemoteException e) {
                sLogger.e("Error while " + (enable ? "enabling " : "disabling ") + "Obd PIDs scanning ", e);
            }
        }
    }

    public void sleep(boolean deep) {
        com.navdy.obd.ICarService carService = this.carServiceConnector.getCarApi();
        if (carService != null) {
            try {
                carService.sleep(deep);
            } catch (android.os.RemoteException e) {
                sLogger.e("Error during call to sleep Remote Exception " + e.getCause());
            }
        }
    }

    public void wakeup() {
        com.navdy.obd.ICarService carService = this.carServiceConnector.getCarApi();
        if (carService != null) {
            try {
                carService.wakeup();
            } catch (android.os.RemoteException e) {
                sLogger.e("Error during call to sleep Remote Exception " + e.getCause());
            }
        }
    }

    @com.squareup.otto.Subscribe
    public void onShutdown(com.navdy.hud.app.event.Shutdown shutdown) {
        if (shutdown.state == com.navdy.hud.app.event.Shutdown.State.SHUTTING_DOWN) {
            this.carServiceConnector.shutdown();
        }
    }

    @com.squareup.otto.Subscribe
    public void onObdStatusRequest(com.navdy.service.library.events.obd.ObdStatusRequest request) {
        int speed = this.speedManager.getObdSpeed();
        this.bus.post(new com.navdy.hud.app.event.RemoteEvent(new com.navdy.service.library.events.obd.ObdStatusResponse(com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS, java.lang.Boolean.valueOf(isConnected()), this.vin, speed != -1 ? java.lang.Integer.valueOf(speed) : null)));
    }

    public int getFuelLevel() {
        if (this.fuelPidCheck.hasIncorrectData || this.fuelPidCheck.isWaitingForValidData()) {
            return -1;
        }
        return (int) getPidValue(47);
    }

    public int getEngineRpm() {
        return (int) getPidValue(12);
    }

    public double getInstantFuelConsumption() {
        return (double) ((int) getPidValue(256));
    }

    public int getDistanceTravelled() {
        return (int) getPidValue(49);
    }

    public long getDistanceTravelledWithNavdy() {
        return com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getSharedPreferences().getLong(TOTAL_DISTANCE_TRAVELLED_WITH_NAVDY_METERS, -1);
    }

    public java.lang.String getVin() {
        return this.vin;
    }

    public java.util.List<com.navdy.obd.ECU> getEcus() {
        return this.ecus;
    }

    public java.lang.String getProtocol() {
        return this.protocol;
    }

    public com.navdy.obd.PidSet getSupportedPids() {
        return this.supportedPidSet;
    }

    public double getPidValue(int pid) {
        if (this.obdPidsCache.containsKey(java.lang.Integer.valueOf(pid))) {
            return ((java.lang.Double) this.obdPidsCache.get(java.lang.Integer.valueOf(pid))).doubleValue();
        }
        return -1.0d;
    }

    public double getBatteryVoltage() {
        com.navdy.obd.ICarService carService = this.carServiceConnector.getCarApi();
        if (carService != null) {
            try {
                this.batteryVoltage = carService.getBatteryVoltage();
            } catch (Throwable t) {
                sLogger.e(t);
            }
        }
        return this.batteryVoltage;
    }

    public double getMinBatteryVoltage() {
        return this.minBatteryVoltage;
    }

    public double getMaxBatteryVoltage() {
        return this.maxBatteryVoltage;
    }

    public void setRecordingPidListener(com.navdy.obd.IPidListener recordingPidListener2) {
        this.recordingPidListener = recordingPidListener2;
    }

    public void injectObdData(java.util.List<com.navdy.obd.Pid> pidsread, java.util.List<com.navdy.obd.Pid> pidschanged) {
        try {
            this.pidListener.pidsRead(pidsread, pidschanged);
        } catch (android.os.RemoteException e) {
            sLogger.e("Remote exception when injecting fake obd data");
        }
    }

    public void setSupportedPidSet(com.navdy.obd.PidSet supportedPids) {
        if (this.firstScan) {
            this.firstScan = true;
            this.supportedPidSet = supportedPids;
        } else if (this.supportedPidSet != null) {
            this.supportedPidSet.merge(supportedPids);
        } else {
            this.supportedPidSet = supportedPids;
        }
        this.bus.post(new com.navdy.hud.app.obd.ObdManager.ObdSupportedPidsChangedEvent());
    }

    public com.navdy.hud.app.obd.ObdDeviceConfigurationManager getObdDeviceConfigurationManager() {
        return this.obdDeviceConfigurationManager;
    }

    @com.squareup.otto.Subscribe
    public void onDriverProfileChanged(com.navdy.hud.app.event.DriverProfileChanged changed) {
        updateObdScanSetting();
    }

    @com.squareup.otto.Subscribe
    public void onDriverProfileUpdated(com.navdy.hud.app.event.DriverProfileUpdated updated) {
        updateObdScanSetting();
    }

    private void updateObdScanSetting() {
        com.navdy.service.library.events.preferences.DriverProfilePreferences.ObdScanSetting savedSetting;
        if (this.driverProfileManager != null) {
            com.navdy.hud.app.profile.DriverProfile profile = this.driverProfileManager.getCurrentProfile();
            if (profile != null) {
                if (!profile.isAutoOnEnabled()) {
                    this.obdDeviceConfigurationManager.setAutoOnEnabled(false);
                } else if (!android.text.TextUtils.isEmpty(profile.getCarMake()) || !android.text.TextUtils.isEmpty(profile.getCarModel()) || !android.text.TextUtils.isEmpty(profile.getCarYear())) {
                    this.obdCanBusRecordingPolicy.onCarDetailsAvailable(profile.getCarMake(), profile.getCarModel(), profile.getCarYear());
                    this.obdDeviceConfigurationManager.setCarDetails(profile.getCarMake(), profile.getCarModel(), profile.getCarYear());
                }
                com.navdy.service.library.events.preferences.DriverProfilePreferences.ObdScanSetting scanSetting = profile.getObdScanSetting();
                sLogger.d("ObdScanSetting received : " + scanSetting);
                if (scanSetting != null) {
                    boolean writeSetting = false;
                    android.content.SharedPreferences preferences = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getSharedPreferences();
                    if (preferences.getBoolean(DEFAULT_SET, false)) {
                        writeSetting = true;
                        savedSetting = isObdScanningEnabled() ? com.navdy.service.library.events.preferences.DriverProfilePreferences.ObdScanSetting.SCAN_DEFAULT_ON : com.navdy.service.library.events.preferences.DriverProfilePreferences.ObdScanSetting.SCAN_DEFAULT_OFF;
                        preferences.edit().remove(DEFAULT_SET).apply();
                    } else {
                        savedSetting = getScanSetting(preferences);
                    }
                    long phoneModificationTime = profile.getObdBlacklistModificationTime();
                    long localModificationTime = preferences.getLong(BLACKLIST_MODIFICATION_TIME, 0);
                    boolean phoneNewer = phoneModificationTime > localModificationTime;
                    sLogger.d("saved setting:" + savedSetting + " blacklist updated: (" + phoneModificationTime + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.SLASH + localModificationTime + ") (phone/local)");
                    sLogger.d("Is Obd enabled :" + isObdScanningEnabled());
                    boolean isDefault = savedSetting == com.navdy.service.library.events.preferences.DriverProfilePreferences.ObdScanSetting.SCAN_DEFAULT_OFF || savedSetting == com.navdy.service.library.events.preferences.DriverProfilePreferences.ObdScanSetting.SCAN_DEFAULT_ON;
                    switch (scanSetting) {
                        case SCAN_DEFAULT_ON:
                            if (phoneNewer && isDefault) {
                                sLogger.d("Setting default to on");
                                enableObdScanning(true);
                                writeSetting = true;
                                break;
                            }
                        case SCAN_ON:
                            enableObdScanning(true);
                            writeSetting = true;
                            break;
                        case SCAN_OFF:
                            enableObdScanning(false);
                            writeSetting = true;
                            break;
                        case SCAN_DEFAULT_OFF:
                            if (phoneNewer && isDefault) {
                                enableObdScanning(false);
                                writeSetting = true;
                                break;
                            }
                    }
                    if (writeSetting) {
                        sLogger.i("Writing scan setting:" + scanSetting + " modTime:" + phoneModificationTime);
                        preferences.edit().putString(SCAN_SETTING, scanSetting.name()).putLong(BLACKLIST_MODIFICATION_TIME, phoneModificationTime).apply();
                    }
                }
            }
        }
    }

    private com.navdy.service.library.events.preferences.DriverProfilePreferences.ObdScanSetting getScanSetting(android.content.SharedPreferences preferences) {
        com.navdy.service.library.events.preferences.DriverProfilePreferences.ObdScanSetting defaultSetting = com.navdy.service.library.events.preferences.DriverProfilePreferences.ObdScanSetting.SCAN_DEFAULT_ON;
        try {
            return com.navdy.service.library.events.preferences.DriverProfilePreferences.ObdScanSetting.valueOf(preferences.getString(SCAN_SETTING, defaultSetting.name()));
        } catch (java.lang.Exception e) {
            sLogger.e("Failed to parse saved scan setting ", e);
            return defaultSetting;
        }
    }

    public boolean isObdScanningEnabled() {
        com.navdy.obd.ICarService carService = this.carServiceConnector.getCarApi();
        if (carService != null) {
            try {
                return carService.isObdPidsScanningEnabled();
            } catch (android.os.RemoteException e) {
                sLogger.e("RemoteException ", e);
            }
        }
        return false;
    }

    public boolean isSpeedPidAvailable() {
        if (this.supportedPidSet != null) {
            return this.supportedPidSet.contains(13);
        }
        return false;
    }

    public void setDriveRecorder(com.navdy.hud.app.debug.DriveRecorder driveRecorder2) {
        this.driveRecorder = driveRecorder2;
    }

    public com.navdy.hud.app.debug.DriveRecorder getDriveRecorder() {
        return this.driveRecorder;
    }

    public void setMode(int mode, boolean persistent) {
        switch (mode) {
            case 0:
            case 1:
                com.navdy.obd.ICarService carService = this.carServiceConnector.getCarApi();
                if (carService != null) {
                    try {
                        carService.setMode(mode, persistent);
                        return;
                    } catch (android.os.RemoteException e) {
                        sLogger.e("RemoteException :" + e);
                        return;
                    }
                } else {
                    return;
                }
            default:
                return;
        }
    }

    public void updateFirmware(com.navdy.hud.app.obd.ObdManager.Firmware firmware) {
        sLogger.d("Updating the firmware of the OBD chip , Version : " + (firmware == com.navdy.hud.app.obd.ObdManager.Firmware.OLD_320 ? "3.2.0" : FIRMWARE_VERSION));
        com.navdy.obd.ICarService carService = this.carServiceConnector.getCarApi();
        java.io.InputStream is = com.navdy.hud.app.HudApplication.getAppContext().getResources().openRawResource(firmware.resource);
        java.lang.String absolutePath = android.os.Environment.getExternalStorageDirectory().getAbsolutePath() + java.io.File.separator + "update.bin";
        java.io.File file = new java.io.File(absolutePath);
        if (file.exists()) {
            com.navdy.service.library.util.IOUtils.deleteFile(com.navdy.hud.app.HudApplication.getAppContext(), file.getAbsolutePath());
        }
        try {
            com.navdy.service.library.util.IOUtils.copyFile(file.getAbsolutePath(), is);
            try {
                carService.updateFirmware(FIRMWARE_VERSION, absolutePath);
            } catch (android.os.RemoteException e) {
                sLogger.e("Remote Exception while updating firmware ", e);
            }
        } catch (java.io.IOException e2) {
            sLogger.e("Error writing to the file ", e2);
        }
    }

    public com.navdy.hud.app.obd.ObdCanBusRecordingPolicy getObdCanBusRecordingPolicy() {
        return this.obdCanBusRecordingPolicy;
    }

    public boolean isCheckEngineLightOn() {
        return this.isCheckEngineLightOn;
    }

    public java.util.List<java.lang.String> getTroubleCodes() {
        return this.troubleCodes;
    }
}
