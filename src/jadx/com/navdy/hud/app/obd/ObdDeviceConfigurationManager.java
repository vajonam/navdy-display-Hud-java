package com.navdy.hud.app.obd;

public class ObdDeviceConfigurationManager {
    public static final java.lang.String ASSETS_FOLDER_PREFIX = "stn_obd";
    public static final java.lang.String CAR_CONFIGURATION_MAPPING_FILE = "configuration_mapping.csv";
    /* access modifiers changed from: private */
    public static com.navdy.hud.app.obd.ObdDeviceConfigurationManager.Configuration[] CAR_DETAILS_CONFIGURATION_MAPPING = null;
    public static final java.lang.String CONFIGURATION_FILE_EXTENSION = "conf";
    public static final java.lang.String CONFIGURATION_NAME_PREFIX = "Navdy OBD-II ";
    public static final java.lang.String DEBUG_CONFIGURATION = "debug";
    public static final java.lang.String DEFAULT_OFF_CONFIGURATION = "no_trigger";
    public static final java.lang.String DEFAULT_ON_CONFIGURATION = "default_on";
    public static final java.lang.String STSATI = "STSATI";
    /* access modifiers changed from: private */
    public static com.navdy.hud.app.obd.ObdDeviceConfigurationManager.Configuration[] VIN_CONFIGURATION_MAPPING = null;
    public static final java.lang.String VIN_CONFIGURATION_MAPPING_FILE = "vin_configuration_mapping.csv";
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.obd.ObdDeviceConfigurationManager.class);
    private com.navdy.hud.app.obd.CarServiceConnector mCarServiceConnector;
    private volatile boolean mConnected;
    private java.lang.String mExpectedConfigurationFileName = null;
    /* access modifiers changed from: private */
    public volatile boolean mIsAutoOnEnabled = true;
    /* access modifiers changed from: private */
    public volatile boolean mIsVinRecognized;
    /* access modifiers changed from: private */
    public volatile java.lang.String mLastKnowVinNumber;

    class Anon1 implements java.lang.Runnable {
        final /* synthetic */ java.lang.String val$configurationFileName;

        Anon1(java.lang.String str) {
            this.val$configurationFileName = str;
        }

        public void run() {
            com.navdy.hud.app.obd.ObdDeviceConfigurationManager.this.bSetConfigurationFile(this.val$configurationFileName);
        }
    }

    class Anon2 implements java.lang.Runnable {
        final /* synthetic */ java.lang.String val$make;
        final /* synthetic */ java.lang.String val$model;
        final /* synthetic */ java.lang.String val$year;

        Anon2(java.lang.String str, java.lang.String str2, java.lang.String str3) {
            this.val$make = str;
            this.val$model = str2;
            this.val$year = str3;
        }

        public void run() {
            if (!com.navdy.hud.app.obd.ObdDeviceConfigurationManager.this.mIsAutoOnEnabled) {
                com.navdy.hud.app.obd.ObdDeviceConfigurationManager.sLogger.d("setCarDetails: Auto on is de selected by the user , so skipping the overwriting of configuration");
            } else if (com.navdy.hud.app.obd.ObdDeviceConfigurationManager.this.mIsVinRecognized) {
                com.navdy.hud.app.obd.ObdDeviceConfigurationManager.sLogger.d("setCarDetails: Vin is recognized and it is used as the preferred source");
            } else {
                java.lang.String name = (android.text.TextUtils.isEmpty(this.val$make) ? "*" : this.val$make) + com.navdy.service.library.util.MusicDataUtils.ALTERNATE_SEPARATOR + (android.text.TextUtils.isEmpty(this.val$model) ? "*" : this.val$model) + com.navdy.service.library.util.MusicDataUtils.ALTERNATE_SEPARATOR + (android.text.TextUtils.isEmpty(this.val$year) ? "*" : this.val$year);
                com.navdy.hud.app.obd.ObdDeviceConfigurationManager.sLogger.d("Vehicle name :" + name);
                if (com.navdy.hud.app.obd.ObdDeviceConfigurationManager.CAR_DETAILS_CONFIGURATION_MAPPING == null) {
                    com.navdy.hud.app.obd.ObdDeviceConfigurationManager.CAR_DETAILS_CONFIGURATION_MAPPING = com.navdy.hud.app.obd.ObdDeviceConfigurationManager.this.loadConfigurationMappingList(com.navdy.hud.app.obd.ObdDeviceConfigurationManager.CAR_CONFIGURATION_MAPPING_FILE);
                }
                com.navdy.hud.app.obd.ObdDeviceConfigurationManager.Configuration matchingConfiguration = com.navdy.hud.app.obd.ObdDeviceConfigurationManager.pickConfiguration(com.navdy.hud.app.obd.ObdDeviceConfigurationManager.CAR_DETAILS_CONFIGURATION_MAPPING, name);
                if (matchingConfiguration != null) {
                    com.navdy.hud.app.obd.ObdDeviceConfigurationManager.this.bSetConfigurationFile(matchingConfiguration.configurationName);
                }
            }
        }
    }

    class Anon3 implements java.lang.Runnable {
        Anon3() {
        }

        public void run() {
            if (com.navdy.hud.app.obd.ObdDeviceConfigurationManager.isValidVin(com.navdy.hud.app.obd.ObdDeviceConfigurationManager.this.mLastKnowVinNumber) && com.navdy.hud.app.obd.ObdDeviceConfigurationManager.this.mIsAutoOnEnabled) {
                if (com.navdy.hud.app.obd.ObdDeviceConfigurationManager.VIN_CONFIGURATION_MAPPING == null) {
                    com.navdy.hud.app.obd.ObdDeviceConfigurationManager.VIN_CONFIGURATION_MAPPING = com.navdy.hud.app.obd.ObdDeviceConfigurationManager.this.loadConfigurationMappingList(com.navdy.hud.app.obd.ObdDeviceConfigurationManager.VIN_CONFIGURATION_MAPPING_FILE);
                }
                com.navdy.hud.app.obd.ObdDeviceConfigurationManager.Configuration matchingConfiguration = com.navdy.hud.app.obd.ObdDeviceConfigurationManager.pickConfiguration(com.navdy.hud.app.obd.ObdDeviceConfigurationManager.VIN_CONFIGURATION_MAPPING, com.navdy.hud.app.obd.ObdDeviceConfigurationManager.this.mLastKnowVinNumber);
                if (matchingConfiguration != null) {
                    com.navdy.hud.app.obd.ObdDeviceConfigurationManager.sLogger.d("decodeVin : got the matching configuration :" + matchingConfiguration.configurationName + ", Vin :" + com.navdy.hud.app.obd.ObdDeviceConfigurationManager.this.mLastKnowVinNumber);
                    com.navdy.hud.app.obd.ObdDeviceConfigurationManager.this.mIsVinRecognized = true;
                    com.navdy.hud.app.obd.ObdDeviceConfigurationManager.this.bSetConfigurationFile(matchingConfiguration.configurationName);
                    return;
                }
                com.navdy.hud.app.obd.ObdDeviceConfigurationManager.this.mIsVinRecognized = false;
            }
        }
    }

    static class Configuration {
        java.lang.String configurationName;
        java.util.regex.Pattern pattern;

        Configuration() {
        }
    }

    static class OBDConfiguration {
        java.lang.String configurationData;
        java.lang.String configurationIdentifier;

        public OBDConfiguration(java.lang.String configurationData2, java.lang.String configurationIdentifier2) {
            this.configurationData = configurationData2;
            this.configurationIdentifier = configurationIdentifier2;
        }
    }

    public ObdDeviceConfigurationManager(com.navdy.hud.app.obd.CarServiceConnector mCarServiceConnector2) {
        this.mCarServiceConnector = mCarServiceConnector2;
    }

    public void setConnectionState(boolean connected) {
        if (connected != this.mConnected) {
            this.mConnected = connected;
            sLogger.d("OBD Connection state changed");
            if (this.mConnected) {
                try {
                    this.mLastKnowVinNumber = this.mCarServiceConnector.getCarApi().getVIN();
                    sLogger.d("Vin number read :" + this.mLastKnowVinNumber);
                    decodeVin();
                } catch (android.os.RemoteException e) {
                    sLogger.e("Remote exception while getting the VIN number ");
                }
            }
        } else {
            this.mLastKnowVinNumber = "";
            this.mIsAutoOnEnabled = true;
            this.mIsVinRecognized = false;
        }
    }

    private void syncConfiguration() {
        if (this.mConnected && !android.text.TextUtils.isEmpty(this.mExpectedConfigurationFileName)) {
            com.navdy.obd.ICarService api = this.mCarServiceConnector.getCarApi();
            com.navdy.hud.app.obd.ObdDeviceConfigurationManager.OBDConfiguration configuration = readConfiguration(this.mExpectedConfigurationFileName);
            try {
                java.lang.String configurationName = api.getCurrentConfigurationName();
                java.lang.String expectedConfigurationName = configuration.configurationIdentifier;
                if (expectedConfigurationName.equals(configurationName)) {
                    sLogger.d("The configuration on the obd chip is as expected");
                    return;
                }
                sLogger.d("The configuration on the obd chip is different");
                sLogger.d("Expected : " + expectedConfigurationName);
                sLogger.d("Actual : " + configurationName);
                sLogger.d("Syncing configuration...");
                applyConfiguration(configuration);
            } catch (android.os.RemoteException re) {
                sLogger.e("Error while applying configuration", re);
            }
        }
    }

    private void applyConfiguration(com.navdy.hud.app.obd.ObdDeviceConfigurationManager.OBDConfiguration configuration) {
        if (configuration != null && this.mConnected) {
            com.navdy.obd.ICarService api = this.mCarServiceConnector.getCarApi();
            if (!android.text.TextUtils.isEmpty(configuration.configurationData) && !android.text.TextUtils.isEmpty(configuration.configurationIdentifier)) {
                sLogger.d("Writing new configuration :" + configuration);
                try {
                    boolean applied = api.applyConfiguration(configuration.configurationData);
                    sLogger.d("New Configuration applied : " + applied + ", configuration : " + api.getCurrentConfigurationName());
                } catch (android.os.RemoteException re) {
                    sLogger.e("Failed to apply the configuration :", re.getCause());
                }
            }
        }
    }

    private com.navdy.hud.app.obd.ObdDeviceConfigurationManager.OBDConfiguration readConfiguration(java.lang.String name) {
        try {
            return extractOBDConfiguration(readObdAssetFile(name + com.navdy.hud.app.framework.glance.GlanceConstants.PERIOD + CONFIGURATION_FILE_EXTENSION));
        } catch (java.io.IOException e) {
            sLogger.e("Error reading the configuration file :" + name);
            return null;
        }
    }

    static com.navdy.hud.app.obd.ObdDeviceConfigurationManager.OBDConfiguration extractOBDConfiguration(java.lang.String fileContent) {
        java.lang.String[] lines;
        java.lang.StringBuilder machineFriendlyConfiguration = new java.lang.StringBuilder();
        java.lang.String configurationIdentifier = null;
        for (java.lang.String line : fileContent.split(com.navdy.hud.app.framework.glance.GlanceConstants.NEWLINE)) {
            if (line.startsWith("ST") || line.startsWith("AT")) {
                machineFriendlyConfiguration.append(line).append(com.navdy.hud.app.framework.glance.GlanceConstants.NEWLINE);
                if (line.startsWith(STSATI)) {
                    configurationIdentifier = line.substring(STSATI.length()).trim();
                }
            }
        }
        return new com.navdy.hud.app.obd.ObdDeviceConfigurationManager.OBDConfiguration(machineFriendlyConfiguration.toString().trim(), configurationIdentifier);
    }

    private java.lang.String readObdAssetFile(java.lang.String assetFileName) throws java.io.IOException {
        java.io.InputStream is = com.navdy.hud.app.HudApplication.getAppContext().getAssets().open("stn_obd/" + assetFileName);
        java.lang.String content = com.navdy.service.library.util.IOUtils.convertInputStreamToString(is, "UTF-8");
        try {
            is.close();
        } catch (java.io.IOException e) {
            sLogger.d("Error closing the stream after reading the asset file");
        }
        return content;
    }

    /* access modifiers changed from: private */
    public void bSetConfigurationFile(java.lang.String configurationName) {
        if (!validateConfigurationName(configurationName)) {
            sLogger.d("Received an invalid configuration name : " + configurationName);
            throw new java.lang.IllegalArgumentException("Configuration does not exists");
        }
        this.mExpectedConfigurationFileName = configurationName;
        syncConfiguration();
    }

    public void setConfigurationFile(java.lang.String configurationFileName) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.obd.ObdDeviceConfigurationManager.Anon1(configurationFileName), 13);
    }

    private boolean validateConfigurationName(java.lang.String configurationName) {
        if (android.text.TextUtils.isEmpty(configurationName)) {
            return false;
        }
        try {
            java.io.InputStream is = com.navdy.hud.app.HudApplication.getAppContext().getAssets().open("stn_obd/" + configurationName + com.navdy.hud.app.framework.glance.GlanceConstants.PERIOD + CONFIGURATION_FILE_EXTENSION);
            if (is == null) {
                return false;
            }
            try {
                is.close();
            } catch (java.io.IOException e) {
            }
            return true;
        } catch (java.io.IOException e2) {
            return false;
        }
    }

    public void setCarDetails(java.lang.String make, java.lang.String model, java.lang.String year) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.obd.ObdDeviceConfigurationManager.Anon2(make, model, year), 13);
    }

    /* access modifiers changed from: private */
    public com.navdy.hud.app.obd.ObdDeviceConfigurationManager.Configuration[] loadConfigurationMappingList(java.lang.String fileName) {
        int i = 0;
        com.navdy.hud.app.obd.ObdDeviceConfigurationManager.Configuration[] configurationsList = null;
        try {
            java.lang.String[] configurationMappingEntries = readObdAssetFile(fileName).split(com.navdy.hud.app.framework.glance.GlanceConstants.NEWLINE);
            configurationsList = new com.navdy.hud.app.obd.ObdDeviceConfigurationManager.Configuration[configurationMappingEntries.length];
            int length = configurationMappingEntries.length;
            int i2 = 0;
            while (i < length) {
                java.lang.String[] parts = configurationMappingEntries[i].split(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA);
                com.navdy.hud.app.obd.ObdDeviceConfigurationManager.Configuration configuration = new com.navdy.hud.app.obd.ObdDeviceConfigurationManager.Configuration();
                configuration.pattern = java.util.regex.Pattern.compile(parts[0], 2);
                configuration.configurationName = parts[1];
                int i3 = i2 + 1;
                configurationsList[i2] = configuration;
                i++;
                i2 = i3;
            }
        } catch (java.io.IOException e) {
            sLogger.d("Failed to load the configuration mapping file");
        }
        return configurationsList;
    }

    public static com.navdy.hud.app.obd.ObdDeviceConfigurationManager.Configuration pickConfiguration(com.navdy.hud.app.obd.ObdDeviceConfigurationManager.Configuration[] configurationMapping, java.lang.String expression) {
        for (com.navdy.hud.app.obd.ObdDeviceConfigurationManager.Configuration configuration : configurationMapping) {
            if (configuration.pattern.matcher(expression).matches()) {
                return configuration;
            }
        }
        return null;
    }

    public void setDecodedCarDetails(com.navdy.hud.app.obd.CarDetails decodedCarDetails) {
    }

    private void decodeVin() {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.obd.ObdDeviceConfigurationManager.Anon3(), 13);
    }

    public void setAutoOnEnabled(boolean enabled) {
        this.mIsAutoOnEnabled = enabled;
        if (!this.mIsAutoOnEnabled) {
            setConfigurationFile(DEFAULT_OFF_CONFIGURATION);
        }
    }

    public boolean isAutoOnEnabled() {
        return this.mIsAutoOnEnabled;
    }

    public static boolean isValidVin(java.lang.String vinNumber) {
        return !android.text.TextUtils.isEmpty(vinNumber) && vinNumber.length() == 17;
    }

    public static void turnOffOBDSleep() {
        com.navdy.hud.app.obd.ObdManager.getInstance().getObdDeviceConfigurationManager().setConfigurationFile("debug");
    }
}
