package com.navdy.hud.app.obd;

public class CarMDVinDecoder {
    public static final int APP_VERSION = 1;
    /* access modifiers changed from: private */
    public static final okhttp3.HttpUrl CAR_MD_API_URL = okhttp3.HttpUrl.parse("https://api2.carmd.com/v2.0/decode");
    private static final java.lang.String DATA = "data";
    public static final long MAX_SIZE = 5000;
    private static final java.lang.String META_CAR_MD_AUTH = "CAR_MD_AUTH";
    private static final java.lang.String META_CAR_MD_TOKEN = "CAR_MD_TOKEN";
    public static final java.lang.String VIN = "vin";
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.obd.CarMDVinDecoder.class);
    /* access modifiers changed from: private */
    public final java.lang.String CAR_MD_AUTH;
    /* access modifiers changed from: private */
    public final java.lang.String CAR_MD_TOKEN;
    /* access modifiers changed from: private */
    public volatile boolean mDecoding = false;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.obd.ObdDeviceConfigurationManager mDeviceConfigurationManager;
    private okhttp3.internal.DiskLruCache mDiskLruCache;
    /* access modifiers changed from: private */
    public okhttp3.OkHttpClient mHttpClient;
    @javax.inject.Inject
    com.navdy.service.library.network.http.IHttpManager mHttpManager;

    class Anon1 implements java.lang.Runnable {
        final /* synthetic */ java.lang.String val$vinNumber;

        /* renamed from: com.navdy.hud.app.obd.CarMDVinDecoder$Anon1$Anon1 reason: collision with other inner class name */
        class C0029Anon1 implements okhttp3.Callback {
            C0029Anon1() {
            }

            public void onFailure(okhttp3.Call call, java.io.IOException e) {
                com.navdy.hud.app.obd.CarMDVinDecoder.sLogger.e("onFailure : Decoding failed ", e);
                com.navdy.hud.app.obd.CarMDVinDecoder.this.mDecoding = false;
            }

            public void onResponse(okhttp3.Call call, okhttp3.Response response) throws java.io.IOException {
                com.navdy.hud.app.obd.CarMDVinDecoder.sLogger.d("onResponse : Decoding response received");
                com.navdy.hud.app.obd.CarMDVinDecoder.this.parseResponse(response.body());
                com.navdy.hud.app.obd.CarMDVinDecoder.this.mDecoding = false;
            }
        }

        Anon1(java.lang.String str) {
            this.val$vinNumber = str;
        }

        public void run() {
            try {
                java.lang.String data = com.navdy.hud.app.obd.CarMDVinDecoder.this.getFromCache(this.val$vinNumber);
                if (!android.text.TextUtils.isEmpty(data)) {
                    com.navdy.hud.app.obd.CarMDVinDecoder.sLogger.d("Entry found in the cache for the vinNumber :" + this.val$vinNumber + ", Data :" + data);
                    com.navdy.hud.app.obd.CarDetails details = com.navdy.hud.app.obd.CarDetails.fromJson(data);
                    if (details != null) {
                        com.navdy.hud.app.obd.CarMDVinDecoder.this.mDeviceConfigurationManager.setDecodedCarDetails(details);
                        com.navdy.hud.app.obd.CarMDVinDecoder.this.mDecoding = false;
                        return;
                    }
                    com.navdy.hud.app.obd.CarMDVinDecoder.sLogger.d("Error parsing the CarMD response to CarDetails");
                }
                if (!com.navdy.hud.app.obd.CarMDVinDecoder.this.isConnected()) {
                    com.navdy.hud.app.obd.CarMDVinDecoder.this.mDecoding = false;
                    return;
                }
                okhttp3.HttpUrl.Builder builder = com.navdy.hud.app.obd.CarMDVinDecoder.CAR_MD_API_URL.newBuilder();
                builder.addQueryParameter("vin", this.val$vinNumber);
                okhttp3.Request.Builder requestBuilder = new okhttp3.Request.Builder().url(builder.build());
                if (!android.text.TextUtils.isEmpty(com.navdy.hud.app.obd.CarMDVinDecoder.this.CAR_MD_AUTH)) {
                    requestBuilder.addHeader("authorization", "Basic " + com.navdy.hud.app.obd.CarMDVinDecoder.this.CAR_MD_AUTH);
                    if (!android.text.TextUtils.isEmpty(com.navdy.hud.app.obd.CarMDVinDecoder.this.CAR_MD_TOKEN)) {
                        requestBuilder.addHeader("partner-token", com.navdy.hud.app.obd.CarMDVinDecoder.this.CAR_MD_TOKEN);
                        com.navdy.hud.app.obd.CarMDVinDecoder.this.mHttpClient.newCall(requestBuilder.build()).enqueue(new com.navdy.hud.app.obd.CarMDVinDecoder.Anon1.C0029Anon1());
                        return;
                    }
                    com.navdy.hud.app.obd.CarMDVinDecoder.sLogger.e("Missing car md token credential !");
                    com.navdy.hud.app.obd.CarMDVinDecoder.this.mDecoding = false;
                    return;
                }
                com.navdy.hud.app.obd.CarMDVinDecoder.sLogger.e("Missing car md auth credential !");
                com.navdy.hud.app.obd.CarMDVinDecoder.this.mDecoding = false;
            } catch (java.lang.Exception e) {
                com.navdy.hud.app.obd.CarMDVinDecoder.sLogger.e("Exception while fetching the vin from CarMD");
                com.navdy.hud.app.obd.CarMDVinDecoder.this.mDecoding = false;
            }
        }
    }

    public CarMDVinDecoder(com.navdy.hud.app.obd.ObdDeviceConfigurationManager obdDeviceConfigurationManager) {
        mortar.Mortar.inject(com.navdy.hud.app.HudApplication.getAppContext(), this);
        this.mDiskLruCache = okhttp3.internal.DiskLruCache.create(okhttp3.internal.io.FileSystem.SYSTEM, new java.io.File(com.navdy.hud.app.storage.PathManager.getInstance().getCarMdResponseDiskCacheFolder()), 1, 1, 5000);
        this.mDeviceConfigurationManager = obdDeviceConfigurationManager;
        this.mHttpClient = this.mHttpManager.getClientCopy().readTimeout(120, java.util.concurrent.TimeUnit.SECONDS).connectTimeout(120, java.util.concurrent.TimeUnit.SECONDS).build();
        this.CAR_MD_AUTH = com.navdy.service.library.util.CredentialUtil.getCredentials(com.navdy.hud.app.HudApplication.getAppContext(), META_CAR_MD_AUTH);
        this.CAR_MD_TOKEN = com.navdy.service.library.util.CredentialUtil.getCredentials(com.navdy.hud.app.HudApplication.getAppContext(), META_CAR_MD_TOKEN);
    }

    public synchronized void decodeVin(java.lang.String vinNumber) {
        if (!this.mDecoding) {
            this.mDecoding = true;
            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.obd.CarMDVinDecoder.Anon1(vinNumber), 1);
        }
    }

    /* access modifiers changed from: private */
    public void parseResponse(okhttp3.ResponseBody responseBody) {
        if (responseBody != null) {
            try {
                org.json.JSONObject dataObject = new org.json.JSONObject(responseBody.string()).getJSONObject(DATA);
                if (dataObject != null) {
                    java.lang.String dataString = dataObject.toString();
                    com.navdy.hud.app.obd.CarDetails details = com.navdy.hud.app.obd.CarDetails.fromJson(dataString);
                    if (details != null) {
                        putToCache(details.vin, dataString);
                        this.mDeviceConfigurationManager.setDecodedCarDetails(details);
                        return;
                    }
                    sLogger.e("Error parsing the CarMD response to car details");
                }
            } catch (org.json.JSONException e) {
                sLogger.e("Error parsing the response body ", e);
            } catch (java.io.IOException e2) {
                sLogger.e("IO Error parsing the response body ", e2);
            }
        }
    }

    /* access modifiers changed from: private */
    public java.lang.String getFromCache(java.lang.String vinNumber) {
        try {
            okhttp3.internal.DiskLruCache.Snapshot snapShot = this.mDiskLruCache.get(vinNumber.toLowerCase());
            if (snapShot == null) {
                return null;
            }
            okio.BufferedSource bufferedSource = okio.Okio.buffer(snapShot.getSource(0));
            java.lang.String readUtf8 = bufferedSource.readUtf8();
            bufferedSource.close();
            return readUtf8;
        } catch (java.io.IOException e) {
            sLogger.d("Exception while retrieving response from cache", e);
            return null;
        }
    }

    private void putToCache(java.lang.String vinNumber, java.lang.String dataString) {
        try {
            okhttp3.internal.DiskLruCache.Editor editor = this.mDiskLruCache.edit(vinNumber.toLowerCase());
            okio.BufferedSink bufferedSink = okio.Okio.buffer(editor.newSink(0));
            bufferedSink.writeUtf8(dataString);
            bufferedSink.close();
            editor.commit();
        } catch (java.io.IOException e) {
            sLogger.e("Exception while putting into cache ", e);
        }
    }

    /* access modifiers changed from: private */
    public boolean isConnected() {
        return com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().isRemoteDeviceConnected() && com.navdy.hud.app.framework.network.NetworkStateManager.isConnectedToNetwork(com.navdy.hud.app.HudApplication.getAppContext());
    }
}
