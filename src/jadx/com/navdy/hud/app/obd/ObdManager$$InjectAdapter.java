package com.navdy.hud.app.obd;

public final class ObdManager$$InjectAdapter extends dagger.internal.Binding<com.navdy.hud.app.obd.ObdManager> implements dagger.MembersInjector<com.navdy.hud.app.obd.ObdManager> {
    private dagger.internal.Binding<com.squareup.otto.Bus> bus;
    private dagger.internal.Binding<com.navdy.hud.app.profile.DriverProfileManager> driverProfileManager;
    private dagger.internal.Binding<com.navdy.hud.app.device.PowerManager> powerManager;
    private dagger.internal.Binding<android.content.SharedPreferences> sharedPreferences;
    private dagger.internal.Binding<com.navdy.hud.app.analytics.TelemetryDataManager> telemetryDataManager;
    private dagger.internal.Binding<com.navdy.hud.app.framework.trips.TripManager> tripManager;

    public ObdManager$$InjectAdapter() {
        super(null, "members/com.navdy.hud.app.obd.ObdManager", false, com.navdy.hud.app.obd.ObdManager.class);
    }

    public void attach(dagger.internal.Linker linker) {
        this.bus = linker.requestBinding("com.squareup.otto.Bus", com.navdy.hud.app.obd.ObdManager.class, getClass().getClassLoader());
        this.driverProfileManager = linker.requestBinding("com.navdy.hud.app.profile.DriverProfileManager", com.navdy.hud.app.obd.ObdManager.class, getClass().getClassLoader());
        this.powerManager = linker.requestBinding("com.navdy.hud.app.device.PowerManager", com.navdy.hud.app.obd.ObdManager.class, getClass().getClassLoader());
        this.tripManager = linker.requestBinding("com.navdy.hud.app.framework.trips.TripManager", com.navdy.hud.app.obd.ObdManager.class, getClass().getClassLoader());
        this.sharedPreferences = linker.requestBinding("android.content.SharedPreferences", com.navdy.hud.app.obd.ObdManager.class, getClass().getClassLoader());
        this.telemetryDataManager = linker.requestBinding("com.navdy.hud.app.analytics.TelemetryDataManager", com.navdy.hud.app.obd.ObdManager.class, getClass().getClassLoader());
    }

    public void getDependencies(java.util.Set<dagger.internal.Binding<?>> set, java.util.Set<dagger.internal.Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.bus);
        injectMembersBindings.add(this.driverProfileManager);
        injectMembersBindings.add(this.powerManager);
        injectMembersBindings.add(this.tripManager);
        injectMembersBindings.add(this.sharedPreferences);
        injectMembersBindings.add(this.telemetryDataManager);
    }

    public void injectMembers(com.navdy.hud.app.obd.ObdManager object) {
        object.bus = (com.squareup.otto.Bus) this.bus.get();
        object.driverProfileManager = (com.navdy.hud.app.profile.DriverProfileManager) this.driverProfileManager.get();
        object.powerManager = (com.navdy.hud.app.device.PowerManager) this.powerManager.get();
        object.tripManager = (com.navdy.hud.app.framework.trips.TripManager) this.tripManager.get();
        object.sharedPreferences = (android.content.SharedPreferences) this.sharedPreferences.get();
        object.telemetryDataManager = (com.navdy.hud.app.analytics.TelemetryDataManager) this.telemetryDataManager.get();
    }
}
