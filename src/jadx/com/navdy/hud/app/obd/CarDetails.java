package com.navdy.hud.app.obd;

public class CarDetails {
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.obd.CarDetails.class);
    java.lang.String aaia;
    java.lang.String engine;
    java.lang.String engineType;
    java.lang.String make;
    java.lang.String model;
    java.lang.String vin;
    java.lang.String year;

    public static boolean matches(com.navdy.hud.app.obd.CarDetails details, java.lang.String vinNumber) {
        return com.navdy.hud.app.obd.ObdDeviceConfigurationManager.isValidVin(vinNumber) && details != null && android.text.TextUtils.equals(details.vin, vinNumber);
    }

    public static com.navdy.hud.app.obd.CarDetails fromJson(java.lang.String data) {
        try {
            return (com.navdy.hud.app.obd.CarDetails) new com.google.gson.Gson().fromJson(data.toString(), com.navdy.hud.app.obd.CarDetails.class);
        } catch (com.google.gson.JsonSyntaxException se) {
            sLogger.e("Exception while parsing the car details response from CarMD ", se);
            return null;
        }
    }
}
