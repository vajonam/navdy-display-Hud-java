package com.navdy.hud.app.obd;

public class CarServiceConnector extends com.navdy.hud.app.common.ServiceReconnector {
    private com.navdy.obd.ICarService carApi;

    public CarServiceConnector(android.content.Context context) {
        super(context, new android.content.Intent(com.navdy.obd.ObdServiceInterface.ACTION_START_AUTO_CONNECT), com.navdy.obd.ICarService.class.getName());
    }

    /* access modifiers changed from: protected */
    public void onConnected(android.content.ComponentName name, android.os.IBinder service) {
        this.carApi = com.navdy.obd.ICarService.Stub.asInterface(service);
        com.navdy.hud.app.obd.ObdManager.getInstance().serviceConnected();
    }

    /* access modifiers changed from: protected */
    public void onDisconnected(android.content.ComponentName name) {
        this.carApi = null;
        com.navdy.hud.app.obd.ObdManager.getInstance().serviceDisconnected();
    }

    public com.navdy.obd.ICarService getCarApi() {
        return this.carApi;
    }
}
