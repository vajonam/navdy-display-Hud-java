package com.navdy.hud.app.obd;

public class ObdCanBusRecordingPolicy {
    private static final long CAN_BUS_MONITORING_DISTANCE_LIMIT_METERS = 40234;
    public static final java.lang.String CAN_PROTOCOL_PREFIX = "ISO 15765-4";
    public static final java.lang.String FILES_TO_UPLOAD_DIRECTORY = "/sdcard/.canBusLogs/upload";
    public static final int MAX_META_DATA_FILES = 10;
    public static final int MAX_UPLOAD_FILES = 5;
    public static final java.lang.String META_DATA_FILE = "/sdcard/.canBusLogs/upload/meta_data_";
    public static final int MINIMUM_TIME_FOR_MOTION_DETECTION = 10000;
    public static final int MINIMUM_TIME_FOR_STOP_DETECTION = 10000;
    private static final float MOVING_SPEED_METERS_PER_SECOND = 1.34112f;
    public static final java.lang.String PREFERENCE_NAVDY_MILES_WHEN_LISTENING_STARTED = "preference_navdy_miles_when_listening_started";
    public static final java.lang.String PREF_CAN_BUS_DATA_RECORDED_AND_SENT = "canBusDataRecordedAndSent";
    public static final java.lang.String PREF_CAN_BUS_DATA_SENT_VERSION = "canBusDataSentVersion";
    public static final java.lang.String PREF_CAN_BUS_DATA_STATUS_REPORTED_VERSION = "canBusDataStatusReportedVersion";
    private static final java.io.File UPLOAD_DIRECTORY_FILE = new java.io.File(FILES_TO_UPLOAD_DIRECTORY);
    private static final java.text.SimpleDateFormat dateFormat = new java.text.SimpleDateFormat("yyyy_MM_dd_HH_mm_ss_SSS");
    private static java.util.concurrent.PriorityBlockingQueue<java.io.File> metaDataFiles = new java.util.concurrent.PriorityBlockingQueue<>(10, new com.navdy.hud.app.util.CrashReportService.FilesModifiedTimeComparator());
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.obd.ObdCanBusRecordingPolicy.class);
    private static java.util.concurrent.PriorityBlockingQueue<java.io.File> uploadFiles = new java.util.concurrent.PriorityBlockingQueue<>(10, new com.navdy.hud.app.util.CrashReportService.FilesModifiedTimeComparator());
    private int canBusMonitoringDataSize;
    private com.navdy.hud.app.obd.ObdCanBusRecordingPolicy.CanBusMonitoringState canBusMonitoringState;
    private android.content.Context context;
    private java.io.File currentMetaDataFile;
    private long firstTimeMovementDetected;
    private long firstTimeStoppingDetected;
    private android.os.Handler handler;
    private boolean isCanBusMonitoringLimitReached;
    private boolean isCanProtocol;
    private boolean isEngineeringBuild;
    private boolean isInstantaneousModeOn;
    private boolean isObdConnected;
    int lastSpeed;
    private java.lang.String make;
    private java.lang.String model;
    private boolean motionDetected;
    private com.navdy.hud.app.obd.ObdManager obdManager;
    private int requiredObdDataVersion;
    private android.content.SharedPreferences sharedPreferences;
    private com.navdy.hud.app.framework.trips.TripManager tripManager;
    private java.lang.String vin;
    private java.lang.String year;

    enum CanBusMonitoringState {
        UNKNOWN,
        SUCCESS,
        FAILURE
    }

    public ObdCanBusRecordingPolicy(android.content.Context context2, android.content.SharedPreferences sharedPreferences2, com.navdy.hud.app.obd.ObdManager obdManager2, com.navdy.hud.app.framework.trips.TripManager tripManager2) {
        this.isEngineeringBuild = !com.navdy.hud.app.util.DeviceUtil.isUserBuild();
        this.firstTimeMovementDetected = 0;
        this.firstTimeStoppingDetected = 0;
        this.isObdConnected = false;
        this.isCanProtocol = false;
        this.isInstantaneousModeOn = false;
        this.motionDetected = false;
        this.lastSpeed = -1;
        this.isCanBusMonitoringLimitReached = false;
        this.canBusMonitoringState = com.navdy.hud.app.obd.ObdCanBusRecordingPolicy.CanBusMonitoringState.UNKNOWN;
        this.canBusMonitoringDataSize = 0;
        this.obdManager = obdManager2;
        this.context = context2;
        this.tripManager = tripManager2;
        this.sharedPreferences = sharedPreferences2;
        this.requiredObdDataVersion = context2.getResources().getInteger(com.navdy.hud.app.R.integer.obd_data_version);
        if (!UPLOAD_DIRECTORY_FILE.exists()) {
            UPLOAD_DIRECTORY_FILE.mkdirs();
        }
        populateFilesQueue();
        this.handler = new android.os.Handler();
    }

    public synchronized void onObdConnectionStateChanged(boolean connected) {
        java.io.BufferedWriter bw;
        sLogger.d("onObdConnectionStateChanged , Connected : " + connected);
        this.isObdConnected = connected;
        if (connected) {
            java.lang.String protocol = this.obdManager.getProtocol();
            if (android.text.TextUtils.isEmpty(protocol) || !protocol.startsWith(CAN_PROTOCOL_PREFIX)) {
                this.isCanProtocol = false;
            } else {
                this.isCanProtocol = true;
            }
        } else {
            this.isCanProtocol = false;
        }
        this.canBusMonitoringState = com.navdy.hud.app.obd.ObdCanBusRecordingPolicy.CanBusMonitoringState.UNKNOWN;
        this.canBusMonitoringDataSize = 0;
        if (connected) {
            long time = java.lang.System.currentTimeMillis();
            if (metaDataFiles.size() > 10) {
                java.io.File oldestFile = (java.io.File) metaDataFiles.poll();
                if (oldestFile != null) {
                    sLogger.d("Deleting file " + oldestFile.getName() + ", As its old");
                    com.navdy.service.library.util.IOUtils.deleteFile(com.navdy.hud.app.HudApplication.getAppContext(), oldestFile.getAbsolutePath());
                }
            }
            this.currentMetaDataFile = getMetaDataFile();
            java.io.FileWriter fw = null;
            java.io.BufferedWriter bw2 = null;
            org.json.JSONObject jsonObject = new org.json.JSONObject();
            try {
                java.io.FileWriter fw2 = new java.io.FileWriter(this.currentMetaDataFile, false);
                try {
                    bw = new java.io.BufferedWriter(fw2);
                } catch (java.io.IOException e) {
                    fw = fw2;
                    try {
                        sLogger.d("Error writing to the metadata file");
                        com.navdy.service.library.util.IOUtils.closeStream(fw);
                        com.navdy.service.library.util.IOUtils.closeStream(bw2);
                    } catch (Throwable th) {
                        th = th;
                        com.navdy.service.library.util.IOUtils.closeStream(fw);
                        com.navdy.service.library.util.IOUtils.closeStream(bw2);
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    fw = fw2;
                    com.navdy.service.library.util.IOUtils.closeStream(fw);
                    com.navdy.service.library.util.IOUtils.closeStream(bw2);
                    throw th;
                }
                try {
                    com.navdy.obd.ICarService carService = this.obdManager.getCarService();
                    this.vin = this.obdManager.getVin();
                    java.text.SimpleDateFormat simpleDateFormat = dateFormat;
                    java.util.Date date = new java.util.Date(time);
                    try {
                        jsonObject.accumulate(com.navdy.hud.app.device.gps.GpsConstants.GPS_EVENT_TIME, simpleDateFormat.format(date));
                        jsonObject.accumulate("vin", this.vin);
                        if (carService != null) {
                            try {
                                jsonObject.accumulate("protocol", carService.getProtocol());
                                java.util.List<com.navdy.obd.ECU> ecus = carService.getEcus();
                                org.json.JSONArray jsonArray = new org.json.JSONArray();
                                if (ecus != null) {
                                    for (int i = 0; i < ecus.size(); i++) {
                                        org.json.JSONObject ecuObject = new org.json.JSONObject();
                                        com.navdy.obd.ECU ecu = (com.navdy.obd.ECU) ecus.get(i);
                                        ecuObject.accumulate("Address ", java.lang.Integer.valueOf(ecu.address));
                                        java.util.List<com.navdy.obd.Pid> pids = ecu.supportedPids.asList();
                                        org.json.JSONArray pidsArray = new org.json.JSONArray();
                                        if (pids != null) {
                                            for (com.navdy.obd.Pid pid : pids) {
                                                pidsArray.put(pid.getId());
                                            }
                                        }
                                        ecuObject.accumulate("PIDs", pidsArray);
                                        jsonArray.put(ecuObject);
                                    }
                                    jsonObject.accumulate("ecus", jsonArray);
                                }
                            } catch (android.os.RemoteException e2) {
                                sLogger.d("Error get the data from obd service");
                            }
                        }
                        if (!android.text.TextUtils.isEmpty(this.make)) {
                            jsonObject.accumulate("Make", this.make);
                        }
                        if (!android.text.TextUtils.isEmpty(this.model)) {
                            jsonObject.accumulate("Model", this.model);
                        }
                        if (!android.text.TextUtils.isEmpty(this.year)) {
                            jsonObject.accumulate("Year", this.year);
                        }
                    } catch (org.json.JSONException e3) {
                        sLogger.d("Error writing meta data JSON ", e3);
                    }
                    bw.write(jsonObject.toString());
                    bw.flush();
                    com.navdy.service.library.util.IOUtils.closeStream(fw2);
                    com.navdy.service.library.util.IOUtils.closeStream(bw);
                    java.io.BufferedWriter bufferedWriter = bw;
                    java.io.FileWriter fileWriter = fw2;
                } catch (java.io.IOException e4) {
                    bw2 = bw;
                    fw = fw2;
                } catch (Throwable th3) {
                    th = th3;
                    bw2 = bw;
                    fw = fw2;
                    com.navdy.service.library.util.IOUtils.closeStream(fw);
                    com.navdy.service.library.util.IOUtils.closeStream(bw2);
                    throw th;
                }
            } catch (java.io.IOException e5) {
                sLogger.d("Error writing to the metadata file");
                com.navdy.service.library.util.IOUtils.closeStream(fw);
                com.navdy.service.library.util.IOUtils.closeStream(bw2);
            }
        } else {
            this.currentMetaDataFile = null;
        }
    }

    public synchronized void onCarDetailsAvailable(java.lang.String make2, java.lang.String model2, java.lang.String year2) {
        java.io.BufferedWriter bw;
        sLogger.d("onCarDetailsAvailable Make : " + make2 + " , Model : " + model2 + ", Year : " + year2);
        reportBusMonitoringState();
        if (!android.text.TextUtils.equals(this.make, make2) || !android.text.TextUtils.equals(this.model, model2) || !android.text.TextUtils.equals(this.year, year2)) {
            sLogger.d("Car details changed");
            this.make = make2;
            this.model = model2;
            this.year = year2;
            if (this.isObdConnected && this.currentMetaDataFile != null) {
                java.io.FileWriter fw = null;
                java.io.BufferedWriter bw2 = null;
                try {
                    java.io.FileWriter fw2 = new java.io.FileWriter(this.currentMetaDataFile, true);
                    try {
                        bw = new java.io.BufferedWriter(fw2);
                    } catch (java.io.IOException e) {
                        fw = fw2;
                        try {
                            sLogger.d("Error writing to the metadata file");
                            com.navdy.service.library.util.IOUtils.closeStream(fw);
                            com.navdy.service.library.util.IOUtils.closeStream(bw2);
                        } catch (Throwable th) {
                            th = th;
                            com.navdy.service.library.util.IOUtils.closeStream(fw);
                            com.navdy.service.library.util.IOUtils.closeStream(bw2);
                            throw th;
                        }
                    } catch (Throwable th2) {
                        th = th2;
                        fw = fw2;
                        com.navdy.service.library.util.IOUtils.closeStream(fw);
                        com.navdy.service.library.util.IOUtils.closeStream(bw2);
                        throw th;
                    }
                    try {
                        if (!android.text.TextUtils.isEmpty(make2)) {
                            bw.write("Make : " + make2 + com.navdy.hud.app.framework.glance.GlanceConstants.NEWLINE);
                        }
                        if (!android.text.TextUtils.isEmpty(model2)) {
                            bw.write("Model : " + model2 + com.navdy.hud.app.framework.glance.GlanceConstants.NEWLINE);
                        }
                        if (!android.text.TextUtils.isEmpty(year2)) {
                            bw.write("Year : " + year2 + com.navdy.hud.app.framework.glance.GlanceConstants.NEWLINE);
                        }
                        bw.flush();
                        com.navdy.service.library.util.IOUtils.closeStream(fw2);
                        com.navdy.service.library.util.IOUtils.closeStream(bw);
                        java.io.BufferedWriter bufferedWriter = bw;
                        java.io.FileWriter fileWriter = fw2;
                    } catch (java.io.IOException e2) {
                        bw2 = bw;
                        fw = fw2;
                        sLogger.d("Error writing to the metadata file");
                        com.navdy.service.library.util.IOUtils.closeStream(fw);
                        com.navdy.service.library.util.IOUtils.closeStream(bw2);
                    } catch (Throwable th3) {
                        th = th3;
                        bw2 = bw;
                        fw = fw2;
                        com.navdy.service.library.util.IOUtils.closeStream(fw);
                        com.navdy.service.library.util.IOUtils.closeStream(bw2);
                        throw th;
                    }
                } catch (java.io.IOException e3) {
                    sLogger.d("Error writing to the metadata file");
                    com.navdy.service.library.util.IOUtils.closeStream(fw);
                    com.navdy.service.library.util.IOUtils.closeStream(bw2);
                }
            }
        } else {
            sLogger.d("Car details same");
        }
    }

    public boolean isCanBusMonitoringNeeded() {
        boolean monitoringNeeded;
        boolean dataRecordedAndSent = this.sharedPreferences.getBoolean(PREF_CAN_BUS_DATA_RECORDED_AND_SENT, false);
        int sentDataVersion = this.sharedPreferences.getInt(PREF_CAN_BUS_DATA_SENT_VERSION, -1);
        sLogger.d("IsCanBusMonitoringNeeded : ? Is Engineering Build " + this.isEngineeringBuild + "Data record sent : " + dataRecordedAndSent + ", Sent Data Version : " + sentDataVersion + ", isMoving : " + this.motionDetected + ", InstantaneousMode : " + this.isInstantaneousModeOn + ", Obd Connected : " + this.isObdConnected + ", Is CAN protocol :" + this.isCanProtocol);
        if (!this.isEngineeringBuild || ((dataRecordedAndSent && sentDataVersion == this.requiredObdDataVersion) || !this.motionDetected || this.isInstantaneousModeOn || !this.isObdConnected || !this.isCanProtocol)) {
            monitoringNeeded = false;
        } else {
            monitoringNeeded = true;
        }
        if (monitoringNeeded) {
            long distanceTravelledWhenMonitoringStarted = this.sharedPreferences.getLong(PREFERENCE_NAVDY_MILES_WHEN_LISTENING_STARTED, -1);
            long distanceDrivenSoFar = this.tripManager.getTotalDistanceTravelledWithNavdy();
            if (distanceTravelledWhenMonitoringStarted == -1) {
                this.sharedPreferences.edit().putLong(PREFERENCE_NAVDY_MILES_WHEN_LISTENING_STARTED, distanceDrivenSoFar).apply();
            } else if (distanceDrivenSoFar - distanceTravelledWhenMonitoringStarted > CAN_BUS_MONITORING_DISTANCE_LIMIT_METERS) {
                sLogger.d("CAN bus monitoring limit reached, Distance recorded when listening started " + distanceTravelledWhenMonitoringStarted + ", Current distance travelled " + distanceDrivenSoFar);
                this.isCanBusMonitoringLimitReached = true;
            }
        } else {
            this.isCanBusMonitoringLimitReached = false;
        }
        return monitoringNeeded;
    }

    public void onNewDataAvailable(java.lang.String filePath) {
        sLogger.d("onNewDataAvailable , File path : " + filePath);
        java.io.File dataFile = new java.io.File(filePath);
        java.lang.String dateString = dateFormat.format(new java.util.Date(java.lang.System.currentTimeMillis()));
        if (dataFile.exists()) {
            java.io.File newFile = new java.io.File(FILES_TO_UPLOAD_DIRECTORY + java.io.File.separator + dataFile.getName() + "" + dateString + ".log");
            sLogger.d("Moving From : " + dataFile.getAbsolutePath() + ", TO : " + newFile.getAbsolutePath());
            dataFile.renameTo(newFile);
        }
        java.io.File[] files = new java.io.File(FILES_TO_UPLOAD_DIRECTORY).listFiles();
        java.util.List<java.io.File> filesToBeBundled = new java.util.ArrayList<>();
        if (files != null) {
            for (java.io.File childrenFile : files) {
                sLogger.d("Child " + childrenFile.getName());
                if (childrenFile.isFile() && !childrenFile.getName().endsWith(".zip")) {
                    filesToBeBundled.add(childrenFile);
                }
            }
            if (filesToBeBundled.size() > 0) {
                java.io.File[] filesToBeBundledArray = new java.io.File[filesToBeBundled.size()];
                filesToBeBundled.toArray(filesToBeBundledArray);
                java.lang.StringBuilder builder = new java.lang.StringBuilder();
                builder.append("CAN_BUS_DATA_");
                if (!android.text.TextUtils.isEmpty(this.make)) {
                    builder.append(this.make + com.navdy.service.library.util.MusicDataUtils.ALTERNATE_SEPARATOR);
                }
                if (!android.text.TextUtils.isEmpty(this.model)) {
                    builder.append(this.model + com.navdy.service.library.util.MusicDataUtils.ALTERNATE_SEPARATOR);
                }
                if (!android.text.TextUtils.isEmpty(this.year)) {
                    builder.append(this.year + com.navdy.service.library.util.MusicDataUtils.ALTERNATE_SEPARATOR);
                }
                if (!android.text.TextUtils.isEmpty(this.vin)) {
                    builder.append(this.vin + com.navdy.service.library.util.MusicDataUtils.ALTERNATE_SEPARATOR);
                }
                builder.append("V(" + this.requiredObdDataVersion + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.CLOSE_BRACKET);
                builder.append(dateString).append(".zip");
                java.io.File uploadFile = new java.io.File(FILES_TO_UPLOAD_DIRECTORY + java.io.File.separator + builder.toString());
                sLogger.d("Compressing the files to " + uploadFile.getName());
                com.navdy.service.library.util.IOUtils.compressFilesToZip(com.navdy.hud.app.HudApplication.getAppContext(), filesToBeBundledArray, uploadFile.getAbsolutePath());
                for (java.io.File temp : filesToBeBundledArray) {
                    com.navdy.service.library.util.IOUtils.deleteFile(com.navdy.hud.app.HudApplication.getAppContext(), temp.getAbsolutePath());
                }
                uploadFiles.add(uploadFile);
                if (uploadFiles.size() > 5) {
                    java.io.File oldestFile = (java.io.File) uploadFiles.poll();
                    if (oldestFile != null) {
                        sLogger.d("Deleting upload file " + oldestFile.getName() + ", As its old");
                        com.navdy.service.library.util.IOUtils.deleteFile(com.navdy.hud.app.HudApplication.getAppContext(), oldestFile.getAbsolutePath());
                    }
                }
                com.navdy.hud.app.service.ObdCANBusDataUploadService.addObdDataFileToQueue(uploadFile);
                com.navdy.hud.app.service.ObdCANBusDataUploadService.syncNow();
                return;
            }
            return;
        }
        sLogger.d("No files found in the upload directory");
    }

    private void populateFilesQueue() {
        java.io.File[] childrenFiles;
        metaDataFiles.clear();
        uploadFiles.clear();
        for (java.io.File childFile : UPLOAD_DIRECTORY_FILE.listFiles()) {
            if (childFile.isFile() && childFile.getName().startsWith("meta_data")) {
                metaDataFiles.add(childFile);
                if (metaDataFiles.size() > 10) {
                    java.io.File oldestFile = (java.io.File) metaDataFiles.poll();
                    if (oldestFile != null) {
                        sLogger.d("Deleting meta data file " + oldestFile.getName() + ", As its old");
                        com.navdy.service.library.util.IOUtils.deleteFile(com.navdy.hud.app.HudApplication.getAppContext(), oldestFile.getAbsolutePath());
                    }
                }
            } else if (childFile.isFile() && childFile.getName().startsWith("CAN_BUS_DATA_")) {
                uploadFiles.add(childFile);
                if (uploadFiles.size() > 5) {
                    java.io.File oldestFile2 = (java.io.File) uploadFiles.poll();
                    if (oldestFile2 != null) {
                        sLogger.d("Deleting upload file " + oldestFile2.getName() + ", As its old");
                        com.navdy.service.library.util.IOUtils.deleteFile(com.navdy.hud.app.HudApplication.getAppContext(), oldestFile2.getAbsolutePath());
                    }
                }
            }
        }
    }

    public void onFileUploaded(java.io.File file) {
        sLogger.d("File successfully uploaded " + file.getAbsolutePath());
        this.sharedPreferences.edit().putBoolean(PREF_CAN_BUS_DATA_RECORDED_AND_SENT, true).commit();
        this.sharedPreferences.edit().putInt(PREF_CAN_BUS_DATA_SENT_VERSION, this.requiredObdDataVersion).commit();
        this.sharedPreferences.edit().putLong(PREFERENCE_NAVDY_MILES_WHEN_LISTENING_STARTED, -1);
        populateFilesQueue();
        com.navdy.hud.app.analytics.AnalyticsSupport.recordObdCanBusDataSent(java.lang.Integer.toString(this.requiredObdDataVersion));
        this.obdManager.updateListener();
    }

    public void onInstantaneousModeChanged(boolean enabled) {
        this.isInstantaneousModeOn = enabled;
    }

    @com.squareup.otto.Subscribe
    public void onDrivingStateChanged(com.navdy.hud.app.event.DrivingStateChange drivingStateChange) {
        boolean motionDetected2 = drivingStateChange.driving;
        if (this.motionDetected != motionDetected2) {
            boolean isCanBusMonitoringNeededPrev = isCanBusMonitoringNeeded();
            this.motionDetected = motionDetected2;
            if (isCanBusMonitoringNeededPrev != isCanBusMonitoringNeeded()) {
                sLogger.d("Can BUS monitoring need changed, update listener");
                this.obdManager.updateListener();
                return;
            }
            sLogger.d("Can BUS monitoring need is not changed due to driving state change");
        }
    }

    private java.io.File getMetaDataFile() {
        java.io.File file = new java.io.File(META_DATA_FILE + dateFormat.format(new java.util.Date(java.lang.System.currentTimeMillis())) + ".txt");
        java.io.File dir = file.getParentFile();
        if (!dir.exists()) {
            sLogger.d("Upload directory not found, creating");
            dir.mkdirs();
        }
        sLogger.d("Creating Meta data file " + file.getName());
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (java.io.IOException e) {
                sLogger.e("IOException while creating file ", e);
            }
        }
        return file;
    }

    public void onCanBusMonitorSuccess(int approxDataSize) {
        this.canBusMonitoringState = com.navdy.hud.app.obd.ObdCanBusRecordingPolicy.CanBusMonitoringState.SUCCESS;
        this.canBusMonitoringDataSize = approxDataSize;
        reportBusMonitoringState();
    }

    public void onCanBusMonitoringFailed() {
        double speed = (double) com.navdy.hud.app.manager.SpeedManager.getInstance().getObdSpeed();
        double rpm = (double) this.obdManager.getEngineRpm();
        sLogger.d("onCanBusMonitoringFailed , RawSpeed : " + speed + ", RPM : " + rpm);
        if (speed > 0.0d || rpm > 0.0d) {
            this.canBusMonitoringState = com.navdy.hud.app.obd.ObdCanBusRecordingPolicy.CanBusMonitoringState.FAILURE;
            reportBusMonitoringState();
        }
    }

    private void reportBusMonitoringState() {
        boolean isMonitoringStateReported;
        boolean z = true;
        int i = 0;
        if (this.sharedPreferences.getInt(PREF_CAN_BUS_DATA_STATUS_REPORTED_VERSION, -1) == this.requiredObdDataVersion) {
            isMonitoringStateReported = true;
        } else {
            isMonitoringStateReported = false;
        }
        sLogger.d("reportBusMonitoringState, Already reported ? : " + isMonitoringStateReported);
        if (!isMonitoringStateReported) {
            sLogger.d("Reporting, Make " + this.make + ", Model : " + this.model + ", Year : " + this.year + ", State : " + this.canBusMonitoringState);
            if (!android.text.TextUtils.isEmpty(this.make) && !android.text.TextUtils.isEmpty(this.model) && !android.text.TextUtils.isEmpty(this.year) && this.canBusMonitoringState != com.navdy.hud.app.obd.ObdCanBusRecordingPolicy.CanBusMonitoringState.UNKNOWN) {
                java.lang.String str = this.vin;
                if (this.canBusMonitoringState != com.navdy.hud.app.obd.ObdCanBusRecordingPolicy.CanBusMonitoringState.SUCCESS) {
                    z = false;
                }
                if (this.canBusMonitoringState == com.navdy.hud.app.obd.ObdCanBusRecordingPolicy.CanBusMonitoringState.SUCCESS) {
                    i = this.canBusMonitoringDataSize;
                }
                com.navdy.hud.app.analytics.AnalyticsSupport.recordObdCanBusMonitoringState(str, z, i, this.requiredObdDataVersion);
                this.sharedPreferences.edit().putInt(PREF_CAN_BUS_DATA_STATUS_REPORTED_VERSION, this.requiredObdDataVersion).apply();
            }
        }
    }

    public boolean isCanBusMonitoringLimitReached() {
        return this.isCanBusMonitoringLimitReached;
    }
}
