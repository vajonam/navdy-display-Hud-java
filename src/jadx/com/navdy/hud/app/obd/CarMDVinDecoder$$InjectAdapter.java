package com.navdy.hud.app.obd;

public final class CarMDVinDecoder$$InjectAdapter extends dagger.internal.Binding<com.navdy.hud.app.obd.CarMDVinDecoder> implements dagger.MembersInjector<com.navdy.hud.app.obd.CarMDVinDecoder> {
    private dagger.internal.Binding<com.navdy.service.library.network.http.IHttpManager> mHttpManager;

    public CarMDVinDecoder$$InjectAdapter() {
        super(null, "members/com.navdy.hud.app.obd.CarMDVinDecoder", false, com.navdy.hud.app.obd.CarMDVinDecoder.class);
    }

    public void attach(dagger.internal.Linker linker) {
        this.mHttpManager = linker.requestBinding("com.navdy.service.library.network.http.IHttpManager", com.navdy.hud.app.obd.CarMDVinDecoder.class, getClass().getClassLoader());
    }

    public void getDependencies(java.util.Set<dagger.internal.Binding<?>> set, java.util.Set<dagger.internal.Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.mHttpManager);
    }

    public void injectMembers(com.navdy.hud.app.obd.CarMDVinDecoder object) {
        object.mHttpManager = (com.navdy.service.library.network.http.IHttpManager) this.mHttpManager.get();
    }
}
