package com.navdy.hud.app.ancs;

public class AncsServiceConnector extends com.navdy.hud.app.common.ServiceReconnector {
    private static final int NOTIFICATION_EVENT_SEND_THRESHOLD = 15000;
    private static final int ONE_HOUR_IN_MS = 3600000;
    /* access modifiers changed from: private */
    public static java.util.HashMap<java.lang.String, java.lang.Boolean> blackList = new java.util.HashMap<>();
    /* access modifiers changed from: private */
    public static java.util.HashMap<java.lang.String, java.lang.Long> enableNotificationTimeMap = new java.util.HashMap<>();
    static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ancs.AncsServiceConnector.class);
    private com.navdy.service.library.events.notification.NotificationCategory[] categoryMap = {com.navdy.service.library.events.notification.NotificationCategory.CATEGORY_OTHER, com.navdy.service.library.events.notification.NotificationCategory.CATEGORY_INCOMING_CALL, com.navdy.service.library.events.notification.NotificationCategory.CATEGORY_MISSED_CALL, com.navdy.service.library.events.notification.NotificationCategory.CATEGORY_VOICE_MAIL, com.navdy.service.library.events.notification.NotificationCategory.CATEGORY_SOCIAL, com.navdy.service.library.events.notification.NotificationCategory.CATEGORY_SCHEDULE, com.navdy.service.library.events.notification.NotificationCategory.CATEGORY_EMAIL, com.navdy.service.library.events.notification.NotificationCategory.CATEGORY_NEWS, com.navdy.service.library.events.notification.NotificationCategory.CATEGORY_HEALTH_AND_FITNESS, com.navdy.service.library.events.notification.NotificationCategory.CATEGORY_BUSINESS_AND_FINANCE, com.navdy.service.library.events.notification.NotificationCategory.CATEGORY_LOCATION, com.navdy.service.library.events.notification.NotificationCategory.CATEGORY_ENTERTAINMENT};
    private java.lang.Runnable connectRunnable = new com.navdy.hud.app.ancs.AncsServiceConnector.Anon2();
    private java.lang.Runnable disconnectRunnable = new com.navdy.hud.app.ancs.AncsServiceConnector.Anon3();
    private com.navdy.ancs.IAncsServiceListener listener = new com.navdy.hud.app.ancs.AncsServiceConnector.Anon1();
    /* access modifiers changed from: private */
    public boolean localeUpToDate;
    protected com.navdy.hud.app.util.NotificationActionCache mActionCache;
    protected com.squareup.otto.Bus mBus;
    protected android.content.Context mContext;
    /* access modifiers changed from: private */
    public com.navdy.service.library.events.DeviceInfo mDeviceInfo;
    protected com.navdy.hud.app.profile.NotificationSettings mNotificationSettings;
    protected com.navdy.ancs.IAncsService mService;
    private com.navdy.service.library.events.notification.NotificationsState notificationsState = com.navdy.service.library.events.notification.NotificationsState.NOTIFICATIONS_STOPPED;
    private boolean reconnect;

    class Anon1 extends com.navdy.ancs.IAncsServiceListener.Stub {
        Anon1() {
        }

        public void onConnectionStateChange(int newState) throws android.os.RemoteException {
            com.navdy.hud.app.ancs.AncsServiceConnector.sLogger.d("ANCS connection state change - " + newState);
            com.navdy.service.library.events.notification.NotificationsState mappedState = null;
            com.navdy.service.library.events.notification.NotificationsError error = null;
            switch (newState) {
                case 0:
                    mappedState = com.navdy.service.library.events.notification.NotificationsState.NOTIFICATIONS_STOPPED;
                    break;
                case 1:
                    mappedState = com.navdy.service.library.events.notification.NotificationsState.NOTIFICATIONS_CONNECTING;
                    break;
                case 2:
                    mappedState = com.navdy.service.library.events.notification.NotificationsState.NOTIFICATIONS_ENABLED;
                    break;
                case 4:
                    error = com.navdy.service.library.events.notification.NotificationsError.NOTIFICATIONS_ERROR_AUTH_FAILED;
                    mappedState = com.navdy.service.library.events.notification.NotificationsState.NOTIFICATIONS_PAIRING_FAILED;
                    break;
                case 5:
                    error = com.navdy.service.library.events.notification.NotificationsError.NOTIFICATIONS_ERROR_BOND_REMOVED;
                    mappedState = com.navdy.service.library.events.notification.NotificationsState.NOTIFICATIONS_PAIRING_FAILED;
                    break;
            }
            if (mappedState != null) {
                com.navdy.hud.app.ancs.AncsServiceConnector.this.setNotificationsState(mappedState, error);
            }
        }

        public void onNotification(com.navdy.ancs.AppleNotification notification) throws android.os.RemoteException {
            java.lang.String appId = notification.getAppId();
            com.navdy.hud.app.ancs.AncsServiceConnector.sLogger.d("Hud listener notified of apple notification id[" + appId);
            try {
                if (!com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().isRemoteDeviceConnected()) {
                    com.navdy.hud.app.ancs.AncsServiceConnector.sLogger.d("not connected");
                    return;
                }
                if (!(com.navdy.hud.app.ancs.AncsServiceConnector.blackList.get(appId) != null) && notification.getEventId() == 0) {
                    java.lang.Boolean enabled = com.navdy.hud.app.ancs.AncsServiceConnector.this.mNotificationSettings.enabled(notification);
                    if (java.lang.Boolean.TRUE.equals(enabled) && !notification.isPreExisting()) {
                        com.navdy.service.library.events.glances.GlanceEvent event = com.navdy.hud.app.ancs.AncsServiceConnector.this.convertToGlanceEvent(notification);
                        if (event == null) {
                            com.navdy.hud.app.ancs.AncsServiceConnector.sLogger.e("glance not converted");
                            return;
                        }
                        com.navdy.hud.app.ancs.AncsServiceConnector.sLogger.d("posting notification");
                        com.navdy.hud.app.ancs.AncsServiceConnector.this.mBus.post(event);
                    } else if (enabled == null) {
                        java.lang.Long lastEventTime = (java.lang.Long) com.navdy.hud.app.ancs.AncsServiceConnector.enableNotificationTimeMap.get(appId);
                        long clockTime = android.os.SystemClock.elapsedRealtime();
                        if (lastEventTime == null || clockTime - lastEventTime.longValue() >= com.glympse.android.lib.StaticConfig.PERMISSION_CHECK_INTERVAL) {
                            com.navdy.service.library.events.notification.NotificationEvent event2 = com.navdy.hud.app.ancs.AncsServiceConnector.this.convertNotification(notification);
                            com.navdy.hud.app.ancs.AncsServiceConnector.sLogger.d("sending unknown notification to client - " + event2);
                            com.navdy.hud.app.ancs.AncsServiceConnector.this.mBus.post(new com.navdy.hud.app.event.RemoteEvent(event2));
                            com.navdy.hud.app.ancs.AncsServiceConnector.enableNotificationTimeMap.put(appId, java.lang.Long.valueOf(clockTime));
                            return;
                        }
                        com.navdy.hud.app.ancs.AncsServiceConnector.sLogger.d("not sending unknown notification to client, within threshold");
                    }
                }
            } catch (Throwable t) {
                com.navdy.hud.app.ancs.AncsServiceConnector.sLogger.e("ancs-notif", t);
            }
        }
    }

    class Anon2 implements java.lang.Runnable {
        Anon2() {
        }

        public void run() {
            com.navdy.hud.app.ancs.AncsServiceConnector.sLogger.d("Establishing ANCS connection");
            if (com.navdy.hud.app.ancs.AncsServiceConnector.this.mService != null && com.navdy.hud.app.ancs.AncsServiceConnector.this.mDeviceInfo != null && com.navdy.hud.app.ancs.AncsServiceConnector.this.localeUpToDate) {
                try {
                    com.navdy.hud.app.ancs.AncsServiceConnector.this.mService.connectToDevice(new android.os.ParcelUuid(java.util.UUID.fromString(com.navdy.hud.app.ancs.AncsServiceConnector.this.mDeviceInfo.deviceUuid)), new com.navdy.service.library.device.NavdyDeviceId(com.navdy.hud.app.ancs.AncsServiceConnector.this.mDeviceInfo.deviceId).getBluetoothAddress());
                } catch (android.os.RemoteException e) {
                    com.navdy.hud.app.ancs.AncsServiceConnector.sLogger.e("Failed to connect to navdy BTLE service", e);
                }
            }
        }
    }

    class Anon3 implements java.lang.Runnable {
        Anon3() {
        }

        public void run() {
            com.navdy.hud.app.ancs.AncsServiceConnector.this.stop();
            com.navdy.hud.app.ancs.AncsServiceConnector.this.restart();
        }
    }

    static {
        blackList.put("com.apple.mobilephone", java.lang.Boolean.valueOf(true));
    }

    public AncsServiceConnector(android.content.Context context, com.squareup.otto.Bus bus) {
        super(context, getServiceIntent(), null);
        this.mContext = context;
        this.mBus = bus;
        this.mNotificationSettings = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile().getNotificationSettings();
        this.mActionCache = new com.navdy.hud.app.util.NotificationActionCache(getClass());
        bus.register(this);
    }

    private static android.content.Intent getServiceIntent() {
        android.content.Intent intent = new android.content.Intent();
        intent.setComponent(new android.content.ComponentName("com.navdy.ancs.app", "com.navdy.ancs.ANCSService"));
        return intent;
    }

    /* access modifiers changed from: private */
    public void setNotificationsState(com.navdy.service.library.events.notification.NotificationsState state, com.navdy.service.library.events.notification.NotificationsError error) {
        if (this.notificationsState != state) {
            this.notificationsState = state;
            this.mBus.post(new com.navdy.hud.app.event.RemoteEvent(new com.navdy.service.library.events.notification.NotificationsStatusUpdate.Builder().state(this.notificationsState).service(com.navdy.service.library.events.notification.ServiceType.SERVICE_ANCS).errorDetails(error).build()));
        }
    }

    /* access modifiers changed from: protected */
    public void onConnected(android.content.ComponentName name, android.os.IBinder service) {
        this.mService = com.navdy.ancs.IAncsService.Stub.asInterface(service);
        try {
            updateNotificationFilters();
            this.mService.addListener(this.listener);
        } catch (android.os.RemoteException e) {
            sLogger.e("Failed to add notification listener", e);
        }
        if (this.reconnect) {
            connect();
        }
    }

    /* access modifiers changed from: protected */
    public void onDisconnected(android.content.ComponentName name) {
        this.mService = null;
    }

    public void performAction(int notificationUid, int actionId) {
        if (this.mService != null) {
            try {
                sLogger.d("Performing action (" + actionId + ") for notification - " + notificationUid);
                this.mService.performNotificationAction(notificationUid, actionId);
            } catch (android.os.RemoteException e) {
                sLogger.e("Exception when performing notification action", e);
            }
        }
    }

    /* access modifiers changed from: private */
    public com.navdy.service.library.events.glances.GlanceEvent convertToGlanceEvent(com.navdy.ancs.AppleNotification notification) {
        boolean whiteListed;
        java.lang.String appId = notification.getAppId();
        java.lang.String id = java.lang.String.valueOf(notification.getNotificationUid());
        java.lang.String title = notification.getTitle();
        java.lang.String subtitle = notification.getSubTitle();
        java.lang.String message = notification.getMessage();
        java.lang.String eventId = java.lang.String.valueOf(notification.getEventId());
        java.util.Date time = notification.getDate();
        if (title == null && message == null && subtitle == null) {
            return null;
        }
        com.navdy.hud.app.framework.glance.GlanceApp app = com.navdy.hud.app.framework.glance.GlanceHelper.getGlancesApp(appId);
        if (app == null) {
            app = com.navdy.hud.app.framework.glance.GlanceApp.GENERIC;
            whiteListed = false;
        } else {
            whiteListed = true;
        }
        sLogger.v("[ancs-notif] appId[" + appId + "]" + " id[" + id + "]" + " title[" + title + "]" + " subTitle[" + subtitle + "]" + " message[" + message + "]" + " eventId[" + eventId + "]" + " time[" + time + "]" + " whitelisted[" + whiteListed + "]" + " app[" + app + "]");
        switch (app) {
            case GOOGLE_CALENDAR:
                return com.navdy.hud.app.ancs.AncsGlanceHelper.buildGoogleCalendarEvent(appId, id, title, subtitle, message, time);
            case GOOGLE_MAIL:
                return com.navdy.hud.app.ancs.AncsGlanceHelper.buildGoogleMailEvent(appId, id, title, subtitle, message, time);
            case GENERIC_MAIL:
                return com.navdy.hud.app.ancs.AncsGlanceHelper.buildGenericMailEvent(appId, id, title, subtitle, message, time);
            case GOOGLE_HANGOUT:
                return com.navdy.hud.app.ancs.AncsGlanceHelper.buildGoogleHangoutEvent(appId, id, title, subtitle, message, time);
            case SLACK:
                return com.navdy.hud.app.ancs.AncsGlanceHelper.buildSlackEvent(appId, id, title, subtitle, message, time);
            case WHATS_APP:
                return com.navdy.hud.app.ancs.AncsGlanceHelper.buildWhatsappEvent(appId, id, title, subtitle, message, time);
            case FACEBOOK_MESSENGER:
                return com.navdy.hud.app.ancs.AncsGlanceHelper.buildFacebookMessengerEvent(appId, id, title, subtitle, message, time);
            case FACEBOOK:
                return com.navdy.hud.app.ancs.AncsGlanceHelper.buildFacebookEvent(appId, id, title, subtitle, message, time);
            case TWITTER:
                return com.navdy.hud.app.ancs.AncsGlanceHelper.buildTwitterEvent(appId, id, title, subtitle, message, time);
            case IMESSAGE:
                return com.navdy.hud.app.ancs.AncsGlanceHelper.buildiMessageEvent(appId, id, title, subtitle, message, time);
            case APPLE_MAIL:
                return com.navdy.hud.app.ancs.AncsGlanceHelper.buildAppleMailEvent(appId, id, title, subtitle, message, time);
            case APPLE_CALENDAR:
                return com.navdy.hud.app.ancs.AncsGlanceHelper.buildAppleCalendarEvent(appId, id, title, subtitle, message, time);
            case GENERIC:
                return com.navdy.hud.app.ancs.AncsGlanceHelper.buildGenericEvent(appId, id, title, subtitle, message, time);
            default:
                sLogger.w("[ancs-notif] app of type [" + app + "] not handled");
                return null;
        }
    }

    /* access modifiers changed from: private */
    public com.navdy.service.library.events.notification.NotificationEvent convertNotification(com.navdy.ancs.AppleNotification notification) {
        java.lang.String title = notification.getTitle();
        return new com.navdy.service.library.events.notification.NotificationEvent(java.lang.Integer.valueOf(notification.getNotificationUid()), mapCategory(notification.getCategoryId()), title, notification.getSubTitle(), notification.getMessage(), notification.getAppId(), buildActions(notification), null, null, null, title, java.lang.Boolean.valueOf(true), notification.getAppName());
    }

    private java.util.List<com.navdy.service.library.events.notification.NotificationAction> buildActions(com.navdy.ancs.AppleNotification notification) {
        if (notification.getAppId().equals(com.navdy.hud.app.profile.NotificationSettings.APP_ID_SMS)) {
            return null;
        }
        return getDefaultActions(notification);
    }

    private java.util.List<com.navdy.service.library.events.notification.NotificationAction> getDefaultActions(com.navdy.ancs.AppleNotification notification) {
        java.util.List<com.navdy.service.library.events.notification.NotificationAction> actions = new java.util.ArrayList<>();
        int notificationId = notification.getNotificationUid();
        if (notification.hasPositiveAction()) {
            actions.add(this.mActionCache.buildAction(notificationId, 0, 0, notification.getPositiveActionLabel()));
        }
        if (notification.hasNegativeAction()) {
            actions.add(this.mActionCache.buildAction(notificationId, 1, 0, notification.getNegativeActionLabel()));
        }
        return actions;
    }

    private com.navdy.service.library.events.notification.NotificationCategory mapCategory(int categoryId) {
        if (categoryId < 0 || categoryId >= this.categoryMap.length) {
            return com.navdy.service.library.events.notification.NotificationCategory.CATEGORY_OTHER;
        }
        return this.categoryMap[categoryId];
    }

    @com.squareup.otto.Subscribe
    public void onNotificationsRequest(com.navdy.service.library.events.notification.NotificationsStatusRequest request) {
        if (request.service == null || com.navdy.service.library.events.notification.ServiceType.SERVICE_ANCS.equals(request.service)) {
            sLogger.i("Received NotificationsStatusRequest:" + request);
            if (request.newState != null) {
                switch (request.newState) {
                    case NOTIFICATIONS_ENABLED:
                        connect();
                        break;
                    case NOTIFICATIONS_STOPPED:
                        disconnect();
                        break;
                }
            }
            this.mBus.post(new com.navdy.hud.app.event.RemoteEvent(new com.navdy.service.library.events.notification.NotificationsStatusUpdate.Builder().state(this.notificationsState).service(com.navdy.service.library.events.notification.ServiceType.SERVICE_ANCS).build()));
        }
    }

    private void updateNotificationFilters() {
        this.mNotificationSettings = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile().getNotificationSettings();
        if (this.mService != null) {
            try {
                long oneHourAgo = java.lang.System.currentTimeMillis() - 3600000;
                java.util.List<java.lang.String> enabledApps = this.mNotificationSettings != null ? this.mNotificationSettings.enabledApps() : null;
                sLogger.d("Setting ANCS to filter apps: " + (enabledApps != null ? java.util.Arrays.toString(enabledApps.toArray()) : com.amazonaws.services.s3.internal.Constants.NULL_VERSION_ID));
                this.mService.setNotificationFilter(enabledApps, oneHourAgo);
            } catch (android.os.RemoteException e) {
                sLogger.w("Failed to update notification filter");
            }
        }
    }

    @com.squareup.otto.Subscribe
    public void onDriverProfileChanged(com.navdy.hud.app.event.DriverProfileChanged event) {
        updateNotificationFilters();
    }

    @com.squareup.otto.Subscribe
    public void onNotificationPreferences(com.navdy.service.library.events.preferences.NotificationPreferences preferences) {
        updateNotificationFilters();
    }

    private boolean isIOS(com.navdy.service.library.events.DeviceInfo deviceInfo) {
        return deviceInfo != null && ((deviceInfo.platform != null && deviceInfo.platform == com.navdy.service.library.events.DeviceInfo.Platform.PLATFORM_iOS) || deviceInfo.model.contains("iPhone"));
    }

    @com.squareup.otto.Subscribe
    public void onNotificationAction(com.navdy.service.library.events.notification.NotificationAction action) {
        if (this.mActionCache.validAction(action)) {
            performAction(action.notificationId.intValue(), action.actionId.intValue());
            this.mBus.post(new com.navdy.service.library.events.ui.DismissScreen(com.navdy.service.library.events.ui.Screen.SCREEN_NOTIFICATION));
            this.mActionCache.markComplete(action);
        }
    }

    @com.squareup.otto.Subscribe
    public void onConnectionChange(com.navdy.service.library.events.connection.ConnectionStateChange change) {
        switch (change.state) {
            case CONNECTION_DISCONNECTED:
                this.mDeviceInfo = null;
                this.localeUpToDate = false;
                disconnect();
                return;
            default:
                return;
        }
    }

    @com.squareup.otto.Subscribe
    public void onDeviceInfoAvailable(com.navdy.hud.app.event.DeviceInfoAvailable event) {
        sLogger.i("device info updated");
        this.mDeviceInfo = event.deviceInfo;
    }

    @com.squareup.otto.Subscribe
    public void onInitPhase(com.navdy.hud.app.event.InitEvents.InitPhase event) {
        switch (event.phase) {
            case LOCALE_UP_TO_DATE:
                this.localeUpToDate = true;
                if (this.reconnect) {
                    connect();
                    return;
                }
                return;
            case SWITCHING_LOCALE:
                shutdown();
                return;
            default:
                return;
        }
    }

    public void shutdown() {
        this.reconnect = false;
        stop();
        super.shutdown();
    }

    @com.squareup.otto.Subscribe
    public void onShutdown(com.navdy.hud.app.event.Shutdown event) {
        if (event.state == com.navdy.hud.app.event.Shutdown.State.CONFIRMED) {
            shutdown();
        }
    }

    /* access modifiers changed from: private */
    public void stop() {
        sLogger.d("Disconnecting ANCS connection");
        if (this.mService != null) {
            try {
                this.mService.disconnect();
            } catch (android.os.RemoteException e) {
                sLogger.e("Failed to disconnect ANCS", e);
            }
        }
    }

    private void connect() {
        this.reconnect = true;
        if (isIOS(this.mDeviceInfo)) {
            com.navdy.service.library.task.TaskManager.getInstance().execute(this.connectRunnable, 1);
        }
    }

    private void disconnect() {
        this.reconnect = false;
        com.navdy.service.library.task.TaskManager.getInstance().execute(this.disconnectRunnable, 1);
    }
}
