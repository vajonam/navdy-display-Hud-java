package com.navdy.hud.app.ancs;

public class AncsGlanceHelper {
    private static com.navdy.service.library.log.Logger sLogger = com.navdy.hud.app.ancs.AncsServiceConnector.sLogger;

    public static com.navdy.service.library.events.glances.GlanceEvent buildGoogleCalendarEvent(java.lang.String appId, java.lang.String id, java.lang.String title, java.lang.String subtitle, java.lang.String message, java.util.Date time) {
        if (android.text.TextUtils.isEmpty(message)) {
            return null;
        }
        java.lang.String meetingTitle = null;
        java.lang.String meetingTime = null;
        java.lang.String meetingLocation = null;
        java.util.StringTokenizer tokenizer = new java.util.StringTokenizer(message, com.navdy.hud.app.framework.glance.GlanceConstants.NEWLINE);
        int counter = 0;
        while (tokenizer.hasMoreElements()) {
            java.lang.String text = (java.lang.String) tokenizer.nextElement();
            switch (counter) {
                case 0:
                    meetingTitle = text.trim();
                    break;
                case 1:
                    meetingTime = text.trim();
                    break;
                case 2:
                    meetingLocation = text.trim();
                    break;
            }
            counter++;
        }
        sLogger.v("[ancs-gcalendar] title[" + meetingTitle + "] when[" + meetingTime + "] location[" + meetingLocation + "]");
        java.lang.String notifId = com.navdy.hud.app.framework.glance.GlanceHelper.getId();
        java.util.List<com.navdy.service.library.events.glances.KeyValue> data = new java.util.ArrayList<>();
        if (meetingTitle != null) {
            data.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.CalendarConstants.CALENDAR_TITLE.name(), meetingTitle));
        }
        if (meetingTime != null) {
            data.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.CalendarConstants.CALENDAR_TIME_STR.name(), meetingTime));
        }
        data.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.CalendarConstants.CALENDAR_TIME.name(), java.lang.String.valueOf(time.getTime())));
        if (meetingLocation != null) {
            data.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.CalendarConstants.CALENDAR_LOCATION.name(), meetingLocation));
        }
        return new com.navdy.service.library.events.glances.GlanceEvent.Builder().glanceType(com.navdy.service.library.events.glances.GlanceEvent.GlanceType.GLANCE_TYPE_CALENDAR).provider(appId).id(notifId).postTime(java.lang.Long.valueOf(time.getTime())).glanceData(data).build();
    }

    public static com.navdy.service.library.events.glances.GlanceEvent buildGoogleMailEvent(java.lang.String appId, java.lang.String id, java.lang.String title, java.lang.String subtitle, java.lang.String message, java.util.Date time) {
        java.lang.String body;
        if (android.text.TextUtils.isEmpty(message)) {
            return null;
        }
        java.lang.String from = null;
        boolean isEmailAddress = false;
        java.lang.String subject = null;
        int index = message.indexOf(com.navdy.hud.app.framework.glance.GlanceConstants.NEWLINE);
        if (index != -1) {
            from = message.substring(0, index).trim();
            if (from.indexOf(com.navdy.hud.app.framework.glance.GlanceConstants.AT_SEPARATOR) != -1) {
                isEmailAddress = true;
            }
            message = message.substring(index + 1);
        }
        int index2 = message.indexOf(com.navdy.hud.app.framework.glance.GlanceConstants.GMAIL_SEPARATOR);
        if (index2 != -1) {
            subject = message.substring(0, index2).trim();
            body = message.substring(index2 + 1).trim();
        } else {
            body = message.trim();
        }
        sLogger.v("[ancs-gmail] from[" + from + "] subject[" + subject + "] body[" + body + "]");
        java.lang.String notifId = com.navdy.hud.app.framework.glance.GlanceHelper.getId();
        java.util.List<com.navdy.service.library.events.glances.KeyValue> data = new java.util.ArrayList<>();
        if (from != null) {
            if (isEmailAddress) {
                data.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.EmailConstants.EMAIL_FROM_EMAIL.name(), from));
            } else {
                data.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.EmailConstants.EMAIL_FROM_NAME.name(), from));
            }
        }
        if (subject != null) {
            data.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.EmailConstants.EMAIL_SUBJECT.name(), subject));
        }
        if (body != null) {
            data.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.EmailConstants.EMAIL_BODY.name(), body));
        }
        return new com.navdy.service.library.events.glances.GlanceEvent.Builder().glanceType(com.navdy.service.library.events.glances.GlanceEvent.GlanceType.GLANCE_TYPE_EMAIL).provider(appId).id(notifId).postTime(java.lang.Long.valueOf(time.getTime())).glanceData(data).build();
    }

    public static com.navdy.service.library.events.glances.GlanceEvent buildGenericMailEvent(java.lang.String appId, java.lang.String id, java.lang.String title, java.lang.String subtitle, java.lang.String message, java.util.Date time) {
        if (!android.text.TextUtils.isEmpty(message) && com.navdy.hud.app.framework.glance.GlanceHelper.MICROSOFT_OUTLOOK.equals(appId)) {
            return buildOutlookMailEvent(appId, id, title, subtitle, message, time);
        }
        return null;
    }

    public static com.navdy.service.library.events.glances.GlanceEvent buildOutlookMailEvent(java.lang.String appId, java.lang.String id, java.lang.String title, java.lang.String subtitle, java.lang.String message, java.util.Date time) {
        java.lang.String from = title;
        java.lang.String subject = null;
        java.lang.String body = null;
        if (!android.text.TextUtils.isEmpty(message)) {
            java.lang.String[] fields = message.split(com.navdy.hud.app.framework.glance.GlanceConstants.NEWLINE);
            if (fields.length > 1) {
                subject = fields[0];
                body = fields[1];
            } else {
                body = fields[0];
            }
        }
        sLogger.v("[ancs-outlook-email] from[" + from + "] subject[" + subject + "] body[" + body + "]");
        java.lang.String notifId = com.navdy.hud.app.framework.glance.GlanceHelper.getId();
        java.util.List<com.navdy.service.library.events.glances.KeyValue> data = new java.util.ArrayList<>();
        if (!android.text.TextUtils.isEmpty(from)) {
            if (from.contains(com.navdy.hud.app.framework.glance.GlanceConstants.AT_SEPARATOR)) {
                data.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.EmailConstants.EMAIL_FROM_EMAIL.name(), from));
            } else {
                data.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.EmailConstants.EMAIL_FROM_NAME.name(), from));
            }
        }
        if (!android.text.TextUtils.isEmpty(subject)) {
            data.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.EmailConstants.EMAIL_SUBJECT.name(), subject));
        }
        if (!android.text.TextUtils.isEmpty(body)) {
            data.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.EmailConstants.EMAIL_BODY.name(), body));
        }
        return new com.navdy.service.library.events.glances.GlanceEvent.Builder().glanceType(com.navdy.service.library.events.glances.GlanceEvent.GlanceType.GLANCE_TYPE_EMAIL).provider(appId).id(notifId).postTime(java.lang.Long.valueOf(time.getTime())).glanceData(data).build();
    }

    public static com.navdy.service.library.events.glances.GlanceEvent buildGoogleHangoutEvent(java.lang.String appId, java.lang.String id, java.lang.String title, java.lang.String subtitle, java.lang.String message, java.util.Date time) {
        return buildMessageEvent(appId, message, time);
    }

    public static com.navdy.service.library.events.glances.GlanceEvent buildSlackEvent(java.lang.String appId, java.lang.String id, java.lang.String title, java.lang.String subtitle, java.lang.String message, java.util.Date time) {
        return buildMessageEvent(appId, message, time);
    }

    public static com.navdy.service.library.events.glances.GlanceEvent buildWhatsappEvent(java.lang.String appId, java.lang.String id, java.lang.String title, java.lang.String subtitle, java.lang.String message, java.util.Date time) {
        java.lang.String notifId = com.navdy.hud.app.framework.glance.GlanceHelper.getId();
        java.util.List<com.navdy.service.library.events.glances.KeyValue> data = new java.util.ArrayList<>();
        data.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.MessageConstants.MESSAGE_FROM.name(), title));
        data.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.MessageConstants.MESSAGE_BODY.name(), message));
        return new com.navdy.service.library.events.glances.GlanceEvent.Builder().glanceType(com.navdy.service.library.events.glances.GlanceEvent.GlanceType.GLANCE_TYPE_MESSAGE).provider(appId).id(notifId).postTime(java.lang.Long.valueOf(time.getTime())).glanceData(data).build();
    }

    public static com.navdy.service.library.events.glances.GlanceEvent buildFacebookMessengerEvent(java.lang.String appId, java.lang.String id, java.lang.String title, java.lang.String subtitle, java.lang.String message, java.util.Date time) {
        return buildMessageEvent(appId, message, time);
    }

    public static com.navdy.service.library.events.glances.GlanceEvent buildFacebookEvent(java.lang.String appId, java.lang.String id, java.lang.String title, java.lang.String subtitle, java.lang.String message, java.util.Date time) {
        java.lang.String notifId = com.navdy.hud.app.framework.glance.GlanceHelper.getId();
        java.util.List<com.navdy.service.library.events.glances.KeyValue> data = new java.util.ArrayList<>();
        data.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.SocialConstants.SOCIAL_MESSAGE.name(), message));
        return new com.navdy.service.library.events.glances.GlanceEvent.Builder().glanceType(com.navdy.service.library.events.glances.GlanceEvent.GlanceType.GLANCE_TYPE_SOCIAL).provider(appId).id(notifId).postTime(java.lang.Long.valueOf(time.getTime())).glanceData(data).build();
    }

    public static com.navdy.service.library.events.glances.GlanceEvent buildTwitterEvent(java.lang.String appId, java.lang.String id, java.lang.String title, java.lang.String subtitle, java.lang.String message, java.util.Date time) {
        if (android.text.TextUtils.isEmpty(message)) {
            return null;
        }
        java.lang.String from = null;
        java.lang.String to = null;
        java.lang.String body = null;
        int index = message.indexOf(com.navdy.hud.app.framework.glance.GlanceConstants.COLON_SEPARATOR);
        java.lang.String notifId = com.navdy.hud.app.framework.glance.GlanceHelper.getId();
        java.util.List<com.navdy.service.library.events.glances.KeyValue> data = new java.util.ArrayList<>();
        if (index != -1) {
            from = message.substring(0, index).trim();
            if (!android.text.TextUtils.isEmpty(from)) {
                int i = from.indexOf(com.navdy.hud.app.framework.glance.GlanceConstants.AT_SEPARATOR);
                if (i != -1) {
                    from = from.substring(i).trim();
                }
            } else {
                from = title;
            }
            message = message.substring(index + 1).trim();
            int index2 = message.indexOf(" ");
            if (index2 != -1) {
                to = message.substring(0, index2).trim();
                body = message.substring(index2 + 1).trim();
            } else {
                body = message.trim();
            }
        } else {
            int index3 = message.indexOf(com.navdy.hud.app.framework.glance.GlanceConstants.NEWLINE);
            if (index3 == -1) {
                int index4 = message.indexOf(" ");
                if (index4 != -1) {
                    to = message.substring(0, index4).trim();
                    body = message.substring(index4 + 1).trim();
                } else {
                    body = message.trim();
                }
            } else if (index3 != -1) {
                from = message.substring(0, index3).trim();
                body = message.substring(index3 + 1).trim();
            }
        }
        if (from != null) {
            data.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.SocialConstants.SOCIAL_FROM.name(), from));
        }
        if (to != null) {
            data.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.SocialConstants.SOCIAL_TO.name(), to));
        }
        if (body != null) {
            data.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.SocialConstants.SOCIAL_MESSAGE.name(), body));
        } else {
            data.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.SocialConstants.SOCIAL_MESSAGE.name(), message));
        }
        return new com.navdy.service.library.events.glances.GlanceEvent.Builder().glanceType(com.navdy.service.library.events.glances.GlanceEvent.GlanceType.GLANCE_TYPE_SOCIAL).provider(appId).id(notifId).postTime(java.lang.Long.valueOf(time.getTime())).glanceData(data).build();
    }

    public static com.navdy.service.library.events.glances.GlanceEvent buildiMessageEvent(java.lang.String appId, java.lang.String id, java.lang.String title, java.lang.String subtitle, java.lang.String message, java.util.Date time) {
        java.lang.String notifId = com.navdy.hud.app.framework.glance.GlanceHelper.getId();
        java.util.List<com.navdy.service.library.events.glances.KeyValue> data = new java.util.ArrayList<>();
        if (com.navdy.hud.app.framework.contacts.ContactUtil.isValidNumber(title)) {
            data.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.MessageConstants.MESSAGE_FROM_NUMBER.name(), title));
        } else {
            data.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.MessageConstants.MESSAGE_FROM.name(), title));
        }
        data.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.MessageConstants.MESSAGE_BODY.name(), message));
        com.navdy.service.library.events.glances.GlanceEvent.Builder iMessageEventBuilder = new com.navdy.service.library.events.glances.GlanceEvent.Builder().glanceType(com.navdy.service.library.events.glances.GlanceEvent.GlanceType.GLANCE_TYPE_MESSAGE).provider(appId).id(notifId).postTime(java.lang.Long.valueOf(time.getTime())).glanceData(data);
        if (com.navdy.hud.app.ui.component.UISettings.supportsIosSms()) {
            java.util.List<com.navdy.service.library.events.glances.GlanceEvent.GlanceActions> action = new java.util.ArrayList<>(1);
            action.add(com.navdy.service.library.events.glances.GlanceEvent.GlanceActions.REPLY);
            iMessageEventBuilder.actions(action);
        }
        return iMessageEventBuilder.build();
    }

    public static com.navdy.service.library.events.glances.GlanceEvent buildAppleMailEvent(java.lang.String appId, java.lang.String id, java.lang.String title, java.lang.String subtitle, java.lang.String message, java.util.Date time) {
        java.lang.String from = title;
        java.lang.String subject = subtitle;
        java.lang.String body = message;
        boolean isEmailAddress = false;
        if (from.indexOf(com.navdy.hud.app.framework.glance.GlanceConstants.AT_SEPARATOR) != -1) {
            isEmailAddress = true;
        }
        java.lang.String notifId = com.navdy.hud.app.framework.glance.GlanceHelper.getId();
        java.util.List<com.navdy.service.library.events.glances.KeyValue> data = new java.util.ArrayList<>();
        if (isEmailAddress) {
            data.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.EmailConstants.EMAIL_FROM_EMAIL.name(), from));
        } else {
            data.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.EmailConstants.EMAIL_FROM_NAME.name(), from));
        }
        if (subject != null) {
            data.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.EmailConstants.EMAIL_SUBJECT.name(), subject));
        }
        if (body != null) {
            if (body.toLowerCase().contains(com.navdy.hud.app.framework.glance.GlanceConstants.INVALID_APPLE_MAIL_GLANCE_BODY)) {
                sLogger.v("invalid apple mail glance:" + body);
                return null;
            }
            data.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.EmailConstants.EMAIL_BODY.name(), body));
        }
        return new com.navdy.service.library.events.glances.GlanceEvent.Builder().glanceType(com.navdy.service.library.events.glances.GlanceEvent.GlanceType.GLANCE_TYPE_EMAIL).provider(appId).id(notifId).postTime(java.lang.Long.valueOf(time.getTime())).glanceData(data).build();
    }

    public static com.navdy.service.library.events.glances.GlanceEvent buildAppleCalendarEvent(java.lang.String appId, java.lang.String id, java.lang.String title, java.lang.String subtitle, java.lang.String message, java.util.Date time) {
        java.lang.String notifId = com.navdy.hud.app.framework.glance.GlanceHelper.getId();
        java.util.List<com.navdy.service.library.events.glances.KeyValue> data = new java.util.ArrayList<>();
        if (title != null) {
            data.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.CalendarConstants.CALENDAR_TITLE.name(), title));
        }
        if (message != null) {
            data.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.CalendarConstants.CALENDAR_LOCATION.name(), message));
        }
        data.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.CalendarConstants.CALENDAR_TIME_STR.name(), com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getTimeHelper().formatTime(time, null)));
        data.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.CalendarConstants.CALENDAR_TIME.name(), java.lang.String.valueOf(time.getTime())));
        return new com.navdy.service.library.events.glances.GlanceEvent.Builder().glanceType(com.navdy.service.library.events.glances.GlanceEvent.GlanceType.GLANCE_TYPE_CALENDAR).provider(appId).id(notifId).postTime(java.lang.Long.valueOf(java.lang.System.currentTimeMillis())).glanceData(data).build();
    }

    public static com.navdy.service.library.events.glances.GlanceEvent buildGenericEvent(java.lang.String appId, java.lang.String id, java.lang.String title, java.lang.String subtitle, java.lang.String message, java.util.Date time) {
        java.lang.String notifId = com.navdy.hud.app.framework.glance.GlanceHelper.getId();
        java.util.List<com.navdy.service.library.events.glances.KeyValue> data = new java.util.ArrayList<>();
        if (title != null) {
            data.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.GenericConstants.GENERIC_TITLE.name(), title));
        }
        if (message != null) {
            data.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.GenericConstants.GENERIC_MESSAGE.name(), message));
        }
        return new com.navdy.service.library.events.glances.GlanceEvent.Builder().glanceType(com.navdy.service.library.events.glances.GlanceEvent.GlanceType.GLANCE_TYPE_GENERIC).provider(appId).id(notifId).postTime(java.lang.Long.valueOf(time.getTime())).glanceData(data).build();
    }

    private static com.navdy.service.library.events.glances.GlanceEvent buildMessageEvent(java.lang.String appId, java.lang.String message, java.util.Date time) {
        java.lang.String body;
        if (android.text.TextUtils.isEmpty(message)) {
            return null;
        }
        java.lang.String from = null;
        int index = message.indexOf(com.navdy.hud.app.framework.glance.GlanceConstants.COLON_SEPARATOR);
        if (index != -1) {
            from = message.substring(0, index).trim();
            body = message.substring(index + 1).trim();
        } else {
            body = message.trim();
        }
        java.lang.String notifId = com.navdy.hud.app.framework.glance.GlanceHelper.getId();
        java.util.List<com.navdy.service.library.events.glances.KeyValue> data = new java.util.ArrayList<>();
        data.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.MessageConstants.MESSAGE_FROM.name(), from));
        data.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.MessageConstants.MESSAGE_BODY.name(), body));
        return new com.navdy.service.library.events.glances.GlanceEvent.Builder().glanceType(com.navdy.service.library.events.glances.GlanceEvent.GlanceType.GLANCE_TYPE_MESSAGE).provider(appId).id(notifId).postTime(java.lang.Long.valueOf(time.getTime())).glanceData(data).build();
    }
}
