package com.navdy.hud.app.common;

public class MainThreadBus extends com.squareup.otto.Bus {
    /* access modifiers changed from: private */
    public com.navdy.hud.app.debug.BusLeakDetector busLeakDetector;
    /* access modifiers changed from: private */
    public boolean isEngBuild;
    /* access modifiers changed from: private */
    public final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.common.MainThreadBus.class);
    private final android.os.Handler mHandler = new android.os.Handler(android.os.Looper.getMainLooper());
    public int threshold;

    class Anon1 implements java.lang.Runnable {
        final /* synthetic */ java.lang.Object val$event;

        Anon1(java.lang.Object obj) {
            this.val$event = obj;
        }

        public void run() {
            long l1 = 0;
            if (com.navdy.hud.app.common.MainThreadBus.this.threshold > 0) {
                l1 = android.os.SystemClock.elapsedRealtime();
            }
            com.navdy.hud.app.common.MainThreadBus.super.post(this.val$event);
            if (com.navdy.hud.app.common.MainThreadBus.this.threshold > 0) {
                long diff = android.os.SystemClock.elapsedRealtime() - l1;
                if (diff >= ((long) com.navdy.hud.app.common.MainThreadBus.this.threshold)) {
                    com.navdy.hud.app.common.MainThreadBus.this.logger.v("[" + diff + "] MainThreadBus-event [" + this.val$event + "]");
                }
            }
        }
    }

    class Anon2 implements java.lang.Runnable {
        final /* synthetic */ java.lang.Object val$object;

        Anon2(java.lang.Object obj) {
            this.val$object = obj;
        }

        public void run() {
            if (com.navdy.hud.app.common.MainThreadBus.this.isEngBuild) {
                com.navdy.hud.app.common.MainThreadBus.this.busLeakDetector.addReference(this.val$object.getClass());
            }
            com.navdy.hud.app.common.MainThreadBus.super.register(this.val$object);
        }
    }

    class Anon3 implements java.lang.Runnable {
        final /* synthetic */ java.lang.Object val$object;

        Anon3(java.lang.Object obj) {
            this.val$object = obj;
        }

        public void run() {
            if (com.navdy.hud.app.common.MainThreadBus.this.isEngBuild) {
                com.navdy.hud.app.common.MainThreadBus.this.busLeakDetector.removeReference(this.val$object.getClass());
            }
            com.navdy.hud.app.common.MainThreadBus.super.unregister(this.val$object);
        }
    }

    public MainThreadBus() {
        this.isEngBuild = !com.navdy.hud.app.util.DeviceUtil.isUserBuild();
        if (this.isEngBuild) {
            this.busLeakDetector = com.navdy.hud.app.debug.BusLeakDetector.getInstance();
        }
    }

    public void post(java.lang.Object event) {
        if (event != null) {
            this.mHandler.post(new com.navdy.hud.app.common.MainThreadBus.Anon1(event));
        }
    }

    public void register(java.lang.Object object) {
        if (object != null) {
            this.mHandler.post(new com.navdy.hud.app.common.MainThreadBus.Anon2(object));
        }
    }

    public void unregister(java.lang.Object object) {
        if (object != null) {
            this.mHandler.post(new com.navdy.hud.app.common.MainThreadBus.Anon3(object));
        }
    }
}
