package com.navdy.hud.app.common;

public class TimeHelper {
    public static final com.navdy.hud.app.common.TimeHelper.UpdateClock CLOCK_CHANGED = new com.navdy.hud.app.common.TimeHelper.UpdateClock();
    public static final com.navdy.hud.app.common.TimeHelper.DateTimeAvailableEvent DATE_TIME_AVAILABLE_EVENT = new com.navdy.hud.app.common.TimeHelper.DateTimeAvailableEvent();
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.common.TimeHelper.class);
    private java.lang.String am;
    private java.lang.String amCompact;
    private com.squareup.otto.Bus bus;
    private java.util.Calendar calendar;
    private java.text.SimpleDateFormat currentTimeFormat;
    private java.text.SimpleDateFormat dateFormat;
    private java.text.SimpleDateFormat dayFormat;
    private java.lang.String pm;
    private java.lang.String pmCompact;
    private java.text.SimpleDateFormat timeFormat12;
    private java.text.SimpleDateFormat timeFormat24;

    public static class DateTimeAvailableEvent {
    }

    public static class UpdateClock {
    }

    public TimeHelper(android.content.Context context, com.squareup.otto.Bus bus2) {
        this.bus = bus2;
        android.content.res.Resources resources = context.getResources();
        this.pmCompact = resources.getString(com.navdy.hud.app.R.string.pm_marker);
        this.pm = resources.getString(com.navdy.hud.app.R.string.pm);
        this.amCompact = resources.getString(com.navdy.hud.app.R.string.am_marker);
        this.am = resources.getString(com.navdy.hud.app.R.string.am);
        updateLocale();
    }

    public void setFormat(com.navdy.service.library.events.settings.DateTimeConfiguration.Clock format) {
        switch (format) {
            case CLOCK_24_HOUR:
                this.currentTimeFormat = this.timeFormat24;
                break;
            case CLOCK_12_HOUR:
                this.currentTimeFormat = this.timeFormat12;
                break;
        }
        this.calendar = java.util.Calendar.getInstance();
    }

    public com.navdy.service.library.events.settings.DateTimeConfiguration.Clock getFormat() {
        if (this.currentTimeFormat == this.timeFormat12) {
            return com.navdy.service.library.events.settings.DateTimeConfiguration.Clock.CLOCK_12_HOUR;
        }
        return com.navdy.service.library.events.settings.DateTimeConfiguration.Clock.CLOCK_24_HOUR;
    }

    public synchronized java.lang.String formatTime(java.util.Date date, java.lang.StringBuilder amPmMarker) {
        java.lang.String ret;
        ret = this.currentTimeFormat.format(date);
        if (amPmMarker != null) {
            amPmMarker.setLength(0);
            if (this.currentTimeFormat == this.timeFormat12) {
                this.calendar.setTime(date);
                if (this.calendar.get(9) == 1) {
                    amPmMarker.append(this.pmCompact);
                }
            }
        }
        return ret;
    }

    public synchronized java.lang.String formatTime12Hour(java.util.Date date, java.lang.StringBuilder amPmMarker, boolean compact_AM_PM) {
        java.lang.String ret;
        ret = this.timeFormat12.format(date);
        if (amPmMarker != null) {
            amPmMarker.setLength(0);
            this.calendar.setTime(date);
            if (this.calendar.get(9) == 1) {
                if (compact_AM_PM) {
                    amPmMarker.append(this.pmCompact);
                } else {
                    amPmMarker.append(this.pm);
                }
            } else if (compact_AM_PM) {
                amPmMarker.append(this.amCompact);
            } else {
                amPmMarker.append(this.am);
            }
        }
        return ret;
    }

    public synchronized void updateLocale() {
        java.lang.String tz = java.util.TimeZone.getDefault().getID();
        sLogger.v("timezone is " + tz);
        java.util.TimeZone timeZone = java.util.TimeZone.getTimeZone(tz);
        this.dayFormat = new java.text.SimpleDateFormat("EEE");
        this.dayFormat.setTimeZone(timeZone);
        this.dateFormat = new java.text.SimpleDateFormat("d MMM");
        this.dateFormat.setTimeZone(timeZone);
        boolean twelveHour = true;
        if (this.currentTimeFormat == this.timeFormat24) {
            twelveHour = false;
        }
        this.timeFormat24 = new java.text.SimpleDateFormat("H:mm");
        this.timeFormat24.setTimeZone(timeZone);
        this.timeFormat12 = new java.text.SimpleDateFormat("h:mm");
        this.timeFormat12.setTimeZone(timeZone);
        this.calendar = java.util.Calendar.getInstance();
        this.calendar.setTimeZone(timeZone);
        if (twelveHour) {
            this.currentTimeFormat = this.timeFormat12;
        } else {
            this.currentTimeFormat = this.timeFormat24;
        }
        this.bus.post(CLOCK_CHANGED);
    }

    public synchronized java.lang.String getDay() {
        return this.dayFormat.format(new java.util.Date());
    }

    public synchronized java.lang.String getDate() {
        return this.dateFormat.format(new java.util.Date());
    }

    public synchronized java.util.TimeZone getTimeZone() {
        java.lang.String tz;
        tz = java.util.TimeZone.getDefault().getID();
        sLogger.v("timezone is " + tz);
        return java.util.TimeZone.getTimeZone(tz);
    }
}
