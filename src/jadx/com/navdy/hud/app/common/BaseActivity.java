package com.navdy.hud.app.common;

public abstract class BaseActivity extends android.app.Activity {
    private mortar.MortarActivityScope activityScope;
    /* access modifiers changed from: protected */
    public com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(getClass());

    /* access modifiers changed from: protected */
    public void onCreate(android.os.Bundle savedInstanceState) {
        this.logger.v("::OnCreate");
        super.onCreate(savedInstanceState);
        mortar.Blueprint blueprint = getBlueprint();
        if (blueprint != null) {
            this.activityScope = mortar.Mortar.requireActivityScope(mortar.Mortar.getScope(getApplication()), blueprint);
            mortar.Mortar.inject(this, this);
            this.activityScope.onCreate(savedInstanceState);
        }
    }

    /* access modifiers changed from: protected */
    public void attachBaseContext(android.content.Context base) {
        super.attachBaseContext(com.navdy.hud.app.profile.HudLocale.onAttach(base));
    }

    public void onResume() {
        this.logger.v("::onResume");
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        this.logger.v("::onPause");
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.logger.v("::onDestroy");
        super.onDestroy();
        if (isFinishing() && this.activityScope != null) {
            mortar.Mortar.getScope(getApplication()).destroyChild(this.activityScope);
            this.activityScope = null;
        }
    }

    public void onConfigurationChanged(android.content.res.Configuration newConfig) {
        this.logger.v("::onConfigurationChanged");
        super.onConfigurationChanged(newConfig);
        com.navdy.hud.app.profile.HudLocale.onAttach(this);
    }

    public void onSaveInstanceState(android.os.Bundle outState) {
        this.logger.v("::onSaveInstanceState");
        super.onSaveInstanceState(outState);
        if (this.activityScope != null) {
            this.activityScope.onSaveInstanceState(outState);
        }
    }

    public boolean isActivityDestroyed() {
        return isFinishing() || isDestroyed();
    }

    public java.lang.Object getSystemService(java.lang.String name) {
        if (!mortar.Mortar.isScopeSystemService(name)) {
            return super.getSystemService(name);
        }
        if (this.activityScope != null) {
            return this.activityScope;
        }
        throw new java.lang.IllegalArgumentException("BaseActivity must override getBlueprint to use Mortar internally");
    }

    /* access modifiers changed from: protected */
    public mortar.Blueprint getBlueprint() {
        return null;
    }
}
