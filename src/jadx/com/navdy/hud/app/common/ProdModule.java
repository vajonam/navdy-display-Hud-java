package com.navdy.hud.app.common;

@dagger.Module(injects = {com.navdy.hud.app.service.ConnectionServiceProxy.class, com.navdy.hud.app.maps.here.HereMapsManager.class, com.navdy.hud.app.maps.here.HereRegionManager.class, com.navdy.hud.app.obd.ObdManager.class, com.navdy.hud.app.util.OTAUpdateService.class, com.navdy.hud.app.maps.MapsEventHandler.class, com.navdy.hud.app.manager.RemoteDeviceManager.class, com.navdy.hud.app.framework.DriverProfileHelper.class, com.navdy.hud.app.framework.notifications.NotificationManager.class, com.navdy.hud.app.framework.trips.TripManager.class, com.navdy.hud.app.ui.activity.Main.Presenter.DeferredServices.class, com.navdy.hud.app.framework.DriverProfileHelper.class, com.navdy.hud.app.debug.DebugReceiver.class, com.navdy.hud.app.framework.network.NetworkStateManager.class, com.navdy.hud.app.service.ShutdownMonitor.class, com.navdy.hud.app.util.ReportIssueService.class, com.navdy.hud.app.obd.CarMDVinDecoder.class, com.navdy.hud.app.debug.DriveRecorder.class, com.navdy.hud.app.HudApplication.class, com.navdy.hud.app.manager.SpeedManager.class, com.navdy.hud.app.view.MusicWidgetPresenter.class, com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.class, com.navdy.hud.app.manager.UpdateReminderManager.class, com.navdy.hud.app.analytics.AnalyticsSupport.TemperatureReporter.class, com.navdy.hud.app.analytics.AnalyticsSupport.NotificationReceiver.class, com.navdy.hud.app.service.GestureVideosSyncService.class, com.navdy.hud.app.service.ObdCANBusDataUploadService.class, com.navdy.hud.app.view.EngineTemperaturePresenter.class}, library = true)
public class ProdModule {
    public static final int MUSIC_DATA_CACHE_SIZE = 2000000;
    public static final java.lang.String MUSIC_RESPONSE_CACHE = "MUSIC_RESPONSE_CACHE";
    public static final java.lang.String PREF_NAME = "App";
    private android.content.Context context;

    public ProdModule(android.content.Context context2) {
        this.context = context2;
    }

    /* access modifiers changed from: 0000 */
    @javax.inject.Singleton
    @dagger.Provides
    public com.squareup.otto.Bus provideBus() {
        return new com.navdy.hud.app.common.MainThreadBus();
    }

    /* access modifiers changed from: 0000 */
    @javax.inject.Singleton
    @dagger.Provides
    public android.content.SharedPreferences provideSharedPreferences() {
        android.preference.PreferenceManager.setDefaultValues(this.context, PREF_NAME, 0, com.navdy.hud.app.R.xml.developer_preferences, true);
        return this.context.getSharedPreferences(PREF_NAME, 0);
    }

    /* access modifiers changed from: 0000 */
    @javax.inject.Singleton
    @dagger.Provides
    public com.navdy.hud.app.service.ConnectionServiceProxy provideConnectionServiceProxy(com.squareup.otto.Bus bus) {
        return new com.navdy.hud.app.service.ConnectionServiceProxy(this.context, bus);
    }

    /* access modifiers changed from: 0000 */
    @javax.inject.Singleton
    @dagger.Provides
    public com.navdy.hud.app.common.TimeHelper provideTimeHelper(com.squareup.otto.Bus bus) {
        return new com.navdy.hud.app.common.TimeHelper(this.context, bus);
    }

    /* access modifiers changed from: 0000 */
    @javax.inject.Singleton
    @dagger.Provides
    public com.navdy.hud.app.device.PowerManager providePowerManager(com.squareup.otto.Bus bus, android.content.SharedPreferences preferences) {
        return new com.navdy.hud.app.device.PowerManager(bus, this.context, preferences);
    }

    /* access modifiers changed from: 0000 */
    @javax.inject.Singleton
    @dagger.Provides
    public com.navdy.hud.app.config.SettingsManager provideSettingsManager(com.squareup.otto.Bus bus, android.content.SharedPreferences preferences) {
        return new com.navdy.hud.app.config.SettingsManager(bus, preferences);
    }

    /* access modifiers changed from: 0000 */
    @javax.inject.Singleton
    @dagger.Provides
    public com.navdy.hud.app.ancs.AncsServiceConnector provideAncsServiceConnector(com.squareup.otto.Bus bus) {
        return new com.navdy.hud.app.ancs.AncsServiceConnector(this.context, bus);
    }

    /* access modifiers changed from: 0000 */
    @javax.inject.Singleton
    @dagger.Provides
    public com.navdy.hud.app.manager.InputManager provideInputManager(com.squareup.otto.Bus bus, com.navdy.hud.app.device.PowerManager powerManager, com.navdy.hud.app.ui.framework.UIStateManager uiStateManager) {
        return new com.navdy.hud.app.manager.InputManager(bus, powerManager, uiStateManager);
    }

    /* access modifiers changed from: 0000 */
    @javax.inject.Singleton
    @dagger.Provides
    public com.navdy.hud.app.device.dial.DialSimulatorMessagesHandler provideDialSimulatorMessagesHandler() {
        return new com.navdy.hud.app.device.dial.DialSimulatorMessagesHandler(this.context);
    }

    /* access modifiers changed from: 0000 */
    @javax.inject.Singleton
    @dagger.Provides
    public com.navdy.hud.app.framework.phonecall.CallManager provideCallManager(com.squareup.otto.Bus bus) {
        return new com.navdy.hud.app.framework.phonecall.CallManager(bus, this.context);
    }

    /* access modifiers changed from: 0000 */
    @javax.inject.Singleton
    @dagger.Provides
    public com.navdy.hud.app.service.pandora.PandoraManager providePandoraManager(com.squareup.otto.Bus bus) {
        return new com.navdy.hud.app.service.pandora.PandoraManager(bus, this.context.getResources());
    }

    /* access modifiers changed from: 0000 */
    @javax.inject.Singleton
    @dagger.Provides
    public com.navdy.hud.app.manager.MusicManager provideMusicManager(com.navdy.hud.app.storage.cache.MessageCache<com.navdy.service.library.events.audio.MusicCollectionResponse> musicCollectionResponseMessageCache, com.squareup.otto.Bus bus, com.navdy.hud.app.ui.framework.UIStateManager uiStateManager, com.navdy.hud.app.service.pandora.PandoraManager pandoraManager, com.navdy.hud.app.util.MusicArtworkCache artworkCache) {
        return new com.navdy.hud.app.manager.MusicManager(musicCollectionResponseMessageCache, bus, this.context.getResources(), uiStateManager, pandoraManager, artworkCache);
    }

    /* access modifiers changed from: 0000 */
    @javax.inject.Singleton
    @dagger.Provides
    public com.navdy.hud.app.service.ConnectionHandler provideConnectionHandler(com.navdy.hud.app.service.ConnectionServiceProxy connectionServiceProxy, com.navdy.hud.app.device.PowerManager powerManager, com.navdy.hud.app.profile.DriverProfileManager driverProfileManager, com.navdy.hud.app.ui.framework.UIStateManager uiStateManager, com.navdy.hud.app.common.TimeHelper timeHelper) {
        return new com.navdy.hud.app.service.ConnectionHandler(this.context, connectionServiceProxy, powerManager, driverProfileManager, uiStateManager, timeHelper);
    }

    /* access modifiers changed from: 0000 */
    @javax.inject.Singleton
    @dagger.Provides
    public com.navdy.hud.app.gesture.GestureServiceConnector provideGestureServiceConnector(com.squareup.otto.Bus bus, com.navdy.hud.app.device.PowerManager powerManager) {
        return new com.navdy.hud.app.gesture.GestureServiceConnector(bus, powerManager);
    }

    /* access modifiers changed from: 0000 */
    @javax.inject.Singleton
    @dagger.Provides
    public com.navdy.hud.app.ui.framework.UIStateManager provideUIStateManager() {
        return new com.navdy.hud.app.ui.framework.UIStateManager();
    }

    /* access modifiers changed from: 0000 */
    @javax.inject.Singleton
    @dagger.Provides
    public com.navdy.hud.app.storage.PathManager providePathManager() {
        return com.navdy.hud.app.storage.PathManager.getInstance();
    }

    /* access modifiers changed from: 0000 */
    @javax.inject.Singleton
    @dagger.Provides
    public com.navdy.hud.app.profile.DriverProfileManager provideDriverProfileManager(com.squareup.otto.Bus bus, com.navdy.hud.app.storage.PathManager pathManager, com.navdy.hud.app.common.TimeHelper timeHelper) {
        return new com.navdy.hud.app.profile.DriverProfileManager(bus, pathManager, timeHelper);
    }

    /* access modifiers changed from: 0000 */
    @javax.inject.Singleton
    @dagger.Provides
    public com.navdy.hud.app.framework.trips.TripManager provideTripManager(com.squareup.otto.Bus bus, android.content.SharedPreferences preferences) {
        return new com.navdy.hud.app.framework.trips.TripManager(bus, preferences);
    }

    /* access modifiers changed from: 0000 */
    @javax.inject.Singleton
    @dagger.Provides
    public com.navdy.hud.app.manager.PairingManager providePairingManager() {
        return new com.navdy.hud.app.manager.PairingManager();
    }

    /* access modifiers changed from: 0000 */
    @javax.inject.Singleton
    @dagger.Provides
    public com.navdy.service.library.network.http.IHttpManager provideHttpManager() {
        return new com.navdy.service.library.network.http.HttpManager();
    }

    /* access modifiers changed from: 0000 */
    @javax.inject.Singleton
    @dagger.Provides
    public com.navdy.hud.app.debug.DriveRecorder provideObdDataReceorder(com.squareup.otto.Bus bus, com.navdy.hud.app.service.ConnectionHandler connectionHandler) {
        return new com.navdy.hud.app.debug.DriveRecorder(bus, connectionHandler);
    }

    /* access modifiers changed from: 0000 */
    @javax.inject.Singleton
    @dagger.Provides
    public com.navdy.hud.app.util.MusicArtworkCache provideArtworkCache() {
        return new com.navdy.hud.app.util.MusicArtworkCache();
    }

    /* access modifiers changed from: 0000 */
    @javax.inject.Singleton
    @dagger.Provides
    public com.navdy.hud.app.framework.voice.VoiceSearchHandler provideVoiceSearchHandler(com.squareup.otto.Bus bus, com.navdy.hud.app.util.FeatureUtil featureUtil) {
        return new com.navdy.hud.app.framework.voice.VoiceSearchHandler(this.context, bus, featureUtil);
    }

    /* access modifiers changed from: 0000 */
    @javax.inject.Singleton
    @dagger.Provides
    public com.navdy.hud.app.util.FeatureUtil provideFeatureUtil(android.content.SharedPreferences sharedPreferences) {
        return new com.navdy.hud.app.util.FeatureUtil(sharedPreferences);
    }

    /* access modifiers changed from: 0000 */
    @javax.inject.Singleton
    @dagger.Provides
    public com.navdy.hud.app.analytics.TelemetryDataManager provideTelemetryDataManager(com.navdy.hud.app.ui.framework.UIStateManager uiStateManager, com.navdy.hud.app.device.PowerManager powerManager, com.squareup.otto.Bus bus, android.content.SharedPreferences sharedPreferences, com.navdy.hud.app.framework.trips.TripManager tripManager) {
        return new com.navdy.hud.app.analytics.TelemetryDataManager(uiStateManager, powerManager, bus, sharedPreferences, tripManager);
    }

    /* access modifiers changed from: 0000 */
    @javax.inject.Singleton
    @dagger.Provides
    public com.navdy.hud.app.storage.cache.MessageCache<com.navdy.service.library.events.audio.MusicCollectionResponse> provideMusicCollectionResponseMessageCache(com.navdy.hud.app.storage.PathManager pathManager) {
        return new com.navdy.hud.app.storage.cache.MessageCache<>(new com.navdy.hud.app.storage.cache.DiskLruCacheAdapter(MUSIC_RESPONSE_CACHE, pathManager.getMusicDiskCacheFolder(), MUSIC_DATA_CACHE_SIZE), com.navdy.service.library.events.audio.MusicCollectionResponse.class);
    }
}
