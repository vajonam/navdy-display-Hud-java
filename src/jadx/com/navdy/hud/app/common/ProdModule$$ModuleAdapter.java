package com.navdy.hud.app.common;

public final class ProdModule$$ModuleAdapter extends dagger.internal.ModuleAdapter<com.navdy.hud.app.common.ProdModule> {
    private static final java.lang.Class<?>[] INCLUDES = new java.lang.Class[0];
    private static final java.lang.String[] INJECTS = {"members/com.navdy.hud.app.service.ConnectionServiceProxy", "members/com.navdy.hud.app.maps.here.HereMapsManager", "members/com.navdy.hud.app.maps.here.HereRegionManager", "members/com.navdy.hud.app.obd.ObdManager", "members/com.navdy.hud.app.util.OTAUpdateService", "members/com.navdy.hud.app.maps.MapsEventHandler", "members/com.navdy.hud.app.manager.RemoteDeviceManager", "members/com.navdy.hud.app.framework.DriverProfileHelper", "members/com.navdy.hud.app.framework.notifications.NotificationManager", "members/com.navdy.hud.app.framework.trips.TripManager", "members/com.navdy.hud.app.ui.activity.Main$Presenter$DeferredServices", "members/com.navdy.hud.app.framework.DriverProfileHelper", "members/com.navdy.hud.app.debug.DebugReceiver", "members/com.navdy.hud.app.framework.network.NetworkStateManager", "members/com.navdy.hud.app.service.ShutdownMonitor", "members/com.navdy.hud.app.util.ReportIssueService", "members/com.navdy.hud.app.obd.CarMDVinDecoder", "members/com.navdy.hud.app.debug.DriveRecorder", "members/com.navdy.hud.app.HudApplication", "members/com.navdy.hud.app.manager.SpeedManager", "members/com.navdy.hud.app.view.MusicWidgetPresenter", "members/com.navdy.hud.app.ui.component.mainmenu.MusicMenu2", "members/com.navdy.hud.app.manager.UpdateReminderManager", "members/com.navdy.hud.app.analytics.AnalyticsSupport$TemperatureReporter", "members/com.navdy.hud.app.analytics.AnalyticsSupport$NotificationReceiver", "members/com.navdy.hud.app.service.GestureVideosSyncService", "members/com.navdy.hud.app.service.ObdCANBusDataUploadService", "members/com.navdy.hud.app.view.EngineTemperaturePresenter"};
    private static final java.lang.Class<?>[] STATIC_INJECTIONS = new java.lang.Class[0];

    /* compiled from: ProdModule$$ModuleAdapter */
    public static final class ProvideAncsServiceConnectorProvidesAdapter extends dagger.internal.ProvidesBinding<com.navdy.hud.app.ancs.AncsServiceConnector> implements javax.inject.Provider<com.navdy.hud.app.ancs.AncsServiceConnector> {
        private dagger.internal.Binding<com.squareup.otto.Bus> bus;
        private final com.navdy.hud.app.common.ProdModule module;

        public ProvideAncsServiceConnectorProvidesAdapter(com.navdy.hud.app.common.ProdModule module2) {
            super("com.navdy.hud.app.ancs.AncsServiceConnector", true, "com.navdy.hud.app.common.ProdModule", "provideAncsServiceConnector");
            this.module = module2;
            setLibrary(true);
        }

        public void attach(dagger.internal.Linker linker) {
            this.bus = linker.requestBinding("com.squareup.otto.Bus", com.navdy.hud.app.common.ProdModule.class, getClass().getClassLoader());
        }

        public void getDependencies(java.util.Set<dagger.internal.Binding<?>> getBindings, java.util.Set<dagger.internal.Binding<?>> set) {
            getBindings.add(this.bus);
        }

        public com.navdy.hud.app.ancs.AncsServiceConnector get() {
            return this.module.provideAncsServiceConnector((com.squareup.otto.Bus) this.bus.get());
        }
    }

    /* compiled from: ProdModule$$ModuleAdapter */
    public static final class ProvideArtworkCacheProvidesAdapter extends dagger.internal.ProvidesBinding<com.navdy.hud.app.util.MusicArtworkCache> implements javax.inject.Provider<com.navdy.hud.app.util.MusicArtworkCache> {
        private final com.navdy.hud.app.common.ProdModule module;

        public ProvideArtworkCacheProvidesAdapter(com.navdy.hud.app.common.ProdModule module2) {
            super("com.navdy.hud.app.util.MusicArtworkCache", true, "com.navdy.hud.app.common.ProdModule", "provideArtworkCache");
            this.module = module2;
            setLibrary(true);
        }

        public com.navdy.hud.app.util.MusicArtworkCache get() {
            return this.module.provideArtworkCache();
        }
    }

    /* compiled from: ProdModule$$ModuleAdapter */
    public static final class ProvideBusProvidesAdapter extends dagger.internal.ProvidesBinding<com.squareup.otto.Bus> implements javax.inject.Provider<com.squareup.otto.Bus> {
        private final com.navdy.hud.app.common.ProdModule module;

        public ProvideBusProvidesAdapter(com.navdy.hud.app.common.ProdModule module2) {
            super("com.squareup.otto.Bus", true, "com.navdy.hud.app.common.ProdModule", "provideBus");
            this.module = module2;
            setLibrary(true);
        }

        public com.squareup.otto.Bus get() {
            return this.module.provideBus();
        }
    }

    /* compiled from: ProdModule$$ModuleAdapter */
    public static final class ProvideCallManagerProvidesAdapter extends dagger.internal.ProvidesBinding<com.navdy.hud.app.framework.phonecall.CallManager> implements javax.inject.Provider<com.navdy.hud.app.framework.phonecall.CallManager> {
        private dagger.internal.Binding<com.squareup.otto.Bus> bus;
        private final com.navdy.hud.app.common.ProdModule module;

        public ProvideCallManagerProvidesAdapter(com.navdy.hud.app.common.ProdModule module2) {
            super("com.navdy.hud.app.framework.phonecall.CallManager", true, "com.navdy.hud.app.common.ProdModule", "provideCallManager");
            this.module = module2;
            setLibrary(true);
        }

        public void attach(dagger.internal.Linker linker) {
            this.bus = linker.requestBinding("com.squareup.otto.Bus", com.navdy.hud.app.common.ProdModule.class, getClass().getClassLoader());
        }

        public void getDependencies(java.util.Set<dagger.internal.Binding<?>> getBindings, java.util.Set<dagger.internal.Binding<?>> set) {
            getBindings.add(this.bus);
        }

        public com.navdy.hud.app.framework.phonecall.CallManager get() {
            return this.module.provideCallManager((com.squareup.otto.Bus) this.bus.get());
        }
    }

    /* compiled from: ProdModule$$ModuleAdapter */
    public static final class ProvideConnectionHandlerProvidesAdapter extends dagger.internal.ProvidesBinding<com.navdy.hud.app.service.ConnectionHandler> implements javax.inject.Provider<com.navdy.hud.app.service.ConnectionHandler> {
        private dagger.internal.Binding<com.navdy.hud.app.service.ConnectionServiceProxy> connectionServiceProxy;
        private dagger.internal.Binding<com.navdy.hud.app.profile.DriverProfileManager> driverProfileManager;
        private final com.navdy.hud.app.common.ProdModule module;
        private dagger.internal.Binding<com.navdy.hud.app.device.PowerManager> powerManager;
        private dagger.internal.Binding<com.navdy.hud.app.common.TimeHelper> timeHelper;
        private dagger.internal.Binding<com.navdy.hud.app.ui.framework.UIStateManager> uiStateManager;

        public ProvideConnectionHandlerProvidesAdapter(com.navdy.hud.app.common.ProdModule module2) {
            super("com.navdy.hud.app.service.ConnectionHandler", true, "com.navdy.hud.app.common.ProdModule", "provideConnectionHandler");
            this.module = module2;
            setLibrary(true);
        }

        public void attach(dagger.internal.Linker linker) {
            this.connectionServiceProxy = linker.requestBinding("com.navdy.hud.app.service.ConnectionServiceProxy", com.navdy.hud.app.common.ProdModule.class, getClass().getClassLoader());
            this.powerManager = linker.requestBinding("com.navdy.hud.app.device.PowerManager", com.navdy.hud.app.common.ProdModule.class, getClass().getClassLoader());
            this.driverProfileManager = linker.requestBinding("com.navdy.hud.app.profile.DriverProfileManager", com.navdy.hud.app.common.ProdModule.class, getClass().getClassLoader());
            this.uiStateManager = linker.requestBinding("com.navdy.hud.app.ui.framework.UIStateManager", com.navdy.hud.app.common.ProdModule.class, getClass().getClassLoader());
            this.timeHelper = linker.requestBinding("com.navdy.hud.app.common.TimeHelper", com.navdy.hud.app.common.ProdModule.class, getClass().getClassLoader());
        }

        public void getDependencies(java.util.Set<dagger.internal.Binding<?>> getBindings, java.util.Set<dagger.internal.Binding<?>> set) {
            getBindings.add(this.connectionServiceProxy);
            getBindings.add(this.powerManager);
            getBindings.add(this.driverProfileManager);
            getBindings.add(this.uiStateManager);
            getBindings.add(this.timeHelper);
        }

        public com.navdy.hud.app.service.ConnectionHandler get() {
            return this.module.provideConnectionHandler((com.navdy.hud.app.service.ConnectionServiceProxy) this.connectionServiceProxy.get(), (com.navdy.hud.app.device.PowerManager) this.powerManager.get(), (com.navdy.hud.app.profile.DriverProfileManager) this.driverProfileManager.get(), (com.navdy.hud.app.ui.framework.UIStateManager) this.uiStateManager.get(), (com.navdy.hud.app.common.TimeHelper) this.timeHelper.get());
        }
    }

    /* compiled from: ProdModule$$ModuleAdapter */
    public static final class ProvideConnectionServiceProxyProvidesAdapter extends dagger.internal.ProvidesBinding<com.navdy.hud.app.service.ConnectionServiceProxy> implements javax.inject.Provider<com.navdy.hud.app.service.ConnectionServiceProxy> {
        private dagger.internal.Binding<com.squareup.otto.Bus> bus;
        private final com.navdy.hud.app.common.ProdModule module;

        public ProvideConnectionServiceProxyProvidesAdapter(com.navdy.hud.app.common.ProdModule module2) {
            super("com.navdy.hud.app.service.ConnectionServiceProxy", true, "com.navdy.hud.app.common.ProdModule", "provideConnectionServiceProxy");
            this.module = module2;
            setLibrary(true);
        }

        public void attach(dagger.internal.Linker linker) {
            this.bus = linker.requestBinding("com.squareup.otto.Bus", com.navdy.hud.app.common.ProdModule.class, getClass().getClassLoader());
        }

        public void getDependencies(java.util.Set<dagger.internal.Binding<?>> getBindings, java.util.Set<dagger.internal.Binding<?>> set) {
            getBindings.add(this.bus);
        }

        public com.navdy.hud.app.service.ConnectionServiceProxy get() {
            return this.module.provideConnectionServiceProxy((com.squareup.otto.Bus) this.bus.get());
        }
    }

    /* compiled from: ProdModule$$ModuleAdapter */
    public static final class ProvideDialSimulatorMessagesHandlerProvidesAdapter extends dagger.internal.ProvidesBinding<com.navdy.hud.app.device.dial.DialSimulatorMessagesHandler> implements javax.inject.Provider<com.navdy.hud.app.device.dial.DialSimulatorMessagesHandler> {
        private final com.navdy.hud.app.common.ProdModule module;

        public ProvideDialSimulatorMessagesHandlerProvidesAdapter(com.navdy.hud.app.common.ProdModule module2) {
            super("com.navdy.hud.app.device.dial.DialSimulatorMessagesHandler", true, "com.navdy.hud.app.common.ProdModule", "provideDialSimulatorMessagesHandler");
            this.module = module2;
            setLibrary(true);
        }

        public com.navdy.hud.app.device.dial.DialSimulatorMessagesHandler get() {
            return this.module.provideDialSimulatorMessagesHandler();
        }
    }

    /* compiled from: ProdModule$$ModuleAdapter */
    public static final class ProvideDriverProfileManagerProvidesAdapter extends dagger.internal.ProvidesBinding<com.navdy.hud.app.profile.DriverProfileManager> implements javax.inject.Provider<com.navdy.hud.app.profile.DriverProfileManager> {
        private dagger.internal.Binding<com.squareup.otto.Bus> bus;
        private final com.navdy.hud.app.common.ProdModule module;
        private dagger.internal.Binding<com.navdy.hud.app.storage.PathManager> pathManager;
        private dagger.internal.Binding<com.navdy.hud.app.common.TimeHelper> timeHelper;

        public ProvideDriverProfileManagerProvidesAdapter(com.navdy.hud.app.common.ProdModule module2) {
            super("com.navdy.hud.app.profile.DriverProfileManager", true, "com.navdy.hud.app.common.ProdModule", "provideDriverProfileManager");
            this.module = module2;
            setLibrary(true);
        }

        public void attach(dagger.internal.Linker linker) {
            this.bus = linker.requestBinding("com.squareup.otto.Bus", com.navdy.hud.app.common.ProdModule.class, getClass().getClassLoader());
            this.pathManager = linker.requestBinding("com.navdy.hud.app.storage.PathManager", com.navdy.hud.app.common.ProdModule.class, getClass().getClassLoader());
            this.timeHelper = linker.requestBinding("com.navdy.hud.app.common.TimeHelper", com.navdy.hud.app.common.ProdModule.class, getClass().getClassLoader());
        }

        public void getDependencies(java.util.Set<dagger.internal.Binding<?>> getBindings, java.util.Set<dagger.internal.Binding<?>> set) {
            getBindings.add(this.bus);
            getBindings.add(this.pathManager);
            getBindings.add(this.timeHelper);
        }

        public com.navdy.hud.app.profile.DriverProfileManager get() {
            return this.module.provideDriverProfileManager((com.squareup.otto.Bus) this.bus.get(), (com.navdy.hud.app.storage.PathManager) this.pathManager.get(), (com.navdy.hud.app.common.TimeHelper) this.timeHelper.get());
        }
    }

    /* compiled from: ProdModule$$ModuleAdapter */
    public static final class ProvideFeatureUtilProvidesAdapter extends dagger.internal.ProvidesBinding<com.navdy.hud.app.util.FeatureUtil> implements javax.inject.Provider<com.navdy.hud.app.util.FeatureUtil> {
        private final com.navdy.hud.app.common.ProdModule module;
        private dagger.internal.Binding<android.content.SharedPreferences> sharedPreferences;

        public ProvideFeatureUtilProvidesAdapter(com.navdy.hud.app.common.ProdModule module2) {
            super("com.navdy.hud.app.util.FeatureUtil", true, "com.navdy.hud.app.common.ProdModule", "provideFeatureUtil");
            this.module = module2;
            setLibrary(true);
        }

        public void attach(dagger.internal.Linker linker) {
            this.sharedPreferences = linker.requestBinding("android.content.SharedPreferences", com.navdy.hud.app.common.ProdModule.class, getClass().getClassLoader());
        }

        public void getDependencies(java.util.Set<dagger.internal.Binding<?>> getBindings, java.util.Set<dagger.internal.Binding<?>> set) {
            getBindings.add(this.sharedPreferences);
        }

        public com.navdy.hud.app.util.FeatureUtil get() {
            return this.module.provideFeatureUtil((android.content.SharedPreferences) this.sharedPreferences.get());
        }
    }

    /* compiled from: ProdModule$$ModuleAdapter */
    public static final class ProvideGestureServiceConnectorProvidesAdapter extends dagger.internal.ProvidesBinding<com.navdy.hud.app.gesture.GestureServiceConnector> implements javax.inject.Provider<com.navdy.hud.app.gesture.GestureServiceConnector> {
        private dagger.internal.Binding<com.squareup.otto.Bus> bus;
        private final com.navdy.hud.app.common.ProdModule module;
        private dagger.internal.Binding<com.navdy.hud.app.device.PowerManager> powerManager;

        public ProvideGestureServiceConnectorProvidesAdapter(com.navdy.hud.app.common.ProdModule module2) {
            super("com.navdy.hud.app.gesture.GestureServiceConnector", true, "com.navdy.hud.app.common.ProdModule", "provideGestureServiceConnector");
            this.module = module2;
            setLibrary(true);
        }

        public void attach(dagger.internal.Linker linker) {
            this.bus = linker.requestBinding("com.squareup.otto.Bus", com.navdy.hud.app.common.ProdModule.class, getClass().getClassLoader());
            this.powerManager = linker.requestBinding("com.navdy.hud.app.device.PowerManager", com.navdy.hud.app.common.ProdModule.class, getClass().getClassLoader());
        }

        public void getDependencies(java.util.Set<dagger.internal.Binding<?>> getBindings, java.util.Set<dagger.internal.Binding<?>> set) {
            getBindings.add(this.bus);
            getBindings.add(this.powerManager);
        }

        public com.navdy.hud.app.gesture.GestureServiceConnector get() {
            return this.module.provideGestureServiceConnector((com.squareup.otto.Bus) this.bus.get(), (com.navdy.hud.app.device.PowerManager) this.powerManager.get());
        }
    }

    /* compiled from: ProdModule$$ModuleAdapter */
    public static final class ProvideHttpManagerProvidesAdapter extends dagger.internal.ProvidesBinding<com.navdy.service.library.network.http.IHttpManager> implements javax.inject.Provider<com.navdy.service.library.network.http.IHttpManager> {
        private final com.navdy.hud.app.common.ProdModule module;

        public ProvideHttpManagerProvidesAdapter(com.navdy.hud.app.common.ProdModule module2) {
            super("com.navdy.service.library.network.http.IHttpManager", true, "com.navdy.hud.app.common.ProdModule", "provideHttpManager");
            this.module = module2;
            setLibrary(true);
        }

        public com.navdy.service.library.network.http.IHttpManager get() {
            return this.module.provideHttpManager();
        }
    }

    /* compiled from: ProdModule$$ModuleAdapter */
    public static final class ProvideInputManagerProvidesAdapter extends dagger.internal.ProvidesBinding<com.navdy.hud.app.manager.InputManager> implements javax.inject.Provider<com.navdy.hud.app.manager.InputManager> {
        private dagger.internal.Binding<com.squareup.otto.Bus> bus;
        private final com.navdy.hud.app.common.ProdModule module;
        private dagger.internal.Binding<com.navdy.hud.app.device.PowerManager> powerManager;
        private dagger.internal.Binding<com.navdy.hud.app.ui.framework.UIStateManager> uiStateManager;

        public ProvideInputManagerProvidesAdapter(com.navdy.hud.app.common.ProdModule module2) {
            super("com.navdy.hud.app.manager.InputManager", true, "com.navdy.hud.app.common.ProdModule", "provideInputManager");
            this.module = module2;
            setLibrary(true);
        }

        public void attach(dagger.internal.Linker linker) {
            this.bus = linker.requestBinding("com.squareup.otto.Bus", com.navdy.hud.app.common.ProdModule.class, getClass().getClassLoader());
            this.powerManager = linker.requestBinding("com.navdy.hud.app.device.PowerManager", com.navdy.hud.app.common.ProdModule.class, getClass().getClassLoader());
            this.uiStateManager = linker.requestBinding("com.navdy.hud.app.ui.framework.UIStateManager", com.navdy.hud.app.common.ProdModule.class, getClass().getClassLoader());
        }

        public void getDependencies(java.util.Set<dagger.internal.Binding<?>> getBindings, java.util.Set<dagger.internal.Binding<?>> set) {
            getBindings.add(this.bus);
            getBindings.add(this.powerManager);
            getBindings.add(this.uiStateManager);
        }

        public com.navdy.hud.app.manager.InputManager get() {
            return this.module.provideInputManager((com.squareup.otto.Bus) this.bus.get(), (com.navdy.hud.app.device.PowerManager) this.powerManager.get(), (com.navdy.hud.app.ui.framework.UIStateManager) this.uiStateManager.get());
        }
    }

    /* compiled from: ProdModule$$ModuleAdapter */
    public static final class ProvideMusicCollectionResponseMessageCacheProvidesAdapter extends dagger.internal.ProvidesBinding<com.navdy.hud.app.storage.cache.MessageCache<com.navdy.service.library.events.audio.MusicCollectionResponse>> implements javax.inject.Provider<com.navdy.hud.app.storage.cache.MessageCache<com.navdy.service.library.events.audio.MusicCollectionResponse>> {
        private final com.navdy.hud.app.common.ProdModule module;
        private dagger.internal.Binding<com.navdy.hud.app.storage.PathManager> pathManager;

        public ProvideMusicCollectionResponseMessageCacheProvidesAdapter(com.navdy.hud.app.common.ProdModule module2) {
            super("com.navdy.hud.app.storage.cache.MessageCache<com.navdy.service.library.events.audio.MusicCollectionResponse>", true, "com.navdy.hud.app.common.ProdModule", "provideMusicCollectionResponseMessageCache");
            this.module = module2;
            setLibrary(true);
        }

        public void attach(dagger.internal.Linker linker) {
            this.pathManager = linker.requestBinding("com.navdy.hud.app.storage.PathManager", com.navdy.hud.app.common.ProdModule.class, getClass().getClassLoader());
        }

        public void getDependencies(java.util.Set<dagger.internal.Binding<?>> getBindings, java.util.Set<dagger.internal.Binding<?>> set) {
            getBindings.add(this.pathManager);
        }

        public com.navdy.hud.app.storage.cache.MessageCache<com.navdy.service.library.events.audio.MusicCollectionResponse> get() {
            return this.module.provideMusicCollectionResponseMessageCache((com.navdy.hud.app.storage.PathManager) this.pathManager.get());
        }
    }

    /* compiled from: ProdModule$$ModuleAdapter */
    public static final class ProvideMusicManagerProvidesAdapter extends dagger.internal.ProvidesBinding<com.navdy.hud.app.manager.MusicManager> implements javax.inject.Provider<com.navdy.hud.app.manager.MusicManager> {
        private dagger.internal.Binding<com.navdy.hud.app.util.MusicArtworkCache> artworkCache;
        private dagger.internal.Binding<com.squareup.otto.Bus> bus;
        private final com.navdy.hud.app.common.ProdModule module;
        private dagger.internal.Binding<com.navdy.hud.app.storage.cache.MessageCache<com.navdy.service.library.events.audio.MusicCollectionResponse>> musicCollectionResponseMessageCache;
        private dagger.internal.Binding<com.navdy.hud.app.service.pandora.PandoraManager> pandoraManager;
        private dagger.internal.Binding<com.navdy.hud.app.ui.framework.UIStateManager> uiStateManager;

        public ProvideMusicManagerProvidesAdapter(com.navdy.hud.app.common.ProdModule module2) {
            super("com.navdy.hud.app.manager.MusicManager", true, "com.navdy.hud.app.common.ProdModule", "provideMusicManager");
            this.module = module2;
            setLibrary(true);
        }

        public void attach(dagger.internal.Linker linker) {
            this.musicCollectionResponseMessageCache = linker.requestBinding("com.navdy.hud.app.storage.cache.MessageCache<com.navdy.service.library.events.audio.MusicCollectionResponse>", com.navdy.hud.app.common.ProdModule.class, getClass().getClassLoader());
            this.bus = linker.requestBinding("com.squareup.otto.Bus", com.navdy.hud.app.common.ProdModule.class, getClass().getClassLoader());
            this.uiStateManager = linker.requestBinding("com.navdy.hud.app.ui.framework.UIStateManager", com.navdy.hud.app.common.ProdModule.class, getClass().getClassLoader());
            this.pandoraManager = linker.requestBinding("com.navdy.hud.app.service.pandora.PandoraManager", com.navdy.hud.app.common.ProdModule.class, getClass().getClassLoader());
            this.artworkCache = linker.requestBinding("com.navdy.hud.app.util.MusicArtworkCache", com.navdy.hud.app.common.ProdModule.class, getClass().getClassLoader());
        }

        public void getDependencies(java.util.Set<dagger.internal.Binding<?>> getBindings, java.util.Set<dagger.internal.Binding<?>> set) {
            getBindings.add(this.musicCollectionResponseMessageCache);
            getBindings.add(this.bus);
            getBindings.add(this.uiStateManager);
            getBindings.add(this.pandoraManager);
            getBindings.add(this.artworkCache);
        }

        public com.navdy.hud.app.manager.MusicManager get() {
            return this.module.provideMusicManager((com.navdy.hud.app.storage.cache.MessageCache) this.musicCollectionResponseMessageCache.get(), (com.squareup.otto.Bus) this.bus.get(), (com.navdy.hud.app.ui.framework.UIStateManager) this.uiStateManager.get(), (com.navdy.hud.app.service.pandora.PandoraManager) this.pandoraManager.get(), (com.navdy.hud.app.util.MusicArtworkCache) this.artworkCache.get());
        }
    }

    /* compiled from: ProdModule$$ModuleAdapter */
    public static final class ProvideObdDataReceorderProvidesAdapter extends dagger.internal.ProvidesBinding<com.navdy.hud.app.debug.DriveRecorder> implements javax.inject.Provider<com.navdy.hud.app.debug.DriveRecorder> {
        private dagger.internal.Binding<com.squareup.otto.Bus> bus;
        private dagger.internal.Binding<com.navdy.hud.app.service.ConnectionHandler> connectionHandler;
        private final com.navdy.hud.app.common.ProdModule module;

        public ProvideObdDataReceorderProvidesAdapter(com.navdy.hud.app.common.ProdModule module2) {
            super("com.navdy.hud.app.debug.DriveRecorder", true, "com.navdy.hud.app.common.ProdModule", "provideObdDataReceorder");
            this.module = module2;
            setLibrary(true);
        }

        public void attach(dagger.internal.Linker linker) {
            this.bus = linker.requestBinding("com.squareup.otto.Bus", com.navdy.hud.app.common.ProdModule.class, getClass().getClassLoader());
            this.connectionHandler = linker.requestBinding("com.navdy.hud.app.service.ConnectionHandler", com.navdy.hud.app.common.ProdModule.class, getClass().getClassLoader());
        }

        public void getDependencies(java.util.Set<dagger.internal.Binding<?>> getBindings, java.util.Set<dagger.internal.Binding<?>> set) {
            getBindings.add(this.bus);
            getBindings.add(this.connectionHandler);
        }

        public com.navdy.hud.app.debug.DriveRecorder get() {
            return this.module.provideObdDataReceorder((com.squareup.otto.Bus) this.bus.get(), (com.navdy.hud.app.service.ConnectionHandler) this.connectionHandler.get());
        }
    }

    /* compiled from: ProdModule$$ModuleAdapter */
    public static final class ProvidePairingManagerProvidesAdapter extends dagger.internal.ProvidesBinding<com.navdy.hud.app.manager.PairingManager> implements javax.inject.Provider<com.navdy.hud.app.manager.PairingManager> {
        private final com.navdy.hud.app.common.ProdModule module;

        public ProvidePairingManagerProvidesAdapter(com.navdy.hud.app.common.ProdModule module2) {
            super("com.navdy.hud.app.manager.PairingManager", true, "com.navdy.hud.app.common.ProdModule", "providePairingManager");
            this.module = module2;
            setLibrary(true);
        }

        public com.navdy.hud.app.manager.PairingManager get() {
            return this.module.providePairingManager();
        }
    }

    /* compiled from: ProdModule$$ModuleAdapter */
    public static final class ProvidePandoraManagerProvidesAdapter extends dagger.internal.ProvidesBinding<com.navdy.hud.app.service.pandora.PandoraManager> implements javax.inject.Provider<com.navdy.hud.app.service.pandora.PandoraManager> {
        private dagger.internal.Binding<com.squareup.otto.Bus> bus;
        private final com.navdy.hud.app.common.ProdModule module;

        public ProvidePandoraManagerProvidesAdapter(com.navdy.hud.app.common.ProdModule module2) {
            super("com.navdy.hud.app.service.pandora.PandoraManager", true, "com.navdy.hud.app.common.ProdModule", "providePandoraManager");
            this.module = module2;
            setLibrary(true);
        }

        public void attach(dagger.internal.Linker linker) {
            this.bus = linker.requestBinding("com.squareup.otto.Bus", com.navdy.hud.app.common.ProdModule.class, getClass().getClassLoader());
        }

        public void getDependencies(java.util.Set<dagger.internal.Binding<?>> getBindings, java.util.Set<dagger.internal.Binding<?>> set) {
            getBindings.add(this.bus);
        }

        public com.navdy.hud.app.service.pandora.PandoraManager get() {
            return this.module.providePandoraManager((com.squareup.otto.Bus) this.bus.get());
        }
    }

    /* compiled from: ProdModule$$ModuleAdapter */
    public static final class ProvidePathManagerProvidesAdapter extends dagger.internal.ProvidesBinding<com.navdy.hud.app.storage.PathManager> implements javax.inject.Provider<com.navdy.hud.app.storage.PathManager> {
        private final com.navdy.hud.app.common.ProdModule module;

        public ProvidePathManagerProvidesAdapter(com.navdy.hud.app.common.ProdModule module2) {
            super("com.navdy.hud.app.storage.PathManager", true, "com.navdy.hud.app.common.ProdModule", "providePathManager");
            this.module = module2;
            setLibrary(true);
        }

        public com.navdy.hud.app.storage.PathManager get() {
            return this.module.providePathManager();
        }
    }

    /* compiled from: ProdModule$$ModuleAdapter */
    public static final class ProvidePowerManagerProvidesAdapter extends dagger.internal.ProvidesBinding<com.navdy.hud.app.device.PowerManager> implements javax.inject.Provider<com.navdy.hud.app.device.PowerManager> {
        private dagger.internal.Binding<com.squareup.otto.Bus> bus;
        private final com.navdy.hud.app.common.ProdModule module;
        private dagger.internal.Binding<android.content.SharedPreferences> preferences;

        public ProvidePowerManagerProvidesAdapter(com.navdy.hud.app.common.ProdModule module2) {
            super("com.navdy.hud.app.device.PowerManager", true, "com.navdy.hud.app.common.ProdModule", "providePowerManager");
            this.module = module2;
            setLibrary(true);
        }

        public void attach(dagger.internal.Linker linker) {
            this.bus = linker.requestBinding("com.squareup.otto.Bus", com.navdy.hud.app.common.ProdModule.class, getClass().getClassLoader());
            this.preferences = linker.requestBinding("android.content.SharedPreferences", com.navdy.hud.app.common.ProdModule.class, getClass().getClassLoader());
        }

        public void getDependencies(java.util.Set<dagger.internal.Binding<?>> getBindings, java.util.Set<dagger.internal.Binding<?>> set) {
            getBindings.add(this.bus);
            getBindings.add(this.preferences);
        }

        public com.navdy.hud.app.device.PowerManager get() {
            return this.module.providePowerManager((com.squareup.otto.Bus) this.bus.get(), (android.content.SharedPreferences) this.preferences.get());
        }
    }

    /* compiled from: ProdModule$$ModuleAdapter */
    public static final class ProvideSettingsManagerProvidesAdapter extends dagger.internal.ProvidesBinding<com.navdy.hud.app.config.SettingsManager> implements javax.inject.Provider<com.navdy.hud.app.config.SettingsManager> {
        private dagger.internal.Binding<com.squareup.otto.Bus> bus;
        private final com.navdy.hud.app.common.ProdModule module;
        private dagger.internal.Binding<android.content.SharedPreferences> preferences;

        public ProvideSettingsManagerProvidesAdapter(com.navdy.hud.app.common.ProdModule module2) {
            super("com.navdy.hud.app.config.SettingsManager", true, "com.navdy.hud.app.common.ProdModule", "provideSettingsManager");
            this.module = module2;
            setLibrary(true);
        }

        public void attach(dagger.internal.Linker linker) {
            this.bus = linker.requestBinding("com.squareup.otto.Bus", com.navdy.hud.app.common.ProdModule.class, getClass().getClassLoader());
            this.preferences = linker.requestBinding("android.content.SharedPreferences", com.navdy.hud.app.common.ProdModule.class, getClass().getClassLoader());
        }

        public void getDependencies(java.util.Set<dagger.internal.Binding<?>> getBindings, java.util.Set<dagger.internal.Binding<?>> set) {
            getBindings.add(this.bus);
            getBindings.add(this.preferences);
        }

        public com.navdy.hud.app.config.SettingsManager get() {
            return this.module.provideSettingsManager((com.squareup.otto.Bus) this.bus.get(), (android.content.SharedPreferences) this.preferences.get());
        }
    }

    /* compiled from: ProdModule$$ModuleAdapter */
    public static final class ProvideSharedPreferencesProvidesAdapter extends dagger.internal.ProvidesBinding<android.content.SharedPreferences> implements javax.inject.Provider<android.content.SharedPreferences> {
        private final com.navdy.hud.app.common.ProdModule module;

        public ProvideSharedPreferencesProvidesAdapter(com.navdy.hud.app.common.ProdModule module2) {
            super("android.content.SharedPreferences", true, "com.navdy.hud.app.common.ProdModule", "provideSharedPreferences");
            this.module = module2;
            setLibrary(true);
        }

        public android.content.SharedPreferences get() {
            return this.module.provideSharedPreferences();
        }
    }

    /* compiled from: ProdModule$$ModuleAdapter */
    public static final class ProvideTelemetryDataManagerProvidesAdapter extends dagger.internal.ProvidesBinding<com.navdy.hud.app.analytics.TelemetryDataManager> implements javax.inject.Provider<com.navdy.hud.app.analytics.TelemetryDataManager> {
        private dagger.internal.Binding<com.squareup.otto.Bus> bus;
        private final com.navdy.hud.app.common.ProdModule module;
        private dagger.internal.Binding<com.navdy.hud.app.device.PowerManager> powerManager;
        private dagger.internal.Binding<android.content.SharedPreferences> sharedPreferences;
        private dagger.internal.Binding<com.navdy.hud.app.framework.trips.TripManager> tripManager;
        private dagger.internal.Binding<com.navdy.hud.app.ui.framework.UIStateManager> uiStateManager;

        public ProvideTelemetryDataManagerProvidesAdapter(com.navdy.hud.app.common.ProdModule module2) {
            super("com.navdy.hud.app.analytics.TelemetryDataManager", true, "com.navdy.hud.app.common.ProdModule", "provideTelemetryDataManager");
            this.module = module2;
            setLibrary(true);
        }

        public void attach(dagger.internal.Linker linker) {
            this.uiStateManager = linker.requestBinding("com.navdy.hud.app.ui.framework.UIStateManager", com.navdy.hud.app.common.ProdModule.class, getClass().getClassLoader());
            this.powerManager = linker.requestBinding("com.navdy.hud.app.device.PowerManager", com.navdy.hud.app.common.ProdModule.class, getClass().getClassLoader());
            this.bus = linker.requestBinding("com.squareup.otto.Bus", com.navdy.hud.app.common.ProdModule.class, getClass().getClassLoader());
            this.sharedPreferences = linker.requestBinding("android.content.SharedPreferences", com.navdy.hud.app.common.ProdModule.class, getClass().getClassLoader());
            this.tripManager = linker.requestBinding("com.navdy.hud.app.framework.trips.TripManager", com.navdy.hud.app.common.ProdModule.class, getClass().getClassLoader());
        }

        public void getDependencies(java.util.Set<dagger.internal.Binding<?>> getBindings, java.util.Set<dagger.internal.Binding<?>> set) {
            getBindings.add(this.uiStateManager);
            getBindings.add(this.powerManager);
            getBindings.add(this.bus);
            getBindings.add(this.sharedPreferences);
            getBindings.add(this.tripManager);
        }

        public com.navdy.hud.app.analytics.TelemetryDataManager get() {
            return this.module.provideTelemetryDataManager((com.navdy.hud.app.ui.framework.UIStateManager) this.uiStateManager.get(), (com.navdy.hud.app.device.PowerManager) this.powerManager.get(), (com.squareup.otto.Bus) this.bus.get(), (android.content.SharedPreferences) this.sharedPreferences.get(), (com.navdy.hud.app.framework.trips.TripManager) this.tripManager.get());
        }
    }

    /* compiled from: ProdModule$$ModuleAdapter */
    public static final class ProvideTimeHelperProvidesAdapter extends dagger.internal.ProvidesBinding<com.navdy.hud.app.common.TimeHelper> implements javax.inject.Provider<com.navdy.hud.app.common.TimeHelper> {
        private dagger.internal.Binding<com.squareup.otto.Bus> bus;
        private final com.navdy.hud.app.common.ProdModule module;

        public ProvideTimeHelperProvidesAdapter(com.navdy.hud.app.common.ProdModule module2) {
            super("com.navdy.hud.app.common.TimeHelper", true, "com.navdy.hud.app.common.ProdModule", "provideTimeHelper");
            this.module = module2;
            setLibrary(true);
        }

        public void attach(dagger.internal.Linker linker) {
            this.bus = linker.requestBinding("com.squareup.otto.Bus", com.navdy.hud.app.common.ProdModule.class, getClass().getClassLoader());
        }

        public void getDependencies(java.util.Set<dagger.internal.Binding<?>> getBindings, java.util.Set<dagger.internal.Binding<?>> set) {
            getBindings.add(this.bus);
        }

        public com.navdy.hud.app.common.TimeHelper get() {
            return this.module.provideTimeHelper((com.squareup.otto.Bus) this.bus.get());
        }
    }

    /* compiled from: ProdModule$$ModuleAdapter */
    public static final class ProvideTripManagerProvidesAdapter extends dagger.internal.ProvidesBinding<com.navdy.hud.app.framework.trips.TripManager> implements javax.inject.Provider<com.navdy.hud.app.framework.trips.TripManager> {
        private dagger.internal.Binding<com.squareup.otto.Bus> bus;
        private final com.navdy.hud.app.common.ProdModule module;
        private dagger.internal.Binding<android.content.SharedPreferences> preferences;

        public ProvideTripManagerProvidesAdapter(com.navdy.hud.app.common.ProdModule module2) {
            super("com.navdy.hud.app.framework.trips.TripManager", true, "com.navdy.hud.app.common.ProdModule", "provideTripManager");
            this.module = module2;
            setLibrary(true);
        }

        public void attach(dagger.internal.Linker linker) {
            this.bus = linker.requestBinding("com.squareup.otto.Bus", com.navdy.hud.app.common.ProdModule.class, getClass().getClassLoader());
            this.preferences = linker.requestBinding("android.content.SharedPreferences", com.navdy.hud.app.common.ProdModule.class, getClass().getClassLoader());
        }

        public void getDependencies(java.util.Set<dagger.internal.Binding<?>> getBindings, java.util.Set<dagger.internal.Binding<?>> set) {
            getBindings.add(this.bus);
            getBindings.add(this.preferences);
        }

        public com.navdy.hud.app.framework.trips.TripManager get() {
            return this.module.provideTripManager((com.squareup.otto.Bus) this.bus.get(), (android.content.SharedPreferences) this.preferences.get());
        }
    }

    /* compiled from: ProdModule$$ModuleAdapter */
    public static final class ProvideUIStateManagerProvidesAdapter extends dagger.internal.ProvidesBinding<com.navdy.hud.app.ui.framework.UIStateManager> implements javax.inject.Provider<com.navdy.hud.app.ui.framework.UIStateManager> {
        private final com.navdy.hud.app.common.ProdModule module;

        public ProvideUIStateManagerProvidesAdapter(com.navdy.hud.app.common.ProdModule module2) {
            super("com.navdy.hud.app.ui.framework.UIStateManager", true, "com.navdy.hud.app.common.ProdModule", "provideUIStateManager");
            this.module = module2;
            setLibrary(true);
        }

        public com.navdy.hud.app.ui.framework.UIStateManager get() {
            return this.module.provideUIStateManager();
        }
    }

    /* compiled from: ProdModule$$ModuleAdapter */
    public static final class ProvideVoiceSearchHandlerProvidesAdapter extends dagger.internal.ProvidesBinding<com.navdy.hud.app.framework.voice.VoiceSearchHandler> implements javax.inject.Provider<com.navdy.hud.app.framework.voice.VoiceSearchHandler> {
        private dagger.internal.Binding<com.squareup.otto.Bus> bus;
        private dagger.internal.Binding<com.navdy.hud.app.util.FeatureUtil> featureUtil;
        private final com.navdy.hud.app.common.ProdModule module;

        public ProvideVoiceSearchHandlerProvidesAdapter(com.navdy.hud.app.common.ProdModule module2) {
            super("com.navdy.hud.app.framework.voice.VoiceSearchHandler", true, "com.navdy.hud.app.common.ProdModule", "provideVoiceSearchHandler");
            this.module = module2;
            setLibrary(true);
        }

        public void attach(dagger.internal.Linker linker) {
            this.bus = linker.requestBinding("com.squareup.otto.Bus", com.navdy.hud.app.common.ProdModule.class, getClass().getClassLoader());
            this.featureUtil = linker.requestBinding("com.navdy.hud.app.util.FeatureUtil", com.navdy.hud.app.common.ProdModule.class, getClass().getClassLoader());
        }

        public void getDependencies(java.util.Set<dagger.internal.Binding<?>> getBindings, java.util.Set<dagger.internal.Binding<?>> set) {
            getBindings.add(this.bus);
            getBindings.add(this.featureUtil);
        }

        public com.navdy.hud.app.framework.voice.VoiceSearchHandler get() {
            return this.module.provideVoiceSearchHandler((com.squareup.otto.Bus) this.bus.get(), (com.navdy.hud.app.util.FeatureUtil) this.featureUtil.get());
        }
    }

    public ProdModule$$ModuleAdapter() {
        super(com.navdy.hud.app.common.ProdModule.class, INJECTS, STATIC_INJECTIONS, false, INCLUDES, true, true);
    }

    public void getBindings(dagger.internal.BindingsGroup bindings, com.navdy.hud.app.common.ProdModule module) {
        bindings.contributeProvidesBinding("com.squareup.otto.Bus", new com.navdy.hud.app.common.ProdModule$$ModuleAdapter.ProvideBusProvidesAdapter(module));
        bindings.contributeProvidesBinding("android.content.SharedPreferences", new com.navdy.hud.app.common.ProdModule$$ModuleAdapter.ProvideSharedPreferencesProvidesAdapter(module));
        bindings.contributeProvidesBinding("com.navdy.hud.app.service.ConnectionServiceProxy", new com.navdy.hud.app.common.ProdModule$$ModuleAdapter.ProvideConnectionServiceProxyProvidesAdapter(module));
        bindings.contributeProvidesBinding("com.navdy.hud.app.common.TimeHelper", new com.navdy.hud.app.common.ProdModule$$ModuleAdapter.ProvideTimeHelperProvidesAdapter(module));
        bindings.contributeProvidesBinding("com.navdy.hud.app.device.PowerManager", new com.navdy.hud.app.common.ProdModule$$ModuleAdapter.ProvidePowerManagerProvidesAdapter(module));
        bindings.contributeProvidesBinding("com.navdy.hud.app.config.SettingsManager", new com.navdy.hud.app.common.ProdModule$$ModuleAdapter.ProvideSettingsManagerProvidesAdapter(module));
        bindings.contributeProvidesBinding("com.navdy.hud.app.ancs.AncsServiceConnector", new com.navdy.hud.app.common.ProdModule$$ModuleAdapter.ProvideAncsServiceConnectorProvidesAdapter(module));
        bindings.contributeProvidesBinding("com.navdy.hud.app.manager.InputManager", new com.navdy.hud.app.common.ProdModule$$ModuleAdapter.ProvideInputManagerProvidesAdapter(module));
        bindings.contributeProvidesBinding("com.navdy.hud.app.device.dial.DialSimulatorMessagesHandler", new com.navdy.hud.app.common.ProdModule$$ModuleAdapter.ProvideDialSimulatorMessagesHandlerProvidesAdapter(module));
        bindings.contributeProvidesBinding("com.navdy.hud.app.framework.phonecall.CallManager", new com.navdy.hud.app.common.ProdModule$$ModuleAdapter.ProvideCallManagerProvidesAdapter(module));
        bindings.contributeProvidesBinding("com.navdy.hud.app.service.pandora.PandoraManager", new com.navdy.hud.app.common.ProdModule$$ModuleAdapter.ProvidePandoraManagerProvidesAdapter(module));
        bindings.contributeProvidesBinding("com.navdy.hud.app.manager.MusicManager", new com.navdy.hud.app.common.ProdModule$$ModuleAdapter.ProvideMusicManagerProvidesAdapter(module));
        bindings.contributeProvidesBinding("com.navdy.hud.app.service.ConnectionHandler", new com.navdy.hud.app.common.ProdModule$$ModuleAdapter.ProvideConnectionHandlerProvidesAdapter(module));
        bindings.contributeProvidesBinding("com.navdy.hud.app.gesture.GestureServiceConnector", new com.navdy.hud.app.common.ProdModule$$ModuleAdapter.ProvideGestureServiceConnectorProvidesAdapter(module));
        bindings.contributeProvidesBinding("com.navdy.hud.app.ui.framework.UIStateManager", new com.navdy.hud.app.common.ProdModule$$ModuleAdapter.ProvideUIStateManagerProvidesAdapter(module));
        bindings.contributeProvidesBinding("com.navdy.hud.app.storage.PathManager", new com.navdy.hud.app.common.ProdModule$$ModuleAdapter.ProvidePathManagerProvidesAdapter(module));
        bindings.contributeProvidesBinding("com.navdy.hud.app.profile.DriverProfileManager", new com.navdy.hud.app.common.ProdModule$$ModuleAdapter.ProvideDriverProfileManagerProvidesAdapter(module));
        bindings.contributeProvidesBinding("com.navdy.hud.app.framework.trips.TripManager", new com.navdy.hud.app.common.ProdModule$$ModuleAdapter.ProvideTripManagerProvidesAdapter(module));
        bindings.contributeProvidesBinding("com.navdy.hud.app.manager.PairingManager", new com.navdy.hud.app.common.ProdModule$$ModuleAdapter.ProvidePairingManagerProvidesAdapter(module));
        bindings.contributeProvidesBinding("com.navdy.service.library.network.http.IHttpManager", new com.navdy.hud.app.common.ProdModule$$ModuleAdapter.ProvideHttpManagerProvidesAdapter(module));
        bindings.contributeProvidesBinding("com.navdy.hud.app.debug.DriveRecorder", new com.navdy.hud.app.common.ProdModule$$ModuleAdapter.ProvideObdDataReceorderProvidesAdapter(module));
        bindings.contributeProvidesBinding("com.navdy.hud.app.util.MusicArtworkCache", new com.navdy.hud.app.common.ProdModule$$ModuleAdapter.ProvideArtworkCacheProvidesAdapter(module));
        bindings.contributeProvidesBinding("com.navdy.hud.app.framework.voice.VoiceSearchHandler", new com.navdy.hud.app.common.ProdModule$$ModuleAdapter.ProvideVoiceSearchHandlerProvidesAdapter(module));
        bindings.contributeProvidesBinding("com.navdy.hud.app.util.FeatureUtil", new com.navdy.hud.app.common.ProdModule$$ModuleAdapter.ProvideFeatureUtilProvidesAdapter(module));
        bindings.contributeProvidesBinding("com.navdy.hud.app.analytics.TelemetryDataManager", new com.navdy.hud.app.common.ProdModule$$ModuleAdapter.ProvideTelemetryDataManagerProvidesAdapter(module));
        bindings.contributeProvidesBinding("com.navdy.hud.app.storage.cache.MessageCache<com.navdy.service.library.events.audio.MusicCollectionResponse>", new com.navdy.hud.app.common.ProdModule$$ModuleAdapter.ProvideMusicCollectionResponseMessageCacheProvidesAdapter(module));
    }
}
