package com.navdy.hud.app.common;

public abstract class ServiceReconnector {
    private static final int RECONNECT_INTERVAL_MS = 15000;
    private static final int RETRY_INTERVAL_MS = 60000;
    /* access modifiers changed from: private */
    public java.lang.String action;
    /* access modifiers changed from: private */
    public java.lang.Runnable connectRunnable = new com.navdy.hud.app.common.ServiceReconnector.Anon1();
    /* access modifiers changed from: private */
    public android.content.Context context;
    /* access modifiers changed from: private */
    public android.os.Handler handler;
    /* access modifiers changed from: private */
    public final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(getClass());
    /* access modifiers changed from: private */
    public android.content.ServiceConnection serviceConnection = new com.navdy.hud.app.common.ServiceReconnector.Anon2();
    protected android.content.Intent serviceIntent;
    boolean shuttingDown;

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            try {
                android.content.ComponentName component = com.navdy.hud.app.common.ServiceReconnector.this.serviceIntent.getComponent();
                if (component == null) {
                    component = com.navdy.hud.app.common.ServiceReconnector.this.getServiceComponent(com.navdy.hud.app.common.ServiceReconnector.this.serviceIntent);
                }
                if (component == null) {
                    com.navdy.hud.app.common.ServiceReconnector.this.handler.postDelayed(this, 60000);
                    return;
                }
                com.navdy.hud.app.common.ServiceReconnector.this.serviceIntent.setComponent(component);
                com.navdy.hud.app.common.ServiceReconnector.this.logger.i("Trying to start service");
                if (com.navdy.hud.app.common.ServiceReconnector.this.context.startService(com.navdy.hud.app.common.ServiceReconnector.this.serviceIntent) != null) {
                    com.navdy.hud.app.common.ServiceReconnector.this.serviceIntent.setAction(com.navdy.hud.app.common.ServiceReconnector.this.action);
                    if (!com.navdy.hud.app.common.ServiceReconnector.this.context.bindService(com.navdy.hud.app.common.ServiceReconnector.this.serviceIntent, com.navdy.hud.app.common.ServiceReconnector.this.serviceConnection, 0)) {
                        com.navdy.hud.app.common.ServiceReconnector.this.logger.e("Unable to bind to service - aborting");
                        return;
                    }
                    return;
                }
                com.navdy.hud.app.common.ServiceReconnector.this.logger.e("Service doesn't exist (uninstalled?) - retrying");
                com.navdy.hud.app.common.ServiceReconnector.this.handler.postDelayed(this, 60000);
            } catch (java.lang.SecurityException ex) {
                com.navdy.hud.app.common.ServiceReconnector.this.logger.e("Security exception connecting to service - aborting", ex);
            }
        }
    }

    class Anon2 implements android.content.ServiceConnection {
        Anon2() {
        }

        public void onServiceConnected(android.content.ComponentName name, android.os.IBinder service) {
            com.navdy.hud.app.common.ServiceReconnector.this.logger.i("ServiceConnection established with " + name);
            com.navdy.hud.app.common.ServiceReconnector.this.onConnected(name, service);
        }

        public void onServiceDisconnected(android.content.ComponentName name) {
            com.navdy.hud.app.common.ServiceReconnector.this.onDisconnected(name);
            if (!com.navdy.hud.app.common.ServiceReconnector.this.shuttingDown) {
                com.navdy.hud.app.common.ServiceReconnector.this.logger.i("Service disconnected - will try reconnecting");
                com.navdy.hud.app.common.ServiceReconnector.this.handler.postDelayed(com.navdy.hud.app.common.ServiceReconnector.this.connectRunnable, com.glympse.android.lib.StaticConfig.PERMISSION_CHECK_INTERVAL);
            }
        }
    }

    /* access modifiers changed from: protected */
    public abstract void onConnected(android.content.ComponentName componentName, android.os.IBinder iBinder);

    /* access modifiers changed from: protected */
    public abstract void onDisconnected(android.content.ComponentName componentName);

    public ServiceReconnector(android.content.Context context2, android.content.Intent serviceIntent2, java.lang.String action2) {
        this.logger.i("Establishing service connection");
        this.handler = new android.os.Handler(android.os.Looper.getMainLooper());
        this.context = context2;
        this.serviceIntent = serviceIntent2;
        this.action = action2;
        this.handler.post(this.connectRunnable);
    }

    public void shutdown() {
        this.logger.i("shutting down service");
        this.shuttingDown = true;
        this.handler.removeCallbacks(this.connectRunnable);
        this.context.stopService(this.serviceIntent);
    }

    public void restart() {
        this.context.stopService(this.serviceIntent);
    }

    /* access modifiers changed from: private */
    public android.content.ComponentName getServiceComponent(android.content.Intent serviceIntent2) {
        java.util.List<android.content.pm.ResolveInfo> pkgAppsList = this.context.getPackageManager().queryIntentServices(serviceIntent2, 0);
        if (pkgAppsList.isEmpty()) {
            return null;
        }
        android.content.pm.ResolveInfo info = (android.content.pm.ResolveInfo) pkgAppsList.get(0);
        return new android.content.ComponentName(info.serviceInfo.applicationInfo.packageName, info.serviceInfo.name);
    }
}
