package com.navdy.hud.app.storage;

public class PathManager implements com.navdy.service.library.file.IFileTransferAuthority {
    private static final java.lang.String ACTIVE_ROUTE_DIR = ".activeroute";
    private static final java.lang.String CAR_MD_RESPONSE_DISK_CACHE_FILES_DIR = "car_md_cache";
    private static java.lang.String CRASH_INFO_TEXT_FILE_NAME = "info.txt";
    private static final java.lang.String DATA_LOGS_DIR = "DataLogs";
    private static final java.lang.String DRIVER_PROFILES_DIR = "DriverProfiles";
    private static final java.lang.String GESTURE_VIDEOS_SYNC_FOLDER = "gesture_videos";
    private static final java.lang.String HERE_MAPS_CONFIG_BASE_PATH = "/.here-maps";
    /* access modifiers changed from: private */
    public static final java.util.regex.Pattern HERE_MAPS_CONFIG_DIRS_PATTERN = java.util.regex.Pattern.compile(HERE_MAPS_CONFIG_DIRS_REGEX);
    private static final java.lang.String HERE_MAPS_CONFIG_DIRS_REGEX = "[0-9]{10}";
    private static final java.lang.String HERE_MAP_DATA_DIR = ".here-maps";
    public static final java.lang.String HERE_MAP_META_JSON_FILE = "meta.json";
    private static final java.lang.String HUD_DATABASE_DIR = "/.db";
    private static final java.lang.String IMAGE_DISK_CACHE_FILES_DIR = "img_disk_cache";
    private static final java.lang.String KERNEL_CRASH_CONSOLE_RAMOOPS = "/sys/fs/pstore/console-ramoops";
    private static final java.lang.String KERNEL_CRASH_CONSOLE_RAMOOPS_0 = "/sys/fs/pstore/console-ramoops-0";
    private static final java.lang.String KERNEL_CRASH_DMESG_RAMOOPS = "/sys/fs/pstore/dmesg-ramoops-0";
    private static final java.lang.String[] KERNEL_CRASH_FILES = {KERNEL_CRASH_CONSOLE_RAMOOPS, KERNEL_CRASH_CONSOLE_RAMOOPS_0, KERNEL_CRASH_DMESG_RAMOOPS};
    private static final java.lang.String LOGS_FOLDER = "templogs";
    private static final java.lang.String MUSIC_LIBRARY_DISK_CACHE_FILES_DIR = "music_disk_cache";
    private static final java.lang.String NAVIGATION_FILES_DIR = "navigation_issues";
    private static final java.lang.String NON_FATAL_CRASH_REPORT_DIR = "/sdcard/.logs/snapshot/";
    private static final java.lang.String SYSTEM_CACHE_DIR = "/cache";
    private static final java.lang.String TEMP_FILE_TIMESTAMP_FORMAT = "'display_log'_yyyy_MM_dd-HH_mm_ss'.zip'";
    private static final java.lang.String TIMESTAMP_MWCONFIG_LATEST = com.navdy.hud.app.util.DeviceUtil.getCurrentHereSdkTimestamp();
    private static java.text.SimpleDateFormat format = new java.text.SimpleDateFormat(TEMP_FILE_TIMESTAMP_FORMAT);
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.storage.PathManager.class);
    private static final com.navdy.hud.app.storage.PathManager sSingleton = new com.navdy.hud.app.storage.PathManager();
    private java.lang.String activeRouteInfoDir;
    private java.lang.String carMdResponseDiskCacheFolder;
    private java.lang.String databaseDir;
    private java.lang.String driverProfilesDir;
    private java.lang.String gestureVideosSyncFolder;
    private java.io.File hereMapsConfigDirs;
    private java.lang.String hereMapsDataDirectory;
    private java.lang.String hereVoiceSkinsPath;
    private java.lang.String imageDiskCacheFolder;
    private volatile boolean initialized;
    private final java.lang.String mapsPartitionPath;
    private java.lang.String musicDiskCacheFolder;
    private java.lang.String navigationIssuesFolder;
    private java.lang.String tempLogsFolder = (com.navdy.hud.app.HudApplication.getAppContext().getCacheDir().getAbsolutePath() + java.io.File.separator + LOGS_FOLDER);

    class Anon1 implements java.io.FileFilter {
        Anon1() {
        }

        public boolean accept(java.io.File file) {
            return file.isDirectory() && com.navdy.hud.app.storage.PathManager.HERE_MAPS_CONFIG_DIRS_PATTERN.matcher(file.getName()).matches();
        }
    }

    public static com.navdy.hud.app.storage.PathManager getInstance() {
        return sSingleton;
    }

    private PathManager() {
        java.lang.String externalPath = android.os.Environment.getExternalStorageDirectory().getAbsolutePath();
        java.lang.String internalAppPath = com.navdy.hud.app.HudApplication.getAppContext().getFilesDir().getAbsolutePath();
        this.driverProfilesDir = internalAppPath + java.io.File.separator + DRIVER_PROFILES_DIR;
        this.mapsPartitionPath = com.navdy.hud.app.util.os.SystemProperties.get("ro.maps_partition", externalPath);
        this.hereMapsConfigDirs = new java.io.File(this.mapsPartitionPath + java.io.File.separator + HERE_MAPS_CONFIG_BASE_PATH);
        this.databaseDir = internalAppPath + java.io.File.separator + HUD_DATABASE_DIR;
        this.activeRouteInfoDir = internalAppPath + java.io.File.separator + ACTIVE_ROUTE_DIR;
        this.navigationIssuesFolder = internalAppPath + java.io.File.separator + NAVIGATION_FILES_DIR;
        this.carMdResponseDiskCacheFolder = internalAppPath + java.io.File.separator + CAR_MD_RESPONSE_DISK_CACHE_FILES_DIR;
        this.imageDiskCacheFolder = internalAppPath + java.io.File.separator + IMAGE_DISK_CACHE_FILES_DIR;
        this.musicDiskCacheFolder = internalAppPath + java.io.File.separator + MUSIC_LIBRARY_DISK_CACHE_FILES_DIR;
        this.gestureVideosSyncFolder = this.mapsPartitionPath + java.io.File.separator + GESTURE_VIDEOS_SYNC_FOLDER;
        java.io.File voiceSkinsFilepath = new java.io.File(this.mapsPartitionPath + java.io.File.separator + ".here-maps/voices-download");
        sLogger.v("voiceSkins path=" + voiceSkinsFilepath);
        this.hereVoiceSkinsPath = voiceSkinsFilepath.getAbsolutePath();
        this.hereMapsDataDirectory = this.mapsPartitionPath + java.io.File.separator + HERE_MAP_DATA_DIR;
    }

    private void init() {
        if (!this.initialized) {
            this.initialized = true;
            new java.io.File(this.tempLogsFolder).mkdirs();
            new java.io.File(this.databaseDir).mkdirs();
            new java.io.File(this.driverProfilesDir).mkdirs();
            new java.io.File(this.activeRouteInfoDir).mkdirs();
            new java.io.File(this.navigationIssuesFolder).mkdirs();
            new java.io.File(this.carMdResponseDiskCacheFolder).mkdirs();
            new java.io.File(NON_FATAL_CRASH_REPORT_DIR).mkdirs();
            new java.io.File(this.imageDiskCacheFolder).mkdirs();
            new java.io.File(this.musicDiskCacheFolder).mkdirs();
            new java.io.File(this.hereVoiceSkinsPath).mkdirs();
            new java.io.File(this.gestureVideosSyncFolder).mkdirs();
        }
    }

    public java.util.List<java.lang.String> getHereMapsConfigDirs() {
        if (!this.initialized) {
            init();
        }
        java.util.List<java.lang.String> pathList = new java.util.ArrayList<>();
        java.io.File[] hereDirs = this.hereMapsConfigDirs.listFiles(new com.navdy.hud.app.storage.PathManager.Anon1());
        if (hereDirs != null) {
            for (java.io.File f : hereDirs) {
                pathList.add(f.getAbsolutePath());
            }
        }
        return pathList;
    }

    public java.lang.String getLatestHereMapsConfigPath() {
        if (!this.initialized) {
            init();
        }
        return this.hereMapsConfigDirs.getAbsolutePath() + java.io.File.separator + TIMESTAMP_MWCONFIG_LATEST;
    }

    public java.lang.String getHereVoiceSkinsPath() {
        if (!this.initialized) {
            init();
        }
        return this.hereVoiceSkinsPath;
    }

    public java.lang.String[] getKernelCrashFiles() {
        if (!this.initialized) {
            init();
        }
        return KERNEL_CRASH_FILES;
    }

    public java.lang.String getDriverProfilesDir() {
        if (!this.initialized) {
            init();
        }
        return this.driverProfilesDir;
    }

    public java.lang.String getMapsPartitionPath() {
        if (!this.initialized) {
            init();
        }
        return this.mapsPartitionPath;
    }

    public java.lang.String getHereMapsDataDirectory() {
        if (!this.initialized) {
            init();
        }
        return this.hereMapsDataDirectory;
    }

    public java.lang.String getGestureVideosSyncFolder() {
        if (!this.initialized) {
            init();
        }
        return this.gestureVideosSyncFolder;
    }

    public boolean isFileTypeAllowed(com.navdy.service.library.events.file.FileType fileType) {
        switch (fileType) {
            case FILE_TYPE_OTA:
            case FILE_TYPE_LOGS:
            case FILE_TYPE_PERF_TEST:
                return true;
            default:
                return false;
        }
    }

    public java.lang.String getDirectoryForFileType(com.navdy.service.library.events.file.FileType fileType) {
        if (!this.initialized) {
            init();
        }
        switch (fileType) {
            case FILE_TYPE_OTA:
                return SYSTEM_CACHE_DIR;
            case FILE_TYPE_LOGS:
                return this.tempLogsFolder;
            default:
                return null;
        }
    }

    public java.lang.String getFileToSend(com.navdy.service.library.events.file.FileType fileType) {
        if (fileType == com.navdy.service.library.events.file.FileType.FILE_TYPE_LOGS) {
            java.lang.String tempLogFolder = getDirectoryForFileType(fileType);
            java.lang.String stagingPath = tempLogFolder + java.io.File.separator + "stage";
            java.io.File stagingDirectory = new java.io.File(stagingPath);
            com.navdy.service.library.util.IOUtils.deleteDirectory(com.navdy.hud.app.HudApplication.getAppContext(), stagingDirectory);
            com.navdy.service.library.util.IOUtils.createDirectory(stagingDirectory);
            try {
                com.navdy.service.library.util.LogUtils.copyComprehensiveSystemLogs(stagingPath);
                collectEnvironmentInfo(stagingPath);
                collectGpsLog(stagingPath);
                com.navdy.hud.app.util.DeviceUtil.copyHEREMapsDataInfo(stagingPath);
                com.navdy.hud.app.util.DeviceUtil.takeDeviceScreenShot(stagingDirectory.getAbsolutePath() + java.io.File.separator + "HUDScreenShot.png");
                java.util.List<java.io.File> driveLogFiles = com.navdy.hud.app.util.ReportIssueService.getLatestDriveLogFiles(1);
                if (driveLogFiles != null && driveLogFiles.size() > 0) {
                    for (java.io.File driveLogFile : driveLogFiles) {
                        com.navdy.service.library.util.IOUtils.copyFile(driveLogFile.getAbsolutePath(), stagingDirectory.getAbsolutePath() + java.io.File.separator + driveLogFile.getName());
                    }
                }
                java.io.File[] logFiles = stagingDirectory.listFiles();
                java.lang.String absolutePath = tempLogFolder + java.io.File.separator + generateTempFileName();
                com.navdy.hud.app.util.CrashReportService.compressCrashReportsToZip(logFiles, absolutePath);
                if (new java.io.File(absolutePath).exists()) {
                    return absolutePath;
                }
            } catch (Throwable th) {
                return null;
            }
        }
        return null;
    }

    public void onFileSent(com.navdy.service.library.events.file.FileType fileType) {
        if (fileType == com.navdy.service.library.events.file.FileType.FILE_TYPE_LOGS) {
            com.navdy.hud.app.util.CrashReportService.clearCrashReports();
        }
    }

    public static synchronized java.lang.String generateTempFileName() {
        java.lang.String format2;
        synchronized (com.navdy.hud.app.storage.PathManager.class) {
            format2 = format.format(java.lang.Long.valueOf(java.lang.System.currentTimeMillis()));
        }
        return format2;
    }

    public java.lang.String getDatabaseDir() {
        if (!this.initialized) {
            init();
        }
        return this.databaseDir;
    }

    public java.lang.String getActiveRouteInfoDir() {
        if (!this.initialized) {
            init();
        }
        return this.activeRouteInfoDir;
    }

    public java.lang.String getNavigationIssuesDir() {
        if (!this.initialized) {
            init();
        }
        return this.navigationIssuesFolder;
    }

    public java.lang.String getCarMdResponseDiskCacheFolder() {
        if (!this.initialized) {
            init();
        }
        return this.carMdResponseDiskCacheFolder;
    }

    public java.lang.String getNonFatalCrashReportDir() {
        if (!this.initialized) {
            init();
        }
        return NON_FATAL_CRASH_REPORT_DIR;
    }

    public java.lang.String getImageDiskCacheFolder() {
        if (!this.initialized) {
            init();
        }
        return this.imageDiskCacheFolder;
    }

    public java.lang.String getMusicDiskCacheFolder() {
        if (!this.initialized) {
            init();
        }
        return this.musicDiskCacheFolder;
    }

    private void collectGpsLog(java.lang.String stagingPath) {
        try {
            android.content.Intent intent = new android.content.Intent(com.navdy.hud.app.device.gps.GpsConstants.GPS_COLLECT_LOGS);
            intent.putExtra(com.navdy.hud.app.device.gps.GpsConstants.GPS_EXTRA_LOG_PATH, stagingPath);
            com.navdy.hud.app.HudApplication.getAppContext().sendBroadcastAsUser(intent, android.os.Process.myUserHandle());
            com.navdy.hud.app.util.GenericUtil.sleep(3000);
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }

    public void collectEnvironmentInfo(java.lang.String stagingPath) {
        java.io.FileWriter fileWriter = null;
        try {
            java.io.FileWriter fileWriter2 = new java.io.FileWriter(new java.io.File(stagingPath + java.io.File.separator + CRASH_INFO_TEXT_FILE_NAME));
            try {
                fileWriter2.write(com.navdy.hud.app.util.os.PropsFileUpdater.readProps());
                com.navdy.service.library.util.IOUtils.closeStream(fileWriter2);
                java.io.FileWriter fileWriter3 = fileWriter2;
            } catch (Throwable th) {
                th = th;
                fileWriter = fileWriter2;
                com.navdy.service.library.util.IOUtils.closeStream(fileWriter);
                throw th;
            }
        } catch (Throwable th2) {
            t = th2;
            sLogger.e(t);
            com.navdy.service.library.util.IOUtils.closeStream(fileWriter);
        }
    }
}
