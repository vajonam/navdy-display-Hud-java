package com.navdy.hud.app.storage.cache;

public class DiskLruCache {
    /* access modifiers changed from: private */
    public android.content.Context appContext = com.navdy.hud.app.HudApplication.getAppContext();
    private java.lang.String cacheName;
    /* access modifiers changed from: private */
    public com.navdy.service.library.log.Logger logger;
    private com.navdy.hud.app.storage.cache.DiskLruCache.MemoryLruCache lruCache;
    private int maxSize;
    private java.io.File rootPath;
    private com.navdy.service.library.task.TaskManager taskManager = com.navdy.service.library.task.TaskManager.getInstance();

    class Anon1 implements java.util.Comparator<java.io.File> {
        Anon1() {
        }

        public int compare(java.io.File lhs, java.io.File rhs) {
            return java.lang.Long.compare(lhs.lastModified(), rhs.lastModified());
        }
    }

    class Anon2 implements java.lang.Runnable {
        final /* synthetic */ java.lang.String val$name;
        final /* synthetic */ byte[] val$val;

        Anon2(java.lang.String str, byte[] bArr) {
            this.val$name = str;
            this.val$val = bArr;
        }

        public void run() {
            java.io.FileOutputStream fileOutputStream = null;
            try {
                java.io.FileOutputStream fileOutputStream2 = new java.io.FileOutputStream(com.navdy.hud.app.storage.cache.DiskLruCache.this.getFilePath(this.val$name));
                try {
                    fileOutputStream2.write(this.val$val);
                    com.navdy.service.library.util.IOUtils.fileSync(fileOutputStream2);
                    com.navdy.service.library.util.IOUtils.closeStream(fileOutputStream2);
                    java.io.FileOutputStream fileOutputStream3 = fileOutputStream2;
                } catch (Throwable th) {
                    th = th;
                    fileOutputStream = fileOutputStream2;
                    com.navdy.service.library.util.IOUtils.closeStream(fileOutputStream);
                    throw th;
                }
            } catch (Throwable th2) {
                t = th2;
                com.navdy.hud.app.storage.cache.DiskLruCache.this.logger.e(t);
                com.navdy.service.library.util.IOUtils.closeStream(fileOutputStream);
            }
        }
    }

    class Anon3 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.hud.app.storage.cache.DiskLruCache.FileEntry val$entry;

        Anon3(com.navdy.hud.app.storage.cache.DiskLruCache.FileEntry fileEntry) {
            this.val$entry = fileEntry;
        }

        public void run() {
            if (!com.navdy.service.library.util.IOUtils.deleteFile(com.navdy.hud.app.storage.cache.DiskLruCache.this.appContext, com.navdy.hud.app.storage.cache.DiskLruCache.this.getFilePath(this.val$entry.name))) {
                com.navdy.hud.app.storage.cache.DiskLruCache.this.logger.v("file not deleted [" + this.val$entry.name + "]");
            }
        }
    }

    class Anon4 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.hud.app.storage.cache.DiskLruCache.FileEntry val$entry;

        Anon4(com.navdy.hud.app.storage.cache.DiskLruCache.FileEntry fileEntry) {
            this.val$entry = fileEntry;
        }

        public void run() {
            java.io.File f = new java.io.File(com.navdy.hud.app.storage.cache.DiskLruCache.this.getFilePath(this.val$entry.name));
            if (!f.setLastModified(java.lang.System.currentTimeMillis())) {
                com.navdy.hud.app.storage.cache.DiskLruCache.this.logger.v("file time not changed [" + f.getAbsolutePath() + "]");
            }
        }
    }

    private static class FileEntry {
        java.lang.String name;
        boolean removed;
        int size;

        FileEntry(java.lang.String name2, int size2) {
            this.name = name2;
            this.size = size2;
        }
    }

    private static class MemoryLruCache extends android.util.LruCache<java.lang.String, com.navdy.hud.app.storage.cache.DiskLruCache.FileEntry> {
        private com.navdy.hud.app.storage.cache.DiskLruCache parent;

        public MemoryLruCache(com.navdy.hud.app.storage.cache.DiskLruCache parent2, int maxSize) {
            super(maxSize);
            this.parent = parent2;
        }

        /* access modifiers changed from: protected */
        public int sizeOf(java.lang.String key, com.navdy.hud.app.storage.cache.DiskLruCache.FileEntry value) {
            return value.size;
        }

        /* access modifiers changed from: protected */
        public void entryRemoved(boolean evicted, java.lang.String key, com.navdy.hud.app.storage.cache.DiskLruCache.FileEntry oldValue, com.navdy.hud.app.storage.cache.DiskLruCache.FileEntry newValue) {
            if (oldValue != null) {
                this.parent.removeFile(oldValue);
                oldValue.removed = true;
            }
        }
    }

    public DiskLruCache(java.lang.String cacheName2, java.lang.String rootPath2, int maxSize2) {
        this.logger = new com.navdy.service.library.log.Logger("DLC-" + cacheName2);
        this.cacheName = cacheName2;
        this.rootPath = new java.io.File(rootPath2);
        this.rootPath.mkdirs();
        this.maxSize = maxSize2;
        this.lruCache = new com.navdy.hud.app.storage.cache.DiskLruCache.MemoryLruCache(this, maxSize2);
        init();
    }

    private void init() {
        this.logger.v("init start");
        java.io.File[] list = this.rootPath.listFiles();
        if (list != null) {
            java.util.ArrayList<java.io.File> cacheFiles = new java.util.ArrayList<>();
            for (int i = 0; i < list.length; i++) {
                if (list[i].isFile()) {
                    cacheFiles.add(list[i]);
                }
            }
            int n = cacheFiles.size();
            this.logger.v("init: files in cache[" + n + "]");
            if (n == 0) {
                this.logger.v("init: cache empty");
                return;
            }
            java.util.Collections.sort(cacheFiles, new com.navdy.hud.app.storage.cache.DiskLruCache.Anon1());
            java.util.Iterator<java.io.File> iterator = cacheFiles.iterator();
            while (iterator.hasNext()) {
                java.io.File f = (java.io.File) iterator.next();
                java.lang.String name = f.getName();
                this.lruCache.put(name, new com.navdy.hud.app.storage.cache.DiskLruCache.FileEntry(name, (int) f.length()));
            }
            this.logger.v("init complete");
        }
    }

    private void addFile(java.lang.String name, byte[] val) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.storage.cache.DiskLruCache.Anon2(name, val), 22);
    }

    /* access modifiers changed from: private */
    public void removeFile(com.navdy.hud.app.storage.cache.DiskLruCache.FileEntry entry) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.storage.cache.DiskLruCache.Anon3(entry), 22);
    }

    public void updateFile(com.navdy.hud.app.storage.cache.DiskLruCache.FileEntry entry) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.storage.cache.DiskLruCache.Anon4(entry), 22);
    }

    public synchronized void put(java.lang.String name, byte[] val) {
        com.navdy.hud.app.storage.cache.DiskLruCache.FileEntry entry = new com.navdy.hud.app.storage.cache.DiskLruCache.FileEntry(name, val.length);
        this.lruCache.put(name, entry);
        if (!entry.removed) {
            addFile(name, val);
        }
    }

    public synchronized boolean contains(java.lang.String name) {
        return ((com.navdy.hud.app.storage.cache.DiskLruCache.FileEntry) this.lruCache.get(name)) != null;
    }

    public synchronized byte[] get(java.lang.String name) {
        byte[] bArr = null;
        synchronized (this) {
            com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
            com.navdy.hud.app.storage.cache.DiskLruCache.FileEntry entry = (com.navdy.hud.app.storage.cache.DiskLruCache.FileEntry) this.lruCache.get(name);
            if (entry != null) {
                updateFile(entry);
                try {
                    bArr = com.navdy.service.library.util.IOUtils.readBinaryFile(getFilePath(name));
                } catch (Throwable ex) {
                    this.logger.e(ex);
                }
            }
        }
        return bArr;
    }

    public synchronized void clear() {
        this.lruCache.evictAll();
    }

    public synchronized void remove(java.lang.String name) {
        this.lruCache.remove(name);
    }

    /* access modifiers changed from: private */
    public java.lang.String getFilePath(java.lang.String name) {
        return this.rootPath + java.io.File.separator + name;
    }
}
