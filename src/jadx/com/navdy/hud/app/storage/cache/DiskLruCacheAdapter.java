package com.navdy.hud.app.storage.cache;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0010\u0012\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0006\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u001d\u0012\u0006\u0010\u0004\u001a\u00020\u0002\u0012\u0006\u0010\u0005\u001a\u00020\u0002\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\bJ\b\u0010\r\u001a\u00020\u000eH\u0016J\u0010\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0002H\u0016J\u0013\u0010\u0012\u001a\u0004\u0018\u00010\u00032\u0006\u0010\u0011\u001a\u00020\u0002H\u0096\u0002J\u0018\u0010\u0013\u001a\u00020\u000e2\u0006\u0010\u0011\u001a\u00020\u00022\u0006\u0010\u0014\u001a\u00020\u0003H\u0016J\u0010\u0010\u0015\u001a\u00020\u000e2\u0006\u0010\u0011\u001a\u00020\u0002H\u0016R\u0011\u0010\t\u001a\u00020\n\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\f\u00a8\u0006\u0016"}, d2 = {"Lcom/navdy/hud/app/storage/cache/DiskLruCacheAdapter;", "Lcom/navdy/hud/app/storage/cache/Cache;", "", "", "cacheName", "directory", "maxSize", "", "(Ljava/lang/String;Ljava/lang/String;I)V", "diskLruCache", "Lcom/navdy/hud/app/storage/cache/DiskLruCache;", "getDiskLruCache", "()Lcom/navdy/hud/app/storage/cache/DiskLruCache;", "clear", "", "contains", "", "key", "get", "put", "data", "remove", "app_hudDebug"}, k = 1, mv = {1, 1, 6})
/* compiled from: DiskLruCacheAdapter.kt */
public final class DiskLruCacheAdapter implements com.navdy.hud.app.storage.cache.Cache<java.lang.String, byte[]> {
    @org.jetbrains.annotations.NotNull
    private final com.navdy.hud.app.storage.cache.DiskLruCache diskLruCache;

    public DiskLruCacheAdapter(@org.jetbrains.annotations.NotNull java.lang.String cacheName, @org.jetbrains.annotations.NotNull java.lang.String directory, int maxSize) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(cacheName, "cacheName");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(directory, "directory");
        this.diskLruCache = new com.navdy.hud.app.storage.cache.DiskLruCache(cacheName, directory, maxSize);
    }

    @org.jetbrains.annotations.NotNull
    public final com.navdy.hud.app.storage.cache.DiskLruCache getDiskLruCache() {
        return this.diskLruCache;
    }

    @org.jetbrains.annotations.Nullable
    public byte[] get(@org.jetbrains.annotations.NotNull java.lang.String key) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(key, "key");
        return this.diskLruCache.get(key);
    }

    public void put(@org.jetbrains.annotations.NotNull java.lang.String key, @org.jetbrains.annotations.NotNull byte[] data) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(key, "key");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(data, "data");
        this.diskLruCache.put(key, data);
    }

    public void remove(@org.jetbrains.annotations.NotNull java.lang.String key) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(key, "key");
        this.diskLruCache.remove(key);
    }

    public void clear() {
        this.diskLruCache.clear();
    }

    public boolean contains(@org.jetbrains.annotations.NotNull java.lang.String key) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(key, "key");
        return this.diskLruCache.contains(key);
    }
}
