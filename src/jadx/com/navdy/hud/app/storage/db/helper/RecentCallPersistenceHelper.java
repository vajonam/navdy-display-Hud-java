package com.navdy.hud.app.storage.db.helper;

public class RecentCallPersistenceHelper {
    private static final java.lang.String BULK_INSERT_SQL = "INSERT INTO recent_calls(device_id,category,name,number,number_type,call_time,call_type,def_image,number_numeric) VALUES (?,?,?,?,?,?,?,?,?)";
    private static final int CALL_TIME_ORDINAL = 5;
    private static final int CALL_TYPE_ORDINAL = 4;
    private static final int CATEGORY_ORDINAL = 0;
    private static final int DEFAULT_IMAGE_INDEX_ORDINAL = 6;
    private static final java.util.List<com.navdy.hud.app.framework.recentcall.RecentCall> EMPTY_LIST = new java.util.LinkedList();
    private static final int NAME_ORDINAL = 1;
    private static final int NUMBER_NUMERIC_ORDINAL = 7;
    private static final int NUMBER_ORDINAL = 2;
    private static final int NUMBER_TYPE_ORDINAL = 3;
    private static final java.lang.String[] RECENT_CALL_ARGS = new java.lang.String[1];
    private static final java.lang.String RECENT_CALL_ORDER_BY = "call_time DESC";
    private static final java.lang.String[] RECENT_CALL_PROJECTION = {com.navdy.hud.app.storage.db.table.RecentCallsTable.CATEGORY, "name", "number", "number_type", com.navdy.hud.app.storage.db.table.RecentCallsTable.CALL_TYPE, com.navdy.hud.app.storage.db.table.RecentCallsTable.CALL_TIME, "def_image", "number_numeric"};
    private static final java.lang.String RECENT_CALL_WHERE = "device_id=?";
    private static final java.lang.Object lockObj = new java.lang.Object();
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.storage.db.helper.RecentCallPersistenceHelper.class);
    private static final java.util.Comparator<com.navdy.hud.app.framework.recentcall.RecentCall> sReverseComparator = new com.navdy.hud.app.storage.db.helper.RecentCallPersistenceHelper.Anon1();

    static class Anon1 implements java.util.Comparator<com.navdy.hud.app.framework.recentcall.RecentCall> {
        Anon1() {
        }

        public int compare(com.navdy.hud.app.framework.recentcall.RecentCall lhs, com.navdy.hud.app.framework.recentcall.RecentCall rhs) {
            return rhs.compareTo(lhs);
        }
    }

    public static java.util.List<com.navdy.hud.app.framework.recentcall.RecentCall> getRecentsCalls(java.lang.String driverId) {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        if (com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile().isDefaultProfile()) {
            return EMPTY_LIST;
        }
        synchronized (lockObj) {
            android.database.sqlite.SQLiteDatabase database = com.navdy.hud.app.storage.db.HudDatabase.getInstance().getWritableDatabase();
            if (database == null) {
                throw new com.navdy.hud.app.storage.db.DatabaseUtil.DatabaseNotAvailable();
            }
            RECENT_CALL_ARGS[0] = driverId;
            android.database.Cursor cursor = database.query(com.navdy.hud.app.storage.db.table.RecentCallsTable.TABLE_NAME, RECENT_CALL_PROJECTION, RECENT_CALL_WHERE, RECENT_CALL_ARGS, null, null, RECENT_CALL_ORDER_BY);
            if (cursor != null) {
                try {
                    if (cursor.moveToFirst()) {
                        com.navdy.hud.app.framework.contacts.ContactImageHelper instance = com.navdy.hud.app.framework.contacts.ContactImageHelper.getInstance();
                        java.util.ArrayList arrayList = new java.util.ArrayList();
                        do {
                            int category = cursor.getInt(0);
                            java.lang.String name = cursor.getString(1);
                            java.lang.String number = cursor.getString(2);
                            int numberType = cursor.getInt(3);
                            int callType = cursor.getInt(4);
                            long callTime = cursor.getLong(5);
                            int imageIndex = cursor.getInt(6);
                            long numericNumber = cursor.getLong(7);
                            java.util.ArrayList arrayList2 = arrayList;
                            arrayList2.add(new com.navdy.hud.app.framework.recentcall.RecentCall(name, com.navdy.hud.app.framework.recentcall.RecentCall.Category.buildFromValue(category), number, com.navdy.hud.app.framework.contacts.NumberType.buildFromValue(numberType), new java.util.Date(1000 * callTime), com.navdy.hud.app.framework.recentcall.RecentCall.CallType.buildFromValue(callType), imageIndex, numericNumber));
                        } while (cursor.moveToNext());
                        return arrayList;
                    }
                } finally {
                    com.navdy.service.library.util.IOUtils.closeStream(cursor);
                }
            }
            java.util.List<com.navdy.hud.app.framework.recentcall.RecentCall> list = EMPTY_LIST;
            com.navdy.service.library.util.IOUtils.closeStream(cursor);
            return list;
        }
    }

    /* JADX INFO: finally extract failed */
    public static void storeRecentCalls(java.lang.String driverId, java.util.List<com.navdy.hud.app.framework.recentcall.RecentCall> newCalls, boolean fromPbap) {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        com.navdy.hud.app.framework.recentcall.RecentCallManager recentCallManager = com.navdy.hud.app.framework.recentcall.RecentCallManager.getInstance();
        java.util.List<com.navdy.hud.app.framework.recentcall.RecentCall> mergedCalls = mergeRecentCalls(recentCallManager.getRecentCalls(), newCalls, fromPbap);
        deleteRecentCalls(driverId);
        if (mergedCalls.size() == 0) {
            recentCallManager.setRecentCalls(null);
            return;
        }
        if (fromPbap) {
            com.navdy.hud.app.framework.contacts.PhoneImageDownloader.getInstance().clearAllPhotoCheckEntries();
        }
        synchronized (lockObj) {
            android.database.sqlite.SQLiteDatabase database = com.navdy.hud.app.storage.db.HudDatabase.getInstance().getWritableDatabase();
            if (database == null) {
                throw new com.navdy.hud.app.storage.db.DatabaseUtil.DatabaseNotAvailable();
            }
            android.database.sqlite.SQLiteStatement sqLiteStatement = database.compileStatement(BULK_INSERT_SQL);
            int rows = 0;
            try {
                com.navdy.hud.app.framework.contacts.ContactImageHelper contactImageHelper = com.navdy.hud.app.framework.contacts.ContactImageHelper.getInstance();
                database.beginTransaction();
                for (com.navdy.hud.app.framework.recentcall.RecentCall recentCall : mergedCalls) {
                    sqLiteStatement.clearBindings();
                    sqLiteStatement.bindString(1, driverId);
                    sqLiteStatement.bindLong(2, (long) recentCall.category.getValue());
                    if (!android.text.TextUtils.isEmpty(recentCall.name)) {
                        sqLiteStatement.bindString(3, recentCall.name);
                    } else {
                        sqLiteStatement.bindNull(3);
                    }
                    sqLiteStatement.bindString(4, recentCall.number);
                    sqLiteStatement.bindLong(5, (long) recentCall.numberType.getValue());
                    sqLiteStatement.bindLong(6, recentCall.callTime.getTime() / 1000);
                    sqLiteStatement.bindLong(7, (long) recentCall.callType.getValue());
                    recentCall.defaultImageIndex = contactImageHelper.getContactImageIndex(recentCall.number);
                    sqLiteStatement.bindLong(8, (long) recentCall.defaultImageIndex);
                    sqLiteStatement.bindLong(9, recentCall.numericNumber);
                    sqLiteStatement.execute();
                    rows++;
                    if (fromPbap) {
                        com.navdy.hud.app.framework.contacts.PhoneImageDownloader.getInstance().submitDownload(recentCall.number, com.navdy.hud.app.framework.contacts.PhoneImageDownloader.Priority.NORMAL, com.navdy.service.library.events.photo.PhotoType.PHOTO_CONTACT, recentCall.name);
                    }
                }
                database.setTransactionSuccessful();
                com.navdy.hud.app.framework.recentcall.RecentCallManager.getInstance().setRecentCalls(mergedCalls);
                database.endTransaction();
                sLogger.v("recent-calls rows added:" + rows);
            } catch (Throwable th) {
                database.endTransaction();
                throw th;
            }
        }
    }

    private static void deleteRecentCalls(java.lang.String driverId) {
        try {
            synchronized (lockObj) {
                android.database.sqlite.SQLiteDatabase database = com.navdy.hud.app.storage.db.HudDatabase.getInstance().getWritableDatabase();
                if (database == null) {
                    throw new com.navdy.hud.app.storage.db.DatabaseUtil.DatabaseNotAvailable();
                }
                RECENT_CALL_ARGS[0] = driverId;
                sLogger.v("recent-calls rows deleted:" + database.delete(com.navdy.hud.app.storage.db.table.RecentCallsTable.TABLE_NAME, RECENT_CALL_WHERE, RECENT_CALL_ARGS));
            }
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }

    private static java.util.List<com.navdy.hud.app.framework.recentcall.RecentCall> mergeRecentCalls(java.util.List<com.navdy.hud.app.framework.recentcall.RecentCall> existingCalls, java.util.List<com.navdy.hud.app.framework.recentcall.RecentCall> newCalls, boolean fromPbap) {
        java.util.HashMap<java.lang.Long, com.navdy.hud.app.framework.recentcall.RecentCall> map = new java.util.HashMap<>(52);
        if (existingCalls != null) {
            for (com.navdy.hud.app.framework.recentcall.RecentCall existingCall : existingCalls) {
                if (!fromPbap || existingCall.category != com.navdy.hud.app.framework.recentcall.RecentCall.Category.PHONE_CALL) {
                    map.put(java.lang.Long.valueOf(existingCall.numericNumber), existingCall);
                }
            }
        }
        if (newCalls != null) {
            for (com.navdy.hud.app.framework.recentcall.RecentCall newCall : newCalls) {
                com.navdy.hud.app.framework.recentcall.RecentCall existingCall2 = (com.navdy.hud.app.framework.recentcall.RecentCall) map.get(java.lang.Long.valueOf(newCall.numericNumber));
                if (existingCall2 == null) {
                    map.put(java.lang.Long.valueOf(newCall.numericNumber), newCall);
                } else if (fromPbap) {
                    map.put(java.lang.Long.valueOf(newCall.numericNumber), newCall);
                } else if (newCall.callTime.getTime() >= existingCall2.callTime.getTime()) {
                    if (newCall.category == com.navdy.hud.app.framework.recentcall.RecentCall.Category.MESSAGE && android.text.TextUtils.isEmpty(newCall.name)) {
                        newCall.name = existingCall2.name;
                    }
                    map.put(java.lang.Long.valueOf(newCall.numericNumber), newCall);
                }
            }
        }
        java.util.List<com.navdy.hud.app.framework.recentcall.RecentCall> mergedList = new java.util.ArrayList<>(map.values());
        java.util.Collections.sort(mergedList, sReverseComparator);
        if (mergedList.size() <= 30) {
            return mergedList;
        }
        java.util.List<com.navdy.hud.app.framework.recentcall.RecentCall> arrayList = new java.util.ArrayList<>(30);
        for (int i = 0; i < 30; i++) {
            arrayList.add(mergedList.get(i));
        }
        return arrayList;
    }
}
