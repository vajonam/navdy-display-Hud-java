package com.navdy.hud.app.storage.db.table;

public class MusicArtworkCacheTable {
    public static final java.lang.String ALBUM = "album";
    public static final java.lang.String AUTHOR = "author";
    public static final java.lang.String FILE_NAME = "file_name";
    public static final java.lang.String NAME = "name";
    public static final java.lang.String TABLE_NAME = "music_artwork_cache";
    public static final java.lang.String TYPE = "type";
    public static final java.lang.String TYPE_AUDIOBOOK = "audiobook";
    public static final java.lang.String TYPE_MUSIC = "music";
    public static final java.lang.String TYPE_PLAYLIST = "playlist";
    public static final java.lang.String TYPE_PODCAST = "podcast";
    private static final java.lang.String[] VALID_TYPES = {TYPE_MUSIC, TYPE_PLAYLIST, TYPE_PODCAST, TYPE_AUDIOBOOK};
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.storage.db.table.MusicArtworkCacheTable.class);

    public static void createTable(android.database.sqlite.SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " (" + "type" + " TEXT NOT NULL CHECK(" + "type" + " IN " + validTypes() + "), " + AUTHOR + " TEXT, " + ALBUM + " TEXT, " + "name" + " TEXT, " + FILE_NAME + " TEXT NOT NULL, UNIQUE (" + "type" + ", " + AUTHOR + ", " + ALBUM + ", " + "name" + ") ON CONFLICT REPLACE);");
        com.navdy.hud.app.storage.db.DatabaseUtil.createIndex(db, TABLE_NAME, "type", sLogger);
        com.navdy.hud.app.storage.db.DatabaseUtil.createIndex(db, TABLE_NAME, ALBUM, sLogger);
        com.navdy.hud.app.storage.db.DatabaseUtil.createIndex(db, TABLE_NAME, AUTHOR, sLogger);
        com.navdy.hud.app.storage.db.DatabaseUtil.createIndex(db, TABLE_NAME, "name", sLogger);
    }

    private static java.lang.String validTypes() {
        java.lang.StringBuilder sb = new java.lang.StringBuilder();
        sb.append("('");
        sb.append(VALID_TYPES[0]);
        for (int i = 1; i < VALID_TYPES.length; i++) {
            sb.append("', '");
            sb.append(VALID_TYPES[i]);
        }
        sb.append("')");
        return sb.toString();
    }
}
