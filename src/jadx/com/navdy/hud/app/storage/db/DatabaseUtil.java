package com.navdy.hud.app.storage.db;

public class DatabaseUtil {
    public static final java.lang.String ROW_ID = "rowid";

    public static class DatabaseNotAvailable extends java.lang.RuntimeException {
    }

    public static void dropTable(android.database.sqlite.SQLiteDatabase db, java.lang.String tableName, com.navdy.service.library.log.Logger logger) {
        try {
            if (!android.text.TextUtils.isEmpty(tableName)) {
                db.execSQL("DROP TABLE IF EXISTS " + tableName);
                logger.v("dropped table[" + tableName + "]");
            }
        } catch (Throwable t) {
            logger.e(t);
        }
    }

    public static void createIndex(android.database.sqlite.SQLiteDatabase db, java.lang.String tableName, java.lang.String key, com.navdy.service.library.log.Logger logger) {
        java.lang.String indexName = tableName + com.navdy.service.library.util.MusicDataUtils.ALTERNATE_SEPARATOR + key;
        db.execSQL("CREATE INDEX IF NOT EXISTS " + indexName + " ON " + tableName + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.OPEN_BRACKET + key + ");");
        logger.v("createdIndex:" + indexName);
    }
}
