package com.navdy.hud.app.storage.db;

public class HudDatabase extends android.database.sqlite.SQLiteOpenHelper {
    private static final java.lang.String DATABASE_FILE = (com.navdy.hud.app.storage.PathManager.getInstance().getDatabaseDir() + java.io.File.separator + DATABASE_NAME);
    private static final java.lang.String DATABASE_NAME = "hud.db";
    private static final int DATABASE_VERSION = 17;
    private static final java.lang.String UPGRADE_METHOD_PREFIX = "upgradeDatabase_";
    private static final int VERSION_1 = 1;
    private static final int VERSION_10 = 10;
    private static final int VERSION_11 = 11;
    private static final int VERSION_12 = 12;
    private static final int VERSION_13 = 13;
    private static final int VERSION_14 = 14;
    private static final int VERSION_15 = 15;
    private static final int VERSION_16 = 16;
    private static final int VERSION_17 = 17;
    private static final int VERSION_2 = 2;
    private static final int VERSION_3 = 3;
    private static final int VERSION_4 = 4;
    private static final int VERSION_5 = 5;
    private static final int VERSION_6 = 6;
    private static final int VERSION_7 = 7;
    private static final int VERSION_8 = 8;
    private static final int VERSION_9 = 9;
    /* access modifiers changed from: private */
    public static final android.content.Context context = com.navdy.hud.app.HudApplication.getAppContext();
    private static final java.lang.Object lockObj = new java.lang.Object();
    /* access modifiers changed from: private */
    public static volatile boolean sDBError;
    /* access modifiers changed from: private */
    public static volatile java.lang.Throwable sDBErrorStr;
    private static android.database.DatabaseErrorHandler sDatabaseErrorHandler = new com.navdy.hud.app.storage.db.HudDatabase.Anon1();
    /* access modifiers changed from: private */
    public static com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.storage.db.HudDatabase.class);
    private static volatile com.navdy.hud.app.storage.db.HudDatabase sSingleton;

    static class Anon1 implements android.database.DatabaseErrorHandler {
        Anon1() {
        }

        public void onCorruption(android.database.sqlite.SQLiteDatabase dbObj) {
            com.navdy.hud.app.storage.db.HudDatabase.sLogger.e("**** DB Corrupt ****");
            com.navdy.hud.app.storage.db.HudDatabase.sDBError = true;
            com.navdy.hud.app.storage.db.HudDatabase.sDBErrorStr = null;
            try {
                if (!dbObj.isOpen()) {
                    com.navdy.hud.app.storage.db.HudDatabase.deleteDatabaseFile();
                    return;
                }
                java.util.List<android.util.Pair<java.lang.String, java.lang.String>> attachedDbs = null;
                try {
                    attachedDbs = dbObj.getAttachedDbs();
                } catch (android.database.sqlite.SQLiteException e) {
                    com.navdy.hud.app.storage.db.HudDatabase.sLogger.i((java.lang.Throwable) e);
                } catch (Throwable th) {
                    java.lang.Throwable th2 = th;
                    if (attachedDbs != null) {
                        for (android.util.Pair<java.lang.String, java.lang.String> p : attachedDbs) {
                            com.navdy.service.library.util.IOUtils.deleteFile(com.navdy.hud.app.storage.db.HudDatabase.context, (java.lang.String) p.second);
                        }
                    }
                    com.navdy.hud.app.storage.db.HudDatabase.deleteDatabaseFile();
                    throw th2;
                }
                try {
                    dbObj.close();
                } catch (android.database.sqlite.SQLiteException e2) {
                    com.navdy.hud.app.storage.db.HudDatabase.sLogger.i((java.lang.Throwable) e2);
                }
                if (attachedDbs != null) {
                    for (android.util.Pair<java.lang.String, java.lang.String> p2 : attachedDbs) {
                        com.navdy.service.library.util.IOUtils.deleteFile(com.navdy.hud.app.storage.db.HudDatabase.context, (java.lang.String) p2.second);
                    }
                }
                com.navdy.hud.app.storage.db.HudDatabase.deleteDatabaseFile();
            } catch (Throwable t) {
                com.navdy.hud.app.storage.db.HudDatabase.sLogger.e(t);
            }
        }
    }

    static class Anon2 implements java.io.FilenameFilter {
        Anon2() {
        }

        public boolean accept(java.io.File dir, java.lang.String filename) {
            if (filename.contains("native_crash_")) {
                return true;
            }
            return false;
        }
    }

    public static com.navdy.hud.app.storage.db.HudDatabase getInstance() {
        if (sSingleton == null) {
            synchronized (lockObj) {
                if (sSingleton == null) {
                    try {
                        if (!isDatabaseStable(DATABASE_FILE)) {
                            sLogger.i("db is not stable, deleting it");
                            deleteDatabaseFile();
                            sLogger.i("db delete complete");
                        }
                        sLogger.v("creating db-instance");
                        sSingleton = new com.navdy.hud.app.storage.db.HudDatabase();
                        sLogger.v("created db-instance");
                    } catch (Throwable t) {
                        sDBError = true;
                        sDBErrorStr = t;
                        sLogger.e(t);
                    }
                }
            }
        }
        return sSingleton;
    }

    private HudDatabase() {
        super(context, DATABASE_FILE, null, 17, sDatabaseErrorHandler);
    }

    /* JADX INFO: finally extract failed */
    public void onCreate(android.database.sqlite.SQLiteDatabase db) {
        sDBError = false;
        sDBErrorStr = null;
        sLogger.v("onCreate::start::");
        db.beginTransaction();
        try {
            com.navdy.hud.app.storage.db.table.RecentCallsTable.createTable(db);
            com.navdy.hud.app.storage.db.table.FavoriteContactsTable.createTable(db);
            com.navdy.hud.app.storage.db.table.VinInformationTable.createTable(db);
            com.navdy.hud.app.storage.db.table.MusicArtworkCacheTable.createTable(db);
            db.setTransactionSuccessful();
            db.endTransaction();
            sLogger.v("onCreate::end::");
        } catch (Throwable th) {
            db.endTransaction();
            throw th;
        }
    }

    public void onUpgrade(android.database.sqlite.SQLiteDatabase db, int oldVersion, int newVersion) {
        sLogger.v("onUpgrade::start:: " + oldVersion + " ==>" + newVersion);
        java.lang.Object[] params = {db};
        java.lang.Class[] reflectionParams = {android.database.sqlite.SQLiteDatabase.class};
        int i = oldVersion + 1;
        while (i <= newVersion) {
            try {
                sLogger.v("onUpgrade:: " + i);
                com.navdy.hud.app.storage.db.HudDatabase.class.getMethod(UPGRADE_METHOD_PREFIX + i, reflectionParams).invoke(null, params);
                i++;
            } catch (Throwable t) {
                sLogger.e(t);
                throw new java.lang.RuntimeException(t);
            }
        }
        sLogger.v("onUpgrade::end:: " + oldVersion + " ==>" + newVersion);
    }

    public void onOpen(android.database.sqlite.SQLiteDatabase db) {
        sLogger.v("onOpen::");
        super.onOpen(db);
    }

    public synchronized android.database.sqlite.SQLiteDatabase getWritableDatabase() {
        android.database.sqlite.SQLiteDatabase db;
        db = null;
        try {
            db = super.getWritableDatabase();
        } catch (Throwable ex) {
            sLogger.e(ex);
            if (ex instanceof android.database.sqlite.SQLiteDatabaseCorruptException) {
                sLogger.e(":: db corrupted");
            } else {
                sDBError = true;
                sDBErrorStr = ex;
            }
        }
        return db;
    }

    /* JADX INFO: finally extract failed */
    private static boolean isDatabaseIntegrityFine(android.database.sqlite.SQLiteDatabase db) {
        boolean z;
        android.database.sqlite.SQLiteStatement stmt = null;
        try {
            long l1 = java.lang.System.currentTimeMillis();
            android.database.sqlite.SQLiteStatement stmt2 = db.compileStatement("PRAGMA integrity_check");
            java.lang.String result = stmt2.simpleQueryForString();
            sLogger.v("db integrity check took:" + (java.lang.System.currentTimeMillis() - l1));
            if (android.text.TextUtils.isEmpty(result) || !"ok".equalsIgnoreCase(result)) {
                z = false;
                if (stmt2 != null) {
                    stmt2.close();
                }
            } else {
                z = true;
                if (stmt2 != null) {
                    stmt2.close();
                }
            }
            return z;
        } catch (Throwable th) {
            if (stmt != null) {
                stmt.close();
            }
            throw th;
        }
    }

    private static boolean isDatabaseStable(java.lang.String dbFilePath) {
        java.io.File dbFile = new java.io.File(dbFilePath);
        if (!dbFile.exists()) {
            return true;
        }
        try {
            android.database.sqlite.SQLiteDatabase dbase = android.database.sqlite.SQLiteDatabase.openDatabase(dbFilePath, null, 16);
            if (dbase == null) {
                sLogger.e("dbase could not be opened");
                return false;
            }
            try {
                if (!isDatabaseIntegrityFine(dbase)) {
                    sLogger.e("data integrity is not valid");
                    dbase.close();
                    return false;
                }
                if (dbase.inTransaction()) {
                    sLogger.v("dbase in transaction");
                    dbase.endTransaction();
                }
                if (dbase.isDbLockedByCurrentThread()) {
                    sLogger.v("dbase is currently locked");
                }
                long l1 = java.lang.System.currentTimeMillis();
                dbase.execSQL("VACUUM");
                sLogger.i("dbase VACCUM complete in " + (java.lang.System.currentTimeMillis() - l1));
                if (dbFile.exists() && dbFile.canRead()) {
                    return true;
                }
                sLogger.e("dbase not stable");
                return false;
            } catch (java.lang.Exception e) {
                sLogger.e((java.lang.Throwable) e);
            } finally {
                dbase.close();
            }
        } catch (android.database.sqlite.SQLiteException e2) {
            sLogger.e("dbase could not be opened", e2);
            return false;
        }
    }

    public static void deleteDatabaseFile() {
        sLogger.v("deleteDatabaseFile");
        try {
            java.io.File dbFile = new java.io.File(DATABASE_FILE);
            if (dbFile.exists()) {
                java.lang.String str = dbFile.getAbsolutePath();
                sLogger.e("delete:" + com.navdy.service.library.util.IOUtils.deleteFile(context, str) + " - " + str);
            }
            java.io.File file = new java.io.File(DATABASE_FILE + "-journal");
            if (file.exists()) {
                java.lang.String str2 = file.getAbsolutePath();
                sLogger.e("delete:" + com.navdy.service.library.util.IOUtils.deleteFile(context, str2) + " - " + str2);
            }
            java.io.File file2 = new java.io.File(DATABASE_FILE + "-shm");
            if (file2.exists()) {
                java.lang.String str3 = file2.getAbsolutePath();
                sLogger.e("delete:" + com.navdy.service.library.util.IOUtils.deleteFile(context, str3) + " - " + str3);
            }
            java.io.File file3 = new java.io.File(DATABASE_FILE + "-wal");
            if (file3.exists()) {
                java.lang.String str4 = file3.getAbsolutePath();
                sLogger.e("delete:" + com.navdy.service.library.util.IOUtils.deleteFile(context, str4) + " - " + str4);
            }
            android.database.sqlite.SQLiteDatabase.deleteDatabase(dbFile);
        } catch (Throwable th) {
            sLogger.e("Error while trying to delete dbase");
        }
    }

    public static boolean isInErrorState() {
        return sDBError;
    }

    public static java.lang.Throwable getErrorStr() {
        return sDBErrorStr;
    }

    public static void upgradeDatabase_2(android.database.sqlite.SQLiteDatabase db) {
        com.navdy.hud.app.storage.db.table.RecentCallsTable.upgradeDatabase_2(db);
        com.navdy.hud.app.storage.db.table.FavoriteContactsTable.createTable(db);
    }

    public static void upgradeDatabase_3(android.database.sqlite.SQLiteDatabase db) {
    }

    public static void upgradeDatabase_4(android.database.sqlite.SQLiteDatabase db) {
    }

    public static void upgradeDatabase_5(android.database.sqlite.SQLiteDatabase db) {
    }

    public static void upgradeDatabase_6(android.database.sqlite.SQLiteDatabase db) {
    }

    public static void upgradeDatabase_7(android.database.sqlite.SQLiteDatabase db) {
        java.io.File dir = new java.io.File(com.navdy.hud.app.storage.PathManager.getInstance().getHereVoiceSkinsPath(), "en-US_TTS");
        com.navdy.service.library.util.IOUtils.deleteDirectory(com.navdy.hud.app.HudApplication.getAppContext(), dir);
        sLogger.v("deleted old here voice skins [" + dir + "]");
    }

    public static void upgradeDatabase_8(android.database.sqlite.SQLiteDatabase db) {
        com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getSharedPreferences().edit().putString(com.navdy.hud.app.settings.HUDSettings.LED_BRIGHTNESS, "255").apply();
        sLogger.v("default max LED brightness changed to 255");
    }

    public static void upgradeDatabase_9(android.database.sqlite.SQLiteDatabase db) {
        com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getSharedPreferences().edit().putBoolean(com.navdy.hud.app.settings.HUDSettings.GESTURE_ENGINE, false).apply();
        sLogger.v("disabling gesture engine");
    }

    @android.annotation.SuppressLint({"CommitPrefEdits"})
    public static void upgradeDatabase_10(android.database.sqlite.SQLiteDatabase db) {
        com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getSharedPreferences().edit().putBoolean(com.navdy.hud.app.settings.HUDSettings.GESTURE_ENGINE, true).commit();
        sLogger.v("enabling gesture engine");
    }

    public static void upgradeDatabase_11(android.database.sqlite.SQLiteDatabase db) {
        java.lang.String property = "persist.sys.hud_gps";
        try {
            sLogger.v("trying to set [" + property + "]");
            char c = com.navdy.hud.app.util.SerialNumber.instance.revisionCode.charAt(0);
            if (c < '4' || c > '6') {
                sLogger.v("not setting property [" + android.os.Build.SERIAL + "]");
                return;
            }
            com.navdy.hud.app.util.os.SystemProperties.set(property, "-1");
            sLogger.v("setting property for [" + android.os.Build.SERIAL + "] to [-1] digit[" + c + "]");
        } catch (Throwable t) {
            sLogger.e("dbupgrade --> 11 failed", t);
        }
    }

    public static void upgradeDatabase_12(android.database.sqlite.SQLiteDatabase db) {
        try {
            android.content.Context context2 = com.navdy.hud.app.HudApplication.getAppContext();
            com.navdy.service.library.util.IOUtils.deleteDirectory(context2, new java.io.File(context2.getFilesDir(), ".Fabric"));
            sLogger.v("dbupgrade --> Fabric dir removed");
        } catch (Throwable t) {
            sLogger.e("dbupgrade --> 12 failed", t);
        }
    }

    public static void upgradeDatabase_13(android.database.sqlite.SQLiteDatabase db) {
        com.navdy.hud.app.storage.db.table.VinInformationTable.createTable(db);
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0045 A[Catch:{ Throwable -> 0x00b4, Throwable -> 0x00bc }] */
    public static void upgradeDatabase_14(android.database.sqlite.SQLiteDatabase db) {
        java.io.File f;
        java.lang.String NATIVE_CRASH_PATH = "/data/anr";
        java.lang.String str = "native_crash_";
        try {
            sLogger.v("looking for native crash files");
            java.io.File f2 = new java.io.File(NATIVE_CRASH_PATH);
            if (!f2.exists() || !f2.isDirectory()) {
                sLogger.v("no native crashes to delete");
                sLogger.v("looking for tombstone");
                java.lang.String TOMBSTONE_PATH = "/data/tombstones";
                f = new java.io.File(TOMBSTONE_PATH);
                if (f.exists() || !f.isDirectory()) {
                    sLogger.v("no tombstones to delete");
                }
                java.lang.String[] list = f.list();
                if (list == null || list.length == 0) {
                    sLogger.v("no tombstones");
                    return;
                }
                for (int i = 0; i < list.length; i++) {
                    java.io.File tombstoneFile = new java.io.File(TOMBSTONE_PATH + java.io.File.separator + list[i]);
                    java.lang.String fileName = tombstoneFile.getAbsolutePath();
                    sLogger.v("removing tombstone file[" + fileName + "] :" + tombstoneFile.delete());
                }
                return;
            }
            java.lang.String[] list2 = f2.list(new com.navdy.hud.app.storage.db.HudDatabase.Anon2());
            if (list2 == null || list2.length == 0) {
                sLogger.v("no native crash files");
                sLogger.v("looking for tombstone");
                java.lang.String TOMBSTONE_PATH2 = "/data/tombstones";
                f = new java.io.File(TOMBSTONE_PATH2);
                if (f.exists()) {
                }
                sLogger.v("no tombstones to delete");
            }
            for (int i2 = 0; i2 < list2.length; i2++) {
                java.io.File nativeCrashFile = new java.io.File(NATIVE_CRASH_PATH + java.io.File.separator + list2[i2]);
                java.lang.String fileName2 = nativeCrashFile.getAbsolutePath();
                sLogger.v("removing native crash file[" + fileName2 + "] :" + nativeCrashFile.delete());
            }
            sLogger.v("looking for tombstone");
            java.lang.String TOMBSTONE_PATH22 = "/data/tombstones";
            f = new java.io.File(TOMBSTONE_PATH22);
            if (f.exists()) {
            }
            sLogger.v("no tombstones to delete");
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }

    @android.annotation.SuppressLint({"CommitPrefEdits"})
    public static void upgradeDatabase_15(android.database.sqlite.SQLiteDatabase db) {
        com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getSharedPreferences().edit().remove(com.navdy.hud.app.device.PowerManager.LAST_LOW_VOLTAGE_EVENT).commit();
    }

    public static void upgradeDatabase_16(android.database.sqlite.SQLiteDatabase db) {
        com.navdy.hud.app.storage.db.table.MusicArtworkCacheTable.createTable(db);
    }

    @android.annotation.SuppressLint({"CommitPrefEdits"})
    public static void upgradeDatabase_17(android.database.sqlite.SQLiteDatabase db) {
        com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getSharedPreferences().edit().remove(com.navdy.hud.app.manager.UpdateReminderManager.DIAL_REMINDER_TIME).commit();
    }
}
