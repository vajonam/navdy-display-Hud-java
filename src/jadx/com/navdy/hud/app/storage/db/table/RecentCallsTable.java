package com.navdy.hud.app.storage.db.table;

public class RecentCallsTable {
    public static final java.lang.String CALL_TIME = "call_time";
    public static final java.lang.String CALL_TYPE = "call_type";
    public static final java.lang.String CATEGORY = "category";
    public static final java.lang.String DEFAULT_IMAGE_INDEX = "def_image";
    public static final java.lang.String DRIVER_ID = "device_id";
    public static final java.lang.String NAME = "name";
    public static final java.lang.String NUMBER = "number";
    public static final java.lang.String NUMBER_NUMERIC = "number_numeric";
    public static final java.lang.String NUMBER_TYPE = "number_type";
    public static final java.lang.String TABLE_NAME = "recent_calls";
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.storage.db.table.RecentCallsTable.class);

    public static void createTable(android.database.sqlite.SQLiteDatabase db) {
        createTable_2(db);
    }

    public static void createTable_2(android.database.sqlite.SQLiteDatabase db) {
        java.lang.String tableName = TABLE_NAME;
        db.execSQL("CREATE TABLE IF NOT EXISTS " + tableName + " (" + "device_id" + " TEXT NOT NULL," + CATEGORY + " INTEGER NOT NULL," + "name" + " TEXT," + "number" + " TEXT NOT NULL," + "number_type" + " INTEGER NOT NULL," + CALL_TIME + " INTEGER," + CALL_TYPE + " INTEGER," + "def_image" + " INTEGER," + "number_numeric" + " INTEGER" + ");");
        sLogger.v("createdTable:" + tableName);
        java.lang.String indexName = tableName + com.navdy.service.library.util.MusicDataUtils.ALTERNATE_SEPARATOR + "device_id";
        db.execSQL("CREATE INDEX IF NOT EXISTS " + indexName + " ON " + tableName + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.OPEN_BRACKET + "device_id" + ");");
        sLogger.v("createdIndex:" + indexName);
        java.lang.String indexName2 = tableName + com.navdy.service.library.util.MusicDataUtils.ALTERNATE_SEPARATOR + CATEGORY;
        db.execSQL("CREATE INDEX IF NOT EXISTS " + indexName2 + " ON " + tableName + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.OPEN_BRACKET + CATEGORY + ");");
        sLogger.v("createdIndex:" + indexName2);
        java.lang.String indexName3 = tableName + com.navdy.service.library.util.MusicDataUtils.ALTERNATE_SEPARATOR + "number";
        db.execSQL("CREATE INDEX IF NOT EXISTS " + indexName3 + " ON " + tableName + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.OPEN_BRACKET + "number" + ");");
        sLogger.v("createdIndex:" + indexName3);
        java.lang.String indexName4 = tableName + com.navdy.service.library.util.MusicDataUtils.ALTERNATE_SEPARATOR + "number_type";
        db.execSQL("CREATE INDEX IF NOT EXISTS " + indexName4 + " ON " + tableName + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.OPEN_BRACKET + "number_type" + ");");
        sLogger.v("createdIndex:" + indexName4);
        java.lang.String indexName5 = tableName + com.navdy.service.library.util.MusicDataUtils.ALTERNATE_SEPARATOR + CALL_TYPE;
        db.execSQL("CREATE INDEX IF NOT EXISTS " + indexName5 + " ON " + tableName + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.OPEN_BRACKET + CALL_TYPE + ");");
        sLogger.v("createdIndex:" + indexName5);
        java.lang.String indexName6 = tableName + com.navdy.service.library.util.MusicDataUtils.ALTERNATE_SEPARATOR + "def_image";
        db.execSQL("CREATE INDEX IF NOT EXISTS " + indexName6 + " ON " + tableName + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.OPEN_BRACKET + "def_image" + ");");
        sLogger.v("createdIndex:" + indexName6);
        java.lang.String indexName7 = tableName + com.navdy.service.library.util.MusicDataUtils.ALTERNATE_SEPARATOR + "number_numeric";
        db.execSQL("CREATE INDEX IF NOT EXISTS " + indexName7 + " ON " + tableName + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.OPEN_BRACKET + "number_numeric" + ");");
        sLogger.v("createdIndex:" + indexName7);
    }

    public static void upgradeDatabase_2(android.database.sqlite.SQLiteDatabase db) {
        com.navdy.hud.app.storage.db.DatabaseUtil.dropTable(db, TABLE_NAME, sLogger);
        createTable_2(db);
    }
}
