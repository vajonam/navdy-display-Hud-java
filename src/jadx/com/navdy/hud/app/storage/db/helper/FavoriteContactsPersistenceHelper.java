package com.navdy.hud.app.storage.db.helper;

public class FavoriteContactsPersistenceHelper {
    private static final java.lang.String BULK_INSERT_SQL = "INSERT INTO fav_contacts(device_id,name,number,number_type,def_image,number_numeric) VALUES (?,?,?,?,?,?)";
    private static final int DEFAULT_IMAGE_INDEX_ORDINAL = 3;
    private static final java.util.List<com.navdy.hud.app.framework.contacts.Contact> EMPTY_LIST = new java.util.LinkedList();
    private static final java.lang.String[] FAV_CONTACT_ARGS = new java.lang.String[1];
    private static final java.lang.String FAV_CONTACT_ORDER_BY = "rowid";
    private static final java.lang.String[] FAV_CONTACT_PROJECTION = {"name", "number", "number_type", "def_image", "number_numeric"};
    private static final java.lang.String FAV_CONTACT_WHERE = "device_id=?";
    private static final int NAME_ORDINAL = 0;
    private static final int NUMBER_NUMERIC_ORDINAL = 4;
    private static final int NUMBER_ORDINAL = 1;
    private static final int NUMBER_TYPE_ORDINAL = 2;
    private static final java.lang.Object lockObj = new java.lang.Object();
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.storage.db.helper.FavoriteContactsPersistenceHelper.class);

    public static java.util.List<com.navdy.hud.app.framework.contacts.Contact> getFavoriteContacts(java.lang.String driverId) {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        if (com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile().isDefaultProfile()) {
            return EMPTY_LIST;
        }
        synchronized (lockObj) {
            android.database.sqlite.SQLiteDatabase database = com.navdy.hud.app.storage.db.HudDatabase.getInstance().getWritableDatabase();
            if (database == null) {
                throw new com.navdy.hud.app.storage.db.DatabaseUtil.DatabaseNotAvailable();
            }
            FAV_CONTACT_ARGS[0] = driverId;
            android.database.Cursor cursor = database.query(com.navdy.hud.app.storage.db.table.FavoriteContactsTable.TABLE_NAME, FAV_CONTACT_PROJECTION, FAV_CONTACT_WHERE, FAV_CONTACT_ARGS, null, null, "rowid");
            if (cursor != null) {
                try {
                    if (cursor.moveToFirst()) {
                        com.navdy.hud.app.framework.contacts.ContactImageHelper instance = com.navdy.hud.app.framework.contacts.ContactImageHelper.getInstance();
                        java.util.List<com.navdy.hud.app.framework.contacts.Contact> favContacts = new java.util.ArrayList<>();
                        do {
                            java.lang.String name = cursor.getString(0);
                            java.lang.String number = cursor.getString(1);
                            int numberType = cursor.getInt(2);
                            favContacts.add(new com.navdy.hud.app.framework.contacts.Contact(name, number, com.navdy.hud.app.framework.contacts.NumberType.buildFromValue(numberType), cursor.getInt(3), cursor.getLong(4)));
                        } while (cursor.moveToNext());
                        return favContacts;
                    }
                } finally {
                    com.navdy.service.library.util.IOUtils.closeStream(cursor);
                }
            }
            java.util.List<com.navdy.hud.app.framework.contacts.Contact> list = EMPTY_LIST;
            com.navdy.service.library.util.IOUtils.closeStream(cursor);
            return list;
        }
    }

    /* JADX INFO: finally extract failed */
    public static void storeFavoriteContacts(java.lang.String driverId, java.util.List<com.navdy.hud.app.framework.contacts.Contact> favContacts, boolean refreshPhotos) {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        sLogger.v("fav-contacts passed [" + favContacts.size() + "]");
        com.navdy.hud.app.framework.contacts.FavoriteContactsManager favoriteContactsManager = com.navdy.hud.app.framework.contacts.FavoriteContactsManager.getInstance();
        deleteFavoriteContacts(driverId);
        if (favContacts.size() == 0) {
            favoriteContactsManager.setFavoriteContacts(null);
            return;
        }
        com.navdy.hud.app.framework.contacts.PhoneImageDownloader.getInstance().clearAllPhotoCheckEntries();
        synchronized (lockObj) {
            android.database.sqlite.SQLiteDatabase database = com.navdy.hud.app.storage.db.HudDatabase.getInstance().getWritableDatabase();
            if (database == null) {
                throw new com.navdy.hud.app.storage.db.DatabaseUtil.DatabaseNotAvailable();
            }
            android.database.sqlite.SQLiteStatement sqLiteStatement = database.compileStatement(BULK_INSERT_SQL);
            int rows = 0;
            try {
                com.navdy.hud.app.framework.contacts.ContactImageHelper contactImageHelper = com.navdy.hud.app.framework.contacts.ContactImageHelper.getInstance();
                database.beginTransaction();
                for (com.navdy.hud.app.framework.contacts.Contact favContact : favContacts) {
                    sqLiteStatement.clearBindings();
                    sqLiteStatement.bindString(1, driverId);
                    if (!android.text.TextUtils.isEmpty(favContact.name)) {
                        sqLiteStatement.bindString(2, favContact.name);
                    } else {
                        sqLiteStatement.bindNull(2);
                    }
                    sqLiteStatement.bindString(3, favContact.number);
                    sqLiteStatement.bindLong(4, (long) favContact.numberType.getValue());
                    favContact.defaultImageIndex = contactImageHelper.getContactImageIndex(favContact.number);
                    sqLiteStatement.bindLong(5, (long) favContact.defaultImageIndex);
                    sqLiteStatement.bindLong(6, favContact.numericNumber);
                    sqLiteStatement.execute();
                    rows++;
                    if (refreshPhotos) {
                        com.navdy.hud.app.framework.contacts.PhoneImageDownloader.getInstance().submitDownload(favContact.number, com.navdy.hud.app.framework.contacts.PhoneImageDownloader.Priority.NORMAL, com.navdy.service.library.events.photo.PhotoType.PHOTO_CONTACT, favContact.name);
                    }
                }
                database.setTransactionSuccessful();
                com.navdy.hud.app.framework.contacts.FavoriteContactsManager.getInstance().setFavoriteContacts(favContacts);
                database.endTransaction();
                sLogger.v("fav-contacts rows added:" + rows);
            } catch (Throwable th) {
                database.endTransaction();
                throw th;
            }
        }
    }

    private static void deleteFavoriteContacts(java.lang.String driverId) {
        try {
            synchronized (lockObj) {
                android.database.sqlite.SQLiteDatabase database = com.navdy.hud.app.storage.db.HudDatabase.getInstance().getWritableDatabase();
                if (database == null) {
                    throw new com.navdy.hud.app.storage.db.DatabaseUtil.DatabaseNotAvailable();
                }
                FAV_CONTACT_ARGS[0] = driverId;
                sLogger.v("fav-contact rows deleted:" + database.delete(com.navdy.hud.app.storage.db.table.FavoriteContactsTable.TABLE_NAME, FAV_CONTACT_WHERE, FAV_CONTACT_ARGS));
            }
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }
}
