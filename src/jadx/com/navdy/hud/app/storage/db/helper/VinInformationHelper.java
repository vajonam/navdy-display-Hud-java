package com.navdy.hud.app.storage.db.helper;

public class VinInformationHelper {
    private static final java.lang.String[] DATASET_ARGS = new java.lang.String[1];
    private static final java.lang.String[] DATASET_PROJECTION = {"info"};
    private static final java.lang.String DATASET_WHERE = "vin=?";
    private static final int VIN_INFO_ORDINAL = 0;
    public static final java.lang.String VIN_SHARED_PREF = "vin";
    private static final java.lang.String VIN_SHARED_PREF_NAME = "activeVin";
    private static final java.lang.Object lockObj = new java.lang.Object();
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.storage.db.helper.VinInformationHelper.class);

    public static java.lang.String getVinInfo(java.lang.String vin) {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        synchronized (lockObj) {
            android.database.sqlite.SQLiteDatabase database = com.navdy.hud.app.storage.db.HudDatabase.getInstance().getWritableDatabase();
            if (database == null) {
                throw new com.navdy.hud.app.storage.db.DatabaseUtil.DatabaseNotAvailable();
            }
            DATASET_ARGS[0] = vin;
            android.database.Cursor cursor = database.query(com.navdy.hud.app.storage.db.table.VinInformationTable.TABLE_NAME, DATASET_PROJECTION, DATASET_WHERE, DATASET_ARGS, null, null, null);
            if (cursor != null) {
                try {
                    if (cursor.moveToFirst()) {
                        java.lang.String string = cursor.getString(0);
                        return string;
                    }
                } finally {
                    com.navdy.service.library.util.IOUtils.closeStream(cursor);
                }
            }
            com.navdy.service.library.util.IOUtils.closeStream(cursor);
            return null;
        }
    }

    public static void storeVinInfo(java.lang.String vin, java.lang.String info) {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        deleteVinInfo(vin);
        synchronized (lockObj) {
            try {
                android.database.sqlite.SQLiteDatabase database = com.navdy.hud.app.storage.db.HudDatabase.getInstance().getWritableDatabase();
                if (database == null) {
                    throw new com.navdy.hud.app.storage.db.DatabaseUtil.DatabaseNotAvailable();
                }
                android.content.ContentValues values = new android.content.ContentValues();
                values.put("vin", vin);
                values.put("info", info);
                database.insert(com.navdy.hud.app.storage.db.table.VinInformationTable.TABLE_NAME, null, values);
                sLogger.v("added vin info [ " + vin + " , " + info + "]");
            } catch (Throwable t) {
                sLogger.e(t);
            }
        }
    }

    public static void deleteVinInfo(java.lang.String vin) {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        synchronized (lockObj) {
            try {
                android.database.sqlite.SQLiteDatabase database = com.navdy.hud.app.storage.db.HudDatabase.getInstance().getWritableDatabase();
                if (database == null) {
                    throw new com.navdy.hud.app.storage.db.DatabaseUtil.DatabaseNotAvailable();
                }
                DATASET_ARGS[0] = vin;
                sLogger.v("vin info deleted:" + vin + " result=" + database.delete(com.navdy.hud.app.storage.db.table.VinInformationTable.TABLE_NAME, DATASET_WHERE, DATASET_ARGS));
            } catch (Throwable t) {
                sLogger.e(t);
            }
        }
    }

    public static android.content.SharedPreferences getVinPreference() {
        return com.navdy.hud.app.HudApplication.getAppContext().getSharedPreferences(VIN_SHARED_PREF_NAME, 4);
    }
}
