package com.navdy.hud.app.storage.db.table;

public class VinInformationTable {
    public static final java.lang.String TABLE_NAME = "vininfo";
    public static final java.lang.String UNKNOWN_VIN = "UNKNOWN_VIN";
    public static final java.lang.String VIN_INFO = "info";
    public static final java.lang.String VIN_NUMBER = "vin";
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.storage.db.table.VinInformationTable.class);

    public static void createTable(android.database.sqlite.SQLiteDatabase db) {
        createTable_1(db);
    }

    public static void createTable_1(android.database.sqlite.SQLiteDatabase db) {
        java.lang.String tableName = TABLE_NAME;
        db.execSQL("CREATE TABLE IF NOT EXISTS " + tableName + " (" + "vin" + " TEXT NOT NULL," + "info" + " TEXT" + ");");
        sLogger.v("createdTable:" + tableName);
        com.navdy.hud.app.storage.db.DatabaseUtil.createIndex(db, TABLE_NAME, "vin", sLogger);
    }
}
