package com.navdy.hud.app;

public final class HudApplication$$InjectAdapter extends dagger.internal.Binding<com.navdy.hud.app.HudApplication> implements javax.inject.Provider<com.navdy.hud.app.HudApplication>, dagger.MembersInjector<com.navdy.hud.app.HudApplication> {
    private dagger.internal.Binding<com.squareup.otto.Bus> bus;

    public HudApplication$$InjectAdapter() {
        super("com.navdy.hud.app.HudApplication", "members/com.navdy.hud.app.HudApplication", false, com.navdy.hud.app.HudApplication.class);
    }

    public void attach(dagger.internal.Linker linker) {
        this.bus = linker.requestBinding("com.squareup.otto.Bus", com.navdy.hud.app.HudApplication.class, getClass().getClassLoader());
    }

    public void getDependencies(java.util.Set<dagger.internal.Binding<?>> set, java.util.Set<dagger.internal.Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.bus);
    }

    public com.navdy.hud.app.HudApplication get() {
        com.navdy.hud.app.HudApplication result = new com.navdy.hud.app.HudApplication();
        injectMembers(result);
        return result;
    }

    public void injectMembers(com.navdy.hud.app.HudApplication object) {
        object.bus = (com.squareup.otto.Bus) this.bus.get();
    }
}
