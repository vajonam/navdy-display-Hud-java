package com.navdy.hud.app;

public class HudApplication extends android.support.multidex.MultiDexApplication {
    public static final java.lang.String NOT_A_CRASH = "NAVDY_NOT_A_CRASH";
    private static final java.lang.String TAG = "HudApplication";
    private static android.content.Context sAppContext;
    private static com.navdy.hud.app.HudApplication sApplication;
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.HudApplication.class);
    @javax.inject.Inject
    public com.squareup.otto.Bus bus;
    private android.os.Handler handler = new android.os.Handler(android.os.Looper.getMainLooper());
    private boolean initialized;
    private com.navdy.hud.app.event.Shutdown.Reason lastSeenReason = com.navdy.hud.app.event.Shutdown.Reason.UNKNOWN;
    private mortar.MortarScope rootScope;
    private com.navdy.hud.app.manager.UpdateReminderManager updateReminderManager;

    class Anon1 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.hud.app.debug.DriveRecorder val$dataRecorder;

        Anon1(com.navdy.hud.app.debug.DriveRecorder driveRecorder) {
            this.val$dataRecorder = driveRecorder;
        }

        public void run() {
            this.val$dataRecorder.load();
        }
    }

    class Anon2 implements java.lang.Runnable {

        class Anon1 implements java.lang.Runnable {
            Anon1() {
            }

            public void run() {
                com.navdy.hud.app.util.CrashReporter.getInstance().checkForKernelCrashes(com.navdy.hud.app.storage.PathManager.getInstance().getKernelCrashFiles());
            }
        }

        Anon2() {
        }

        public void run() {
            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.HudApplication.Anon2.Anon1(), 1);
        }
    }

    class Anon3 implements java.lang.Runnable {

        class Anon1 implements java.lang.Runnable {
            Anon1() {
            }

            public void run() {
                com.navdy.hud.app.util.CrashReporter crashReporter = com.navdy.hud.app.util.CrashReporter.getInstance();
                crashReporter.checkForAnr();
                crashReporter.checkForTombstones();
                crashReporter.checkForOTAFailure();
            }
        }

        Anon3() {
        }

        public void run() {
            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.HudApplication.Anon3.Anon1(), 1);
        }
    }

    class Anon4 implements java.lang.Runnable {
        Anon4() {
        }

        public void run() {
            long initialTime = android.os.SystemClock.elapsedRealtime();
            com.navdy.hud.app.framework.glympse.GlympseManager.getInstance();
            com.navdy.hud.app.HudApplication.sLogger.v("time taken by Glympse init: " + (android.os.SystemClock.elapsedRealtime() - initialTime) + " ms");
        }
    }

    public static void setContext(android.content.Context context) {
        sAppContext = context;
    }

    /* access modifiers changed from: protected */
    public void attachBaseContext(android.content.Context base) {
        super.attachBaseContext(com.navdy.hud.app.profile.HudLocale.onAttach(base));
    }

    public void onCreate() {
        if (com.navdy.hud.app.BuildConfig.DEBUG) {
            android.os.StrictMode.setThreadPolicy(new android.os.StrictMode.ThreadPolicy.Builder().detectAll().penaltyLog().penaltyFlashScreen().penaltyDeathOnNetwork().build());
            android.os.StrictMode.setVmPolicy(new android.os.StrictMode.VmPolicy.Builder().detectAll().penaltyLog().build());
            android.util.Log.v("", "enabling StrictMode");
        }
        android.util.Log.e("", "::onCreate locale:" + getResources().getConfiguration().locale);
        sApplication = this;
        sAppContext = sApplication;
        super.onCreate();
        java.lang.String processName = com.navdy.service.library.util.SystemUtils.getProcessName(sAppContext, android.os.Process.myPid());
        java.lang.String packageName = getPackageName();
        initLogger(false);
        sLogger.d(processName + " starting  on " + android.os.Build.BRAND + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA + android.os.Build.HARDWARE + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA + android.os.Build.MODEL);
        if (processName.equalsIgnoreCase(packageName + ":connectionService")) {
            sLogger.v("startup:" + processName + " :connection service");
            initConnectionService(processName);
        } else if (processName.equalsIgnoreCase(packageName)) {
            sLogger.v("startup:" + processName + " :hud app locale=" + java.util.Locale.getDefault().toString());
            sLogger.v(processName + ":initializing Mortar");
            long l1 = android.os.SystemClock.elapsedRealtime();
            com.navdy.hud.app.common.ProdModule module = new com.navdy.hud.app.common.ProdModule(this);
            this.rootScope = mortar.Mortar.createRootScope(com.navdy.hud.app.BuildConfig.DEBUG, dagger.ObjectGraph.create(module));
            this.rootScope.getObjectGraph().inject(this);
            sLogger.v(processName + ":mortar init took:" + (android.os.SystemClock.elapsedRealtime() - l1));
            initTaskManager(processName);
            sLogger.i("init network state manager");
            com.navdy.hud.app.framework.network.NetworkStateManager.getInstance();
            sLogger.i("*** starting hud sticky-service ***");
            android.content.Intent intent = new android.content.Intent();
            intent.setClass(this, com.navdy.hud.app.service.StickyService.class);
            startService(intent);
            sLogger.i("*** started hud sticky-service ***");
            android.content.IntentFilter filter = new android.content.IntentFilter();
            filter.addAction(com.navdy.hud.app.device.gps.GpsConstants.GPS_EVENT_SWITCH);
            filter.addAction(com.navdy.hud.app.device.gps.GpsConstants.GPS_EVENT_WARM_RESET_UBLOX);
            filter.addAction(com.navdy.hud.app.device.gps.GpsConstants.GPS_SATELLITE_STATUS);
            filter.addAction(com.navdy.hud.app.device.gps.GpsConstants.GPS_COLLECT_LOGS);
            filter.addAction(com.navdy.hud.app.device.gps.GpsConstants.GPS_EVENT_DRIVING_STARTED);
            filter.addAction(com.navdy.hud.app.device.gps.GpsConstants.GPS_EVENT_DRIVING_STOPPED);
            filter.addAction(com.navdy.hud.app.device.gps.GpsConstants.GPS_EVENT_ENABLE_ESF_RAW);
            registerReceiver(new com.navdy.hud.app.maps.GpsEventsReceiver(), filter);
            sLogger.v("registered GpsEventsReceiver");
            android.content.IntentFilter analyticsFilter = new android.content.IntentFilter();
            analyticsFilter.addAction(com.navdy.hud.app.analytics.AnalyticsSupport.ANALYTICS_INTENT);
            registerReceiver(new com.navdy.hud.app.analytics.AnalyticsSupport.AnalyticsIntentsReceiver(), analyticsFilter);
            sLogger.v("registered AnalyticsIntentsReceiver");
            android.content.Intent myIntent = new android.content.Intent(this, com.navdy.hud.app.ui.activity.MainActivity.class);
            myIntent.addFlags(268435456);
            startActivity(myIntent);
        } else {
            sLogger.v("startup:" + processName + " :no-op");
        }
        sLogger.v("startup:" + processName + " :initialization done");
    }

    public mortar.MortarScope getRootScope() {
        return this.rootScope;
    }

    public java.lang.Object getSystemService(java.lang.String name) {
        if (mortar.Mortar.isScopeSystemService(name)) {
            return this.rootScope;
        }
        return super.getSystemService(name);
    }

    public static android.content.Context getAppContext() {
        return sAppContext;
    }

    public static com.navdy.hud.app.HudApplication getApplication() {
        return sApplication;
    }

    private static void initLogger(boolean initializingApp) {
        com.navdy.service.library.log.Logger.init(new com.navdy.service.library.log.LogAppender[]{new com.navdy.service.library.log.LogcatAppender()});
    }

    private void initTaskManager(java.lang.String processName) {
        sLogger.v(processName + ":initializing taskMgr");
        com.navdy.service.library.task.TaskManager taskManager = com.navdy.service.library.task.TaskManager.getInstance();
        try {
            taskManager.addTaskQueue(1, 3);
            taskManager.addTaskQueue(5, 1);
            taskManager.addTaskQueue(8, 1);
            taskManager.addTaskQueue(6, 1);
            taskManager.addTaskQueue(7, 1);
            taskManager.addTaskQueue(9, 1);
            taskManager.addTaskQueue(10, 1);
            taskManager.addTaskQueue(11, 1);
            taskManager.addTaskQueue(12, 1);
            taskManager.addTaskQueue(13, 1);
            taskManager.addTaskQueue(14, 1);
            taskManager.addTaskQueue(22, 1);
            taskManager.addTaskQueue(15, 1);
            taskManager.addTaskQueue(16, 1);
            taskManager.addTaskQueue(17, 1);
            taskManager.addTaskQueue(18, 1);
            taskManager.addTaskQueue(2, 5);
            taskManager.addTaskQueue(3, 1);
            taskManager.addTaskQueue(4, 1);
            taskManager.addTaskQueue(19, 1);
            taskManager.addTaskQueue(20, 1);
            taskManager.addTaskQueue(21, 1);
            taskManager.addTaskQueue(23, 1);
            taskManager.init();
        } catch (java.lang.IllegalStateException e) {
            if (!e.getMessage().equals("already initialized")) {
                throw e;
            }
        }
    }

    private void initConnectionService(java.lang.String processName) {
        sLogger.v(processName + ":initializing taskMgr");
        com.navdy.service.library.task.TaskManager taskManager = com.navdy.service.library.task.TaskManager.getInstance();
        taskManager.addTaskQueue(1, 3);
        taskManager.addTaskQueue(5, 1);
        taskManager.addTaskQueue(9, 1);
        taskManager.init();
        sLogger.v(processName + ":initializing gps manager");
        com.navdy.hud.app.device.gps.GpsManager.getInstance();
        registerReceiver(new com.navdy.hud.app.receiver.LogLevelReceiver(), new android.content.IntentFilter(com.navdy.service.library.log.Logger.ACTION_RELOAD));
    }

    public void initHudApp() {
        if (!this.initialized) {
            this.initialized = true;
            java.lang.String processName = com.navdy.service.library.util.SystemUtils.getProcessName(sAppContext, android.os.Process.myPid());
            com.navdy.hud.app.util.NavdyNativeLibWrapper.loadlibrary();
            if (com.navdy.hud.app.util.CrashReporter.isEnabled()) {
                sLogger.v(processName + ":initializing crash reporter");
                com.navdy.hud.app.util.CrashReporter.getInstance().installCrashHandler(sAppContext);
            } else {
                sLogger.v(processName + ":crash reporter not installed");
            }
            if (com.navdy.hud.app.util.DeviceUtil.isNavdyDevice()) {
                com.navdy.hud.app.util.os.PropsFileUpdater.run();
                com.navdy.hud.app.device.ProjectorBrightness.init();
            }
            sLogger.v(processName + ":updating logging setup");
            initLogger(true);
            android.content.IntentFilter otaFilter = new android.content.IntentFilter();
            otaFilter.addAction(com.navdy.hud.app.service.FileTransferHandler.ACTION_OTA_DOWNLOAD);
            android.content.SharedPreferences prefs = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getSharedPreferences();
            if (prefs == null) {
                sLogger.e("unable to get SharedPreferences");
            } else {
                registerReceiver(new com.navdy.hud.app.util.OTAUpdateService.OTADownloadIntentsReceiver(prefs), otaFilter);
            }
            com.navdy.hud.app.util.picasso.PicassoUtil.initPicasso(sAppContext);
            com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getSharedPreferences().edit();
            try {
                sLogger.v("dbase:opening...");
                long l1 = android.os.SystemClock.elapsedRealtime();
                com.navdy.hud.app.storage.db.HudDatabase.getInstance().getWritableDatabase();
                sLogger.v("dbase: opened[" + (android.os.SystemClock.elapsedRealtime() - l1) + "]");
            } catch (Throwable th) {
                sLogger.e("dbase: error opening", t);
            }
            com.navdy.hud.app.analytics.AnalyticsSupport.analyticsApplicationInit(this);
            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.HudApplication.Anon1((com.navdy.hud.app.debug.DriveRecorder) this.rootScope.getObjectGraph().get(com.navdy.hud.app.debug.DriveRecorder.class)), 1);
            sLogger.v(processName + ":initializing here maps engine");
            com.navdy.hud.app.maps.here.HereMapsManager.getInstance();
            sLogger.v(processName + ":called initialized");
            java.lang.String str = com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.speedMph;
            com.here.android.mpa.common.ViewRect viewRect = com.navdy.hud.app.ui.component.homescreen.HomeScreenConstants.routeOverviewRect;
            int i = com.navdy.hud.app.ui.component.homescreen.SmartDashViewResourceValues.middleGaugeShrinkLeftX;
            java.lang.String s = com.navdy.hud.app.framework.voice.TTSUtils.TTS_DIAL_BATTERY_EXTREMELY_LOW;
            int n = com.navdy.hud.app.framework.glance.GlanceConstants.colorFacebook;
            com.navdy.hud.app.framework.glance.GlanceHelper.initMessageAttributes();
            if (com.navdy.hud.app.util.CrashReporter.isEnabled()) {
                this.handler.post(new com.navdy.hud.app.HudApplication.Anon2());
                this.handler.postDelayed(new com.navdy.hud.app.HudApplication.Anon3(), 45000);
            }
            com.navdy.hud.app.device.dial.DialManager.getInstance();
            com.navdy.hud.app.framework.recentcall.RecentCallManager.getInstance();
            com.navdy.hud.app.bluetooth.pbap.PBAPClientManager.getInstance();
            com.navdy.hud.app.framework.contacts.ContactImageHelper.getInstance();
            com.navdy.hud.app.framework.contacts.FavoriteContactsManager.getInstance();
            com.navdy.hud.app.framework.destinations.DestinationsManager.getInstance();
            com.navdy.hud.app.framework.message.MessageManager.getInstance();
            com.navdy.hud.app.framework.glance.GlanceHandler.getInstance();
            com.navdy.hud.app.util.ReportIssueService.initialize();
            com.navdy.hud.app.service.ShutdownMonitor.getInstance();
            if (!com.navdy.hud.app.util.DeviceUtil.isNavdyDevice()) {
                com.navdy.hud.app.audio.SoundUtils.init();
            }
            com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getFeatureUtil();
            if (com.navdy.hud.app.util.DeviceUtil.isNavdyDevice()) {
                sLogger.v("start dead reckoning mgr");
                com.navdy.hud.app.device.gps.GpsDeadReckoningManager.getInstance();
            }
            this.updateReminderManager = new com.navdy.hud.app.manager.UpdateReminderManager(this);
            sLogger.i("init network b/w controller");
            com.navdy.hud.app.framework.network.NetworkBandwidthController.getInstance();
            com.navdy.hud.app.util.os.CpuProfiler.getInstance().start();
            this.handler.post(new com.navdy.hud.app.HudApplication.Anon4());
            sLogger.v(processName + ":background init done");
        }
    }

    public static boolean isDeveloperBuild() {
        return com.navdy.hud.app.BuildConfig.DEBUG && com.navdy.hud.app.BuildConfig.VERSION_NAME.endsWith("-dev");
    }

    public com.squareup.otto.Bus getBus() {
        return this.bus;
    }

    public void setShutdownReason(com.navdy.hud.app.event.Shutdown.Reason reason) {
        this.lastSeenReason = reason;
    }

    public void shutdown() {
        sLogger.i("shutting down HUD");
        com.navdy.hud.app.util.CrashReporter.getInstance().stopCrashReporting(true);
        android.content.Intent intent = new android.content.Intent();
        intent.setClass(this, com.navdy.hud.app.service.StickyService.class);
        stopService(intent);
        if (this.bus != null) {
            sLogger.i("shutting down HUD - reason: " + this.lastSeenReason);
            this.bus.post(new com.navdy.hud.app.event.Shutdown(this.lastSeenReason, com.navdy.hud.app.event.Shutdown.State.SHUTTING_DOWN));
        }
        com.navdy.hud.app.maps.here.HereMapsManager.getInstance().getLocationFixManager().shutdown();
    }
}
