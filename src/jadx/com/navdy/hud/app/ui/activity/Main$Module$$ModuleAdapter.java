package com.navdy.hud.app.ui.activity;

public final class Main$Module$$ModuleAdapter extends dagger.internal.ModuleAdapter<com.navdy.hud.app.ui.activity.Main.Module> {
    private static final java.lang.Class<?>[] INCLUDES = new java.lang.Class[0];
    private static final java.lang.String[] INJECTS = {"members/com.navdy.hud.app.ui.activity.MainActivity", "members/com.navdy.hud.app.view.MainView", "members/com.navdy.hud.app.view.NotificationView", "members/com.navdy.hud.app.presenter.NotificationPresenter", "members/com.navdy.hud.app.ui.framework.UIStateManager", "members/com.navdy.hud.app.view.ContainerView", "members/com.navdy.hud.app.debug.GestureEngine"};
    private static final java.lang.Class<?>[] STATIC_INJECTIONS = new java.lang.Class[0];

    public Main$Module$$ModuleAdapter() {
        super(com.navdy.hud.app.ui.activity.Main.Module.class, INJECTS, STATIC_INJECTIONS, false, INCLUDES, true, false);
    }
}
