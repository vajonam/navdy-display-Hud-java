package com.navdy.hud.app.ui.activity;

public class MainActivity extends com.navdy.hud.app.ui.activity.HudBaseActivity {
    private static final java.lang.String ACTION_PAIRING_CANCEL = "android.bluetooth.device.action.PAIRING_CANCEL";
    private android.content.BroadcastReceiver bluetoothReceiver = new com.navdy.hud.app.ui.activity.MainActivity.Anon2();
    @butterknife.InjectView(2131624246)
    com.navdy.hud.app.view.ContainerView container;
    android.widget.TextView debugText;
    @javax.inject.Inject
    com.navdy.hud.app.ui.activity.Main.Presenter mainPresenter;
    @javax.inject.Inject
    com.navdy.hud.app.manager.PairingManager pairingManager;
    @javax.inject.Inject
    com.navdy.hud.app.device.PowerManager powerManager;

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            ((com.navdy.hud.app.HudApplication) com.navdy.hud.app.ui.activity.MainActivity.this.getApplication()).initHudApp();
        }
    }

    class Anon2 extends android.content.BroadcastReceiver {
        Anon2() {
        }

        public void onReceive(android.content.Context context, android.content.Intent intent) {
            java.lang.String action = intent.getAction();
            android.bluetooth.BluetoothDevice device = (android.bluetooth.BluetoothDevice) intent.getParcelableExtra("android.bluetooth.device.extra.DEVICE");
            com.squareup.otto.Bus bus = com.navdy.hud.app.ui.activity.MainActivity.this.mainPresenter.bus;
            if (action.equals("android.bluetooth.device.action.BOND_STATE_CHANGED")) {
                int bondState = intent.getIntExtra("android.bluetooth.device.extra.BOND_STATE", Integer.MIN_VALUE);
                int oldState = intent.getIntExtra("android.bluetooth.device.extra.PREVIOUS_BOND_STATE", Integer.MIN_VALUE);
                com.navdy.hud.app.ui.activity.MainActivity.this.logger.d("Bond state change - device:" + device + " state:" + bondState + " prevState:" + oldState);
                bus.post(new com.navdy.hud.app.ui.component.bluetooth.BluetoothPairing.BondStateChange(device, oldState, bondState));
            } else if (action.equals("android.bluetooth.adapter.action.STATE_CHANGED")) {
                int btState = intent.getIntExtra("android.bluetooth.adapter.extra.STATE", Integer.MIN_VALUE);
                if (btState == 12 || btState == 10) {
                    bus.post(new com.navdy.hud.app.event.InitEvents.BluetoothStateChanged(btState == 12));
                }
            } else if (action.equals("android.bluetooth.device.action.PAIRING_REQUEST")) {
                abortBroadcast();
                if (com.navdy.hud.app.device.dial.DialManager.getInstance().isDialDevice(device)) {
                    com.navdy.hud.app.ui.activity.MainActivity.this.logger.w("Pairing request from dial device, ignore [" + device.getName() + "]");
                    return;
                }
                int type = intent.getIntExtra("android.bluetooth.device.extra.PAIRING_VARIANT", Integer.MIN_VALUE);
                int pairingKey = intent.getIntExtra("android.bluetooth.device.extra.PAIRING_KEY", Integer.MIN_VALUE);
                com.navdy.hud.app.ui.activity.MainActivity.this.logger.d("Showing pairing request from " + device + " of type:" + type + " with pin:" + pairingKey);
                android.os.Bundle bundle = new android.os.Bundle();
                bundle.putParcelable(com.navdy.hud.app.ui.component.bluetooth.BluetoothPairing.ARG_DEVICE, device);
                bundle.putInt(com.navdy.hud.app.ui.component.bluetooth.BluetoothPairing.ARG_VARIANT, type);
                bundle.putInt(com.navdy.hud.app.ui.component.bluetooth.BluetoothPairing.ARG_PIN, pairingKey);
                boolean autoAccept = com.navdy.hud.app.ui.activity.MainActivity.this.pairingManager.isAutoPairing();
                android.os.Bundle extra = getResultExtras(false);
                if (!autoAccept && extra != null) {
                    autoAccept = extra.getBoolean("auto", false);
                }
                bundle.putBoolean("auto", autoAccept);
                com.navdy.hud.app.ui.component.bluetooth.BluetoothPairing.showBluetoothPairingToast(bundle);
            } else if (action.equals(com.navdy.hud.app.ui.activity.MainActivity.ACTION_PAIRING_CANCEL)) {
                abortBroadcast();
                bus.post(new com.navdy.hud.app.ui.component.bluetooth.BluetoothPairing.PairingCancelled(device));
            }
        }
    }

    /* access modifiers changed from: protected */
    public mortar.Blueprint getBlueprint() {
        return new com.navdy.hud.app.ui.activity.Main();
    }

    /* access modifiers changed from: protected */
    public void onCreate(android.os.Bundle savedInstanceState) {
        boolean z;
        super.onCreate(savedInstanceState);
        if (com.navdy.hud.app.util.DeviceUtil.isNavdyDevice()) {
            setContentView(com.navdy.hud.app.R.layout.main_view_lyt);
        } else {
            setContentView(com.navdy.hud.app.R.layout.activity_fullscreen);
            this.debugText = (android.widget.TextView) findViewById(com.navdy.hud.app.R.id.debug_text);
            android.content.res.Resources resources = getResources();
            int hudWidth = (int) resources.getDimension(com.navdy.hud.app.R.dimen.dashboard_width);
            this.debugText.setText("Device:  " + android.os.Build.DEVICE + com.navdy.hud.app.framework.glance.GlanceConstants.COLON_SEPARATOR + android.os.Build.MODEL + " width = " + hudWidth + " height = " + ((int) resources.getDimension(com.navdy.hud.app.R.dimen.dashboard_height)) + " scale factor = " + (hudWidth / 640));
        }
        butterknife.ButterKnife.inject((android.app.Activity) this);
        android.preference.PreferenceManager.setDefaultValues(this, "HUD", 0, com.navdy.hud.app.R.xml.developer_preferences, false);
        if (!this.powerManager.inQuietMode()) {
            z = true;
        } else {
            z = false;
        }
        makeImmersive(z);
        android.content.IntentFilter intentFilter = new android.content.IntentFilter();
        intentFilter.addAction("android.bluetooth.device.action.PAIRING_REQUEST");
        intentFilter.addAction("android.bluetooth.device.action.BOND_STATE_CHANGED");
        intentFilter.addAction("android.bluetooth.adapter.action.STATE_CHANGED");
        intentFilter.addAction(ACTION_PAIRING_CANCEL);
        registerReceiver(this.bluetoothReceiver, intentFilter);
        this.mainPresenter.bus.register(this);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(this.bluetoothReceiver);
    }

    @com.squareup.otto.Subscribe
    public void onInitEvent(com.navdy.hud.app.event.InitEvents.InitPhase event) {
        if (event.phase == com.navdy.hud.app.event.InitEvents.Phase.POST_START) {
            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.ui.activity.MainActivity.Anon1(), 1);
        }
    }

    @com.squareup.otto.Subscribe
    public void onWakeup(com.navdy.hud.app.event.Wakeup event) {
        this.logger.v("enableNormalMode(): FLAG_KEEP_SCREEN_ON set");
        getWindow().addFlags(128);
    }

    public void onConfigurationChanged(android.content.res.Configuration newConfig) {
        this.logger.v("onConfigurationChanged:" + newConfig);
        super.onConfigurationChanged(newConfig);
    }

    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            makeImmersive(!this.powerManager.inQuietMode());
        }
    }
}
