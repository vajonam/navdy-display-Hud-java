package com.navdy.hud.app.ui.activity;

public class MainActivity$$ViewInjector {
    public static void inject(butterknife.ButterKnife.Finder finder, com.navdy.hud.app.ui.activity.MainActivity target, java.lang.Object source) {
        target.container = (com.navdy.hud.app.view.ContainerView) finder.findRequiredView(source, com.navdy.hud.app.R.id.container, "field 'container'");
    }

    public static void reset(com.navdy.hud.app.ui.activity.MainActivity target) {
        target.container = null;
    }
}
