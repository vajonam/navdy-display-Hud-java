package com.navdy.hud.app.ui.activity;

public final class HudBaseActivity$$InjectAdapter extends dagger.internal.Binding<com.navdy.hud.app.ui.activity.HudBaseActivity> implements dagger.MembersInjector<com.navdy.hud.app.ui.activity.HudBaseActivity> {
    private dagger.internal.Binding<com.navdy.hud.app.manager.InputManager> inputManager;
    private dagger.internal.Binding<com.navdy.hud.app.common.BaseActivity> supertype;

    public HudBaseActivity$$InjectAdapter() {
        super(null, "members/com.navdy.hud.app.ui.activity.HudBaseActivity", false, com.navdy.hud.app.ui.activity.HudBaseActivity.class);
    }

    public void attach(dagger.internal.Linker linker) {
        this.inputManager = linker.requestBinding("com.navdy.hud.app.manager.InputManager", com.navdy.hud.app.ui.activity.HudBaseActivity.class, getClass().getClassLoader());
        dagger.internal.Linker linker2 = linker;
        this.supertype = linker2.requestBinding("members/com.navdy.hud.app.common.BaseActivity", com.navdy.hud.app.ui.activity.HudBaseActivity.class, getClass().getClassLoader(), false, true);
    }

    public void getDependencies(java.util.Set<dagger.internal.Binding<?>> set, java.util.Set<dagger.internal.Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.inputManager);
        injectMembersBindings.add(this.supertype);
    }

    public void injectMembers(com.navdy.hud.app.ui.activity.HudBaseActivity object) {
        object.inputManager = (com.navdy.hud.app.manager.InputManager) this.inputManager.get();
        this.supertype.injectMembers(object);
    }
}
