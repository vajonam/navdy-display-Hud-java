package com.navdy.hud.app.ui.activity;

public final class Main$Presenter$DeferredServices$$InjectAdapter extends dagger.internal.Binding<com.navdy.hud.app.ui.activity.Main.Presenter.DeferredServices> implements dagger.MembersInjector<com.navdy.hud.app.ui.activity.Main.Presenter.DeferredServices> {
    private dagger.internal.Binding<com.navdy.hud.app.ancs.AncsServiceConnector> ancsService;
    private dagger.internal.Binding<com.navdy.hud.app.gesture.GestureServiceConnector> gestureService;

    public Main$Presenter$DeferredServices$$InjectAdapter() {
        super(null, "members/com.navdy.hud.app.ui.activity.Main$Presenter$DeferredServices", false, com.navdy.hud.app.ui.activity.Main.Presenter.DeferredServices.class);
    }

    public void attach(dagger.internal.Linker linker) {
        this.gestureService = linker.requestBinding("com.navdy.hud.app.gesture.GestureServiceConnector", com.navdy.hud.app.ui.activity.Main.Presenter.DeferredServices.class, getClass().getClassLoader());
        this.ancsService = linker.requestBinding("com.navdy.hud.app.ancs.AncsServiceConnector", com.navdy.hud.app.ui.activity.Main.Presenter.DeferredServices.class, getClass().getClassLoader());
    }

    public void getDependencies(java.util.Set<dagger.internal.Binding<?>> set, java.util.Set<dagger.internal.Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.gestureService);
        injectMembersBindings.add(this.ancsService);
    }

    public void injectMembers(com.navdy.hud.app.ui.activity.Main.Presenter.DeferredServices object) {
        object.gestureService = (com.navdy.hud.app.gesture.GestureServiceConnector) this.gestureService.get();
        object.ancsService = (com.navdy.hud.app.ancs.AncsServiceConnector) this.ancsService.get();
    }
}
