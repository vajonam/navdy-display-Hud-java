package com.navdy.hud.app.ui.activity;

public final class Main$Presenter$$InjectAdapter extends dagger.internal.Binding<com.navdy.hud.app.ui.activity.Main.Presenter> implements javax.inject.Provider<com.navdy.hud.app.ui.activity.Main.Presenter>, dagger.MembersInjector<com.navdy.hud.app.ui.activity.Main.Presenter> {
    private dagger.internal.Binding<com.squareup.otto.Bus> bus;
    private dagger.internal.Binding<com.navdy.hud.app.framework.phonecall.CallManager> callManager;
    private dagger.internal.Binding<com.navdy.hud.app.service.ConnectionHandler> connectionHandler;
    private dagger.internal.Binding<com.navdy.hud.app.device.dial.DialSimulatorMessagesHandler> dialSimulatorMessagesHandler;
    private dagger.internal.Binding<com.navdy.hud.app.profile.DriverProfileManager> driverProfileManager;
    private dagger.internal.Binding<android.content.SharedPreferences> globalPreferences;
    private dagger.internal.Binding<com.navdy.hud.app.manager.InputManager> inputManager;
    private dagger.internal.Binding<com.navdy.hud.app.manager.MusicManager> musicManager;
    private dagger.internal.Binding<com.navdy.hud.app.service.pandora.PandoraManager> pandoraManager;
    private dagger.internal.Binding<com.navdy.hud.app.device.PowerManager> powerManager;
    private dagger.internal.Binding<android.content.SharedPreferences> preferences;
    private dagger.internal.Binding<com.navdy.hud.app.config.SettingsManager> settingsManager;
    private dagger.internal.Binding<com.navdy.hud.app.ui.framework.BasePresenter> supertype;
    private dagger.internal.Binding<com.navdy.hud.app.framework.trips.TripManager> tripManager;
    private dagger.internal.Binding<com.navdy.hud.app.ui.framework.UIStateManager> uiStateManager;

    public Main$Presenter$$InjectAdapter() {
        super("com.navdy.hud.app.ui.activity.Main$Presenter", "members/com.navdy.hud.app.ui.activity.Main$Presenter", true, com.navdy.hud.app.ui.activity.Main.Presenter.class);
    }

    public void attach(dagger.internal.Linker linker) {
        this.bus = linker.requestBinding("com.squareup.otto.Bus", com.navdy.hud.app.ui.activity.Main.Presenter.class, getClass().getClassLoader());
        this.inputManager = linker.requestBinding("com.navdy.hud.app.manager.InputManager", com.navdy.hud.app.ui.activity.Main.Presenter.class, getClass().getClassLoader());
        this.connectionHandler = linker.requestBinding("com.navdy.hud.app.service.ConnectionHandler", com.navdy.hud.app.ui.activity.Main.Presenter.class, getClass().getClassLoader());
        this.uiStateManager = linker.requestBinding("com.navdy.hud.app.ui.framework.UIStateManager", com.navdy.hud.app.ui.activity.Main.Presenter.class, getClass().getClassLoader());
        this.callManager = linker.requestBinding("com.navdy.hud.app.framework.phonecall.CallManager", com.navdy.hud.app.ui.activity.Main.Presenter.class, getClass().getClassLoader());
        this.musicManager = linker.requestBinding("com.navdy.hud.app.manager.MusicManager", com.navdy.hud.app.ui.activity.Main.Presenter.class, getClass().getClassLoader());
        this.pandoraManager = linker.requestBinding("com.navdy.hud.app.service.pandora.PandoraManager", com.navdy.hud.app.ui.activity.Main.Presenter.class, getClass().getClassLoader());
        this.dialSimulatorMessagesHandler = linker.requestBinding("com.navdy.hud.app.device.dial.DialSimulatorMessagesHandler", com.navdy.hud.app.ui.activity.Main.Presenter.class, getClass().getClassLoader());
        this.globalPreferences = linker.requestBinding("android.content.SharedPreferences", com.navdy.hud.app.ui.activity.Main.Presenter.class, getClass().getClassLoader());
        this.settingsManager = linker.requestBinding("com.navdy.hud.app.config.SettingsManager", com.navdy.hud.app.ui.activity.Main.Presenter.class, getClass().getClassLoader());
        this.powerManager = linker.requestBinding("com.navdy.hud.app.device.PowerManager", com.navdy.hud.app.ui.activity.Main.Presenter.class, getClass().getClassLoader());
        this.driverProfileManager = linker.requestBinding("com.navdy.hud.app.profile.DriverProfileManager", com.navdy.hud.app.ui.activity.Main.Presenter.class, getClass().getClassLoader());
        this.tripManager = linker.requestBinding("com.navdy.hud.app.framework.trips.TripManager", com.navdy.hud.app.ui.activity.Main.Presenter.class, getClass().getClassLoader());
        this.preferences = linker.requestBinding("android.content.SharedPreferences", com.navdy.hud.app.ui.activity.Main.Presenter.class, getClass().getClassLoader());
        dagger.internal.Linker linker2 = linker;
        this.supertype = linker2.requestBinding("members/com.navdy.hud.app.ui.framework.BasePresenter", com.navdy.hud.app.ui.activity.Main.Presenter.class, getClass().getClassLoader(), false, true);
    }

    public void getDependencies(java.util.Set<dagger.internal.Binding<?>> set, java.util.Set<dagger.internal.Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.bus);
        injectMembersBindings.add(this.inputManager);
        injectMembersBindings.add(this.connectionHandler);
        injectMembersBindings.add(this.uiStateManager);
        injectMembersBindings.add(this.callManager);
        injectMembersBindings.add(this.musicManager);
        injectMembersBindings.add(this.pandoraManager);
        injectMembersBindings.add(this.dialSimulatorMessagesHandler);
        injectMembersBindings.add(this.globalPreferences);
        injectMembersBindings.add(this.settingsManager);
        injectMembersBindings.add(this.powerManager);
        injectMembersBindings.add(this.driverProfileManager);
        injectMembersBindings.add(this.tripManager);
        injectMembersBindings.add(this.preferences);
        injectMembersBindings.add(this.supertype);
    }

    public com.navdy.hud.app.ui.activity.Main.Presenter get() {
        com.navdy.hud.app.ui.activity.Main.Presenter result = new com.navdy.hud.app.ui.activity.Main.Presenter();
        injectMembers(result);
        return result;
    }

    public void injectMembers(com.navdy.hud.app.ui.activity.Main.Presenter object) {
        object.bus = (com.squareup.otto.Bus) this.bus.get();
        object.inputManager = (com.navdy.hud.app.manager.InputManager) this.inputManager.get();
        object.connectionHandler = (com.navdy.hud.app.service.ConnectionHandler) this.connectionHandler.get();
        object.uiStateManager = (com.navdy.hud.app.ui.framework.UIStateManager) this.uiStateManager.get();
        object.callManager = (com.navdy.hud.app.framework.phonecall.CallManager) this.callManager.get();
        object.musicManager = (com.navdy.hud.app.manager.MusicManager) this.musicManager.get();
        object.pandoraManager = (com.navdy.hud.app.service.pandora.PandoraManager) this.pandoraManager.get();
        object.dialSimulatorMessagesHandler = (com.navdy.hud.app.device.dial.DialSimulatorMessagesHandler) this.dialSimulatorMessagesHandler.get();
        object.globalPreferences = (android.content.SharedPreferences) this.globalPreferences.get();
        object.settingsManager = (com.navdy.hud.app.config.SettingsManager) this.settingsManager.get();
        object.powerManager = (com.navdy.hud.app.device.PowerManager) this.powerManager.get();
        object.driverProfileManager = (com.navdy.hud.app.profile.DriverProfileManager) this.driverProfileManager.get();
        object.tripManager = (com.navdy.hud.app.framework.trips.TripManager) this.tripManager.get();
        object.preferences = (android.content.SharedPreferences) this.preferences.get();
        this.supertype.injectMembers(object);
    }
}
