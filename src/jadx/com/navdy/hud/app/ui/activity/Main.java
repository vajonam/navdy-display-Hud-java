package com.navdy.hud.app.ui.activity;

public class Main extends com.navdy.hud.app.screen.BaseScreen {
    public static final java.lang.String DEMO_VIDEO_PROPERTY = "persist.sys.demo.video";
    private static final java.lang.String INSTALLING_OTA_TOAST_ID = "#ota#toast";
    public static final java.lang.String MIME_TYPE_VIDEO = "video/mp4";
    static final java.lang.String PREFERENCE_HOME_SCREEN_MODE = "PREFERENCE_LAST_HOME_SCREEN_MODE";
    /* access modifiers changed from: private */
    public static android.util.SparseArray<java.lang.Class> SCREEN_MAP = new android.util.SparseArray<>();
    public static final int SNAPSHOT_TITLE_PICKER_DELAY_MILLIS = 2000;
    public static com.navdy.hud.app.ui.activity.Main.ProtocolStatus mProtocolStatus = com.navdy.hud.app.ui.activity.Main.ProtocolStatus.PROTOCOL_VALID;
    /* access modifiers changed from: private */
    public static com.navdy.hud.app.ui.activity.Main mainScreen;
    /* access modifiers changed from: private */
    public static com.navdy.hud.app.ui.activity.Main.Presenter presenter;
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.activity.Main.class);

    public interface INotificationExtensionView {
        void onStart();

        void onStop();
    }

    @dagger.Module(addsTo = com.navdy.hud.app.common.ProdModule.class, injects = {com.navdy.hud.app.ui.activity.MainActivity.class, com.navdy.hud.app.view.MainView.class, com.navdy.hud.app.view.NotificationView.class, com.navdy.hud.app.presenter.NotificationPresenter.class, com.navdy.hud.app.ui.framework.UIStateManager.class, com.navdy.hud.app.view.ContainerView.class, com.navdy.hud.app.debug.GestureEngine.class})
    public class Module {
        public Module() {
        }
    }

    @javax.inject.Singleton
    public static class Presenter extends com.navdy.hud.app.ui.framework.BasePresenter<com.navdy.hud.app.view.MainView> implements flow.Flow.Listener, com.navdy.hud.app.manager.InputManager.IInputHandler {
        public static final java.lang.String MAIN_SETTINGS = "main.settings";
        private boolean animationRunning;
        @javax.inject.Inject
        com.squareup.otto.Bus bus;
        @javax.inject.Inject
        com.navdy.hud.app.framework.phonecall.CallManager callManager;
        @javax.inject.Inject
        com.navdy.hud.app.service.ConnectionHandler connectionHandler;
        private boolean createdScreens;
        private com.navdy.service.library.events.ui.Screen currentScreen;
        private int defaultNotifColor;
        @javax.inject.Inject
        com.navdy.hud.app.device.dial.DialSimulatorMessagesHandler dialSimulatorMessagesHandler;
        @javax.inject.Inject
        com.navdy.hud.app.profile.DriverProfileManager driverProfileManager;

        /* renamed from: flow reason: collision with root package name */
        flow.Flow f6flow;
        @javax.inject.Inject
        android.content.SharedPreferences globalPreferences;
        private android.os.Handler handler = new android.os.Handler();
        private com.navdy.hud.app.settings.HUDSettings hudSettings;
        private com.navdy.hud.app.manager.InitManager initManager;
        private com.navdy.hud.app.gesture.MultipleClickGestureDetector innerEventsDetector = new com.navdy.hud.app.gesture.MultipleClickGestureDetector(4, new com.navdy.hud.app.ui.activity.Main.Presenter.Anon1());
        @javax.inject.Inject
        com.navdy.hud.app.manager.InputManager inputManager;
        @javax.inject.Inject
        com.navdy.hud.app.manager.MusicManager musicManager;
        /* access modifiers changed from: private */
        public boolean notifAnimationRunning;
        private com.navdy.hud.app.ui.framework.INotificationAnimationListener notificationAnimationListener = new com.navdy.hud.app.ui.activity.Main.Presenter.Anon4();
        private boolean notificationColorEnabled = true;
        private com.navdy.hud.app.framework.notifications.NotificationManager notificationManager;
        private java.util.Queue<com.navdy.hud.app.ui.activity.Main.ScreenInfo> operationQueue = new java.util.LinkedList();
        @javax.inject.Inject
        com.navdy.hud.app.service.pandora.PandoraManager pandoraManager;
        @javax.inject.Inject
        com.navdy.hud.app.device.PowerManager powerManager;
        @javax.inject.Inject
        android.content.SharedPreferences preferences;
        private com.navdy.hud.app.ui.framework.IScreenAnimationListener screenAnimationListener = new com.navdy.hud.app.ui.activity.Main.Presenter.Anon3();
        /* access modifiers changed from: private */
        public com.navdy.hud.app.ui.activity.Main.Presenter.DeferredServices services;
        private com.navdy.hud.app.settings.MainScreenSettings settings;
        @javax.inject.Inject
        com.navdy.hud.app.config.SettingsManager settingsManager;
        private boolean showingPortrait;
        private boolean systemTrayEnabled = true;
        @javax.inject.Inject
        com.navdy.hud.app.framework.trips.TripManager tripManager;
        @javax.inject.Inject
        com.navdy.hud.app.ui.framework.UIStateManager uiStateManager;
        private com.navdy.hud.app.framework.voice.VoiceAssistNotification voiceAssistNotification;
        private com.navdy.hud.app.framework.voice.VoiceSearchNotification voiceSearchNotification;

        class Anon1 implements com.navdy.hud.app.gesture.MultipleClickGestureDetector.IMultipleClickKeyGesture {
            Anon1() {
            }

            public void onMultipleClick(int count) {
                com.navdy.hud.app.ui.activity.Main.Presenter.this.executeMultipleKeyEvent(count);
            }

            public boolean onGesture(com.navdy.service.library.events.input.GestureEvent event) {
                return false;
            }

            public boolean onKey(com.navdy.hud.app.manager.InputManager.CustomKeyEvent event) {
                return com.navdy.hud.app.ui.activity.Main.Presenter.this.executeKeyEvent(event);
            }

            public com.navdy.hud.app.manager.InputManager.IInputHandler nextHandler() {
                return null;
            }
        }

        class Anon2 implements java.lang.Runnable {
            Anon2() {
            }

            public void run() {
                com.navdy.hud.app.util.ReportIssueService.showSnapshotMenu();
            }
        }

        class Anon3 implements com.navdy.hud.app.ui.framework.IScreenAnimationListener {
            Anon3() {
            }

            public void onStart(com.navdy.hud.app.screen.BaseScreen in, com.navdy.hud.app.screen.BaseScreen out) {
                com.navdy.hud.app.ui.activity.Main.sLogger.v("screen anim start in[" + in + "]  out[" + out + "]");
                com.navdy.hud.app.ui.activity.Main.Presenter.this.clearInputFocus();
                com.navdy.hud.app.ui.framework.UIStateManager uIStateManager = com.navdy.hud.app.ui.activity.Main.Presenter.this.uiStateManager;
                if (!com.navdy.hud.app.ui.framework.UIStateManager.isFullscreenMode(in.getScreen())) {
                    com.navdy.hud.app.ui.activity.Main.Presenter.this.setNotificationColorVisibility(4);
                    com.navdy.hud.app.ui.activity.Main.Presenter.this.setSystemTrayVisibility(4);
                }
            }

            public void onStop(com.navdy.hud.app.screen.BaseScreen in, com.navdy.hud.app.screen.BaseScreen out) {
                com.navdy.hud.app.ui.activity.Main.sLogger.v("screen anim stop in[" + in + "]  out[" + out + "]");
                com.navdy.hud.app.ui.activity.Main.Presenter.this.updateSystemTrayVisibility(com.navdy.hud.app.ui.activity.Main.ScreenCategory.SCREEN, in.getScreen());
            }
        }

        class Anon4 implements com.navdy.hud.app.ui.framework.INotificationAnimationListener {
            Anon4() {
            }

            public void onStart(java.lang.String id, com.navdy.hud.app.framework.notifications.NotificationType type, com.navdy.hud.app.ui.framework.UIStateManager.Mode mode) {
                com.navdy.hud.app.ui.activity.Main.sLogger.v("notification anim start id[" + id + "]  type[" + type + "] mode[" + mode + "]");
                if (mode == com.navdy.hud.app.ui.framework.UIStateManager.Mode.EXPAND) {
                    com.navdy.hud.app.ui.activity.Main.Presenter.this.setNotificationColorVisibility(4);
                    com.navdy.hud.app.ui.activity.Main.Presenter.this.setSystemTrayVisibility(4);
                }
                com.navdy.hud.app.ui.activity.Main.Presenter.this.clearInputFocus();
                com.navdy.hud.app.ui.activity.Main.Presenter.this.notifAnimationRunning = true;
            }

            public void onStop(java.lang.String id, com.navdy.hud.app.framework.notifications.NotificationType type, com.navdy.hud.app.ui.framework.UIStateManager.Mode mode) {
                com.navdy.hud.app.ui.activity.Main.sLogger.v("notification anim stop id[" + id + "]  type[" + type + "] mode[" + mode + "]");
                com.navdy.hud.app.ui.activity.Main.Presenter.this.notifAnimationRunning = false;
                if (mode == com.navdy.hud.app.ui.framework.UIStateManager.Mode.COLLAPSE) {
                    com.navdy.hud.app.ui.activity.Main.Presenter.this.updateSystemTrayVisibility(com.navdy.hud.app.ui.activity.Main.ScreenCategory.NOTIFICATION, null);
                }
                com.navdy.hud.app.ui.activity.Main.Presenter.this.runQueuedOperation();
            }
        }

        class Anon5 implements java.lang.Runnable {
            Anon5() {
            }

            public void run() {
                com.navdy.hud.app.ui.activity.Main.sLogger.v("gesture is enabled, starting");
                com.navdy.hud.app.ui.activity.Main.Presenter.this.services.gestureService.start();
            }
        }

        class Anon6 implements java.lang.Runnable {
            Anon6() {
            }

            public void run() {
                com.navdy.hud.app.ui.activity.Main.Presenter.this.services.gestureService.stop();
            }
        }

        class Anon7 implements java.lang.Runnable {
            final /* synthetic */ com.navdy.hud.app.ui.activity.Main.ScreenInfo val$info;

            Anon7(com.navdy.hud.app.ui.activity.Main.ScreenInfo screenInfo) {
                this.val$info = screenInfo;
            }

            public void run() {
                com.navdy.hud.app.ui.activity.Main.Presenter.this.updateScreen(this.val$info.screen, this.val$info.args, this.val$info.args2, true, this.val$info.dismissScreen);
            }
        }

        class Anon8 implements com.navdy.hud.app.framework.toast.IToastCallback {
            android.animation.ObjectAnimator animator;

            class Anon1 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
                Anon1() {
                }

                public void onAnimationEnd(android.animation.Animator animation) {
                    if (com.navdy.hud.app.ui.activity.Main.Presenter.Anon8.this.animator != null) {
                        com.navdy.hud.app.ui.activity.Main.Presenter.Anon8.this.animator.setStartDelay(33);
                        com.navdy.hud.app.ui.activity.Main.Presenter.Anon8.this.animator.start();
                    }
                }
            }

            Anon8() {
            }

            public void onStart(com.navdy.hud.app.view.ToastView view) {
                com.navdy.hud.app.ui.component.ConfirmationLayout lyt = view.getView();
                lyt.title1.setMaxLines(2);
                lyt.title1.setSingleLine(false);
                lyt.title2.setMaxLines(2);
                lyt.title2.setSingleLine(false);
                if (this.animator == null) {
                    this.animator = android.animation.ObjectAnimator.ofFloat(lyt.sideImage, android.view.View.ROTATION, new float[]{360.0f});
                    this.animator.setDuration(500);
                    this.animator.setInterpolator(new android.view.animation.AccelerateDecelerateInterpolator());
                    this.animator.addListener(new com.navdy.hud.app.ui.activity.Main.Presenter.Anon8.Anon1());
                }
                if (!this.animator.isRunning()) {
                    this.animator.start();
                }
            }

            public void onStop() {
                if (this.animator != null) {
                    this.animator.removeAllListeners();
                    this.animator.cancel();
                }
            }

            public boolean onKey(com.navdy.hud.app.manager.InputManager.CustomKeyEvent event) {
                return false;
            }

            public void executeChoiceItem(int pos, int id) {
            }
        }

        public static class DeferredServices {
            @javax.inject.Inject
            public com.navdy.hud.app.ancs.AncsServiceConnector ancsService;
            @javax.inject.Inject
            public com.navdy.hud.app.gesture.GestureServiceConnector gestureService;

            DeferredServices(android.content.Context context) {
                mortar.Mortar.inject(context, this);
            }
        }

        /* access modifiers changed from: private */
        public void executeMultipleKeyEvent(int count) {
            if (count == 2) {
                com.navdy.hud.app.ui.activity.Main.sLogger.v("Double click");
                takeNotificationAction();
            } else if (count == 3) {
                com.navdy.hud.app.ui.activity.Main.sLogger.v("Triple click");
                com.navdy.hud.app.ui.activity.Main.showBrightnessNotification();
            } else if (count >= 4) {
                com.navdy.hud.app.ui.activity.Main.sLogger.d("4 clicks, dumping a snapshot");
                com.navdy.hud.app.util.ReportIssueService.dumpSnapshotAsync();
                if (!com.navdy.hud.app.util.DeviceUtil.isUserBuild()) {
                    this.handler.postDelayed(new com.navdy.hud.app.ui.activity.Main.Presenter.Anon2(), 2000);
                }
            }
        }

        /* access modifiers changed from: private */
        public boolean executeKeyEvent(com.navdy.hud.app.manager.InputManager.CustomKeyEvent event) {
            switch (event) {
                case SELECT:
                    this.bus.post(new com.navdy.service.library.events.ui.ShowScreen.Builder().screen(com.navdy.service.library.events.ui.Screen.SCREEN_MAIN_MENU).build());
                    return true;
                case LONG_PRESS:
                    com.navdy.hud.app.manager.RemoteDeviceManager remoteDeviceManager = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance();
                    if (!remoteDeviceManager.isRemoteDeviceConnected() || !remoteDeviceManager.isAppConnected()) {
                        this.connectionHandler.sendConnectionNotification();
                    } else if (com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile().getLongPressAction() == com.navdy.service.library.events.preferences.DriverProfilePreferences.DialLongPressAction.DIAL_LONG_PRESS_PLACE_SEARCH) {
                        com.navdy.hud.app.ui.activity.Main.sLogger.v("longpress, Place Search ");
                        launchVoiceSearch();
                    } else {
                        startVoiceAssistance(false);
                    }
                    return true;
                case POWER_BUTTON_LONG_PRESS:
                    com.navdy.hud.app.ui.activity.Main.sLogger.v("power button click: show shutdown");
                    this.bus.post(new com.navdy.hud.app.event.ShowScreenWithArgs(com.navdy.service.library.events.ui.Screen.SCREEN_SHUTDOWN_CONFIRMATION, com.navdy.hud.app.event.Shutdown.Reason.POWER_BUTTON.asBundle(), false));
                    return true;
                case POWER_BUTTON_DOUBLE_CLICK:
                    com.navdy.hud.app.ui.activity.Main.sLogger.v("power button double click: show dial pairing");
                    this.bus.post(new com.navdy.service.library.events.ui.ShowScreen.Builder().screen(com.navdy.service.library.events.ui.Screen.SCREEN_DIAL_PAIRING).build());
                    return true;
                case POWER_BUTTON_CLICK:
                    com.navdy.hud.app.ui.activity.Main.sLogger.v("power button click: no-op");
                    return false;
                default:
                    return false;
            }
        }

        public void onLoad(android.os.Bundle savedInstanceState) {
            com.navdy.hud.app.ui.activity.Main.presenter = this;
            this.notificationManager = com.navdy.hud.app.framework.notifications.NotificationManager.getInstance();
            this.voiceAssistNotification = new com.navdy.hud.app.framework.voice.VoiceAssistNotification();
            this.voiceSearchNotification = new com.navdy.hud.app.framework.voice.VoiceSearchNotification();
            this.defaultNotifColor = 0;
            this.uiStateManager.setRootScreen(com.navdy.hud.app.ui.activity.Main.mainScreen);
            this.uiStateManager.addScreenAnimationListener(this.screenAnimationListener);
            this.uiStateManager.addNotificationAnimationListener(this.notificationAnimationListener);
            super.onLoad(savedInstanceState);
            com.navdy.hud.app.settings.MainScreenSettings savedSettings = null;
            if (savedInstanceState != null) {
                savedSettings = (com.navdy.hud.app.settings.MainScreenSettings) savedInstanceState.getParcelable(MAIN_SETTINGS);
            }
            if (savedSettings == null) {
                java.lang.String json = this.preferences.getString(MAIN_SETTINGS, null);
                try {
                    savedSettings = (com.navdy.hud.app.settings.MainScreenSettings) new com.google.gson.Gson().fromJson(json, com.navdy.hud.app.settings.MainScreenSettings.class);
                } catch (Throwable th) {
                    com.navdy.hud.app.ui.activity.Main.sLogger.e("Failed to parse json " + json);
                }
            }
            if (savedSettings == null) {
                savedSettings = new com.navdy.hud.app.settings.MainScreenSettings();
            }
            this.settings = savedSettings;
            this.settingsManager.addSetting(com.navdy.hud.app.device.gps.GpsUtils.SHOW_RAW_GPS);
            this.hudSettings = new com.navdy.hud.app.settings.HUDSettings(this.preferences);
            updateDisplayFormat();
            this.bus.register(this);
            this.connectionHandler.connect();
            setupFlow();
            updateView();
            this.bus.register(this.dialSimulatorMessagesHandler);
            this.bus.register(this.musicManager);
            this.bus.register(this.pandoraManager);
            this.bus.register(this.tripManager);
            this.initManager = new com.navdy.hud.app.manager.InitManager(this.bus, this.connectionHandler);
            this.initManager.start();
        }

        private void signalBootComplete() {
            com.navdy.hud.app.util.os.SystemProperties.set("dev.app_started", com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE);
            com.navdy.hud.app.util.os.SystemProperties.set("dev.late_service", com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE);
        }

        @com.squareup.otto.Subscribe
        public void onInitEvent(com.navdy.hud.app.event.InitEvents.InitPhase event) {
            boolean hasDemoVideoProperty;
            switch (event.phase) {
                case PRE_USER_INTERACTION:
                    if (this.powerManager.inQuietMode()) {
                        this.powerManager.enterSleepMode();
                    } else {
                        com.navdy.hud.app.device.light.HUDLightUtils.resetFrontLED(com.navdy.hud.app.device.light.LightManager.getInstance());
                    }
                    java.lang.String demoVideo = com.navdy.hud.app.util.os.SystemProperties.get(com.navdy.hud.app.ui.activity.Main.DEMO_VIDEO_PROPERTY);
                    if (!android.text.TextUtils.isEmpty(demoVideo)) {
                        hasDemoVideoProperty = true;
                    } else {
                        hasDemoVideoProperty = false;
                    }
                    if (this.preferences.getBoolean(com.navdy.hud.app.debug.SettingsActivity.START_VIDEO_ON_BOOT_KEY, false) || hasDemoVideoProperty) {
                        java.io.File videoFolder = android.os.Environment.getExternalStoragePublicDirectory(android.os.Environment.DIRECTORY_MOVIES);
                        signalBootComplete();
                        java.io.File videoFile = new java.io.File(videoFolder, hasDemoVideoProperty ? demoVideo : "navdy-demo.mov");
                        com.navdy.hud.app.ui.activity.Main.sLogger.d("Trying to play video: " + videoFile);
                        if (videoFile.exists()) {
                            playMedia(com.navdy.hud.app.HudApplication.getAppContext(), android.net.Uri.fromFile(videoFile));
                            return;
                        } else {
                            com.navdy.hud.app.ui.activity.Main.sLogger.d("Missing demo video file...");
                            return;
                        }
                    } else {
                        return;
                    }
                case POST_START:
                    com.navdy.hud.app.ui.activity.Main.sLogger.v("initializing obd manager");
                    com.navdy.hud.app.obd.ObdManager.getInstance();
                    if (!this.powerManager.inQuietMode()) {
                        startLateServices();
                    }
                    signalBootComplete();
                    return;
                default:
                    return;
            }
        }

        @com.squareup.otto.Subscribe
        public void onWakeUp(com.navdy.hud.app.event.Wakeup event) {
            startLateServices();
        }

        private void startLateServices() {
            com.navdy.hud.app.ui.activity.Main.sLogger.v("starting update service");
            android.content.Context context = com.navdy.hud.app.HudApplication.getAppContext();
            android.content.Intent updateServiceIntent = new android.content.Intent(context, com.navdy.hud.app.util.OTAUpdateService.class);
            updateServiceIntent.putExtra("COMMAND", com.navdy.hud.app.util.OTAUpdateService.COMMAND_VERIFY_UPDATE);
            context.startService(updateServiceIntent);
            com.navdy.hud.app.util.ReportIssueService.scheduleSync();
            com.navdy.hud.app.ui.activity.Main.sLogger.v("initializing additional services");
            this.services = new com.navdy.hud.app.ui.activity.Main.Presenter.DeferredServices(context);
            if (com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getFeatureUtil().isFeatureEnabled(com.navdy.hud.app.util.FeatureUtil.Feature.GESTURE_ENGINE)) {
                com.navdy.hud.app.ui.activity.Main.sLogger.v("gesture is supported");
                com.navdy.service.library.events.preferences.InputPreferences inputPreferences = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile().getInputPreferences();
                if (inputPreferences == null || !inputPreferences.use_gestures.booleanValue()) {
                    com.navdy.hud.app.ui.activity.Main.sLogger.v("gesture is not enabled");
                } else {
                    com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.ui.activity.Main.Presenter.Anon5(), 1);
                }
            }
        }

        @com.squareup.otto.Subscribe
        public void onShutdownEvent(com.navdy.hud.app.event.Shutdown event) {
            switch (event.state) {
                case CONFIRMATION:
                    this.bus.post(new com.navdy.hud.app.event.ShowScreenWithArgs(com.navdy.service.library.events.ui.Screen.SCREEN_SHUTDOWN_CONFIRMATION, event.reason.asBundle(), false));
                    return;
                case SHUTTING_DOWN:
                    android.content.Context context = com.navdy.hud.app.HudApplication.getAppContext();
                    com.navdy.hud.app.ui.activity.Main.sLogger.i("Stopping update service");
                    context.stopService(new android.content.Intent(context, com.navdy.hud.app.util.OTAUpdateService.class));
                    return;
                default:
                    return;
            }
        }

        @com.squareup.otto.Subscribe
        public void onDriverProfileChanged(com.navdy.hud.app.event.DriverProfileChanged event) {
            updateDisplayFormat();
        }

        @com.squareup.otto.Subscribe
        public void onDriverProfileUpdate(com.navdy.hud.app.event.DriverProfileUpdated event) {
            if (event.state == com.navdy.hud.app.event.DriverProfileUpdated.State.UPDATED) {
                updateDisplayFormat();
            }
        }

        private void updateDisplayFormat() {
            setDisplayFormat(this.driverProfileManager.getCurrentProfile().getDisplayFormat());
        }

        private void setDisplayFormat(com.navdy.service.library.events.preferences.DriverProfilePreferences.DisplayFormat format) {
            com.navdy.hud.app.ui.activity.Main.sLogger.i("Switching display format to " + format);
            com.navdy.hud.app.view.MainView mainView = (com.navdy.hud.app.view.MainView) getView();
            if (mainView != null) {
                mainView.setDisplayFormat(format);
            }
        }

        public void playMedia(android.content.Context context, android.net.Uri file) {
            com.navdy.hud.app.ui.activity.Main.sLogger.d("playMedia:" + file);
            try {
                android.content.Intent intent = new android.content.Intent("android.intent.action.VIEW");
                intent.setDataAndType(file, com.navdy.hud.app.ui.activity.Main.MIME_TYPE_VIDEO);
                intent.addFlags(268435456);
                context.startActivity(intent);
            } catch (java.lang.Exception e) {
                com.navdy.hud.app.ui.activity.Main.sLogger.e("Unable to start video loop", e);
            }
        }

        public void onSave(android.os.Bundle outState) {
            outState.putParcelable(MAIN_SETTINGS, this.settings);
            persistSettings();
            if (this.services != null) {
                com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.ui.activity.Main.Presenter.Anon6(), 1);
            }
            super.onSave(outState);
        }

        /* access modifiers changed from: private */
        public void runQueuedOperation() {
            int size = this.operationQueue.size();
            if (size > 0) {
                com.navdy.hud.app.ui.activity.Main.ScreenInfo info = (com.navdy.hud.app.ui.activity.Main.ScreenInfo) this.operationQueue.remove();
                com.navdy.hud.app.ui.activity.Main.sLogger.v("runQueuedOperation size:" + size + " posting:" + info.screen);
                this.handler.post(new com.navdy.hud.app.ui.activity.Main.Presenter.Anon7(info));
                return;
            }
            setInputFocus();
            this.animationRunning = false;
            com.navdy.hud.app.ui.activity.Main.sLogger.v("animationRunning:false");
        }

        private void persistSettings() {
            com.navdy.hud.app.ui.activity.Main.sLogger.d("Starting to persist settings");
            java.lang.String json = new com.google.gson.Gson().toJson((java.lang.Object) this.settings);
            this.preferences.edit().putString(MAIN_SETTINGS, json).apply();
            com.navdy.hud.app.ui.activity.Main.sLogger.d("Persisted settings:" + json);
        }

        public void startVoiceAssistance(boolean voiceSearch) {
            com.navdy.hud.app.ui.activity.Main.sLogger.v("startVoiceAssistance , Voice search : " + voiceSearch);
            if (this.notificationManager.isNotificationViewShowing()) {
                com.navdy.hud.app.ui.activity.Main.sLogger.v("startVoiceAssistance:add pending");
                this.notificationManager.addPendingNotification(voiceSearch ? this.voiceSearchNotification : this.voiceAssistNotification);
                if (this.notificationManager.isExpanded() || this.notificationManager.isExpandedNotificationVisible()) {
                    this.notificationManager.collapseExpandedNotification(true, true);
                } else {
                    this.notificationManager.collapseNotification();
                }
            } else {
                launchVoiceAssistance(voiceSearch);
            }
        }

        private void launchVoiceAssistance(boolean voiceSearch) {
            com.navdy.hud.app.framework.toast.ToastManager toastManager = com.navdy.hud.app.framework.toast.ToastManager.getInstance();
            if (!toastManager.isToastDisplayed() || !android.text.TextUtils.equals(toastManager.getCurrentToastId(), com.navdy.hud.app.framework.phonecall.CallNotification.CALL_NOTIFICATION_TOAST_ID)) {
                toastManager.disableToasts(true);
                com.navdy.hud.app.ui.activity.Main.sLogger.v("startVoiceAssistance: disable toast, current notif:" + this.notificationManager.getCurrentNotification());
                this.notificationManager.addNotification(voiceSearch ? this.voiceSearchNotification : this.voiceAssistNotification);
                return;
            }
            com.navdy.hud.app.ui.activity.Main.sLogger.v("startVoiceAssistance:cannot launch, phone toast");
        }

        private void launchVoiceSearch() {
            com.navdy.hud.app.framework.notifications.NotificationManager notificationManager2 = com.navdy.hud.app.framework.notifications.NotificationManager.getInstance();
            com.navdy.hud.app.framework.voice.VoiceSearchNotification voiceSearchNotification2 = (com.navdy.hud.app.framework.voice.VoiceSearchNotification) notificationManager2.getNotification(com.navdy.hud.app.framework.notifications.NotificationId.VOICE_SEARCH_NOTIFICATION_ID);
            if (voiceSearchNotification2 == null) {
                voiceSearchNotification2 = new com.navdy.hud.app.framework.voice.VoiceSearchNotification();
            }
            notificationManager2.addNotification(voiceSearchNotification2);
        }

        public com.navdy.hud.app.service.ConnectionHandler getConnectionHandler() {
            return this.connectionHandler;
        }

        /* access modifiers changed from: protected */
        public void onExitScope() {
            super.onExitScope();
            com.navdy.hud.app.ui.activity.Main.sLogger.i("Shutting down main service");
            this.connectionHandler.disconnect();
        }

        public boolean onGesture(com.navdy.service.library.events.input.GestureEvent event) {
            if (event == null || event.gesture == null) {
                return false;
            }
            switch (event.gesture) {
                case GESTURE_SWIPE_LEFT:
                    com.navdy.hud.app.framework.notifications.NotificationManager notificationManager2 = com.navdy.hud.app.framework.notifications.NotificationManager.getInstance();
                    if (notificationManager2.getNotificationCount() > 0) {
                        com.navdy.hud.app.analytics.AnalyticsSupport.setGlanceNavigationSource("swipe");
                    }
                    notificationManager2.expandNotification();
                    break;
            }
            return true;
        }

        public boolean onKey(com.navdy.hud.app.manager.InputManager.CustomKeyEvent event) {
            return this.innerEventsDetector.onKey(event);
        }

        private boolean isPortrait() {
            com.navdy.hud.app.view.MainView mainView = (com.navdy.hud.app.view.MainView) getView();
            if (mainView != null) {
                this.showingPortrait = com.navdy.hud.app.util.ScreenOrientation.isPortrait((android.app.Activity) mainView.getContext());
            }
            return this.showingPortrait;
        }

        public com.navdy.hud.app.settings.MainScreenSettings.ScreenConfiguration getScreenConfiguration() {
            return isPortrait() ? this.settings.getPortraitConfiguration() : this.settings.getLandscapeConfiguration();
        }

        public void setMargins(int left, int top, int right, int bottom) {
            getScreenConfiguration().setMargins(new android.graphics.Rect(0, top, 0, 0));
            persistSettings();
            updateView();
        }

        public void updateView() {
            com.navdy.hud.app.view.MainView mainView = (com.navdy.hud.app.view.MainView) getView();
            if (mainView != null) {
                mainView.setVerticalOffset(getScreenConfiguration().getMargins().top);
                setNotificationColorVisibility(0);
                setSystemTrayVisibility(0);
                com.navdy.hud.app.ui.component.SystemTrayView systemTrayView = getSystemTray();
                if (systemTrayView != null) {
                    systemTrayView.setState(com.navdy.hud.app.ui.component.SystemTrayView.Device.Dial, com.navdy.hud.app.device.dial.DialManager.getInstance().isDialConnected() ? com.navdy.hud.app.ui.component.SystemTrayView.State.CONNECTED : com.navdy.hud.app.ui.component.SystemTrayView.State.DISCONNECTED);
                    systemTrayView.setState(com.navdy.hud.app.ui.component.SystemTrayView.Device.Phone, com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().isRemoteDeviceConnected() ? com.navdy.hud.app.ui.component.SystemTrayView.State.CONNECTED : com.navdy.hud.app.ui.component.SystemTrayView.State.DISCONNECTED);
                    com.navdy.hud.app.maps.here.HereMapsManager hereMapsManager = com.navdy.hud.app.maps.here.HereMapsManager.getInstance();
                    if (hereMapsManager.isInitialized()) {
                        com.navdy.hud.app.maps.here.HereLocationFixManager locationFixManager = hereMapsManager.getLocationFixManager();
                        if (locationFixManager != null && !locationFixManager.hasLocationFix()) {
                            systemTrayView.setState(com.navdy.hud.app.ui.component.SystemTrayView.Device.Gps, com.navdy.hud.app.ui.component.SystemTrayView.State.LOCATION_LOST);
                        }
                    }
                }
            }
        }

        /* access modifiers changed from: 0000 */
        public boolean isMainUIShrunk() {
            com.navdy.hud.app.view.MainView mainView = (com.navdy.hud.app.view.MainView) getView();
            if (mainView != null) {
                return mainView.isMainUIShrunk();
            }
            return false;
        }

        /* access modifiers changed from: 0000 */
        public boolean isNotificationViewShowing() {
            com.navdy.hud.app.view.MainView mainView = (com.navdy.hud.app.view.MainView) getView();
            if (mainView != null) {
                return mainView.isNotificationViewShowing();
            }
            return false;
        }

        /* access modifiers changed from: 0000 */
        public boolean isNotificationExpanding() {
            com.navdy.hud.app.view.MainView mainView = (com.navdy.hud.app.view.MainView) getView();
            if (mainView != null) {
                return mainView.isNotificationExpanding();
            }
            return false;
        }

        /* access modifiers changed from: 0000 */
        public boolean isNotificationCollapsing() {
            com.navdy.hud.app.view.MainView mainView = (com.navdy.hud.app.view.MainView) getView();
            if (mainView != null) {
                return mainView.isNotificationCollapsing();
            }
            return false;
        }

        /* access modifiers changed from: 0000 */
        public boolean isScreenAnimating() {
            com.navdy.hud.app.view.MainView mainView = (com.navdy.hud.app.view.MainView) getView();
            if (mainView != null) {
                return mainView.isScreenAnimating();
            }
            return false;
        }

        /* access modifiers changed from: 0000 */
        public com.navdy.hud.app.view.NotificationView getNotificationView() {
            com.navdy.hud.app.view.MainView mainView = (com.navdy.hud.app.view.MainView) getView();
            if (mainView != null) {
                return mainView.getNotificationView();
            }
            return null;
        }

        /* access modifiers changed from: 0000 */
        public android.widget.FrameLayout getExpandedNotificationView() {
            com.navdy.hud.app.view.MainView mainView = (com.navdy.hud.app.view.MainView) getView();
            if (mainView != null) {
                return mainView.getExpandedNotificationView();
            }
            return null;
        }

        /* access modifiers changed from: 0000 */
        public android.view.ViewGroup getNotificationExtensionView() {
            com.navdy.hud.app.view.MainView mainView = (com.navdy.hud.app.view.MainView) getView();
            if (mainView != null) {
                return mainView.getNotificationExtensionView();
            }
            return null;
        }

        public void addNotificationExtensionView(android.view.View view) {
            com.navdy.hud.app.view.MainView mainView = (com.navdy.hud.app.view.MainView) getView();
            if (mainView != null) {
                mainView.addNotificationExtensionView(view);
            }
        }

        public void removeNotificationExtensionView() {
            com.navdy.hud.app.view.MainView mainView = (com.navdy.hud.app.view.MainView) getView();
            if (mainView != null) {
                mainView.removeNotificationExtensionView();
            }
        }

        public void injectMainLowerView(android.view.View view) {
            com.navdy.hud.app.view.MainView mainView = (com.navdy.hud.app.view.MainView) getView();
            if (mainView != null) {
                mainView.injectMainLowerView(view);
            }
        }

        public void ejectMainLowerView() {
            com.navdy.hud.app.view.MainView mainView = (com.navdy.hud.app.view.MainView) getView();
            if (mainView != null) {
                mainView.ejectMainLowerView();
            }
        }

        public boolean isMainLowerViewVisible() {
            com.navdy.hud.app.view.MainView mainView = (com.navdy.hud.app.view.MainView) getView();
            if (mainView != null) {
                return mainView.isMainLowerViewVisible();
            }
            return false;
        }

        /* access modifiers changed from: 0000 */
        public android.view.View getExpandedNotificationCoverView() {
            com.navdy.hud.app.view.MainView mainView = (com.navdy.hud.app.view.MainView) getView();
            if (mainView != null) {
                return mainView.getExpandedNotificationCoverView();
            }
            return null;
        }

        /* access modifiers changed from: 0000 */
        public com.navdy.hud.app.ui.component.carousel.CarouselIndicator getNotificationIndicator() {
            com.navdy.hud.app.view.MainView mainView = (com.navdy.hud.app.view.MainView) getView();
            if (mainView != null) {
                return mainView.getNotificationIndicator();
            }
            return null;
        }

        /* access modifiers changed from: 0000 */
        public com.navdy.hud.app.ui.component.carousel.ProgressIndicator getNotificationScrollIndicator() {
            com.navdy.hud.app.view.MainView mainView = (com.navdy.hud.app.view.MainView) getView();
            if (mainView != null) {
                return mainView.getNotificationScrollIndicator();
            }
            return null;
        }

        /* access modifiers changed from: 0000 */
        public void setNotificationColorVisibility(int visibility) {
            if (!this.notificationColorEnabled) {
                com.navdy.hud.app.ui.activity.Main.sLogger.v("notifColor is disabled");
                return;
            }
            com.navdy.hud.app.view.MainView view = (com.navdy.hud.app.view.MainView) getView();
            if (view != null) {
                if (visibility != 0) {
                    view.setNotificationColor(this.defaultNotifColor);
                    view.setNotificationColorVisibility(visibility);
                } else if (this.notificationManager.getCurrentNotification() == null || (!isNotificationExpanding() && !isNotificationViewShowing())) {
                    int color = this.notificationManager.getNotificationColor();
                    if (color == -1) {
                        color = this.defaultNotifColor;
                    }
                    view.setNotificationColor(color);
                    view.setNotificationColorVisibility(visibility);
                } else {
                    com.navdy.hud.app.ui.activity.Main.sLogger.v("not showing notifcolor, pending notif");
                    return;
                }
                com.navdy.hud.app.ui.activity.Main.sLogger.v("notifColor: " + (visibility == 0 ? "visible" : "invisible"));
            }
        }

        public void setSystemTrayVisibility(int visibility) {
            if (!this.systemTrayEnabled) {
                com.navdy.hud.app.ui.activity.Main.sLogger.v("systemtray is disabled");
                return;
            }
            com.navdy.hud.app.view.MainView view = (com.navdy.hud.app.view.MainView) getView();
            if (view == null) {
                return;
            }
            if (visibility != 0 || this.notificationManager.getCurrentNotification() == null || (!isNotificationExpanding() && !isNotificationViewShowing())) {
                view.getSystemTray().setVisibility(visibility);
                com.navdy.hud.app.ui.activity.Main.sLogger.v("systemtray is " + (visibility == 0 ? "visible" : "invisible"));
                return;
            }
            com.navdy.hud.app.ui.activity.Main.sLogger.v("not showing systemtray, pending notif");
        }

        /* access modifiers changed from: 0000 */
        public void enableSystemTray(boolean enabled) {
            this.systemTrayEnabled = enabled;
            com.navdy.hud.app.view.MainView view = (com.navdy.hud.app.view.MainView) getView();
            if (view != null) {
                com.navdy.hud.app.ui.component.SystemTrayView systemTrayView = view.getSystemTray();
                if (!this.systemTrayEnabled) {
                    systemTrayView.setVisibility(4);
                } else {
                    setSystemTrayVisibility(0);
                }
            }
        }

        /* access modifiers changed from: 0000 */
        public void enableNotificationColor(boolean enabled) {
            this.notificationColorEnabled = enabled;
            com.navdy.hud.app.view.MainView view = (com.navdy.hud.app.view.MainView) getView();
            if (view != null) {
                if (!this.notificationColorEnabled) {
                    view.setNotificationColorVisibility(4);
                    com.navdy.hud.app.ui.activity.Main.sLogger.v("notif invisible");
                    return;
                }
                com.navdy.hud.app.screen.BaseScreen screen = this.uiStateManager.getCurrentScreen();
                if (screen != null) {
                    com.navdy.hud.app.ui.framework.UIStateManager uIStateManager = this.uiStateManager;
                    if (com.navdy.hud.app.ui.framework.UIStateManager.isFullscreenMode(screen.getScreen())) {
                        setNotificationColorVisibility(0);
                        com.navdy.hud.app.ui.activity.Main.sLogger.v("notif visible");
                        return;
                    }
                }
                view.setNotificationColorVisibility(4);
                com.navdy.hud.app.ui.activity.Main.sLogger.v("notif invisible");
            }
        }

        /* access modifiers changed from: 0000 */
        public com.navdy.hud.app.ui.component.SystemTrayView getSystemTray() {
            com.navdy.hud.app.view.MainView view = (com.navdy.hud.app.view.MainView) getView();
            if (view == null) {
                return null;
            }
            return view.getSystemTray();
        }

        public com.navdy.hud.app.manager.InputManager.IInputHandler nextHandler() {
            return null;
        }

        private void setupFlow() {
            flow.Backstack backstack = flow.Backstack.single(new com.navdy.hud.app.screen.FirstLaunchScreen());
            this.currentScreen = com.navdy.service.library.events.ui.Screen.SCREEN_FIRST_LAUNCH;
            this.uiStateManager.setCurrentViewMode(this.currentScreen);
            this.f6flow = new flow.Flow(backstack, this);
            this.f6flow.resetTo(this.f6flow.getBackstack().current().getScreen());
        }

        public void go(flow.Backstack nextBackstack, flow.Flow.Direction direction, flow.Flow.Callback callback) {
            com.navdy.hud.app.view.MainView mainView = (com.navdy.hud.app.view.MainView) getView();
            if (mainView != null) {
                com.navdy.hud.app.view.ContainerView container = mainView.getContainerView();
                com.navdy.hud.app.screen.BaseScreen screen = (com.navdy.hud.app.screen.BaseScreen) nextBackstack.current().getScreen();
                int animationOut = -1;
                flow.Backstack backstack = this.f6flow.getBackstack();
                if (backstack.size() > 0) {
                    animationOut = ((com.navdy.hud.app.screen.BaseScreen) backstack.current().getScreen()).getAnimationOut(flow.Flow.Direction.FORWARD);
                }
                container.showScreen(screen, flow.Flow.Direction.FORWARD, screen.getAnimationIn(flow.Flow.Direction.FORWARD), animationOut);
            }
            callback.onComplete();
        }

        public void createScreens() {
            if (!this.createdScreens) {
                com.navdy.hud.app.view.MainView mainView = (com.navdy.hud.app.view.MainView) getView();
                if (mainView != null) {
                    com.navdy.hud.app.ui.activity.Main.sLogger.v("createScreens");
                    com.navdy.hud.app.view.ContainerView container = mainView.getContainerView();
                    this.createdScreens = true;
                    container.createScreens();
                }
            }
        }

        public void clearInputFocus() {
            if (!(this.inputManager.getFocus() instanceof com.navdy.hud.app.view.ToastView)) {
                this.inputManager.setFocus(null);
                com.navdy.hud.app.ui.activity.Main.sLogger.v("[focus] cleared");
            }
        }

        public void setInputFocus() {
            if (this.inputManager.getFocus() instanceof com.navdy.hud.app.view.ToastView) {
                com.navdy.hud.app.ui.activity.Main.sLogger.v("[focus] not setting focus[toast]");
                return;
            }
            com.navdy.hud.app.view.MainView mainView = (com.navdy.hud.app.view.MainView) getView();
            if (mainView == null) {
                com.navdy.hud.app.ui.activity.Main.sLogger.v("[focus] mainview is null");
            } else if (isNotificationViewShowing()) {
                this.inputManager.setFocus(mainView.getNotificationView());
                com.navdy.hud.app.ui.activity.Main.sLogger.v("[focus] notification");
            } else {
                com.navdy.hud.app.view.ContainerView containerView = mainView.getContainerView();
                if (containerView == null) {
                    this.inputManager.setFocus(mainView);
                    com.navdy.hud.app.ui.activity.Main.sLogger.v("[focus] mainView");
                    return;
                }
                android.view.View currentView = containerView.getCurrentView();
                if (currentView instanceof com.navdy.hud.app.manager.InputManager.IInputHandler) {
                    this.inputManager.setFocus((com.navdy.hud.app.manager.InputManager.IInputHandler) currentView);
                    com.navdy.hud.app.ui.activity.Main.sLogger.v("[focus] screen:" + currentView.getClass().getSimpleName());
                    return;
                }
                this.inputManager.setFocus(mainView);
                com.navdy.hud.app.ui.activity.Main.sLogger.v("[focus] mainView");
            }
        }

        @com.squareup.otto.Subscribe
        public void onDisplayScaleChange(com.navdy.hud.app.event.DisplayScaleChange event) {
            setDisplayFormat(event.format);
        }

        @com.squareup.otto.Subscribe
        public void onReadSettings(com.navdy.service.library.events.settings.ReadSettingsRequest request) {
            com.navdy.service.library.device.RemoteDevice device = this.connectionHandler.getRemoteDevice();
            if (device != null) {
                com.navdy.hud.app.settings.MainScreenSettings.ScreenConfiguration currentConfig = getScreenConfiguration();
                android.graphics.Rect margins = currentConfig.getMargins();
                device.postEvent((com.squareup.wire.Message) new com.navdy.service.library.events.settings.ReadSettingsResponse(new com.navdy.service.library.events.settings.ScreenConfiguration(java.lang.Integer.valueOf(margins.left), java.lang.Integer.valueOf(margins.top), java.lang.Integer.valueOf(margins.right), java.lang.Integer.valueOf(margins.bottom), java.lang.Float.valueOf(currentConfig.getScaleX()), java.lang.Float.valueOf(currentConfig.getScaleY())), this.hudSettings.readSettings(request.settings)));
            }
        }

        @com.squareup.otto.Subscribe
        public void onUpdateSettings(com.navdy.service.library.events.settings.UpdateSettings request) {
            com.navdy.service.library.events.settings.ScreenConfiguration configuration = request.screenConfiguration;
            if (configuration != null) {
                com.navdy.hud.app.settings.MainScreenSettings.ScreenConfiguration currentConfiguration = getScreenConfiguration();
                currentConfiguration.setMargins(new android.graphics.Rect(configuration.marginLeft.intValue(), configuration.marginTop.intValue(), configuration.marginRight.intValue(), configuration.marginBottom.intValue()));
                currentConfiguration.setScaleX(configuration.scaleX.floatValue());
                currentConfiguration.setScaleY(configuration.scaleY.floatValue());
            }
            this.hudSettings.updateSettings(request.settings);
            persistSettings();
            updateView();
        }

        @com.squareup.otto.Subscribe
        public void onNotificationChange(com.navdy.hud.app.framework.notifications.NotificationManager.NotificationChange event) {
            com.navdy.hud.app.view.MainView view = (com.navdy.hud.app.view.MainView) getView();
            if (view != null) {
                setNotificationColorVisibility(view.getNotificationColorVisibility());
            }
        }

        private static mortar.Blueprint lookupScreen(com.navdy.service.library.events.ui.Screen screen) {
            try {
                switch (screen) {
                    case SCREEN_HYBRID_MAP:
                    case SCREEN_DASHBOARD:
                        screen = com.navdy.service.library.events.ui.Screen.SCREEN_HOME;
                        break;
                }
                java.lang.Class clz = (java.lang.Class) com.navdy.hud.app.ui.activity.Main.SCREEN_MAP.get(screen.ordinal());
                if (clz != null) {
                    return (mortar.Blueprint) clz.newInstance();
                }
            } catch (java.lang.InstantiationException e) {
                com.navdy.hud.app.ui.activity.Main.sLogger.e("Failed to instantiate screen", e);
            } catch (java.lang.IllegalAccessException e2) {
                com.navdy.hud.app.ui.activity.Main.sLogger.e("Failed to instantiate screen", e2);
            }
            return null;
        }

        @com.squareup.otto.Subscribe
        public void onShowScreen(com.navdy.service.library.events.ui.ShowScreen event) {
            if (this.uiStateManager.getRootScreen() == null) {
                com.navdy.hud.app.ui.activity.Main.sLogger.w("Main screen not up, cannot show screen:" + event);
            } else if (event.arguments == null || event.arguments.size() == 0) {
                updateScreen(event.screen, null, null, false, false);
            } else {
                java.util.List<com.navdy.service.library.events.glances.KeyValue> arguments = event.arguments;
                android.os.Bundle bundle = new android.os.Bundle();
                for (com.navdy.service.library.events.glances.KeyValue argument : arguments) {
                    if (!(argument.key == null || argument.value == null)) {
                        bundle.putString(argument.key, argument.value);
                    }
                }
                updateScreen(event.screen, bundle, null, false, false);
            }
        }

        @com.squareup.otto.Subscribe
        public void onShowScreen(com.navdy.hud.app.event.ShowScreenWithArgs event) {
            if (this.uiStateManager.getRootScreen() == null) {
                com.navdy.hud.app.ui.activity.Main.sLogger.w("Main screen not up, cannot show screen-args:" + event);
                return;
            }
            updateScreen(event.screen, event.args, event.args2, event.ignoreAnimation, false);
        }

        /* access modifiers changed from: private */
        public void updateScreen(com.navdy.service.library.events.ui.Screen screen, android.os.Bundle args, java.lang.Object args2, boolean ignoreAnimationRunning, boolean dismissScreen) {
            boolean voiceSearch;
            android.content.SharedPreferences preferredPreferences;
            com.navdy.hud.app.ui.activity.Main.sLogger.v("updateScreen screen[" + screen + "] animRunning[" + this.animationRunning + "] ignoreAnim [" + ignoreAnimationRunning + "] dismiss[" + dismissScreen + "]");
            if (ignoreAnimationRunning || !this.animationRunning) {
                this.animationRunning = true;
                com.navdy.hud.app.ui.activity.Main.sLogger.v("animationRunning:true");
                boolean notif = false;
                switch (screen) {
                    case SCREEN_MUSIC:
                        if (com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().isRemoteDeviceConnected()) {
                            this.musicManager.showMusicNotification();
                        } else {
                            this.connectionHandler.sendConnectionNotification();
                        }
                        notif = true;
                        break;
                    case SCREEN_VOICE_CONTROL:
                        if (com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().isRemoteDeviceConnected()) {
                            if (args != null) {
                                if (args.getBoolean(com.navdy.hud.app.framework.notifications.NotificationManager.EXTRA_VOICE_SEARCH_NOTIFICATION, false)) {
                                    voiceSearch = true;
                                    startVoiceAssistance(voiceSearch);
                                }
                            }
                            voiceSearch = false;
                            startVoiceAssistance(voiceSearch);
                        } else {
                            this.connectionHandler.sendConnectionNotification();
                        }
                        notif = true;
                        break;
                    case SCREEN_BRIGHTNESS:
                        com.navdy.hud.app.ui.activity.Main.showBrightnessNotification();
                        notif = true;
                        break;
                }
                if (notif) {
                    com.navdy.hud.app.ui.activity.Main.sLogger.v("notif screen");
                    runQueuedOperation();
                    return;
                }
                if (screen == com.navdy.service.library.events.ui.Screen.SCREEN_BACK) {
                    screen = this.uiStateManager.getCurrentViewMode();
                }
                if (!com.navdy.hud.app.ui.framework.UIStateManager.isSidePanelMode(screen)) {
                    clearViews();
                    mortar.Blueprint targetScreen = lookupScreen(screen);
                    if (targetScreen instanceof com.navdy.hud.app.screen.BaseScreen) {
                        ((com.navdy.hud.app.screen.BaseScreen) targetScreen).setArguments(args, args2);
                    }
                    if (targetScreen instanceof com.navdy.hud.app.ui.component.homescreen.HomeScreen) {
                        switch (screen) {
                            case SCREEN_HYBRID_MAP:
                                screen = com.navdy.service.library.events.ui.Screen.SCREEN_HOME;
                                com.navdy.hud.app.ui.activity.Main.saveHomeScreenPreference(this.globalPreferences, com.navdy.hud.app.ui.component.homescreen.HomeScreen.DisplayMode.MAP.ordinal());
                                ((com.navdy.hud.app.ui.component.homescreen.HomeScreen) targetScreen).setDisplayMode(com.navdy.hud.app.ui.component.homescreen.HomeScreen.DisplayMode.MAP);
                                break;
                            case SCREEN_DASHBOARD:
                                com.navdy.hud.app.ui.activity.Main.saveHomeScreenPreference(this.globalPreferences, com.navdy.hud.app.ui.component.homescreen.HomeScreen.DisplayMode.SMART_DASH.ordinal());
                                ((com.navdy.hud.app.ui.component.homescreen.HomeScreen) targetScreen).setDisplayMode(com.navdy.hud.app.ui.component.homescreen.HomeScreen.DisplayMode.SMART_DASH);
                                screen = com.navdy.service.library.events.ui.Screen.SCREEN_HOME;
                                break;
                            case SCREEN_HOME:
                                com.navdy.hud.app.ui.component.homescreen.HomeScreen.DisplayMode mode = ((com.navdy.hud.app.ui.component.homescreen.HomeScreen) targetScreen).getDisplayMode();
                                int ordinal = mode.ordinal();
                                boolean isDefaultProfile = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile().isDefaultProfile();
                                android.content.SharedPreferences driverPreferences = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getDriverProfileManager().getLocalPreferencesForCurrentDriverProfile(com.navdy.hud.app.HudApplication.getAppContext());
                                if (isDefaultProfile || driverPreferences == null) {
                                    preferredPreferences = this.globalPreferences;
                                } else {
                                    preferredPreferences = driverPreferences;
                                }
                                if (preferredPreferences != null) {
                                    ordinal = preferredPreferences.getInt(com.navdy.hud.app.ui.activity.Main.PREFERENCE_HOME_SCREEN_MODE, mode.ordinal());
                                }
                                if (ordinal != mode.ordinal()) {
                                    if (ordinal == com.navdy.hud.app.ui.component.homescreen.HomeScreen.DisplayMode.MAP.ordinal()) {
                                        ((com.navdy.hud.app.ui.component.homescreen.HomeScreen) targetScreen).setDisplayMode(com.navdy.hud.app.ui.component.homescreen.HomeScreen.DisplayMode.MAP);
                                    } else if (ordinal == com.navdy.hud.app.ui.component.homescreen.HomeScreen.DisplayMode.SMART_DASH.ordinal()) {
                                        ((com.navdy.hud.app.ui.component.homescreen.HomeScreen) targetScreen).setDisplayMode(com.navdy.hud.app.ui.component.homescreen.HomeScreen.DisplayMode.SMART_DASH);
                                    }
                                    com.navdy.hud.app.ui.activity.Main.saveHomeScreenPreference(this.globalPreferences, ordinal);
                                    break;
                                }
                                break;
                        }
                    }
                    if (com.navdy.hud.app.ui.framework.UIStateManager.isFullscreenMode(screen)) {
                        this.uiStateManager.setCurrentViewMode(screen);
                    }
                    if (targetScreen != null) {
                        this.currentScreen = screen;
                        this.f6flow.replaceTo(targetScreen);
                    }
                } else if (com.navdy.hud.app.ui.framework.UIStateManager.isOverlayMode(this.currentScreen)) {
                    this.bus.post(new com.navdy.hud.app.event.ShowScreenWithArgs(com.navdy.service.library.events.ui.Screen.SCREEN_BACK, null, true));
                    this.bus.post(new com.navdy.hud.app.event.ShowScreenWithArgs(screen, args, args2, true));
                } else {
                    switch (screen) {
                        case SCREEN_NOTIFICATION:
                            if (!dismissScreen) {
                                showNotification();
                                return;
                            }
                            com.navdy.hud.app.view.MainView mainView = (com.navdy.hud.app.view.MainView) getView();
                            if (mainView != null && mainView.isNotificationViewShowing() && !this.notifAnimationRunning) {
                                com.navdy.hud.app.ui.activity.Main.sLogger.v("NotificationManager:calling dismissNotification");
                                mainView.dismissNotification();
                                return;
                            }
                            return;
                        default:
                            return;
                    }
                }
            } else {
                this.operationQueue.add(new com.navdy.hud.app.ui.activity.Main.ScreenInfo(screen, args, args2, dismissScreen));
            }
        }

        private boolean verifyProtocolVersion(com.navdy.service.library.events.DeviceInfo deviceInfo) {
            int remoteMajorVersion = -1;
            try {
                remoteMajorVersion = java.lang.Integer.parseInt(deviceInfo.protocolVersion.split("\\.", 2)[0]);
            } catch (java.lang.NumberFormatException e) {
                com.navdy.hud.app.ui.activity.Main.sLogger.e("invalid remote device protocol version: " + deviceInfo.protocolVersion);
            }
            if (com.navdy.service.library.Version.PROTOCOL_VERSION.majorVersion == remoteMajorVersion || (com.navdy.service.library.Version.PROTOCOL_VERSION.majorVersion == 1 && remoteMajorVersion == 0)) {
                com.navdy.hud.app.ui.activity.Main.mProtocolStatus = com.navdy.hud.app.ui.activity.Main.ProtocolStatus.PROTOCOL_VALID;
                return true;
            }
            if (com.navdy.service.library.Version.PROTOCOL_VERSION.majorVersion < remoteMajorVersion) {
                com.navdy.hud.app.ui.activity.Main.mProtocolStatus = com.navdy.hud.app.ui.activity.Main.ProtocolStatus.PROTOCOL_LOCAL_NEEDS_UPDATE;
            } else {
                com.navdy.hud.app.ui.activity.Main.mProtocolStatus = com.navdy.hud.app.ui.activity.Main.ProtocolStatus.PROTOCOL_REMOTE_NEEDS_UPDATE;
            }
            com.navdy.hud.app.ui.activity.Main.sLogger.e(java.lang.String.format("protocol version mismatch remote = %s, local = %s", new java.lang.Object[]{deviceInfo.protocolVersion, com.navdy.service.library.Version.PROTOCOL_VERSION.toString()}));
            return false;
        }

        @com.squareup.otto.Subscribe
        public void onDeviceInfo(com.navdy.service.library.events.DeviceInfo deviceInfo) {
            if (verifyProtocolVersion(deviceInfo)) {
                com.navdy.service.library.events.connection.ConnectionStateChange.Builder builder = new com.navdy.service.library.events.connection.ConnectionStateChange.Builder();
                builder.state(com.navdy.service.library.events.connection.ConnectionStateChange.ConnectionState.CONNECTION_VERIFIED);
                builder.remoteDeviceId(deviceInfo.deviceId);
                this.bus.post(builder.build());
                return;
            }
            this.bus.post(new com.navdy.service.library.events.ui.ShowScreen.Builder().screen(com.navdy.service.library.events.ui.Screen.SCREEN_FORCE_UPDATE).build());
        }

        @com.squareup.otto.Subscribe
        public void onDeviceConnectionStateChange(com.navdy.service.library.events.connection.ConnectionStateChange connectionStateChange) {
            com.navdy.hud.app.ui.activity.Main.sLogger.v("status:" + connectionStateChange.remoteDeviceId + " : " + connectionStateChange.state.name());
            com.navdy.hud.app.ui.component.SystemTrayView systemTrayView = getSystemTray();
            switch (connectionStateChange.state) {
                case CONNECTION_VERIFIED:
                    this.musicManager.initialize();
                    if (systemTrayView != null) {
                        systemTrayView.setState(com.navdy.hud.app.ui.component.SystemTrayView.Device.Phone, com.navdy.hud.app.ui.component.SystemTrayView.State.CONNECTED);
                        return;
                    }
                    return;
                case CONNECTION_DISCONNECTED:
                    if (systemTrayView != null) {
                        systemTrayView.setState(com.navdy.hud.app.ui.component.SystemTrayView.Device.Phone, com.navdy.hud.app.ui.component.SystemTrayView.State.DISCONNECTED);
                    }
                    if (com.navdy.hud.app.ui.activity.Main.mProtocolStatus == com.navdy.hud.app.ui.activity.Main.ProtocolStatus.PROTOCOL_LOCAL_NEEDS_UPDATE && this.currentScreen == com.navdy.service.library.events.ui.Screen.SCREEN_FORCE_UPDATE) {
                        this.bus.post(new com.navdy.service.library.events.ui.ShowScreen.Builder().screen(com.navdy.service.library.events.ui.Screen.SCREEN_HOME).build());
                    }
                    com.navdy.hud.app.ui.activity.Main.mProtocolStatus = com.navdy.hud.app.ui.activity.Main.ProtocolStatus.PROTOCOL_VALID;
                    return;
                default:
                    return;
            }
        }

        @com.squareup.otto.Subscribe
        public void onPhoneBatteryStatusEvent(com.navdy.service.library.events.callcontrol.PhoneBatteryStatus event) {
            com.navdy.hud.app.ui.activity.Main.sLogger.v("PhoneBatteryStatus status[" + event.status + "] level[" + event.status + "] charging[" + event.charging + "]");
            com.navdy.hud.app.ui.component.SystemTrayView systemTrayView = getSystemTray();
            if (systemTrayView != null) {
                if (java.lang.Boolean.TRUE.equals(event.charging)) {
                    systemTrayView.setState(com.navdy.hud.app.ui.component.SystemTrayView.Device.Phone, com.navdy.hud.app.ui.component.SystemTrayView.State.OK_BATTERY);
                    return;
                }
                switch (event.status) {
                    case BATTERY_OK:
                        systemTrayView.setState(com.navdy.hud.app.ui.component.SystemTrayView.Device.Phone, com.navdy.hud.app.ui.component.SystemTrayView.State.OK_BATTERY);
                        return;
                    case BATTERY_EXTREMELY_LOW:
                        systemTrayView.setState(com.navdy.hud.app.ui.component.SystemTrayView.Device.Phone, com.navdy.hud.app.ui.component.SystemTrayView.State.EXTREMELY_LOW_BATTERY);
                        return;
                    case BATTERY_LOW:
                        systemTrayView.setState(com.navdy.hud.app.ui.component.SystemTrayView.Device.Phone, com.navdy.hud.app.ui.component.SystemTrayView.State.LOW_BATTERY);
                        return;
                    default:
                        return;
                }
            }
        }

        @com.squareup.otto.Subscribe
        public void onDialConnectionEvent(com.navdy.hud.app.device.dial.DialConstants.DialConnectionStatus event) {
            com.navdy.hud.app.ui.component.SystemTrayView systemTrayView = getSystemTray();
            if (systemTrayView != null) {
                systemTrayView.setState(com.navdy.hud.app.ui.component.SystemTrayView.Device.Dial, event.status == com.navdy.hud.app.device.dial.DialConstants.DialConnectionStatus.Status.CONNECTED ? com.navdy.hud.app.ui.component.SystemTrayView.State.CONNECTED : com.navdy.hud.app.ui.component.SystemTrayView.State.DISCONNECTED);
            }
        }

        @com.squareup.otto.Subscribe
        public void onDialBatteryEvent(com.navdy.hud.app.device.dial.DialConstants.BatteryChangeEvent event) {
            com.navdy.hud.app.ui.component.SystemTrayView systemTrayView = getSystemTray();
            if (systemTrayView != null) {
                com.navdy.hud.app.device.dial.DialConstants.NotificationReason reason = com.navdy.hud.app.device.dial.DialManager.getInstance().getBatteryNotificationReason();
                com.navdy.hud.app.ui.component.SystemTrayView.State state = null;
                if (reason != null) {
                    switch (reason) {
                        case EXTREMELY_LOW_BATTERY:
                            state = com.navdy.hud.app.ui.component.SystemTrayView.State.EXTREMELY_LOW_BATTERY;
                            break;
                        case LOW_BATTERY:
                        case VERY_LOW_BATTERY:
                            state = com.navdy.hud.app.ui.component.SystemTrayView.State.LOW_BATTERY;
                            break;
                        case OK_BATTERY:
                            state = com.navdy.hud.app.ui.component.SystemTrayView.State.OK_BATTERY;
                            break;
                    }
                } else {
                    state = com.navdy.hud.app.ui.component.SystemTrayView.State.OK_BATTERY;
                }
                systemTrayView.setState(com.navdy.hud.app.ui.component.SystemTrayView.Device.Dial, state);
            }
        }

        @com.squareup.otto.Subscribe
        public void onNetworkStateChange(com.navdy.service.library.events.settings.NetworkStateChange event) {
            com.navdy.hud.app.ui.component.SystemTrayView systemTrayView = getSystemTray();
            if (systemTrayView != null) {
                if (java.lang.Boolean.TRUE.equals(event.networkAvailable)) {
                    systemTrayView.setState(com.navdy.hud.app.ui.component.SystemTrayView.Device.Phone, com.navdy.hud.app.ui.component.SystemTrayView.State.PHONE_NETWORK_AVAILABLE);
                } else {
                    systemTrayView.setState(com.navdy.hud.app.ui.component.SystemTrayView.Device.Phone, com.navdy.hud.app.ui.component.SystemTrayView.State.NO_PHONE_NETWORK);
                }
            }
        }

        @com.squareup.otto.Subscribe
        public void onLocationFixChangeEvent(com.navdy.hud.app.maps.MapEvents.LocationFix event) {
            com.navdy.hud.app.ui.component.SystemTrayView systemTrayView = getSystemTray();
            if (systemTrayView != null) {
                if (event.locationAvailable) {
                    systemTrayView.setState(com.navdy.hud.app.ui.component.SystemTrayView.Device.Gps, com.navdy.hud.app.ui.component.SystemTrayView.State.LOCATION_AVAILABLE);
                    if (com.navdy.hud.app.device.gps.GpsUtils.isDebugRawGpsPosEnabled()) {
                        onGpsSwitchEvent(new com.navdy.hud.app.device.gps.GpsUtils.GpsSwitch(event.usingPhoneLocation, event.usingLocalGpsLocation));
                        return;
                    }
                    return;
                }
                systemTrayView.setState(com.navdy.hud.app.ui.component.SystemTrayView.Device.Gps, com.navdy.hud.app.ui.component.SystemTrayView.State.LOCATION_LOST);
            }
        }

        @com.squareup.otto.Subscribe
        public void onGpsSatelliteData(com.navdy.hud.app.device.gps.GpsUtils.GpsSatelliteData satelliteData) {
            if (com.navdy.hud.app.device.gps.GpsUtils.isDebugRawGpsPosEnabled()) {
                int fixType = satelliteData.data.getInt(com.navdy.hud.app.device.gps.GpsConstants.GPS_FIX_TYPE, 0);
                com.navdy.hud.app.ui.component.SystemTrayView systemTrayView = getSystemTray();
                if (systemTrayView != null) {
                    java.lang.String fixTypeStr = null;
                    switch (fixType) {
                        case 1:
                            fixTypeStr = "2D";
                            break;
                        case 2:
                            fixTypeStr = "Diff";
                            break;
                        case 6:
                            fixTypeStr = "DR";
                            break;
                    }
                    systemTrayView.setDebugGpsMarker(fixTypeStr);
                }
            }
        }

        @com.squareup.otto.Subscribe
        public void onGpsSwitchEvent(com.navdy.hud.app.device.gps.GpsUtils.GpsSwitch event) {
            java.lang.String str;
            if (com.navdy.hud.app.device.gps.GpsUtils.isDebugRawGpsPosEnabled()) {
                com.navdy.hud.app.ui.activity.Main.sLogger.v("gps_switch_event:" + event.usingPhone);
                com.navdy.hud.app.ui.component.SystemTrayView systemTrayView = getSystemTray();
                if (systemTrayView != null) {
                    if (event.usingPhone) {
                        str = com.navdy.hud.app.device.gps.GpsConstants.GPS_PHONE_MARKER;
                    } else {
                        str = com.navdy.hud.app.device.gps.GpsConstants.GPS_UBLOX_MARKER;
                    }
                    systemTrayView.setDebugGpsMarker(str);
                }
            }
        }

        @com.squareup.otto.Subscribe
        public void onInstallingUpdate(com.navdy.hud.app.util.OTAUpdateService.InstallingUpdate event) {
            showInstallingUpdateToast();
        }

        private void showInstallingUpdateToast() {
            com.navdy.hud.app.framework.toast.ToastManager toastManager = com.navdy.hud.app.framework.toast.ToastManager.getInstance();
            toastManager.clearAllPendingToast();
            toastManager.disableToasts(false);
            android.content.res.Resources resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
            android.os.Bundle bundle = new android.os.Bundle();
            toastManager.dismissCurrentToast(com.navdy.hud.app.ui.activity.Main.INSTALLING_OTA_TOAST_ID);
            bundle.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE, resources.getString(com.navdy.hud.app.R.string.warning));
            bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_IMAGE, com.navdy.hud.app.R.drawable.icon_warning);
            bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_SIDE_IMAGE, com.navdy.hud.app.R.drawable.icon_sm_spinner);
            int powerWarning = com.navdy.hud.app.obd.ObdManager.getInstance().getConnectionType() == com.navdy.hud.app.obd.ObdManager.ConnectionType.POWER_ONLY ? com.navdy.hud.app.R.string.do_not_remove_power_or_turn_off_vehicle : com.navdy.hud.app.R.string.do_not_remove_power;
            bundle.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_1, resources.getString(com.navdy.hud.app.R.string.preparing_to_install_update));
            bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_1_STYLE, com.navdy.hud.app.R.style.installing_update_title_1);
            bundle.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_2, resources.getString(powerWarning));
            bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_2_STYLE, com.navdy.hud.app.R.style.installing_update_title_2);
            bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_INFO_CONTAINER_MAX_WIDTH, (int) resources.getDimension(com.navdy.hud.app.R.dimen.toast_installing_update_width));
            bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_INFO_CONTAINER_LEFT_PADDING, (int) resources.getDimension(com.navdy.hud.app.R.dimen.toast_installing_update_padding));
            toastManager.addToast(new com.navdy.hud.app.framework.toast.ToastManager.ToastParams(com.navdy.hud.app.ui.activity.Main.INSTALLING_OTA_TOAST_ID, bundle, new com.navdy.hud.app.ui.activity.Main.Presenter.Anon8(), true, true));
        }

        private void clearViews() {
            com.navdy.hud.app.view.MainView mainView = (com.navdy.hud.app.view.MainView) getView();
            if (mainView != null && mainView.isNotificationViewShowing() && !this.notifAnimationRunning) {
                com.navdy.hud.app.ui.activity.Main.sLogger.v("NotificationManager:dismissNotification");
                mainView.dismissNotification();
            }
        }

        private void showNotification() {
            com.navdy.hud.app.view.MainView mainView = (com.navdy.hud.app.view.MainView) getView();
            if (mainView != null) {
                com.navdy.hud.app.framework.notifications.INotification notification = this.notificationManager.getCurrentNotification();
                if (notification == null) {
                    com.navdy.hud.app.ui.activity.Main.sLogger.v("no current notification to be displayed");
                    runQueuedOperation();
                    return;
                }
                com.navdy.hud.app.ui.activity.Main.sLogger.v("NotificationManager:calling shownotification");
                com.navdy.hud.app.ui.component.homescreen.HomeScreenView homeScreenView = this.uiStateManager.getHomescreenView();
                if (homeScreenView.hasModeView() && homeScreenView.isModeVisible()) {
                    com.navdy.hud.app.ui.activity.Main.sLogger.v("mode-view: reset mode view show notif");
                    homeScreenView.resetModeView();
                }
                mainView.showNotification(notification.getId(), notification.getType());
            }
        }

        private void takeNotificationAction() {
            com.navdy.hud.app.manager.RemoteDeviceManager remoteDeviceManager = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance();
            if (!remoteDeviceManager.isRemoteDeviceConnected() || !remoteDeviceManager.isAppConnected()) {
                this.connectionHandler.sendConnectionNotification();
            } else if (this.notificationManager.isNotificationPresent(com.navdy.hud.app.framework.notifications.NotificationId.PHONE_CALL_NOTIFICATION_ID)) {
                com.navdy.hud.app.ui.activity.Main.sLogger.v("show phone call");
                this.notificationManager.expandNotification();
            } else {
                com.navdy.hud.app.ui.activity.Main.sLogger.v("show music");
                this.musicManager.showMusicNotification();
            }
        }

        private void hideNotification() {
            com.navdy.hud.app.view.MainView mainView = (com.navdy.hud.app.view.MainView) getView();
            if (mainView != null && this.notificationManager.getCurrentNotification() != null) {
                com.navdy.hud.app.ui.activity.Main.sLogger.v("NotificationManager:calling shownotification");
                mainView.dismissNotification();
            }
        }

        @com.squareup.otto.Subscribe
        public void onDismissScreen(com.navdy.service.library.events.ui.DismissScreen event) {
            if (event.screen == com.navdy.service.library.events.ui.Screen.SCREEN_NOTIFICATION) {
                updateScreen(com.navdy.service.library.events.ui.Screen.SCREEN_NOTIFICATION, null, null, false, true);
            }
        }

        /* access modifiers changed from: private */
        public void updateSystemTrayVisibility(com.navdy.hud.app.ui.activity.Main.ScreenCategory category, com.navdy.service.library.events.ui.Screen screen) {
            switch (category) {
                case NOTIFICATION:
                    com.navdy.hud.app.screen.BaseScreen baseScreen = this.uiStateManager.getCurrentScreen();
                    if (baseScreen != null) {
                        screen = baseScreen.getScreen();
                    }
                    com.navdy.hud.app.ui.framework.UIStateManager uIStateManager = this.uiStateManager;
                    if (com.navdy.hud.app.ui.framework.UIStateManager.isFullscreenMode(screen)) {
                        setNotificationColorVisibility(0);
                        setSystemTrayVisibility(0);
                        return;
                    }
                    return;
                case SCREEN:
                    if (!this.notifAnimationRunning) {
                        runQueuedOperation();
                    }
                    if (!this.notifAnimationRunning && !isNotificationViewShowing()) {
                        com.navdy.hud.app.ui.framework.UIStateManager uIStateManager2 = this.uiStateManager;
                        if (com.navdy.hud.app.ui.framework.UIStateManager.isFullscreenMode(screen)) {
                            setNotificationColorVisibility(0);
                            setSystemTrayVisibility(0);
                            return;
                        }
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    public enum ProtocolStatus {
        PROTOCOL_VALID,
        PROTOCOL_REMOTE_NEEDS_UPDATE,
        PROTOCOL_LOCAL_NEEDS_UPDATE
    }

    private enum ScreenCategory {
        NOTIFICATION,
        SCREEN
    }

    private static class ScreenInfo {
        android.os.Bundle args;
        java.lang.Object args2;
        boolean dismissScreen;
        com.navdy.service.library.events.ui.Screen screen;

        ScreenInfo(com.navdy.service.library.events.ui.Screen screen2, android.os.Bundle args3, java.lang.Object args22, boolean dismissScreen2) {
            this.screen = screen2;
            this.args = args3;
            this.args2 = args22;
            this.dismissScreen = dismissScreen2;
        }
    }

    static {
        SCREEN_MAP.put(com.navdy.service.library.events.ui.Screen.SCREEN_FIRST_LAUNCH.ordinal(), com.navdy.hud.app.screen.FirstLaunchScreen.class);
        SCREEN_MAP.put(com.navdy.service.library.events.ui.Screen.SCREEN_HOME.ordinal(), com.navdy.hud.app.ui.component.homescreen.HomeScreen.class);
        SCREEN_MAP.put(com.navdy.service.library.events.ui.Screen.SCREEN_WELCOME.ordinal(), com.navdy.hud.app.screen.WelcomeScreen.class);
        SCREEN_MAP.put(com.navdy.service.library.events.ui.Screen.SCREEN_MAIN_MENU.ordinal(), com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.class);
        SCREEN_MAP.put(com.navdy.service.library.events.ui.Screen.SCREEN_OTA_CONFIRMATION.ordinal(), com.navdy.hud.app.screen.OSUpdateConfirmationScreen.class);
        SCREEN_MAP.put(com.navdy.service.library.events.ui.Screen.SCREEN_SHUTDOWN_CONFIRMATION.ordinal(), com.navdy.hud.app.screen.ShutDownScreen.class);
        SCREEN_MAP.put(com.navdy.service.library.events.ui.Screen.SCREEN_AUTO_BRIGHTNESS.ordinal(), com.navdy.hud.app.screen.AutoBrightnessScreen.class);
        SCREEN_MAP.put(com.navdy.service.library.events.ui.Screen.SCREEN_FACTORY_RESET.ordinal(), com.navdy.hud.app.screen.FactoryResetScreen.class);
        SCREEN_MAP.put(com.navdy.service.library.events.ui.Screen.SCREEN_DIAL_PAIRING.ordinal(), com.navdy.hud.app.screen.DialManagerScreen.class);
        SCREEN_MAP.put(com.navdy.service.library.events.ui.Screen.SCREEN_DIAL_UPDATE_PROGRESS.ordinal(), com.navdy.hud.app.screen.DialUpdateProgressScreen.class);
        SCREEN_MAP.put(com.navdy.service.library.events.ui.Screen.SCREEN_TEMPERATURE_WARNING.ordinal(), com.navdy.hud.app.screen.TemperatureWarningScreen.class);
        SCREEN_MAP.put(com.navdy.service.library.events.ui.Screen.SCREEN_FORCE_UPDATE.ordinal(), com.navdy.hud.app.screen.ForceUpdateScreen.class);
        SCREEN_MAP.put(com.navdy.service.library.events.ui.Screen.SCREEN_DIAL_UPDATE_CONFIRMATION.ordinal(), com.navdy.hud.app.screen.DialUpdateConfirmationScreen.class);
        SCREEN_MAP.put(com.navdy.service.library.events.ui.Screen.SCREEN_GESTURE_LEARNING.ordinal(), com.navdy.hud.app.screen.GestureLearningScreen.class);
        SCREEN_MAP.put(com.navdy.service.library.events.ui.Screen.SCREEN_DESTINATION_PICKER.ordinal(), com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.class);
        SCREEN_MAP.put(com.navdy.service.library.events.ui.Screen.SCREEN_MUSIC_DETAILS.ordinal(), com.navdy.hud.app.framework.music.MusicDetailsScreen.class);
    }

    public Main() {
        mainScreen = this;
    }

    public java.lang.String getMortarScopeName() {
        return getClass().getName();
    }

    public java.lang.Object getDaggerModule() {
        return new com.navdy.hud.app.ui.activity.Main.Module();
    }

    public com.navdy.service.library.events.ui.Screen getScreen() {
        return null;
    }

    public int getAnimationIn(flow.Flow.Direction direction) {
        return -1;
    }

    public int getAnimationOut(flow.Flow.Direction direction) {
        return -1;
    }

    public com.navdy.hud.app.manager.InputManager.IInputHandler getInputHandler() {
        return presenter;
    }

    /* access modifiers changed from: private */
    public static void showBrightnessNotification() {
        if (com.navdy.hud.app.settings.HUDSettings.USE_ADAPTIVE_AUTOBRIGHTNESS) {
            sLogger.v("Use adaptive autobrightness");
            com.navdy.hud.app.framework.AdaptiveBrightnessNotification.showNotification();
            return;
        }
        sLogger.v("Use non-adaptive autobrightness");
        com.navdy.hud.app.framework.BrightnessNotification.showNotification();
    }

    public boolean isMainUIShrunk() {
        if (presenter == null) {
            return false;
        }
        return presenter.isMainUIShrunk();
    }

    public boolean isNotificationViewShowing() {
        if (presenter == null) {
            return false;
        }
        return presenter.isNotificationViewShowing();
    }

    public boolean isNotificationExpanding() {
        if (presenter == null) {
            return false;
        }
        return presenter.isNotificationExpanding();
    }

    public boolean isNotificationCollapsing() {
        if (presenter == null) {
            return false;
        }
        return presenter.isNotificationCollapsing();
    }

    public boolean isScreenAnimating() {
        if (presenter == null) {
            return false;
        }
        return presenter.isScreenAnimating();
    }

    public com.navdy.hud.app.view.NotificationView getNotificationView() {
        if (presenter == null) {
            return null;
        }
        return presenter.getNotificationView();
    }

    public android.widget.FrameLayout getExpandedNotificationView() {
        if (presenter == null) {
            return null;
        }
        return presenter.getExpandedNotificationView();
    }

    public android.view.View getExpandedNotificationCoverView() {
        if (presenter == null) {
            return null;
        }
        return presenter.getExpandedNotificationCoverView();
    }

    public android.view.ViewGroup getNotificationExtensionViewHolder() {
        if (presenter != null) {
            return presenter.getNotificationExtensionView();
        }
        return null;
    }

    public void addNotificationExtensionView(android.view.View view) {
        if (presenter != null) {
            presenter.addNotificationExtensionView(view);
        }
    }

    public void removeNotificationExtensionView() {
        if (presenter != null) {
            presenter.removeNotificationExtensionView();
        }
    }

    public void injectMainLowerView(android.view.View view) {
        if (presenter != null) {
            presenter.injectMainLowerView(view);
        }
    }

    public void ejectMainLowerView() {
        if (presenter != null) {
            presenter.ejectMainLowerView();
        }
    }

    public boolean isMainLowerViewVisible() {
        if (presenter != null) {
            return presenter.isMainLowerViewVisible();
        }
        return false;
    }

    public com.navdy.hud.app.ui.component.carousel.CarouselIndicator getNotificationIndicator() {
        if (presenter == null) {
            return null;
        }
        return presenter.getNotificationIndicator();
    }

    public com.navdy.hud.app.ui.component.carousel.ProgressIndicator getNotificationScrollIndicator() {
        if (presenter == null) {
            return null;
        }
        return presenter.getNotificationScrollIndicator();
    }

    public void setInputFocus() {
        if (presenter != null) {
            presenter.setInputFocus();
        }
    }

    public void clearInputFocus() {
        if (presenter != null) {
            presenter.clearInputFocus();
        }
    }

    public void handleKey(com.navdy.hud.app.manager.InputManager.CustomKeyEvent event) {
        if (presenter != null) {
            presenter.onKey(event);
        }
    }

    public void setSystemTrayVisibility(int visibility) {
        if (presenter != null) {
            presenter.setSystemTrayVisibility(visibility);
        }
    }

    public void enableSystemTray(boolean enabled) {
        if (presenter != null) {
            presenter.enableSystemTray(enabled);
        }
    }

    public void enableNotificationColor(boolean enabled) {
        if (presenter != null) {
            presenter.enableNotificationColor(enabled);
        }
    }

    public static void saveHomeScreenPreference(android.content.SharedPreferences globalPreferences, int ordinal) {
        boolean isDefaultProfile = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile().isDefaultProfile();
        android.content.SharedPreferences driverPreferences = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getDriverProfileManager().getLocalPreferencesForCurrentDriverProfile(com.navdy.hud.app.HudApplication.getAppContext());
        if (!isDefaultProfile && driverPreferences != null) {
            driverPreferences.edit().putInt(PREFERENCE_HOME_SCREEN_MODE, ordinal).apply();
        }
        if (globalPreferences != null) {
            globalPreferences.edit().putInt(PREFERENCE_HOME_SCREEN_MODE, ordinal).apply();
        }
    }

    public static void handleLibraryInitializationError(java.lang.String message) {
        sLogger.e("Forcing update due to initialization failure: " + message);
        mProtocolStatus = com.navdy.hud.app.ui.activity.Main.ProtocolStatus.PROTOCOL_LOCAL_NEEDS_UPDATE;
        if (presenter != null) {
            presenter.bus.post(new com.navdy.service.library.events.ui.ShowScreen.Builder().screen(com.navdy.service.library.events.ui.Screen.SCREEN_FORCE_UPDATE).build());
        } else {
            sLogger.e("unable to launch FORCE_UPDATE screen.");
        }
    }
}
