package com.navdy.hud.app.ui.activity;

public final class MainActivity$$InjectAdapter extends dagger.internal.Binding<com.navdy.hud.app.ui.activity.MainActivity> implements javax.inject.Provider<com.navdy.hud.app.ui.activity.MainActivity>, dagger.MembersInjector<com.navdy.hud.app.ui.activity.MainActivity> {
    private dagger.internal.Binding<com.navdy.hud.app.ui.activity.Main.Presenter> mainPresenter;
    private dagger.internal.Binding<com.navdy.hud.app.manager.PairingManager> pairingManager;
    private dagger.internal.Binding<com.navdy.hud.app.device.PowerManager> powerManager;
    private dagger.internal.Binding<com.navdy.hud.app.ui.activity.HudBaseActivity> supertype;

    public MainActivity$$InjectAdapter() {
        super("com.navdy.hud.app.ui.activity.MainActivity", "members/com.navdy.hud.app.ui.activity.MainActivity", false, com.navdy.hud.app.ui.activity.MainActivity.class);
    }

    public void attach(dagger.internal.Linker linker) {
        this.mainPresenter = linker.requestBinding("com.navdy.hud.app.ui.activity.Main$Presenter", com.navdy.hud.app.ui.activity.MainActivity.class, getClass().getClassLoader());
        this.pairingManager = linker.requestBinding("com.navdy.hud.app.manager.PairingManager", com.navdy.hud.app.ui.activity.MainActivity.class, getClass().getClassLoader());
        this.powerManager = linker.requestBinding("com.navdy.hud.app.device.PowerManager", com.navdy.hud.app.ui.activity.MainActivity.class, getClass().getClassLoader());
        dagger.internal.Linker linker2 = linker;
        this.supertype = linker2.requestBinding("members/com.navdy.hud.app.ui.activity.HudBaseActivity", com.navdy.hud.app.ui.activity.MainActivity.class, getClass().getClassLoader(), false, true);
    }

    public void getDependencies(java.util.Set<dagger.internal.Binding<?>> set, java.util.Set<dagger.internal.Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.mainPresenter);
        injectMembersBindings.add(this.pairingManager);
        injectMembersBindings.add(this.powerManager);
        injectMembersBindings.add(this.supertype);
    }

    public com.navdy.hud.app.ui.activity.MainActivity get() {
        com.navdy.hud.app.ui.activity.MainActivity result = new com.navdy.hud.app.ui.activity.MainActivity();
        injectMembers(result);
        return result;
    }

    public void injectMembers(com.navdy.hud.app.ui.activity.MainActivity object) {
        object.mainPresenter = (com.navdy.hud.app.ui.activity.Main.Presenter) this.mainPresenter.get();
        object.pairingManager = (com.navdy.hud.app.manager.PairingManager) this.pairingManager.get();
        object.powerManager = (com.navdy.hud.app.device.PowerManager) this.powerManager.get();
        this.supertype.injectMembers(object);
    }
}
