package com.navdy.hud.app.ui.activity;

public abstract class HudBaseActivity extends com.navdy.hud.app.common.BaseActivity {
    @javax.inject.Inject
    com.navdy.hud.app.manager.InputManager inputManager;

    public boolean onKeyUp(int keyCode, android.view.KeyEvent event) {
        boolean handled = false;
        if (this.inputManager != null) {
            handled = this.inputManager.onKeyUp(keyCode, event);
        }
        if (handled) {
            return true;
        }
        return super.onKeyUp(keyCode, event);
    }

    public boolean onKeyDown(int keyCode, android.view.KeyEvent event) {
        boolean handled = false;
        this.logger.v("keydown:" + keyCode);
        if (this.inputManager != null) {
            handled = this.inputManager.onKeyDown(keyCode, event);
        }
        if (handled) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public boolean onKeyLongPress(int keyCode, android.view.KeyEvent event) {
        boolean handled = false;
        if (this.inputManager != null) {
            handled = this.inputManager.onKeyLongPress(keyCode, event);
        }
        if (handled) {
            return true;
        }
        return super.onKeyLongPress(keyCode, event);
    }

    /* access modifiers changed from: protected */
    public void makeImmersive(boolean keepScreenOn) {
        android.view.Window window = getWindow();
        int flags = 4718592;
        if (keepScreenOn) {
            flags = 4718592 | 128;
        }
        window.addFlags(flags);
        window.getDecorView().setSystemUiVisibility(5894);
        this.logger.v("setting immersive mode");
    }
}
