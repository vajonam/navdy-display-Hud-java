package com.navdy.hud.app.ui.framework;

public class BasePresenter<V extends android.view.View> extends mortar.ViewPresenter<V> {
    protected boolean active;

    public void onLoad(android.os.Bundle savedInstanceState) {
        this.active = true;
        super.onLoad(savedInstanceState);
    }

    /* access modifiers changed from: protected */
    public void onUnload() {
    }

    public void dropView(V view) {
        android.view.View v = (android.view.View) getView();
        if (v != null) {
            if (view == v) {
                this.active = false;
                onUnload();
            }
            super.dropView(v);
        }
    }

    public boolean isActive() {
        return this.active;
    }
}
