package com.navdy.hud.app.ui.framework;

public interface INotificationAnimationListener {
    void onStart(java.lang.String str, com.navdy.hud.app.framework.notifications.NotificationType notificationType, com.navdy.hud.app.ui.framework.UIStateManager.Mode mode);

    void onStop(java.lang.String str, com.navdy.hud.app.framework.notifications.NotificationType notificationType, com.navdy.hud.app.ui.framework.UIStateManager.Mode mode);
}
