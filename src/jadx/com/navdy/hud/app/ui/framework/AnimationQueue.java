package com.navdy.hud.app.ui.framework;

public class AnimationQueue {
    /* access modifiers changed from: private */
    public boolean animationRunning;
    private java.util.Queue<android.animation.AnimatorSet> animatorQueue = new java.util.concurrent.LinkedBlockingQueue();
    private com.navdy.hud.app.ui.framework.DefaultAnimationListener defaultAnimationListener = new com.navdy.hud.app.ui.framework.AnimationQueue.Anon1();

    class Anon1 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
        Anon1() {
        }

        public void onAnimationEnd(android.animation.Animator animation) {
            com.navdy.hud.app.ui.framework.AnimationQueue.this.animationRunning = false;
            com.navdy.hud.app.ui.framework.AnimationQueue.this.runQueuedAnimation();
        }
    }

    public boolean isAnimationRunning() {
        return this.animationRunning;
    }

    public void startAnimation(android.animation.AnimatorSet animatorSet) {
        if (!this.animationRunning) {
            this.animationRunning = true;
            animatorSet.addListener(this.defaultAnimationListener);
            animatorSet.start();
            return;
        }
        this.animatorQueue.add(animatorSet);
    }

    /* access modifiers changed from: private */
    public void runQueuedAnimation() {
        if (this.animatorQueue.size() != 0) {
            startAnimation((android.animation.AnimatorSet) this.animatorQueue.remove());
        }
    }
}
