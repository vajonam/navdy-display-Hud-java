package com.navdy.hud.app.ui.framework;

public interface IScreenAnimationListener {
    void onStart(com.navdy.hud.app.screen.BaseScreen baseScreen, com.navdy.hud.app.screen.BaseScreen baseScreen2);

    void onStop(com.navdy.hud.app.screen.BaseScreen baseScreen, com.navdy.hud.app.screen.BaseScreen baseScreen2);
}
