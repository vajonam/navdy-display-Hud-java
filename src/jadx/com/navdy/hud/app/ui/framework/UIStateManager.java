package com.navdy.hud.app.ui.framework;

public class UIStateManager {
    private static final java.util.EnumSet<com.navdy.service.library.events.ui.Screen> FULLSCREEN_MODES = java.util.EnumSet.of(com.navdy.service.library.events.ui.Screen.SCREEN_FIRST_LAUNCH, com.navdy.service.library.events.ui.Screen.SCREEN_HOME, com.navdy.service.library.events.ui.Screen.SCREEN_WELCOME, com.navdy.service.library.events.ui.Screen.SCREEN_DIAL_PAIRING);
    private static final java.util.EnumSet<com.navdy.service.library.events.ui.Screen> MAIN_SCREENS_SET = java.util.EnumSet.of(com.navdy.service.library.events.ui.Screen.SCREEN_HOME);
    private static final java.util.EnumSet<com.navdy.service.library.events.ui.Screen> OVERLAY_MODES = java.util.EnumSet.of(com.navdy.service.library.events.ui.Screen.SCREEN_MENU, new com.navdy.service.library.events.ui.Screen[]{com.navdy.service.library.events.ui.Screen.SCREEN_MAIN_MENU, com.navdy.service.library.events.ui.Screen.SCREEN_RECENT_CALLS, com.navdy.service.library.events.ui.Screen.SCREEN_RECOMMENDED_PLACES, com.navdy.service.library.events.ui.Screen.SCREEN_FAVORITE_PLACES, com.navdy.service.library.events.ui.Screen.SCREEN_OTA_CONFIRMATION, com.navdy.service.library.events.ui.Screen.SCREEN_BRIGHTNESS, com.navdy.service.library.events.ui.Screen.SCREEN_FAVORITE_CONTACTS, com.navdy.service.library.events.ui.Screen.SCREEN_SHUTDOWN_CONFIRMATION, com.navdy.service.library.events.ui.Screen.SCREEN_SETTINGS, com.navdy.service.library.events.ui.Screen.SCREEN_AUTO_BRIGHTNESS, com.navdy.service.library.events.ui.Screen.SCREEN_FACTORY_RESET, com.navdy.service.library.events.ui.Screen.SCREEN_REPORT_ISSUE, com.navdy.service.library.events.ui.Screen.SCREEN_TOAST, com.navdy.service.library.events.ui.Screen.SCREEN_FORCE_UPDATE, com.navdy.service.library.events.ui.Screen.SCREEN_GESTURE_LEARNING, com.navdy.service.library.events.ui.Screen.SCREEN_DESTINATION_PICKER, com.navdy.service.library.events.ui.Screen.SCREEN_MUSIC_DETAILS});
    private static final java.util.EnumSet<com.navdy.service.library.events.ui.Screen> PAUSE_HOME_SCREEN_SET = java.util.EnumSet.of(com.navdy.service.library.events.ui.Screen.SCREEN_MENU, new com.navdy.service.library.events.ui.Screen[]{com.navdy.service.library.events.ui.Screen.SCREEN_RECENT_CALLS, com.navdy.service.library.events.ui.Screen.SCREEN_RECOMMENDED_PLACES, com.navdy.service.library.events.ui.Screen.SCREEN_FAVORITE_PLACES, com.navdy.service.library.events.ui.Screen.SCREEN_OTA_CONFIRMATION, com.navdy.service.library.events.ui.Screen.SCREEN_FAVORITE_CONTACTS, com.navdy.service.library.events.ui.Screen.SCREEN_SHUTDOWN_CONFIRMATION, com.navdy.service.library.events.ui.Screen.SCREEN_SETTINGS, com.navdy.service.library.events.ui.Screen.SCREEN_AUTO_BRIGHTNESS, com.navdy.service.library.events.ui.Screen.SCREEN_FACTORY_RESET, com.navdy.service.library.events.ui.Screen.SCREEN_REPORT_ISSUE, com.navdy.service.library.events.ui.Screen.SCREEN_FORCE_UPDATE, com.navdy.service.library.events.ui.Screen.SCREEN_GESTURE_LEARNING, com.navdy.service.library.events.ui.Screen.SCREEN_MUSIC_DETAILS});
    private static final java.util.EnumSet<com.navdy.service.library.events.ui.Screen> SIDE_PANEL_MODES = java.util.EnumSet.of(com.navdy.service.library.events.ui.Screen.SCREEN_NOTIFICATION);
    private volatile com.navdy.hud.app.ui.component.homescreen.SmartDashViewConstants.Type currentDashboardType;
    private com.navdy.hud.app.screen.BaseScreen currentScreen;
    private volatile com.navdy.service.library.events.ui.Screen currentViewMode;
    private com.navdy.service.library.events.ui.Screen defaultMainActiveScreen = com.navdy.service.library.events.ui.Screen.SCREEN_HOME;
    private com.navdy.hud.app.ui.component.homescreen.HomeScreenView homeScreenView;
    private com.navdy.hud.app.screen.BaseScreen mainActiveScreen;
    private int mainPanelWidth;
    private java.util.HashSet<com.navdy.hud.app.ui.framework.INotificationAnimationListener> notificationListeners = new java.util.HashSet<>();
    private com.navdy.hud.app.ui.activity.Main rootScreen;
    private java.util.HashSet<com.navdy.hud.app.ui.framework.IScreenAnimationListener> screenListeners = new java.util.HashSet<>();
    private int sidePanelWidth;
    private com.navdy.hud.app.view.ToastView toastView;

    public enum Mode {
        EXPAND,
        COLLAPSE
    }

    public static boolean isSidePanelMode(com.navdy.service.library.events.ui.Screen screen) {
        return SIDE_PANEL_MODES.contains(screen);
    }

    public static boolean isOverlayMode(com.navdy.service.library.events.ui.Screen screen) {
        return OVERLAY_MODES.contains(screen);
    }

    public static boolean isFullscreenMode(com.navdy.service.library.events.ui.Screen screen) {
        return FULLSCREEN_MODES.contains(screen);
    }

    public static boolean isPauseHomescreen(com.navdy.service.library.events.ui.Screen screen) {
        return PAUSE_HOME_SCREEN_SET.contains(screen);
    }

    public com.navdy.hud.app.ui.component.homescreen.NavigationView getNavigationView() {
        if (this.homeScreenView != null) {
            return this.homeScreenView.getNavigationView();
        }
        return null;
    }

    public com.navdy.hud.app.ui.component.homescreen.SmartDashView getSmartDashView() {
        if (this.homeScreenView != null) {
            return (com.navdy.hud.app.ui.component.homescreen.SmartDashView) this.homeScreenView.getSmartDashView();
        }
        return null;
    }

    public void setCurrentViewMode(com.navdy.service.library.events.ui.Screen screen) {
        this.currentViewMode = screen;
    }

    public com.navdy.service.library.events.ui.Screen getCurrentViewMode() {
        return this.currentViewMode;
    }

    public void setCurrentDashboardType(com.navdy.hud.app.ui.component.homescreen.SmartDashViewConstants.Type dashboardType) {
        this.currentDashboardType = dashboardType;
    }

    public com.navdy.hud.app.ui.component.homescreen.SmartDashViewConstants.Type getCurrentDashboardType() {
        return this.currentDashboardType;
    }

    public boolean isMainUIShrunk() {
        if (this.rootScreen != null) {
            return this.rootScreen.isMainUIShrunk();
        }
        return false;
    }

    public void setMainActiveScreen(com.navdy.hud.app.screen.BaseScreen screen) {
        if (isFullscreenMode(screen.getScreen())) {
            this.mainActiveScreen = screen;
            com.navdy.service.library.events.ui.Screen s = screen.getScreen();
            if (!(s == com.navdy.service.library.events.ui.Screen.SCREEN_WELCOME || s == com.navdy.service.library.events.ui.Screen.SCREEN_DIAL_PAIRING || s == com.navdy.service.library.events.ui.Screen.SCREEN_FIRST_LAUNCH)) {
                this.defaultMainActiveScreen = s;
            }
        } else {
            this.mainActiveScreen = null;
        }
        this.currentScreen = screen;
    }

    public void setHomescreenView(com.navdy.hud.app.ui.component.homescreen.HomeScreenView view) {
        this.homeScreenView = view;
    }

    public com.navdy.hud.app.ui.component.homescreen.HomeScreenView getHomescreenView() {
        return this.homeScreenView;
    }

    public com.navdy.hud.app.view.ToastView getToastView() {
        return this.toastView;
    }

    public void setToastView(com.navdy.hud.app.view.ToastView toastView2) {
        this.toastView = toastView2;
    }

    public com.navdy.hud.app.screen.BaseScreen getMainActiveScreen() {
        return this.mainActiveScreen;
    }

    public com.navdy.service.library.events.ui.Screen getDefaultMainActiveScreen() {
        return this.defaultMainActiveScreen;
    }

    public void setRootScreen(com.navdy.hud.app.ui.activity.Main rootScreen2) {
        this.rootScreen = rootScreen2;
    }

    public com.navdy.hud.app.ui.activity.Main getRootScreen() {
        return this.rootScreen;
    }

    public com.navdy.hud.app.screen.BaseScreen getCurrentScreen() {
        return this.currentScreen;
    }

    public void addScreenAnimationListener(com.navdy.hud.app.ui.framework.IScreenAnimationListener listener) {
        checkListener(listener);
        this.screenListeners.add(listener);
    }

    public void removeScreenAnimationListener(com.navdy.hud.app.ui.framework.IScreenAnimationListener listener) {
        if (listener != null) {
            this.screenListeners.remove(listener);
        }
    }

    public void addNotificationAnimationListener(com.navdy.hud.app.ui.framework.INotificationAnimationListener listener) {
        synchronized (this.notificationListeners) {
            checkListener(listener);
            this.notificationListeners.add(listener);
        }
    }

    public void removeNotificationAnimationListener(com.navdy.hud.app.ui.framework.INotificationAnimationListener listener) {
        synchronized (this.notificationListeners) {
            if (listener != null) {
                this.notificationListeners.remove(listener);
            }
        }
    }

    private void checkListener(java.lang.Object listener) {
        if (listener == null) {
            throw new java.lang.IllegalArgumentException();
        }
    }

    public void postScreenAnimationEvent(boolean start, com.navdy.hud.app.screen.BaseScreen in, com.navdy.hud.app.screen.BaseScreen out) {
        java.util.Iterator it = this.screenListeners.iterator();
        while (it.hasNext()) {
            com.navdy.hud.app.ui.framework.IScreenAnimationListener listener = (com.navdy.hud.app.ui.framework.IScreenAnimationListener) it.next();
            if (start) {
                listener.onStart(in, out);
            } else {
                listener.onStop(in, out);
            }
        }
    }

    public void postNotificationAnimationEvent(boolean start, java.lang.String id, com.navdy.hud.app.framework.notifications.NotificationType type, com.navdy.hud.app.ui.framework.UIStateManager.Mode mode) {
        synchronized (this.notificationListeners) {
            java.util.Iterator it = this.notificationListeners.iterator();
            while (it.hasNext()) {
                com.navdy.hud.app.ui.framework.INotificationAnimationListener listener = (com.navdy.hud.app.ui.framework.INotificationAnimationListener) it.next();
                if (start) {
                    listener.onStart(id, type, mode);
                } else {
                    listener.onStop(id, type, mode);
                }
            }
        }
    }

    public java.util.EnumSet<com.navdy.service.library.events.ui.Screen> getMainScreensSet() {
        return MAIN_SCREENS_SET;
    }

    public com.navdy.hud.app.view.MainView.CustomAnimationMode getCustomAnimateMode() {
        com.navdy.hud.app.view.MainView.CustomAnimationMode mode = com.navdy.hud.app.view.MainView.CustomAnimationMode.EXPAND;
        if (isMainUIShrunk() && (this.rootScreen.isNotificationViewShowing() || this.rootScreen.isNotificationExpanding())) {
            mode = com.navdy.hud.app.view.MainView.CustomAnimationMode.SHRINK_LEFT;
        }
        if (mode == null || this.homeScreenView == null || mode != com.navdy.hud.app.view.MainView.CustomAnimationMode.EXPAND || !this.homeScreenView.isModeVisible()) {
            return mode;
        }
        return com.navdy.hud.app.view.MainView.CustomAnimationMode.SHRINK_MODE;
    }

    public boolean isWelcomeScreenOn() {
        com.navdy.hud.app.screen.BaseScreen screen = getCurrentScreen();
        if (screen == null || screen.getScreen() != com.navdy.service.library.events.ui.Screen.SCREEN_WELCOME) {
            return false;
        }
        return true;
    }

    public boolean isDialPairingScreenOn() {
        com.navdy.hud.app.screen.BaseScreen screen = getCurrentScreen();
        if (screen == null || screen.getScreen() != com.navdy.service.library.events.ui.Screen.SCREEN_DIAL_PAIRING) {
            return false;
        }
        return true;
    }

    public boolean isDialPairingScreenScanning() {
        com.navdy.hud.app.screen.BaseScreen screen = getCurrentScreen();
        if (screen == null || screen.getScreen() != com.navdy.service.library.events.ui.Screen.SCREEN_DIAL_PAIRING) {
            return false;
        }
        return ((com.navdy.hud.app.screen.DialManagerScreen) screen).isScanningMode();
    }

    public int getMainPanelWidth() {
        return this.mainPanelWidth;
    }

    public void setMainPanelWidth(int n) {
        this.mainPanelWidth = n;
    }

    public int getSidePanelWidth() {
        return this.sidePanelWidth;
    }

    public void setSidePanelWidth(int n) {
        this.sidePanelWidth = n;
    }

    public void showSystemTray(int visibility) {
        if (this.rootScreen != null) {
            this.rootScreen.setSystemTrayVisibility(visibility);
        }
    }

    public void enableSystemTray(boolean enabled) {
        if (this.rootScreen != null) {
            this.rootScreen.enableSystemTray(enabled);
        }
    }

    public void setInputFocus() {
        if (this.rootScreen != null) {
            this.rootScreen.setInputFocus();
        }
    }

    public void enableNotificationColor(boolean enabled) {
        if (this.rootScreen != null) {
            this.rootScreen.enableNotificationColor(enabled);
        }
    }

    public boolean isNavigationActive() {
        com.navdy.hud.app.ui.component.homescreen.HomeScreenView homeScreenView2 = getHomescreenView();
        if (homeScreenView2 != null) {
            return homeScreenView2.isNavigationActive();
        }
        return false;
    }
}
