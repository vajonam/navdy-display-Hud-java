package com.navdy.hud.app.ui.component.mainmenu;

public class MainMenuView2 extends android.widget.RelativeLayout implements com.navdy.hud.app.manager.InputManager.IInputHandler {
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.mainmenu.MainMenuView2.class);
    private com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.Callback callback;
    @butterknife.InjectView(2131624312)
    public com.navdy.hud.app.ui.component.ConfirmationLayout confirmationLayout;
    @javax.inject.Inject
    public com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.Presenter presenter;
    @butterknife.InjectView(2131624311)
    android.view.View rightBackground;
    public android.view.View scrimCover;
    com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent vmenuComponent;

    class Anon1 implements com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.Callback {
        Anon1() {
        }

        public void select(com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState selection) {
            com.navdy.hud.app.ui.component.mainmenu.MainMenuView2.this.presenter.selectItem(selection);
        }

        public void onLoad() {
            com.navdy.hud.app.ui.component.mainmenu.MainMenuView2.sLogger.v("onLoad");
            com.navdy.hud.app.ui.component.mainmenu.MainMenuView2.this.presenter.resetSelectedItem();
        }

        public boolean isItemClickable(com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState selection) {
            return com.navdy.hud.app.ui.component.mainmenu.MainMenuView2.this.presenter.isItemClickable(selection);
        }

        public void onBindToView(com.navdy.hud.app.ui.component.vlist.VerticalList.Model model, android.view.View view, int pos, com.navdy.hud.app.ui.component.vlist.VerticalList.ModelState state) {
            com.navdy.hud.app.ui.component.mainmenu.IMenu menu = com.navdy.hud.app.ui.component.mainmenu.MainMenuView2.this.presenter.getCurrentMenu();
            if (menu != null) {
                menu.onBindToView(model, view, pos, state);
            }
        }

        public void onItemSelected(com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState selection) {
            com.navdy.hud.app.ui.component.mainmenu.IMenu menu = com.navdy.hud.app.ui.component.mainmenu.MainMenuView2.this.presenter.getCurrentMenu();
            if (menu != null) {
                menu.onItemSelected(selection);
            }
        }

        public void onScrollIdle() {
            com.navdy.hud.app.ui.component.mainmenu.IMenu menu = com.navdy.hud.app.ui.component.mainmenu.MainMenuView2.this.presenter.getCurrentMenu();
            if (menu != null) {
                menu.onScrollIdle();
            }
        }

        public void onFastScrollStart() {
            com.navdy.hud.app.ui.component.mainmenu.IMenu menu = com.navdy.hud.app.ui.component.mainmenu.MainMenuView2.this.presenter.getCurrentMenu();
            if (menu != null) {
                menu.onFastScrollStart();
            }
        }

        public void onFastScrollEnd() {
            com.navdy.hud.app.ui.component.mainmenu.IMenu menu = com.navdy.hud.app.ui.component.mainmenu.MainMenuView2.this.presenter.getCurrentMenu();
            if (menu != null) {
                menu.onFastScrollEnd();
            }
        }

        public void showToolTip() {
            com.navdy.hud.app.ui.component.mainmenu.IMenu menu = com.navdy.hud.app.ui.component.mainmenu.MainMenuView2.this.presenter.getCurrentMenu();
            if (menu != null) {
                menu.showToolTip();
            }
        }

        public void close() {
            com.navdy.hud.app.ui.component.mainmenu.MainMenuView2.this.presenter.close();
        }

        public boolean isClosed() {
            return com.navdy.hud.app.ui.component.mainmenu.MainMenuView2.this.presenter.isClosed();
        }
    }

    public MainMenuView2(android.content.Context context) {
        this(context, null);
    }

    public MainMenuView2(android.content.Context context, android.util.AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MainMenuView2(android.content.Context context, android.util.AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.callback = new com.navdy.hud.app.ui.component.mainmenu.MainMenuView2.Anon1();
        if (!isInEditMode()) {
            mortar.Mortar.inject(context, this);
        }
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
        butterknife.ButterKnife.inject((android.view.View) this);
        this.vmenuComponent = new com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent(this, this.callback, true);
        this.vmenuComponent.leftContainer.setAlpha(0.0f);
        this.vmenuComponent.rightContainer.setAlpha(0.0f);
        this.scrimCover = android.view.LayoutInflater.from(getContext()).inflate(com.navdy.hud.app.R.layout.screen_main_menu_2_scrim, null);
        this.scrimCover.setLayoutParams(new android.view.ViewGroup.MarginLayoutParams(-1, -1));
        this.vmenuComponent.rightContainer.addView(this.scrimCover);
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.presenter != null) {
            this.presenter.takeView(this);
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.vmenuComponent.clear();
        this.presenter.sendCloseEvent();
        this.vmenuComponent.verticalList.cancelLoadingAnimation(1);
        if (this.presenter != null) {
            this.presenter.dropView(this);
        }
    }

    public boolean onGesture(com.navdy.service.library.events.input.GestureEvent event) {
        if (this.confirmationLayout.getVisibility() == 0) {
            return false;
        }
        return this.vmenuComponent.handleGesture(event);
    }

    public boolean onKey(com.navdy.hud.app.manager.InputManager.CustomKeyEvent event) {
        if (this.confirmationLayout.getVisibility() == 0) {
            return this.confirmationLayout.handleKey(event);
        }
        return this.vmenuComponent.handleKey(event);
    }

    public com.navdy.hud.app.manager.InputManager.IInputHandler nextHandler() {
        return com.navdy.hud.app.manager.InputManager.nextContainingHandler(this);
    }

    /* access modifiers changed from: 0000 */
    public void performSelectionAnimation(java.lang.Runnable endAction, int delay) {
        this.vmenuComponent.performSelectionAnimation(endAction, delay);
    }
}
