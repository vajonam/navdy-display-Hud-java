package com.navdy.hud.app.ui.component.homescreen;

public class HomeScreenUtils {
    private static final float DENSITY = com.navdy.hud.app.HudApplication.getAppContext().getResources().getDisplayMetrics().density;

    static class Anon1 implements android.animation.ValueAnimator.AnimatorUpdateListener {
        final /* synthetic */ android.view.View val$view;

        Anon1(android.view.View view) {
            this.val$view = view;
        }

        public void onAnimationUpdate(android.animation.ValueAnimator animation) {
            int width = ((java.lang.Integer) animation.getAnimatedValue()).intValue();
            android.view.ViewGroup.MarginLayoutParams margins = (android.view.ViewGroup.MarginLayoutParams) this.val$view.getLayoutParams();
            if (width != margins.width) {
                margins.width = width;
                this.val$view.invalidate();
                this.val$view.requestLayout();
            }
        }
    }

    static class Anon2 implements android.animation.ValueAnimator.AnimatorUpdateListener {
        final /* synthetic */ android.widget.ProgressBar val$progressBar;

        Anon2(android.widget.ProgressBar progressBar) {
            this.val$progressBar = progressBar;
        }

        public void onAnimationUpdate(android.animation.ValueAnimator valueAnimator) {
            this.val$progressBar.setProgress(((java.lang.Integer) valueAnimator.getAnimatedValue()).intValue());
        }
    }

    static class Anon3 implements android.animation.ValueAnimator.AnimatorUpdateListener {
        final /* synthetic */ android.view.View val$view;

        Anon3(android.view.View view) {
            this.val$view = view;
        }

        public void onAnimationUpdate(android.animation.ValueAnimator animation) {
            com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.setWidth(this.val$view, ((java.lang.Integer) animation.getAnimatedValue()).intValue());
        }
    }

    static class Anon4 implements android.animation.ValueAnimator.AnimatorUpdateListener {
        final /* synthetic */ android.view.View val$view;

        Anon4(android.view.View view) {
            this.val$view = view;
        }

        public void onAnimationUpdate(android.animation.ValueAnimator animation) {
            com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.setViewLeftMargin(this.val$view, ((java.lang.Integer) animation.getAnimatedValue()).intValue());
        }
    }

    static class Anon5 implements android.animation.ValueAnimator.AnimatorUpdateListener {
        final /* synthetic */ android.view.View val$view;

        Anon5(android.view.View view) {
            this.val$view = view;
        }

        public void onAnimationUpdate(android.animation.ValueAnimator animation) {
            com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.setViewRightMargin(this.val$view, ((java.lang.Integer) animation.getAnimatedValue()).intValue());
        }
    }

    static class Anon6 implements android.animation.ValueAnimator.AnimatorUpdateListener {
        final /* synthetic */ android.view.View val$view;

        Anon6(android.view.View view) {
            this.val$view = view;
        }

        public void onAnimationUpdate(android.animation.ValueAnimator animation) {
            com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.setViewLeftMargin(this.val$view, ((java.lang.Integer) animation.getAnimatedValue()).intValue());
        }
    }

    static class Anon7 implements android.animation.ValueAnimator.AnimatorUpdateListener {
        final /* synthetic */ android.view.View val$view;

        Anon7(android.view.View view) {
            this.val$view = view;
        }

        public void onAnimationUpdate(android.animation.ValueAnimator animation) {
            com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.setViewRightMargin(this.val$view, ((java.lang.Integer) animation.getAnimatedValue()).intValue());
        }
    }

    public static android.animation.ObjectAnimator getXPositionAnimator(android.view.View view, float xPos) {
        return android.animation.ObjectAnimator.ofFloat(view, "x", new float[]{xPos});
    }

    public static android.animation.ObjectAnimator getYPositionAnimator(android.view.View view, float yPos) {
        return android.animation.ObjectAnimator.ofFloat(view, "y", new float[]{yPos});
    }

    public static android.animation.ObjectAnimator getTranslationXPositionAnimator(android.view.View view, int xPosBy) {
        return android.animation.ObjectAnimator.ofFloat(view, android.view.View.TRANSLATION_X, new float[]{(float) xPosBy});
    }

    public static android.animation.ObjectAnimator getTranslationYPositionAnimator(android.view.View view, int yPosBy) {
        return android.animation.ObjectAnimator.ofFloat(view, android.view.View.TRANSLATION_Y, new float[]{(float) yPosBy});
    }

    public static android.animation.ValueAnimator getWidthAnimator(android.view.View view, int targetWidth) {
        android.animation.ValueAnimator widthAnimator = android.animation.ValueAnimator.ofInt(new int[]{((android.view.ViewGroup.MarginLayoutParams) view.getLayoutParams()).width, targetWidth});
        widthAnimator.addUpdateListener(new com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.Anon1(view));
        return widthAnimator;
    }

    public static android.animation.ValueAnimator getProgressBarAnimator(android.widget.ProgressBar progressBar, int progress) {
        android.animation.ValueAnimator progressAnimator = android.animation.ValueAnimator.ofInt(new int[]{progressBar.getProgress(), progress});
        progressAnimator.addUpdateListener(new com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.Anon2(progressBar));
        return progressAnimator;
    }

    public static android.animation.ValueAnimator getWidthAnimator(android.view.View view, int startWidth, int targetWidth) {
        android.animation.ValueAnimator widthAnimator = android.animation.ValueAnimator.ofInt(new int[]{startWidth, targetWidth});
        widthAnimator.addUpdateListener(new com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.Anon3(view));
        return widthAnimator;
    }

    public static void setWidth(android.view.View view, int width) {
        ((android.view.ViewGroup.MarginLayoutParams) view.getLayoutParams()).width = width;
        view.invalidate();
        view.requestLayout();
    }

    public static android.animation.AnimatorSet getMarginAnimator(android.view.View view, int marginLeft, int marginRight) {
        android.view.ViewGroup.MarginLayoutParams margins = (android.view.ViewGroup.MarginLayoutParams) view.getLayoutParams();
        android.animation.ValueAnimator v1 = android.animation.ValueAnimator.ofInt(new int[]{margins.leftMargin, marginLeft});
        v1.addUpdateListener(new com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.Anon4(view));
        android.animation.ValueAnimator v2 = android.animation.ValueAnimator.ofInt(new int[]{margins.rightMargin, marginRight});
        v2.addUpdateListener(new com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.Anon5(view));
        android.animation.AnimatorSet set = new android.animation.AnimatorSet();
        set.playTogether(new android.animation.Animator[]{v1, v2});
        return set;
    }

    public static android.animation.Animator getLeftMarginAnimator(android.view.View view, int marginLeft) {
        android.animation.ValueAnimator valueAnimator = android.animation.ValueAnimator.ofInt(new int[]{((android.view.ViewGroup.MarginLayoutParams) view.getLayoutParams()).leftMargin, marginLeft});
        valueAnimator.addUpdateListener(new com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.Anon6(view));
        return valueAnimator;
    }

    public static android.animation.Animator getRightMarginAnimator(android.view.View view, int marginRight) {
        android.animation.ValueAnimator valueAnimator = android.animation.ValueAnimator.ofInt(new int[]{((android.view.ViewGroup.MarginLayoutParams) view.getLayoutParams()).rightMargin, marginRight});
        valueAnimator.addUpdateListener(new com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.Anon7(view));
        return valueAnimator;
    }

    public static void setViewLeftMargin(android.view.View view, int left) {
        android.view.ViewGroup.MarginLayoutParams marginLayoutParams = (android.view.ViewGroup.MarginLayoutParams) view.getLayoutParams();
        if (left != marginLayoutParams.leftMargin) {
            marginLayoutParams.leftMargin = left;
            view.setLayoutParams(marginLayoutParams);
        }
    }

    public static void setViewRightMargin(android.view.View view, int right) {
        android.view.ViewGroup.MarginLayoutParams marginLayoutParams = (android.view.ViewGroup.MarginLayoutParams) view.getLayoutParams();
        if (marginLayoutParams.rightMargin != right) {
            marginLayoutParams.rightMargin = right;
            view.setLayoutParams(marginLayoutParams);
        }
    }

    public static android.animation.ObjectAnimator getAlphaAnimator(android.view.View view, int alpha) {
        return android.animation.ObjectAnimator.ofFloat(view, "alpha", new float[]{(float) alpha});
    }

    public static android.animation.AnimatorSet getFadeInAndScaleUpAnimator(android.view.View view) {
        android.animation.ObjectAnimator fadeInAnim = android.animation.ObjectAnimator.ofFloat(view, "alpha", new float[]{0.0f, 1.0f});
        android.animation.ObjectAnimator scaleUpXAnim = android.animation.ObjectAnimator.ofFloat(view, "scaleX", new float[]{1.0f});
        android.animation.ObjectAnimator scaleUpYAnim = android.animation.ObjectAnimator.ofFloat(view, "scaleY", new float[]{1.0f});
        android.animation.AnimatorSet fadeInAndScaleUp = new android.animation.AnimatorSet();
        fadeInAndScaleUp.play(fadeInAnim).with(scaleUpXAnim).with(scaleUpYAnim);
        return fadeInAndScaleUp;
    }

    public static android.animation.AnimatorSet getFadeOutAndScaleDownAnimator(android.view.View view) {
        android.animation.ObjectAnimator fadeOutAnim = android.animation.ObjectAnimator.ofFloat(view, "alpha", new float[]{0.0f});
        android.animation.ObjectAnimator scaleDownXAnim = android.animation.ObjectAnimator.ofFloat(view, "scaleX", new float[]{0.9f});
        android.animation.ObjectAnimator scaleDownYAnim = android.animation.ObjectAnimator.ofFloat(view, "scaleY", new float[]{0.9f});
        android.animation.AnimatorSet fadeOutWithScaleDown = new android.animation.AnimatorSet();
        fadeOutWithScaleDown.play(fadeOutAnim).with(scaleDownXAnim).with(scaleDownYAnim);
        return fadeOutWithScaleDown;
    }
}
