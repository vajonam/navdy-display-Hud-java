package com.navdy.hud.app.ui.component;

public class CarouselTextView extends android.widget.TextView {
    private static final long CAROUSEL_ANIM_DURATION = 500;
    private static final long CAROUSEL_VIEW_INTERVAL = 5000;
    private java.lang.Runnable carouselChangeRunnable = new com.navdy.hud.app.ui.component.CarouselTextView.Anon1();
    /* access modifiers changed from: private */
    public int currentTextIndex;
    /* access modifiers changed from: private */
    public final android.os.Handler handler = new android.os.Handler(android.os.Looper.getMainLooper());
    /* access modifiers changed from: private */
    public java.util.List<java.lang.String> textList;
    private final float translationY = getResources().getDimension(com.navdy.hud.app.R.dimen.text_fade_anim_translationy);

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            if (com.navdy.hud.app.ui.component.CarouselTextView.this.textList != null) {
                int size = com.navdy.hud.app.ui.component.CarouselTextView.this.textList.size();
                if (size > 0) {
                    com.navdy.hud.app.ui.component.CarouselTextView.this.currentTextIndex = (com.navdy.hud.app.ui.component.CarouselTextView.this.currentTextIndex + 1) % size;
                    com.navdy.hud.app.ui.component.CarouselTextView.this.rotateWithAnimation((java.lang.String) com.navdy.hud.app.ui.component.CarouselTextView.this.textList.get(com.navdy.hud.app.ui.component.CarouselTextView.this.currentTextIndex));
                    com.navdy.hud.app.ui.component.CarouselTextView.this.handler.postDelayed(this, 5000);
                }
            }
        }
    }

    class Anon2 implements java.lang.Runnable {
        final /* synthetic */ java.lang.String val$text;

        Anon2(java.lang.String str) {
            this.val$text = str;
        }

        public void run() {
            com.navdy.hud.app.ui.component.CarouselTextView.this.setText(this.val$text);
            com.navdy.hud.app.ui.component.CarouselTextView.this.animate().translationY(0.0f).alpha(1.0f).setDuration(com.navdy.hud.app.ui.component.CarouselTextView.CAROUSEL_ANIM_DURATION);
        }
    }

    public CarouselTextView(android.content.Context context, android.util.AttributeSet attrs) {
        super(context, attrs);
    }

    public void setCarousel(int... resIds) {
        this.handler.removeCallbacks(this.carouselChangeRunnable);
        this.textList = new java.util.ArrayList();
        for (int resId : resIds) {
            this.textList.add(getResources().getString(resId));
        }
        this.currentTextIndex = -1;
        this.handler.post(this.carouselChangeRunnable);
    }

    /* access modifiers changed from: private */
    public void rotateWithAnimation(java.lang.String text) {
        animate().translationY(this.translationY).alpha(0.0f).setDuration(CAROUSEL_ANIM_DURATION).withEndAction(new com.navdy.hud.app.ui.component.CarouselTextView.Anon2(text));
    }

    public void clear() {
        animate().cancel();
        this.handler.removeCallbacks(this.carouselChangeRunnable);
        this.textList = null;
        this.currentTextIndex = -1;
    }
}
