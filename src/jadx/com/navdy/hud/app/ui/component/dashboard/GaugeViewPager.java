package com.navdy.hud.app.ui.component.dashboard;

public class GaugeViewPager extends android.widget.FrameLayout {
    public static final int FAST_SCROLL_ANIMATION_DURATION = 100;
    public static final int REGULAR_SCROLL_ANIMATION_DURATION = 200;
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.dashboard.GaugeViewPager.class);
    /* access modifiers changed from: private */
    public com.navdy.hud.app.ui.component.dashboard.GaugeViewPager.Operation currentOperation;
    /* access modifiers changed from: private */
    public volatile boolean isAnimationRunning;
    private com.navdy.hud.app.ui.component.dashboard.GaugeViewPager.Adapter mAdapter;
    private com.navdy.hud.app.ui.component.dashboard.GaugeViewPager.ChangeListener mChangeListener;
    private int mCurrentPosition;
    /* access modifiers changed from: private */
    public int mNextPosition;
    /* access modifiers changed from: private */
    public int mPreviousPosition;
    /* access modifiers changed from: private */
    public android.widget.LinearLayout mSlider;
    /* access modifiers changed from: private */
    public java.util.Queue<com.navdy.hud.app.ui.component.dashboard.GaugeViewPager.Operation> operationQueue;
    final android.animation.ValueAnimator positionAnimator;

    public interface Adapter {
        int getCount();

        int getExcludedPosition();

        com.navdy.hud.app.view.DashboardWidgetPresenter getPresenter(int i);

        android.view.View getView(int i, android.view.View view, android.view.ViewGroup viewGroup, boolean z);
    }

    class Anon1 implements android.animation.ValueAnimator.AnimatorUpdateListener {
        Anon1() {
        }

        public void onAnimationUpdate(android.animation.ValueAnimator valueAnimator) {
            android.view.ViewGroup.MarginLayoutParams marginParams = (android.view.ViewGroup.MarginLayoutParams) com.navdy.hud.app.ui.component.dashboard.GaugeViewPager.this.mSlider.getLayoutParams();
            marginParams.topMargin = ((java.lang.Integer) valueAnimator.getAnimatedValue()).intValue();
            com.navdy.hud.app.ui.component.dashboard.GaugeViewPager.this.mSlider.setLayoutParams(marginParams);
        }
    }

    class Anon2 implements android.animation.Animator.AnimatorListener {

        class Anon1 implements java.lang.Runnable {
            Anon1() {
            }

            public void run() {
                boolean z;
                com.navdy.hud.app.ui.component.dashboard.GaugeViewPager.this.setSelectedPosition(com.navdy.hud.app.ui.component.dashboard.GaugeViewPager.this.currentOperation == com.navdy.hud.app.ui.component.dashboard.GaugeViewPager.Operation.NEXT ? com.navdy.hud.app.ui.component.dashboard.GaugeViewPager.this.mNextPosition : com.navdy.hud.app.ui.component.dashboard.GaugeViewPager.this.mPreviousPosition);
                com.navdy.hud.app.ui.component.dashboard.GaugeViewPager.Operation nextOperationToBePerformed = com.navdy.hud.app.ui.component.dashboard.GaugeViewPager.this.operationQueue.size() > 0 ? (com.navdy.hud.app.ui.component.dashboard.GaugeViewPager.Operation) com.navdy.hud.app.ui.component.dashboard.GaugeViewPager.this.operationQueue.remove() : null;
                if (nextOperationToBePerformed != null) {
                    com.navdy.hud.app.ui.component.dashboard.GaugeViewPager gaugeViewPager = com.navdy.hud.app.ui.component.dashboard.GaugeViewPager.this;
                    if (com.navdy.hud.app.ui.component.dashboard.GaugeViewPager.this.operationQueue.size() > 0) {
                        z = true;
                    } else {
                        z = false;
                    }
                    gaugeViewPager.performOperation(nextOperationToBePerformed, z);
                    return;
                }
                com.navdy.hud.app.ui.component.dashboard.GaugeViewPager.this.isAnimationRunning = false;
            }
        }

        Anon2() {
        }

        public void onAnimationStart(android.animation.Animator animator) {
            com.navdy.hud.app.ui.component.dashboard.GaugeViewPager.this.isAnimationRunning = true;
        }

        public void onAnimationEnd(android.animation.Animator animator) {
            com.navdy.hud.app.ui.component.dashboard.GaugeViewPager.this.getHandler().post(new com.navdy.hud.app.ui.component.dashboard.GaugeViewPager.Anon2.Anon1());
        }

        public void onAnimationCancel(android.animation.Animator animator) {
        }

        public void onAnimationRepeat(android.animation.Animator animator) {
        }
    }

    public interface ChangeListener {
        void onGaugeChanged(int i);
    }

    enum Operation {
        NEXT,
        PREVIOUS
    }

    public GaugeViewPager(android.content.Context context) {
        this(context, null);
    }

    public GaugeViewPager(android.content.Context context, android.util.AttributeSet attrs) {
        this(context, attrs, -1);
    }

    public GaugeViewPager(android.content.Context context, android.util.AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mCurrentPosition = -1;
        this.mNextPosition = -1;
        this.mPreviousPosition = -1;
        this.operationQueue = new java.util.LinkedList();
        this.isAnimationRunning = false;
        this.positionAnimator = new android.animation.ValueAnimator();
        this.positionAnimator.addUpdateListener(new com.navdy.hud.app.ui.component.dashboard.GaugeViewPager.Anon1());
        this.positionAnimator.addListener(new com.navdy.hud.app.ui.component.dashboard.GaugeViewPager.Anon2());
    }

    public GaugeViewPager(android.content.Context context, android.util.AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.mCurrentPosition = -1;
        this.mNextPosition = -1;
        this.mPreviousPosition = -1;
        this.operationQueue = new java.util.LinkedList();
        this.isAnimationRunning = false;
        this.positionAnimator = new android.animation.ValueAnimator();
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
        removeAllViews();
        this.mSlider = new android.widget.LinearLayout(getContext());
        this.mSlider.setOrientation(1);
        this.mSlider.setLayoutParams(new android.view.ViewGroup.MarginLayoutParams(-1, -2));
        addView(this.mSlider);
        android.view.LayoutInflater.from(getContext()).inflate(com.navdy.hud.app.R.layout.gauge_view_scrims, this, true);
        for (int i = 0; i < 3; i++) {
            android.widget.FrameLayout frameLayout = new android.widget.FrameLayout(getContext());
            if (i == 0 || i == 1) {
                frameLayout.setLayoutParams(new android.view.ViewGroup.MarginLayoutParams(getResources().getDimensionPixelSize(com.navdy.hud.app.R.dimen.gauge_frame_width), getResources().getDimensionPixelSize(com.navdy.hud.app.R.dimen.gauge_frame_height) + getResources().getDimensionPixelSize(com.navdy.hud.app.R.dimen.gauge_transition_margin)));
            } else {
                frameLayout.setLayoutParams(new android.view.ViewGroup.MarginLayoutParams(getResources().getDimensionPixelSize(com.navdy.hud.app.R.dimen.gauge_frame_width), getResources().getDimensionPixelSize(com.navdy.hud.app.R.dimen.gauge_frame_height)));
            }
            this.mSlider.addView(frameLayout);
        }
        ((android.view.ViewGroup.MarginLayoutParams) this.mSlider.getLayoutParams()).topMargin = -getMaxTopMarginValue();
    }

    private int getMaxTopMarginValue() {
        return getResources().getDimensionPixelSize(com.navdy.hud.app.R.dimen.gauge_transition_margin) + getResources().getDimensionPixelSize(com.navdy.hud.app.R.dimen.gauge_frame_height);
    }

    public void setSelectedPosition(int position) {
        if (this.mAdapter == null) {
            throw new java.lang.IllegalStateException("Set the Adapter before setting the position");
        }
        int oldPosition = this.mCurrentPosition;
        if (position >= 0 && position < this.mAdapter.getCount()) {
            int nextPosition = ((this.mAdapter.getCount() + position) - 1) % this.mAdapter.getCount();
            int previousPosition = (position + 1) % this.mAdapter.getCount();
            if (nextPosition == this.mAdapter.getExcludedPosition()) {
                nextPosition = ((this.mAdapter.getCount() + nextPosition) - 1) % this.mAdapter.getCount();
            }
            if (previousPosition == this.mAdapter.getExcludedPosition()) {
                previousPosition = (previousPosition + 1) % this.mAdapter.getCount();
            }
            this.mNextPosition = nextPosition;
            this.mPreviousPosition = previousPosition;
            this.mCurrentPosition = position;
            populateView(0, this.mNextPosition);
            populateView(1, this.mCurrentPosition);
            populateView(2, this.mPreviousPosition);
            ((android.view.ViewGroup.MarginLayoutParams) this.mSlider.getLayoutParams()).topMargin = -getMaxTopMarginValue();
            if (this.mChangeListener != null && this.mCurrentPosition != oldPosition) {
                this.mChangeListener.onGaugeChanged(this.mCurrentPosition);
            }
        }
    }

    public void updateState() {
        setSelectedPosition(getSelectedPosition());
    }

    private void populateView(int localPosition, int adapterPosition) {
        boolean z = true;
        android.view.ViewGroup localView = (android.view.ViewGroup) this.mSlider.getChildAt(localPosition);
        android.view.View child = localView.getChildAt(0);
        com.navdy.hud.app.ui.component.dashboard.GaugeViewPager.Adapter adapter = this.mAdapter;
        if (localPosition != 1) {
            z = false;
        }
        android.view.View newView = adapter.getView(adapterPosition, child, localView, z);
        if (newView != child) {
            localView.removeAllViews();
            localView.addView(newView);
        }
    }

    public int getSelectedPosition() {
        return this.mCurrentPosition;
    }

    public void setAdapter(com.navdy.hud.app.ui.component.dashboard.GaugeViewPager.Adapter adapter) {
        this.mAdapter = adapter;
        setSelectedPosition(0);
    }

    public void moveNext() {
        if (this.isAnimationRunning) {
            if (this.operationQueue.peek() != com.navdy.hud.app.ui.component.dashboard.GaugeViewPager.Operation.NEXT) {
                this.operationQueue.clear();
            }
            this.operationQueue.add(com.navdy.hud.app.ui.component.dashboard.GaugeViewPager.Operation.NEXT);
            return;
        }
        performOperation(com.navdy.hud.app.ui.component.dashboard.GaugeViewPager.Operation.NEXT, false);
    }

    public void movePrevious() {
        if (this.isAnimationRunning) {
            if (this.operationQueue.peek() != com.navdy.hud.app.ui.component.dashboard.GaugeViewPager.Operation.PREVIOUS) {
                this.operationQueue.clear();
            }
            this.operationQueue.add(com.navdy.hud.app.ui.component.dashboard.GaugeViewPager.Operation.PREVIOUS);
            return;
        }
        performOperation(com.navdy.hud.app.ui.component.dashboard.GaugeViewPager.Operation.PREVIOUS, false);
    }

    /* access modifiers changed from: private */
    public void performOperation(com.navdy.hud.app.ui.component.dashboard.GaugeViewPager.Operation operation, boolean fastScroll) {
        this.currentOperation = operation;
        if (operation == com.navdy.hud.app.ui.component.dashboard.GaugeViewPager.Operation.NEXT) {
            updateWidgetView(this.mNextPosition);
            this.positionAnimator.setIntValues(new int[]{-getMaxTopMarginValue(), 0});
        } else if (operation == com.navdy.hud.app.ui.component.dashboard.GaugeViewPager.Operation.PREVIOUS) {
            updateWidgetView(this.mPreviousPosition);
            this.positionAnimator.setIntValues(new int[]{-getMaxTopMarginValue(), (-getMaxTopMarginValue()) * 2});
        }
        this.positionAnimator.setDuration(fastScroll ? 100 : 200);
        this.positionAnimator.start();
    }

    public void setChangeListener(com.navdy.hud.app.ui.component.dashboard.GaugeViewPager.ChangeListener changeListener) {
        this.mChangeListener = changeListener;
    }

    private void updateWidgetView(int position) {
        com.navdy.hud.app.view.DashboardWidgetPresenter presenter = this.mAdapter.getPresenter(position);
        if (presenter != null) {
            sLogger.v("widget:: update before animation");
            presenter.setWidgetVisibleToUser(true);
        }
    }
}
