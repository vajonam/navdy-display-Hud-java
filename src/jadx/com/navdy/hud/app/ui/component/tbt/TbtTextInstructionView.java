package com.navdy.hud.app.ui.component.tbt;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u0007\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u000e\u0018\u0000 82\u00020\u0001:\u000289B\u000f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004B\u0017\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007B\u001f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\b\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ(\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u00102\u0006\u0010\u001f\u001a\u00020 2\u0006\u0010\u001a\u001a\u00020\u001b2\u0006\u0010!\u001a\u00020\tH\u0002J\u0006\u0010\"\u001a\u00020\u001dJ\u0010\u0010#\u001a\u00020$Anon2\u0006\u0010\u001e\u001a\u00020\u0010H\u0002J\u001a\u0010%\u001a\u00020\u001d2\u0006\u0010&\u001a\u00020\f2\n\u0010'\u001a\u00060(R\u00020)J \u0010*\u001a\u00020\u001d2\u0006\u0010+\u001a\u00020,2\u0006\u0010-\u001a\u00020\u000e2\u0006\u0010.\u001a\u00020\u000eH\u0002J\u000e\u0010/\u001a\u00020\u001d2\u0006\u0010&\u001a\u00020\fJ\u0010\u00100\u001a\u00020\u001d2\u0006\u00101\u001a\u00020\tH\u0002J\u0018\u00102\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u00102\u0006\u0010.\u001a\u00020\u000eH\u0002J8\u00103\u001a\u00020\u000e2\u0006\u00104\u001a\u00020 2\u0006\u0010!\u001a\u00020\t2\u0006\u0010\u001e\u001a\u00020\u00102\u0006\u0010\u001a\u001a\u00020\u001b2\u0006\u00105\u001a\u00020\u000e2\u0006\u00106\u001a\u00020\u000eH\u0002J\"\u00107\u001a\u00020\u001d2\u0006\u0010+\u001a\u00020,2\b\b\u0002\u0010-\u001a\u00020\u000e2\b\b\u0002\u0010.\u001a\u00020\u000eR\u0010\u0010\u000b\u001a\u0004\u0018\u00010\fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000f\u001a\u0004\u0018\u00010\u0010X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0012\u001a\u0004\u0018\u00010\u0013X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0014\u001a\u0004\u0018\u00010\u0010X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0015\u001a\u0004\u0018\u00010\u0010X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0001X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u0017\u001a\u00060\u0018j\u0002`\u0019X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u001bX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006:"}, d2 = {"Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;", "Landroid/widget/TextView;", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "attrs", "Landroid/util/AttributeSet;", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "defStyleAttr", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "currentMode", "Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;", "instructionWidthIncludesNext", "", "lastInstruction", "", "lastInstructionWidth", "lastManeuverState", "Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;", "lastRoadText", "lastTurnText", "sizeCalculationTextView", "stringBuilder", "Ljava/lang/StringBuilder;", "Lkotlin/text/StringBuilder;", "widthInfo", "Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$WidthInfo;", "calculateTextWidth", "", "instruction", "textSize", "", "maxWidth", "clear", "getBoldText", "Landroid/text/SpannableStringBuilder;", "getCustomAnimator", "mode", "mainBuilder", "Landroid/animation/AnimatorSet$Builder;", "Landroid/animation/AnimatorSet;", "setInstruction", "event", "Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;", "adjustWidthForNextManeuver", "nextManeuverVisible", "setMode", "setViewWidth", "width", "setWidth", "tryTextSize", "fontSize", "check2Lines", "applyBold", "updateDisplay", "Companion", "WidthInfo", "app_hudDebug"}, k = 1, mv = {1, 1, 6})
/* compiled from: TbtTextInstructionView.kt */
public final class TbtTextInstructionView extends android.widget.TextView {
    public static final com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView.Companion Companion = new com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView.Companion(null);
    /* access modifiers changed from: private */
    public static final int fullWidth = Companion.getResources().getDimensionPixelSize(com.navdy.hud.app.R.dimen.tbt_instruction_full_w);
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger("TbtTextInstructionView");
    /* access modifiers changed from: private */
    public static final int mediumWidth = Companion.getResources().getDimensionPixelSize(com.navdy.hud.app.R.dimen.tbt_instruction_medium_w);
    /* access modifiers changed from: private */
    public static final android.content.res.Resources resources;
    /* access modifiers changed from: private */
    public static final int singleLineMinWidth = Companion.getResources().getDimensionPixelSize(com.navdy.hud.app.R.dimen.tbt_instruction_min_w);
    /* access modifiers changed from: private */
    public static final float size18 = Companion.getResources().getDimension(com.navdy.hud.app.R.dimen.tbt_instruction_18);
    /* access modifiers changed from: private */
    public static final float size20 = Companion.getResources().getDimension(com.navdy.hud.app.R.dimen.tbt_instruction_20);
    /* access modifiers changed from: private */
    public static final float size22 = Companion.getResources().getDimension(com.navdy.hud.app.R.dimen.tbt_instruction_22);
    /* access modifiers changed from: private */
    public static final float size24 = Companion.getResources().getDimension(com.navdy.hud.app.R.dimen.tbt_instruction_24);
    /* access modifiers changed from: private */
    public static final float size26 = Companion.getResources().getDimension(com.navdy.hud.app.R.dimen.tbt_instruction_26);
    /* access modifiers changed from: private */
    public static final int smallWidth = Companion.getResources().getDimensionPixelSize(com.navdy.hud.app.R.dimen.tbt_instruction_small_w);
    private java.util.HashMap _$_findViewCache;
    private com.navdy.hud.app.view.MainView.CustomAnimationMode currentMode;
    private boolean instructionWidthIncludesNext;
    private java.lang.String lastInstruction;
    private int lastInstructionWidth = -1;
    private com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.ManeuverState lastManeuverState;
    private java.lang.String lastRoadText;
    private java.lang.String lastTurnText;
    private final android.widget.TextView sizeCalculationTextView = new android.widget.TextView(com.navdy.hud.app.HudApplication.getAppContext());
    private final java.lang.StringBuilder stringBuilder = new java.lang.StringBuilder();
    private final com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView.WidthInfo widthInfo = new com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView.WidthInfo();

    @kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0007\n\u0002\b\r\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u0014\u0010\u0007\u001a\u00020\bX\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0014\u0010\u000b\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\f\u0010\u0006R\u0014\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0014\u0010\u0011\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0006R\u0014\u0010\u0013\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0016R\u0014\u0010\u0017\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0016R\u0014\u0010\u0019\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u0016R\u0014\u0010\u001b\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001c\u0010\u0016R\u0014\u0010\u001d\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001e\u0010\u0016R\u0014\u0010\u001f\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b \u0010\u0006\u00a8\u0006!"}, d2 = {"Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;", "", "()V", "fullWidth", "", "getFullWidth", "()I", "logger", "Lcom/navdy/service/library/log/Logger;", "getLogger", "()Lcom/navdy/service/library/log/Logger;", "mediumWidth", "getMediumWidth", "resources", "Landroid/content/res/Resources;", "getResources", "()Landroid/content/res/Resources;", "singleLineMinWidth", "getSingleLineMinWidth", "size18", "", "getSize18", "()F", "size20", "getSize20", "size22", "getSize22", "size24", "getSize24", "size26", "getSize26", "smallWidth", "getSmallWidth", "app_hudDebug"}, k = 1, mv = {1, 1, 6})
    /* compiled from: TbtTextInstructionView.kt */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(kotlin.jvm.internal.DefaultConstructorMarker $constructor_marker) {
            this();
        }

        private final com.navdy.service.library.log.Logger getLogger() {
            return com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView.logger;
        }

        /* access modifiers changed from: private */
        public final android.content.res.Resources getResources() {
            return com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView.resources;
        }

        /* access modifiers changed from: private */
        public final int getFullWidth() {
            return com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView.fullWidth;
        }

        /* access modifiers changed from: private */
        public final int getMediumWidth() {
            return com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView.mediumWidth;
        }

        /* access modifiers changed from: private */
        public final int getSmallWidth() {
            return com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView.smallWidth;
        }

        /* access modifiers changed from: private */
        public final int getSingleLineMinWidth() {
            return com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView.singleLineMinWidth;
        }

        /* access modifiers changed from: private */
        public final float getSize26() {
            return com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView.size26;
        }

        /* access modifiers changed from: private */
        public final float getSize24() {
            return com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView.size24;
        }

        /* access modifiers changed from: private */
        public final float getSize22() {
            return com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView.size22;
        }

        /* access modifiers changed from: private */
        public final float getSize20() {
            return com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView.size20;
        }

        /* access modifiers changed from: private */
        public final float getSize18() {
            return com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView.size18;
        }
    }

    @kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u000b\n\u0002\u0010\u0002\n\u0000\b\u0002\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u000f\u001a\u00020\u0010R\u001a\u0010\u0003\u001a\u00020\u0004X\u0080\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u001a\u0010\t\u001a\u00020\u0004X\u0080\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\n\u0010\u0006\"\u0004\b\u000b\u0010\bR\u001a\u0010\f\u001a\u00020\u0004X\u0080\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u0006\"\u0004\b\u000e\u0010\b\u00a8\u0006\u0011"}, d2 = {"Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$WidthInfo;", "", "()V", "lineWidth1", "", "getLineWidth1$app_hudDebug", "()I", "setLineWidth1$app_hudDebug", "(I)V", "lineWidth2", "getLineWidth2$app_hudDebug", "setLineWidth2$app_hudDebug", "numLines", "getNumLines$app_hudDebug", "setNumLines$app_hudDebug", "clear", "", "app_hudDebug"}, k = 1, mv = {1, 1, 6})
    /* compiled from: TbtTextInstructionView.kt */
    private static final class WidthInfo {
        private int lineWidth1;
        private int lineWidth2;
        private int numLines;

        public final int getNumLines$app_hudDebug() {
            return this.numLines;
        }

        public final void setNumLines$app_hudDebug(int i) {
            this.numLines = i;
        }

        public final int getLineWidth1$app_hudDebug() {
            return this.lineWidth1;
        }

        public final void setLineWidth1$app_hudDebug(int i) {
            this.lineWidth1 = i;
        }

        public final int getLineWidth2$app_hudDebug() {
            return this.lineWidth2;
        }

        public final void setLineWidth2$app_hudDebug(int i) {
            this.lineWidth2 = i;
        }

        public final void clear() {
            this.numLines = 0;
            this.lineWidth1 = 0;
            this.lineWidth2 = 0;
        }
    }

    public void _$_clearFindViewByIdCache() {
        if (this._$_findViewCache != null) {
            this._$_findViewCache.clear();
        }
    }

    public android.view.View _$_findCachedViewById(int i) {
        if (this._$_findViewCache == null) {
            this._$_findViewCache = new java.util.HashMap();
        }
        android.view.View view = (android.view.View) this._$_findViewCache.get(java.lang.Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        android.view.View findViewById = findViewById(i);
        this._$_findViewCache.put(java.lang.Integer.valueOf(i), findViewById);
        return findViewById;
    }

    static {
        android.content.res.Resources resources2 = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(resources2, "HudApplication.getAppContext().resources");
        resources = resources2;
    }

    public TbtTextInstructionView(@org.jetbrains.annotations.NotNull android.content.Context context) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(context, "context");
        super(context);
    }

    public TbtTextInstructionView(@org.jetbrains.annotations.NotNull android.content.Context context, @org.jetbrains.annotations.NotNull android.util.AttributeSet attrs) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(context, "context");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(attrs, "attrs");
        super(context, attrs);
    }

    public TbtTextInstructionView(@org.jetbrains.annotations.NotNull android.content.Context context, @org.jetbrains.annotations.NotNull android.util.AttributeSet attrs, int defStyleAttr) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(context, "context");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(attrs, "attrs");
        super(context, attrs, defStyleAttr);
    }

    public final void setMode(@org.jetbrains.annotations.NotNull com.navdy.hud.app.view.MainView.CustomAnimationMode mode) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(mode, net.hockeyapp.android.LoginActivity.EXTRA_MODE);
        if (!kotlin.jvm.internal.Intrinsics.areEqual((java.lang.Object) this.currentMode, (java.lang.Object) mode)) {
            if (this.lastInstructionWidth != -1) {
                boolean setWidth = false;
                android.view.ViewGroup.LayoutParams layoutParams = getLayoutParams();
                if (layoutParams == null) {
                    throw new kotlin.TypeCastException("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
                }
                android.view.ViewGroup.MarginLayoutParams lytParams = (android.view.ViewGroup.MarginLayoutParams) layoutParams;
                switch (mode) {
                    case EXPAND:
                        if (kotlin.jvm.internal.Intrinsics.areEqual((java.lang.Object) this.currentMode, (java.lang.Object) com.navdy.hud.app.view.MainView.CustomAnimationMode.SHRINK_LEFT) && lytParams.width == Companion.getMediumWidth()) {
                            setWidth = true;
                            break;
                        }
                    case SHRINK_LEFT:
                        if (kotlin.jvm.internal.Intrinsics.areEqual((java.lang.Object) this.currentMode, (java.lang.Object) com.navdy.hud.app.view.MainView.CustomAnimationMode.EXPAND) && this.lastInstructionWidth > Companion.getMediumWidth()) {
                            setWidth = true;
                            break;
                        }
                }
                if (setWidth) {
                    java.lang.String str = this.lastInstruction;
                    if (str != null) {
                        setWidth(str, true);
                    }
                }
            } else if (kotlin.jvm.internal.Intrinsics.areEqual((java.lang.Object) mode, (java.lang.Object) com.navdy.hud.app.view.MainView.CustomAnimationMode.EXPAND)) {
                setViewWidth(Companion.getFullWidth());
            } else {
                setViewWidth(Companion.getMediumWidth());
            }
            this.currentMode = mode;
        }
    }

    public final void getCustomAnimator(@org.jetbrains.annotations.NotNull com.navdy.hud.app.view.MainView.CustomAnimationMode mode, @org.jetbrains.annotations.NotNull android.animation.AnimatorSet.Builder mainBuilder) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(mode, net.hockeyapp.android.LoginActivity.EXTRA_MODE);
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(mainBuilder, "mainBuilder");
    }

    public final void clear() {
        setText("");
        this.lastTurnText = null;
        this.lastRoadText = null;
        this.lastInstruction = null;
        this.lastInstructionWidth = -1;
        this.instructionWidthIncludesNext = false;
    }

    public static /* bridge */ /* synthetic */ void updateDisplay$default(com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView tbtTextInstructionView, com.navdy.hud.app.maps.MapEvents.ManeuverDisplay maneuverDisplay, boolean z, boolean z2, int i, java.lang.Object obj) {
        if ((i & 2) != 0) {
            z = false;
        }
        if ((i & 4) != 0) {
            z2 = false;
        }
        tbtTextInstructionView.updateDisplay(maneuverDisplay, z, z2);
    }

    public final void updateDisplay(@org.jetbrains.annotations.NotNull com.navdy.hud.app.maps.MapEvents.ManeuverDisplay event, boolean adjustWidthForNextManeuver, boolean nextManeuverVisible) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(event, "event");
        setInstruction(event, adjustWidthForNextManeuver, nextManeuverVisible);
        this.lastManeuverState = event.maneuverState;
    }

    private final void setInstruction(com.navdy.hud.app.maps.MapEvents.ManeuverDisplay event, boolean adjustWidthForNextManeuver, boolean nextManeuverVisible) {
        if (!android.text.TextUtils.equals(event.pendingTurn, this.lastTurnText) || !android.text.TextUtils.equals(event.pendingRoad, this.lastRoadText) || adjustWidthForNextManeuver) {
            this.lastTurnText = event.pendingTurn;
            this.lastRoadText = event.pendingRoad;
            this.stringBuilder.setLength(0);
            if (this.lastTurnText != null && com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.canShowTurnText(this.lastTurnText)) {
                this.stringBuilder.append(this.lastTurnText);
                this.stringBuilder.append(" ");
            }
            if (this.lastRoadText != null) {
                this.stringBuilder.append(this.lastRoadText);
            }
            java.lang.String str = this.stringBuilder.toString();
            this.lastInstruction = str;
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(str, "str");
            setWidth(str, nextManeuverVisible);
        }
    }

    private final void setWidth(java.lang.String instruction, boolean nextManeuverVisible) {
        this.lastInstructionWidth = -1;
        int maxWidth = 0;
        com.navdy.hud.app.view.MainView.CustomAnimationMode customAnimationMode = this.currentMode;
        if (customAnimationMode != null) {
            switch (customAnimationMode) {
                case EXPAND:
                    if (!nextManeuverVisible) {
                        maxWidth = Companion.getFullWidth();
                        break;
                    } else {
                        maxWidth = Companion.getMediumWidth();
                        break;
                    }
                case SHRINK_LEFT:
                    maxWidth = Companion.getSmallWidth();
                    break;
            }
        }
        if (!tryTextSize(Companion.getSize26(), maxWidth, instruction, this.widthInfo, false, false)) {
            if (!tryTextSize(Companion.getSize24(), maxWidth, instruction, this.widthInfo, true, false)) {
                if (!tryTextSize(Companion.getSize22(), maxWidth, instruction, this.widthInfo, true, false)) {
                    if (!tryTextSize(Companion.getSize20(), maxWidth, instruction, this.widthInfo, true, true)) {
                        setTextSize(Companion.getSize18());
                        setText(getBoldText(instruction));
                        setViewWidth(maxWidth);
                        this.lastInstructionWidth = maxWidth;
                    }
                }
            }
        }
    }

    private final void calculateTextWidth(java.lang.String instruction, float textSize, com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView.WidthInfo widthInfo2, int maxWidth) {
        widthInfo2.clear();
        this.sizeCalculationTextView.setTextSize(textSize);
        android.text.StaticLayout layout = new android.text.StaticLayout(instruction, this.sizeCalculationTextView.getPaint(), maxWidth, android.text.Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);
        widthInfo2.setNumLines$app_hudDebug(layout.getLineCount());
        widthInfo2.setLineWidth1$app_hudDebug((int) layout.getLineMax(0));
        if (widthInfo2.getNumLines$app_hudDebug() > 1) {
            widthInfo2.setLineWidth2$app_hudDebug((int) layout.getLineMax(1));
        }
    }

    private final void setViewWidth(int width) {
        android.view.ViewGroup.LayoutParams layoutParams = getLayoutParams();
        if (layoutParams == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
        }
        ((android.view.ViewGroup.MarginLayoutParams) layoutParams).width = width;
        requestLayout();
    }

    private final android.text.SpannableStringBuilder getBoldText(java.lang.String instruction) {
        android.text.SpannableStringBuilder spannableStringBuilder = new android.text.SpannableStringBuilder();
        spannableStringBuilder.append(instruction);
        spannableStringBuilder.setSpan(new android.text.style.StyleSpan(1), 0, spannableStringBuilder.length(), 33);
        return spannableStringBuilder;
    }

    private final boolean tryTextSize(float fontSize, int maxWidth, java.lang.String instruction, com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView.WidthInfo widthInfo2, boolean check2Lines, boolean applyBold) {
        calculateTextWidth(instruction, fontSize, widthInfo2, maxWidth);
        if (widthInfo2.getNumLines$app_hudDebug() == 1) {
            setTextSize(fontSize);
            setText(!applyBold ? instruction : getBoldText(instruction));
            int newWidth = widthInfo2.getLineWidth1$app_hudDebug();
            if (newWidth < Companion.getSingleLineMinWidth()) {
                newWidth = Companion.getSingleLineMinWidth();
            }
            setViewWidth(newWidth);
            this.lastInstructionWidth = newWidth;
            return true;
        } else if (!check2Lines || widthInfo2.getNumLines$app_hudDebug() != 2) {
            return false;
        } else {
            setTextSize(fontSize);
            setText(!applyBold ? instruction : getBoldText(instruction));
            setViewWidth(maxWidth);
            this.lastInstructionWidth = maxWidth;
            return true;
        }
    }
}
