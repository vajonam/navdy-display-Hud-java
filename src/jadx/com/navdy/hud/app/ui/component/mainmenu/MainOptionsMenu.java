package com.navdy.hud.app.ui.component.mainmenu;

public class MainOptionsMenu implements com.navdy.hud.app.ui.component.mainmenu.IMenu {
    private static final java.lang.String always = resources.getString(com.navdy.hud.app.R.string.always);
    private static final com.navdy.hud.app.ui.component.vlist.VerticalList.Model back;
    private static final int dashColor = resources.getColor(com.navdy.hud.app.R.color.mm_dash_options);
    private static final java.lang.String dashTitle = resources.getString(com.navdy.hud.app.R.string.carousel_menu_smartdash_options);
    private static final int mapColor = resources.getColor(com.navdy.hud.app.R.color.mm_map_options);
    private static final java.lang.String mapTitle = resources.getString(com.navdy.hud.app.R.string.carousel_menu_map_options);
    private static final java.lang.String never = resources.getString(com.navdy.hud.app.R.string.never);
    private static final java.lang.String offLabel = resources.getString(com.navdy.hud.app.R.string.off);
    private static final java.lang.String onLabel = resources.getString(com.navdy.hud.app.R.string.on);
    private static final java.lang.String onlyWhenSpeeding = resources.getString(com.navdy.hud.app.R.string.only_when_speeding);
    private static final com.navdy.hud.app.ui.component.vlist.VerticalList.Model pauseDemo;
    private static final com.navdy.hud.app.ui.component.vlist.VerticalList.Model playDemo;
    private static final com.navdy.hud.app.ui.component.vlist.VerticalList.Model rawGps;
    private static final android.content.res.Resources resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
    private static final com.navdy.hud.app.ui.component.vlist.VerticalList.Model restartDemo;
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.mainmenu.MainOptionsMenu.class);
    private static final com.navdy.hud.app.ui.component.vlist.VerticalList.Model scrollLeftGauge;
    private static final com.navdy.hud.app.ui.component.vlist.VerticalList.Model scrollRightGauge;
    private static final com.navdy.hud.app.ui.component.vlist.VerticalList.Model selectCenterGauge;
    private static final com.navdy.hud.app.ui.component.vlist.VerticalList.Model showSpeedLimit;
    private static final com.navdy.hud.app.ui.component.vlist.VerticalList.Model sideGauges;
    private static final int switchesFluctuatorColor = resources.getColor(com.navdy.hud.app.R.color.mm_options_side_gauges_halo);
    private final com.navdy.hud.app.ui.component.vlist.VerticalList.Model autoZoomSwitch;
    private int backSelection;
    private com.squareup.otto.Bus bus;
    private java.util.List<com.navdy.hud.app.ui.component.vlist.VerticalList.Model> cachedList;
    private com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu dashGaugeConfiguratorMenu;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.debug.DriveRecorder driveRecorder;
    private com.navdy.hud.app.profile.DriverProfileManager driverProfileManager;
    private com.navdy.hud.app.ui.component.homescreen.HomeScreenView homeScreenView;
    private java.util.List<com.navdy.hud.app.ui.component.vlist.VerticalList.Model> homeScreenWidgetSwitches = new java.util.ArrayList();
    private com.navdy.hud.app.ui.component.mainmenu.MainOptionsMenu.Mode mode;
    private com.navdy.hud.app.ui.component.mainmenu.IMenu parent;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.Presenter presenter;
    private com.navdy.hud.app.ui.component.mainmenu.SpeedLimitSettingMenu speedLimitSettingMenu;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.ui.framework.UIStateManager uiStateManager;
    private com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent vscrollComponent;

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            com.navdy.hud.app.ui.component.mainmenu.MainOptionsMenu.this.presenter.close();
            if (com.navdy.hud.app.ui.component.mainmenu.MainOptionsMenu.this.driveRecorder.isDemoPaused()) {
                com.navdy.hud.app.ui.component.mainmenu.MainOptionsMenu.this.driveRecorder.performDemoPlaybackAction(com.navdy.hud.app.debug.DriveRecorder.Action.RESUME, true);
            } else {
                com.navdy.hud.app.ui.component.mainmenu.MainOptionsMenu.this.driveRecorder.performDemoPlaybackAction(com.navdy.hud.app.debug.DriveRecorder.Action.PLAY, true);
            }
        }
    }

    class Anon2 implements java.lang.Runnable {
        Anon2() {
        }

        public void run() {
            com.navdy.hud.app.ui.component.mainmenu.MainOptionsMenu.this.presenter.close();
            com.navdy.hud.app.ui.component.mainmenu.MainOptionsMenu.this.driveRecorder.performDemoPlaybackAction(com.navdy.hud.app.debug.DriveRecorder.Action.PAUSE, true);
        }
    }

    class Anon3 implements java.lang.Runnable {
        Anon3() {
        }

        public void run() {
            com.navdy.hud.app.ui.component.mainmenu.MainOptionsMenu.this.presenter.close();
            com.navdy.hud.app.ui.component.mainmenu.MainOptionsMenu.this.driveRecorder.performDemoPlaybackAction(com.navdy.hud.app.debug.DriveRecorder.Action.RESTART, true);
        }
    }

    class Anon4 implements java.lang.Runnable {
        Anon4() {
        }

        public void run() {
            com.navdy.hud.app.ui.component.mainmenu.MainOptionsMenu.this.presenter.close();
            com.navdy.hud.app.ui.component.homescreen.SmartDashView smartDashView = com.navdy.hud.app.ui.component.mainmenu.MainOptionsMenu.this.uiStateManager.getSmartDashView();
            if (smartDashView != null) {
                smartDashView.onScrollableSideOptionSelected();
            }
        }
    }

    enum Mode {
        MAP,
        DASH
    }

    static {
        int backColor = resources.getColor(com.navdy.hud.app.R.color.mm_back);
        back = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.menu_back, com.navdy.hud.app.R.drawable.icon_mm_back, backColor, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, backColor, resources.getString(com.navdy.hud.app.R.string.back), null);
        java.lang.String title = resources.getString(com.navdy.hud.app.R.string.show_raw_gps);
        int fluctuatorColor = resources.getColor(com.navdy.hud.app.R.color.mm_options_manual_zoom);
        boolean isOn = com.navdy.hud.app.device.gps.GpsUtils.SHOW_RAW_GPS.isEnabled();
        rawGps = com.navdy.hud.app.ui.component.vlist.viewholder.SwitchViewHolder.Companion.buildModel(com.navdy.hud.app.R.id.main_menu_options_raw_gps, fluctuatorColor, title, resources.getString(isOn ? com.navdy.hud.app.R.string.si_enabled : com.navdy.hud.app.R.string.si_disabled), isOn, true);
        java.lang.String title2 = resources.getString(com.navdy.hud.app.R.string.play_demo);
        int fluctuatorColor2 = resources.getColor(com.navdy.hud.app.R.color.mm_options_demo);
        playDemo = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.main_menu_options_play_demo, com.navdy.hud.app.R.drawable.icon_mm_demo_play_2, fluctuatorColor2, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, fluctuatorColor2, title2, null);
        java.lang.String title3 = resources.getString(com.navdy.hud.app.R.string.pause_demo);
        int fluctuatorColor3 = resources.getColor(com.navdy.hud.app.R.color.mm_options_demo);
        pauseDemo = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.main_menu_options_pause_demo, com.navdy.hud.app.R.drawable.icon_mm_demo_pause_2, fluctuatorColor3, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, fluctuatorColor3, title3, null);
        java.lang.String title4 = resources.getString(com.navdy.hud.app.R.string.restart_demo);
        int fluctuatorColor4 = resources.getColor(com.navdy.hud.app.R.color.mm_options_demo);
        restartDemo = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.main_menu_options_restart_demo, com.navdy.hud.app.R.drawable.icon_mm_demo_restart_2, fluctuatorColor4, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, fluctuatorColor4, title4, null);
        java.lang.String title5 = resources.getString(com.navdy.hud.app.R.string.scroll_left_gauge);
        int fluctuatorColor5 = resources.getColor(com.navdy.hud.app.R.color.mm_options_scroll_gauge);
        scrollLeftGauge = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.main_menu_options_scroll_left, com.navdy.hud.app.R.drawable.icon_options_dash_scroll_left_2, fluctuatorColor5, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, fluctuatorColor5, title5, null);
        java.lang.String title6 = resources.getString(com.navdy.hud.app.R.string.scroll_right_gauge);
        int fluctuatorColor6 = resources.getColor(com.navdy.hud.app.R.color.mm_options_scroll_gauge);
        scrollRightGauge = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.main_menu_options_scroll_right, com.navdy.hud.app.R.drawable.icon_options_dash_scroll_right_2, fluctuatorColor6, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, fluctuatorColor6, title6, null);
        java.lang.String title7 = resources.getString(com.navdy.hud.app.R.string.carousel_menu_smartdash_select_center_gauge);
        int fluctuatorColor7 = resources.getColor(com.navdy.hud.app.R.color.mm_options_scroll_gauge);
        selectCenterGauge = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.main_menu_options_select_center_gauge, com.navdy.hud.app.R.drawable.icon_center_gauge, fluctuatorColor7, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, fluctuatorColor7, title7, null);
        java.lang.String title8 = resources.getString(com.navdy.hud.app.R.string.carousel_menu_smartdash_side_gauges);
        int fluctuatorColor8 = resources.getColor(com.navdy.hud.app.R.color.mm_options_scroll_gauge);
        sideGauges = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.main_menu_options_side_gauges, com.navdy.hud.app.R.drawable.icon_side_gauges, fluctuatorColor8, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, fluctuatorColor8, title8, null);
        java.lang.String title9 = resources.getString(com.navdy.hud.app.R.string.show_speed_limit);
        int fluctuatorColor9 = resources.getColor(com.navdy.hud.app.R.color.mm_map_options);
        showSpeedLimit = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.mm_show_speed_limit, com.navdy.hud.app.R.drawable.icon_side_gauges, fluctuatorColor9, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, fluctuatorColor9, title9, null);
    }

    MainOptionsMenu(com.squareup.otto.Bus bus2, com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent vscrollComponent2, com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.Presenter presenter2, com.navdy.hud.app.ui.component.mainmenu.IMenu parent2) {
        this.bus = bus2;
        this.vscrollComponent = vscrollComponent2;
        this.presenter = presenter2;
        this.parent = parent2;
        com.navdy.hud.app.manager.RemoteDeviceManager remoteDeviceManager = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance();
        this.driveRecorder = remoteDeviceManager.getDriveRecorder();
        this.driverProfileManager = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getDriverProfileManager();
        this.uiStateManager = remoteDeviceManager.getUiStateManager();
        this.dashGaugeConfiguratorMenu = new com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu(bus2, remoteDeviceManager.getSharedPreferences(), this.vscrollComponent, this.presenter, this);
        this.speedLimitSettingMenu = new com.navdy.hud.app.ui.component.mainmenu.SpeedLimitSettingMenu(this.uiStateManager.getHomescreenView(), this.vscrollComponent, this.presenter, this);
        java.lang.String title = resources.getString(com.navdy.hud.app.R.string.auto_zoom);
        com.navdy.service.library.events.preferences.LocalPreferences localPreferences = this.driverProfileManager.getLocalPreferences();
        boolean isAutoZoomOn = localPreferences.manualZoom == null || !localPreferences.manualZoom.booleanValue();
        this.autoZoomSwitch = com.navdy.hud.app.ui.component.vlist.viewholder.SwitchViewHolder.Companion.buildModel(com.navdy.hud.app.R.id.main_menu_options_auto_zoom, switchesFluctuatorColor, title, isAutoZoomOn ? onLabel : offLabel, isAutoZoomOn, true);
        this.homeScreenView = this.uiStateManager.getHomescreenView();
        for (com.navdy.hud.app.ui.component.homescreen.HomeScreenView.HomeScreenWidgetInfo homeScreenWidgetInfo : this.homeScreenView.getHomeScreenWidgetInfoList()) {
            java.lang.String widgetSwitchTitle = resources.getString(homeScreenWidgetInfo.labelResourceId);
            boolean isWidgetEnabled = this.homeScreenView.getIsWidgetEnabled(homeScreenWidgetInfo.id);
            this.homeScreenWidgetSwitches.add(com.navdy.hud.app.ui.component.vlist.viewholder.SwitchViewHolder.Companion.buildModel(homeScreenWidgetInfo.id, switchesFluctuatorColor, widgetSwitchTitle, isWidgetEnabled ? onLabel : offLabel, isWidgetEnabled, true));
        }
    }

    /* access modifiers changed from: 0000 */
    public void setMode(com.navdy.hud.app.ui.component.mainmenu.MainOptionsMenu.Mode mode2) {
        this.mode = mode2;
    }

    public java.util.List<com.navdy.hud.app.ui.component.vlist.VerticalList.Model> getItems() {
        java.util.List<com.navdy.hud.app.ui.component.vlist.VerticalList.Model> list = new java.util.ArrayList<>();
        list.add(back);
        switch (this.mode) {
            case MAP:
                if (com.navdy.hud.app.maps.here.HereMapsManager.getInstance().isInitialized()) {
                    list.add(this.autoZoomSwitch);
                    if (!com.navdy.hud.app.util.DeviceUtil.isUserBuild()) {
                        list.add(rawGps);
                    }
                }
                list.addAll(this.homeScreenWidgetSwitches);
                if (!this.homeScreenView.getIsWidgetEnabled(com.navdy.hud.app.R.id.home_screen_speed_limit_sign)) {
                    showSpeedLimit.subTitle = never;
                } else {
                    boolean isAlwaysShowSpeedLimitSign = this.homeScreenView.isAlwaysShowSpeedLimitSign();
                    showSpeedLimit.subTitle = isAlwaysShowSpeedLimitSign ? always : onlyWhenSpeeding;
                }
                list.add(showSpeedLimit);
                boolean isDemoAvailable = this.driveRecorder.isDemoAvailable();
                boolean isPlaying = this.driveRecorder.isDemoPlaying();
                if (isDemoAvailable) {
                    if (isPlaying) {
                        list.add(pauseDemo);
                        list.add(restartDemo);
                        break;
                    } else {
                        list.add(playDemo);
                        break;
                    }
                }
                break;
            case DASH:
                com.navdy.hud.app.ui.component.homescreen.SmartDashView smartDashView = this.uiStateManager.getSmartDashView();
                if (smartDashView != null) {
                    switch (smartDashView.getCurrentScrollableSideOption()) {
                        case 0:
                            list.add(scrollLeftGauge);
                            break;
                        case 1:
                            list.add(scrollRightGauge);
                            break;
                    }
                }
                list.add(selectCenterGauge);
                list.add(sideGauges);
                break;
        }
        this.cachedList = list;
        return list;
    }

    public int getInitialSelection() {
        return 1;
    }

    public com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex getScrollIndex() {
        return null;
    }

    public void setBackSelectionPos(int n) {
        this.backSelection = n;
    }

    public void setBackSelectionId(int id) {
    }

    public void setSelectedIcon() {
        int icon = 0;
        int color = 0;
        java.lang.String title = null;
        switch (this.mode) {
            case MAP:
                icon = com.navdy.hud.app.R.drawable.icon_main_menu_map_options;
                color = mapColor;
                title = mapTitle;
                break;
            case DASH:
                icon = com.navdy.hud.app.R.drawable.icon_main_menu_dash_options;
                color = dashColor;
                title = dashTitle;
                break;
        }
        this.vscrollComponent.setSelectedIconColorImage(icon, color, null, 1.0f);
        this.vscrollComponent.selectedText.setText(title);
    }

    public boolean selectItem(com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState selection) {
        boolean z;
        boolean enabled = true;
        sLogger.v("select id:" + selection.id + " pos:" + selection.pos);
        com.navdy.hud.app.ui.component.vlist.VerticalList.Model model = selection.model;
        switch (selection.id) {
            case com.navdy.hud.app.R.id.main_menu_options_auto_zoom /*2131623977*/:
                if (!model.isOn) {
                    z = true;
                } else {
                    z = false;
                }
                model.isOn = z;
                model.subTitle = model.isOn ? onLabel : offLabel;
                com.navdy.service.library.events.preferences.LocalPreferences.Builder builder = new com.navdy.service.library.events.preferences.LocalPreferences.Builder(this.driverProfileManager.getLocalPreferences());
                if (model.isOn) {
                    enabled = false;
                }
                this.driverProfileManager.updateLocalPreferences(builder.manualZoom(java.lang.Boolean.valueOf(enabled)).manualZoomLevel(java.lang.Float.valueOf(-1.0f)).build());
                com.navdy.hud.app.analytics.AnalyticsSupport.recordOptionSelection(model.isOn ? "Auto Zoom" : "Manual Zoom");
                this.presenter.refreshDataforPos(selection.pos);
                return false;
            case com.navdy.hud.app.R.id.main_menu_options_pause_demo /*2131623981*/:
                this.presenter.performSelectionAnimation(new com.navdy.hud.app.ui.component.mainmenu.MainOptionsMenu.Anon2());
                break;
            case com.navdy.hud.app.R.id.main_menu_options_play_demo /*2131623982*/:
                this.presenter.performSelectionAnimation(new com.navdy.hud.app.ui.component.mainmenu.MainOptionsMenu.Anon1());
                break;
            case com.navdy.hud.app.R.id.main_menu_options_raw_gps /*2131623983*/:
                sLogger.i("Toggling raw gps");
                if (model.isOn) {
                    enabled = false;
                }
                model.isOn = enabled;
                model.subTitle = resources.getString(enabled ? com.navdy.hud.app.R.string.si_enabled : com.navdy.hud.app.R.string.si_disabled);
                com.navdy.hud.app.device.gps.GpsUtils.SHOW_RAW_GPS.setEnabled(enabled);
                this.presenter.refreshDataforPos(selection.pos);
                return false;
            case com.navdy.hud.app.R.id.main_menu_options_restart_demo /*2131623985*/:
                this.presenter.performSelectionAnimation(new com.navdy.hud.app.ui.component.mainmenu.MainOptionsMenu.Anon3());
                break;
            case com.navdy.hud.app.R.id.main_menu_options_scroll_left /*2131623986*/:
            case com.navdy.hud.app.R.id.main_menu_options_scroll_right /*2131623987*/:
                this.presenter.performSelectionAnimation(new com.navdy.hud.app.ui.component.mainmenu.MainOptionsMenu.Anon4());
                break;
            case com.navdy.hud.app.R.id.main_menu_options_select_center_gauge /*2131623988*/:
                this.dashGaugeConfiguratorMenu.setGaugeType(com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu.GaugeType.CENTER);
                this.presenter.loadMenu(this.dashGaugeConfiguratorMenu, com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel.SUB_LEVEL, selection.pos, 0);
                break;
            case com.navdy.hud.app.R.id.main_menu_options_side_gauges /*2131623989*/:
                this.dashGaugeConfiguratorMenu.setGaugeType(com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu.GaugeType.SIDE);
                this.presenter.loadMenu(this.dashGaugeConfiguratorMenu, com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel.SUB_LEVEL, selection.pos, 0);
                break;
            case com.navdy.hud.app.R.id.menu_back /*2131624007*/:
                sLogger.v("back");
                com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("back");
                this.presenter.loadMenu(this.parent, com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel.BACK_TO_PARENT, this.backSelection, 0);
                break;
            case com.navdy.hud.app.R.id.mm_show_speed_limit /*2131624018*/:
                this.presenter.loadMenu(this.speedLimitSettingMenu, com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel.SUB_LEVEL, selection.pos, 0);
                break;
            default:
                if (this.homeScreenView.isAHomeScreenWidgetId(selection.id)) {
                    if (model.isOn) {
                        enabled = false;
                    }
                    model.isOn = enabled;
                    model.subTitle = model.isOn ? onLabel : offLabel;
                    this.homeScreenView.setIsWidgetEnabled(selection.id, model.isOn);
                    this.presenter.refreshDataforPos(selection.pos);
                    return false;
                }
                break;
        }
        return true;
    }

    public com.navdy.hud.app.ui.component.mainmenu.IMenu.Menu getType() {
        return com.navdy.hud.app.ui.component.mainmenu.IMenu.Menu.MAIN_OPTIONS;
    }

    public boolean isItemClickable(int id, int pos) {
        return true;
    }

    public com.navdy.hud.app.ui.component.vlist.VerticalList.Model getModelfromPos(int pos) {
        if (this.cachedList == null || this.cachedList.size() <= pos) {
            return null;
        }
        return (com.navdy.hud.app.ui.component.vlist.VerticalList.Model) this.cachedList.get(pos);
    }

    public boolean isBindCallsEnabled() {
        return false;
    }

    public void onBindToView(com.navdy.hud.app.ui.component.vlist.VerticalList.Model model, android.view.View view, int pos, com.navdy.hud.app.ui.component.vlist.VerticalList.ModelState state) {
    }

    public com.navdy.hud.app.ui.component.mainmenu.IMenu getChildMenu(com.navdy.hud.app.ui.component.mainmenu.IMenu parent2, java.lang.String args, java.lang.String path) {
        return null;
    }

    public void onUnload(com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel level) {
    }

    public void onItemSelected(com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState selection) {
    }

    public void onScrollIdle() {
    }

    public void onFastScrollStart() {
    }

    public void onFastScrollEnd() {
    }

    public void showToolTip() {
    }

    public boolean isFirstItemEmpty() {
        return true;
    }
}
