package com.navdy.hud.app.ui.component.main;

public class MainLowerView extends android.widget.FrameLayout {
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.main.MainLowerView.class);
    private com.navdy.hud.app.ui.component.homescreen.HomeScreenView homeScreenView;
    private com.navdy.hud.app.ui.component.homescreen.NavigationView navigationView;

    public MainLowerView(android.content.Context context) {
        super(context);
    }

    public MainLowerView(android.content.Context context, android.util.AttributeSet attrs) {
        super(context, attrs);
    }

    public MainLowerView(android.content.Context context, android.util.AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void injectView(android.view.View view) {
        sLogger.v("injectView");
        if (initView() && com.navdy.hud.app.maps.here.HereMapsManager.getInstance().isInitialized()) {
            removeAllViews();
            addView(view);
            setAlpha(0.0f);
            setVisibility(0);
            if (!this.homeScreenView.isNavigationActive()) {
                sLogger.v("injectView: set to nav mode");
                this.homeScreenView.setMode(com.navdy.hud.app.maps.NavigationMode.MAP_ON_ROUTE);
            } else {
                sLogger.v("injectView: nav active already");
            }
            animate().alpha(1.0f).start();
        }
    }

    public void ejectView() {
        sLogger.v("ejectView");
        if (initView() && com.navdy.hud.app.maps.here.HereMapsManager.getInstance().isInitialized() && getVisibility() == 0) {
            animate().cancel();
            setVisibility(8);
            setAlpha(0.0f);
            removeAllViews();
            if (com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().getNavController().getState() != com.navdy.hud.app.maps.here.HereNavController.State.NAVIGATING) {
                sLogger.v("ejectView: reset to open mode");
                this.homeScreenView.setMode(com.navdy.hud.app.maps.NavigationMode.MAP);
                return;
            }
            sLogger.v("ejectView: nav active already");
        }
    }

    private boolean initView() {
        if (this.navigationView == null) {
            this.navigationView = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getUiStateManager().getNavigationView();
        }
        if (this.homeScreenView == null) {
            this.homeScreenView = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getUiStateManager().getHomescreenView();
        }
        if (this.navigationView == null || this.homeScreenView == null) {
            return false;
        }
        return true;
    }
}
