package com.navdy.hud.app.ui.component.homescreen;

public class ImageWidgetPresenter extends com.navdy.hud.app.view.DashboardWidgetPresenter {
    private java.lang.String id;
    private android.graphics.drawable.Drawable mDrawable;

    public ImageWidgetPresenter(android.content.Context context, int imageResource, java.lang.String id2) {
        if (imageResource > 0) {
            this.mDrawable = context.getResources().getDrawable(imageResource);
        }
        this.id = id2;
    }

    public void setView(com.navdy.hud.app.view.DashboardWidgetView dashboardWidgetView) {
        if (dashboardWidgetView != null) {
            dashboardWidgetView.setContentView((int) com.navdy.hud.app.R.layout.smart_dash_widget_layout);
        }
        super.setView(dashboardWidgetView);
    }

    public android.graphics.drawable.Drawable getDrawable() {
        return this.mDrawable;
    }

    /* access modifiers changed from: protected */
    public void updateGauge() {
    }

    public java.lang.String getWidgetIdentifier() {
        return this.id;
    }

    public java.lang.String getWidgetName() {
        return null;
    }
}
