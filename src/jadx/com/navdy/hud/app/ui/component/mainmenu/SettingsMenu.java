package com.navdy.hud.app.ui.component.mainmenu;

public class SettingsMenu implements com.navdy.hud.app.ui.component.mainmenu.IMenu {
    private static final com.navdy.hud.app.ui.component.vlist.VerticalList.Model back;
    private static final com.navdy.hud.app.ui.component.vlist.VerticalList.Model brightness;
    private static final com.navdy.hud.app.ui.component.vlist.VerticalList.Model connectPhone;
    private static final com.navdy.hud.app.ui.component.vlist.VerticalList.Model dialFwUpdate;
    private static final com.navdy.hud.app.ui.component.vlist.VerticalList.Model displayFwUpdate;
    private static final com.navdy.hud.app.ui.component.vlist.VerticalList.Model factoryReset;
    private static final com.navdy.hud.app.ui.component.vlist.VerticalList.Model learningGestures;
    private static final android.content.res.Resources resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.mainmenu.SettingsMenu.class);
    private static final java.lang.String setting = resources.getString(com.navdy.hud.app.R.string.carousel_menu_settings_title);
    private static final int settingColor = resources.getColor(com.navdy.hud.app.R.color.mm_settings);
    private static final com.navdy.hud.app.ui.component.vlist.VerticalList.Model shutdown;
    private static final com.navdy.hud.app.ui.component.vlist.VerticalList.Model systemInfo = com.navdy.hud.app.ui.component.vlist.viewholder.IconsTwoViewHolder.buildModel(com.navdy.hud.app.R.id.main_menu_system_info, com.navdy.hud.app.R.drawable.icon_settings_navdy_data, com.navdy.hud.app.R.drawable.icon_settings_navdy_data_sm, resources.getColor(com.navdy.hud.app.R.color.mm_settings_sys_info), -1, resources.getString(com.navdy.hud.app.R.string.carousel_menu_system_info_title), null);
    private int backSelection;
    private int backSelectionId;
    private com.squareup.otto.Bus bus;
    private java.util.List<com.navdy.hud.app.ui.component.vlist.VerticalList.Model> cachedList;
    private com.navdy.hud.app.ui.component.mainmenu.IMenu parent;
    private com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.Presenter presenter;
    private com.navdy.hud.app.ui.component.mainmenu.SystemInfoMenu systemInfoMenu;
    private com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent vscrollComponent;

    static {
        java.lang.String title = resources.getString(com.navdy.hud.app.R.string.back);
        int fluctuatorColor = resources.getColor(com.navdy.hud.app.R.color.mm_back);
        back = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.menu_back, com.navdy.hud.app.R.drawable.icon_mm_back, fluctuatorColor, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, fluctuatorColor, title, null);
        java.lang.String title2 = resources.getString(com.navdy.hud.app.R.string.carousel_settings_brightness);
        int fluctuatorColor2 = resources.getColor(com.navdy.hud.app.R.color.mm_settings_brightness);
        brightness = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.settings_menu_brightness, com.navdy.hud.app.R.drawable.icon_settings_brightness_2, fluctuatorColor2, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, fluctuatorColor2, title2, null);
        java.lang.String title3 = resources.getString(com.navdy.hud.app.R.string.carousel_settings_connect_phone_title);
        int fluctuatorColor3 = resources.getColor(com.navdy.hud.app.R.color.mm_connnect_phone);
        connectPhone = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.settings_menu_connect_phone, com.navdy.hud.app.R.drawable.icon_settings_connect_phone_2, fluctuatorColor3, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, fluctuatorColor3, title3, null);
        java.lang.String title4 = resources.getString(com.navdy.hud.app.R.string.carousel_settings_learning_gesture_title);
        int fluctuatorColor4 = resources.getColor(com.navdy.hud.app.R.color.mm_settings_learn_gestures);
        learningGestures = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.settings_menu_learning_gestures, com.navdy.hud.app.R.drawable.icon_settings_learn_gestures_2, fluctuatorColor4, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, fluctuatorColor4, title4, null);
        java.lang.String title5 = resources.getString(com.navdy.hud.app.R.string.carousel_settings_software_update_title);
        int fluctuatorColor5 = resources.getColor(com.navdy.hud.app.R.color.mm_settings_update_display);
        displayFwUpdate = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.settings_menu_software_update, com.navdy.hud.app.R.drawable.icon_software_update_2, fluctuatorColor5, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, fluctuatorColor5, title5, null);
        java.lang.String title6 = resources.getString(com.navdy.hud.app.R.string.carousel_settings_dial_update_title);
        int fluctuatorColor6 = resources.getColor(com.navdy.hud.app.R.color.mm_settings_update_dial);
        dialFwUpdate = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.settings_menu_dial_update, com.navdy.hud.app.R.drawable.icon_dial_update_2, fluctuatorColor6, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, fluctuatorColor6, title6, null);
        java.lang.String title7 = resources.getString(com.navdy.hud.app.R.string.carousel_settings_shutdown_title);
        int fluctuatorColor7 = resources.getColor(com.navdy.hud.app.R.color.mm_settings_shutdown);
        shutdown = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.settings_menu_shutdown, com.navdy.hud.app.R.drawable.icon_settings_shutdown_2, fluctuatorColor7, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, fluctuatorColor7, title7, null);
        java.lang.String title8 = resources.getString(com.navdy.hud.app.R.string.carousel_settings_factory_reset_title);
        int fluctuatorColor8 = resources.getColor(com.navdy.hud.app.R.color.mm_settings_factory_reset);
        factoryReset = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.settings_menu_factory_reset, com.navdy.hud.app.R.drawable.icon_settings_factory_reset_2, fluctuatorColor8, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, fluctuatorColor8, title8, null);
    }

    public SettingsMenu(com.squareup.otto.Bus bus2, com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent vscrollComponent2, com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.Presenter presenter2, com.navdy.hud.app.ui.component.mainmenu.IMenu parent2) {
        this.bus = bus2;
        this.vscrollComponent = vscrollComponent2;
        this.presenter = presenter2;
        this.parent = parent2;
    }

    public java.util.List<com.navdy.hud.app.ui.component.vlist.VerticalList.Model> getItems() {
        if (this.cachedList != null) {
            return this.cachedList;
        }
        java.util.List<com.navdy.hud.app.ui.component.vlist.VerticalList.Model> list = new java.util.ArrayList<>();
        list.add(back);
        list.add(brightness);
        list.add(connectPhone);
        list.add(learningGestures);
        if (com.navdy.hud.app.util.OTAUpdateService.isUpdateAvailable()) {
            list.add(displayFwUpdate);
        }
        com.navdy.hud.app.device.dial.DialManager dialManager = com.navdy.hud.app.device.dial.DialManager.getInstance();
        if (dialManager.isDialConnected() && dialManager.getDialFirmwareUpdater().getVersions().isUpdateAvailable()) {
            list.add(dialFwUpdate);
        }
        list.add(shutdown);
        list.add(systemInfo);
        list.add(factoryReset);
        this.cachedList = list;
        return list;
    }

    public int getInitialSelection() {
        return 1;
    }

    public com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex getScrollIndex() {
        return null;
    }

    public void setBackSelectionPos(int n) {
        this.backSelection = n;
    }

    public void setBackSelectionId(int id) {
        this.backSelectionId = id;
    }

    public void setSelectedIcon() {
        this.vscrollComponent.setSelectedIconColorImage(com.navdy.hud.app.R.drawable.icon_mm_settings_2, settingColor, null, 1.0f);
        this.vscrollComponent.selectedText.setText(setting);
    }

    public boolean isItemClickable(int id, int pos) {
        return true;
    }

    public com.navdy.hud.app.ui.component.vlist.VerticalList.Model getModelfromPos(int pos) {
        if (this.cachedList == null || this.cachedList.size() <= pos) {
            return null;
        }
        return (com.navdy.hud.app.ui.component.vlist.VerticalList.Model) this.cachedList.get(pos);
    }

    public boolean isBindCallsEnabled() {
        return false;
    }

    public void onBindToView(com.navdy.hud.app.ui.component.vlist.VerticalList.Model model, android.view.View view, int pos, com.navdy.hud.app.ui.component.vlist.VerticalList.ModelState state) {
    }

    public com.navdy.hud.app.ui.component.mainmenu.IMenu getChildMenu(com.navdy.hud.app.ui.component.mainmenu.IMenu parent2, java.lang.String args, java.lang.String path) {
        return null;
    }

    public void onUnload(com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel level) {
    }

    public void onItemSelected(com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState selection) {
    }

    public void onScrollIdle() {
    }

    public void onFastScrollStart() {
    }

    public void onFastScrollEnd() {
    }

    public void showToolTip() {
    }

    public boolean isFirstItemEmpty() {
        return true;
    }

    public boolean selectItem(com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState selection) {
        sLogger.v("select id:" + selection.id + " pos:" + selection.pos);
        switch (selection.id) {
            case com.navdy.hud.app.R.id.main_menu_system_info /*2131624003*/:
                sLogger.v("system info");
                com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("system_info");
                if (this.systemInfoMenu == null) {
                    this.systemInfoMenu = new com.navdy.hud.app.ui.component.mainmenu.SystemInfoMenu(this.bus, this.vscrollComponent, this.presenter, this);
                }
                this.presenter.loadMenu(this.systemInfoMenu, com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel.SUB_LEVEL, selection.pos, 1, true);
                break;
            case com.navdy.hud.app.R.id.menu_back /*2131624007*/:
                sLogger.v("back");
                com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("back");
                this.presenter.loadMenu(this.parent, com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel.BACK_TO_PARENT, this.backSelection, this.backSelectionId);
                this.backSelectionId = 0;
                break;
            case com.navdy.hud.app.R.id.settings_menu_auto_brightness /*2131624058*/:
                sLogger.v("auto brightness");
                com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("auto_brightness");
                this.presenter.performSelectionAnimation(new com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils.ActionRunnable(this.bus, com.navdy.service.library.events.ui.Screen.SCREEN_AUTO_BRIGHTNESS));
                break;
            case com.navdy.hud.app.R.id.settings_menu_brightness /*2131624060*/:
                sLogger.v("brightness");
                com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("brightness");
                this.presenter.performSelectionAnimation(new com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils.ActionRunnable(this.bus, com.navdy.service.library.events.ui.Screen.SCREEN_BRIGHTNESS));
                break;
            case com.navdy.hud.app.R.id.settings_menu_connect_phone /*2131624061*/:
                sLogger.v("connect phone");
                com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("connect_phone");
                android.os.Bundle args = new android.os.Bundle();
                args.putString(com.navdy.hud.app.screen.WelcomeScreen.ARG_ACTION, com.navdy.hud.app.screen.WelcomeScreen.ACTION_SWITCH_PHONE);
                this.presenter.performSelectionAnimation(new com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils.ActionRunnable(this.bus, com.navdy.service.library.events.ui.Screen.SCREEN_WELCOME, args, false));
                break;
            case com.navdy.hud.app.R.id.settings_menu_dial_pairing /*2131624062*/:
                sLogger.v("Dial pairing");
                com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("dial_pairing");
                this.presenter.performSelectionAnimation(new com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils.ActionRunnable(this.bus, com.navdy.service.library.events.ui.Screen.SCREEN_DIAL_PAIRING));
                break;
            case com.navdy.hud.app.R.id.settings_menu_dial_update /*2131624063*/:
                sLogger.v("dial update");
                com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("dial_update");
                android.os.Bundle args2 = new android.os.Bundle();
                args2.putBoolean(com.navdy.hud.app.manager.UpdateReminderManager.EXTRA_UPDATE_REMINDER, false);
                this.presenter.performSelectionAnimation(new com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils.ActionRunnable(this.bus, com.navdy.service.library.events.ui.Screen.SCREEN_DIAL_UPDATE_CONFIRMATION, args2, false));
                break;
            case com.navdy.hud.app.R.id.settings_menu_factory_reset /*2131624065*/:
                sLogger.v("factory reset");
                com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("factory_reset");
                this.presenter.performSelectionAnimation(new com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils.ActionRunnable(this.bus, com.navdy.service.library.events.ui.Screen.SCREEN_FACTORY_RESET));
                break;
            case com.navdy.hud.app.R.id.settings_menu_learning_gestures /*2131624066*/:
                sLogger.v("Learning gestures");
                com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("learning_gestures");
                this.presenter.performSelectionAnimation(new com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils.ActionRunnable(this.bus, com.navdy.service.library.events.ui.Screen.SCREEN_GESTURE_LEARNING));
                break;
            case com.navdy.hud.app.R.id.settings_menu_shutdown /*2131624067*/:
                sLogger.v("shutdown");
                com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("shutdown");
                this.presenter.performSelectionAnimation(new com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils.ActionRunnable(this.bus, com.navdy.service.library.events.ui.Screen.SCREEN_SHUTDOWN_CONFIRMATION, com.navdy.hud.app.event.Shutdown.Reason.MENU.asBundle(), false));
                break;
            case com.navdy.hud.app.R.id.settings_menu_software_update /*2131624068*/:
                sLogger.v("software update");
                com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("software_update");
                android.os.Bundle args3 = new android.os.Bundle();
                args3.putBoolean(com.navdy.hud.app.manager.UpdateReminderManager.EXTRA_UPDATE_REMINDER, false);
                this.presenter.performSelectionAnimation(new com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils.ActionRunnable(this.bus, com.navdy.service.library.events.ui.Screen.SCREEN_OTA_CONFIRMATION, args3, false));
                break;
        }
        return true;
    }

    public com.navdy.hud.app.ui.component.mainmenu.IMenu.Menu getType() {
        return com.navdy.hud.app.ui.component.mainmenu.IMenu.Menu.SETTINGS;
    }
}
