package com.navdy.hud.app.ui.component.homescreen;

public class TbtEarlyView$$ViewInjector {
    public static void inject(butterknife.ButterKnife.Finder finder, com.navdy.hud.app.ui.component.homescreen.TbtEarlyView target, java.lang.Object source) {
        target.earlyManeuverImage = (android.widget.ImageView) finder.findRequiredView(source, com.navdy.hud.app.R.id.earlyManeuverImage, "field 'earlyManeuverImage'");
    }

    public static void reset(com.navdy.hud.app.ui.component.homescreen.TbtEarlyView target) {
        target.earlyManeuverImage = null;
    }
}
