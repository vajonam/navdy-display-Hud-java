package com.navdy.hud.app.ui.component.carousel;

public interface AnimationStrategy {

    public enum Direction {
        LEFT,
        RIGHT
    }

    android.animation.AnimatorSet buildLayoutAnimation(android.animation.Animator.AnimatorListener animatorListener, com.navdy.hud.app.ui.component.carousel.CarouselLayout carouselLayout, int i, int i2);

    android.animation.AnimatorSet createHiddenViewAnimation(com.navdy.hud.app.ui.component.carousel.CarouselLayout carouselLayout, com.navdy.hud.app.ui.component.carousel.AnimationStrategy.Direction direction);

    android.animation.AnimatorSet createMiddleLeftViewAnimation(com.navdy.hud.app.ui.component.carousel.CarouselLayout carouselLayout, com.navdy.hud.app.ui.component.carousel.AnimationStrategy.Direction direction);

    android.animation.AnimatorSet createMiddleRightViewAnimation(com.navdy.hud.app.ui.component.carousel.CarouselLayout carouselLayout, com.navdy.hud.app.ui.component.carousel.AnimationStrategy.Direction direction);

    android.animation.AnimatorSet createNewMiddleRightViewAnimation(com.navdy.hud.app.ui.component.carousel.CarouselLayout carouselLayout, com.navdy.hud.app.ui.component.carousel.AnimationStrategy.Direction direction);

    android.animation.AnimatorSet createSideViewToMiddleAnimation(com.navdy.hud.app.ui.component.carousel.CarouselLayout carouselLayout, com.navdy.hud.app.ui.component.carousel.AnimationStrategy.Direction direction);

    android.animation.Animator createViewOutAnimation(com.navdy.hud.app.ui.component.carousel.CarouselLayout carouselLayout, com.navdy.hud.app.ui.component.carousel.AnimationStrategy.Direction direction);
}
