package com.navdy.hud.app.ui.component.vlist;

public class VerticalRecyclerView extends android.support.v7.widget.RecyclerView {
    public VerticalRecyclerView(android.content.Context context) {
        super(context);
    }

    public VerticalRecyclerView(android.content.Context context, android.util.AttributeSet attrs) {
        super(context, attrs);
    }

    public VerticalRecyclerView(android.content.Context context, android.util.AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }
}
