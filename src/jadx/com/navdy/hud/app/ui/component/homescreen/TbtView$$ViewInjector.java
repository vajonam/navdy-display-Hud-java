package com.navdy.hud.app.ui.component.homescreen;

public class TbtView$$ViewInjector {
    public static void inject(butterknife.ButterKnife.Finder finder, com.navdy.hud.app.ui.component.homescreen.TbtView target, java.lang.Object source) {
        target.activeMapDistance = (android.widget.TextView) finder.findRequiredView(source, com.navdy.hud.app.R.id.activeMapDistance, "field 'activeMapDistance'");
        target.activeMapIcon = (android.widget.ImageView) finder.findRequiredView(source, com.navdy.hud.app.R.id.activeMapIcon, "field 'activeMapIcon'");
        target.nextMapIconContainer = finder.findRequiredView(source, com.navdy.hud.app.R.id.nextMapIconContainer, "field 'nextMapIconContainer'");
        target.nextMapIcon = (android.widget.ImageView) finder.findRequiredView(source, com.navdy.hud.app.R.id.nextMapIcon, "field 'nextMapIcon'");
        target.activeMapInstruction = (android.widget.TextView) finder.findRequiredView(source, com.navdy.hud.app.R.id.activeMapInstruction, "field 'activeMapInstruction'");
        target.nextMapInstructionContainer = finder.findRequiredView(source, com.navdy.hud.app.R.id.nextMapInstructionContainer, "field 'nextMapInstructionContainer'");
        target.nextMapInstruction = (android.widget.TextView) finder.findRequiredView(source, com.navdy.hud.app.R.id.nextMapInstruction, "field 'nextMapInstruction'");
        target.progressBar = (android.widget.ProgressBar) finder.findRequiredView(source, com.navdy.hud.app.R.id.progress_bar, "field 'progressBar'");
        target.nowIcon = finder.findRequiredView(source, com.navdy.hud.app.R.id.now_icon, "field 'nowIcon'");
    }

    public static void reset(com.navdy.hud.app.ui.component.homescreen.TbtView target) {
        target.activeMapDistance = null;
        target.activeMapIcon = null;
        target.nextMapIconContainer = null;
        target.nextMapIcon = null;
        target.activeMapInstruction = null;
        target.nextMapInstructionContainer = null;
        target.nextMapInstruction = null;
        target.progressBar = null;
        target.nowIcon = null;
    }
}
