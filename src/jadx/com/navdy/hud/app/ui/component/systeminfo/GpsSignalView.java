package com.navdy.hud.app.ui.component.systeminfo;

public class GpsSignalView extends android.widget.LinearLayout {
    private com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.systeminfo.GpsSignalView.class);

    public static class SatelliteData implements java.lang.Comparable<com.navdy.hud.app.ui.component.systeminfo.GpsSignalView.SatelliteData> {
        int cNo;
        java.lang.String provider;
        int satelliteId;

        public SatelliteData(int satelliteId2, int cNo2, java.lang.String provider2) {
            this.satelliteId = satelliteId2;
            this.cNo = cNo2;
            this.provider = provider2;
        }

        public int compareTo(com.navdy.hud.app.ui.component.systeminfo.GpsSignalView.SatelliteData another) {
            return another.cNo - this.cNo;
        }
    }

    public GpsSignalView(android.content.Context context) {
        super(context);
    }

    public GpsSignalView(android.content.Context context, android.util.AttributeSet attrs) {
        super(context, attrs);
    }

    public GpsSignalView(android.content.Context context, android.util.AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setOrientation(0);
    }

    public void setSatelliteData(com.navdy.hud.app.ui.component.systeminfo.GpsSignalView.SatelliteData[] satelliteData) {
        removeAllViews();
        android.view.LayoutInflater inflater = android.view.LayoutInflater.from(getContext());
        android.content.res.Resources resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        int separator = resources.getDimensionPixelSize(com.navdy.hud.app.R.dimen.satellite_width_separator);
        int width = resources.getDimensionPixelSize(com.navdy.hud.app.R.dimen.satellite_width);
        for (int i = 0; i < satelliteData.length; i++) {
            android.view.ViewGroup layout = (android.view.ViewGroup) inflater.inflate(com.navdy.hud.app.R.layout.screen_home_system_info_gps_satellite, null);
            layout.setLayoutParams(new android.widget.LinearLayout.LayoutParams(width, -1));
            ((android.widget.TextView) layout.findViewById(com.navdy.hud.app.R.id.satellite_cno)).setText(java.lang.String.valueOf(satelliteData[i].cNo));
            ((android.widget.TextView) layout.findViewById(com.navdy.hud.app.R.id.satellite_type)).setText(satelliteData[i].provider);
            ((android.widget.TextView) layout.findViewById(com.navdy.hud.app.R.id.satellite_id)).setText(java.lang.String.valueOf(satelliteData[i].satelliteId));
            ((android.widget.RelativeLayout.LayoutParams) layout.findViewById(com.navdy.hud.app.R.id.satellite_strength).getLayoutParams()).height = satelliteData[i].cNo * 2;
            addView(layout);
            if (i < satelliteData.length - 1) {
                android.view.View v = new android.view.View(getContext());
                v.setLayoutParams(new android.widget.LinearLayout.LayoutParams(separator, separator));
                addView(v);
            }
        }
        this.logger.v("set satellite =" + satelliteData.length);
    }
}
