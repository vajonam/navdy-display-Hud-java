package com.navdy.hud.app.ui.component.mainmenu;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\b\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\b\u0002"}, d2 = {"<anonymous>", "", "run"}, k = 3, mv = {1, 1, 6})
/* compiled from: ReportIssueMenu.kt */
final class ReportIssueMenu$takeSnapshot$Anon1 implements java.lang.Runnable {
    final /* synthetic */ int $id;
    final /* synthetic */ com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu this$Anon0;

    @kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\b\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\b\u0002"}, d2 = {"<anonymous>", "", "run"}, k = 3, mv = {1, 1, 6})
    /* compiled from: ReportIssueMenu.kt */
    static final class Anon1 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$takeSnapshot$Anon1 this$Anon0;

        Anon1(com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$takeSnapshot$Anon1 reportIssueMenu$takeSnapshot$Anon1) {
            this.this$Anon0 = reportIssueMenu$takeSnapshot$Anon1;
        }

        public final void run() {
            com.navdy.hud.app.util.ReportIssueService.submitJiraTicketAsync((java.lang.String) com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu.Companion.getIdToTitleMap().get(java.lang.Integer.valueOf(this.this$Anon0.$id)));
        }
    }

    ReportIssueMenu$takeSnapshot$Anon1(com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu reportIssueMenu, int i) {
        this.this$Anon0 = reportIssueMenu;
        this.$id = i;
    }

    public final void run() {
        this.this$Anon0.presenter.close(new com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$takeSnapshot$Anon1.Anon1(this));
    }
}
