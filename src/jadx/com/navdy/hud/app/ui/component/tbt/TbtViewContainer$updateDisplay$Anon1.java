package com.navdy.hud.app.ui.component.tbt;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u0019\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\b\n\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016\u00a8\u0006\u0007"}, d2 = {"com/navdy/hud/app/ui/component/tbt/TbtViewContainer$updateDisplay$Anon1", "Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;", "(Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;)V", "onAnimationEnd", "", "animation", "Landroid/animation/Animator;", "app_hudDebug"}, k = 1, mv = {1, 1, 6})
/* compiled from: TbtViewContainer.kt */
public final class TbtViewContainer$updateDisplay$Anon1 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
    final /* synthetic */ com.navdy.hud.app.ui.component.tbt.TbtViewContainer this$Anon0;

    TbtViewContainer$updateDisplay$Anon1(com.navdy.hud.app.ui.component.tbt.TbtViewContainer $outer) {
        this.this$Anon0 = $outer;
    }

    public void onAnimationEnd(@org.jetbrains.annotations.NotNull android.animation.Animator animation) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(animation, "animation");
        com.navdy.hud.app.ui.component.tbt.TbtView access$getInactiveView$p = this.this$Anon0.inactiveView;
        if (access$getInactiveView$p != null) {
            access$getInactiveView$p.setTranslationY(-com.navdy.hud.app.ui.component.tbt.TbtViewContainer.Companion.getItemHeight());
        }
    }
}
