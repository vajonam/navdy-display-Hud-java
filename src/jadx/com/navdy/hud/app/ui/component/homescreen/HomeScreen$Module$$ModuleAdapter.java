package com.navdy.hud.app.ui.component.homescreen;

public final class HomeScreen$Module$$ModuleAdapter extends dagger.internal.ModuleAdapter<com.navdy.hud.app.ui.component.homescreen.HomeScreen.Module> {
    private static final java.lang.Class<?>[] INCLUDES = new java.lang.Class[0];
    private static final java.lang.String[] INJECTS = {"members/com.navdy.hud.app.ui.component.homescreen.HomeScreenView"};
    private static final java.lang.Class<?>[] STATIC_INJECTIONS = new java.lang.Class[0];

    public HomeScreen$Module$$ModuleAdapter() {
        super(com.navdy.hud.app.ui.component.homescreen.HomeScreen.Module.class, INJECTS, STATIC_INJECTIONS, false, INCLUDES, true, false);
    }
}
