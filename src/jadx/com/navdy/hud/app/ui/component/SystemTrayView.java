package com.navdy.hud.app.ui.component;

public class SystemTrayView extends android.widget.LinearLayout {
    static java.util.HashMap<com.navdy.hud.app.ui.component.SystemTrayView.State, java.lang.Integer> dialStateResMap = new java.util.HashMap<>();
    static java.util.HashMap<com.navdy.hud.app.ui.component.SystemTrayView.State, java.lang.Integer> phoneStateResMap = new java.util.HashMap<>();
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.SystemTrayView.class);
    private android.widget.TextView debugGpsMarker;
    private com.navdy.hud.app.ui.component.SystemTrayView.State dialBattery;
    private boolean dialConnected;
    @butterknife.InjectView(2131624467)
    android.widget.ImageView dialImageView;
    @butterknife.InjectView(2131624466)
    android.widget.ImageView locationImageView;
    private boolean noNetwork;
    private com.navdy.hud.app.ui.component.SystemTrayView.State phoneBattery;
    private boolean phoneConnected;
    @butterknife.InjectView(2131624470)
    android.widget.ImageView phoneImageView;
    @butterknife.InjectView(2131624469)
    android.widget.ImageView phoneNetworkImageView;

    public enum Device {
        Phone,
        Dial,
        Gps
    }

    public enum State {
        CONNECTED,
        DISCONNECTED,
        LOW_BATTERY,
        EXTREMELY_LOW_BATTERY,
        OK_BATTERY,
        NO_PHONE_NETWORK,
        PHONE_NETWORK_AVAILABLE,
        LOCATION_LOST,
        LOCATION_AVAILABLE
    }

    static {
        dialStateResMap.put(com.navdy.hud.app.ui.component.SystemTrayView.State.LOW_BATTERY, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_dial_batterylow_2_copy));
        dialStateResMap.put(com.navdy.hud.app.ui.component.SystemTrayView.State.EXTREMELY_LOW_BATTERY, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_dial_batterylow_copy));
        dialStateResMap.put(com.navdy.hud.app.ui.component.SystemTrayView.State.DISCONNECTED, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_dial_missing_copy));
        phoneStateResMap.put(com.navdy.hud.app.ui.component.SystemTrayView.State.LOW_BATTERY, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_device_batterylow_copy));
        phoneStateResMap.put(com.navdy.hud.app.ui.component.SystemTrayView.State.EXTREMELY_LOW_BATTERY, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_device_batterylow_2_copy));
        phoneStateResMap.put(com.navdy.hud.app.ui.component.SystemTrayView.State.DISCONNECTED, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_device_missing_copy));
    }

    public SystemTrayView(android.content.Context context) {
        this(context, null);
    }

    public SystemTrayView(android.content.Context context, android.util.AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SystemTrayView(android.content.Context context, android.util.AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.phoneBattery = com.navdy.hud.app.ui.component.SystemTrayView.State.OK_BATTERY;
        this.dialBattery = com.navdy.hud.app.ui.component.SystemTrayView.State.OK_BATTERY;
        init();
    }

    private void init() {
        android.view.LayoutInflater.from(getContext()).inflate(com.navdy.hud.app.R.layout.system_tray, this, true);
        setOrientation(0);
        butterknife.ButterKnife.inject((android.view.View) this);
        this.dialImageView.setVisibility(8);
        this.phoneImageView.setVisibility(8);
        this.phoneNetworkImageView.setVisibility(8);
        this.locationImageView.setVisibility(8);
    }

    public void setState(com.navdy.hud.app.ui.component.SystemTrayView.Device device, com.navdy.hud.app.ui.component.SystemTrayView.State state) {
        switch (device) {
            case Phone:
                handlePhoneState(state);
                return;
            case Dial:
                handleDialState(state);
                return;
            case Gps:
                handleGpsState(state);
                return;
            default:
                return;
        }
    }

    public void setDebugGpsMarker(java.lang.String marker) {
        if (marker == null) {
            try {
                if (this.debugGpsMarker != null) {
                    removeView(this.debugGpsMarker);
                    this.debugGpsMarker = null;
                }
            } catch (Throwable t) {
                sLogger.e(t);
            }
        } else {
            if (this.debugGpsMarker == null) {
                android.content.Context context = getContext();
                this.debugGpsMarker = new android.widget.TextView(context);
                this.debugGpsMarker.setTextAppearance(context, com.navdy.hud.app.R.style.glance_title_3);
                this.debugGpsMarker.setTextColor(-1);
                android.widget.LinearLayout.LayoutParams lytParams = new android.widget.LinearLayout.LayoutParams(-2, -2);
                lytParams.gravity = 16;
                addView(this.debugGpsMarker, 0, lytParams);
            }
            this.debugGpsMarker.setText(marker);
        }
    }

    private void handlePhoneState(com.navdy.hud.app.ui.component.SystemTrayView.State state) {
        boolean noNetworkAvailable;
        boolean connected = true;
        if (state == null) {
            this.phoneImageView.setImageResource(0);
            this.phoneImageView.setVisibility(8);
            this.phoneNetworkImageView.setVisibility(8);
            return;
        }
        switch (state) {
            case CONNECTED:
            case DISCONNECTED:
                if (state != com.navdy.hud.app.ui.component.SystemTrayView.State.CONNECTED) {
                    connected = false;
                }
                if (this.phoneConnected != connected) {
                    this.phoneConnected = connected;
                    this.noNetwork = false;
                    this.phoneBattery = com.navdy.hud.app.ui.component.SystemTrayView.State.OK_BATTERY;
                    updateViews();
                    return;
                }
                return;
            case OK_BATTERY:
            case LOW_BATTERY:
            case EXTREMELY_LOW_BATTERY:
                if (this.phoneBattery != state) {
                    this.phoneBattery = state;
                    updateViews();
                    return;
                }
                return;
            case PHONE_NETWORK_AVAILABLE:
            case NO_PHONE_NETWORK:
                if (state == com.navdy.hud.app.ui.component.SystemTrayView.State.NO_PHONE_NETWORK) {
                    noNetworkAvailable = true;
                } else {
                    noNetworkAvailable = false;
                }
                if (this.noNetwork != noNetworkAvailable) {
                    this.noNetwork = noNetworkAvailable;
                    updateViews();
                    return;
                }
                return;
            default:
                return;
        }
    }

    private void handleDialState(com.navdy.hud.app.ui.component.SystemTrayView.State state) {
        boolean connected = false;
        if (state == null) {
            this.dialImageView.setImageResource(0);
            this.dialImageView.setVisibility(8);
            return;
        }
        switch (state) {
            case CONNECTED:
            case DISCONNECTED:
                if (state == com.navdy.hud.app.ui.component.SystemTrayView.State.CONNECTED) {
                    connected = true;
                }
                if (this.dialConnected != connected) {
                    this.dialConnected = connected;
                    updateViews();
                    return;
                }
                return;
            case OK_BATTERY:
            case LOW_BATTERY:
            case EXTREMELY_LOW_BATTERY:
                if (this.dialBattery != state) {
                    this.dialBattery = state;
                    updateViews();
                    return;
                }
                return;
            default:
                return;
        }
    }

    private void updateViews() {
        int i = 0;
        this.phoneNetworkImageView.setVisibility((!this.noNetwork || !this.phoneConnected) ? 8 : 0);
        java.util.HashMap<com.navdy.hud.app.ui.component.SystemTrayView.State, java.lang.Integer> hashMap = phoneStateResMap;
        com.navdy.hud.app.ui.component.SystemTrayView.State state = com.navdy.hud.app.ui.component.SystemTrayView.State.OK_BATTERY;
        if (this.noNetwork) {
            i = com.navdy.hud.app.R.drawable.icon_device_battery_ok;
        }
        hashMap.put(state, java.lang.Integer.valueOf(i));
        updateDeviceStatus(this.phoneImageView, phoneStateResMap, this.phoneConnected ? this.phoneBattery : com.navdy.hud.app.ui.component.SystemTrayView.State.DISCONNECTED);
        updateDeviceStatus(this.dialImageView, dialStateResMap, this.dialConnected ? this.dialBattery : com.navdy.hud.app.ui.component.SystemTrayView.State.DISCONNECTED);
    }

    private void updateDeviceStatus(android.widget.ImageView view, java.util.HashMap<com.navdy.hud.app.ui.component.SystemTrayView.State, java.lang.Integer> mapping, com.navdy.hud.app.ui.component.SystemTrayView.State batteryState) {
        int resId;
        int i = 0;
        java.lang.Integer resource = (java.lang.Integer) mapping.get(batteryState);
        if (resource != null) {
            resId = resource.intValue();
        } else {
            resId = 0;
        }
        view.setImageResource(resId);
        if (resId == 0) {
            i = 8;
        }
        view.setVisibility(i);
    }

    private void handleGpsState(com.navdy.hud.app.ui.component.SystemTrayView.State state) {
        switch (state) {
            case LOCATION_LOST:
                this.locationImageView.setVisibility(0);
                setDebugGpsMarker(null);
                return;
            case LOCATION_AVAILABLE:
                this.locationImageView.setVisibility(8);
                return;
            default:
                return;
        }
    }
}
