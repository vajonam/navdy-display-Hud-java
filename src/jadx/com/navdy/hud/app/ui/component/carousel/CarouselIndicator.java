package com.navdy.hud.app.ui.component.carousel;

public class CarouselIndicator extends android.widget.FrameLayout {
    public static final int PROGRESS_INDICATOR_HORIZONTAL_THRESHOLD = 12;
    public static final int PROGRESS_INDICATOR_VERTICAL_THRESHOLD = 8;
    private int barParentSize = -1;
    private int barSize = -1;
    private int circleFocusSize;
    private int circleMargin;
    private int circleSize;
    private int currentItemPaddingRadius;
    private boolean fullBackground;
    private int itemCount;
    private int itemPadding;
    private int itemRadius;
    private com.navdy.hud.app.ui.component.carousel.CarouselIndicator.Orientation orientation = com.navdy.hud.app.ui.component.carousel.CarouselIndicator.Orientation.HORIZONTAL;
    private com.navdy.hud.app.ui.component.carousel.IProgressIndicator progressIndicator;
    private int roundRadius;
    private int viewPadding;

    public enum Orientation {
        HORIZONTAL,
        VERTICAL
    }

    public CarouselIndicator(android.content.Context context) {
        super(context, null);
        init(context, null);
    }

    public CarouselIndicator(android.content.Context context, android.util.AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public CarouselIndicator(android.content.Context context, android.util.AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(android.content.Context context, android.util.AttributeSet attrs) {
        android.content.res.Resources resources = context.getResources();
        this.circleSize = (int) resources.getDimension(com.navdy.hud.app.R.dimen.carousel_circle_indicator_size);
        this.circleFocusSize = (int) resources.getDimension(com.navdy.hud.app.R.dimen.carousel_circle_indicator_focus_size);
        this.circleMargin = (int) resources.getDimension(com.navdy.hud.app.R.dimen.carousel_circle_indicator_margin);
        this.roundRadius = (int) resources.getDimension(com.navdy.hud.app.R.dimen.carousel_progress_round_radius);
        this.itemRadius = (int) resources.getDimension(com.navdy.hud.app.R.dimen.carousel_progress_item_radius);
        this.itemPadding = (int) resources.getDimension(com.navdy.hud.app.R.dimen.carousel_progress_item_padding);
        this.currentItemPaddingRadius = (int) resources.getDimension(com.navdy.hud.app.R.dimen.carousel_progress_currentitem_padding_radius);
        if (attrs != null) {
            android.content.res.TypedArray typedArray = context.obtainStyledAttributes(attrs, com.navdy.hud.app.R.styleable.CarouselIndicator);
            this.circleSize = (int) typedArray.getDimension(0, (float) this.circleSize);
            this.circleFocusSize = (int) typedArray.getDimension(1, (float) this.circleFocusSize);
            this.circleMargin = (int) typedArray.getDimension(2, (float) this.circleMargin);
            this.roundRadius = (int) typedArray.getDimension(3, (float) this.roundRadius);
            this.itemRadius = (int) typedArray.getDimension(4, (float) this.itemRadius);
            this.itemPadding = (int) typedArray.getDimension(5, (float) this.itemPadding);
            this.currentItemPaddingRadius = (int) typedArray.getDimension(6, (float) this.currentItemPaddingRadius);
            this.fullBackground = typedArray.getBoolean(7, false);
            this.viewPadding = (int) typedArray.getDimension(8, 0.0f);
            this.barSize = (int) typedArray.getDimension(9, -1.0f);
            this.barParentSize = (int) typedArray.getDimension(10, -1.0f);
            typedArray.recycle();
        }
    }

    public void setOrientation(com.navdy.hud.app.ui.component.carousel.CarouselIndicator.Orientation orientation2) {
        this.orientation = orientation2;
    }

    public void setItemCount(int n) {
        android.widget.FrameLayout.LayoutParams lytParams;
        int width;
        int height;
        if (n >= 0) {
            this.itemCount = n;
            int defaultColor = com.navdy.hud.app.HudApplication.getAppContext().getResources().getColor(17170443);
            if (n > (this.orientation == com.navdy.hud.app.ui.component.carousel.CarouselIndicator.Orientation.HORIZONTAL ? 12 : 8)) {
                if (this.progressIndicator == null || !(this.progressIndicator instanceof com.navdy.hud.app.ui.component.carousel.ProgressIndicator)) {
                    removeAllViews();
                    this.progressIndicator = new com.navdy.hud.app.ui.component.carousel.ProgressIndicator(getContext());
                    ((com.navdy.hud.app.ui.component.carousel.ProgressIndicator) this.progressIndicator).setProperties(this.roundRadius, this.itemRadius, this.itemPadding, this.currentItemPaddingRadius, defaultColor, -1, this.fullBackground, this.viewPadding, this.barSize, this.barParentSize);
                    this.progressIndicator.setOrientation(this.orientation);
                    android.content.res.Resources resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
                    if (this.barSize == -1) {
                        if (this.orientation == com.navdy.hud.app.ui.component.carousel.CarouselIndicator.Orientation.HORIZONTAL) {
                            height = (int) resources.getDimension(com.navdy.hud.app.R.dimen.carousel_progress_indicator_h);
                            width = (int) resources.getDimension(com.navdy.hud.app.R.dimen.carousel_progress_indicator_w);
                        } else {
                            width = (int) resources.getDimension(com.navdy.hud.app.R.dimen.carousel_progress_indicator_vertical_w);
                            height = (int) resources.getDimension(com.navdy.hud.app.R.dimen.carousel_progress_indicator_vertical_h);
                        }
                        lytParams = new android.widget.FrameLayout.LayoutParams(width, height);
                    } else {
                        lytParams = new android.widget.FrameLayout.LayoutParams(-1, -1);
                    }
                    lytParams.gravity = 17;
                    addView((android.view.View) this.progressIndicator, lytParams);
                }
                this.progressIndicator.setItemCount(n);
                return;
            }
            if (this.progressIndicator == null || !(this.progressIndicator instanceof com.navdy.hud.app.ui.component.carousel.CircleIndicator)) {
                removeAllViews();
                this.progressIndicator = new com.navdy.hud.app.ui.component.carousel.CircleIndicator(getContext());
                ((com.navdy.hud.app.ui.component.carousel.CircleIndicator) this.progressIndicator).setProperties(this.circleSize, this.circleFocusSize, this.circleMargin, defaultColor);
                this.progressIndicator.setOrientation(this.orientation);
                android.widget.FrameLayout.LayoutParams lytParams2 = new android.widget.FrameLayout.LayoutParams(-2, -2);
                lytParams2.gravity = 17;
                addView((android.view.View) this.progressIndicator, lytParams2);
            }
            this.progressIndicator.setItemCount(n);
        }
    }

    public void setCurrentItem(int n) {
        if (this.progressIndicator != null) {
            this.progressIndicator.setCurrentItem(n);
        }
    }

    public void setCurrentItem(int n, int color) {
        if (this.progressIndicator != null) {
            this.progressIndicator.setCurrentItem(n, color);
        }
    }

    public int getCurrentItem() {
        if (this.progressIndicator != null) {
            return this.progressIndicator.getCurrentItem();
        }
        return -1;
    }

    public int getItemCount() {
        return this.itemCount;
    }

    public android.animation.AnimatorSet getItemMoveAnimator(int toPos, int color) {
        if (this.progressIndicator != null) {
            return this.progressIndicator.getItemMoveAnimator(toPos, color);
        }
        return null;
    }

    public android.graphics.RectF getItemPos(int n) {
        if (this.progressIndicator != null) {
            return this.progressIndicator.getItemPos(n);
        }
        return null;
    }
}
