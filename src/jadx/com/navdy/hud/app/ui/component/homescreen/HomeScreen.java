package com.navdy.hud.app.ui.component.homescreen;

@flow.Layout(2130903117)
public class HomeScreen extends com.navdy.hud.app.screen.BaseScreen {
    /* access modifiers changed from: private */
    public static com.navdy.hud.app.ui.component.homescreen.HomeScreen.Presenter presenter;

    public enum DisplayMode {
        MAP,
        SMART_DASH
    }

    @dagger.Module(addsTo = com.navdy.hud.app.ui.activity.Main.Module.class, injects = {com.navdy.hud.app.ui.component.homescreen.HomeScreenView.class})
    public class Module {
        public Module() {
        }
    }

    @javax.inject.Singleton
    public static class Presenter extends com.navdy.hud.app.ui.framework.BasePresenter<com.navdy.hud.app.ui.component.homescreen.HomeScreenView> {
        private static final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.homescreen.HomeScreen.Presenter.class);
        @javax.inject.Inject
        com.squareup.otto.Bus bus;

        public void onLoad(android.os.Bundle savedInstanceState) {
            com.navdy.hud.app.ui.component.homescreen.HomeScreen.presenter = this;
        }

        /* access modifiers changed from: protected */
        public void onUnload() {
            com.navdy.hud.app.ui.component.homescreen.HomeScreen.presenter = null;
        }

        /* access modifiers changed from: 0000 */
        public com.navdy.hud.app.ui.component.homescreen.HomeScreenView getHomeScreenView() {
            return (com.navdy.hud.app.ui.component.homescreen.HomeScreenView) getView();
        }

        /* access modifiers changed from: 0000 */
        public void setDisplayMode(com.navdy.hud.app.ui.component.homescreen.HomeScreen.DisplayMode mode) {
            com.navdy.hud.app.ui.component.homescreen.HomeScreenView view = getHomeScreenView();
            if (view != null) {
                view.setDisplayMode(mode);
            }
        }

        /* access modifiers changed from: 0000 */
        public com.navdy.hud.app.ui.component.homescreen.HomeScreen.DisplayMode getDisplayMode() {
            com.navdy.hud.app.ui.component.homescreen.HomeScreenView view = getHomeScreenView();
            if (view != null) {
                return view.getDisplayMode();
            }
            return null;
        }
    }

    public java.lang.String getMortarScopeName() {
        return getClass().getName();
    }

    public java.lang.Object getDaggerModule() {
        return new com.navdy.hud.app.ui.component.homescreen.HomeScreen.Module();
    }

    public com.navdy.service.library.events.ui.Screen getScreen() {
        return com.navdy.service.library.events.ui.Screen.SCREEN_HOME;
    }

    public int getAnimationIn(flow.Flow.Direction direction) {
        return 17432576;
    }

    public int getAnimationOut(flow.Flow.Direction direction) {
        return 17432577;
    }

    public com.navdy.hud.app.ui.component.homescreen.HomeScreen.DisplayMode getDisplayMode() {
        if (presenter != null) {
            return presenter.getDisplayMode();
        }
        return null;
    }

    public void setDisplayMode(com.navdy.hud.app.ui.component.homescreen.HomeScreen.DisplayMode mode) {
        if (presenter != null) {
            presenter.setDisplayMode(mode);
        }
    }

    public com.navdy.hud.app.ui.component.homescreen.HomeScreenView getHomeScreenView() {
        if (presenter != null) {
            return presenter.getHomeScreenView();
        }
        return null;
    }
}
