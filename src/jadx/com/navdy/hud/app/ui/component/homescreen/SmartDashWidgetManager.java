package com.navdy.hud.app.ui.component.homescreen;

public class SmartDashWidgetManager {
    public static final java.lang.String ANALOG_CLOCK_WIDGET_ID = "ANALOG_CLOCK_WIDGET";
    public static final java.lang.String CALENDAR_WIDGET_ID = "CALENDAR_WIDGET";
    public static final java.lang.String COMPASS_WIDGET_ID = "COMPASS_WIDGET";
    public static final java.lang.String DIGITAL_CLOCK_2_WIDGET_ID = "DIGITAL_CLOCK_2_WIDGET";
    public static final java.lang.String DIGITAL_CLOCK_WIDGET_ID = "DIGITAL_CLOCK_WIDGET";
    public static final java.lang.String DRIVE_SCORE_GAUGE_ID = "DRIVE_SCORE_GAUGE_ID";
    public static final java.lang.String EMPTY_WIDGET_ID = "EMPTY_WIDGET";
    public static final java.lang.String ENGINE_TEMPERATURE_GAUGE_ID = "ENGINE_TEMPERATURE_GAUGE_ID";
    public static final java.lang.String ETA_GAUGE_ID = "ETA_GAUGE_ID";
    public static final java.lang.String FUEL_GAUGE_ID = "FUEL_GAUGE_ID";
    private static java.util.List<java.lang.String> GAUGES = new java.util.ArrayList();
    private static java.util.HashSet<java.lang.String> GAUGE_NAME_LOOKUP = new java.util.HashSet<>();
    public static final java.lang.String GFORCE_WIDGET_ID = "GFORCE_WIDGET";
    public static final java.lang.String MPG_AVG_WIDGET_ID = "MPG_AVG_WIDGET";
    public static final java.lang.String MPG_GRAPH_WIDGET_ID = "MPG_GRAPH_WIDGET";
    public static final java.lang.String MUSIC_WIDGET_ID = "MUSIC_WIDGET";
    public static final java.lang.String PREFERENCE_GAUGE_ENABLED = "PREF_GAUGE_ENABLED_";
    public static final java.lang.String SPEED_LIMIT_SIGN_GAUGE_ID = "SPEED_LIMIT_SIGN_GAUGE_ID";
    public static final java.lang.String TRAFFIC_INCIDENT_GAUGE_ID = "TRAFFIC_INCIDENT_GAUGE_ID";
    public static final java.lang.String WEATHER_WIDGET_ID = "WEATHER_GRAPH_WIDGET";
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.class);
    private com.squareup.otto.Bus bus;
    private android.content.SharedPreferences currentUserPreferences;
    private com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.IWidgetFilter filter;
    private android.content.SharedPreferences globalPreferences;
    private com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.LifecycleEvent lastLifeCycleEvent = null;
    private android.content.Context mContext;
    private java.util.List<java.lang.String> mGaugeIds;
    private boolean mLoaded = false;
    private java.util.HashMap<java.lang.String, java.lang.Integer> mWidgetIndexMap;
    private java.util.HashMap<java.lang.Integer, com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.SmartDashWidgetCache> mWidgetPresentersCache = new java.util.HashMap<>();

    public interface IWidgetFilter {
        boolean filter(java.lang.String str);
    }

    public enum LifecycleEvent {
        PAUSE,
        RESUME
    }

    public enum Reload {
        RELOAD_CACHE,
        RELOADED
    }

    public class SmartDashWidgetCache {
        private android.content.Context mContext;
        private java.util.List<java.lang.String> mGaugeIds = new java.util.ArrayList();
        private java.util.HashMap<java.lang.String, java.lang.Integer> mWidgetIndexMap = new java.util.HashMap<>();
        private java.util.HashMap<java.lang.String, com.navdy.hud.app.view.DashboardWidgetPresenter> mWidgetPresentersMap = new java.util.HashMap<>();

        public SmartDashWidgetCache(android.content.Context context) {
            this.mContext = context;
        }

        public void add(java.lang.String identifier) {
            if (!this.mWidgetIndexMap.containsKey(identifier)) {
                this.mGaugeIds.add(identifier);
                this.mWidgetIndexMap.put(identifier, java.lang.Integer.valueOf(this.mGaugeIds.size() - 1));
                this.mWidgetPresentersMap.put(identifier, com.navdy.hud.app.ui.component.homescreen.DashWidgetPresenterFactory.createDashWidgetPresenter(this.mContext, identifier));
                initializeWidget(identifier);
            }
        }

        public int getIndexForWidget(java.lang.String identifier) {
            if (this.mWidgetIndexMap.containsKey(identifier)) {
                return ((java.lang.Integer) this.mWidgetIndexMap.get(identifier)).intValue();
            }
            return -1;
        }

        public com.navdy.hud.app.view.DashboardWidgetPresenter getWidgetPresenter(int index) {
            if (index < 0 || index >= this.mGaugeIds.size()) {
                return null;
            }
            return (com.navdy.hud.app.view.DashboardWidgetPresenter) this.mWidgetPresentersMap.get((java.lang.String) this.mGaugeIds.get(index));
        }

        public com.navdy.hud.app.view.DashboardWidgetPresenter getWidgetPresenter(java.lang.String identifier) {
            return getWidgetPresenter(getIndexForWidget(identifier));
        }

        public int getWidgetsCount() {
            return this.mGaugeIds.size();
        }

        private void initializeWidget(java.lang.String identifier) {
            char c = 65535;
            switch (identifier.hashCode()) {
                case -131933527:
                    if (identifier.equals(com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.ENGINE_TEMPERATURE_GAUGE_ID)) {
                        c = 2;
                        break;
                    }
                    break;
                case 460083427:
                    if (identifier.equals(com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.DRIVE_SCORE_GAUGE_ID)) {
                        c = 1;
                        break;
                    }
                    break;
                case 1168400042:
                    if (identifier.equals(com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.FUEL_GAUGE_ID)) {
                        c = 0;
                        break;
                    }
                    break;
            }
            switch (c) {
                case 0:
                    com.navdy.hud.app.view.FuelGaugePresenter2 fuelGaugePresenter2 = (com.navdy.hud.app.view.FuelGaugePresenter2) getWidgetPresenter(identifier);
                    int fuelLevel = com.navdy.hud.app.obd.ObdManager.getInstance().getFuelLevel();
                    if (fuelGaugePresenter2 != null) {
                        fuelGaugePresenter2.setFuelLevel(fuelLevel);
                        return;
                    }
                    return;
                case 1:
                    com.navdy.hud.app.view.DriveScoreGaugePresenter driveScoreGaugePresenter = (com.navdy.hud.app.view.DriveScoreGaugePresenter) getWidgetPresenter(identifier);
                    if (driveScoreGaugePresenter != null) {
                        driveScoreGaugePresenter.setDriveScoreUpdated(com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getTelemetryDataManager().getDriveScoreUpdatedEvent());
                        return;
                    }
                    return;
                case 2:
                    com.navdy.hud.app.view.EngineTemperaturePresenter engineTemperaturePresenter = (com.navdy.hud.app.view.EngineTemperaturePresenter) getWidgetPresenter(identifier);
                    double engineTemperature = com.navdy.hud.app.obd.ObdManager.getInstance().getPidValue(5);
                    if (engineTemperaturePresenter != null) {
                        if (engineTemperature == -1.0d) {
                            engineTemperature = com.navdy.hud.app.view.EngineTemperaturePresenter.Companion.getTEMPERATURE_GAUGE_MID_POINT_CELSIUS();
                        }
                        engineTemperaturePresenter.setEngineTemperature(engineTemperature);
                        return;
                    }
                    return;
                default:
                    return;
            }
        }

        public void updateWidget(java.lang.String identifier, java.lang.Object data) {
            if (!android.text.TextUtils.isEmpty(identifier)) {
                char c = 65535;
                switch (identifier.hashCode()) {
                    case -1874661839:
                        if (identifier.equals(com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.COMPASS_WIDGET_ID)) {
                            c = 1;
                            break;
                        }
                        break;
                    case -1158963060:
                        if (identifier.equals(com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.MPG_AVG_WIDGET_ID)) {
                            c = 2;
                            break;
                        }
                        break;
                    case -131933527:
                        if (identifier.equals(com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.ENGINE_TEMPERATURE_GAUGE_ID)) {
                            c = 4;
                            break;
                        }
                        break;
                    case 1119776967:
                        if (identifier.equals(com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.SPEED_LIMIT_SIGN_GAUGE_ID)) {
                            c = 3;
                            break;
                        }
                        break;
                    case 1168400042:
                        if (identifier.equals(com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.FUEL_GAUGE_ID)) {
                            c = 0;
                            break;
                        }
                        break;
                }
                switch (c) {
                    case 0:
                        com.navdy.hud.app.view.FuelGaugePresenter2 fuelGaugePresenter2 = (com.navdy.hud.app.view.FuelGaugePresenter2) getWidgetPresenter(identifier);
                        if (fuelGaugePresenter2 != null && (data instanceof java.lang.Double)) {
                            fuelGaugePresenter2.setFuelLevel(((java.lang.Number) data).intValue());
                            return;
                        }
                        return;
                    case 1:
                        com.navdy.hud.app.view.CompassPresenter compassPresenter = (com.navdy.hud.app.view.CompassPresenter) getWidgetPresenter(identifier);
                        if (compassPresenter != null) {
                            compassPresenter.setHeadingAngle(((java.lang.Number) data).doubleValue());
                            return;
                        }
                        return;
                    case 2:
                        com.navdy.hud.app.view.MPGGaugePresenter mpgGaugePresenter2 = (com.navdy.hud.app.view.MPGGaugePresenter) getWidgetPresenter(identifier);
                        if (mpgGaugePresenter2 != null) {
                            mpgGaugePresenter2.setCurrentMPG(((java.lang.Number) data).doubleValue());
                            return;
                        }
                        return;
                    case 3:
                        com.navdy.hud.app.view.SpeedLimitSignPresenter speedLimitSignPresenter = (com.navdy.hud.app.view.SpeedLimitSignPresenter) getWidgetPresenter(identifier);
                        if (speedLimitSignPresenter != null) {
                            speedLimitSignPresenter.setSpeedLimit(((java.lang.Number) data).intValue());
                            return;
                        }
                        return;
                    case 4:
                        com.navdy.hud.app.view.EngineTemperaturePresenter engineTemperaturePresenter = (com.navdy.hud.app.view.EngineTemperaturePresenter) getWidgetPresenter(identifier);
                        if (engineTemperaturePresenter != null) {
                            engineTemperaturePresenter.setEngineTemperature((double) ((java.lang.Number) data).intValue());
                            return;
                        }
                        return;
                    default:
                        return;
                }
            }
        }

        public java.util.HashMap<java.lang.String, com.navdy.hud.app.view.DashboardWidgetPresenter> getWidgetPresentersMap() {
            return this.mWidgetPresentersMap;
        }

        public void clear() {
            this.mGaugeIds.clear();
            this.mWidgetPresentersMap.clear();
            this.mWidgetIndexMap.clear();
        }
    }

    public static class UserPreferenceChanged {
    }

    static {
        GAUGES.add(CALENDAR_WIDGET_ID);
        GAUGES.add(COMPASS_WIDGET_ID);
        GAUGES.add(ANALOG_CLOCK_WIDGET_ID);
        GAUGES.add(DIGITAL_CLOCK_WIDGET_ID);
        GAUGES.add(DIGITAL_CLOCK_2_WIDGET_ID);
        GAUGES.add(DRIVE_SCORE_GAUGE_ID);
        GAUGES.add(ENGINE_TEMPERATURE_GAUGE_ID);
        GAUGES.add(FUEL_GAUGE_ID);
        GAUGES.add(GFORCE_WIDGET_ID);
        GAUGES.add(MUSIC_WIDGET_ID);
        GAUGES.add(MPG_AVG_WIDGET_ID);
        GAUGES.add(SPEED_LIMIT_SIGN_GAUGE_ID);
        GAUGES.add(TRAFFIC_INCIDENT_GAUGE_ID);
        GAUGES.add(ETA_GAUGE_ID);
        GAUGES.add(EMPTY_WIDGET_ID);
        for (java.lang.String name : GAUGES) {
            GAUGE_NAME_LOOKUP.add(name);
        }
    }

    public boolean isGaugeOn(java.lang.String gaugeIdentifier) {
        if (this.currentUserPreferences == null) {
            sLogger.d("isGaugeOn : Current Preferences is not set, using the global preferences");
            this.currentUserPreferences = this.globalPreferences;
        }
        return this.currentUserPreferences.getBoolean(PREFERENCE_GAUGE_ENABLED + gaugeIdentifier, true);
    }

    public void setGaugeOn(java.lang.String identifier, boolean on) {
        if (this.currentUserPreferences == null) {
            sLogger.d("setGaugeOn : Current Preferences is not set, using the global preferences");
            this.currentUserPreferences = this.globalPreferences;
        }
        this.currentUserPreferences.edit().putBoolean(PREFERENCE_GAUGE_ENABLED + identifier, on).apply();
    }

    public static boolean isValidGaugeId(java.lang.String gaugeName) {
        return !android.text.TextUtils.isEmpty(gaugeName) && GAUGE_NAME_LOOKUP.contains(gaugeName);
    }

    public SmartDashWidgetManager(android.content.SharedPreferences globalPreferences2, android.content.Context context) {
        this.globalPreferences = globalPreferences2;
        this.mContext = context;
        this.mGaugeIds = new java.util.ArrayList();
        this.mWidgetIndexMap = new java.util.HashMap<>();
        this.bus = new com.squareup.otto.Bus();
    }

    public void reLoadAvailableWidgets(boolean loadAll) {
        sLogger.v("reLoadAvailableWidgets");
        this.mGaugeIds.clear();
        this.mWidgetIndexMap.clear();
        boolean isDefaultProfile = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile().isDefaultProfile();
        sLogger.d("Default profile ? " + isDefaultProfile);
        this.currentUserPreferences = isDefaultProfile ? this.globalPreferences : com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getDriverProfileManager().getLocalPreferencesForCurrentDriverProfile(com.navdy.hud.app.HudApplication.getAppContext());
        int i = 0;
        for (java.lang.String identifier : GAUGES) {
            if ((loadAll || isGaugeOn(identifier)) && this.filter != null && !this.filter.filter(identifier)) {
                this.mGaugeIds.add(identifier);
                sLogger.d("adding Gauge : " + identifier + ", at : " + i);
                int i2 = i + 1;
                this.mWidgetIndexMap.put(identifier, java.lang.Integer.valueOf(i));
                i = i2;
            } else {
                sLogger.d("Not adding Gauge, as its filtered out , ID : " + identifier);
            }
        }
        if (this.mGaugeIds.size() == 0) {
            this.mGaugeIds.add(EMPTY_WIDGET_ID);
            this.mWidgetIndexMap.put(EMPTY_WIDGET_ID, java.lang.Integer.valueOf(0));
        }
        if (this.mLoaded) {
            this.bus.post(com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.Reload.RELOAD_CACHE);
            this.bus.post(com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.Reload.RELOADED);
            return;
        }
        this.mLoaded = true;
    }

    public int getIndexForWidgetIdentifier(java.lang.String identifier) {
        if (this.mWidgetIndexMap.containsKey(identifier)) {
            return ((java.lang.Integer) this.mWidgetIndexMap.get(identifier)).intValue();
        }
        return -1;
    }

    public java.lang.String getWidgetIdentifierForIndex(int index) {
        if (index < 0 || index >= this.mGaugeIds.size()) {
            return null;
        }
        return (java.lang.String) this.mGaugeIds.get(index);
    }

    public void registerForChanges(java.lang.Object object) {
        this.bus.register(object);
    }

    public com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.SmartDashWidgetCache buildSmartDashWidgetCache(int clientIdentifier) {
        com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.SmartDashWidgetCache cache;
        if (this.mWidgetPresentersCache.containsKey(java.lang.Integer.valueOf(clientIdentifier))) {
            cache = (com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.SmartDashWidgetCache) this.mWidgetPresentersCache.get(java.lang.Integer.valueOf(clientIdentifier));
            cache.clear();
            sLogger.v("widget::: cache cleared for " + clientIdentifier);
        } else {
            cache = new com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.SmartDashWidgetCache(this.mContext);
            this.mWidgetPresentersCache.put(java.lang.Integer.valueOf(clientIdentifier), cache);
            sLogger.v("widget::: cache created for " + clientIdentifier);
        }
        for (java.lang.String gaugeId : this.mGaugeIds) {
            cache.add(gaugeId);
        }
        if (this.lastLifeCycleEvent != null) {
            sendLifecycleEvent(this.lastLifeCycleEvent, cache.getWidgetPresentersMap().values().iterator());
        }
        return cache;
    }

    public void updateWidget(java.lang.String identifier, java.lang.Object data) {
        java.util.Collection<com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.SmartDashWidgetCache> cacheCollection = this.mWidgetPresentersCache.values();
        if (cacheCollection != null) {
            for (com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.SmartDashWidgetCache cache : cacheCollection) {
                if (cache != null) {
                    cache.updateWidget(identifier, data);
                }
            }
        }
    }

    public void onPause() {
        this.lastLifeCycleEvent = com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.LifecycleEvent.PAUSE;
        sendLifecycleEvent(com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.LifecycleEvent.PAUSE);
    }

    public void onResume() {
        this.lastLifeCycleEvent = com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.LifecycleEvent.RESUME;
        sendLifecycleEvent(com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.LifecycleEvent.RESUME);
    }

    private void sendLifecycleEvent(com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.LifecycleEvent event) {
        for (com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.SmartDashWidgetCache cache : this.mWidgetPresentersCache.values()) {
            sendLifecycleEvent(event, cache.getWidgetPresentersMap().values().iterator());
        }
    }

    private void sendLifecycleEvent(com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.LifecycleEvent event, java.util.Iterator<com.navdy.hud.app.view.DashboardWidgetPresenter> iterator) {
        while (iterator.hasNext()) {
            switch (event) {
                case PAUSE:
                    ((com.navdy.hud.app.view.DashboardWidgetPresenter) iterator.next()).onPause();
                    break;
                case RESUME:
                    ((com.navdy.hud.app.view.DashboardWidgetPresenter) iterator.next()).onResume();
                    break;
            }
        }
    }

    public void setFilter(com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.IWidgetFilter filter2) {
        this.filter = filter2;
    }
}
