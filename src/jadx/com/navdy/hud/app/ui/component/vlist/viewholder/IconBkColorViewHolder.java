package com.navdy.hud.app.ui.component.vlist.viewholder;

public class IconBkColorViewHolder extends com.navdy.hud.app.ui.component.vlist.viewholder.IconBaseViewHolder {
    private static final com.navdy.service.library.log.Logger sLogger = com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.sLogger;

    public static com.navdy.hud.app.ui.component.vlist.VerticalList.Model buildModel(int id, int icon, int iconSelectedColor, int iconDeselectedColor, int iconFluctuatorColor, java.lang.String title, java.lang.String subTitle) {
        return buildModel(id, icon, iconSelectedColor, iconDeselectedColor, iconFluctuatorColor, title, subTitle, null, com.navdy.hud.app.ui.component.image.IconColorImageView.IconShape.CIRCLE);
    }

    public static com.navdy.hud.app.ui.component.vlist.VerticalList.Model buildModel(int id, int icon, int iconSelectedColor, int iconDeselectedColor, int iconFluctuatorColor, java.lang.String title, java.lang.String subTitle, java.lang.String subTitle2) {
        return buildModel(id, icon, iconSelectedColor, iconDeselectedColor, iconFluctuatorColor, title, subTitle, subTitle2, com.navdy.hud.app.ui.component.image.IconColorImageView.IconShape.CIRCLE);
    }

    public static com.navdy.hud.app.ui.component.vlist.VerticalList.Model buildModel(int id, int icon, int iconSelectedColor, int iconDeselectedColor, int iconFluctuatorColor, java.lang.String title, java.lang.String subTitle, java.lang.String subTitle2, com.navdy.hud.app.ui.component.image.IconColorImageView.IconShape iconShape) {
        com.navdy.hud.app.ui.component.vlist.VerticalList.Model model = com.navdy.hud.app.ui.component.vlist.VerticalModelCache.getFromCache(com.navdy.hud.app.ui.component.vlist.VerticalList.ModelType.ICON_BKCOLOR);
        if (model == null) {
            model = new com.navdy.hud.app.ui.component.vlist.VerticalList.Model();
        }
        model.type = com.navdy.hud.app.ui.component.vlist.VerticalList.ModelType.ICON_BKCOLOR;
        model.id = id;
        model.icon = icon;
        model.iconSelectedColor = iconSelectedColor;
        model.iconDeselectedColor = iconDeselectedColor;
        model.iconFluctuatorColor = iconFluctuatorColor;
        model.title = title;
        model.subTitle = subTitle;
        model.subTitle2 = subTitle2;
        model.iconShape = iconShape;
        return model;
    }

    public static com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder buildViewHolder(android.view.ViewGroup parent, com.navdy.hud.app.ui.component.vlist.VerticalList vlist, android.os.Handler handler) {
        return new com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder(getLayout(parent, com.navdy.hud.app.R.layout.vlist_item, com.navdy.hud.app.R.layout.crossfade_image_bkcolor_lyt), vlist, handler);
    }

    private IconBkColorViewHolder(android.view.ViewGroup layout, com.navdy.hud.app.ui.component.vlist.VerticalList vlist, android.os.Handler handler) {
        super(layout, vlist, handler);
    }

    public com.navdy.hud.app.ui.component.vlist.VerticalList.ModelType getModelType() {
        return com.navdy.hud.app.ui.component.vlist.VerticalList.ModelType.ICON_BKCOLOR;
    }

    public void setItemState(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.State state, com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.AnimationType animation, int duration, boolean startFluctuator) {
        super.setItemState(state, animation, duration, startFluctuator);
        float iconScaleFactor = 0.0f;
        com.navdy.hud.app.ui.component.image.CrossFadeImageView.Mode mode = null;
        switch (state) {
            case SELECTED:
                iconScaleFactor = 1.0f;
                mode = com.navdy.hud.app.ui.component.image.CrossFadeImageView.Mode.BIG;
                break;
            case UNSELECTED:
                iconScaleFactor = 0.6f;
                mode = com.navdy.hud.app.ui.component.image.CrossFadeImageView.Mode.SMALL;
                break;
        }
        switch (animation) {
            case NONE:
            case INIT:
                this.imageContainer.setScaleX(iconScaleFactor);
                this.imageContainer.setScaleY(iconScaleFactor);
                this.crossFadeImageView.setMode(mode);
                return;
            case MOVE:
                android.animation.PropertyValuesHolder p1 = android.animation.PropertyValuesHolder.ofFloat(android.view.View.SCALE_X, new float[]{iconScaleFactor});
                android.animation.PropertyValuesHolder p2 = android.animation.PropertyValuesHolder.ofFloat(android.view.View.SCALE_Y, new float[]{iconScaleFactor});
                this.animatorSetBuilder.with(android.animation.ObjectAnimator.ofPropertyValuesHolder(this.imageContainer, new android.animation.PropertyValuesHolder[]{p1, p2}));
                if (this.crossFadeImageView.getMode() != mode) {
                    this.animatorSetBuilder.with(this.crossFadeImageView.getCrossFadeAnimator());
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void preBind(com.navdy.hud.app.ui.component.vlist.VerticalList.Model model, com.navdy.hud.app.ui.component.vlist.VerticalList.ModelState modelState) {
        super.preBind(model, modelState);
        ((com.navdy.hud.app.ui.component.image.IconColorImageView) this.crossFadeImageView.getBig()).setIconShape(model.iconShape);
        ((com.navdy.hud.app.ui.component.image.IconColorImageView) this.crossFadeImageView.getSmall()).setIconShape(model.iconShape);
    }

    public void bind(com.navdy.hud.app.ui.component.vlist.VerticalList.Model model, com.navdy.hud.app.ui.component.vlist.VerticalList.ModelState modelState) {
        super.bind(model, modelState);
        setIcon(model.icon, model.iconSelectedColor, model.iconDeselectedColor, modelState.updateImage, modelState.updateSmallImage);
    }

    private void setIcon(int icon, int iconSelectedColor, int iconDeselectedColor, boolean updateImage, boolean updateSmallImage) {
        com.navdy.hud.app.ui.component.image.IconColorImageView big = (com.navdy.hud.app.ui.component.image.IconColorImageView) this.crossFadeImageView.getBig();
        if (updateImage) {
            big.setDraw(true);
            big.setIcon(icon, iconSelectedColor, null, 0.83f);
        } else {
            big.setDraw(false);
        }
        com.navdy.hud.app.ui.component.image.IconColorImageView small = (com.navdy.hud.app.ui.component.image.IconColorImageView) this.crossFadeImageView.getSmall();
        if (updateSmallImage) {
            small.setDraw(true);
            small.setIcon(icon, iconDeselectedColor, null, 0.83f);
            return;
        }
        small.setDraw(false);
    }
}
