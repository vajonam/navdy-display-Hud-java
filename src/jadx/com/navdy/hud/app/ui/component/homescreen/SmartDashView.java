package com.navdy.hud.app.ui.component.homescreen;

public class SmartDashView extends com.navdy.hud.app.ui.component.homescreen.BaseSmartDashView implements com.navdy.hud.app.ui.component.dashboard.IDashboardOptionsAdapter {
    public static final int HEADING_EXPIRE_INTERVAL = 3000;
    public static final long MINIMUM_INTERVAL_BETWEEN_RECORDING_USER_PREFERENCE = java.util.concurrent.TimeUnit.MINUTES.toMillis(15);
    public static final float NAVIGATION_MODE_SCALE_FACTOR = 0.8f;
    private static final boolean SHOW_ETA = false;
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.homescreen.SmartDashView.class);
    private int activeModeGaugeMargin;
    /* access modifiers changed from: private */
    public java.lang.String activeRightGaugeId;
    /* access modifiers changed from: private */
    public java.lang.String activeleftGaugeId;
    @butterknife.InjectView(2131624372)
    android.widget.TextView etaAmPm;
    @butterknife.InjectView(2131624371)
    android.widget.TextView etaText;
    private int fullModeGaugeTopMargin;
    private android.content.SharedPreferences globalPreferences;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.util.HeadingDataUtil headingDataUtil = new com.navdy.hud.app.util.HeadingDataUtil();
    public boolean isShowingDriveScoreGauge;
    /* access modifiers changed from: private */
    public java.lang.String lastETA;
    /* access modifiers changed from: private */
    public java.lang.String lastETA_AmPm;
    private double lastHeading = 0.0d;
    private long lastHeadingSampleTime = 0;
    private long lastUserPreferenceRecordedTime = 0;
    @butterknife.InjectView(2131624370)
    android.view.ViewGroup mEtaLayout;
    com.navdy.hud.app.ui.component.homescreen.SmartDashView.GaugeViewPagerAdapter mLeftAdapter;
    @butterknife.InjectView(2131624366)
    com.navdy.hud.app.ui.component.dashboard.GaugeViewPager mLeftGaugeViewPager;
    @butterknife.InjectView(2131624368)
    com.navdy.hud.app.view.GaugeView mMiddleGaugeView;
    com.navdy.hud.app.ui.component.homescreen.SmartDashView.GaugeViewPagerAdapter mRightAdapter;
    @butterknife.InjectView(2131624369)
    com.navdy.hud.app.ui.component.dashboard.GaugeViewPager mRightGaugeViewPager;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager mSmartDashWidgetManager;
    private int middleGauge = 1;
    private int middleGaugeShrinkEarlyTbtOffset;
    private int middleGaugeShrinkMargin;
    private int middleGaugeShrinkModeMargin;
    private boolean paused;
    private int scrollableSide = 1;
    com.navdy.hud.app.view.SpeedometerGaugePresenter2 speedometerGaugePresenter2;
    com.navdy.hud.app.view.TachometerGaugePresenter tachometerPresenter;

    class Anon1 implements com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.IWidgetFilter {
        final /* synthetic */ com.navdy.hud.app.ui.component.homescreen.HomeScreenView val$homeScreenView;

        Anon1(com.navdy.hud.app.ui.component.homescreen.HomeScreenView homeScreenView) {
            this.val$homeScreenView = homeScreenView;
        }

        public boolean filter(java.lang.String identifier) {
            boolean z = true;
            android.content.SharedPreferences sharedPreferences = this.val$homeScreenView.getDriverPreferences();
            java.lang.String leftGaugeId = sharedPreferences.getString(com.navdy.hud.app.ui.component.dashboard.IDashboardOptionsAdapter.PREFERENCE_LEFT_GAUGE_ID, com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.ANALOG_CLOCK_WIDGET_ID);
            java.lang.String rightGaugeId = sharedPreferences.getString(com.navdy.hud.app.ui.component.dashboard.IDashboardOptionsAdapter.PREFERENCE_RIGHT_GAUGE_ID, com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.COMPASS_WIDGET_ID);
            if (!android.text.TextUtils.isEmpty(identifier) && (identifier.equals(leftGaugeId) || identifier.equals(rightGaugeId))) {
                return false;
            }
            char c = 65535;
            switch (identifier.hashCode()) {
                case -1158963060:
                    if (identifier.equals(com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.MPG_AVG_WIDGET_ID)) {
                        c = 3;
                        break;
                    }
                    break;
                case -131933527:
                    if (identifier.equals(com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.ENGINE_TEMPERATURE_GAUGE_ID)) {
                        c = 2;
                        break;
                    }
                    break;
                case 1168400042:
                    if (identifier.equals(com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.FUEL_GAUGE_ID)) {
                        c = 1;
                        break;
                    }
                    break;
                case 1872543020:
                    if (identifier.equals(com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.TRAFFIC_INCIDENT_GAUGE_ID)) {
                        c = 0;
                        break;
                    }
                    break;
            }
            switch (c) {
                case 0:
                    if (com.navdy.hud.app.maps.MapSettings.isTrafficDashWidgetsEnabled()) {
                        z = false;
                    }
                    return z;
                case 1:
                    com.navdy.obd.PidSet pidSet = com.navdy.hud.app.obd.ObdManager.getInstance().getSupportedPids();
                    if (pidSet == null || !pidSet.contains(47)) {
                        return true;
                    }
                    return false;
                case 2:
                    com.navdy.obd.PidSet pidSet2 = com.navdy.hud.app.obd.ObdManager.getInstance().getSupportedPids();
                    if (pidSet2 == null || !pidSet2.contains(5)) {
                        return true;
                    }
                    return false;
                case 3:
                    com.navdy.obd.PidSet pidSet3 = com.navdy.hud.app.obd.ObdManager.getInstance().getSupportedPids();
                    if (pidSet3 == null || !pidSet3.contains(256)) {
                        return true;
                    }
                    return false;
                default:
                    return false;
            }
        }
    }

    class Anon2 {
        final /* synthetic */ com.navdy.hud.app.ui.component.homescreen.HomeScreenView val$homeScreenView;

        Anon2(com.navdy.hud.app.ui.component.homescreen.HomeScreenView homeScreenView) {
            this.val$homeScreenView = homeScreenView;
        }

        @com.squareup.otto.Subscribe
        public void onGpsLocationChanged(android.location.Location location) {
            com.navdy.hud.app.ui.component.homescreen.SmartDashView.this.headingDataUtil.setHeading((double) location.getBearing());
            double newHeadingValue = com.navdy.hud.app.ui.component.homescreen.SmartDashView.this.headingDataUtil.getHeading();
            if (com.navdy.hud.app.ui.component.homescreen.SmartDashView.sLogger.isLoggable(2)) {
                com.navdy.hud.app.ui.component.homescreen.SmartDashView.sLogger.v("SmartDash New: Heading :" + newHeadingValue + ", " + com.navdy.hud.app.device.gps.GpsUtils.getHeadingDirection(newHeadingValue) + ", Provider :" + location.getProvider());
            }
            com.navdy.hud.app.ui.component.homescreen.SmartDashView.this.mSmartDashWidgetManager.updateWidget(com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.COMPASS_WIDGET_ID, java.lang.Double.valueOf(newHeadingValue));
        }

        @com.squareup.otto.Subscribe
        public void onLocationFixChangeEvent(com.navdy.hud.app.maps.MapEvents.LocationFix event) {
            if (!event.locationAvailable) {
                com.navdy.hud.app.ui.component.homescreen.SmartDashView.this.headingDataUtil.reset();
                com.navdy.hud.app.ui.component.homescreen.SmartDashView.this.mSmartDashWidgetManager.updateWidget(com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.COMPASS_WIDGET_ID, java.lang.Float.valueOf(0.0f));
            }
        }

        @com.squareup.otto.Subscribe
        public void onDriverProfileChanged(com.navdy.hud.app.event.DriverProfileChanged profileChanged) {
            this.val$homeScreenView.updateDriverPrefs();
            if (com.navdy.hud.app.ui.component.homescreen.SmartDashView.this.mSmartDashWidgetManager != null) {
                com.navdy.hud.app.ui.component.homescreen.SmartDashView.this.mSmartDashWidgetManager.reLoadAvailableWidgets(false);
            }
        }

        @com.squareup.otto.Subscribe
        public void updateETA(com.navdy.hud.app.maps.MapEvents.ManeuverDisplay event) {
            if (this.val$homeScreenView.isNavigationActive()) {
                if (event.etaDate == null) {
                    com.navdy.hud.app.ui.component.homescreen.SmartDashView.sLogger.w("etaDate is not set");
                    return;
                }
                if (!android.text.TextUtils.equals(event.eta, com.navdy.hud.app.ui.component.homescreen.SmartDashView.this.lastETA)) {
                    com.navdy.hud.app.ui.component.homescreen.SmartDashView.this.lastETA = event.eta;
                    com.navdy.hud.app.ui.component.homescreen.SmartDashView.this.etaText.setText(com.navdy.hud.app.ui.component.homescreen.SmartDashView.this.lastETA);
                }
                if (!android.text.TextUtils.equals(event.etaAmPm, com.navdy.hud.app.ui.component.homescreen.SmartDashView.this.lastETA_AmPm)) {
                    com.navdy.hud.app.ui.component.homescreen.SmartDashView.this.lastETA_AmPm = event.etaAmPm;
                    com.navdy.hud.app.ui.component.homescreen.SmartDashView.this.etaAmPm.setText(com.navdy.hud.app.ui.component.homescreen.SmartDashView.this.lastETA_AmPm);
                }
            }
        }

        @com.squareup.otto.Subscribe
        public void ObdStateChangeEvent(com.navdy.hud.app.obd.ObdManager.ObdConnectionStatusEvent event) {
            com.navdy.hud.app.obd.ObdManager obdManager = com.navdy.hud.app.obd.ObdManager.getInstance();
            if (com.navdy.hud.app.ui.component.homescreen.SmartDashView.this.tachometerPresenter != null) {
                int rpm = obdManager.getEngineRpm();
                if (rpm < 0) {
                    rpm = 0;
                }
                com.navdy.hud.app.ui.component.homescreen.SmartDashView.this.tachometerPresenter.setRPM(rpm);
            }
            if (com.navdy.hud.app.ui.component.homescreen.SmartDashView.this.mSmartDashWidgetManager != null) {
                com.navdy.hud.app.ui.component.homescreen.SmartDashView.this.mSmartDashWidgetManager.reLoadAvailableWidgets(false);
            }
        }

        @com.squareup.otto.Subscribe
        public void onSupportedPidEventsChange(com.navdy.hud.app.obd.ObdManager.ObdSupportedPidsChangedEvent event) {
            com.navdy.hud.app.ui.component.homescreen.SmartDashView.this.mSmartDashWidgetManager.reLoadAvailableWidgets(false);
        }

        @com.squareup.otto.Subscribe
        public void onUserGaugePreferencesChanged(com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.UserPreferenceChanged userPreferenceChanged) {
            com.navdy.hud.app.ui.component.homescreen.SmartDashView.this.mSmartDashWidgetManager.reLoadAvailableWidgets(false);
        }

        @com.squareup.otto.Subscribe
        public void onDashboardPreferences(com.navdy.service.library.events.preferences.DashboardPreferences dashboardPreferences) {
            if (dashboardPreferences != null) {
                if (dashboardPreferences.middleGauge != null) {
                    switch (com.navdy.hud.app.ui.component.homescreen.SmartDashView.Anon8.$SwitchMap$com$navdy$service$library$events$preferences$MiddleGauge[dashboardPreferences.middleGauge.ordinal()]) {
                        case 1:
                            com.navdy.hud.app.ui.component.homescreen.SmartDashView.this.savePreference(com.navdy.hud.app.ui.component.dashboard.IDashboardOptionsAdapter.PREFERENCE_MIDDLE_GAUGE, java.lang.Integer.valueOf(1));
                            break;
                        case 2:
                            com.navdy.hud.app.ui.component.homescreen.SmartDashView.this.savePreference(com.navdy.hud.app.ui.component.dashboard.IDashboardOptionsAdapter.PREFERENCE_MIDDLE_GAUGE, java.lang.Integer.valueOf(0));
                            break;
                    }
                }
                if (dashboardPreferences.scrollableSide != null) {
                    switch (com.navdy.hud.app.ui.component.homescreen.SmartDashView.Anon8.$SwitchMap$com$navdy$service$library$events$preferences$ScrollableSide[dashboardPreferences.scrollableSide.ordinal()]) {
                        case 1:
                            com.navdy.hud.app.ui.component.homescreen.SmartDashView.this.savePreference(com.navdy.hud.app.ui.component.dashboard.IDashboardOptionsAdapter.PREFERENCE_SCROLLABLE_SIDE, java.lang.Integer.valueOf(0));
                            break;
                        case 2:
                            com.navdy.hud.app.ui.component.homescreen.SmartDashView.this.savePreference(com.navdy.hud.app.ui.component.dashboard.IDashboardOptionsAdapter.PREFERENCE_SCROLLABLE_SIDE, java.lang.Integer.valueOf(1));
                            break;
                    }
                }
                if (com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.isValidGaugeId(dashboardPreferences.leftGaugeId)) {
                    com.navdy.hud.app.ui.component.homescreen.SmartDashView.this.savePreference(com.navdy.hud.app.ui.component.dashboard.IDashboardOptionsAdapter.PREFERENCE_LEFT_GAUGE_ID, dashboardPreferences.leftGaugeId);
                }
                if (com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.isValidGaugeId(dashboardPreferences.rightGaugeId)) {
                    com.navdy.hud.app.ui.component.homescreen.SmartDashView.this.savePreference(com.navdy.hud.app.ui.component.dashboard.IDashboardOptionsAdapter.PREFERENCE_RIGHT_GAUGE_ID, dashboardPreferences.rightGaugeId);
                }
                com.navdy.hud.app.ui.component.homescreen.SmartDashView.this.showViewsAccordingToUserPreferences();
            }
        }
    }

    class Anon3 implements com.navdy.hud.app.ui.component.dashboard.GaugeViewPager.ChangeListener {
        Anon3() {
        }

        public void onGaugeChanged(int position) {
            java.lang.String identifier = com.navdy.hud.app.ui.component.homescreen.SmartDashView.this.mSmartDashWidgetManager.getWidgetIdentifierForIndex(position);
            com.navdy.hud.app.ui.component.homescreen.SmartDashView.sLogger.d("onGaugeChanged, (Left) , Widget ID : " + identifier + " , Position : " + position);
            com.navdy.hud.app.ui.component.homescreen.SmartDashView.this.savePreference(com.navdy.hud.app.ui.component.dashboard.IDashboardOptionsAdapter.PREFERENCE_LEFT_GAUGE_ID, identifier);
            com.navdy.hud.app.ui.component.homescreen.SmartDashView.this.mRightAdapter.setExcludedPosition(position);
            com.navdy.hud.app.ui.component.homescreen.SmartDashView.this.mRightGaugeViewPager.updateState();
        }
    }

    class Anon4 implements com.navdy.hud.app.ui.component.dashboard.GaugeViewPager.ChangeListener {
        Anon4() {
        }

        public void onGaugeChanged(int position) {
            java.lang.String identifier = com.navdy.hud.app.ui.component.homescreen.SmartDashView.this.mSmartDashWidgetManager.getWidgetIdentifierForIndex(position);
            com.navdy.hud.app.ui.component.homescreen.SmartDashView.sLogger.d("onGaugeChanged, (Right) , Widget ID : " + identifier + " , Position : " + position);
            com.navdy.hud.app.ui.component.homescreen.SmartDashView.this.savePreference(com.navdy.hud.app.ui.component.dashboard.IDashboardOptionsAdapter.PREFERENCE_RIGHT_GAUGE_ID, identifier);
            com.navdy.hud.app.ui.component.homescreen.SmartDashView.this.mLeftAdapter.setExcludedPosition(position);
            com.navdy.hud.app.ui.component.homescreen.SmartDashView.this.mLeftGaugeViewPager.updateState();
        }
    }

    class Anon5 implements android.animation.ValueAnimator.AnimatorUpdateListener {
        final /* synthetic */ android.view.ViewGroup.MarginLayoutParams val$marginParams;

        Anon5(android.view.ViewGroup.MarginLayoutParams marginLayoutParams) {
            this.val$marginParams = marginLayoutParams;
        }

        public void onAnimationUpdate(android.animation.ValueAnimator animation) {
            this.val$marginParams.leftMargin = ((java.lang.Integer) animation.getAnimatedValue()).intValue();
            com.navdy.hud.app.ui.component.homescreen.SmartDashView.this.mMiddleGaugeView.setLayoutParams(this.val$marginParams);
        }
    }

    class Anon6 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
        Anon6() {
        }

        public void onAnimationEnd(android.animation.Animator animation) {
            com.navdy.hud.app.ui.component.homescreen.SmartDashView.this.mLeftGaugeViewPager.setVisibility(0);
            com.navdy.hud.app.ui.component.homescreen.SmartDashView.this.mRightGaugeViewPager.setVisibility(0);
            if (com.navdy.hud.app.ui.component.homescreen.SmartDashView.this.homeScreenView.isNavigationActive()) {
                com.navdy.hud.app.ui.component.homescreen.SmartDashView.this.showEta(true);
            }
        }
    }

    class Anon7 implements android.animation.ValueAnimator.AnimatorUpdateListener {
        final /* synthetic */ android.view.ViewGroup.MarginLayoutParams val$marginParams;

        Anon7(android.view.ViewGroup.MarginLayoutParams marginLayoutParams) {
            this.val$marginParams = marginLayoutParams;
        }

        public void onAnimationUpdate(android.animation.ValueAnimator animation) {
            this.val$marginParams.leftMargin = ((java.lang.Integer) animation.getAnimatedValue()).intValue();
            com.navdy.hud.app.ui.component.homescreen.SmartDashView.this.mMiddleGaugeView.setLayoutParams(this.val$marginParams);
        }
    }

    static /* synthetic */ class Anon8 {
        static final /* synthetic */ int[] $SwitchMap$com$navdy$service$library$events$preferences$MiddleGauge = new int[com.navdy.service.library.events.preferences.MiddleGauge.values().length];
        static final /* synthetic */ int[] $SwitchMap$com$navdy$service$library$events$preferences$ScrollableSide = new int[com.navdy.service.library.events.preferences.ScrollableSide.values().length];

        static {
            $SwitchMap$com$navdy$hud$app$view$MainView$CustomAnimationMode = new int[com.navdy.hud.app.view.MainView.CustomAnimationMode.values().length];
            try {
                $SwitchMap$com$navdy$hud$app$view$MainView$CustomAnimationMode[com.navdy.hud.app.view.MainView.CustomAnimationMode.EXPAND.ordinal()] = 1;
            } catch (java.lang.NoSuchFieldError e) {
            }
            try {
                $SwitchMap$com$navdy$hud$app$view$MainView$CustomAnimationMode[com.navdy.hud.app.view.MainView.CustomAnimationMode.SHRINK_LEFT.ordinal()] = 2;
            } catch (java.lang.NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$com$navdy$hud$app$view$MainView$CustomAnimationMode[com.navdy.hud.app.view.MainView.CustomAnimationMode.SHRINK_MODE.ordinal()] = 3;
            } catch (java.lang.NoSuchFieldError e3) {
            }
            $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent = new int[com.navdy.hud.app.manager.InputManager.CustomKeyEvent.values().length];
            try {
                $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[com.navdy.hud.app.manager.InputManager.CustomKeyEvent.LEFT.ordinal()] = 1;
            } catch (java.lang.NoSuchFieldError e4) {
            }
            try {
                $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[com.navdy.hud.app.manager.InputManager.CustomKeyEvent.RIGHT.ordinal()] = 2;
            } catch (java.lang.NoSuchFieldError e5) {
            }
            try {
                $SwitchMap$com$navdy$service$library$events$preferences$ScrollableSide[com.navdy.service.library.events.preferences.ScrollableSide.LEFT.ordinal()] = 1;
            } catch (java.lang.NoSuchFieldError e6) {
            }
            try {
                $SwitchMap$com$navdy$service$library$events$preferences$ScrollableSide[com.navdy.service.library.events.preferences.ScrollableSide.RIGHT.ordinal()] = 2;
            } catch (java.lang.NoSuchFieldError e7) {
            }
            try {
                $SwitchMap$com$navdy$service$library$events$preferences$MiddleGauge[com.navdy.service.library.events.preferences.MiddleGauge.SPEEDOMETER.ordinal()] = 1;
            } catch (java.lang.NoSuchFieldError e8) {
            }
            try {
                $SwitchMap$com$navdy$service$library$events$preferences$MiddleGauge[com.navdy.service.library.events.preferences.MiddleGauge.TACHOMETER.ordinal()] = 2;
            } catch (java.lang.NoSuchFieldError e9) {
            }
        }
    }

    class GaugeViewPagerAdapter implements com.navdy.hud.app.ui.component.dashboard.GaugeViewPager.Adapter {
        private int mExcludedPosition = -1;
        private boolean mLeft;
        private com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager mSmartDashWidgetManager;
        private com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.SmartDashWidgetCache mWidgetCache;

        public GaugeViewPagerAdapter(com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager smartDashWidgetManager, boolean left) {
            this.mSmartDashWidgetManager = smartDashWidgetManager;
            this.mLeft = left;
            this.mWidgetCache = this.mSmartDashWidgetManager.buildSmartDashWidgetCache(this.mLeft ? 0 : 1);
            this.mSmartDashWidgetManager.registerForChanges(this);
        }

        @com.squareup.otto.Subscribe
        public void onWidgetsReload(com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.Reload reload) {
            if (reload == com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.Reload.RELOAD_CACHE) {
                this.mWidgetCache = this.mSmartDashWidgetManager.buildSmartDashWidgetCache(this.mLeft ? 0 : 1);
            }
        }

        public int getCount() {
            return this.mWidgetCache.getWidgetsCount();
        }

        public int getExcludedPosition() {
            return this.mExcludedPosition;
        }

        public void setExcludedPosition(int excludedPosition) {
            if (this.mSmartDashWidgetManager.getIndexForWidgetIdentifier(com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.EMPTY_WIDGET_ID) == excludedPosition) {
                this.mExcludedPosition = -1;
            } else {
                this.mExcludedPosition = excludedPosition;
            }
        }

        public android.view.View getView(int position, android.view.View recycledView, android.view.ViewGroup parent, boolean isActive) {
            com.navdy.hud.app.view.DashboardWidgetView gaugeView;
            boolean z = false;
            if (recycledView == null) {
                gaugeView = new com.navdy.hud.app.view.DashboardWidgetView(parent.getContext());
                gaugeView.setLayoutParams(new android.view.ViewGroup.LayoutParams(parent.getResources().getDimensionPixelSize(com.navdy.hud.app.R.dimen.gauge_frame_width), parent.getResources().getDimensionPixelSize(com.navdy.hud.app.R.dimen.gauge_frame_height)));
            } else {
                gaugeView = (com.navdy.hud.app.view.DashboardWidgetView) recycledView;
            }
            com.navdy.hud.app.view.DashboardWidgetPresenter oldPresenter = (com.navdy.hud.app.view.DashboardWidgetPresenter) gaugeView.getTag();
            com.navdy.hud.app.view.DashboardWidgetPresenter presenter = this.mWidgetCache.getWidgetPresenter(position);
            if (!(oldPresenter == null || oldPresenter == presenter || oldPresenter.getWidgetView() != gaugeView)) {
                oldPresenter.setView(null, null);
            }
            if (presenter != null) {
                android.os.Bundle args = new android.os.Bundle();
                args.putInt(com.navdy.hud.app.view.DashboardWidgetPresenter.EXTRA_GRAVITY, this.mLeft ? 0 : 2);
                args.putBoolean(com.navdy.hud.app.view.DashboardWidgetPresenter.EXTRA_IS_ACTIVE, isActive);
                if (!isActive) {
                    presenter.setWidgetVisibleToUser(false);
                } else {
                    if (this.mLeft) {
                        com.navdy.hud.app.ui.component.homescreen.SmartDashView.this.activeleftGaugeId = presenter.getWidgetIdentifier();
                    } else {
                        com.navdy.hud.app.ui.component.homescreen.SmartDashView.this.activeRightGaugeId = presenter.getWidgetIdentifier();
                    }
                    com.navdy.hud.app.ui.component.homescreen.SmartDashView smartDashView = com.navdy.hud.app.ui.component.homescreen.SmartDashView.this;
                    if (com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.DRIVE_SCORE_GAUGE_ID.equals(com.navdy.hud.app.ui.component.homescreen.SmartDashView.this.activeleftGaugeId) || com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.DRIVE_SCORE_GAUGE_ID.equals(com.navdy.hud.app.ui.component.homescreen.SmartDashView.this.activeRightGaugeId)) {
                        z = true;
                    }
                    smartDashView.isShowingDriveScoreGauge = z;
                    presenter.setWidgetVisibleToUser(true);
                }
                presenter.setView(gaugeView, args);
            }
            gaugeView.setTag(presenter);
            return gaugeView;
        }

        public com.navdy.hud.app.view.DashboardWidgetPresenter getPresenter(int position) {
            return this.mWidgetCache.getWidgetPresenter(position);
        }
    }

    public SmartDashView(android.content.Context context) {
        super(context);
    }

    public SmartDashView(android.content.Context context, android.util.AttributeSet attrs) {
        super(context, attrs);
    }

    public SmartDashView(android.content.Context context, android.util.AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
        this.fullModeGaugeTopMargin = getResources().getDimensionPixelSize(com.navdy.hud.app.R.dimen.gauge_frame_top_margin_full_mode);
        this.activeModeGaugeMargin = getResources().getDimensionPixelSize(com.navdy.hud.app.R.dimen.gauge_frame_top_margin_active_mode);
    }

    public void init(com.navdy.hud.app.ui.component.homescreen.HomeScreenView homeScreenView) {
        this.globalPreferences = homeScreenView.globalPreferences;
        this.mSmartDashWidgetManager = new com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager(this.globalPreferences, getContext());
        android.content.SharedPreferences driverPreferences = homeScreenView.getDriverPreferences();
        this.mSmartDashWidgetManager.setFilter(new com.navdy.hud.app.ui.component.homescreen.SmartDashView.Anon1(homeScreenView));
        this.mSmartDashWidgetManager.registerForChanges(this);
        this.tachometerPresenter = new com.navdy.hud.app.view.TachometerGaugePresenter(getContext());
        this.speedometerGaugePresenter2 = new com.navdy.hud.app.view.SpeedometerGaugePresenter2(getContext());
        this.bus.register(new com.navdy.hud.app.ui.component.homescreen.SmartDashView.Anon2(homeScreenView));
        android.content.res.Resources resources = getResources();
        this.middleGaugeShrinkMargin = resources.getDimensionPixelSize(com.navdy.hud.app.R.dimen.middle_gauge_shrink_margin);
        this.middleGaugeShrinkModeMargin = resources.getDimensionPixelSize(com.navdy.hud.app.R.dimen.middle_gauge_shrink_mode_margin);
        this.middleGaugeShrinkEarlyTbtOffset = getResources().getDimensionPixelSize(com.navdy.hud.app.R.dimen.middle_gauge_shrink_early_tbt_offset);
        super.init(homeScreenView);
    }

    @com.squareup.otto.Subscribe
    public void onReload(com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.Reload reload) {
        if (reload == com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.Reload.RELOADED) {
            showViewsAccordingToUserPreferences();
        }
    }

    private void showMiddleGaugeAccordingToUserPreferences() {
        this.middleGauge = (com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile().isDefaultProfile() ? this.globalPreferences : this.homeScreenView.getDriverPreferences()).getInt(com.navdy.hud.app.ui.component.dashboard.IDashboardOptionsAdapter.PREFERENCE_MIDDLE_GAUGE, 1);
        sLogger.d("Middle Gauge : " + this.middleGauge);
        com.navdy.hud.app.obd.ObdManager obdManager = com.navdy.hud.app.obd.ObdManager.getInstance();
        switch (this.middleGauge) {
            case 0:
                if (obdManager.isConnected()) {
                    com.navdy.obd.PidSet supportedPids = obdManager.getSupportedPids();
                    if (supportedPids == null || !supportedPids.contains(12)) {
                        showSpeedometerGauge();
                        return;
                    } else {
                        showTachometer();
                        return;
                    }
                } else {
                    showSpeedometerGauge();
                    return;
                }
            case 1:
                showSpeedometerGauge();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: private */
    public void showViewsAccordingToUserPreferences() {
        sLogger.d("showViewsAccordingToUserPreferences");
        boolean isDefaultProfile = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile().isDefaultProfile();
        android.content.SharedPreferences sharedPreferences = isDefaultProfile ? this.globalPreferences : this.homeScreenView.getDriverPreferences();
        this.scrollableSide = sharedPreferences.getInt(com.navdy.hud.app.ui.component.dashboard.IDashboardOptionsAdapter.PREFERENCE_SCROLLABLE_SIDE, 1);
        sLogger.v("scrollableSide:" + this.scrollableSide);
        showMiddleGaugeAccordingToUserPreferences();
        java.lang.String leftGaugeId = sharedPreferences.getString(com.navdy.hud.app.ui.component.dashboard.IDashboardOptionsAdapter.PREFERENCE_LEFT_GAUGE_ID, com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.ANALOG_CLOCK_WIDGET_ID);
        int leftGaugeIndex = this.mSmartDashWidgetManager.getIndexForWidgetIdentifier(leftGaugeId);
        sLogger.d("Left Gauge ID : " + leftGaugeId + ", Index : " + leftGaugeIndex);
        if (leftGaugeIndex < 0) {
            leftGaugeIndex = this.mSmartDashWidgetManager.getIndexForWidgetIdentifier(com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.EMPTY_WIDGET_ID);
        }
        this.mLeftGaugeViewPager.setSelectedPosition(leftGaugeIndex);
        java.lang.String rightGaugeId = sharedPreferences.getString(com.navdy.hud.app.ui.component.dashboard.IDashboardOptionsAdapter.PREFERENCE_RIGHT_GAUGE_ID, com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.COMPASS_WIDGET_ID);
        int rightGaugeIndex = this.mSmartDashWidgetManager.getIndexForWidgetIdentifier(rightGaugeId);
        sLogger.d("Right Gauge ID : " + rightGaugeId + ", Index : " + rightGaugeIndex);
        if (rightGaugeIndex < 0) {
            rightGaugeIndex = this.mSmartDashWidgetManager.getIndexForWidgetIdentifier(com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.EMPTY_WIDGET_ID);
        }
        this.mRightGaugeViewPager.setSelectedPosition(rightGaugeIndex);
        if (!isDefaultProfile) {
            this.globalPreferences.edit().putInt(com.navdy.hud.app.ui.component.dashboard.IDashboardOptionsAdapter.PREFERENCE_MIDDLE_GAUGE, this.middleGauge).putInt(com.navdy.hud.app.ui.component.dashboard.IDashboardOptionsAdapter.PREFERENCE_SCROLLABLE_SIDE, this.scrollableSide).putString(com.navdy.hud.app.ui.component.dashboard.IDashboardOptionsAdapter.PREFERENCE_LEFT_GAUGE_ID, leftGaugeId).putString(com.navdy.hud.app.ui.component.dashboard.IDashboardOptionsAdapter.PREFERENCE_RIGHT_GAUGE_ID, rightGaugeId).apply();
        }
        this.mLeftAdapter.setExcludedPosition(rightGaugeIndex);
        this.mRightAdapter.setExcludedPosition(leftGaugeIndex);
        recordUsersWidgetPreference();
    }

    private void showTachometer() {
        this.speedometerGaugePresenter2.onPause();
        this.speedometerGaugePresenter2.setWidgetVisibleToUser(false);
        this.speedometerGaugePresenter2.setView(null);
        if (!this.paused) {
            this.tachometerPresenter.onResume();
        }
        this.tachometerPresenter.setWidgetVisibleToUser(true);
        this.tachometerPresenter.setView(this.mMiddleGaugeView);
        int engineRpm = com.navdy.hud.app.obd.ObdManager.getInstance().getEngineRpm();
        if (!(engineRpm == -1 || this.tachometerPresenter == null)) {
            this.tachometerPresenter.setRPM(engineRpm);
        }
        int speed = com.navdy.hud.app.manager.SpeedManager.getInstance().getCurrentSpeed();
        if (this.tachometerPresenter != null) {
            this.tachometerPresenter.setSpeed(speed);
        }
    }

    private void showSpeedometerGauge() {
        this.tachometerPresenter.onPause();
        this.tachometerPresenter.setWidgetVisibleToUser(false);
        this.tachometerPresenter.setView(null);
        if (!this.paused) {
            this.speedometerGaugePresenter2.onResume();
        }
        this.speedometerGaugePresenter2.setWidgetVisibleToUser(true);
        this.speedometerGaugePresenter2.setView(this.mMiddleGaugeView);
        this.speedometerGaugePresenter2.setSpeed(com.navdy.hud.app.manager.SpeedManager.getInstance().getCurrentSpeed());
    }

    /* access modifiers changed from: protected */
    public void initializeView() {
        super.initializeView();
        this.mSmartDashWidgetManager.reLoadAvailableWidgets(false);
        if (this.mLeftAdapter == null) {
            this.mLeftAdapter = new com.navdy.hud.app.ui.component.homescreen.SmartDashView.GaugeViewPagerAdapter(this.mSmartDashWidgetManager, true);
            this.mLeftGaugeViewPager.setAdapter(this.mLeftAdapter);
            this.mLeftGaugeViewPager.setChangeListener(new com.navdy.hud.app.ui.component.homescreen.SmartDashView.Anon3());
        }
        if (this.mRightAdapter == null) {
            this.mRightAdapter = new com.navdy.hud.app.ui.component.homescreen.SmartDashView.GaugeViewPagerAdapter(this.mSmartDashWidgetManager, false);
            this.mRightGaugeViewPager.setAdapter(this.mRightAdapter);
            this.mRightGaugeViewPager.setChangeListener(new com.navdy.hud.app.ui.component.homescreen.SmartDashView.Anon4());
        }
        showViewsAccordingToUserPreferences();
    }

    public boolean shouldShowTopViews() {
        return false;
    }

    public int getMiddleGaugeOption() {
        return this.middleGauge == 0 ? 1 : 0;
    }

    public void onTachoMeterSelected() {
        if (this.middleGauge != 0) {
            this.middleGauge = 0;
            savePreference(com.navdy.hud.app.ui.component.dashboard.IDashboardOptionsAdapter.PREFERENCE_MIDDLE_GAUGE, java.lang.Integer.valueOf(0));
            showMiddleGaugeAccordingToUserPreferences();
        }
    }

    public void onSpeedoMeterSelected() {
        if (this.middleGauge != 1) {
            this.middleGauge = 1;
            savePreference(com.navdy.hud.app.ui.component.dashboard.IDashboardOptionsAdapter.PREFERENCE_MIDDLE_GAUGE, java.lang.Integer.valueOf(1));
            showMiddleGaugeAccordingToUserPreferences();
        }
    }

    public int getCurrentScrollableSideOption() {
        return this.scrollableSide == 0 ? 1 : 0;
    }

    public void onScrollableSideOptionSelected() {
        switch (this.scrollableSide) {
            case 0:
                this.scrollableSide = 1;
                savePreference(com.navdy.hud.app.ui.component.dashboard.IDashboardOptionsAdapter.PREFERENCE_SCROLLABLE_SIDE, java.lang.Integer.valueOf(1));
                return;
            case 1:
                this.scrollableSide = 0;
                savePreference(com.navdy.hud.app.ui.component.dashboard.IDashboardOptionsAdapter.PREFERENCE_SCROLLABLE_SIDE, java.lang.Integer.valueOf(0));
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: private */
    public void savePreference(java.lang.String key, java.lang.Object value) {
        boolean isDefaultProfile = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile().isDefaultProfile();
        android.content.SharedPreferences sharedPreferences = this.homeScreenView.getDriverPreferences();
        if (!isDefaultProfile) {
            if (value instanceof java.lang.String) {
                sharedPreferences.edit().putString(key, (java.lang.String) value).apply();
            } else if (value instanceof java.lang.Integer) {
                sharedPreferences.edit().putInt(key, ((java.lang.Integer) value).intValue()).apply();
            }
        }
        if (value instanceof java.lang.String) {
            this.globalPreferences.edit().putString(key, (java.lang.String) value).apply();
        } else if (value instanceof java.lang.Integer) {
            this.globalPreferences.edit().putInt(key, ((java.lang.Integer) value).intValue()).apply();
        }
    }

    public boolean onKey(com.navdy.hud.app.manager.InputManager.CustomKeyEvent event) {
        com.navdy.hud.app.ui.component.dashboard.GaugeViewPager gaugeViewPager = this.scrollableSide == 1 ? this.mRightGaugeViewPager : this.mLeftGaugeViewPager;
        switch (event) {
            case LEFT:
                gaugeViewPager.moveNext();
                break;
            case RIGHT:
                gaugeViewPager.movePrevious();
                break;
        }
        return false;
    }

    public void ObdPidChangeEvent(com.navdy.hud.app.obd.ObdManager.ObdPidChangeEvent event) {
        super.ObdPidChangeEvent(event);
        switch (event.pid.getId()) {
            case 5:
                this.mSmartDashWidgetManager.updateWidget(com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.ENGINE_TEMPERATURE_GAUGE_ID, java.lang.Double.valueOf(event.pid.getValue()));
                return;
            case 12:
                if (this.tachometerPresenter != null) {
                    this.tachometerPresenter.setRPM((int) event.pid.getValue());
                    return;
                }
                return;
            case 47:
                this.mSmartDashWidgetManager.updateWidget(com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.FUEL_GAUGE_ID, java.lang.Double.valueOf(event.pid.getValue()));
                return;
            case 256:
                this.mSmartDashWidgetManager.updateWidget(com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.MPG_AVG_WIDGET_ID, java.lang.Double.valueOf(event.pid.getValue()));
                return;
            default:
                return;
        }
    }

    public void updateLayoutForMode(com.navdy.hud.app.maps.NavigationMode navigationMode, boolean forced) {
        boolean navigationActive = this.homeScreenView.isNavigationActive();
        sLogger.v("updateLayoutForMode:" + navigationMode + " forced=" + forced + " nav:" + navigationActive);
        if (navigationActive) {
            sLogger.v("updateLayoutForMode: active");
            ((android.view.ViewGroup.MarginLayoutParams) this.mLeftGaugeViewPager.getLayoutParams()).topMargin = this.activeModeGaugeMargin;
            ((android.view.ViewGroup.MarginLayoutParams) this.mRightGaugeViewPager.getLayoutParams()).topMargin = this.activeModeGaugeMargin;
            ((android.view.ViewGroup.MarginLayoutParams) this.mMiddleGaugeView.getLayoutParams()).topMargin = getResources().getDimensionPixelSize(com.navdy.hud.app.R.dimen.tachometer_tbt_mode_margin_top);
            this.mMiddleGaugeView.setScaleX(0.8f);
            this.mMiddleGaugeView.setScaleY(0.8f);
            this.mEtaLayout.setVisibility(0);
            invalidate();
            requestLayout();
        } else {
            sLogger.v("updateLayoutForMode: not active");
            ((android.view.ViewGroup.MarginLayoutParams) this.mLeftGaugeViewPager.getLayoutParams()).topMargin = this.fullModeGaugeTopMargin;
            ((android.view.ViewGroup.MarginLayoutParams) this.mRightGaugeViewPager.getLayoutParams()).topMargin = this.fullModeGaugeTopMargin;
            ((android.view.ViewGroup.MarginLayoutParams) this.mMiddleGaugeView.getLayoutParams()).topMargin = getResources().getDimensionPixelSize(com.navdy.hud.app.R.dimen.tachometer_full_mode_margin_top);
            this.mMiddleGaugeView.setScaleX(1.0f);
            this.mMiddleGaugeView.setScaleY(1.0f);
            this.mEtaLayout.setVisibility(8);
            invalidate();
            requestLayout();
        }
        showEta(navigationActive);
        invalidate();
        requestLayout();
    }

    public void adjustDashHeight(int height) {
        super.adjustDashHeight(height);
    }

    /* access modifiers changed from: protected */
    public void updateSpeed(int speed) {
        super.updateSpeed(speed);
        if (this.tachometerPresenter != null) {
            this.tachometerPresenter.setSpeed(speed);
        }
        if (this.speedometerGaugePresenter2 != null) {
            this.speedometerGaugePresenter2.setSpeed(speed);
        }
    }

    /* access modifiers changed from: protected */
    public void updateSpeedLimit(int speedLimit) {
        super.updateSpeedLimit(speedLimit);
        if (this.tachometerPresenter != null) {
            this.tachometerPresenter.setSpeedLimit(speedLimit);
        }
        if (this.speedometerGaugePresenter2 != null) {
            this.speedometerGaugePresenter2.setSpeedLimit(speedLimit);
        }
        this.mSmartDashWidgetManager.updateWidget(com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.SPEED_LIMIT_SIGN_GAUGE_ID, java.lang.Integer.valueOf(speedLimit));
    }

    /* access modifiers changed from: protected */
    public void animateToFullMode() {
        super.animateToFullMode();
    }

    /* access modifiers changed from: protected */
    public void animateToFullModeInternal(android.animation.AnimatorSet.Builder builder) {
        super.animateToFullModeInternal(builder);
    }

    public void setView(com.navdy.hud.app.view.MainView.CustomAnimationMode mode) {
        sLogger.v("setview: " + mode);
        super.setView(mode);
        switch (mode) {
            case EXPAND:
                this.mLeftGaugeViewPager.setVisibility(0);
                this.mRightGaugeViewPager.setVisibility(0);
                if (this.homeScreenView.isNavigationActive()) {
                    showEta(true);
                }
                ((android.view.ViewGroup.MarginLayoutParams) this.mMiddleGaugeView.getLayoutParams()).leftMargin = 0;
                return;
            case SHRINK_LEFT:
                this.mLeftGaugeViewPager.setVisibility(8);
                this.mRightGaugeViewPager.setVisibility(8);
                showEta(false);
                ((android.view.ViewGroup.MarginLayoutParams) this.mMiddleGaugeView.getLayoutParams()).leftMargin = getResources().getDimensionPixelSize(com.navdy.hud.app.R.dimen.middle_gauge_shrink_margin);
                return;
            default:
                return;
        }
    }

    public void getCustomAnimator(com.navdy.hud.app.view.MainView.CustomAnimationMode mode, android.animation.AnimatorSet.Builder mainBuilder) {
        int targetMargin;
        sLogger.v("getCustomAnimator" + mode);
        super.getCustomAnimator(mode, mainBuilder);
        android.animation.AnimatorSet set = new android.animation.AnimatorSet();
        switch (mode) {
            case EXPAND:
                android.view.ViewGroup.MarginLayoutParams marginParams = (android.view.ViewGroup.MarginLayoutParams) this.mMiddleGaugeView.getLayoutParams();
                int srcMargin = marginParams.leftMargin;
                android.animation.ValueAnimator valueAnimator = new android.animation.ValueAnimator();
                valueAnimator.setIntValues(new int[]{srcMargin, 0});
                valueAnimator.addUpdateListener(new com.navdy.hud.app.ui.component.homescreen.SmartDashView.Anon5(marginParams));
                valueAnimator.addListener(new com.navdy.hud.app.ui.component.homescreen.SmartDashView.Anon6());
                set.playTogether(new android.animation.Animator[]{valueAnimator});
                break;
            case SHRINK_LEFT:
            case SHRINK_MODE:
                this.mLeftGaugeViewPager.setVisibility(8);
                this.mRightGaugeViewPager.setVisibility(8);
                showEta(false);
                android.view.ViewGroup.MarginLayoutParams marginParams2 = (android.view.ViewGroup.MarginLayoutParams) this.mMiddleGaugeView.getLayoutParams();
                int srcMargin2 = marginParams2.leftMargin;
                if (mode == com.navdy.hud.app.view.MainView.CustomAnimationMode.SHRINK_LEFT) {
                    targetMargin = this.middleGaugeShrinkMargin;
                } else {
                    targetMargin = this.middleGaugeShrinkModeMargin;
                }
                android.animation.ValueAnimator valueAnimator2 = new android.animation.ValueAnimator();
                valueAnimator2.setIntValues(new int[]{srcMargin2, targetMargin});
                valueAnimator2.addUpdateListener(new com.navdy.hud.app.ui.component.homescreen.SmartDashView.Anon7(marginParams2));
                set.playTogether(new android.animation.Animator[]{valueAnimator2});
                break;
        }
        mainBuilder.with(set);
    }

    /* access modifiers changed from: private */
    public void showEta(boolean show) {
        if (this.mEtaLayout.getVisibility() == 0) {
            this.mEtaLayout.setVisibility(8);
        }
    }

    public void onPause() {
        if (!this.paused) {
            sLogger.v("::onPause");
            this.paused = true;
            if (this.mSmartDashWidgetManager != null) {
                sLogger.v("widgets disabled");
                this.mSmartDashWidgetManager.onPause();
                if (this.tachometerPresenter != null) {
                    this.tachometerPresenter.onPause();
                }
                if (this.speedometerGaugePresenter2 != null) {
                    this.speedometerGaugePresenter2.onPause();
                }
            }
        }
    }

    public void onResume() {
        if (this.paused) {
            sLogger.v("::onResume");
            this.paused = false;
            if (this.mSmartDashWidgetManager != null) {
                sLogger.v("widgets enabled");
                this.mSmartDashWidgetManager.onResume();
                if (this.tachometerPresenter != null && this.tachometerPresenter.isWidgetActive()) {
                    this.tachometerPresenter.onResume();
                }
                if (this.speedometerGaugePresenter2 != null && this.speedometerGaugePresenter2.isWidgetActive()) {
                    this.speedometerGaugePresenter2.onResume();
                }
            }
            recordUsersWidgetPreference();
        }
    }

    public void recordUsersWidgetPreference() {
        sLogger.d("Recording the users Widget Preference");
        long currentTime = android.os.SystemClock.elapsedRealtime();
        if (com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile().isDefaultProfile()) {
            return;
        }
        if (this.lastUserPreferenceRecordedTime <= 0 || currentTime - this.lastUserPreferenceRecordedTime >= MINIMUM_INTERVAL_BETWEEN_RECORDING_USER_PREFERENCE) {
            android.content.SharedPreferences sharedPreferences = this.homeScreenView.getDriverPreferences();
            java.lang.String leftGaugeId = sharedPreferences.getString(com.navdy.hud.app.ui.component.dashboard.IDashboardOptionsAdapter.PREFERENCE_LEFT_GAUGE_ID, com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.ANALOG_CLOCK_WIDGET_ID);
            java.lang.String rightGaugeId = sharedPreferences.getString(com.navdy.hud.app.ui.component.dashboard.IDashboardOptionsAdapter.PREFERENCE_RIGHT_GAUGE_ID, com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.COMPASS_WIDGET_ID);
            com.navdy.hud.app.analytics.AnalyticsSupport.recordDashPreference(leftGaugeId, true);
            com.navdy.hud.app.analytics.AnalyticsSupport.recordDashPreference(rightGaugeId, false);
            this.lastUserPreferenceRecordedTime = currentTime;
        }
    }

    public java.lang.String getActiveleftGaugeId() {
        return this.activeleftGaugeId;
    }

    public java.lang.String getActiveRightGaugeId() {
        return this.activeRightGaugeId;
    }
}
