package com.navdy.hud.app.ui.component.vlist.viewholder;

public class TitleSubtitleViewHolder extends com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder {
    private static final com.navdy.service.library.log.Logger sLogger = com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.sLogger;
    private android.widget.TextView subTitle;
    private android.widget.TextView title;

    public static com.navdy.hud.app.ui.component.vlist.VerticalList.Model buildModel(java.lang.String title2, java.lang.String subTitle2) {
        com.navdy.hud.app.ui.component.vlist.VerticalList.Model model = new com.navdy.hud.app.ui.component.vlist.VerticalList.Model();
        model.type = com.navdy.hud.app.ui.component.vlist.VerticalList.ModelType.TITLE_SUBTITLE;
        model.id = com.navdy.hud.app.R.id.vlist_title_subtitle;
        model.title = title2;
        model.subTitle = subTitle2;
        return model;
    }

    public static com.navdy.hud.app.ui.component.vlist.viewholder.TitleSubtitleViewHolder buildViewHolder(android.view.ViewGroup parent, com.navdy.hud.app.ui.component.vlist.VerticalList vlist, android.os.Handler handler) {
        return new com.navdy.hud.app.ui.component.vlist.viewholder.TitleSubtitleViewHolder((android.view.ViewGroup) android.view.LayoutInflater.from(parent.getContext()).inflate(com.navdy.hud.app.R.layout.vlist_title_subtitle, parent, false), vlist, handler);
    }

    public TitleSubtitleViewHolder(android.view.ViewGroup layout, com.navdy.hud.app.ui.component.vlist.VerticalList vlist, android.os.Handler handler) {
        super(layout, vlist, handler);
        this.title = (android.widget.TextView) layout.findViewById(com.navdy.hud.app.R.id.title);
        this.subTitle = (android.widget.TextView) layout.findViewById(com.navdy.hud.app.R.id.subTitle);
    }

    public com.navdy.hud.app.ui.component.vlist.VerticalList.ModelType getModelType() {
        return com.navdy.hud.app.ui.component.vlist.VerticalList.ModelType.TITLE_SUBTITLE;
    }

    public void setItemState(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.State state, com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.AnimationType animation, int duration, boolean startFluctuator) {
    }

    public void bind(com.navdy.hud.app.ui.component.vlist.VerticalList.Model model, com.navdy.hud.app.ui.component.vlist.VerticalList.ModelState modelState) {
        this.title.setText(model.title);
        this.subTitle.setText(model.subTitle);
    }

    public void clearAnimation() {
    }

    public void select(com.navdy.hud.app.ui.component.vlist.VerticalList.Model model, int pos, int duration) {
    }

    public void copyAndPosition(android.widget.ImageView imageC, android.widget.TextView titleC, android.widget.TextView subTitleC, android.widget.TextView subTitle2C, boolean setImage) {
    }
}
