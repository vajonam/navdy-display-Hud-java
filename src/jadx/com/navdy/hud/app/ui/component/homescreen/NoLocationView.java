package com.navdy.hud.app.ui.component.homescreen;

public class NoLocationView extends android.widget.RelativeLayout {
    private com.squareup.otto.Bus bus;
    private com.navdy.service.library.log.Logger logger;
    /* access modifiers changed from: private */
    public android.animation.ObjectAnimator noLocationAnimator;
    @butterknife.InjectView(2131624341)
    android.widget.ImageView noLocationImage;
    @butterknife.InjectView(2131624342)
    android.widget.LinearLayout noLocationTextContainer;

    class Anon1 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
        Anon1() {
        }

        public void onAnimationEnd(android.animation.Animator animation) {
            if (com.navdy.hud.app.ui.component.homescreen.NoLocationView.this.getVisibility() == 0) {
                com.navdy.hud.app.ui.component.homescreen.NoLocationView.this.noLocationAnimator.setStartDelay(33);
                com.navdy.hud.app.ui.component.homescreen.NoLocationView.this.noLocationAnimator.start();
            }
        }
    }

    public NoLocationView(android.content.Context context) {
        this(context, null);
    }

    public NoLocationView(android.content.Context context, android.util.AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public NoLocationView(android.content.Context context, android.util.AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        this.logger = com.navdy.hud.app.ui.component.homescreen.HomeScreenView.sLogger;
        super.onFinishInflate();
        butterknife.ButterKnife.inject((android.view.View) this);
        this.bus = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getBus();
        this.bus.register(this);
    }

    public boolean isAcquiringLocation() {
        return getVisibility() == 0;
    }

    public void hideLocationUI() {
        this.logger.v("hideLocationUI");
        setVisibility(8);
        if (this.noLocationAnimator != null) {
            this.logger.v("cancelled animation");
            this.noLocationAnimator.cancel();
            this.noLocationImage.setRotation(0.0f);
        }
    }

    public void showLocationUI() {
        this.logger.v("showLocationUI");
        setVisibility(0);
        if (this.noLocationAnimator == null) {
            this.noLocationAnimator = android.animation.ObjectAnimator.ofFloat(this.noLocationImage, android.view.View.ROTATION, new float[]{360.0f});
            this.noLocationAnimator.setDuration(500);
            this.noLocationAnimator.setInterpolator(new android.view.animation.AccelerateDecelerateInterpolator());
            this.noLocationAnimator.addListener(new com.navdy.hud.app.ui.component.homescreen.NoLocationView.Anon1());
        }
        this.noLocationAnimator.start();
    }

    public void setView(com.navdy.hud.app.view.MainView.CustomAnimationMode mode) {
        switch (mode) {
            case EXPAND:
                this.noLocationImage.setX((float) com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.noLocationImageX);
                this.noLocationTextContainer.setX((float) com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.noLocationTextX);
                return;
            case SHRINK_LEFT:
                this.noLocationImage.setX((float) com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.noLocationImageShrinkLeftX);
                this.noLocationTextContainer.setX((float) com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.noLocationTextShrinkLeftX);
                return;
            default:
                return;
        }
    }

    public void getCustomAnimator(com.navdy.hud.app.view.MainView.CustomAnimationMode mode, android.animation.AnimatorSet.Builder builder) {
        switch (mode) {
            case EXPAND:
                builder.with(com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getXPositionAnimator(this.noLocationImage, (float) com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.noLocationImageX));
                builder.with(com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getXPositionAnimator(this.noLocationTextContainer, (float) com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.noLocationTextX));
                return;
            case SHRINK_LEFT:
                builder.with(com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getXPositionAnimator(this.noLocationImage, (float) com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.noLocationImageShrinkLeftX));
                builder.with(com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getXPositionAnimator(this.noLocationTextContainer, (float) com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.noLocationTextShrinkLeftX));
                return;
            default:
                return;
        }
    }
}
