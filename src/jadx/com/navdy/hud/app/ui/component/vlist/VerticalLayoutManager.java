package com.navdy.hud.app.ui.component.vlist;

public class VerticalLayoutManager extends android.support.v7.widget.LinearLayoutManager {
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.vlist.VerticalLayoutManager.class);
    private com.navdy.hud.app.ui.component.vlist.VerticalList.Callback callback;
    private android.content.Context context;
    private boolean listLoaded;
    private com.navdy.hud.app.ui.component.vlist.VerticalRecyclerView recyclerView;
    private com.navdy.hud.app.ui.component.vlist.VerticalList vlist;

    private static class VerticalScroller extends android.support.v7.widget.LinearSmoothScroller {
        private android.view.animation.Interpolator interpolator = com.navdy.hud.app.ui.component.vlist.VerticalList.getInterpolator();
        private com.navdy.hud.app.ui.component.vlist.VerticalLayoutManager layoutManager;
        private int position;
        private com.navdy.hud.app.ui.component.vlist.VerticalRecyclerView recyclerView;
        private int snapPreference;
        private com.navdy.hud.app.ui.component.vlist.VerticalList vlist;

        public VerticalScroller(android.content.Context context, com.navdy.hud.app.ui.component.vlist.VerticalList vlist2, com.navdy.hud.app.ui.component.vlist.VerticalLayoutManager layoutManager2, com.navdy.hud.app.ui.component.vlist.VerticalRecyclerView recyclerView2, int position2, int snapPreference2) {
            super(context);
            this.vlist = vlist2;
            this.layoutManager = layoutManager2;
            this.recyclerView = recyclerView2;
            this.position = position2;
            this.snapPreference = snapPreference2;
        }

        public android.graphics.PointF computeScrollVectorForPosition(int targetPosition) {
            return this.layoutManager.computeScrollVectorForPosition(targetPosition);
        }

        /* access modifiers changed from: protected */
        public int getVerticalSnapPreference() {
            return this.snapPreference;
        }

        /* access modifiers changed from: protected */
        public void onStart() {
            super.onStart();
        }

        /* access modifiers changed from: protected */
        public void onStop() {
            super.onStop();
        }

        /* access modifiers changed from: protected */
        public int calculateTimeForScrolling(int dx) {
            return super.calculateTimeForScrolling(dx);
        }

        /* access modifiers changed from: protected */
        public int calculateTimeForDeceleration(int dx) {
            return this.vlist.animationDuration;
        }

        /* access modifiers changed from: protected */
        public void onSeekTargetStep(int dx, int dy, android.support.v7.widget.RecyclerView.State state, android.support.v7.widget.RecyclerView.SmoothScroller.Action action) {
            super.onSeekTargetStep(dx, dy, state, action);
        }

        /* access modifiers changed from: protected */
        public void updateActionForInterimTarget(android.support.v7.widget.RecyclerView.SmoothScroller.Action action) {
            super.updateActionForInterimTarget(action);
        }

        /* access modifiers changed from: protected */
        public void onTargetFound(android.view.View targetView, android.support.v7.widget.RecyclerView.State state, android.support.v7.widget.RecyclerView.SmoothScroller.Action action) {
            int dx = calculateDxToMakeVisible(targetView, getHorizontalSnapPreference());
            int dy = calculateDyToMakeVisible(targetView, getVerticalSnapPreference());
            int time = calculateTimeForDeceleration((int) java.lang.Math.sqrt((double) ((dx * dx) + (dy * dy))));
            if (time > 0) {
                action.update(-dx, -dy, time, this.interpolator);
            }
            if (com.navdy.hud.app.ui.component.vlist.VerticalLayoutManager.sLogger.isLoggable(2)) {
                com.navdy.hud.app.ui.component.vlist.VerticalLayoutManager.sLogger.v("target found dy=" + action.getDy());
            }
            this.vlist.targetFound(action.getDy());
        }

        /* access modifiers changed from: protected */
        public float calculateSpeedPerPixel(android.util.DisplayMetrics displayMetrics) {
            return super.calculateSpeedPerPixel(displayMetrics);
        }
    }

    public VerticalLayoutManager(android.content.Context context2, com.navdy.hud.app.ui.component.vlist.VerticalList vlist2, com.navdy.hud.app.ui.component.vlist.VerticalList.Callback callback2, com.navdy.hud.app.ui.component.vlist.VerticalRecyclerView recyclerView2) {
        super(context2);
        this.context = context2;
        this.vlist = vlist2;
        this.callback = callback2;
        this.recyclerView = recyclerView2;
    }

    public void smoothScrollToPosition(int position, int snapPreference) {
        com.navdy.hud.app.ui.component.vlist.VerticalLayoutManager.VerticalScroller verticalScroller = new com.navdy.hud.app.ui.component.vlist.VerticalLayoutManager.VerticalScroller(this.context, this.vlist, this, this.recyclerView, position, snapPreference);
        verticalScroller.setTargetPosition(position);
        startSmoothScroll(verticalScroller);
    }

    public void onLayoutChildren(android.support.v7.widget.RecyclerView.Recycler recycler, android.support.v7.widget.RecyclerView.State state) {
        super.onLayoutChildren(recycler, state);
        if (!this.listLoaded && !state.isPreLayout()) {
            this.listLoaded = true;
            this.callback.onLoad();
        }
    }

    public void clear() {
        this.listLoaded = false;
    }
}
