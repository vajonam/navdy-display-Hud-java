package com.navdy.hud.app.ui.component.mainmenu;

public class MainMenuView2$$ViewInjector {
    public static void inject(butterknife.ButterKnife.Finder finder, com.navdy.hud.app.ui.component.mainmenu.MainMenuView2 target, java.lang.Object source) {
        target.rightBackground = finder.findRequiredView(source, com.navdy.hud.app.R.id.rightBackground, "field 'rightBackground'");
        target.confirmationLayout = (com.navdy.hud.app.ui.component.ConfirmationLayout) finder.findRequiredView(source, com.navdy.hud.app.R.id.confirmationLayout, "field 'confirmationLayout'");
    }

    public static void reset(com.navdy.hud.app.ui.component.mainmenu.MainMenuView2 target) {
        target.rightBackground = null;
        target.confirmationLayout = null;
    }
}
