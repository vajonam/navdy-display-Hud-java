package com.navdy.hud.app.ui.component;

public class SystemTrayView$$ViewInjector {
    public static void inject(butterknife.ButterKnife.Finder finder, com.navdy.hud.app.ui.component.SystemTrayView target, java.lang.Object source) {
        target.dialImageView = (android.widget.ImageView) finder.findRequiredView(source, com.navdy.hud.app.R.id.dial, "field 'dialImageView'");
        target.phoneImageView = (android.widget.ImageView) finder.findRequiredView(source, com.navdy.hud.app.R.id.phone, "field 'phoneImageView'");
        target.phoneNetworkImageView = (android.widget.ImageView) finder.findRequiredView(source, com.navdy.hud.app.R.id.phoneNetwork, "field 'phoneNetworkImageView'");
        target.locationImageView = (android.widget.ImageView) finder.findRequiredView(source, com.navdy.hud.app.R.id.gps, "field 'locationImageView'");
    }

    public static void reset(com.navdy.hud.app.ui.component.SystemTrayView target) {
        target.dialImageView = null;
        target.phoneImageView = null;
        target.phoneNetworkImageView = null;
        target.locationImageView = null;
    }
}
