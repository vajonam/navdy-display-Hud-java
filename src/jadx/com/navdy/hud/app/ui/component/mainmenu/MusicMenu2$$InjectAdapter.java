package com.navdy.hud.app.ui.component.mainmenu;

public final class MusicMenu2$$InjectAdapter extends dagger.internal.Binding<com.navdy.hud.app.ui.component.mainmenu.MusicMenu2> implements dagger.MembersInjector<com.navdy.hud.app.ui.component.mainmenu.MusicMenu2> {
    private dagger.internal.Binding<com.navdy.hud.app.util.MusicArtworkCache> musicArtworkCache;
    private dagger.internal.Binding<com.navdy.hud.app.storage.cache.MessageCache<com.navdy.service.library.events.audio.MusicCollectionResponse>> musicCollectionResponseMessageCache;

    public MusicMenu2$$InjectAdapter() {
        super(null, "members/com.navdy.hud.app.ui.component.mainmenu.MusicMenu2", false, com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.class);
    }

    public void attach(dagger.internal.Linker linker) {
        this.musicArtworkCache = linker.requestBinding("com.navdy.hud.app.util.MusicArtworkCache", com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.class, getClass().getClassLoader());
        this.musicCollectionResponseMessageCache = linker.requestBinding("com.navdy.hud.app.storage.cache.MessageCache<com.navdy.service.library.events.audio.MusicCollectionResponse>", com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.class, getClass().getClassLoader());
    }

    public void getDependencies(java.util.Set<dagger.internal.Binding<?>> set, java.util.Set<dagger.internal.Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.musicArtworkCache);
        injectMembersBindings.add(this.musicCollectionResponseMessageCache);
    }

    public void injectMembers(com.navdy.hud.app.ui.component.mainmenu.MusicMenu2 object) {
        object.musicArtworkCache = (com.navdy.hud.app.util.MusicArtworkCache) this.musicArtworkCache.get();
        object.musicCollectionResponseMessageCache = (com.navdy.hud.app.storage.cache.MessageCache) this.musicCollectionResponseMessageCache.get();
    }
}
