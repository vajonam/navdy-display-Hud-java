package com.navdy.hud.app.ui.component.carousel;

public class ProgressIndicator extends android.view.View implements com.navdy.hud.app.ui.component.carousel.IProgressIndicator {
    private boolean animating;
    private int animatingColor;
    /* access modifiers changed from: private */
    public float animatingPos;
    private int backgroundColor;
    private int barParentSize = -1;
    private int barSize = -1;
    private int blackColor;
    private int currentItem = -1;
    private int currentItemColor = -1;
    private int currentItemPaddingRadius;
    private int defaultColor;
    private boolean fullBackground;
    private int greyColor;
    private int itemCount;
    private int itemPadding;
    private int itemRadius;
    private com.navdy.hud.app.ui.component.carousel.CarouselIndicator.Orientation orientation;
    private android.graphics.Paint paint;
    private int roundRadius;
    private int viewPadding;

    class Anon1 implements android.animation.ValueAnimator.AnimatorUpdateListener {
        Anon1() {
        }

        public void onAnimationUpdate(android.animation.ValueAnimator animation) {
            com.navdy.hud.app.ui.component.carousel.ProgressIndicator.this.animatingPos = ((java.lang.Float) animation.getAnimatedValue()).floatValue();
            com.navdy.hud.app.ui.component.carousel.ProgressIndicator.this.invalidate();
        }
    }

    class Anon2 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
        Anon2() {
        }

        public void onAnimationEnd(android.animation.Animator animation) {
        }
    }

    public ProgressIndicator(android.content.Context context) {
        super(context);
        init(context);
    }

    public ProgressIndicator(android.content.Context context, android.util.AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        if (this.barSize != -1) {
            int width = getMeasuredWidth();
            int height = getMeasuredHeight();
            if (this.orientation == com.navdy.hud.app.ui.component.carousel.CarouselIndicator.Orientation.VERTICAL) {
                width = this.barParentSize;
            } else {
                height = this.barParentSize;
            }
            setMeasuredDimension(width, height);
        }
    }

    private void init(android.content.Context context) {
        android.content.res.Resources resources = getResources();
        this.blackColor = resources.getColor(17170444);
        this.greyColor = resources.getColor(com.navdy.hud.app.R.color.grey_4a);
        this.paint = new android.graphics.Paint();
        this.paint.setStrokeWidth(0.0f);
        this.paint.setAntiAlias(true);
    }

    public void setProperties(int roundRadius2, int itemRadius2, int itemPadding2, int currentItemPaddingRadius2, int defaultColor2, int backgroundColor2, boolean fullBackground2, int viewPadding2, int barSize2, int barParentSize2) {
        this.roundRadius = roundRadius2;
        this.itemRadius = itemRadius2;
        this.itemPadding = itemPadding2;
        this.viewPadding = viewPadding2;
        this.currentItemPaddingRadius = currentItemPaddingRadius2;
        this.defaultColor = defaultColor2;
        this.barSize = barSize2;
        this.barParentSize = barParentSize2;
        if (backgroundColor2 != -1) {
            this.backgroundColor = backgroundColor2;
        } else {
            this.backgroundColor = this.greyColor;
        }
        this.fullBackground = fullBackground2;
        if (this.fullBackground && viewPadding2 > 0) {
            this.viewPadding += itemRadius2;
        }
    }

    public void setOrientation(com.navdy.hud.app.ui.component.carousel.CarouselIndicator.Orientation orientation2) {
        this.orientation = orientation2;
    }

    public void setItemCount(int n) {
        if (n <= 0) {
            throw new java.lang.IllegalArgumentException();
        }
        this.currentItem = -1;
        this.itemCount = n;
    }

    public int getCurrentItem() {
        return this.currentItem;
    }

    public void setCurrentItem(int n) {
        setCurrentItem(n, -1);
    }

    public void setCurrentItem(int n, int color) {
        if (n >= 0 && n <= this.itemCount - 1) {
            this.currentItem = n;
            this.currentItemColor = color;
            this.animating = false;
            this.animatingPos = -1.0f;
            this.animatingColor = -1;
            invalidate();
        }
    }

    /* access modifiers changed from: protected */
    public void onDraw(android.graphics.Canvas canvas) {
        float x;
        float y;
        android.graphics.RectF rectF;
        float f;
        super.onDraw(canvas);
        float width = (float) getWidth();
        float height = (float) getHeight();
        int left = 0;
        int top = 0;
        float barWidth = width;
        float barHeight = height;
        if (this.barSize != -1) {
            if (this.orientation == com.navdy.hud.app.ui.component.carousel.CarouselIndicator.Orientation.VERTICAL) {
                left = ((int) (width - ((float) this.barSize))) / 2;
                barWidth = (float) this.barSize;
            } else {
                top = ((int) (width - ((float) this.barSize))) / 2;
                barHeight = (float) this.barSize;
            }
        }
        this.paint.setColor(this.backgroundColor);
        if (this.itemCount == 0) {
            canvas.drawRoundRect(new android.graphics.RectF((float) left, (float) top, ((float) left) + barWidth, ((float) top) + barHeight), (float) this.roundRadius, (float) this.roundRadius, this.paint);
            return;
        }
        if (this.itemCount == 1) {
            canvas.drawRoundRect(new android.graphics.RectF((float) left, (float) top, ((float) left) + barWidth, ((float) top) + barHeight), (float) this.roundRadius, (float) this.roundRadius, this.paint);
        } else if (this.fullBackground) {
            canvas.drawRoundRect(new android.graphics.RectF((float) left, (float) top, ((float) left) + barWidth, ((float) top) + barHeight), (float) this.roundRadius, (float) this.roundRadius, this.paint);
        } else {
            if (this.orientation == com.navdy.hud.app.ui.component.carousel.CarouselIndicator.Orientation.HORIZONTAL) {
                x = (float) this.itemRadius;
                y = height / 2.0f;
                rectF = new android.graphics.RectF((float) ((this.itemRadius * 2) + this.itemPadding), 0.0f, width, height);
            } else {
                x = width / 2.0f;
                y = (float) this.itemRadius;
                rectF = new android.graphics.RectF(0.0f, (float) ((this.itemRadius * 2) + this.itemPadding), width, height);
            }
            canvas.drawCircle(x, y, (float) this.itemRadius, this.paint);
            canvas.drawRoundRect(rectF, (float) this.roundRadius, (float) this.roundRadius, this.paint);
        }
        int padding = this.currentItemPaddingRadius + this.itemRadius;
        if (this.animating) {
            drawItem(canvas, width, height, this.animatingPos, padding, this.animatingColor);
        } else if (this.currentItem != -1) {
            if (this.orientation == com.navdy.hud.app.ui.component.carousel.CarouselIndicator.Orientation.HORIZONTAL) {
                f = width;
            } else {
                f = height;
            }
            float targetPos = getItemTargetPos(f, this.currentItem);
            if (this.fullBackground && this.viewPadding > 0) {
                if (this.currentItem == 0) {
                    targetPos = (float) this.viewPadding;
                } else if (this.currentItem == this.itemCount - 1) {
                    targetPos = (float) (getHeight() - this.viewPadding);
                } else {
                    int lastItem = getHeight() - this.viewPadding;
                    int newPos = ((int) targetPos) + this.viewPadding;
                    targetPos = newPos <= lastItem ? (float) newPos : (float) lastItem;
                }
            }
            int color = this.defaultColor;
            if (this.currentItemColor != -1) {
                color = this.currentItemColor;
            }
            drawItem(canvas, width, height, targetPos, padding, color);
        }
    }

    private float getItemTargetPos(float dimension, int itemPos) {
        float targetPos;
        if (this.fullBackground && this.viewPadding > 0) {
            dimension -= (float) (this.viewPadding * 2);
        }
        if (this.itemCount <= 0) {
            return 0.0f;
        }
        if (this.itemCount == 1) {
            return dimension - ((float) (this.itemRadius + this.currentItemPaddingRadius));
        }
        float minPos = (float) (this.itemRadius + this.currentItemPaddingRadius);
        float maxPos = dimension - ((float) (this.itemRadius + this.currentItemPaddingRadius));
        if (itemPos == 0) {
            targetPos = (float) (this.itemRadius + this.currentItemPaddingRadius);
        } else if (itemPos == this.itemCount - 1) {
            targetPos = dimension - ((float) (this.itemRadius + this.currentItemPaddingRadius));
        } else {
            targetPos = ((float) itemPos) * (dimension / ((float) (this.itemCount - 1)));
        }
        if (targetPos < minPos) {
            return minPos;
        }
        if (targetPos > maxPos) {
            return maxPos;
        }
        return targetPos;
    }

    public android.animation.AnimatorSet getItemMoveAnimator(int toPos, int color) {
        float f;
        float f2;
        float width = (float) getWidth();
        float height = (float) getHeight();
        if (this.orientation == com.navdy.hud.app.ui.component.carousel.CarouselIndicator.Orientation.HORIZONTAL) {
            f = width;
        } else {
            f = height;
        }
        float currentPos = getItemTargetPos(f, this.currentItem);
        if (this.orientation == com.navdy.hud.app.ui.component.carousel.CarouselIndicator.Orientation.HORIZONTAL) {
            f2 = width;
        } else {
            f2 = height;
        }
        float targetPos = getItemTargetPos(f2, toPos);
        if (this.fullBackground) {
            if (this.orientation != com.navdy.hud.app.ui.component.carousel.CarouselIndicator.Orientation.HORIZONTAL) {
                width = height;
            }
            int lastItem = (int) getItemTargetPos(width, this.itemCount - 1);
            int newPos = ((int) targetPos) + this.viewPadding;
            if (newPos < lastItem - this.viewPadding) {
                targetPos = (float) newPos;
            } else {
                targetPos = (float) (lastItem - this.viewPadding);
            }
        }
        android.animation.ValueAnimator animator = android.animation.ValueAnimator.ofFloat(new float[]{currentPos, targetPos});
        animator.addUpdateListener(new com.navdy.hud.app.ui.component.carousel.ProgressIndicator.Anon1());
        animator.addListener(new com.navdy.hud.app.ui.component.carousel.ProgressIndicator.Anon2());
        android.animation.AnimatorSet set = new android.animation.AnimatorSet();
        set.play(animator);
        this.animatingPos = currentPos;
        this.animatingColor = color;
        this.animating = true;
        return set;
    }

    public android.graphics.RectF getItemPos(int n) {
        float x;
        float y;
        if (n < 0 || n >= this.itemCount) {
            return null;
        }
        float width = (float) getWidth();
        float height = (float) getHeight();
        if (this.orientation != com.navdy.hud.app.ui.component.carousel.CarouselIndicator.Orientation.HORIZONTAL) {
            width = height;
        }
        float targetPos = getItemTargetPos(width, this.currentItem);
        if (this.orientation == com.navdy.hud.app.ui.component.carousel.CarouselIndicator.Orientation.HORIZONTAL) {
            x = targetPos;
            y = 0.0f;
        } else {
            x = 0.0f;
            y = targetPos;
        }
        return new android.graphics.RectF(x, y, 0.0f, 0.0f);
    }

    private void drawItem(android.graphics.Canvas canvas, float width, float height, float targetPos, int padding, int color) {
        this.paint.setColor(this.blackColor);
        if (this.orientation == com.navdy.hud.app.ui.component.carousel.CarouselIndicator.Orientation.HORIZONTAL) {
            canvas.drawCircle(targetPos, height / 2.0f, (float) padding, this.paint);
        } else {
            canvas.drawCircle(width / 2.0f, targetPos, (float) padding, this.paint);
        }
        this.paint.setColor(color);
        if (this.orientation == com.navdy.hud.app.ui.component.carousel.CarouselIndicator.Orientation.HORIZONTAL) {
            canvas.drawCircle(targetPos, height / 2.0f, (float) this.itemRadius, this.paint);
        } else {
            canvas.drawCircle(width / 2.0f, targetPos, (float) this.itemRadius, this.paint);
        }
    }

    public void setBackgroundColor(int color) {
        this.backgroundColor = color;
    }
}
