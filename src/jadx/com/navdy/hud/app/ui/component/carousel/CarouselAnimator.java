package com.navdy.hud.app.ui.component.carousel;

public class CarouselAnimator implements com.navdy.hud.app.ui.component.carousel.AnimationStrategy {
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.carousel.CarouselAnimator.class);

    public android.animation.AnimatorSet createMiddleLeftViewAnimation(com.navdy.hud.app.ui.component.carousel.CarouselLayout carousel, com.navdy.hud.app.ui.component.carousel.AnimationStrategy.Direction direction) {
        android.view.View targetPosView;
        float scaleFactor;
        android.animation.AnimatorSet animatorSet = new android.animation.AnimatorSet();
        if (direction == com.navdy.hud.app.ui.component.carousel.AnimationStrategy.Direction.LEFT) {
            targetPosView = carousel.leftView;
        } else {
            targetPosView = carousel.rightView;
        }
        if (carousel.middleLeftView.getScaleX() == 1.0f) {
            scaleFactor = ((float) carousel.sideImageSize) / ((float) carousel.mainImageSize);
        } else {
            scaleFactor = 1.0f;
        }
        android.animation.ObjectAnimator a1 = android.animation.ObjectAnimator.ofFloat(carousel.middleLeftView, "scaleX", new float[]{scaleFactor});
        android.animation.ObjectAnimator a2 = android.animation.ObjectAnimator.ofFloat(carousel.middleLeftView, "scaleY", new float[]{scaleFactor});
        android.animation.ObjectAnimator a3 = android.animation.ObjectAnimator.ofFloat(carousel.middleLeftView, "x", new float[]{targetPosView.getX()});
        android.animation.ObjectAnimator a4 = android.animation.ObjectAnimator.ofFloat(carousel.middleLeftView, "y", new float[]{targetPosView.getY()});
        android.animation.AnimatorSet fade = ((com.navdy.hud.app.ui.component.image.CrossFadeImageView) carousel.middleLeftView).getCrossFadeAnimator();
        carousel.middleLeftView.setPivotX(0.5f);
        carousel.middleLeftView.setPivotY(0.5f);
        animatorSet.playTogether(new android.animation.Animator[]{a1, a2, a3, a4, fade});
        return animatorSet;
    }

    public android.animation.AnimatorSet createMiddleRightViewAnimation(com.navdy.hud.app.ui.component.carousel.CarouselLayout carousel, com.navdy.hud.app.ui.component.carousel.AnimationStrategy.Direction direction) {
        android.view.View targetPosView;
        android.animation.AnimatorSet animatorSet = new android.animation.AnimatorSet();
        float scaleFactor = ((float) carousel.sideImageSize) / ((float) carousel.middleRightView.getMeasuredHeight());
        if (direction == com.navdy.hud.app.ui.component.carousel.AnimationStrategy.Direction.LEFT) {
            targetPosView = carousel.leftView;
        } else {
            targetPosView = carousel.rightView;
        }
        animatorSet.playTogether(new android.animation.Animator[]{android.animation.ObjectAnimator.ofFloat(carousel.middleRightView, "scaleX", new float[]{scaleFactor}), android.animation.ObjectAnimator.ofFloat(carousel.middleRightView, "scaleY", new float[]{scaleFactor}), android.animation.ObjectAnimator.ofFloat(carousel.middleRightView, "x", new float[]{targetPosView.getX() + ((float) ((carousel.mainViewDividerPadding + carousel.sideImageSize) / 2))}), android.animation.ObjectAnimator.ofFloat(carousel.middleRightView, "y", new float[]{carousel.middleRightView.getY()}), android.animation.ObjectAnimator.ofFloat(carousel.middleRightView, "alpha", new float[]{0.0f})});
        return animatorSet;
    }

    public android.animation.AnimatorSet createSideViewToMiddleAnimation(com.navdy.hud.app.ui.component.carousel.CarouselLayout carousel, com.navdy.hud.app.ui.component.carousel.AnimationStrategy.Direction direction) {
        android.view.View sideImageView;
        float scaleFactor;
        android.animation.AnimatorSet animatorSet = new android.animation.AnimatorSet();
        if (direction == com.navdy.hud.app.ui.component.carousel.AnimationStrategy.Direction.RIGHT) {
            sideImageView = carousel.leftView;
        } else {
            sideImageView = carousel.rightView;
        }
        if (sideImageView.getScaleX() == 1.0f) {
            scaleFactor = ((float) carousel.mainImageSize) / ((float) carousel.sideImageSize);
        } else {
            scaleFactor = 1.0f;
        }
        android.animation.ObjectAnimator a1 = android.animation.ObjectAnimator.ofFloat(sideImageView, "scaleX", new float[]{scaleFactor});
        android.animation.ObjectAnimator a2 = android.animation.ObjectAnimator.ofFloat(sideImageView, "scaleY", new float[]{scaleFactor});
        android.animation.ObjectAnimator a3 = android.animation.ObjectAnimator.ofFloat(sideImageView, "x", new float[]{carousel.middleLeftView.getX()});
        android.animation.ObjectAnimator a4 = android.animation.ObjectAnimator.ofFloat(sideImageView, "y", new float[]{carousel.middleLeftView.getY()});
        android.animation.AnimatorSet fade = ((com.navdy.hud.app.ui.component.image.CrossFadeImageView) sideImageView).getCrossFadeAnimator();
        sideImageView.setPivotX(0.5f);
        sideImageView.setPivotY(0.5f);
        animatorSet.playTogether(new android.animation.Animator[]{a1, a2, a3, a4, fade});
        return animatorSet;
    }

    public android.animation.Animator createViewOutAnimation(com.navdy.hud.app.ui.component.carousel.CarouselLayout carousel, com.navdy.hud.app.ui.component.carousel.AnimationStrategy.Direction direction) {
        if (direction == com.navdy.hud.app.ui.component.carousel.AnimationStrategy.Direction.RIGHT) {
            return android.animation.ObjectAnimator.ofFloat(carousel.rightView, "x", new float[]{carousel.rightView.getX() + ((float) carousel.getMeasuredWidth())});
        }
        return android.animation.ObjectAnimator.ofFloat(carousel.leftView, "x", new float[]{-(carousel.leftView.getX() + ((float) carousel.leftView.getMeasuredWidth()))});
    }

    public android.animation.AnimatorSet createHiddenViewAnimation(com.navdy.hud.app.ui.component.carousel.CarouselLayout carousel, com.navdy.hud.app.ui.component.carousel.AnimationStrategy.Direction direction) {
        android.view.View srcView;
        android.view.View targetPosView;
        android.animation.AnimatorSet animatorSet = new android.animation.AnimatorSet();
        if (direction == com.navdy.hud.app.ui.component.carousel.AnimationStrategy.Direction.RIGHT) {
            srcView = carousel.newLeftView;
            targetPosView = carousel.leftView;
        } else {
            srcView = carousel.newRightView;
            targetPosView = carousel.rightView;
        }
        animatorSet.play(android.animation.ObjectAnimator.ofFloat(srcView, "x", new float[]{targetPosView.getX()}));
        return animatorSet;
    }

    public android.animation.AnimatorSet createNewMiddleRightViewAnimation(com.navdy.hud.app.ui.component.carousel.CarouselLayout carousel, com.navdy.hud.app.ui.component.carousel.AnimationStrategy.Direction direction) {
        android.animation.AnimatorSet animatorSet = new android.animation.AnimatorSet();
        android.animation.ObjectAnimator a1 = android.animation.ObjectAnimator.ofFloat(carousel.newMiddleRightView, "x", new float[]{carousel.middleLeftView.getX() + ((float) carousel.mainViewDividerPadding) + ((float) carousel.mainImageSize)});
        if (direction == com.navdy.hud.app.ui.component.carousel.AnimationStrategy.Direction.RIGHT) {
            animatorSet.playTogether(new android.animation.Animator[]{a1, android.animation.ObjectAnimator.ofFloat(carousel.newMiddleRightView, "alpha", new float[]{1.0f})});
        } else {
            animatorSet.playTogether(new android.animation.Animator[]{a1});
        }
        return animatorSet;
    }

    public android.animation.AnimatorSet buildLayoutAnimation(android.animation.Animator.AnimatorListener listener, com.navdy.hud.app.ui.component.carousel.CarouselLayout carousel, int currentPos, int newPos) {
        return null;
    }
}
