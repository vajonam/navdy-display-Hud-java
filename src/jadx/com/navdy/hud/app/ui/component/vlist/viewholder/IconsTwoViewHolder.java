package com.navdy.hud.app.ui.component.vlist.viewholder;

public class IconsTwoViewHolder extends com.navdy.hud.app.ui.component.vlist.viewholder.IconBaseViewHolder {
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.vlist.viewholder.IconsTwoViewHolder.class);
    private int imageIconSize = -1;

    public static com.navdy.hud.app.ui.component.vlist.VerticalList.Model buildModel(int id, int icon, int iconSmall, int iconFluctuatorColor, int iconDeselectedColor, java.lang.String title, java.lang.String subTitle) {
        return buildModel(id, icon, iconSmall, iconFluctuatorColor, iconDeselectedColor, title, subTitle, null);
    }

    public static com.navdy.hud.app.ui.component.vlist.VerticalList.Model buildModel(int id, int icon, int iconSmall, int iconFluctuatorColor, int iconDeselectedColor, java.lang.String title, java.lang.String subTitle, java.lang.String subTitle2) {
        com.navdy.hud.app.ui.component.vlist.VerticalList.Model model = com.navdy.hud.app.ui.component.vlist.VerticalModelCache.getFromCache(com.navdy.hud.app.ui.component.vlist.VerticalList.ModelType.TWO_ICONS);
        if (model == null) {
            model = new com.navdy.hud.app.ui.component.vlist.VerticalList.Model();
        }
        model.type = com.navdy.hud.app.ui.component.vlist.VerticalList.ModelType.TWO_ICONS;
        model.id = id;
        model.icon = icon;
        model.iconSmall = iconSmall;
        model.iconFluctuatorColor = iconFluctuatorColor;
        model.iconDeselectedColor = iconDeselectedColor;
        model.title = title;
        model.subTitle = subTitle;
        model.subTitle2 = subTitle2;
        return model;
    }

    public static com.navdy.hud.app.ui.component.vlist.viewholder.IconsTwoViewHolder buildViewHolder(android.view.ViewGroup parent, com.navdy.hud.app.ui.component.vlist.VerticalList vlist, android.os.Handler handler) {
        return new com.navdy.hud.app.ui.component.vlist.viewholder.IconsTwoViewHolder(getLayout(parent, com.navdy.hud.app.R.layout.vlist_item, com.navdy.hud.app.R.layout.crossfade_image_lyt), vlist, handler);
    }

    private IconsTwoViewHolder(android.view.ViewGroup layout, com.navdy.hud.app.ui.component.vlist.VerticalList vlist, android.os.Handler handler) {
        super(layout, vlist, handler);
    }

    public com.navdy.hud.app.ui.component.vlist.VerticalList.ModelType getModelType() {
        return com.navdy.hud.app.ui.component.vlist.VerticalList.ModelType.TWO_ICONS;
    }

    public void setItemState(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.State state, com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.AnimationType animation, int duration, boolean startFluctuator) {
        super.setItemState(state, animation, duration, startFluctuator);
        int iconSize = 0;
        com.navdy.hud.app.ui.component.image.CrossFadeImageView.Mode mode = null;
        switch (state) {
            case SELECTED:
                iconSize = selectedIconSize;
                mode = com.navdy.hud.app.ui.component.image.CrossFadeImageView.Mode.BIG;
                break;
            case UNSELECTED:
                if (!this.iconScaleAnimationDisabled) {
                    iconSize = unselectedIconSize;
                    mode = com.navdy.hud.app.ui.component.image.CrossFadeImageView.Mode.SMALL;
                    break;
                } else {
                    iconSize = selectedIconSize;
                    mode = com.navdy.hud.app.ui.component.image.CrossFadeImageView.Mode.SMALL;
                    break;
                }
        }
        if (this.imageIconSize != -1) {
            iconSize = this.imageIconSize;
        }
        switch (animation) {
            case NONE:
            case INIT:
                android.view.ViewGroup.MarginLayoutParams layoutParams = (android.view.ViewGroup.MarginLayoutParams) this.iconContainer.getLayoutParams();
                layoutParams.width = iconSize;
                layoutParams.height = iconSize;
                this.crossFadeImageView.setMode(mode);
                return;
            case MOVE:
                this.animatorSetBuilder.with(com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils.animateDimension(this.iconContainer, iconSize));
                if (this.crossFadeImageView.getMode() != mode) {
                    this.animatorSetBuilder.with(this.crossFadeImageView.getCrossFadeAnimator());
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void bind(com.navdy.hud.app.ui.component.vlist.VerticalList.Model model, com.navdy.hud.app.ui.component.vlist.VerticalList.ModelState modelState) {
        super.bind(model, modelState);
        setIcon(model.icon, model.iconSmall, modelState.updateImage, modelState.updateSmallImage, model.iconDeselectedColor, model.iconSize);
    }

    private void setIcon(int icon, int iconSmall, boolean updateImage, boolean updateSmallImage, int deselectedColor, int iconSize) {
        com.navdy.hud.app.ui.component.image.InitialsImageView.Style bigStyle;
        this.imageIconSize = iconSize;
        java.lang.String initials = null;
        if (this.extras != null) {
            initials = (java.lang.String) this.extras.get(com.navdy.hud.app.ui.component.vlist.VerticalList.Model.INITIALS);
        }
        if (updateImage) {
            if (initials != null) {
                bigStyle = com.navdy.hud.app.ui.component.image.InitialsImageView.Style.MEDIUM;
            } else {
                bigStyle = com.navdy.hud.app.ui.component.image.InitialsImageView.Style.DEFAULT;
            }
            ((com.navdy.hud.app.ui.component.image.InitialsImageView) this.crossFadeImageView.getBig()).setImage(icon, initials, bigStyle);
        }
        if (updateSmallImage) {
            com.navdy.hud.app.ui.component.image.InitialsImageView small = (com.navdy.hud.app.ui.component.image.InitialsImageView) this.crossFadeImageView.getSmall();
            small.setAlpha(1.0f);
            if (iconSmall == 0) {
                small.setBkColor(deselectedColor);
            } else {
                small.setBkColor(0);
            }
            small.setImage(iconSmall, initials, com.navdy.hud.app.ui.component.image.InitialsImageView.Style.TINY);
        }
    }
}
