package com.navdy.hud.app.ui.component.vlist.viewholder;

public class IconOneViewHolder extends com.navdy.hud.app.ui.component.vlist.viewholder.IconBaseViewHolder {
    private static final com.navdy.service.library.log.Logger sLogger = com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.sLogger;

    public static com.navdy.hud.app.ui.component.vlist.VerticalList.Model buildModel(int id, int icon, int iconFluctuatorColor, java.lang.String title, java.lang.String subTitle) {
        return buildModel(id, icon, iconFluctuatorColor, title, subTitle, null);
    }

    public static com.navdy.hud.app.ui.component.vlist.VerticalList.Model buildModel(int id, int icon, int iconFluctuatorColor, java.lang.String title, java.lang.String subTitle, java.lang.String subTitle2) {
        com.navdy.hud.app.ui.component.vlist.VerticalList.Model model = new com.navdy.hud.app.ui.component.vlist.VerticalList.Model();
        model.type = com.navdy.hud.app.ui.component.vlist.VerticalList.ModelType.ICON;
        model.id = id;
        model.icon = icon;
        model.iconFluctuatorColor = iconFluctuatorColor;
        model.title = title;
        model.subTitle = subTitle;
        model.subTitle2 = subTitle2;
        return model;
    }

    public static com.navdy.hud.app.ui.component.vlist.viewholder.IconOneViewHolder buildViewHolder(android.view.ViewGroup parent, com.navdy.hud.app.ui.component.vlist.VerticalList vlist, android.os.Handler handler) {
        return new com.navdy.hud.app.ui.component.vlist.viewholder.IconOneViewHolder(getLayout(parent, com.navdy.hud.app.R.layout.vlist_item, com.navdy.hud.app.R.layout.crossfade_image_lyt), vlist, handler);
    }

    private IconOneViewHolder(android.view.ViewGroup layout, com.navdy.hud.app.ui.component.vlist.VerticalList vlist, android.os.Handler handler) {
        super(layout, vlist, handler);
    }

    public com.navdy.hud.app.ui.component.vlist.VerticalList.ModelType getModelType() {
        return com.navdy.hud.app.ui.component.vlist.VerticalList.ModelType.ICON;
    }

    public void setItemState(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.State state, com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.AnimationType animation, int duration, boolean startFluctuator) {
        super.setItemState(state, animation, duration, startFluctuator);
        float iconScaleFactor = 0.0f;
        float iconAlphaFactor = 0.0f;
        com.navdy.hud.app.ui.component.image.CrossFadeImageView.Mode mode = null;
        switch (state) {
            case SELECTED:
                iconScaleFactor = 1.0f;
                iconAlphaFactor = 1.0f;
                mode = com.navdy.hud.app.ui.component.image.CrossFadeImageView.Mode.BIG;
                break;
            case UNSELECTED:
                iconScaleFactor = 0.6f;
                iconAlphaFactor = 0.5f;
                mode = com.navdy.hud.app.ui.component.image.CrossFadeImageView.Mode.SMALL;
                break;
        }
        switch (animation) {
            case NONE:
            case INIT:
                this.imageContainer.setScaleX(iconScaleFactor);
                this.imageContainer.setScaleY(iconScaleFactor);
                this.imageContainer.setAlpha(iconAlphaFactor);
                this.crossFadeImageView.setMode(mode);
                return;
            case MOVE:
                android.animation.PropertyValuesHolder p1 = android.animation.PropertyValuesHolder.ofFloat(android.view.View.SCALE_X, new float[]{iconScaleFactor});
                android.animation.PropertyValuesHolder p2 = android.animation.PropertyValuesHolder.ofFloat(android.view.View.SCALE_Y, new float[]{iconScaleFactor});
                android.animation.PropertyValuesHolder p3 = android.animation.PropertyValuesHolder.ofFloat(android.view.View.ALPHA, new float[]{iconAlphaFactor});
                this.animatorSetBuilder.with(android.animation.ObjectAnimator.ofPropertyValuesHolder(this.imageContainer, new android.animation.PropertyValuesHolder[]{p1, p2, p3}));
                if (this.crossFadeImageView.getMode() != mode) {
                    this.animatorSetBuilder.with(this.crossFadeImageView.getCrossFadeAnimator());
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void bind(com.navdy.hud.app.ui.component.vlist.VerticalList.Model model, com.navdy.hud.app.ui.component.vlist.VerticalList.ModelState modelState) {
        super.bind(model, modelState);
        setIcon(model.icon, modelState.updateImage, modelState.updateSmallImage);
    }

    private void setIcon(int icon, boolean updateImage, boolean updateSmallImage) {
        com.navdy.hud.app.ui.component.image.InitialsImageView.Style smallStyle;
        com.navdy.hud.app.ui.component.image.InitialsImageView.Style bigStyle;
        java.lang.String initials = null;
        if (this.extras != null) {
            initials = (java.lang.String) this.extras.get(com.navdy.hud.app.ui.component.vlist.VerticalList.Model.INITIALS);
        }
        if (updateImage) {
            if (initials != null) {
                bigStyle = com.navdy.hud.app.ui.component.image.InitialsImageView.Style.MEDIUM;
            } else {
                bigStyle = com.navdy.hud.app.ui.component.image.InitialsImageView.Style.DEFAULT;
            }
            ((com.navdy.hud.app.ui.component.image.InitialsImageView) this.crossFadeImageView.getBig()).setImage(icon, null, bigStyle);
        }
        if (updateSmallImage) {
            if (initials != null) {
                smallStyle = com.navdy.hud.app.ui.component.image.InitialsImageView.Style.TINY;
            } else {
                smallStyle = com.navdy.hud.app.ui.component.image.InitialsImageView.Style.DEFAULT;
            }
            com.navdy.hud.app.ui.component.image.InitialsImageView small = (com.navdy.hud.app.ui.component.image.InitialsImageView) this.crossFadeImageView.getSmall();
            small.setAlpha(0.5f);
            small.setImage(icon, initials, smallStyle);
        }
    }
}
