package com.navdy.hud.app.ui.component.destination;

public class DestinationPickerView extends android.widget.RelativeLayout implements com.navdy.hud.app.manager.InputManager.IInputHandler {
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.destination.DestinationPickerView.class);
    private com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.Callback callback;
    @butterknife.InjectView(2131624312)
    com.navdy.hud.app.ui.component.ConfirmationLayout confirmationLayout;
    @javax.inject.Inject
    public com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.Presenter presenter;
    @butterknife.InjectView(2131624311)
    android.view.View rightBackground;
    com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent vmenuComponent;

    class Anon1 implements com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.Callback {

        /* renamed from: com.navdy.hud.app.ui.component.destination.DestinationPickerView$Anon1$Anon1 reason: collision with other inner class name */
        class C0032Anon1 implements java.lang.Runnable {
            C0032Anon1() {
            }

            public void run() {
                if (com.navdy.hud.app.ui.component.destination.DestinationPickerView.this.presenter.isAlive()) {
                    if (com.navdy.hud.app.ui.component.destination.DestinationPickerView.this.presenter.isShowingRouteMap()) {
                        com.navdy.hud.app.ui.component.destination.DestinationPickerView.this.presenter.startMapFluctuator();
                    } else if (com.navdy.hud.app.ui.component.destination.DestinationPickerView.this.presenter.isShowDestinationMap()) {
                        com.navdy.hud.app.ui.component.destination.DestinationPickerView.this.presenter.itemSelected(com.navdy.hud.app.ui.component.destination.DestinationPickerView.this.presenter.initialSelection);
                        com.navdy.hud.app.ui.component.destination.DestinationPickerView.this.presenter.itemSelection = com.navdy.hud.app.ui.component.destination.DestinationPickerView.this.presenter.initialSelection;
                    }
                }
            }
        }

        Anon1() {
        }

        public void select(com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState selection) {
            selection.pos++;
            com.navdy.hud.app.ui.component.destination.DestinationPickerView.this.presenter.itemClicked(selection);
        }

        public void onLoad() {
            com.navdy.hud.app.ui.component.destination.DestinationPickerView.sLogger.v("onLoad");
            if (com.navdy.hud.app.ui.component.destination.DestinationPickerView.this.presenter != null && (com.navdy.hud.app.ui.component.destination.DestinationPickerView.this.presenter.isShowingRouteMap() || com.navdy.hud.app.ui.component.destination.DestinationPickerView.this.presenter.isShowDestinationMap())) {
                com.navdy.hud.app.ui.component.destination.DestinationPickerView.this.setBackgroundColor(0);
                com.navdy.hud.app.ui.component.destination.DestinationPickerView.this.getHandler().postDelayed(new com.navdy.hud.app.ui.component.destination.DestinationPickerView.Anon1.C0032Anon1(), 1000);
            }
            com.navdy.hud.app.ui.component.destination.DestinationPickerView.this.vmenuComponent.animateIn(null);
        }

        public boolean isItemClickable(com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState selection) {
            return true;
        }

        public void onBindToView(com.navdy.hud.app.ui.component.vlist.VerticalList.Model model, android.view.View view, int pos, com.navdy.hud.app.ui.component.vlist.VerticalList.ModelState state) {
        }

        public void onItemSelected(com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState selection) {
            selection.pos++;
            com.navdy.hud.app.ui.component.destination.DestinationPickerView.this.presenter.itemSelected(selection);
        }

        public void onScrollIdle() {
        }

        public void onFastScrollStart() {
        }

        public void onFastScrollEnd() {
        }

        public void showToolTip() {
        }

        public void close() {
            com.navdy.hud.app.analytics.AnalyticsSupport.recordRouteSelectionCancelled();
            com.navdy.hud.app.ui.component.destination.DestinationPickerView.this.presenter.close();
        }

        public boolean isClosed() {
            return com.navdy.hud.app.ui.component.destination.DestinationPickerView.this.presenter.isClosed();
        }
    }

    public DestinationPickerView(android.content.Context context) {
        this(context, null);
    }

    public DestinationPickerView(android.content.Context context, android.util.AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public DestinationPickerView(android.content.Context context, android.util.AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.callback = new com.navdy.hud.app.ui.component.destination.DestinationPickerView.Anon1();
        if (!isInEditMode()) {
            mortar.Mortar.inject(context, this);
        } else {
            com.navdy.hud.app.HudApplication.setContext(context);
        }
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
        butterknife.ButterKnife.inject((android.view.View) this);
        this.vmenuComponent = new com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent(this, this.callback, false);
        this.vmenuComponent.leftContainer.setAlpha(0.0f);
        this.vmenuComponent.rightContainer.setAlpha(0.0f);
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.presenter != null) {
            this.presenter.takeView(this);
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.vmenuComponent.clear();
        if (this.presenter != null) {
            this.presenter.dropView(this);
        }
    }

    public boolean onGesture(com.navdy.service.library.events.input.GestureEvent event) {
        if (this.confirmationLayout.getVisibility() == 0) {
            return false;
        }
        return this.vmenuComponent.handleGesture(event);
    }

    public boolean onKey(com.navdy.hud.app.manager.InputManager.CustomKeyEvent event) {
        if (this.confirmationLayout.getVisibility() == 0) {
            return this.confirmationLayout.handleKey(event);
        }
        return this.vmenuComponent.handleKey(event);
    }

    public com.navdy.hud.app.manager.InputManager.IInputHandler nextHandler() {
        return com.navdy.hud.app.manager.InputManager.nextContainingHandler(this);
    }
}
