package com.navdy.hud.app.ui.component.homescreen;

public class NoLocationView$$ViewInjector {
    public static void inject(butterknife.ButterKnife.Finder finder, com.navdy.hud.app.ui.component.homescreen.NoLocationView target, java.lang.Object source) {
        target.noLocationImage = (android.widget.ImageView) finder.findRequiredView(source, com.navdy.hud.app.R.id.noLocationImage, "field 'noLocationImage'");
        target.noLocationTextContainer = (android.widget.LinearLayout) finder.findRequiredView(source, com.navdy.hud.app.R.id.noLocationTextContainer, "field 'noLocationTextContainer'");
    }

    public static void reset(com.navdy.hud.app.ui.component.homescreen.NoLocationView target) {
        target.noLocationImage = null;
        target.noLocationTextContainer = null;
    }
}
