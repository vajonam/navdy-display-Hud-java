package com.navdy.hud.app.ui.component;

public class ChoiceLayout2 extends android.support.constraint.ConstraintLayout {
    private static final float DEFAULT_SCALE = 0.5f;
    private static final java.lang.String EMPTY = "";
    private static int defaultHaloDuration;
    private static int defaultHaloEndRadius;
    private static int defaultHaloMiddleRadius;
    private static int defaultHaloStartDelay;
    private static int defaultHaloStartRadius;
    private static int defaultIconHaloSize;
    private static int defaultIconSize;
    private static int defaultItemPadding = -1;
    private static int defaultLabelSize;
    private static int defaultTopPadding;
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.ChoiceLayout2.class);
    private java.util.List<com.navdy.hud.app.ui.component.ChoiceLayout2.Choice> choices;
    private boolean clickMode;
    /* access modifiers changed from: private */
    public int currentSelection;
    private int haloDuration;
    private int haloEndRadius;
    private int haloMiddleRadius;
    private int haloStartDelay;
    private int haloStartRadius;
    private int iconHaloSize;
    private int iconSize;
    private int itemPadding;
    private int itemTopPadding;
    private int labelSize;
    private android.widget.TextView labelView;
    private com.navdy.hud.app.ui.component.ChoiceLayout2.IListener listener;
    private float scale;
    private com.navdy.hud.app.ui.component.ChoiceLayout2.Selection selection;
    private java.util.List<com.navdy.hud.app.ui.component.ChoiceLayout2.ViewContainer> viewContainers;

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            com.navdy.hud.app.ui.component.ChoiceLayout2.this.callClickListener(com.navdy.hud.app.ui.component.ChoiceLayout2.this.currentSelection);
        }
    }

    public static class Choice implements android.os.Parcelable {
        public static final android.os.Parcelable.Creator<com.navdy.hud.app.ui.component.ChoiceLayout2.Choice> CREATOR = new com.navdy.hud.app.ui.component.ChoiceLayout2.Choice.Anon1();
        /* access modifiers changed from: private */
        public final int fluctuatorColor;
        /* access modifiers changed from: private */
        public final int id;
        /* access modifiers changed from: private */
        public final java.lang.String label;
        /* access modifiers changed from: private */
        public final int resIdSelected;
        /* access modifiers changed from: private */
        public final int resIdUnselected;
        /* access modifiers changed from: private */
        public final int selectedColor;
        /* access modifiers changed from: private */
        public final int unselectedColor;

        static class Anon1 implements android.os.Parcelable.Creator<com.navdy.hud.app.ui.component.ChoiceLayout2.Choice> {
            Anon1() {
            }

            public com.navdy.hud.app.ui.component.ChoiceLayout2.Choice createFromParcel(android.os.Parcel in) {
                return new com.navdy.hud.app.ui.component.ChoiceLayout2.Choice(in);
            }

            public com.navdy.hud.app.ui.component.ChoiceLayout2.Choice[] newArray(int size) {
                return new com.navdy.hud.app.ui.component.ChoiceLayout2.Choice[size];
            }
        }

        public Choice(int id2, int resIdSelected2, int selectedColor2, int resIdUnselected2, int unselectedColor2, java.lang.String label2, int fluctuatorColor2) {
            this.id = id2;
            this.resIdSelected = resIdSelected2;
            this.selectedColor = selectedColor2;
            this.resIdUnselected = resIdUnselected2;
            this.unselectedColor = unselectedColor2;
            this.label = label2;
            this.fluctuatorColor = fluctuatorColor2;
        }

        public Choice(android.os.Parcel in) {
            this.id = in.readInt();
            this.resIdSelected = in.readInt();
            this.selectedColor = in.readInt();
            this.resIdUnselected = in.readInt();
            this.unselectedColor = in.readInt();
            java.lang.String str = in.readString();
            if (android.text.TextUtils.isEmpty(str)) {
                this.label = null;
            } else {
                this.label = str;
            }
            this.fluctuatorColor = in.readInt();
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(android.os.Parcel dest, int flags) {
            dest.writeInt(this.id);
            dest.writeInt(this.resIdSelected);
            dest.writeInt(this.selectedColor);
            dest.writeInt(this.resIdUnselected);
            dest.writeInt(this.unselectedColor);
            dest.writeString(this.label != null ? this.label : "");
            dest.writeInt(this.fluctuatorColor);
        }

        public int getId() {
            return this.id;
        }
    }

    public interface IListener {
        void executeItem(com.navdy.hud.app.ui.component.ChoiceLayout2.Selection selection);

        void itemSelected(com.navdy.hud.app.ui.component.ChoiceLayout2.Selection selection);
    }

    public static class Selection {
        public int id;
        public int pos;
    }

    private static class ViewContainer {
        com.navdy.hud.app.ui.component.image.IconColorImageView big;
        com.navdy.hud.app.ui.component.image.CrossFadeImageView crossFadeImageView;
        com.navdy.hud.app.ui.component.HaloView haloView;
        android.view.ViewGroup iconContainer;
        android.view.View parent;
        com.navdy.hud.app.ui.component.image.IconColorImageView small;

        private ViewContainer() {
        }

        /* synthetic */ ViewContainer(com.navdy.hud.app.ui.component.ChoiceLayout2.Anon1 x0) {
            this();
        }
    }

    private static void initDefaults(android.content.Context context) {
        if (defaultItemPadding == -1) {
            android.content.res.Resources resources = context.getResources();
            defaultItemPadding = resources.getDimensionPixelSize(com.navdy.hud.app.R.dimen.choices2_lyt_item_padding);
            defaultTopPadding = resources.getDimensionPixelSize(com.navdy.hud.app.R.dimen.choices2_lyt_item_top_padding);
            defaultLabelSize = resources.getDimensionPixelSize(com.navdy.hud.app.R.dimen.choices2_lyt_text_size);
            defaultIconSize = resources.getDimensionPixelSize(com.navdy.hud.app.R.dimen.choices2_lyt_icon_size);
            defaultIconHaloSize = resources.getDimensionPixelSize(com.navdy.hud.app.R.dimen.choices2_lyt_icon_halo_size);
            defaultHaloStartRadius = resources.getDimensionPixelSize(com.navdy.hud.app.R.dimen.choices2_lyt_halo_start_radius);
            defaultHaloEndRadius = resources.getDimensionPixelSize(com.navdy.hud.app.R.dimen.choices2_lyt_halo_end_radius);
            defaultHaloMiddleRadius = resources.getDimensionPixelSize(com.navdy.hud.app.R.dimen.choices2_lyt_halo_middle_radius);
            defaultHaloStartDelay = resources.getInteger(com.navdy.hud.app.R.integer.choices2_lyt_halo_start_delay);
            defaultHaloDuration = resources.getInteger(com.navdy.hud.app.R.integer.choices2_lyt_halo_duration);
        }
    }

    public ChoiceLayout2(android.content.Context context) {
        this(context, null);
    }

    public ChoiceLayout2(android.content.Context context, android.util.AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ChoiceLayout2(android.content.Context context, android.util.AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.listener = null;
        this.currentSelection = -1;
        this.viewContainers = new java.util.ArrayList();
        this.selection = new com.navdy.hud.app.ui.component.ChoiceLayout2.Selection();
        initDefaults(context);
        android.view.LayoutInflater.from(context).inflate(com.navdy.hud.app.R.layout.choices2_lyt, this, true);
        this.labelView = (android.widget.TextView) findViewById(com.navdy.hud.app.R.id.label);
        android.content.res.TypedArray a = context.obtainStyledAttributes(attrs, com.navdy.hud.app.R.styleable.ChoiceLayout2, defStyleAttr, 0);
        if (a != null) {
            this.itemPadding = a.getDimensionPixelSize(1, defaultItemPadding);
            this.itemTopPadding = a.getDimensionPixelSize(0, defaultTopPadding);
            this.labelSize = a.getDimensionPixelSize(2, defaultLabelSize);
            this.iconSize = a.getDimensionPixelSize(3, defaultIconSize);
            this.iconHaloSize = a.getDimensionPixelSize(4, defaultIconHaloSize);
            this.haloStartRadius = a.getDimensionPixelSize(5, defaultHaloStartRadius);
            this.haloEndRadius = a.getDimensionPixelSize(6, defaultHaloEndRadius);
            this.haloMiddleRadius = a.getDimensionPixelSize(7, defaultHaloMiddleRadius);
            this.haloStartDelay = a.getInteger(8, defaultHaloStartDelay);
            this.haloDuration = a.getInteger(9, defaultHaloDuration);
            a.recycle();
        }
    }

    public void setChoices(java.util.List<com.navdy.hud.app.ui.component.ChoiceLayout2.Choice> choices2, int initialSelection, com.navdy.hud.app.ui.component.ChoiceLayout2.IListener listener2) {
        setChoices(choices2, initialSelection, listener2, 0.5f);
    }

    public void setChoices(java.util.List<com.navdy.hud.app.ui.component.ChoiceLayout2.Choice> choices2, int initialSelection, com.navdy.hud.app.ui.component.ChoiceLayout2.IListener listener2, float scale2) {
        resetClickMode();
        this.choices = choices2;
        this.listener = listener2;
        this.scale = scale2;
        if (choices2 == null || choices2.size() <= 0) {
            sLogger.v("setChoices:clear");
            this.currentSelection = -1;
            removeViewContainers();
            this.labelView.setText("");
            invalidate();
            requestLayout();
            return;
        }
        if (initialSelection < 0 || initialSelection >= choices2.size()) {
            this.currentSelection = 0;
        } else {
            this.currentSelection = initialSelection;
        }
        sLogger.v("setChoices:" + choices2.size() + " sel:" + this.currentSelection);
        buildChoices();
        for (com.navdy.hud.app.ui.component.ChoiceLayout2.ViewContainer viewContainer : this.viewContainers) {
            viewContainer.crossFadeImageView.setMode(com.navdy.hud.app.ui.component.image.CrossFadeImageView.Mode.SMALL);
        }
        startFluctuator(this.currentSelection, true);
    }

    public java.util.List<com.navdy.hud.app.ui.component.ChoiceLayout2.Choice> getChoices() {
        return this.choices;
    }

    private void buildChoices() {
        removeViewContainers();
        if (this.choices != null) {
            android.view.LayoutInflater inflater = android.view.LayoutInflater.from(getContext());
            int len = this.choices.size();
            for (int i = 0; i < len; i++) {
                com.navdy.hud.app.ui.component.ChoiceLayout2.Choice choice = (com.navdy.hud.app.ui.component.ChoiceLayout2.Choice) this.choices.get(i);
                com.navdy.hud.app.ui.component.ChoiceLayout2.ViewContainer viewContainer = new com.navdy.hud.app.ui.component.ChoiceLayout2.ViewContainer(null);
                viewContainer.parent = inflater.inflate(com.navdy.hud.app.R.layout.choices2_lyt_item, this, false);
                viewContainer.parent.setId(android.view.View.generateViewId());
                viewContainer.iconContainer = (android.view.ViewGroup) viewContainer.parent.findViewById(com.navdy.hud.app.R.id.iconContainer);
                viewContainer.crossFadeImageView = (com.navdy.hud.app.ui.component.image.CrossFadeImageView) viewContainer.parent.findViewById(com.navdy.hud.app.R.id.crossFadeImageView);
                viewContainer.crossFadeImageView.inject(com.navdy.hud.app.ui.component.image.CrossFadeImageView.Mode.SMALL);
                viewContainer.big = (com.navdy.hud.app.ui.component.image.IconColorImageView) viewContainer.parent.findViewById(com.navdy.hud.app.R.id.big);
                viewContainer.small = (com.navdy.hud.app.ui.component.image.IconColorImageView) viewContainer.parent.findViewById(com.navdy.hud.app.R.id.small);
                viewContainer.haloView = (com.navdy.hud.app.ui.component.HaloView) viewContainer.parent.findViewById(com.navdy.hud.app.R.id.halo);
                this.viewContainers.add(viewContainer);
                android.support.constraint.ConstraintLayout.LayoutParams layoutParams = new android.support.constraint.ConstraintLayout.LayoutParams(this.iconHaloSize, this.iconHaloSize);
                if (i != 0) {
                    layoutParams.leftMargin = this.itemPadding;
                }
                android.view.ViewGroup.MarginLayoutParams marginLayoutParams = (android.view.ViewGroup.MarginLayoutParams) viewContainer.iconContainer.getLayoutParams();
                marginLayoutParams.width = this.iconSize;
                marginLayoutParams.height = this.iconSize;
                viewContainer.haloView.setStartRadius(this.haloStartRadius);
                viewContainer.haloView.setMiddleRadius(this.haloMiddleRadius);
                viewContainer.haloView.setEndRadius(this.haloEndRadius);
                viewContainer.haloView.setDuration(this.haloDuration);
                viewContainer.haloView.setStartDelay(this.haloStartDelay);
                viewContainer.big.setIcon(choice.resIdSelected, choice.selectedColor, null, this.scale);
                viewContainer.small.setIcon(choice.resIdUnselected, choice.unselectedColor, null, this.scale);
                viewContainer.haloView.setVisibility(4);
                viewContainer.haloView.setStrokeColor(choice.fluctuatorColor);
                addView(viewContainer.parent, layoutParams);
            }
            this.labelView.setTextSize((float) this.labelSize);
            android.support.constraint.ConstraintSet constraintSet = new android.support.constraint.ConstraintSet();
            constraintSet.clone((android.support.constraint.ConstraintLayout) this);
            int len2 = this.viewContainers.size();
            int parentId = getId();
            int labelId = this.labelView.getId();
            for (int i2 = 0; i2 < len2; i2++) {
                int viewId = ((com.navdy.hud.app.ui.component.ChoiceLayout2.ViewContainer) this.viewContainers.get(i2)).parent.getId();
                if (i2 == 0) {
                    constraintSet.setHorizontalChainStyle(viewId, 2);
                    constraintSet.connect(viewId, 6, parentId, 6, 0);
                }
                if (i2 == len2 - 1) {
                    constraintSet.connect(viewId, 7, parentId, 7, 0);
                    if (len2 > 1) {
                        constraintSet.connect(viewId, 1, ((com.navdy.hud.app.ui.component.ChoiceLayout2.ViewContainer) this.viewContainers.get(i2 - 1)).parent.getId(), 2, 0);
                    }
                } else if (len2 > 1) {
                    constraintSet.connect(viewId, 2, ((com.navdy.hud.app.ui.component.ChoiceLayout2.ViewContainer) this.viewContainers.get(i2 + 1)).parent.getId(), 1, 0);
                    if (i2 != 0) {
                        constraintSet.connect(viewId, 1, ((com.navdy.hud.app.ui.component.ChoiceLayout2.ViewContainer) this.viewContainers.get(i2 - 1)).parent.getId(), 2, 0);
                    }
                }
                constraintSet.connect(viewId, 3, labelId, 4, this.itemTopPadding);
            }
            constraintSet.applyTo(this);
            invalidate();
            requestLayout();
        }
    }

    public boolean moveSelectionLeft() {
        if (this.clickMode) {
            sLogger.v("left: click mode on");
            return false;
        } else if (this.currentSelection == 0) {
            return false;
        } else {
            this.currentSelection--;
            stopFluctuator(this.currentSelection + 1, true);
            startFluctuator(this.currentSelection, true);
            callSelectListener(this.currentSelection);
            return true;
        }
    }

    public boolean moveSelectionRight() {
        if (this.clickMode) {
            sLogger.v("right: click mode on");
            return false;
        } else if (this.currentSelection == this.choices.size() - 1) {
            return false;
        } else {
            this.currentSelection++;
            stopFluctuator(this.currentSelection - 1, true);
            startFluctuator(this.currentSelection, true);
            callSelectListener(this.currentSelection);
            return true;
        }
    }

    public void executeSelectedItem() {
        if (this.clickMode) {
            sLogger.v("select: click mode on");
        } else if (isItemInBounds(this.currentSelection)) {
            this.clickMode = true;
            stopFluctuator(this.currentSelection, false);
            com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils.performClick(((com.navdy.hud.app.ui.component.ChoiceLayout2.ViewContainer) this.viewContainers.get(this.currentSelection)).iconContainer, 50, new com.navdy.hud.app.ui.component.ChoiceLayout2.Anon1());
        }
    }

    private int getItemCount() {
        if (this.choices != null) {
            return this.choices.size();
        }
        return 0;
    }

    /* access modifiers changed from: private */
    public void callClickListener(int pos) {
        if (this.listener != null) {
            com.navdy.hud.app.audio.SoundUtils.playSound(com.navdy.hud.app.audio.SoundUtils.Sound.MENU_SELECT);
            this.selection.id = ((com.navdy.hud.app.ui.component.ChoiceLayout2.Choice) this.choices.get(pos)).id;
            this.selection.pos = pos;
            this.listener.executeItem(this.selection);
        }
    }

    private void callSelectListener(int pos) {
        if (this.listener != null) {
            com.navdy.hud.app.audio.SoundUtils.playSound(com.navdy.hud.app.audio.SoundUtils.Sound.MENU_MOVE);
            this.selection.id = ((com.navdy.hud.app.ui.component.ChoiceLayout2.Choice) this.choices.get(pos)).id;
            this.selection.pos = pos;
            this.listener.itemSelected(this.selection);
        }
    }

    public com.navdy.hud.app.ui.component.ChoiceLayout2.Selection getCurrentSelection() {
        if (!isItemInBounds(this.currentSelection)) {
            return null;
        }
        this.selection.id = ((com.navdy.hud.app.ui.component.ChoiceLayout2.Choice) this.choices.get(this.currentSelection)).id;
        this.selection.pos = this.currentSelection;
        return this.selection;
    }

    public com.navdy.hud.app.ui.component.ChoiceLayout2.Choice getCurrentSelectedChoice() {
        com.navdy.hud.app.ui.component.ChoiceLayout2.Selection selection2 = getCurrentSelection();
        if (selection2 != null) {
            return (com.navdy.hud.app.ui.component.ChoiceLayout2.Choice) this.choices.get(selection2.pos);
        }
        return null;
    }

    private void removeViewContainers() {
        for (com.navdy.hud.app.ui.component.ChoiceLayout2.ViewContainer viewContainer : this.viewContainers) {
            removeView(viewContainer.parent);
        }
        this.viewContainers.clear();
    }

    private boolean isItemInBounds(int item) {
        if (this.choices == null || item < 0 || item >= this.choices.size()) {
            return false;
        }
        return true;
    }

    private void startFluctuator(int item, boolean changeColor) {
        sLogger.v("startFluctuator:" + item + " current=" + this.currentSelection);
        if (isItemInBounds(item)) {
            com.navdy.hud.app.ui.component.ChoiceLayout2.ViewContainer viewContainer = (com.navdy.hud.app.ui.component.ChoiceLayout2.ViewContainer) this.viewContainers.get(item);
            com.navdy.hud.app.ui.component.ChoiceLayout2.Choice choice = (com.navdy.hud.app.ui.component.ChoiceLayout2.Choice) this.choices.get(item);
            if (changeColor) {
                viewContainer.crossFadeImageView.setMode(com.navdy.hud.app.ui.component.image.CrossFadeImageView.Mode.BIG);
            }
            viewContainer.haloView.setVisibility(0);
            viewContainer.haloView.start();
            this.labelView.setText(choice.label);
        }
    }

    private void stopFluctuator(int item, boolean changeColor) {
        sLogger.v("stopFluctuator:" + item + " current=" + this.currentSelection);
        if (isItemInBounds(item)) {
            com.navdy.hud.app.ui.component.ChoiceLayout2.Choice choice = (com.navdy.hud.app.ui.component.ChoiceLayout2.Choice) this.choices.get(item);
            com.navdy.hud.app.ui.component.ChoiceLayout2.ViewContainer viewContainer = (com.navdy.hud.app.ui.component.ChoiceLayout2.ViewContainer) this.viewContainers.get(item);
            if (changeColor) {
                viewContainer.crossFadeImageView.setMode(com.navdy.hud.app.ui.component.image.CrossFadeImageView.Mode.SMALL);
            }
            viewContainer.haloView.setVisibility(8);
            viewContainer.haloView.stop();
        }
    }

    private void resetClickMode() {
        this.clickMode = false;
    }

    public void clear() {
        sLogger.v("clear");
        stopFluctuator(this.currentSelection, false);
        resetClickMode();
    }
}
