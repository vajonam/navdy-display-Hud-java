package com.navdy.hud.app.ui.component.image;

public class IconColorImageView extends android.widget.ImageView {
    private int bkColor;
    private android.graphics.Shader bkColorGradient;
    private android.content.Context context;
    private boolean draw;
    private int iconResource;
    private com.navdy.hud.app.ui.component.image.IconColorImageView.IconShape iconShape;
    private android.graphics.Paint paint;
    private float scaleF;

    public enum IconShape {
        CIRCLE,
        SQUARE
    }

    public IconColorImageView(android.content.Context context2) {
        this(context2, null, 0);
    }

    public IconColorImageView(android.content.Context context2, android.util.AttributeSet attrs) {
        this(context2, attrs, 0);
    }

    public IconColorImageView(android.content.Context context2, android.util.AttributeSet attrs, int defStyleAttr) {
        super(context2, attrs, defStyleAttr);
        this.iconShape = com.navdy.hud.app.ui.component.image.IconColorImageView.IconShape.CIRCLE;
        this.draw = true;
        this.context = context2;
        init();
    }

    private void init() {
        this.paint = new android.graphics.Paint();
        this.paint.setStrokeWidth(0.0f);
        this.paint.setAntiAlias(true);
    }

    public void setIcon(int iconResource2, int bkColor2, android.graphics.Shader gradient, float scaleF2) {
        this.iconResource = iconResource2;
        this.bkColor = bkColor2;
        this.bkColorGradient = gradient;
        this.scaleF = scaleF2;
        invalidate();
    }

    public void setIconShape(com.navdy.hud.app.ui.component.image.IconColorImageView.IconShape iconShape2) {
        this.iconShape = iconShape2;
    }

    /* access modifiers changed from: protected */
    public void onDraw(android.graphics.Canvas canvas) {
        int width = getWidth();
        int height = getHeight();
        canvas.drawColor(0);
        if (this.bkColorGradient != null) {
            this.paint.setShader(this.bkColorGradient);
        } else {
            this.paint.setShader(null);
            this.paint.setColor(this.bkColor);
        }
        if (this.iconShape == null || this.iconShape == com.navdy.hud.app.ui.component.image.IconColorImageView.IconShape.CIRCLE) {
            canvas.drawCircle((float) (width / 2), (float) (height / 2), (float) (width / 2), this.paint);
        } else {
            canvas.drawRect(0.0f, 0.0f, (float) width, (float) height, this.paint);
        }
        if (!this.draw) {
            super.onDraw(canvas);
        } else if (this.iconResource != 0) {
            android.graphics.drawable.Drawable drawable = this.context.getDrawable(this.iconResource);
            if (drawable != null) {
                android.graphics.Bitmap bitmap = ((android.graphics.drawable.BitmapDrawable) drawable).getBitmap();
                if (bitmap != null) {
                    canvas.scale(this.scaleF, this.scaleF);
                    canvas.drawBitmap(bitmap, 0.0f, 0.0f, null);
                }
            }
        }
    }

    public void setDraw(boolean b) {
        this.draw = b;
    }
}
