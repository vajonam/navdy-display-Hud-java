package com.navdy.hud.app.ui.component.vlist.viewholder;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u0019\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\b\n\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0012\u0010\u0003\u001a\u00020\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u0016\u00a8\u0006\u0007"}, d2 = {"com/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$select$Anon3", "Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;", "(Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;I)V", "onAnimationEnd", "", "animation", "Landroid/animation/Animator;", "app_hudDebug"}, k = 1, mv = {1, 1, 6})
/* compiled from: SwitchViewHolder.kt */
public final class SwitchViewHolder$select$Anon3 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
    final /* synthetic */ com.navdy.hud.app.ui.component.vlist.VerticalList.Model $model;
    final /* synthetic */ int $pos;
    final /* synthetic */ com.navdy.hud.app.ui.component.vlist.viewholder.SwitchViewHolder this$Anon0;

    SwitchViewHolder$select$Anon3(com.navdy.hud.app.ui.component.vlist.viewholder.SwitchViewHolder $outer, com.navdy.hud.app.ui.component.vlist.VerticalList.Model $captured_local_variable$Anon1, int $captured_local_variable$Anon2) {
        this.this$Anon0 = $outer;
        this.$model = $captured_local_variable$Anon1;
        this.$pos = $captured_local_variable$Anon2;
    }

    public void onAnimationEnd(@org.jetbrains.annotations.Nullable android.animation.Animator animation) {
        super.onAnimationEnd(animation);
        this.this$Anon0.isOn = !this.this$Anon0.isOn;
        this.this$Anon0.drawSwitch(this.this$Anon0.isOn, this.this$Anon0.isEnabled);
        com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState selectionState = this.this$Anon0.vlist.getItemSelectionState();
        selectionState.set(this.$model, this.$model.id, this.$pos, -1, -1);
        this.this$Anon0.vlist.performSelectAction(selectionState);
        this.this$Anon0.vlist.unlock();
    }
}
