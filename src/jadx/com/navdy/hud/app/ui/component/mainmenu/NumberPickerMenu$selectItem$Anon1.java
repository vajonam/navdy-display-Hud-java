package com.navdy.hud.app.ui.component.mainmenu;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\b\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\b\u0002"}, d2 = {"<anonymous>", "", "run"}, k = 3, mv = {1, 1, 6})
/* compiled from: NumberPickerMenu.kt */
final class NumberPickerMenu$selectItem$Anon1 implements java.lang.Runnable {
    final /* synthetic */ int $contactIndex;
    final /* synthetic */ com.navdy.hud.app.ui.component.mainmenu.NumberPickerMenu this$Anon0;

    NumberPickerMenu$selectItem$Anon1(com.navdy.hud.app.ui.component.mainmenu.NumberPickerMenu numberPickerMenu, int i) {
        this.this$Anon0 = numberPickerMenu;
        this.$contactIndex = i;
    }

    public final void run() {
        this.this$Anon0.callback.selected((com.navdy.hud.app.framework.contacts.Contact) this.this$Anon0.contacts.get(this.$contactIndex));
    }
}
