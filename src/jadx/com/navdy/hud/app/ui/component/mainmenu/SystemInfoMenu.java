package com.navdy.hud.app.ui.component.mainmenu;

public class SystemInfoMenu implements com.navdy.hud.app.ui.component.mainmenu.IMenu {
    private static final int UPDATE_FREQUENCY = 2000;
    private static final com.navdy.hud.app.ui.component.vlist.VerticalList.Model back;
    private static final java.text.SimpleDateFormat dateFormat = new java.text.SimpleDateFormat("MM/dd/yyyy");
    private static java.lang.String fuelUnitText = resources.getString(com.navdy.hud.app.R.string.litre);
    private static final com.navdy.hud.app.ui.component.vlist.VerticalList.Model infoModel = com.navdy.hud.app.ui.component.vlist.viewholder.ScrollableViewHolder.buildModel(com.navdy.hud.app.R.layout.system_info_lyt);
    private static final java.lang.String infoTitle = resources.getString(com.navdy.hud.app.R.string.carousel_menu_system_info_title);
    private static final java.lang.String noData = resources.getString(com.navdy.hud.app.R.string.si_no_data);
    private static final java.lang.String noFixStr = resources.getString(com.navdy.hud.app.R.string.gps_no_fix);
    private static final java.lang.String off = resources.getString(com.navdy.hud.app.R.string.si_off);
    private static final java.lang.String on = resources.getString(com.navdy.hud.app.R.string.si_on);
    private static java.lang.String pressureUnitText = resources.getString(com.navdy.hud.app.R.string.pressure_unit);
    private static final android.content.res.Resources resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.mainmenu.SystemInfoMenu.class);
    @butterknife.InjectView(2131624424)
    protected android.widget.TextView autoOn;
    private int backSelection;
    private com.squareup.otto.Bus bus;
    private java.util.List<com.navdy.hud.app.ui.component.vlist.VerticalList.Model> cachedList;
    @butterknife.InjectView(2131624443)
    protected android.widget.TextView carModel;
    @butterknife.InjectView(2131624454)
    protected android.widget.TextView checkEngineLight;
    @butterknife.InjectView(2131624440)
    protected android.widget.TextView deviceMileage;
    @butterknife.InjectView(2131624464)
    protected android.widget.TextView dialBt;
    @butterknife.InjectView(2131624462)
    protected android.widget.TextView dialFw;
    @butterknife.InjectView(2131624465)
    protected android.widget.TextView displayBt;
    @butterknife.InjectView(2131624461)
    protected android.widget.TextView displayFw;
    @butterknife.InjectView(2131624458)
    protected android.widget.TextView engineOilPressure;
    @butterknife.InjectView(2131624457)
    protected android.view.ViewGroup engineOilPressureLayout;
    @butterknife.InjectView(2131624452)
    protected android.widget.TextView engineTemp;
    @butterknife.InjectView(2131624455)
    protected android.widget.TextView engineTroubleCodes;
    @butterknife.InjectView(2131624438)
    protected android.widget.TextView fix;
    @butterknife.InjectView(2131624446)
    protected android.widget.TextView fuel;
    @butterknife.InjectView(2131624423)
    protected android.widget.TextView gestures;
    @butterknife.InjectView(2131624435)
    protected android.widget.TextView gps;
    @butterknife.InjectView(2131624441)
    protected android.widget.TextView gpsSpeed;
    /* access modifiers changed from: private */
    public android.os.Handler handler;
    private com.navdy.hud.app.maps.here.HereLocationFixManager hereLocationFixManager;
    @butterknife.InjectView(2131624431)
    protected android.widget.TextView hereMapVerified;
    @butterknife.InjectView(2131624429)
    protected android.widget.TextView hereMapsVersion;
    @butterknife.InjectView(2131624430)
    protected android.widget.TextView hereOfflineMaps;
    @butterknife.InjectView(2131624427)
    protected android.widget.TextView hereSdk;
    @butterknife.InjectView(2131624428)
    protected android.widget.TextView hereUpdated;
    @butterknife.InjectView(2131624432)
    protected android.widget.TextView hereVoiceVerified;
    @butterknife.InjectView(2131624451)
    protected android.widget.TextView maf;
    @butterknife.InjectView(2131624442)
    protected android.widget.TextView make;
    @butterknife.InjectView(2131624425)
    protected android.widget.TextView obdData;
    @butterknife.InjectView(2131624445)
    protected android.view.ViewGroup obdDataLayout;
    @butterknife.InjectView(2131624463)
    protected android.widget.TextView obdFw;
    private com.navdy.hud.app.obd.ObdManager obdManager;
    @butterknife.InjectView(2131624450)
    protected android.widget.TextView odo;
    @butterknife.InjectView(2131624449)
    protected android.view.ViewGroup odometerLayout;
    private com.navdy.hud.app.ui.component.mainmenu.IMenu parent;
    private com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.Presenter presenter;
    protected boolean registered;
    @butterknife.InjectView(2131624433)
    protected android.widget.TextView routeCalc;
    @butterknife.InjectView(2131624434)
    protected android.widget.TextView routePref;
    @butterknife.InjectView(2131624447)
    protected android.widget.TextView rpm;
    @butterknife.InjectView(2131624437)
    com.navdy.hud.app.ui.component.systeminfo.GpsSignalView satelliteView;
    @butterknife.InjectView(2131624436)
    protected android.widget.TextView satellites;
    @butterknife.InjectView(2131624456)
    protected android.view.ViewGroup specialPidsLayout;
    @butterknife.InjectView(2131624448)
    protected android.widget.TextView speed;
    private com.navdy.hud.app.manager.SpeedManager speedManager;
    @butterknife.InjectView(2131624439)
    protected android.widget.TextView temperature;
    @butterknife.InjectView(2131624460)
    protected android.widget.TextView tripFuel;
    @butterknife.InjectView(2131624459)
    protected android.view.ViewGroup tripFuelLayout;
    protected java.lang.String ubloxFixType;
    @butterknife.InjectView(2131624426)
    protected android.widget.TextView uiScaling;
    private java.lang.Runnable updateRunnable = new com.navdy.hud.app.ui.component.mainmenu.SystemInfoMenu.Anon1();
    @butterknife.InjectView(2131624453)
    protected android.widget.TextView voltage;
    private com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent vscrollComponent;
    @butterknife.InjectView(2131624444)
    protected android.widget.TextView year;

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            if (com.navdy.hud.app.ui.component.mainmenu.SystemInfoMenu.this.isMenuLoaded()) {
                com.navdy.hud.app.ui.component.mainmenu.SystemInfoMenu.this.updateTemperature();
                com.navdy.hud.app.ui.component.mainmenu.SystemInfoMenu.this.updateVoltage();
                com.navdy.hud.app.ui.component.mainmenu.SystemInfoMenu.this.setFix();
                com.navdy.hud.app.ui.component.mainmenu.SystemInfoMenu.this.handler.removeCallbacks(this);
                com.navdy.hud.app.ui.component.mainmenu.SystemInfoMenu.this.handler.postDelayed(this, 2000);
            }
        }
    }

    static {
        int backColor = resources.getColor(com.navdy.hud.app.R.color.mm_back);
        back = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.menu_back, com.navdy.hud.app.R.drawable.icon_mm_back, backColor, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, backColor, resources.getString(com.navdy.hud.app.R.string.back), null);
    }

    SystemInfoMenu(com.squareup.otto.Bus bus2, com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent vscrollComponent2, com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.Presenter presenter2, com.navdy.hud.app.ui.component.mainmenu.IMenu parent2) {
        this.bus = bus2;
        this.vscrollComponent = vscrollComponent2;
        this.presenter = presenter2;
        this.parent = parent2;
        this.handler = new android.os.Handler(android.os.Looper.getMainLooper());
        this.obdManager = com.navdy.hud.app.obd.ObdManager.getInstance();
        this.speedManager = com.navdy.hud.app.manager.SpeedManager.getInstance();
    }

    public java.util.List<com.navdy.hud.app.ui.component.vlist.VerticalList.Model> getItems() {
        java.util.List<com.navdy.hud.app.ui.component.vlist.VerticalList.Model> list = new java.util.ArrayList<>();
        list.add(back);
        list.add(infoModel);
        this.cachedList = list;
        return list;
    }

    public int getInitialSelection() {
        return 1;
    }

    public com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex getScrollIndex() {
        return null;
    }

    public void setBackSelectionPos(int n) {
        this.backSelection = n;
    }

    public void setBackSelectionId(int id) {
    }

    public void setSelectedIcon() {
        this.vscrollComponent.setSelectedIconImage(com.navdy.hud.app.R.drawable.icon_settings_navdy_data, null, com.navdy.hud.app.ui.component.image.InitialsImageView.Style.DEFAULT);
        this.vscrollComponent.selectedText.setText(infoTitle);
    }

    public boolean selectItem(com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState selection) {
        sLogger.v("select id:" + selection.id + " pos:" + selection.pos);
        switch (selection.id) {
            case com.navdy.hud.app.R.id.menu_back /*2131624007*/:
                sLogger.v("back");
                com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("back");
                this.presenter.loadMenu(this.parent, com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel.BACK_TO_PARENT, this.backSelection, 0);
                break;
        }
        return true;
    }

    public com.navdy.hud.app.ui.component.mainmenu.IMenu.Menu getType() {
        return com.navdy.hud.app.ui.component.mainmenu.IMenu.Menu.SYSTEM_INFO;
    }

    public boolean isItemClickable(int id, int pos) {
        if (pos == 0) {
            return true;
        }
        return false;
    }

    public com.navdy.hud.app.ui.component.vlist.VerticalList.Model getModelfromPos(int pos) {
        if (this.cachedList == null || this.cachedList.size() <= pos) {
            return null;
        }
        return (com.navdy.hud.app.ui.component.vlist.VerticalList.Model) this.cachedList.get(pos);
    }

    public boolean isBindCallsEnabled() {
        return true;
    }

    public void onBindToView(com.navdy.hud.app.ui.component.vlist.VerticalList.Model model, android.view.View view, int pos, com.navdy.hud.app.ui.component.vlist.VerticalList.ModelState state) {
        if (pos == 1 && ((android.view.ViewGroup) view).getChildCount() > 0) {
            android.view.ViewGroup viewGroup = (android.view.ViewGroup) ((android.view.ViewGroup) view).getChildAt(0);
            butterknife.ButterKnife.inject((java.lang.Object) this, view);
            this.satelliteView.setVisibility(com.navdy.hud.app.ui.component.UISettings.advancedGpsStatsEnabled() ? 0 : 8);
            update();
            if (!this.registered) {
                this.bus.register(this);
                this.registered = true;
                com.navdy.hud.app.obd.ObdManager.getInstance().enableInstantaneousMode(true);
                sLogger.v("registered, enabled instantaneous mode");
            }
            this.handler.removeCallbacks(this.updateRunnable);
            this.handler.postDelayed(this.updateRunnable, 2000);
        }
    }

    public com.navdy.hud.app.ui.component.mainmenu.IMenu getChildMenu(com.navdy.hud.app.ui.component.mainmenu.IMenu parent2, java.lang.String args, java.lang.String path) {
        return null;
    }

    public void onUnload(com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel level) {
        if (this.registered) {
            if (com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getUiStateManager().getHomescreenView().getDisplayMode() != com.navdy.hud.app.ui.component.homescreen.HomeScreen.DisplayMode.SMART_DASH) {
                sLogger.v("onUnload: disable instantaneous mode");
                com.navdy.hud.app.obd.ObdManager.getInstance().enableInstantaneousMode(false);
            } else {
                sLogger.v("onUnload: instantaneous mode still enabled");
            }
            this.bus.unregister(this);
            this.registered = false;
            sLogger.v("unregistered");
        }
        this.displayFw = null;
        this.dialFw = null;
        this.obdFw = null;
        this.dialBt = null;
        this.displayBt = null;
        this.gps = null;
        this.satellites = null;
        this.fix = null;
        this.temperature = null;
        this.deviceMileage = null;
        this.hereSdk = null;
        this.hereUpdated = null;
        this.hereMapsVersion = null;
        this.hereOfflineMaps = null;
        this.hereMapVerified = null;
        this.hereVoiceVerified = null;
        this.make = null;
        this.carModel = null;
        this.year = null;
        this.fuel = null;
        this.rpm = null;
        this.speed = null;
        this.odo = null;
        this.maf = null;
        this.engineTemp = null;
        this.voltage = null;
        this.gestures = null;
        this.autoOn = null;
        this.obdData = null;
        this.uiScaling = null;
        this.routeCalc = null;
        this.routePref = null;
    }

    public void onItemSelected(com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState selection) {
    }

    public void onScrollIdle() {
    }

    public void onFastScrollStart() {
    }

    public void onFastScrollEnd() {
    }

    public void showToolTip() {
    }

    public boolean isFirstItemEmpty() {
        return true;
    }

    private void update() {
        updateVersions();
        updateStats();
        updateHere();
        updateVehicleInfo();
        updateGenericPref();
        updateNavPref();
    }

    private java.lang.String removeBrackets(java.lang.String str) {
        return str.replace(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.OPEN_BRACKET, "").replace(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.CLOSE_BRACKET, "");
    }

    private void updateVersions() {
        java.lang.String versionName = com.navdy.hud.app.BuildConfig.VERSION_NAME;
        if (com.navdy.hud.app.util.DeviceUtil.isUserBuild()) {
            versionName = com.navdy.hud.app.util.OTAUpdateService.shortVersion(versionName);
        }
        this.displayFw.setText(versionName);
        com.navdy.hud.app.device.dial.DialManager dialManager = com.navdy.hud.app.device.dial.DialManager.getInstance();
        if (dialManager.isDialConnected()) {
            try {
                com.navdy.hud.app.device.dial.DialFirmwareUpdater dialFirmwareUpdater = dialManager.getDialFirmwareUpdater();
                java.lang.String str = null;
                if (dialFirmwareUpdater != null) {
                    int incremental = dialFirmwareUpdater.getVersions().dial.incrementalVersion;
                    if (incremental != -1) {
                        str = java.lang.String.valueOf(incremental);
                    }
                    if (!android.text.TextUtils.isEmpty(str)) {
                        this.dialFw.setText(str);
                    }
                }
            } catch (Throwable t) {
                sLogger.e(t);
            }
            java.lang.String str2 = dialManager.getDialName();
            if (!android.text.TextUtils.isEmpty(str2)) {
                this.dialBt.setText(removeBrackets(str2));
            }
        }
        java.lang.String str3 = android.bluetooth.BluetoothAdapter.getDefaultAdapter().getName();
        if (!android.text.TextUtils.isEmpty(str3)) {
            this.displayBt.setText(removeBrackets(str3));
        }
        java.lang.String str4 = this.obdManager.getObdChipFirmwareVersion();
        if (!android.text.TextUtils.isEmpty(str4)) {
            this.obdFw.setText(str4);
        }
    }

    private void updateStats() {
        updateTemperature();
        updateDeviceMileage();
        updateGPSSpeed();
        com.navdy.hud.app.maps.here.HereMapsManager hereMapsManager = com.navdy.hud.app.maps.here.HereMapsManager.getInstance();
        if (hereMapsManager.isInitialized()) {
            this.hereLocationFixManager = hereMapsManager.getLocationFixManager();
        }
        setFix();
    }

    private void updateDeviceMileage() {
        if (this.presenter != null) {
            long d = this.presenter.tripManager.getTotalDistanceTravelledWithNavdy();
            sLogger.v("navdy distance travelled:" + d);
            if (d > 0) {
                com.navdy.hud.app.maps.util.DistanceConverter.Distance distance = new com.navdy.hud.app.maps.util.DistanceConverter.Distance();
                com.navdy.hud.app.maps.util.DistanceConverter.convertToDistance(com.navdy.hud.app.manager.SpeedManager.getInstance().getSpeedUnit(), (float) d, distance);
                this.deviceMileage.setText(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.getFormattedDistance(distance.value, distance.unit, false));
            }
        }
    }

    /* access modifiers changed from: private */
    public void updateTemperature() {
        int temp = com.navdy.hud.app.analytics.AnalyticsSupport.getCurrentTemperature();
        if (temp != -1) {
            this.temperature.setText(java.lang.String.valueOf(temp) + " " + kotlin.text.Typography.degree + resources.getString(com.navdy.hud.app.R.string.si_celsius));
        }
    }

    private void updateVehicleInfo() {
        com.navdy.hud.app.profile.DriverProfile profile = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getDriverProfileManager().getCurrentProfile();
        java.lang.String str = profile.getCarMake();
        if (!android.text.TextUtils.isEmpty(str)) {
            this.make.setText(str);
        }
        java.lang.String str2 = profile.getCarModel();
        if (!android.text.TextUtils.isEmpty(str2)) {
            this.carModel.setText(str2);
        }
        java.lang.String str3 = profile.getCarYear();
        if (!android.text.TextUtils.isEmpty(str3)) {
            this.year.setText(str3);
        }
        updateDynamicVehicleInfo();
    }

    private void updateHere() {
        com.navdy.hud.app.maps.here.HereMapsManager hereMapsManager = com.navdy.hud.app.maps.here.HereMapsManager.getInstance();
        if (!hereMapsManager.isInitializing()) {
            if (hereMapsManager.isInitialized()) {
                this.hereSdk.setText(hereMapsManager.getVersion());
                if (hereMapsManager.isVoiceSkinsLoaded()) {
                    this.hereVoiceVerified.setText(resources.getString(com.navdy.hud.app.R.string.si_yes));
                } else {
                    this.hereVoiceVerified.setText(resources.getString(com.navdy.hud.app.R.string.si_no));
                }
                if (hereMapsManager.isMapDataVerified()) {
                    sLogger.v("map package installed count:" + hereMapsManager.getMapPackageCount());
                    this.hereMapVerified.setText(resources.getString(com.navdy.hud.app.R.string.si_yes));
                } else {
                    this.hereMapVerified.setText(resources.getString(com.navdy.hud.app.R.string.si_no));
                }
                com.navdy.hud.app.maps.here.HereOfflineMapsVersion offlineMapsVersion = com.navdy.hud.app.maps.here.HereMapsManager.getInstance().getOfflineMapsVersion();
                if (offlineMapsVersion != null) {
                    java.lang.String mapPackages = null;
                    java.util.List<java.lang.String> offlinePackages = offlineMapsVersion.getPackages();
                    if (offlinePackages != null) {
                        int len = offlinePackages.size();
                        java.lang.StringBuilder builder = new java.lang.StringBuilder();
                        for (int i = 0; i < len; i++) {
                            if (i != 0) {
                                builder.append(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA);
                                builder.append(" ");
                            }
                            builder.append((java.lang.String) offlinePackages.get(i));
                        }
                        mapPackages = builder.toString();
                    }
                    java.lang.String dateUpdated = offlineMapsVersion.getDate() != null ? dateFormat.format(offlineMapsVersion.getDate()) : null;
                    java.lang.String mapVersion = offlineMapsVersion.getVersion();
                    if (!android.text.TextUtils.isEmpty(dateUpdated)) {
                        this.hereUpdated.setText(dateUpdated);
                    }
                    if (!android.text.TextUtils.isEmpty(mapPackages)) {
                        this.hereOfflineMaps.setText(mapPackages);
                    }
                    if (!android.text.TextUtils.isEmpty(mapVersion)) {
                        this.hereMapsVersion.setText(mapVersion);
                        return;
                    }
                    return;
                }
                return;
            }
            this.hereSdk.setText(hereMapsManager.getError().name());
        }
    }

    private void updateDynamicVehicleInfo() {
        int i;
        boolean shouldShowSpecialPidsSection;
        boolean obdConnected = this.obdManager.isConnected();
        com.navdy.hud.app.obd.ObdManager.ConnectionType connectionType = this.obdManager.getConnectionType();
        android.view.ViewGroup viewGroup = this.obdDataLayout;
        if (connectionType == com.navdy.hud.app.obd.ObdManager.ConnectionType.POWER_ONLY) {
            i = 8;
        } else {
            i = 0;
        }
        viewGroup.setVisibility(i);
        if (obdConnected) {
            updateFuel();
            updateRPM();
            updateSpeed();
            updateMAF();
            updateEngineTemperature();
            updateVoltage();
            updateOdometer();
            com.navdy.obd.PidSet supportedPids = this.obdManager.getSupportedPids();
            if (supportedPids.contains((int) com.navdy.obd.Pids.ENGINE_OIL_PRESSURE) || supportedPids.contains((int) com.navdy.obd.Pids.ENGINE_TRIP_FUEL)) {
                shouldShowSpecialPidsSection = true;
            } else {
                shouldShowSpecialPidsSection = false;
            }
            if (!shouldShowSpecialPidsSection) {
                this.specialPidsLayout.setVisibility(8);
            } else {
                this.specialPidsLayout.setVisibility(0);
                updateEngineFuelPressure();
                updateEngineTripFuel();
            }
            this.checkEngineLight.setText(this.obdManager.isCheckEngineLightOn() ? on : off);
            java.util.List<java.lang.String> troubleCodes = this.obdManager.getTroubleCodes();
            if (troubleCodes == null || troubleCodes.size() <= 0) {
                this.engineTroubleCodes.setText(noData);
                return;
            }
            java.lang.StringBuilder builder = new java.lang.StringBuilder();
            for (java.lang.String troubleCode : troubleCodes) {
                builder.append(troubleCode + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA);
            }
            builder.deleteCharAt(builder.length() - 1);
            this.engineTroubleCodes.setText(builder.toString());
            return;
        }
        this.specialPidsLayout.setVisibility(8);
        this.odometerLayout.setVisibility(8);
        this.checkEngineLight.setText(noData);
        this.engineTroubleCodes.setText(noData);
    }

    private void updateEngineFuelPressure() {
        int val = (int) this.obdManager.getPidValue(com.navdy.obd.Pids.ENGINE_OIL_PRESSURE);
        if (val > 0) {
            this.engineOilPressure.setText(val + " " + pressureUnitText);
        } else {
            this.engineOilPressure.setText(noData);
        }
    }

    private void updateEngineTripFuel() {
        int val = (int) this.obdManager.getPidValue(com.navdy.obd.Pids.ENGINE_TRIP_FUEL);
        if (val > 0) {
            this.tripFuel.setText(val + " " + fuelUnitText);
        } else {
            this.tripFuel.setText(noData);
        }
    }

    private void updateFuel() {
        int val = this.obdManager.getFuelLevel();
        if (val != -1) {
            this.fuel.setText(resources.getString(com.navdy.hud.app.R.string.si_fuel_level, new java.lang.Object[]{java.lang.Integer.valueOf(val)}));
            return;
        }
        this.fuel.setText(noData);
    }

    private void updateRPM() {
        int val = this.obdManager.getEngineRpm();
        if (val != -1) {
            this.rpm.setText(java.lang.String.valueOf(val));
        } else {
            this.rpm.setText(noData);
        }
    }

    private void updateSpeed() {
        int val = this.speedManager.getCurrentSpeed();
        if (val != -1) {
            this.speed.setText(java.lang.String.valueOf(val));
        } else {
            this.speed.setText(noData);
        }
    }

    private void updateGPSSpeed() {
        int val = this.speedManager.getGpsSpeed();
        if (val != -1) {
            this.gpsSpeed.setText(java.lang.String.valueOf(val));
        } else {
            this.gpsSpeed.setText(noData);
        }
    }

    private void updateMAF() {
        int val = (int) this.obdManager.getInstantFuelConsumption();
        if (val > 0) {
            this.maf.setText(java.lang.String.valueOf(val));
        } else {
            this.maf.setText(noData);
        }
    }

    /* access modifiers changed from: private */
    public void updateVoltage() {
        double dbl = this.obdManager.getBatteryVoltage();
        if (dbl > 0.0d) {
            this.voltage.setText(java.lang.String.format("%.1f", new java.lang.Object[]{java.lang.Double.valueOf(dbl)}) + resources.getString(com.navdy.hud.app.R.string.si_volt));
            return;
        }
        this.voltage.setText(noData);
    }

    private void updateEngineTemperature() {
        int unit;
        int rounded;
        com.navdy.hud.app.profile.DriverProfile profile = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getDriverProfileManager().getCurrentProfile();
        double celsius = this.obdManager.getPidValue(5);
        if (celsius > 0.0d) {
            switch (profile.getUnitSystem()) {
                case UNIT_SYSTEM_METRIC:
                    unit = com.navdy.hud.app.R.string.si_celsius;
                    rounded = (int) java.lang.Math.round(celsius);
                    break;
                default:
                    unit = com.navdy.hud.app.R.string.si_fahrenheit;
                    rounded = (int) java.lang.Math.round((1.8d * celsius) + 32.0d);
                    break;
            }
            this.engineTemp.setText(java.lang.String.format("%d", new java.lang.Object[]{java.lang.Integer.valueOf(rounded)}) + " " + kotlin.text.Typography.degree + resources.getString(unit));
            return;
        }
        this.engineTemp.setText(noData);
    }

    private void updateOdometer() {
        if (this.obdManager.getSupportedPids().contains((int) com.navdy.obd.Pids.TOTAL_VEHICLE_DISTANCE)) {
            this.odometerLayout.setVisibility(0);
            int val = (int) this.obdManager.getPidValue(com.navdy.obd.Pids.TOTAL_VEHICLE_DISTANCE);
            if (val > 0) {
                com.navdy.hud.app.maps.util.DistanceConverter.Distance distance = new com.navdy.hud.app.maps.util.DistanceConverter.Distance();
                com.navdy.hud.app.maps.util.DistanceConverter.convertToDistance(this.speedManager.getSpeedUnit(), (float) val, distance);
                this.odo.setText(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.getFormattedDistance(distance.value, distance.unit, false));
                return;
            }
            this.odo.setText(noData);
            return;
        }
        this.odometerLayout.setVisibility(8);
    }

    private void updateGenericPref() {
        com.navdy.hud.app.profile.DriverProfile profile = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getDriverProfileManager().getCurrentProfile();
        com.navdy.service.library.events.preferences.InputPreferences inputPref = profile.getInputPreferences();
        if (inputPref != null) {
            if (java.lang.Boolean.TRUE.equals(inputPref.use_gestures)) {
                this.gestures.setText(resources.getString(com.navdy.hud.app.R.string.si_enabled));
            } else {
                this.gestures.setText(resources.getString(com.navdy.hud.app.R.string.si_disabled));
            }
        }
        com.navdy.hud.app.obd.ObdDeviceConfigurationManager configurationManager = this.obdManager.getObdDeviceConfigurationManager();
        if (configurationManager != null) {
            if (configurationManager.isAutoOnEnabled()) {
                this.autoOn.setText(resources.getString(com.navdy.hud.app.R.string.si_enabled));
            } else {
                this.autoOn.setText(resources.getString(com.navdy.hud.app.R.string.si_disabled));
            }
        }
        com.navdy.service.library.events.preferences.DriverProfilePreferences.ObdScanSetting scanSetting = profile.getObdScanSetting();
        if (scanSetting != null) {
            switch (scanSetting) {
                case SCAN_ON:
                case SCAN_DEFAULT_ON:
                    this.obdData.setText(resources.getString(com.navdy.hud.app.R.string.si_on));
                    break;
                default:
                    this.obdData.setText(resources.getString(com.navdy.hud.app.R.string.si_off));
                    break;
            }
        }
        com.navdy.service.library.events.preferences.DriverProfilePreferences.DisplayFormat displayFormat = profile.getDisplayFormat();
        if (displayFormat != null) {
            switch (displayFormat) {
                case DISPLAY_FORMAT_COMPACT:
                    this.uiScaling.setText(resources.getString(com.navdy.hud.app.R.string.si_compact));
                    return;
                case DISPLAY_FORMAT_NORMAL:
                    this.uiScaling.setText(resources.getString(com.navdy.hud.app.R.string.si_normal));
                    return;
                default:
                    return;
            }
        }
    }

    private void updateNavPref() {
        com.navdy.service.library.events.preferences.NavigationPreferences navPref = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile().getNavigationPreferences();
        if (navPref != null) {
            switch (navPref.routingType) {
                case ROUTING_FASTEST:
                    this.routeCalc.setText(resources.getString(com.navdy.hud.app.R.string.si_fastest));
                    break;
                case ROUTING_SHORTEST:
                    this.routeCalc.setText(resources.getString(com.navdy.hud.app.R.string.si_shortest));
                    break;
            }
            java.lang.StringBuilder stringBuilder = new java.lang.StringBuilder();
            if (java.lang.Boolean.TRUE.equals(navPref.allowHighways)) {
                stringBuilder.append(resources.getString(com.navdy.hud.app.R.string.si_highways));
            }
            if (java.lang.Boolean.TRUE.equals(navPref.allowTollRoads)) {
                formatRoutePref(stringBuilder, resources.getString(com.navdy.hud.app.R.string.si_tollroads));
            }
            if (java.lang.Boolean.TRUE.equals(navPref.allowFerries)) {
                formatRoutePref(stringBuilder, resources.getString(com.navdy.hud.app.R.string.si_ferries));
            }
            if (java.lang.Boolean.TRUE.equals(navPref.allowTunnels)) {
                formatRoutePref(stringBuilder, resources.getString(com.navdy.hud.app.R.string.si_tunnels));
            }
            if (java.lang.Boolean.TRUE.equals(navPref.allowUnpavedRoads)) {
                formatRoutePref(stringBuilder, resources.getString(com.navdy.hud.app.R.string.si_unpaved_roads));
            }
            if (java.lang.Boolean.TRUE.equals(navPref.allowAutoTrains)) {
                formatRoutePref(stringBuilder, resources.getString(com.navdy.hud.app.R.string.si_auto_trains));
            }
            stringBuilder.append(com.navdy.hud.app.framework.glance.GlanceConstants.NEWLINE);
            this.routePref.setText(stringBuilder.toString());
        }
    }

    private void formatRoutePref(java.lang.StringBuilder stringBuilder, java.lang.String str) {
        boolean uppercase = false;
        if (stringBuilder.length() > 0) {
            stringBuilder.append(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA);
            stringBuilder.append(" ");
        } else {
            uppercase = true;
        }
        if (uppercase) {
            str = java.lang.Character.toUpperCase(str.charAt(0)) + str.substring(1);
        }
        stringBuilder.append(str);
    }

    @com.squareup.otto.Subscribe
    public void onGpsSatelliteData(com.navdy.hud.app.device.gps.GpsUtils.GpsSatelliteData satelliteData) {
        try {
            if (isMenuLoaded()) {
                int seen = satelliteData.data.getInt(com.navdy.hud.app.device.gps.GpsConstants.GPS_SATELLITE_SEEN, 0);
                int used = satelliteData.data.getInt(com.navdy.hud.app.device.gps.GpsConstants.GPS_SATELLITE_USED, 0);
                if (seen > 0 || used > 0) {
                    this.satellites.setText(resources.getString(com.navdy.hud.app.R.string.si_seen, new java.lang.Object[]{java.lang.Integer.valueOf(used), java.lang.Integer.valueOf(seen)}));
                } else {
                    this.satellites.setText(noData);
                }
                this.ubloxFixType = getFixTypeDescription(satelliteData.data.getInt(com.navdy.hud.app.device.gps.GpsConstants.GPS_FIX_TYPE, 0));
                int max = satelliteData.data.getInt(com.navdy.hud.app.device.gps.GpsConstants.GPS_SATELLITE_MAX_DB, 0);
                if (max > 0) {
                    this.gps.setText(resources.getString(com.navdy.hud.app.R.string.si_gps_signal, new java.lang.Object[]{java.lang.Integer.valueOf(max)}));
                } else {
                    this.gps.setText(noData);
                }
                if (com.navdy.hud.app.ui.component.UISettings.advancedGpsStatsEnabled()) {
                    com.navdy.hud.app.ui.component.systeminfo.GpsSignalView.SatelliteData[] data = new com.navdy.hud.app.ui.component.systeminfo.GpsSignalView.SatelliteData[seen];
                    for (int i = 0; i < seen; i++) {
                        data[i] = new com.navdy.hud.app.ui.component.systeminfo.GpsSignalView.SatelliteData(satelliteData.data.getInt(com.navdy.hud.app.device.gps.GpsConstants.GPS_SATELLITE_ID + (i + 1)), satelliteData.data.getInt(com.navdy.hud.app.device.gps.GpsConstants.GPS_SATELLITE_DB + (i + 1)), satelliteData.data.getString(com.navdy.hud.app.device.gps.GpsConstants.GPS_SATELLITE_PROVIDER + (i + 1)));
                    }
                    java.util.Arrays.sort(data);
                    this.satelliteView.setSatelliteData(data);
                }
                sLogger.v("satellite data { seen=" + seen + " used=" + used + " fixType=" + this.ubloxFixType + " maxDB=" + max);
            }
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }

    private static java.lang.String getFixTypeDescription(int fixType) {
        switch (fixType) {
            case 0:
                return "No Fix";
            case 1:
                return "Standard 2D/3D";
            case 2:
                return "Differential";
            case 4:
                return "RTK Fixed";
            case 5:
                return "RTK Float";
            case 6:
                return "DR Fix";
            default:
                return "Unknown";
        }
    }

    @com.squareup.otto.Subscribe
    public void onNavigationPrefChangeEvent(com.navdy.service.library.events.preferences.NavigationPreferences preferences) {
        if (isMenuLoaded()) {
            updateNavPref();
        }
    }

    @com.squareup.otto.Subscribe
    public void onDriverProfilePrefChangeEvent(com.navdy.hud.app.event.DriverProfileUpdated event) {
        if (isMenuLoaded() && event.state == com.navdy.hud.app.event.DriverProfileUpdated.State.UPDATED) {
            updateGenericPref();
        }
    }

    @com.squareup.otto.Subscribe
    public void onSupportedPidsChanged(com.navdy.hud.app.obd.ObdManager.ObdSupportedPidsChangedEvent supportedPidsChangedEvent) {
        if (isMenuLoaded()) {
            updateSpeed();
            updateGPSSpeed();
            updateDynamicVehicleInfo();
        }
    }

    @com.squareup.otto.Subscribe
    public void onPidChangeEvent(com.navdy.hud.app.obd.ObdManager.ObdPidChangeEvent event) {
        if (isMenuLoaded()) {
            switch (event.pid.getId()) {
                case 5:
                    updateEngineTemperature();
                    return;
                case 12:
                    updateRPM();
                    return;
                case 13:
                    updateSpeed();
                    return;
                case 47:
                    updateFuel();
                    return;
                case 256:
                    updateMAF();
                    return;
                case com.navdy.obd.Pids.ENGINE_OIL_PRESSURE /*258*/:
                    updateEngineFuelPressure();
                    return;
                case com.navdy.obd.Pids.ENGINE_TRIP_FUEL /*259*/:
                    updateEngineTripFuel();
                    return;
                case com.navdy.obd.Pids.TOTAL_VEHICLE_DISTANCE /*260*/:
                    updateOdometer();
                    return;
                default:
                    return;
            }
        }
    }

    @com.squareup.otto.Subscribe
    public void ObdStateChangeEvent(com.navdy.hud.app.obd.ObdManager.ObdConnectionStatusEvent event) {
        if (isMenuLoaded()) {
            updateSpeed();
            updateDynamicVehicleInfo();
        }
    }

    @com.squareup.otto.Subscribe
    public void onSpeedUnitChanged(com.navdy.hud.app.manager.SpeedManager.SpeedUnitChanged event) {
        if (isMenuLoaded()) {
            updateSpeed();
            updateGPSSpeed();
        }
    }

    @com.squareup.otto.Subscribe
    public void GPSSpeedChangeEvent(com.navdy.hud.app.maps.MapEvents.GPSSpeedEvent event) {
        if (isMenuLoaded()) {
            updateSpeed();
            updateGPSSpeed();
        }
    }

    @com.squareup.otto.Subscribe
    public void onSpeedDataExpired(com.navdy.hud.app.manager.SpeedManager.SpeedDataExpired speedDataExpired) {
        if (isMenuLoaded()) {
            updateSpeed();
            updateGPSSpeed();
        }
    }

    /* access modifiers changed from: private */
    public boolean isMenuLoaded() {
        return this.dialFw != null;
    }

    /* access modifiers changed from: private */
    public void setFix() {
        if (this.hereLocationFixManager != null) {
            android.content.res.Resources res = com.navdy.hud.app.HudApplication.getAppContext().getResources();
            com.navdy.hud.app.maps.MapEvents.LocationFix event = this.hereLocationFixManager.getLocationFixEvent();
            sLogger.v("fix [" + event.locationAvailable + "] phone[" + event.usingPhoneLocation + "] u-blox[" + event.usingLocalGpsLocation + "] type[" + this.ubloxFixType + "]");
            if (!event.locationAvailable) {
                this.fix.setText(noFixStr);
            } else if (event.usingPhoneLocation) {
                this.fix.setText(res.getText(com.navdy.hud.app.R.string.gps_phone));
            } else if (event.usingLocalGpsLocation) {
                this.fix.setText(this.ubloxFixType != null ? this.ubloxFixType : res.getText(com.navdy.hud.app.R.string.gps_ublox_navdy));
            } else {
                this.fix.setText(noFixStr);
            }
        }
    }
}
