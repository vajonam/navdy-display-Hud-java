package com.navdy.hud.app.ui.component.destination;

public final class DestinationPickerView$$InjectAdapter extends dagger.internal.Binding<com.navdy.hud.app.ui.component.destination.DestinationPickerView> implements dagger.MembersInjector<com.navdy.hud.app.ui.component.destination.DestinationPickerView> {
    private dagger.internal.Binding<com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.Presenter> presenter;

    public DestinationPickerView$$InjectAdapter() {
        super(null, "members/com.navdy.hud.app.ui.component.destination.DestinationPickerView", false, com.navdy.hud.app.ui.component.destination.DestinationPickerView.class);
    }

    public void attach(dagger.internal.Linker linker) {
        this.presenter = linker.requestBinding("com.navdy.hud.app.ui.component.destination.DestinationPickerScreen$Presenter", com.navdy.hud.app.ui.component.destination.DestinationPickerView.class, getClass().getClassLoader());
    }

    public void getDependencies(java.util.Set<dagger.internal.Binding<?>> set, java.util.Set<dagger.internal.Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.presenter);
    }

    public void injectMembers(com.navdy.hud.app.ui.component.destination.DestinationPickerView object) {
        object.presenter = (com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.Presenter) this.presenter.get();
    }
}
