package com.navdy.hud.app.ui.component.vlist.viewholder;

public class LoadingViewHolder extends com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder {
    private static final int LOADING_ANIMATION_DELAY = 33;
    private static final int LOADING_ANIMATION_DURATION = 500;
    private static final int loadingImageSize;
    private static final int loadingImageSizeUnselected;
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.sLogger;
    /* access modifiers changed from: private */
    public android.animation.ObjectAnimator loadingAnimator;
    private android.widget.ImageView loadingImage;

    class Anon1 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
        Anon1() {
        }

        public void onAnimationEnd(android.animation.Animator animation) {
            if (com.navdy.hud.app.ui.component.vlist.viewholder.LoadingViewHolder.this.loadingAnimator != null) {
                com.navdy.hud.app.ui.component.vlist.viewholder.LoadingViewHolder.this.loadingAnimator.setStartDelay(33);
                com.navdy.hud.app.ui.component.vlist.viewholder.LoadingViewHolder.this.loadingAnimator.start();
                return;
            }
            com.navdy.hud.app.ui.component.vlist.viewholder.LoadingViewHolder.sLogger.v("abandon loading animation");
        }
    }

    static {
        android.content.res.Resources resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        loadingImageSize = resources.getDimensionPixelSize(com.navdy.hud.app.R.dimen.vlist_loading_image);
        loadingImageSizeUnselected = resources.getDimensionPixelSize(com.navdy.hud.app.R.dimen.vlist_loading_image_unselected);
    }

    public static com.navdy.hud.app.ui.component.vlist.viewholder.LoadingViewHolder buildViewHolder(android.view.ViewGroup parent, com.navdy.hud.app.ui.component.vlist.VerticalList vlist, android.os.Handler handler) {
        return new com.navdy.hud.app.ui.component.vlist.viewholder.LoadingViewHolder((android.view.ViewGroup) android.view.LayoutInflater.from(parent.getContext()).inflate(com.navdy.hud.app.R.layout.vlist_loading_item, parent, false), vlist, handler);
    }

    public static com.navdy.hud.app.ui.component.vlist.VerticalList.Model buildModel() {
        com.navdy.hud.app.ui.component.vlist.VerticalList.Model model = new com.navdy.hud.app.ui.component.vlist.VerticalList.Model();
        model.type = com.navdy.hud.app.ui.component.vlist.VerticalList.ModelType.LOADING;
        model.id = com.navdy.hud.app.R.id.vlist_loading;
        return model;
    }

    public LoadingViewHolder(android.view.ViewGroup layout, com.navdy.hud.app.ui.component.vlist.VerticalList vlist, android.os.Handler handler) {
        super(layout, vlist, handler);
        this.loadingImage = (android.widget.ImageView) layout.findViewById(com.navdy.hud.app.R.id.loading);
    }

    public com.navdy.hud.app.ui.component.vlist.VerticalList.ModelType getModelType() {
        return com.navdy.hud.app.ui.component.vlist.VerticalList.ModelType.LOADING;
    }

    public void setItemState(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.State state, com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.AnimationType animation, int duration, boolean startFluctuator) {
        int size;
        if (state == com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.State.SELECTED) {
            size = loadingImageSize;
        } else {
            size = loadingImageSizeUnselected;
        }
        switch (animation) {
            case NONE:
            case INIT:
                startLoadingAnimation();
                android.view.ViewGroup.MarginLayoutParams lytParams = (android.view.ViewGroup.MarginLayoutParams) this.loadingImage.getLayoutParams();
                lytParams.width = size;
                lytParams.height = size;
                return;
            case MOVE:
                startLoadingAnimation();
                android.animation.Animator animator = com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils.animateDimension(this.loadingImage, size);
                animator.setDuration((long) duration);
                animator.setInterpolator(this.interpolator);
                animator.start();
                return;
            default:
                return;
        }
    }

    public void bind(com.navdy.hud.app.ui.component.vlist.VerticalList.Model model, com.navdy.hud.app.ui.component.vlist.VerticalList.ModelState modelState) {
    }

    public void clearAnimation() {
        stopLoadingAnimation();
    }

    public void select(com.navdy.hud.app.ui.component.vlist.VerticalList.Model model, int pos, int duration) {
    }

    public void copyAndPosition(android.widget.ImageView imageC, android.widget.TextView titleC, android.widget.TextView subTitleC, android.widget.TextView subTitle2C, boolean setImage) {
    }

    private void startLoadingAnimation() {
        if (this.loadingAnimator == null) {
            this.loadingAnimator = android.animation.ObjectAnimator.ofFloat(this.loadingImage, android.view.View.ROTATION, new float[]{360.0f});
            this.loadingAnimator.setDuration(500);
            this.loadingAnimator.setInterpolator(new android.view.animation.AccelerateDecelerateInterpolator());
            this.loadingAnimator.addListener(new com.navdy.hud.app.ui.component.vlist.viewholder.LoadingViewHolder.Anon1());
        }
        sLogger.v("started loading animation");
        this.loadingAnimator.start();
    }

    private void stopLoadingAnimation() {
        if (this.loadingAnimator != null) {
            sLogger.v("cancelled loading animation");
            this.loadingAnimator.removeAllListeners();
            this.loadingAnimator.cancel();
            this.loadingImage.setRotation(0.0f);
            this.loadingAnimator = null;
        }
    }
}
