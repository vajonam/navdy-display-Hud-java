package com.navdy.hud.app.ui.component.carousel;

public interface IProgressIndicator {
    int getCurrentItem();

    android.animation.AnimatorSet getItemMoveAnimator(int i, int i2);

    android.graphics.RectF getItemPos(int i);

    void setCurrentItem(int i);

    void setCurrentItem(int i, int i2);

    void setItemCount(int i);

    void setOrientation(com.navdy.hud.app.ui.component.carousel.CarouselIndicator.Orientation orientation);
}
