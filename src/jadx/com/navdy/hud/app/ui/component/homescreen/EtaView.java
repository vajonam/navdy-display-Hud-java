package com.navdy.hud.app.ui.component.homescreen;

public class EtaView extends android.widget.RelativeLayout {
    public static final int CLOCK_UPDATE_INTERVAL = 30000;
    @butterknife.InjectView(2131624334)
    android.widget.TextView bottomTextView;
    private android.content.BroadcastReceiver broadCastReceiver;
    private com.squareup.otto.Bus bus;
    private android.os.Handler handler;
    private com.navdy.hud.app.ui.component.homescreen.HomeScreenView homeScreenView;
    private java.lang.String lastETA;
    private java.lang.String lastETA_AmPm;
    private java.lang.String lastTimeLeft;
    private java.lang.String lastTta;
    /* access modifiers changed from: private */
    public com.navdy.service.library.log.Logger logger;
    /* access modifiers changed from: private */
    public java.lang.Runnable runnable;
    private boolean showEta;
    private java.lang.StringBuilder stringBuilder;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.common.TimeHelper timeHelper;
    @butterknife.InjectView(2131624333)
    android.widget.TextView topTextView;

    class Anon1 extends android.content.BroadcastReceiver {
        Anon1() {
        }

        public void onReceive(android.content.Context context, android.content.Intent intent) {
            if ("android.intent.action.TIMEZONE_CHANGED".equals(intent.getAction())) {
                com.navdy.hud.app.ui.component.homescreen.EtaView.this.logger.v("timezone change broadcast");
                com.navdy.hud.app.ui.component.homescreen.EtaView.this.timeHelper.updateLocale();
            }
        }
    }

    class Anon2 implements java.lang.Runnable {
        Anon2() {
        }

        public void run() {
            com.navdy.hud.app.ui.component.homescreen.EtaView.this.updateTime();
            com.navdy.hud.app.ui.component.homescreen.EtaView.this.postDelayed(com.navdy.hud.app.ui.component.homescreen.EtaView.this.runnable, 30000);
        }
    }

    class Anon3 implements java.lang.Runnable {
        Anon3() {
        }

        public void run() {
            com.navdy.hud.app.ui.component.homescreen.EtaView.this.updateTime();
            com.navdy.hud.app.ui.component.homescreen.EtaView.this.updateEta();
        }
    }

    public EtaView(android.content.Context context) {
        this(context, null);
    }

    public EtaView(android.content.Context context, android.util.AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public EtaView(android.content.Context context, android.util.AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.stringBuilder = new java.lang.StringBuilder();
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        this.logger = com.navdy.hud.app.ui.component.homescreen.HomeScreenView.sLogger;
        this.handler = new android.os.Handler();
        super.onFinishInflate();
        butterknife.ButterKnife.inject((android.view.View) this);
        if (!isInEditMode()) {
            if (this.timeHelper == null) {
                this.timeHelper = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getTimeHelper();
            }
            android.content.IntentFilter intentFilter = new android.content.IntentFilter();
            intentFilter.addAction("android.intent.action.TIMEZONE_CHANGED");
            this.broadCastReceiver = new com.navdy.hud.app.ui.component.homescreen.EtaView.Anon1();
            com.navdy.hud.app.HudApplication.getAppContext().registerReceiver(this.broadCastReceiver, intentFilter);
            this.runnable = new com.navdy.hud.app.ui.component.homescreen.EtaView.Anon2();
            post(this.runnable);
        }
    }

    public void init(com.navdy.hud.app.ui.component.homescreen.HomeScreenView homeScreenView2) {
        this.homeScreenView = homeScreenView2;
        this.bus = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getBus();
        this.bus.register(this);
    }

    public void setShowEta(boolean showEta2) {
        this.showEta = showEta2;
        this.handler.post(new com.navdy.hud.app.ui.component.homescreen.EtaView.Anon3());
    }

    @com.squareup.otto.Subscribe
    public void onTrafficDelayEvent(com.navdy.hud.app.maps.MapEvents.TrafficDelayEvent event) {
        setEtaTextColor(com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.badTrafficColor);
    }

    @com.squareup.otto.Subscribe
    public void onTrafficDelayDismissEvent(com.navdy.hud.app.maps.MapEvents.TrafficDelayDismissEvent event) {
        setEtaTextColor(-1);
    }

    private void setEtaTextColor(int color) {
        if (this.showEta) {
            this.bottomTextView.setTextColor(color);
        }
    }

    public void clearState() {
        this.lastETA = null;
        this.lastETA_AmPm = null;
        this.lastTta = null;
        this.lastTimeLeft = null;
        this.topTextView.setText("");
        this.bottomTextView.setText("");
        setEtaTextColor(-1);
    }

    /* access modifiers changed from: private */
    public void updateEta() {
        if (this.showEta) {
            this.bottomTextView.setText(this.lastTimeLeft);
        }
    }

    @com.squareup.otto.Subscribe
    public void onTimeSettingsChange(com.navdy.hud.app.common.TimeHelper.UpdateClock event) {
        updateTime();
    }

    @com.squareup.otto.Subscribe
    public void onManeuverDisplay(com.navdy.hud.app.maps.MapEvents.ManeuverDisplay event) {
        long time;
        if (this.homeScreenView.isNavigationActive()) {
            if (event.etaDate == null) {
                this.logger.w("etaDate is not set");
                return;
            }
            long time2 = event.etaDate.getTime() - java.lang.System.currentTimeMillis();
            if (time2 < 0) {
                time = 0;
            } else {
                time = java.util.concurrent.TimeUnit.MILLISECONDS.toMinutes(time2);
            }
            this.lastTimeLeft = com.navdy.hud.app.maps.util.RouteUtils.formatEtaMinutes(getResources(), (int) time);
            if (!android.text.TextUtils.equals(event.eta, this.lastETA)) {
                this.lastETA = event.eta;
                this.lastTta = com.navdy.hud.app.maps.here.HereMapUtil.convertDateToTta(event.etaDate);
            }
            if (!android.text.TextUtils.equals(event.etaAmPm, this.lastETA_AmPm)) {
                this.lastETA_AmPm = event.etaAmPm;
            }
            updateEta();
        }
    }

    /* access modifiers changed from: private */
    public void updateTime() {
        java.lang.String time = this.timeHelper.formatTime(new java.util.Date(), this.stringBuilder);
        if (this.showEta) {
            this.topTextView.setText(this.lastETA + this.lastETA_AmPm);
            return;
        }
        this.topTextView.setText(this.timeHelper.getDay());
        this.bottomTextView.setText(time + this.stringBuilder.toString().toLowerCase());
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.handler != null) {
            this.handler.removeCallbacks(this.runnable);
        }
    }

    public void setView(com.navdy.hud.app.view.MainView.CustomAnimationMode mode) {
    }

    public java.lang.String getLastTta() {
        return this.lastTta;
    }
}
