package com.navdy.hud.app.ui.component.homescreen;

public class OpenRoadView$$ViewInjector {
    public static void inject(butterknife.ButterKnife.Finder finder, com.navdy.hud.app.ui.component.homescreen.OpenRoadView target, java.lang.Object source) {
        target.openMapRoadInfo = (android.widget.TextView) finder.findRequiredView(source, com.navdy.hud.app.R.id.openMapRoadInfo, "field 'openMapRoadInfo'");
    }

    public static void reset(com.navdy.hud.app.ui.component.homescreen.OpenRoadView target) {
        target.openMapRoadInfo = null;
    }
}
