package com.navdy.hud.app.ui.component.mainmenu;

public class PlacesMenu implements com.navdy.hud.app.ui.component.mainmenu.IMenu {
    public static final int CANCEL_POSITION = 1;
    private static java.util.List<java.lang.String> CONFIRMATION_CHOICES = new java.util.ArrayList();
    public static final int NAVIGATE_POSITION = 0;
    private static final int SELECTION_ANIMATION_DELAY = 1000;
    private static final com.navdy.hud.app.ui.component.vlist.VerticalList.Model back;
    private static final int backColor = resources.getColor(com.navdy.hud.app.R.color.mm_back);
    public static final int bkColorUnselected = resources.getColor(com.navdy.hud.app.R.color.icon_bk_color_unselected);
    private static final int contactColor = resources.getColor(com.navdy.hud.app.R.color.mm_place_contact);
    private static final int favColor = resources.getColor(com.navdy.hud.app.R.color.mm_place_favorite);
    private static final int gasColor = resources.getColor(com.navdy.hud.app.R.color.mm_place_gas);
    private static final int homeColor = resources.getColor(com.navdy.hud.app.R.color.mm_place_home);
    public static final java.lang.String places = resources.getString(com.navdy.hud.app.R.string.carousel_menu_map_places);
    private static final int placesColor = resources.getColor(com.navdy.hud.app.R.color.mm_places);
    private static final int recentColor = resources.getColor(com.navdy.hud.app.R.color.mm_recent_places);
    private static final com.navdy.hud.app.ui.component.vlist.VerticalList.Model recentPlaces;
    private static final android.content.res.Resources resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.mainmenu.PlacesMenu.class);
    private static final com.navdy.hud.app.ui.component.vlist.VerticalList.Model search;
    public static final java.lang.String suggested = resources.getString(com.navdy.hud.app.R.string.suggested);
    public static final int suggestedColor = resources.getColor(com.navdy.hud.app.R.color.mm_place_suggested);
    private static final int workColor = resources.getColor(com.navdy.hud.app.R.color.mm_place_work);
    private int backSelection;
    private int backSelectionId;
    private com.squareup.otto.Bus bus;
    private java.util.List<com.navdy.hud.app.ui.component.vlist.VerticalList.Model> cachedList;
    private java.util.HashSet<java.lang.String> latlngSet = new java.util.HashSet<>();
    private com.navdy.hud.app.ui.component.mainmenu.NearbyPlacesMenu nearbyPlacesMenu;
    private com.navdy.hud.app.ui.component.mainmenu.IMenu parent;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.Presenter presenter;
    private com.navdy.hud.app.ui.component.mainmenu.RecentPlacesMenu recentPlacesMenu;
    private java.util.List<com.navdy.hud.app.ui.component.vlist.VerticalList.Model> returnToCacheList;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent vscrollComponent;

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            com.navdy.hud.app.ui.component.mainmenu.PlacesMenu.this.presenter.close();
            com.navdy.hud.app.maps.MapsNotification.showMapsEngineNotInitializedToast();
        }
    }

    class Anon2 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.hud.app.ui.component.vlist.VerticalList.Model val$model;

        class Anon1 implements java.lang.Runnable {
            Anon1() {
            }

            public void run() {
                com.navdy.hud.app.framework.destinations.DestinationsManager.getInstance().requestNavigation((com.navdy.hud.app.framework.destinations.Destination) com.navdy.hud.app.ui.component.mainmenu.PlacesMenu.Anon2.this.val$model.state);
            }
        }

        Anon2(com.navdy.hud.app.ui.component.vlist.VerticalList.Model model) {
            this.val$model = model;
        }

        public void run() {
            com.navdy.hud.app.ui.component.mainmenu.PlacesMenu.this.presenter.close(new com.navdy.hud.app.ui.component.mainmenu.PlacesMenu.Anon2.Anon1());
        }
    }

    class Anon3 implements com.navdy.hud.app.ui.component.ChoiceLayout.IListener {
        final /* synthetic */ com.navdy.hud.app.framework.destinations.Destination val$destination;

        class Anon1 implements java.lang.Runnable {
            Anon1() {
            }

            public void run() {
                com.navdy.hud.app.framework.destinations.DestinationsManager.getInstance().requestNavigation(com.navdy.hud.app.ui.component.mainmenu.PlacesMenu.Anon3.this.val$destination);
            }
        }

        Anon3(com.navdy.hud.app.framework.destinations.Destination destination) {
            this.val$destination = destination;
        }

        public void executeItem(int pos, int id) {
            com.navdy.hud.app.ui.component.ConfirmationLayout confirmationLayout = com.navdy.hud.app.ui.component.mainmenu.PlacesMenu.this.presenter.getConfirmationLayout();
            if (confirmationLayout == null) {
                com.navdy.hud.app.ui.component.mainmenu.PlacesMenu.sLogger.v("confirmation layout not found");
                return;
            }
            switch (pos) {
                case 0:
                    com.navdy.hud.app.ui.component.mainmenu.PlacesMenu.sLogger.v("called requestNavigation");
                    com.navdy.hud.app.ui.component.mainmenu.PlacesMenu.this.presenter.close(new com.navdy.hud.app.ui.component.mainmenu.PlacesMenu.Anon3.Anon1());
                    return;
                case 1:
                    confirmationLayout.setVisibility(8);
                    com.navdy.hud.app.ui.component.mainmenu.PlacesMenu.this.presenter.reset();
                    com.navdy.hud.app.ui.component.mainmenu.PlacesMenu.this.vscrollComponent.verticalList.unlock();
                    return;
                default:
                    return;
            }
        }

        public void itemSelected(int pos, int id) {
        }
    }

    static {
        java.lang.String title = resources.getString(com.navdy.hud.app.R.string.back);
        int fluctuatorColor = backColor;
        back = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.menu_back, com.navdy.hud.app.R.drawable.icon_mm_back, fluctuatorColor, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, fluctuatorColor, title, null);
        java.lang.String title2 = resources.getString(com.navdy.hud.app.R.string.carousel_menu_recent_place);
        int fluctuatorColor2 = resources.getColor(com.navdy.hud.app.R.color.mm_recent_places);
        recentPlaces = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.places_menu_recent, com.navdy.hud.app.R.drawable.icon_place_recent_2, fluctuatorColor2, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, fluctuatorColor2, title2, null);
        java.lang.String title3 = resources.getString(com.navdy.hud.app.R.string.carousel_menu_search_title);
        int fluctuatorColor3 = resources.getColor(com.navdy.hud.app.R.color.mm_search);
        search = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.main_menu_search, com.navdy.hud.app.R.drawable.icon_mm_search_2, fluctuatorColor3, bkColorUnselected, fluctuatorColor3, title3, null);
        CONFIRMATION_CHOICES.add(resources.getString(com.navdy.hud.app.R.string.destination_start_navigation_yes));
        CONFIRMATION_CHOICES.add(resources.getString(com.navdy.hud.app.R.string.destination_start_navigation_cancel));
    }

    public PlacesMenu(com.squareup.otto.Bus bus2, com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent vscrollComponent2, com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.Presenter presenter2, com.navdy.hud.app.ui.component.mainmenu.IMenu parent2) {
        this.bus = bus2;
        this.vscrollComponent = vscrollComponent2;
        this.presenter = presenter2;
        this.parent = parent2;
    }

    public java.util.List<com.navdy.hud.app.ui.component.vlist.VerticalList.Model> getItems() {
        if (this.cachedList != null) {
            return this.cachedList;
        }
        java.util.List<com.navdy.hud.app.ui.component.vlist.VerticalList.Model> list = new java.util.ArrayList<>();
        this.returnToCacheList = new java.util.ArrayList();
        list.add(back);
        com.navdy.service.library.events.DeviceInfo deviceInfo = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getRemoteDeviceInfo();
        boolean isConnected = !com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile().isDefaultProfile();
        if (isConnected && deviceInfo == null) {
            isConnected = false;
        }
        sLogger.v("isConnected:" + isConnected);
        if (isConnected && com.navdy.hud.app.util.RemoteCapabilitiesUtil.supportsPlaceSearch()) {
            list.add(search);
        }
        list.add(recentPlaces);
        int firstIndex = list.size();
        int counter = 0;
        com.navdy.hud.app.framework.destinations.DestinationsManager destinationsManager = com.navdy.hud.app.framework.destinations.DestinationsManager.getInstance();
        this.latlngSet.clear();
        java.util.List<com.navdy.hud.app.framework.destinations.Destination> suggestedDestinations = destinationsManager.getSuggestedDestinations();
        if (suggestedDestinations == null || suggestedDestinations.size() <= 0) {
            sLogger.v("no suggested destinations");
        } else {
            sLogger.v("suggested destinations:" + suggestedDestinations.size());
            for (com.navdy.hud.app.framework.destinations.Destination destination : suggestedDestinations) {
                int counter2 = counter + 1;
                com.navdy.hud.app.ui.component.vlist.VerticalList.Model model = buildModel(destination, counter);
                model.subTitle = suggested;
                model.state = destination;
                model.extras = new java.util.HashMap();
                model.extras.put(com.navdy.hud.app.ui.component.vlist.VerticalList.Model.SUBTITLE_COLOR, java.lang.String.valueOf(suggestedColor));
                list.add(model);
                this.returnToCacheList.add(model);
                this.latlngSet.add(destination.displayPositionLatitude + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA + destination.displayPositionLongitude + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA + destination.navigationPositionLatitude + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA + destination.navigationPositionLongitude);
                counter = counter2;
            }
        }
        java.util.List<com.navdy.hud.app.framework.destinations.Destination> favoriteDestinations = destinationsManager.getFavoriteDestinations();
        if (favoriteDestinations == null || favoriteDestinations.size() <= 0) {
            sLogger.v("no favorite destinations");
        } else {
            sLogger.v("favorite destinations:" + favoriteDestinations.size());
            for (com.navdy.hud.app.framework.destinations.Destination destination2 : favoriteDestinations) {
                if (this.latlngSet.contains(destination2.displayPositionLatitude + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA + destination2.displayPositionLongitude + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA + destination2.navigationPositionLatitude + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA + destination2.navigationPositionLongitude)) {
                    sLogger.v("latlng already seen in suggested:" + destination2.destinationTitle);
                } else {
                    int counter3 = counter + 1;
                    com.navdy.hud.app.ui.component.vlist.VerticalList.Model model2 = buildModel(destination2, counter);
                    model2.state = destination2;
                    list.add(model2);
                    this.returnToCacheList.add(model2);
                    counter = counter3;
                }
            }
        }
        com.navdy.hud.app.framework.destinations.DestinationsManager.GasDestination gasDestination = com.navdy.hud.app.framework.destinations.DestinationsManager.getInstance().getGasDestination();
        if (gasDestination != null) {
            int counter4 = counter + 1;
            com.navdy.hud.app.ui.component.vlist.VerticalList.Model model3 = buildModel(gasDestination.destination, counter);
            model3.subTitle = suggested;
            model3.state = gasDestination.destination;
            model3.extras = new java.util.HashMap();
            model3.extras.put(com.navdy.hud.app.ui.component.vlist.VerticalList.Model.SUBTITLE_COLOR, java.lang.String.valueOf(suggestedColor));
            if (gasDestination.showFirst) {
                list.add(firstIndex, model3);
            } else {
                list.add(model3);
            }
            this.returnToCacheList.add(model3);
            int i = counter4;
        }
        this.cachedList = list;
        return list;
    }

    public int getInitialSelection() {
        if (this.cachedList == null) {
            return 0;
        }
        int index = this.cachedList.indexOf(recentPlaces);
        if (index != -1) {
            return index;
        }
        if (this.cachedList.size() >= 3) {
            return 2;
        }
        return 1;
    }

    public com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex getScrollIndex() {
        return null;
    }

    public void setBackSelectionPos(int n) {
        this.backSelection = n;
    }

    public void setBackSelectionId(int id) {
        this.backSelectionId = id;
    }

    public void setSelectedIcon() {
        this.vscrollComponent.setSelectedIconColorImage(com.navdy.hud.app.R.drawable.icon_mm_places_2, placesColor, null, 1.0f);
        this.vscrollComponent.selectedText.setText(places);
    }

    public com.navdy.hud.app.ui.component.vlist.VerticalList.Model getModelfromPos(int pos) {
        if (this.cachedList == null || this.cachedList.size() <= pos) {
            return null;
        }
        return (com.navdy.hud.app.ui.component.vlist.VerticalList.Model) this.cachedList.get(pos);
    }

    public boolean isBindCallsEnabled() {
        return false;
    }

    public void onBindToView(com.navdy.hud.app.ui.component.vlist.VerticalList.Model model, android.view.View view, int pos, com.navdy.hud.app.ui.component.vlist.VerticalList.ModelState state) {
    }

    public com.navdy.hud.app.ui.component.mainmenu.IMenu getChildMenu(com.navdy.hud.app.ui.component.mainmenu.IMenu parent2, java.lang.String args, java.lang.String path) {
        return null;
    }

    public void onUnload(com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel level) {
        switch (level) {
            case CLOSE:
                if (this.returnToCacheList != null) {
                    sLogger.v("pm:unload add to cache");
                    com.navdy.hud.app.ui.component.vlist.VerticalModelCache.addToCache(this.returnToCacheList);
                    this.returnToCacheList = null;
                }
                if (this.recentPlacesMenu != null) {
                    this.recentPlacesMenu.onUnload(com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel.CLOSE);
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void onItemSelected(com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState selection) {
    }

    public void onScrollIdle() {
    }

    public void onFastScrollStart() {
    }

    public void onFastScrollEnd() {
    }

    public void showToolTip() {
    }

    public boolean isFirstItemEmpty() {
        return true;
    }

    public boolean selectItem(com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState selection) {
        sLogger.v("select id:" + selection.id + " pos:" + selection.pos);
        switch (selection.id) {
            case com.navdy.hud.app.R.id.main_menu_search /*2131623995*/:
                sLogger.v("search");
                com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("search");
                if (this.nearbyPlacesMenu == null) {
                    this.nearbyPlacesMenu = new com.navdy.hud.app.ui.component.mainmenu.NearbyPlacesMenu(this.bus, this.vscrollComponent, this.presenter, this);
                }
                this.presenter.loadMenu(this.nearbyPlacesMenu, com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel.SUB_LEVEL, selection.pos, 0);
                break;
            case com.navdy.hud.app.R.id.menu_back /*2131624007*/:
                sLogger.v("back");
                com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("back");
                this.presenter.loadMenu(this.parent, com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel.BACK_TO_PARENT, this.backSelection, this.backSelectionId);
                this.backSelectionId = 0;
                break;
            case com.navdy.hud.app.R.id.places_menu_recent /*2131624036*/:
                sLogger.v("recent places");
                com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("recent_places");
                if (this.recentPlacesMenu == null) {
                    this.recentPlacesMenu = new com.navdy.hud.app.ui.component.mainmenu.RecentPlacesMenu(this.bus, this.vscrollComponent, this.presenter, this);
                }
                this.presenter.loadMenu(this.recentPlacesMenu, com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel.SUB_LEVEL, selection.pos, 0);
                break;
            default:
                launchDestination((com.navdy.hud.app.ui.component.vlist.VerticalList.Model) this.cachedList.get(selection.pos));
                break;
        }
        return true;
    }

    public com.navdy.hud.app.ui.component.mainmenu.IMenu.Menu getType() {
        return com.navdy.hud.app.ui.component.mainmenu.IMenu.Menu.PLACES;
    }

    public boolean isItemClickable(int id, int pos) {
        return true;
    }

    public static com.navdy.hud.app.ui.component.vlist.VerticalList.Model buildModel(com.navdy.hud.app.framework.destinations.Destination destination, int id) {
        java.lang.String subTitle;
        java.lang.String title = destination.destinationTitle;
        if (destination.recentTimeLabel != null) {
            subTitle = destination.recentTimeLabel;
        } else {
            subTitle = destination.destinationSubtitle;
        }
        return getPlaceModel(destination, id, title, subTitle);
    }

    public static com.navdy.hud.app.ui.component.vlist.VerticalList.Model getPlaceModel(com.navdy.hud.app.framework.destinations.Destination destination, int id, java.lang.String title, java.lang.String subTitle) {
        if (destination == null) {
            return com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(id, com.navdy.hud.app.R.drawable.icon_mm_places_2, placesColor, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, placesColor, title, subTitle);
        }
        switch (destination.favoriteDestinationType) {
            case FAVORITE_HOME:
                return com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(id, com.navdy.hud.app.R.drawable.icon_place_home_2, homeColor, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, homeColor, title, subTitle);
            case FAVORITE_WORK:
                return com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(id, com.navdy.hud.app.R.drawable.icon_place_work_2, workColor, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, workColor, title, subTitle);
            case FAVORITE_CONTACT:
                com.navdy.hud.app.ui.component.vlist.VerticalList.Model model = com.navdy.hud.app.ui.component.vlist.viewholder.IconsTwoViewHolder.buildModel(id, com.navdy.hud.app.R.drawable.icon_user_bg_1, 0, contactColor, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, title, subTitle);
                model.extras = new java.util.HashMap<>();
                model.extras.put(com.navdy.hud.app.ui.component.vlist.VerticalList.Model.INITIALS, destination.initials);
                return model;
            default:
                if (destination.placeCategory == com.navdy.hud.app.framework.destinations.Destination.PlaceCategory.SUGGESTED_RECENT) {
                    return com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(id, com.navdy.hud.app.R.drawable.icon_place_recent_2, recentColor, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, recentColor, title, subTitle);
                }
                switch (destination.favoriteDestinationType) {
                    case FAVORITE_CUSTOM:
                        int i = id;
                        r1 = com.navdy.hud.app.R.drawable.icon_place_favorite_2;
                        return com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(i, com.navdy.hud.app.R.drawable.icon_place_favorite_2, favColor, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, favColor, title, subTitle);
                    case FAVORITE_CALENDAR:
                        return com.navdy.hud.app.ui.component.vlist.viewholder.IconsTwoViewHolder.buildModel(id, com.navdy.hud.app.R.drawable.icon_place_calendar, com.navdy.hud.app.R.drawable.icon_place_calendar_sm, homeColor, -1, title, subTitle);
                    default:
                        if (destination.destinationType == com.navdy.hud.app.framework.destinations.Destination.DestinationType.FIND_GAS) {
                            return com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(id, com.navdy.hud.app.R.drawable.icon_places_gas_2, gasColor, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, gasColor, title, subTitle);
                        } else if (destination.recommendation || destination.placeCategory != com.navdy.hud.app.framework.destinations.Destination.PlaceCategory.RECENT) {
                            int i2 = id;
                            r1 = com.navdy.hud.app.R.drawable.icon_place_favorite_2;
                            return com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(i2, com.navdy.hud.app.R.drawable.icon_place_favorite_2, favColor, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, favColor, title, subTitle);
                        } else {
                            return com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(id, com.navdy.hud.app.R.drawable.icon_place_recent_2, recentColor, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, recentColor, title, subTitle);
                        }
                }
        }
    }

    /* access modifiers changed from: 0000 */
    public void launchDestination(com.navdy.hud.app.ui.component.vlist.VerticalList.Model m) {
        if (!com.navdy.hud.app.maps.here.HereMapsManager.getInstance().isInitialized()) {
            sLogger.w("Here maps engine not initialized, exit");
            this.presenter.performSelectionAnimation(new com.navdy.hud.app.ui.component.mainmenu.PlacesMenu.Anon1());
            return;
        }
        com.navdy.hud.app.ui.component.vlist.VerticalList.Model model = new com.navdy.hud.app.ui.component.vlist.VerticalList.Model(m);
        com.navdy.hud.app.maps.here.HereNavigationManager hereNavigationManager = com.navdy.hud.app.maps.here.HereNavigationManager.getInstance();
        if (!hereNavigationManager.isNavigationModeOn() || hereNavigationManager.hasArrived()) {
            sLogger.v("called requestNavigation");
            this.presenter.performSelectionAnimation(new com.navdy.hud.app.ui.component.mainmenu.PlacesMenu.Anon2(model), 1000);
            return;
        }
        com.navdy.hud.app.framework.destinations.Destination destination = (com.navdy.hud.app.framework.destinations.Destination) model.state;
        com.navdy.hud.app.ui.component.ConfirmationLayout confirmationLayout = this.presenter.getConfirmationLayout();
        if (confirmationLayout == null) {
            sLogger.v("confirmation layout not found");
            return;
        }
        com.navdy.hud.app.ui.component.destination.DestinationConfirmationHelper.configure(confirmationLayout, model, destination);
        confirmationLayout.setChoices(CONFIRMATION_CHOICES, 0, new com.navdy.hud.app.ui.component.mainmenu.PlacesMenu.Anon3(destination));
        confirmationLayout.setVisibility(0);
    }
}
