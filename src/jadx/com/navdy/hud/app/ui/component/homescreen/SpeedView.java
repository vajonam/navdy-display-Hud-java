package com.navdy.hud.app.ui.component.homescreen;

public class SpeedView extends android.widget.LinearLayout {
    public static final boolean ANIMATE_UNIT_VIEW = false;
    private com.squareup.otto.Bus bus;
    private int excessiveSpeedingWarningColor;
    private int lastSpeed;
    private com.navdy.hud.app.manager.SpeedManager.SpeedUnit lastSpeedUnit;
    private com.navdy.service.library.log.Logger logger;
    private int speedLimit;
    private com.navdy.hud.app.manager.SpeedManager speedManager;
    @butterknife.InjectView(2131624344)
    android.widget.TextView speedUnitView;
    @butterknife.InjectView(2131624345)
    android.widget.TextView speedView;
    private int speedingWarningColor;

    public SpeedView(android.content.Context context) {
        this(context, null);
    }

    public SpeedView(android.content.Context context, android.util.AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SpeedView(android.content.Context context, android.util.AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.lastSpeed = -1;
        this.speedingWarningColor = context.getResources().getColor(com.navdy.hud.app.R.color.cyan);
        this.excessiveSpeedingWarningColor = context.getResources().getColor(com.navdy.hud.app.R.color.speed_very_fast_warning_color);
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        this.logger = com.navdy.hud.app.ui.component.homescreen.HomeScreenView.sLogger;
        super.onFinishInflate();
        butterknife.ButterKnife.inject((android.view.View) this);
        if (!isInEditMode()) {
            this.speedManager = com.navdy.hud.app.manager.SpeedManager.getInstance();
            this.bus = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getBus();
            this.bus.register(this);
        }
    }

    @com.squareup.otto.Subscribe
    public void ObdStateChangeEvent(com.navdy.hud.app.obd.ObdManager.ObdConnectionStatusEvent event) {
        this.lastSpeed = -1;
        this.lastSpeedUnit = null;
        setTrackingSpeed();
    }

    @com.squareup.otto.Subscribe
    public void ObdPidChangeEvent(com.navdy.hud.app.obd.ObdManager.ObdPidChangeEvent event) {
        switch (event.pid.getId()) {
            case 13:
                setTrackingSpeed();
                return;
            default:
                return;
        }
    }

    @com.squareup.otto.Subscribe
    public void onSpeedUnitChanged(com.navdy.hud.app.manager.SpeedManager.SpeedUnitChanged event) {
        setTrackingSpeed();
    }

    @com.squareup.otto.Subscribe
    public void GPSSpeedChangeEvent(com.navdy.hud.app.maps.MapEvents.GPSSpeedEvent event) {
        setTrackingSpeed();
    }

    @com.squareup.otto.Subscribe
    public void onSpeedDataExpired(com.navdy.hud.app.manager.SpeedManager.SpeedDataExpired speedDataExpired) {
        setTrackingSpeed();
    }

    private void setTrackingSpeed() {
        int speed = this.speedManager.getCurrentSpeed();
        if (speed < 0) {
            speed = 0;
        }
        com.navdy.hud.app.manager.SpeedManager.SpeedUnit speedUnit = this.speedManager.getSpeedUnit();
        if (speed != this.lastSpeed) {
            this.lastSpeed = speed;
            this.speedView.setText(java.lang.String.valueOf(speed));
        }
        if (speedUnit != this.lastSpeedUnit) {
            this.lastSpeedUnit = speedUnit;
            java.lang.String str = "";
            switch (speedUnit) {
                case MILES_PER_HOUR:
                    str = com.navdy.hud.app.ui.component.homescreen.HomeScreenConstants.SPEED_MPH;
                    break;
                case KILOMETERS_PER_HOUR:
                    str = com.navdy.hud.app.ui.component.homescreen.HomeScreenConstants.SPEED_KM;
                    break;
                case METERS_PER_SECOND:
                    str = com.navdy.hud.app.ui.component.homescreen.HomeScreenConstants.SPEED_METERS;
                    break;
            }
            this.speedUnitView.setText(str);
        }
        int speedLimitThreshold = 0;
        switch (speedUnit) {
            case MILES_PER_HOUR:
                speedLimitThreshold = 8;
                break;
            case KILOMETERS_PER_HOUR:
                speedLimitThreshold = 13;
                break;
        }
        if (this.speedLimit <= 0) {
            this.speedView.setTextColor(-1);
        } else if (speed >= this.speedLimit + speedLimitThreshold) {
            this.speedView.setTextColor(this.excessiveSpeedingWarningColor);
        } else if (speed >= this.speedLimit) {
            this.speedView.setTextColor(this.speedingWarningColor);
        } else {
            this.speedView.setTextColor(-1);
        }
    }

    public void setSpeedLimit(int speedLimit2) {
        this.speedLimit = speedLimit2;
        setTrackingSpeed();
    }

    public void clearState() {
        this.lastSpeed = -1;
        this.lastSpeedUnit = null;
        setTrackingSpeed();
    }

    public void getTopAnimator(android.animation.AnimatorSet.Builder builder, boolean out) {
    }

    public void resetTopViewsAnimator() {
        this.speedUnitView.setAlpha(1.0f);
        this.speedUnitView.setTranslationX(0.0f);
        this.speedView.setVisibility(0);
    }
}
