package com.navdy.hud.app.ui.component.vmenu;

public class VerticalMenuComponent {
    public static final int ANIMATION_IN_OUT_DURATION = 150;
    public static final int CLICK_ANIMATION_DURATION = 50;
    public static final int CLOSE_ANIMATION_DURATION = 150;
    private static final int FAST_SCROLL_APPEARANCE_ANIM_TIME = 50;
    private static final int FAST_SCROLL_CLOSE_MENU_WAIT_PERIOD = 150;
    private static final int FAST_SCROLL_EXIT_DELAY = 800;
    private static final int FAST_SCROLL_TRIGGER_CLICKS = 10;
    private static final int FAST_SCROLL_TRIGGER_TIME = ((int) java.util.concurrent.TimeUnit.SECONDS.toMillis(2));
    private static final int INDICATOR_FADE_IDLE_DURATION = 2000;
    private static final int INDICATOR_FADE_IN_DURATION = 500;
    private static final int INDICATOR_FADE_OUT_DURATION = 500;
    private static final float MAIN_TO_SELECTED_ICON_SCALE = 1.0f;
    public static final int MIN_FAST_SCROLL_ITEM = 40;
    public static final int MIN_INDEX_ENTRY_COUNT = 2;
    private static final int NO_ANIMATION = -1;
    private static final float SELECTED_TO_MAIN_ICON_SCALE = 1.2f;
    public static final int animateTranslateY;
    private static final int closeContainerHeight;
    private static final int closeContainerScrollY;
    private static final int closeHaloColor;
    private static final int indicatorY;
    private static final int[] location = new int[2];
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger("VerticalMenuC");
    private android.view.animation.Interpolator accelerateInterpolator = new android.view.animation.AccelerateInterpolator();
    /* access modifiers changed from: private */
    public android.widget.ImageView animImageView;
    /* access modifiers changed from: private */
    public android.widget.TextView animSubTitle;
    /* access modifiers changed from: private */
    public android.widget.TextView animSubTitle2;
    /* access modifiers changed from: private */
    public android.widget.TextView animTitle;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.Callback callback;
    private int clickCount;
    private long clickTime;
    @butterknife.InjectView(2131624509)
    public android.view.ViewGroup closeContainer;
    @butterknife.InjectView(2131624508)
    public android.view.View closeContainerScrim;
    @butterknife.InjectView(2131624511)
    public com.navdy.hud.app.ui.component.HaloView closeHalo;
    @butterknife.InjectView(2131624510)
    public android.view.ViewGroup closeIconContainer;
    private java.lang.Runnable closeIconEndAction = new com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.Anon9();
    /* access modifiers changed from: private */
    public boolean closeMenuAnimationActive;
    private com.navdy.hud.app.ui.framework.DefaultAnimationListener closeMenuHideListener = new com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.Anon5();
    private com.navdy.hud.app.ui.framework.DefaultAnimationListener closeMenuShowListener = new com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.Anon1();
    private com.navdy.hud.app.ui.component.vlist.VerticalList.ContainerCallback containerCallback = new com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.Anon7();
    private android.view.animation.Interpolator decelerateInterpolator = new android.view.animation.DecelerateInterpolator();
    private java.lang.Runnable fadeOutRunnable = new com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.Anon8();
    private final int fastScrollCloseWaitPeriod;
    @butterknife.InjectView(2131624505)
    public android.view.ViewGroup fastScrollContainer;
    private int fastScrollCurrentItem;
    private com.navdy.hud.app.ui.framework.DefaultAnimationListener fastScrollIn = new com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.Anon2();
    private com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex fastScrollIndex;
    private int fastScrollOffset;
    private com.navdy.hud.app.ui.framework.DefaultAnimationListener fastScrollOut = new com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.Anon3();
    @butterknife.InjectView(2131624506)
    public android.widget.TextView fastScrollText;
    /* access modifiers changed from: private */
    public java.lang.Runnable fastScrollTimeout = new com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.Anon4();
    /* access modifiers changed from: private */
    public android.os.Handler handler = new android.os.Handler();
    @butterknife.InjectView(2131624507)
    public com.navdy.hud.app.ui.component.carousel.CarouselIndicator indicator;
    private long lastUpEvent;
    @butterknife.InjectView(2131624496)
    public android.view.ViewGroup leftContainer;
    private android.view.animation.Interpolator linearInterpolator = new android.view.animation.LinearInterpolator();
    /* access modifiers changed from: private */
    public boolean listLoaded;
    private java.util.HashSet<com.navdy.hud.app.manager.InputManager.CustomKeyEvent> overrideDefaultKeyEvents = new java.util.HashSet<>();
    private android.view.ViewGroup parentContainer;
    @butterknife.InjectView(2131624503)
    public com.navdy.hud.app.ui.component.vlist.VerticalRecyclerView recyclerView;
    @butterknife.InjectView(2131624502)
    public android.view.ViewGroup rightContainer;
    private com.navdy.hud.app.ui.component.vlist.VerticalList.Direction scrollDirection;
    @butterknife.InjectView(2131624501)
    public android.widget.FrameLayout selectedCustomView;
    @butterknife.InjectView(2131624498)
    public com.navdy.hud.app.ui.component.image.IconColorImageView selectedIconColorImage;
    @butterknife.InjectView(2131624499)
    public com.navdy.hud.app.ui.component.image.InitialsImageView selectedIconImage;
    @butterknife.InjectView(2131624497)
    public android.view.ViewGroup selectedImage;
    @butterknife.InjectView(2131624500)
    public android.widget.TextView selectedText;
    /* access modifiers changed from: private */
    public boolean stoppingFastScrolling;
    private java.util.HashMap<java.lang.Integer, android.graphics.Bitmap> sublistAnimationCache = new java.util.HashMap<>();
    @butterknife.InjectView(2131624504)
    public com.navdy.hud.app.view.ToolTipView toolTip;
    public com.navdy.hud.app.ui.component.vlist.VerticalList verticalList;
    private com.navdy.hud.app.ui.component.vlist.VerticalList.Callback vlistCallback = new com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.Anon6();

    class Anon1 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
        Anon1() {
        }

        public void onAnimationStart(android.animation.Animator animation) {
            com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.sLogger.v("closeMenuShowListener");
            com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.this.closeContainer.setVisibility(0);
            com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.this.closeContainerScrim.setVisibility(0);
            com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.this.closeContainerScrim.setAlpha(0.0f);
        }

        public void onAnimationEnd(android.animation.Animator animation) {
            com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.this.closeMenuAnimationActive = false;
        }
    }

    class Anon10 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
        final /* synthetic */ java.lang.Runnable val$endAction;
        final /* synthetic */ int val$startDelay;

        Anon10(java.lang.Runnable runnable, int i) {
            this.val$endAction = runnable;
            this.val$startDelay = i;
        }

        public void onAnimationEnd(android.animation.Animator animation) {
            android.view.ViewPropertyAnimator vp = com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.this.rightContainer.animate().alpha(0.0f).setDuration(150).withEndAction(this.val$endAction);
            if (this.val$startDelay > 0) {
                vp.setStartDelay((long) this.val$startDelay);
            }
            vp.start();
        }
    }

    class Anon11 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
        final /* synthetic */ java.lang.Runnable val$endAction;

        class Anon1 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
            Anon1() {
            }

            public void onAnimationEnd(android.animation.Animator animation) {
                com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.this.animImageView.setVisibility(4);
                com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.this.animTitle.setVisibility(4);
                com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.this.animSubTitle.setVisibility(4);
                com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.this.animSubTitle2.setVisibility(4);
                if (com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.Anon11.this.val$endAction != null) {
                    com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.Anon11.this.val$endAction.run();
                }
            }
        }

        Anon11(java.lang.Runnable runnable) {
            this.val$endAction = runnable;
        }

        public void onAnimationEnd(android.animation.Animator animation) {
            android.animation.AnimatorSet set = new android.animation.AnimatorSet();
            android.animation.AnimatorSet.Builder builder = set.play(android.animation.ObjectAnimator.ofFloat(com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.this.rightContainer, android.view.View.ALPHA, new float[]{1.0f}));
            builder.with(android.animation.ObjectAnimator.ofFloat(com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.this.selectedImage, android.view.View.ALPHA, new float[]{1.0f}));
            builder.with(android.animation.ObjectAnimator.ofFloat(com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.this.selectedText, android.view.View.ALPHA, new float[]{1.0f}));
            set.setDuration(100);
            set.addListener(new com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.Anon11.Anon1());
            set.start();
        }
    }

    class Anon12 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
        final /* synthetic */ java.lang.Runnable val$startAction;

        Anon12(java.lang.Runnable runnable) {
            this.val$startAction = runnable;
        }

        public void onAnimationEnd(android.animation.Animator animation) {
            com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.this.rightContainer.setAlpha(0.0f);
            com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.this.rightContainer.setScaleX(1.0f);
            com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.this.rightContainer.setScaleY(1.0f);
            if (this.val$startAction != null) {
                this.val$startAction.run();
            }
        }
    }

    class Anon13 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
        final /* synthetic */ java.lang.Runnable val$endAction;
        final /* synthetic */ java.lang.Runnable val$startAction;

        Anon13(java.lang.Runnable runnable, java.lang.Runnable runnable2) {
            this.val$startAction = runnable;
            this.val$endAction = runnable2;
        }

        public void onAnimationStart(android.animation.Animator animation) {
            if (this.val$startAction != null) {
                this.val$startAction.run();
            }
            com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.this.rightContainer.setPivotX(0.0f);
            com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.this.rightContainer.setPivotY((float) (com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.this.recyclerView.getMeasuredHeight() / 2));
            com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.this.rightContainer.setScaleX(0.5f);
            com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.this.rightContainer.setScaleY(0.5f);
        }

        public void onAnimationEnd(android.animation.Animator animation) {
            com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.this.selectedImage.setAlpha(1.0f);
            com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.this.selectedText.setAlpha(1.0f);
            com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.this.animImageView.setVisibility(4);
            com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.this.animTitle.setVisibility(4);
            com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.this.animSubTitle.setVisibility(4);
            if (this.val$endAction != null) {
                this.val$endAction.run();
            }
        }
    }

    class Anon2 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
        Anon2() {
        }

        public void onAnimationEnd(android.animation.Animator animation) {
            com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.this.handler.removeCallbacks(com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.this.fastScrollTimeout);
            com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.this.handler.postDelayed(com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.this.fastScrollTimeout, 800);
        }
    }

    class Anon3 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
        Anon3() {
        }

        public void onAnimationEnd(android.animation.Animator animation) {
            com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.this.stoppingFastScrolling = false;
            com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.this.fastScrollContainer.setVisibility(8);
            com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.this.startFadeOut();
        }
    }

    class Anon4 implements java.lang.Runnable {
        Anon4() {
        }

        public void run() {
            com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.this.stopFastScrolling();
        }
    }

    class Anon5 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
        Anon5() {
        }

        public void onAnimationEnd(android.animation.Animator animation) {
            com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.sLogger.v("closeMenuHideListener");
            com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.this.closeMenuAnimationActive = false;
            com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.this.closeContainer.setVisibility(8);
            com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.this.closeContainerScrim.setVisibility(8);
        }
    }

    class Anon6 implements com.navdy.hud.app.ui.component.vlist.VerticalList.Callback {
        Anon6() {
        }

        public void select(com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState selection) {
            com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.this.callback.select(selection);
        }

        public void onLoad() {
            com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.this.listLoaded = true;
            com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.this.callback.onLoad();
        }

        public void onBindToView(com.navdy.hud.app.ui.component.vlist.VerticalList.Model model, android.view.View view, int pos, com.navdy.hud.app.ui.component.vlist.VerticalList.ModelState state) {
            com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.this.callback.onBindToView(model, view, pos, state);
        }

        public void onItemSelected(com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState selection) {
            com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.this.callback.onItemSelected(selection);
        }

        public void onScrollIdle() {
            com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.this.callback.onScrollIdle();
        }
    }

    class Anon7 implements com.navdy.hud.app.ui.component.vlist.VerticalList.ContainerCallback {
        Anon7() {
        }

        public boolean isCloseMenuVisible() {
            return com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.this.isCloseMenuVisible();
        }

        public boolean isFastScrolling() {
            return com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.this.isFastScrollVisible();
        }

        public void showToolTips() {
            if (com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.this.callback != null) {
                com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.this.callback.showToolTip();
            }
        }

        public void hideToolTips() {
            com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.this.hideToolTip();
        }
    }

    class Anon8 implements java.lang.Runnable {
        Anon8() {
        }

        public void run() {
            com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.sLogger.v("fadeOutRunnable");
            com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.this.fadeoutIndicator(500);
        }
    }

    class Anon9 implements java.lang.Runnable {
        Anon9() {
        }

        public void run() {
            com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.this.callback.close();
        }
    }

    public interface Callback {
        void close();

        boolean isClosed();

        boolean isItemClickable(com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState itemSelectionState);

        void onBindToView(com.navdy.hud.app.ui.component.vlist.VerticalList.Model model, android.view.View view, int i, com.navdy.hud.app.ui.component.vlist.VerticalList.ModelState modelState);

        void onFastScrollEnd();

        void onFastScrollStart();

        void onItemSelected(com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState itemSelectionState);

        void onLoad();

        void onScrollIdle();

        void select(com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState itemSelectionState);

        void showToolTip();
    }

    static {
        android.content.res.Resources resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        closeContainerHeight = resources.getDimensionPixelSize(com.navdy.hud.app.R.dimen.vmenu_close_height);
        closeContainerScrollY = resources.getDimensionPixelSize(com.navdy.hud.app.R.dimen.vmenu_close_scroll_y);
        indicatorY = resources.getDimensionPixelSize(com.navdy.hud.app.R.dimen.vmenu_indicator_y);
        animateTranslateY = resources.getDimensionPixelSize(com.navdy.hud.app.R.dimen.vmenu_anim_translate_y);
        closeHaloColor = resources.getColor(com.navdy.hud.app.R.color.close_halo);
    }

    public VerticalMenuComponent(android.view.ViewGroup root, com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.Callback callback2, boolean allowTwoLineTitles) {
        if (root == null || callback2 == null) {
            throw new java.lang.IllegalArgumentException();
        }
        this.fastScrollCloseWaitPeriod = com.navdy.hud.app.ui.component.UISettings.isVerticalListNoCloseTimeout() ? 0 : 150;
        this.callback = callback2;
        butterknife.ButterKnife.inject((java.lang.Object) this, (android.view.View) root);
        this.parentContainer = root;
        this.closeContainer.setY((float) (-closeContainerHeight));
        this.indicator.setY((float) indicatorY);
        fadeoutIndicator(-1);
        this.verticalList = new com.navdy.hud.app.ui.component.vlist.VerticalList(this.recyclerView, this.indicator, this.vlistCallback, this.containerCallback, allowTwoLineTitles);
        this.closeHalo.setStrokeColor(closeHaloColor);
        this.toolTip.setParams(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.mainIconSize, com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.iconMargin, 4);
        android.content.Context context = root.getContext();
        this.animImageView = new android.widget.ImageView(context);
        this.animImageView.setVisibility(4);
        this.parentContainer.addView(this.animImageView, new android.view.ViewGroup.MarginLayoutParams(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.selectedIconSize, com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.selectedIconSize));
        this.animTitle = new android.widget.TextView(context);
        this.animTitle.setTextAppearance(context, com.navdy.hud.app.R.style.vlist_title);
        this.animTitle.setVisibility(4);
        this.parentContainer.addView(this.animTitle, new android.view.ViewGroup.MarginLayoutParams(-2, -2));
        this.animSubTitle = new android.widget.TextView(context);
        this.animSubTitle.setTextAppearance(context, com.navdy.hud.app.R.style.vlist_subtitle);
        this.animSubTitle.setVisibility(4);
        this.parentContainer.addView(this.animSubTitle, new android.view.ViewGroup.MarginLayoutParams(-2, -2));
        this.animSubTitle2 = new android.widget.TextView(context);
        this.animSubTitle2.setTextAppearance(context, com.navdy.hud.app.R.style.vlist_subtitle);
        this.animSubTitle2.setVisibility(4);
        this.parentContainer.addView(this.animSubTitle2, new android.view.ViewGroup.MarginLayoutParams(-2, -2));
    }

    public void setLeftContainerWidth(int width) {
        sLogger.v("leftContainer width=" + width);
        ((android.view.ViewGroup.MarginLayoutParams) this.leftContainer.getLayoutParams()).width = width;
    }

    public void setRightContainerWidth(int width) {
        sLogger.v("rightContainer width=" + width);
        ((android.view.ViewGroup.MarginLayoutParams) this.rightContainer.getLayoutParams()).width = width;
    }

    public void updateView(java.util.List<com.navdy.hud.app.ui.component.vlist.VerticalList.Model> list, int initialSelection, boolean firstEntryBlank) {
        updateView(list, initialSelection, firstEntryBlank, null);
    }

    public void updateView(java.util.List<com.navdy.hud.app.ui.component.vlist.VerticalList.Model> list, int initialSelection, boolean firstEntryBlank, com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex scrollIndex) {
        updateView(list, initialSelection, firstEntryBlank, false, scrollIndex);
    }

    public void updateView(java.util.List<com.navdy.hud.app.ui.component.vlist.VerticalList.Model> list, int initialSelection, boolean firstEntryBlank, boolean hasScrollModel, com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex scrollIndex) {
        this.listLoaded = false;
        this.fastScrollIndex = null;
        if (scrollIndex != null) {
            int n = list.size();
            if (n < 40 || scrollIndex.getEntryCount() < 2) {
                sLogger.i("fast scroll threshold not met:" + n + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA + scrollIndex.getEntryCount());
            } else {
                this.fastScrollIndex = scrollIndex;
                sLogger.i("fast scroll available:" + n);
            }
        }
        if (!hasScrollModel) {
            this.verticalList.updateView(list, initialSelection, firstEntryBlank);
        } else {
            this.verticalList.updateViewWithScrollableContent(list, initialSelection, firstEntryBlank);
        }
    }

    private void showCloseMenu() {
        if (!isCloseMenuVisible()) {
            sLogger.v("showCloseMenu");
            android.animation.AnimatorSet set = new android.animation.AnimatorSet();
            set.setDuration(150);
            set.setInterpolator(this.linearInterpolator);
            set.addListener(this.closeMenuShowListener);
            set.playTogether(new android.animation.Animator[]{android.animation.ObjectAnimator.ofFloat(this.closeContainer, android.view.View.Y, new float[]{0.0f}), android.animation.ObjectAnimator.ofFloat(this.closeContainerScrim, android.view.View.ALPHA, new float[]{1.0f}), android.animation.ObjectAnimator.ofFloat(this.leftContainer, android.view.View.Y, new float[]{(float) closeContainerScrollY}), android.animation.ObjectAnimator.ofFloat(this.rightContainer, android.view.View.Y, new float[]{(float) closeContainerScrollY}), android.animation.ObjectAnimator.ofFloat(this.indicator, android.view.View.Y, new float[]{(float) (indicatorY + closeContainerScrollY)})});
            this.closeMenuAnimationActive = true;
            set.start();
            this.verticalList.animate(this.verticalList.getRawPosition(), false, 150, true, true);
            fadeoutIndicator(150);
        }
    }

    private void hideCloseMenu() {
        if (isCloseMenuVisible()) {
            sLogger.v("hideCloseMenu");
            android.animation.AnimatorSet set = new android.animation.AnimatorSet();
            set.setDuration(150);
            set.setInterpolator(this.linearInterpolator);
            set.addListener(this.closeMenuHideListener);
            set.playTogether(new android.animation.Animator[]{android.animation.ObjectAnimator.ofFloat(this.closeContainer, android.view.View.Y, new float[]{(float) (-closeContainerHeight)}), android.animation.ObjectAnimator.ofFloat(this.closeContainerScrim, android.view.View.ALPHA, new float[]{0.0f}), android.animation.ObjectAnimator.ofFloat(this.leftContainer, android.view.View.Y, new float[]{0.0f}), android.animation.ObjectAnimator.ofFloat(this.rightContainer, android.view.View.Y, new float[]{0.0f}), android.animation.ObjectAnimator.ofFloat(this.indicator, android.view.View.Y, new float[]{(float) indicatorY})});
            this.closeMenuAnimationActive = true;
            set.start();
            this.verticalList.animate(this.verticalList.getRawPosition(), true, 150, false, true);
        }
    }

    public boolean isCloseMenuVisible() {
        return this.closeContainer.getVisibility() == 0;
    }

    public void animateIn(android.animation.Animator.AnimatorListener listener) {
        sLogger.v("animateIn");
        android.animation.AnimatorSet set = new android.animation.AnimatorSet();
        set.setDuration(150);
        set.setInterpolator(this.decelerateInterpolator);
        if (listener != null) {
            set.addListener(listener);
        }
        android.animation.PropertyValuesHolder p1 = android.animation.PropertyValuesHolder.ofFloat(android.view.View.TRANSLATION_Y, new float[]{(float) animateTranslateY, 0.0f});
        android.animation.PropertyValuesHolder p2 = android.animation.PropertyValuesHolder.ofFloat(android.view.View.ALPHA, new float[]{1.0f});
        android.animation.PropertyValuesHolder p3 = android.animation.PropertyValuesHolder.ofFloat(android.view.View.TRANSLATION_Y, new float[]{(float) animateTranslateY, 0.0f});
        android.animation.PropertyValuesHolder p4 = android.animation.PropertyValuesHolder.ofFloat(android.view.View.ALPHA, new float[]{1.0f});
        set.playTogether(new android.animation.Animator[]{android.animation.ObjectAnimator.ofPropertyValuesHolder(this.leftContainer, new android.animation.PropertyValuesHolder[]{p1, p2}), android.animation.ObjectAnimator.ofPropertyValuesHolder(this.rightContainer, new android.animation.PropertyValuesHolder[]{p3, p4})});
        set.start();
    }

    public void animateOut(android.animation.Animator.AnimatorListener listener) {
        sLogger.v("animateOut");
        this.indicator.setVisibility(4);
        android.animation.AnimatorSet set = new android.animation.AnimatorSet();
        set.setDuration(150);
        set.setInterpolator(this.accelerateInterpolator);
        if (listener != null) {
            set.addListener(listener);
        }
        android.animation.PropertyValuesHolder p1 = android.animation.PropertyValuesHolder.ofFloat(android.view.View.Y, new float[]{this.leftContainer.getY() + ((float) animateTranslateY)});
        android.animation.PropertyValuesHolder p2 = android.animation.PropertyValuesHolder.ofFloat(android.view.View.ALPHA, new float[]{0.0f});
        android.animation.PropertyValuesHolder p3 = android.animation.PropertyValuesHolder.ofFloat(android.view.View.Y, new float[]{this.rightContainer.getY() + ((float) animateTranslateY)});
        android.animation.PropertyValuesHolder p4 = android.animation.PropertyValuesHolder.ofFloat(android.view.View.ALPHA, new float[]{0.0f});
        android.animation.PropertyValuesHolder p5 = android.animation.PropertyValuesHolder.ofFloat(android.view.View.TRANSLATION_Y, new float[]{(float) animateTranslateY});
        android.animation.PropertyValuesHolder p6 = android.animation.PropertyValuesHolder.ofFloat(android.view.View.ALPHA, new float[]{0.0f});
        set.playTogether(new android.animation.Animator[]{android.animation.ObjectAnimator.ofPropertyValuesHolder(this.leftContainer, new android.animation.PropertyValuesHolder[]{p1, p2}), android.animation.ObjectAnimator.ofPropertyValuesHolder(this.rightContainer, new android.animation.PropertyValuesHolder[]{p3, p4}), android.animation.ObjectAnimator.ofPropertyValuesHolder(this.closeContainer, new android.animation.PropertyValuesHolder[]{p5, p6})});
        set.start();
    }

    public boolean handleGesture(com.navdy.service.library.events.input.GestureEvent event) {
        if (!this.listLoaded) {
            sLogger.v("list not loaded yet");
            return false;
        } else if (this.closeMenuAnimationActive) {
            sLogger.v("close menu animation is active, no-op");
            return false;
        } else {
            switch (event.gesture) {
                case GESTURE_SWIPE_RIGHT:
                    this.callback.close();
                    return true;
                default:
                    return false;
            }
        }
    }

    public boolean handleKey(com.navdy.hud.app.manager.InputManager.CustomKeyEvent event) {
        if (!this.listLoaded) {
            sLogger.v("list not loaded yet");
            return false;
        } else if (this.closeMenuAnimationActive) {
            sLogger.v("close menu animation is active, no-op");
            return false;
        } else {
            long now = android.os.SystemClock.elapsedRealtime();
            switch (event) {
                case LEFT:
                    if (this.verticalList.getCurrentPosition() == 0) {
                        if (this.verticalList.up().keyHandled) {
                            this.lastUpEvent = now;
                            return true;
                        } else if (android.os.SystemClock.elapsedRealtime() - this.lastUpEvent < ((long) this.fastScrollCloseWaitPeriod)) {
                            sLogger.v("user scrolled fast");
                            this.lastUpEvent = now;
                            return true;
                        } else {
                            this.lastUpEvent = now;
                            showCloseMenu();
                        }
                    } else if (isFastScrollAvailable() && checkFastScroll(com.navdy.hud.app.ui.component.vlist.VerticalList.Direction.UP, -1, now)) {
                        return true;
                    } else {
                        this.lastUpEvent = now;
                        if (this.verticalList.up().listMoved) {
                            fadeinIndicator(500);
                            startFadeOut();
                        }
                    }
                    return true;
                case RIGHT:
                    if (isCloseMenuVisible()) {
                        hideCloseMenu();
                    } else if (isFastScrollAvailable() && checkFastScroll(com.navdy.hud.app.ui.component.vlist.VerticalList.Direction.DOWN, 1, now)) {
                        return true;
                    } else {
                        hideToolTip();
                        if (this.verticalList.down().listMoved) {
                            fadeinIndicator(500);
                            startFadeOut();
                        }
                    }
                    return true;
                case SELECT:
                    if (!isCloseMenuVisible() || this.callback.isClosed()) {
                        int pos = this.verticalList.getCurrentPosition();
                        com.navdy.hud.app.ui.component.vlist.VerticalList.Model current = this.verticalList.getCurrentModel();
                        if (current == null) {
                            return false;
                        }
                        switch (current.id) {
                            case com.navdy.hud.app.R.id.vlist_content_loading /*2131624081*/:
                            case com.navdy.hud.app.R.id.vlist_loading /*2131624083*/:
                                return false;
                            default:
                                if (!isFastScrollAvailable() || !isFastScrollVisible()) {
                                    com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState itemSelectionState = this.verticalList.getItemSelectionState();
                                    itemSelectionState.set(current, current.id, pos, -1, -1);
                                    if (this.callback.isItemClickable(itemSelectionState)) {
                                        this.verticalList.select();
                                        break;
                                    } else {
                                        return false;
                                    }
                                } else {
                                    selectFastScroll();
                                    return true;
                                }
                        }
                    } else {
                        this.closeHalo.setVisibility(4);
                        com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils.performClick(this.closeIconContainer, 50, this.closeIconEndAction);
                    }
                    return true;
                default:
                    if (this.overrideDefaultKeyEvents.contains(event)) {
                        return true;
                    }
                    return false;
            }
        }
    }

    public void fadeinIndicator(int duration) {
        sLogger.v("fadeinIndicator:" + duration);
        if (duration == -1) {
            this.indicator.setAlpha(1.0f);
        } else if (this.indicator.getAlpha() != 1.0f) {
            this.indicator.animate().alpha(1.0f).setDuration((long) duration).start();
        }
    }

    public void fadeoutIndicator(int duration) {
        if (duration == -1) {
            this.indicator.setAlpha(0.0f);
        } else if (this.indicator.getAlpha() != 0.0f) {
            this.indicator.animate().alpha(0.0f).setDuration((long) duration).start();
        }
    }

    /* access modifiers changed from: private */
    public void startFadeOut() {
        this.handler.removeCallbacks(this.fadeOutRunnable);
        this.handler.postDelayed(this.fadeOutRunnable, 2000);
    }

    public void setSelectedIconColorImage(int resourceId, int bkColor, android.graphics.Shader gradient, float scaleF) {
        setSelectedIconColorImage(resourceId, bkColor, gradient, scaleF, com.navdy.hud.app.ui.component.image.IconColorImageView.IconShape.CIRCLE);
    }

    public void setSelectedIconColorImage(int resourceId, int bkColor, android.graphics.Shader gradient, float scaleF, com.navdy.hud.app.ui.component.image.IconColorImageView.IconShape iconShape) {
        this.selectedIconColorImage.setIcon(resourceId, bkColor, gradient, scaleF);
        this.selectedIconColorImage.setVisibility(0);
        this.selectedIconColorImage.setIconShape(iconShape);
        this.selectedIconImage.setVisibility(8);
        this.selectedCustomView.setVisibility(8);
    }

    public void setSelectedIconImage(int resourceId, java.lang.String initials, com.navdy.hud.app.ui.component.image.InitialsImageView.Style style) {
        this.selectedIconImage.setImage(resourceId, initials, style);
        this.selectedIconImage.setVisibility(0);
        this.selectedIconColorImage.setVisibility(8);
        this.selectedCustomView.setVisibility(8);
    }

    public void setSelectedIconImage(android.graphics.Bitmap bitmap) {
        this.selectedIconImage.setInitials(null, com.navdy.hud.app.ui.component.image.InitialsImageView.Style.DEFAULT);
        this.selectedIconImage.setImageBitmap(bitmap);
        this.selectedIconImage.setVisibility(0);
        this.selectedIconColorImage.setVisibility(8);
        this.selectedCustomView.setVisibility(8);
    }

    public void showSelectedCustomView() {
        this.selectedCustomView.setVisibility(0);
        this.selectedIconImage.setVisibility(8);
        this.selectedIconColorImage.setVisibility(8);
    }

    public android.view.ViewGroup getSelectedCustomImage() {
        return this.selectedCustomView;
    }

    public void performSelectionAnimation(java.lang.Runnable endAction) {
        performSelectionAnimation(endAction, 0);
    }

    public void performSelectionAnimation(java.lang.Runnable endAction, int startDelay) {
        sLogger.v("performSelectionAnimation");
        android.animation.AnimatorSet set = new android.animation.AnimatorSet();
        android.animation.AnimatorSet.Builder builder = set.play(android.animation.ObjectAnimator.ofFloat(this.leftContainer, android.view.View.ALPHA, new float[]{0.0f}));
        builder.with(android.animation.ObjectAnimator.ofFloat(this.indicator, android.view.View.ALPHA, new float[]{0.0f}));
        this.verticalList.addCurrentHighlightAnimation(builder);
        set.setDuration(50);
        set.addListener(new com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.Anon10(endAction, startDelay));
        set.start();
    }

    public void performBackAnimation(java.lang.Runnable startAction, java.lang.Runnable endAction, com.navdy.hud.app.ui.component.vlist.VerticalList.Model model, int xOffset) {
        android.widget.ImageView from;
        sLogger.v("performBackAnimation");
        float adjustment = (((float) com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.selectedIconSize) * 0.20000005f) / 2.0f;
        this.animImageView.setScaleX(SELECTED_TO_MAIN_ICON_SCALE);
        this.animImageView.setScaleY(SELECTED_TO_MAIN_ICON_SCALE);
        this.animImageView.setX(this.selectedImage.getX() + adjustment);
        this.animImageView.setY(this.selectedImage.getY() + adjustment);
        if (this.selectedIconColorImage.getVisibility() == 0) {
            from = this.selectedIconColorImage;
        } else {
            from = this.selectedIconImage;
        }
        android.graphics.Bitmap cacheBitmap = getFromCache(model);
        if (cacheBitmap == null) {
            com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils.copyImage(from, this.animImageView);
            addToCache(this.animImageView, model);
        } else {
            this.animImageView.setImageBitmap(cacheBitmap);
        }
        this.animImageView.setVisibility(0);
        this.selectedImage.setAlpha(0.0f);
        android.animation.PropertyValuesHolder p1 = android.animation.PropertyValuesHolder.ofFloat(android.view.View.SCALE_X, new float[]{1.0f});
        android.animation.PropertyValuesHolder p2 = android.animation.PropertyValuesHolder.ofFloat(android.view.View.SCALE_Y, new float[]{1.0f});
        android.animation.PropertyValuesHolder p3 = android.animation.PropertyValuesHolder.ofFloat(android.view.View.X, new float[]{(float) (com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.selectedImageX + xOffset)});
        android.animation.PropertyValuesHolder p4 = android.animation.PropertyValuesHolder.ofFloat(android.view.View.Y, new float[]{(float) com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.selectedImageY});
        android.animation.ObjectAnimator animatorImage = android.animation.ObjectAnimator.ofPropertyValuesHolder(this.animImageView, new android.animation.PropertyValuesHolder[]{p1, p2, p3, p4});
        animatorImage.setDuration((long) 150);
        com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.Anon11 anon11 = new com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.Anon11(endAction);
        animatorImage.addListener(anon11);
        animatorImage.start();
        this.animTitle.setX((float) (com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.selectedTextX - animateTranslateY));
        if (model.fontInfo == null) {
            com.navdy.hud.app.ui.component.vlist.VerticalList.setFontSize(model, this.verticalList.allowsTwoLineTitles());
        }
        this.animTitle.setTextSize(model.fontInfo.titleFontSize);
        this.animTitle.setAlpha(0.0f);
        this.animTitle.setVisibility(0);
        boolean hasSubtitle = false;
        if (model == null || model.subTitle == null) {
            this.animSubTitle.setText("");
        } else {
            this.animSubTitle.setX((float) (com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.selectedTextX - animateTranslateY));
            this.animSubTitle.setText(model.subTitle);
            this.animSubTitle.setTextSize(model.fontInfo.subTitleFontSize);
            this.animSubTitle.setAlpha(0.0f);
            this.animSubTitle.setVisibility(0);
            hasSubtitle = true;
        }
        boolean hasSubtitle2 = false;
        if (model == null || model.subTitle2 == null) {
            this.animSubTitle2.setText("");
        } else {
            this.animSubTitle2.setX((float) (com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.selectedTextX - animateTranslateY));
            this.animSubTitle2.setText(model.subTitle2);
            this.animSubTitle2.setTextSize(model.fontInfo.subTitle2FontSize);
            this.animSubTitle2.setAlpha(0.0f);
            this.animSubTitle2.setVisibility(0);
            hasSubtitle2 = true;
        }
        android.animation.AnimatorSet textSet = new android.animation.AnimatorSet();
        android.animation.PropertyValuesHolder p12 = android.animation.PropertyValuesHolder.ofFloat(android.view.View.ALPHA, new float[]{1.0f});
        android.animation.PropertyValuesHolder p22 = android.animation.PropertyValuesHolder.ofFloat(android.view.View.X, new float[]{this.animTitle.getX() + ((float) animateTranslateY)});
        android.animation.AnimatorSet.Builder textBuilder = textSet.play(android.animation.ObjectAnimator.ofPropertyValuesHolder(this.animTitle, new android.animation.PropertyValuesHolder[]{p12, p22}));
        if (hasSubtitle) {
            android.animation.PropertyValuesHolder p13 = android.animation.PropertyValuesHolder.ofFloat(android.view.View.ALPHA, new float[]{1.0f});
            android.animation.PropertyValuesHolder p23 = android.animation.PropertyValuesHolder.ofFloat(android.view.View.X, new float[]{this.animSubTitle.getX() + ((float) animateTranslateY)});
            textBuilder.with(android.animation.ObjectAnimator.ofPropertyValuesHolder(this.animSubTitle, new android.animation.PropertyValuesHolder[]{p13, p23}));
        }
        if (hasSubtitle2) {
            android.animation.PropertyValuesHolder p14 = android.animation.PropertyValuesHolder.ofFloat(android.view.View.ALPHA, new float[]{1.0f});
            android.animation.PropertyValuesHolder p24 = android.animation.PropertyValuesHolder.ofFloat(android.view.View.X, new float[]{this.animSubTitle2.getX() + ((float) animateTranslateY)});
            textBuilder.with(android.animation.ObjectAnimator.ofPropertyValuesHolder(this.animSubTitle2, new android.animation.PropertyValuesHolder[]{p14, p24}));
        }
        textSet.setStartDelay((long) ((int) (0.6666667f * ((float) 150))));
        textSet.setDuration((long) 50);
        textSet.start();
        android.animation.ObjectAnimator mainTitle = android.animation.ObjectAnimator.ofFloat(this.selectedText, android.view.View.ALPHA, new float[]{0.0f});
        mainTitle.setDuration((long) 50);
        mainTitle.start();
        this.rightContainer.setPivotX(0.0f);
        this.rightContainer.setPivotY((float) (this.recyclerView.getMeasuredHeight() / 2));
        android.animation.AnimatorSet rightAnimator = new android.animation.AnimatorSet();
        android.animation.PropertyValuesHolder p15 = android.animation.PropertyValuesHolder.ofFloat(android.view.View.SCALE_X, new float[]{0.5f});
        android.animation.PropertyValuesHolder p25 = android.animation.PropertyValuesHolder.ofFloat(android.view.View.SCALE_Y, new float[]{0.5f});
        android.animation.PropertyValuesHolder p32 = android.animation.PropertyValuesHolder.ofFloat(android.view.View.ALPHA, new float[]{0.0f});
        rightAnimator.setDuration((long) ((int) (0.6666667f * ((float) 150))));
        rightAnimator.play(android.animation.ObjectAnimator.ofPropertyValuesHolder(this.rightContainer, new android.animation.PropertyValuesHolder[]{p15, p25, p32}));
        com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.Anon12 anon12 = new com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.Anon12(startAction);
        rightAnimator.addListener(anon12);
        rightAnimator.start();
    }

    public void performEnterAnimation(java.lang.Runnable startAction, java.lang.Runnable endAction, com.navdy.hud.app.ui.component.vlist.VerticalList.Model model) {
        sLogger.v("performEnterAnimation");
        this.selectedImage.getLocationOnScreen(location);
        this.selectedImage.setAlpha(0.0f);
        this.selectedText.setAlpha(0.0f);
        this.indicator.setAlpha(0.0f);
        this.animImageView.setScaleX(1.0f);
        this.animImageView.setScaleY(1.0f);
        this.animImageView.setImageBitmap(null);
        android.graphics.Bitmap cacheBitmap = null;
        if (model.type != com.navdy.hud.app.ui.component.vlist.VerticalList.ModelType.ICON_OPTIONS) {
            cacheBitmap = getFromCache(model);
        }
        this.verticalList.copyAndPosition(this.animImageView, this.animTitle, this.animSubTitle, this.animSubTitle2, cacheBitmap == null);
        if (cacheBitmap != null) {
            this.animImageView.setImageBitmap(cacheBitmap);
        } else {
            addToCache(this.animImageView, model);
        }
        this.animImageView.setVisibility(0);
        this.animTitle.setAlpha(1.0f);
        this.animTitle.setVisibility(0);
        if (this.animSubTitle.getText().length() > 0) {
            this.animSubTitle.setAlpha(1.0f);
            this.animSubTitle.setVisibility(0);
        }
        this.rightContainer.setAlpha(0.0f);
        float adjustment = (((float) com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.selectedIconSize) * -0.20000005f) / 2.0f;
        android.animation.ObjectAnimator animatorImage = android.animation.ObjectAnimator.ofPropertyValuesHolder(this.animImageView, new android.animation.PropertyValuesHolder[]{android.animation.PropertyValuesHolder.ofFloat(android.view.View.SCALE_X, new float[]{1.2f}), android.animation.PropertyValuesHolder.ofFloat(android.view.View.SCALE_Y, new float[]{1.2f}), android.animation.PropertyValuesHolder.ofFloat(android.view.View.X, new float[]{((float) location[0]) - adjustment}), android.animation.PropertyValuesHolder.ofFloat(android.view.View.Y, new float[]{((float) (location[1] - com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.rootTopOffset)) - adjustment})});
        animatorImage.setDuration((long) 150);
        animatorImage.addListener(new com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.Anon13(startAction, endAction));
        animatorImage.start();
        this.animTitle.getLocationOnScreen(location);
        android.animation.AnimatorSet textSet = new android.animation.AnimatorSet();
        android.animation.AnimatorSet animatorSet = textSet;
        android.animation.AnimatorSet.Builder textBuilder = animatorSet.play(android.animation.ObjectAnimator.ofPropertyValuesHolder(this.animTitle, new android.animation.PropertyValuesHolder[]{android.animation.PropertyValuesHolder.ofFloat(android.view.View.ALPHA, new float[]{0.0f}), android.animation.PropertyValuesHolder.ofFloat(android.view.View.X, new float[]{this.animTitle.getX() - ((float) animateTranslateY)})}));
        if (this.animSubTitle.getText().length() > 0) {
            android.animation.AnimatorSet.Builder builder = textBuilder;
            builder.with(android.animation.ObjectAnimator.ofPropertyValuesHolder(this.animSubTitle, new android.animation.PropertyValuesHolder[]{android.animation.PropertyValuesHolder.ofFloat(android.view.View.ALPHA, new float[]{0.0f}), android.animation.PropertyValuesHolder.ofFloat(android.view.View.X, new float[]{this.animSubTitle.getX() - ((float) animateTranslateY)})}));
        }
        textSet.setDuration((long) 50);
        textSet.start();
        android.animation.ObjectAnimator mainTitle = android.animation.ObjectAnimator.ofFloat(this.selectedText, android.view.View.ALPHA, new float[]{1.0f});
        mainTitle.setDuration((long) 50);
        mainTitle.setStartDelay((long) ((int) (0.6666667f * ((float) 150))));
        mainTitle.start();
        android.animation.AnimatorSet rightAnimator = new android.animation.AnimatorSet();
        android.animation.PropertyValuesHolder p1 = android.animation.PropertyValuesHolder.ofFloat(android.view.View.SCALE_X, new float[]{1.0f});
        android.animation.PropertyValuesHolder p2 = android.animation.PropertyValuesHolder.ofFloat(android.view.View.SCALE_Y, new float[]{1.0f});
        android.animation.PropertyValuesHolder p3 = android.animation.PropertyValuesHolder.ofFloat(android.view.View.ALPHA, new float[]{1.0f});
        rightAnimator.setStartDelay((long) 50);
        rightAnimator.setDuration((long) ((int) (0.6666667f * ((float) 150))));
        rightAnimator.play(android.animation.ObjectAnimator.ofPropertyValuesHolder(this.rightContainer, new android.animation.PropertyValuesHolder[]{p1, p2, p3}));
        rightAnimator.start();
    }

    public void unlock() {
        this.verticalList.unlock();
    }

    public void unlock(boolean startFluctuator) {
        this.verticalList.unlock(startFluctuator);
    }

    private void addToCache(android.widget.ImageView imageView, com.navdy.hud.app.ui.component.vlist.VerticalList.Model model) {
        if (model != null) {
            android.graphics.drawable.BitmapDrawable drawable = (android.graphics.drawable.BitmapDrawable) imageView.getDrawable();
            if (drawable != null) {
                android.graphics.Bitmap bitmap = drawable.getBitmap();
                if (bitmap != null) {
                    this.sublistAnimationCache.put(java.lang.Integer.valueOf(java.lang.System.identityHashCode(model)), bitmap);
                }
            }
        }
    }

    private android.graphics.Bitmap getFromCache(com.navdy.hud.app.ui.component.vlist.VerticalList.Model model) {
        if (model == null) {
            return null;
        }
        return (android.graphics.Bitmap) this.sublistAnimationCache.get(java.lang.Integer.valueOf(java.lang.System.identityHashCode(model)));
    }

    public void clear() {
        this.sublistAnimationCache.clear();
        this.verticalList.unlock(false);
        this.verticalList.clearAllAnimations();
    }

    public void setOverrideDefaultKeyEvents(com.navdy.hud.app.manager.InputManager.CustomKeyEvent event, boolean override) {
        if (override) {
            this.overrideDefaultKeyEvents.add(event);
        } else {
            this.overrideDefaultKeyEvents.remove(event);
        }
    }

    private boolean isFastScrollAvailable() {
        return this.fastScrollIndex != null;
    }

    /* access modifiers changed from: private */
    public boolean isFastScrollVisible() {
        return this.fastScrollContainer.getVisibility() == 0;
    }

    private void startFastScrolling() {
        if (isFastScrollAvailable()) {
            this.verticalList.lock();
            java.lang.String title = this.fastScrollIndex.getTitle(this.fastScrollCurrentItem);
            sLogger.v("startFastScrolling:" + title);
            this.fastScrollContainer.setAlpha(0.0f);
            this.fastScrollContainer.setVisibility(0);
            this.fastScrollText.setText(title);
            this.stoppingFastScrolling = false;
            this.callback.onFastScrollStart();
            this.fastScrollContainer.animate().alpha(1.0f).setDuration(50).setListener(this.fastScrollIn).start();
        }
    }

    /* access modifiers changed from: private */
    public void stopFastScrolling() {
        if (isFastScrollAvailable()) {
            java.lang.String title = java.lang.String.valueOf(this.fastScrollText.getText());
            int pos = this.fastScrollIndex.getPosition(title);
            if (this.verticalList.isFirstEntryBlank()) {
                pos++;
            }
            if (this.fastScrollIndex != null) {
                pos += this.fastScrollIndex.getOffset();
            }
            sLogger.v("stopFastScrolling:" + pos + " , " + title);
            this.callback.onFastScrollEnd();
            this.stoppingFastScrolling = true;
            this.verticalList.unlock();
            this.verticalList.scrollToPosition(pos);
            this.fastScrollContainer.animate().alpha(0.0f).setDuration(50).setListener(this.fastScrollOut).start();
        }
    }

    private void changeScrollIndex(int n) {
        this.handler.removeCallbacks(this.fastScrollTimeout);
        int newIndex = this.fastScrollCurrentItem + n;
        if (newIndex < 0 || newIndex >= this.fastScrollIndex.length) {
            int pos = this.fastScrollIndex.getPosition(this.fastScrollIndex.getTitle(this.fastScrollCurrentItem));
            if (pos != this.indicator.getCurrentItem()) {
                this.indicator.setCurrentItem(pos);
            }
        } else {
            java.lang.String title = this.fastScrollIndex.getTitle(newIndex);
            this.fastScrollText.setText(title);
            this.fastScrollCurrentItem = newIndex;
            this.indicator.setCurrentItem(this.fastScrollIndex.getPosition(title));
        }
        this.handler.postDelayed(this.fastScrollTimeout, 800);
    }

    public com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex getFastScrollIndex() {
        return this.fastScrollIndex;
    }

    private boolean checkFastScroll(com.navdy.hud.app.ui.component.vlist.VerticalList.Direction direction, int changeIndex, long now) {
        if (this.stoppingFastScrolling) {
            return true;
        }
        if (isFastScrollVisible()) {
            changeScrollIndex(changeIndex);
            return true;
        }
        if (this.scrollDirection != direction) {
            this.scrollDirection = direction;
            this.clickCount = 0;
            this.clickTime = now;
        } else if (now - this.clickTime > ((long) FAST_SCROLL_TRIGGER_TIME)) {
            this.clickTime = now;
            this.clickCount = 1;
        } else {
            this.clickCount++;
            if (this.clickCount >= 10) {
                this.fastScrollCurrentItem = this.fastScrollIndex.getIndexForPosition(this.verticalList.getCurrentPosition());
                if (this.scrollDirection == com.navdy.hud.app.ui.component.vlist.VerticalList.Direction.DOWN && this.fastScrollCurrentItem == this.fastScrollIndex.getEntryCount() - 1) {
                    sLogger.v("checkFastScroll: not allowed,last element");
                    this.clickCount = 0;
                    this.clickTime = now;
                    return false;
                }
                startFastScrolling();
                fadeinIndicator(-1);
                this.handler.removeCallbacks(this.fadeOutRunnable);
                return true;
            }
        }
        return false;
    }

    private void selectFastScroll() {
        sLogger.v("selectFastScroll");
        this.handler.removeCallbacks(this.fastScrollTimeout);
        this.fastScrollTimeout.run();
    }

    public void showToolTip(int posIndex, java.lang.String text) {
        this.toolTip.show(posIndex, text);
    }

    public void hideToolTip() {
        this.toolTip.hide();
    }
}
