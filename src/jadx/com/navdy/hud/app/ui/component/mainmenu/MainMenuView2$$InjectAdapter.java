package com.navdy.hud.app.ui.component.mainmenu;

public final class MainMenuView2$$InjectAdapter extends dagger.internal.Binding<com.navdy.hud.app.ui.component.mainmenu.MainMenuView2> implements dagger.MembersInjector<com.navdy.hud.app.ui.component.mainmenu.MainMenuView2> {
    private dagger.internal.Binding<com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.Presenter> presenter;

    public MainMenuView2$$InjectAdapter() {
        super(null, "members/com.navdy.hud.app.ui.component.mainmenu.MainMenuView2", false, com.navdy.hud.app.ui.component.mainmenu.MainMenuView2.class);
    }

    public void attach(dagger.internal.Linker linker) {
        this.presenter = linker.requestBinding("com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter", com.navdy.hud.app.ui.component.mainmenu.MainMenuView2.class, getClass().getClassLoader());
    }

    public void getDependencies(java.util.Set<dagger.internal.Binding<?>> set, java.util.Set<dagger.internal.Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.presenter);
    }

    public void injectMembers(com.navdy.hud.app.ui.component.mainmenu.MainMenuView2 object) {
        object.presenter = (com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.Presenter) this.presenter.get();
    }
}
