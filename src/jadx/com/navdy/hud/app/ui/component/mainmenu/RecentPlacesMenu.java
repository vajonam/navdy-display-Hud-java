package com.navdy.hud.app.ui.component.mainmenu;

public class RecentPlacesMenu implements com.navdy.hud.app.ui.component.mainmenu.IMenu {
    private static final com.navdy.hud.app.ui.component.vlist.VerticalList.Model back;
    private static final java.lang.String places = resources.getString(com.navdy.hud.app.R.string.carousel_menu_recent_place);
    private static final int placesColor = resources.getColor(com.navdy.hud.app.R.color.mm_recent_places);
    private static final android.content.res.Resources resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.mainmenu.RecentPlacesMenu.class);
    private int backSelection;
    private com.squareup.otto.Bus bus;
    private java.util.List<com.navdy.hud.app.ui.component.vlist.VerticalList.Model> cachedList;
    private com.navdy.hud.app.ui.component.mainmenu.IMenu parent;
    private com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.Presenter presenter;
    private java.util.List<com.navdy.hud.app.ui.component.vlist.VerticalList.Model> returnToCacheList;
    private com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent vscrollComponent;

    static {
        java.lang.String title = resources.getString(com.navdy.hud.app.R.string.back);
        int fluctuatorColor = resources.getColor(com.navdy.hud.app.R.color.mm_back);
        back = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.menu_back, com.navdy.hud.app.R.drawable.icon_mm_back, fluctuatorColor, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, fluctuatorColor, title, null);
    }

    public RecentPlacesMenu(com.squareup.otto.Bus bus2, com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent vscrollComponent2, com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.Presenter presenter2, com.navdy.hud.app.ui.component.mainmenu.IMenu parent2) {
        this.bus = bus2;
        this.vscrollComponent = vscrollComponent2;
        this.presenter = presenter2;
        this.parent = parent2;
    }

    public java.util.List<com.navdy.hud.app.ui.component.vlist.VerticalList.Model> getItems() {
        if (this.cachedList != null) {
            return this.cachedList;
        }
        java.util.List<com.navdy.hud.app.ui.component.vlist.VerticalList.Model> list = new java.util.ArrayList<>();
        this.returnToCacheList = new java.util.ArrayList();
        list.add(back);
        java.util.List<com.navdy.hud.app.framework.destinations.Destination> recentDestinations = com.navdy.hud.app.framework.destinations.DestinationsManager.getInstance().getRecentDestinations();
        if (recentDestinations == null || recentDestinations.size() <= 0) {
            sLogger.v("no recent destinations");
        } else {
            sLogger.v("recent destinations:" + recentDestinations.size());
            int counter = 0;
            for (com.navdy.hud.app.framework.destinations.Destination destination : recentDestinations) {
                int counter2 = counter + 1;
                com.navdy.hud.app.ui.component.vlist.VerticalList.Model model = com.navdy.hud.app.ui.component.mainmenu.PlacesMenu.buildModel(destination, counter);
                model.state = destination;
                list.add(model);
                this.returnToCacheList.add(model);
                counter = counter2;
            }
        }
        this.cachedList = list;
        return list;
    }

    public int getInitialSelection() {
        if (this.cachedList == null || this.cachedList.size() <= 1) {
            return 0;
        }
        return 1;
    }

    public com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex getScrollIndex() {
        return null;
    }

    public void setBackSelectionPos(int n) {
        this.backSelection = n;
    }

    public void setBackSelectionId(int id) {
    }

    public void setSelectedIcon() {
        this.vscrollComponent.setSelectedIconColorImage(com.navdy.hud.app.R.drawable.icon_place_recent_2, placesColor, null, 1.0f);
        this.vscrollComponent.selectedText.setText(places);
    }

    public com.navdy.hud.app.ui.component.vlist.VerticalList.Model getModelfromPos(int pos) {
        if (this.cachedList == null || this.cachedList.size() <= pos) {
            return null;
        }
        return (com.navdy.hud.app.ui.component.vlist.VerticalList.Model) this.cachedList.get(pos);
    }

    public boolean isBindCallsEnabled() {
        return false;
    }

    public void onBindToView(com.navdy.hud.app.ui.component.vlist.VerticalList.Model model, android.view.View view, int pos, com.navdy.hud.app.ui.component.vlist.VerticalList.ModelState state) {
    }

    public com.navdy.hud.app.ui.component.mainmenu.IMenu getChildMenu(com.navdy.hud.app.ui.component.mainmenu.IMenu parent2, java.lang.String args, java.lang.String path) {
        return null;
    }

    public void onUnload(com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel level) {
        switch (level) {
            case CLOSE:
                if (this.returnToCacheList != null) {
                    sLogger.v("rpm:unload add to cache");
                    com.navdy.hud.app.ui.component.vlist.VerticalModelCache.addToCache(this.returnToCacheList);
                    this.returnToCacheList = null;
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void onItemSelected(com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState selection) {
    }

    public void onScrollIdle() {
    }

    public void onFastScrollStart() {
    }

    public void onFastScrollEnd() {
    }

    public void showToolTip() {
    }

    public boolean isFirstItemEmpty() {
        return true;
    }

    public boolean selectItem(com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState selection) {
        sLogger.v("select id:" + selection.id + " pos:" + selection.pos);
        switch (selection.id) {
            case com.navdy.hud.app.R.id.menu_back /*2131624007*/:
                sLogger.v("back");
                com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("back");
                this.presenter.loadMenu(this.parent, com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel.BACK_TO_PARENT, this.backSelection, 0);
                break;
            default:
                ((com.navdy.hud.app.ui.component.mainmenu.PlacesMenu) this.parent).launchDestination((com.navdy.hud.app.ui.component.vlist.VerticalList.Model) this.cachedList.get(selection.pos));
                break;
        }
        return true;
    }

    public com.navdy.hud.app.ui.component.mainmenu.IMenu.Menu getType() {
        return com.navdy.hud.app.ui.component.mainmenu.IMenu.Menu.RECENT_PLACES;
    }

    public boolean isItemClickable(int id, int pos) {
        return true;
    }
}
