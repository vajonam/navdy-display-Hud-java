package com.navdy.hud.app.ui.component;

public class HaloView extends android.view.View {
    /* access modifiers changed from: private */
    public int animationDelay;
    private int animationDuration;
    private com.navdy.hud.app.ui.framework.DefaultAnimationListener animationListener;
    private android.animation.AnimatorSet animatorSet;
    /* access modifiers changed from: private */
    public android.animation.AnimatorSet currentAnimatorSet;
    protected float currentStrokeWidth;
    protected float endRadius;
    protected float endStrokeWidth;
    private android.animation.AnimatorSet firstAnimatorSet;
    private boolean firstIterationDone;
    private android.animation.ValueAnimator firstRadiusAnimator;
    /* access modifiers changed from: private */
    public android.os.Handler handler;
    private android.view.animation.Interpolator interpolator;
    protected float middleRadius;
    protected float middleStrokeWidth;
    private android.graphics.Paint paint;
    private android.animation.ValueAnimator radiusAnimator;
    private android.animation.AnimatorSet reverseAnimatorSet;
    private android.animation.ValueAnimator reverseRadiusAnimator;
    protected float startRadius;
    /* access modifiers changed from: private */
    public java.lang.Runnable startRunnable;
    /* access modifiers changed from: private */
    public boolean started;
    private int strokeColor;
    private android.animation.ValueAnimator.AnimatorUpdateListener updateListener;

    class Anon1 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
        Anon1() {
        }

        public void onAnimationEnd(android.animation.Animator animation) {
            if (com.navdy.hud.app.ui.component.HaloView.this.started) {
                com.navdy.hud.app.ui.component.HaloView.this.handler.removeCallbacks(com.navdy.hud.app.ui.component.HaloView.this.startRunnable);
                com.navdy.hud.app.ui.component.HaloView.this.handler.postDelayed(com.navdy.hud.app.ui.component.HaloView.this.startRunnable, (long) com.navdy.hud.app.ui.component.HaloView.this.animationDelay);
            }
        }
    }

    class Anon2 implements java.lang.Runnable {
        Anon2() {
        }

        public void run() {
            com.navdy.hud.app.ui.component.HaloView.this.toggleAnimator();
            com.navdy.hud.app.ui.component.HaloView.this.currentAnimatorSet.start();
        }
    }

    class Anon3 implements android.animation.ValueAnimator.AnimatorUpdateListener {
        Anon3() {
        }

        public void onAnimationUpdate(android.animation.ValueAnimator animation) {
            com.navdy.hud.app.ui.component.HaloView.this.onAnimationUpdateInternal(animation);
        }
    }

    public HaloView(android.content.Context context) {
        this(context, null);
    }

    public HaloView(android.content.Context context, android.util.AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public HaloView(android.content.Context context, android.util.AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.interpolator = new android.view.animation.AccelerateDecelerateInterpolator();
        this.handler = new android.os.Handler(android.os.Looper.getMainLooper());
        this.animationListener = new com.navdy.hud.app.ui.component.HaloView.Anon1();
        this.startRunnable = new com.navdy.hud.app.ui.component.HaloView.Anon2();
        this.updateListener = new com.navdy.hud.app.ui.component.HaloView.Anon3();
        android.content.res.TypedArray a = context.obtainStyledAttributes(attrs, com.navdy.hud.app.R.styleable.HaloView, defStyleAttr, 0);
        if (a != null) {
            this.strokeColor = a.getColor(0, -1);
            this.startRadius = a.getDimension(1, 0.0f);
            this.endRadius = a.getDimension(2, 0.0f);
            this.middleRadius = a.getDimension(3, 0.0f);
            this.animationDuration = a.getInteger(4, 0);
            this.animationDelay = a.getInteger(5, 0);
            a.recycle();
        }
        this.endStrokeWidth = this.endRadius - this.startRadius;
        if (this.endStrokeWidth < 0.0f) {
            this.endStrokeWidth = 0.0f;
        }
        this.middleStrokeWidth = this.endRadius - this.middleRadius;
        if (this.middleStrokeWidth < 0.0f) {
            this.middleStrokeWidth = 0.0f;
        }
        this.paint = new android.graphics.Paint();
        this.paint.setStrokeWidth(0.0f);
        this.paint.setAntiAlias(true);
        this.firstRadiusAnimator = android.animation.ValueAnimator.ofFloat(new float[]{this.startRadius, this.endRadius});
        this.radiusAnimator = android.animation.ValueAnimator.ofFloat(new float[]{this.middleRadius, this.endRadius});
        this.reverseRadiusAnimator = android.animation.ValueAnimator.ofFloat(new float[]{this.endRadius, this.middleRadius});
        this.firstRadiusAnimator.addUpdateListener(this.updateListener);
        this.radiusAnimator.addUpdateListener(this.updateListener);
        this.reverseRadiusAnimator.addUpdateListener(this.updateListener);
        this.firstAnimatorSet = new android.animation.AnimatorSet();
        this.firstAnimatorSet.setDuration((long) this.animationDuration);
        this.firstAnimatorSet.setInterpolator(this.interpolator);
        this.animatorSet = new android.animation.AnimatorSet();
        this.animatorSet.setDuration((long) this.animationDuration);
        this.animatorSet.setInterpolator(this.interpolator);
        this.reverseAnimatorSet = new android.animation.AnimatorSet();
        this.reverseAnimatorSet.setDuration((long) this.animationDuration);
        this.reverseAnimatorSet.setInterpolator(this.interpolator);
        this.animatorSet.play(this.radiusAnimator);
        this.reverseAnimatorSet.play(this.reverseRadiusAnimator);
        this.currentStrokeWidth = this.endStrokeWidth;
        invalidate();
    }

    public void onAnimationUpdateInternal(android.animation.ValueAnimator animation) {
        this.currentStrokeWidth = ((java.lang.Float) animation.getAnimatedValue()).floatValue() - this.startRadius;
        invalidate();
    }

    public void onDraw(android.graphics.Canvas canvas) {
        super.onDraw(canvas);
        onDrawInternal(canvas);
    }

    public void onDrawInternal(android.graphics.Canvas canvas) {
        this.paint.setStyle(android.graphics.Paint.Style.FILL);
        this.paint.setStrokeWidth(0.0f);
        this.paint.setColor(this.strokeColor);
        canvas.drawCircle((float) (getWidth() / 2), (float) (getHeight() / 2), this.startRadius + this.currentStrokeWidth, this.paint);
    }

    public void start() {
        stop();
        this.started = true;
        this.firstAnimatorSet.addListener(this.animationListener);
        this.animatorSet.addListener(this.animationListener);
        this.reverseAnimatorSet.addListener(this.animationListener);
        if (!this.firstIterationDone) {
            this.firstIterationDone = true;
            this.currentAnimatorSet = this.animatorSet;
            this.firstAnimatorSet.start();
            return;
        }
        this.currentAnimatorSet = this.reverseAnimatorSet;
        this.reverseAnimatorSet.start();
    }

    public void stop() {
        this.started = false;
        this.handler.removeCallbacks(this.startRunnable);
        if (this.firstAnimatorSet.isRunning()) {
            this.firstAnimatorSet.removeAllListeners();
            this.firstAnimatorSet.cancel();
        }
        if (this.animatorSet.isRunning()) {
            this.animatorSet.removeAllListeners();
            this.animatorSet.cancel();
        }
        if (this.reverseAnimatorSet.isRunning()) {
            this.reverseAnimatorSet.removeAllListeners();
            this.reverseAnimatorSet.cancel();
        }
        this.currentStrokeWidth = this.endStrokeWidth;
        requestLayout();
    }

    /* access modifiers changed from: private */
    public void toggleAnimator() {
        if (this.currentAnimatorSet == this.animatorSet) {
            this.currentAnimatorSet = this.reverseAnimatorSet;
        } else {
            this.currentAnimatorSet = this.animatorSet;
        }
    }

    public void setStrokeColor(int color) {
        this.strokeColor = color;
        invalidate();
    }

    public void setStrokeWidth(int n) {
        this.currentStrokeWidth = (float) n;
    }

    public void setStartRadius(int n) {
        this.startRadius = (float) n;
    }

    public void setMiddleRadius(int n) {
        this.middleRadius = (float) n;
    }

    public void setEndRadius(int n) {
        this.endRadius = (float) n;
    }

    public void setStartDelay(int n) {
        this.animationDelay = n;
    }

    public void setDuration(int n) {
        this.animationDuration = n;
    }
}
