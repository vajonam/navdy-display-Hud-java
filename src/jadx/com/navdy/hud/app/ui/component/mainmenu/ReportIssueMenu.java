package com.navdy.hud.app.ui.component.mainmenu;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000~\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u000b\b\u0000\u0018\u0000 =2\u00020\u0001:\u0002=>B\u001f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0001\u00a2\u0006\u0002\u0010\u0007B)\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\b\u0010\u0006\u001a\u0004\u0018\u00010\u0001\u0012\b\b\u0002\u0010\b\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\"\u0010\u0011\u001a\u0004\u0018\u00010\u00012\u0006\u0010\u0006\u001a\u00020\u00012\u0006\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0013H\u0016J\b\u0010\u0015\u001a\u00020\fH\u0016J\u0010\u0010\u0016\u001a\n\u0012\u0004\u0012\u00020\u0010\u0018\u00010\u000fH\u0016J\u0012\u0010\u0017\u001a\u0004\u0018\u00010\u00102\u0006\u0010\u0018\u001a\u00020\fH\u0016J\n\u0010\u0019\u001a\u0004\u0018\u00010\u001aH\u0016J\b\u0010\u001b\u001a\u00020\u001cH\u0016J\b\u0010\u001d\u001a\u00020\u001eH\u0016J\b\u0010\u001f\u001a\u00020\u001eH\u0016J\u0018\u0010 \u001a\u00020\u001e2\u0006\u0010!\u001a\u00020\f2\u0006\u0010\u0018\u001a\u00020\fH\u0016J(\u0010\"\u001a\u00020#2\u0006\u0010$\u001a\u00020\u00102\u0006\u0010%\u001a\u00020&2\u0006\u0010\u0018\u001a\u00020\f2\u0006\u0010'\u001a\u00020(H\u0016J\b\u0010)\u001a\u00020#H\u0016J\b\u0010*\u001a\u00020#H\u0016J\u0010\u0010+\u001a\u00020#2\u0006\u0010,\u001a\u00020-H\u0016J\b\u0010.\u001a\u00020#H\u0016J\u0010\u0010/\u001a\u00020#2\u0006\u00100\u001a\u000201H\u0016J\u0010\u00102\u001a\u00020#2\u0006\u00103\u001a\u000204H\u0002J\u0010\u00105\u001a\u00020\u001e2\u0006\u0010,\u001a\u00020-H\u0016J\u0010\u00106\u001a\u00020#2\u0006\u0010!\u001a\u00020\fH\u0016J\u0010\u00107\u001a\u00020#2\u0006\u00108\u001a\u00020\fH\u0016J\b\u00109\u001a\u00020#H\u0016J\b\u0010:\u001a\u00020#H\u0002J\b\u0010;\u001a\u00020#H\u0016J\u0010\u0010<\u001a\u00020#2\u0006\u0010!\u001a\u00020\fH\u0002R\u000e\u0010\u000b\u001a\u00020\fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u000e\u001a\n\u0012\u0004\u0012\u00020\u0010\u0018\u00010\u000fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0006\u001a\u0004\u0018\u00010\u0001X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006?"}, d2 = {"Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;", "Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;", "vscrollComponent", "Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;", "presenter", "Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;", "parent", "(Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;)V", "type", "Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$ReportIssueMenuType;", "(Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$ReportIssueMenuType;)V", "backSelection", "", "backSelectionId", "currentItems", "", "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;", "getChildMenu", "args", "", "path", "getInitialSelection", "getItems", "getModelfromPos", "pos", "getScrollIndex", "Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;", "getType", "Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;", "isBindCallsEnabled", "", "isFirstItemEmpty", "isItemClickable", "id", "onBindToView", "", "model", "view", "Landroid/view/View;", "state", "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;", "onFastScrollEnd", "onFastScrollStart", "onItemSelected", "selection", "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;", "onScrollIdle", "onUnload", "level", "Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;", "reportIssue", "issueType", "Lcom/navdy/hud/app/util/ReportIssueService$IssueType;", "selectItem", "setBackSelectionId", "setBackSelectionPos", "n", "setSelectedIcon", "showSentToast", "showToolTip", "takeSnapshot", "Companion", "ReportIssueMenuType", "app_hudDebug"}, k = 1, mv = {1, 1, 6})
/* compiled from: ReportIssueMenu.kt */
public final class ReportIssueMenu implements com.navdy.hud.app.ui.component.mainmenu.IMenu {
    public static final com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu.Companion Companion = new com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu.Companion(null);
    /* access modifiers changed from: private */
    public static final java.lang.String NAV_ISSUE_SENT_TOAST_ID = NAV_ISSUE_SENT_TOAST_ID;
    /* access modifiers changed from: private */
    public static final int TOAST_TIMEOUT = 1000;
    /* access modifiers changed from: private */
    public static final com.navdy.hud.app.ui.component.vlist.VerticalList.Model back;
    /* access modifiers changed from: private */
    public static final com.navdy.hud.app.ui.component.vlist.VerticalList.Model driveScore;
    /* access modifiers changed from: private */
    public static final com.navdy.hud.app.ui.component.vlist.VerticalList.Model etaInaccurate;
    /* access modifiers changed from: private */
    @org.jetbrains.annotations.NotNull
    public static final java.util.HashMap<java.lang.Integer, java.lang.String> idToTitleMap = kotlin.collections.MapsKt.hashMapOf(kotlin.TuplesKt.to(java.lang.Integer.valueOf(com.navdy.hud.app.R.id.take_snapshot_maps), "Maps"), kotlin.TuplesKt.to(java.lang.Integer.valueOf(com.navdy.hud.app.R.id.take_snapshot_navigation), "Navigation"), kotlin.TuplesKt.to(java.lang.Integer.valueOf(com.navdy.hud.app.R.id.take_snapshot_smart_dash), "Smart_Dash"), kotlin.TuplesKt.to(java.lang.Integer.valueOf(com.navdy.hud.app.R.id.take_snapshot_drive_score), "Drive_Score"));
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu.class);
    /* access modifiers changed from: private */
    public static final com.navdy.hud.app.ui.component.vlist.VerticalList.Model maps;
    /* access modifiers changed from: private */
    public static final com.navdy.hud.app.ui.component.vlist.VerticalList.Model navigation;
    /* access modifiers changed from: private */
    public static final com.navdy.hud.app.ui.component.vlist.VerticalList.Model notFastestRoute;
    /* access modifiers changed from: private */
    public static final com.navdy.hud.app.ui.component.vlist.VerticalList.Model permanentClosure;
    /* access modifiers changed from: private */
    public static final java.lang.String reportIssue;
    /* access modifiers changed from: private */
    public static final int reportIssueColor;
    /* access modifiers changed from: private */
    public static final java.util.ArrayList<com.navdy.hud.app.ui.component.vlist.VerticalList.Model> reportIssueItems = new java.util.ArrayList<>();
    /* access modifiers changed from: private */
    public static final com.navdy.hud.app.ui.component.vlist.VerticalList.Model roadBlocked;
    /* access modifiers changed from: private */
    public static final com.navdy.hud.app.ui.component.vlist.VerticalList.Model smartDash;
    /* access modifiers changed from: private */
    public static final java.util.ArrayList<com.navdy.hud.app.ui.component.vlist.VerticalList.Model> takeSnapShotItems = new java.util.ArrayList<>();
    /* access modifiers changed from: private */
    public static final java.lang.String takeSnapshot;
    /* access modifiers changed from: private */
    public static final int takeSnapshotColor;
    /* access modifiers changed from: private */
    public static final java.lang.String toastSentSuccessfully;
    /* access modifiers changed from: private */
    public static final com.navdy.hud.app.ui.component.vlist.VerticalList.Model wrongDirection;
    /* access modifiers changed from: private */
    public static final com.navdy.hud.app.ui.component.vlist.VerticalList.Model wrongRoadName;
    private int backSelection;
    private int backSelectionId;
    private java.util.List<? extends com.navdy.hud.app.ui.component.vlist.VerticalList.Model> currentItems;
    private final com.navdy.hud.app.ui.component.mainmenu.IMenu parent;
    /* access modifiers changed from: private */
    public final com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.Presenter presenter;
    private final com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu.ReportIssueMenuType type;
    private final com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent vscrollComponent;

    @kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u000f\n\u0002\u0018\u0002\n\u0002\b\u0013\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\u00020\u0004X\u0082D\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u0014\u0010\u0007\u001a\u00020\bX\u0082D\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0014\u0010\u000b\u001a\u00020\fX\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0014\u0010\u000f\u001a\u00020\fX\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u000eR\u0014\u0010\u0011\u001a\u00020\fX\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u000eR-\u0010\u0013\u001a\u001e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\u00040\u0014j\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\u0004`\u0015\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0017R\u0014\u0010\u0018\u001a\u00020\u0019X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u001bR\u0014\u0010\u001c\u001a\u00020\fX\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u000eR\u0014\u0010\u001e\u001a\u00020\fX\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001f\u0010\u000eR\u0014\u0010 \u001a\u00020\fX\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b!\u0010\u000eR\u0014\u0010\"\u001a\u00020\fX\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b#\u0010\u000eR\u0014\u0010$\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b%\u0010\u0006R\u0014\u0010&\u001a\u00020\bX\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b'\u0010\nR\u001a\u0010(\u001a\b\u0012\u0004\u0012\u00020\f0)X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b*\u0010+R\u0014\u0010,\u001a\u00020\fX\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b-\u0010\u000eR\u0014\u0010.\u001a\u00020\fX\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b/\u0010\u000eR\u001a\u00100\u001a\b\u0012\u0004\u0012\u00020\f0)X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b1\u0010+R\u0014\u00102\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b3\u0010\u0006R\u0014\u00104\u001a\u00020\bX\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b5\u0010\nR\u0014\u00106\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b7\u0010\u0006R\u0014\u00108\u001a\u00020\fX\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b9\u0010\u000eR\u0014\u0010:\u001a\u00020\fX\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b;\u0010\u000e\u00a8\u0006<"}, d2 = {"Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;", "", "()V", "NAV_ISSUE_SENT_TOAST_ID", "", "getNAV_ISSUE_SENT_TOAST_ID", "()Ljava/lang/String;", "TOAST_TIMEOUT", "", "getTOAST_TIMEOUT", "()I", "back", "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;", "getBack", "()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;", "driveScore", "getDriveScore", "etaInaccurate", "getEtaInaccurate", "idToTitleMap", "Ljava/util/HashMap;", "Lkotlin/collections/HashMap;", "getIdToTitleMap", "()Ljava/util/HashMap;", "logger", "Lcom/navdy/service/library/log/Logger;", "getLogger", "()Lcom/navdy/service/library/log/Logger;", "maps", "getMaps", "navigation", "getNavigation", "notFastestRoute", "getNotFastestRoute", "permanentClosure", "getPermanentClosure", "reportIssue", "getReportIssue", "reportIssueColor", "getReportIssueColor", "reportIssueItems", "Ljava/util/ArrayList;", "getReportIssueItems", "()Ljava/util/ArrayList;", "roadBlocked", "getRoadBlocked", "smartDash", "getSmartDash", "takeSnapShotItems", "getTakeSnapShotItems", "takeSnapshot", "getTakeSnapshot", "takeSnapshotColor", "getTakeSnapshotColor", "toastSentSuccessfully", "getToastSentSuccessfully", "wrongDirection", "getWrongDirection", "wrongRoadName", "getWrongRoadName", "app_hudDebug"}, k = 1, mv = {1, 1, 6})
    /* compiled from: ReportIssueMenu.kt */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(kotlin.jvm.internal.DefaultConstructorMarker $constructor_marker) {
            this();
        }

        /* access modifiers changed from: private */
        public final com.navdy.service.library.log.Logger getLogger() {
            return com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu.logger;
        }

        @org.jetbrains.annotations.NotNull
        public final java.util.HashMap<java.lang.Integer, java.lang.String> getIdToTitleMap() {
            return com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu.idToTitleMap;
        }

        /* access modifiers changed from: private */
        public final int getTOAST_TIMEOUT() {
            return com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu.TOAST_TIMEOUT;
        }

        /* access modifiers changed from: private */
        public final java.lang.String getNAV_ISSUE_SENT_TOAST_ID() {
            return com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu.NAV_ISSUE_SENT_TOAST_ID;
        }

        /* access modifiers changed from: private */
        public final com.navdy.hud.app.ui.component.vlist.VerticalList.Model getBack() {
            return com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu.back;
        }

        /* access modifiers changed from: private */
        public final com.navdy.hud.app.ui.component.vlist.VerticalList.Model getEtaInaccurate() {
            return com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu.etaInaccurate;
        }

        /* access modifiers changed from: private */
        public final com.navdy.hud.app.ui.component.vlist.VerticalList.Model getNotFastestRoute() {
            return com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu.notFastestRoute;
        }

        /* access modifiers changed from: private */
        public final com.navdy.hud.app.ui.component.vlist.VerticalList.Model getRoadBlocked() {
            return com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu.roadBlocked;
        }

        /* access modifiers changed from: private */
        public final com.navdy.hud.app.ui.component.vlist.VerticalList.Model getWrongDirection() {
            return com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu.wrongDirection;
        }

        /* access modifiers changed from: private */
        public final com.navdy.hud.app.ui.component.vlist.VerticalList.Model getWrongRoadName() {
            return com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu.wrongRoadName;
        }

        /* access modifiers changed from: private */
        public final com.navdy.hud.app.ui.component.vlist.VerticalList.Model getPermanentClosure() {
            return com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu.permanentClosure;
        }

        /* access modifiers changed from: private */
        public final com.navdy.hud.app.ui.component.vlist.VerticalList.Model getMaps() {
            return com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu.maps;
        }

        /* access modifiers changed from: private */
        public final com.navdy.hud.app.ui.component.vlist.VerticalList.Model getNavigation() {
            return com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu.navigation;
        }

        /* access modifiers changed from: private */
        public final com.navdy.hud.app.ui.component.vlist.VerticalList.Model getDriveScore() {
            return com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu.driveScore;
        }

        /* access modifiers changed from: private */
        public final com.navdy.hud.app.ui.component.vlist.VerticalList.Model getSmartDash() {
            return com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu.smartDash;
        }

        /* access modifiers changed from: private */
        public final java.lang.String getToastSentSuccessfully() {
            return com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu.toastSentSuccessfully;
        }

        /* access modifiers changed from: private */
        public final java.lang.String getReportIssue() {
            return com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu.reportIssue;
        }

        /* access modifiers changed from: private */
        public final int getReportIssueColor() {
            return com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu.reportIssueColor;
        }

        /* access modifiers changed from: private */
        public final java.lang.String getTakeSnapshot() {
            return com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu.takeSnapshot;
        }

        /* access modifiers changed from: private */
        public final int getTakeSnapshotColor() {
            return com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu.takeSnapshotColor;
        }

        /* access modifiers changed from: private */
        public final java.util.ArrayList<com.navdy.hud.app.ui.component.vlist.VerticalList.Model> getReportIssueItems() {
            return com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu.reportIssueItems;
        }

        /* access modifiers changed from: private */
        public final java.util.ArrayList<com.navdy.hud.app.ui.component.vlist.VerticalList.Model> getTakeSnapShotItems() {
            return com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu.takeSnapShotItems;
        }
    }

    @kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0004\b\u0080\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004\u00a8\u0006\u0005"}, d2 = {"Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$ReportIssueMenuType;", "", "(Ljava/lang/String;I)V", "NAVIGATION_ISSUES", "SNAP_SHOT", "app_hudDebug"}, k = 1, mv = {1, 1, 6})
    /* compiled from: ReportIssueMenu.kt */
    public enum ReportIssueMenuType {
        NAVIGATION_ISSUES,
        SNAP_SHOT
    }

    public ReportIssueMenu(@org.jetbrains.annotations.NotNull com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent vscrollComponent2, @org.jetbrains.annotations.NotNull com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.Presenter presenter2, @org.jetbrains.annotations.Nullable com.navdy.hud.app.ui.component.mainmenu.IMenu parent2, @org.jetbrains.annotations.NotNull com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu.ReportIssueMenuType type2) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(vscrollComponent2, "vscrollComponent");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(presenter2, "presenter");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(type2, "type");
        this.vscrollComponent = vscrollComponent2;
        this.presenter = presenter2;
        this.parent = parent2;
        this.type = type2;
    }

    public /* synthetic */ ReportIssueMenu(com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent verticalMenuComponent, com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.Presenter presenter2, com.navdy.hud.app.ui.component.mainmenu.IMenu iMenu, com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu.ReportIssueMenuType reportIssueMenuType, int i, kotlin.jvm.internal.DefaultConstructorMarker defaultConstructorMarker) {
        if ((i & 8) != 0) {
            reportIssueMenuType = com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu.ReportIssueMenuType.NAVIGATION_ISSUES;
        }
        this(verticalMenuComponent, presenter2, iMenu, reportIssueMenuType);
    }

    public ReportIssueMenu(@org.jetbrains.annotations.NotNull com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent vscrollComponent2, @org.jetbrains.annotations.NotNull com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.Presenter presenter2, @org.jetbrains.annotations.NotNull com.navdy.hud.app.ui.component.mainmenu.IMenu parent2) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(vscrollComponent2, "vscrollComponent");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(presenter2, "presenter");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(parent2, "parent");
        this(vscrollComponent2, presenter2, parent2, com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu.ReportIssueMenuType.NAVIGATION_ISSUES);
    }

    static {
        android.content.res.Resources resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        java.lang.String string = resources.getString(com.navdy.hud.app.R.string.issue_successfully_sent);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(string, "resources.getString(R.st\u2026.issue_successfully_sent)");
        toastSentSuccessfully = string;
        java.lang.String string2 = resources.getString(com.navdy.hud.app.R.string.report_navigation_issue);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(string2, "resources.getString(R.st\u2026.report_navigation_issue)");
        reportIssue = string2;
        java.lang.String string3 = resources.getString(com.navdy.hud.app.R.string.take_snapshot);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(string3, "resources.getString(R.string.take_snapshot)");
        takeSnapshot = string3;
        reportIssueColor = resources.getColor(com.navdy.hud.app.R.color.mm_options_report_issue);
        takeSnapshotColor = resources.getColor(com.navdy.hud.app.R.color.options_dash_purple);
        int bkColorUnselected = resources.getColor(com.navdy.hud.app.R.color.icon_bk_color_unselected);
        com.navdy.hud.app.ui.component.vlist.VerticalList.Model buildModel = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.menu_back, com.navdy.hud.app.R.drawable.icon_mm_back, Companion.getReportIssueColor(), com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, Companion.getReportIssueColor(), resources.getString(com.navdy.hud.app.R.string.back), null);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(buildModel, "IconBkColorViewHolder.bu\u2026back), null\n            )");
        back = buildModel;
        com.navdy.hud.app.ui.component.vlist.VerticalList.Model buildModel2 = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.report_issue_menu_eta_inaccurate, com.navdy.hud.app.R.drawable.icon_eta, Companion.getReportIssueColor(), bkColorUnselected, Companion.getReportIssueColor(), resources.getString(com.navdy.hud.app.util.ReportIssueService.IssueType.INEFFICIENT_ROUTE_ETA_TRAFFIC.getTitleStringResource()), null);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(buildModel2, "IconBkColorViewHolder.bu\u2026urce), null\n            )");
        etaInaccurate = buildModel2;
        com.navdy.hud.app.ui.component.vlist.VerticalList.Model buildModel3 = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.report_issue_menu_not_fastest_route, com.navdy.hud.app.R.drawable.icon_bad_route, Companion.getReportIssueColor(), bkColorUnselected, Companion.getReportIssueColor(), resources.getString(com.navdy.hud.app.util.ReportIssueService.IssueType.INEFFICIENT_ROUTE_SELECTED.getTitleStringResource()), null);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(buildModel3, "IconBkColorViewHolder.bu\u2026urce), null\n            )");
        notFastestRoute = buildModel3;
        com.navdy.hud.app.ui.component.vlist.VerticalList.Model buildModel4 = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.report_issue_menu_road_blocked, com.navdy.hud.app.R.drawable.icon_road_closed_temp, Companion.getReportIssueColor(), bkColorUnselected, Companion.getReportIssueColor(), resources.getString(com.navdy.hud.app.util.ReportIssueService.IssueType.ROAD_CLOSED.getTitleStringResource()), resources.getString(com.navdy.hud.app.util.ReportIssueService.IssueType.ROAD_CLOSED.getMessageStringResource()));
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(buildModel4, "IconBkColorViewHolder.bu\u2026ngResource)\n            )");
        roadBlocked = buildModel4;
        com.navdy.hud.app.ui.component.vlist.VerticalList.Model buildModel5 = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.report_issue_menu_wrong_direction, com.navdy.hud.app.R.drawable.icon_bad_map_info, Companion.getReportIssueColor(), bkColorUnselected, Companion.getReportIssueColor(), resources.getString(com.navdy.hud.app.util.ReportIssueService.IssueType.WRONG_DIRECTION.getTitleStringResource()), null);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(buildModel5, "IconBkColorViewHolder.bu\u2026urce), null\n            )");
        wrongDirection = buildModel5;
        com.navdy.hud.app.ui.component.vlist.VerticalList.Model buildModel6 = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.report_issue_menu_wrong_road_name, com.navdy.hud.app.R.drawable.icon_wrong_road_name, Companion.getReportIssueColor(), bkColorUnselected, Companion.getReportIssueColor(), resources.getString(com.navdy.hud.app.util.ReportIssueService.IssueType.ROAD_NAME.getTitleStringResource()), null);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(buildModel6, "IconBkColorViewHolder.bu\u2026urce), null\n            )");
        wrongRoadName = buildModel6;
        com.navdy.hud.app.ui.component.vlist.VerticalList.Model buildModel7 = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.report_issue_menu_permanent_closure, com.navdy.hud.app.R.drawable.icon_road_closed_perm, Companion.getReportIssueColor(), bkColorUnselected, Companion.getReportIssueColor(), resources.getString(com.navdy.hud.app.util.ReportIssueService.IssueType.ROAD_CLOSED_PERMANENT.getTitleStringResource()), null);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(buildModel7, "IconBkColorViewHolder.bu\u2026urce), null\n            )");
        permanentClosure = buildModel7;
        Companion.getReportIssueItems().add(Companion.getBack());
        Companion.getReportIssueItems().add(Companion.getEtaInaccurate());
        Companion.getReportIssueItems().add(Companion.getNotFastestRoute());
        Companion.getReportIssueItems().add(Companion.getRoadBlocked());
        Companion.getReportIssueItems().add(Companion.getWrongDirection());
        Companion.getReportIssueItems().add(Companion.getWrongRoadName());
        Companion.getReportIssueItems().add(Companion.getPermanentClosure());
        com.navdy.hud.app.ui.component.vlist.VerticalList.Model buildModel8 = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.take_snapshot_maps, com.navdy.hud.app.R.drawable.icon_options_report_issue_2, Companion.getTakeSnapshotColor(), bkColorUnselected, Companion.getTakeSnapshotColor(), resources.getString(com.navdy.hud.app.R.string.snapshot_maps), null);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(buildModel8, "IconBkColorViewHolder.bu\u2026ing.snapshot_maps), null)");
        maps = buildModel8;
        com.navdy.hud.app.ui.component.vlist.VerticalList.Model buildModel9 = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.take_snapshot_navigation, com.navdy.hud.app.R.drawable.icon_options_report_issue_2, Companion.getTakeSnapshotColor(), bkColorUnselected, Companion.getTakeSnapshotColor(), resources.getString(com.navdy.hud.app.R.string.snapshot_navigation), null);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(buildModel9, "IconBkColorViewHolder.bu\u2026apshot_navigation), null)");
        navigation = buildModel9;
        com.navdy.hud.app.ui.component.vlist.VerticalList.Model buildModel10 = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.take_snapshot_smart_dash, com.navdy.hud.app.R.drawable.icon_options_report_issue_2, Companion.getTakeSnapshotColor(), bkColorUnselected, Companion.getTakeSnapshotColor(), resources.getString(com.navdy.hud.app.R.string.snapshot_smart_dash), null);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(buildModel10, "IconBkColorViewHolder.bu\u2026apshot_smart_dash), null)");
        driveScore = buildModel10;
        com.navdy.hud.app.ui.component.vlist.VerticalList.Model buildModel11 = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.take_snapshot_drive_score, com.navdy.hud.app.R.drawable.icon_options_report_issue_2, Companion.getTakeSnapshotColor(), bkColorUnselected, Companion.getTakeSnapshotColor(), resources.getString(com.navdy.hud.app.R.string.snapshot_drive_score), null);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(buildModel11, "IconBkColorViewHolder.bu\u2026pshot_drive_score), null)");
        smartDash = buildModel11;
        Companion.getTakeSnapShotItems().add(Companion.getMaps());
        Companion.getTakeSnapShotItems().add(Companion.getNavigation());
        Companion.getTakeSnapShotItems().add(Companion.getDriveScore());
        Companion.getTakeSnapShotItems().add(Companion.getSmartDash());
    }

    @org.jetbrains.annotations.Nullable
    public java.util.List<com.navdy.hud.app.ui.component.vlist.VerticalList.Model> getItems() {
        switch (this.type) {
            case NAVIGATION_ISSUES:
                this.currentItems = Companion.getReportIssueItems();
                break;
            case SNAP_SHOT:
                this.currentItems = Companion.getTakeSnapShotItems();
                break;
            default:
                return null;
        }
        return this.currentItems;
    }

    public int getInitialSelection() {
        switch (this.type) {
            case NAVIGATION_ISSUES:
                return 1;
            case SNAP_SHOT:
                return 0;
            default:
                throw new kotlin.NoWhenBranchMatchedException();
        }
    }

    @org.jetbrains.annotations.Nullable
    public com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex getScrollIndex() {
        return null;
    }

    public void setBackSelectionPos(int n) {
        this.backSelection = n;
    }

    public void setBackSelectionId(int id) {
        this.backSelectionId = id;
    }

    public void setSelectedIcon() {
        switch (this.type) {
            case NAVIGATION_ISSUES:
                this.vscrollComponent.setSelectedIconColorImage(com.navdy.hud.app.R.drawable.icon_options_report_issue_2, Companion.getReportIssueColor(), null, 1.0f);
                this.vscrollComponent.selectedText.setText(Companion.getReportIssue());
                return;
            case SNAP_SHOT:
                this.vscrollComponent.setSelectedIconColorImage(com.navdy.hud.app.R.drawable.icon_options_report_issue_2, Companion.getTakeSnapshotColor(), null, 1.0f);
                this.vscrollComponent.selectedText.setText(Companion.getTakeSnapshot());
                return;
            default:
                return;
        }
    }

    public boolean selectItem(@org.jetbrains.annotations.NotNull com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState selection) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(selection, "selection");
        Companion.getLogger().v("select id:" + selection.id + " pos:" + selection.pos);
        switch (selection.id) {
            case com.navdy.hud.app.R.id.menu_back /*2131624007*/:
                Companion.getLogger().v("back");
                com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("report-issue-back");
                this.presenter.loadMenu(this.parent, com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel.BACK_TO_PARENT, this.backSelection, this.backSelectionId, true);
                break;
            case com.navdy.hud.app.R.id.report_issue_menu_eta_inaccurate /*2131624042*/:
                Companion.getLogger().v("ETA inaccurate");
                com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("report-issue-eta-inaccurate");
                reportIssue(com.navdy.hud.app.util.ReportIssueService.IssueType.INEFFICIENT_ROUTE_ETA_TRAFFIC);
                break;
            case com.navdy.hud.app.R.id.report_issue_menu_not_fastest_route /*2131624043*/:
                Companion.getLogger().v("Not the fastest route");
                com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("report-issue-not-fastest-route");
                reportIssue(com.navdy.hud.app.util.ReportIssueService.IssueType.INEFFICIENT_ROUTE_SELECTED);
                break;
            case com.navdy.hud.app.R.id.report_issue_menu_permanent_closure /*2131624044*/:
                Companion.getLogger().v("Permanent closure");
                com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("report-issue-permanent-closure");
                reportIssue(com.navdy.hud.app.util.ReportIssueService.IssueType.ROAD_CLOSED_PERMANENT);
                break;
            case com.navdy.hud.app.R.id.report_issue_menu_road_blocked /*2131624045*/:
                Companion.getLogger().v("Road blocked");
                com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("report-issue-road-blocked");
                reportIssue(com.navdy.hud.app.util.ReportIssueService.IssueType.ROAD_CLOSED);
                break;
            case com.navdy.hud.app.R.id.report_issue_menu_wrong_direction /*2131624046*/:
                Companion.getLogger().v("Wrong direction");
                com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("report-issue-wrong-direction");
                reportIssue(com.navdy.hud.app.util.ReportIssueService.IssueType.WRONG_DIRECTION);
                break;
            case com.navdy.hud.app.R.id.report_issue_menu_wrong_road_name /*2131624047*/:
                Companion.getLogger().v("Wrong road name");
                com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("report-issue-wrong-road-name");
                reportIssue(com.navdy.hud.app.util.ReportIssueService.IssueType.ROAD_NAME);
                break;
            case com.navdy.hud.app.R.id.take_snapshot_drive_score /*2131624072*/:
            case com.navdy.hud.app.R.id.take_snapshot_maps /*2131624073*/:
            case com.navdy.hud.app.R.id.take_snapshot_navigation /*2131624074*/:
            case com.navdy.hud.app.R.id.take_snapshot_smart_dash /*2131624075*/:
                takeSnapshot(selection.id);
                break;
        }
        return false;
    }

    private final void reportIssue(com.navdy.hud.app.util.ReportIssueService.IssueType issueType) {
        this.presenter.performSelectionAnimation(new com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$reportIssue$Anon1(this, issueType));
    }

    private final void takeSnapshot(int id) {
        this.presenter.performSelectionAnimation(new com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$takeSnapshot$Anon1(this, id));
    }

    /* access modifiers changed from: private */
    public final void showSentToast() {
        android.os.Bundle bundle = new android.os.Bundle();
        bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_TIMEOUT, Companion.getTOAST_TIMEOUT());
        bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_IMAGE, com.navdy.hud.app.R.drawable.icon_report_issue);
        bundle.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_2, Companion.getToastSentSuccessfully());
        bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_2_STYLE, com.navdy.hud.app.R.style.Glances_1);
        bundle.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_TTS, com.navdy.hud.app.framework.voice.TTSUtils.TTS_NAV_STOPPED);
        bundle.putBoolean(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_NO_START_DELAY, true);
        bundle.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_SHOW_SCREEN_ID, com.navdy.service.library.events.ui.Screen.SCREEN_BACK.name());
        com.navdy.hud.app.framework.toast.ToastManager.getInstance().addToast(new com.navdy.hud.app.framework.toast.ToastManager.ToastParams(Companion.getNAV_ISSUE_SENT_TOAST_ID(), bundle, null, true, false));
    }

    @org.jetbrains.annotations.NotNull
    public com.navdy.hud.app.ui.component.mainmenu.IMenu.Menu getType() {
        return com.navdy.hud.app.ui.component.mainmenu.IMenu.Menu.REPORT_ISSUE;
    }

    public boolean isItemClickable(int id, int pos) {
        return true;
    }

    @org.jetbrains.annotations.Nullable
    public com.navdy.hud.app.ui.component.vlist.VerticalList.Model getModelfromPos(int pos) {
        if (this.currentItems != null) {
            java.util.List<? extends com.navdy.hud.app.ui.component.vlist.VerticalList.Model> list = this.currentItems;
            if (list == null) {
                kotlin.jvm.internal.Intrinsics.throwNpe();
            }
            if (list.size() > pos) {
                java.util.List<? extends com.navdy.hud.app.ui.component.vlist.VerticalList.Model> list2 = this.currentItems;
                if (list2 == null) {
                    kotlin.jvm.internal.Intrinsics.throwNpe();
                }
                return (com.navdy.hud.app.ui.component.vlist.VerticalList.Model) list2.get(pos);
            }
        }
        return null;
    }

    public boolean isBindCallsEnabled() {
        return false;
    }

    public void onBindToView(@org.jetbrains.annotations.NotNull com.navdy.hud.app.ui.component.vlist.VerticalList.Model model, @org.jetbrains.annotations.NotNull android.view.View view, int pos, @org.jetbrains.annotations.NotNull com.navdy.hud.app.ui.component.vlist.VerticalList.ModelState state) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(model, "model");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(view, "view");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(state, com.amazonaws.mobileconnectors.s3.transferutility.TransferTable.COLUMN_STATE);
    }

    @org.jetbrains.annotations.Nullable
    public com.navdy.hud.app.ui.component.mainmenu.IMenu getChildMenu(@org.jetbrains.annotations.NotNull com.navdy.hud.app.ui.component.mainmenu.IMenu parent2, @org.jetbrains.annotations.NotNull java.lang.String args, @org.jetbrains.annotations.NotNull java.lang.String path) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(parent2, "parent");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(args, "args");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(path, "path");
        return null;
    }

    public void onUnload(@org.jetbrains.annotations.NotNull com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel level) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(level, "level");
    }

    public void onItemSelected(@org.jetbrains.annotations.NotNull com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState selection) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(selection, "selection");
    }

    public void onScrollIdle() {
    }

    public void onFastScrollStart() {
    }

    public void onFastScrollEnd() {
    }

    public void showToolTip() {
    }

    public boolean isFirstItemEmpty() {
        return true;
    }
}
