package com.navdy.hud.app.ui.component.mainmenu;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u00d2\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\n\u0018\u0000 r2\u00020\u0001:\u0002rsB-\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u0001\u00a2\u0006\u0002\u0010\u000bJ\u0010\u00109\u001a\u00020:2\u0006\u0010;\u001a\u00020<H\u0007J\"\u0010=\u001a\u0004\u0018\u00010\u00012\u0006\u0010\n\u001a\u00020\u00012\u0006\u0010>\u001a\u00020?2\u0006\u0010@\u001a\u00020?H\u0016J\b\u0010A\u001a\u00020\rH\u0016J\u0010\u0010B\u001a\n\u0012\u0004\u0012\u00020D\u0018\u00010CH\u0016J\u0012\u0010E\u001a\u0004\u0018\u00010D2\u0006\u0010F\u001a\u00020\rH\u0016J\n\u0010G\u001a\u0004\u0018\u00010HH\u0016J\n\u0010I\u001a\u0004\u0018\u00010JH\u0016J\b\u0010K\u001a\u00020'H\u0016J\b\u0010L\u001a\u00020'H\u0016J\u0018\u0010M\u001a\u00020'2\u0006\u0010N\u001a\u00020\r2\u0006\u0010F\u001a\u00020\rH\u0016J(\u0010O\u001a\u00020:2\u0006\u0010P\u001a\u00020D2\u0006\u0010Q\u001a\u00020R2\u0006\u0010F\u001a\u00020\r2\u0006\u0010S\u001a\u00020TH\u0016J\u0010\u0010U\u001a\u00020:2\u0006\u0010V\u001a\u00020WH\u0007J\u0010\u0010X\u001a\u00020:2\u0006\u0010Y\u001a\u00020ZH\u0007J\b\u0010[\u001a\u00020:H\u0016J\b\u0010\\\u001a\u00020:H\u0016J\u0010\u0010]\u001a\u00020:2\u0006\u0010^\u001a\u00020_H\u0007J\u0010\u0010`\u001a\u00020:2\u0006\u0010a\u001a\u00020bH\u0016J\u0010\u0010c\u001a\u00020:2\u0006\u0010;\u001a\u00020dH\u0007J\u0010\u0010e\u001a\u00020:2\u0006\u0010;\u001a\u00020fH\u0007J\b\u0010g\u001a\u00020:H\u0016J\u0010\u0010h\u001a\u00020:2\u0006\u0010i\u001a\u00020jH\u0016J\u0010\u0010k\u001a\u00020'2\u0006\u0010a\u001a\u00020bH\u0016J\u0010\u0010l\u001a\u00020:2\u0006\u0010N\u001a\u00020\rH\u0016J\u0010\u0010m\u001a\u00020:2\u0006\u0010n\u001a\u00020\rH\u0016J\b\u0010o\u001a\u00020:H\u0016J\u000e\u0010p\u001a\u00020:2\u0006\u0010F\u001a\u00020\rJ\b\u0010q\u001a\u00020:H\u0016R\u000e\u0010\f\u001a\u00020\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R$\u0010\u0011\u001a\u00020\u00102\u0006\u0010\u000f\u001a\u00020\u0010@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0012\u0010\u0013\"\u0004\b\u0014\u0010\u0015R\u001a\u0010\u0016\u001a\u00020\u0017X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0018\u0010\u0019\"\u0004\b\u001a\u0010\u001bR\u0011\u0010\u001c\u001a\u00020\u001d\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001e\u0010\u001fR\u001a\u0010 \u001a\u00020!X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\"\u0010#\"\u0004\b$\u0010%R\u000e\u0010\n\u001a\u00020\u0001X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010&\u001a\u00020'X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b(\u0010)\"\u0004\b*\u0010+R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R \u0010,\u001a\b\u0018\u00010-R\u00020.X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b/\u00100\"\u0004\b1\u00102R\u0011\u00103\u001a\u00020.\u00a2\u0006\b\n\u0000\u001a\u0004\b4\u00105R\u001a\u00106\u001a\u00020'X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b7\u0010)\"\u0004\b8\u0010+R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006t"}, d2 = {"Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;", "Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;", "bus", "Lcom/squareup/otto/Bus;", "sharedPreferences", "Landroid/content/SharedPreferences;", "vscrollComponent", "Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;", "presenter", "Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;", "parent", "(Lcom/squareup/otto/Bus;Landroid/content/SharedPreferences;Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;)V", "backSelection", "", "backSelectionId", "value", "Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;", "gaugeType", "getGaugeType", "()Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;", "setGaugeType", "(Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;)V", "gaugeView", "Lcom/navdy/hud/app/view/DashboardWidgetView;", "getGaugeView", "()Lcom/navdy/hud/app/view/DashboardWidgetView;", "setGaugeView", "(Lcom/navdy/hud/app/view/DashboardWidgetView;)V", "gaugeViewContainer", "Landroid/widget/FrameLayout;", "getGaugeViewContainer", "()Landroid/widget/FrameLayout;", "headingDataUtil", "Lcom/navdy/hud/app/util/HeadingDataUtil;", "getHeadingDataUtil", "()Lcom/navdy/hud/app/util/HeadingDataUtil;", "setHeadingDataUtil", "(Lcom/navdy/hud/app/util/HeadingDataUtil;)V", "registered", "", "getRegistered", "()Z", "setRegistered", "(Z)V", "smartDashWidgetCache", "Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;", "Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;", "getSmartDashWidgetCache", "()Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;", "setSmartDashWidgetCache", "(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;)V", "smartDashWidgetManager", "getSmartDashWidgetManager", "()Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;", "userPreferenceChanged", "getUserPreferenceChanged", "setUserPreferenceChanged", "ObdPidChangeEvent", "", "event", "Lcom/navdy/hud/app/obd/ObdManager$ObdPidChangeEvent;", "getChildMenu", "args", "", "path", "getInitialSelection", "getItems", "", "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;", "getModelfromPos", "pos", "getScrollIndex", "Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;", "getType", "Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;", "isBindCallsEnabled", "isFirstItemEmpty", "isItemClickable", "id", "onBindToView", "model", "view", "Landroid/view/View;", "state", "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;", "onDriveScoreUpdated", "driveScoreUpdated", "Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;", "onDriverProfileChanged", "profileChanged", "Lcom/navdy/hud/app/event/DriverProfileChanged;", "onFastScrollEnd", "onFastScrollStart", "onGpsLocationChanged", "location", "Landroid/location/Location;", "onItemSelected", "selection", "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;", "onLocationFixChangeEvent", "Lcom/navdy/hud/app/maps/MapEvents$LocationFix;", "onMapEvent", "Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;", "onScrollIdle", "onUnload", "level", "Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;", "selectItem", "setBackSelectionId", "setBackSelectionPos", "n", "setSelectedIcon", "showGauge", "showToolTip", "Companion", "GaugeType", "app_hudDebug"}, k = 1, mv = {1, 1, 6})
/* compiled from: DashGaugeConfiguratorMenu.kt */
public final class DashGaugeConfiguratorMenu implements com.navdy.hud.app.ui.component.mainmenu.IMenu {
    public static final com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu.Companion Companion = new com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu.Companion(null);
    /* access modifiers changed from: private */
    public static final com.navdy.hud.app.ui.component.vlist.VerticalList.Model back;
    /* access modifiers changed from: private */
    public static final int backColor;
    /* access modifiers changed from: private */
    public static final int bkColorUnselected;
    /* access modifiers changed from: private */
    public static final int centerGaugeBackgroundColor;
    /* access modifiers changed from: private */
    public static final java.util.ArrayList<com.navdy.hud.app.ui.component.vlist.VerticalList.Model> centerGaugeOptionsList = new java.util.ArrayList<>();
    /* access modifiers changed from: private */
    public static final java.lang.String centerGaugeTitle;
    /* access modifiers changed from: private */
    public static final java.lang.String offLabel;
    /* access modifiers changed from: private */
    public static final java.lang.String onLabel;
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(Companion.getClass());
    /* access modifiers changed from: private */
    public static final java.util.ArrayList<com.navdy.hud.app.ui.component.vlist.VerticalList.Model> sideGaugesOptionsList = new java.util.ArrayList<>();
    /* access modifiers changed from: private */
    public static final java.lang.String sideGaugesTitle;
    /* access modifiers changed from: private */
    public static final com.navdy.hud.app.ui.component.vlist.VerticalList.Model speedoMeter;
    /* access modifiers changed from: private */
    public static final com.navdy.hud.app.ui.component.vlist.VerticalList.Model tachoMeter;
    /* access modifiers changed from: private */
    public static final com.navdy.hud.app.ui.framework.UIStateManager uiStateManager;
    /* access modifiers changed from: private */
    public static final java.lang.String unavailableLabel;
    private int backSelection;
    private int backSelectionId;
    private final com.squareup.otto.Bus bus;
    @org.jetbrains.annotations.NotNull
    private com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu.GaugeType gaugeType;
    @org.jetbrains.annotations.NotNull
    private com.navdy.hud.app.view.DashboardWidgetView gaugeView;
    @org.jetbrains.annotations.NotNull
    private final android.widget.FrameLayout gaugeViewContainer;
    @org.jetbrains.annotations.NotNull
    private com.navdy.hud.app.util.HeadingDataUtil headingDataUtil = new com.navdy.hud.app.util.HeadingDataUtil();
    private final com.navdy.hud.app.ui.component.mainmenu.IMenu parent;
    /* access modifiers changed from: private */
    public final com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.Presenter presenter;
    private boolean registered;
    private final android.content.SharedPreferences sharedPreferences;
    @org.jetbrains.annotations.Nullable
    private com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.SmartDashWidgetCache smartDashWidgetCache;
    @org.jetbrains.annotations.NotNull
    private final com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager smartDashWidgetManager;
    private boolean userPreferenceChanged;
    private final com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent vscrollComponent;

    @kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u0010\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\b\u0005"}, d2 = {"<anonymous>", "", "identifier", "", "kotlin.jvm.PlatformType", "filter"}, k = 3, mv = {1, 1, 6})
    /* compiled from: DashGaugeConfiguratorMenu.kt */
    static final class Anon1 implements com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.IWidgetFilter {
        public static final com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu.Anon1 INSTANCE = new com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu.Anon1();

        Anon1() {
        }

        public final boolean filter(java.lang.String identifier) {
            if (identifier != null) {
                switch (identifier.hashCode()) {
                    case 811991446:
                        if (identifier.equals(com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.EMPTY_WIDGET_ID)) {
                            return true;
                        }
                        break;
                    case 1872543020:
                        if (identifier.equals(com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.TRAFFIC_INCIDENT_GAUGE_ID)) {
                            if (com.navdy.hud.app.maps.MapSettings.isTrafficDashWidgetsEnabled()) {
                                return false;
                            }
                            return true;
                        }
                        break;
                }
            }
            return false;
        }
    }

    @kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u0019\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\b\n\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0007\u00a8\u0006\u0007"}, d2 = {"com/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Anon2", "", "(Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;)V", "onReload", "", "reload", "Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$Reload;", "app_hudDebug"}, k = 1, mv = {1, 1, 6})
    /* compiled from: DashGaugeConfiguratorMenu.kt */
    public static final class Anon2 {
        final /* synthetic */ com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu this$Anon0;

        Anon2(com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu $outer) {
            this.this$Anon0 = $outer;
        }

        @com.squareup.otto.Subscribe
        public final void onReload(@org.jetbrains.annotations.NotNull com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.Reload reload) {
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(reload, "reload");
            switch (com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu.WhenMappings.$EnumSwitchMapping$Anon0[reload.ordinal()]) {
                case 1:
                    this.this$Anon0.presenter.updateCurrentMenu(this.this$Anon0);
                    return;
                case 2:
                    this.this$Anon0.setSmartDashWidgetCache(this.this$Anon0.getSmartDashWidgetManager().buildSmartDashWidgetCache(0));
                    this.this$Anon0.presenter.updateCurrentMenu(this.this$Anon0);
                    return;
                default:
                    return;
            }
        }
    }

    @kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u0014\u0010\u0007\u001a\u00020\bX\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0014\u0010\u000b\u001a\u00020\bX\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\f\u0010\nR\u0014\u0010\r\u001a\u00020\bX\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\nR\u001a\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00040\u0010X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012R\u0014\u0010\u0013\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0016R\u0014\u0010\u0017\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0016R\u0014\u0010\u0019\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u0016R\u0014\u0010\u001b\u001a\u00020\u001cX\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u001eR\u001a\u0010\u001f\u001a\b\u0012\u0004\u0012\u00020\u00040\u0010X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b \u0010\u0012R\u0014\u0010!\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\"\u0010\u0016R\u0014\u0010#\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b$\u0010\u0006R\u0014\u0010%\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b&\u0010\u0006R\u0014\u0010'\u001a\u00020(X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b)\u0010*R\u0014\u0010+\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b,\u0010\u0016\u00a8\u0006-"}, d2 = {"Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;", "", "()V", "back", "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;", "getBack", "()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;", "backColor", "", "getBackColor", "()I", "bkColorUnselected", "getBkColorUnselected", "centerGaugeBackgroundColor", "getCenterGaugeBackgroundColor", "centerGaugeOptionsList", "Ljava/util/ArrayList;", "getCenterGaugeOptionsList", "()Ljava/util/ArrayList;", "centerGaugeTitle", "", "getCenterGaugeTitle", "()Ljava/lang/String;", "offLabel", "getOffLabel", "onLabel", "getOnLabel", "sLogger", "Lcom/navdy/service/library/log/Logger;", "getSLogger", "()Lcom/navdy/service/library/log/Logger;", "sideGaugesOptionsList", "getSideGaugesOptionsList", "sideGaugesTitle", "getSideGaugesTitle", "speedoMeter", "getSpeedoMeter", "tachoMeter", "getTachoMeter", "uiStateManager", "Lcom/navdy/hud/app/ui/framework/UIStateManager;", "getUiStateManager", "()Lcom/navdy/hud/app/ui/framework/UIStateManager;", "unavailableLabel", "getUnavailableLabel", "app_hudDebug"}, k = 1, mv = {1, 1, 6})
    /* compiled from: DashGaugeConfiguratorMenu.kt */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(kotlin.jvm.internal.DefaultConstructorMarker $constructor_marker) {
            this();
        }

        /* access modifiers changed from: private */
        public final com.navdy.hud.app.ui.component.vlist.VerticalList.Model getBack() {
            return com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu.back;
        }

        /* access modifiers changed from: private */
        public final com.navdy.hud.app.ui.component.vlist.VerticalList.Model getTachoMeter() {
            return com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu.tachoMeter;
        }

        /* access modifiers changed from: private */
        public final com.navdy.hud.app.ui.component.vlist.VerticalList.Model getSpeedoMeter() {
            return com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu.speedoMeter;
        }

        /* access modifiers changed from: private */
        public final int getBackColor() {
            return com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu.backColor;
        }

        /* access modifiers changed from: private */
        public final int getBkColorUnselected() {
            return com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu.bkColorUnselected;
        }

        /* access modifiers changed from: private */
        public final java.util.ArrayList<com.navdy.hud.app.ui.component.vlist.VerticalList.Model> getCenterGaugeOptionsList() {
            return com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu.centerGaugeOptionsList;
        }

        /* access modifiers changed from: private */
        public final java.util.ArrayList<com.navdy.hud.app.ui.component.vlist.VerticalList.Model> getSideGaugesOptionsList() {
            return com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu.sideGaugesOptionsList;
        }

        /* access modifiers changed from: private */
        public final com.navdy.service.library.log.Logger getSLogger() {
            return com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu.sLogger;
        }

        /* access modifiers changed from: private */
        public final java.lang.String getCenterGaugeTitle() {
            return com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu.centerGaugeTitle;
        }

        /* access modifiers changed from: private */
        public final java.lang.String getSideGaugesTitle() {
            return com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu.sideGaugesTitle;
        }

        /* access modifiers changed from: private */
        public final com.navdy.hud.app.ui.framework.UIStateManager getUiStateManager() {
            return com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu.uiStateManager;
        }

        /* access modifiers changed from: private */
        public final int getCenterGaugeBackgroundColor() {
            return com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu.centerGaugeBackgroundColor;
        }

        /* access modifiers changed from: private */
        public final java.lang.String getOnLabel() {
            return com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu.onLabel;
        }

        /* access modifiers changed from: private */
        public final java.lang.String getOffLabel() {
            return com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu.offLabel;
        }

        /* access modifiers changed from: private */
        public final java.lang.String getUnavailableLabel() {
            return com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu.unavailableLabel;
        }
    }

    @kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0004\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004\u00a8\u0006\u0005"}, d2 = {"Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;", "", "(Ljava/lang/String;I)V", "CENTER", "SIDE", "app_hudDebug"}, k = 1, mv = {1, 1, 6})
    /* compiled from: DashGaugeConfiguratorMenu.kt */
    public enum GaugeType {
        CENTER,
        SIDE
    }

    @kotlin.Metadata(bv = {1, 0, 1}, k = 3, mv = {1, 1, 6})
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$Anon0 = new int[com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.Reload.values().length];

        static {
            $EnumSwitchMapping$Anon0[com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.Reload.RELOADED.ordinal()] = 1;
            $EnumSwitchMapping$Anon0[com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.Reload.RELOAD_CACHE.ordinal()] = 2;
            $EnumSwitchMapping$Anon1 = new int[com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu.GaugeType.values().length];
            $EnumSwitchMapping$Anon1[com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu.GaugeType.CENTER.ordinal()] = 1;
            $EnumSwitchMapping$Anon1[com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu.GaugeType.SIDE.ordinal()] = 2;
            $EnumSwitchMapping$Anon2 = new int[com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu.GaugeType.values().length];
            $EnumSwitchMapping$Anon2[com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu.GaugeType.CENTER.ordinal()] = 1;
            $EnumSwitchMapping$Anon2[com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu.GaugeType.SIDE.ordinal()] = 2;
            $EnumSwitchMapping$Anon3 = new int[com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu.GaugeType.values().length];
            $EnumSwitchMapping$Anon3[com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu.GaugeType.CENTER.ordinal()] = 1;
            $EnumSwitchMapping$Anon3[com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu.GaugeType.SIDE.ordinal()] = 2;
            $EnumSwitchMapping$Anon4 = new int[com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu.GaugeType.values().length];
            $EnumSwitchMapping$Anon4[com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu.GaugeType.CENTER.ordinal()] = 1;
            $EnumSwitchMapping$Anon4[com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu.GaugeType.SIDE.ordinal()] = 2;
        }
    }

    public DashGaugeConfiguratorMenu(@org.jetbrains.annotations.NotNull com.squareup.otto.Bus bus2, @org.jetbrains.annotations.NotNull android.content.SharedPreferences sharedPreferences2, @org.jetbrains.annotations.NotNull com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent vscrollComponent2, @org.jetbrains.annotations.NotNull com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.Presenter presenter2, @org.jetbrains.annotations.NotNull com.navdy.hud.app.ui.component.mainmenu.IMenu parent2) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(bus2, "bus");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(sharedPreferences2, "sharedPreferences");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(vscrollComponent2, "vscrollComponent");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(presenter2, "presenter");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(parent2, "parent");
        this.bus = bus2;
        this.sharedPreferences = sharedPreferences2;
        this.vscrollComponent = vscrollComponent2;
        this.presenter = presenter2;
        this.parent = parent2;
        android.widget.FrameLayout frameLayout = this.vscrollComponent.selectedCustomView;
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(frameLayout, "vscrollComponent.selectedCustomView");
        this.gaugeViewContainer = frameLayout;
        this.gaugeView = new com.navdy.hud.app.view.DashboardWidgetView(this.gaugeViewContainer.getContext());
        this.gaugeView.setLayoutParams(new android.view.ViewGroup.LayoutParams(-1, -1));
        this.gaugeViewContainer.removeAllViews();
        this.gaugeViewContainer.addView(this.gaugeView);
        this.smartDashWidgetManager = new com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager(this.sharedPreferences, com.navdy.hud.app.HudApplication.getAppContext());
        this.smartDashWidgetManager.onResume();
        this.smartDashWidgetManager.setFilter(com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu.Anon1.INSTANCE);
        this.smartDashWidgetManager.reLoadAvailableWidgets(true);
        this.smartDashWidgetManager.registerForChanges(new com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu.Anon2(this));
        this.gaugeType = com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu.GaugeType.CENTER;
    }

    @org.jetbrains.annotations.NotNull
    public final com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager getSmartDashWidgetManager() {
        return this.smartDashWidgetManager;
    }

    @org.jetbrains.annotations.Nullable
    public final com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.SmartDashWidgetCache getSmartDashWidgetCache() {
        return this.smartDashWidgetCache;
    }

    public final void setSmartDashWidgetCache(@org.jetbrains.annotations.Nullable com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.SmartDashWidgetCache smartDashWidgetCache2) {
        this.smartDashWidgetCache = smartDashWidgetCache2;
    }

    @org.jetbrains.annotations.NotNull
    public final android.widget.FrameLayout getGaugeViewContainer() {
        return this.gaugeViewContainer;
    }

    @org.jetbrains.annotations.NotNull
    public final com.navdy.hud.app.view.DashboardWidgetView getGaugeView() {
        return this.gaugeView;
    }

    public final void setGaugeView(@org.jetbrains.annotations.NotNull com.navdy.hud.app.view.DashboardWidgetView dashboardWidgetView) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(dashboardWidgetView, "<set-?>");
        this.gaugeView = dashboardWidgetView;
    }

    @org.jetbrains.annotations.NotNull
    public final com.navdy.hud.app.util.HeadingDataUtil getHeadingDataUtil() {
        return this.headingDataUtil;
    }

    public final void setHeadingDataUtil(@org.jetbrains.annotations.NotNull com.navdy.hud.app.util.HeadingDataUtil headingDataUtil2) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(headingDataUtil2, "<set-?>");
        this.headingDataUtil = headingDataUtil2;
    }

    public final boolean getUserPreferenceChanged() {
        return this.userPreferenceChanged;
    }

    public final void setUserPreferenceChanged(boolean z) {
        this.userPreferenceChanged = z;
    }

    static {
        android.content.res.Resources resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        backColor = resources.getColor(com.navdy.hud.app.R.color.mm_back);
        bkColorUnselected = resources.getColor(com.navdy.hud.app.R.color.icon_bk_color_unselected);
        java.lang.CharSequence text = resources.getText(com.navdy.hud.app.R.string.carousel_menu_smartdash_select_center_gauge);
        if (text == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type kotlin.String");
        }
        centerGaugeTitle = (java.lang.String) text;
        java.lang.CharSequence text2 = resources.getText(com.navdy.hud.app.R.string.carousel_menu_smartdash_side_gauges);
        if (text2 == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type kotlin.String");
        }
        sideGaugesTitle = (java.lang.String) text2;
        java.lang.CharSequence text3 = resources.getText(com.navdy.hud.app.R.string.on);
        if (text3 == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type kotlin.String");
        }
        onLabel = (java.lang.String) text3;
        java.lang.CharSequence text4 = resources.getText(com.navdy.hud.app.R.string.off);
        if (text4 == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type kotlin.String");
        }
        offLabel = (java.lang.String) text4;
        java.lang.CharSequence text5 = resources.getText(com.navdy.hud.app.R.string.unavailable);
        if (text5 == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type kotlin.String");
        }
        unavailableLabel = (java.lang.String) text5;
        com.navdy.hud.app.ui.framework.UIStateManager uiStateManager2 = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getUiStateManager();
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(uiStateManager2, "remoteDeviceManager.uiStateManager");
        uiStateManager = uiStateManager2;
        centerGaugeBackgroundColor = resources.getColor(com.navdy.hud.app.R.color.mm_options_scroll_gauge);
        int fluctuatorColor = Companion.getBackColor();
        com.navdy.hud.app.ui.component.vlist.VerticalList.Model buildModel = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.menu_back, com.navdy.hud.app.R.drawable.icon_mm_back, fluctuatorColor, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, fluctuatorColor, resources.getString(com.navdy.hud.app.R.string.back), null);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(buildModel, "IconBkColorViewHolder.bu\u2026tuatorColor, title, null)");
        back = buildModel;
        java.lang.String title = resources.getString(com.navdy.hud.app.R.string.tachometer);
        com.navdy.hud.app.ui.component.vlist.VerticalList.Model buildModel2 = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.main_menu_options_tachometer, com.navdy.hud.app.R.drawable.icon_options_dash_tachometer_2, -16777216, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, resources.getColor(com.navdy.hud.app.R.color.mm_options_tachometer), title, null);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(buildModel2, "IconBkColorViewHolder.bu\u2026tuatorColor, title, null)");
        tachoMeter = buildModel2;
        java.lang.String title2 = resources.getString(com.navdy.hud.app.R.string.speedometer);
        com.navdy.hud.app.ui.component.vlist.VerticalList.Model buildModel3 = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.main_menu_options_speedometer, com.navdy.hud.app.R.drawable.icon_options_dash_speedometer_2, -16777216, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, resources.getColor(com.navdy.hud.app.R.color.mm_options_speedometer), title2, null);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(buildModel3, "IconBkColorViewHolder.bu\u2026tuatorColor, title, null)");
        speedoMeter = buildModel3;
        Companion.getCenterGaugeOptionsList().add(Companion.getBack());
        Companion.getCenterGaugeOptionsList().add(Companion.getTachoMeter());
        Companion.getCenterGaugeOptionsList().add(Companion.getSpeedoMeter());
    }

    public final boolean getRegistered() {
        return this.registered;
    }

    public final void setRegistered(boolean z) {
        this.registered = z;
    }

    @org.jetbrains.annotations.NotNull
    public final com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu.GaugeType getGaugeType() {
        return this.gaugeType;
    }

    public final void setGaugeType(@org.jetbrains.annotations.NotNull com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu.GaugeType value) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(value, com.glympse.android.hal.NotificationListener.INTENT_EXTRA_VALUE);
        this.gaugeType = value;
        if (kotlin.jvm.internal.Intrinsics.areEqual((java.lang.Object) value, (java.lang.Object) com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu.GaugeType.SIDE)) {
            if (!this.registered) {
                this.bus.register(this);
                this.registered = true;
            }
        } else if (this.registered) {
            this.bus.unregister(this);
            this.registered = false;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x006a, code lost:
        if (r3 != null) goto L_0x006c;
     */
    @org.jetbrains.annotations.Nullable
    public java.util.List<com.navdy.hud.app.ui.component.vlist.VerticalList.Model> getItems() {
        java.lang.String title;
        boolean isGaugeEnabled;
        java.lang.String subtitle;
        switch (this.gaugeType) {
            case CENTER:
                return Companion.getCenterGaugeOptionsList();
            case SIDE:
                this.smartDashWidgetCache = this.smartDashWidgetManager.buildSmartDashWidgetCache(0);
                Companion.getSideGaugesOptionsList().clear();
                Companion.getSideGaugesOptionsList().add(Companion.getBack());
                if (this.smartDashWidgetCache != null) {
                    com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.SmartDashWidgetCache $receiver = this.smartDashWidgetCache;
                    if ($receiver == null) {
                        kotlin.jvm.internal.Intrinsics.throwNpe();
                    }
                    int fluctuatorColor = com.navdy.hud.app.HudApplication.getAppContext().getResources().getColor(com.navdy.hud.app.R.color.mm_options_side_gauges_halo);
                    int i = 0;
                    int widgetsCount = $receiver.getWidgetsCount() - 1;
                    if (0 <= widgetsCount) {
                        while (true) {
                            com.navdy.hud.app.view.DashboardWidgetPresenter widgetPresenter = $receiver.getWidgetPresenter(i);
                            if (widgetPresenter != null) {
                                title = widgetPresenter.getWidgetName();
                                break;
                            }
                            title = "";
                            com.navdy.hud.app.view.DashboardWidgetPresenter widgetPresenter2 = $receiver.getWidgetPresenter(i);
                            java.lang.String identifier = widgetPresenter2 != null ? widgetPresenter2.getWidgetIdentifier() : null;
                            boolean isGaugeOn = this.smartDashWidgetManager.isGaugeOn(identifier);
                            com.navdy.obd.PidSet pidSet = com.navdy.hud.app.obd.ObdManager.getInstance().getSupportedPids();
                            if (identifier != null) {
                                switch (identifier.hashCode()) {
                                    case -1158963060:
                                        if (identifier.equals(com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.MPG_AVG_WIDGET_ID)) {
                                            isGaugeEnabled = pidSet != null && pidSet.contains(256);
                                            break;
                                        }
                                        break;
                                    case -131933527:
                                        if (identifier.equals(com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.ENGINE_TEMPERATURE_GAUGE_ID)) {
                                            isGaugeEnabled = pidSet != null && pidSet.contains(5);
                                            break;
                                        }
                                        break;
                                    case 1168400042:
                                        if (identifier.equals(com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.FUEL_GAUGE_ID)) {
                                            isGaugeEnabled = pidSet != null && pidSet.contains(47);
                                            break;
                                        }
                                        break;
                                }
                            }
                            isGaugeEnabled = true;
                            if (isGaugeEnabled) {
                                subtitle = isGaugeOn ? Companion.getOnLabel() : Companion.getOffLabel();
                            } else {
                                subtitle = Companion.getUnavailableLabel();
                            }
                            Companion.getSideGaugesOptionsList().add(com.navdy.hud.app.ui.component.vlist.viewholder.SwitchViewHolder.Companion.buildModel(i, fluctuatorColor, title, subtitle, isGaugeOn, isGaugeEnabled));
                            if (i != widgetsCount) {
                                i++;
                            }
                        }
                    }
                }
                return Companion.getSideGaugesOptionsList();
            default:
                throw new kotlin.NoWhenBranchMatchedException();
        }
    }

    public int getInitialSelection() {
        return 1;
    }

    @org.jetbrains.annotations.Nullable
    public com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex getScrollIndex() {
        return null;
    }

    public void setBackSelectionPos(int n) {
        this.backSelection = n;
    }

    public void setBackSelectionId(int id) {
        this.backSelectionId = id;
    }

    public void setSelectedIcon() {
        switch (this.gaugeType) {
            case CENTER:
                this.vscrollComponent.setSelectedIconColorImage(com.navdy.hud.app.R.drawable.icon_center_gauge, Companion.getCenterGaugeBackgroundColor(), null, 1.0f);
                this.vscrollComponent.selectedText.setText(Companion.getCenterGaugeTitle());
                return;
            case SIDE:
                this.vscrollComponent.showSelectedCustomView();
                this.vscrollComponent.selectedText.setText(null);
                if (this.smartDashWidgetCache == null) {
                    getItems();
                }
                showGauge(1);
                return;
            default:
                return;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:0x00a2, code lost:
        if (r4 != null) goto L_0x00a4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00d4, code lost:
        if (r4 != null) goto L_0x00d6;
     */
    public boolean selectItem(@org.jetbrains.annotations.NotNull com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState selection) {
        java.lang.String str;
        java.lang.String str2;
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(selection, "selection");
        Companion.getSLogger().v("select id:" + selection.id + " pos:" + selection.pos);
        switch (this.gaugeType) {
            case CENTER:
                switch (selection.id) {
                    case com.navdy.hud.app.R.id.main_menu_options_speedometer /*2131623990*/:
                        this.presenter.close();
                        com.navdy.hud.app.ui.component.homescreen.SmartDashView smartDashView = Companion.getUiStateManager().getSmartDashView();
                        if (smartDashView != null) {
                            smartDashView.onSpeedoMeterSelected();
                            break;
                        }
                        break;
                    case com.navdy.hud.app.R.id.main_menu_options_tachometer /*2131623991*/:
                        this.presenter.close();
                        com.navdy.hud.app.ui.component.homescreen.SmartDashView smartDashView2 = Companion.getUiStateManager().getSmartDashView();
                        if (smartDashView2 != null) {
                            smartDashView2.onTachoMeterSelected();
                            break;
                        }
                        break;
                    case com.navdy.hud.app.R.id.menu_back /*2131624007*/:
                        this.presenter.loadMenu(this.parent, com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel.BACK_TO_PARENT, this.backSelection, 0);
                        break;
                }
            case SIDE:
                switch (selection.id) {
                    case com.navdy.hud.app.R.id.menu_back /*2131624007*/:
                        this.presenter.loadMenu(this.parent, com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel.BACK_TO_PARENT, this.backSelection, 0);
                        break;
                    default:
                        com.navdy.hud.app.ui.component.vlist.VerticalList.Model model = selection.model;
                        int id = selection.id;
                        com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.SmartDashWidgetCache smartDashWidgetCache2 = this.smartDashWidgetCache;
                        com.navdy.hud.app.view.DashboardWidgetPresenter widgetPresenter = smartDashWidgetCache2 != null ? smartDashWidgetCache2.getWidgetPresenter(id) : null;
                        if (model.isOn) {
                            model.isOn = false;
                            model.subTitle = Companion.getOffLabel();
                            com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager smartDashWidgetManager2 = this.smartDashWidgetManager;
                            if (widgetPresenter != null) {
                                str2 = widgetPresenter.getWidgetIdentifier();
                                break;
                            }
                            str2 = "";
                            smartDashWidgetManager2.setGaugeOn(str2, false);
                        } else {
                            model.isOn = true;
                            model.subTitle = Companion.getOnLabel();
                            com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager smartDashWidgetManager3 = this.smartDashWidgetManager;
                            if (widgetPresenter != null) {
                                str = widgetPresenter.getWidgetIdentifier();
                                break;
                            }
                            str = "";
                            smartDashWidgetManager3.setGaugeOn(str, true);
                        }
                        this.userPreferenceChanged = true;
                        this.presenter.refreshDataforPos(selection.pos);
                        break;
                }
        }
        return false;
    }

    @org.jetbrains.annotations.Nullable
    public com.navdy.hud.app.ui.component.mainmenu.IMenu.Menu getType() {
        return com.navdy.hud.app.ui.component.mainmenu.IMenu.Menu.MAIN_OPTIONS;
    }

    public boolean isItemClickable(int id, int pos) {
        return true;
    }

    @org.jetbrains.annotations.Nullable
    public com.navdy.hud.app.ui.component.vlist.VerticalList.Model getModelfromPos(int pos) {
        switch (this.gaugeType) {
            case CENTER:
                return (com.navdy.hud.app.ui.component.vlist.VerticalList.Model) Companion.getCenterGaugeOptionsList().get(pos);
            case SIDE:
                return (com.navdy.hud.app.ui.component.vlist.VerticalList.Model) Companion.getSideGaugesOptionsList().get(pos);
            default:
                throw new kotlin.NoWhenBranchMatchedException();
        }
    }

    public boolean isBindCallsEnabled() {
        return false;
    }

    public void onBindToView(@org.jetbrains.annotations.NotNull com.navdy.hud.app.ui.component.vlist.VerticalList.Model model, @org.jetbrains.annotations.NotNull android.view.View view, int pos, @org.jetbrains.annotations.NotNull com.navdy.hud.app.ui.component.vlist.VerticalList.ModelState state) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(model, "model");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(view, "view");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(state, com.amazonaws.mobileconnectors.s3.transferutility.TransferTable.COLUMN_STATE);
    }

    @org.jetbrains.annotations.Nullable
    public com.navdy.hud.app.ui.component.mainmenu.IMenu getChildMenu(@org.jetbrains.annotations.NotNull com.navdy.hud.app.ui.component.mainmenu.IMenu parent2, @org.jetbrains.annotations.NotNull java.lang.String args, @org.jetbrains.annotations.NotNull java.lang.String path) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(parent2, "parent");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(args, "args");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(path, "path");
        return null;
    }

    public void onUnload(@org.jetbrains.annotations.NotNull com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel level) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(level, "level");
        com.navdy.hud.app.view.DashboardWidgetView dashboardWidgetView = this.gaugeView;
        java.lang.Object obj = dashboardWidgetView != null ? dashboardWidgetView.getTag() : null;
        if (!(obj instanceof com.navdy.hud.app.view.DashboardWidgetPresenter)) {
            obj = null;
        }
        com.navdy.hud.app.view.DashboardWidgetPresenter dashboardWidgetPresenter = (com.navdy.hud.app.view.DashboardWidgetPresenter) obj;
        if (dashboardWidgetPresenter != null) {
            dashboardWidgetPresenter.setView(null, null);
        }
        if (this.userPreferenceChanged) {
            Companion.getSLogger().d("User preference has changed " + this.userPreferenceChanged);
            this.bus.post(new com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.UserPreferenceChanged());
        } else {
            Companion.getSLogger().d("User preference has not been changed " + this.userPreferenceChanged);
        }
        if (this.registered) {
            this.bus.unregister(this);
            this.registered = false;
        }
    }

    public void onItemSelected(@org.jetbrains.annotations.NotNull com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState selection) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(selection, "selection");
        if (kotlin.jvm.internal.Intrinsics.areEqual((java.lang.Object) this.gaugeType, (java.lang.Object) com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu.GaugeType.SIDE)) {
            showGauge(selection.pos);
        }
    }

    public final void showGauge(int pos) {
        com.navdy.hud.app.view.DashboardWidgetPresenter newPresenter;
        if (pos > 0) {
            this.vscrollComponent.showSelectedCustomView();
            this.vscrollComponent.selectedText.setText(null);
            java.lang.Object tag = this.gaugeView.getTag();
            if (!(tag instanceof com.navdy.hud.app.view.DashboardWidgetPresenter)) {
                tag = null;
            }
            com.navdy.hud.app.view.DashboardWidgetPresenter oldPresenter = (com.navdy.hud.app.view.DashboardWidgetPresenter) tag;
            com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.SmartDashWidgetCache smartDashWidgetCache2 = this.smartDashWidgetCache;
            if (smartDashWidgetCache2 != null) {
                newPresenter = smartDashWidgetCache2.getWidgetPresenter(pos - 1);
            } else {
                newPresenter = null;
            }
            if (!(oldPresenter == null || oldPresenter == this.presenter || oldPresenter.getWidgetView() != this.gaugeView)) {
                oldPresenter.setView(null, null);
            }
            if (newPresenter != null) {
                android.os.Bundle args = new android.os.Bundle();
                args.putInt(com.navdy.hud.app.view.DashboardWidgetPresenter.EXTRA_GRAVITY, 0);
                args.putBoolean(com.navdy.hud.app.view.DashboardWidgetPresenter.EXTRA_IS_ACTIVE, true);
                newPresenter.setView(this.gaugeView, args);
                newPresenter.setWidgetVisibleToUser(true);
            }
            this.gaugeView.setTag(newPresenter);
            return;
        }
        this.vscrollComponent.setSelectedIconColorImage(com.navdy.hud.app.R.drawable.icon_side_gauges, Companion.getCenterGaugeBackgroundColor(), null, 1.0f);
        this.vscrollComponent.selectedText.setText(Companion.getSideGaugesTitle());
    }

    @com.squareup.otto.Subscribe
    public final void onMapEvent(@org.jetbrains.annotations.NotNull com.navdy.hud.app.maps.MapEvents.ManeuverDisplay event) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(event, "event");
        this.smartDashWidgetManager.updateWidget(com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.SPEED_LIMIT_SIGN_GAUGE_ID, java.lang.Integer.valueOf(java.lang.Math.round((float) com.navdy.hud.app.manager.SpeedManager.convert((double) event.currentSpeedLimit, com.navdy.hud.app.manager.SpeedManager.SpeedUnit.METERS_PER_SECOND, com.navdy.hud.app.manager.SpeedManager.getInstance().getSpeedUnit()))));
    }

    @com.squareup.otto.Subscribe
    public final void ObdPidChangeEvent(@org.jetbrains.annotations.NotNull com.navdy.hud.app.obd.ObdManager.ObdPidChangeEvent event) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(event, "event");
        switch (event.pid.getId()) {
            case 5:
                this.smartDashWidgetManager.updateWidget(com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.ENGINE_TEMPERATURE_GAUGE_ID, java.lang.Double.valueOf(event.pid.getValue()));
                return;
            case 47:
                this.smartDashWidgetManager.updateWidget(com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.FUEL_GAUGE_ID, java.lang.Double.valueOf(event.pid.getValue()));
                return;
            case 256:
                this.smartDashWidgetManager.updateWidget(com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.MPG_AVG_WIDGET_ID, java.lang.Double.valueOf(event.pid.getValue()));
                return;
            default:
                return;
        }
    }

    @com.squareup.otto.Subscribe
    public final void onGpsLocationChanged(@org.jetbrains.annotations.NotNull android.location.Location location) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(location, "location");
        this.headingDataUtil.setHeading((double) location.getBearing());
        this.smartDashWidgetManager.updateWidget(com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.COMPASS_WIDGET_ID, java.lang.Double.valueOf(this.headingDataUtil.getHeading()));
    }

    @com.squareup.otto.Subscribe
    public final void onLocationFixChangeEvent(@org.jetbrains.annotations.NotNull com.navdy.hud.app.maps.MapEvents.LocationFix event) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(event, "event");
        if (!event.locationAvailable) {
            this.headingDataUtil.reset();
            this.smartDashWidgetManager.updateWidget(com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.COMPASS_WIDGET_ID, java.lang.Integer.valueOf(0));
        }
    }

    @com.squareup.otto.Subscribe
    public final void onDriverProfileChanged(@org.jetbrains.annotations.NotNull com.navdy.hud.app.event.DriverProfileChanged profileChanged) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(profileChanged, "profileChanged");
        Companion.getSLogger().d("onDriverProfileChanged, reloading the widgets");
        com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager smartDashWidgetManager2 = this.smartDashWidgetManager;
        if (smartDashWidgetManager2 != null) {
            smartDashWidgetManager2.reLoadAvailableWidgets(false);
        }
    }

    @com.squareup.otto.Subscribe
    public final void onDriveScoreUpdated(@org.jetbrains.annotations.NotNull com.navdy.hud.app.analytics.TelemetryDataManager.DriveScoreUpdated driveScoreUpdated) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(driveScoreUpdated, "driveScoreUpdated");
        this.smartDashWidgetManager.updateWidget(com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.DRIVE_SCORE_GAUGE_ID, driveScoreUpdated);
    }

    public void onScrollIdle() {
    }

    public void onFastScrollStart() {
    }

    public void onFastScrollEnd() {
    }

    public void showToolTip() {
    }

    public boolean isFirstItemEmpty() {
        return false;
    }
}
