package com.navdy.hud.app.ui.component.mainmenu;

public class MusicMenu2 implements com.navdy.hud.app.ui.component.mainmenu.IMenu {
    private static java.util.Map<com.navdy.service.library.events.audio.MusicCollectionType, java.lang.String> analyticsTypeStringMap = new java.util.HashMap();
    private static int androidBgColor = resources.getColor(com.navdy.hud.app.R.color.music_android_local);
    private static int appleMusicBgColor = resources.getColor(com.navdy.hud.app.R.color.music_apple_music);
    private static int applePodcastsBgColor = resources.getColor(com.navdy.hud.app.R.color.music_apple_podcasts);
    private static final java.lang.Object artworkRequestLock = new java.lang.Object();
    private static java.util.Stack<android.util.Pair<com.navdy.service.library.events.audio.MusicArtworkRequest, com.navdy.service.library.events.audio.MusicCollectionInfo>> artworkRequestQueue = new java.util.Stack<>();
    private static int artworkSize = resources.getDimensionPixelSize(com.navdy.hud.app.R.dimen.vmenu_selected_image);
    private static com.navdy.hud.app.ui.component.vlist.VerticalList.Model back = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.menu_back, com.navdy.hud.app.R.drawable.icon_mm_back, backColor, bgColorUnselected, backColor, resources.getString(com.navdy.hud.app.R.string.back), null);
    private static int backColor = resources.getColor(com.navdy.hud.app.R.color.mm_back);
    private static int bgColorUnselected = resources.getColor(com.navdy.hud.app.R.color.grey_4a);
    private static com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.BusDelegate busDelegate;
    private static com.navdy.service.library.events.audio.MusicCollectionInfo infoForCurrentArtworkRequest = null;
    private static java.util.Map<java.lang.String, com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.MusicMenuData> lastPlayedMusicMenuData = new java.util.HashMap();
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.class);
    private static java.util.Map<com.navdy.service.library.events.audio.MusicCollectionSource, java.lang.Integer> menuSourceIconMap = new java.util.HashMap();
    private static java.util.Map<com.navdy.service.library.events.audio.MusicCollectionSource, java.lang.String> menuSourceStringMap = new java.util.HashMap();
    private static java.util.Map<com.navdy.service.library.events.audio.MusicCollectionType, java.lang.Integer> menuTypeIconMap = new java.util.HashMap();
    private static java.util.Map<com.navdy.service.library.events.audio.MusicCollectionType, java.lang.String> menuTypeStringMap = new java.util.HashMap();
    private static int mmMusicColor = resources.getColor(com.navdy.hud.app.R.color.mm_music);
    private static android.content.res.Resources resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
    private static java.lang.String shuffle = resources.getString(com.navdy.hud.app.R.string.shuffle);
    private static java.lang.String shuffleOff = resources.getString(com.navdy.hud.app.R.string.shuffle_off);
    private static java.lang.String shuffleOn = resources.getString(com.navdy.hud.app.R.string.shuffle_on);
    private static java.lang.String shufflePlay = resources.getString(com.navdy.hud.app.R.string.shuffle_play);
    private static int sourceBgColor = resources.getColor(17170443);
    private static int transparentColor = resources.getColor(17170445);
    private boolean anyPlayersPermitted = true;
    private long artWorkRequestTime = 0;
    private int backSelection;
    private int backSelectionId;
    private int bgColorSelected;
    /* access modifiers changed from: private */
    public com.squareup.otto.Bus bus;
    /* access modifiers changed from: private */
    public java.util.List<com.navdy.hud.app.ui.component.vlist.VerticalList.Model> cachedList = null;
    private com.navdy.service.library.events.audio.MusicCollectionRequest currentMusicCollectionRequest;
    private long currentMusicCollectionRequestTime;
    private com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex fastScrollIndex;
    /* access modifiers changed from: private */
    public android.os.Handler handler = new android.os.Handler(android.os.Looper.getMainLooper());
    private int highlightedItem = 1;
    private boolean initialized = false;
    private boolean isRequestingMusicCollection = false;
    private boolean isShuffling = false;
    private final com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.IndexOffset loadingOffset = new com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.IndexOffset(null);
    /* access modifiers changed from: private */
    public com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.MusicMenuData menuData;
    private android.graphics.drawable.AnimationDrawable musicAnimation;
    @javax.inject.Inject
    com.navdy.hud.app.util.MusicArtworkCache musicArtworkCache;
    @javax.inject.Inject
    com.navdy.hud.app.storage.cache.MessageCache<com.navdy.service.library.events.audio.MusicCollectionResponse> musicCollectionResponseMessageCache;
    private boolean musicCollectionSyncComplete = false;
    /* access modifiers changed from: private */
    public final com.navdy.hud.app.manager.MusicManager musicManager;
    private int nextRequestLimit = -1;
    private int nextRequestOffset = -1;
    private int nowPlayingTrackPosition;
    private com.navdy.hud.app.ui.component.mainmenu.IMenu parentMenu;
    private boolean partialRefresh;
    private java.lang.String path = null;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.Presenter presenter;
    private int requestMusicOffset = -1;
    private java.util.Map<java.lang.String, java.lang.Integer> requestedArtworkModelPositions = new java.util.HashMap();
    private java.util.List<com.navdy.hud.app.ui.component.vlist.VerticalList.Model> returnToCacheList = null;
    private java.util.Map<java.lang.String, java.lang.Integer> trackIdToPositionMap = null;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent vmenuComponent;

    class Anon1 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.service.library.events.audio.MusicCollectionRequest val$musicCollectionRequest;

        Anon1(com.navdy.service.library.events.audio.MusicCollectionRequest musicCollectionRequest) {
            this.val$musicCollectionRequest = musicCollectionRequest;
        }

        public void run() {
            java.lang.String cacheKeyForRequest = com.navdy.hud.app.ExtensionsKt.cacheKey(this.val$musicCollectionRequest);
            com.navdy.service.library.events.audio.MusicCollectionResponse response = (com.navdy.service.library.events.audio.MusicCollectionResponse) com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.this.musicCollectionResponseMessageCache.get(cacheKeyForRequest);
            if (response != null) {
                com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.logger.d("Cache hit for MusicCollectionRequest " + this.val$musicCollectionRequest + ", Key :" + cacheKeyForRequest);
                com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.this.bus.post(response);
                return;
            }
            com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.logger.d("Cache miss for MusicCollectionRequest " + this.val$musicCollectionRequest + ", Key :" + cacheKeyForRequest);
            com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.this.bus.post(new com.navdy.hud.app.event.RemoteEvent(this.val$musicCollectionRequest));
        }
    }

    class Anon2 implements com.navdy.hud.app.util.MusicArtworkCache.Callback {
        Anon2() {
        }

        public void onHit(@android.support.annotation.NonNull byte[] data) {
            com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.this.handleArtwork(data, null, com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.this.collectionIdString(com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.this.menuData.musicCollectionInfo), false);
        }

        public void onMiss() {
            com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.this.postOrQueueArtworkRequest(new com.navdy.service.library.events.audio.MusicArtworkRequest.Builder().collectionSource(com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.this.menuData.musicCollectionInfo.collectionSource).collectionType(com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.this.menuData.musicCollectionInfo.collectionType).collectionId(com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.this.menuData.musicCollectionInfo.collectionId).size(java.lang.Integer.valueOf(200)).build(), com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.this.menuData.musicCollectionInfo);
        }
    }

    class Anon3 implements java.lang.Runnable {
        Anon3() {
        }

        public void run() {
            com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.this.presenter.reset();
            com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.this.vmenuComponent.verticalList.unlock();
        }
    }

    class Anon4 implements com.navdy.hud.app.ui.framework.IScreenAnimationListener {
        final /* synthetic */ com.navdy.hud.app.ui.framework.UIStateManager val$uiStateManager;

        class Anon1 implements java.lang.Runnable {
            final /* synthetic */ com.navdy.hud.app.ui.framework.IScreenAnimationListener val$listener;

            Anon1(com.navdy.hud.app.ui.framework.IScreenAnimationListener iScreenAnimationListener) {
                this.val$listener = iScreenAnimationListener;
            }

            public void run() {
                com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.Anon4.this.val$uiStateManager.removeScreenAnimationListener(this.val$listener);
            }
        }

        Anon4(com.navdy.hud.app.ui.framework.UIStateManager uIStateManager) {
            this.val$uiStateManager = uIStateManager;
        }

        public void onStart(com.navdy.hud.app.screen.BaseScreen in, com.navdy.hud.app.screen.BaseScreen out) {
        }

        public void onStop(com.navdy.hud.app.screen.BaseScreen in, com.navdy.hud.app.screen.BaseScreen out) {
            com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.this.musicManager.showMusicNotification();
            com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.this.handler.post(new com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.Anon4.Anon1(this));
        }
    }

    class Anon5 implements com.navdy.hud.app.util.MusicArtworkCache.Callback {
        final /* synthetic */ com.navdy.service.library.events.audio.MusicCollectionInfo val$collectionInfo;
        final /* synthetic */ int val$pos;

        Anon5(int i, com.navdy.service.library.events.audio.MusicCollectionInfo musicCollectionInfo) {
            this.val$pos = i;
            this.val$collectionInfo = musicCollectionInfo;
        }

        public void onHit(@android.support.annotation.NonNull byte[] data) {
            com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.this.handleArtwork(data, java.lang.Integer.valueOf(this.val$pos), com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.this.collectionIdString(this.val$collectionInfo), false);
        }

        public void onMiss() {
            com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.logger.i("Requesting artwork for collection: " + this.val$collectionInfo);
            com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.this.postOrQueueArtworkRequest(new com.navdy.service.library.events.audio.MusicArtworkRequest.Builder().collectionSource(this.val$collectionInfo.collectionSource).collectionType(this.val$collectionInfo.collectionType).collectionId(this.val$collectionInfo.collectionId).size(java.lang.Integer.valueOf(200)).build(), this.val$collectionInfo);
        }
    }

    class Anon6 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.service.library.events.audio.MusicArtworkResponse val$artworkResponse;
        final /* synthetic */ java.lang.String val$idString;
        final /* synthetic */ java.lang.Integer val$pos;

        Anon6(com.navdy.service.library.events.audio.MusicArtworkResponse musicArtworkResponse, java.lang.Integer num, java.lang.String str) {
            this.val$artworkResponse = musicArtworkResponse;
            this.val$pos = num;
            this.val$idString = str;
        }

        public void run() {
            byte[] byteArray = this.val$artworkResponse.photo.toByteArray();
            if (byteArray == null || byteArray.length == 0) {
                com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.logger.w("Received photo has null or empty byte array");
            } else {
                com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.this.handleArtwork(byteArray, this.val$pos, this.val$idString, true);
            }
        }
    }

    class Anon7 implements java.lang.Runnable {
        final /* synthetic */ android.graphics.Bitmap val$artwork;
        final /* synthetic */ byte[] val$byteArray;
        final /* synthetic */ java.lang.String val$idString;
        final /* synthetic */ java.lang.Integer val$pos;
        final /* synthetic */ boolean val$saveInDiskCache;

        Anon7(java.lang.String str, android.graphics.Bitmap bitmap, java.lang.Integer num, boolean z, byte[] bArr) {
            this.val$idString = str;
            this.val$artwork = bitmap;
            this.val$pos = num;
            this.val$saveInDiskCache = z;
            this.val$byteArray = bArr;
        }

        public void run() {
            com.navdy.service.library.events.audio.MusicCollectionInfo info;
            com.navdy.hud.app.util.picasso.PicassoUtil.setBitmapInCache(this.val$idString, this.val$artwork);
            if (this.val$pos != null) {
                if (com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.this.presenter.getCurrentMenu() == com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.this) {
                    com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.logger.d("Refreshing artwork for position: " + this.val$pos);
                    com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.this.presenter.refreshDataforPos(this.val$pos.intValue());
                }
                int size = com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.this.cachedList.size();
                if (this.val$pos.intValue() < 0 || this.val$pos.intValue() >= size) {
                    com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.logger.e("Refreshing artwork invalid index: " + this.val$pos + " size=" + size);
                    return;
                }
                java.lang.Object state = ((com.navdy.hud.app.ui.component.vlist.VerticalList.Model) com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.this.cachedList.get(this.val$pos.intValue())).state;
                if (state instanceof com.navdy.service.library.events.audio.MusicCollectionInfo) {
                    info = (com.navdy.service.library.events.audio.MusicCollectionInfo) state;
                } else {
                    com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.logger.e("Something has gone terribly wrong.");
                    return;
                }
            } else {
                com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.this.vmenuComponent.setSelectedIconImage(this.val$artwork);
                info = com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.this.menuData.musicCollectionInfo;
            }
            if (this.val$saveInDiskCache && info != null && info.collectionType != null) {
                com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.logger.d("Setting bitmap in cache for collection: " + info);
                com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.this.musicArtworkCache.putArtwork(info, this.val$byteArray);
            }
        }
    }

    private static class BusDelegate implements com.navdy.hud.app.manager.MusicManager.MusicUpdateListener {
        com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.Presenter presenter;

        BusDelegate(com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.Presenter presenter2) {
            this.presenter = presenter2;
        }

        @com.squareup.otto.Subscribe
        public void onMusicCollectionResponse(com.navdy.service.library.events.audio.MusicCollectionResponse musicCollectionResponse) {
            com.navdy.hud.app.ui.component.mainmenu.IMenu menu = this.presenter.getCurrentMenu();
            if (menu != null && menu.getType() == com.navdy.hud.app.ui.component.mainmenu.IMenu.Menu.MUSIC) {
                ((com.navdy.hud.app.ui.component.mainmenu.MusicMenu2) menu).onMusicCollectionResponse(musicCollectionResponse);
            }
        }

        @com.squareup.otto.Subscribe
        public void onMusicArtworkResponse(com.navdy.service.library.events.audio.MusicArtworkResponse musicArtworkResponse) {
            com.navdy.hud.app.ui.component.mainmenu.IMenu menu = this.presenter.getCurrentMenu();
            if (menu != null && menu.getType() == com.navdy.hud.app.ui.component.mainmenu.IMenu.Menu.MUSIC) {
                ((com.navdy.hud.app.ui.component.mainmenu.MusicMenu2) menu).onMusicArtworkResponse(musicArtworkResponse);
            }
        }

        public void onAlbumArtUpdate(@android.support.annotation.Nullable okio.ByteString artwork, boolean animate) {
        }

        public void onTrackUpdated(com.navdy.service.library.events.audio.MusicTrackInfo trackInfo, java.util.Set<com.navdy.hud.app.manager.MusicManager.MediaControl> set, boolean willOpenNotification) {
            com.navdy.hud.app.ui.component.mainmenu.IMenu menu = this.presenter.getCurrentMenu();
            if (menu != null && menu.getType() == com.navdy.hud.app.ui.component.mainmenu.IMenu.Menu.MUSIC) {
                ((com.navdy.hud.app.ui.component.mainmenu.MusicMenu2) menu).onTrackUpdated(trackInfo);
            }
        }
    }

    private static class IndexOffset {
        int limit;
        int offset;

        private IndexOffset() {
        }

        /* synthetic */ IndexOffset(com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.Anon1 x0) {
            this();
        }

        /* access modifiers changed from: 0000 */
        public void clear() {
            this.offset = 0;
            this.limit = -1;
        }
    }

    private class MusicMenuData {
        com.navdy.service.library.events.audio.MusicCollectionInfo musicCollectionInfo;
        java.util.List<com.navdy.service.library.events.audio.MusicCollectionInfo> musicCollections = null;
        java.util.List<com.navdy.service.library.events.audio.MusicCharacterMap> musicIndex = null;
        java.util.List<com.navdy.service.library.events.audio.MusicTrackInfo> musicTracks = null;

        MusicMenuData() {
        }

        MusicMenuData(com.navdy.service.library.events.audio.MusicCollectionInfo musicCollectionInfo2, java.util.List<com.navdy.service.library.events.audio.MusicCollectionInfo> musicCollections2) {
            this.musicCollectionInfo = musicCollectionInfo2;
            this.musicCollections = musicCollections2;
        }
    }

    static {
        menuSourceStringMap.put(com.navdy.service.library.events.audio.MusicCollectionSource.COLLECTION_SOURCE_ANDROID_LOCAL, resources.getString(com.navdy.hud.app.R.string.local_music));
        menuSourceStringMap.put(com.navdy.service.library.events.audio.MusicCollectionSource.COLLECTION_SOURCE_IOS_MEDIA_PLAYER, resources.getString(com.navdy.hud.app.R.string.music_library));
        menuSourceIconMap.put(com.navdy.service.library.events.audio.MusicCollectionSource.COLLECTION_SOURCE_ANDROID_LOCAL, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_google_pm));
        menuSourceIconMap.put(com.navdy.service.library.events.audio.MusicCollectionSource.COLLECTION_SOURCE_IOS_MEDIA_PLAYER, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_apple_music));
        menuTypeStringMap.put(com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_ALBUMS, resources.getString(com.navdy.hud.app.R.string.albums));
        menuTypeStringMap.put(com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_ARTISTS, resources.getString(com.navdy.hud.app.R.string.artists));
        menuTypeStringMap.put(com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_PLAYLISTS, resources.getString(com.navdy.hud.app.R.string.playlists));
        menuTypeStringMap.put(com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_PODCASTS, resources.getString(com.navdy.hud.app.R.string.podcasts));
        menuTypeIconMap.put(com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_ALBUMS, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_album));
        menuTypeIconMap.put(com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_ARTISTS, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_artist));
        menuTypeIconMap.put(com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_PLAYLISTS, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_playlist));
        menuTypeIconMap.put(com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_PODCASTS, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_podcasts));
        analyticsTypeStringMap.put(com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_ALBUMS, "Album");
        analyticsTypeStringMap.put(com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_ARTISTS, "Artist");
        analyticsTypeStringMap.put(com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_PLAYLISTS, "Playlist");
        analyticsTypeStringMap.put(com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_PODCASTS, "Podcast");
    }

    /* access modifiers changed from: private */
    public void onTrackUpdated(com.navdy.service.library.events.audio.MusicTrackInfo musicTrackInfo) {
        if (!isThisQueuePlaying()) {
            logger.w("This queue isn't playing");
            return;
        }
        java.lang.String trackId = musicTrackInfo.trackId;
        if (!(this.trackIdToPositionMap == null || trackId == null || !this.trackIdToPositionMap.containsKey(trackId))) {
            java.lang.Integer pos = (java.lang.Integer) this.trackIdToPositionMap.get(trackId);
            if (this.nowPlayingTrackPosition > 0 && this.nowPlayingTrackPosition != pos.intValue()) {
                stopAudioAnimation();
                this.presenter.refreshDataforPos(this.nowPlayingTrackPosition);
            }
            this.presenter.refreshDataforPos(pos.intValue());
            this.nowPlayingTrackPosition = pos.intValue();
        }
        boolean shuffle2 = this.musicManager.isShuffling();
        if (this.isShuffling != shuffle2) {
            this.isShuffling = shuffle2;
            this.vmenuComponent.verticalList.refreshData(1, getShuffleModel());
        }
    }

    MusicMenu2(com.squareup.otto.Bus bus2, com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent vmenuComponent2, com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.Presenter presenter2, com.navdy.hud.app.ui.component.mainmenu.IMenu parentMenu2, com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.MusicMenuData menuData2) {
        this.bus = bus2;
        this.vmenuComponent = vmenuComponent2;
        this.presenter = presenter2;
        this.parentMenu = parentMenu2;
        if (menuData2 != null) {
            this.menuData = menuData2;
        } else {
            this.menuData = new com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.MusicMenuData();
        }
        mortar.Mortar.inject(com.navdy.hud.app.HudApplication.getAppContext(), this);
        logger.d("MusicMenu " + this.menuData.musicCollectionInfo);
        this.musicManager = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getMusicManager();
    }

    public java.util.List<com.navdy.hud.app.ui.component.vlist.VerticalList.Model> getItems() {
        int icon;
        java.lang.String title;
        java.lang.String subtitle;
        logger.v("getItems " + getPath());
        setupMenu();
        java.util.ArrayList arrayList = new java.util.ArrayList();
        this.returnToCacheList = new java.util.ArrayList();
        if (!this.musicManager.hasMusicCapabilities()) {
            logger.e("Navigated to Music menu with no music capabilities");
            arrayList.add(back);
        } else if (!this.anyPlayersPermitted) {
            if (com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getRemoteDevicePlatform() == com.navdy.service.library.events.DeviceInfo.Platform.PLATFORM_iOS) {
                title = resources.getString(com.navdy.hud.app.R.string.music_enable_permissions_title_ios);
                subtitle = resources.getString(com.navdy.hud.app.R.string.music_enable_permissions_subtitle_ios);
            } else {
                title = resources.getString(com.navdy.hud.app.R.string.music_enable_permissions_title_android);
                subtitle = resources.getString(com.navdy.hud.app.R.string.music_enable_permissions_subtitle_android);
            }
            com.navdy.hud.app.ui.component.vlist.VerticalList.Model emptyBack = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.menu_back, com.navdy.hud.app.R.drawable.icon_mm_back, backColor, bgColorUnselected, backColor, title, subtitle);
            arrayList.add(emptyBack);
            this.returnToCacheList.add(emptyBack);
        } else if (this.menuData.musicCollectionInfo.size == null || this.menuData.musicCollectionInfo.size.intValue() != 0) {
            arrayList.add(back);
            if (this.menuData.musicCollectionInfo.collectionSource == null) {
                if (busDelegate == null && this.parentMenu.getType() == com.navdy.hud.app.ui.component.mainmenu.IMenu.Menu.MAIN) {
                    logger.v("bus-register getItems");
                    busDelegate = new com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.BusDelegate(this.presenter);
                    this.bus.register(busDelegate);
                    this.presenter.setScrollIdleEvents(true);
                }
                for (int i = 0; i < this.menuData.musicCollections.size(); i++) {
                    com.navdy.service.library.events.audio.MusicCollectionInfo collection = (com.navdy.service.library.events.audio.MusicCollectionInfo) this.menuData.musicCollections.get(i);
                    com.navdy.hud.app.ui.component.vlist.VerticalList.Model model = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(i, ((java.lang.Integer) menuSourceIconMap.get(collection.collectionSource)).intValue(), sourceBgColor, bgColorUnselected, sourceBgColor, (java.lang.String) menuSourceStringMap.get(collection.collectionSource), null);
                    model.state = collection;
                    arrayList.add(model);
                    this.returnToCacheList.add(model);
                    java.util.List<com.navdy.service.library.events.audio.MusicCollectionType> collectionTypes = this.musicManager.getCollectionTypesForSource(collection.collectionSource);
                    if (collection.collectionSource == com.navdy.service.library.events.audio.MusicCollectionSource.COLLECTION_SOURCE_IOS_MEDIA_PLAYER) {
                        if (collectionTypes.contains(com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_PODCASTS)) {
                            com.navdy.hud.app.ui.component.vlist.VerticalList.Model podcastsModel = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.apple_podcasts, ((java.lang.Integer) menuTypeIconMap.get(com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_PODCASTS)).intValue(), applePodcastsBgColor, bgColorUnselected, applePodcastsBgColor, resources.getString(com.navdy.hud.app.R.string.apple_podcasts), null);
                            podcastsModel.state = com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_PODCASTS;
                            arrayList.add(podcastsModel);
                            this.returnToCacheList.add(podcastsModel);
                        }
                    }
                }
            } else if (this.menuData.musicCollectionInfo.collectionType == null) {
                for (int i2 = 0; i2 < this.menuData.musicCollections.size(); i2++) {
                    com.navdy.service.library.events.audio.MusicCollectionType collectionType = ((com.navdy.service.library.events.audio.MusicCollectionInfo) this.menuData.musicCollections.get(i2)).collectionType;
                    if (this.menuData.musicCollectionInfo.collectionSource != com.navdy.service.library.events.audio.MusicCollectionSource.COLLECTION_SOURCE_IOS_MEDIA_PLAYER || collectionType != com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_PODCASTS) {
                        com.navdy.hud.app.ui.component.vlist.VerticalList.Model model2 = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(i2, ((java.lang.Integer) menuTypeIconMap.get(collectionType)).intValue(), this.bgColorSelected, bgColorUnselected, this.bgColorSelected, (java.lang.String) menuTypeStringMap.get(collectionType), null);
                        model2.state = collectionType;
                        arrayList.add(model2);
                        this.returnToCacheList.add(model2);
                    }
                }
            } else if (this.menuData.musicCollections == null && this.menuData.musicTracks == null) {
                arrayList.add(com.navdy.hud.app.ui.component.vlist.viewholder.LoadingViewHolder.buildModel());
                requestMusicCollection(0, -1, isIndexSupported(this.menuData.musicCollectionInfo));
            } else if (this.menuData.musicCollections != null && this.menuData.musicCollections.size() > 0) {
                logger.i("Loading music collections for type " + this.backSelection);
                if (this.menuData.musicIndex != null && this.fastScrollIndex == null) {
                    this.fastScrollIndex = checkCharacterMap(this.menuData.musicIndex, 0, 0, false);
                }
                java.lang.Integer collectionIcon = null;
                if (!(this.menuData.musicCollectionInfo == null || this.menuData.musicCollectionInfo.collectionType == null)) {
                    collectionIcon = (java.lang.Integer) menuTypeIconMap.get(this.menuData.musicCollectionInfo.collectionType);
                }
                if (collectionIcon == null) {
                    collectionIcon = java.lang.Integer.valueOf(0);
                }
                if (this.menuData.musicCollectionInfo.collectionType == com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_ARTISTS && !android.text.TextUtils.isEmpty(this.menuData.musicCollectionInfo.collectionId) && ((java.lang.Boolean) com.squareup.wire.Wire.get(this.menuData.musicCollectionInfo.canShuffle, java.lang.Boolean.valueOf(false))).booleanValue()) {
                    arrayList.add(getShuffleModel());
                    this.highlightedItem = 2;
                }
                for (int i3 = 0; i3 < this.menuData.musicCollections.size(); i3++) {
                    com.navdy.service.library.events.audio.MusicCollectionInfo collectionInfo = (com.navdy.service.library.events.audio.MusicCollectionInfo) this.menuData.musicCollections.get(i3);
                    if (collectionInfo == null) {
                        com.navdy.hud.app.ui.component.vlist.VerticalList.Model contentPlaceHolder = com.navdy.hud.app.ui.component.vlist.viewholder.ContentLoadingViewHolder.buildModel(collectionIcon.intValue(), this.bgColorSelected, bgColorUnselected);
                        arrayList.add(contentPlaceHolder);
                        this.returnToCacheList.add(contentPlaceHolder);
                    } else {
                        com.navdy.hud.app.ui.component.vlist.VerticalList.Model model3 = buildModelfromCollectionInfo(i3, collectionInfo);
                        arrayList.add(model3);
                        this.returnToCacheList.add(model3);
                    }
                }
            } else if (this.menuData.musicTracks == null || this.menuData.musicTracks.size() <= 0) {
                logger.e("No collections or tracks in this collection...");
            } else {
                if (this.trackIdToPositionMap == null) {
                    this.trackIdToPositionMap = new java.util.HashMap();
                }
                if (this.menuData.musicCollectionInfo.collectionType == com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_PODCASTS) {
                    icon = com.navdy.hud.app.R.drawable.icon_play_music;
                } else {
                    icon = com.navdy.hud.app.R.drawable.icon_song;
                    if (this.menuData.musicTracks.size() > 1) {
                        arrayList.add(getShuffleModel());
                        if (this.menuData.musicCollectionInfo.collectionType != com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_PLAYLISTS) {
                            this.highlightedItem = 2;
                        }
                    }
                }
                for (int i4 = 0; i4 < this.menuData.musicTracks.size(); i4++) {
                    com.navdy.service.library.events.audio.MusicTrackInfo musicTrackInfo = (com.navdy.service.library.events.audio.MusicTrackInfo) this.menuData.musicTracks.get(i4);
                    if (musicTrackInfo == null) {
                        com.navdy.hud.app.ui.component.vlist.VerticalList.Model contentPlaceHolder2 = com.navdy.hud.app.ui.component.vlist.viewholder.ContentLoadingViewHolder.buildModel(icon, this.bgColorSelected, bgColorUnselected);
                        arrayList.add(contentPlaceHolder2);
                        this.returnToCacheList.add(contentPlaceHolder2);
                    } else {
                        com.navdy.hud.app.ui.component.vlist.VerticalList.Model model4 = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(i4, icon, this.bgColorSelected, bgColorUnselected, this.bgColorSelected, musicTrackInfo.name, musicTrackInfo.author);
                        model4.state = musicTrackInfo;
                        arrayList.add(model4);
                        this.returnToCacheList.add(model4);
                        this.trackIdToPositionMap.put(musicTrackInfo.trackId, java.lang.Integer.valueOf(arrayList.size() - 1));
                    }
                }
                if (this.menuData.musicTracks.size() < this.menuData.musicCollectionInfo.size.intValue()) {
                    arrayList.add(com.navdy.hud.app.ui.component.vlist.viewholder.LoadingViewHolder.buildModel());
                }
            }
            this.cachedList = arrayList;
        } else {
            com.navdy.hud.app.ui.component.vlist.VerticalList.Model emptyBack2 = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.menu_back, com.navdy.hud.app.R.drawable.icon_mm_back, backColor, bgColorUnselected, backColor, resources.getString(com.navdy.hud.app.R.string.back), resources.getString(com.navdy.hud.app.R.string.empty_queue, new java.lang.Object[]{menuTypeStringMap.get(this.menuData.musicCollectionInfo.collectionType)}));
            arrayList.add(emptyBack2);
            this.returnToCacheList.add(emptyBack2);
        }
        return arrayList;
    }

    private com.navdy.hud.app.ui.component.vlist.VerticalList.Model getShuffleModel() {
        java.lang.String shuffleTitle;
        java.lang.String shuffleSubtitle = null;
        if (isThisQueuePlaying()) {
            shuffleTitle = shuffle;
            if (this.musicManager.isShuffling()) {
                shuffleSubtitle = shuffleOn;
                this.isShuffling = true;
            } else {
                shuffleSubtitle = shuffleOff;
            }
        } else {
            shuffleTitle = shufflePlay;
        }
        return com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.music_browser_shuffle, com.navdy.hud.app.R.drawable.icon_shuffle, this.bgColorSelected, bgColorUnselected, this.bgColorSelected, shuffleTitle, shuffleSubtitle);
    }

    private void setupMenu() {
        if (busDelegate == null) {
            logger.v("bus-register ctor");
            busDelegate = new com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.BusDelegate(this.presenter);
            this.bus.register(busDelegate);
            this.presenter.setScrollIdleEvents(true);
            this.musicManager.addMusicUpdateListener(busDelegate);
        }
        if (!this.initialized) {
            this.initialized = true;
            if (!this.musicManager.hasMusicCapabilities()) {
                logger.e("Navigated to Music menu with no music capabilities");
                return;
            }
            if (this.menuData.musicCollectionInfo == null) {
                java.util.List<com.navdy.service.library.events.audio.MusicCapability> capabilities = this.musicManager.getMusicCapabilities().capabilities;
                this.anyPlayersPermitted = false;
                java.util.List<com.navdy.service.library.events.audio.MusicCollectionSource> musicCollectionSources = new java.util.ArrayList<>(capabilities.size());
                for (com.navdy.service.library.events.audio.MusicCapability musicCapability : capabilities) {
                    if (musicCapability.authorizationStatus == com.navdy.service.library.events.audio.MusicCapability.MusicAuthorizationStatus.MUSIC_AUTHORIZATION_AUTHORIZED) {
                        this.anyPlayersPermitted = true;
                        musicCollectionSources.add(musicCapability.collectionSource);
                    }
                }
                if (capabilities.size() == 1 && ((com.navdy.service.library.events.audio.MusicCapability) capabilities.get(0)).collectionSource == com.navdy.service.library.events.audio.MusicCollectionSource.COLLECTION_SOURCE_ANDROID_LOCAL) {
                    this.menuData.musicCollectionInfo = new com.navdy.service.library.events.audio.MusicCollectionInfo.Builder().collectionSource(com.navdy.service.library.events.audio.MusicCollectionSource.COLLECTION_SOURCE_ANDROID_LOCAL).build();
                    java.util.List<com.navdy.service.library.events.audio.MusicCollectionType> collectionTypes = this.musicManager.getCollectionTypesForSource(com.navdy.service.library.events.audio.MusicCollectionSource.COLLECTION_SOURCE_ANDROID_LOCAL);
                    this.menuData.musicCollections = new java.util.ArrayList(collectionTypes.size());
                    for (com.navdy.service.library.events.audio.MusicCollectionType collectionType : collectionTypes) {
                        this.menuData.musicCollections.add(new com.navdy.service.library.events.audio.MusicCollectionInfo.Builder().collectionSource(com.navdy.service.library.events.audio.MusicCollectionSource.COLLECTION_SOURCE_ANDROID_LOCAL).collectionType(collectionType).build());
                    }
                } else {
                    this.menuData.musicCollectionInfo = new com.navdy.service.library.events.audio.MusicCollectionInfo.Builder().build();
                    this.menuData.musicCollections = new java.util.ArrayList(musicCollectionSources.size());
                    for (com.navdy.service.library.events.audio.MusicCollectionSource collectionSource : musicCollectionSources) {
                        this.menuData.musicCollections.add(new com.navdy.service.library.events.audio.MusicCollectionInfo.Builder().collectionSource(collectionSource).build());
                    }
                }
            }
            setupIconBgColor();
        }
    }

    private void requestMusicCollection(int offset, int limit, boolean askForIndex) {
        logger.d("requestMusicCollection Offset : " + offset + ", Limit : " + limit + ", AFI : " + askForIndex);
        if (this.musicCollectionSyncComplete) {
            logger.i("sync is complete");
        } else if (this.isRequestingMusicCollection) {
            logger.i("Request already in process next=" + offset + " limit:" + limit);
            this.nextRequestOffset = offset;
            this.nextRequestLimit = limit;
        } else {
            this.nextRequestOffset = -1;
            this.nextRequestLimit = -1;
            this.isRequestingMusicCollection = true;
            this.requestMusicOffset = offset;
            if (limit == -1) {
                limit = 25;
            }
            logger.i("request music =" + offset + " limit:" + limit);
            if (this.menuData.musicCollections == null && this.menuData.musicTracks == null) {
                limit = 50;
            }
            com.navdy.service.library.events.audio.MusicCollectionRequest.Builder builder = new com.navdy.service.library.events.audio.MusicCollectionRequest.Builder().collectionSource(this.menuData.musicCollectionInfo.collectionSource).collectionType(this.menuData.musicCollectionInfo.collectionType).collectionId(this.menuData.musicCollectionInfo.collectionId).offset(java.lang.Integer.valueOf(offset)).limit(java.lang.Integer.valueOf(limit));
            if (this.menuData.musicCollectionInfo.collectionType == com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_ARTISTS) {
                builder.groupBy(com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_ALBUMS);
            }
            if (askForIndex) {
                builder.includeCharacterMap(java.lang.Boolean.valueOf(true));
            }
            com.navdy.service.library.events.audio.MusicCollectionRequest musicCollectionRequest = builder.build();
            this.currentMusicCollectionRequest = musicCollectionRequest;
            this.currentMusicCollectionRequestTime = android.os.SystemClock.elapsedRealtime();
            logger.i("got-asked offset=" + offset + " limit=" + limit);
            logger.d("[Timing] MusicCollectionRequest " + musicCollectionRequest);
            if (this.musicManager.isMusicLibraryCachingEnabled()) {
                com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.Anon1(musicCollectionRequest), 1);
            } else {
                this.bus.post(new com.navdy.hud.app.event.RemoteEvent(musicCollectionRequest));
            }
        }
    }

    /* access modifiers changed from: private */
    public void postOrQueueArtworkRequest(com.navdy.service.library.events.audio.MusicArtworkRequest artworkRequest, com.navdy.service.library.events.audio.MusicCollectionInfo collectionInfo) {
        synchronized (artworkRequestLock) {
            if (!artworkRequestQueue.isEmpty() || infoForCurrentArtworkRequest != null) {
                logger.d("REQUESTQUEUE postOrQueueArtworkRequest queueing: " + artworkRequest + " title:" + collectionInfo.name);
                artworkRequestQueue.push(new android.util.Pair(artworkRequest, collectionInfo));
            } else {
                logger.d("REQUESTQUEUE postOrQueueArtworkRequest posting: " + artworkRequest + " title:" + collectionInfo.name);
                infoForCurrentArtworkRequest = collectionInfo;
                this.artWorkRequestTime = android.os.SystemClock.elapsedRealtime();
                logger.d("[Timing] Making MusicArtWorkRequest " + artworkRequest);
                this.bus.post(new com.navdy.hud.app.event.RemoteEvent(artworkRequest));
            }
        }
    }

    private void purgeArtworkQueue() {
        synchronized (artworkRequestLock) {
            int size = artworkRequestQueue.size();
            artworkRequestQueue.clear();
            logger.d("REQUESTQUEUE purged: " + size);
        }
    }

    public int getInitialSelection() {
        logger.i("getInitialSelection " + this.highlightedItem);
        return this.highlightedItem;
    }

    public com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex getScrollIndex() {
        return this.fastScrollIndex;
    }

    public void setBackSelectionPos(int n) {
        this.backSelection = n;
    }

    public void setBackSelectionId(int id) {
        logger.i("setBackSelectionId: " + id);
        this.backSelectionId = id;
    }

    public void setSelectedIcon() {
        logger.i("setSelectedIcon");
        setupMenu();
        if (this.parentMenu.getType() == com.navdy.hud.app.ui.component.mainmenu.IMenu.Menu.MAIN) {
            this.vmenuComponent.setSelectedIconColorImage(com.navdy.hud.app.R.drawable.icon_mm_music_2, mmMusicColor, null, 1.0f);
            this.vmenuComponent.selectedText.setText(com.navdy.hud.app.R.string.music);
        } else if (this.menuData.musicCollectionInfo.collectionType == null) {
            this.vmenuComponent.setSelectedIconColorImage(((java.lang.Integer) menuSourceIconMap.get(this.menuData.musicCollectionInfo.collectionSource)).intValue(), sourceBgColor, null, 1.0f);
            this.vmenuComponent.selectedText.setText((java.lang.CharSequence) menuSourceStringMap.get(this.menuData.musicCollectionInfo.collectionSource));
        } else if (android.text.TextUtils.isEmpty(this.menuData.musicCollectionInfo.collectionId)) {
            this.vmenuComponent.setSelectedIconColorImage(((java.lang.Integer) menuTypeIconMap.get(this.menuData.musicCollectionInfo.collectionType)).intValue(), this.bgColorSelected, null, 1.0f);
            this.vmenuComponent.selectedText.setText((java.lang.CharSequence) menuTypeStringMap.get(this.menuData.musicCollectionInfo.collectionType));
        } else {
            android.graphics.Bitmap artwork = com.navdy.hud.app.util.picasso.PicassoUtil.getBitmapfromCache(collectionIdString(this.menuData.musicCollectionInfo));
            if (artwork != null) {
                this.vmenuComponent.setSelectedIconImage(artwork);
            } else {
                this.vmenuComponent.setSelectedIconColorImage(((java.lang.Integer) menuTypeIconMap.get(this.menuData.musicCollectionInfo.collectionType)).intValue(), this.bgColorSelected, null, 1.0f, this.menuData.musicCollectionInfo.collectionType == com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_ARTISTS ? com.navdy.hud.app.ui.component.image.IconColorImageView.IconShape.CIRCLE : com.navdy.hud.app.ui.component.image.IconColorImageView.IconShape.SQUARE);
                this.musicArtworkCache.getArtwork(this.menuData.musicCollectionInfo, new com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.Anon2());
            }
            this.vmenuComponent.selectedText.setText(this.menuData.musicCollectionInfo.name);
        }
    }

    public boolean selectItem(com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState selection) {
        com.navdy.service.library.events.audio.MusicShuffleMode shuffleMode;
        logger.i("onItemClicked " + selection.id + ", " + selection.pos);
        if (selection.id == com.navdy.hud.app.R.id.menu_back) {
            this.presenter.loadMenu(this.parentMenu, com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel.BACK_TO_PARENT, this.backSelection, this.backSelectionId);
            this.backSelection = 0;
            this.backSelectionId = 0;
        } else if (selection.id == com.navdy.hud.app.R.id.music_browser_shuffle) {
            if (isThisQueuePlaying()) {
                if (this.musicManager.isShuffling()) {
                    shuffleMode = com.navdy.service.library.events.audio.MusicShuffleMode.MUSIC_SHUFFLE_MODE_OFF;
                } else {
                    shuffleMode = com.navdy.service.library.events.audio.MusicShuffleMode.MUSIC_SHUFFLE_MODE_SONGS;
                }
                this.bus.post(new com.navdy.hud.app.event.RemoteEvent(new com.navdy.service.library.events.audio.MusicEvent.Builder().action(com.navdy.service.library.events.audio.MusicEvent.Action.MUSIC_ACTION_MODE_CHANGE).shuffleMode(shuffleMode).build()));
                this.handler.post(new com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.Anon3());
            } else {
                com.navdy.service.library.events.audio.MusicCollectionInfo collectionInfo = this.menuData.musicCollectionInfo;
                this.bus.post(new com.navdy.hud.app.event.RemoteEvent(new com.navdy.service.library.events.audio.MusicEvent.Builder().collectionSource(collectionInfo.collectionSource).collectionType(collectionInfo.collectionType).collectionId(collectionInfo.collectionId).action(com.navdy.service.library.events.audio.MusicEvent.Action.MUSIC_ACTION_PLAY).shuffleMode(com.navdy.service.library.events.audio.MusicShuffleMode.MUSIC_SHUFFLE_MODE_SONGS).build()));
                showMusicPlayer();
            }
        } else if (this.menuData.musicTracks == null || this.menuData.musicTracks.size() <= 0) {
            this.presenter.loadMenu(getMusicPlayerMenu(selection.id, selection.pos), com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel.SUB_LEVEL, selection.pos, 0);
        } else {
            com.navdy.hud.app.ui.component.vlist.VerticalList.Model model = null;
            if (this.cachedList != null && selection.pos >= 0 && selection.pos < this.cachedList.size()) {
                model = (com.navdy.hud.app.ui.component.vlist.VerticalList.Model) this.cachedList.get(selection.pos);
            }
            logger.v("onItemClicked track:" + (model != null ? model.title : com.amazonaws.services.s3.internal.Constants.NULL_VERSION_ID));
            com.navdy.service.library.events.audio.MusicCollectionInfo collectionInfo2 = this.menuData.musicCollectionInfo;
            this.bus.post(new com.navdy.hud.app.event.RemoteEvent(new com.navdy.service.library.events.audio.MusicEvent.Builder().collectionSource(collectionInfo2.collectionSource).collectionType(collectionInfo2.collectionType).collectionId(collectionInfo2.collectionId).index(java.lang.Integer.valueOf(selection.id)).action(com.navdy.service.library.events.audio.MusicEvent.Action.MUSIC_ACTION_PLAY).shuffleMode(com.navdy.service.library.events.audio.MusicShuffleMode.MUSIC_SHUFFLE_MODE_OFF).build()));
            showMusicPlayer();
            com.navdy.hud.app.analytics.AnalyticsSupport.recordMusicBrowsePlayAction((java.lang.String) analyticsTypeStringMap.get(collectionInfo2.collectionType));
        }
        return true;
    }

    private void showMusicPlayer() {
        logger.i("showMusicPlayer");
        com.navdy.hud.app.ui.framework.UIStateManager uiStateManager = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getUiStateManager();
        uiStateManager.addScreenAnimationListener(new com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.Anon4(uiStateManager));
        this.presenter.close();
        clearMenuData();
        storeMenuData();
        this.musicManager.setMusicMenuPath(getPath());
    }

    public com.navdy.hud.app.ui.component.mainmenu.IMenu.Menu getType() {
        return com.navdy.hud.app.ui.component.mainmenu.IMenu.Menu.MUSIC;
    }

    public boolean isItemClickable(int id, int pos) {
        return true;
    }

    public com.navdy.hud.app.ui.component.vlist.VerticalList.Model getModelfromPos(int pos) {
        if (this.cachedList == null || this.cachedList.size() <= pos) {
            return null;
        }
        return (com.navdy.hud.app.ui.component.vlist.VerticalList.Model) this.cachedList.get(pos);
    }

    public boolean isBindCallsEnabled() {
        return true;
    }

    /* access modifiers changed from: private */
    public java.lang.String collectionIdString(com.navdy.service.library.events.audio.MusicCollectionInfo info) {
        if (info == null) {
            return null;
        }
        com.navdy.service.library.events.audio.MusicCollectionInfo defaults = (com.navdy.service.library.events.audio.MusicCollectionInfo) com.navdy.service.library.events.MessageStore.removeNulls(info);
        return ((java.lang.String) menuSourceStringMap.get(defaults.collectionSource)) + " - " + ((java.lang.String) menuTypeStringMap.get(defaults.collectionType)) + " - " + defaults.collectionId;
    }

    private java.lang.String collectionIdString(com.navdy.service.library.events.audio.MusicArtworkResponse artworkResponse) {
        return ((java.lang.String) menuSourceStringMap.get(artworkResponse.collectionSource)) + " - " + ((java.lang.String) menuTypeStringMap.get(artworkResponse.collectionType)) + " - " + artworkResponse.collectionId;
    }

    public void onBindToView(com.navdy.hud.app.ui.component.vlist.VerticalList.Model model, android.view.View view, int pos, com.navdy.hud.app.ui.component.vlist.VerticalList.ModelState state) {
        if (this.partialRefresh && pos == this.vmenuComponent.verticalList.getRawPosition()) {
            this.partialRefresh = false;
            logger.v("partial refresh:" + pos);
            this.vmenuComponent.verticalList.setViewHolderState(pos, com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.State.SELECTED, com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.AnimationType.NONE, 0);
        }
        logger.v("onBindToView:" + pos + " current=" + this.vmenuComponent.verticalList.getCurrentPosition());
        if (model.state instanceof com.navdy.service.library.events.audio.MusicTrackInfo) {
            com.navdy.hud.app.ui.component.image.IconColorImageView imageView = (com.navdy.hud.app.ui.component.image.IconColorImageView) com.navdy.hud.app.ui.component.vlist.VerticalList.findImageView(view);
            com.navdy.hud.app.ui.component.image.IconColorImageView smallImageView = (com.navdy.hud.app.ui.component.image.IconColorImageView) com.navdy.hud.app.ui.component.vlist.VerticalList.findSmallImageView(view);
            com.navdy.service.library.events.audio.MusicTrackInfo musicTrackInfo = (com.navdy.service.library.events.audio.MusicTrackInfo) model.state;
            com.navdy.service.library.events.audio.MusicTrackInfo nowPlayingTrack = this.musicManager.getCurrentTrack();
            if (nowPlayingTrack == null || !android.text.TextUtils.equals(nowPlayingTrack.trackId, musicTrackInfo.trackId)) {
                state.updateImage = true;
                state.updateSmallImage = true;
                return;
            }
            state.updateImage = false;
            state.updateSmallImage = false;
            if (com.navdy.hud.app.manager.MusicManager.tryingToPlay(nowPlayingTrack.playbackState)) {
                startAudioAnimation(imageView);
            } else {
                imageView.setImageResource(com.navdy.hud.app.R.drawable.audio_seq_32_sm);
                stopAudioAnimation();
            }
            smallImageView.setImageResource(com.navdy.hud.app.R.drawable.audio_seq_32_sm);
        } else if (model.state instanceof com.navdy.service.library.events.audio.MusicCollectionInfo) {
            com.navdy.service.library.events.audio.MusicCollectionInfo collectionInfo = (com.navdy.service.library.events.audio.MusicCollectionInfo) model.state;
            if (!android.text.TextUtils.isEmpty(collectionInfo.collectionId)) {
                com.navdy.hud.app.ui.component.image.IconColorImageView imageView2 = (com.navdy.hud.app.ui.component.image.IconColorImageView) com.navdy.hud.app.ui.component.vlist.VerticalList.findImageView(view);
                com.navdy.hud.app.ui.component.image.IconColorImageView smallImageView2 = (com.navdy.hud.app.ui.component.image.IconColorImageView) com.navdy.hud.app.ui.component.vlist.VerticalList.findSmallImageView(view);
                imageView2.setTag(null);
                java.lang.String idString = collectionIdString(collectionInfo);
                android.graphics.Bitmap bitmap = com.navdy.hud.app.util.picasso.PicassoUtil.getBitmapfromCache(idString);
                if (bitmap != null) {
                    imageView2.setImageBitmap(bitmap);
                    smallImageView2.setImageBitmap(bitmap);
                    state.updateImage = false;
                    state.updateSmallImage = false;
                    return;
                }
                imageView2.setImageBitmap(null);
                smallImageView2.setImageBitmap(null);
                if (this.requestedArtworkModelPositions.get(idString) != null) {
                    logger.i("Already requested artwork for collection: " + idString);
                    return;
                }
                this.requestedArtworkModelPositions.put(idString, java.lang.Integer.valueOf(pos));
                logger.i("Checking cache for artwork for collection: " + collectionInfo);
                this.musicArtworkCache.getArtwork(collectionInfo, new com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.Anon5(pos, collectionInfo));
            }
        } else {
            state.updateImage = true;
            state.updateSmallImage = true;
        }
    }

    public com.navdy.hud.app.ui.component.mainmenu.IMenu getChildMenu(com.navdy.hud.app.ui.component.mainmenu.IMenu parent, java.lang.String args, java.lang.String path2) {
        logger.v("getChildMenu:" + args + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA + path2);
        if (!this.musicManager.hasMusicCapabilities()) {
            logger.e("No music capabilities...");
            return null;
        }
        java.lang.String[] backData = args.split(com.navdy.hud.app.framework.glance.GlanceConstants.COLON_SEPARATOR);
        com.navdy.hud.app.ui.component.mainmenu.MusicMenu2 menu = getMusicPlayerMenu(java.lang.Integer.valueOf(backData[0]).intValue(), java.lang.Integer.valueOf(backData[1]).intValue());
        java.lang.String element = null;
        if (path2 != null) {
            if (path2.indexOf(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.SLASH) == 0) {
                element = path2.substring(1);
                int index = element.indexOf(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.SLASH);
                if (index >= 0) {
                    path2 = path2.substring(index + 1);
                    element = element.substring(0, index);
                } else {
                    path2 = null;
                }
            } else {
                path2 = null;
            }
        }
        if (!android.text.TextUtils.isEmpty(element)) {
            return menu.getChildMenu(this, element, path2);
        }
        return menu;
    }

    public void onUnload(com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel level) {
        switch (level) {
            case BACK_TO_PARENT:
                if (busDelegate != null && this.parentMenu.getType() == com.navdy.hud.app.ui.component.mainmenu.IMenu.Menu.MAIN) {
                    this.bus.unregister(busDelegate);
                    this.presenter.setScrollIdleEvents(false);
                    purgeArtworkQueue();
                    infoForCurrentArtworkRequest = null;
                    this.isRequestingMusicCollection = false;
                    this.musicManager.removeMusicUpdateListener(busDelegate);
                    stopAudioAnimation();
                    busDelegate = null;
                    logger.v("onUnload-back bus-unregister");
                }
                if (this.returnToCacheList != null) {
                    logger.v("onUnload add to cache");
                    com.navdy.hud.app.ui.component.vlist.VerticalModelCache.addToCache(this.returnToCacheList);
                    this.returnToCacheList = null;
                    return;
                }
                return;
            case CLOSE:
                if (busDelegate != null) {
                    this.bus.unregister(busDelegate);
                    this.presenter.setScrollIdleEvents(false);
                    purgeArtworkQueue();
                    infoForCurrentArtworkRequest = null;
                    this.isRequestingMusicCollection = false;
                    this.musicManager.removeMusicUpdateListener(busDelegate);
                    stopAudioAnimation();
                    busDelegate = null;
                    logger.v("onUnload-close bus-unregister");
                }
                if (this.returnToCacheList != null) {
                    logger.v("onUnload add to cache");
                    com.navdy.hud.app.ui.component.vlist.VerticalModelCache.addToCache(this.returnToCacheList);
                    this.returnToCacheList = null;
                }
                if (this.parentMenu != null && this.parentMenu.getType() == com.navdy.hud.app.ui.component.mainmenu.IMenu.Menu.MUSIC) {
                    this.parentMenu.onUnload(com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel.CLOSE);
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void onItemSelected(com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState selection) {
    }

    public void onScrollIdle() {
        boolean hasCollectionInfo;
        boolean hasTrackInfo;
        if (!this.musicCollectionSyncComplete) {
            if (this.menuData.musicCollections == null || this.menuData.musicCollections.size() <= 0) {
                hasCollectionInfo = false;
            } else {
                hasCollectionInfo = true;
            }
            if (this.menuData.musicTracks == null || this.menuData.musicTracks.size() <= 0) {
                hasTrackInfo = false;
            } else {
                hasTrackInfo = true;
            }
            if (this.menuData.musicCollectionInfo != null && this.menuData.musicCollectionInfo.collectionType != null && this.cachedList != null) {
                if (hasCollectionInfo || hasTrackInfo) {
                    int currentPos = this.vmenuComponent.verticalList.getCurrentPosition() - 1;
                    if (hasShuffleEntry()) {
                        currentPos--;
                    }
                    if (currentPos < 0) {
                        currentPos = 0;
                    }
                    int size = hasCollectionInfo ? this.menuData.musicCollections.size() : this.menuData.musicTracks.size();
                    if (currentPos >= size) {
                        logger.v("onScrollIdle pos(" + currentPos + ") >= size(" + size + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.CLOSE_BRACKET);
                        return;
                    }
                    com.squareup.wire.Message obj = hasCollectionInfo ? (com.squareup.wire.Message) this.menuData.musicCollections.get(currentPos) : (com.squareup.wire.Message) this.menuData.musicTracks.get(currentPos);
                    if (obj != null) {
                        int newPos = currentPos - 1;
                        if (newPos >= 0) {
                            obj = hasCollectionInfo ? (com.squareup.wire.Message) this.menuData.musicCollections.get(newPos) : (com.squareup.wire.Message) this.menuData.musicTracks.get(newPos);
                            if (obj == null) {
                                currentPos--;
                            }
                        }
                        if (obj != null) {
                            int newPos2 = currentPos + 1;
                            if (newPos2 <= size - 1) {
                                obj = hasCollectionInfo ? (com.squareup.wire.Message) this.menuData.musicCollections.get(newPos2) : (com.squareup.wire.Message) this.menuData.musicTracks.get(newPos2);
                            }
                            if (obj == null) {
                                currentPos++;
                            }
                        }
                    }
                    if (obj == null) {
                        calculateOffset(currentPos, 25, hasCollectionInfo);
                        logger.v("onScrollIdle newOffset:" + this.loadingOffset.offset + " count=" + this.loadingOffset.limit);
                        requestMusicCollection(this.loadingOffset.offset, this.loadingOffset.limit, false);
                    }
                }
            }
        }
    }

    public void onFastScrollStart() {
    }

    public void onFastScrollEnd() {
        logger.v("REQUESTQUEUE onFastScrollEnd");
        purgeArtworkQueue();
    }

    public void showToolTip() {
    }

    public boolean isFirstItemEmpty() {
        return true;
    }

    /* access modifiers changed from: private */
    public void onMusicCollectionResponse(com.navdy.service.library.events.audio.MusicCollectionResponse musicCollectionResponse) {
        if (!android.text.TextUtils.equals(collectionIdString(musicCollectionResponse.collectionInfo), collectionIdString(this.menuData.musicCollectionInfo))) {
            logger.w("wrong collection");
        } else if (this.requestMusicOffset == -1) {
            logger.w("no outstanding request");
        } else {
            long musicCollectionRequestResponseTime = android.os.SystemClock.elapsedRealtime() - this.currentMusicCollectionRequestTime;
            if (this.musicManager.isMusicLibraryCachingEnabled()) {
                this.musicCollectionResponseMessageCache.put(com.navdy.hud.app.ExtensionsKt.cacheKey(this.currentMusicCollectionRequest), musicCollectionResponse);
            }
            if (this.menuData.musicCollectionInfo == null) {
                this.menuData.musicCollectionInfo = musicCollectionResponse.collectionInfo;
            } else {
                com.navdy.service.library.events.audio.MusicCollectionInfo backupMusicCollectionInfo = this.menuData.musicCollectionInfo;
                com.navdy.service.library.events.audio.MusicCollectionInfo.Builder builder = new com.navdy.service.library.events.audio.MusicCollectionInfo.Builder(musicCollectionResponse.collectionInfo);
                if (android.text.TextUtils.isEmpty(musicCollectionResponse.collectionInfo.name)) {
                    builder.name(backupMusicCollectionInfo.name);
                }
                if (android.text.TextUtils.isEmpty(musicCollectionResponse.collectionInfo.subtitle)) {
                    builder.subtitle(backupMusicCollectionInfo.subtitle);
                }
                this.menuData.musicCollectionInfo = builder.build();
            }
            boolean firstResponse = true;
            com.navdy.hud.app.ui.component.vlist.VerticalList.Model[] updatedModels = null;
            if (musicCollectionResponse.musicCollections != null && musicCollectionResponse.musicCollections.size() > 0) {
                if (this.menuData.musicCollections == null) {
                    this.menuData.musicCollections = new java.util.ArrayList();
                } else {
                    firstResponse = false;
                }
                logger.d("[Timing] MusicCollectionRequest , received response  in : " + musicCollectionRequestResponseTime + " MS, Collections : " + musicCollectionResponse.musicCollections.size());
                if (firstResponse) {
                    this.menuData.musicCollections.addAll(musicCollectionResponse.musicCollections);
                    int total = this.menuData.musicCollectionInfo.size.intValue();
                    int current = musicCollectionResponse.musicCollections.size();
                    this.fastScrollIndex = checkCharacterMap(musicCollectionResponse.characterMap, total, current, true);
                    if (this.fastScrollIndex != null) {
                        this.menuData.musicIndex = musicCollectionResponse.characterMap;
                    }
                    if (total != current) {
                        int remaining = total - current;
                        for (int i = 0; i < remaining; i++) {
                            this.menuData.musicCollections.add(null);
                        }
                        logger.v("1-resp added[" + remaining + "]");
                    }
                } else {
                    int total2 = this.menuData.musicCollectionInfo.size.intValue();
                    int items = musicCollectionResponse.musicCollections.size();
                    updatedModels = new com.navdy.hud.app.ui.component.vlist.VerticalList.Model[items];
                    logger.v("2-resp offset[" + this.requestMusicOffset + "] total[" + total2 + "] got[" + items + "]");
                    for (int i2 = 0; i2 < items; i2++) {
                        com.navdy.service.library.events.audio.MusicCollectionInfo collectionInfo = (com.navdy.service.library.events.audio.MusicCollectionInfo) musicCollectionResponse.musicCollections.get(i2);
                        this.menuData.musicCollections.set(this.requestMusicOffset + i2, collectionInfo);
                        updatedModels[i2] = buildModelfromCollectionInfo(this.requestMusicOffset + i2, collectionInfo);
                    }
                }
            } else if (musicCollectionResponse.musicTracks == null || musicCollectionResponse.musicTracks.size() <= 0) {
                logger.w("No collections or tracks in this collection...");
                this.musicCollectionSyncComplete = true;
            } else {
                if (this.menuData.musicTracks == null) {
                    this.menuData.musicTracks = new java.util.ArrayList();
                } else {
                    firstResponse = false;
                }
                logger.d("[Timing] MusicCollectionRequest , received response  in : " + musicCollectionRequestResponseTime + " MS, Tracks : " + musicCollectionResponse.musicTracks.size());
                if (firstResponse) {
                    this.menuData.musicTracks.addAll(musicCollectionResponse.musicTracks);
                    int total3 = this.menuData.musicCollectionInfo.size.intValue();
                    int current2 = this.menuData.musicTracks.size();
                    this.fastScrollIndex = checkCharacterMap(musicCollectionResponse.characterMap, total3, current2, true);
                    if (this.fastScrollIndex != null) {
                        this.menuData.musicIndex = musicCollectionResponse.characterMap;
                    }
                    if (total3 != current2) {
                        int remaining2 = total3 - current2;
                        for (int i3 = 0; i3 < remaining2; i3++) {
                            this.menuData.musicTracks.add(null);
                        }
                        logger.v("1-resp-track added[" + remaining2 + "]");
                    }
                } else {
                    int total4 = this.menuData.musicCollectionInfo.size.intValue();
                    int items2 = musicCollectionResponse.musicTracks.size();
                    updatedModels = new com.navdy.hud.app.ui.component.vlist.VerticalList.Model[items2];
                    logger.v("2-resp-track offset[" + this.requestMusicOffset + "] total[" + total4 + "]  got[" + items2 + "]");
                    for (int i4 = 0; i4 < items2; i4++) {
                        com.navdy.service.library.events.audio.MusicTrackInfo trackInfo = (com.navdy.service.library.events.audio.MusicTrackInfo) musicCollectionResponse.musicTracks.get(i4);
                        this.menuData.musicTracks.set(this.requestMusicOffset + i4, trackInfo);
                        com.navdy.hud.app.ui.component.vlist.VerticalList.Model model = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(this.requestMusicOffset + i4, com.navdy.hud.app.R.drawable.icon_song, this.bgColorSelected, bgColorUnselected, this.bgColorSelected, trackInfo.name, trackInfo.author);
                        model.state = trackInfo;
                        updatedModels[i4] = model;
                    }
                }
            }
            if (firstResponse || updatedModels == null) {
                this.presenter.cancelLoadingAnimation(1);
                this.highlightedItem = this.vmenuComponent.verticalList.getCurrentPosition();
                this.presenter.updateCurrentMenu(this);
            } else {
                int pos = this.requestMusicOffset + 1;
                if (hasShuffleEntry()) {
                    pos++;
                }
                logger.v("updatemodels pos=" + pos + " len=" + updatedModels.length);
                this.partialRefresh = true;
                if (this.cachedList != null) {
                    int len = this.cachedList.size();
                    int i5 = 0;
                    while (true) {
                        if (i5 >= updatedModels.length) {
                            break;
                        }
                        int newPos = pos + i5;
                        if (newPos >= len) {
                            logger.v("invalid index:" + newPos + " size:" + len);
                            break;
                        }
                        this.cachedList.set(newPos, updatedModels[i5]);
                        i5++;
                    }
                }
                this.presenter.refreshData(pos, updatedModels, this);
            }
            this.requestMusicOffset = -1;
            this.isRequestingMusicCollection = false;
            if (this.nextRequestOffset != -1) {
                boolean request = false;
                if (this.menuData.musicCollections != null) {
                    if (this.menuData.musicCollections.get(this.nextRequestOffset) == null) {
                        request = true;
                    }
                } else if (this.menuData.musicTracks != null && this.menuData.musicTracks.get(this.nextRequestOffset) == null) {
                    request = true;
                }
                if (request) {
                    requestMusicCollection(this.nextRequestOffset, this.nextRequestLimit, false);
                    logger.v("pending request:" + this.nextRequestLimit + " limit=" + this.nextRequestLimit);
                }
                this.nextRequestOffset = -1;
                this.nextRequestLimit = -1;
            }
        }
    }

    /* access modifiers changed from: private */
    public void onMusicArtworkResponse(com.navdy.service.library.events.audio.MusicArtworkResponse artworkResponse) {
        logger.d("REQUESTQUEUE onMusicArtworkResponse " + artworkResponse);
        java.lang.String idString = collectionIdString(artworkResponse);
        java.lang.Integer pos = (java.lang.Integer) this.requestedArtworkModelPositions.get(idString);
        if (pos != null || android.text.TextUtils.equals(idString, collectionIdString(this.menuData.musicCollectionInfo))) {
            long artWorkResponseTime = android.os.SystemClock.elapsedRealtime() - this.artWorkRequestTime;
            if (artworkResponse.photo != null) {
                logger.d("[Timing] MusicArtworkRequest, response received in " + artWorkResponseTime + " MS, Size : " + artworkResponse.photo.size());
                com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.Anon6(artworkResponse, pos, idString), 1);
            } else {
                logger.w("No photo in artwork response");
            }
            synchronized (artworkRequestLock) {
                if (!artworkRequestQueue.isEmpty()) {
                    android.util.Pair<com.navdy.service.library.events.audio.MusicArtworkRequest, com.navdy.service.library.events.audio.MusicCollectionInfo> artworkRequestPair = (android.util.Pair) artworkRequestQueue.pop();
                    com.navdy.service.library.events.audio.MusicArtworkRequest artworkRequest = (com.navdy.service.library.events.audio.MusicArtworkRequest) artworkRequestPair.first;
                    logger.d("REQUESTQUEUE onMusicArtworkResponse posting: " + artworkRequest);
                    infoForCurrentArtworkRequest = (com.navdy.service.library.events.audio.MusicCollectionInfo) artworkRequestPair.second;
                    this.artWorkRequestTime = android.os.SystemClock.elapsedRealtime();
                    logger.d("[Timing] Making MusicArtWorkRequest " + artworkRequest);
                    this.bus.post(new com.navdy.hud.app.event.RemoteEvent(artworkRequest));
                } else {
                    logger.d("REQUESTQUEUE onMusicArtworkResponse queue empty");
                    infoForCurrentArtworkRequest = null;
                }
            }
            return;
        }
        logger.w("Wrong menu");
    }

    /* access modifiers changed from: private */
    public void handleArtwork(byte[] byteArray, java.lang.Integer pos, java.lang.String idString, boolean saveInDiskCache) {
        android.graphics.Bitmap artwork;
        int size = artworkSize;
        android.graphics.Bitmap bitmap = com.navdy.service.library.util.ScalingUtilities.decodeByteArray(byteArray, size, size, com.navdy.service.library.util.ScalingUtilities.ScalingLogic.FIT);
        if (com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_ARTISTS == this.menuData.musicCollectionInfo.collectionType) {
            artwork = new com.makeramen.RoundedTransformationBuilder().oval(true).build().transform(bitmap);
        } else {
            artwork = bitmap;
        }
        this.handler.post(new com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.Anon7(idString, artwork, pos, saveInDiskCache, byteArray));
    }

    private com.navdy.hud.app.ui.component.mainmenu.MusicMenu2 getMusicPlayerMenu(int id, int pos) {
        com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.MusicMenuData musicMenuData;
        logger.d("getMusicPlayerMenu " + id + ", " + pos);
        java.lang.String subPath = getSubPath(id, pos);
        if (lastPlayedMusicMenuData.containsKey(subPath)) {
            musicMenuData = (com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.MusicMenuData) lastPlayedMusicMenuData.get(subPath);
        } else {
            com.navdy.service.library.events.audio.MusicCollectionInfo info = null;
            java.util.List<com.navdy.service.library.events.audio.MusicCollectionInfo> collections = null;
            if (this.menuData.musicCollectionInfo.collectionSource == null) {
                if (id == com.navdy.hud.app.R.id.apple_podcasts) {
                    info = new com.navdy.service.library.events.audio.MusicCollectionInfo.Builder().collectionSource(com.navdy.service.library.events.audio.MusicCollectionSource.COLLECTION_SOURCE_IOS_MEDIA_PLAYER).collectionType(com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_PODCASTS).build();
                } else {
                    com.navdy.service.library.events.audio.MusicCollectionInfo collectionInfo = (com.navdy.service.library.events.audio.MusicCollectionInfo) this.menuData.musicCollections.get(id);
                    info = new com.navdy.service.library.events.audio.MusicCollectionInfo.Builder().collectionSource(collectionInfo.collectionSource).build();
                    java.util.List<com.navdy.service.library.events.audio.MusicCollectionType> collectionTypes = this.musicManager.getCollectionTypesForSource(collectionInfo.collectionSource);
                    collections = new java.util.ArrayList<>(collectionTypes.size());
                    for (com.navdy.service.library.events.audio.MusicCollectionType collectionType : collectionTypes) {
                        collections.add(new com.navdy.service.library.events.audio.MusicCollectionInfo.Builder().collectionSource(collectionInfo.collectionSource).collectionType(collectionType).build());
                    }
                }
            } else if (this.menuData.musicCollectionInfo.collectionType == null) {
                info = new com.navdy.service.library.events.audio.MusicCollectionInfo.Builder().collectionSource(this.menuData.musicCollectionInfo.collectionSource).collectionType(((com.navdy.service.library.events.audio.MusicCollectionInfo) this.menuData.musicCollections.get(id)).collectionType).build();
            } else if (this.menuData.musicCollections.size() > 0) {
                info = (com.navdy.service.library.events.audio.MusicCollectionInfo) this.menuData.musicCollections.get(id);
                logger.i("fetching collection " + info.collectionId);
            }
            musicMenuData = new com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.MusicMenuData(info, collections);
        }
        com.navdy.hud.app.ui.component.mainmenu.MusicMenu2 menu = new com.navdy.hud.app.ui.component.mainmenu.MusicMenu2(this.bus, this.vmenuComponent, this.presenter, this, musicMenuData);
        menu.setBackSelectionPos(pos);
        menu.setBackSelectionId(id);
        return menu;
    }

    public java.lang.String getPath() {
        if (this.path == null) {
            if (this.parentMenu.getType() == com.navdy.hud.app.ui.component.mainmenu.IMenu.Menu.MUSIC) {
                this.path = ((com.navdy.hud.app.ui.component.mainmenu.MusicMenu2) this.parentMenu).getPath() + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.SLASH + this.backSelectionId + com.navdy.hud.app.framework.glance.GlanceConstants.COLON_SEPARATOR + this.backSelection;
            } else {
                this.path = com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.SLASH + com.navdy.hud.app.ui.component.mainmenu.IMenu.Menu.MUSIC.name();
            }
        }
        return this.path;
    }

    private java.lang.String getSubPath(int id, int pos) {
        return getPath() + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.SLASH + id + com.navdy.hud.app.framework.glance.GlanceConstants.COLON_SEPARATOR + pos;
    }

    public static void clearMenuData() {
        lastPlayedMusicMenuData.clear();
    }

    private void storeMenuData() {
        if (this.parentMenu.getType() == com.navdy.hud.app.ui.component.mainmenu.IMenu.Menu.MUSIC) {
            ((com.navdy.hud.app.ui.component.mainmenu.MusicMenu2) this.parentMenu).storeMenuData();
        }
        lastPlayedMusicMenuData.put(getPath(), this.menuData);
    }

    private void setupIconBgColor() {
        if (this.menuData.musicCollectionInfo == null) {
            return;
        }
        if (this.menuData.musicCollectionInfo.collectionSource == com.navdy.service.library.events.audio.MusicCollectionSource.COLLECTION_SOURCE_ANDROID_LOCAL) {
            this.bgColorSelected = androidBgColor;
        } else if (this.menuData.musicCollectionInfo.collectionType == com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_PODCASTS) {
            this.bgColorSelected = applePodcastsBgColor;
        } else {
            this.bgColorSelected = appleMusicBgColor;
        }
    }

    private void startAudioAnimation(com.navdy.hud.app.ui.component.image.IconColorImageView imageView) {
        imageView.animate().cancel();
        imageView.setImageResource(com.navdy.hud.app.R.drawable.audio_loop);
        android.graphics.drawable.AnimationDrawable frameAnimation = (android.graphics.drawable.AnimationDrawable) imageView.getDrawable();
        frameAnimation.start();
        this.musicAnimation = frameAnimation;
        logger.v("startAudioAnimation");
    }

    private void stopAudioAnimation() {
        if (this.musicAnimation != null && this.musicAnimation.isRunning()) {
            this.musicAnimation.stop();
            logger.v("stopAudioAnimation");
        }
        this.musicAnimation = null;
    }

    private boolean isThisQueuePlaying() {
        return android.text.TextUtils.equals(getPath(), this.musicManager.getMusicMenuPath());
    }

    private com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex checkCharacterMap(java.util.List<com.navdy.service.library.events.audio.MusicCharacterMap> list, int total, int current, boolean check) {
        boolean hasCharacterMap = list != null;
        int size = -1;
        if (hasCharacterMap) {
            size = list.size();
        }
        if (hasCharacterMap) {
            if (!check || (total > 40 && size >= 2)) {
                com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex scrollIndex = buildIndexFromCharacterMap(list);
                if (scrollIndex != null) {
                    logger.v("checkCharacterMap index-yes len[" + total + "] map[" + size + "] index[" + scrollIndex.getEntryCount() + "]");
                    return scrollIndex;
                }
                logger.v("checkCharacterMap index-error len[" + total + "] map[" + size + "]");
                return scrollIndex;
            }
            logger.v("checkCharacterMap index-no created len[" + total + "] index-len[" + size + "]");
        }
        return null;
    }

    private com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex buildIndexFromCharacterMap(java.util.List<com.navdy.service.library.events.audio.MusicCharacterMap> list) {
        try {
            com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex.Builder builder = new com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex.Builder();
            for (com.navdy.service.library.events.audio.MusicCharacterMap map : list) {
                builder.setEntry(map.character.charAt(0), map.offset.intValue());
            }
            builder.positionOffset(1);
            return builder.build();
        } catch (Throwable t) {
            logger.e("buildIndexFromCharacterMap", t);
            return null;
        }
    }

    private boolean isIndexSupported(com.navdy.service.library.events.audio.MusicCollectionInfo collectionInfo) {
        if (!(collectionInfo == null || collectionInfo.collectionType == null)) {
            switch (collectionInfo.collectionType) {
                case COLLECTION_TYPE_PLAYLISTS:
                case COLLECTION_TYPE_ARTISTS:
                case COLLECTION_TYPE_ALBUMS:
                    return true;
            }
        }
        return false;
    }

    private void calculateOffset(int currentPos, int pageLimit, boolean hasCollectionInfo) {
        int top = (currentPos / pageLimit) * pageLimit;
        int bottom = java.lang.Math.min((hasCollectionInfo ? this.menuData.musicCollections.size() : this.menuData.musicTracks.size()) - 1, top + pageLimit);
        this.loadingOffset.clear();
        this.loadingOffset.offset = top;
        this.loadingOffset.limit = (bottom - top) + 1;
    }

    private com.navdy.hud.app.ui.component.vlist.VerticalList.Model buildModelfromCollectionInfo(int id, com.navdy.service.library.events.audio.MusicCollectionInfo collectionInfo) {
        java.lang.String subtitle;
        if (collectionInfo.subtitle != null) {
            subtitle = collectionInfo.subtitle;
        } else {
            subtitle = resources.getQuantityString(collectionInfo.collectionType == com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_PODCASTS ? com.navdy.hud.app.R.plurals.episodes_count : com.navdy.hud.app.R.plurals.songs_count, collectionInfo.size.intValue(), new java.lang.Object[]{collectionInfo.size});
        }
        com.navdy.hud.app.ui.component.vlist.VerticalList.Model model = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(id, ((java.lang.Integer) menuTypeIconMap.get(collectionInfo.collectionType)).intValue(), this.bgColorSelected, bgColorUnselected, transparentColor, collectionInfo.name, subtitle, null, collectionInfo.collectionType == com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_ARTISTS ? com.navdy.hud.app.ui.component.image.IconColorImageView.IconShape.CIRCLE : com.navdy.hud.app.ui.component.image.IconColorImageView.IconShape.SQUARE);
        model.state = collectionInfo;
        return model;
    }

    private boolean hasShuffleEntry() {
        if (this.cachedList == null || this.cachedList.size() < 2 || ((com.navdy.hud.app.ui.component.vlist.VerticalList.Model) this.cachedList.get(1)).id != com.navdy.hud.app.R.id.music_browser_shuffle) {
            return false;
        }
        return true;
    }
}
