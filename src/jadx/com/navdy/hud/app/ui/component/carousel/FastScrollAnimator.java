package com.navdy.hud.app.ui.component.carousel;

public class FastScrollAnimator implements com.navdy.hud.app.ui.component.carousel.AnimationStrategy {
    public static final int ANIMATION_HERO = 50;
    public static final int ANIMATION_HERO_SINGLE = 133;
    public static final int ANIMATION_HERO_TEXT = 165;
    public static final int ANIMATION_HERO_TRANSLATE_X = ((int) com.navdy.hud.app.HudApplication.getAppContext().getResources().getDimension(com.navdy.hud.app.R.dimen.carousel_fastscroll_hero_translate_x));
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.class);
    /* access modifiers changed from: private */
    public com.navdy.hud.app.ui.component.carousel.CarouselLayout carouselLayout;
    /* access modifiers changed from: private */
    public boolean endPending;
    /* access modifiers changed from: private */
    public android.view.View hiddenView;
    /* access modifiers changed from: private */
    public long lastScrollAnimationFinishTime;

    class Anon1 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
        final /* synthetic */ android.animation.Animator.AnimatorListener val$listener;
        final /* synthetic */ int val$newPos;

        Anon1(android.animation.Animator.AnimatorListener animatorListener, int i) {
            this.val$listener = animatorListener;
            this.val$newPos = i;
        }

        public void onAnimationStart(android.animation.Animator animation) {
            com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.this.endPending = true;
            if (this.val$listener != null) {
                this.val$listener.onAnimationStart(animation);
            }
            com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.this.hiddenView.setVisibility(0);
            com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.this.carouselLayout.leftView.setAlpha(0.0f);
            com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.this.carouselLayout.rightView.setAlpha(0.0f);
            com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.this.carouselLayout.middleRightView.setAlpha(0.0f);
            if (!com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.this.carouselLayout.isAnimationPending()) {
                com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.this.carouselLayout.carouselAdapter.getView(this.val$newPos - 1, com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.this.carouselLayout.leftView, com.navdy.hud.app.ui.component.carousel.Carousel.ViewType.SIDE, com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.this.carouselLayout.imageLytResourceId, com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.this.carouselLayout.sideImageSize);
                com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.this.carouselLayout.carouselAdapter.getView(this.val$newPos + 1, com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.this.carouselLayout.rightView, com.navdy.hud.app.ui.component.carousel.Carousel.ViewType.SIDE, com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.this.carouselLayout.imageLytResourceId, com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.this.carouselLayout.sideImageSize);
                com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.this.carouselLayout.carouselAdapter.getView(this.val$newPos, com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.this.carouselLayout.middleRightView, com.navdy.hud.app.ui.component.carousel.Carousel.ViewType.MIDDLE_RIGHT, com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.this.carouselLayout.imageLytResourceId, com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.this.carouselLayout.sideImageSize);
            }
        }

        public void onAnimationEnd(android.animation.Animator animation) {
            com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.this.lastScrollAnimationFinishTime = android.os.SystemClock.elapsedRealtime();
            if (!com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.this.carouselLayout.isAnimationPending()) {
                com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.this.endAnimation(this.val$listener, this.val$newPos, false);
                return;
            }
            com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.this.carouselLayout.currentItem = this.val$newPos;
            android.view.View newMiddleLeft = com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.this.hiddenView;
            com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.this.hiddenView = com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.this.carouselLayout.middleLeftView;
            com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.this.hiddenView.setVisibility(4);
            com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.this.hiddenView.setAlpha(0.0f);
            com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.this.carouselLayout.middleLeftView = newMiddleLeft;
            if (this.val$listener != null) {
                this.val$listener.onAnimationEnd(animation);
            }
            com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.this.carouselLayout.selectedItemView = com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.this.carouselLayout.middleLeftView;
            if (com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.this.carouselLayout.carouselIndicator != null) {
                com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.this.carouselLayout.carouselIndicator.setCurrentItem(com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.this.carouselLayout.currentItem);
            }
            if (com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.this.carouselLayout.itemChangeListener != null) {
                com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.this.carouselLayout.itemChangeListener.onCurrentItemChanged(com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.this.carouselLayout.currentItem, ((com.navdy.hud.app.ui.component.carousel.Carousel.Model) com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.this.carouselLayout.model.get(com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.this.carouselLayout.currentItem)).id);
            }
            com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.this.carouselLayout.runQueuedOperation();
        }
    }

    class Anon2 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
        final /* synthetic */ android.animation.Animator.AnimatorListener val$listener;
        final /* synthetic */ int val$newPos;
        final /* synthetic */ boolean val$skipEnd;

        Anon2(boolean z, int i, android.animation.Animator.AnimatorListener animatorListener) {
            this.val$skipEnd = z;
            this.val$newPos = i;
            this.val$listener = animatorListener;
        }

        public void onAnimationEnd(android.animation.Animator animation) {
            com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.this.endPending = false;
            if (!this.val$skipEnd) {
                com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.this.carouselLayout.currentItem = this.val$newPos;
                android.view.View newMiddleLeft = com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.this.hiddenView;
                com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.this.hiddenView = com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.this.carouselLayout.middleLeftView;
                com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.this.hiddenView.setVisibility(4);
                com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.this.hiddenView.setAlpha(0.0f);
                com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.this.carouselLayout.middleLeftView = newMiddleLeft;
                if (this.val$listener != null) {
                    this.val$listener.onAnimationEnd(animation);
                }
                com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.this.carouselLayout.selectedItemView = com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.this.carouselLayout.middleLeftView;
                if (com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.this.carouselLayout.carouselIndicator != null) {
                    com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.this.carouselLayout.carouselIndicator.setCurrentItem(com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.this.carouselLayout.currentItem);
                }
                if (com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.this.carouselLayout.itemChangeListener != null) {
                    com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.this.carouselLayout.itemChangeListener.onCurrentItemChanged(com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.this.carouselLayout.currentItem, ((com.navdy.hud.app.ui.component.carousel.Carousel.Model) com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.this.carouselLayout.model.get(com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.this.carouselLayout.currentItem)).id);
                }
            }
            com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.this.carouselLayout.runQueuedOperation();
        }
    }

    FastScrollAnimator(com.navdy.hud.app.ui.component.carousel.CarouselLayout carouselLayout2) {
        this.carouselLayout = carouselLayout2;
        this.hiddenView = carouselLayout2.buildView(0, 0, com.navdy.hud.app.ui.component.carousel.Carousel.ViewType.MIDDLE_LEFT, false);
    }

    public android.animation.AnimatorSet createMiddleLeftViewAnimation(com.navdy.hud.app.ui.component.carousel.CarouselLayout carousel, com.navdy.hud.app.ui.component.carousel.AnimationStrategy.Direction direction) {
        return null;
    }

    public android.animation.AnimatorSet createMiddleRightViewAnimation(com.navdy.hud.app.ui.component.carousel.CarouselLayout carousel, com.navdy.hud.app.ui.component.carousel.AnimationStrategy.Direction direction) {
        return null;
    }

    public android.animation.AnimatorSet createSideViewToMiddleAnimation(com.navdy.hud.app.ui.component.carousel.CarouselLayout carousel, com.navdy.hud.app.ui.component.carousel.AnimationStrategy.Direction direction) {
        return null;
    }

    public android.animation.Animator createViewOutAnimation(com.navdy.hud.app.ui.component.carousel.CarouselLayout carousel, com.navdy.hud.app.ui.component.carousel.AnimationStrategy.Direction direction) {
        return null;
    }

    public android.animation.AnimatorSet createHiddenViewAnimation(com.navdy.hud.app.ui.component.carousel.CarouselLayout carousel, com.navdy.hud.app.ui.component.carousel.AnimationStrategy.Direction direction) {
        return null;
    }

    public android.animation.AnimatorSet createNewMiddleRightViewAnimation(com.navdy.hud.app.ui.component.carousel.CarouselLayout carousel, com.navdy.hud.app.ui.component.carousel.AnimationStrategy.Direction direction) {
        return null;
    }

    public android.animation.AnimatorSet buildLayoutAnimation(android.animation.Animator.AnimatorListener listener, com.navdy.hud.app.ui.component.carousel.CarouselLayout carousel, int currentPos, int newPos) {
        int translateX;
        int hiddenX;
        android.animation.AnimatorSet animatorSet = new android.animation.AnimatorSet();
        if ((newPos > currentPos ? com.navdy.hud.app.ui.component.carousel.AnimationStrategy.Direction.LEFT : com.navdy.hud.app.ui.component.carousel.AnimationStrategy.Direction.RIGHT) == com.navdy.hud.app.ui.component.carousel.AnimationStrategy.Direction.LEFT) {
            translateX = (int) (carousel.middleLeftView.getX() - ((float) ANIMATION_HERO_TRANSLATE_X));
            hiddenX = (int) (carousel.middleLeftView.getX() + ((float) ANIMATION_HERO_TRANSLATE_X));
        } else {
            translateX = (int) (carousel.middleLeftView.getX() + ((float) ANIMATION_HERO_TRANSLATE_X));
            hiddenX = (int) (carousel.middleLeftView.getX() - ((float) ANIMATION_HERO_TRANSLATE_X));
        }
        this.hiddenView.setX((float) hiddenX);
        this.hiddenView.setAlpha(0.0f);
        android.animation.AnimatorSet.Builder builder = animatorSet.play(android.animation.ObjectAnimator.ofFloat(carousel.middleLeftView, android.view.View.X, new float[]{(float) translateX}));
        builder.with(android.animation.ObjectAnimator.ofFloat(carousel.middleLeftView, android.view.View.ALPHA, new float[]{0.0f}));
        builder.with(android.animation.ObjectAnimator.ofFloat(this.hiddenView, android.view.View.X, new float[]{this.carouselLayout.middleLeftView.getX()}));
        builder.with(android.animation.ObjectAnimator.ofFloat(this.hiddenView, android.view.View.ALPHA, new float[]{1.0f}));
        this.carouselLayout.carouselAdapter.getView(newPos, this.hiddenView, com.navdy.hud.app.ui.component.carousel.Carousel.ViewType.MIDDLE_LEFT, this.carouselLayout.imageLytResourceId, this.carouselLayout.mainImageSize);
        if (this.carouselLayout.carouselIndicator != null) {
            android.animation.AnimatorSet indicatorAnimator = this.carouselLayout.carouselIndicator.getItemMoveAnimator(newPos, -1);
            if (indicatorAnimator != null) {
                builder.with(indicatorAnimator);
            }
        }
        animatorSet.setInterpolator(this.carouselLayout.interpolator);
        animatorSet.addListener(new com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.Anon1(listener, newPos));
        int duration = 50;
        if (!this.carouselLayout.isAnimationPending() && android.os.SystemClock.elapsedRealtime() - this.lastScrollAnimationFinishTime > 50) {
            duration = 133;
        }
        animatorSet.setDuration((long) duration);
        if (sLogger.isLoggable(3)) {
            sLogger.v("dur=" + duration);
        }
        return animatorSet;
    }

    /* access modifiers changed from: 0000 */
    public void endAnimation(android.animation.Animator.AnimatorListener listener, int newPos, boolean skipEnd) {
        android.animation.AnimatorSet set = new android.animation.AnimatorSet();
        android.animation.ObjectAnimator alpha1 = android.animation.ObjectAnimator.ofFloat(this.carouselLayout.leftView, android.view.View.ALPHA, new float[]{1.0f});
        android.animation.ObjectAnimator alpha2 = android.animation.ObjectAnimator.ofFloat(this.carouselLayout.rightView, android.view.View.ALPHA, new float[]{1.0f});
        android.animation.ObjectAnimator alpha3 = android.animation.ObjectAnimator.ofFloat(this.carouselLayout.middleRightView, android.view.View.ALPHA, new float[]{1.0f});
        set.setDuration(165);
        set.playTogether(new android.animation.Animator[]{alpha1, alpha2, alpha3});
        set.addListener(new com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.Anon2(skipEnd, newPos, listener));
        set.start();
    }

    /* access modifiers changed from: 0000 */
    public void endAnimation() {
        if (this.endPending) {
            this.endPending = false;
            this.carouselLayout.carouselAdapter.getView(this.carouselLayout.currentItem - 1, this.carouselLayout.leftView, com.navdy.hud.app.ui.component.carousel.Carousel.ViewType.SIDE, this.carouselLayout.imageLytResourceId, this.carouselLayout.sideImageSize);
            this.carouselLayout.carouselAdapter.getView(this.carouselLayout.currentItem + 1, this.carouselLayout.rightView, com.navdy.hud.app.ui.component.carousel.Carousel.ViewType.SIDE, this.carouselLayout.imageLytResourceId, this.carouselLayout.sideImageSize);
            this.carouselLayout.carouselAdapter.getView(this.carouselLayout.currentItem, this.carouselLayout.middleRightView, com.navdy.hud.app.ui.component.carousel.Carousel.ViewType.MIDDLE_RIGHT, this.carouselLayout.imageLytResourceId, this.carouselLayout.sideImageSize);
            endAnimation(null, this.carouselLayout.currentItem, true);
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean isEndPending() {
        return this.endPending;
    }
}
