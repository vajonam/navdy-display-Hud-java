package com.navdy.hud.app.ui.component.tbt;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u0000 \u001b2\u00020\u0001:\u0001\u001bB\u000f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004B\u0017\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007B\u001f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\b\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0006\u0010\u0010\u001a\u00020\u0011J\u001a\u0010\u0012\u001a\u00020\u00112\u0006\u0010\u0013\u001a\u00020\f2\n\u0010\u0014\u001a\u00060\u0015R\u00020\u0016J\u000e\u0010\u0017\u001a\u00020\u00112\u0006\u0010\u0013\u001a\u00020\fJ\u000e\u0010\u0018\u001a\u00020\u00112\u0006\u0010\u0019\u001a\u00020\u001aR\u0010\u0010\u000b\u001a\u0004\u0018\u00010\fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\r\u001a\u0004\u0018\u00010\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001c"}, d2 = {"Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView;", "Landroid/widget/ImageView;", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "attrs", "Landroid/util/AttributeSet;", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "defStyleAttr", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "currentMode", "Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;", "lastManeuverState", "Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;", "lastTurnIconId", "clear", "", "getCustomAnimator", "mode", "mainBuilder", "Landroid/animation/AnimatorSet$Builder;", "Landroid/animation/AnimatorSet;", "setMode", "updateDisplay", "event", "Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;", "Companion", "app_hudDebug"}, k = 1, mv = {1, 1, 6})
/* compiled from: TbtDirectionView.kt */
public final class TbtDirectionView extends android.widget.ImageView {
    public static final com.navdy.hud.app.ui.component.tbt.TbtDirectionView.Companion Companion = new com.navdy.hud.app.ui.component.tbt.TbtDirectionView.Companion(null);
    /* access modifiers changed from: private */
    public static final int fullWidth = Companion.getResources().getDimensionPixelSize(com.navdy.hud.app.R.dimen.tbt_direction_full_w);
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger("TbtDirectionView");
    /* access modifiers changed from: private */
    public static final int mediumWidth = Companion.getResources().getDimensionPixelSize(com.navdy.hud.app.R.dimen.tbt_direction_medium_w);
    /* access modifiers changed from: private */
    public static final android.content.res.Resources resources;
    private java.util.HashMap _$_findViewCache;
    private com.navdy.hud.app.view.MainView.CustomAnimationMode currentMode;
    private com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.ManeuverState lastManeuverState;
    private int lastTurnIconId = -1;

    @kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u0014\u0010\u0007\u001a\u00020\bX\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0014\u0010\u000b\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\f\u0010\u0006R\u0014\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010\u00a8\u0006\u0011"}, d2 = {"Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView$Companion;", "", "()V", "fullWidth", "", "getFullWidth", "()I", "logger", "Lcom/navdy/service/library/log/Logger;", "getLogger", "()Lcom/navdy/service/library/log/Logger;", "mediumWidth", "getMediumWidth", "resources", "Landroid/content/res/Resources;", "getResources", "()Landroid/content/res/Resources;", "app_hudDebug"}, k = 1, mv = {1, 1, 6})
    /* compiled from: TbtDirectionView.kt */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(kotlin.jvm.internal.DefaultConstructorMarker $constructor_marker) {
            this();
        }

        private final com.navdy.service.library.log.Logger getLogger() {
            return com.navdy.hud.app.ui.component.tbt.TbtDirectionView.logger;
        }

        /* access modifiers changed from: private */
        public final android.content.res.Resources getResources() {
            return com.navdy.hud.app.ui.component.tbt.TbtDirectionView.resources;
        }

        /* access modifiers changed from: private */
        public final int getFullWidth() {
            return com.navdy.hud.app.ui.component.tbt.TbtDirectionView.fullWidth;
        }

        /* access modifiers changed from: private */
        public final int getMediumWidth() {
            return com.navdy.hud.app.ui.component.tbt.TbtDirectionView.mediumWidth;
        }
    }

    public void _$_clearFindViewByIdCache() {
        if (this._$_findViewCache != null) {
            this._$_findViewCache.clear();
        }
    }

    public android.view.View _$_findCachedViewById(int i) {
        if (this._$_findViewCache == null) {
            this._$_findViewCache = new java.util.HashMap();
        }
        android.view.View view = (android.view.View) this._$_findViewCache.get(java.lang.Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        android.view.View findViewById = findViewById(i);
        this._$_findViewCache.put(java.lang.Integer.valueOf(i), findViewById);
        return findViewById;
    }

    static {
        android.content.res.Resources resources2 = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(resources2, "HudApplication.getAppContext().resources");
        resources = resources2;
    }

    public TbtDirectionView(@org.jetbrains.annotations.NotNull android.content.Context context) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(context, "context");
        super(context);
    }

    public TbtDirectionView(@org.jetbrains.annotations.NotNull android.content.Context context, @org.jetbrains.annotations.NotNull android.util.AttributeSet attrs) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(context, "context");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(attrs, "attrs");
        super(context, attrs);
    }

    public TbtDirectionView(@org.jetbrains.annotations.NotNull android.content.Context context, @org.jetbrains.annotations.NotNull android.util.AttributeSet attrs, int defStyleAttr) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(context, "context");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(attrs, "attrs");
        super(context, attrs, defStyleAttr);
    }

    public final void setMode(@org.jetbrains.annotations.NotNull com.navdy.hud.app.view.MainView.CustomAnimationMode mode) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(mode, net.hockeyapp.android.LoginActivity.EXTRA_MODE);
        if (!kotlin.jvm.internal.Intrinsics.areEqual((java.lang.Object) this.currentMode, (java.lang.Object) mode)) {
            android.view.ViewGroup.LayoutParams layoutParams = getLayoutParams();
            if (layoutParams == null) {
                throw new kotlin.TypeCastException("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
            }
            android.view.ViewGroup.MarginLayoutParams lytParams = (android.view.ViewGroup.MarginLayoutParams) layoutParams;
            if (kotlin.jvm.internal.Intrinsics.areEqual((java.lang.Object) mode, (java.lang.Object) com.navdy.hud.app.view.MainView.CustomAnimationMode.EXPAND)) {
                lytParams.width = Companion.getFullWidth();
                lytParams.height = Companion.getFullWidth();
            } else {
                lytParams.width = Companion.getMediumWidth();
                lytParams.height = Companion.getMediumWidth();
            }
            requestLayout();
            this.currentMode = mode;
        }
    }

    public final void getCustomAnimator(@org.jetbrains.annotations.NotNull com.navdy.hud.app.view.MainView.CustomAnimationMode mode, @org.jetbrains.annotations.NotNull android.animation.AnimatorSet.Builder mainBuilder) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(mode, net.hockeyapp.android.LoginActivity.EXTRA_MODE);
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(mainBuilder, "mainBuilder");
    }

    public final void clear() {
        this.lastTurnIconId = -1;
        setImageDrawable(null);
    }

    public final void updateDisplay(@org.jetbrains.annotations.NotNull com.navdy.hud.app.maps.MapEvents.ManeuverDisplay event) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(event, "event");
        if (event.turnIconId != -1) {
            setImageResource(event.turnIconId);
        } else {
            setImageDrawable(null);
        }
        this.lastManeuverState = event.maneuverState;
        this.lastTurnIconId = event.turnIconId;
    }
}
