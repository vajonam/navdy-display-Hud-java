package com.navdy.hud.app.ui.component.homescreen;

public class MpgView$$ViewInjector {
    public static void inject(butterknife.ButterKnife.Finder finder, com.navdy.hud.app.ui.component.homescreen.MpgView target, java.lang.Object source) {
        target.mpgTextView = (android.widget.TextView) finder.findRequiredView(source, com.navdy.hud.app.R.id.txt_mpg, "field 'mpgTextView'");
        target.mpgLabelTextView = (android.widget.TextView) finder.findRequiredView(source, com.navdy.hud.app.R.id.txt_mpg_label, "field 'mpgLabelTextView'");
    }

    public static void reset(com.navdy.hud.app.ui.component.homescreen.MpgView target) {
        target.mpgTextView = null;
        target.mpgLabelTextView = null;
    }
}
