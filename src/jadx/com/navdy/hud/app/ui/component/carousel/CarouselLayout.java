package com.navdy.hud.app.ui.component.carousel;

public class CarouselLayout extends android.widget.FrameLayout implements com.navdy.hud.app.manager.InputManager.IInputHandler, com.navdy.hud.app.gesture.GestureDetector.GestureListener {
    static final /* synthetic */ boolean $assertionsDisabled = (!com.navdy.hud.app.ui.component.carousel.CarouselLayout.class.desiredAssertionStatus());
    private static final int DEFAULT_ANIM_DURATION = 250;
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.carousel.CarouselLayout.class);
    /* access modifiers changed from: private */
    public int animationDuration;
    boolean animationRunning;
    private com.navdy.hud.app.ui.component.carousel.AnimationStrategy animator;
    com.navdy.hud.app.ui.component.carousel.CarouselAdapter carouselAdapter;
    com.navdy.hud.app.ui.component.carousel.CarouselIndicator carouselIndicator;
    int currentItem = -1;
    protected com.navdy.hud.app.gesture.GestureDetector detector = new com.navdy.hud.app.gesture.GestureDetector(this);
    private boolean exitOnDoubleClick;
    private boolean fastScrollAnimation;
    private android.os.Handler handler = new android.os.Handler(android.os.Looper.getMainLooper());
    int imageLytResourceId;
    android.view.LayoutInflater inflater;
    int infoLayoutResourceId;
    android.view.animation.Interpolator interpolator;
    com.navdy.hud.app.ui.component.carousel.Carousel.Listener itemChangeListener;
    com.navdy.hud.app.ui.component.carousel.CarouselLayout.Operation lastScrollAnimationOperation;
    /* access modifiers changed from: private */
    public int lastTouchX;
    /* access modifiers changed from: private */
    public int lastTouchY;
    android.view.View leftView;
    int mainImageSize;
    int mainLeftPadding;
    int mainRightPadding;
    int mainViewDividerPadding;
    android.view.View middleLeftView;
    android.view.View middleRightView;
    java.util.List<com.navdy.hud.app.ui.component.carousel.Carousel.Model> model;
    private com.navdy.hud.app.gesture.MultipleClickGestureDetector multipleClickGestureDetector;
    android.view.View newLeftView;
    android.view.View newMiddleLeftView;
    android.view.View newMiddleRightView;
    android.view.View newRightView;
    private java.util.Queue<com.navdy.hud.app.ui.component.carousel.CarouselLayout.OperationInfo> operationQueue = new java.util.LinkedList();
    int rightImageStart;
    int rightSectionHeight;
    int rightSectionWidth;
    android.view.View rightView;
    android.view.View rootContainer;
    android.view.View selectedItemView;
    int sideImageSize;
    private com.navdy.hud.app.ui.component.carousel.Carousel.ViewCacheManager viewCacheManager = new com.navdy.hud.app.ui.component.carousel.Carousel.ViewCacheManager(3);
    int viewPadding;
    com.navdy.hud.app.ui.component.carousel.Carousel.ViewProcessor viewProcessor;
    boolean viewsScaled;

    class Anon1 implements android.view.View.OnTouchListener {
        Anon1() {
        }

        public boolean onTouch(android.view.View v, android.view.MotionEvent event) {
            if (event.getAction() == 1) {
                com.navdy.hud.app.ui.component.carousel.CarouselLayout.this.lastTouchX = (int) event.getX();
                com.navdy.hud.app.ui.component.carousel.CarouselLayout.this.lastTouchY = (int) event.getY();
            }
            return false;
        }
    }

    class Anon2 implements android.view.View.OnClickListener {
        Anon2() {
        }

        public void onClick(android.view.View v) {
            android.graphics.Rect hitRect = new android.graphics.Rect();
            int[] myPos = new int[2];
            int[] parentPos = new int[2];
            com.navdy.hud.app.ui.component.carousel.CarouselLayout.this.getLocationInWindow(myPos);
            android.view.View[] views = {com.navdy.hud.app.ui.component.carousel.CarouselLayout.this.leftView, com.navdy.hud.app.ui.component.carousel.CarouselLayout.this.middleLeftView, com.navdy.hud.app.ui.component.carousel.CarouselLayout.this.rightView};
            int i = 0;
            while (i < views.length) {
                android.view.View childView = views[i];
                ((android.view.View) childView.getParent()).getLocationInWindow(parentPos);
                int touchX = com.navdy.hud.app.ui.component.carousel.CarouselLayout.this.lastTouchX - (parentPos[0] - myPos[0]);
                int touchY = com.navdy.hud.app.ui.component.carousel.CarouselLayout.this.lastTouchY - (parentPos[1] - myPos[1]);
                childView.getHitRect(hitRect);
                if (!hitRect.contains(touchX, touchY)) {
                    i++;
                } else if (i == 1) {
                    com.navdy.hud.app.ui.component.carousel.CarouselLayout.this.selectItem();
                    return;
                } else {
                    com.navdy.hud.app.ui.component.carousel.CarouselLayout.this.move(null, i > 1, true, false, com.navdy.hud.app.ui.component.carousel.CarouselLayout.this.animationDuration);
                    return;
                }
            }
        }
    }

    class Anon3 implements com.navdy.hud.app.gesture.MultipleClickGestureDetector.IMultipleClickKeyGesture {
        Anon3() {
        }

        public void onMultipleClick(int count) {
            if (com.navdy.hud.app.ui.component.carousel.CarouselLayout.this.itemChangeListener != null) {
                com.navdy.hud.app.ui.component.carousel.CarouselLayout.this.itemChangeListener.onExit();
            }
        }

        public boolean onGesture(com.navdy.service.library.events.input.GestureEvent event) {
            return false;
        }

        public boolean onKey(com.navdy.hud.app.manager.InputManager.CustomKeyEvent event) {
            return com.navdy.hud.app.ui.component.carousel.CarouselLayout.this.handleKey(event);
        }

        public com.navdy.hud.app.manager.InputManager.IInputHandler nextHandler() {
            return null;
        }
    }

    class Anon4 implements java.lang.Runnable {
        final /* synthetic */ boolean val$animated;
        final /* synthetic */ java.util.List val$newModel;
        final /* synthetic */ int val$position;

        Anon4(java.util.List list, int i, boolean z) {
            this.val$newModel = list;
            this.val$position = i;
            this.val$animated = z;
        }

        public void run() {
            com.navdy.hud.app.ui.component.carousel.CarouselLayout.this.setModelInternal(this.val$newModel, this.val$position, this.val$animated);
        }
    }

    class Anon5 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
        final /* synthetic */ java.util.List val$newModel;
        final /* synthetic */ int val$position;

        Anon5(java.util.List list, int i) {
            this.val$newModel = list;
            this.val$position = i;
        }

        public void onAnimationStart(android.animation.Animator animator) {
            com.navdy.hud.app.ui.component.carousel.CarouselLayout.this.model = this.val$newModel;
            if (com.navdy.hud.app.ui.component.carousel.CarouselLayout.this.itemChangeListener != null) {
                com.navdy.hud.app.ui.component.carousel.CarouselLayout.this.itemChangeListener.onCurrentItemChanging(com.navdy.hud.app.ui.component.carousel.CarouselLayout.this.currentItem, this.val$position, ((com.navdy.hud.app.ui.component.carousel.Carousel.Model) com.navdy.hud.app.ui.component.carousel.CarouselLayout.this.model.get(this.val$position)).id);
            }
        }

        public void onAnimationEnd(android.animation.Animator animation) {
            com.navdy.hud.app.ui.component.carousel.CarouselLayout.this.setCurrentItem(this.val$position, false);
            com.navdy.hud.app.ui.component.carousel.CarouselLayout.this.runQueuedOperation();
        }
    }

    class Anon6 implements java.lang.Runnable {
        final /* synthetic */ boolean val$keyPress;
        final /* synthetic */ android.animation.Animator.AnimatorListener val$listener;
        final /* synthetic */ boolean val$next;
        final /* synthetic */ com.navdy.hud.app.ui.component.carousel.CarouselLayout.OperationInfo val$operationInfo;

        Anon6(android.animation.Animator.AnimatorListener animatorListener, boolean z, boolean z2, com.navdy.hud.app.ui.component.carousel.CarouselLayout.OperationInfo operationInfo) {
            this.val$listener = animatorListener;
            this.val$next = z;
            this.val$keyPress = z2;
            this.val$operationInfo = operationInfo;
        }

        public void run() {
            com.navdy.hud.app.ui.component.carousel.CarouselLayout.this.move(this.val$listener, this.val$next, this.val$keyPress, true, this.val$operationInfo.animationDuration);
        }
    }

    class Anon7 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.hud.app.ui.component.carousel.Carousel.Model val$item;
        final /* synthetic */ android.animation.Animator.AnimatorListener val$listener;
        final /* synthetic */ int val$pos;
        final /* synthetic */ boolean val$preserveCurrentItem;

        Anon7(android.animation.Animator.AnimatorListener animatorListener, int i, com.navdy.hud.app.ui.component.carousel.Carousel.Model model, boolean z) {
            this.val$listener = animatorListener;
            this.val$pos = i;
            this.val$item = model;
            this.val$preserveCurrentItem = z;
        }

        public void run() {
            com.navdy.hud.app.ui.component.carousel.CarouselLayout.this.doInsert(this.val$listener, this.val$pos, this.val$item, this.val$preserveCurrentItem);
        }
    }

    class Anon8 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
        final /* synthetic */ android.animation.Animator.AnimatorListener val$listener;
        final /* synthetic */ int val$newPos;
        final /* synthetic */ boolean val$next;
        final /* synthetic */ int val$oldPos;

        Anon8(android.animation.Animator.AnimatorListener animatorListener, int i, int i2, boolean z) {
            this.val$listener = animatorListener;
            this.val$oldPos = i;
            this.val$newPos = i2;
            this.val$next = z;
        }

        public void onAnimationStart(android.animation.Animator animation) {
            if (this.val$listener != null) {
                this.val$listener.onAnimationStart(animation);
            }
            if (com.navdy.hud.app.ui.component.carousel.CarouselLayout.this.newMiddleRightView != null) {
                com.navdy.hud.app.ui.component.carousel.CarouselLayout.this.newMiddleRightView.setVisibility(0);
            }
            if (com.navdy.hud.app.ui.component.carousel.CarouselLayout.this.itemChangeListener != null && this.val$oldPos != this.val$newPos) {
                com.navdy.hud.app.ui.component.carousel.CarouselLayout.this.itemChangeListener.onCurrentItemChanging(this.val$oldPos, this.val$newPos, ((com.navdy.hud.app.ui.component.carousel.Carousel.Model) com.navdy.hud.app.ui.component.carousel.CarouselLayout.this.model.get(this.val$newPos)).id);
            }
        }

        public void onAnimationEnd(android.animation.Animator animation) {
            com.navdy.hud.app.ui.component.carousel.CarouselLayout.this.currentItem = this.val$newPos;
            if (this.val$listener != null) {
                this.val$listener.onAnimationEnd(animation);
            }
            com.navdy.hud.app.ui.component.carousel.CarouselLayout.this.changeSelectedItem(this.val$next, true);
            if (this.val$next) {
                if (com.navdy.hud.app.ui.component.carousel.CarouselLayout.this.leftView != null) {
                    com.navdy.hud.app.ui.component.carousel.CarouselLayout.this.recycleView(com.navdy.hud.app.ui.component.carousel.Carousel.ViewType.SIDE, com.navdy.hud.app.ui.component.carousel.CarouselLayout.this.leftView);
                    com.navdy.hud.app.ui.component.carousel.CarouselLayout.this.leftView = null;
                }
                com.navdy.hud.app.ui.component.carousel.CarouselLayout.this.leftView = com.navdy.hud.app.ui.component.carousel.CarouselLayout.this.middleLeftView;
                com.navdy.hud.app.ui.component.carousel.CarouselLayout.this.middleLeftView = com.navdy.hud.app.ui.component.carousel.CarouselLayout.this.rightView;
                com.navdy.hud.app.ui.component.carousel.CarouselLayout.this.rightView = com.navdy.hud.app.ui.component.carousel.CarouselLayout.this.newRightView;
            } else {
                if (com.navdy.hud.app.ui.component.carousel.CarouselLayout.this.rightView != null) {
                    com.navdy.hud.app.ui.component.carousel.CarouselLayout.this.recycleView(com.navdy.hud.app.ui.component.carousel.Carousel.ViewType.SIDE, com.navdy.hud.app.ui.component.carousel.CarouselLayout.this.rightView);
                    com.navdy.hud.app.ui.component.carousel.CarouselLayout.this.rightView = null;
                }
                com.navdy.hud.app.ui.component.carousel.CarouselLayout.this.rightView = com.navdy.hud.app.ui.component.carousel.CarouselLayout.this.middleLeftView;
                com.navdy.hud.app.ui.component.carousel.CarouselLayout.this.middleLeftView = com.navdy.hud.app.ui.component.carousel.CarouselLayout.this.newMiddleLeftView != null ? com.navdy.hud.app.ui.component.carousel.CarouselLayout.this.newMiddleLeftView : com.navdy.hud.app.ui.component.carousel.CarouselLayout.this.leftView;
                com.navdy.hud.app.ui.component.carousel.CarouselLayout.this.leftView = com.navdy.hud.app.ui.component.carousel.CarouselLayout.this.newLeftView;
            }
            if (com.navdy.hud.app.ui.component.carousel.CarouselLayout.this.newMiddleRightView != null) {
                com.navdy.hud.app.ui.component.carousel.CarouselLayout.this.recycleView(com.navdy.hud.app.ui.component.carousel.Carousel.ViewType.MIDDLE_RIGHT, com.navdy.hud.app.ui.component.carousel.CarouselLayout.this.middleRightView);
                com.navdy.hud.app.ui.component.carousel.CarouselLayout.this.middleRightView = com.navdy.hud.app.ui.component.carousel.CarouselLayout.this.newMiddleRightView;
                com.navdy.hud.app.ui.component.carousel.CarouselLayout.this.newMiddleRightView = null;
            }
            com.navdy.hud.app.ui.component.carousel.CarouselLayout.this.newMiddleLeftView = null;
            com.navdy.hud.app.ui.component.carousel.CarouselLayout.this.newLeftView = null;
            com.navdy.hud.app.ui.component.carousel.CarouselLayout.this.newRightView = null;
            if (com.navdy.hud.app.ui.component.carousel.CarouselLayout.this.leftView == null) {
                com.navdy.hud.app.ui.component.carousel.CarouselLayout.this.leftView = com.navdy.hud.app.ui.component.carousel.CarouselLayout.this.addSideView(this.val$newPos - 1, com.navdy.hud.app.ui.component.carousel.CarouselLayout.this.viewPadding, false);
            }
            if (com.navdy.hud.app.ui.component.carousel.CarouselLayout.this.rightView == null) {
                com.navdy.hud.app.ui.component.carousel.CarouselLayout.this.rightView = com.navdy.hud.app.ui.component.carousel.CarouselLayout.this.addSideView(this.val$newPos + 1, com.navdy.hud.app.ui.component.carousel.CarouselLayout.this.viewPadding + com.navdy.hud.app.ui.component.carousel.CarouselLayout.this.sideImageSize + com.navdy.hud.app.ui.component.carousel.CarouselLayout.this.rightImageStart, false);
            }
            if (com.navdy.hud.app.ui.component.carousel.CarouselLayout.this.carouselIndicator != null) {
                com.navdy.hud.app.ui.component.carousel.CarouselLayout.this.carouselIndicator.setCurrentItem(com.navdy.hud.app.ui.component.carousel.CarouselLayout.this.currentItem);
            }
            if (com.navdy.hud.app.ui.component.carousel.CarouselLayout.this.itemChangeListener != null) {
                com.navdy.hud.app.ui.component.carousel.CarouselLayout.this.itemChangeListener.onCurrentItemChanged(com.navdy.hud.app.ui.component.carousel.CarouselLayout.this.currentItem, ((com.navdy.hud.app.ui.component.carousel.Carousel.Model) com.navdy.hud.app.ui.component.carousel.CarouselLayout.this.model.get(com.navdy.hud.app.ui.component.carousel.CarouselLayout.this.currentItem)).id);
            }
            com.navdy.hud.app.ui.component.carousel.CarouselLayout.this.runQueuedOperation();
        }
    }

    class Anon9 implements java.lang.Runnable {
        Anon9() {
        }

        public void run() {
            com.navdy.hud.app.ui.component.carousel.CarouselLayout.this.animationRunning = false;
            com.navdy.hud.app.ui.component.carousel.CarouselLayout.this.selectItem();
        }
    }

    enum Operation {
        FORWARD,
        BACK,
        SELECT,
        OTHER
    }

    static class OperationInfo {
        int animationDuration = -1;
        int count = 1;
        java.lang.Runnable runnable;
        com.navdy.hud.app.ui.component.carousel.CarouselLayout.Operation type;

        OperationInfo(com.navdy.hud.app.ui.component.carousel.CarouselLayout.Operation type2, java.lang.Runnable runnable2) {
            this.type = type2;
            this.runnable = runnable2;
        }

        OperationInfo(com.navdy.hud.app.ui.component.carousel.CarouselLayout.Operation type2, java.lang.Runnable runnable2, int animationDuration2) {
            this.type = type2;
            this.runnable = runnable2;
            this.animationDuration = animationDuration2;
        }
    }

    public CarouselLayout(android.content.Context context) {
        super(context, null);
    }

    public CarouselLayout(android.content.Context context, android.util.AttributeSet attrs) {
        super(context, attrs);
        this.inflater = android.view.LayoutInflater.from(context);
        initFromAttributes(context, attrs);
        setWillNotDraw(false);
        if (!com.navdy.hud.app.util.DeviceUtil.isNavdyDevice()) {
            setOnTouchListener(new com.navdy.hud.app.ui.component.carousel.CarouselLayout.Anon1());
            setOnClickListener(new com.navdy.hud.app.ui.component.carousel.CarouselLayout.Anon2());
        }
    }

    /* JADX INFO: finally extract failed */
    private void initFromAttributes(android.content.Context context, android.util.AttributeSet attrs) {
        boolean z = true;
        android.content.res.TypedArray a = context.getTheme().obtainStyledAttributes(attrs, com.navdy.hud.app.R.styleable.Carousel, 0, 0);
        if ($assertionsDisabled || a != null) {
            android.content.res.Resources resources = getResources();
            try {
                this.animationDuration = a.getInt(0, 250);
                this.interpolator = android.view.animation.AnimationUtils.loadInterpolator(getContext(), a.getResourceId(1, 17432580));
                this.viewPadding = (int) a.getDimension(2, resources.getDimension(com.navdy.hud.app.R.dimen.carousel_side_margin));
                this.sideImageSize = (int) a.getDimension(3, resources.getDimension(com.navdy.hud.app.R.dimen.carousel_side_image_size));
                this.mainImageSize = (int) a.getDimension(4, resources.getDimension(com.navdy.hud.app.R.dimen.carousel_main_image_size));
                this.rightImageStart = (int) a.getDimension(7, resources.getDimension(com.navdy.hud.app.R.dimen.carousel_main_right_section_start));
                this.mainLeftPadding = (int) a.getDimension(5, resources.getDimension(com.navdy.hud.app.R.dimen.carousel_main_left_padding));
                this.mainRightPadding = (int) a.getDimension(6, resources.getDimension(com.navdy.hud.app.R.dimen.carousel_main_right_padding));
                this.rightSectionWidth = (int) a.getDimension(9, resources.getDimension(com.navdy.hud.app.R.dimen.carousel_main_right_section_width));
                if (this.mainImageSize == this.sideImageSize) {
                    z = false;
                }
                this.viewsScaled = z;
                sLogger.v("view scaled=" + this.viewsScaled);
                a.recycle();
                this.rightSectionHeight = (int) getResources().getDimension(com.navdy.hud.app.R.dimen.dashboard_box_height);
            } catch (Throwable th) {
                a.recycle();
                throw th;
            }
        } else {
            throw new java.lang.AssertionError();
        }
    }

    public void init(com.navdy.hud.app.ui.component.carousel.Carousel.InitParams params) {
        if (this.model != null) {
            throw new java.lang.IllegalStateException();
        } else if (params.model == null || params.rootContainer == null || params.infoLayoutResourceId == 0 || params.imageLytResourceId == 0) {
            throw new java.lang.IllegalArgumentException();
        } else {
            this.model = params.model;
            this.infoLayoutResourceId = params.infoLayoutResourceId;
            this.viewProcessor = params.viewProcessor;
            this.rootContainer = params.rootContainer;
            this.carouselIndicator = params.carouselIndicator;
            this.carouselAdapter = new com.navdy.hud.app.ui.component.carousel.CarouselAdapter(this);
            this.imageLytResourceId = params.imageLytResourceId;
            this.fastScrollAnimation = params.fastScrollAnimation;
            this.exitOnDoubleClick = params.exitOnDoubleClick;
            com.navdy.hud.app.ui.component.carousel.AnimationStrategy carouselAnimator = params.animator != null ? params.animator : this.fastScrollAnimation ? new com.navdy.hud.app.ui.component.carousel.FastScrollAnimator(this) : new com.navdy.hud.app.ui.component.carousel.CarouselAnimator();
            this.animator = carouselAnimator;
            if (this.exitOnDoubleClick) {
                this.multipleClickGestureDetector = new com.navdy.hud.app.gesture.MultipleClickGestureDetector(2, new com.navdy.hud.app.ui.component.carousel.CarouselLayout.Anon3());
            }
        }
    }

    public void setListener(com.navdy.hud.app.ui.component.carousel.Carousel.Listener carouselListener) {
        this.itemChangeListener = carouselListener;
    }

    public com.navdy.hud.app.ui.component.carousel.Carousel.Listener getListener() {
        return this.itemChangeListener;
    }

    public int getCurrentItem() {
        return this.currentItem;
    }

    public com.navdy.hud.app.ui.component.carousel.Carousel.Model getCurrentModel() {
        return (com.navdy.hud.app.ui.component.carousel.Carousel.Model) this.model.get(this.currentItem);
    }

    public com.navdy.hud.app.ui.component.carousel.Carousel.Model getModel(int item) {
        if (item < 0 || item >= this.model.size()) {
            return null;
        }
        return (com.navdy.hud.app.ui.component.carousel.Carousel.Model) this.model.get(item);
    }

    public int getCount() {
        if (this.model == null) {
            return 0;
        }
        return this.model.size();
    }

    public void setModel(java.util.List<com.navdy.hud.app.ui.component.carousel.Carousel.Model> newModel, int position, boolean animated) {
        if (this.animationRunning) {
            clearOperationQueue();
            this.operationQueue.add(new com.navdy.hud.app.ui.component.carousel.CarouselLayout.OperationInfo(com.navdy.hud.app.ui.component.carousel.CarouselLayout.Operation.OTHER, new com.navdy.hud.app.ui.component.carousel.CarouselLayout.Anon4(newModel, position, animated)));
            return;
        }
        setModelInternal(newModel, position, animated);
    }

    /* access modifiers changed from: private */
    public void setModelInternal(java.util.List<com.navdy.hud.app.ui.component.carousel.Carousel.Model> newModel, int position, boolean animated) {
        if (this.currentItem == -1 || !animated) {
            this.model = newModel;
            setCurrentItem(position, false);
            runQueuedOperation();
            return;
        }
        android.animation.AnimatorSet animatorSet = new android.animation.AnimatorSet();
        android.animation.AnimatorSet.Builder builder = animatorSet.play(android.animation.ObjectAnimator.ofFloat(this.middleLeftView, android.view.View.ALPHA, new float[]{0.0f}));
        if (this.middleRightView != null) {
            builder.with(android.animation.ObjectAnimator.ofFloat(this.middleRightView, android.view.View.ALPHA, new float[]{0.0f}));
        }
        if (this.leftView != null) {
            builder.with(android.animation.ObjectAnimator.ofFloat(this.leftView, android.view.View.ALPHA, new float[]{0.0f}));
        }
        if (this.rightView != null) {
            builder.with(android.animation.ObjectAnimator.ofFloat(this.rightView, android.view.View.ALPHA, new float[]{0.0f}));
        }
        animatorSet.setDuration(250);
        animatorSet.addListener(new com.navdy.hud.app.ui.component.carousel.CarouselLayout.Anon5(newModel, position));
        this.animationRunning = true;
        animatorSet.start();
    }

    /* access modifiers changed from: private */
    public void setCurrentItem(int position, boolean callOnCurrentItemChanging) {
        boolean z = true;
        if (position >= 0 && position < this.model.size()) {
            if (this.currentItem != -1) {
                recycleViews();
            }
            this.leftView = addSideView(position - 1, this.viewPadding, position > 0);
            this.middleLeftView = addMiddleLeftView(position, this.viewPadding + this.sideImageSize + this.mainLeftPadding, true);
            this.middleRightView = addMiddleRightView(position, this.viewPadding + this.sideImageSize + this.mainLeftPadding + this.mainImageSize, true);
            int i = position + 1;
            int i2 = this.viewPadding + this.sideImageSize + this.rightImageStart;
            if (position + 1 >= this.model.size()) {
                z = false;
            }
            this.rightView = addSideView(i, i2, z);
            if (callOnCurrentItemChanging && this.itemChangeListener != null) {
                this.itemChangeListener.onCurrentItemChanging(this.currentItem, position, ((com.navdy.hud.app.ui.component.carousel.Carousel.Model) this.model.get(position)).id);
            }
            this.currentItem = position;
            if (this.carouselIndicator != null) {
                this.carouselIndicator.setCurrentItem(this.currentItem);
            }
            if (this.itemChangeListener != null) {
                this.itemChangeListener.onCurrentItemChanged(this.currentItem, ((com.navdy.hud.app.ui.component.carousel.Carousel.Model) this.model.get(position)).id);
            }
            if (this.middleLeftView != null && this.middleLeftView.getVisibility() == 0) {
                this.selectedItemView = this.middleLeftView;
            }
        }
    }

    public void setCurrentItem(int position) {
        setCurrentItem(position, true);
    }

    public void movePrevious(android.animation.Animator.AnimatorListener listener) {
        move(listener, false, false, false, this.animationDuration);
    }

    public void moveNext(android.animation.Animator.AnimatorListener listener) {
        move(listener, true, false, false, this.animationDuration);
    }

    /* access modifiers changed from: private */
    public void move(android.animation.Animator.AnimatorListener listener, boolean next, boolean keyPress, boolean ignoreAnimationRunning, int duration) {
        com.navdy.hud.app.ui.component.carousel.CarouselLayout.Operation operation;
        if (next) {
            operation = com.navdy.hud.app.ui.component.carousel.CarouselLayout.Operation.FORWARD;
        } else {
            operation = com.navdy.hud.app.ui.component.carousel.CarouselLayout.Operation.BACK;
        }
        if (ignoreAnimationRunning || !this.animationRunning) {
            int newPos = this.currentItem + (next ? 1 : -1);
            if (this.currentItem < 0 || newPos < 0 || newPos >= getCount()) {
                if (sLogger.isLoggable(2)) {
                    sLogger.v("cannot go " + (next ? "next" : "prev"));
                }
                runQueuedOperation();
                return;
            }
            this.lastScrollAnimationOperation = operation;
            if (this.animator instanceof com.navdy.hud.app.ui.component.carousel.FastScrollAnimator) {
                android.animation.AnimatorSet animatorSet = this.animator.buildLayoutAnimation(listener, this, this.currentItem, newPos);
                this.animationRunning = true;
                if (sLogger.isLoggable(2)) {
                    sLogger.v("move:" + operation.name() + " duration =" + animatorSet.getDuration());
                }
                animatorSet.start();
            } else {
                android.animation.AnimatorSet animatorSet2 = buildLayoutAnimation(listener, this.currentItem, newPos);
                animatorSet2.setDuration((long) duration);
                this.animationRunning = true;
                if (sLogger.isLoggable(2)) {
                    sLogger.v("move:" + operation.name() + " duration =" + animatorSet2.getDuration());
                }
                animatorSet2.start();
            }
            com.navdy.hud.app.audio.SoundUtils.playSound(com.navdy.hud.app.audio.SoundUtils.Sound.MENU_MOVE);
        } else if (this.fastScrollAnimation || this.operationQueue.size() < 3) {
            if (this.fastScrollAnimation) {
                if (this.operationQueue.size() > 0) {
                    com.navdy.hud.app.ui.component.carousel.CarouselLayout.OperationInfo last = (com.navdy.hud.app.ui.component.carousel.CarouselLayout.OperationInfo) this.operationQueue.peek();
                    if (last.type == operation) {
                        last.count++;
                        return;
                    }
                } else if (this.lastScrollAnimationOperation == operation) {
                    duration = 50;
                }
            }
            com.navdy.hud.app.ui.component.carousel.CarouselLayout.OperationInfo operationInfo = new com.navdy.hud.app.ui.component.carousel.CarouselLayout.OperationInfo(operation, null, duration);
            operationInfo.runnable = new com.navdy.hud.app.ui.component.carousel.CarouselLayout.Anon6(listener, next, keyPress, operationInfo);
            this.operationQueue.add(operationInfo);
        }
    }

    public void insert(android.animation.Animator.AnimatorListener listener, int pos, com.navdy.hud.app.ui.component.carousel.Carousel.Model item, boolean preserveCurrentItem) {
        if (this.animationRunning) {
            this.operationQueue.add(new com.navdy.hud.app.ui.component.carousel.CarouselLayout.OperationInfo(com.navdy.hud.app.ui.component.carousel.CarouselLayout.Operation.OTHER, new com.navdy.hud.app.ui.component.carousel.CarouselLayout.Anon7(listener, pos, item, preserveCurrentItem)));
            return;
        }
        doInsert(listener, pos, item, preserveCurrentItem);
    }

    /* access modifiers changed from: private */
    public void doInsert(android.animation.Animator.AnimatorListener listener, int pos, com.navdy.hud.app.ui.component.carousel.Carousel.Model item, boolean preserveCurrentItem) {
        this.model.add(pos, item);
        android.animation.AnimatorSet animatorSet = new android.animation.AnimatorSet();
        java.util.Collection<android.animation.Animator> animatorCollection = new java.util.ArrayList<>();
        int previous = this.currentItem;
        com.navdy.hud.app.ui.component.carousel.AnimationStrategy.Direction direction = com.navdy.hud.app.ui.component.carousel.AnimationStrategy.Direction.RIGHT;
        int leftX = this.viewPadding;
        int rightX = this.viewPadding + this.sideImageSize + this.rightImageStart;
        int middleLeftX = this.viewPadding + this.sideImageSize + this.mainLeftPadding;
        int middleRightX = this.viewPadding + this.sideImageSize + this.mainLeftPadding + this.mainImageSize;
        if (pos <= this.currentItem + 1) {
            if (pos == this.currentItem + 1) {
                this.newRightView = addSideView(pos, rightX, false);
                direction = com.navdy.hud.app.ui.component.carousel.AnimationStrategy.Direction.RIGHT;
                animatorCollection.add(this.animator.createViewOutAnimation(this, direction));
            } else if (pos == this.currentItem) {
                if (preserveCurrentItem) {
                    this.newLeftView = addSideView(pos, leftX, false);
                    direction = com.navdy.hud.app.ui.component.carousel.AnimationStrategy.Direction.LEFT;
                    animatorCollection.add(this.animator.createViewOutAnimation(this, direction));
                    this.currentItem++;
                } else {
                    this.newMiddleLeftView = addMiddleLeftView(pos, middleLeftX, false);
                    this.newMiddleRightView = addMiddleRightView(pos, middleRightX, false);
                    direction = com.navdy.hud.app.ui.component.carousel.AnimationStrategy.Direction.RIGHT;
                    if (this.rightView != null) {
                        animatorCollection.add(this.animator.createViewOutAnimation(this, direction));
                    }
                    animatorCollection.add(this.animator.createMiddleLeftViewAnimation(this, direction));
                    animatorCollection.add(this.animator.createMiddleRightViewAnimation(this, direction));
                }
            } else if (pos <= this.currentItem - 1) {
                if (preserveCurrentItem) {
                    this.currentItem++;
                } else {
                    move(listener, true, true, false, this.animationDuration);
                    return;
                }
            }
        }
        animatorSet.addListener(buildListener(listener, direction == com.navdy.hud.app.ui.component.carousel.AnimationStrategy.Direction.LEFT, previous, this.currentItem));
        animatorSet.playTogether(animatorCollection);
        this.animationRunning = true;
        animatorSet.start();
    }

    private android.animation.AnimatorSet buildLayoutAnimation(android.animation.Animator.AnimatorListener listener, int pos, int newPos) {
        android.animation.AnimatorSet animatorSet = new android.animation.AnimatorSet();
        boolean next = newPos > pos;
        com.navdy.hud.app.ui.component.carousel.AnimationStrategy.Direction direction = next ? com.navdy.hud.app.ui.component.carousel.AnimationStrategy.Direction.LEFT : com.navdy.hud.app.ui.component.carousel.AnimationStrategy.Direction.RIGHT;
        int offset = this.mainViewDividerPadding + this.sideImageSize;
        if (next) {
            this.newMiddleRightView = addMiddleRightView(newPos, ((int) this.rightView.getX()) + offset, false);
            this.newRightView = addHiddenView(newPos + 1, com.navdy.hud.app.ui.component.carousel.AnimationStrategy.Direction.RIGHT, newPos + 1 < this.model.size());
        } else {
            this.newLeftView = addHiddenView(newPos - 1, com.navdy.hud.app.ui.component.carousel.AnimationStrategy.Direction.LEFT, newPos > 0);
            this.newMiddleRightView = addMiddleRightView(newPos, ((int) this.leftView.getX()) + offset, false);
            this.newMiddleRightView.setAlpha(0.0f);
        }
        android.animation.AnimatorSet.Builder builder = animatorSet.play(this.animator.createViewOutAnimation(this, direction));
        builder.with(this.animator.createMiddleLeftViewAnimation(this, direction));
        builder.with(this.animator.createMiddleRightViewAnimation(this, direction));
        builder.with(this.animator.createSideViewToMiddleAnimation(this, direction));
        builder.with(this.animator.createNewMiddleRightViewAnimation(this, direction));
        builder.with(this.animator.createHiddenViewAnimation(this, direction));
        if (this.carouselIndicator != null) {
            android.animation.AnimatorSet indicatorAnimator = this.carouselIndicator.getItemMoveAnimator(newPos, -1);
            if (indicatorAnimator != null) {
                builder.with(indicatorAnimator);
            }
        }
        animatorSet.setInterpolator(this.interpolator);
        animatorSet.addListener(buildListener(listener, next, this.currentItem, newPos));
        return animatorSet;
    }

    private android.animation.Animator.AnimatorListener buildListener(android.animation.Animator.AnimatorListener listener, boolean next, int oldPos, int newPos) {
        return new com.navdy.hud.app.ui.component.carousel.CarouselLayout.Anon8(listener, oldPos, newPos, next);
    }

    private void recycleViews() {
        recycleView(com.navdy.hud.app.ui.component.carousel.Carousel.ViewType.SIDE, this.leftView);
        this.leftView = null;
        recycleView(com.navdy.hud.app.ui.component.carousel.Carousel.ViewType.SIDE, this.rightView);
        this.rightView = null;
        recycleView(com.navdy.hud.app.ui.component.carousel.Carousel.ViewType.MIDDLE_LEFT, this.middleLeftView);
        this.middleLeftView = null;
        recycleView(com.navdy.hud.app.ui.component.carousel.Carousel.ViewType.MIDDLE_RIGHT, this.middleRightView);
        this.middleRightView = null;
    }

    /* access modifiers changed from: private */
    public void recycleView(com.navdy.hud.app.ui.component.carousel.Carousel.ViewType viewType, android.view.View view) {
        if (view != null) {
            removeView(view);
            view.setScaleX(1.0f);
            view.setScaleY(1.0f);
            view.setVisibility(0);
            view.setTranslationX(0.0f);
            view.setTranslationY(0.0f);
            view.setAlpha(1.0f);
            if (view instanceof com.navdy.hud.app.ui.component.image.CrossFadeImageView) {
                com.navdy.hud.app.ui.component.image.CrossFadeImageView crossfadeImageView = (com.navdy.hud.app.ui.component.image.CrossFadeImageView) view;
                android.widget.ImageView imageView = (android.widget.ImageView) crossfadeImageView.getBig();
                imageView.setImageResource(0);
                imageView.setPivotX(0.0f);
                imageView.setPivotY(0.0f);
                if (this.viewsScaled && (imageView instanceof com.navdy.hud.app.ui.component.image.InitialsImageView)) {
                    ((com.navdy.hud.app.ui.component.image.InitialsImageView) imageView).setScaled(true);
                }
                android.widget.ImageView imageView2 = (android.widget.ImageView) crossfadeImageView.getSmall();
                imageView2.setImageResource(0);
                imageView2.setPivotX(0.0f);
                imageView2.setPivotY(0.0f);
                if (this.viewsScaled && (imageView2 instanceof com.navdy.hud.app.ui.component.image.InitialsImageView)) {
                    ((com.navdy.hud.app.ui.component.image.InitialsImageView) imageView2).setScaled(true);
                }
            }
            this.viewCacheManager.putView(viewType, view);
        }
    }

    /* access modifiers changed from: private */
    public void changeSelectedItem(boolean next, boolean keyPress) {
        if (this.selectedItemView != null) {
            if (next) {
                this.selectedItemView = this.rightView;
            } else {
                this.selectedItemView = this.leftView;
            }
        }
    }

    public void selectItem() {
        if (this.animationRunning) {
            java.lang.Runnable runnable = new com.navdy.hud.app.ui.component.carousel.CarouselLayout.Anon9();
            sLogger.w("queueing up select event");
            this.operationQueue.add(new com.navdy.hud.app.ui.component.carousel.CarouselLayout.OperationInfo(com.navdy.hud.app.ui.component.carousel.CarouselLayout.Operation.SELECT, runnable));
            return;
        }
        clearOperationQueue();
        if (this.selectedItemView == null) {
            sLogger.v("no item selected");
            runQueuedOperation();
        } else if (this.selectedItemView == this.middleLeftView) {
            int id = ((java.lang.Integer) this.selectedItemView.getTag(com.navdy.hud.app.R.id.item_id)).intValue();
            com.navdy.hud.app.audio.SoundUtils.playSound(com.navdy.hud.app.audio.SoundUtils.Sound.MENU_SELECT);
            if (this.itemChangeListener != null) {
                sLogger.v("execute item:" + id);
                this.itemChangeListener.onExecuteItem(id, this.currentItem);
                return;
            }
            sLogger.v("no carousel listener");
            runQueuedOperation();
        } else {
            sLogger.v("no match");
            runQueuedOperation();
        }
    }

    /* access modifiers changed from: 0000 */
    public android.view.View buildView(int item, int xPos, com.navdy.hud.app.ui.component.carousel.Carousel.ViewType viewType, boolean visible) {
        int w = 0;
        int h = 0;
        int layoutId = 0;
        switch (viewType) {
            case SIDE:
                h = this.sideImageSize;
                w = this.sideImageSize;
                layoutId = this.imageLytResourceId;
                break;
            case MIDDLE_LEFT:
                h = this.mainImageSize;
                w = this.mainImageSize;
                layoutId = this.imageLytResourceId;
                break;
            case MIDDLE_RIGHT:
                h = this.rightSectionHeight;
                w = this.rightSectionWidth;
                layoutId = this.infoLayoutResourceId;
                break;
        }
        android.widget.FrameLayout.LayoutParams lytParams = new android.widget.FrameLayout.LayoutParams(w, h);
        lytParams.gravity = 16;
        android.view.View convertView = this.viewCacheManager.getView(viewType);
        android.view.View view = this.carouselAdapter.getView(item, convertView, viewType, layoutId, w);
        if (convertView == null && this.viewsScaled && viewType == com.navdy.hud.app.ui.component.carousel.Carousel.ViewType.SIDE && (view instanceof com.navdy.hud.app.ui.component.image.CrossFadeImageView)) {
            com.navdy.hud.app.ui.component.image.CrossFadeImageView crossFadeImageView = (com.navdy.hud.app.ui.component.image.CrossFadeImageView) view;
            android.view.View big = crossFadeImageView.getBig();
            if (big instanceof com.navdy.hud.app.ui.component.image.InitialsImageView) {
                ((com.navdy.hud.app.ui.component.image.InitialsImageView) big).setScaled(true);
            }
            if (crossFadeImageView.getSmall() instanceof com.navdy.hud.app.ui.component.image.InitialsImageView) {
                ((com.navdy.hud.app.ui.component.image.InitialsImageView) big).setScaled(true);
            }
        }
        view.setX((float) xPos);
        view.setVisibility(visible ? 0 : 4);
        addView(view, lytParams);
        return view;
    }

    /* access modifiers changed from: 0000 */
    public android.view.View addSideView(int item, int xPos, boolean visible) {
        return buildView(item, xPos, com.navdy.hud.app.ui.component.carousel.Carousel.ViewType.SIDE, visible);
    }

    /* access modifiers changed from: 0000 */
    public android.view.View addMiddleLeftView(int item, int xPos, boolean visible) {
        return buildView(item, xPos, com.navdy.hud.app.ui.component.carousel.Carousel.ViewType.MIDDLE_LEFT, visible);
    }

    /* access modifiers changed from: 0000 */
    public android.view.View addMiddleRightView(int item, int xPos, boolean visible) {
        return buildView(item, xPos, com.navdy.hud.app.ui.component.carousel.Carousel.ViewType.MIDDLE_RIGHT, visible);
    }

    /* access modifiers changed from: 0000 */
    public android.view.View addHiddenView(int item, com.navdy.hud.app.ui.component.carousel.AnimationStrategy.Direction direction, boolean visible) {
        float x;
        if (direction == com.navdy.hud.app.ui.component.carousel.AnimationStrategy.Direction.RIGHT) {
            x = this.rightView.getX() + ((float) this.mainImageSize) + ((float) this.mainViewDividerPadding) + ((float) this.rightSectionWidth) + ((float) this.mainRightPadding);
        } else {
            x = -(this.leftView.getX() + ((float) this.leftView.getMeasuredWidth()));
        }
        return addSideView(item, (int) x, visible);
    }

    public void onClick() {
    }

    public void onTrackHand(float normalized) {
    }

    public boolean onGesture(com.navdy.service.library.events.input.GestureEvent event) {
        if (this.currentItem == -1) {
            return false;
        }
        this.detector.onGesture(event);
        switch (event.gesture) {
        }
        return true;
    }

    public boolean onKey(com.navdy.hud.app.manager.InputManager.CustomKeyEvent event) {
        if (this.multipleClickGestureDetector != null) {
            return this.multipleClickGestureDetector.onKey(event);
        }
        return handleKey(event);
    }

    /* access modifiers changed from: private */
    public boolean handleKey(com.navdy.hud.app.manager.InputManager.CustomKeyEvent event) {
        if (this.currentItem == -1) {
            return true;
        }
        switch (event) {
            case LEFT:
                move(null, false, true, false, this.animationDuration);
                return true;
            case RIGHT:
                move(null, true, true, false, this.animationDuration);
                return true;
            case SELECT:
                selectItem();
                return true;
            default:
                return false;
        }
    }

    public com.navdy.hud.app.manager.InputManager.IInputHandler nextHandler() {
        return null;
    }

    /* access modifiers changed from: 0000 */
    public void runQueuedOperation() {
        if (this.operationQueue.size() > 0) {
            com.navdy.hud.app.ui.component.carousel.CarouselLayout.OperationInfo operationInfo = (com.navdy.hud.app.ui.component.carousel.CarouselLayout.OperationInfo) this.operationQueue.peek();
            if (operationInfo.count == 1) {
                this.operationQueue.remove();
            } else {
                operationInfo.count--;
            }
            this.handler.post(operationInfo.runnable);
            return;
        }
        if (this.animator instanceof com.navdy.hud.app.ui.component.carousel.FastScrollAnimator) {
            com.navdy.hud.app.ui.component.carousel.FastScrollAnimator fastScrollAnimator = (com.navdy.hud.app.ui.component.carousel.FastScrollAnimator) this.animator;
            if (fastScrollAnimator.isEndPending()) {
                fastScrollAnimator.endAnimation();
                return;
            }
        }
        this.lastScrollAnimationOperation = null;
        this.animationRunning = false;
    }

    public void clearOperationQueue() {
        this.operationQueue.clear();
    }

    public void notifyDatasetChanged(java.util.List<com.navdy.hud.app.ui.component.carousel.Carousel.Model> list, int selectedItem) {
        this.model = list;
        setCurrentItem(selectedItem, true);
    }

    public void reload() {
        setCurrentItem(this.currentItem, true);
    }

    /* access modifiers changed from: 0000 */
    public boolean isAnimationPending() {
        if (this.operationQueue.size() <= 0 || ((com.navdy.hud.app.ui.component.carousel.CarouselLayout.OperationInfo) this.operationQueue.peek()).type != this.lastScrollAnimationOperation) {
            return false;
        }
        return true;
    }

    public boolean isAnimationEndPending() {
        if (this.animator instanceof com.navdy.hud.app.ui.component.carousel.FastScrollAnimator) {
            return ((com.navdy.hud.app.ui.component.carousel.FastScrollAnimator) this.animator).isEndPending();
        }
        return false;
    }
}
