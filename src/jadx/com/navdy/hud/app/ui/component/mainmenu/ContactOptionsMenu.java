package com.navdy.hud.app.ui.component.mainmenu;

class ContactOptionsMenu implements com.navdy.hud.app.ui.component.mainmenu.IMenu {
    private static final com.navdy.hud.app.ui.component.vlist.VerticalList.Model back;
    private static final int backColor = resources.getColor(com.navdy.hud.app.R.color.mm_back);
    private static final int callColor = resources.getColor(com.navdy.hud.app.R.color.mm_contacts);
    private static final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.mainmenu.ContactOptionsMenu.class);
    private static final int messageColor = resources.getColor(com.navdy.hud.app.R.color.share_location);
    private static final android.content.res.Resources resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
    private static final int shareLocationColor = resources.getColor(com.navdy.hud.app.R.color.share_location);
    private static final int shareTripLocationColor = resources.getColor(com.navdy.hud.app.R.color.mm_active_trip);
    private int backSelection;
    private int backSelectionId;
    /* access modifiers changed from: private */
    public final com.squareup.otto.Bus bus;
    private java.util.List<com.navdy.hud.app.ui.component.vlist.VerticalList.Model> cachedList;
    private final java.util.List<com.navdy.hud.app.framework.contacts.Contact> contacts;
    private boolean hasScrollModel;
    private com.navdy.hud.app.ui.component.mainmenu.MessagePickerMenu messagePickerMenu;
    /* access modifiers changed from: private */
    public final java.lang.String notifId;
    private com.navdy.hud.app.framework.contacts.Contact numberPicked;
    private final com.navdy.hud.app.ui.component.mainmenu.IMenu parent;
    /* access modifiers changed from: private */
    public final com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.Presenter presenter;
    private final com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent vscrollComponent;

    class Anon1 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.hud.app.framework.contacts.Contact val$contact;

        /* renamed from: com.navdy.hud.app.ui.component.mainmenu.ContactOptionsMenu$Anon1$Anon1 reason: collision with other inner class name */
        class C0033Anon1 implements java.lang.Runnable {
            C0033Anon1() {
            }

            public void run() {
                com.navdy.hud.app.ui.component.mainmenu.ContactOptionsMenu.this.makeCall(com.navdy.hud.app.ui.component.mainmenu.ContactOptionsMenu.Anon1.this.val$contact);
            }
        }

        Anon1(com.navdy.hud.app.framework.contacts.Contact contact) {
            this.val$contact = contact;
        }

        public void run() {
            com.navdy.hud.app.ui.component.mainmenu.ContactOptionsMenu.this.presenter.close(new com.navdy.hud.app.ui.component.mainmenu.ContactOptionsMenu.Anon1.C0033Anon1());
        }
    }

    class Anon2 implements com.navdy.hud.app.ui.component.mainmenu.NumberPickerMenu.Callback {
        Anon2() {
        }

        public void selected(@org.jetbrains.annotations.NotNull com.navdy.hud.app.framework.contacts.Contact selectedContact) {
            com.navdy.hud.app.ui.component.mainmenu.ContactOptionsMenu.this.makeCall(selectedContact);
        }
    }

    class Anon3 implements com.navdy.hud.app.ui.component.mainmenu.NumberPickerMenu.Callback {
        Anon3() {
        }

        public void selected(@org.jetbrains.annotations.NotNull com.navdy.hud.app.framework.contacts.Contact selectedContact) {
            com.navdy.hud.app.ui.component.mainmenu.ContactOptionsMenu.this.launchShareTripMenu(selectedContact);
        }
    }

    class Anon4 implements com.navdy.hud.app.ui.component.mainmenu.NumberPickerMenu.Callback {
        Anon4() {
        }

        public void selected(@org.jetbrains.annotations.NotNull com.navdy.hud.app.framework.contacts.Contact selectedContact) {
            com.navdy.hud.app.ui.component.mainmenu.ContactOptionsMenu.this.launchShareTripMenu(selectedContact);
        }
    }

    class Anon5 implements com.navdy.hud.app.ui.component.mainmenu.NumberPickerMenu.Callback {
        Anon5() {
        }

        public void selected(@org.jetbrains.annotations.NotNull com.navdy.hud.app.framework.contacts.Contact selectedContact) {
        }
    }

    class Anon6 implements java.lang.Runnable {
        final /* synthetic */ android.os.Bundle val$args;
        final /* synthetic */ com.navdy.hud.app.framework.contacts.Contact val$contact;

        Anon6(android.os.Bundle bundle, com.navdy.hud.app.framework.contacts.Contact contact) {
            this.val$args = bundle;
            this.val$contact = contact;
        }

        public void run() {
            com.navdy.hud.app.ui.component.mainmenu.ContactOptionsMenu.this.bus.post(new com.navdy.hud.app.event.ShowScreenWithArgs(com.navdy.service.library.events.ui.Screen.SCREEN_DESTINATION_PICKER, this.val$args, new com.navdy.hud.app.ui.component.mainmenu.ShareTripMenu(this.val$contact, com.navdy.hud.app.ui.component.mainmenu.ContactOptionsMenu.this.notifId), false));
        }
    }

    static {
        java.lang.String title = resources.getString(com.navdy.hud.app.R.string.back);
        int fluctuatorColor = backColor;
        back = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.menu_back, com.navdy.hud.app.R.drawable.icon_mm_back, fluctuatorColor, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, fluctuatorColor, title, null);
    }

    ContactOptionsMenu(java.util.List<com.navdy.hud.app.framework.contacts.Contact> contacts2, com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent vscrollComponent2, com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.Presenter presenter2, com.navdy.hud.app.ui.component.mainmenu.IMenu parent2, com.squareup.otto.Bus bus2) {
        this(contacts2, null, vscrollComponent2, presenter2, parent2, bus2);
    }

    ContactOptionsMenu(java.util.List<com.navdy.hud.app.framework.contacts.Contact> contacts2, java.lang.String notifId2, com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent vscrollComponent2, com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.Presenter presenter2, com.navdy.hud.app.ui.component.mainmenu.IMenu parent2, com.squareup.otto.Bus bus2) {
        this.contacts = contacts2;
        this.notifId = notifId2;
        this.vscrollComponent = vscrollComponent2;
        this.presenter = presenter2;
        this.parent = parent2;
        this.bus = bus2;
    }

    public java.util.List<com.navdy.hud.app.ui.component.vlist.VerticalList.Model> getItems() {
        if (this.cachedList != null) {
            return this.cachedList;
        }
        java.util.ArrayList arrayList = new java.util.ArrayList();
        if (this.notifId == null) {
            arrayList.add(back);
        } else {
            arrayList.add(com.navdy.hud.app.ui.component.vlist.viewholder.TitleViewHolder.buildModel(resources.getString(com.navdy.hud.app.R.string.pick_reply)));
        }
        arrayList.add(com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.menu_call, com.navdy.hud.app.R.drawable.icon_mm_contacts_2, callColor, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, callColor, resources.getString(com.navdy.hud.app.R.string.call), null));
        boolean glympseManagerSynced = com.navdy.hud.app.framework.glympse.GlympseManager.getInstance().isSynced();
        logger.v("glympse synced:" + glympseManagerSynced);
        if (com.navdy.hud.app.maps.here.HereMapsManager.getInstance().isInitialized() && glympseManagerSynced) {
            if (com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().isNavigationModeOn()) {
                arrayList.add(com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.menu_share_trip, com.navdy.hud.app.R.drawable.icon_badge_active_trip, shareTripLocationColor, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, shareTripLocationColor, resources.getString(com.navdy.hud.app.R.string.share_trip), null));
            } else {
                arrayList.add(com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.menu_share_location, com.navdy.hud.app.R.drawable.icon_share_location, shareLocationColor, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, shareLocationColor, resources.getString(com.navdy.hud.app.R.string.share_location), null));
            }
        }
        boolean addMessage = false;
        if (com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getRemoteDeviceInfo() != null) {
            switch (com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getRemoteDeviceInfo().platform) {
                case PLATFORM_Android:
                    addMessage = true;
                    break;
                case PLATFORM_iOS:
                    if (com.navdy.hud.app.ui.component.UISettings.supportsIosSms()) {
                        addMessage = true;
                        break;
                    }
                    break;
            }
        }
        if (addMessage) {
            arrayList.add(com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.menu_message, com.navdy.hud.app.R.drawable.icon_message, messageColor, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, messageColor, resources.getString(com.navdy.hud.app.R.string.message), null));
        }
        this.cachedList = arrayList;
        return arrayList;
    }

    public int getInitialSelection() {
        return (this.cachedList == null || this.cachedList.isEmpty() || this.notifId != null) ? 0 : 1;
    }

    public com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex getScrollIndex() {
        return null;
    }

    public void setBackSelectionPos(int n) {
        this.backSelection = n;
    }

    public void setBackSelectionId(int id) {
        this.backSelectionId = id;
    }

    public void setSelectedIcon() {
        com.navdy.hud.app.framework.contacts.Contact contact = (com.navdy.hud.app.framework.contacts.Contact) this.contacts.get(0);
        if (this.contacts.size() == 1) {
            android.graphics.Bitmap bitmap = com.navdy.hud.app.util.picasso.PicassoUtil.getBitmapfromCache(com.navdy.hud.app.framework.contacts.PhoneImageDownloader.getInstance().getImagePath(contact.number, com.navdy.service.library.events.photo.PhotoType.PHOTO_CONTACT));
            if (bitmap != null) {
                this.vscrollComponent.setSelectedIconImage(bitmap);
            } else if (android.text.TextUtils.isEmpty(contact.name)) {
                this.vscrollComponent.setSelectedIconImage(com.navdy.hud.app.R.drawable.icon_user_bg_4, null, com.navdy.hud.app.ui.component.image.InitialsImageView.Style.LARGE);
            } else {
                this.vscrollComponent.setSelectedIconImage(com.navdy.hud.app.framework.contacts.ContactImageHelper.getInstance().getResourceId(contact.defaultImageIndex), contact.initials, com.navdy.hud.app.ui.component.image.InitialsImageView.Style.LARGE);
            }
        } else {
            com.navdy.hud.app.framework.contacts.ContactImageHelper contactImageHelper = com.navdy.hud.app.framework.contacts.ContactImageHelper.getInstance();
            this.vscrollComponent.setSelectedIconImage(contactImageHelper.getResourceId(contactImageHelper.getContactImageIndex(contact.name)), contact.initials, com.navdy.hud.app.ui.component.image.InitialsImageView.Style.LARGE);
        }
        if (android.text.TextUtils.isEmpty(contact.name)) {
            this.vscrollComponent.selectedText.setText(contact.formattedNumber);
        } else if (this.contacts.size() != 1 || android.text.TextUtils.isEmpty(contact.numberTypeStr)) {
            this.vscrollComponent.selectedText.setText(contact.name);
        } else {
            this.vscrollComponent.selectedText.setText(contact.name + com.navdy.hud.app.framework.glance.GlanceConstants.NEWLINE + contact.numberTypeStr);
        }
    }

    public boolean selectItem(com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState selection) {
        logger.v("select id:" + selection.id + " pos:" + selection.pos);
        int i = selection.pos;
        com.navdy.hud.app.framework.contacts.Contact contact = (com.navdy.hud.app.framework.contacts.Contact) this.contacts.get(0);
        switch (selection.id) {
            case com.navdy.hud.app.R.id.menu_back /*2131624007*/:
                logger.v("back");
                com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("back");
                this.presenter.loadMenu(this.parent, com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel.BACK_TO_PARENT, this.backSelection, this.backSelectionId, this.hasScrollModel);
                this.backSelectionId = 0;
                break;
            case com.navdy.hud.app.R.id.menu_call /*2131624008*/:
                logger.v("call contact");
                if (this.contacts.size() != 1) {
                    this.presenter.loadMenu(new com.navdy.hud.app.ui.component.mainmenu.NumberPickerMenu(this.vscrollComponent, this.presenter, this, com.navdy.hud.app.ui.component.mainmenu.NumberPickerMenu.Mode.CALL, this.contacts, null, com.navdy.hud.app.R.drawable.icon_mm_contacts_2, callColor, new com.navdy.hud.app.ui.component.mainmenu.ContactOptionsMenu.Anon2()), com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel.SUB_LEVEL, selection.pos, 0);
                    break;
                } else {
                    com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("call_contact");
                    this.presenter.performSelectionAnimation(new com.navdy.hud.app.ui.component.mainmenu.ContactOptionsMenu.Anon1(contact));
                    break;
                }
            case com.navdy.hud.app.R.id.menu_message /*2131624010*/:
                logger.v("send message");
                com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("send_message");
                if (this.contacts.size() != 1) {
                    this.presenter.loadMenu(new com.navdy.hud.app.ui.component.mainmenu.NumberPickerMenu(this.vscrollComponent, this.presenter, this, com.navdy.hud.app.ui.component.mainmenu.NumberPickerMenu.Mode.MESSAGE, this.contacts, this.notifId, com.navdy.hud.app.R.drawable.icon_message, messageColor, new com.navdy.hud.app.ui.component.mainmenu.ContactOptionsMenu.Anon5()), com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel.SUB_LEVEL, selection.pos, 0);
                    break;
                } else {
                    if (this.messagePickerMenu == null) {
                        this.messagePickerMenu = new com.navdy.hud.app.ui.component.mainmenu.MessagePickerMenu(this.vscrollComponent, this.presenter, this, com.navdy.hud.app.framework.glance.GlanceConstants.getCannedMessages(), contact, this.notifId);
                    }
                    this.presenter.loadMenu(this.messagePickerMenu, com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel.SUB_LEVEL, selection.pos, 0);
                    break;
                }
            case com.navdy.hud.app.R.id.menu_share_location /*2131624014*/:
                logger.v("share location");
                com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("share_location");
                if (this.contacts.size() != 1) {
                    this.presenter.loadMenu(new com.navdy.hud.app.ui.component.mainmenu.NumberPickerMenu(this.vscrollComponent, this.presenter, this, com.navdy.hud.app.ui.component.mainmenu.NumberPickerMenu.Mode.SHARE_TRIP, this.contacts, null, com.navdy.hud.app.R.drawable.icon_share_location, shareLocationColor, new com.navdy.hud.app.ui.component.mainmenu.ContactOptionsMenu.Anon4()), com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel.SUB_LEVEL, selection.pos, 0);
                    break;
                } else {
                    launchShareTripMenu(contact);
                    break;
                }
            case com.navdy.hud.app.R.id.menu_share_trip /*2131624015*/:
                logger.v("share trip");
                com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("share_trip");
                if (this.contacts.size() != 1) {
                    this.presenter.loadMenu(new com.navdy.hud.app.ui.component.mainmenu.NumberPickerMenu(this.vscrollComponent, this.presenter, this, com.navdy.hud.app.ui.component.mainmenu.NumberPickerMenu.Mode.SHARE_TRIP, this.contacts, null, com.navdy.hud.app.R.drawable.icon_badge_active_trip, shareTripLocationColor, new com.navdy.hud.app.ui.component.mainmenu.ContactOptionsMenu.Anon3()), com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel.SUB_LEVEL, selection.pos, 0);
                    break;
                } else {
                    launchShareTripMenu(contact);
                    break;
                }
        }
        return true;
    }

    public com.navdy.hud.app.ui.component.mainmenu.IMenu.Menu getType() {
        return com.navdy.hud.app.ui.component.mainmenu.IMenu.Menu.CONTACT_OPTIONS;
    }

    public boolean isItemClickable(int id, int pos) {
        return true;
    }

    public com.navdy.hud.app.ui.component.vlist.VerticalList.Model getModelfromPos(int pos) {
        if (this.cachedList == null || this.cachedList.size() <= pos) {
            return null;
        }
        return (com.navdy.hud.app.ui.component.vlist.VerticalList.Model) this.cachedList.get(pos);
    }

    public boolean isBindCallsEnabled() {
        return false;
    }

    public void onBindToView(com.navdy.hud.app.ui.component.vlist.VerticalList.Model model, android.view.View view, int pos, com.navdy.hud.app.ui.component.vlist.VerticalList.ModelState state) {
    }

    public com.navdy.hud.app.ui.component.mainmenu.IMenu getChildMenu(com.navdy.hud.app.ui.component.mainmenu.IMenu parent2, java.lang.String args, java.lang.String path) {
        return null;
    }

    public void onUnload(com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel level) {
    }

    public void onItemSelected(com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState selection) {
    }

    public void onScrollIdle() {
    }

    public void onFastScrollStart() {
    }

    public void onFastScrollEnd() {
    }

    public void showToolTip() {
    }

    public boolean isFirstItemEmpty() {
        return this.notifId == null;
    }

    /* access modifiers changed from: private */
    public void launchShareTripMenu(com.navdy.hud.app.framework.contacts.Contact contact) {
        android.os.Bundle args = new android.os.Bundle();
        args.putInt(com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.PICKER_DESTINATION_ICON, com.navdy.hud.app.R.drawable.icon_pin_dot_destination_blue);
        args.putInt(com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.PICKER_LEFT_ICON, com.navdy.hud.app.R.drawable.icon_message);
        args.putInt(com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.PICKER_LEFT_ICON_BKCOLOR, resources.getColor(com.navdy.hud.app.R.color.share_location_trip_color));
        com.here.android.mpa.common.GeoCoordinate geoCoordinate = com.navdy.hud.app.maps.here.HereMapsManager.getInstance().getLocationFixManager().getLastGeoCoordinate();
        com.navdy.service.library.events.location.Coordinate destination = null;
        com.navdy.service.library.events.navigation.NavigationRouteRequest request = com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().getCurrentNavigationRouteRequest();
        if (request != null) {
            destination = request.destination;
        }
        args.putBoolean(com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.PICKER_SHOW_ROUTE_MAP, true);
        if (geoCoordinate != null) {
            args.putDouble(com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.PICKER_MAP_START_LAT, geoCoordinate.getLatitude());
            args.putDouble(com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.PICKER_MAP_START_LNG, geoCoordinate.getLongitude());
        }
        java.lang.String title = getContactName(contact);
        if (destination != null) {
            args.putDouble(com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.PICKER_MAP_END_LAT, destination.latitude.doubleValue());
            args.putDouble(com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.PICKER_MAP_END_LNG, destination.longitude.doubleValue());
            args.putString(com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.PICKER_TITLE, resources.getString(com.navdy.hud.app.R.string.share_your_trip_with, new java.lang.Object[]{title}));
        } else {
            args.putString(com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.PICKER_TITLE, resources.getString(com.navdy.hud.app.R.string.share_your_location_with, new java.lang.Object[]{title}));
        }
        java.lang.String[] messages = com.navdy.hud.app.framework.glympse.GlympseManager.getInstance().getMessages();
        com.navdy.hud.app.ui.component.destination.DestinationParcelable[] destinationParcelables = new com.navdy.hud.app.ui.component.destination.DestinationParcelable[messages.length];
        java.lang.String currentRouteId = com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().getCurrentRouteId();
        for (int i = 0; i < messages.length; i++) {
            destinationParcelables[i] = getDestinationParcelable(messages[i]);
            if (currentRouteId != null) {
                destinationParcelables[i].setRouteId(currentRouteId);
            }
        }
        args.putParcelableArray(com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.PICKER_DESTINATIONS, destinationParcelables);
        this.presenter.close(new com.navdy.hud.app.ui.component.mainmenu.ContactOptionsMenu.Anon6(args, contact));
    }

    private com.navdy.hud.app.ui.component.destination.DestinationParcelable getDestinationParcelable(java.lang.String message) {
        return new com.navdy.hud.app.ui.component.destination.DestinationParcelable(0, message, null, false, null, false, null, 0.0d, 0.0d, 0.0d, 0.0d, com.navdy.hud.app.R.drawable.icon_message, 0, shareTripLocationColor, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, com.navdy.hud.app.ui.component.destination.DestinationParcelable.DestinationType.NONE, null);
    }

    private java.lang.String getContactName(com.navdy.hud.app.framework.contacts.Contact contact) {
        if (!android.text.TextUtils.isEmpty(contact.name)) {
            return contact.name;
        }
        if (!android.text.TextUtils.isEmpty(contact.formattedNumber)) {
            return contact.formattedNumber;
        }
        return "";
    }

    private void removeNotification() {
        if (this.notifId != null) {
            com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().removeNotification(this.notifId);
        }
    }

    /* access modifiers changed from: private */
    public void makeCall(com.navdy.hud.app.framework.contacts.Contact contact) {
        removeNotification();
        com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getCallManager().dial(contact.number, null, contact.name);
    }

    public void setScrollModel(boolean b) {
        this.hasScrollModel = b;
    }
}
