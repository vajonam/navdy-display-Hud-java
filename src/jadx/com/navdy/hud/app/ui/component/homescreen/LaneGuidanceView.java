package com.navdy.hud.app.ui.component.homescreen;

public class LaneGuidanceView extends android.widget.LinearLayout implements com.navdy.hud.app.ui.component.homescreen.IHomeScreenLifecycle {
    private com.squareup.otto.Bus bus;
    private int iconH;
    private int iconW;
    private com.navdy.hud.app.maps.MapEvents.DisplayTrafficLaneInfo lastEvent;
    private com.navdy.service.library.log.Logger logger;
    private int mapIconIndicatorBottomMarginWithLane;
    private boolean needsMeasurement;
    private boolean paused;
    private int separatorColor;
    private int separatorH;
    private int separatorMargin;
    private int separatorW;
    private com.navdy.hud.app.ui.framework.UIStateManager uiStateManager;

    public LaneGuidanceView(android.content.Context context) {
        this(context, null);
    }

    public LaneGuidanceView(android.content.Context context, android.util.AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public LaneGuidanceView(android.content.Context context, android.util.AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        if (this.needsMeasurement) {
            int w = getMeasuredWidth();
            if (w > 0) {
                setX((float) (((com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.transformCenterIconWidth / 2) + getMapIconX(null)) - (w / 2)));
                this.needsMeasurement = false;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        this.logger = com.navdy.hud.app.ui.component.homescreen.HomeScreenView.sLogger;
        super.onFinishInflate();
        setY((float) com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.laneGuidanceY);
        com.navdy.hud.app.manager.RemoteDeviceManager remoteDeviceManager = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance();
        this.bus = remoteDeviceManager.getBus();
        this.uiStateManager = remoteDeviceManager.getUiStateManager();
        android.content.res.Resources resources = getContext().getResources();
        this.iconH = resources.getDimensionPixelSize(com.navdy.hud.app.R.dimen.lane_guidance_icon_h);
        this.iconW = resources.getDimensionPixelSize(com.navdy.hud.app.R.dimen.lane_guidance_icon_w);
        this.separatorH = resources.getDimensionPixelSize(com.navdy.hud.app.R.dimen.lane_guidance_separator_h);
        this.separatorW = resources.getDimensionPixelSize(com.navdy.hud.app.R.dimen.lane_guidance_separator_w);
        this.separatorMargin = resources.getDimensionPixelSize(com.navdy.hud.app.R.dimen.lane_guidance_separator_margin);
        this.separatorColor = resources.getColor(17170443);
        this.mapIconIndicatorBottomMarginWithLane = resources.getDimensionPixelSize(com.navdy.hud.app.R.dimen.map_icon_indicator_bottom_margin_with_lane);
    }

    public void init(com.navdy.hud.app.ui.component.homescreen.HomeScreenView homeScreenView) {
        this.bus.register(this);
    }

    public void setView(com.navdy.hud.app.view.MainView.CustomAnimationMode mode) {
    }

    public void getCustomAnimator(com.navdy.hud.app.view.MainView.CustomAnimationMode mode, android.animation.AnimatorSet.Builder mainBuilder) {
        if (getVisibility() == 0 && getMeasuredWidth() != 0) {
            int w = getMeasuredWidth();
            int mapIconX = getMapIconX(mode);
            mainBuilder.with(com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getXPositionAnimator(this, (float) (((com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.transformCenterIconWidth / 2) + mapIconX) - (w / 2))));
            mainBuilder.with(com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getXPositionAnimator(this.uiStateManager.getHomescreenView().getLaneGuidanceIconIndicator(), (float) mapIconX));
        }
    }

    @com.squareup.otto.Subscribe
    public void onLaneInfoShow(com.navdy.hud.app.maps.MapEvents.DisplayTrafficLaneInfo event) {
        if (this.uiStateManager.getHomescreenView().getDisplayMode() != com.navdy.hud.app.ui.component.homescreen.HomeScreen.DisplayMode.MAP) {
            onHideLaneInfo(null);
            this.lastEvent = event;
            return;
        }
        this.logger.v("LaneGuidanceView:showLanes");
        this.lastEvent = event;
        if (!this.paused) {
            removeAllViews();
            android.content.Context context = getContext();
            int size = event.laneData.size();
            addSeparator(context, false, true);
            for (int i = 0; i < size; i++) {
                android.widget.ImageView imageView = new android.widget.ImageView(context);
                imageView.setLayoutParams(new android.widget.LinearLayout.LayoutParams(this.iconW, this.iconH));
                android.graphics.drawable.Drawable[] icons = ((com.navdy.hud.app.maps.MapEvents.LaneData) event.laneData.get(i)).icons;
                if (icons == null || icons.length <= 0) {
                    imageView.setImageResource(0);
                } else {
                    imageView.setImageDrawable(new android.graphics.drawable.LayerDrawable(icons));
                }
                addView(imageView);
                if (i < size - 1) {
                    addSeparator(context, true, true);
                }
            }
            addSeparator(context, true, false);
            showMapIconIndicator();
            this.needsMeasurement = true;
            setVisibility(0);
        }
    }

    @com.squareup.otto.Subscribe
    public void onHideLaneInfo(com.navdy.hud.app.maps.MapEvents.HideTrafficLaneInfo event) {
        this.lastEvent = null;
        this.needsMeasurement = false;
        if (getVisibility() == 0) {
            this.logger.v("LaneGuidanceView:hideLanes");
            hideMapIconIndicator();
            setVisibility(8);
            removeAllViews();
        }
    }

    private void addSeparator(android.content.Context context, boolean leftMargin, boolean rightMargin) {
        android.view.View separator = new android.view.View(context);
        android.widget.LinearLayout.LayoutParams lytParams = new android.widget.LinearLayout.LayoutParams(this.separatorW, this.separatorH);
        lytParams.gravity = 80;
        if (leftMargin) {
            lytParams.leftMargin = this.separatorMargin;
        }
        if (rightMargin) {
            lytParams.rightMargin = this.separatorMargin;
        }
        separator.setLayoutParams(lytParams);
        separator.setBackgroundColor(this.separatorColor);
        addView(separator, lytParams);
    }

    private void showMapIconIndicator() {
        android.widget.ImageView view = this.uiStateManager.getHomescreenView().getLaneGuidanceIconIndicator();
        view.setX((float) getMapIconX(null));
        ((android.view.ViewGroup.MarginLayoutParams) view.getLayoutParams()).bottomMargin = this.mapIconIndicatorBottomMarginWithLane;
        view.setVisibility(0);
    }

    private void hideMapIconIndicator() {
        this.uiStateManager.getHomescreenView().getLaneGuidanceIconIndicator().setVisibility(8);
    }

    private int getMapIconX(com.navdy.hud.app.view.MainView.CustomAnimationMode mode) {
        if (mode != null) {
            switch (mode) {
                case SHRINK_LEFT:
                    return com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.iconIndicatorShrinkLeft_X;
                case EXPAND:
                    return com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.iconIndicatorX;
                default:
                    return 0;
            }
        } else {
            com.navdy.hud.app.ui.activity.Main rootScreen = this.uiStateManager.getRootScreen();
            if (rootScreen.isNotificationViewShowing() || rootScreen.isNotificationExpanding()) {
                return com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.iconIndicatorShrinkLeft_X;
            }
            return com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.iconIndicatorX;
        }
    }

    public void showLastEvent() {
        if (this.lastEvent != null) {
            onLaneInfoShow(this.lastEvent);
        }
    }

    public void onPause() {
        if (!this.paused) {
            this.paused = true;
            this.logger.v("::onPause:lane");
        }
    }

    public void onResume() {
        if (this.paused) {
            this.paused = false;
            this.logger.v("::onResume:lane");
            if (this.uiStateManager.getHomescreenView().getDisplayMode() == com.navdy.hud.app.ui.component.homescreen.HomeScreen.DisplayMode.MAP) {
                com.navdy.hud.app.maps.MapEvents.DisplayTrafficLaneInfo event = this.lastEvent;
                if (event != null) {
                    this.lastEvent = null;
                    onLaneInfoShow(event);
                    this.logger.v("::onResume:shown last lane");
                }
            }
        }
    }
}
