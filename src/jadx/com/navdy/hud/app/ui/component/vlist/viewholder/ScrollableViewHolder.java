package com.navdy.hud.app.ui.component.vlist.viewholder;

public class ScrollableViewHolder extends com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder {
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.vlist.viewholder.ScrollableViewHolder.class);

    public static com.navdy.hud.app.ui.component.vlist.VerticalList.Model buildModel(int layoutId) {
        com.navdy.hud.app.ui.component.vlist.VerticalList.Model model = new com.navdy.hud.app.ui.component.vlist.VerticalList.Model();
        model.type = com.navdy.hud.app.ui.component.vlist.VerticalList.ModelType.SCROLL_CONTENT;
        model.id = com.navdy.hud.app.R.id.vlist_scroll_item;
        model.scrollItemLayoutId = layoutId;
        return model;
    }

    public static com.navdy.hud.app.ui.component.vlist.viewholder.ScrollableViewHolder buildViewHolder(android.view.ViewGroup parent, com.navdy.hud.app.ui.component.vlist.VerticalList vlist, android.os.Handler handler) {
        return new com.navdy.hud.app.ui.component.vlist.viewholder.ScrollableViewHolder((android.view.ViewGroup) android.view.LayoutInflater.from(parent.getContext()).inflate(com.navdy.hud.app.R.layout.vlist_scroll_item, parent, false), vlist, handler);
    }

    public ScrollableViewHolder(android.view.ViewGroup layout, com.navdy.hud.app.ui.component.vlist.VerticalList vlist, android.os.Handler handler) {
        super(layout, vlist, handler);
    }

    public com.navdy.hud.app.ui.component.vlist.VerticalList.ModelType getModelType() {
        return com.navdy.hud.app.ui.component.vlist.VerticalList.ModelType.SCROLL_CONTENT;
    }

    public void setItemState(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.State state, com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.AnimationType animation, int duration, boolean startFluctuator) {
    }

    public void preBind(com.navdy.hud.app.ui.component.vlist.VerticalList.Model model, com.navdy.hud.app.ui.component.vlist.VerticalList.ModelState modelState) {
        if (this.layout.getChildCount() == 0) {
            this.layout.addView((android.view.ViewGroup) android.view.LayoutInflater.from(this.layout.getContext()).inflate(model.scrollItemLayoutId, this.layout, false), new android.widget.FrameLayout.LayoutParams(-1, -2));
        }
    }

    public void bind(com.navdy.hud.app.ui.component.vlist.VerticalList.Model model, com.navdy.hud.app.ui.component.vlist.VerticalList.ModelState modelState) {
    }

    public void clearAnimation() {
    }

    public void select(com.navdy.hud.app.ui.component.vlist.VerticalList.Model model, int pos, int duration) {
    }

    public void copyAndPosition(android.widget.ImageView imageC, android.widget.TextView titleC, android.widget.TextView subTitleC, android.widget.TextView subTitle2C, boolean setImage) {
    }
}
