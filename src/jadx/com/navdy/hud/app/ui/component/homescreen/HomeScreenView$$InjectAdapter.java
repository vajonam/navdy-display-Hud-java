package com.navdy.hud.app.ui.component.homescreen;

public final class HomeScreenView$$InjectAdapter extends dagger.internal.Binding<com.navdy.hud.app.ui.component.homescreen.HomeScreenView> implements dagger.MembersInjector<com.navdy.hud.app.ui.component.homescreen.HomeScreenView> {
    private dagger.internal.Binding<com.squareup.otto.Bus> bus;
    private dagger.internal.Binding<android.content.SharedPreferences> globalPreferences;
    private dagger.internal.Binding<com.navdy.hud.app.ui.component.homescreen.HomeScreen.Presenter> presenter;

    public HomeScreenView$$InjectAdapter() {
        super(null, "members/com.navdy.hud.app.ui.component.homescreen.HomeScreenView", false, com.navdy.hud.app.ui.component.homescreen.HomeScreenView.class);
    }

    public void attach(dagger.internal.Linker linker) {
        this.presenter = linker.requestBinding("com.navdy.hud.app.ui.component.homescreen.HomeScreen$Presenter", com.navdy.hud.app.ui.component.homescreen.HomeScreenView.class, getClass().getClassLoader());
        this.bus = linker.requestBinding("com.squareup.otto.Bus", com.navdy.hud.app.ui.component.homescreen.HomeScreenView.class, getClass().getClassLoader());
        this.globalPreferences = linker.requestBinding("android.content.SharedPreferences", com.navdy.hud.app.ui.component.homescreen.HomeScreenView.class, getClass().getClassLoader());
    }

    public void getDependencies(java.util.Set<dagger.internal.Binding<?>> set, java.util.Set<dagger.internal.Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.presenter);
        injectMembersBindings.add(this.bus);
        injectMembersBindings.add(this.globalPreferences);
    }

    public void injectMembers(com.navdy.hud.app.ui.component.homescreen.HomeScreenView object) {
        object.presenter = (com.navdy.hud.app.ui.component.homescreen.HomeScreen.Presenter) this.presenter.get();
        object.bus = (com.squareup.otto.Bus) this.bus.get();
        object.globalPreferences = (android.content.SharedPreferences) this.globalPreferences.get();
    }
}
