package com.navdy.hud.app.ui.component.destination;

public class DestinationConfirmationHelper {
    private static float[] FONT_SIZES = {30.0f, 26.0f, 26.0f, 22.0f};
    private static int[] MAX_LINES = {1, 1, 2, 2};

    public static void configure(com.navdy.hud.app.ui.component.ConfirmationLayout confirmationLayout, com.navdy.hud.app.ui.component.destination.DestinationParcelable destination) {
        configure(confirmationLayout, null, destination.destinationTitle, destination.destinationSubTitle, destination.destinationSubTitleFormatted, destination.iconUnselected == 0, destination.icon, destination.iconSelectedColor);
    }

    public static void configure(com.navdy.hud.app.ui.component.ConfirmationLayout confirmationLayout, com.navdy.hud.app.ui.component.vlist.VerticalList.Model model, com.navdy.hud.app.framework.destinations.Destination destination) {
        configure(confirmationLayout, destination, destination.destinationTitle, destination.destinationSubtitle, false, model.type == com.navdy.hud.app.ui.component.vlist.VerticalList.ModelType.ICON_BKCOLOR, model.icon, model.iconSelectedColor);
    }

    private static void setInitials(com.navdy.hud.app.framework.destinations.Destination destination, com.navdy.hud.app.ui.component.image.InitialsImageView initialsImageView) {
        com.navdy.hud.app.ui.component.image.InitialsImageView.Style style;
        if (destination == null || android.text.TextUtils.isEmpty(destination.initials)) {
            initialsImageView.setInitials(null, com.navdy.hud.app.ui.component.image.InitialsImageView.Style.DEFAULT);
            return;
        }
        if (destination.isInitialNumber) {
            if (destination.initials.length() <= 3) {
                style = com.navdy.hud.app.ui.component.image.InitialsImageView.Style.LARGE;
            } else {
                style = com.navdy.hud.app.ui.component.image.InitialsImageView.Style.MEDIUM;
            }
        } else if (destination.initials.length() <= 2) {
            style = com.navdy.hud.app.ui.component.image.InitialsImageView.Style.LARGE;
        } else {
            style = com.navdy.hud.app.ui.component.image.InitialsImageView.Style.MEDIUM;
        }
        initialsImageView.setInitials(destination.initials, style);
    }

    private static void setImage(com.navdy.hud.app.ui.component.ConfirmationLayout confirmationLayout, com.navdy.hud.app.framework.destinations.Destination destination, boolean hasBkColor, int icon, int iconBkColor) {
        if (hasBkColor) {
            confirmationLayout.screenImageIconBkColor.setIcon(icon, iconBkColor, null, 1.25f);
            confirmationLayout.screenImage.setVisibility(8);
            confirmationLayout.screenImageIconBkColor.setVisibility(0);
            return;
        }
        setInitials(destination, confirmationLayout.screenImage);
        confirmationLayout.screenImage.setImageResource(icon);
        confirmationLayout.screenImage.setVisibility(0);
        confirmationLayout.screenImageIconBkColor.setVisibility(8);
    }

    private static void configure(com.navdy.hud.app.ui.component.ConfirmationLayout confirmationLayout, com.navdy.hud.app.framework.destinations.Destination destination, java.lang.String title, java.lang.CharSequence subTitle, boolean subTitleFormatted, boolean hasBkColor, int icon, int iconBkColor) {
        setImage(confirmationLayout, destination, hasBkColor, icon, iconBkColor);
        confirmationLayout.screenTitle.setText(com.navdy.hud.app.R.string.destination_change_navigation_title);
        confirmationLayout.title1.setVisibility(8);
        android.widget.TextView titleTextView = confirmationLayout.title2;
        android.widget.TextView subTitleTextView = confirmationLayout.title3;
        com.navdy.hud.app.view.MaxWidthLinearLayout infoContainer = (com.navdy.hud.app.view.MaxWidthLinearLayout) confirmationLayout.findViewById(com.navdy.hud.app.R.id.infoContainer);
        int padding = infoContainer.getPaddingStart() + infoContainer.getPaddingEnd();
        int maxWidth = infoContainer.getMaxWidth();
        titleTextView.setText(title);
        int[] holder = new int[2];
        com.navdy.hud.app.util.ViewUtil.autosize(titleTextView, MAX_LINES, maxWidth - padding, FONT_SIZES, holder);
        titleTextView.setMaxLines(holder[1]);
        if (!android.text.TextUtils.isEmpty(subTitle) && subTitleFormatted) {
            subTitle = android.text.Html.fromHtml((java.lang.String) subTitle);
        }
        com.navdy.hud.app.util.ViewUtil.applyTextAndStyle(subTitleTextView, subTitle, 2, com.navdy.hud.app.R.style.destination_subtitle_two_line);
        titleTextView.setVisibility(0);
        subTitleTextView.setVisibility(0);
    }
}
