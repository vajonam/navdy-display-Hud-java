package com.navdy.hud.app.ui.component.homescreen;

public class SpeedView$$ViewInjector {
    public static void inject(butterknife.ButterKnife.Finder finder, com.navdy.hud.app.ui.component.homescreen.SpeedView target, java.lang.Object source) {
        target.speedView = (android.widget.TextView) finder.findRequiredView(source, com.navdy.hud.app.R.id.speedView, "field 'speedView'");
        target.speedUnitView = (android.widget.TextView) finder.findRequiredView(source, com.navdy.hud.app.R.id.speedUnitView, "field 'speedUnitView'");
    }

    public static void reset(com.navdy.hud.app.ui.component.homescreen.SpeedView target) {
        target.speedView = null;
        target.speedUnitView = null;
    }
}
