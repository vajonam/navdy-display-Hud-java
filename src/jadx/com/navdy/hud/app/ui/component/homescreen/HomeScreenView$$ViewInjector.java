package com.navdy.hud.app.ui.component.homescreen;

public class HomeScreenView$$ViewInjector {
    public static void inject(butterknife.ButterKnife.Finder finder, com.navdy.hud.app.ui.component.homescreen.HomeScreenView target, java.lang.Object source) {
        target.mapContainer = (com.navdy.hud.app.ui.component.homescreen.NavigationView) finder.findRequiredView(source, com.navdy.hud.app.R.id.map_container, "field 'mapContainer'");
        target.smartDashContainer = (com.navdy.hud.app.ui.component.homescreen.BaseSmartDashView) finder.findRequiredView(source, com.navdy.hud.app.R.id.smartDashContainer, "field 'smartDashContainer'");
        target.openMapRoadInfoContainer = (com.navdy.hud.app.ui.component.homescreen.OpenRoadView) finder.findRequiredView(source, com.navdy.hud.app.R.id.openMapRoadInfoContainer, "field 'openMapRoadInfoContainer'");
        target.tbtView = (com.navdy.hud.app.ui.component.tbt.TbtViewContainer) finder.findRequiredView(source, com.navdy.hud.app.R.id.activeMapRoadInfoContainer, "field 'tbtView'");
        target.recalcRouteContainer = (com.navdy.hud.app.ui.component.homescreen.RecalculatingView) finder.findRequiredView(source, com.navdy.hud.app.R.id.recalcRouteContainer, "field 'recalcRouteContainer'");
        target.etaClockView = (com.navdy.hud.app.ui.component.homescreen.EtaView) finder.findRequiredView(source, com.navdy.hud.app.R.id.activeEtaContainer, "field 'etaClockView'");
        target.navigationViewsContainer = (android.widget.RelativeLayout) finder.findRequiredView(source, com.navdy.hud.app.R.id.navigation_views_container, "field 'navigationViewsContainer'");
        target.laneGuidanceView = (com.navdy.hud.app.ui.component.homescreen.LaneGuidanceView) finder.findRequiredView(source, com.navdy.hud.app.R.id.laneGuidance, "field 'laneGuidanceView'");
        target.laneGuidanceIconIndicator = (android.widget.ImageView) finder.findRequiredView(source, com.navdy.hud.app.R.id.lane_guidance_map_icon_indicator, "field 'laneGuidanceIconIndicator'");
        target.mainscreenRightSection = (com.navdy.hud.app.ui.component.homescreen.HomeScreenRightSectionView) finder.findRequiredView(source, com.navdy.hud.app.R.id.mainscreenRightSection, "field 'mainscreenRightSection'");
        target.mapViewSpeedContainer = (android.view.ViewGroup) finder.findRequiredView(source, com.navdy.hud.app.R.id.map_view_speed_container, "field 'mapViewSpeedContainer'");
        target.speedView = (com.navdy.hud.app.ui.component.homescreen.SpeedView) finder.findRequiredView(source, com.navdy.hud.app.R.id.speedContainer, "field 'speedView'");
        target.speedLimitSignView = (com.navdy.hud.app.view.SpeedLimitSignView) finder.findRequiredView(source, com.navdy.hud.app.R.id.home_screen_speed_limit_sign, "field 'speedLimitSignView'");
        target.driveScoreWidgetView = (com.navdy.hud.app.view.DashboardWidgetView) finder.findRequiredView(source, com.navdy.hud.app.R.id.drive_score_events, "field 'driveScoreWidgetView'");
        target.mapMask = (android.widget.ImageView) finder.findRequiredView(source, com.navdy.hud.app.R.id.map_mask, "field 'mapMask'");
    }

    public static void reset(com.navdy.hud.app.ui.component.homescreen.HomeScreenView target) {
        target.mapContainer = null;
        target.smartDashContainer = null;
        target.openMapRoadInfoContainer = null;
        target.tbtView = null;
        target.recalcRouteContainer = null;
        target.etaClockView = null;
        target.navigationViewsContainer = null;
        target.laneGuidanceView = null;
        target.laneGuidanceIconIndicator = null;
        target.mainscreenRightSection = null;
        target.mapViewSpeedContainer = null;
        target.speedView = null;
        target.speedLimitSignView = null;
        target.driveScoreWidgetView = null;
        target.mapMask = null;
    }
}
