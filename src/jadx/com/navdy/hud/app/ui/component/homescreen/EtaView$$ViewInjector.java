package com.navdy.hud.app.ui.component.homescreen;

public class EtaView$$ViewInjector {
    public static void inject(butterknife.ButterKnife.Finder finder, com.navdy.hud.app.ui.component.homescreen.EtaView target, java.lang.Object source) {
        target.topTextView = (android.widget.TextView) finder.findRequiredView(source, com.navdy.hud.app.R.id.top_text_view, "field 'topTextView'");
        target.bottomTextView = (android.widget.TextView) finder.findRequiredView(source, com.navdy.hud.app.R.id.bottom_text_view, "field 'bottomTextView'");
    }

    public static void reset(com.navdy.hud.app.ui.component.homescreen.EtaView target) {
        target.topTextView = null;
        target.bottomTextView = null;
    }
}
