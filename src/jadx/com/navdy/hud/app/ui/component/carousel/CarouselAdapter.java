package com.navdy.hud.app.ui.component.carousel;

public class CarouselAdapter {
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.carousel.CarouselAdapter.class);
    com.navdy.hud.app.ui.component.carousel.CarouselLayout carousel;

    public CarouselAdapter(com.navdy.hud.app.ui.component.carousel.CarouselLayout carousel2) {
        this.carousel = carousel2;
    }

    public android.view.View getView(int position, android.view.View convertView, com.navdy.hud.app.ui.component.carousel.Carousel.ViewType type, int layoutId, int size) {
        android.view.View view;
        if (convertView != null) {
            convertView.setTag(com.navdy.hud.app.R.id.item_id, null);
        }
        com.navdy.hud.app.ui.component.carousel.Carousel.Model item = null;
        if (position >= 0 && position < this.carousel.model.size()) {
            item = (com.navdy.hud.app.ui.component.carousel.Carousel.Model) this.carousel.model.get(position);
        }
        if (convertView == null) {
            sLogger.v("create " + type + " view");
            view = this.carousel.inflater.inflate(layoutId, null);
            if (view instanceof com.navdy.hud.app.ui.component.image.CrossFadeImageView) {
                if (type == com.navdy.hud.app.ui.component.carousel.Carousel.ViewType.SIDE) {
                    ((com.navdy.hud.app.ui.component.image.CrossFadeImageView) view).inject(com.navdy.hud.app.ui.component.image.CrossFadeImageView.Mode.SMALL);
                } else if (type == com.navdy.hud.app.ui.component.carousel.Carousel.ViewType.MIDDLE_LEFT) {
                    ((com.navdy.hud.app.ui.component.image.CrossFadeImageView) view).inject(com.navdy.hud.app.ui.component.image.CrossFadeImageView.Mode.BIG);
                }
            }
        } else {
            view = convertView;
            if (view instanceof com.navdy.hud.app.ui.component.image.CrossFadeImageView) {
                if (type == com.navdy.hud.app.ui.component.carousel.Carousel.ViewType.SIDE) {
                    ((com.navdy.hud.app.ui.component.image.CrossFadeImageView) view).setMode(com.navdy.hud.app.ui.component.image.CrossFadeImageView.Mode.SMALL);
                } else if (type == com.navdy.hud.app.ui.component.carousel.Carousel.ViewType.MIDDLE_LEFT) {
                    ((com.navdy.hud.app.ui.component.image.CrossFadeImageView) view).setMode(com.navdy.hud.app.ui.component.image.CrossFadeImageView.Mode.BIG);
                }
            }
        }
        if (item != null) {
            view.setTag(com.navdy.hud.app.R.id.item_id, java.lang.Integer.valueOf(item.id));
            view.setVisibility(0);
            processView(type, item, view, position, size);
        } else {
            view.setVisibility(4);
            view.setTag(com.navdy.hud.app.R.id.item_id, null);
        }
        return view;
    }

    private void processView(com.navdy.hud.app.ui.component.carousel.Carousel.ViewType viewType, com.navdy.hud.app.ui.component.carousel.Carousel.Model item, android.view.View view, int position, int size) {
        if (viewType == com.navdy.hud.app.ui.component.carousel.Carousel.ViewType.MIDDLE_RIGHT) {
            processInfoView(item, view, position);
        } else {
            processImageView(viewType, item, view, position, size);
        }
    }

    private void processImageView(com.navdy.hud.app.ui.component.carousel.Carousel.ViewType viewType, com.navdy.hud.app.ui.component.carousel.Carousel.Model item, android.view.View imageView, int position, int size) {
        if (imageView instanceof com.navdy.hud.app.ui.component.image.CrossFadeImageView) {
            com.navdy.hud.app.ui.component.image.CrossFadeImageView crossFadeImageView = (com.navdy.hud.app.ui.component.image.CrossFadeImageView) imageView;
            com.navdy.hud.app.ui.component.image.InitialsImageView big = (com.navdy.hud.app.ui.component.image.InitialsImageView) crossFadeImageView.getBig();
            android.view.View small = crossFadeImageView.getSmall();
            big.setImage(item.largeImageRes, null, com.navdy.hud.app.ui.component.image.InitialsImageView.Style.DEFAULT);
            if (this.carousel.imageLytResourceId == com.navdy.hud.app.R.layout.crossfade_image_lyt) {
                ((com.navdy.hud.app.ui.component.image.InitialsImageView) small).setImage(item.smallImageRes, null, com.navdy.hud.app.ui.component.image.InitialsImageView.Style.DEFAULT);
            } else {
                ((com.navdy.hud.app.ui.component.image.ColorImageView) small).setColor(item.smallImageColor);
            }
        } else if (imageView instanceof com.navdy.hud.app.ui.component.image.InitialsImageView) {
            com.navdy.hud.app.ui.component.image.InitialsImageView initialsImageView = (com.navdy.hud.app.ui.component.image.InitialsImageView) imageView;
            if (viewType == com.navdy.hud.app.ui.component.carousel.Carousel.ViewType.MIDDLE_LEFT) {
                initialsImageView.setImage(item.largeImageRes, null, com.navdy.hud.app.ui.component.image.InitialsImageView.Style.DEFAULT);
            } else {
                initialsImageView.setImage(item.smallImageRes, null, com.navdy.hud.app.ui.component.image.InitialsImageView.Style.DEFAULT);
            }
        }
        if (this.carousel.viewProcessor != null) {
            try {
                if (viewType == com.navdy.hud.app.ui.component.carousel.Carousel.ViewType.MIDDLE_LEFT) {
                    this.carousel.viewProcessor.processLargeImageView(item, imageView, position, size, size);
                } else {
                    this.carousel.viewProcessor.processSmallImageView(item, imageView, position, size, size);
                }
            } catch (Throwable t) {
                sLogger.e(t);
            }
        }
    }

    private void processInfoView(com.navdy.hud.app.ui.component.carousel.Carousel.Model item, android.view.View view, int position) {
        if (item.infoMap != null) {
            android.view.ViewGroup viewGroup = (android.view.ViewGroup) view;
            int childCount = viewGroup.getChildCount();
            for (int i = 0; i < childCount; i++) {
                android.view.View child = viewGroup.getChildAt(i);
                if (child instanceof android.widget.TextView) {
                    java.lang.String text = (java.lang.String) item.infoMap.get(java.lang.Integer.valueOf(child.getId()));
                    if (text != null) {
                        ((android.widget.TextView) child).setText(text);
                        child.setVisibility(0);
                    } else {
                        child.setVisibility(8);
                    }
                }
            }
        }
        if (this.carousel.viewProcessor != null) {
            try {
                this.carousel.viewProcessor.processInfoView(item, view, position);
            } catch (Throwable t) {
                sLogger.e(t);
            }
        }
    }
}
