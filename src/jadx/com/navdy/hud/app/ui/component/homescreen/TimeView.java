package com.navdy.hud.app.ui.component.homescreen;

public class TimeView extends android.widget.RelativeLayout {
    public static final int CLOCK_UPDATE_INTERVAL = 30000;
    @butterknife.InjectView(2131624395)
    android.widget.TextView ampmTextView;
    private com.squareup.otto.Bus bus;
    @butterknife.InjectView(2131624162)
    android.widget.TextView dayTextView;
    private android.os.Handler handler;
    /* access modifiers changed from: private */
    public com.navdy.service.library.log.Logger logger;
    /* access modifiers changed from: private */
    public java.lang.Runnable runnable;
    private java.lang.StringBuilder stringBuilder;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.common.TimeHelper timeHelper;
    @butterknife.InjectView(2131624394)
    android.widget.TextView timeTextView;

    class Anon1 extends android.content.BroadcastReceiver {
        Anon1() {
        }

        public void onReceive(android.content.Context context, android.content.Intent intent) {
            if ("android.intent.action.TIMEZONE_CHANGED".equals(intent.getAction())) {
                com.navdy.hud.app.ui.component.homescreen.TimeView.this.logger.v("timezone change broadcast");
                com.navdy.hud.app.ui.component.homescreen.TimeView.this.timeHelper.updateLocale();
            }
        }
    }

    class Anon2 implements java.lang.Runnable {
        Anon2() {
        }

        public void run() {
            com.navdy.hud.app.ui.component.homescreen.TimeView.this.updateTime();
            com.navdy.hud.app.ui.component.homescreen.TimeView.this.postDelayed(com.navdy.hud.app.ui.component.homescreen.TimeView.this.runnable, 30000);
        }
    }

    public TimeView(android.content.Context context) {
        this(context, null);
    }

    public TimeView(android.content.Context context, android.util.AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TimeView(android.content.Context context, android.util.AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.stringBuilder = new java.lang.StringBuilder();
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        this.logger = com.navdy.hud.app.ui.component.homescreen.HomeScreenView.sLogger;
        super.onFinishInflate();
        butterknife.ButterKnife.inject((android.view.View) this);
        if (!isInEditMode()) {
            this.timeHelper = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getTimeHelper();
            updateTime();
            android.content.IntentFilter intentFilter = new android.content.IntentFilter();
            intentFilter.addAction("android.intent.action.TIMEZONE_CHANGED");
            com.navdy.hud.app.HudApplication.getAppContext().registerReceiver(new com.navdy.hud.app.ui.component.homescreen.TimeView.Anon1(), intentFilter);
            this.runnable = new com.navdy.hud.app.ui.component.homescreen.TimeView.Anon2();
            postDelayed(this.runnable, 30000);
            this.bus = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getBus();
            this.bus.register(this);
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.handler != null) {
            this.handler.removeCallbacks(this.runnable);
        }
    }

    @com.squareup.otto.Subscribe
    public void onTimeSettingsChange(com.navdy.hud.app.common.TimeHelper.UpdateClock event) {
        updateTime();
    }

    /* access modifiers changed from: private */
    public void updateTime() {
        this.dayTextView.setText(this.timeHelper.getDay());
        this.timeTextView.setText(this.timeHelper.formatTime(new java.util.Date(), this.stringBuilder));
        this.ampmTextView.setText(this.stringBuilder.toString());
    }

    public void setView(com.navdy.hud.app.view.MainView.CustomAnimationMode mode) {
    }
}
