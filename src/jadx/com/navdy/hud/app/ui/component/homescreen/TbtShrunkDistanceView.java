package com.navdy.hud.app.ui.component.homescreen;

public class TbtShrunkDistanceView extends android.widget.RelativeLayout {
    private static final java.lang.String TBT_SHRUNK_MODE = "persist.sys.tbtdistanceshrunk";
    private static final java.lang.String TBT_SHRUNK_MODE_DISTANCE_NUMBER = "distance_number";
    private static final java.lang.String TBT_SHRUNK_MODE_PROGRESS_BAR = "progress_bar";
    private com.navdy.service.library.log.Logger logger;
    private final com.navdy.hud.app.ui.component.homescreen.TbtShrunkDistanceView.Mode mode;
    @butterknife.InjectView(2131624392)
    android.view.View nowShrunkIcon;
    @butterknife.InjectView(2131624390)
    android.widget.ProgressBar progressBarShrunk;
    @butterknife.InjectView(2131624391)
    android.widget.TextView tbtShrunkDistance;

    private enum Mode {
        PROGRESS_BAR,
        DISTANCE_NUMBER
    }

    public TbtShrunkDistanceView(android.content.Context context) {
        this(context, null);
    }

    public TbtShrunkDistanceView(android.content.Context context, android.util.AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TbtShrunkDistanceView(android.content.Context context, android.util.AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if (com.navdy.hud.app.util.os.SystemProperties.get(TBT_SHRUNK_MODE, TBT_SHRUNK_MODE_PROGRESS_BAR).equals(TBT_SHRUNK_MODE_DISTANCE_NUMBER)) {
            this.mode = com.navdy.hud.app.ui.component.homescreen.TbtShrunkDistanceView.Mode.DISTANCE_NUMBER;
        } else {
            this.mode = com.navdy.hud.app.ui.component.homescreen.TbtShrunkDistanceView.Mode.PROGRESS_BAR;
        }
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        this.logger = com.navdy.hud.app.ui.component.homescreen.HomeScreenView.sLogger;
        super.onFinishInflate();
        butterknife.ButterKnife.inject((android.view.View) this);
        setY((float) com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadShrunkDistanceY);
        switch (this.mode) {
            case DISTANCE_NUMBER:
                this.progressBarShrunk.setVisibility(8);
                return;
            default:
                this.tbtShrunkDistance.setVisibility(8);
                return;
        }
    }

    public void setView(com.navdy.hud.app.view.MainView.CustomAnimationMode mode2) {
        switch (mode2) {
            case EXPAND:
                setX((float) com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadShrunkDistanceX);
                return;
            case SHRINK_LEFT:
                setX((float) com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadShrunkDistanceShrinkLeftX);
                return;
            default:
                return;
        }
    }

    public void setDistanceText(java.lang.String text) {
        this.tbtShrunkDistance.setText(text);
    }

    public void showNowIcon() {
        com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getFadeInAndScaleUpAnimator(this.nowShrunkIcon).setDuration(250).start();
        if (this.mode == com.navdy.hud.app.ui.component.homescreen.TbtShrunkDistanceView.Mode.DISTANCE_NUMBER) {
            this.tbtShrunkDistance.setVisibility(4);
        }
    }

    public void hideNowIcon() {
        com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getFadeOutAndScaleDownAnimator(this.nowShrunkIcon).setDuration(250).start();
        if (this.mode == com.navdy.hud.app.ui.component.homescreen.TbtShrunkDistanceView.Mode.DISTANCE_NUMBER) {
            this.tbtShrunkDistance.setVisibility(0);
        }
    }

    public void initProgressBar() {
        if (this.mode == com.navdy.hud.app.ui.component.homescreen.TbtShrunkDistanceView.Mode.PROGRESS_BAR) {
            setRegularBackground();
            com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.setWidth(this.progressBarShrunk, com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadShrunkProgressBarWidth);
            animateProgressBar(0.0d);
        }
    }

    public void hideProgressBar() {
        if (this.mode == com.navdy.hud.app.ui.component.homescreen.TbtShrunkDistanceView.Mode.PROGRESS_BAR) {
            com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.setWidth(this.progressBarShrunk, 0);
            setTransparentBackground();
        }
    }

    public void animateProgressBar(double progress) {
        if (this.mode == com.navdy.hud.app.ui.component.homescreen.TbtShrunkDistanceView.Mode.PROGRESS_BAR) {
            com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getProgressBarAnimator(this.progressBarShrunk, (int) (100.0d * progress)).setDuration(250).start();
        }
    }

    private void setRegularBackground() {
        setBackgroundResource(17170444);
    }

    private void setTransparentBackground() {
        setBackgroundResource(17170445);
    }
}
