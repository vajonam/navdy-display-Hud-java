package com.navdy.hud.app.ui.component.vlist.viewholder;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u0019\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\b\n\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016\u00a8\u0006\u0007"}, d2 = {"com/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$fluctuatorStartListener$Anon1", "Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;", "(Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;Landroid/os/Handler;)V", "onAnimationEnd", "", "animation", "Landroid/animation/Animator;", "app_hudDebug"}, k = 1, mv = {1, 1, 6})
/* compiled from: SwitchViewHolder.kt */
public final class SwitchViewHolder$fluctuatorStartListener$Anon1 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
    final /* synthetic */ android.os.Handler $handler;
    final /* synthetic */ com.navdy.hud.app.ui.component.vlist.viewholder.SwitchViewHolder this$Anon0;

    SwitchViewHolder$fluctuatorStartListener$Anon1(com.navdy.hud.app.ui.component.vlist.viewholder.SwitchViewHolder $outer, android.os.Handler $captured_local_variable$Anon1) {
        this.this$Anon0 = $outer;
        this.$handler = $captured_local_variable$Anon1;
    }

    public void onAnimationEnd(@org.jetbrains.annotations.NotNull android.animation.Animator animation) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(animation, "animation");
        this.this$Anon0.itemAnimatorSet = null;
        this.$handler.removeCallbacks(this.this$Anon0.fluctuatorRunnable);
        this.$handler.postDelayed(this.this$Anon0.fluctuatorRunnable, (long) this.this$Anon0.getHALO_DELAY_START_DURATION());
    }
}
