package com.navdy.hud.app.ui.component.mainmenu;

public class MainMenu implements com.navdy.hud.app.ui.component.mainmenu.IMenu {
    /* access modifiers changed from: private */
    public static final int ACTIVITY_TRAY_UPDATE = ((int) java.util.concurrent.TimeUnit.SECONDS.toMillis(1));
    /* access modifiers changed from: private */
    public static final int ETA_UPDATE_INTERVAL = ((int) java.util.concurrent.TimeUnit.SECONDS.toMillis(30));
    private static final java.lang.String activeTripTitle = resources.getString(com.navdy.hud.app.R.string.mm_active_trip);
    private static final java.util.ArrayList<java.lang.Integer> activityTrayIcon = new java.util.ArrayList<>();
    private static final java.util.ArrayList<java.lang.Integer> activityTrayIconId = new java.util.ArrayList<>();
    private static final java.util.ArrayList<java.lang.Integer> activityTraySelectedColor = new java.util.ArrayList<>();
    private static final java.util.ArrayList<java.lang.Integer> activityTrayUnSelectedColor = new java.util.ArrayList<>();
    public static final int bkColorUnselected = resources.getColor(com.navdy.hud.app.R.color.icon_bk_color_unselected);
    private static final com.navdy.hud.app.framework.phonecall.CallManager callManager = remoteDeviceManager.getCallManager();
    static final java.util.HashMap<java.lang.String, com.navdy.hud.app.ui.component.mainmenu.IMenu.Menu> childMenus = new java.util.HashMap<>();
    private static final com.navdy.hud.app.ui.component.vlist.VerticalList.Model connectPhone;
    private static final com.navdy.hud.app.ui.component.vlist.VerticalList.Model contacts;
    private static final java.lang.String dashTitle = resources.getString(com.navdy.hud.app.R.string.carousel_menu_smartdash_options);
    public static final com.navdy.hud.app.ui.component.vlist.VerticalList.FontInfo defaultFontInfo = com.navdy.hud.app.ui.component.vlist.VerticalList.getFontInfo(com.navdy.hud.app.ui.component.vlist.VerticalList.FontSize.FONT_SIZE_26);
    private static final int glancesColor = resources.getColor(com.navdy.hud.app.R.color.mm_glances);
    /* access modifiers changed from: private */
    public static final android.os.Handler handler = new android.os.Handler(android.os.Looper.getMainLooper());
    private static final java.lang.String mapTitle = resources.getString(com.navdy.hud.app.R.string.carousel_menu_map_options);
    private static final com.navdy.hud.app.ui.component.vlist.VerticalList.Model maps;
    private static final com.navdy.hud.app.ui.component.vlist.VerticalList.Model musicControl;
    /* access modifiers changed from: private */
    public static com.navdy.hud.app.manager.MusicManager musicManager = remoteDeviceManager.getMusicManager();
    private static final int nowPlayingColor = resources.getColor(com.navdy.hud.app.R.color.music_now_playing);
    private static final com.navdy.hud.app.ui.component.vlist.VerticalList.Model places;
    private static final com.navdy.hud.app.manager.RemoteDeviceManager remoteDeviceManager = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance();
    private static final android.content.res.Resources resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.mainmenu.MainMenu.class);
    private static final com.navdy.hud.app.ui.component.vlist.VerticalList.Model settings;
    private static final com.navdy.hud.app.ui.component.vlist.VerticalList.Model smartDash;
    private static final com.navdy.hud.app.ui.framework.UIStateManager uiStateManager = remoteDeviceManager.getUiStateManager();
    private static final com.navdy.hud.app.ui.component.vlist.VerticalList.Model voiceAssistanceGoogle;
    private static final com.navdy.hud.app.ui.component.vlist.VerticalList.Model voiceAssistanceSiri;
    private static final com.navdy.hud.app.ui.component.vlist.VerticalList.Model voiceSearch;
    private com.navdy.hud.app.ui.component.mainmenu.ActiveTripMenu activeTripMenu;
    private java.lang.Runnable activityTrayRunnable = new com.navdy.hud.app.ui.component.mainmenu.MainMenu.Anon2();
    /* access modifiers changed from: private */
    public boolean activityTrayRunning;
    private int activityTraySelection = -1;
    private int activityTraySelectionId = -1;
    /* access modifiers changed from: private */
    public com.navdy.service.library.events.audio.MusicTrackInfo addedTrackInfo;
    /* access modifiers changed from: private */
    public com.squareup.otto.Bus bus;
    private boolean busRegistered;
    private java.util.List<com.navdy.hud.app.ui.component.vlist.VerticalList.Model> cachedList;
    private com.navdy.hud.app.ui.component.mainmenu.ContactsMenu contactsMenu;
    /* access modifiers changed from: private */
    public int curToolTipId = -1;
    /* access modifiers changed from: private */
    public int curToolTipPos = -1;
    private java.lang.String currentTta;
    private java.lang.Runnable etaRunnable = new com.navdy.hud.app.ui.component.mainmenu.MainMenu.Anon1();
    private com.navdy.hud.app.ui.component.mainmenu.MainOptionsMenu mainOptionsMenu;
    private com.navdy.hud.app.ui.component.mainmenu.IMenu musicMenu;
    private com.navdy.hud.app.manager.MusicManager.MusicUpdateListener musicUpdateListener = new com.navdy.hud.app.ui.component.mainmenu.MainMenu.Anon3();
    private com.navdy.hud.app.ui.component.mainmenu.PlacesMenu placesMenu;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.Presenter presenter;
    private boolean registeredMusicCallback;
    private int selectedIndex;
    private com.navdy.hud.app.ui.component.mainmenu.SettingsMenu settingsMenu;
    private java.lang.String swVersion;
    private com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent vscrollComponent;

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            com.navdy.hud.app.ui.component.mainmenu.MainMenu.this.fillTta();
            com.navdy.hud.app.ui.component.mainmenu.MainMenu.handler.postDelayed(this, (long) com.navdy.hud.app.ui.component.mainmenu.MainMenu.ETA_UPDATE_INTERVAL);
        }
    }

    class Anon2 implements java.lang.Runnable {
        Anon2() {
        }

        public void run() {
            if (com.navdy.hud.app.ui.component.mainmenu.MainMenu.this.activityTrayRunning) {
                if (com.navdy.hud.app.ui.component.mainmenu.MainMenu.this.curToolTipPos < 0) {
                    com.navdy.hud.app.ui.component.mainmenu.MainMenu.this.activityTrayRunning = false;
                    return;
                }
                java.lang.String toolTip = com.navdy.hud.app.ui.component.mainmenu.MainMenu.this.getToolTipString(com.navdy.hud.app.R.id.main_menu_call);
                if (toolTip != null) {
                    com.navdy.hud.app.ui.component.mainmenu.MainMenu.this.presenter.showToolTip(com.navdy.hud.app.ui.component.mainmenu.MainMenu.this.curToolTipPos, toolTip);
                    com.navdy.hud.app.ui.component.mainmenu.MainMenu.handler.postDelayed(this, (long) com.navdy.hud.app.ui.component.mainmenu.MainMenu.ACTIVITY_TRAY_UPDATE);
                }
            }
        }
    }

    class Anon3 implements com.navdy.hud.app.manager.MusicManager.MusicUpdateListener {

        class Anon1 implements java.lang.Runnable {
            final /* synthetic */ com.navdy.service.library.events.audio.MusicTrackInfo val$trackInfo;

            Anon1(com.navdy.service.library.events.audio.MusicTrackInfo musicTrackInfo) {
                this.val$trackInfo = musicTrackInfo;
            }

            public void run() {
                com.navdy.hud.app.ui.component.mainmenu.MainMenu.this.addedTrackInfo = this.val$trackInfo;
                if (com.navdy.hud.app.ui.component.mainmenu.MainMenu.this.curToolTipId == com.navdy.hud.app.R.id.main_menu_now_playing && com.navdy.hud.app.ui.component.mainmenu.MainMenu.this.curToolTipPos >= 0) {
                    com.navdy.hud.app.ui.component.mainmenu.MainMenu.sLogger.v("update music tool tip");
                    java.lang.String str = com.navdy.hud.app.ui.component.mainmenu.MainMenu.this.getMusicTrackInfo();
                    if (str != null) {
                        com.navdy.hud.app.ui.component.mainmenu.MainMenu.this.presenter.showToolTip(com.navdy.hud.app.ui.component.mainmenu.MainMenu.this.curToolTipPos, str);
                    }
                }
            }
        }

        Anon3() {
        }

        public void onAlbumArtUpdate(@android.support.annotation.Nullable okio.ByteString artwork, boolean animate) {
        }

        public void onTrackUpdated(com.navdy.service.library.events.audio.MusicTrackInfo trackInfo, java.util.Set<com.navdy.hud.app.manager.MusicManager.MediaControl> set, boolean willOpenNotification) {
            if (!com.navdy.hud.app.ui.component.mainmenu.MainMenu.this.isSameTrack(com.navdy.hud.app.ui.component.mainmenu.MainMenu.this.addedTrackInfo, trackInfo) && com.navdy.hud.app.ui.component.mainmenu.MainMenu.this.canShowTrackInfo(trackInfo)) {
                com.navdy.hud.app.ui.component.mainmenu.MainMenu.handler.post(new com.navdy.hud.app.ui.component.mainmenu.MainMenu.Anon3.Anon1(trackInfo));
            }
        }
    }

    class Anon4 implements java.lang.Runnable {
        Anon4() {
        }

        public void run() {
            com.navdy.hud.app.framework.notifications.NotificationManager notificationManager = com.navdy.hud.app.framework.notifications.NotificationManager.getInstance();
            com.navdy.hud.app.framework.voice.VoiceSearchNotification voiceSearchNotification = (com.navdy.hud.app.framework.voice.VoiceSearchNotification) notificationManager.getNotification(com.navdy.hud.app.framework.notifications.NotificationId.VOICE_SEARCH_NOTIFICATION_ID);
            if (voiceSearchNotification == null) {
                voiceSearchNotification = new com.navdy.hud.app.framework.voice.VoiceSearchNotification();
            }
            notificationManager.addNotification(voiceSearchNotification);
        }
    }

    class Anon5 implements java.lang.Runnable {
        Anon5() {
        }

        public void run() {
            com.navdy.hud.app.ui.component.mainmenu.MainMenu.this.bus.post(com.navdy.service.library.events.ui.Screen.SCREEN_BACK);
            com.navdy.hud.app.ui.component.mainmenu.MainMenu.musicManager.showMusicNotification();
        }
    }

    static {
        childMenus.put(com.navdy.hud.app.ui.component.mainmenu.IMenu.Menu.SETTINGS.name(), com.navdy.hud.app.ui.component.mainmenu.IMenu.Menu.SETTINGS);
        childMenus.put(com.navdy.hud.app.ui.component.mainmenu.IMenu.Menu.PLACES.name(), com.navdy.hud.app.ui.component.mainmenu.IMenu.Menu.PLACES);
        childMenus.put(com.navdy.hud.app.ui.component.mainmenu.IMenu.Menu.CONTACTS.name(), com.navdy.hud.app.ui.component.mainmenu.IMenu.Menu.CONTACTS);
        childMenus.put(com.navdy.hud.app.ui.component.mainmenu.IMenu.Menu.MUSIC.name(), com.navdy.hud.app.ui.component.mainmenu.IMenu.Menu.MUSIC);
        java.lang.String title = resources.getString(com.navdy.hud.app.R.string.mm_voice_search);
        java.lang.String subTitle = resources.getString(com.navdy.hud.app.R.string.mm_voice_search_description);
        int fluctuatorColor = resources.getColor(com.navdy.hud.app.R.color.mm_voice_search);
        voiceSearch = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.main_menu_voice_search, com.navdy.hud.app.R.drawable.icon_mm_voice_search_2, fluctuatorColor, bkColorUnselected, fluctuatorColor, title, subTitle);
        java.lang.String title2 = resources.getString(com.navdy.hud.app.R.string.carousel_menu_smartdash_title);
        int fluctuatorColor2 = resources.getColor(com.navdy.hud.app.R.color.mm_dash);
        smartDash = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.main_menu_smart_dash, com.navdy.hud.app.R.drawable.icon_mm_dash_2, fluctuatorColor2, bkColorUnselected, fluctuatorColor2, title2, null);
        java.lang.String title3 = resources.getString(com.navdy.hud.app.R.string.carousel_menu_map_title);
        int fluctuatorColor3 = resources.getColor(com.navdy.hud.app.R.color.mm_map);
        maps = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.main_menu_maps, com.navdy.hud.app.R.drawable.icon_mm_map_2, fluctuatorColor3, bkColorUnselected, fluctuatorColor3, title3, null);
        java.lang.String title4 = resources.getString(com.navdy.hud.app.R.string.carousel_menu_map_places);
        int fluctuatorColor4 = resources.getColor(com.navdy.hud.app.R.color.mm_places);
        places = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.main_menu_places, com.navdy.hud.app.R.drawable.icon_mm_places_2, fluctuatorColor4, bkColorUnselected, fluctuatorColor4, title4, null);
        java.lang.String title5 = resources.getString(com.navdy.hud.app.R.string.carousel_menu_music_control);
        int fluctuatorColor5 = resources.getColor(com.navdy.hud.app.R.color.mm_music);
        musicControl = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.main_menu_music, com.navdy.hud.app.R.drawable.icon_mm_music_2, fluctuatorColor5, bkColorUnselected, fluctuatorColor5, title5, null);
        java.lang.String title6 = resources.getString(com.navdy.hud.app.R.string.carousel_menu_voice_control_siri);
        int fluctuatorColor6 = resources.getColor(com.navdy.hud.app.R.color.mm_voice_assist_siri);
        voiceAssistanceSiri = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.main_menu_voice, com.navdy.hud.app.R.drawable.icon_mm_siri_2, fluctuatorColor6, bkColorUnselected, fluctuatorColor6, title6, null);
        java.lang.String title7 = resources.getString(com.navdy.hud.app.R.string.carousel_menu_voice_control_google);
        int fluctuatorColor7 = resources.getColor(com.navdy.hud.app.R.color.mm_voice_assist_gnow);
        voiceAssistanceGoogle = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.main_menu_voice, com.navdy.hud.app.R.drawable.icon_mm_googlenow_2, fluctuatorColor7, bkColorUnselected, fluctuatorColor7, title7, null);
        java.lang.String title8 = resources.getString(com.navdy.hud.app.R.string.carousel_menu_contacts);
        int fluctuatorColor8 = resources.getColor(com.navdy.hud.app.R.color.mm_contacts);
        contacts = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.main_contacts, com.navdy.hud.app.R.drawable.icon_mm_contacts_2, fluctuatorColor8, bkColorUnselected, fluctuatorColor8, title8, null);
        java.lang.String title9 = resources.getString(com.navdy.hud.app.R.string.carousel_menu_settings_title);
        int fluctuatorColor9 = resources.getColor(com.navdy.hud.app.R.color.mm_settings);
        settings = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.main_menu_settings, com.navdy.hud.app.R.drawable.icon_mm_settings_2, fluctuatorColor9, bkColorUnselected, fluctuatorColor9, title9, null);
        java.lang.String title10 = resources.getString(com.navdy.hud.app.R.string.carousel_settings_connect_phone_title);
        int fluctuatorColor10 = resources.getColor(com.navdy.hud.app.R.color.mm_connnect_phone);
        connectPhone = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.main_menu_settings_connect_phone, com.navdy.hud.app.R.drawable.icon_settings_connect_phone_2, fluctuatorColor10, bkColorUnselected, fluctuatorColor10, title10, null);
    }

    public MainMenu(com.squareup.otto.Bus bus2, com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent vscrollComponent2, com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.Presenter presenter2) {
        this.bus = bus2;
        this.vscrollComponent = vscrollComponent2;
        this.presenter = presenter2;
    }

    public java.util.List<com.navdy.hud.app.ui.component.vlist.VerticalList.Model> getItems() {
        clearNowPlayingState();
        stopActivityTrayRunnable();
        this.curToolTipId = -1;
        this.curToolTipPos = -1;
        com.navdy.service.library.events.DeviceInfo deviceInfo = remoteDeviceManager.getRemoteDeviceInfo();
        java.util.List<com.navdy.hud.app.ui.component.vlist.VerticalList.Model> list = new java.util.ArrayList<>();
        boolean isConnected = !com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile().isDefaultProfile();
        if (isConnected && deviceInfo == null) {
            isConnected = false;
        }
        sLogger.v("isConnected:" + isConnected);
        this.selectedIndex = -1;
        com.navdy.hud.app.maps.here.HereMapsManager hereMapsManager = com.navdy.hud.app.maps.here.HereMapsManager.getInstance();
        com.navdy.hud.app.ui.component.vlist.VerticalList.Model activityTray = getActivityTrayModel(isConnected);
        if (hereMapsManager.isInitialized() && hereMapsManager.isNavigationModeOn()) {
            this.selectedIndex = list.size();
            if (activityTray.iconIds != null) {
                int i = 0;
                while (true) {
                    if (i >= activityTray.iconIds.length) {
                        break;
                    } else if (activityTray.iconIds[i] == com.navdy.hud.app.R.id.main_menu_active_trip) {
                        activityTray.currentIconSelection = i;
                        break;
                    } else {
                        i++;
                    }
                }
            }
        }
        list.add(activityTray);
        boolean navPossible = com.navdy.hud.app.maps.here.HereMapsManager.getInstance().isNavigationPossible();
        boolean isDeviceCapableOfVoiceSearch = com.navdy.hud.app.util.RemoteCapabilitiesUtil.supportsVoiceSearch();
        if (isConnected) {
            if (!navPossible) {
                sLogger.v("nav not possible");
            }
            addLongPressAction(deviceInfo, list, isDeviceCapableOfVoiceSearch, navPossible, true);
        } else {
            this.selectedIndex = list.size();
            list.add(connectPhone);
        }
        if (this.selectedIndex == -1) {
            this.selectedIndex = list.size();
        }
        list.add(maps);
        list.add(smartDash);
        if (isConnected) {
            if (navPossible) {
                list.add(places);
            }
            list.add(contacts);
        }
        com.navdy.hud.app.ui.component.vlist.VerticalList.Model glances = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.main_menu_glances, com.navdy.hud.app.R.drawable.icon_mm_glances_2, glancesColor, bkColorUnselected, glancesColor, resources.getString(com.navdy.hud.app.R.string.mm_glances), null);
        com.navdy.service.library.events.preferences.NotificationPreferences prefs = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile().getNotificationPreferences();
        if (!(prefs.enabled != null ? prefs.enabled.booleanValue() : false)) {
            glances.title = resources.getString(com.navdy.hud.app.R.string.mm_glances_disabled);
            glances.subTitle = null;
        } else {
            int notificationCount = com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().getNotificationCount();
            glances.title = resources.getString(com.navdy.hud.app.R.string.mm_glances);
            glances.subTitle = resources.getString(com.navdy.hud.app.R.string.mm_glances_subtitle, new java.lang.Object[]{java.lang.Integer.valueOf(notificationCount)});
        }
        list.add(glances);
        if (isConnected) {
            list.add(musicControl);
            addLongPressAction(deviceInfo, list, isDeviceCapableOfVoiceSearch, navPossible, false);
        }
        list.add(settings);
        this.cachedList = list;
        if (!this.registeredMusicCallback) {
            musicManager.addMusicUpdateListener(this.musicUpdateListener);
            this.registeredMusicCallback = true;
            sLogger.v("register music");
        }
        if (!this.busRegistered) {
            this.bus.register(this);
            this.busRegistered = true;
            sLogger.v("registered bus");
        }
        return list;
    }

    private void addVoiceAssistance(com.navdy.service.library.events.DeviceInfo deviceInfo, java.util.List<com.navdy.hud.app.ui.component.vlist.VerticalList.Model> list) {
        if (deviceInfo.platform == com.navdy.service.library.events.DeviceInfo.Platform.PLATFORM_iOS) {
            list.add(voiceAssistanceSiri);
        } else {
            list.add(voiceAssistanceGoogle);
        }
    }

    private void addLongPressAction(com.navdy.service.library.events.DeviceInfo deviceInfo, java.util.List<com.navdy.hud.app.ui.component.vlist.VerticalList.Model> list, boolean isDeviceCapableOfVoiceSearch, boolean navPossible, boolean start) {
        if (start) {
            if (isLongPressActionPlaceSearch()) {
                if (this.selectedIndex == -1) {
                    this.selectedIndex = list.size();
                }
                addVoiceAssistance(deviceInfo, list);
            } else if (isDeviceCapableOfVoiceSearch && navPossible) {
                if (this.selectedIndex == -1) {
                    this.selectedIndex = list.size();
                }
                list.add(voiceSearch);
            }
        } else if (!isLongPressActionPlaceSearch()) {
            addVoiceAssistance(deviceInfo, list);
        } else if (isDeviceCapableOfVoiceSearch && navPossible) {
            list.add(voiceSearch);
        }
    }

    public int getInitialSelection() {
        return this.selectedIndex;
    }

    public com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex getScrollIndex() {
        return null;
    }

    public void setBackSelectionPos(int n) {
    }

    public void setBackSelectionId(int id) {
    }

    public void setSelectedIcon() {
        if (this.activityTraySelection != -1 && this.presenter.getCurrentSelection() == 0) {
            java.lang.String toolTip = getToolTipString(this.activityTraySelectionId);
            if (toolTip != null) {
                this.presenter.showToolTip(this.activityTraySelection, toolTip);
            }
        }
        this.activityTraySelection = -1;
        this.activityTraySelectionId = -1;
        android.content.res.Resources resources2 = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        int size = resources2.getDimensionPixelSize(com.navdy.hud.app.R.dimen.vmenu_selected_image);
        this.vscrollComponent.setSelectedIconColorImage(com.navdy.hud.app.R.drawable.icon_mm_navdy, -1, new android.graphics.LinearGradient(0.0f, 0.0f, (float) size, (float) size, new int[]{resources2.getColor(com.navdy.hud.app.R.color.mm_icon_gradient_start), resources2.getColor(com.navdy.hud.app.R.color.mm_icon_gradient_end)}, null, android.graphics.Shader.TileMode.MIRROR), 1.0f);
        this.vscrollComponent.selectedText.setText(resources2.getString(com.navdy.hud.app.R.string.menu_str));
    }

    public boolean isItemClickable(int id, int pos) {
        switch (id) {
            case com.navdy.hud.app.R.id.main_menu_no_fav_contacts /*2131623972*/:
            case com.navdy.hud.app.R.id.main_menu_no_fav_places /*2131623973*/:
            case com.navdy.hud.app.R.id.main_menu_no_recent_contacts /*2131623974*/:
            case com.navdy.hud.app.R.id.main_menu_no_suggested_places /*2131623975*/:
                return false;
            default:
                return true;
        }
    }

    public com.navdy.hud.app.ui.component.vlist.VerticalList.Model getModelfromPos(int pos) {
        if (this.cachedList == null || this.cachedList.size() <= pos) {
            return null;
        }
        return (com.navdy.hud.app.ui.component.vlist.VerticalList.Model) this.cachedList.get(pos);
    }

    public boolean isBindCallsEnabled() {
        return false;
    }

    public void onBindToView(com.navdy.hud.app.ui.component.vlist.VerticalList.Model model, android.view.View view, int pos, com.navdy.hud.app.ui.component.vlist.VerticalList.ModelState state) {
    }

    public void onItemSelected(com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState selection) {
        switch (selection.id) {
            case com.navdy.hud.app.R.id.main_menu_activity_tray /*2131623958*/:
                java.lang.String toolTipStr = getToolTipString(selection.subId);
                if (toolTipStr != null) {
                    stopActivityTrayRunnable();
                    switch (selection.subId) {
                        case com.navdy.hud.app.R.id.main_menu_call /*2131623960*/:
                            startActivityTrayRunnable(selection.subId, selection.subPosition);
                            break;
                        case com.navdy.hud.app.R.id.main_menu_now_playing /*2131623976*/:
                            this.curToolTipId = selection.subId;
                            this.curToolTipPos = selection.subPosition;
                            break;
                        default:
                            this.curToolTipId = -1;
                            this.curToolTipPos = -1;
                            break;
                    }
                    this.presenter.showToolTip(selection.subPosition, toolTipStr);
                    return;
                }
                stopActivityTrayRunnable();
                this.presenter.hideToolTip();
                return;
            default:
                stopActivityTrayRunnable();
                this.presenter.hideToolTip();
                return;
        }
    }

    public void onScrollIdle() {
    }

    public void onFastScrollStart() {
    }

    public void onFastScrollEnd() {
    }

    public void showToolTip() {
        int subId = this.presenter.getCurrentSubSelectionId();
        int subPos = this.presenter.getCurrentSubSelectionPos();
        java.lang.String toolTipStr = getToolTipString(subId);
        if (toolTipStr != null) {
            stopActivityTrayRunnable();
            if (subId == com.navdy.hud.app.R.id.main_menu_call) {
                startActivityTrayRunnable(subId, subPos);
            }
            this.presenter.showToolTip(subPos, toolTipStr);
            return;
        }
        stopActivityTrayRunnable();
        this.presenter.hideToolTip();
    }

    public boolean isFirstItemEmpty() {
        return true;
    }

    public boolean selectItem(com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState selection) {
        sLogger.v("select id:" + selection.id + " pos:" + selection.pos);
        switch (selection.id) {
            case com.navdy.hud.app.R.id.main_contacts /*2131623956*/:
                sLogger.v("contacts");
                com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("contacts");
                createChildMenu(com.navdy.hud.app.ui.component.mainmenu.IMenu.Menu.CONTACTS, null);
                this.presenter.loadMenu(this.contactsMenu, com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel.SUB_LEVEL, selection.pos, 0);
                break;
            case com.navdy.hud.app.R.id.main_menu_activity_tray /*2131623958*/:
                switch (selection.subId) {
                    case com.navdy.hud.app.R.id.main_menu_active_trip /*2131623957*/:
                        sLogger.v("trip options");
                        com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("trip-options");
                        if (this.activeTripMenu == null) {
                            this.activeTripMenu = new com.navdy.hud.app.ui.component.mainmenu.ActiveTripMenu(this.bus, this.vscrollComponent, this.presenter, this);
                        }
                        this.activityTraySelection = selection.subPosition;
                        this.activityTraySelectionId = selection.subId;
                        this.presenter.loadMenu(this.activeTripMenu, com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel.SUB_LEVEL, selection.pos, 1, true);
                        break;
                    case com.navdy.hud.app.R.id.main_menu_call /*2131623960*/:
                        sLogger.v("phone call");
                        com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("phone_call_mm");
                        com.navdy.hud.app.framework.notifications.NotificationManager notificationManager = com.navdy.hud.app.framework.notifications.NotificationManager.getInstance();
                        if (notificationManager.getNotification(com.navdy.hud.app.framework.notifications.NotificationId.PHONE_CALL_NOTIFICATION_ID) != null && notificationManager.makeNotificationCurrent(true)) {
                            sLogger.d("phone call");
                            notificationManager.showNotification();
                            break;
                        } else {
                            this.presenter.performSelectionAnimation(new com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils.ActionRunnable(this.bus, com.navdy.service.library.events.ui.Screen.SCREEN_BACK));
                            break;
                        }
                    case com.navdy.hud.app.R.id.main_menu_glances /*2131623966*/:
                        handleGlances();
                        break;
                    case com.navdy.hud.app.R.id.main_menu_maps_options /*2131623969*/:
                        sLogger.v("map options");
                        com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("hybrid_map-options");
                        if (this.mainOptionsMenu == null) {
                            this.mainOptionsMenu = new com.navdy.hud.app.ui.component.mainmenu.MainOptionsMenu(this.bus, this.vscrollComponent, this.presenter, this);
                        }
                        this.mainOptionsMenu.setMode(com.navdy.hud.app.ui.component.mainmenu.MainOptionsMenu.Mode.MAP);
                        this.activityTraySelection = selection.subPosition;
                        this.activityTraySelectionId = selection.subId;
                        this.presenter.loadMenu(this.mainOptionsMenu, com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel.SUB_LEVEL, selection.pos, 0);
                        break;
                    case com.navdy.hud.app.R.id.main_menu_now_playing /*2131623976*/:
                        sLogger.v("now_playing");
                        com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("now_playing");
                        this.presenter.performSelectionAnimation(new com.navdy.hud.app.ui.component.mainmenu.MainMenu.Anon5());
                        break;
                    case com.navdy.hud.app.R.id.main_menu_smart_dash_options /*2131624000*/:
                        sLogger.v("dash-options");
                        com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("dash-options");
                        if (this.mainOptionsMenu == null) {
                            this.mainOptionsMenu = new com.navdy.hud.app.ui.component.mainmenu.MainOptionsMenu(this.bus, this.vscrollComponent, this.presenter, this);
                        }
                        this.mainOptionsMenu.setMode(com.navdy.hud.app.ui.component.mainmenu.MainOptionsMenu.Mode.DASH);
                        this.activityTraySelection = selection.subPosition;
                        this.activityTraySelectionId = selection.subId;
                        this.presenter.loadMenu(this.mainOptionsMenu, com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel.SUB_LEVEL, selection.pos, 0);
                        break;
                    case com.navdy.hud.app.R.id.settings_menu_dial_update /*2131624063*/:
                        sLogger.v("dial update");
                        com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("dial_update_mm");
                        android.os.Bundle args = new android.os.Bundle();
                        args.putBoolean(com.navdy.hud.app.manager.UpdateReminderManager.EXTRA_UPDATE_REMINDER, false);
                        this.presenter.performSelectionAnimation(new com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils.ActionRunnable(this.bus, com.navdy.service.library.events.ui.Screen.SCREEN_DIAL_UPDATE_CONFIRMATION, args, false));
                        break;
                    case com.navdy.hud.app.R.id.settings_menu_software_update /*2131624068*/:
                        sLogger.v("software update");
                        com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("software_update_mm");
                        android.os.Bundle args2 = new android.os.Bundle();
                        args2.putBoolean(com.navdy.hud.app.manager.UpdateReminderManager.EXTRA_UPDATE_REMINDER, false);
                        this.presenter.performSelectionAnimation(new com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils.ActionRunnable(this.bus, com.navdy.service.library.events.ui.Screen.SCREEN_OTA_CONFIRMATION, args2, false));
                        break;
                }
            case com.navdy.hud.app.R.id.main_menu_glances /*2131623966*/:
                handleGlances();
                break;
            case com.navdy.hud.app.R.id.main_menu_maps /*2131623968*/:
                sLogger.v("map");
                com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("hybrid_map");
                this.presenter.performSelectionAnimation(new com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils.ActionRunnable(this.bus, com.navdy.service.library.events.ui.Screen.SCREEN_HYBRID_MAP));
                break;
            case com.navdy.hud.app.R.id.main_menu_music /*2131623970*/:
                com.navdy.hud.app.manager.MusicManager musicManager2 = remoteDeviceManager.getMusicManager();
                if (com.navdy.hud.app.ui.component.UISettings.isMusicBrowsingEnabled() && musicManager2.hasMusicCapabilities()) {
                    sLogger.v("music browse");
                    com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection(com.navdy.hud.app.storage.db.table.MusicArtworkCacheTable.TYPE_MUSIC);
                    createChildMenu(com.navdy.hud.app.ui.component.mainmenu.IMenu.Menu.MUSIC, null);
                    this.presenter.loadMenu(this.musicMenu, com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel.SUB_LEVEL, selection.pos, 0);
                    break;
                } else {
                    sLogger.v(com.navdy.hud.app.storage.db.table.MusicArtworkCacheTable.TYPE_MUSIC);
                    com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection(com.navdy.hud.app.storage.db.table.MusicArtworkCacheTable.TYPE_MUSIC);
                    this.presenter.performSelectionAnimation(new com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils.ActionRunnable(this.bus, com.navdy.service.library.events.ui.Screen.SCREEN_MUSIC));
                    break;
                }
                break;
            case com.navdy.hud.app.R.id.main_menu_places /*2131623993*/:
                sLogger.v("places");
                com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("places");
                createChildMenu(com.navdy.hud.app.ui.component.mainmenu.IMenu.Menu.PLACES, null);
                this.presenter.loadMenu(this.placesMenu, com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel.SUB_LEVEL, selection.pos, 0);
                break;
            case com.navdy.hud.app.R.id.main_menu_settings /*2131623996*/:
                sLogger.v("settings");
                com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("settings");
                createChildMenu(com.navdy.hud.app.ui.component.mainmenu.IMenu.Menu.SETTINGS, null);
                this.presenter.loadMenu(this.settingsMenu, com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel.SUB_LEVEL, selection.pos, 0);
                break;
            case com.navdy.hud.app.R.id.main_menu_settings_connect_phone /*2131623997*/:
                sLogger.v("connect phone");
                com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("connect_phone");
                android.os.Bundle args3 = new android.os.Bundle();
                args3.putString(com.navdy.hud.app.screen.WelcomeScreen.ARG_ACTION, com.navdy.hud.app.screen.WelcomeScreen.ACTION_SWITCH_PHONE);
                this.presenter.performSelectionAnimation(new com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils.ActionRunnable(this.bus, com.navdy.service.library.events.ui.Screen.SCREEN_WELCOME, args3, false));
                break;
            case com.navdy.hud.app.R.id.main_menu_smart_dash /*2131623999*/:
                sLogger.v("dash");
                com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("dash");
                this.presenter.performSelectionAnimation(new com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils.ActionRunnable(this.bus, com.navdy.service.library.events.ui.Screen.SCREEN_DASHBOARD));
                break;
            case com.navdy.hud.app.R.id.main_menu_voice /*2131624005*/:
                sLogger.v("voice-assist");
                com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("voice_assist");
                this.presenter.performSelectionAnimation(new com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils.ActionRunnable(this.bus, com.navdy.service.library.events.ui.Screen.SCREEN_VOICE_CONTROL));
                break;
            case com.navdy.hud.app.R.id.main_menu_voice_search /*2131624006*/:
                sLogger.v("voice_search");
                com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("voice_search");
                this.presenter.performSelectionAnimation(new com.navdy.hud.app.ui.component.mainmenu.MainMenu.Anon4());
                break;
        }
        return true;
    }

    public com.navdy.hud.app.ui.component.mainmenu.IMenu.Menu getType() {
        return com.navdy.hud.app.ui.component.mainmenu.IMenu.Menu.MAIN;
    }

    public com.navdy.hud.app.ui.component.mainmenu.IMenu getChildMenu(com.navdy.hud.app.ui.component.mainmenu.IMenu parent, java.lang.String args, java.lang.String path) {
        sLogger.v("getChildMenu:" + args + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA + path);
        com.navdy.hud.app.ui.component.mainmenu.IMenu.Menu m = (com.navdy.hud.app.ui.component.mainmenu.IMenu.Menu) childMenus.get(args);
        if (m != null) {
            return createChildMenu(m, path);
        }
        sLogger.v("getChildMenu: not found");
        return null;
    }

    public void onUnload(com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel level) {
        handler.removeCallbacks(this.etaRunnable);
        if (this.registeredMusicCallback) {
            this.registeredMusicCallback = false;
            musicManager.removeMusicUpdateListener(this.musicUpdateListener);
            sLogger.v("unregister");
        }
        if (this.busRegistered) {
            this.bus.unregister(this);
            this.busRegistered = false;
            sLogger.v("bus unregistered");
        }
        clearNowPlayingState();
        if (level == com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel.CLOSE) {
            if (this.placesMenu != null) {
                this.placesMenu.onUnload(com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel.CLOSE);
            }
            if (this.contactsMenu != null) {
                this.contactsMenu.onUnload(com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel.CLOSE);
            }
            if (this.musicMenu != null) {
                this.musicMenu.onUnload(com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel.CLOSE);
            }
        }
    }

    public int getActivityTraySelection() {
        return this.activityTraySelection;
    }

    public void clearState() {
        this.settingsMenu = null;
        this.placesMenu = null;
        this.contactsMenu = null;
        this.mainOptionsMenu = null;
        this.activeTripMenu = null;
        this.musicMenu = null;
    }

    private com.navdy.hud.app.ui.component.mainmenu.IMenu createChildMenu(com.navdy.hud.app.ui.component.mainmenu.IMenu.Menu menu, java.lang.String path) {
        sLogger.v("createChildMenu:" + path);
        java.lang.String element = null;
        if (path != null) {
            if (path.indexOf(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.SLASH) == 0) {
                element = path.substring(1);
                int index = element.indexOf(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.SLASH);
                if (index >= 0) {
                    path = path.substring(index + 1);
                    element = element.substring(0, index);
                } else {
                    path = null;
                }
            } else {
                path = null;
            }
        }
        switch (menu) {
            case CONTACTS:
                if (this.contactsMenu == null) {
                    this.contactsMenu = new com.navdy.hud.app.ui.component.mainmenu.ContactsMenu(this.bus, this.vscrollComponent, this.presenter, this);
                }
                if (!android.text.TextUtils.isEmpty(element)) {
                    com.navdy.hud.app.ui.component.mainmenu.IMenu child = this.contactsMenu.getChildMenu(this, element, path);
                    if (child != null) {
                        return child;
                    }
                }
                this.contactsMenu.setBackSelectionId(com.navdy.hud.app.R.id.main_contacts);
                return this.contactsMenu;
            case PLACES:
                if (this.placesMenu == null) {
                    this.placesMenu = new com.navdy.hud.app.ui.component.mainmenu.PlacesMenu(this.bus, this.vscrollComponent, this.presenter, this);
                }
                if (!android.text.TextUtils.isEmpty(element)) {
                    com.navdy.hud.app.ui.component.mainmenu.IMenu child2 = this.placesMenu.getChildMenu(this, element, path);
                    if (child2 != null) {
                        return child2;
                    }
                }
                this.placesMenu.setBackSelectionId(com.navdy.hud.app.R.id.main_menu_places);
                return this.placesMenu;
            case SETTINGS:
                if (this.settingsMenu == null) {
                    this.settingsMenu = new com.navdy.hud.app.ui.component.mainmenu.SettingsMenu(this.bus, this.vscrollComponent, this.presenter, this);
                }
                if (!android.text.TextUtils.isEmpty(element)) {
                    com.navdy.hud.app.ui.component.mainmenu.IMenu child3 = this.settingsMenu.getChildMenu(this, element, path);
                    if (child3 != null) {
                        return child3;
                    }
                }
                this.settingsMenu.setBackSelectionId(com.navdy.hud.app.R.id.main_menu_settings);
                return this.settingsMenu;
            case MUSIC:
                if (this.musicMenu == null) {
                    this.musicMenu = new com.navdy.hud.app.ui.component.mainmenu.MusicMenu2(this.bus, this.vscrollComponent, this.presenter, this, null);
                }
                this.musicMenu.setBackSelectionId(com.navdy.hud.app.R.id.main_menu_music);
                if (!android.text.TextUtils.isEmpty(element)) {
                    com.navdy.hud.app.ui.component.mainmenu.IMenu child4 = this.musicMenu.getChildMenu(this, element, path);
                    if (child4 != null) {
                        return child4;
                    }
                }
                return this.musicMenu;
            default:
                return null;
        }
    }

    /* access modifiers changed from: private */
    public void fillTta() {
        try {
            java.lang.String ttaString = com.navdy.hud.app.maps.here.HereMapUtil.getCurrentTtaStringWithDestination();
            if (!android.text.TextUtils.isEmpty(ttaString)) {
                this.currentTta = ttaString;
            }
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }

    @com.squareup.otto.Subscribe
    public void arrivalEvent(com.navdy.hud.app.maps.MapEvents.ArrivalEvent event) {
        sLogger.v("arrival event");
        handler.removeCallbacks(this.etaRunnable);
        fillTta();
    }

    private boolean isLongPressActionPlaceSearch() {
        if (com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile().getLongPressAction() == com.navdy.service.library.events.preferences.DriverProfilePreferences.DialLongPressAction.DIAL_LONG_PRESS_PLACE_SEARCH) {
            return true;
        }
        return false;
    }

    private com.navdy.hud.app.ui.component.vlist.VerticalList.Model getActivityTrayModel(boolean isConnected) {
        activityTrayIcon.clear();
        activityTrayIconId.clear();
        activityTraySelectedColor.clear();
        activityTrayUnSelectedColor.clear();
        boolean callAdded = false;
        if (isConnected && callManager.isCallInProgress()) {
            activityTrayIcon.add(java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_mm_contacts_2));
            activityTrayIconId.add(java.lang.Integer.valueOf(com.navdy.hud.app.R.id.main_menu_call));
            activityTraySelectedColor.add(java.lang.Integer.valueOf(resources.getColor(com.navdy.hud.app.R.color.mm_connnect_phone)));
            activityTrayUnSelectedColor.add(java.lang.Integer.valueOf(bkColorUnselected));
            callAdded = true;
        }
        handler.removeCallbacks(this.etaRunnable);
        boolean tripOptionSlot = false;
        if (com.navdy.hud.app.maps.here.HereMapsManager.getInstance().isInitialized() && com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().isNavigationModeOn()) {
            sLogger.v("nav on adding trip menu");
            activityTrayIcon.add(java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_badge_active_trip));
            activityTrayIconId.add(java.lang.Integer.valueOf(com.navdy.hud.app.R.id.main_menu_active_trip));
            activityTraySelectedColor.add(java.lang.Integer.valueOf(resources.getColor(com.navdy.hud.app.R.color.mm_active_trip)));
            activityTrayUnSelectedColor.add(java.lang.Integer.valueOf(bkColorUnselected));
            fillTta();
            handler.postDelayed(this.etaRunnable, (long) ETA_UPDATE_INTERVAL);
            tripOptionSlot = true;
        }
        if (com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().getNotificationCount() > 0) {
            activityTrayIcon.add(java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_mm_glances_2));
            activityTrayIconId.add(java.lang.Integer.valueOf(com.navdy.hud.app.R.id.main_menu_glances));
            activityTraySelectedColor.add(java.lang.Integer.valueOf(glancesColor));
            activityTrayUnSelectedColor.add(java.lang.Integer.valueOf(bkColorUnselected));
        }
        if (isConnected && !callAdded) {
            com.navdy.service.library.events.audio.MusicTrackInfo trackInfo = musicManager.getCurrentTrack();
            if (canShowTrackInfo(trackInfo)) {
                activityTrayIcon.add(java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_mm_music_2));
                activityTrayIconId.add(java.lang.Integer.valueOf(com.navdy.hud.app.R.id.main_menu_now_playing));
                activityTraySelectedColor.add(java.lang.Integer.valueOf(nowPlayingColor));
                activityTrayUnSelectedColor.add(java.lang.Integer.valueOf(bkColorUnselected));
                this.addedTrackInfo = trackInfo;
                sLogger.v("added now playing");
            } else {
                sLogger.v("not added now playing");
            }
        }
        if (!tripOptionSlot && com.navdy.hud.app.util.OTAUpdateService.isUpdateAvailable()) {
            this.swVersion = resources.getString(com.navdy.hud.app.R.string.mm_navdy_update, new java.lang.Object[]{com.navdy.hud.app.util.OTAUpdateService.getSWUpdateVersion()});
            sLogger.v("add navdy s/w");
            activityTrayIcon.add(java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_software_update_2));
            activityTrayIconId.add(java.lang.Integer.valueOf(com.navdy.hud.app.R.id.settings_menu_software_update));
            activityTraySelectedColor.add(java.lang.Integer.valueOf(resources.getColor(com.navdy.hud.app.R.color.mm_settings_update_display)));
            activityTrayUnSelectedColor.add(java.lang.Integer.valueOf(bkColorUnselected));
            tripOptionSlot = true;
        }
        if (!tripOptionSlot) {
            com.navdy.hud.app.device.dial.DialManager dialManager = com.navdy.hud.app.device.dial.DialManager.getInstance();
            if (dialManager.isDialConnected() && dialManager.getDialFirmwareUpdater().getVersions().isUpdateAvailable()) {
                sLogger.v("add dial s/w");
                activityTrayIcon.add(java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_dial_update_2));
                activityTrayIconId.add(java.lang.Integer.valueOf(com.navdy.hud.app.R.id.settings_menu_dial_update));
                activityTraySelectedColor.add(java.lang.Integer.valueOf(resources.getColor(com.navdy.hud.app.R.color.mm_settings_update_dial)));
                activityTrayUnSelectedColor.add(java.lang.Integer.valueOf(bkColorUnselected));
            }
        }
        com.navdy.service.library.events.ui.Screen screen = uiStateManager.getDefaultMainActiveScreen();
        boolean dashModeOn = false;
        boolean mapModeOn = false;
        if (!(screen == null || screen != com.navdy.service.library.events.ui.Screen.SCREEN_HOME || uiStateManager.getHomescreenView() == null)) {
            switch (uiStateManager.getHomescreenView().getDisplayMode()) {
                case SMART_DASH:
                    dashModeOn = true;
                    break;
                case MAP:
                    mapModeOn = true;
                    break;
            }
        }
        if (dashModeOn) {
            activityTrayIcon.add(java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_main_menu_dash_options));
            activityTrayIconId.add(java.lang.Integer.valueOf(com.navdy.hud.app.R.id.main_menu_smart_dash_options));
            activityTraySelectedColor.add(java.lang.Integer.valueOf(resources.getColor(com.navdy.hud.app.R.color.mm_dash_options)));
            activityTrayUnSelectedColor.add(java.lang.Integer.valueOf(bkColorUnselected));
        } else if (mapModeOn) {
            activityTrayIcon.add(java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.icon_main_menu_map_options));
            activityTrayIconId.add(java.lang.Integer.valueOf(com.navdy.hud.app.R.id.main_menu_maps_options));
            activityTraySelectedColor.add(java.lang.Integer.valueOf(resources.getColor(com.navdy.hud.app.R.color.mm_map_options)));
            activityTrayUnSelectedColor.add(java.lang.Integer.valueOf(bkColorUnselected));
        }
        int size = activityTrayIcon.size();
        int[] icons = new int[size];
        int[] iconIds = new int[size];
        int[] iconSelectedColor = new int[size];
        int[] iconUnselectedColor = new int[size];
        int counter = 0;
        for (int i = size - 1; i >= 0; i--) {
            icons[counter] = ((java.lang.Integer) activityTrayIcon.get(i)).intValue();
            iconIds[counter] = ((java.lang.Integer) activityTrayIconId.get(i)).intValue();
            iconSelectedColor[counter] = ((java.lang.Integer) activityTraySelectedColor.get(i)).intValue();
            iconUnselectedColor[counter] = ((java.lang.Integer) activityTrayUnSelectedColor.get(i)).intValue();
            counter++;
        }
        int selection = this.activityTraySelection;
        if (selection == -1) {
            selection = size - 1;
        }
        return com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder.buildModel(com.navdy.hud.app.R.id.main_menu_activity_tray, icons, iconIds, iconSelectedColor, iconUnselectedColor, iconSelectedColor, selection, true);
    }

    /* access modifiers changed from: private */
    public boolean isSameTrack(com.navdy.service.library.events.audio.MusicTrackInfo oldTrackInfo, com.navdy.service.library.events.audio.MusicTrackInfo newTrackInfo) {
        if (oldTrackInfo == null || newTrackInfo == null || !android.text.TextUtils.equals(oldTrackInfo.name, newTrackInfo.name) || !android.text.TextUtils.equals(oldTrackInfo.author, newTrackInfo.author)) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: private */
    public boolean canShowTrackInfo(com.navdy.service.library.events.audio.MusicTrackInfo musicTrackInfo) {
        if (musicTrackInfo == null || ((!com.navdy.hud.app.manager.MusicManager.tryingToPlay(musicTrackInfo.playbackState) && musicTrackInfo.playbackState != com.navdy.service.library.events.audio.MusicPlaybackState.PLAYBACK_PAUSED) || android.text.TextUtils.isEmpty(musicTrackInfo.name))) {
            return false;
        }
        return true;
    }

    private void clearNowPlayingState() {
        this.addedTrackInfo = null;
    }

    private void handleGlances() {
        boolean isGlancedEnabled;
        sLogger.v("glances");
        com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("glances");
        android.content.res.Resources resources2 = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        com.navdy.service.library.events.preferences.NotificationPreferences prefs = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile().getNotificationPreferences();
        if (prefs.enabled != null) {
            isGlancedEnabled = prefs.enabled.booleanValue();
        } else {
            isGlancedEnabled = false;
        }
        if (isGlancedEnabled) {
            com.navdy.hud.app.framework.notifications.NotificationManager notificationManager = com.navdy.hud.app.framework.notifications.NotificationManager.getInstance();
            if (notificationManager.makeNotificationCurrent(true)) {
                sLogger.d("glances available");
                com.navdy.hud.app.analytics.AnalyticsSupport.setGlanceNavigationSource("dial");
                notificationManager.showNotification();
                return;
            }
            sLogger.d("no glances found");
            android.os.Bundle bundle = new android.os.Bundle();
            bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_TIMEOUT, 1000);
            bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_IMAGE, com.navdy.hud.app.R.drawable.icon_qm_glances_grey);
            bundle.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_2, resources2.getString(com.navdy.hud.app.R.string.glances_none));
            bundle.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_2_STYLE, com.navdy.hud.app.R.style.Glances_1);
            com.navdy.hud.app.framework.toast.ToastManager.getInstance().addToast(new com.navdy.hud.app.framework.toast.ToastManager.ToastParams("glance-none", bundle, null, true, false));
            this.presenter.performSelectionAnimation(new com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils.ActionRunnable(this.bus, com.navdy.service.library.events.ui.Screen.SCREEN_BACK));
            return;
        }
        sLogger.v("glances are not enabled");
        android.os.Bundle bundle2 = new android.os.Bundle();
        bundle2.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_CHOICE_LAYOUT_PADDING, (int) resources2.getDimension(com.navdy.hud.app.R.dimen.no_glances_choice_padding));
        bundle2.putBoolean(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_DEFAULT_CHOICE, true);
        bundle2.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_TIMEOUT, 5000);
        bundle2.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_IMAGE, com.navdy.hud.app.R.drawable.icon_glances_disabled_modal);
        bundle2.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_SIDE_IMAGE, com.navdy.hud.app.R.drawable.icon_disabled);
        bundle2.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_2, resources2.getString(com.navdy.hud.app.R.string.glances_disabled));
        bundle2.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_2_STYLE, com.navdy.hud.app.R.style.Glances_Disabled_1);
        bundle2.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_3, resources2.getString(com.navdy.hud.app.R.string.glances_disabled_msg));
        bundle2.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_3_STYLE, com.navdy.hud.app.R.style.Glances_Disabled_2);
        bundle2.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_INFO_CONTAINER_MAX_WIDTH, (int) resources2.getDimension(com.navdy.hud.app.R.dimen.no_glances_width));
        bundle2.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_TTS, com.navdy.hud.app.framework.voice.TTSUtils.TTS_GLANCES_DISABLED);
        com.navdy.hud.app.framework.toast.ToastManager.getInstance().addToast(new com.navdy.hud.app.framework.toast.ToastManager.ToastParams("glance-disabled", bundle2, null, true, false));
        this.presenter.performSelectionAnimation(new com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils.ActionRunnable(this.bus, com.navdy.service.library.events.ui.Screen.SCREEN_BACK));
    }

    /* access modifiers changed from: private */
    public java.lang.String getToolTipString(int id) {
        switch (id) {
            case com.navdy.hud.app.R.id.main_menu_active_trip /*2131623957*/:
                if (this.currentTta == null) {
                    return activeTripTitle;
                }
                return this.currentTta;
            case com.navdy.hud.app.R.id.main_menu_call /*2131623960*/:
                com.navdy.hud.app.framework.phonecall.CallNotification callNotification = (com.navdy.hud.app.framework.phonecall.CallNotification) com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().getNotification(com.navdy.hud.app.framework.notifications.NotificationId.PHONE_CALL_NOTIFICATION_ID);
                if (callNotification == null || !callManager.isCallInProgress()) {
                    return null;
                }
                java.lang.String name = callNotification.getCaller();
                if (name == null) {
                    return null;
                }
                int time = callManager.getCurrentCallDuration();
                if (time < 0) {
                    return null;
                }
                java.lang.String duration = getDurationStr((long) time);
                return resources.getString(com.navdy.hud.app.R.string.mm_call_info, new java.lang.Object[]{name, duration});
            case com.navdy.hud.app.R.id.main_menu_glances /*2131623966*/:
                int notificationCount = com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().getNotificationCount();
                if (notificationCount == 1) {
                    return resources.getString(com.navdy.hud.app.R.string.mm_glances_single);
                }
                return resources.getString(com.navdy.hud.app.R.string.mm_glances_subtitle, new java.lang.Object[]{java.lang.Integer.valueOf(notificationCount)});
            case com.navdy.hud.app.R.id.main_menu_maps_options /*2131623969*/:
                return mapTitle;
            case com.navdy.hud.app.R.id.main_menu_now_playing /*2131623976*/:
                return getMusicTrackInfo();
            case com.navdy.hud.app.R.id.main_menu_smart_dash_options /*2131624000*/:
                return dashTitle;
            case com.navdy.hud.app.R.id.settings_menu_dial_update /*2131624063*/:
                java.lang.String updateTargetVersion = "1.0." + com.navdy.hud.app.device.dial.DialManager.getInstance().getDialFirmwareUpdater().getVersions().local.incrementalVersion;
                return resources.getString(com.navdy.hud.app.R.string.mm_dial_update, new java.lang.Object[]{updateTargetVersion});
            case com.navdy.hud.app.R.id.settings_menu_software_update /*2131624068*/:
                return this.swVersion;
            default:
                return null;
        }
    }

    private java.lang.String getDurationStr(long time) {
        if (time < 3600) {
            int minutes = ((int) time) / 60;
            return java.lang.String.format("%02d:%02d", new java.lang.Object[]{java.lang.Integer.valueOf(minutes), java.lang.Integer.valueOf((int) (time - ((long) (minutes * 60))))});
        }
        int hours = ((int) time) / 3600;
        long time2 = (long) (((int) time) - ((hours * 60) * 60));
        int minutes2 = ((int) time2) / 60;
        return java.lang.String.format("%02d:%02d:%02d", new java.lang.Object[]{java.lang.Integer.valueOf(hours), java.lang.Integer.valueOf(minutes2), java.lang.Integer.valueOf((int) (time2 - ((long) (minutes2 * 60))))});
    }

    /* access modifiers changed from: private */
    public java.lang.String getMusicTrackInfo() {
        boolean isValidAuthor;
        boolean isValidName;
        if (this.addedTrackInfo == null) {
            return null;
        }
        java.lang.String name = this.addedTrackInfo.name;
        java.lang.String author = this.addedTrackInfo.author;
        if (!android.text.TextUtils.isEmpty(author)) {
            isValidAuthor = true;
        } else {
            isValidAuthor = false;
        }
        if (!android.text.TextUtils.isEmpty(name)) {
            isValidName = true;
        } else {
            isValidName = false;
        }
        if (isValidAuthor && isValidName) {
            return author + " - " + name;
        }
        if (!isValidName) {
            return author;
        }
        return name;
    }

    private void startActivityTrayRunnable(int id, int pos) {
        this.curToolTipId = id;
        this.curToolTipPos = pos;
        this.activityTrayRunning = true;
        handler.postDelayed(this.activityTrayRunnable, (long) ACTIVITY_TRAY_UPDATE);
    }

    private void stopActivityTrayRunnable() {
        this.curToolTipId = -1;
        this.curToolTipPos = -1;
        this.activityTrayRunning = false;
        handler.removeCallbacks(this.activityTrayRunnable);
    }
}
