package com.navdy.hud.app.ui.component.mainmenu;

@flow.Layout(2130903134)
public class MainMenuScreen2 extends com.navdy.hud.app.screen.BaseScreen {
    public static final java.lang.String ARG_CONTACTS_LIST = "CONTACTS";
    public static final java.lang.String ARG_MENU_MODE = "MENU_MODE";
    public static final java.lang.String ARG_MENU_PATH = "MENU_PATH";
    public static final java.lang.String ARG_NOTIFICATION_ID = "NOTIF_ID";
    static final java.lang.String SLASH = "/";
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.class);

    public enum MenuMode {
        MAIN_MENU,
        REPLY_PICKER,
        SNAPSHOT_TITLE_PICKER
    }

    @dagger.Module(addsTo = com.navdy.hud.app.ui.activity.Main.Module.class, injects = {com.navdy.hud.app.ui.component.mainmenu.MainMenuView2.class})
    public class Module {
        public Module() {
        }
    }

    @javax.inject.Singleton
    public static class Presenter extends com.navdy.hud.app.ui.framework.BasePresenter<com.navdy.hud.app.ui.component.mainmenu.MainMenuView2> {
        private boolean animateIn;
        @javax.inject.Inject
        com.squareup.otto.Bus bus;
        private boolean closed;
        /* access modifiers changed from: private */
        public com.navdy.hud.app.ui.component.mainmenu.IMenu currentMenu;
        private boolean firstLoad;
        private boolean handledSelection;
        private android.view.View[] leftView;
        private com.navdy.hud.app.ui.component.mainmenu.MainMenu mainMenu;
        private boolean mapShown;
        private com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.MenuMode menuMode;
        private java.lang.String menuPath;
        private boolean navEnded;
        private boolean registered;
        @javax.inject.Inject
        android.content.SharedPreferences sharedPreferences;
        private com.navdy.hud.app.ui.component.homescreen.HomeScreen.DisplayMode switchBackMode;
        @javax.inject.Inject
        com.navdy.hud.app.framework.trips.TripManager tripManager;
        private com.navdy.hud.app.ui.framework.UIStateManager uiStateManager;

        class Anon1 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
            final /* synthetic */ java.lang.Runnable val$endAction;

            Anon1(java.lang.Runnable runnable) {
                this.val$endAction = runnable;
            }

            public void onAnimationEnd(android.animation.Animator animation) {
                com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.sLogger.v("post back");
                com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.Presenter.this.bus.post(new com.navdy.service.library.events.ui.ShowScreen.Builder().screen(com.navdy.service.library.events.ui.Screen.SCREEN_BACK).build());
                if (this.val$endAction != null) {
                    this.val$endAction.run();
                }
            }
        }

        class Anon2 implements java.lang.Runnable {
            final /* synthetic */ boolean val$hasScrollModel;

            Anon2(boolean z) {
                this.val$hasScrollModel = z;
            }

            public void run() {
                com.navdy.hud.app.ui.component.mainmenu.MainMenuView2 view = (com.navdy.hud.app.ui.component.mainmenu.MainMenuView2) com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.Presenter.this.getView();
                if (view != null) {
                    com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.sLogger.v("performEnterAnimation:" + com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.Presenter.this.currentMenu.getType());
                    view.vmenuComponent.unlock(false);
                    view.vmenuComponent.verticalList.setBindCallbacks(com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.Presenter.this.currentMenu.isBindCallsEnabled());
                    view.vmenuComponent.updateView(com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.Presenter.this.currentMenu.getItems(), com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.Presenter.this.currentMenu.getInitialSelection(), true, this.val$hasScrollModel, com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.Presenter.this.currentMenu.getScrollIndex());
                }
            }
        }

        class Anon3 implements java.lang.Runnable {
            final /* synthetic */ boolean val$hasScrollModel;
            final /* synthetic */ int val$itemSelection;
            final /* synthetic */ com.navdy.hud.app.ui.component.mainmenu.IMenu val$menu;
            final /* synthetic */ java.util.List val$parentList;

            Anon3(com.navdy.hud.app.ui.component.mainmenu.IMenu iMenu, java.util.List list, int i, boolean z) {
                this.val$menu = iMenu;
                this.val$parentList = list;
                this.val$itemSelection = i;
                this.val$hasScrollModel = z;
            }

            public void run() {
                com.navdy.hud.app.ui.component.mainmenu.MainMenuView2 view = (com.navdy.hud.app.ui.component.mainmenu.MainMenuView2) com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.Presenter.this.getView();
                if (view != null) {
                    com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.sLogger.v("Loading menu:" + this.val$menu.getType());
                    if (com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.Presenter.this.currentMenu != null) {
                        com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.Presenter.this.currentMenu.onUnload(com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel.BACK_TO_PARENT);
                    }
                    com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.Presenter.this.currentMenu = this.val$menu;
                    com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.Presenter.this.currentMenu.setSelectedIcon();
                    com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.sLogger.v("performBackAnimation:" + com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.Presenter.this.currentMenu.getType());
                    view.vmenuComponent.unlock(false);
                    view.vmenuComponent.verticalList.setBindCallbacks(com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.Presenter.this.currentMenu.isBindCallsEnabled());
                    view.vmenuComponent.updateView(this.val$parentList, this.val$itemSelection, com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.Presenter.this.currentMenu.isFirstItemEmpty(), this.val$hasScrollModel, com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.Presenter.this.currentMenu.getScrollIndex());
                }
            }
        }

        public void onLoad(android.os.Bundle savedInstanceState) {
            reset();
            this.uiStateManager = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getUiStateManager();
            this.bus.register(this);
            this.registered = true;
            this.firstLoad = true;
            this.menuPath = null;
            this.menuMode = com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.MenuMode.MAIN_MENU;
            if (savedInstanceState != null) {
                this.menuPath = savedInstanceState.getString(com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.ARG_MENU_PATH);
                com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.sLogger.v("menu_path:" + this.menuPath);
                int modeOrdinal = savedInstanceState.getInt(com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.ARG_MENU_MODE, -1);
                if (modeOrdinal != -1) {
                    this.menuMode = com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.MenuMode.values()[modeOrdinal];
                    com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.sLogger.v("menu_mode:" + this.menuMode);
                }
            }
            updateView(savedInstanceState);
            super.onLoad(savedInstanceState);
        }

        public void onUnload() {
            if (this.registered) {
                this.registered = false;
                this.bus.unregister(this);
            }
            reset();
            this.mainMenu = null;
            this.currentMenu = null;
            super.onUnload();
        }

        /* access modifiers changed from: 0000 */
        public void reset() {
            this.closed = false;
            this.handledSelection = false;
            this.animateIn = false;
            this.firstLoad = false;
            this.mapShown = false;
            this.navEnded = false;
        }

        /* access modifiers changed from: protected */
        public void updateView(android.os.Bundle savedInstanceState) {
            com.navdy.hud.app.ui.component.mainmenu.MainMenuView2 view = (com.navdy.hud.app.ui.component.mainmenu.MainMenuView2) getView();
            if (view != null) {
                init(view, savedInstanceState);
            }
        }

        /* access modifiers changed from: 0000 */
        public void init(com.navdy.hud.app.ui.component.mainmenu.MainMenuView2 view, android.os.Bundle savedInstanceState) {
            switch (this.menuMode) {
                case MAIN_MENU:
                    this.mainMenu = new com.navdy.hud.app.ui.component.mainmenu.MainMenu(this.bus, view.vmenuComponent, this);
                    if (android.text.TextUtils.isEmpty(this.menuPath)) {
                        loadMenu(this.mainMenu, null, 0, 0);
                        return;
                    } else {
                        loadMenu(buildMenuPath(this.mainMenu, this.menuPath), null, 0, 0);
                        return;
                    }
                case REPLY_PICKER:
                    java.util.ArrayList<android.os.Parcelable> list = savedInstanceState.getParcelableArrayList(com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.ARG_CONTACTS_LIST);
                    java.lang.String notifId = savedInstanceState.getString(com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.ARG_NOTIFICATION_ID);
                    java.util.List<com.navdy.hud.app.framework.contacts.Contact> contacts = new java.util.ArrayList<>();
                    java.util.Iterator it = list.iterator();
                    while (it.hasNext()) {
                        contacts.add((com.navdy.hud.app.framework.contacts.Contact) ((android.os.Parcelable) it.next()));
                    }
                    this.currentMenu = new com.navdy.hud.app.ui.component.mainmenu.ContactOptionsMenu(contacts, notifId, view.vmenuComponent, this, null, this.bus);
                    loadMenu(this.currentMenu, null, 0, 0);
                    return;
                case SNAPSHOT_TITLE_PICKER:
                    this.currentMenu = new com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu(view.vmenuComponent, this, null, com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu.ReportIssueMenuType.SNAP_SHOT);
                    loadMenu(this.currentMenu, null, 0, 0);
                    return;
                default:
                    return;
            }
        }

        /* access modifiers changed from: 0000 */
        public boolean selectItem(com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState selection) {
            if (this.currentMenu == null) {
                return true;
            }
            if (this.handledSelection) {
                com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.sLogger.v("already handled [" + selection.id + "], " + selection.pos);
                return true;
            }
            this.handledSelection = this.currentMenu.selectItem(selection);
            return this.handledSelection;
        }

        /* access modifiers changed from: 0000 */
        public void resetSelectedItem() {
            com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.sLogger.v("resetSelectedItem");
            this.handledSelection = false;
            if (!this.animateIn) {
                this.animateIn = true;
                com.navdy.hud.app.ui.component.mainmenu.MainMenuView2 view = (com.navdy.hud.app.ui.component.mainmenu.MainMenuView2) getView();
                if (view != null) {
                    view.vmenuComponent.animateIn(null);
                }
            }
        }

        /* access modifiers changed from: 0000 */
        public void close() {
            if (this.currentMenu != null && this.currentMenu.getType() == com.navdy.hud.app.ui.component.mainmenu.IMenu.Menu.SEARCH) {
                com.navdy.hud.app.analytics.AnalyticsSupport.recordNearbySearchClosed();
            }
            close(null);
        }

        /* access modifiers changed from: 0000 */
        public void close(java.lang.Runnable endAction) {
            if (this.closed) {
                com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.sLogger.v("already closed");
                return;
            }
            this.closed = true;
            com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.sLogger.v("close");
            com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("exit");
            com.navdy.hud.app.ui.component.mainmenu.MainMenuView2 view = (com.navdy.hud.app.ui.component.mainmenu.MainMenuView2) getView();
            if (view != null) {
                view.vmenuComponent.animateOut(new com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.Presenter.Anon1(endAction));
            }
        }

        /* access modifiers changed from: 0000 */
        public boolean isClosed() {
            return this.closed;
        }

        /* access modifiers changed from: 0000 */
        public boolean isItemClickable(com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState selection) {
            if (this.currentMenu != null) {
                return this.currentMenu.isItemClickable(selection.id, selection.pos);
            }
            return false;
        }

        /* access modifiers changed from: 0000 */
        public void loadMenu(com.navdy.hud.app.ui.component.mainmenu.IMenu menu, com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel level, int backSelection, int backSelectionId) {
            loadMenu(menu, level, backSelection, backSelectionId, false);
        }

        /* access modifiers changed from: 0000 */
        public void loadMenu(com.navdy.hud.app.ui.component.mainmenu.IMenu menu, com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel level, int backSelection, int backSelectionId, boolean hasScrollModel) {
            loadMenu(menu, level, backSelection, backSelectionId, hasScrollModel, 0);
        }

        /* access modifiers changed from: 0000 */
        public void loadMenu(com.navdy.hud.app.ui.component.mainmenu.IMenu menu, com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel level, int backSelection, int backSelectionId, boolean hasScrollModel, int xOffset) {
            hideToolTip();
            if (level == com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel.ROOT && isMapShown()) {
                disableMapViews();
            }
            com.navdy.hud.app.ui.component.mainmenu.MainMenuView2 view = (com.navdy.hud.app.ui.component.mainmenu.MainMenuView2) getView();
            if (view == null) {
                return;
            }
            if (this.firstLoad) {
                com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.sLogger.v("Loading menu:" + menu.getType());
                this.currentMenu = menu;
                this.currentMenu.setSelectedIcon();
                this.firstLoad = false;
                view.vmenuComponent.verticalList.setBindCallbacks(this.currentMenu.isBindCallsEnabled());
                view.vmenuComponent.updateView(this.currentMenu.getItems(), this.currentMenu.getInitialSelection(), this.currentMenu.isFirstItemEmpty());
                return;
            }
            switch (level) {
                case SUB_LEVEL:
                    com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.sLogger.v("Loading menu:" + menu.getType());
                    com.navdy.hud.app.ui.component.vlist.VerticalList.Model item = this.currentMenu.getModelfromPos(backSelection);
                    if (this.currentMenu != null) {
                        this.currentMenu.onUnload(com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel.SUB_LEVEL);
                    }
                    this.currentMenu = menu;
                    this.currentMenu.setBackSelectionPos(backSelection);
                    this.currentMenu.setSelectedIcon();
                    view.vmenuComponent.performEnterAnimation(new com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.Presenter.Anon2(hasScrollModel), null, item);
                    return;
                case BACK_TO_PARENT:
                    com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.sLogger.v("backSelectionId:" + backSelectionId);
                    java.util.List<com.navdy.hud.app.ui.component.vlist.VerticalList.Model> parentList = menu.getItems();
                    int selection = backSelection;
                    if (backSelectionId != 0) {
                        int index = findEntry(parentList, backSelectionId);
                        if (index != -1) {
                            selection = index;
                        }
                        com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.sLogger.v("backSelectionId:" + backSelectionId + " index=" + index);
                    }
                    com.navdy.hud.app.ui.component.vlist.VerticalList.Model item2 = null;
                    if (selection < parentList.size()) {
                        item2 = (com.navdy.hud.app.ui.component.vlist.VerticalList.Model) parentList.get(selection);
                    }
                    view.vmenuComponent.performBackAnimation(new com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.Presenter.Anon3(menu, parentList, selection, hasScrollModel), null, item2, xOffset);
                    return;
                case REFRESH_CURRENT:
                    if (this.currentMenu == menu) {
                        com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.sLogger.v("refresh current menu:" + this.currentMenu.getType());
                        view.vmenuComponent.updateView(this.currentMenu.getItems(), this.currentMenu.getInitialSelection(), true, hasScrollModel, view.vmenuComponent.getFastScrollIndex());
                        return;
                    }
                    com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.sLogger.w("refresh current menu different cur=" + this.currentMenu.getType() + " passed:" + menu.getType());
                    return;
                case ROOT:
                    com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.sLogger.v("refresh root menu:" + menu.getType());
                    this.currentMenu = menu;
                    this.currentMenu.setSelectedIcon();
                    view.vmenuComponent.updateView(menu.getItems(), menu.getInitialSelection(), this.currentMenu.isFirstItemEmpty());
                    return;
                default:
                    return;
            }
        }

        /* access modifiers changed from: 0000 */
        public void updateCurrentMenu(com.navdy.hud.app.ui.component.mainmenu.IMenu menu) {
            com.navdy.hud.app.ui.component.mainmenu.MainMenuView2 view = (com.navdy.hud.app.ui.component.mainmenu.MainMenuView2) getView();
            if (view != null) {
                if (this.closed) {
                    com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.sLogger.v("updateCurrentMenu already closed");
                } else if (this.currentMenu == menu) {
                    com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.sLogger.v("updateCurrentMenu:" + menu.getType());
                    view.vmenuComponent.unlock(false);
                    view.vmenuComponent.verticalList.setBindCallbacks(this.currentMenu.isBindCallsEnabled());
                    view.vmenuComponent.updateView(this.currentMenu.getItems(), this.currentMenu.getInitialSelection(), true, false, this.currentMenu.getScrollIndex());
                } else {
                    com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.sLogger.v("updateCurrentMenu update menu:" + menu.getType() + " different from current:" + this.currentMenu.getType());
                }
            }
        }

        /* access modifiers changed from: 0000 */
        public void refreshData(int pos, com.navdy.hud.app.ui.component.vlist.VerticalList.Model[] models, com.navdy.hud.app.ui.component.mainmenu.IMenu menu) {
            com.navdy.hud.app.ui.component.mainmenu.MainMenuView2 view = (com.navdy.hud.app.ui.component.mainmenu.MainMenuView2) getView();
            if (view != null) {
                if (this.closed) {
                    com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.sLogger.v("refreshData(s) already closed");
                } else if (this.currentMenu == menu) {
                    com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.sLogger.v("refreshData(s):" + menu.getType());
                    view.vmenuComponent.verticalList.refreshData(pos, models);
                } else {
                    com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.sLogger.v("refreshData(s) update menu:" + menu.getType() + " different from current:" + this.currentMenu.getType());
                }
            }
        }

        /* access modifiers changed from: 0000 */
        public void performSelectionAnimation(java.lang.Runnable endAction) {
            performSelectionAnimation(endAction, 0);
        }

        /* access modifiers changed from: 0000 */
        public void performSelectionAnimation(java.lang.Runnable endAction, int delay) {
            com.navdy.hud.app.ui.component.mainmenu.MainMenuView2 view = (com.navdy.hud.app.ui.component.mainmenu.MainMenuView2) getView();
            if (view != null) {
                view.performSelectionAnimation(endAction, delay);
            }
        }

        /* access modifiers changed from: 0000 */
        public com.navdy.hud.app.ui.component.ConfirmationLayout getConfirmationLayout() {
            com.navdy.hud.app.ui.component.mainmenu.MainMenuView2 view = (com.navdy.hud.app.ui.component.mainmenu.MainMenuView2) getView();
            if (view != null) {
                return view.confirmationLayout;
            }
            return null;
        }

        /* access modifiers changed from: 0000 */
        public com.navdy.hud.app.ui.component.mainmenu.IMenu getCurrentMenu() {
            return this.currentMenu;
        }

        /* access modifiers changed from: 0000 */
        public void refreshDataforPos(int pos) {
            refreshDataforPos(pos, true);
        }

        /* access modifiers changed from: 0000 */
        public void refreshDataforPos(int pos, boolean startFluctuator) {
            com.navdy.hud.app.ui.component.mainmenu.MainMenuView2 view = (com.navdy.hud.app.ui.component.mainmenu.MainMenuView2) getView();
            if (view != null) {
                view.vmenuComponent.verticalList.refreshData(pos, null, !startFluctuator);
            }
        }

        /* access modifiers changed from: 0000 */
        public int getCurrentSelection() {
            com.navdy.hud.app.ui.component.mainmenu.MainMenuView2 view = (com.navdy.hud.app.ui.component.mainmenu.MainMenuView2) getView();
            if (view != null) {
                return view.vmenuComponent.verticalList.getCurrentPosition();
            }
            return 0;
        }

        /* access modifiers changed from: 0000 */
        public void setInitialItemState(boolean b) {
            com.navdy.hud.app.ui.component.mainmenu.MainMenuView2 view = (com.navdy.hud.app.ui.component.mainmenu.MainMenuView2) getView();
            if (view != null) {
                view.vmenuComponent.verticalList.adapter.setInitialState(b);
            }
        }

        /* access modifiers changed from: 0000 */
        public com.navdy.hud.app.ui.component.mainmenu.IMenu buildMenuPath(com.navdy.hud.app.ui.component.mainmenu.MainMenu menu, java.lang.String menuPath2) {
            java.lang.String menuPath3;
            try {
                if (menuPath2.indexOf("/") != 0) {
                    com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.sLogger.w("menu_path does not start with slash");
                    return menu;
                }
                java.lang.String element = menuPath2.substring(1);
                int index = element.indexOf("/");
                if (index >= 0) {
                    menuPath3 = menuPath2.substring(index + 1);
                    element = element.substring(0, index);
                } else {
                    menuPath3 = null;
                }
                com.navdy.hud.app.ui.component.mainmenu.IMenu child = menu.getChildMenu(null, element, menuPath3);
                if (child != null) {
                    return child;
                }
                return menu;
            } catch (Throwable t) {
                com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.sLogger.e(t);
                return menu;
            }
        }

        private int findEntry(java.util.List<com.navdy.hud.app.ui.component.vlist.VerticalList.Model> list, int id) {
            int len = list.size();
            for (int i = 0; i < len; i++) {
                if (((com.navdy.hud.app.ui.component.vlist.VerticalList.Model) list.get(i)).id == id) {
                    return i;
                }
            }
            return -1;
        }

        /* access modifiers changed from: 0000 */
        public void cancelLoadingAnimation(int pos) {
            com.navdy.hud.app.ui.component.mainmenu.MainMenuView2 view = (com.navdy.hud.app.ui.component.mainmenu.MainMenuView2) getView();
            if (view != null) {
                view.vmenuComponent.verticalList.cancelLoadingAnimation(pos);
            }
        }

        /* access modifiers changed from: 0000 */
        public void setScrollIdleEvents(boolean send) {
            com.navdy.hud.app.ui.component.mainmenu.MainMenuView2 view = (com.navdy.hud.app.ui.component.mainmenu.MainMenuView2) getView();
            if (view != null) {
                view.vmenuComponent.verticalList.setScrollIdleEvent(send);
            }
        }

        @com.squareup.otto.Subscribe
        public void onConnectionStateChange(com.navdy.service.library.events.connection.ConnectionStateChange event) {
            if (this.currentMenu != null) {
                switch (event.state) {
                    case CONNECTION_DISCONNECTED:
                        switch (this.menuMode) {
                            case MAIN_MENU:
                                com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.sLogger.v("disconnected:" + this.currentMenu.getType());
                                if (this.mainMenu != null) {
                                    this.mainMenu.clearState();
                                }
                                if (this.currentMenu.getType() == com.navdy.hud.app.ui.component.mainmenu.IMenu.Menu.MAIN) {
                                    com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.sLogger.v("disconnected: refresh main menu");
                                    loadMenu(this.currentMenu, com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel.REFRESH_CURRENT, 0, 0);
                                    return;
                                }
                                com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.sLogger.v("disconnected: refresh menu,back to root");
                                loadMenu(this.mainMenu, com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel.ROOT, 0, 0);
                                return;
                            case REPLY_PICKER:
                                close();
                                return;
                            default:
                                return;
                        }
                    default:
                        return;
                }
            }
        }

        @com.squareup.otto.Subscribe
        public void onDeviceInfoAvailable(com.navdy.hud.app.event.DeviceInfoAvailable event) {
            if (this.currentMenu != null && this.menuMode == com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.MenuMode.MAIN_MENU) {
                com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.sLogger.v("onDeviceInfoAvailable:" + this.currentMenu.getType());
                if (this.currentMenu.getType() == com.navdy.hud.app.ui.component.mainmenu.IMenu.Menu.MAIN && this.currentMenu.getModelfromPos(this.currentMenu.getInitialSelection()).id == com.navdy.hud.app.R.id.main_menu_settings_connect_phone) {
                    com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.sLogger.v("connected: refresh main menu");
                    loadMenu(this.currentMenu, com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel.REFRESH_CURRENT, 0, 0);
                }
            }
        }

        @com.squareup.otto.Subscribe
        public void onNavigationModeChanged(com.navdy.hud.app.maps.MapEvents.NavigationModeChange event) {
            if (this.currentMenu != null && this.menuMode == com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.MenuMode.MAIN_MENU) {
                com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.sLogger.v("onNavigationModeChanged:" + event.navigationMode);
                switch (this.currentMenu.getType()) {
                    case MAIN:
                        loadMenu(this.currentMenu, com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel.REFRESH_CURRENT, 0, 0);
                        return;
                    case ACTIVE_TRIP:
                        loadMenu(this.mainMenu, com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel.ROOT, 0, 0);
                        return;
                    default:
                        return;
                }
            }
        }

        @com.squareup.otto.Subscribe
        public void onMapEngineReady(com.navdy.hud.app.maps.MapEvents.MapEngineReady event) {
            if (this.currentMenu != null && this.menuMode == com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.MenuMode.MAIN_MENU) {
                com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.sLogger.v("onMapEngineReady");
                switch (this.currentMenu.getType()) {
                    case MAIN:
                        loadMenu(this.currentMenu, com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel.REFRESH_CURRENT, 0, 0);
                        return;
                    default:
                        return;
                }
            }
        }

        /* access modifiers changed from: 0000 */
        public void sendCloseEvent() {
            hideMap();
            if (this.currentMenu != this.mainMenu) {
                this.currentMenu.onUnload(com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel.CLOSE);
            }
            if (this.mainMenu != null) {
                this.mainMenu.onUnload(com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel.CLOSE);
            }
        }

        public void showScrimCover() {
            com.navdy.hud.app.ui.component.mainmenu.MainMenuView2 view = (com.navdy.hud.app.ui.component.mainmenu.MainMenuView2) getView();
            if (view != null) {
                com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.sLogger.v("scrim-show");
                view.scrimCover.setVisibility(0);
            }
        }

        public void hideScrimCover() {
            com.navdy.hud.app.ui.component.mainmenu.MainMenuView2 view = (com.navdy.hud.app.ui.component.mainmenu.MainMenuView2) getView();
            if (view != null) {
                com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.sLogger.v("scrim-hide");
                view.scrimCover.setVisibility(8);
            }
        }

        public void showToolTip(int posIndex, java.lang.String text) {
            com.navdy.hud.app.ui.component.mainmenu.MainMenuView2 view = (com.navdy.hud.app.ui.component.mainmenu.MainMenuView2) getView();
            if (view != null) {
                view.vmenuComponent.showToolTip(posIndex, text);
            }
        }

        public void hideToolTip() {
            com.navdy.hud.app.ui.component.mainmenu.MainMenuView2 view = (com.navdy.hud.app.ui.component.mainmenu.MainMenuView2) getView();
            if (view != null) {
                view.vmenuComponent.hideToolTip();
            }
        }

        public int getCurrentSubSelectionId() {
            com.navdy.hud.app.ui.component.mainmenu.MainMenuView2 view = (com.navdy.hud.app.ui.component.mainmenu.MainMenuView2) getView();
            if (view != null) {
                com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder vh = view.vmenuComponent.verticalList.getCurrentViewHolder();
                if (vh instanceof com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder) {
                    return ((com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder) vh).getCurrentSelectionId();
                }
            }
            return -1;
        }

        public int getCurrentSubSelectionPos() {
            com.navdy.hud.app.ui.component.mainmenu.MainMenuView2 view = (com.navdy.hud.app.ui.component.mainmenu.MainMenuView2) getView();
            if (view != null) {
                com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder vh = view.vmenuComponent.verticalList.getCurrentViewHolder();
                if (vh instanceof com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder) {
                    return ((com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder) vh).getCurrentSelection();
                }
            }
            return -1;
        }

        public boolean isMapShown() {
            return this.mapShown;
        }

        public void showMap() {
            com.here.android.mpa.common.GeoBoundingBox boundingBox;
            com.here.android.mpa.common.GeoBoundingBox boundingBox2;
            if (this.mapShown) {
                com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.sLogger.v("showMap: already shown");
                return;
            }
            this.mapShown = true;
            if (((com.navdy.hud.app.ui.component.mainmenu.MainMenuView2) getView()) != null) {
                com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.sLogger.v("showMap");
                com.navdy.hud.app.ui.component.homescreen.HomeScreenView homeScreenView = this.uiStateManager.getHomescreenView();
                com.navdy.hud.app.ui.component.homescreen.NavigationView navigationView = this.uiStateManager.getNavigationView();
                this.switchBackMode = homeScreenView.getDisplayMode();
                if (this.switchBackMode == com.navdy.hud.app.ui.component.homescreen.HomeScreen.DisplayMode.MAP) {
                    com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.sLogger.v("showMap: switchbackmode null");
                    this.switchBackMode = null;
                } else {
                    com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.sLogger.v(" showMap switchbackmode: " + this.switchBackMode);
                    homeScreenView.setDisplayMode(com.navdy.hud.app.ui.component.homescreen.HomeScreen.DisplayMode.MAP);
                }
                com.navdy.hud.app.maps.here.HereNavigationManager hereNavigationManager = com.navdy.hud.app.maps.here.HereNavigationManager.getInstance();
                com.here.android.mpa.common.GeoCoordinate start = com.navdy.hud.app.maps.here.HereMapsManager.getInstance().getLocationFixManager().getLastGeoCoordinate();
                com.here.android.mpa.common.GeoCoordinate end = null;
                com.navdy.service.library.events.navigation.NavigationRouteRequest current = hereNavigationManager.getCurrentNavigationRouteRequest();
                if (current != null) {
                    if (current.destination.latitude.doubleValue() != 0.0d && current.destination.longitude.doubleValue() != 0.0d) {
                        end = new com.here.android.mpa.common.GeoCoordinate(current.destination.latitude.doubleValue(), current.destination.longitude.doubleValue());
                    } else if (!(current.destinationDisplay == null || current.destinationDisplay.latitude.doubleValue() == 0.0d || current.destinationDisplay.longitude.doubleValue() == 0.0d)) {
                        end = new com.here.android.mpa.common.GeoCoordinate(current.destinationDisplay.latitude.doubleValue(), current.destinationDisplay.longitude.doubleValue());
                    }
                }
                java.lang.String routeId = hereNavigationManager.getCurrentRouteId();
                com.here.android.mpa.routing.Route route = null;
                if (routeId != null) {
                    com.navdy.hud.app.maps.here.HereRouteCache.RouteInfo routeInfo = com.navdy.hud.app.maps.here.HereRouteCache.getInstance().getRoute(routeId);
                    if (routeInfo != null) {
                        route = routeInfo.route;
                        boundingBox = routeInfo.route.getBoundingBox();
                        if (boundingBox == null && start != null) {
                            boundingBox2 = new com.here.android.mpa.common.GeoBoundingBox(start, 5000.0f, 5000.0f);
                            navigationView.switchToRouteSearchMode(start, end, boundingBox2, route, false, null, -1);
                            enableMapViews();
                        }
                        boundingBox2 = boundingBox;
                        navigationView.switchToRouteSearchMode(start, end, boundingBox2, route, false, null, -1);
                        enableMapViews();
                    }
                }
                boundingBox = null;
                try {
                    boundingBox2 = new com.here.android.mpa.common.GeoBoundingBox(start, 5000.0f, 5000.0f);
                } catch (Throwable t) {
                    com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.sLogger.e(t);
                }
                navigationView.switchToRouteSearchMode(start, end, boundingBox2, route, false, null, -1);
                enableMapViews();
            }
        }

        public void showBoundingBox(com.here.android.mpa.common.GeoBoundingBox boundingBox) {
            if (((com.navdy.hud.app.ui.component.mainmenu.MainMenuView2) getView()) != null && this.mapShown && boundingBox != null) {
                this.uiStateManager.getNavigationView().zoomToBoundBox(boundingBox, null, false, false);
            }
        }

        public com.here.android.mpa.common.GeoBoundingBox getCurrentRouteBoundingBox() {
            java.lang.String routeId = com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().getCurrentRouteId();
            if (routeId != null) {
                com.navdy.hud.app.maps.here.HereRouteCache.RouteInfo routeInfo = com.navdy.hud.app.maps.here.HereRouteCache.getInstance().getRoute(routeId);
                if (routeInfo != null) {
                    return routeInfo.route.getBoundingBox();
                }
            }
            return null;
        }

        public void setNavEnded() {
            this.navEnded = true;
        }

        public void hideMap() {
            boolean z = true;
            if (this.mapShown) {
                this.mapShown = false;
                if (((com.navdy.hud.app.ui.component.mainmenu.MainMenuView2) getView()) != null) {
                    com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.sLogger.v("hideMap");
                    disableMapViews();
                    if (this.switchBackMode != null) {
                        com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.sLogger.v("hideMap switchbackmode: " + this.switchBackMode);
                        this.uiStateManager.getHomescreenView().setDisplayMode(this.switchBackMode);
                    }
                    com.navdy.hud.app.ui.component.homescreen.NavigationView navigationView = this.uiStateManager.getNavigationView();
                    if (!this.navEnded) {
                        com.navdy.hud.app.maps.here.HereNavigationManager hereNavigationManager = com.navdy.hud.app.maps.here.HereNavigationManager.getInstance();
                        if (!hereNavigationManager.isNavigationModeOn()) {
                            this.navEnded = true;
                        } else if (hereNavigationManager.hasArrived()) {
                            this.navEnded = true;
                        }
                    }
                    if (this.navEnded) {
                        z = false;
                    }
                    navigationView.switchBackfromRouteSearchMode(z);
                }
            }
        }

        public void cleanMapFluctuator() {
            com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.sLogger.v("cleanMapFluctuator");
            com.navdy.hud.app.ui.component.homescreen.NavigationView navigationView = this.uiStateManager.getNavigationView();
            if (navigationView != null) {
                navigationView.cleanupFluctuator();
            }
        }

        public void setViewBackgroundColor(int color) {
            com.navdy.hud.app.ui.component.mainmenu.MainMenuView2 view = (com.navdy.hud.app.ui.component.mainmenu.MainMenuView2) getView();
            if (view != null) {
                view.setBackgroundColor(color);
            }
        }

        public void enableMapViews() {
            com.navdy.hud.app.ui.component.mainmenu.MainMenuView2 view = (com.navdy.hud.app.ui.component.mainmenu.MainMenuView2) getView();
            if (view != null) {
                int nChilds = view.vmenuComponent.leftContainer.getChildCount();
                com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.sLogger.v("enableMapViews:" + nChilds);
                this.leftView = new android.view.View[nChilds];
                for (int i = 0; i < nChilds; i++) {
                    this.leftView[i] = view.vmenuComponent.leftContainer.getChildAt(i);
                }
                view.vmenuComponent.leftContainer.removeAllViews();
                view.vmenuComponent.leftContainer.setBackgroundColor(0);
                view.vmenuComponent.leftContainer.addView(android.view.LayoutInflater.from(view.getContext()).inflate(com.navdy.hud.app.R.layout.active_trip_menu_mask_lyt, view, false));
                view.vmenuComponent.rightContainer.setBackgroundColor(-16777216);
                view.vmenuComponent.closeContainer.setBackgroundColor(-16777216);
                view.rightBackground.setVisibility(0);
            }
        }

        public void disableMapViews() {
            com.navdy.hud.app.ui.component.mainmenu.MainMenuView2 view = (com.navdy.hud.app.ui.component.mainmenu.MainMenuView2) getView();
            if (view != null) {
                com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.sLogger.v("disableMapViews");
                view.setBackgroundColor(-16777216);
                view.vmenuComponent.leftContainer.removeAllViews();
                view.vmenuComponent.leftContainer.setBackgroundColor(-16777216);
                if (this.leftView != null) {
                    for (android.view.View addView : this.leftView) {
                        view.vmenuComponent.leftContainer.addView(addView);
                    }
                    this.leftView = null;
                }
                view.vmenuComponent.rightContainer.setBackgroundColor(0);
                view.vmenuComponent.closeContainer.setBackgroundColor(0);
                view.rightBackground.setVisibility(8);
            }
        }
    }

    public java.lang.String getMortarScopeName() {
        return getClass().getName();
    }

    public java.lang.Object getDaggerModule() {
        return new com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.Module();
    }

    public com.navdy.service.library.events.ui.Screen getScreen() {
        return com.navdy.service.library.events.ui.Screen.SCREEN_MAIN_MENU;
    }

    public int getAnimationIn(flow.Flow.Direction direction) {
        return -1;
    }

    public int getAnimationOut(flow.Flow.Direction direction) {
        return -1;
    }
}
