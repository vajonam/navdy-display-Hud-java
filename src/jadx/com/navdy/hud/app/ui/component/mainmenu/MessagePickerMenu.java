package com.navdy.hud.app.ui.component.mainmenu;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000x\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\n\u0018\u0000 <2\u00020\u0001:\u0001<B=\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0001\u0012\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\b\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\b\u0010\f\u001a\u0004\u0018\u00010\t\u00a2\u0006\u0002\u0010\rJ(\u0010\u0014\u001a\u0004\u0018\u00010\u00012\b\u0010\u0006\u001a\u0004\u0018\u00010\u00012\b\u0010\u0015\u001a\u0004\u0018\u00010\t2\b\u0010\u0016\u001a\u0004\u0018\u00010\tH\u0016J\b\u0010\u0017\u001a\u00020\u000fH\u0016J\u000e\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u00130\u0012H\u0016J\u0012\u0010\u0019\u001a\u0004\u0018\u00010\u00132\u0006\u0010\u001a\u001a\u00020\u000fH\u0016J\n\u0010\u001b\u001a\u0004\u0018\u00010\u001cH\u0016J\b\u0010\u001d\u001a\u00020\u001eH\u0016J\b\u0010\u001f\u001a\u00020 H\u0016J\b\u0010!\u001a\u00020 H\u0016J\u0018\u0010\"\u001a\u00020 2\u0006\u0010#\u001a\u00020\u000f2\u0006\u0010\u001a\u001a\u00020\u000fH\u0016J.\u0010$\u001a\u00020%2\b\u0010&\u001a\u0004\u0018\u00010\u00132\b\u0010'\u001a\u0004\u0018\u00010(2\u0006\u0010\u001a\u001a\u00020\u000f2\b\u0010)\u001a\u0004\u0018\u00010*H\u0016J\b\u0010+\u001a\u00020%H\u0016J\b\u0010,\u001a\u00020%H\u0016J\u0012\u0010-\u001a\u00020%2\b\u0010.\u001a\u0004\u0018\u00010/H\u0016J\b\u00100\u001a\u00020%H\u0016J\u0012\u00101\u001a\u00020%2\b\u00102\u001a\u0004\u0018\u000103H\u0016J\u0010\u00104\u001a\u00020 2\u0006\u0010.\u001a\u00020/H\u0016J\u0010\u00105\u001a\u00020%2\u0006\u00106\u001a\u00020\tH\u0002J\u0010\u00107\u001a\u00020%2\u0006\u0010#\u001a\u00020\u000fH\u0016J\u0010\u00108\u001a\u00020%2\u0006\u00109\u001a\u00020\u000fH\u0016J\b\u0010:\u001a\u00020%H\u0016J\b\u0010;\u001a\u00020%H\u0016R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u000fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0011\u001a\n\u0012\u0004\u0012\u00020\u0013\u0018\u00010\u0012X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\f\u001a\u0004\u0018\u00010\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0001X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006="}, d2 = {"Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;", "Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;", "vscrollComponent", "Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;", "presenter", "Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;", "parent", "messages", "", "", "contact", "Lcom/navdy/hud/app/framework/contacts/Contact;", "notificationId", "(Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Ljava/util/List;Lcom/navdy/hud/app/framework/contacts/Contact;Ljava/lang/String;)V", "backSelection", "", "backSelectionId", "cachedList", "", "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;", "getChildMenu", "args", "path", "getInitialSelection", "getItems", "getModelfromPos", "pos", "getScrollIndex", "Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;", "getType", "Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;", "isBindCallsEnabled", "", "isFirstItemEmpty", "isItemClickable", "id", "onBindToView", "", "model", "view", "Landroid/view/View;", "state", "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;", "onFastScrollEnd", "onFastScrollStart", "onItemSelected", "selection", "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;", "onScrollIdle", "onUnload", "level", "Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;", "selectItem", "sendMessage", "message", "setBackSelectionId", "setBackSelectionPos", "n", "setSelectedIcon", "showToolTip", "Companion", "app_hudDebug"}, k = 1, mv = {1, 1, 6})
/* compiled from: MessagePickerMenu.kt */
public final class MessagePickerMenu implements com.navdy.hud.app.ui.component.mainmenu.IMenu {
    public static final com.navdy.hud.app.ui.component.mainmenu.MessagePickerMenu.Companion Companion = new com.navdy.hud.app.ui.component.mainmenu.MessagePickerMenu.Companion(null);
    /* access modifiers changed from: private */
    public static final int SELECTION_ANIMATION_DELAY = 1000;
    /* access modifiers changed from: private */
    public static final com.navdy.hud.app.ui.component.vlist.VerticalList.Model back = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.menu_back, com.navdy.hud.app.R.drawable.icon_mm_back, Companion.getFluctuatorColor(), com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, Companion.getFluctuatorColor(), Companion.getResources().getString(com.navdy.hud.app.R.string.back), null);
    /* access modifiers changed from: private */
    public static final int baseMessageId = 100;
    /* access modifiers changed from: private */
    public static final android.content.Context context = com.navdy.hud.app.HudApplication.getAppContext();
    /* access modifiers changed from: private */
    public static final int fluctuatorColor = android.support.v4.content.ContextCompat.getColor(Companion.getContext(), com.navdy.hud.app.R.color.mm_back);
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger("MessagePickerMenu");
    /* access modifiers changed from: private */
    public static final int messageColor = android.support.v4.content.ContextCompat.getColor(Companion.getContext(), com.navdy.hud.app.R.color.share_location);
    /* access modifiers changed from: private */
    public static final android.content.res.Resources resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
    private int backSelection;
    private int backSelectionId;
    private java.util.List<com.navdy.hud.app.ui.component.vlist.VerticalList.Model> cachedList;
    private final com.navdy.hud.app.framework.contacts.Contact contact;
    /* access modifiers changed from: private */
    public final java.util.List<java.lang.String> messages;
    private final java.lang.String notificationId;
    private final com.navdy.hud.app.ui.component.mainmenu.IMenu parent;
    /* access modifiers changed from: private */
    public final com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.Presenter presenter;
    private final com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent vscrollComponent;

    @kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\u00020\u0004X\u0082D\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u001c\u0010\u0007\u001a\n \t*\u0004\u0018\u00010\b0\bX\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0014\u0010\f\u001a\u00020\u0004X\u0082D\u00a2\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u0006R\u001c\u0010\u000e\u001a\n \t*\u0004\u0018\u00010\u000f0\u000fX\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u0014\u0010\u0012\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0006R\u0014\u0010\u0014\u001a\u00020\u0015X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0017R\u0014\u0010\u0018\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u0006R\u001c\u0010\u001a\u001a\n \t*\u0004\u0018\u00010\u001b0\u001bX\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001c\u0010\u001d\u00a8\u0006\u001e"}, d2 = {"Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;", "", "()V", "SELECTION_ANIMATION_DELAY", "", "getSELECTION_ANIMATION_DELAY", "()I", "back", "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;", "kotlin.jvm.PlatformType", "getBack", "()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;", "baseMessageId", "getBaseMessageId", "context", "Landroid/content/Context;", "getContext", "()Landroid/content/Context;", "fluctuatorColor", "getFluctuatorColor", "logger", "Lcom/navdy/service/library/log/Logger;", "getLogger", "()Lcom/navdy/service/library/log/Logger;", "messageColor", "getMessageColor", "resources", "Landroid/content/res/Resources;", "getResources", "()Landroid/content/res/Resources;", "app_hudDebug"}, k = 1, mv = {1, 1, 6})
    /* compiled from: MessagePickerMenu.kt */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(kotlin.jvm.internal.DefaultConstructorMarker $constructor_marker) {
            this();
        }

        /* access modifiers changed from: private */
        public final com.navdy.service.library.log.Logger getLogger() {
            return com.navdy.hud.app.ui.component.mainmenu.MessagePickerMenu.logger;
        }

        /* access modifiers changed from: private */
        public final int getSELECTION_ANIMATION_DELAY() {
            return com.navdy.hud.app.ui.component.mainmenu.MessagePickerMenu.SELECTION_ANIMATION_DELAY;
        }

        /* access modifiers changed from: private */
        public final android.content.Context getContext() {
            return com.navdy.hud.app.ui.component.mainmenu.MessagePickerMenu.context;
        }

        /* access modifiers changed from: private */
        public final android.content.res.Resources getResources() {
            return com.navdy.hud.app.ui.component.mainmenu.MessagePickerMenu.resources;
        }

        /* access modifiers changed from: private */
        public final int getFluctuatorColor() {
            return com.navdy.hud.app.ui.component.mainmenu.MessagePickerMenu.fluctuatorColor;
        }

        /* access modifiers changed from: private */
        public final int getMessageColor() {
            return com.navdy.hud.app.ui.component.mainmenu.MessagePickerMenu.messageColor;
        }

        /* access modifiers changed from: private */
        public final com.navdy.hud.app.ui.component.vlist.VerticalList.Model getBack() {
            return com.navdy.hud.app.ui.component.mainmenu.MessagePickerMenu.back;
        }

        /* access modifiers changed from: private */
        public final int getBaseMessageId() {
            return com.navdy.hud.app.ui.component.mainmenu.MessagePickerMenu.baseMessageId;
        }
    }

    public MessagePickerMenu(@org.jetbrains.annotations.NotNull com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent vscrollComponent2, @org.jetbrains.annotations.NotNull com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.Presenter presenter2, @org.jetbrains.annotations.NotNull com.navdy.hud.app.ui.component.mainmenu.IMenu parent2, @org.jetbrains.annotations.NotNull java.util.List<java.lang.String> messages2, @org.jetbrains.annotations.NotNull com.navdy.hud.app.framework.contacts.Contact contact2, @org.jetbrains.annotations.Nullable java.lang.String notificationId2) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(vscrollComponent2, "vscrollComponent");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(presenter2, "presenter");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(parent2, "parent");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(messages2, "messages");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(contact2, "contact");
        this.vscrollComponent = vscrollComponent2;
        this.presenter = presenter2;
        this.parent = parent2;
        this.messages = messages2;
        this.contact = contact2;
        this.notificationId = notificationId2;
    }

    @org.jetbrains.annotations.NotNull
    public java.util.List<com.navdy.hud.app.ui.component.vlist.VerticalList.Model> getItems() {
        if (this.cachedList != null) {
            java.util.List<com.navdy.hud.app.ui.component.vlist.VerticalList.Model> list = this.cachedList;
            if (list != null) {
                return kotlin.jvm.internal.TypeIntrinsics.asMutableList(list);
            }
            throw new kotlin.TypeCastException("null cannot be cast to non-null type kotlin.collections.MutableList<com.navdy.hud.app.ui.component.vlist.VerticalList.Model>");
        }
        java.util.ArrayList list2 = new java.util.ArrayList();
        list2.add(Companion.getBack());
        int counter = 0;
        for (java.lang.String msg : this.messages) {
            int counter2 = counter + 1;
            com.navdy.hud.app.ui.component.vlist.VerticalList.Model model = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(Companion.getBaseMessageId() + counter, com.navdy.hud.app.R.drawable.icon_message, Companion.getMessageColor(), com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, Companion.getMessageColor(), msg, null);
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(model, "IconBkColorViewHolder.bu\u2026 messageColor, msg, null)");
            list2.add(model);
            counter = counter2;
        }
        this.cachedList = list2;
        return list2;
    }

    public int getInitialSelection() {
        return 1;
    }

    @org.jetbrains.annotations.Nullable
    public com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex getScrollIndex() {
        return null;
    }

    public void setBackSelectionPos(int n) {
        this.backSelection = n;
    }

    public void setBackSelectionId(int id) {
        this.backSelectionId = id;
    }

    public void setSelectedIcon() {
        this.vscrollComponent.setSelectedIconColorImage(com.navdy.hud.app.R.drawable.icon_message, Companion.getMessageColor(), null, 1.0f);
        this.vscrollComponent.selectedText.setText(Companion.getResources().getString(com.navdy.hud.app.R.string.message));
    }

    public boolean selectItem(@org.jetbrains.annotations.NotNull com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState selection) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(selection, "selection");
        Companion.getLogger().v("select id:" + selection.id + " pos:" + selection.pos);
        switch (selection.id) {
            case com.navdy.hud.app.R.id.menu_back /*2131624007*/:
                com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("back");
                this.presenter.loadMenu(this.parent, com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel.BACK_TO_PARENT, this.backSelection, this.backSelectionId);
                this.backSelectionId = 0;
                break;
            default:
                this.presenter.performSelectionAnimation(new com.navdy.hud.app.ui.component.mainmenu.MessagePickerMenu$selectItem$Anon1(this, selection), Companion.getSELECTION_ANIMATION_DELAY());
                break;
        }
        return true;
    }

    @org.jetbrains.annotations.NotNull
    public com.navdy.hud.app.ui.component.mainmenu.IMenu.Menu getType() {
        return com.navdy.hud.app.ui.component.mainmenu.IMenu.Menu.MESSAGE_PICKER;
    }

    public boolean isItemClickable(int id, int pos) {
        return true;
    }

    @org.jetbrains.annotations.Nullable
    public com.navdy.hud.app.ui.component.vlist.VerticalList.Model getModelfromPos(int pos) {
        java.util.List list = this.cachedList;
        if (list == null || list.size() <= pos) {
            return null;
        }
        return (com.navdy.hud.app.ui.component.vlist.VerticalList.Model) list.get(pos);
    }

    public boolean isBindCallsEnabled() {
        return false;
    }

    public void onBindToView(@org.jetbrains.annotations.Nullable com.navdy.hud.app.ui.component.vlist.VerticalList.Model model, @org.jetbrains.annotations.Nullable android.view.View view, int pos, @org.jetbrains.annotations.Nullable com.navdy.hud.app.ui.component.vlist.VerticalList.ModelState state) {
    }

    @org.jetbrains.annotations.Nullable
    public com.navdy.hud.app.ui.component.mainmenu.IMenu getChildMenu(@org.jetbrains.annotations.Nullable com.navdy.hud.app.ui.component.mainmenu.IMenu parent2, @org.jetbrains.annotations.Nullable java.lang.String args, @org.jetbrains.annotations.Nullable java.lang.String path) {
        return null;
    }

    public void onUnload(@org.jetbrains.annotations.Nullable com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel level) {
    }

    public void onItemSelected(@org.jetbrains.annotations.Nullable com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState selection) {
    }

    public void onScrollIdle() {
    }

    public void onFastScrollStart() {
    }

    public void onFastScrollEnd() {
    }

    public void showToolTip() {
    }

    /* access modifiers changed from: private */
    public final void sendMessage(java.lang.String message) {
        Companion.getLogger().v("sendMessage:" + this.contact.number);
        com.navdy.hud.app.framework.glance.GlanceHandler glanceHandler = com.navdy.hud.app.framework.glance.GlanceHandler.getInstance();
        if (this.notificationId != null) {
            com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().removeNotification(this.notificationId);
        }
        if (glanceHandler.sendMessage(this.contact.number, message, this.contact.name)) {
            glanceHandler.sendSmsSuccessNotification(java.util.UUID.randomUUID().toString(), this.contact.number, message, this.contact.name);
        } else {
            glanceHandler.sendSmsFailedNotification(this.contact.number, message, this.contact.name);
        }
    }

    public boolean isFirstItemEmpty() {
        return true;
    }
}
