package com.navdy.hud.app.ui.component.homescreen;

public class MpgView extends android.widget.LinearLayout {
    private com.navdy.service.library.log.Logger logger;
    @butterknife.InjectView(2131624347)
    android.widget.TextView mpgLabelTextView;
    @butterknife.InjectView(2131624348)
    android.widget.TextView mpgTextView;
    private com.navdy.hud.app.ui.framework.UIStateManager uiStateManager;

    public MpgView(android.content.Context context) {
        this(context, null);
    }

    public MpgView(android.content.Context context, android.util.AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MpgView(android.content.Context context, android.util.AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        this.logger = com.navdy.hud.app.ui.component.homescreen.HomeScreenView.sLogger;
        super.onFinishInflate();
        butterknife.ButterKnife.inject((android.view.View) this);
        this.uiStateManager = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getUiStateManager();
    }

    /* access modifiers changed from: 0000 */
    public void updateInstantaneousFuelConsumption(double consumption) {
        double mpg = com.navdy.hud.app.util.ConversionUtil.convertLpHundredKmToMPG(consumption);
        java.lang.String mpgText = "- -";
        if (mpg > 0.0d) {
            mpgText = java.lang.String.format("%d", new java.lang.Object[]{java.lang.Integer.valueOf((int) mpg)});
        }
        this.mpgTextView.setText(mpgText);
    }

    public void setView(com.navdy.hud.app.view.MainView.CustomAnimationMode mode) {
        switch (mode) {
            case EXPAND:
                setX((float) com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.speedX);
                return;
            case SHRINK_LEFT:
                setX((float) com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.speedShrinkLeftX);
                return;
            default:
                return;
        }
    }

    public void getTopAnimator(android.animation.AnimatorSet.Builder builder, boolean out) {
        if (out) {
            builder.with(com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getXPositionAnimator(this, (float) (((int) getX()) + com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.topViewSpeedOut)));
            builder.with(android.animation.ObjectAnimator.ofFloat(this, android.view.View.ALPHA, new float[]{1.0f, 0.0f}));
            return;
        }
        builder.with(com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getXPositionAnimator(this, (float) (((int) getX()) - com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.topViewSpeedOut)));
        builder.with(android.animation.ObjectAnimator.ofFloat(this, android.view.View.ALPHA, new float[]{0.0f, 1.0f}));
    }

    public void resetTopViewsAnimator() {
        setAlpha(1.0f);
        setView(this.uiStateManager.getCustomAnimateMode());
    }
}
