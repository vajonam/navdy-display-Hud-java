package com.navdy.hud.app.ui.component.mainmenu;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\b\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\b\u0002"}, d2 = {"<anonymous>", "", "run"}, k = 3, mv = {1, 1, 6})
/* compiled from: MessagePickerMenu.kt */
final class MessagePickerMenu$selectItem$Anon1 implements java.lang.Runnable {
    final /* synthetic */ com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState $selection;
    final /* synthetic */ com.navdy.hud.app.ui.component.mainmenu.MessagePickerMenu this$Anon0;

    @kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\b\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\b\u0002"}, d2 = {"<anonymous>", "", "run"}, k = 3, mv = {1, 1, 6})
    /* compiled from: MessagePickerMenu.kt */
    static final class Anon1 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.hud.app.ui.component.mainmenu.MessagePickerMenu$selectItem$Anon1 this$Anon0;

        Anon1(com.navdy.hud.app.ui.component.mainmenu.MessagePickerMenu$selectItem$Anon1 messagePickerMenu$selectItem$Anon1) {
            this.this$Anon0 = messagePickerMenu$selectItem$Anon1;
        }

        public final void run() {
            this.this$Anon0.this$Anon0.sendMessage((java.lang.String) this.this$Anon0.this$Anon0.messages.get(this.this$Anon0.$selection.id - com.navdy.hud.app.ui.component.mainmenu.MessagePickerMenu.Companion.getBaseMessageId()));
        }
    }

    MessagePickerMenu$selectItem$Anon1(com.navdy.hud.app.ui.component.mainmenu.MessagePickerMenu messagePickerMenu, com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState itemSelectionState) {
        this.this$Anon0 = messagePickerMenu;
        this.$selection = itemSelectionState;
    }

    public final void run() {
        this.this$Anon0.presenter.close(new com.navdy.hud.app.ui.component.mainmenu.MessagePickerMenu$selectItem$Anon1.Anon1(this));
    }
}
