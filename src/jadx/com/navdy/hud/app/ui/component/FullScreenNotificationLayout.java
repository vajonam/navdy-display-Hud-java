package com.navdy.hud.app.ui.component;

public class FullScreenNotificationLayout extends android.widget.RelativeLayout {
    private static final long ANIM_DURATION = 800;
    private boolean autoPopup;
    private java.util.List<android.view.View> bodyViews = new java.util.ArrayList();
    private float directionalTranslation;
    private android.view.View footer;
    private int footerId;
    private android.view.View title;
    private int titleId;

    private enum AnimationDirection {
        UP,
        DOWN
    }

    class Anon1 implements java.lang.Runnable {
        final /* synthetic */ java.lang.Runnable val$endAction;

        Anon1(java.lang.Runnable runnable) {
            this.val$endAction = runnable;
        }

        public void run() {
            com.navdy.hud.app.ui.component.FullScreenNotificationLayout.this.animateInSecondStage(this.val$endAction);
        }
    }

    class Anon2 implements java.lang.Runnable {
        final /* synthetic */ java.lang.Runnable val$endAction;

        Anon2(java.lang.Runnable runnable) {
            this.val$endAction = runnable;
        }

        public void run() {
            com.navdy.hud.app.ui.component.FullScreenNotificationLayout.this.animateOutSecondStage(this.val$endAction);
        }
    }

    class Anon3 implements java.lang.Runnable {
        final /* synthetic */ java.lang.Runnable val$endAction;

        Anon3(java.lang.Runnable runnable) {
            this.val$endAction = runnable;
        }

        public void run() {
            com.navdy.hud.app.ui.component.FullScreenNotificationLayout.this.animateOutSecondStage(this.val$endAction);
        }
    }

    public FullScreenNotificationLayout(android.content.Context context, android.util.AttributeSet attrs) {
        super(context, attrs);
        float translationY = getResources().getDimension(com.navdy.hud.app.R.dimen.text_fade_anim_translationy);
        android.content.res.TypedArray ta = context.obtainStyledAttributes(attrs, com.navdy.hud.app.R.styleable.FullScreenNotificationLayout);
        if (ta != null) {
            try {
                this.titleId = ta.getResourceId(0, 0);
                this.footerId = ta.getResourceId(1, 0);
                if (com.navdy.hud.app.ui.component.FullScreenNotificationLayout.AnimationDirection.values()[ta.getInt(2, 0)] != com.navdy.hud.app.ui.component.FullScreenNotificationLayout.AnimationDirection.UP) {
                    translationY = -translationY;
                }
                this.directionalTranslation = translationY;
                this.autoPopup = ta.getBoolean(3, true);
            } finally {
                ta.recycle();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
        initialize();
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.autoPopup) {
            animateIn();
        }
    }

    private void initialize() {
        this.title = findViewById(this.titleId);
        this.footer = findViewById(this.footerId);
        this.title.setAlpha(0.0f);
        this.footer.setAlpha(0.0f);
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            android.view.View view = getChildAt(i);
            if (!(view.getId() == this.titleId || view.getId() == this.footerId)) {
                view.setAlpha(0.0f);
                view.setTranslationY(this.directionalTranslation);
                this.bodyViews.add(view);
            }
        }
    }

    public void animateIn() {
        animateIn(null);
    }

    public void animateIn(java.lang.Runnable endAction) {
        boolean secondStageTriggered = false;
        for (android.view.View v : this.bodyViews) {
            if (!secondStageTriggered) {
                secondStageTriggered = true;
                if (this.title != null || this.footer != null) {
                    getInBodyAnimation(v).withEndAction(new com.navdy.hud.app.ui.component.FullScreenNotificationLayout.Anon1(endAction));
                } else if (endAction != null) {
                    getInBodyAnimation(v).withEndAction(endAction);
                } else {
                    getInBodyAnimation(v);
                }
            } else {
                getInBodyAnimation(v);
            }
        }
        if (!secondStageTriggered) {
            animateInSecondStage(endAction);
        }
    }

    /* access modifiers changed from: private */
    public void animateInSecondStage(java.lang.Runnable endAction) {
        if (this.title != null) {
            if (endAction != null) {
                getInTitleFooterAnimation(this.title).withEndAction(endAction);
            } else {
                getInTitleFooterAnimation(this.title);
            }
        }
        if (this.footer == null) {
            return;
        }
        if (this.title != null) {
            getInTitleFooterAnimation(this.footer);
        } else if (endAction != null) {
            getInTitleFooterAnimation(this.footer).withEndAction(endAction);
        } else {
            getInTitleFooterAnimation(this.footer);
        }
    }

    public void animateOut(java.lang.Runnable endAction) {
        if (this.title == null && this.footer == null) {
            animateOutSecondStage(endAction);
            return;
        }
        if (this.title != null) {
            getOutTitleFooterAnimation(this.title).withEndAction(new com.navdy.hud.app.ui.component.FullScreenNotificationLayout.Anon2(endAction));
        }
        if (this.footer == null) {
            return;
        }
        if (this.title == null) {
            getOutTitleFooterAnimation(this.footer).withEndAction(new com.navdy.hud.app.ui.component.FullScreenNotificationLayout.Anon3(endAction));
        } else {
            getOutTitleFooterAnimation(this.footer);
        }
    }

    /* access modifiers changed from: private */
    public void animateOutSecondStage(java.lang.Runnable endAction) {
        boolean secondStageTriggered = false;
        for (android.view.View bodyView : this.bodyViews) {
            if (!secondStageTriggered) {
                secondStageTriggered = true;
                if (endAction != null) {
                    getOutBodyAnimation(bodyView).withEndAction(endAction);
                } else {
                    getOutBodyAnimation(bodyView);
                }
            } else {
                getOutBodyAnimation(bodyView);
            }
        }
    }

    private android.view.ViewPropertyAnimator getInBodyAnimation(android.view.View v) {
        return v.animate().alpha(1.0f).translationY(0.0f).setDuration(ANIM_DURATION);
    }

    private android.view.ViewPropertyAnimator getOutBodyAnimation(android.view.View v) {
        return v.animate().alpha(0.0f).translationY(this.directionalTranslation).setDuration(ANIM_DURATION);
    }

    private android.view.ViewPropertyAnimator getInTitleFooterAnimation(android.view.View v) {
        return v.animate().alpha(1.0f).setDuration(ANIM_DURATION);
    }

    private android.view.ViewPropertyAnimator getOutTitleFooterAnimation(android.view.View v) {
        return v.animate().alpha(0.0f).setDuration(ANIM_DURATION);
    }
}
