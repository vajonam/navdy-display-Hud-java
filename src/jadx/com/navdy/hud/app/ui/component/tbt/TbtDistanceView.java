package com.navdy.hud.app.ui.component.tbt;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000r\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u0000 ,2\u00020\u0001:\u0001,B\u000f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004B\u0017\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007B\u001f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\b\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\b\u0010\u001a\u001a\u00020\u001bH\u0002J\u0006\u0010\u001c\u001a\u00020\u001bJ\u001a\u0010\u001d\u001a\u00020\u001b2\u0006\u0010\u001e\u001a\u00020\f2\n\u0010\u001f\u001a\u00060 R\u00020!J\b\u0010\"\u001a\u00020\u001bH\u0014J$\u0010#\u001a\u00020\u001b2\b\u0010$\u001a\u0004\u0018\u00010\u00132\b\u0010%\u001a\u0004\u0018\u00010\u00112\u0006\u0010&\u001a\u00020'H\u0002J\u000e\u0010(\u001a\u00020\u001b2\u0006\u0010\u001e\u001a\u00020\fJ\u000e\u0010)\u001a\u00020\u001b2\u0006\u0010*\u001a\u00020+R\u0010\u0010\u000b\u001a\u0004\u0018\u00010\fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\r\u001a\u0004\u0018\u00010\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0010\u001a\u0004\u0018\u00010\u0011X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0012\u001a\u0004\u0018\u00010\u0013X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0014\u001a\u0004\u0018\u00010\u0015X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0016\u001a\u0004\u0018\u00010\u0017X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0018\u001a\u0004\u0018\u00010\u0019X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006-"}, d2 = {"Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;", "Landroid/support/constraint/ConstraintLayout;", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "attrs", "Landroid/util/AttributeSet;", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "defStyleAttr", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "currentMode", "Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;", "distanceView", "Landroid/widget/TextView;", "initialProgressBarDistance", "lastDistance", "", "lastManeuverState", "Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;", "nowView", "Landroid/widget/ImageView;", "progressAnimator", "Landroid/animation/ValueAnimator;", "progressView", "Landroid/widget/ProgressBar;", "cancelProgressAnimator", "", "clear", "getCustomAnimator", "mode", "mainBuilder", "Landroid/animation/AnimatorSet$Builder;", "Landroid/animation/AnimatorSet;", "onFinishInflate", "setDistance", "maneuverState", "text", "distanceInMeters", "", "setMode", "updateDisplay", "event", "Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;", "Companion", "app_hudDebug"}, k = 1, mv = {1, 1, 6})
/* compiled from: TbtDistanceView.kt */
public final class TbtDistanceView extends android.support.constraint.ConstraintLayout {
    public static final com.navdy.hud.app.ui.component.tbt.TbtDistanceView.Companion Companion = new com.navdy.hud.app.ui.component.tbt.TbtDistanceView.Companion(null);
    /* access modifiers changed from: private */
    public static final int fullWidth = Companion.getResources().getDimensionPixelSize(com.navdy.hud.app.R.dimen.tbt_distance_full_w);
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger("TbtDistanceView");
    /* access modifiers changed from: private */
    public static final int mediumWidth = Companion.getResources().getDimensionPixelSize(com.navdy.hud.app.R.dimen.tbt_distance_medium_w);
    /* access modifiers changed from: private */
    public static final int nowHeightFull = Companion.getResources().getDimensionPixelSize(com.navdy.hud.app.R.dimen.tbt_distance_now_h_full);
    /* access modifiers changed from: private */
    public static final int nowHeightMedium = Companion.getResources().getDimensionPixelSize(com.navdy.hud.app.R.dimen.tbt_distance_now_h_medium);
    /* access modifiers changed from: private */
    public static final int nowWidthFull = Companion.getResources().getDimensionPixelSize(com.navdy.hud.app.R.dimen.tbt_distance_now_w_full);
    /* access modifiers changed from: private */
    public static final int nowWidthMedium = Companion.getResources().getDimensionPixelSize(com.navdy.hud.app.R.dimen.tbt_distance_now_w_medium);
    /* access modifiers changed from: private */
    public static final int progressHeightFull = Companion.getResources().getDimensionPixelSize(com.navdy.hud.app.R.dimen.tbt_distance_progress_h_full);
    /* access modifiers changed from: private */
    public static final int progressHeightMedium = Companion.getResources().getDimensionPixelSize(com.navdy.hud.app.R.dimen.tbt_distance_progress_h_medium);
    /* access modifiers changed from: private */
    public static final android.content.res.Resources resources;
    /* access modifiers changed from: private */
    public static final float size18 = Companion.getResources().getDimension(com.navdy.hud.app.R.dimen.tbt_instruction_18);
    /* access modifiers changed from: private */
    public static final float size22 = Companion.getResources().getDimension(com.navdy.hud.app.R.dimen.tbt_instruction_22);
    private java.util.HashMap _$_findViewCache;
    private com.navdy.hud.app.view.MainView.CustomAnimationMode currentMode;
    private android.widget.TextView distanceView;
    private int initialProgressBarDistance;
    private java.lang.String lastDistance;
    private com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.ManeuverState lastManeuverState;
    private android.widget.ImageView nowView;
    private android.animation.ValueAnimator progressAnimator;
    private android.widget.ProgressBar progressView;

    @kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0011\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0007\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u0014\u0010\u0007\u001a\u00020\bX\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0014\u0010\u000b\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\f\u0010\u0006R\u0014\u0010\r\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u0006R\u0014\u0010\u000f\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0006R\u0014\u0010\u0011\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0006R\u0014\u0010\u0013\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0006R\u0014\u0010\u0015\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0006R\u0014\u0010\u0017\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0006R\u0014\u0010\u0019\u001a\u00020\u001aX\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001b\u0010\u001cR\u0014\u0010\u001d\u001a\u00020\u001eX\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001f\u0010 R\u0014\u0010!\u001a\u00020\u001eX\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\"\u0010 \u00a8\u0006#"}, d2 = {"Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;", "", "()V", "fullWidth", "", "getFullWidth", "()I", "logger", "Lcom/navdy/service/library/log/Logger;", "getLogger", "()Lcom/navdy/service/library/log/Logger;", "mediumWidth", "getMediumWidth", "nowHeightFull", "getNowHeightFull", "nowHeightMedium", "getNowHeightMedium", "nowWidthFull", "getNowWidthFull", "nowWidthMedium", "getNowWidthMedium", "progressHeightFull", "getProgressHeightFull", "progressHeightMedium", "getProgressHeightMedium", "resources", "Landroid/content/res/Resources;", "getResources", "()Landroid/content/res/Resources;", "size18", "", "getSize18", "()F", "size22", "getSize22", "app_hudDebug"}, k = 1, mv = {1, 1, 6})
    /* compiled from: TbtDistanceView.kt */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(kotlin.jvm.internal.DefaultConstructorMarker $constructor_marker) {
            this();
        }

        /* access modifiers changed from: private */
        public final com.navdy.service.library.log.Logger getLogger() {
            return com.navdy.hud.app.ui.component.tbt.TbtDistanceView.logger;
        }

        /* access modifiers changed from: private */
        public final android.content.res.Resources getResources() {
            return com.navdy.hud.app.ui.component.tbt.TbtDistanceView.resources;
        }

        /* access modifiers changed from: private */
        public final int getFullWidth() {
            return com.navdy.hud.app.ui.component.tbt.TbtDistanceView.fullWidth;
        }

        /* access modifiers changed from: private */
        public final int getMediumWidth() {
            return com.navdy.hud.app.ui.component.tbt.TbtDistanceView.mediumWidth;
        }

        /* access modifiers changed from: private */
        public final float getSize22() {
            return com.navdy.hud.app.ui.component.tbt.TbtDistanceView.size22;
        }

        /* access modifiers changed from: private */
        public final float getSize18() {
            return com.navdy.hud.app.ui.component.tbt.TbtDistanceView.size18;
        }

        /* access modifiers changed from: private */
        public final int getProgressHeightFull() {
            return com.navdy.hud.app.ui.component.tbt.TbtDistanceView.progressHeightFull;
        }

        /* access modifiers changed from: private */
        public final int getProgressHeightMedium() {
            return com.navdy.hud.app.ui.component.tbt.TbtDistanceView.progressHeightMedium;
        }

        /* access modifiers changed from: private */
        public final int getNowWidthFull() {
            return com.navdy.hud.app.ui.component.tbt.TbtDistanceView.nowWidthFull;
        }

        /* access modifiers changed from: private */
        public final int getNowHeightFull() {
            return com.navdy.hud.app.ui.component.tbt.TbtDistanceView.nowHeightFull;
        }

        /* access modifiers changed from: private */
        public final int getNowWidthMedium() {
            return com.navdy.hud.app.ui.component.tbt.TbtDistanceView.nowWidthMedium;
        }

        /* access modifiers changed from: private */
        public final int getNowHeightMedium() {
            return com.navdy.hud.app.ui.component.tbt.TbtDistanceView.nowHeightMedium;
        }
    }

    public void _$_clearFindViewByIdCache() {
        if (this._$_findViewCache != null) {
            this._$_findViewCache.clear();
        }
    }

    public android.view.View _$_findCachedViewById(int i) {
        if (this._$_findViewCache == null) {
            this._$_findViewCache = new java.util.HashMap();
        }
        android.view.View view = (android.view.View) this._$_findViewCache.get(java.lang.Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        android.view.View findViewById = findViewById(i);
        this._$_findViewCache.put(java.lang.Integer.valueOf(i), findViewById);
        return findViewById;
    }

    static {
        android.content.res.Resources resources2 = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(resources2, "HudApplication.getAppContext().resources");
        resources = resources2;
    }

    public TbtDistanceView(@org.jetbrains.annotations.NotNull android.content.Context context) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(context, "context");
        super(context);
    }

    public TbtDistanceView(@org.jetbrains.annotations.NotNull android.content.Context context, @org.jetbrains.annotations.NotNull android.util.AttributeSet attrs) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(context, "context");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(attrs, "attrs");
        super(context, attrs);
    }

    public TbtDistanceView(@org.jetbrains.annotations.NotNull android.content.Context context, @org.jetbrains.annotations.NotNull android.util.AttributeSet attrs, int defStyleAttr) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(context, "context");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(attrs, "attrs");
        super(context, attrs, defStyleAttr);
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
        android.view.View findViewById = findViewById(com.navdy.hud.app.R.id.distance);
        if (findViewById == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type android.widget.TextView");
        }
        this.distanceView = (android.widget.TextView) findViewById;
        android.view.View findViewById2 = findViewById(com.navdy.hud.app.R.id.progress);
        if (findViewById2 == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type android.widget.ProgressBar");
        }
        this.progressView = (android.widget.ProgressBar) findViewById2;
        android.view.View findViewById3 = findViewById(com.navdy.hud.app.R.id.now);
        if (findViewById3 == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type android.widget.ImageView");
        }
        this.nowView = (android.widget.ImageView) findViewById3;
    }

    public final void setMode(@org.jetbrains.annotations.NotNull com.navdy.hud.app.view.MainView.CustomAnimationMode mode) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(mode, net.hockeyapp.android.LoginActivity.EXTRA_MODE);
        if (!kotlin.jvm.internal.Intrinsics.areEqual((java.lang.Object) this.currentMode, (java.lang.Object) mode)) {
            int lytWidth = 0;
            float fontSize = 0.0f;
            int progressSize = 0;
            int nowW = 0;
            int nowH = 0;
            switch (mode) {
                case EXPAND:
                    lytWidth = Companion.getFullWidth();
                    fontSize = Companion.getSize22();
                    progressSize = Companion.getProgressHeightFull();
                    nowW = Companion.getNowWidthFull();
                    nowH = Companion.getNowHeightFull();
                    break;
                case SHRINK_LEFT:
                    lytWidth = Companion.getMediumWidth();
                    fontSize = Companion.getSize18();
                    progressSize = Companion.getProgressHeightMedium();
                    nowW = Companion.getNowWidthMedium();
                    nowH = Companion.getNowHeightMedium();
                    break;
            }
            android.view.ViewGroup.LayoutParams layoutParams = getLayoutParams();
            if (layoutParams == null) {
                throw new kotlin.TypeCastException("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
            }
            ((android.view.ViewGroup.MarginLayoutParams) layoutParams).width = lytWidth;
            android.widget.TextView textView = this.distanceView;
            if (textView != null) {
                textView.setTextSize(fontSize);
            }
            android.widget.ProgressBar progressBar = this.progressView;
            android.view.ViewGroup.LayoutParams layoutParams2 = progressBar != null ? progressBar.getLayoutParams() : null;
            if (layoutParams2 == null) {
                throw new kotlin.TypeCastException("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
            }
            ((android.view.ViewGroup.MarginLayoutParams) layoutParams2).height = progressSize;
            android.widget.ImageView imageView = this.nowView;
            android.view.ViewGroup.LayoutParams layoutParams3 = imageView != null ? imageView.getLayoutParams() : null;
            if (layoutParams3 == null) {
                throw new kotlin.TypeCastException("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
            }
            android.view.ViewGroup.MarginLayoutParams lytParams = (android.view.ViewGroup.MarginLayoutParams) layoutParams3;
            lytParams.width = nowW;
            lytParams.height = nowH;
            invalidate();
            requestLayout();
            this.currentMode = mode;
            Companion.getLogger().v("view width = " + lytParams.width);
        }
    }

    public final void getCustomAnimator(@org.jetbrains.annotations.NotNull com.navdy.hud.app.view.MainView.CustomAnimationMode mode, @org.jetbrains.annotations.NotNull android.animation.AnimatorSet.Builder mainBuilder) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(mode, net.hockeyapp.android.LoginActivity.EXTRA_MODE);
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(mainBuilder, "mainBuilder");
    }

    public final void clear() {
        this.lastDistance = null;
        android.widget.TextView textView = this.distanceView;
        if (textView != null) {
            textView.setText("");
        }
        cancelProgressAnimator();
        android.widget.ProgressBar progressBar = this.progressView;
        if (progressBar != null) {
            progressBar.setVisibility(8);
        }
    }

    public final void updateDisplay(@org.jetbrains.annotations.NotNull com.navdy.hud.app.maps.MapEvents.ManeuverDisplay event) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(event, "event");
        setDistance(event.maneuverState, event.distanceToPendingRoadText, event.distanceInMeters);
        this.lastManeuverState = event.maneuverState;
    }

    /* JADX WARNING: Removed duplicated region for block: B:36:0x00a9  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00b8  */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x0113  */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x012a  */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x0139  */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x0162  */
    /* JADX WARNING: Removed duplicated region for block: B:93:0x01cf  */
    private final void setDistance(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.ManeuverState maneuverState, java.lang.String text, long distanceInMeters) {
        boolean showNowIcon;
        boolean hideNowIcon;
        boolean showProgress;
        boolean hideProgress;
        if (android.text.TextUtils.equals(text, this.lastDistance) && kotlin.jvm.internal.Intrinsics.areEqual((java.lang.Object) this.lastManeuverState, (java.lang.Object) maneuverState)) {
            return;
        }
        if (maneuverState == null) {
            cancelProgressAnimator();
            android.widget.ProgressBar progressBar = this.progressView;
            if (progressBar != null) {
                progressBar.setVisibility(8);
            }
            android.widget.TextView textView = this.distanceView;
            if (textView != null) {
                textView.setText("");
            }
            android.widget.ImageView imageView = this.nowView;
            if (imageView != null) {
                imageView.setAlpha(0.0f);
            }
            android.widget.ImageView imageView2 = this.nowView;
            if (imageView2 != null) {
                imageView2.setScaleX(0.9f);
            }
            android.widget.ImageView imageView3 = this.nowView;
            if (imageView3 != null) {
                imageView3.setScaleY(0.9f);
            }
            android.widget.ImageView imageView4 = this.nowView;
            if (imageView4 != null) {
                imageView4.setVisibility(8);
            }
            setVisibility(8);
            return;
        }
        this.lastDistance = text;
        android.widget.TextView textView2 = this.distanceView;
        if (textView2 != null) {
            textView2.setText(text != null ? text : "");
        }
        if (!kotlin.jvm.internal.Intrinsics.areEqual((java.lang.Object) this.lastManeuverState, (java.lang.Object) com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.ManeuverState.NOW)) {
            if (kotlin.jvm.internal.Intrinsics.areEqual((java.lang.Object) maneuverState, (java.lang.Object) com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.ManeuverState.NOW)) {
                showNowIcon = true;
                if (kotlin.jvm.internal.Intrinsics.areEqual((java.lang.Object) this.lastManeuverState, (java.lang.Object) com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.ManeuverState.NOW)) {
                    if (!kotlin.jvm.internal.Intrinsics.areEqual((java.lang.Object) maneuverState, (java.lang.Object) com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.ManeuverState.NOW)) {
                        hideNowIcon = true;
                        if (showNowIcon) {
                            android.widget.ImageView imageView5 = this.nowView;
                            if (imageView5 != null) {
                                imageView5.setAlpha(0.0f);
                            }
                            android.widget.ImageView imageView6 = this.nowView;
                            if (imageView6 != null) {
                                imageView6.setVisibility(0);
                            }
                            android.animation.AnimatorSet fadeIn = com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getFadeInAndScaleUpAnimator(this.nowView);
                            cancelProgressAnimator();
                            android.widget.ProgressBar progressBar2 = this.progressView;
                            if (progressBar2 != null) {
                                progressBar2.setVisibility(8);
                            }
                            android.widget.TextView textView3 = this.distanceView;
                            if (textView3 != null) {
                                textView3.setAlpha(0.0f);
                            }
                            android.animation.AnimatorSet distanceAnimator = new android.animation.AnimatorSet();
                            distanceAnimator.play(fadeIn);
                            distanceAnimator.setDuration(com.navdy.hud.app.ui.component.tbt.TbtViewContainer.Companion.getMANEUVER_PROGRESS_ANIMATION_DURATION()).start();
                        } else if (hideNowIcon) {
                            android.widget.ImageView imageView7 = this.nowView;
                            if (imageView7 != null) {
                                imageView7.setVisibility(8);
                            }
                            android.widget.ImageView imageView8 = this.nowView;
                            if (imageView8 != null) {
                                imageView8.setAlpha(0.0f);
                            }
                            android.animation.ObjectAnimator fadeIn2 = com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getAlphaAnimator(this.distanceView, 1);
                            android.animation.AnimatorSet distanceAnimator2 = new android.animation.AnimatorSet();
                            distanceAnimator2.play(fadeIn2);
                            android.animation.AnimatorSet duration = distanceAnimator2.setDuration(com.navdy.hud.app.ui.component.tbt.TbtViewContainer.Companion.getMANEUVER_PROGRESS_ANIMATION_DURATION());
                            if (duration != null) {
                                duration.start();
                            }
                        } else {
                            android.widget.ImageView imageView9 = this.nowView;
                            if (!kotlin.jvm.internal.Intrinsics.areEqual((java.lang.Object) imageView9 != null ? java.lang.Integer.valueOf(imageView9.getVisibility()) : null, (java.lang.Object) java.lang.Integer.valueOf(0))) {
                                android.widget.TextView textView4 = this.distanceView;
                                if (textView4 != null) {
                                    textView4.setAlpha(1.0f);
                                }
                            }
                        }
                        if (!kotlin.jvm.internal.Intrinsics.areEqual((java.lang.Object) this.lastManeuverState, (java.lang.Object) com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.ManeuverState.SOON)) {
                            if (kotlin.jvm.internal.Intrinsics.areEqual((java.lang.Object) maneuverState, (java.lang.Object) com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.ManeuverState.SOON)) {
                                showProgress = true;
                                if (kotlin.jvm.internal.Intrinsics.areEqual((java.lang.Object) this.lastManeuverState, (java.lang.Object) com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.ManeuverState.SOON)) {
                                    if (!kotlin.jvm.internal.Intrinsics.areEqual((java.lang.Object) maneuverState, (java.lang.Object) com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.ManeuverState.SOON)) {
                                        hideProgress = true;
                                        if (hideProgress) {
                                            android.widget.ProgressBar progressBar3 = this.progressView;
                                            if (progressBar3 != null) {
                                                progressBar3.setVisibility(8);
                                            }
                                            cancelProgressAnimator();
                                            this.initialProgressBarDistance = 0;
                                        } else if (showProgress) {
                                            cancelProgressAnimator();
                                            this.initialProgressBarDistance = (int) distanceInMeters;
                                            android.widget.ProgressBar progressBar4 = this.progressView;
                                            if (progressBar4 != null) {
                                                progressBar4.setProgress(0);
                                            }
                                            android.widget.ProgressBar progressBar5 = this.progressView;
                                            if (progressBar5 != null) {
                                                progressBar5.setVisibility(0);
                                            }
                                            android.widget.ProgressBar progressBar6 = this.progressView;
                                            if (progressBar6 != null) {
                                                progressBar6.requestLayout();
                                            }
                                        } else {
                                            if (kotlin.jvm.internal.Intrinsics.areEqual((java.lang.Object) maneuverState, (java.lang.Object) com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.ManeuverState.SOON)) {
                                                com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getProgressBarAnimator(this.progressView, (int) (((double) 100) * (((double) 1) - (((double) distanceInMeters) / ((double) this.initialProgressBarDistance))))).setDuration(com.navdy.hud.app.ui.component.tbt.TbtViewContainer.Companion.getMANEUVER_PROGRESS_ANIMATION_DURATION()).start();
                                            }
                                        }
                                        setVisibility(0);
                                    }
                                }
                                hideProgress = false;
                                if (hideProgress) {
                                }
                                setVisibility(0);
                            }
                        }
                        showProgress = false;
                        if (kotlin.jvm.internal.Intrinsics.areEqual((java.lang.Object) this.lastManeuverState, (java.lang.Object) com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.ManeuverState.SOON)) {
                        }
                        hideProgress = false;
                        if (hideProgress) {
                        }
                        setVisibility(0);
                    }
                }
                hideNowIcon = false;
                if (showNowIcon) {
                }
                if (!kotlin.jvm.internal.Intrinsics.areEqual((java.lang.Object) this.lastManeuverState, (java.lang.Object) com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.ManeuverState.SOON)) {
                }
                showProgress = false;
                if (kotlin.jvm.internal.Intrinsics.areEqual((java.lang.Object) this.lastManeuverState, (java.lang.Object) com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.ManeuverState.SOON)) {
                }
                hideProgress = false;
                if (hideProgress) {
                }
                setVisibility(0);
            }
        }
        showNowIcon = false;
        if (kotlin.jvm.internal.Intrinsics.areEqual((java.lang.Object) this.lastManeuverState, (java.lang.Object) com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.ManeuverState.NOW)) {
        }
        hideNowIcon = false;
        if (showNowIcon) {
        }
        if (!kotlin.jvm.internal.Intrinsics.areEqual((java.lang.Object) this.lastManeuverState, (java.lang.Object) com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.ManeuverState.SOON)) {
        }
        showProgress = false;
        if (kotlin.jvm.internal.Intrinsics.areEqual((java.lang.Object) this.lastManeuverState, (java.lang.Object) com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.ManeuverState.SOON)) {
        }
        hideProgress = false;
        if (hideProgress) {
        }
        setVisibility(0);
    }

    private final void cancelProgressAnimator() {
        android.animation.ValueAnimator valueAnimator = this.progressAnimator;
        if (valueAnimator != null ? valueAnimator.isRunning() : false) {
            android.animation.ValueAnimator valueAnimator2 = this.progressAnimator;
            if (valueAnimator2 != null) {
                valueAnimator2.removeAllListeners();
            }
            android.animation.ValueAnimator valueAnimator3 = this.progressAnimator;
            if (valueAnimator3 != null) {
                valueAnimator3.cancel();
            }
            this.progressAnimator = null;
        }
    }
}
