package com.navdy.hud.app.ui.component.destination;

public class DestinationParcelable implements android.os.Parcelable {
    public static final android.os.Parcelable.Creator<com.navdy.hud.app.ui.component.destination.DestinationParcelable> CREATOR = new com.navdy.hud.app.ui.component.destination.DestinationParcelable.Anon1();
    private static final java.lang.String EMPTY = "";
    java.util.List<com.navdy.hud.app.framework.contacts.Contact> contacts;
    java.lang.String destinationAddress;
    java.lang.String destinationPlaceId;
    java.lang.String destinationSubTitle;
    java.lang.String destinationSubTitle2;
    boolean destinationSubTitle2Formatted;
    boolean destinationSubTitleFormatted;
    java.lang.String destinationTitle;
    double displayLatitude;
    double displayLongitude;
    public java.lang.String distanceStr;
    int icon;
    int iconDeselectedColor;
    int iconSelectedColor;
    int iconUnselected;
    int id;
    java.lang.String identifier;
    double navLatitude;
    double navLongitude;
    java.util.List<com.navdy.hud.app.framework.contacts.Contact> phoneNumbers;
    com.navdy.service.library.events.places.PlaceType placeType;
    java.lang.String routeId;
    com.navdy.hud.app.ui.component.destination.DestinationParcelable.DestinationType type;

    static class Anon1 implements android.os.Parcelable.Creator<com.navdy.hud.app.ui.component.destination.DestinationParcelable> {
        Anon1() {
        }

        public com.navdy.hud.app.ui.component.destination.DestinationParcelable createFromParcel(android.os.Parcel source) {
            return new com.navdy.hud.app.ui.component.destination.DestinationParcelable(source);
        }

        public com.navdy.hud.app.ui.component.destination.DestinationParcelable[] newArray(int size) {
            return new com.navdy.hud.app.ui.component.destination.DestinationParcelable[size];
        }
    }

    public enum DestinationType {
        NONE,
        DESTINATION
    }

    public DestinationParcelable(int id2, java.lang.String destinationTitle2, java.lang.String destinationSubTitle3, boolean destinationSubTitleFormatted2, java.lang.String destinationSubTitle22, boolean destinationSubTitleFormatted22, java.lang.String destinationAddress2, double navLatitude2, double navLongitude2, double displayLatitude2, double displayLongitude2, int icon2, int iconUnselected2, int iconSelectedColor2, int iconDeselectedColor2, com.navdy.hud.app.ui.component.destination.DestinationParcelable.DestinationType type2, com.navdy.service.library.events.places.PlaceType placeType2) {
        this.id = id2;
        this.destinationTitle = destinationTitle2;
        this.destinationSubTitle = destinationSubTitle3;
        this.destinationSubTitleFormatted = destinationSubTitleFormatted2;
        this.destinationSubTitle2 = destinationSubTitle22;
        this.destinationSubTitle2Formatted = destinationSubTitleFormatted22;
        this.destinationAddress = destinationAddress2;
        this.navLatitude = navLatitude2;
        this.navLongitude = navLongitude2;
        this.displayLatitude = displayLatitude2;
        this.displayLongitude = displayLongitude2;
        this.icon = icon2;
        this.iconUnselected = iconUnselected2;
        this.iconSelectedColor = iconSelectedColor2;
        this.iconDeselectedColor = iconDeselectedColor2;
        this.type = type2;
        this.placeType = placeType2;
    }

    public void setIdentifier(java.lang.String identifier2) {
        this.identifier = identifier2;
    }

    public void setRouteId(java.lang.String routeId2) {
        this.routeId = routeId2;
    }

    public void setPlaceId(java.lang.String placeId) {
        this.destinationPlaceId = placeId;
    }

    public void setContacts(java.util.List<com.navdy.hud.app.framework.contacts.Contact> contacts2) {
        this.contacts = contacts2;
    }

    public void setPhoneNumbers(java.util.List<com.navdy.hud.app.framework.contacts.Contact> phoneNumbers2) {
        this.phoneNumbers = phoneNumbers2;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        byte b;
        byte b2 = 1;
        dest.writeString(this.destinationTitle == null ? "" : this.destinationTitle);
        dest.writeString(this.destinationSubTitle == null ? "" : this.destinationSubTitle);
        if (this.destinationSubTitleFormatted) {
            b = 1;
        } else {
            b = 0;
        }
        dest.writeByte(b);
        dest.writeString(this.destinationSubTitle2 == null ? "" : this.destinationSubTitle2);
        if (!this.destinationSubTitle2Formatted) {
            b2 = 0;
        }
        dest.writeByte(b2);
        dest.writeString(this.destinationAddress == null ? "" : this.destinationAddress);
        dest.writeDouble(this.navLatitude);
        dest.writeDouble(this.navLongitude);
        dest.writeDouble(this.displayLatitude);
        dest.writeDouble(this.displayLongitude);
        dest.writeInt(this.icon);
        dest.writeInt(this.iconUnselected);
        dest.writeInt(this.iconSelectedColor);
        dest.writeInt(this.iconDeselectedColor);
        dest.writeString(this.routeId == null ? "" : this.routeId);
        dest.writeString(this.destinationPlaceId == null ? "" : this.routeId);
        dest.writeInt(this.type.ordinal());
        dest.writeInt(this.id);
        dest.writeInt(this.placeType.ordinal());
        dest.writeList(this.contacts);
        dest.writeList(this.phoneNumbers);
        dest.writeString(this.identifier);
    }

    protected DestinationParcelable(android.os.Parcel in) {
        boolean z = true;
        this.destinationTitle = in.readString();
        this.destinationSubTitle = in.readString();
        this.destinationSubTitleFormatted = in.readByte() == 1;
        this.destinationSubTitle2 = in.readString();
        if (in.readByte() != 1) {
            z = false;
        }
        this.destinationSubTitle2Formatted = z;
        this.destinationAddress = in.readString();
        this.navLatitude = in.readDouble();
        this.navLongitude = in.readDouble();
        this.displayLatitude = in.readDouble();
        this.displayLongitude = in.readDouble();
        this.icon = in.readInt();
        this.iconUnselected = in.readInt();
        this.iconSelectedColor = in.readInt();
        this.iconDeselectedColor = in.readInt();
        this.routeId = in.readString();
        this.destinationPlaceId = in.readString();
        this.type = com.navdy.hud.app.ui.component.destination.DestinationParcelable.DestinationType.values()[in.readInt()];
        this.id = in.readInt();
        this.placeType = com.navdy.service.library.events.places.PlaceType.values()[in.readInt()];
        this.contacts = in.readArrayList(null);
        this.phoneNumbers = in.readArrayList(null);
        this.identifier = in.readString();
    }
}
