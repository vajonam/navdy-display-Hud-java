package com.navdy.hud.app.ui.component.vlist;

public class VerticalAdapter extends android.support.v7.widget.RecyclerView.Adapter<com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder> {
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.vlist.VerticalAdapter.class);
    private java.util.List<com.navdy.hud.app.ui.component.vlist.VerticalList.Model> data;
    private android.os.Handler handler = new android.os.Handler(android.os.Looper.getMainLooper());
    private int highlightIndex = -1;
    private java.util.HashMap<com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder, java.lang.Integer> holderToPosMap = new java.util.HashMap<>();
    private boolean initialState;
    private com.navdy.hud.app.ui.component.vlist.VerticalList.ModelState modelState = new com.navdy.hud.app.ui.component.vlist.VerticalList.ModelState();
    private java.util.HashMap<java.lang.Integer, com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder> posToHolderMap = new java.util.HashMap<>();
    private java.util.HashSet<java.lang.Integer> viewHolderCache = new java.util.HashSet<>();
    private com.navdy.hud.app.ui.component.vlist.VerticalList vlist;

    public VerticalAdapter(java.util.List<com.navdy.hud.app.ui.component.vlist.VerticalList.Model> data2, com.navdy.hud.app.ui.component.vlist.VerticalList vlist2) {
        this.data = data2;
        this.vlist = vlist2;
    }

    public com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder onCreateViewHolder(android.view.ViewGroup parent, int viewType) {
        com.navdy.hud.app.ui.component.vlist.VerticalList.ModelType modelType = com.navdy.hud.app.ui.component.vlist.VerticalList.ModelType.values()[viewType];
        if (sLogger.isLoggable(2)) {
            sLogger.v("onCreateViewHolder: " + modelType);
        }
        switch (modelType) {
            case BLANK:
                return com.navdy.hud.app.ui.component.vlist.viewholder.BlankViewHolder.buildViewHolder(parent, this.vlist, this.handler);
            case TITLE:
                return com.navdy.hud.app.ui.component.vlist.viewholder.TitleViewHolder.buildViewHolder(parent, this.vlist, this.handler);
            case TITLE_SUBTITLE:
                return com.navdy.hud.app.ui.component.vlist.viewholder.TitleSubtitleViewHolder.buildViewHolder(parent, this.vlist, this.handler);
            case ICON_BKCOLOR:
                return com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildViewHolder(parent, this.vlist, this.handler);
            case TWO_ICONS:
                return com.navdy.hud.app.ui.component.vlist.viewholder.IconsTwoViewHolder.buildViewHolder(parent, this.vlist, this.handler);
            case ICON:
                return com.navdy.hud.app.ui.component.vlist.viewholder.IconOneViewHolder.buildViewHolder(parent, this.vlist, this.handler);
            case LOADING:
                return com.navdy.hud.app.ui.component.vlist.viewholder.LoadingViewHolder.buildViewHolder(parent, this.vlist, this.handler);
            case ICON_OPTIONS:
                return com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder.buildViewHolder(parent, this.vlist, this.handler);
            case SCROLL_CONTENT:
                return com.navdy.hud.app.ui.component.vlist.viewholder.ScrollableViewHolder.buildViewHolder(parent, this.vlist, this.handler);
            case LOADING_CONTENT:
                return com.navdy.hud.app.ui.component.vlist.viewholder.ContentLoadingViewHolder.buildViewHolder(parent, this.vlist, this.handler);
            case SWITCH:
                return com.navdy.hud.app.ui.component.vlist.viewholder.SwitchViewHolder.Companion.buildViewHolder(parent, this.vlist, this.handler);
            default:
                throw new java.lang.RuntimeException("implement the model:" + modelType);
        }
    }

    public void onBindViewHolder(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder holder, int position) {
        int userPos;
        if (sLogger.isLoggable(2)) {
            sLogger.v("onBindViewHolder: {" + position + "}");
        }
        boolean highlightItem = false;
        clearPrevPos(holder);
        if (this.highlightIndex != -1) {
            if (this.highlightIndex == position) {
                highlightItem = true;
            }
            this.highlightIndex = -1;
        }
        if (this.viewHolderCache.contains(java.lang.Integer.valueOf(position))) {
            this.holderToPosMap.put(holder, java.lang.Integer.valueOf(position));
            this.posToHolderMap.put(java.lang.Integer.valueOf(position), holder);
        }
        com.navdy.hud.app.ui.component.vlist.VerticalList.Model model = (com.navdy.hud.app.ui.component.vlist.VerticalList.Model) this.data.get(position);
        if (position != holder.getPos() || model.needsRebind) {
            boolean callBind = false;
            switch (holder.getModelType()) {
                case BLANK:
                    return;
                case ICON_BKCOLOR:
                case TWO_ICONS:
                case ICON:
                case SCROLL_CONTENT:
                    callBind = true;
                    break;
            }
            if (!model.fontSizeCheckDone) {
                com.navdy.hud.app.ui.component.vlist.VerticalList.setFontSize(model, this.vlist.allowsTwoLineTitles());
            }
            boolean rebind = model.needsRebind;
            boolean dontStartFluctuator = model.dontStartFluctuator;
            model.needsRebind = false;
            model.dontStartFluctuator = false;
            if (!rebind) {
                holder.clearAnimation();
            }
            holder.setExtras(model.extras);
            holder.setPos(position);
            this.modelState.reset();
            holder.preBind(model, this.modelState);
            if (this.vlist.bindCallbacks && callBind) {
                if (this.vlist.firstEntryBlank) {
                    userPos = position - 1;
                } else {
                    userPos = position;
                }
                this.vlist.callback.onBindToView((com.navdy.hud.app.ui.component.vlist.VerticalList.Model) this.data.get(position), holder.layout, userPos, this.modelState);
            }
            holder.bind(model, this.modelState);
            if (!this.initialState) {
                if (this.vlist.getRawPosition() == position) {
                    this.initialState = true;
                    holder.setState(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.State.SELECTED, com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.AnimationType.INIT, 100);
                    sLogger.v("initial state: position:" + position);
                    return;
                }
                holder.setState(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.State.UNSELECTED, com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.AnimationType.INIT, 100);
            } else if (highlightItem || (rebind && this.vlist.getRawPosition() == position)) {
                holder.setState(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.State.SELECTED, com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.AnimationType.NONE, 0, !dontStartFluctuator);
            } else {
                holder.setState(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.State.UNSELECTED, com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.AnimationType.NONE, 0);
            }
        } else {
            sLogger.i("onBindViewHolder: already bind unselect it");
            holder.setState(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.State.UNSELECTED, com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.AnimationType.NONE, 0);
        }
    }

    public int getItemCount() {
        return this.data.size();
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public int getItemViewType(int position) {
        return ((com.navdy.hud.app.ui.component.vlist.VerticalList.Model) this.data.get(position)).type.ordinal();
    }

    public com.navdy.hud.app.ui.component.vlist.VerticalList.Model getModel(int pos) {
        if (pos < 0 || pos >= this.data.size()) {
            return null;
        }
        return (com.navdy.hud.app.ui.component.vlist.VerticalList.Model) this.data.get(pos);
    }

    public void updateModel(int pos, com.navdy.hud.app.ui.component.vlist.VerticalList.Model model) {
        if (pos >= 0 && pos < this.data.size()) {
            this.data.set(pos, model);
            sLogger.v("updated model:" + pos);
        }
    }

    public void onViewRecycled(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder holder) {
        holder.clearAnimation();
        clearPrevPos(holder);
    }

    public boolean onFailedToRecycleView(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder holder) {
        holder.clearAnimation();
        clearPrevPos(holder);
        return super.onFailedToRecycleView(holder);
    }

    public void setInitialState(boolean b) {
        this.initialState = b;
    }

    public void setViewHolderCacheIndex(int[] index) {
        this.viewHolderCache.clear();
        if (index != null) {
            for (int valueOf : index) {
                this.viewHolderCache.add(java.lang.Integer.valueOf(valueOf));
            }
        }
    }

    private void clearPrevPos(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder holder) {
        java.lang.Integer prevPos = (java.lang.Integer) this.holderToPosMap.remove(holder);
        if (prevPos != null) {
            sLogger.v("clearPrevPos removed:" + prevPos);
            this.posToHolderMap.remove(prevPos);
        }
    }

    public com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder getHolderForPos(int pos) {
        return (com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder) this.posToHolderMap.get(java.lang.Integer.valueOf(pos));
    }

    public void setHighlightIndex(int n) {
        this.highlightIndex = n;
    }
}
