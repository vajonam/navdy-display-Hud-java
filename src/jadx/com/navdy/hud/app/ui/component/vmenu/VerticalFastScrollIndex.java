package com.navdy.hud.app.ui.component.vmenu;

public class VerticalFastScrollIndex {
    private final java.lang.String[] entries;
    public final int length;
    private int[] offsetIndex;
    private final java.util.Map<java.lang.String, java.lang.Integer> offsetMap;
    private final int positionOffset;

    public static class Builder {
        private java.util.LinkedHashMap<java.lang.String, java.lang.Integer> map = new java.util.LinkedHashMap<>();
        private int positionOffset;

        public com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex.Builder setEntry(char ch, int startIndex) {
            this.map.put(java.lang.String.valueOf(ch), java.lang.Integer.valueOf(startIndex));
            return this;
        }

        public com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex.Builder positionOffset(int n) {
            this.positionOffset = n;
            return this;
        }

        public com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex build() {
            int len = this.map.size();
            java.lang.String[] entries = new java.lang.String[len];
            int[] offsetIndex = new int[len];
            int counter = 0;
            for (java.lang.String key : this.map.keySet()) {
                entries[counter] = key;
                offsetIndex[counter] = ((java.lang.Integer) this.map.get(key)).intValue();
                counter++;
            }
            return new com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex(entries, this.map, offsetIndex, this.positionOffset);
        }
    }

    private VerticalFastScrollIndex(java.lang.String[] entries2, java.util.Map<java.lang.String, java.lang.Integer> offsetMap2, int[] offsetIndex2, int positionOffset2) {
        this.entries = entries2;
        this.offsetMap = offsetMap2;
        this.offsetIndex = offsetIndex2;
        this.positionOffset = positionOffset2;
        this.length = entries2.length;
    }

    public java.lang.String getTitle(int index) {
        return this.entries[index];
    }

    public int getIndexForPosition(int position) {
        int index = java.util.Arrays.binarySearch(this.offsetIndex, 0, this.offsetIndex.length, position);
        if (index >= 0) {
            return index;
        }
        int index2 = (-index) - 1;
        if (index2 > 0) {
            return index2 - 1;
        }
        return index2;
    }

    public int getPosition(java.lang.String title) {
        return ((java.lang.Integer) this.offsetMap.get(title)).intValue();
    }

    public int getEntryCount() {
        return this.entries.length;
    }

    public int getOffset() {
        return this.positionOffset;
    }
}
