package com.navdy.hud.app.ui.component.mainmenu;

class NearbyPlacesMenu implements com.navdy.hud.app.ui.component.mainmenu.IMenu {
    private static final com.navdy.hud.app.ui.component.vlist.VerticalList.Model atm;
    private static final com.navdy.hud.app.ui.component.vlist.VerticalList.Model back;
    private static final com.navdy.hud.app.ui.component.vlist.VerticalList.Model coffee;
    private static final com.navdy.hud.app.ui.component.vlist.VerticalList.Model food;
    private static final com.navdy.hud.app.ui.component.vlist.VerticalList.Model gas;
    private static final com.navdy.hud.app.ui.component.vlist.VerticalList.Model groceryStore;
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.mainmenu.NearbyPlacesMenu.class);
    private static final com.navdy.hud.app.ui.component.vlist.VerticalList.Model parking;
    private static final android.content.res.Resources resources;
    private static final java.lang.String search = resources.getString(com.navdy.hud.app.R.string.carousel_menu_search_title);
    private static final int searchColor;
    private int backSelection;
    private int backSelectionId;
    private final com.squareup.otto.Bus bus;
    private java.util.List<com.navdy.hud.app.ui.component.vlist.VerticalList.Model> cachedList;
    /* access modifiers changed from: private */
    public final com.navdy.hud.app.framework.notifications.NotificationManager notificationManager = com.navdy.hud.app.framework.notifications.NotificationManager.getInstance();
    private final com.navdy.hud.app.ui.component.mainmenu.IMenu parent;
    /* access modifiers changed from: private */
    public final com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.Presenter presenter;
    private final com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent vscrollComponent;

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            com.navdy.hud.app.ui.component.mainmenu.NearbyPlacesMenu.this.presenter.close();
            com.navdy.hud.app.maps.MapsNotification.showMapsEngineNotInitializedToast();
        }
    }

    class Anon2 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.service.library.events.places.PlaceType val$placeType;

        class Anon1 implements java.lang.Runnable {
            Anon1() {
            }

            public void run() {
                com.navdy.hud.app.ui.component.mainmenu.NearbyPlacesMenu.logger.v("addNotification for quick search");
                com.navdy.hud.app.ui.component.mainmenu.NearbyPlacesMenu.this.notificationManager.addNotification(new com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification(com.navdy.hud.app.ui.component.mainmenu.NearbyPlacesMenu.Anon2.this.val$placeType));
            }
        }

        Anon2(com.navdy.service.library.events.places.PlaceType placeType) {
            this.val$placeType = placeType;
        }

        public void run() {
            com.navdy.hud.app.ui.component.mainmenu.NearbyPlacesMenu.this.presenter.close(new com.navdy.hud.app.ui.component.mainmenu.NearbyPlacesMenu.Anon2.Anon1());
        }
    }

    static {
        android.content.Context context = com.navdy.hud.app.HudApplication.getAppContext();
        resources = context.getResources();
        searchColor = android.support.v4.content.ContextCompat.getColor(context, com.navdy.hud.app.R.color.mm_search);
        java.lang.String title = resources.getString(com.navdy.hud.app.R.string.back);
        int fluctuatorColor = android.support.v4.content.ContextCompat.getColor(context, com.navdy.hud.app.R.color.mm_back);
        back = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.menu_back, com.navdy.hud.app.R.drawable.icon_mm_back, fluctuatorColor, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, fluctuatorColor, title, null);
        java.lang.String title2 = resources.getString(com.navdy.hud.app.R.string.carousel_search_gas);
        int fluctuatorColor2 = android.support.v4.content.ContextCompat.getColor(context, com.navdy.hud.app.R.color.mm_search_gas);
        gas = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.search_menu_gas, com.navdy.hud.app.R.drawable.icon_place_gas, fluctuatorColor2, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, fluctuatorColor2, title2, null);
        java.lang.String title3 = resources.getString(com.navdy.hud.app.R.string.carousel_search_parking);
        int fluctuatorColor3 = android.support.v4.content.ContextCompat.getColor(context, com.navdy.hud.app.R.color.mm_search_parking);
        parking = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.search_menu_parking, com.navdy.hud.app.R.drawable.icon_place_parking, fluctuatorColor3, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, fluctuatorColor3, title3, null);
        java.lang.String title4 = resources.getString(com.navdy.hud.app.R.string.carousel_search_food);
        int fluctuatorColor4 = android.support.v4.content.ContextCompat.getColor(context, com.navdy.hud.app.R.color.mm_search_food);
        food = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.search_menu_food, com.navdy.hud.app.R.drawable.icon_place_restaurant, fluctuatorColor4, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, fluctuatorColor4, title4, null);
        java.lang.String title5 = resources.getString(com.navdy.hud.app.R.string.carousel_search_grocery_store);
        int fluctuatorColor5 = android.support.v4.content.ContextCompat.getColor(context, com.navdy.hud.app.R.color.mm_search_grocery_store);
        groceryStore = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.search_menu_grocery_store, com.navdy.hud.app.R.drawable.icon_place_store, fluctuatorColor5, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, fluctuatorColor5, title5, null);
        java.lang.String title6 = resources.getString(com.navdy.hud.app.R.string.carousel_search_coffee);
        int fluctuatorColor6 = android.support.v4.content.ContextCompat.getColor(context, com.navdy.hud.app.R.color.mm_search_coffee);
        coffee = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.search_menu_coffee, com.navdy.hud.app.R.drawable.icon_place_coffee, fluctuatorColor6, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, fluctuatorColor6, title6, null);
        java.lang.String title7 = resources.getString(com.navdy.hud.app.R.string.carousel_search_atm);
        int fluctuatorColor7 = android.support.v4.content.ContextCompat.getColor(context, com.navdy.hud.app.R.color.mm_search_atm);
        atm = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.search_menu_atm, com.navdy.hud.app.R.drawable.icon_place_a_t_m, fluctuatorColor7, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, fluctuatorColor7, title7, null);
    }

    NearbyPlacesMenu(com.squareup.otto.Bus bus2, com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent vscrollComponent2, com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.Presenter presenter2, com.navdy.hud.app.ui.component.mainmenu.IMenu parent2) {
        this.bus = bus2;
        this.vscrollComponent = vscrollComponent2;
        this.presenter = presenter2;
        this.parent = parent2;
    }

    public java.util.List<com.navdy.hud.app.ui.component.vlist.VerticalList.Model> getItems() {
        if (this.cachedList != null) {
            return this.cachedList;
        }
        java.util.List<com.navdy.hud.app.ui.component.vlist.VerticalList.Model> list = new java.util.ArrayList<>();
        list.add(back);
        list.add(gas);
        list.add(parking);
        list.add(food);
        list.add(groceryStore);
        list.add(coffee);
        list.add(atm);
        this.cachedList = list;
        return list;
    }

    public int getInitialSelection() {
        return 1;
    }

    public com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex getScrollIndex() {
        return null;
    }

    public void setBackSelectionPos(int n) {
        this.backSelection = n;
    }

    public void setBackSelectionId(int id) {
        this.backSelectionId = id;
    }

    public void setSelectedIcon() {
        this.vscrollComponent.setSelectedIconColorImage(com.navdy.hud.app.R.drawable.icon_mm_search_2, searchColor, null, 1.0f);
        this.vscrollComponent.selectedText.setText(search);
    }

    public boolean selectItem(com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState selection) {
        logger.v("select id:" + selection.id + " pos:" + selection.pos);
        switch (selection.id) {
            case com.navdy.hud.app.R.id.menu_back /*2131624007*/:
                logger.v("back");
                com.navdy.hud.app.analytics.AnalyticsSupport.recordNearbySearchReturnMainMenu();
                this.presenter.loadMenu(this.parent, com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel.BACK_TO_PARENT, this.backSelection, this.backSelectionId);
                break;
            case com.navdy.hud.app.R.id.search_menu_atm /*2131624050*/:
                logger.v("atm");
                launchSearch(com.navdy.service.library.events.places.PlaceType.PLACE_TYPE_ATM);
                break;
            case com.navdy.hud.app.R.id.search_menu_coffee /*2131624051*/:
                logger.v("coffee");
                launchSearch(com.navdy.service.library.events.places.PlaceType.PLACE_TYPE_COFFEE);
                break;
            case com.navdy.hud.app.R.id.search_menu_food /*2131624052*/:
                logger.v("food");
                launchSearch(com.navdy.service.library.events.places.PlaceType.PLACE_TYPE_RESTAURANT);
                break;
            case com.navdy.hud.app.R.id.search_menu_gas /*2131624053*/:
                logger.v("gas");
                launchSearch(com.navdy.service.library.events.places.PlaceType.PLACE_TYPE_GAS);
                break;
            case com.navdy.hud.app.R.id.search_menu_grocery_store /*2131624054*/:
                logger.v("grocery store");
                launchSearch(com.navdy.service.library.events.places.PlaceType.PLACE_TYPE_STORE);
                break;
            case com.navdy.hud.app.R.id.search_menu_hospital /*2131624055*/:
                logger.v("hospital");
                launchSearch(com.navdy.service.library.events.places.PlaceType.PLACE_TYPE_HOSPITAL);
                break;
            case com.navdy.hud.app.R.id.search_menu_parking /*2131624056*/:
                logger.v("parking");
                launchSearch(com.navdy.service.library.events.places.PlaceType.PLACE_TYPE_PARKING);
                break;
        }
        return false;
    }

    private void launchSearch(com.navdy.service.library.events.places.PlaceType placeType) {
        if (!com.navdy.hud.app.maps.here.HereMapsManager.getInstance().isInitialized()) {
            logger.w("Here maps engine not initialized, exit");
            this.presenter.performSelectionAnimation(new com.navdy.hud.app.ui.component.mainmenu.NearbyPlacesMenu.Anon1());
            return;
        }
        this.presenter.performSelectionAnimation(new com.navdy.hud.app.ui.component.mainmenu.NearbyPlacesMenu.Anon2(placeType));
    }

    public com.navdy.hud.app.ui.component.mainmenu.IMenu.Menu getType() {
        return com.navdy.hud.app.ui.component.mainmenu.IMenu.Menu.SEARCH;
    }

    public boolean isItemClickable(int id, int pos) {
        return true;
    }

    public com.navdy.hud.app.ui.component.vlist.VerticalList.Model getModelfromPos(int pos) {
        if (this.cachedList == null || this.cachedList.size() <= pos) {
            return null;
        }
        return (com.navdy.hud.app.ui.component.vlist.VerticalList.Model) this.cachedList.get(pos);
    }

    public boolean isBindCallsEnabled() {
        return false;
    }

    public void onBindToView(com.navdy.hud.app.ui.component.vlist.VerticalList.Model model, android.view.View view, int pos, com.navdy.hud.app.ui.component.vlist.VerticalList.ModelState state) {
    }

    public com.navdy.hud.app.ui.component.mainmenu.IMenu getChildMenu(com.navdy.hud.app.ui.component.mainmenu.IMenu parent2, java.lang.String args, java.lang.String path) {
        return null;
    }

    public void onUnload(com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel level) {
    }

    public void onItemSelected(com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState selection) {
    }

    public void onScrollIdle() {
    }

    public void onFastScrollStart() {
    }

    public void onFastScrollEnd() {
    }

    public void showToolTip() {
    }

    public boolean isFirstItemEmpty() {
        return true;
    }
}
