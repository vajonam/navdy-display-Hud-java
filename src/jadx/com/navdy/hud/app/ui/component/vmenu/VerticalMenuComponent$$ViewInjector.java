package com.navdy.hud.app.ui.component.vmenu;

public class VerticalMenuComponent$$ViewInjector {
    public static void inject(butterknife.ButterKnife.Finder finder, com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent target, java.lang.Object source) {
        target.leftContainer = (android.view.ViewGroup) finder.findRequiredView(source, com.navdy.hud.app.R.id.leftContainer, "field 'leftContainer'");
        target.selectedImage = (android.view.ViewGroup) finder.findRequiredView(source, com.navdy.hud.app.R.id.selectedImage, "field 'selectedImage'");
        target.selectedIconColorImage = (com.navdy.hud.app.ui.component.image.IconColorImageView) finder.findRequiredView(source, com.navdy.hud.app.R.id.selectedIconColorImage, "field 'selectedIconColorImage'");
        target.selectedIconImage = (com.navdy.hud.app.ui.component.image.InitialsImageView) finder.findRequiredView(source, com.navdy.hud.app.R.id.selectedIconImage, "field 'selectedIconImage'");
        target.selectedText = (android.widget.TextView) finder.findRequiredView(source, com.navdy.hud.app.R.id.selectedText, "field 'selectedText'");
        target.rightContainer = (android.view.ViewGroup) finder.findRequiredView(source, com.navdy.hud.app.R.id.rightContainer, "field 'rightContainer'");
        target.indicator = (com.navdy.hud.app.ui.component.carousel.CarouselIndicator) finder.findRequiredView(source, com.navdy.hud.app.R.id.indicator, "field 'indicator'");
        target.recyclerView = (com.navdy.hud.app.ui.component.vlist.VerticalRecyclerView) finder.findRequiredView(source, com.navdy.hud.app.R.id.recyclerView, "field 'recyclerView'");
        target.toolTip = (com.navdy.hud.app.view.ToolTipView) finder.findRequiredView(source, com.navdy.hud.app.R.id.tooltip, "field 'toolTip'");
        target.closeContainerScrim = finder.findRequiredView(source, com.navdy.hud.app.R.id.closeContainerScrim, "field 'closeContainerScrim'");
        target.closeIconContainer = (android.view.ViewGroup) finder.findRequiredView(source, com.navdy.hud.app.R.id.closeIconContainer, "field 'closeIconContainer'");
        target.closeContainer = (android.view.ViewGroup) finder.findRequiredView(source, com.navdy.hud.app.R.id.closeContainer, "field 'closeContainer'");
        target.closeHalo = (com.navdy.hud.app.ui.component.HaloView) finder.findRequiredView(source, com.navdy.hud.app.R.id.closeHalo, "field 'closeHalo'");
        target.fastScrollContainer = (android.view.ViewGroup) finder.findRequiredView(source, com.navdy.hud.app.R.id.fastScrollView, "field 'fastScrollContainer'");
        target.fastScrollText = (android.widget.TextView) finder.findRequiredView(source, com.navdy.hud.app.R.id.fastScrollText, "field 'fastScrollText'");
        target.selectedCustomView = (android.widget.FrameLayout) finder.findRequiredView(source, com.navdy.hud.app.R.id.selectedCustomView, "field 'selectedCustomView'");
    }

    public static void reset(com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent target) {
        target.leftContainer = null;
        target.selectedImage = null;
        target.selectedIconColorImage = null;
        target.selectedIconImage = null;
        target.selectedText = null;
        target.rightContainer = null;
        target.indicator = null;
        target.recyclerView = null;
        target.toolTip = null;
        target.closeContainerScrim = null;
        target.closeIconContainer = null;
        target.closeContainer = null;
        target.closeHalo = null;
        target.fastScrollContainer = null;
        target.fastScrollText = null;
        target.selectedCustomView = null;
    }
}
