package com.navdy.hud.app.ui.component.vlist.viewholder;

public abstract class VerticalViewHolder extends android.support.v7.widget.RecyclerView.ViewHolder {
    public static final java.lang.String EMPTY = "";
    public static final int NO_COLOR = 0;
    public static final int iconMargin;
    public static final int listItemHeight;
    public static final int mainIconSize;
    public static final int rootTopOffset;
    static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.class);
    public static final int scrollDistance = listItemHeight;
    public static final int selectedIconSize;
    public static final int selectedImageX;
    public static final int selectedImageY;
    public static final int selectedTextX;
    public static final int subTitle2Color;
    public static final int subTitleColor;
    public static final float subTitleHeight;
    public static final int textLeftMargin;
    public static final float titleHeight;
    public static final int unselectedIconSize;
    protected com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.State currentState;
    protected java.util.HashMap<java.lang.String, java.lang.String> extras;
    protected android.os.Handler handler;
    protected android.view.animation.Interpolator interpolator = com.navdy.hud.app.ui.component.vlist.VerticalList.getInterpolator();
    protected android.animation.AnimatorSet itemAnimatorSet;
    public android.view.ViewGroup layout;
    protected int pos = -1;
    protected com.navdy.hud.app.ui.component.vlist.VerticalList vlist;

    public enum AnimationType {
        NONE,
        MOVE,
        INIT
    }

    public enum State {
        SELECTED,
        UNSELECTED
    }

    public abstract void bind(com.navdy.hud.app.ui.component.vlist.VerticalList.Model model, com.navdy.hud.app.ui.component.vlist.VerticalList.ModelState modelState);

    public abstract void clearAnimation();

    public abstract void copyAndPosition(android.widget.ImageView imageView, android.widget.TextView textView, android.widget.TextView textView2, android.widget.TextView textView3, boolean z);

    public abstract com.navdy.hud.app.ui.component.vlist.VerticalList.ModelType getModelType();

    public abstract void select(com.navdy.hud.app.ui.component.vlist.VerticalList.Model model, int i, int i2);

    public abstract void setItemState(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.State state, com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.AnimationType animationType, int i, boolean z);

    static {
        android.content.res.Resources resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        selectedIconSize = resources.getDimensionPixelSize(com.navdy.hud.app.R.dimen.vlist_image);
        unselectedIconSize = resources.getDimensionPixelSize(com.navdy.hud.app.R.dimen.vlist_small_image);
        selectedImageX = resources.getDimensionPixelSize(com.navdy.hud.app.R.dimen.vmenu_sel_image_x);
        selectedImageY = resources.getDimensionPixelSize(com.navdy.hud.app.R.dimen.vmenu_sel_image_y);
        mainIconSize = resources.getDimensionPixelSize(com.navdy.hud.app.R.dimen.vlist_image_frame);
        textLeftMargin = resources.getDimensionPixelSize(com.navdy.hud.app.R.dimen.vlist_text_left_margin);
        listItemHeight = resources.getDimensionPixelSize(com.navdy.hud.app.R.dimen.vlist_item_height);
        titleHeight = resources.getDimension(com.navdy.hud.app.R.dimen.vlist_title);
        subTitleHeight = resources.getDimension(com.navdy.hud.app.R.dimen.vlist_subtitle);
        selectedTextX = resources.getDimensionPixelSize(com.navdy.hud.app.R.dimen.vmenu_sel_text_x);
        rootTopOffset = resources.getDimensionPixelSize(com.navdy.hud.app.R.dimen.vmenu_root_top_offset);
        iconMargin = resources.getDimensionPixelOffset(com.navdy.hud.app.R.dimen.vlist_icon_list_margin);
        subTitleColor = resources.getColor(com.navdy.hud.app.R.color.vlist_subtitle);
        subTitle2Color = resources.getColor(com.navdy.hud.app.R.color.vlist_subtitle);
    }

    public VerticalViewHolder(android.view.ViewGroup layout2, com.navdy.hud.app.ui.component.vlist.VerticalList vlist2, android.os.Handler handler2) {
        super(layout2);
        this.layout = layout2;
        this.vlist = vlist2;
        this.handler = handler2;
    }

    public void setPos(int pos2) {
        this.pos = pos2;
        this.currentState = null;
    }

    public int getPos() {
        return this.pos;
    }

    public void setExtras(java.util.HashMap<java.lang.String, java.lang.String> extras2) {
        this.extras = extras2;
    }

    public final void setState(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.State state, com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.AnimationType animation, int duration) {
        setState(state, animation, duration, true);
    }

    public final void setState(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.State state, com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.AnimationType animation, int duration, boolean startFluctuator) {
        switch (getModelType()) {
            case BLANK:
                return;
            default:
                if (sLogger.isLoggable(2)) {
                    sLogger.v("setState {" + this.pos + ", " + state + "} animate=" + animation + " dur=" + duration + " current-raw {" + this.vlist.getRawPosition() + "} vh-id{" + java.lang.System.identityHashCode(this) + "}");
                }
                if (this.currentState != state) {
                    clearAnimation();
                    this.currentState = state;
                    if (state == com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.State.SELECTED) {
                        this.vlist.selectedList.add(java.lang.Integer.valueOf(this.pos));
                    } else {
                        this.vlist.selectedList.remove(java.lang.Integer.valueOf(this.pos));
                    }
                    setItemState(state, animation, duration, startFluctuator);
                    if (this.itemAnimatorSet != null) {
                        this.itemAnimatorSet.setDuration((long) duration);
                        this.itemAnimatorSet.setInterpolator(this.interpolator);
                        this.itemAnimatorSet.start();
                        return;
                    }
                    return;
                } else if (state == com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.State.SELECTED) {
                    this.vlist.selectedList.add(java.lang.Integer.valueOf(this.pos));
                    return;
                } else {
                    this.vlist.selectedList.remove(java.lang.Integer.valueOf(this.pos));
                    return;
                }
        }
    }

    public void stopAnimation() {
        if (this.itemAnimatorSet != null && this.itemAnimatorSet.isRunning()) {
            this.itemAnimatorSet.removeAllListeners();
            this.itemAnimatorSet.cancel();
        }
        this.itemAnimatorSet = null;
    }

    public com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.State getState() {
        return this.currentState;
    }

    public void startFluctuator() {
    }

    public void preBind(com.navdy.hud.app.ui.component.vlist.VerticalList.Model model, com.navdy.hud.app.ui.component.vlist.VerticalList.ModelState modelState) {
    }

    public boolean handleKey(com.navdy.hud.app.manager.InputManager.CustomKeyEvent event) {
        return false;
    }

    public boolean hasToolTip() {
        return false;
    }
}
