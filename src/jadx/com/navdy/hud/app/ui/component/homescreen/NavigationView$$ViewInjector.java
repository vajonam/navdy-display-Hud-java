package com.navdy.hud.app.ui.component.homescreen;

public class NavigationView$$ViewInjector {
    public static void inject(butterknife.ButterKnife.Finder finder, com.navdy.hud.app.ui.component.homescreen.NavigationView target, java.lang.Object source) {
        target.mapView = (com.here.android.mpa.mapping.MapView) finder.findRequiredView(source, com.navdy.hud.app.R.id.mapView, "field 'mapView'");
        target.startDestinationFluctuatorView = (com.navdy.hud.app.ui.component.FluctuatorAnimatorView) finder.findRequiredView(source, com.navdy.hud.app.R.id.startDestinationFluctuator, "field 'startDestinationFluctuatorView'");
        target.mapMask = (android.widget.ImageView) finder.findRequiredView(source, com.navdy.hud.app.R.id.map_mask, "field 'mapMask'");
        target.mapIconIndicator = (android.widget.ImageView) finder.findRequiredView(source, com.navdy.hud.app.R.id.mapIconIndicator, "field 'mapIconIndicator'");
        target.noLocationView = (com.navdy.hud.app.ui.component.homescreen.NoLocationView) finder.findRequiredView(source, com.navdy.hud.app.R.id.noLocationContainer, "field 'noLocationView'");
    }

    public static void reset(com.navdy.hud.app.ui.component.homescreen.NavigationView target) {
        target.mapView = null;
        target.startDestinationFluctuatorView = null;
        target.mapMask = null;
        target.mapIconIndicator = null;
        target.noLocationView = null;
    }
}
