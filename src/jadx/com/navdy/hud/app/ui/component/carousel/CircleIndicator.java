package com.navdy.hud.app.ui.component.carousel;

class CircleIndicator extends android.widget.RelativeLayout implements com.navdy.hud.app.ui.component.carousel.IProgressIndicator {
    private int circleFocusSize;
    private int circleMargin;
    private int circleSize;
    private final android.content.Context context;
    /* access modifiers changed from: private */
    public int currentItem = -1;
    private int currentItemColor = -1;
    private int defaultColor;
    private int id = 100;
    private int itemCount;
    private android.view.View moveAnimatorView;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.ui.component.carousel.CarouselIndicator.Orientation orientation;

    class Anon1 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
        final /* synthetic */ int val$toPos;

        Anon1(int i) {
            this.val$toPos = i;
        }

        public void onAnimationEnd(android.animation.Animator animation) {
            com.navdy.hud.app.ui.component.carousel.CircleIndicator.this.currentItem = this.val$toPos;
        }
    }

    class Anon2 implements android.view.ViewTreeObserver.OnGlobalLayoutListener {
        final /* synthetic */ android.view.View val$view;

        Anon2(android.view.View view) {
            this.val$view = view;
        }

        public void onGlobalLayout() {
            int pos;
            if (com.navdy.hud.app.ui.component.carousel.CircleIndicator.this.orientation == com.navdy.hud.app.ui.component.carousel.CarouselIndicator.Orientation.HORIZONTAL) {
                pos = this.val$view.getLeft();
            } else {
                pos = this.val$view.getTop();
            }
            if (pos != 0) {
                this.val$view.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                com.navdy.hud.app.ui.component.carousel.CircleIndicator.this.setViewPos(this.val$view);
            }
        }
    }

    public CircleIndicator(android.content.Context context2) {
        super(context2);
        this.context = context2;
    }

    public CircleIndicator(android.content.Context context2, android.util.AttributeSet attrs) {
        super(context2, attrs);
        this.context = context2;
    }

    public void setProperties(int circleSize2, int circleFocusSize2, int circleMargin2, int defaultColor2) {
        this.circleSize = circleSize2;
        this.circleFocusSize = circleFocusSize2;
        this.circleMargin = circleMargin2;
        this.defaultColor = defaultColor2;
    }

    public void setOrientation(com.navdy.hud.app.ui.component.carousel.CarouselIndicator.Orientation orientation2) {
        this.orientation = orientation2;
    }

    public android.animation.AnimatorSet getItemMoveAnimator(int toPos, int color) {
        android.animation.AnimatorSet set = null;
        if (this.currentItem != -1 && toPos >= 0 && toPos < this.itemCount) {
            android.view.View childAt = getChildAt(this.currentItem);
            android.view.View targetItemView = getChildAt(toPos);
            android.graphics.drawable.GradientDrawable background = (android.graphics.drawable.GradientDrawable) this.moveAnimatorView.getBackground();
            if (color == -1) {
                background.setColor(this.defaultColor);
            } else {
                background.setColor(color);
            }
            set = new android.animation.AnimatorSet();
            set.addListener(new com.navdy.hud.app.ui.component.carousel.CircleIndicator.Anon1(toPos));
            set.play(android.animation.ObjectAnimator.ofFloat(this.moveAnimatorView, this.orientation == com.navdy.hud.app.ui.component.carousel.CarouselIndicator.Orientation.HORIZONTAL ? android.view.View.X : android.view.View.Y, new float[]{(float) getTargetViewPos(targetItemView)}));
        }
        return set;
    }

    public android.graphics.RectF getItemPos(int n) {
        if (n < 0 || n >= this.itemCount) {
            return null;
        }
        android.view.View view = getChildAt(n);
        android.view.View parent = (android.view.View) view.getParent();
        return new android.graphics.RectF((float) (view.getLeft() + parent.getLeft()), (float) (view.getTop() + parent.getTop()), 0.0f, 0.0f);
    }

    public void setItemCount(int n) {
        if (n < 0) {
            throw new java.lang.IllegalArgumentException();
        }
        this.currentItem = -1;
        if (getChildCount() == 0) {
            this.itemCount = n;
            buildItems(n);
        } else if (this.itemCount != n) {
            int diff = this.itemCount - n;
            if (diff > 0) {
                this.itemCount = n;
                removeView(this.moveAnimatorView);
                for (int i = 0; i < diff; i++) {
                    removeView(getChildAt(getChildCount() - 1));
                }
                int count = getChildCount();
                if (count > 0) {
                    android.view.ViewGroup.MarginLayoutParams params = (android.view.ViewGroup.MarginLayoutParams) getChildAt(count - 1).getLayoutParams();
                    if (this.orientation == com.navdy.hud.app.ui.component.carousel.CarouselIndicator.Orientation.HORIZONTAL) {
                        params.rightMargin = this.circleMargin;
                    } else {
                        params.bottomMargin = this.circleMargin;
                    }
                }
                addView(this.moveAnimatorView);
                return;
            }
            this.itemCount = n;
            removeView(this.moveAnimatorView);
            buildItems(-diff);
            addView(this.moveAnimatorView);
        }
    }

    private void buildItems(int n) {
        if (n != 0) {
            android.view.View prev = null;
            int nItems = getChildCount();
            if (nItems > 0) {
                prev = getChildAt(nItems - 1);
                android.view.ViewGroup.MarginLayoutParams params = (android.view.ViewGroup.MarginLayoutParams) prev.getLayoutParams();
                if (this.orientation == com.navdy.hud.app.ui.component.carousel.CarouselIndicator.Orientation.HORIZONTAL) {
                    params.rightMargin = 0;
                } else {
                    params.bottomMargin = 0;
                }
            }
            for (int i = nItems; i < n + nItems; i++) {
                android.view.View view = new android.view.View(this.context);
                int i2 = this.id;
                this.id = i2 + 1;
                view.setId(i2);
                view.setBackgroundResource(com.navdy.hud.app.R.drawable.circle_page_indicator);
                android.widget.RelativeLayout.LayoutParams lytParams = new android.widget.RelativeLayout.LayoutParams(this.circleSize, this.circleSize);
                if (i == 0 && prev == null) {
                    if (this.orientation == com.navdy.hud.app.ui.component.carousel.CarouselIndicator.Orientation.HORIZONTAL) {
                        lytParams.addRule(9);
                        lytParams.leftMargin = this.circleMargin;
                    } else {
                        lytParams.addRule(10);
                        lytParams.topMargin = this.circleMargin;
                    }
                }
                if (i == this.itemCount - 1) {
                    if (this.orientation == com.navdy.hud.app.ui.component.carousel.CarouselIndicator.Orientation.HORIZONTAL) {
                        lytParams.rightMargin = this.circleMargin;
                    } else {
                        lytParams.bottomMargin = this.circleMargin;
                    }
                }
                if (this.orientation == com.navdy.hud.app.ui.component.carousel.CarouselIndicator.Orientation.HORIZONTAL) {
                    lytParams.addRule(15);
                    if (i > 0) {
                        lytParams.leftMargin = this.circleMargin;
                    }
                    if (prev != null) {
                        lytParams.addRule(1, prev.getId());
                    }
                } else {
                    lytParams.addRule(14);
                    if (i > 0) {
                        lytParams.topMargin = this.circleMargin;
                    }
                    if (prev != null) {
                        lytParams.addRule(3, prev.getId());
                    }
                }
                view.setAlpha(0.5f);
                addView(view, lytParams);
                prev = view;
            }
            if (this.moveAnimatorView == null) {
                this.moveAnimatorView = new android.view.View(this.context);
                this.moveAnimatorView.setBackgroundResource(com.navdy.hud.app.R.drawable.circle_page_indicator_focus);
                this.moveAnimatorView.setLayoutParams(new android.widget.RelativeLayout.LayoutParams(this.circleFocusSize, this.circleFocusSize));
                this.moveAnimatorView.setVisibility(8);
                addView(this.moveAnimatorView);
            }
        }
    }

    public int getCurrentItem() {
        return this.currentItem;
    }

    public void setCurrentItem(int n, int color) {
        int pos;
        if (this.currentItem != n) {
            this.currentItemColor = color;
            if (n >= 0 && n < this.itemCount) {
                android.view.View view = getChildAt(n);
                if (this.orientation == com.navdy.hud.app.ui.component.carousel.CarouselIndicator.Orientation.HORIZONTAL) {
                    pos = view.getLeft();
                } else {
                    pos = view.getTop();
                }
                if (pos == 0) {
                    view.getViewTreeObserver().addOnGlobalLayoutListener(new com.navdy.hud.app.ui.component.carousel.CircleIndicator.Anon2(view));
                } else {
                    setViewPos(view);
                }
            }
            this.currentItem = n;
        }
    }

    public void setCurrentItem(int n) {
        setCurrentItem(n, -1);
    }

    private int getTargetViewPos(android.view.View targetView) {
        if (this.orientation == com.navdy.hud.app.ui.component.carousel.CarouselIndicator.Orientation.HORIZONTAL) {
            return (targetView.getLeft() + (targetView.getWidth() / 2)) - (this.circleFocusSize / 2);
        }
        return (targetView.getTop() + (targetView.getHeight() / 2)) - (this.circleFocusSize / 2);
    }

    /* access modifiers changed from: private */
    public void setViewPos(android.view.View view) {
        float pos = (float) getTargetViewPos(view);
        int itemColor = this.currentItemColor;
        if (itemColor == -1) {
            itemColor = this.defaultColor;
        }
        if (this.orientation == com.navdy.hud.app.ui.component.carousel.CarouselIndicator.Orientation.HORIZONTAL) {
            this.moveAnimatorView.setX(pos);
        } else {
            this.moveAnimatorView.setY(pos);
        }
        ((android.graphics.drawable.GradientDrawable) this.moveAnimatorView.getBackground()).setColor(itemColor);
        this.moveAnimatorView.setVisibility(0);
        this.moveAnimatorView.invalidate();
    }
}
