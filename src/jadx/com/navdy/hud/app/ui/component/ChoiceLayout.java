package com.navdy.hud.app.ui.component;

public class ChoiceLayout extends android.widget.FrameLayout {
    private static final int ANIMATION_EXECUTE_DURATION = 200;
    private static final int ANIMATION_SLIDE_DURATION = 100;
    private static final java.lang.Object ANIMATION_TAG = new java.lang.Object();
    private static final java.lang.String EMPTY = "";
    private static final int KEY_UP_DOWN_DURATION = 50;
    private static int MIN_HIGHLIGHT_VIEW_WIDTH;
    /* access modifiers changed from: private */
    public static int choiceHighlightColor;
    /* access modifiers changed from: private */
    public static int choiceHighlightMargin;
    private static int choiceTextPaddingBottomDefault;
    private static int choiceTextPaddingDefault;
    private static int choiceTextSizeDefault;
    /* access modifiers changed from: private */
    public static int textDeselectionColor;
    /* access modifiers changed from: private */
    public static int textSelectionColor = -1;
    private android.view.ViewTreeObserver.OnGlobalLayoutListener animationLayoutListener;
    /* access modifiers changed from: private */
    public android.animation.AnimatorSet animatorSet;
    protected android.widget.LinearLayout choiceContainer;
    private int choiceTextPaddingBottom;
    private int choiceTextPaddingLeft;
    private int choiceTextPaddingRight;
    private int choiceTextPaddingTop;
    private int choiceTextSize;
    protected java.util.List<com.navdy.hud.app.ui.component.ChoiceLayout.Choice> choices;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.ui.component.ChoiceLayout.Mode currentMode;
    /* access modifiers changed from: private */
    public android.view.View currentSelectionView;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.ui.framework.DefaultAnimationListener executeAnimationListener;
    /* access modifiers changed from: private */
    public android.animation.ValueAnimator.AnimatorUpdateListener executeUpdateListener;
    /* access modifiers changed from: private */
    public android.view.View executeView;
    /* access modifiers changed from: private */
    public boolean executing;
    private android.os.Handler handler;
    private boolean highlightPersistent;
    protected android.view.View highlightView;
    private android.animation.Animator.AnimatorListener highlightViewListener;
    private int highlightVisibility;
    private boolean initialized;
    private com.navdy.hud.app.ui.component.ChoiceLayout.IListener listener;
    /* access modifiers changed from: private */
    public android.view.View oldSelectionView;
    private java.util.Queue<com.navdy.hud.app.ui.component.ChoiceLayout.Operation> operationQueue;
    private boolean operationRunning;
    private boolean pressedDown;
    /* access modifiers changed from: private */
    public int selectedItem;
    private com.navdy.hud.app.ui.framework.DefaultAnimationListener upDownAnimationListener;
    private android.animation.ValueAnimator.AnimatorUpdateListener upDownUpdateListener;

    class Anon1 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
        Anon1() {
        }

        public void onAnimationStart(android.animation.Animator animation) {
            super.onAnimationStart(animation);
        }

        public void onAnimationEnd(android.animation.Animator animation) {
            super.onAnimationEnd(animation);
            com.navdy.hud.app.ui.component.ChoiceLayout.this.runQueuedOperation();
        }
    }

    class Anon2 implements android.animation.ValueAnimator.AnimatorUpdateListener {
        Anon2() {
        }

        public void onAnimationUpdate(android.animation.ValueAnimator animation) {
            ((android.view.ViewGroup.MarginLayoutParams) com.navdy.hud.app.ui.component.ChoiceLayout.this.highlightView.getLayoutParams()).topMargin = ((java.lang.Integer) animation.getAnimatedValue()).intValue();
            com.navdy.hud.app.ui.component.ChoiceLayout.this.highlightView.invalidate();
            com.navdy.hud.app.ui.component.ChoiceLayout.this.highlightView.requestLayout();
        }
    }

    class Anon3 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
        Anon3() {
        }

        public void onAnimationEnd(android.animation.Animator animation) {
            if (com.navdy.hud.app.ui.component.ChoiceLayout.this.executeView != null) {
                if (com.navdy.hud.app.ui.component.ChoiceLayout.this.executeView.getTag() != null) {
                    com.navdy.hud.app.ui.component.ChoiceLayout.this.executeView.setTag(null);
                    if (com.navdy.hud.app.ui.component.ChoiceLayout.this.executeView instanceof android.widget.TextView) {
                        ((android.widget.TextView) com.navdy.hud.app.ui.component.ChoiceLayout.this.executeView).setTextColor(com.navdy.hud.app.ui.component.ChoiceLayout.choiceHighlightColor);
                    } else if (com.navdy.hud.app.ui.component.ChoiceLayout.this.executeView instanceof android.widget.ImageView) {
                        ((android.widget.ImageView) com.navdy.hud.app.ui.component.ChoiceLayout.this.executeView).setColorFilter(com.navdy.hud.app.ui.component.ChoiceLayout.choiceHighlightColor);
                    }
                    android.animation.ValueAnimator valueAnimator = android.animation.ValueAnimator.ofInt(new int[]{0, com.navdy.hud.app.ui.component.ChoiceLayout.choiceHighlightMargin});
                    valueAnimator.addUpdateListener(com.navdy.hud.app.ui.component.ChoiceLayout.this.executeUpdateListener);
                    valueAnimator.addListener(com.navdy.hud.app.ui.component.ChoiceLayout.this.executeAnimationListener);
                    android.animation.ObjectAnimator alphaAnimator = android.animation.ObjectAnimator.ofFloat(com.navdy.hud.app.ui.component.ChoiceLayout.this.highlightView, "alpha", new float[]{1.0f, 0.5f});
                    com.navdy.hud.app.ui.component.ChoiceLayout.this.animatorSet = new android.animation.AnimatorSet();
                    com.navdy.hud.app.ui.component.ChoiceLayout.this.animatorSet.setDuration(200);
                    com.navdy.hud.app.ui.component.ChoiceLayout.this.animatorSet.playTogether(new android.animation.Animator[]{valueAnimator, alphaAnimator});
                    com.navdy.hud.app.ui.component.ChoiceLayout.this.animatorSet.start();
                    return;
                }
                if (!com.navdy.hud.app.ui.component.ChoiceLayout.this.isHighlightPersistent()) {
                    com.navdy.hud.app.ui.component.ChoiceLayout.this.highlightView.setVisibility(4);
                }
                com.navdy.hud.app.ui.component.ChoiceLayout.this.highlightView.setAlpha(1.0f);
                ((android.view.ViewGroup.MarginLayoutParams) com.navdy.hud.app.ui.component.ChoiceLayout.this.highlightView.getLayoutParams()).topMargin = com.navdy.hud.app.ui.component.ChoiceLayout.choiceHighlightMargin;
                com.navdy.hud.app.ui.component.ChoiceLayout.this.executing = false;
                com.navdy.hud.app.ui.component.ChoiceLayout.this.animatorSet = null;
                com.navdy.hud.app.ui.component.ChoiceLayout.this.clearOperationQueue();
                if (com.navdy.hud.app.ui.component.ChoiceLayout.this.selectedItem != -1) {
                    com.navdy.hud.app.ui.component.ChoiceLayout.this.callListener(com.navdy.hud.app.ui.component.ChoiceLayout.this.selectedItem);
                }
                if (com.navdy.hud.app.ui.component.ChoiceLayout.this.executeView instanceof android.widget.TextView) {
                    ((android.widget.TextView) com.navdy.hud.app.ui.component.ChoiceLayout.this.currentSelectionView).setTextColor(com.navdy.hud.app.ui.component.ChoiceLayout.textSelectionColor);
                } else if (com.navdy.hud.app.ui.component.ChoiceLayout.this.executeView instanceof android.widget.ImageView) {
                    ((android.widget.ImageView) com.navdy.hud.app.ui.component.ChoiceLayout.this.currentSelectionView).setColorFilter(com.navdy.hud.app.ui.component.ChoiceLayout.textSelectionColor);
                }
            }
        }
    }

    class Anon4 implements android.animation.ValueAnimator.AnimatorUpdateListener {
        Anon4() {
        }

        public void onAnimationUpdate(android.animation.ValueAnimator animation) {
            if (com.navdy.hud.app.ui.component.ChoiceLayout.this.executeView != null) {
                ((android.view.ViewGroup.MarginLayoutParams) com.navdy.hud.app.ui.component.ChoiceLayout.this.highlightView.getLayoutParams()).topMargin = ((java.lang.Integer) animation.getAnimatedValue()).intValue();
                com.navdy.hud.app.ui.component.ChoiceLayout.this.highlightView.invalidate();
                com.navdy.hud.app.ui.component.ChoiceLayout.this.highlightView.requestLayout();
            }
        }
    }

    class Anon5 implements android.view.ViewTreeObserver.OnGlobalLayoutListener {
        Anon5() {
        }

        public void onGlobalLayout() {
            android.view.View view = com.navdy.hud.app.ui.component.ChoiceLayout.this.choiceContainer.getChildAt(com.navdy.hud.app.ui.component.ChoiceLayout.this.selectedItem);
            if (view != null) {
                view.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                com.navdy.hud.app.ui.component.ChoiceLayout.this.selectInitialItem(com.navdy.hud.app.ui.component.ChoiceLayout.this.selectedItem);
            }
        }
    }

    class Anon6 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
        Anon6() {
        }

        public void onAnimationEnd(android.animation.Animator animator) {
            if (com.navdy.hud.app.ui.component.ChoiceLayout.this.oldSelectionView != null) {
                if (com.navdy.hud.app.ui.component.ChoiceLayout.this.currentMode == com.navdy.hud.app.ui.component.ChoiceLayout.Mode.LABEL) {
                    ((android.widget.TextView) com.navdy.hud.app.ui.component.ChoiceLayout.this.oldSelectionView).setTextColor(com.navdy.hud.app.ui.component.ChoiceLayout.textDeselectionColor);
                } else {
                    ((android.widget.ImageView) com.navdy.hud.app.ui.component.ChoiceLayout.this.oldSelectionView).setColorFilter(com.navdy.hud.app.ui.component.ChoiceLayout.textDeselectionColor);
                }
            }
            java.lang.Integer pos = null;
            if (com.navdy.hud.app.ui.component.ChoiceLayout.this.currentSelectionView != null) {
                java.lang.Object tag = com.navdy.hud.app.ui.component.ChoiceLayout.this.currentSelectionView.getTag();
                if (tag instanceof java.lang.Integer) {
                    pos = (java.lang.Integer) tag;
                }
                if (com.navdy.hud.app.ui.component.ChoiceLayout.this.currentMode == com.navdy.hud.app.ui.component.ChoiceLayout.Mode.LABEL) {
                    ((android.widget.TextView) com.navdy.hud.app.ui.component.ChoiceLayout.this.currentSelectionView).setTextColor(com.navdy.hud.app.ui.component.ChoiceLayout.textSelectionColor);
                } else {
                    ((android.widget.ImageView) com.navdy.hud.app.ui.component.ChoiceLayout.this.currentSelectionView).setColorFilter(com.navdy.hud.app.ui.component.ChoiceLayout.textSelectionColor);
                }
            }
            com.navdy.hud.app.ui.component.ChoiceLayout.this.animatorSet = null;
            if (pos != null) {
                com.navdy.hud.app.ui.component.ChoiceLayout.this.callSelectListener(pos.intValue());
            }
            com.navdy.hud.app.ui.component.ChoiceLayout.this.runQueuedOperation();
        }
    }

    class Anon7 implements android.animation.ValueAnimator.AnimatorUpdateListener {
        Anon7() {
        }

        public void onAnimationUpdate(android.animation.ValueAnimator valueAnimator) {
            ((android.widget.FrameLayout.LayoutParams) com.navdy.hud.app.ui.component.ChoiceLayout.this.highlightView.getLayoutParams()).width = ((java.lang.Integer) valueAnimator.getAnimatedValue()).intValue();
            com.navdy.hud.app.ui.component.ChoiceLayout.this.highlightView.invalidate();
            com.navdy.hud.app.ui.component.ChoiceLayout.this.highlightView.requestLayout();
        }
    }

    class Anon8 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.hud.app.ui.component.ChoiceLayout.Operation val$operation;

        Anon8(com.navdy.hud.app.ui.component.ChoiceLayout.Operation operation) {
            this.val$operation = operation;
        }

        public void run() {
            com.navdy.hud.app.ui.component.ChoiceLayout.this.runOperation(this.val$operation, true);
        }
    }

    public static class Choice implements android.os.Parcelable {
        public static final android.os.Parcelable.Creator<com.navdy.hud.app.ui.component.ChoiceLayout.Choice> CREATOR = new com.navdy.hud.app.ui.component.ChoiceLayout.Choice.Anon1();
        public com.navdy.hud.app.ui.component.ChoiceLayout.Icon icon;
        public int id;
        public java.lang.String label;

        static class Anon1 implements android.os.Parcelable.Creator<com.navdy.hud.app.ui.component.ChoiceLayout.Choice> {
            Anon1() {
            }

            public com.navdy.hud.app.ui.component.ChoiceLayout.Choice createFromParcel(android.os.Parcel in) {
                return new com.navdy.hud.app.ui.component.ChoiceLayout.Choice(in);
            }

            public com.navdy.hud.app.ui.component.ChoiceLayout.Choice[] newArray(int size) {
                return new com.navdy.hud.app.ui.component.ChoiceLayout.Choice[size];
            }
        }

        public Choice(java.lang.String label2, int id2) {
            this.label = label2;
            this.id = id2;
        }

        public Choice(com.navdy.hud.app.ui.component.ChoiceLayout.Icon icon2, int id2) {
            this.icon = icon2;
            this.id = id2;
        }

        public Choice(android.os.Parcel in) {
            this.label = in.readString();
            if (android.text.TextUtils.isEmpty(this.label)) {
                this.label = null;
            }
            int i = in.readInt();
            int j = in.readInt();
            if (!(i == -1 || j == -1)) {
                this.icon = new com.navdy.hud.app.ui.component.ChoiceLayout.Icon(i, j);
            }
            this.id = in.readInt();
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(android.os.Parcel dest, int flags) {
            int i;
            int i2 = -1;
            dest.writeString(this.label != null ? this.label : "");
            if (this.icon != null) {
                i = this.icon.resIdSelected;
            } else {
                i = -1;
            }
            dest.writeInt(i);
            if (this.icon != null) {
                i2 = this.icon.resIdUnSelected;
            }
            dest.writeInt(i2);
            dest.writeInt(this.id);
        }
    }

    public interface IListener {
        void executeItem(int i, int i2);

        void itemSelected(int i, int i2);
    }

    public static class Icon {
        public int resIdSelected;
        public int resIdUnSelected;

        public Icon(int resId) {
            this.resIdSelected = resId;
            this.resIdUnSelected = resId;
        }

        public Icon(int resIdSelected2, int resIdUnSelected2) {
            this.resIdSelected = resIdSelected2;
            this.resIdUnSelected = resIdUnSelected2;
        }
    }

    public enum Mode {
        LABEL,
        ICON
    }

    enum Operation {
        MOVE_LEFT,
        MOVE_RIGHT,
        EXECUTE,
        KEY_DOWN,
        KEY_UP
    }

    /* access modifiers changed from: protected */
    public boolean isHighlightPersistent() {
        return this.highlightPersistent;
    }

    public void setHighlightPersistent(boolean highlightPersistent2) {
        this.highlightPersistent = highlightPersistent2;
    }

    public ChoiceLayout(android.content.Context context) {
        this(context, null);
    }

    public ChoiceLayout(android.content.Context context, android.util.AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ChoiceLayout(android.content.Context context, android.util.AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.highlightPersistent = false;
        this.highlightVisibility = 0;
        this.listener = null;
        this.selectedItem = -1;
        this.handler = new android.os.Handler();
        this.operationQueue = new java.util.LinkedList();
        this.upDownAnimationListener = new com.navdy.hud.app.ui.component.ChoiceLayout.Anon1();
        this.upDownUpdateListener = new com.navdy.hud.app.ui.component.ChoiceLayout.Anon2();
        this.executeAnimationListener = new com.navdy.hud.app.ui.component.ChoiceLayout.Anon3();
        this.executeUpdateListener = new com.navdy.hud.app.ui.component.ChoiceLayout.Anon4();
        this.animationLayoutListener = new com.navdy.hud.app.ui.component.ChoiceLayout.Anon5();
        this.highlightViewListener = new com.navdy.hud.app.ui.component.ChoiceLayout.Anon6();
        init();
        android.content.res.TypedArray a = context.obtainStyledAttributes(attrs, com.navdy.hud.app.R.styleable.ChoiceLayout, defStyleAttr, 0);
        if (a != null) {
            this.choiceTextPaddingLeft = (int) a.getDimension(0, (float) choiceTextPaddingDefault);
            this.choiceTextPaddingRight = (int) a.getDimension(1, (float) choiceTextPaddingDefault);
            this.choiceTextPaddingBottom = (int) a.getDimension(3, (float) choiceTextPaddingBottomDefault);
            this.choiceTextPaddingTop = (int) a.getDimension(2, (float) choiceTextPaddingDefault);
            this.choiceTextSize = (int) a.getDimension(4, (float) choiceTextSizeDefault);
            a.recycle();
        }
    }

    public void setHighlightVisibility(int highlightVisibility2) {
        this.highlightVisibility = highlightVisibility2;
        invalidate();
    }

    private void init() {
        android.view.LayoutInflater.from(getContext()).inflate(com.navdy.hud.app.R.layout.choices_lyt, this, true);
        invalidate();
        this.choiceContainer = (android.widget.LinearLayout) findViewById(com.navdy.hud.app.R.id.choiceContainer);
        this.highlightView = findViewById(com.navdy.hud.app.R.id.choiceHighlight);
        if (textSelectionColor == -1) {
            android.content.res.Resources resources = getResources();
            MIN_HIGHLIGHT_VIEW_WIDTH = resources.getDimensionPixelSize(com.navdy.hud.app.R.dimen.min_highlight_view_width);
            textSelectionColor = resources.getColor(com.navdy.hud.app.R.color.choice_selection);
            textDeselectionColor = resources.getColor(com.navdy.hud.app.R.color.choice_deselection);
            choiceTextPaddingDefault = (int) resources.getDimension(com.navdy.hud.app.R.dimen.choices_lyt_item_padding);
            choiceTextPaddingBottomDefault = (int) resources.getDimension(com.navdy.hud.app.R.dimen.choices_lyt_item_padding_bottom);
            choiceTextSizeDefault = (int) resources.getDimension(com.navdy.hud.app.R.dimen.choices_lyt_text_size);
            choiceHighlightColor = resources.getColor(com.navdy.hud.app.R.color.cyan);
            choiceHighlightMargin = (int) resources.getDimension(com.navdy.hud.app.R.dimen.choices_lyt_highlight_margin);
        }
    }

    public void setChoices(com.navdy.hud.app.ui.component.ChoiceLayout.Mode mode, java.util.List<com.navdy.hud.app.ui.component.ChoiceLayout.Choice> choices2, int initialSelection, com.navdy.hud.app.ui.component.ChoiceLayout.IListener listener2) {
        this.currentMode = mode;
        this.choices = choices2;
        this.listener = listener2;
        this.initialized = false;
        com.navdy.hud.app.ui.component.ChoiceLayout.Operation pendingOp = (com.navdy.hud.app.ui.component.ChoiceLayout.Operation) this.operationQueue.peek();
        this.executeView = null;
        clearOperationQueue();
        this.executing = false;
        this.selectedItem = -1;
        if (choices2 != null && choices2.size() > 0) {
            if (initialSelection < 0 || initialSelection >= choices2.size()) {
                this.selectedItem = 0;
            } else {
                this.selectedItem = initialSelection;
            }
        }
        buildChoices();
        selectInitialItem(this.selectedItem);
        if (pendingOp == com.navdy.hud.app.ui.component.ChoiceLayout.Operation.KEY_UP) {
            ((android.widget.FrameLayout.LayoutParams) this.highlightView.getLayoutParams()).topMargin = choiceHighlightMargin;
        }
    }

    public java.util.List<com.navdy.hud.app.ui.component.ChoiceLayout.Choice> getChoices() {
        return this.choices;
    }

    private void buildChoices() {
        android.view.View view;
        this.choiceContainer.removeAllViews();
        this.currentSelectionView = null;
        this.oldSelectionView = null;
        this.highlightView.setVisibility(4);
        if (this.choices != null) {
            int len = this.choices.size();
            for (int i = 0; i < len; i++) {
                com.navdy.hud.app.ui.component.ChoiceLayout.Choice choice = (com.navdy.hud.app.ui.component.ChoiceLayout.Choice) this.choices.get(i);
                if (this.currentMode == com.navdy.hud.app.ui.component.ChoiceLayout.Mode.LABEL) {
                    android.widget.TextView textView = buildChoice("", i);
                    textView.setText(choice.label);
                    view = textView;
                } else {
                    android.widget.ImageView imageView = new android.widget.ImageView(getContext());
                    imageView.setScaleType(android.widget.ImageView.ScaleType.FIT_CENTER);
                    imageView.setImageResource(choice.icon.resIdUnSelected);
                    imageView.setColorFilter(textDeselectionColor);
                    view = imageView;
                }
                android.widget.LinearLayout.LayoutParams lytParams = new android.widget.LinearLayout.LayoutParams(-2, -2);
                lytParams.setMargins(this.choiceTextPaddingLeft, 0, this.choiceTextPaddingRight, 0);
                this.choiceContainer.addView(view, lytParams);
            }
        }
        invalidate();
        requestLayout();
    }

    private boolean executeSelectedItemInternal() {
        if (!this.initialized || this.executing) {
            return false;
        }
        if (this.selectedItem != -1 && getItemCount() > 0) {
            this.executing = true;
            if (this.highlightVisibility == 0) {
                this.executeView = this.choiceContainer.getChildAt(this.selectedItem);
                if (this.executeView == null) {
                    callListener(this.selectedItem);
                    this.executing = false;
                } else {
                    this.executeView.setTag(ANIMATION_TAG);
                    android.animation.ValueAnimator valueAnimator = android.animation.ValueAnimator.ofInt(new int[]{choiceHighlightMargin, 0});
                    valueAnimator.addListener(this.executeAnimationListener);
                    valueAnimator.addUpdateListener(this.executeUpdateListener);
                    this.animatorSet = new android.animation.AnimatorSet();
                    this.animatorSet.setDuration(200);
                    this.animatorSet.play(valueAnimator);
                    this.animatorSet.start();
                    return true;
                }
            } else {
                callListener(this.selectedItem);
                this.executing = false;
            }
        }
        return false;
    }

    private boolean moveSelectionLeftInternal() {
        if (!this.initialized || this.executing || this.selectedItem == -1) {
            return false;
        }
        return setSelectedItem(this.selectedItem - 1);
    }

    private boolean moveSelectionRightInternal() {
        if (!this.initialized || this.executing || this.selectedItem == -1) {
            return false;
        }
        return setSelectedItem(this.selectedItem + 1);
    }

    private boolean keyDownSelectedItemInternal() {
        if (!this.initialized || this.pressedDown) {
            return false;
        }
        if (this.selectedItem != -1 && getItemCount() > 0) {
            if (this.currentMode == com.navdy.hud.app.ui.component.ChoiceLayout.Mode.LABEL) {
                ((android.widget.TextView) this.currentSelectionView).setTextColor(choiceHighlightColor);
            } else {
                ((android.widget.ImageView) this.currentSelectionView).setColorFilter(choiceHighlightColor);
            }
            if (this.highlightVisibility == 0) {
                this.pressedDown = true;
                if (this.choiceContainer.getChildAt(this.selectedItem) != null) {
                    android.animation.ValueAnimator valueAnimator = android.animation.ValueAnimator.ofInt(new int[]{choiceHighlightMargin, 0});
                    valueAnimator.addListener(this.upDownAnimationListener);
                    valueAnimator.addUpdateListener(this.upDownUpdateListener);
                    this.animatorSet = new android.animation.AnimatorSet();
                    this.animatorSet.setDuration(50);
                    this.animatorSet.play(valueAnimator);
                    this.animatorSet.start();
                    return true;
                }
            }
        }
        return true;
    }

    private boolean keyUpSelectedItemInternal() {
        if (!this.initialized || !this.pressedDown) {
            return false;
        }
        if (this.selectedItem != -1 && getItemCount() > 0) {
            if (this.currentMode == com.navdy.hud.app.ui.component.ChoiceLayout.Mode.LABEL) {
                ((android.widget.TextView) this.currentSelectionView).setTextColor(textSelectionColor);
            } else {
                ((android.widget.ImageView) this.currentSelectionView).setColorFilter(textSelectionColor);
            }
            if (this.highlightVisibility == 0) {
                this.pressedDown = false;
                if (this.choiceContainer.getChildAt(this.selectedItem) != null) {
                    android.animation.ValueAnimator valueAnimator = android.animation.ValueAnimator.ofInt(new int[]{0, choiceHighlightMargin});
                    valueAnimator.addListener(this.upDownAnimationListener);
                    valueAnimator.addUpdateListener(this.upDownUpdateListener);
                    this.animatorSet = new android.animation.AnimatorSet();
                    this.animatorSet.setDuration(50);
                    this.animatorSet.play(valueAnimator);
                    this.animatorSet.start();
                    return true;
                }
            }
        }
        return true;
    }

    public void moveSelectionLeft() {
        runOperation(com.navdy.hud.app.ui.component.ChoiceLayout.Operation.MOVE_LEFT, false);
    }

    public void moveSelectionRight() {
        runOperation(com.navdy.hud.app.ui.component.ChoiceLayout.Operation.MOVE_RIGHT, false);
    }

    public void keyDownSelectedItem() {
        runOperation(com.navdy.hud.app.ui.component.ChoiceLayout.Operation.KEY_DOWN, false);
    }

    public void keyUpSelectedItem() {
        runOperation(com.navdy.hud.app.ui.component.ChoiceLayout.Operation.KEY_UP, false);
    }

    public void executeSelectedItem(boolean animated) {
        runOperation(com.navdy.hud.app.ui.component.ChoiceLayout.Operation.EXECUTE, false);
    }

    private android.widget.TextView buildChoice(java.lang.String label, int pos) {
        android.widget.TextView textView = new android.widget.TextView(getContext());
        textView.setGravity(17);
        textView.setSingleLine(true);
        textView.setText(label);
        textView.setIncludeFontPadding(false);
        textView.setTextSize(0, (float) this.choiceTextSize);
        textView.setTypeface(null, 1);
        textView.setTextColor(textDeselectionColor);
        return textView;
    }

    private int getItemCount() {
        if (this.choices != null) {
            return this.choices.size();
        }
        return 0;
    }

    /* access modifiers changed from: private */
    public void callListener(int pos) {
        if (this.listener != null) {
            com.navdy.hud.app.audio.SoundUtils.playSound(com.navdy.hud.app.audio.SoundUtils.Sound.MENU_SELECT);
            if (this.choices != null && pos >= 0 && pos < this.choices.size()) {
                this.listener.executeItem(pos, ((com.navdy.hud.app.ui.component.ChoiceLayout.Choice) this.choices.get(pos)).id);
            }
        }
    }

    /* access modifiers changed from: private */
    public void callSelectListener(int pos) {
        if (this.listener != null && this.choices != null && pos >= 0 && pos < this.choices.size()) {
            this.listener.itemSelected(pos, ((com.navdy.hud.app.ui.component.ChoiceLayout.Choice) this.choices.get(pos)).id);
        }
    }

    /* access modifiers changed from: protected */
    public boolean setSelectedItem(int pos) {
        int oldWidth;
        if (this.selectedItem == pos) {
            return false;
        }
        int itemCount = getItemCount();
        if (pos < 0 || pos >= itemCount) {
            return false;
        }
        this.selectedItem = pos;
        android.view.View view = this.choiceContainer.getChildAt(pos);
        this.oldSelectionView = this.currentSelectionView;
        this.currentSelectionView = view;
        this.currentSelectionView.setTag(java.lang.Integer.valueOf(pos));
        int width = java.lang.Math.max(view.getMeasuredWidth(), MIN_HIGHLIGHT_VIEW_WIDTH);
        if (width == 0) {
            return false;
        }
        int width2 = width - (view.getPaddingLeft() + view.getPaddingRight());
        if (this.oldSelectionView != null) {
            oldWidth = java.lang.Math.max(this.oldSelectionView.getMeasuredWidth() - (this.oldSelectionView.getPaddingLeft() + this.oldSelectionView.getPaddingRight()), MIN_HIGHLIGHT_VIEW_WIDTH);
        } else {
            oldWidth = width2;
        }
        this.animatorSet = new android.animation.AnimatorSet();
        android.animation.ObjectAnimator a1 = android.animation.ObjectAnimator.ofFloat(this.highlightView, "x", new float[]{getXPositionForHighlight(view)});
        android.animation.ValueAnimator v1 = android.animation.ValueAnimator.ofInt(new int[]{oldWidth, width2});
        v1.addUpdateListener(new com.navdy.hud.app.ui.component.ChoiceLayout.Anon7());
        this.highlightView.setVisibility(this.highlightVisibility);
        this.highlightView.invalidate();
        this.highlightView.requestLayout();
        this.animatorSet.playTogether(new android.animation.Animator[]{a1, v1});
        this.animatorSet.setDuration(100);
        this.animatorSet.addListener(this.highlightViewListener);
        this.animatorSet.start();
        com.navdy.hud.app.audio.SoundUtils.playSound(com.navdy.hud.app.audio.SoundUtils.Sound.MENU_MOVE);
        return true;
    }

    private float getXPositionForHighlight(android.view.View view) {
        int width = view.getMeasuredWidth() - (view.getPaddingLeft() + view.getPaddingRight());
        int left = view.getLeft() + view.getPaddingLeft();
        if (width >= MIN_HIGHLIGHT_VIEW_WIDTH) {
            return (float) left;
        }
        return (float) (left - ((MIN_HIGHLIGHT_VIEW_WIDTH - width) / 2));
    }

    /* access modifiers changed from: private */
    public void selectInitialItem(int pos) {
        if (pos != -1) {
            android.view.View viewToHighlight = this.choiceContainer.getChildAt(pos);
            if (viewToHighlight.getMeasuredWidth() == 0) {
                viewToHighlight.getViewTreeObserver().addOnGlobalLayoutListener(this.animationLayoutListener);
                return;
            }
            int xPos = (int) getXPositionForHighlight(viewToHighlight);
            ((android.widget.FrameLayout.LayoutParams) this.highlightView.getLayoutParams()).width = java.lang.Math.max(viewToHighlight.getMeasuredWidth() - (viewToHighlight.getPaddingLeft() + viewToHighlight.getPaddingRight()), MIN_HIGHLIGHT_VIEW_WIDTH);
            this.highlightView.setX((float) xPos);
            this.highlightView.setVisibility(this.highlightVisibility);
            this.highlightView.invalidate();
            this.highlightView.requestLayout();
            this.currentSelectionView = viewToHighlight;
            this.currentSelectionView.setTag(java.lang.Integer.valueOf(pos));
            if (this.currentMode == com.navdy.hud.app.ui.component.ChoiceLayout.Mode.LABEL) {
                ((android.widget.TextView) this.currentSelectionView).setTextColor(textSelectionColor);
            } else {
                ((android.widget.ImageView) this.currentSelectionView).setColorFilter(textSelectionColor);
            }
            this.initialized = true;
        }
    }

    public void setLabelTextSize(int size) {
        this.choiceTextSize = size;
    }

    public void setItemPadding(int size) {
        this.choiceTextPaddingLeft = size;
        this.choiceTextPaddingRight = size;
    }

    public void resetAnimationElements() {
        if (this.highlightVisibility == 0 && this.highlightView != null) {
            this.highlightView.setVisibility(0);
        }
        if (this.executeView == null) {
            return;
        }
        if (this.executeView instanceof android.widget.TextView) {
            ((android.widget.TextView) this.executeView).setTextColor(textSelectionColor);
            this.executeView = null;
        } else if (this.executeView instanceof android.widget.ImageView) {
            ((android.widget.ImageView) this.executeView).setColorFilter(textSelectionColor);
            this.executeView = null;
        }
    }

    public boolean hasChoiceId(int id) {
        for (com.navdy.hud.app.ui.component.ChoiceLayout.Choice choice : this.choices) {
            if (choice.id == id) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: private */
    public void runOperation(com.navdy.hud.app.ui.component.ChoiceLayout.Operation operation, boolean ignoreCurrent) {
        if (ignoreCurrent || !this.operationRunning) {
            this.operationRunning = true;
            switch (operation) {
                case MOVE_LEFT:
                    if (!moveSelectionLeftInternal()) {
                        runQueuedOperation();
                        return;
                    }
                    return;
                case MOVE_RIGHT:
                    if (!moveSelectionRightInternal()) {
                        runQueuedOperation();
                        return;
                    }
                    return;
                case EXECUTE:
                    if (!executeSelectedItemInternal()) {
                        runQueuedOperation();
                        return;
                    }
                    return;
                case KEY_DOWN:
                    if (!keyDownSelectedItemInternal()) {
                        runQueuedOperation();
                        return;
                    }
                    return;
                case KEY_UP:
                    if (!keyUpSelectedItemInternal()) {
                        runQueuedOperation();
                        return;
                    }
                    return;
                default:
                    return;
            }
        } else {
            this.operationQueue.add(operation);
        }
    }

    @android.support.annotation.Nullable
    public com.navdy.hud.app.ui.component.ChoiceLayout.Choice getSelectedItem() {
        if (this.choices == null || this.selectedItem < 0 || this.selectedItem >= this.choices.size()) {
            return null;
        }
        return (com.navdy.hud.app.ui.component.ChoiceLayout.Choice) this.choices.get(this.selectedItem);
    }

    public int getSelectedItemIndex() {
        return this.selectedItem;
    }

    /* access modifiers changed from: private */
    public void runQueuedOperation() {
        if (this.operationQueue.size() > 0) {
            this.handler.post(new com.navdy.hud.app.ui.component.ChoiceLayout.Anon8((com.navdy.hud.app.ui.component.ChoiceLayout.Operation) this.operationQueue.remove()));
            return;
        }
        this.operationRunning = false;
    }

    public void clearOperationQueue() {
        if (this.animatorSet != null && this.animatorSet.isRunning()) {
            this.animatorSet.removeAllListeners();
            this.animatorSet.cancel();
            this.animatorSet = null;
        }
        this.operationQueue.clear();
        this.operationRunning = false;
    }
}
