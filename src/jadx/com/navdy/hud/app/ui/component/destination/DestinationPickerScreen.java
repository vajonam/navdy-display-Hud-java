package com.navdy.hud.app.ui.component.destination;

@flow.Layout(2130903110)
public class DestinationPickerScreen extends com.navdy.hud.app.screen.BaseScreen {
    private static final int CANCEL_POSITION = 1;
    /* access modifiers changed from: private */
    public static java.util.List<java.lang.String> CONFIRMATION_CHOICES = new java.util.ArrayList();
    private static final float MIN_DISTANCE_EXPANSION = 3000.0f;
    private static final float MIN_DISTANCE_THRESHOLD = 5000.0f;
    private static final int NAVIGATE_POSITION = 0;
    public static final java.lang.String PICKER_DESTINATIONS = "PICKER_DESTINATIONS";
    public static final java.lang.String PICKER_DESTINATION_ICON = "PICKER_DESTINATION_ICON";
    public static final java.lang.String PICKER_HIDE_IF_NAV_STOPS = "PICKER_HIDE";
    public static final java.lang.String PICKER_INITIAL_SELECTION = "PICKER_INITIAL_SELECTION";
    public static final java.lang.String PICKER_LEFT_ICON = "PICKER_LEFT_ICON";
    public static final java.lang.String PICKER_LEFT_ICON_BKCOLOR = "PICKER_LEFT_ICON_BKCOLOR";
    public static final java.lang.String PICKER_LEFT_TITLE = "PICKER_LEFT_TITLE";
    public static final java.lang.String PICKER_MAP_END_LAT = "PICKER_MAP_END_LAT";
    public static final java.lang.String PICKER_MAP_END_LNG = "PICKER_MAP_END_LNG";
    public static final java.lang.String PICKER_MAP_START_LAT = "PICKER_MAP_START_LAT";
    public static final java.lang.String PICKER_MAP_START_LNG = "PICKER_MAP_START_LNG";
    public static final java.lang.String PICKER_SHOW_DESTINATION_MAP = "PICKER_SHOW_DESTINATION_MAP";
    public static final java.lang.String PICKER_SHOW_ROUTE_MAP = "PICKER_SHOW_ROUTE_MAP";
    public static final java.lang.String PICKER_TITLE = "PICKER_TITLE";
    private static final int SELECTION_ANIMATION_DELAY = 1000;
    /* access modifiers changed from: private */
    public static com.navdy.hud.app.ui.component.destination.DestinationPickerScreen instance;
    private static final java.util.Map<com.navdy.service.library.events.places.PlaceType, com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.PlaceTypeResourceHolder> placeResMapping = new java.util.HashMap();
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.class);

    public static class DestinationPickerState {
        public boolean doNotAddOriginalRoute;
    }

    @dagger.Module(addsTo = com.navdy.hud.app.ui.activity.Main.Module.class, injects = {com.navdy.hud.app.ui.component.destination.DestinationPickerView.class})
    public class Module {
        public Module() {
        }
    }

    public static class PlaceTypeResourceHolder {
        public final int colorRes;
        public final int destinationIcon;
        public final int iconRes;
        public final int titleRes;

        private PlaceTypeResourceHolder(int iconRes2, int titleRes2, int colorRes2, int destinationIcon2) {
            this.iconRes = iconRes2;
            this.titleRes = titleRes2;
            this.colorRes = colorRes2;
            this.destinationIcon = destinationIcon2;
        }
    }

    @javax.inject.Singleton
    public static class Presenter extends com.navdy.hud.app.ui.framework.BasePresenter<com.navdy.hud.app.ui.component.destination.DestinationPickerView> {
        @javax.inject.Inject
        com.squareup.otto.Bus bus;
        private boolean closed;
        /* access modifiers changed from: private */
        public java.util.List<com.navdy.hud.app.ui.component.vlist.VerticalList.Model> data;
        private java.util.List<com.here.android.mpa.common.GeoBoundingBox> destinationGeoBBoxList;
        private java.util.List<com.here.android.mpa.common.GeoCoordinate> destinationGeoList;
        private int destinationIcon;
        com.navdy.hud.app.ui.component.destination.DestinationParcelable[] destinations;
        boolean doNotAddOriginalRoute;
        private com.here.android.mpa.common.GeoCoordinate end;
        /* access modifiers changed from: private */
        public boolean handledSelection;
        private boolean hideScreenOnNavStop;
        private com.navdy.hud.app.ui.component.homescreen.HomeScreenView homeScreenView;
        int initialSelection;
        int itemSelection;
        private int leftBkColor;
        private int leftIcon;
        private java.lang.String leftTitle;
        private com.navdy.hud.app.ui.component.homescreen.NavigationView navigationView;
        /* access modifiers changed from: private */
        public com.navdy.hud.app.ui.component.destination.IDestinationPicker pickerCallback;
        private boolean registered;
        private boolean showDestinationMap;
        /* access modifiers changed from: private */
        public boolean showRouteMap;
        private com.here.android.mpa.common.GeoCoordinate start;
        private com.navdy.hud.app.ui.component.homescreen.HomeScreen.DisplayMode switchBackMode;
        private java.lang.String title;

        class Anon1 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
            final /* synthetic */ java.lang.Runnable val$endAction;

            Anon1(java.lang.Runnable runnable) {
                this.val$endAction = runnable;
            }

            public void onAnimationEnd(android.animation.Animator animation) {
                com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.sLogger.v("post back");
                com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.Presenter.this.bus.post(new com.navdy.service.library.events.ui.ShowScreen.Builder().screen(com.navdy.service.library.events.ui.Screen.SCREEN_BACK).build());
                if (this.val$endAction != null) {
                    this.val$endAction.run();
                }
                if (com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.Presenter.this.pickerCallback != null) {
                    com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.Presenter.this.pickerCallback.onDestinationPickerClosed();
                }
            }
        }

        class Anon2 implements java.lang.Runnable {
            Anon2() {
            }

            public void run() {
                com.navdy.hud.app.maps.MapsNotification.showMapsEngineNotInitializedToast();
            }
        }

        class Anon3 implements java.lang.Runnable {
            final /* synthetic */ com.navdy.hud.app.ui.component.destination.DestinationParcelable val$dest;

            Anon3(com.navdy.hud.app.ui.component.destination.DestinationParcelable destinationParcelable) {
                this.val$dest = destinationParcelable;
            }

            public void run() {
                com.navdy.hud.app.framework.destinations.Destination d = new com.navdy.hud.app.framework.destinations.Destination.Builder().destinationTitle(this.val$dest.destinationTitle).destinationSubtitle(this.val$dest.destinationSubTitle).fullAddress(this.val$dest.destinationAddress).displayPositionLatitude(this.val$dest.displayLatitude).displayPositionLongitude(this.val$dest.displayLongitude).navigationPositionLatitude(this.val$dest.navLatitude).navigationPositionLongitude(this.val$dest.navLongitude).destinationType(com.navdy.hud.app.framework.destinations.Destination.DestinationType.DEFAULT).identifier(this.val$dest.identifier).favoriteDestinationType(com.navdy.hud.app.framework.destinations.Destination.FavoriteDestinationType.FAVORITE_NONE).destinationIcon(this.val$dest.icon).destinationIconBkColor(this.val$dest.iconSelectedColor).destinationPlaceId(this.val$dest.destinationPlaceId).placeType(this.val$dest.placeType).distanceStr(this.val$dest.distanceStr).contacts(this.val$dest.contacts).phoneNumbers(this.val$dest.phoneNumbers).build();
                com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.sLogger.v("call requestNavigation");
                com.navdy.hud.app.framework.destinations.DestinationsManager.getInstance().requestNavigationWithNavLookup(d);
            }
        }

        class Anon4 implements com.navdy.hud.app.ui.component.ChoiceLayout.IListener {
            final /* synthetic */ com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState val$selection;

            Anon4(com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState itemSelectionState) {
                this.val$selection = itemSelectionState;
            }

            public void executeItem(int pos, int id) {
                com.navdy.hud.app.ui.component.destination.DestinationPickerView view1 = (com.navdy.hud.app.ui.component.destination.DestinationPickerView) com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.Presenter.this.getView();
                if (view1 == null) {
                    com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.sLogger.v("not alive");
                    return;
                }
                switch (pos) {
                    case 0:
                        com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.sLogger.v("called presenter select");
                        com.navdy.hud.app.ui.component.destination.DestinationPickerView v = (com.navdy.hud.app.ui.component.destination.DestinationPickerView) com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.Presenter.this.getView();
                        if (v != null) {
                            com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.Presenter.this.performAction(v, this.val$selection.id, this.val$selection.pos);
                            return;
                        }
                        return;
                    case 1:
                        view1.confirmationLayout.setVisibility(8);
                        com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.Presenter.this.clearSelection();
                        view1.vmenuComponent.verticalList.unlock();
                        return;
                    default:
                        return;
                }
            }

            public void itemSelected(int pos, int id) {
            }
        }

        class Anon5 implements java.lang.Runnable {
            final /* synthetic */ int val$id;
            final /* synthetic */ int val$pos;

            Anon5(int i, int i2) {
                this.val$pos = i;
                this.val$id = i2;
            }

            public void run() {
                com.navdy.hud.app.ui.component.destination.DestinationPickerView view = (com.navdy.hud.app.ui.component.destination.DestinationPickerView) com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.Presenter.this.getView();
                if (view != null) {
                    if (com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.Presenter.this.data == null || this.val$pos >= com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.Presenter.this.data.size()) {
                        com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.sLogger.w("invalid list,no selection");
                        com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.Presenter.this.handledSelection = false;
                        return;
                    }
                    boolean override = false;
                    if (com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.Presenter.this.pickerCallback != null) {
                        com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.DestinationPickerState state = new com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.DestinationPickerState();
                        com.navdy.hud.app.ui.component.destination.DestinationParcelable destinationParcelable = (com.navdy.hud.app.ui.component.destination.DestinationParcelable) ((com.navdy.hud.app.ui.component.vlist.VerticalList.Model) com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.Presenter.this.data.get(this.val$pos)).state;
                        override = com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.Presenter.this.pickerCallback.onItemClicked((destinationParcelable == null || destinationParcelable.id == 0) ? this.val$id : destinationParcelable.id, this.val$pos - 1, state);
                        if (com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.Presenter.this.showRouteMap) {
                            com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.sLogger.v("map-route- doNotAddOriginalRoute=" + state.doNotAddOriginalRoute);
                            com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.Presenter.this.doNotAddOriginalRoute = state.doNotAddOriginalRoute;
                        }
                    }
                    if (override || com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.Presenter.this.isNotADestination(this.val$pos)) {
                        com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.sLogger.v("overridden");
                        com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.Presenter.this.close();
                    } else {
                        com.navdy.hud.app.ui.component.vlist.VerticalList.Model model = (com.navdy.hud.app.ui.component.vlist.VerticalList.Model) com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.Presenter.this.data.get(this.val$pos);
                        com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.sLogger.v("not overriden, launching pos:" + this.val$pos + " title=" + model.title);
                        com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.Presenter.this.launchDestination(view, (com.navdy.hud.app.ui.component.destination.DestinationParcelable) model.state);
                    }
                    com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.Presenter.this.handledSelection = true;
                }
            }
        }

        public void onLoad(android.os.Bundle savedInstanceState) {
            com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.sLogger.v("onLoad");
            com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().enableNotifications(false);
            com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().enablePhoneNotifications(true);
            clearState();
            reset();
            this.bus.register(this);
            this.registered = true;
            com.navdy.hud.app.ui.framework.UIStateManager uiStateManager = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getUiStateManager();
            this.navigationView = uiStateManager.getNavigationView();
            this.homeScreenView = uiStateManager.getHomescreenView();
            if (savedInstanceState != null) {
                this.leftTitle = savedInstanceState.getString(com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.PICKER_LEFT_TITLE);
                this.leftIcon = savedInstanceState.getInt(com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.PICKER_LEFT_ICON);
                this.leftBkColor = savedInstanceState.getInt(com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.PICKER_LEFT_ICON_BKCOLOR);
                this.initialSelection = savedInstanceState.getInt(com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.PICKER_INITIAL_SELECTION, 0);
                this.title = savedInstanceState.getString(com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.PICKER_TITLE);
                this.destinations = (com.navdy.hud.app.ui.component.destination.DestinationParcelable[]) savedInstanceState.getParcelableArray(com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.PICKER_DESTINATIONS);
                this.hideScreenOnNavStop = savedInstanceState.getBoolean(com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.PICKER_HIDE_IF_NAV_STOPS);
                this.showRouteMap = savedInstanceState.getBoolean(com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.PICKER_SHOW_ROUTE_MAP);
                if (this.showRouteMap) {
                    com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.sLogger.v("show route map");
                    double lat = savedInstanceState.getDouble(com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.PICKER_MAP_START_LAT);
                    double lng = savedInstanceState.getDouble(com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.PICKER_MAP_START_LNG);
                    if (lat == 0.0d || lng == 0.0d) {
                        this.start = null;
                    } else {
                        this.start = new com.here.android.mpa.common.GeoCoordinate(lat, lng);
                    }
                    double lat2 = savedInstanceState.getDouble(com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.PICKER_MAP_END_LAT);
                    double lng2 = savedInstanceState.getDouble(com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.PICKER_MAP_END_LNG);
                    if (lat2 == 0.0d || lng2 == 0.0d) {
                        this.end = null;
                    } else {
                        this.end = new com.here.android.mpa.common.GeoCoordinate(lat2, lng2);
                    }
                } else {
                    this.showDestinationMap = savedInstanceState.getBoolean(com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.PICKER_SHOW_DESTINATION_MAP);
                    if (this.showDestinationMap) {
                        this.destinationIcon = savedInstanceState.getInt(com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.PICKER_DESTINATION_ICON, -1);
                        com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.sLogger.v("show dest map");
                        buildDestinationBBox();
                    }
                    this.start = com.navdy.hud.app.maps.here.HereMapsManager.getInstance().getLocationFixManager().getLastGeoCoordinate();
                    com.navdy.service.library.events.navigation.NavigationRouteRequest request = com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().getCurrentNavigationRouteRequest();
                    if (request != null) {
                        this.end = new com.here.android.mpa.common.GeoCoordinate(request.destination.latitude.doubleValue(), request.destination.longitude.doubleValue());
                    } else {
                        this.end = null;
                    }
                }
                if (this.showRouteMap || this.showDestinationMap) {
                    this.switchBackMode = this.homeScreenView.getDisplayMode();
                    if (this.switchBackMode == com.navdy.hud.app.ui.component.homescreen.HomeScreen.DisplayMode.MAP) {
                        com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.sLogger.v("switchbackmode: null");
                    } else {
                        com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.sLogger.v(" onLoad switchbackmode: " + this.switchBackMode);
                        this.homeScreenView.setDisplayMode(com.navdy.hud.app.ui.component.homescreen.HomeScreen.DisplayMode.MAP);
                    }
                }
            }
            if (com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.instance != null) {
                this.pickerCallback = (com.navdy.hud.app.ui.component.destination.IDestinationPicker) com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.instance.arguments2;
                com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.instance = null;
            } else {
                this.pickerCallback = null;
            }
            updateView();
            super.onLoad(savedInstanceState);
        }

        private void updateView() {
            com.navdy.hud.app.ui.component.vlist.VerticalList.Model destinationModel;
            com.here.android.mpa.common.GeoBoundingBox boundingBox;
            com.here.android.mpa.common.GeoBoundingBox boundingBox2;
            com.navdy.hud.app.ui.component.destination.DestinationPickerView view = (com.navdy.hud.app.ui.component.destination.DestinationPickerView) getView();
            if (view != null) {
                android.content.res.Resources resources = view.getResources();
                if (this.showRouteMap || this.showDestinationMap) {
                    view.vmenuComponent.leftContainer.removeAllViews();
                    view.vmenuComponent.leftContainer.addView(android.view.LayoutInflater.from(view.getContext()).inflate(com.navdy.hud.app.R.layout.screen_destination_picker_content, view, false));
                    view.vmenuComponent.setLeftContainerWidth(resources.getDimensionPixelSize(com.navdy.hud.app.R.dimen.dest_picker_map_left_container_w));
                    view.vmenuComponent.setRightContainerWidth(resources.getDimensionPixelSize(com.navdy.hud.app.R.dimen.dest_picker_map_right_container_w));
                    view.vmenuComponent.rightContainer.setBackgroundColor(-16777216);
                    view.vmenuComponent.closeContainer.setBackgroundColor(-16777216);
                    view.setBackgroundColor(-16777216);
                    view.rightBackground.setVisibility(0);
                    if (this.showRouteMap) {
                        java.lang.String routeId = this.destinations[0].routeId;
                        com.here.android.mpa.routing.Route route = null;
                        if (routeId != null) {
                            com.navdy.hud.app.maps.here.HereRouteCache.RouteInfo routeInfo = com.navdy.hud.app.maps.here.HereRouteCache.getInstance().getRoute(routeId);
                            if (routeInfo != null) {
                                route = routeInfo.route;
                                boundingBox = routeInfo.route.getBoundingBox();
                                if (boundingBox == null && this.start != null) {
                                    boundingBox2 = new com.here.android.mpa.common.GeoBoundingBox(this.start, com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.MIN_DISTANCE_THRESHOLD, com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.MIN_DISTANCE_THRESHOLD);
                                    this.navigationView.switchToRouteSearchMode(this.start, this.end, boundingBox2, route, false, null, -1);
                                }
                                boundingBox2 = boundingBox;
                                this.navigationView.switchToRouteSearchMode(this.start, this.end, boundingBox2, route, false, null, -1);
                            }
                        }
                        boundingBox = null;
                        try {
                            boundingBox2 = new com.here.android.mpa.common.GeoBoundingBox(this.start, com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.MIN_DISTANCE_THRESHOLD, com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.MIN_DISTANCE_THRESHOLD);
                        } catch (Throwable t) {
                            com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.sLogger.e(t);
                        }
                        this.navigationView.switchToRouteSearchMode(this.start, this.end, boundingBox2, route, false, null, -1);
                    } else {
                        this.navigationView.switchToRouteSearchMode(this.start, this.end, (com.here.android.mpa.common.GeoBoundingBox) this.destinationGeoBBoxList.get(0), com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().getCurrentRoute(), false, this.destinationGeoList, this.destinationIcon);
                    }
                } else {
                    view.vmenuComponent.setLeftContainerWidth(resources.getDimensionPixelSize(com.navdy.hud.app.R.dimen.dest_picker_left_container_w));
                    view.vmenuComponent.setRightContainerWidth(resources.getDimensionPixelSize(com.navdy.hud.app.R.dimen.dest_picker_right_container_w));
                }
                if (this.leftTitle != null) {
                    view.vmenuComponent.selectedText.setText(this.leftTitle);
                }
                if (this.leftIcon != 0) {
                    if (this.leftBkColor != 0) {
                        view.vmenuComponent.setSelectedIconColorImage(this.leftIcon, this.leftBkColor, null, 1.0f);
                    } else {
                        view.vmenuComponent.setSelectedIconImage(this.leftIcon, null, com.navdy.hud.app.ui.component.image.InitialsImageView.Style.DEFAULT);
                    }
                }
                if (this.destinations == null || this.destinations.length == 0) {
                    com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.sLogger.w("no destinations");
                    this.bus.post(new com.navdy.service.library.events.ui.ShowScreen.Builder().screen(com.navdy.service.library.events.ui.Screen.SCREEN_BACK).build());
                    return;
                }
                com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.sLogger.v("title:" + this.title);
                com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.sLogger.v("destinations:" + this.destinations.length);
                java.util.ArrayList arrayList = new java.util.ArrayList();
                arrayList.add(com.navdy.hud.app.ui.component.vlist.viewholder.TitleViewHolder.buildModel(this.title));
                for (int i = 0; i < this.destinations.length; i++) {
                    if (this.destinations[i].iconUnselected == 0) {
                        destinationModel = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(this.destinations[i].id, this.destinations[i].icon, this.destinations[i].iconSelectedColor, this.destinations[i].iconDeselectedColor, this.destinations[i].iconSelectedColor, this.destinations[i].destinationTitle, this.destinations[i].destinationSubTitle, this.destinations[i].destinationSubTitle2);
                    } else {
                        destinationModel = com.navdy.hud.app.ui.component.vlist.viewholder.IconsTwoViewHolder.buildModel(this.destinations[i].id, this.destinations[i].icon, this.destinations[i].iconUnselected, this.destinations[i].iconSelectedColor, this.destinations[i].iconDeselectedColor, this.destinations[i].destinationTitle, this.destinations[i].destinationSubTitle, this.destinations[i].destinationSubTitle2);
                    }
                    destinationModel.subTitleFormatted = this.destinations[i].destinationSubTitleFormatted;
                    destinationModel.subTitle2Formatted = this.destinations[i].destinationSubTitle2Formatted;
                    destinationModel.state = this.destinations[i];
                    arrayList.add(destinationModel);
                }
                this.data = arrayList;
                view.vmenuComponent.updateView(this.data, this.initialSelection, false);
            }
        }

        public void onUnload() {
            boolean addRoute;
            if (!this.doNotAddOriginalRoute) {
                addRoute = true;
            } else {
                addRoute = false;
            }
            if (!this.showRouteMap) {
                addRoute = true;
            }
            com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.sLogger.v("onUnload addbackRoute:" + addRoute);
            com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.instance = null;
            clearState();
            this.navigationView.switchBackfromRouteSearchMode(addRoute);
            if (this.switchBackMode != null) {
                com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.sLogger.v("onUnload switchbackmode: " + this.switchBackMode);
                this.homeScreenView.setDisplayMode(this.switchBackMode);
            }
            com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().enableNotifications(true);
            this.pickerCallback = null;
            if (this.registered) {
                this.registered = false;
                this.bus.unregister(this);
            }
            reset();
            super.onUnload();
        }

        /* access modifiers changed from: 0000 */
        public void clearState() {
            this.initialSelection = 0;
            this.title = null;
            this.destinations = null;
            this.showRouteMap = false;
            this.showDestinationMap = false;
            this.destinationIcon = -1;
            this.switchBackMode = null;
            this.destinationGeoList = null;
            this.destinationGeoBBoxList = null;
            this.itemSelection = -1;
            this.start = null;
            this.end = null;
            this.data = null;
            this.pickerCallback = null;
            this.doNotAddOriginalRoute = false;
        }

        /* access modifiers changed from: 0000 */
        public void reset() {
            this.closed = false;
            this.handledSelection = false;
            this.data = null;
            this.hideScreenOnNavStop = false;
        }

        /* access modifiers changed from: 0000 */
        public void clearSelection() {
            this.closed = false;
            this.handledSelection = false;
        }

        /* access modifiers changed from: 0000 */
        public boolean itemClicked(com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState selection) {
            com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.sLogger.v("itemClicked:" + selection.id + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA + selection.pos);
            if (this.handledSelection) {
                com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.sLogger.v("already handled [" + selection.id + "], " + selection.pos);
                return true;
            }
            this.handledSelection = true;
            com.navdy.hud.app.ui.component.destination.DestinationPickerView view = (com.navdy.hud.app.ui.component.destination.DestinationPickerView) getView();
            if (view == null) {
                return true;
            }
            if (this.showDestinationMap && !isNotADestination(selection.pos)) {
                com.navdy.hud.app.maps.here.HereNavigationManager hereNavigationManager = com.navdy.hud.app.maps.here.HereNavigationManager.getInstance();
                if (hereNavigationManager.isNavigationModeOn() && !hereNavigationManager.hasArrived()) {
                    showConfirmation(view, selection);
                    return true;
                }
            }
            performAction(view, selection.id, selection.pos);
            return this.handledSelection;
        }

        /* access modifiers changed from: 0000 */
        public boolean isShowingRouteMap() {
            return this.showRouteMap;
        }

        /* access modifiers changed from: 0000 */
        public boolean isAlive() {
            return this.registered;
        }

        /* access modifiers changed from: 0000 */
        public boolean isShowDestinationMap() {
            return this.showDestinationMap;
        }

        /* access modifiers changed from: 0000 */
        public void startMapFluctuator() {
            if (this.registered && this.navigationView != null) {
                this.navigationView.cleanupFluctuator();
                this.navigationView.startFluctuator();
            }
        }

        /* access modifiers changed from: 0000 */
        public void itemSelected(com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState selection) {
            if (this.data != null) {
                com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.sLogger.v("itemSelected:" + selection.id + " , " + selection.pos);
                if (this.pickerCallback != null) {
                    if (this.pickerCallback.onItemSelected(selection.id, selection.pos - 1, new com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.DestinationPickerState())) {
                        com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.sLogger.v("overridden onItemSelected : " + selection.id + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA + selection.pos);
                        return;
                    }
                }
                if (isNotADestination(selection.pos)) {
                    this.navigationView.changeMarkerSelection(-1, this.itemSelection);
                    this.itemSelection = -1;
                    return;
                }
                itemSelected(selection.pos - 1);
            }
        }

        /* access modifiers changed from: 0000 */
        public void itemSelected(int pos) {
            com.navdy.hud.app.analytics.AnalyticsSupport.recordNearbySearchDestinationScroll(pos);
            if (this.showRouteMap) {
                java.lang.String routeId = this.destinations[pos].routeId;
                com.navdy.hud.app.maps.here.HereRouteCache.RouteInfo routeInfo = com.navdy.hud.app.maps.here.HereRouteCache.getInstance().getRoute(routeId);
                if (routeInfo != null) {
                    this.navigationView.zoomToBoundBox(routeInfo.route.getBoundingBox(), routeInfo.route, true, true);
                    return;
                }
                com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.sLogger.v("itemSelected: route not found:" + routeId);
            } else if (this.showDestinationMap) {
                this.navigationView.changeMarkerSelection(pos, this.itemSelection);
                com.here.android.mpa.common.GeoBoundingBox boundingBox = (com.here.android.mpa.common.GeoBoundingBox) this.destinationGeoBBoxList.get(pos);
                if (boundingBox != null) {
                    this.navigationView.zoomToBoundBox(boundingBox, null, false, true);
                }
                this.itemSelection = pos;
            }
        }

        /* access modifiers changed from: private */
        public boolean isNotADestination(int pos) {
            com.navdy.hud.app.ui.component.destination.DestinationParcelable destination = (com.navdy.hud.app.ui.component.destination.DestinationParcelable) ((com.navdy.hud.app.ui.component.vlist.VerticalList.Model) this.data.get(pos)).state;
            return (destination == null || destination.type == com.navdy.hud.app.ui.component.destination.DestinationParcelable.DestinationType.DESTINATION) ? false : true;
        }

        /* access modifiers changed from: 0000 */
        public void close() {
            close(null);
        }

        /* access modifiers changed from: 0000 */
        public void close(java.lang.Runnable endAction) {
            if (this.closed) {
                com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.sLogger.v("already closed");
                return;
            }
            if (this.showRouteMap || this.showDestinationMap) {
                com.navdy.hud.app.ui.component.destination.DestinationPickerView view = (com.navdy.hud.app.ui.component.destination.DestinationPickerView) getView();
                if (view != null) {
                    view.setBackgroundColor(-16777216);
                }
                if (this.navigationView != null) {
                    this.navigationView.setMapMaskVisibility(0);
                }
            }
            com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().enableNotifications(true);
            this.closed = true;
            com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.sLogger.v("close");
            com.navdy.hud.app.ui.component.destination.DestinationPickerView view2 = (com.navdy.hud.app.ui.component.destination.DestinationPickerView) getView();
            if (view2 != null) {
                view2.vmenuComponent.animateOut(new com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.Presenter.Anon1(endAction));
            }
        }

        /* access modifiers changed from: 0000 */
        public boolean isClosed() {
            return this.closed;
        }

        /* access modifiers changed from: private */
        public void launchDestination(com.navdy.hud.app.ui.component.destination.DestinationPickerView view, com.navdy.hud.app.ui.component.destination.DestinationParcelable dest) {
            com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.sLogger.v("launchDestination {" + dest.destinationTitle + "} display {" + dest.displayLatitude + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA + dest.displayLongitude + "}" + " nav {" + dest.navLatitude + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA + dest.navLongitude + "}");
            if (!com.navdy.hud.app.maps.here.HereMapsManager.getInstance().isInitialized()) {
                com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.sLogger.w("Here maps engine not initialized, exit");
                close(new com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.Presenter.Anon2());
                return;
            }
            close(new com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.Presenter.Anon3(dest));
        }

        private void buildDestinationBBox() {
            java.util.ArrayList<com.here.android.mpa.common.GeoCoordinate> list = new java.util.ArrayList<>();
            com.here.android.mpa.common.GeoCoordinate current = com.navdy.hud.app.maps.here.HereMapsManager.getInstance().getLocationFixManager().getLastGeoCoordinate();
            com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.sLogger.v("destinationbbox-ce:" + current);
            this.destinationGeoList = new java.util.ArrayList();
            this.destinationGeoBBoxList = new java.util.ArrayList();
            for (int i = 0; i < this.destinations.length; i++) {
                if (this.destinations[i].displayLatitude == 0.0d && this.destinations[i].displayLongitude == 0.0d) {
                    this.destinationGeoList.add(null);
                    this.destinationGeoBBoxList.add(null);
                } else {
                    list.clear();
                    if (current != null) {
                        list.add(current);
                    }
                    com.here.android.mpa.common.GeoCoordinate geoCoordinate = new com.here.android.mpa.common.GeoCoordinate(this.destinations[i].displayLatitude, this.destinations[i].displayLongitude);
                    list.add(geoCoordinate);
                    com.here.android.mpa.common.GeoBoundingBox bbox = com.here.android.mpa.common.GeoBoundingBox.getBoundingBoxContainingGeoCoordinates(list);
                    if (current != null && ((float) ((long) current.distanceTo(geoCoordinate))) <= com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.MIN_DISTANCE_THRESHOLD) {
                        try {
                            bbox.expand(com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.MIN_DISTANCE_EXPANSION, com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.MIN_DISTANCE_EXPANSION);
                        } catch (Throwable t) {
                            com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.sLogger.e("expand bbox", t);
                        }
                    }
                    this.destinationGeoList.add(geoCoordinate);
                    this.destinationGeoBBoxList.add(bbox);
                }
            }
        }

        @com.squareup.otto.Subscribe
        public void onNavigationModeChanged(com.navdy.hud.app.maps.MapEvents.NavigationModeChange event) {
            if (this.hideScreenOnNavStop && ((com.navdy.hud.app.ui.component.destination.DestinationPickerView) getView()) != null) {
                com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.sLogger.v("onNavigationModeChanged event[" + event.navigationMode + "]");
                if (event.navigationMode == com.navdy.hud.app.maps.NavigationMode.MAP) {
                    close();
                }
            }
        }

        private void showConfirmation(com.navdy.hud.app.ui.component.destination.DestinationPickerView view, com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState selection) {
            com.navdy.hud.app.ui.component.destination.DestinationConfirmationHelper.configure(view.confirmationLayout, (com.navdy.hud.app.ui.component.destination.DestinationParcelable) ((com.navdy.hud.app.ui.component.vlist.VerticalList.Model) this.data.get(selection.pos)).state);
            view.confirmationLayout.setChoices(com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.CONFIRMATION_CHOICES, 0, new com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.Presenter.Anon4(selection));
            view.confirmationLayout.setVisibility(0);
        }

        /* access modifiers changed from: private */
        public void performAction(com.navdy.hud.app.ui.component.destination.DestinationPickerView view, int id, int pos) {
            com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.sLogger.v("performAction {" + id + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA + pos + "}");
            com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().enableNotifications(true);
            if (this.showRouteMap || this.showDestinationMap) {
                view.setBackgroundColor(-16777216);
                if (this.navigationView != null) {
                    this.navigationView.setMapMaskVisibility(0);
                }
            }
            view.vmenuComponent.performSelectionAnimation(new com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.Presenter.Anon5(pos, id), 1000);
        }
    }

    static {
        android.content.res.Resources resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        CONFIRMATION_CHOICES.add(resources.getString(com.navdy.hud.app.R.string.destination_start_navigation_yes));
        CONFIRMATION_CHOICES.add(resources.getString(com.navdy.hud.app.R.string.destination_start_navigation_cancel));
        placeResMapping.put(com.navdy.service.library.events.places.PlaceType.PLACE_TYPE_GAS, new com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.PlaceTypeResourceHolder(com.navdy.hud.app.R.drawable.icon_place_gas, com.navdy.hud.app.R.string.gas, com.navdy.hud.app.R.color.mm_search_gas, com.navdy.hud.app.R.drawable.icon_pin_dot_destination_indigo));
        placeResMapping.put(com.navdy.service.library.events.places.PlaceType.PLACE_TYPE_PARKING, new com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.PlaceTypeResourceHolder(com.navdy.hud.app.R.drawable.icon_place_parking, com.navdy.hud.app.R.string.parking, com.navdy.hud.app.R.color.mm_search_parking, com.navdy.hud.app.R.drawable.icon_pin_dot_destination_indigo));
        placeResMapping.put(com.navdy.service.library.events.places.PlaceType.PLACE_TYPE_RESTAURANT, new com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.PlaceTypeResourceHolder(com.navdy.hud.app.R.drawable.icon_place_restaurant, com.navdy.hud.app.R.string.food, com.navdy.hud.app.R.color.mm_search_food, com.navdy.hud.app.R.drawable.icon_pin_dot_destination_orange));
        placeResMapping.put(com.navdy.service.library.events.places.PlaceType.PLACE_TYPE_STORE, new com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.PlaceTypeResourceHolder(com.navdy.hud.app.R.drawable.icon_place_store, com.navdy.hud.app.R.string.grocery_store, com.navdy.hud.app.R.color.mm_search_grocery_store, com.navdy.hud.app.R.drawable.icon_pin_dot_destination_blue));
        placeResMapping.put(com.navdy.service.library.events.places.PlaceType.PLACE_TYPE_COFFEE, new com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.PlaceTypeResourceHolder(com.navdy.hud.app.R.drawable.icon_place_coffee, com.navdy.hud.app.R.string.coffee, com.navdy.hud.app.R.color.mm_search_coffee, com.navdy.hud.app.R.drawable.icon_pin_dot_destination_orange));
        placeResMapping.put(com.navdy.service.library.events.places.PlaceType.PLACE_TYPE_ATM, new com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.PlaceTypeResourceHolder(com.navdy.hud.app.R.drawable.icon_place_a_t_m, com.navdy.hud.app.R.string.atm, com.navdy.hud.app.R.color.mm_search_atm, com.navdy.hud.app.R.drawable.icon_pin_dot_destination_blue));
        placeResMapping.put(com.navdy.service.library.events.places.PlaceType.PLACE_TYPE_HOSPITAL, new com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.PlaceTypeResourceHolder(com.navdy.hud.app.R.drawable.icon_place_hospital, com.navdy.hud.app.R.string.hospital, com.navdy.hud.app.R.color.mm_search_hospital, com.navdy.hud.app.R.drawable.icon_pin_dot_destination_red));
    }

    public DestinationPickerScreen() {
        instance = this;
    }

    public java.lang.String getMortarScopeName() {
        return getClass().getName();
    }

    public java.lang.Object getDaggerModule() {
        return new com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.Module();
    }

    public com.navdy.service.library.events.ui.Screen getScreen() {
        return com.navdy.service.library.events.ui.Screen.SCREEN_DESTINATION_PICKER;
    }

    public int getAnimationIn(flow.Flow.Direction direction) {
        return -1;
    }

    public int getAnimationOut(flow.Flow.Direction direction) {
        return -1;
    }

    public static com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.PlaceTypeResourceHolder getPlaceTypeHolder(com.navdy.service.library.events.places.PlaceType placeType) {
        return (com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.PlaceTypeResourceHolder) placeResMapping.get(placeType);
    }
}
