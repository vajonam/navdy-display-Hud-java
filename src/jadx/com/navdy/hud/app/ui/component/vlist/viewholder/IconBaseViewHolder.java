package com.navdy.hud.app.ui.component.vlist.viewholder;

public abstract class IconBaseViewHolder extends com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder {
    public static final int FLUCTUATOR_OPACITY_ALPHA = 153;
    public static final int HALO_DELAY_START_DURATION = 100;
    private static final int[] location = new int[2];
    private static final com.navdy.service.library.log.Logger sLogger = com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.sLogger;
    protected android.animation.AnimatorSet.Builder animatorSetBuilder;
    protected com.navdy.hud.app.ui.component.image.CrossFadeImageView crossFadeImageView;
    /* access modifiers changed from: private */
    public java.lang.Runnable fluctuatorRunnable = new com.navdy.hud.app.ui.component.vlist.viewholder.IconBaseViewHolder.Anon2();
    private com.navdy.hud.app.ui.framework.DefaultAnimationListener fluctuatorStartListener = new com.navdy.hud.app.ui.component.vlist.viewholder.IconBaseViewHolder.Anon1();
    protected com.navdy.hud.app.ui.component.HaloView haloView;
    private boolean hasIconFluctuatorColor;
    private boolean hasSubTitle;
    private boolean hasSubTitle2;
    protected android.view.ViewGroup iconContainer;
    protected boolean iconScaleAnimationDisabled;
    protected android.view.ViewGroup imageContainer;
    protected android.widget.TextView subTitle;
    protected android.widget.TextView subTitle2;
    protected boolean textAnimationDisabled;
    protected android.widget.TextView title;
    private float titleSelectedTopMargin;
    private float titleUnselectedScale;

    class Anon1 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
        Anon1() {
        }

        public void onAnimationEnd(android.animation.Animator animation) {
            com.navdy.hud.app.ui.component.vlist.viewholder.IconBaseViewHolder.this.itemAnimatorSet = null;
            com.navdy.hud.app.ui.component.vlist.viewholder.IconBaseViewHolder.this.handler.removeCallbacks(com.navdy.hud.app.ui.component.vlist.viewholder.IconBaseViewHolder.this.fluctuatorRunnable);
            com.navdy.hud.app.ui.component.vlist.viewholder.IconBaseViewHolder.this.handler.postDelayed(com.navdy.hud.app.ui.component.vlist.viewholder.IconBaseViewHolder.this.fluctuatorRunnable, 100);
        }
    }

    class Anon2 implements java.lang.Runnable {
        Anon2() {
        }

        public void run() {
            com.navdy.hud.app.ui.component.vlist.viewholder.IconBaseViewHolder.this.startFluctuator();
        }
    }

    class Anon3 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.hud.app.ui.component.vlist.VerticalList.Model val$model;
        final /* synthetic */ int val$pos;

        Anon3(com.navdy.hud.app.ui.component.vlist.VerticalList.Model model, int i) {
            this.val$model = model;
            this.val$pos = i;
        }

        public void run() {
            com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState selectionState = com.navdy.hud.app.ui.component.vlist.viewholder.IconBaseViewHolder.this.vlist.getItemSelectionState();
            selectionState.set(this.val$model, this.val$model.id, this.val$pos, -1, -1);
            com.navdy.hud.app.ui.component.vlist.viewholder.IconBaseViewHolder.this.vlist.performSelectAction(selectionState);
        }
    }

    static android.view.ViewGroup getLayout(android.view.ViewGroup parent, int lytId, int subLytId) {
        android.view.LayoutInflater inflater = android.view.LayoutInflater.from(parent.getContext());
        android.view.ViewGroup layout = (android.view.ViewGroup) inflater.inflate(lytId, parent, false);
        android.view.ViewGroup iconContainer2 = (android.view.ViewGroup) layout.findViewById(com.navdy.hud.app.R.id.iconContainer);
        com.navdy.hud.app.ui.component.image.CrossFadeImageView image = (com.navdy.hud.app.ui.component.image.CrossFadeImageView) inflater.inflate(subLytId, iconContainer2, false);
        image.inject(com.navdy.hud.app.ui.component.image.CrossFadeImageView.Mode.SMALL);
        image.setId(com.navdy.hud.app.R.id.vlist_image);
        iconContainer2.addView(image);
        return layout;
    }

    public IconBaseViewHolder(android.view.ViewGroup layout, com.navdy.hud.app.ui.component.vlist.VerticalList vlist, android.os.Handler handler) {
        super(layout, vlist, handler);
        this.imageContainer = (android.view.ViewGroup) layout.findViewById(com.navdy.hud.app.R.id.imageContainer);
        this.iconContainer = (android.view.ViewGroup) layout.findViewById(com.navdy.hud.app.R.id.iconContainer);
        this.haloView = (com.navdy.hud.app.ui.component.HaloView) layout.findViewById(com.navdy.hud.app.R.id.halo);
        this.haloView.setVisibility(8);
        this.crossFadeImageView = (com.navdy.hud.app.ui.component.image.CrossFadeImageView) layout.findViewById(com.navdy.hud.app.R.id.vlist_image);
        this.title = (android.widget.TextView) layout.findViewById(com.navdy.hud.app.R.id.title);
        this.subTitle = (android.widget.TextView) layout.findViewById(com.navdy.hud.app.R.id.subTitle);
        this.subTitle2 = (android.widget.TextView) layout.findViewById(com.navdy.hud.app.R.id.subTitle2);
    }

    public void setItemState(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.State state, com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.AnimationType animation, int duration, boolean startFluctuator) {
        float titleScaleFactor = 0.0f;
        float titleTopMargin = 0.0f;
        float subTitleAlpha = 0.0f;
        float subTitleScaleFactor = 0.0f;
        float subTitle2Alpha = 0.0f;
        float subTitle2ScaleFactor = 0.0f;
        com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.State itemState = state;
        this.animatorSetBuilder = null;
        if (this.textAnimationDisabled) {
            if (animation == com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.AnimationType.MOVE) {
                this.itemAnimatorSet = new android.animation.AnimatorSet();
                this.animatorSetBuilder = this.itemAnimatorSet.play(android.animation.ValueAnimator.ofFloat(new float[]{0.0f, 1.0f}));
                return;
            }
            itemState = com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.State.SELECTED;
        }
        switch (itemState) {
            case SELECTED:
                titleScaleFactor = 1.0f;
                titleTopMargin = this.titleSelectedTopMargin;
                subTitleAlpha = 1.0f;
                subTitleScaleFactor = 1.0f;
                subTitle2Alpha = 1.0f;
                subTitle2ScaleFactor = 1.0f;
                break;
            case UNSELECTED:
                titleScaleFactor = this.titleUnselectedScale;
                titleTopMargin = 0.0f;
                subTitleAlpha = 0.0f;
                subTitleScaleFactor = this.titleUnselectedScale;
                subTitle2Alpha = 0.0f;
                subTitle2ScaleFactor = this.titleUnselectedScale;
                break;
        }
        if (!this.hasSubTitle) {
            titleTopMargin = 0.0f;
            subTitleAlpha = 0.0f;
            subTitle2Alpha = 0.0f;
        }
        this.title.setPivotX(0.0f);
        this.title.setPivotY(titleHeight / 2.0f);
        switch (animation) {
            case INIT:
            case NONE:
                this.title.setScaleX(titleScaleFactor);
                this.title.setScaleY(titleScaleFactor);
                ((android.view.ViewGroup.MarginLayoutParams) this.title.getLayoutParams()).topMargin = (int) titleTopMargin;
                this.title.requestLayout();
                break;
            case MOVE:
                this.itemAnimatorSet = new android.animation.AnimatorSet();
                android.animation.PropertyValuesHolder p1 = android.animation.PropertyValuesHolder.ofFloat(android.view.View.SCALE_X, new float[]{titleScaleFactor});
                android.animation.PropertyValuesHolder p2 = android.animation.PropertyValuesHolder.ofFloat(android.view.View.SCALE_Y, new float[]{titleScaleFactor});
                this.animatorSetBuilder = this.itemAnimatorSet.play(android.animation.ObjectAnimator.ofPropertyValuesHolder(this.title, new android.animation.PropertyValuesHolder[]{p1, p2}));
                this.animatorSetBuilder.with(com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils.animateMargin(this.title, (int) titleTopMargin));
                break;
        }
        this.subTitle.setPivotX(0.0f);
        this.subTitle.setPivotY(subTitleHeight / 2.0f);
        com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.AnimationType animation2 = animation;
        if (!this.hasSubTitle) {
            animation2 = com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.AnimationType.NONE;
        }
        switch (animation2) {
            case INIT:
            case NONE:
                this.subTitle.setAlpha(subTitleAlpha);
                this.subTitle.setScaleX(subTitleScaleFactor);
                this.subTitle.setScaleY(subTitleScaleFactor);
                break;
            case MOVE:
                android.animation.PropertyValuesHolder p12 = android.animation.PropertyValuesHolder.ofFloat(android.view.View.SCALE_X, new float[]{subTitleScaleFactor});
                android.animation.PropertyValuesHolder p22 = android.animation.PropertyValuesHolder.ofFloat(android.view.View.SCALE_Y, new float[]{subTitleScaleFactor});
                android.animation.PropertyValuesHolder p3 = android.animation.PropertyValuesHolder.ofFloat(android.view.View.ALPHA, new float[]{subTitleAlpha});
                this.animatorSetBuilder.with(android.animation.ObjectAnimator.ofPropertyValuesHolder(this.subTitle, new android.animation.PropertyValuesHolder[]{p12, p22, p3}));
                break;
        }
        this.subTitle2.setPivotX(0.0f);
        this.subTitle2.setPivotY(subTitleHeight / 2.0f);
        com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.AnimationType animation22 = animation;
        if (!this.hasSubTitle2) {
            animation22 = com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.AnimationType.NONE;
        }
        switch (animation22) {
            case INIT:
            case NONE:
                this.subTitle2.setAlpha(subTitle2Alpha);
                this.subTitle2.setScaleX(subTitle2ScaleFactor);
                this.subTitle2.setScaleY(subTitle2ScaleFactor);
                break;
            case MOVE:
                android.animation.PropertyValuesHolder p13 = android.animation.PropertyValuesHolder.ofFloat(android.view.View.SCALE_X, new float[]{subTitle2ScaleFactor});
                android.animation.PropertyValuesHolder p23 = android.animation.PropertyValuesHolder.ofFloat(android.view.View.SCALE_Y, new float[]{subTitle2ScaleFactor});
                android.animation.PropertyValuesHolder p32 = android.animation.PropertyValuesHolder.ofFloat(android.view.View.ALPHA, new float[]{subTitleAlpha});
                this.animatorSetBuilder.with(android.animation.ObjectAnimator.ofPropertyValuesHolder(this.subTitle2, new android.animation.PropertyValuesHolder[]{p13, p23, p32}));
                break;
        }
        if (itemState == com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.State.SELECTED && startFluctuator) {
            if (this.itemAnimatorSet != null) {
                this.itemAnimatorSet.addListener(this.fluctuatorStartListener);
            } else {
                startFluctuator();
            }
        }
    }

    public void clearAnimation() {
        stopFluctuator();
        stopAnimation();
        this.currentState = null;
        this.layout.setVisibility(0);
        this.layout.setAlpha(1.0f);
    }

    private void setTitle(java.lang.String str) {
        this.title.setText(str);
    }

    private void setSubTitle(java.lang.String str, boolean formatted) {
        if (str == null) {
            this.subTitle.setText("");
            this.hasSubTitle = false;
            return;
        }
        int c = getSubTitleColor();
        if (c == 0) {
            this.subTitle.setTextColor(subTitleColor);
        } else {
            this.subTitle.setTextColor(c);
        }
        if (formatted) {
            this.subTitle.setText(android.text.Html.fromHtml(str));
        } else {
            this.subTitle.setText(str);
        }
        this.hasSubTitle = true;
    }

    private void setSubTitle2(java.lang.String str, boolean formatted) {
        if (str == null) {
            this.subTitle2.setText("");
            this.subTitle2.setVisibility(8);
            this.hasSubTitle2 = false;
            return;
        }
        int c = getSubTitle2Color();
        if (c == 0) {
            this.subTitle2.setTextColor(subTitle2Color);
        } else {
            this.subTitle2.setTextColor(c);
        }
        if (formatted) {
            this.subTitle2.setText(android.text.Html.fromHtml(str));
        } else {
            this.subTitle2.setText(str);
        }
        this.subTitle2.setVisibility(0);
        this.hasSubTitle2 = true;
    }

    private void setIconFluctuatorColor(int color) {
        if (color != 0) {
            int fluctuatorColor = android.graphics.Color.argb(FLUCTUATOR_OPACITY_ALPHA, android.graphics.Color.red(color), android.graphics.Color.green(color), android.graphics.Color.blue(color));
            this.hasIconFluctuatorColor = true;
            this.haloView.setStrokeColor(fluctuatorColor);
            return;
        }
        this.hasIconFluctuatorColor = false;
    }

    public void startFluctuator() {
        if (this.hasIconFluctuatorColor) {
            this.haloView.setVisibility(0);
            this.haloView.start();
        }
    }

    private void stopFluctuator() {
        if (this.hasIconFluctuatorColor) {
            this.handler.removeCallbacks(this.fluctuatorRunnable);
            this.haloView.setVisibility(8);
            this.haloView.stop();
        }
    }

    public void select(com.navdy.hud.app.ui.component.vlist.VerticalList.Model model, int pos, int duration) {
        if (this.hasIconFluctuatorColor) {
            this.haloView.setVisibility(4);
            this.haloView.stop();
        }
        com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils.performClick(this.iconContainer, duration, new com.navdy.hud.app.ui.component.vlist.viewholder.IconBaseViewHolder.Anon3(model, pos));
    }

    private int getSubTitleColor() {
        if (this.extras == null) {
            return 0;
        }
        return getTextColor(com.navdy.hud.app.ui.component.vlist.VerticalList.Model.SUBTITLE_COLOR);
    }

    private int getSubTitle2Color() {
        if (this.extras == null) {
            return 0;
        }
        return getTextColor(com.navdy.hud.app.ui.component.vlist.VerticalList.Model.SUBTITLE_2_COLOR);
    }

    private int getTextColor(java.lang.String property) {
        int i = 0;
        if (this.extras == null) {
            return i;
        }
        java.lang.String str = (java.lang.String) this.extras.get(property);
        if (str == null) {
            return i;
        }
        try {
            return java.lang.Integer.parseInt(str);
        } catch (java.lang.NumberFormatException e) {
            return i;
        }
    }

    public void copyAndPosition(android.widget.ImageView imageC, android.widget.TextView titleC, android.widget.TextView subTitleC, android.widget.TextView subTitle2C, boolean setImage) {
        android.view.View view = this.crossFadeImageView.getBig();
        if (setImage) {
            com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils.copyImage((android.widget.ImageView) view, imageC);
        }
        imageC.setX((float) selectedImageX);
        imageC.setY((float) selectedImageY);
        titleC.setText(this.title.getText());
        titleC.setX((float) selectedTextX);
        this.title.getLocationOnScreen(location);
        titleC.setY((float) (location[1] - rootTopOffset));
        if (this.hasSubTitle) {
            subTitleC.setText(this.subTitle.getText());
            subTitleC.setX((float) selectedTextX);
            this.subTitle.getLocationOnScreen(location);
            subTitleC.setY((float) (location[1] - rootTopOffset));
        } else {
            subTitleC.setText("");
        }
        if (this.hasSubTitle2) {
            subTitle2C.setText(this.subTitle2.getText());
            subTitle2C.setX((float) selectedTextX);
            this.subTitle2.getLocationOnScreen(location);
            subTitle2C.setY((float) (location[1] - rootTopOffset));
            return;
        }
        subTitle2C.setText("");
    }

    public void preBind(com.navdy.hud.app.ui.component.vlist.VerticalList.Model model, com.navdy.hud.app.ui.component.vlist.VerticalList.ModelState modelState) {
        ((android.view.ViewGroup.MarginLayoutParams) this.title.getLayoutParams()).topMargin = (int) model.fontInfo.titleFontTopMargin;
        this.title.setTextSize(model.fontInfo.titleFontSize);
        setMultiLineStyles(this.title, model.fontInfo.titleSingleLine, -1);
        this.title.setLineSpacing(0.0f, 0.9f);
        this.titleSelectedTopMargin = (float) ((int) model.fontInfo.titleFontTopMargin);
        ((android.view.ViewGroup.MarginLayoutParams) this.subTitle.getLayoutParams()).topMargin = (int) model.fontInfo.subTitleFontTopMargin;
        setMultiLineStyles(this.subTitle, !model.subTitle_2Lines, model.subTitle_2Lines ? com.navdy.hud.app.R.style.vlist_subtitle_2_line : com.navdy.hud.app.R.style.vlist_subtitle);
        this.subTitle.setTextSize(model.fontInfo.subTitleFontSize);
        ((android.view.ViewGroup.MarginLayoutParams) this.subTitle2.getLayoutParams()).topMargin = (int) model.fontInfo.subTitle2FontTopMargin;
        this.subTitle2.setTextSize(model.fontInfo.subTitle2FontSize);
        this.titleUnselectedScale = model.fontInfo.titleScale;
        this.crossFadeImageView.setSmallAlpha(-1.0f);
    }

    private void setMultiLineStyles(android.widget.TextView view, boolean singleLine, int style) {
        if (style != -1) {
            view.setTextAppearance(view.getContext(), style);
        }
        view.setSingleLine(singleLine);
        if (singleLine) {
            view.setEllipsize(null);
            return;
        }
        view.setMaxLines(2);
        view.setEllipsize(android.text.TextUtils.TruncateAt.END);
    }

    public void bind(com.navdy.hud.app.ui.component.vlist.VerticalList.Model model, com.navdy.hud.app.ui.component.vlist.VerticalList.ModelState modelState) {
        if (modelState.updateTitle) {
            setTitle(model.title);
        }
        if (modelState.updateSubTitle) {
            if (model.subTitle != null) {
                setSubTitle(model.subTitle, model.subTitleFormatted);
            } else {
                setSubTitle(null, false);
            }
        }
        if (modelState.updateSubTitle2) {
            if (model.subTitle2 != null) {
                setSubTitle2(model.subTitle2, model.subTitle2Formatted);
            } else {
                setSubTitle2(null, false);
            }
        }
        this.textAnimationDisabled = model.noTextAnimation;
        this.iconScaleAnimationDisabled = model.noImageScaleAnimation;
        setIconFluctuatorColor(model.iconFluctuatorColor);
    }
}
