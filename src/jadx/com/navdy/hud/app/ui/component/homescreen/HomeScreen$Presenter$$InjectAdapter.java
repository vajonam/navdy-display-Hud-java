package com.navdy.hud.app.ui.component.homescreen;

public final class HomeScreen$Presenter$$InjectAdapter extends dagger.internal.Binding<com.navdy.hud.app.ui.component.homescreen.HomeScreen.Presenter> implements javax.inject.Provider<com.navdy.hud.app.ui.component.homescreen.HomeScreen.Presenter>, dagger.MembersInjector<com.navdy.hud.app.ui.component.homescreen.HomeScreen.Presenter> {
    private dagger.internal.Binding<com.squareup.otto.Bus> bus;
    private dagger.internal.Binding<com.navdy.hud.app.ui.framework.BasePresenter> supertype;

    public HomeScreen$Presenter$$InjectAdapter() {
        super("com.navdy.hud.app.ui.component.homescreen.HomeScreen$Presenter", "members/com.navdy.hud.app.ui.component.homescreen.HomeScreen$Presenter", true, com.navdy.hud.app.ui.component.homescreen.HomeScreen.Presenter.class);
    }

    public void attach(dagger.internal.Linker linker) {
        this.bus = linker.requestBinding("com.squareup.otto.Bus", com.navdy.hud.app.ui.component.homescreen.HomeScreen.Presenter.class, getClass().getClassLoader());
        dagger.internal.Linker linker2 = linker;
        this.supertype = linker2.requestBinding("members/com.navdy.hud.app.ui.framework.BasePresenter", com.navdy.hud.app.ui.component.homescreen.HomeScreen.Presenter.class, getClass().getClassLoader(), false, true);
    }

    public void getDependencies(java.util.Set<dagger.internal.Binding<?>> set, java.util.Set<dagger.internal.Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.bus);
        injectMembersBindings.add(this.supertype);
    }

    public com.navdy.hud.app.ui.component.homescreen.HomeScreen.Presenter get() {
        com.navdy.hud.app.ui.component.homescreen.HomeScreen.Presenter result = new com.navdy.hud.app.ui.component.homescreen.HomeScreen.Presenter();
        injectMembers(result);
        return result;
    }

    public void injectMembers(com.navdy.hud.app.ui.component.homescreen.HomeScreen.Presenter object) {
        object.bus = (com.squareup.otto.Bus) this.bus.get();
        this.supertype.injectMembers(object);
    }
}
