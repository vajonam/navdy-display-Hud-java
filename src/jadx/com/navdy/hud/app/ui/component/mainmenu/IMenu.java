package com.navdy.hud.app.ui.component.mainmenu;

public interface IMenu {

    public enum Menu {
        MAIN,
        SETTINGS,
        PLACES,
        RECENT_PLACES,
        CONTACTS,
        RECENT_CONTACTS,
        CONTACT_OPTIONS,
        MAIN_OPTIONS,
        MUSIC,
        SEARCH,
        REPORT_ISSUE,
        ACTIVE_TRIP,
        SYSTEM_INFO,
        TEST_FAST_SCROLL_MENU,
        NUMBER_PICKER,
        MESSAGE_PICKER
    }

    public enum MenuLevel {
        BACK_TO_PARENT,
        SUB_LEVEL,
        CLOSE,
        REFRESH_CURRENT,
        ROOT
    }

    com.navdy.hud.app.ui.component.mainmenu.IMenu getChildMenu(com.navdy.hud.app.ui.component.mainmenu.IMenu iMenu, java.lang.String str, java.lang.String str2);

    int getInitialSelection();

    java.util.List<com.navdy.hud.app.ui.component.vlist.VerticalList.Model> getItems();

    com.navdy.hud.app.ui.component.vlist.VerticalList.Model getModelfromPos(int i);

    com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex getScrollIndex();

    com.navdy.hud.app.ui.component.mainmenu.IMenu.Menu getType();

    boolean isBindCallsEnabled();

    boolean isFirstItemEmpty();

    boolean isItemClickable(int i, int i2);

    void onBindToView(com.navdy.hud.app.ui.component.vlist.VerticalList.Model model, android.view.View view, int i, com.navdy.hud.app.ui.component.vlist.VerticalList.ModelState modelState);

    void onFastScrollEnd();

    void onFastScrollStart();

    void onItemSelected(com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState itemSelectionState);

    void onScrollIdle();

    void onUnload(com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel menuLevel);

    boolean selectItem(com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState itemSelectionState);

    void setBackSelectionId(int i);

    void setBackSelectionPos(int i);

    void setSelectedIcon();

    void showToolTip();
}
