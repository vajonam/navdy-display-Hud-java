package com.navdy.hud.app.ui.component.tbt;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u0000 %2\u00020\u0001:\u0001%B\u000f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004B\u0017\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007B\u001f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\b\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0006\u0010\u0017\u001a\u00020\u0018J\u001a\u0010\u0019\u001a\u00020\u00182\u0006\u0010\u001a\u001a\u00020\f2\n\u0010\u001b\u001a\u00060\u001cR\u00020\u001dJ\b\u0010\u001e\u001a\u00020\u0018H\u0014J\u0012\u0010\u001f\u001a\u00020\u00182\b\u0010\u001a\u001a\u0004\u0018\u00010\fH\u0002J\u000e\u0010 \u001a\u00020\u00182\u0006\u0010\u001a\u001a\u00020\fJ\u0010\u0010!\u001a\u00020\u00182\u0006\u0010\u001a\u001a\u00020\fH\u0002J\u000e\u0010\"\u001a\u00020\u00182\u0006\u0010#\u001a\u00020$R\u0010\u0010\u000b\u001a\u0004\u0018\u00010\fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\r\u001a\u0004\u0018\u00010\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000f\u001a\u0004\u0018\u00010\u0010X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0011\u001a\u0004\u0018\u00010\u0012X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0013\u001a\u0004\u0018\u00010\u0014X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0015\u001a\u0004\u0018\u00010\u0016X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006&"}, d2 = {"Lcom/navdy/hud/app/ui/component/tbt/TbtView;", "Landroid/widget/FrameLayout;", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "attrs", "Landroid/util/AttributeSet;", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "defStyleAttr", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "currentMode", "Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;", "directionView", "Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView;", "distanceView", "Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;", "groupContainer", "Landroid/support/constraint/ConstraintLayout;", "tbtNextManeuverView", "Lcom/navdy/hud/app/ui/component/tbt/TbtNextManeuverView;", "textInstructionView", "Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;", "clear", "", "getCustomAnimator", "mode", "mainBuilder", "Landroid/animation/AnimatorSet$Builder;", "Landroid/animation/AnimatorSet;", "onFinishInflate", "setConstraints", "setView", "setViewAttrs", "updateDisplay", "event", "Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;", "Companion", "app_hudDebug"}, k = 1, mv = {1, 1, 6})
/* compiled from: TbtView.kt */
public final class TbtView extends android.widget.FrameLayout {
    public static final com.navdy.hud.app.ui.component.tbt.TbtView.Companion Companion = new com.navdy.hud.app.ui.component.tbt.TbtView.Companion(null);
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger("TbtView");
    /* access modifiers changed from: private */
    public static final int paddingFull = Companion.getResources().getDimensionPixelSize(com.navdy.hud.app.R.dimen.tbt_padding_full);
    /* access modifiers changed from: private */
    public static final int paddingMedium = Companion.getResources().getDimensionPixelSize(com.navdy.hud.app.R.dimen.tbt_padding_medium);
    /* access modifiers changed from: private */
    public static final android.content.res.Resources resources;
    /* access modifiers changed from: private */
    public static final float shrinkLeftX = Companion.getResources().getDimension(com.navdy.hud.app.R.dimen.tbt_shrinkleft_x);
    private java.util.HashMap _$_findViewCache;
    private com.navdy.hud.app.view.MainView.CustomAnimationMode currentMode;
    private com.navdy.hud.app.ui.component.tbt.TbtDirectionView directionView;
    private com.navdy.hud.app.ui.component.tbt.TbtDistanceView distanceView;
    private android.support.constraint.ConstraintLayout groupContainer;
    private com.navdy.hud.app.ui.component.tbt.TbtNextManeuverView tbtNextManeuverView;
    private com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView textInstructionView;

    @kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0007\n\u0002\b\u0003\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u0011\u0010\u0007\u001a\u00020\b\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0011\u0010\u000b\u001a\u00020\b\u00a2\u0006\b\n\u0000\u001a\u0004\b\f\u0010\nR\u0014\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0011\u0010\u0011\u001a\u00020\u0012\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014\u00a8\u0006\u0015"}, d2 = {"Lcom/navdy/hud/app/ui/component/tbt/TbtView$Companion;", "", "()V", "logger", "Lcom/navdy/service/library/log/Logger;", "getLogger", "()Lcom/navdy/service/library/log/Logger;", "paddingFull", "", "getPaddingFull", "()I", "paddingMedium", "getPaddingMedium", "resources", "Landroid/content/res/Resources;", "getResources", "()Landroid/content/res/Resources;", "shrinkLeftX", "", "getShrinkLeftX", "()F", "app_hudDebug"}, k = 1, mv = {1, 1, 6})
    /* compiled from: TbtView.kt */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(kotlin.jvm.internal.DefaultConstructorMarker $constructor_marker) {
            this();
        }

        private final com.navdy.service.library.log.Logger getLogger() {
            return com.navdy.hud.app.ui.component.tbt.TbtView.logger;
        }

        /* access modifiers changed from: private */
        public final android.content.res.Resources getResources() {
            return com.navdy.hud.app.ui.component.tbt.TbtView.resources;
        }

        public final int getPaddingFull() {
            return com.navdy.hud.app.ui.component.tbt.TbtView.paddingFull;
        }

        public final int getPaddingMedium() {
            return com.navdy.hud.app.ui.component.tbt.TbtView.paddingMedium;
        }

        public final float getShrinkLeftX() {
            return com.navdy.hud.app.ui.component.tbt.TbtView.shrinkLeftX;
        }
    }

    public void _$_clearFindViewByIdCache() {
        if (this._$_findViewCache != null) {
            this._$_findViewCache.clear();
        }
    }

    public android.view.View _$_findCachedViewById(int i) {
        if (this._$_findViewCache == null) {
            this._$_findViewCache = new java.util.HashMap();
        }
        android.view.View view = (android.view.View) this._$_findViewCache.get(java.lang.Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        android.view.View findViewById = findViewById(i);
        this._$_findViewCache.put(java.lang.Integer.valueOf(i), findViewById);
        return findViewById;
    }

    static {
        android.content.res.Resources resources2 = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(resources2, "HudApplication.getAppContext().resources");
        resources = resources2;
    }

    public TbtView(@org.jetbrains.annotations.NotNull android.content.Context context) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(context, "context");
        super(context);
    }

    public TbtView(@org.jetbrains.annotations.NotNull android.content.Context context, @org.jetbrains.annotations.NotNull android.util.AttributeSet attrs) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(context, "context");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(attrs, "attrs");
        super(context, attrs);
    }

    public TbtView(@org.jetbrains.annotations.NotNull android.content.Context context, @org.jetbrains.annotations.NotNull android.util.AttributeSet attrs, int defStyleAttr) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(context, "context");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(attrs, "attrs");
        super(context, attrs, defStyleAttr);
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
        android.view.View findViewById = findViewById(com.navdy.hud.app.R.id.groupContainer);
        if (findViewById == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type android.support.constraint.ConstraintLayout");
        }
        this.groupContainer = (android.support.constraint.ConstraintLayout) findViewById;
        android.view.View findViewById2 = findViewById(com.navdy.hud.app.R.id.tbtDistanceView);
        if (findViewById2 == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type com.navdy.hud.app.ui.component.tbt.TbtDistanceView");
        }
        this.distanceView = (com.navdy.hud.app.ui.component.tbt.TbtDistanceView) findViewById2;
        android.view.View findViewById3 = findViewById(com.navdy.hud.app.R.id.tbtDirectionView);
        if (findViewById3 == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type com.navdy.hud.app.ui.component.tbt.TbtDirectionView");
        }
        this.directionView = (com.navdy.hud.app.ui.component.tbt.TbtDirectionView) findViewById3;
        android.view.View findViewById4 = findViewById(com.navdy.hud.app.R.id.tbtTextInstructionView);
        if (findViewById4 == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView");
        }
        this.textInstructionView = (com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView) findViewById4;
        android.view.View findViewById5 = findViewById(com.navdy.hud.app.R.id.tbtNextManeuverView);
        if (findViewById5 == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type com.navdy.hud.app.ui.component.tbt.TbtNextManeuverView");
        }
        this.tbtNextManeuverView = (com.navdy.hud.app.ui.component.tbt.TbtNextManeuverView) findViewById5;
    }

    public final void clear() {
        com.navdy.hud.app.ui.component.tbt.TbtDistanceView tbtDistanceView = this.distanceView;
        if (tbtDistanceView != null) {
            tbtDistanceView.clear();
        }
        com.navdy.hud.app.ui.component.tbt.TbtDirectionView tbtDirectionView = this.directionView;
        if (tbtDirectionView != null) {
            tbtDirectionView.clear();
        }
        com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView tbtTextInstructionView = this.textInstructionView;
        if (tbtTextInstructionView != null) {
            tbtTextInstructionView.clear();
        }
        com.navdy.hud.app.ui.component.tbt.TbtNextManeuverView tbtNextManeuverView2 = this.tbtNextManeuverView;
        if (tbtNextManeuverView2 != null) {
            tbtNextManeuverView2.clear();
        }
    }

    public final void setView(@org.jetbrains.annotations.NotNull com.navdy.hud.app.view.MainView.CustomAnimationMode mode) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(mode, net.hockeyapp.android.LoginActivity.EXTRA_MODE);
        setViewAttrs(mode);
        switch (mode) {
            case EXPAND:
                setX(0.0f);
                return;
            case SHRINK_LEFT:
                setX(Companion.getShrinkLeftX());
                return;
            default:
                return;
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 2 */
    public final void getCustomAnimator(@org.jetbrains.annotations.NotNull com.navdy.hud.app.view.MainView.CustomAnimationMode mode, @org.jetbrains.annotations.NotNull android.animation.AnimatorSet.Builder mainBuilder) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(mode, net.hockeyapp.android.LoginActivity.EXTRA_MODE);
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(mainBuilder, "mainBuilder");
        setViewAttrs(mode);
        switch (mode) {
            case EXPAND:
                mainBuilder.with(com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getXPositionAnimator(this, 0.0f));
                return;
            case SHRINK_LEFT:
                mainBuilder.with(com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getXPositionAnimator(this, Companion.getShrinkLeftX()));
                return;
            default:
                return;
        }
    }

    private final void setViewAttrs(com.navdy.hud.app.view.MainView.CustomAnimationMode mode) {
        com.navdy.hud.app.ui.component.tbt.TbtDistanceView tbtDistanceView = this.distanceView;
        if (tbtDistanceView != null) {
            tbtDistanceView.setMode(mode);
        }
        com.navdy.hud.app.ui.component.tbt.TbtDirectionView tbtDirectionView = this.directionView;
        if (tbtDirectionView != null) {
            tbtDirectionView.setMode(mode);
        }
        com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView tbtTextInstructionView = this.textInstructionView;
        if (tbtTextInstructionView != null) {
            tbtTextInstructionView.setMode(mode);
        }
        com.navdy.hud.app.ui.component.tbt.TbtNextManeuverView tbtNextManeuverView2 = this.tbtNextManeuverView;
        if (tbtNextManeuverView2 != null) {
            tbtNextManeuverView2.setMode(mode);
        }
        setConstraints(mode);
        this.currentMode = mode;
    }

    public final void updateDisplay(@org.jetbrains.annotations.NotNull com.navdy.hud.app.maps.MapEvents.ManeuverDisplay event) {
        java.lang.Integer num;
        java.lang.Integer num2;
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(event, "event");
        com.navdy.hud.app.ui.component.tbt.TbtDistanceView tbtDistanceView = this.distanceView;
        if (tbtDistanceView != null) {
            tbtDistanceView.updateDisplay(event);
        }
        com.navdy.hud.app.ui.component.tbt.TbtDirectionView tbtDirectionView = this.directionView;
        if (tbtDirectionView != null) {
            tbtDirectionView.updateDisplay(event);
        }
        com.navdy.hud.app.ui.component.tbt.TbtNextManeuverView tbtNextManeuverView2 = this.tbtNextManeuverView;
        if (tbtNextManeuverView2 != null) {
            num = java.lang.Integer.valueOf(tbtNextManeuverView2.getVisibility());
        } else {
            num = null;
        }
        boolean visibleBefore = kotlin.jvm.internal.Intrinsics.areEqual((java.lang.Object) num, (java.lang.Object) java.lang.Integer.valueOf(0));
        com.navdy.hud.app.ui.component.tbt.TbtNextManeuverView tbtNextManeuverView3 = this.tbtNextManeuverView;
        if (tbtNextManeuverView3 != null) {
            tbtNextManeuverView3.updateDisplay(event);
        }
        com.navdy.hud.app.ui.component.tbt.TbtNextManeuverView tbtNextManeuverView4 = this.tbtNextManeuverView;
        if (tbtNextManeuverView4 != null) {
            num2 = java.lang.Integer.valueOf(tbtNextManeuverView4.getVisibility());
        } else {
            num2 = null;
        }
        boolean visibleAfter = kotlin.jvm.internal.Intrinsics.areEqual((java.lang.Object) num2, (java.lang.Object) java.lang.Integer.valueOf(0));
        if (visibleBefore != visibleAfter) {
            com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView tbtTextInstructionView = this.textInstructionView;
            if (tbtTextInstructionView != null) {
                tbtTextInstructionView.updateDisplay(event, true, visibleAfter);
            }
            setConstraints(this.currentMode);
            return;
        }
        com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView tbtTextInstructionView2 = this.textInstructionView;
        if (tbtTextInstructionView2 != null) {
            com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView.updateDisplay$default(tbtTextInstructionView2, event, false, false, 6, null);
        }
    }

    private final void setConstraints(com.navdy.hud.app.view.MainView.CustomAnimationMode mode) {
        java.lang.Integer num;
        int padding;
        int i;
        int i2;
        int i3;
        if (mode != null) {
            com.navdy.hud.app.ui.component.tbt.TbtNextManeuverView tbtNextManeuverView2 = this.tbtNextManeuverView;
            if (tbtNextManeuverView2 != null) {
                num = java.lang.Integer.valueOf(tbtNextManeuverView2.getVisibility());
            } else {
                num = null;
            }
            boolean addNextManeuver = kotlin.jvm.internal.Intrinsics.areEqual((java.lang.Object) num, (java.lang.Object) java.lang.Integer.valueOf(0));
            switch (mode) {
                case EXPAND:
                    if (!addNextManeuver) {
                        padding = Companion.getPaddingFull();
                        break;
                    } else {
                        padding = Companion.getPaddingMedium();
                        break;
                    }
                default:
                    padding = Companion.getPaddingMedium();
                    break;
            }
            android.view.ViewGroup.LayoutParams layoutParams = ((com.navdy.hud.app.ui.component.tbt.TbtDirectionView) findViewById(com.navdy.hud.app.R.id.tbtDirectionView)).getLayoutParams();
            if (layoutParams == null) {
                throw new kotlin.TypeCastException("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
            }
            ((android.view.ViewGroup.MarginLayoutParams) layoutParams).leftMargin = padding;
            android.view.ViewGroup.LayoutParams layoutParams2 = ((com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView) findViewById(com.navdy.hud.app.R.id.tbtTextInstructionView)).getLayoutParams();
            if (layoutParams2 == null) {
                throw new kotlin.TypeCastException("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
            }
            ((android.view.ViewGroup.MarginLayoutParams) layoutParams2).leftMargin = padding;
            if (addNextManeuver) {
                com.navdy.hud.app.ui.component.tbt.TbtNextManeuverView tbtNextManeuverView3 = this.tbtNextManeuverView;
                android.view.ViewGroup.LayoutParams layoutParams3 = tbtNextManeuverView3 != null ? tbtNextManeuverView3.getLayoutParams() : null;
                if (layoutParams3 == null) {
                    throw new kotlin.TypeCastException("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
                }
                ((android.view.ViewGroup.MarginLayoutParams) layoutParams3).leftMargin = Companion.getPaddingMedium() * 2;
            }
            android.support.constraint.ConstraintSet constraintSet = new android.support.constraint.ConstraintSet();
            constraintSet.clone(this.groupContainer);
            com.navdy.hud.app.ui.component.tbt.TbtDirectionView tbtDirectionView = this.directionView;
            int i4 = tbtDirectionView != null ? tbtDirectionView.getId() : 0;
            com.navdy.hud.app.ui.component.tbt.TbtDistanceView tbtDistanceView = this.distanceView;
            constraintSet.connect(i4, 1, tbtDistanceView != null ? tbtDistanceView.getId() : 0, 2, 0);
            com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView tbtTextInstructionView = this.textInstructionView;
            if (tbtTextInstructionView != null) {
                i = tbtTextInstructionView.getId();
            } else {
                i = 0;
            }
            com.navdy.hud.app.ui.component.tbt.TbtDirectionView tbtDirectionView2 = this.directionView;
            constraintSet.connect(i, 1, tbtDirectionView2 != null ? tbtDirectionView2.getId() : 0, 2, 0);
            if (addNextManeuver) {
                com.navdy.hud.app.ui.component.tbt.TbtNextManeuverView tbtNextManeuverView4 = this.tbtNextManeuverView;
                if (tbtNextManeuverView4 != null) {
                    i2 = tbtNextManeuverView4.getId();
                } else {
                    i2 = 0;
                }
                com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView tbtTextInstructionView2 = this.textInstructionView;
                if (tbtTextInstructionView2 != null) {
                    i3 = tbtTextInstructionView2.getId();
                } else {
                    i3 = 0;
                }
                constraintSet.connect(i2, 1, i3, 2, 0);
            }
            constraintSet.applyTo(this.groupContainer);
            invalidate();
            requestLayout();
        }
    }
}
