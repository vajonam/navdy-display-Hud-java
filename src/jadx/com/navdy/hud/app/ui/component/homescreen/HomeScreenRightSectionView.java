package com.navdy.hud.app.ui.component.homescreen;

public class HomeScreenRightSectionView extends android.widget.FrameLayout {
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.homescreen.HomeScreenRightSectionView.class);

    public HomeScreenRightSectionView(android.content.Context context) {
        super(context);
    }

    public HomeScreenRightSectionView(android.content.Context context, android.util.AttributeSet attrs) {
        super(context, attrs);
    }

    public HomeScreenRightSectionView(android.content.Context context, android.util.AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void updateLayoutForMode(com.navdy.hud.app.maps.NavigationMode navigationMode, com.navdy.hud.app.ui.component.homescreen.HomeScreenView homeScreenView) {
        ((android.view.ViewGroup.MarginLayoutParams) getLayoutParams()).height = homeScreenView.isNavigationActive() ? com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeMapHeight : com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.openMapHeight;
        requestLayout();
    }

    public void injectRightSection(android.view.View view) {
        sLogger.v("injectRightSection");
        removeAllViews();
        addView(view);
    }

    public void ejectRightSection() {
        sLogger.v("ejectRightSection");
        removeAllViews();
    }

    public boolean isViewVisible() {
        return getX() < ((float) com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.mainScreenRightSectionX);
    }

    public boolean hasView() {
        return getChildCount() > 0;
    }

    public void setView(com.navdy.hud.app.view.MainView.CustomAnimationMode mode) {
        sLogger.v("setview: " + mode);
        setX((float) com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.mainScreenRightSectionX);
    }

    public void getCustomAnimator(com.navdy.hud.app.view.MainView.CustomAnimationMode mode, android.animation.AnimatorSet.Builder mainBuilder) {
        switch (mode) {
            case SHRINK_MODE:
                mainBuilder.with(com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getXPositionAnimator(this, (float) com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.mainScreenRightSectionShrinkX));
                return;
            case EXPAND:
                mainBuilder.with(com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getXPositionAnimator(this, (float) com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.mainScreenRightSectionX));
                return;
            default:
                return;
        }
    }
}
