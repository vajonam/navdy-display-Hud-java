package com.navdy.hud.app.ui.component.homescreen;

public class SmartDashView$$ViewInjector {
    public static void inject(butterknife.ButterKnife.Finder finder, com.navdy.hud.app.ui.component.homescreen.SmartDashView target, java.lang.Object source) {
        target.mLeftGaugeViewPager = (com.navdy.hud.app.ui.component.dashboard.GaugeViewPager) finder.findRequiredView(source, com.navdy.hud.app.R.id.left_gauge, "field 'mLeftGaugeViewPager'");
        target.mRightGaugeViewPager = (com.navdy.hud.app.ui.component.dashboard.GaugeViewPager) finder.findRequiredView(source, com.navdy.hud.app.R.id.right_gauge, "field 'mRightGaugeViewPager'");
        target.mMiddleGaugeView = (com.navdy.hud.app.view.GaugeView) finder.findRequiredView(source, com.navdy.hud.app.R.id.middle_gauge, "field 'mMiddleGaugeView'");
        target.mEtaLayout = (android.view.ViewGroup) finder.findRequiredView(source, com.navdy.hud.app.R.id.eta_layout, "field 'mEtaLayout'");
        target.etaText = (android.widget.TextView) finder.findRequiredView(source, com.navdy.hud.app.R.id.eta_time, "field 'etaText'");
        target.etaAmPm = (android.widget.TextView) finder.findRequiredView(source, com.navdy.hud.app.R.id.eta_time_amPm, "field 'etaAmPm'");
    }

    public static void reset(com.navdy.hud.app.ui.component.homescreen.SmartDashView target) {
        target.mLeftGaugeViewPager = null;
        target.mRightGaugeViewPager = null;
        target.mMiddleGaugeView = null;
        target.mEtaLayout = null;
        target.etaText = null;
        target.etaAmPm = null;
    }
}
