package com.navdy.hud.app.ui.component.carousel;

public class ShrinkAnimator extends com.navdy.hud.app.ui.component.carousel.CarouselAnimator {
    public android.animation.Animator createViewOutAnimation(com.navdy.hud.app.ui.component.carousel.CarouselLayout carousel, com.navdy.hud.app.ui.component.carousel.AnimationStrategy.Direction direction) {
        android.view.View target;
        float newX;
        if (direction == com.navdy.hud.app.ui.component.carousel.AnimationStrategy.Direction.RIGHT) {
            target = carousel.rightView;
            newX = ((target.getX() + ((float) target.getMeasuredWidth())) + ((float) (carousel.viewPadding / 2))) - (20.0f / 2.0f);
        } else {
            target = carousel.leftView;
            newX = (target.getX() - ((float) (carousel.viewPadding / 2))) - (20.0f / 2.0f);
        }
        float newY = (target.getY() + ((float) (target.getMeasuredHeight() / 2))) - (20.0f / 2.0f);
        float scaleFactor = 20.0f / ((float) target.getMeasuredWidth());
        android.animation.AnimatorSet animatorSet = new android.animation.AnimatorSet();
        animatorSet.playTogether(new android.animation.Animator[]{android.animation.ObjectAnimator.ofFloat(target, "x", new float[]{newX}), android.animation.ObjectAnimator.ofFloat(target, "y", new float[]{newY}), android.animation.ObjectAnimator.ofFloat(target, "scaleX", new float[]{scaleFactor}), android.animation.ObjectAnimator.ofFloat(target, "scaleY", new float[]{scaleFactor})});
        target.setPivotX(0.5f);
        target.setPivotY(0.5f);
        return animatorSet;
    }

    public android.animation.AnimatorSet createHiddenViewAnimation(com.navdy.hud.app.ui.component.carousel.CarouselLayout carousel, com.navdy.hud.app.ui.component.carousel.AnimationStrategy.Direction direction) {
        android.view.View srcView;
        android.view.View targetPosView;
        int i;
        int measuredWidth;
        android.animation.AnimatorSet animatorSet = new android.animation.AnimatorSet();
        if (direction == com.navdy.hud.app.ui.component.carousel.AnimationStrategy.Direction.RIGHT) {
            srcView = carousel.newLeftView;
            targetPosView = carousel.leftView;
        } else {
            srcView = carousel.newRightView;
            targetPosView = carousel.rightView;
        }
        float scaleFactor = 20.0f / ((float) targetPosView.getMeasuredWidth());
        srcView.setScaleX(scaleFactor);
        srcView.setScaleY(scaleFactor);
        float x = targetPosView.getX();
        if (direction == com.navdy.hud.app.ui.component.carousel.AnimationStrategy.Direction.RIGHT) {
            i = -1;
        } else {
            i = 1;
        }
        float f = x + ((float) ((i * carousel.viewPadding) / 2));
        if (direction == com.navdy.hud.app.ui.component.carousel.AnimationStrategy.Direction.RIGHT) {
            measuredWidth = 0;
        } else {
            measuredWidth = targetPosView.getMeasuredWidth();
        }
        srcView.setX(((float) measuredWidth) + f);
        srcView.setY((targetPosView.getY() + ((float) (targetPosView.getMeasuredHeight() / 2))) - (20.0f / 2.0f));
        animatorSet.playTogether(new android.animation.Animator[]{android.animation.ObjectAnimator.ofFloat(srcView, "x", new float[]{targetPosView.getX()}), android.animation.ObjectAnimator.ofFloat(srcView, "y", new float[]{targetPosView.getY()}), android.animation.ObjectAnimator.ofFloat(srcView, "scaleX", new float[]{1.0f}), android.animation.ObjectAnimator.ofFloat(srcView, "scaleY", new float[]{1.0f})});
        srcView.setPivotX(0.5f);
        srcView.setPivotY(0.5f);
        return animatorSet;
    }
}
