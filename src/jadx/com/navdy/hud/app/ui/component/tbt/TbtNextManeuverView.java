package com.navdy.hud.app.ui.component.tbt;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000X\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u0000 !2\u00020\u0001:\u0001!B\u000f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004B\u0017\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007B\u001f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\b\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0006\u0010\u0014\u001a\u00020\u0015J\u001a\u0010\u0016\u001a\u00020\u00152\u0006\u0010\u0017\u001a\u00020\f2\n\u0010\u0018\u001a\u00060\u0019R\u00020\u001aJ\b\u0010\u001b\u001a\u00020\u0015H\u0014J\u000e\u0010\u001c\u001a\u00020\u00152\u0006\u0010\u0017\u001a\u00020\fJ\u000e\u0010\u001d\u001a\u00020\u00152\u0006\u0010\u001e\u001a\u00020\u001fJ\b\u0010 \u001a\u00020\u0015H\u0002R\u0010\u0010\u000b\u001a\u0004\u0018\u00010\fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\r\u001a\u0004\u0018\u00010\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0010\u001a\u0004\u0018\u00010\u0011X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0012\u001a\u0004\u0018\u00010\u0013X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\""}, d2 = {"Lcom/navdy/hud/app/ui/component/tbt/TbtNextManeuverView;", "Landroid/widget/LinearLayout;", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "attrs", "Landroid/util/AttributeSet;", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "defStyleAttr", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "currentMode", "Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;", "lastManeuverState", "Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;", "lastNextTurnIconId", "nextManeuverIcon", "Landroid/widget/ImageView;", "nextManeuverText", "Landroid/widget/TextView;", "clear", "", "getCustomAnimator", "mode", "mainBuilder", "Landroid/animation/AnimatorSet$Builder;", "Landroid/animation/AnimatorSet;", "onFinishInflate", "setMode", "updateDisplay", "event", "Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;", "updateIcon", "Companion", "app_hudDebug"}, k = 1, mv = {1, 1, 6})
/* compiled from: TbtNextManeuverView.kt */
public final class TbtNextManeuverView extends android.widget.LinearLayout {
    public static final com.navdy.hud.app.ui.component.tbt.TbtNextManeuverView.Companion Companion = new com.navdy.hud.app.ui.component.tbt.TbtNextManeuverView.Companion(null);
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger("TbtNextManeuverView");
    private java.util.HashMap _$_findViewCache;
    private com.navdy.hud.app.view.MainView.CustomAnimationMode currentMode;
    private com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.ManeuverState lastManeuverState;
    private int lastNextTurnIconId = -1;
    private android.widget.ImageView nextManeuverIcon;
    private android.widget.TextView nextManeuverText;

    @kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0007"}, d2 = {"Lcom/navdy/hud/app/ui/component/tbt/TbtNextManeuverView$Companion;", "", "()V", "logger", "Lcom/navdy/service/library/log/Logger;", "getLogger", "()Lcom/navdy/service/library/log/Logger;", "app_hudDebug"}, k = 1, mv = {1, 1, 6})
    /* compiled from: TbtNextManeuverView.kt */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(kotlin.jvm.internal.DefaultConstructorMarker $constructor_marker) {
            this();
        }

        private final com.navdy.service.library.log.Logger getLogger() {
            return com.navdy.hud.app.ui.component.tbt.TbtNextManeuverView.logger;
        }
    }

    public void _$_clearFindViewByIdCache() {
        if (this._$_findViewCache != null) {
            this._$_findViewCache.clear();
        }
    }

    public android.view.View _$_findCachedViewById(int i) {
        if (this._$_findViewCache == null) {
            this._$_findViewCache = new java.util.HashMap();
        }
        android.view.View view = (android.view.View) this._$_findViewCache.get(java.lang.Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        android.view.View findViewById = findViewById(i);
        this._$_findViewCache.put(java.lang.Integer.valueOf(i), findViewById);
        return findViewById;
    }

    public TbtNextManeuverView(@org.jetbrains.annotations.NotNull android.content.Context context) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(context, "context");
        super(context);
    }

    public TbtNextManeuverView(@org.jetbrains.annotations.NotNull android.content.Context context, @org.jetbrains.annotations.NotNull android.util.AttributeSet attrs) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(context, "context");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(attrs, "attrs");
        super(context, attrs);
    }

    public TbtNextManeuverView(@org.jetbrains.annotations.NotNull android.content.Context context, @org.jetbrains.annotations.NotNull android.util.AttributeSet attrs, int defStyleAttr) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(context, "context");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(attrs, "attrs");
        super(context, attrs, defStyleAttr);
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
        android.view.View findViewById = findViewById(com.navdy.hud.app.R.id.nextManeuverText);
        if (findViewById == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type android.widget.TextView");
        }
        this.nextManeuverText = (android.widget.TextView) findViewById;
        android.view.View findViewById2 = findViewById(com.navdy.hud.app.R.id.nextManeuverIcon);
        if (findViewById2 == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type android.widget.ImageView");
        }
        this.nextManeuverIcon = (android.widget.ImageView) findViewById2;
    }

    public final void setMode(@org.jetbrains.annotations.NotNull com.navdy.hud.app.view.MainView.CustomAnimationMode mode) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(mode, net.hockeyapp.android.LoginActivity.EXTRA_MODE);
        if (!kotlin.jvm.internal.Intrinsics.areEqual((java.lang.Object) this.currentMode, (java.lang.Object) mode)) {
            this.currentMode = mode;
            if (kotlin.jvm.internal.Intrinsics.areEqual((java.lang.Object) mode, (java.lang.Object) com.navdy.hud.app.view.MainView.CustomAnimationMode.EXPAND)) {
                updateIcon();
            } else {
                setVisibility(8);
            }
        }
    }

    public final void getCustomAnimator(@org.jetbrains.annotations.NotNull com.navdy.hud.app.view.MainView.CustomAnimationMode mode, @org.jetbrains.annotations.NotNull android.animation.AnimatorSet.Builder mainBuilder) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(mode, net.hockeyapp.android.LoginActivity.EXTRA_MODE);
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(mainBuilder, "mainBuilder");
    }

    public final void clear() {
        this.lastNextTurnIconId = -1;
        android.widget.ImageView imageView = this.nextManeuverIcon;
        if (imageView != null) {
            imageView.setImageDrawable(null);
        }
        setVisibility(8);
    }

    public final void updateDisplay(@org.jetbrains.annotations.NotNull com.navdy.hud.app.maps.MapEvents.ManeuverDisplay event) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(event, "event");
        if (this.lastNextTurnIconId != event.nextTurnIconId) {
            this.lastNextTurnIconId = event.nextTurnIconId;
            this.lastManeuverState = event.maneuverState;
            updateIcon();
        }
    }

    private final void updateIcon() {
        if (this.lastNextTurnIconId != -1) {
            android.widget.ImageView imageView = this.nextManeuverIcon;
            if (imageView != null) {
                imageView.setImageResource(this.lastNextTurnIconId);
            }
            if (kotlin.jvm.internal.Intrinsics.areEqual((java.lang.Object) this.currentMode, (java.lang.Object) com.navdy.hud.app.view.MainView.CustomAnimationMode.SHRINK_LEFT)) {
                setVisibility(8);
            } else {
                setVisibility(0);
            }
        } else {
            android.widget.ImageView imageView2 = this.nextManeuverIcon;
            if (imageView2 != null) {
                imageView2.setImageDrawable(null);
            }
            setVisibility(8);
        }
    }
}
