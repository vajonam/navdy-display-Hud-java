package com.navdy.hud.app.ui.component.vlist.viewholder;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u00a1\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0017\n\u0002\u0010\u0007\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0002\b\b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006*\u0001\u0019\u0018\u0000 u2\u00020\u0001:\u0001uB\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\bJ\u0018\u0010P\u001a\u00020Q2\u0006\u0010R\u001a\u00020S2\u0006\u0010T\u001a\u00020UH\u0016J\b\u0010V\u001a\u00020QH\u0016J0\u0010W\u001a\u00020Q2\u0006\u0010X\u001a\u00020Y2\u0006\u0010Z\u001a\u00020-2\u0006\u0010[\u001a\u00020-2\u0006\u0010\\\u001a\u00020-2\u0006\u0010]\u001a\u00020\"H\u0016J\u0016\u0010^\u001a\u00020Q2\u0006\u0010+\u001a\u00020\"2\u0006\u0010*\u001a\u00020\"J\b\u0010_\u001a\u00020`H\u0016J\b\u0010a\u001a\u00020\nH\u0002J\b\u0010b\u001a\u00020\nH\u0002J\u0010\u0010c\u001a\u00020\n2\u0006\u0010d\u001a\u00020eH\u0002J\u0018\u0010f\u001a\u00020Q2\u0006\u0010R\u001a\u00020S2\u0006\u0010T\u001a\u00020UH\u0016J \u0010g\u001a\u00020Q2\u0006\u0010R\u001a\u00020S2\u0006\u0010h\u001a\u00020\n2\u0006\u0010i\u001a\u00020\nH\u0016J\u0010\u0010j\u001a\u00020Q2\u0006\u0010k\u001a\u00020\nH\u0002J(\u0010l\u001a\u00020Q2\u0006\u0010m\u001a\u00020n2\u0006\u0010o\u001a\u00020p2\u0006\u0010i\u001a\u00020\n2\u0006\u0010q\u001a\u00020\"H\u0016J\u001a\u00100\u001a\u00020Q2\b\u0010r\u001a\u0004\u0018\u00010e2\u0006\u0010s\u001a\u00020\"H\u0002J\u001a\u00104\u001a\u00020Q2\b\u0010r\u001a\u0004\u0018\u00010e2\u0006\u0010s\u001a\u00020\"H\u0002J\u0010\u0010L\u001a\u00020Q2\u0006\u0010r\u001a\u00020eH\u0002J\b\u0010q\u001a\u00020QH\u0016J\b\u0010t\u001a\u00020QH\u0002R\u0014\u0010\t\u001a\u00020\nX\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0014\u0010\r\u001a\u00020\nX\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\fR \u0010\u000f\u001a\b\u0018\u00010\u0010R\u00020\u0011X\u0084\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0012\u0010\u0013\"\u0004\b\u0014\u0010\u0015R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0018\u001a\u00020\u0019X\u0082\u0004\u00a2\u0006\u0004\n\u0002\u0010\u001aR\u001a\u0010\u001b\u001a\u00020\u001cX\u0084\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001d\u0010\u001e\"\u0004\b\u001f\u0010 R\u000e\u0010!\u001a\u00020\"X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010#\u001a\u00020\"X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010$\u001a\u00020\"X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010%\u001a\u00020\"X\u0084\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b&\u0010'\"\u0004\b(\u0010)R\u000e\u0010*\u001a\u00020\"X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010+\u001a\u00020\"X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010,\u001a\u00020-X\u0084\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b.\u0010/\"\u0004\b0\u00101R\u001a\u00102\u001a\u00020-X\u0084\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b3\u0010/\"\u0004\b4\u00101R\u001a\u00105\u001a\u000206X\u0084\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b7\u00108\"\u0004\b9\u0010:R\u001a\u0010;\u001a\u00020\u0003X\u0084\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b<\u0010=\"\u0004\b>\u0010?R\u001a\u0010@\u001a\u000206X\u0084\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bA\u00108\"\u0004\bB\u0010:R\u001a\u0010C\u001a\u00020\"X\u0084\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bD\u0010'\"\u0004\bE\u0010)R\u001a\u0010F\u001a\u00020\u0003X\u0084\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bG\u0010=\"\u0004\bH\u0010?R\u000e\u0010I\u001a\u00020\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010J\u001a\u00020-X\u0084\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bK\u0010/\"\u0004\bL\u00101R\u000e\u0010M\u001a\u00020NX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010O\u001a\u00020NX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006v"}, d2 = {"Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;", "Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;", "layout", "Landroid/view/ViewGroup;", "vlist", "Lcom/navdy/hud/app/ui/component/vlist/VerticalList;", "handler", "Landroid/os/Handler;", "(Landroid/view/ViewGroup;Lcom/navdy/hud/app/ui/component/vlist/VerticalList;Landroid/os/Handler;)V", "FLUCTUATOR_OPACITY_ALPHA", "", "getFLUCTUATOR_OPACITY_ALPHA", "()I", "HALO_DELAY_START_DURATION", "getHALO_DELAY_START_DURATION", "animatorSetBuilder", "Landroid/animation/AnimatorSet$Builder;", "Landroid/animation/AnimatorSet;", "getAnimatorSetBuilder", "()Landroid/animation/AnimatorSet$Builder;", "setAnimatorSetBuilder", "(Landroid/animation/AnimatorSet$Builder;)V", "fluctuatorRunnable", "Ljava/lang/Runnable;", "fluctuatorStartListener", "com/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$fluctuatorStartListener$Anon1", "Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$fluctuatorStartListener$Anon1;", "haloView", "Lcom/navdy/hud/app/ui/component/SwitchHaloView;", "getHaloView", "()Lcom/navdy/hud/app/ui/component/SwitchHaloView;", "setHaloView", "(Lcom/navdy/hud/app/ui/component/SwitchHaloView;)V", "hasIconFluctuatorColor", "", "hasSubTitle", "hasSubTitle2", "iconScaleAnimationDisabled", "getIconScaleAnimationDisabled", "()Z", "setIconScaleAnimationDisabled", "(Z)V", "isEnabled", "isOn", "subTitle", "Landroid/widget/TextView;", "getSubTitle", "()Landroid/widget/TextView;", "setSubTitle", "(Landroid/widget/TextView;)V", "subTitle2", "getSubTitle2", "setSubTitle2", "switchBackground", "Landroid/view/View;", "getSwitchBackground", "()Landroid/view/View;", "setSwitchBackground", "(Landroid/view/View;)V", "switchContainer", "getSwitchContainer", "()Landroid/view/ViewGroup;", "setSwitchContainer", "(Landroid/view/ViewGroup;)V", "switchThumb", "getSwitchThumb", "setSwitchThumb", "textAnimationDisabled", "getTextAnimationDisabled", "setTextAnimationDisabled", "textContainer", "getTextContainer", "setTextContainer", "thumbMargin", "title", "getTitle", "setTitle", "titleSelectedTopMargin", "", "titleUnselectedScale", "bind", "", "model", "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;", "modelState", "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;", "clearAnimation", "copyAndPosition", "imageC", "Landroid/widget/ImageView;", "titleC", "subTitleC", "subTitle2C", "setImage", "drawSwitch", "getModelType", "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;", "getSubTitle2Color", "getSubTitleColor", "getTextColor", "property", "", "preBind", "select", "pos", "duration", "setIconFluctuatorColor", "color", "setItemState", "state", "Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;", "animation", "Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;", "startFluctuator", "str", "formatted", "stopFluctuator", "Companion", "app_hudDebug"}, k = 1, mv = {1, 1, 6})
/* compiled from: SwitchViewHolder.kt */
public final class SwitchViewHolder extends com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder {
    public static final com.navdy.hud.app.ui.component.vlist.viewholder.SwitchViewHolder.Companion Companion = new com.navdy.hud.app.ui.component.vlist.viewholder.SwitchViewHolder.Companion(null);
    /* access modifiers changed from: private */
    @org.jetbrains.annotations.NotNull
    public static final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(Companion.getClass());
    private final int FLUCTUATOR_OPACITY_ALPHA = com.navdy.hud.app.ui.component.vlist.viewholder.IconBaseViewHolder.FLUCTUATOR_OPACITY_ALPHA;
    private final int HALO_DELAY_START_DURATION = 100;
    @org.jetbrains.annotations.Nullable
    private android.animation.AnimatorSet.Builder animatorSetBuilder;
    /* access modifiers changed from: private */
    public final java.lang.Runnable fluctuatorRunnable;
    private final com.navdy.hud.app.ui.component.vlist.viewholder.SwitchViewHolder$fluctuatorStartListener$Anon1 fluctuatorStartListener;
    @org.jetbrains.annotations.NotNull
    private com.navdy.hud.app.ui.component.SwitchHaloView haloView;
    private boolean hasIconFluctuatorColor;
    private boolean hasSubTitle;
    private boolean hasSubTitle2;
    private boolean iconScaleAnimationDisabled;
    /* access modifiers changed from: private */
    public boolean isEnabled;
    /* access modifiers changed from: private */
    public boolean isOn;
    @org.jetbrains.annotations.NotNull
    private android.widget.TextView subTitle;
    @org.jetbrains.annotations.NotNull
    private android.widget.TextView subTitle2;
    @org.jetbrains.annotations.NotNull
    private android.view.View switchBackground;
    @org.jetbrains.annotations.NotNull
    private android.view.ViewGroup switchContainer;
    @org.jetbrains.annotations.NotNull
    private android.view.View switchThumb;
    private boolean textAnimationDisabled;
    @org.jetbrains.annotations.NotNull
    private android.view.ViewGroup textContainer;
    private int thumbMargin;
    @org.jetbrains.annotations.NotNull
    private android.widget.TextView title;
    private float titleSelectedTopMargin;
    private float titleUnselectedScale;

    @kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J:\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\n2\u0006\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\r2\b\b\u0002\u0010\u000f\u001a\u00020\u00102\b\b\u0002\u0010\u0011\u001a\u00020\u0010J\u001e\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0019J\u0016\u0010\u001a\u001a\u00020\u00152\u0006\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u001b\u001a\u00020\nR\u0011\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u001c"}, d2 = {"Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$Companion;", "", "()V", "logger", "Lcom/navdy/service/library/log/Logger;", "getLogger", "()Lcom/navdy/service/library/log/Logger;", "buildModel", "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;", "id", "", "iconFluctuatorColor", "title", "", "subTitle", "isOn", "", "isEnabled", "buildViewHolder", "Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;", "parent", "Landroid/view/ViewGroup;", "vlist", "Lcom/navdy/hud/app/ui/component/vlist/VerticalList;", "handler", "Landroid/os/Handler;", "getLayout", "lytId", "app_hudDebug"}, k = 1, mv = {1, 1, 6})
    /* compiled from: SwitchViewHolder.kt */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(kotlin.jvm.internal.DefaultConstructorMarker $constructor_marker) {
            this();
        }

        @org.jetbrains.annotations.NotNull
        public final com.navdy.service.library.log.Logger getLogger() {
            return com.navdy.hud.app.ui.component.vlist.viewholder.SwitchViewHolder.logger;
        }

        @org.jetbrains.annotations.NotNull
        public static /* bridge */ /* synthetic */ com.navdy.hud.app.ui.component.vlist.VerticalList.Model buildModel$default(com.navdy.hud.app.ui.component.vlist.viewholder.SwitchViewHolder.Companion companion, int i, int i2, java.lang.String str, java.lang.String str2, boolean z, boolean z2, int i3, java.lang.Object obj) {
            boolean z3 = false;
            boolean z4 = (i3 & 16) != 0 ? false : z;
            if ((i3 & 32) == 0) {
                z3 = z2;
            }
            return companion.buildModel(i, i2, str, str2, z4, z3);
        }

        @org.jetbrains.annotations.NotNull
        public final com.navdy.hud.app.ui.component.vlist.VerticalList.Model buildModel(int id, int iconFluctuatorColor, @org.jetbrains.annotations.NotNull java.lang.String title, @org.jetbrains.annotations.NotNull java.lang.String subTitle, boolean isOn, boolean isEnabled) {
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(title, "title");
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(subTitle, "subTitle");
            com.navdy.hud.app.ui.component.vlist.VerticalList.Model model = com.navdy.hud.app.ui.component.vlist.VerticalModelCache.getFromCache(com.navdy.hud.app.ui.component.vlist.VerticalList.ModelType.SWITCH);
            if (model == null) {
                model = new com.navdy.hud.app.ui.component.vlist.VerticalList.Model();
            }
            model.type = com.navdy.hud.app.ui.component.vlist.VerticalList.ModelType.SWITCH;
            model.id = id;
            model.iconFluctuatorColor = iconFluctuatorColor;
            model.title = title;
            model.subTitle = subTitle;
            model.isOn = isOn;
            model.isEnabled = isEnabled;
            return model;
        }

        @org.jetbrains.annotations.NotNull
        public final com.navdy.hud.app.ui.component.vlist.viewholder.SwitchViewHolder buildViewHolder(@org.jetbrains.annotations.NotNull android.view.ViewGroup parent, @org.jetbrains.annotations.NotNull com.navdy.hud.app.ui.component.vlist.VerticalList vlist, @org.jetbrains.annotations.NotNull android.os.Handler handler) {
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(parent, "parent");
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(vlist, "vlist");
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(handler, "handler");
            return new com.navdy.hud.app.ui.component.vlist.viewholder.SwitchViewHolder(getLayout(parent, com.navdy.hud.app.R.layout.vlist_toggle_switch), vlist, handler);
        }

        @org.jetbrains.annotations.NotNull
        public final android.view.ViewGroup getLayout(@org.jetbrains.annotations.NotNull android.view.ViewGroup parent, int lytId) {
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(parent, "parent");
            android.view.View inflate = android.view.LayoutInflater.from(parent.getContext()).inflate(lytId, parent, false);
            if (inflate != null) {
                return (android.view.ViewGroup) inflate;
            }
            throw new kotlin.TypeCastException("null cannot be cast to non-null type android.view.ViewGroup");
        }
    }

    public SwitchViewHolder(@org.jetbrains.annotations.NotNull android.view.ViewGroup layout, @org.jetbrains.annotations.NotNull com.navdy.hud.app.ui.component.vlist.VerticalList vlist, @org.jetbrains.annotations.NotNull android.os.Handler handler) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(layout, "layout");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(vlist, "vlist");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(handler, "handler");
        super(layout, vlist, handler);
        this.fluctuatorStartListener = new com.navdy.hud.app.ui.component.vlist.viewholder.SwitchViewHolder$fluctuatorStartListener$Anon1(this, handler);
        this.fluctuatorRunnable = new com.navdy.hud.app.ui.component.vlist.viewholder.SwitchViewHolder$fluctuatorRunnable$Anon1(this);
        android.view.View findViewById = layout.findViewById(com.navdy.hud.app.R.id.textContainer);
        if (findViewById == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type android.view.ViewGroup");
        }
        this.textContainer = (android.view.ViewGroup) findViewById;
        android.view.View findViewById2 = layout.findViewById(com.navdy.hud.app.R.id.title);
        if (findViewById2 == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type android.widget.TextView");
        }
        this.title = (android.widget.TextView) findViewById2;
        android.view.View findViewById3 = layout.findViewById(com.navdy.hud.app.R.id.subTitle);
        if (findViewById3 == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type android.widget.TextView");
        }
        this.subTitle = (android.widget.TextView) findViewById3;
        android.view.View findViewById4 = layout.findViewById(com.navdy.hud.app.R.id.subTitle2);
        if (findViewById4 == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type android.widget.TextView");
        }
        this.subTitle2 = (android.widget.TextView) findViewById4;
        android.view.View findViewById5 = layout.findViewById(com.navdy.hud.app.R.id.toggleSwitchBackground);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(findViewById5, "layout.findViewById(R.id.toggleSwitchBackground)");
        this.switchBackground = findViewById5;
        android.view.View findViewById6 = layout.findViewById(com.navdy.hud.app.R.id.toggleSwitchThumb);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(findViewById6, "layout.findViewById(R.id.toggleSwitchThumb)");
        this.switchThumb = findViewById6;
        android.view.View findViewById7 = layout.findViewById(com.navdy.hud.app.R.id.imageContainer);
        if (findViewById7 == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type android.view.ViewGroup");
        }
        this.switchContainer = (android.view.ViewGroup) findViewById7;
        this.thumbMargin = layout.getResources().getDimensionPixelSize(com.navdy.hud.app.R.dimen.vlist_toggle_switch_thumb_margin);
        android.view.View findViewById8 = layout.findViewById(com.navdy.hud.app.R.id.halo);
        if (findViewById8 == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type com.navdy.hud.app.ui.component.SwitchHaloView");
        }
        this.haloView = (com.navdy.hud.app.ui.component.SwitchHaloView) findViewById8;
        this.haloView.setVisibility(8);
    }

    public final int getFLUCTUATOR_OPACITY_ALPHA() {
        return this.FLUCTUATOR_OPACITY_ALPHA;
    }

    public final int getHALO_DELAY_START_DURATION() {
        return this.HALO_DELAY_START_DURATION;
    }

    /* access modifiers changed from: protected */
    @org.jetbrains.annotations.NotNull
    public final android.view.ViewGroup getTextContainer() {
        return this.textContainer;
    }

    /* access modifiers changed from: protected */
    public final void setTextContainer(@org.jetbrains.annotations.NotNull android.view.ViewGroup viewGroup) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(viewGroup, "<set-?>");
        this.textContainer = viewGroup;
    }

    /* access modifiers changed from: protected */
    @org.jetbrains.annotations.NotNull
    public final android.widget.TextView getTitle() {
        return this.title;
    }

    /* access modifiers changed from: protected */
    public final void setTitle(@org.jetbrains.annotations.NotNull android.widget.TextView textView) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(textView, "<set-?>");
        this.title = textView;
    }

    /* access modifiers changed from: protected */
    @org.jetbrains.annotations.NotNull
    public final android.widget.TextView getSubTitle() {
        return this.subTitle;
    }

    /* access modifiers changed from: protected */
    public final void setSubTitle(@org.jetbrains.annotations.NotNull android.widget.TextView textView) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(textView, "<set-?>");
        this.subTitle = textView;
    }

    /* access modifiers changed from: protected */
    @org.jetbrains.annotations.NotNull
    public final android.widget.TextView getSubTitle2() {
        return this.subTitle2;
    }

    /* access modifiers changed from: protected */
    public final void setSubTitle2(@org.jetbrains.annotations.NotNull android.widget.TextView textView) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(textView, "<set-?>");
        this.subTitle2 = textView;
    }

    /* access modifiers changed from: protected */
    @org.jetbrains.annotations.NotNull
    public final android.view.View getSwitchBackground() {
        return this.switchBackground;
    }

    /* access modifiers changed from: protected */
    public final void setSwitchBackground(@org.jetbrains.annotations.NotNull android.view.View view) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(view, "<set-?>");
        this.switchBackground = view;
    }

    /* access modifiers changed from: protected */
    @org.jetbrains.annotations.NotNull
    public final android.view.View getSwitchThumb() {
        return this.switchThumb;
    }

    /* access modifiers changed from: protected */
    public final void setSwitchThumb(@org.jetbrains.annotations.NotNull android.view.View view) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(view, "<set-?>");
        this.switchThumb = view;
    }

    /* access modifiers changed from: protected */
    @org.jetbrains.annotations.NotNull
    public final android.view.ViewGroup getSwitchContainer() {
        return this.switchContainer;
    }

    /* access modifiers changed from: protected */
    public final void setSwitchContainer(@org.jetbrains.annotations.NotNull android.view.ViewGroup viewGroup) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(viewGroup, "<set-?>");
        this.switchContainer = viewGroup;
    }

    /* access modifiers changed from: protected */
    @org.jetbrains.annotations.NotNull
    public final com.navdy.hud.app.ui.component.SwitchHaloView getHaloView() {
        return this.haloView;
    }

    /* access modifiers changed from: protected */
    public final void setHaloView(@org.jetbrains.annotations.NotNull com.navdy.hud.app.ui.component.SwitchHaloView switchHaloView) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(switchHaloView, "<set-?>");
        this.haloView = switchHaloView;
    }

    /* access modifiers changed from: protected */
    public final boolean getTextAnimationDisabled() {
        return this.textAnimationDisabled;
    }

    /* access modifiers changed from: protected */
    public final void setTextAnimationDisabled(boolean z) {
        this.textAnimationDisabled = z;
    }

    /* access modifiers changed from: protected */
    public final boolean getIconScaleAnimationDisabled() {
        return this.iconScaleAnimationDisabled;
    }

    /* access modifiers changed from: protected */
    public final void setIconScaleAnimationDisabled(boolean z) {
        this.iconScaleAnimationDisabled = z;
    }

    /* access modifiers changed from: protected */
    @org.jetbrains.annotations.Nullable
    public final android.animation.AnimatorSet.Builder getAnimatorSetBuilder() {
        return this.animatorSetBuilder;
    }

    /* access modifiers changed from: protected */
    public final void setAnimatorSetBuilder(@org.jetbrains.annotations.Nullable android.animation.AnimatorSet.Builder builder) {
        this.animatorSetBuilder = builder;
    }

    public void setItemState(@org.jetbrains.annotations.NotNull com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.State state, @org.jetbrains.annotations.NotNull com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.AnimationType animation, int duration, boolean startFluctuator) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(state, com.amazonaws.mobileconnectors.s3.transferutility.TransferTable.COLUMN_STATE);
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(animation, "animation");
        float switchScaleFactor = 0.0f;
        float titleScaleFactor = 0.0f;
        float titleTopMargin = 0.0f;
        float subTitleAlpha = 0.0f;
        float subTitleScaleFactor = 0.0f;
        float subTitle2Alpha = 0.0f;
        float subTitle2ScaleFactor = 0.0f;
        com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.State itemState = state;
        this.animatorSetBuilder = null;
        if (this.textAnimationDisabled) {
            if (kotlin.jvm.internal.Intrinsics.areEqual((java.lang.Object) animation, (java.lang.Object) com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.AnimationType.MOVE)) {
                this.itemAnimatorSet = new android.animation.AnimatorSet();
                this.animatorSetBuilder = this.itemAnimatorSet.play(android.animation.ValueAnimator.ofFloat(new float[]{0.0f, 1.0f}));
                return;
            }
            itemState = com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.State.SELECTED;
        }
        switch (itemState) {
            case SELECTED:
                switchScaleFactor = 1.0f;
                titleScaleFactor = 1.0f;
                titleTopMargin = this.titleSelectedTopMargin;
                subTitleAlpha = 1.0f;
                subTitleScaleFactor = 1.0f;
                subTitle2Alpha = 1.0f;
                subTitle2ScaleFactor = 1.0f;
                break;
            case UNSELECTED:
                switchScaleFactor = 0.6f;
                titleScaleFactor = this.titleUnselectedScale;
                titleTopMargin = 0.0f;
                subTitleAlpha = 0.0f;
                subTitleScaleFactor = this.titleUnselectedScale;
                subTitle2Alpha = 0.0f;
                subTitle2ScaleFactor = this.titleUnselectedScale;
                break;
        }
        if (!this.hasSubTitle) {
            titleTopMargin = 0.0f;
            subTitleAlpha = 0.0f;
            subTitle2Alpha = 0.0f;
        }
        switch (animation) {
            case INIT:
            case NONE:
                this.switchContainer.setScaleX(switchScaleFactor);
                this.switchContainer.setScaleY(switchScaleFactor);
                break;
            case MOVE:
                this.itemAnimatorSet = new android.animation.AnimatorSet();
                android.animation.PropertyValuesHolder switchP1 = android.animation.PropertyValuesHolder.ofFloat(android.view.View.SCALE_X, new float[]{switchScaleFactor});
                android.animation.PropertyValuesHolder switchP2 = android.animation.PropertyValuesHolder.ofFloat(android.view.View.SCALE_Y, new float[]{switchScaleFactor});
                this.animatorSetBuilder = this.itemAnimatorSet.play(android.animation.ObjectAnimator.ofPropertyValuesHolder(this.title, new android.animation.PropertyValuesHolder[]{switchP1, switchP2}));
                android.animation.AnimatorSet.Builder builder = this.animatorSetBuilder;
                if (builder == null) {
                    kotlin.jvm.internal.Intrinsics.throwNpe();
                }
                builder.with(android.animation.ObjectAnimator.ofPropertyValuesHolder(this.switchContainer, new android.animation.PropertyValuesHolder[]{switchP1, switchP2}));
                break;
        }
        this.title.setPivotX(0.0f);
        this.title.setPivotY(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.titleHeight / ((float) 2));
        switch (animation) {
            case INIT:
            case NONE:
                this.title.setScaleX(titleScaleFactor);
                this.title.setScaleY(titleScaleFactor);
                android.view.ViewGroup.LayoutParams layoutParams = this.title.getLayoutParams();
                if (layoutParams != null) {
                    ((android.view.ViewGroup.MarginLayoutParams) layoutParams).topMargin = (int) titleTopMargin;
                    this.title.requestLayout();
                    break;
                } else {
                    throw new kotlin.TypeCastException("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
                }
            case MOVE:
                android.animation.PropertyValuesHolder p1 = android.animation.PropertyValuesHolder.ofFloat(android.view.View.SCALE_X, new float[]{titleScaleFactor});
                android.animation.PropertyValuesHolder p2 = android.animation.PropertyValuesHolder.ofFloat(android.view.View.SCALE_Y, new float[]{titleScaleFactor});
                android.animation.AnimatorSet.Builder builder2 = this.animatorSetBuilder;
                if (builder2 == null) {
                    kotlin.jvm.internal.Intrinsics.throwNpe();
                }
                builder2.with(android.animation.ObjectAnimator.ofPropertyValuesHolder(this.title, new android.animation.PropertyValuesHolder[]{p1, p2}));
                android.animation.AnimatorSet.Builder builder3 = this.animatorSetBuilder;
                if (builder3 == null) {
                    kotlin.jvm.internal.Intrinsics.throwNpe();
                }
                builder3.with(com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils.animateMargin(this.title, (int) titleTopMargin));
                break;
        }
        this.subTitle.setPivotX(0.0f);
        this.subTitle.setPivotY(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.subTitleHeight / ((float) 2));
        com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.AnimationType animation2 = animation;
        if (!this.hasSubTitle) {
            animation2 = com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.AnimationType.NONE;
        }
        switch (animation2) {
            case INIT:
            case NONE:
                this.subTitle.setAlpha(subTitleAlpha);
                this.subTitle.setScaleX(subTitleScaleFactor);
                this.subTitle.setScaleY(subTitleScaleFactor);
                break;
            case MOVE:
                android.animation.PropertyValuesHolder p12 = android.animation.PropertyValuesHolder.ofFloat(android.view.View.SCALE_X, new float[]{subTitleScaleFactor});
                android.animation.PropertyValuesHolder p22 = android.animation.PropertyValuesHolder.ofFloat(android.view.View.SCALE_Y, new float[]{subTitleScaleFactor});
                android.animation.PropertyValuesHolder p3 = android.animation.PropertyValuesHolder.ofFloat(android.view.View.ALPHA, new float[]{subTitleAlpha});
                android.animation.AnimatorSet.Builder builder4 = this.animatorSetBuilder;
                if (builder4 == null) {
                    kotlin.jvm.internal.Intrinsics.throwNpe();
                }
                builder4.with(android.animation.ObjectAnimator.ofPropertyValuesHolder(this.subTitle, new android.animation.PropertyValuesHolder[]{p12, p22, p3}));
                break;
        }
        this.subTitle2.setPivotX(0.0f);
        this.subTitle2.setPivotY(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.subTitleHeight / ((float) 2));
        com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.AnimationType animation22 = animation;
        if (!this.hasSubTitle2) {
            animation22 = com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.AnimationType.NONE;
        }
        switch (animation22) {
            case INIT:
            case NONE:
                this.subTitle2.setAlpha(subTitle2Alpha);
                this.subTitle2.setScaleX(subTitle2ScaleFactor);
                this.subTitle2.setScaleY(subTitle2ScaleFactor);
                break;
            case MOVE:
                android.animation.PropertyValuesHolder p13 = android.animation.PropertyValuesHolder.ofFloat(android.view.View.SCALE_X, new float[]{subTitle2ScaleFactor});
                android.animation.PropertyValuesHolder p23 = android.animation.PropertyValuesHolder.ofFloat(android.view.View.SCALE_Y, new float[]{subTitle2ScaleFactor});
                android.animation.PropertyValuesHolder p32 = android.animation.PropertyValuesHolder.ofFloat(android.view.View.ALPHA, new float[]{subTitleAlpha});
                android.animation.AnimatorSet.Builder builder5 = this.animatorSetBuilder;
                if (builder5 == null) {
                    kotlin.jvm.internal.Intrinsics.throwNpe();
                }
                builder5.with(android.animation.ObjectAnimator.ofPropertyValuesHolder(this.subTitle2, new android.animation.PropertyValuesHolder[]{p13, p23, p32}));
                break;
        }
        if (kotlin.jvm.internal.Intrinsics.areEqual((java.lang.Object) itemState, (java.lang.Object) com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.State.SELECTED) && startFluctuator) {
            if (this.itemAnimatorSet != null) {
                this.itemAnimatorSet.addListener(this.fluctuatorStartListener);
                return;
            }
            startFluctuator();
        }
    }

    public void clearAnimation() {
        stopFluctuator();
        stopAnimation();
        this.currentState = null;
        this.layout.setVisibility(0);
        this.layout.setAlpha(1.0f);
    }

    private final void setTitle(java.lang.String str) {
        this.title.setText(str);
    }

    private final void setSubTitle(java.lang.String str, boolean formatted) {
        if (str == null) {
            this.subTitle.setText("");
            this.hasSubTitle = false;
            return;
        }
        int c = getSubTitleColor();
        if (c == 0) {
            this.subTitle.setTextColor(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.subTitleColor);
        } else {
            this.subTitle.setTextColor(c);
        }
        if (formatted) {
            this.subTitle.setText(android.text.Html.fromHtml(str));
        } else {
            this.subTitle.setText(str);
        }
        this.hasSubTitle = true;
    }

    private final void setSubTitle2(java.lang.String str, boolean formatted) {
        if (str == null) {
            this.subTitle2.setText("");
            this.subTitle2.setVisibility(8);
            this.hasSubTitle2 = false;
            return;
        }
        int c = getSubTitle2Color();
        if (c == 0) {
            this.subTitle2.setTextColor(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.subTitle2Color);
        } else {
            this.subTitle2.setTextColor(c);
        }
        if (formatted) {
            this.subTitle2.setText(android.text.Html.fromHtml(str));
        } else {
            this.subTitle2.setText(str);
        }
        this.subTitle2.setVisibility(0);
        this.hasSubTitle2 = true;
    }

    private final void setIconFluctuatorColor(int color) {
        if (color != 0) {
            int fluctuatorColor = android.graphics.Color.argb(this.FLUCTUATOR_OPACITY_ALPHA, android.graphics.Color.red(color), android.graphics.Color.green(color), android.graphics.Color.blue(color));
            this.hasIconFluctuatorColor = true;
            this.haloView.setStrokeColor(fluctuatorColor);
            return;
        }
        this.hasIconFluctuatorColor = false;
    }

    public void startFluctuator() {
        if (this.hasIconFluctuatorColor) {
            this.haloView.setVisibility(0);
            this.haloView.start();
        }
    }

    private final void stopFluctuator() {
        if (this.hasIconFluctuatorColor) {
            this.handler.removeCallbacks(this.fluctuatorRunnable);
            this.haloView.setVisibility(8);
            this.haloView.stop();
        }
    }

    public void select(@org.jetbrains.annotations.NotNull com.navdy.hud.app.ui.component.vlist.VerticalList.Model model, int pos, int duration) {
        android.animation.ValueAnimator turnOnThumbAnimator;
        android.animation.ValueAnimator clipLevelAnimator;
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(model, "model");
        if (this.hasIconFluctuatorColor) {
            this.haloView.setVisibility(4);
            this.haloView.stop();
        }
        if (!this.isEnabled) {
            this.vlist.unlock();
            return;
        }
        android.animation.AnimatorSet animatorSet = new android.animation.AnimatorSet();
        android.animation.PropertyValuesHolder clickDownXPropertyHolder = android.animation.PropertyValuesHolder.ofFloat(android.view.View.SCALE_X, new float[]{1.0f, 0.8f});
        android.animation.PropertyValuesHolder clickDownYPropertyHolder = android.animation.PropertyValuesHolder.ofFloat(android.view.View.SCALE_Y, new float[]{1.0f, 0.8f});
        android.animation.ObjectAnimator clickDownAnimator = android.animation.ObjectAnimator.ofPropertyValuesHolder(this.switchContainer, new android.animation.PropertyValuesHolder[]{clickDownXPropertyHolder, clickDownYPropertyHolder});
        clickDownAnimator.setDuration((long) duration);
        android.animation.PropertyValuesHolder clickUpXPropertyHolder = android.animation.PropertyValuesHolder.ofFloat(android.view.View.SCALE_X, new float[]{0.8f, 1.0f});
        android.animation.PropertyValuesHolder clickUpYPropertyHolder = android.animation.PropertyValuesHolder.ofFloat(android.view.View.SCALE_Y, new float[]{0.8f, 1.0f});
        android.animation.ObjectAnimator clickUpAnimator = android.animation.ObjectAnimator.ofPropertyValuesHolder(this.switchContainer, new android.animation.PropertyValuesHolder[]{clickUpXPropertyHolder, clickUpYPropertyHolder});
        clickUpAnimator.setDuration((long) duration);
        android.animation.AnimatorSet clickAnimatorSet = new android.animation.AnimatorSet();
        clickAnimatorSet.playSequentially(new android.animation.Animator[]{clickDownAnimator, clickUpAnimator});
        android.animation.AnimatorSet switchAnimatorSet = new android.animation.AnimatorSet();
        android.graphics.drawable.Drawable background = this.switchBackground.getBackground();
        if (background == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type android.graphics.drawable.LayerDrawable");
        }
        android.graphics.drawable.Drawable drawable = ((android.graphics.drawable.LayerDrawable) background).getDrawable(1);
        if (drawable == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type android.graphics.drawable.ClipDrawable");
        }
        android.graphics.drawable.ClipDrawable clipDrawable = (android.graphics.drawable.ClipDrawable) drawable;
        android.view.ViewGroup.LayoutParams layoutParams = this.switchThumb.getLayoutParams();
        if (layoutParams == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
        }
        android.view.ViewGroup.MarginLayoutParams marginLayoutParamas = (android.view.ViewGroup.MarginLayoutParams) layoutParams;
        android.animation.ValueAnimator valueAnimator = null;
        android.animation.ValueAnimator valueAnimator2 = null;
        if (this.isOn) {
            turnOnThumbAnimator = android.animation.ValueAnimator.ofFloat(new float[]{(float) this.thumbMargin, 0.0f});
            turnOnThumbAnimator.setDuration(100);
            clipLevelAnimator = android.animation.ValueAnimator.ofFloat(new float[]{5000.0f, 1000.0f});
            clickAnimatorSet.setDuration(100);
        } else {
            turnOnThumbAnimator = android.animation.ValueAnimator.ofFloat(new float[]{0.0f, (float) this.thumbMargin});
            turnOnThumbAnimator.setDuration(100);
            clipLevelAnimator = android.animation.ValueAnimator.ofFloat(new float[]{3000.0f, 7000.0f});
            clickAnimatorSet.setDuration(100);
        }
        if (clipLevelAnimator != null) {
            com.navdy.hud.app.ui.component.vlist.viewholder.SwitchViewHolder$select$Anon1 switchViewHolder$select$Anon1 = new com.navdy.hud.app.ui.component.vlist.viewholder.SwitchViewHolder$select$Anon1(this, clipDrawable);
            clipLevelAnimator.addUpdateListener(switchViewHolder$select$Anon1);
        }
        if (turnOnThumbAnimator != null) {
            com.navdy.hud.app.ui.component.vlist.viewholder.SwitchViewHolder$select$Anon2 switchViewHolder$select$Anon2 = new com.navdy.hud.app.ui.component.vlist.viewholder.SwitchViewHolder$select$Anon2(this, marginLayoutParamas);
            turnOnThumbAnimator.addUpdateListener(switchViewHolder$select$Anon2);
        }
        switchAnimatorSet.playTogether(new android.animation.Animator[]{turnOnThumbAnimator, clipLevelAnimator});
        animatorSet.play(switchAnimatorSet).after(clickAnimatorSet);
        com.navdy.hud.app.ui.component.vlist.viewholder.SwitchViewHolder$select$Anon3 switchViewHolder$select$Anon3 = new com.navdy.hud.app.ui.component.vlist.viewholder.SwitchViewHolder$select$Anon3(this, model, pos);
        animatorSet.addListener(switchViewHolder$select$Anon3);
        animatorSet.start();
    }

    private final int getSubTitleColor() {
        if (this.extras == null) {
            return 0;
        }
        return getTextColor(com.navdy.hud.app.ui.component.vlist.VerticalList.Model.SUBTITLE_COLOR);
    }

    private final int getSubTitle2Color() {
        if (this.extras == null) {
            return 0;
        }
        return getTextColor(com.navdy.hud.app.ui.component.vlist.VerticalList.Model.SUBTITLE_2_COLOR);
    }

    private final int getTextColor(java.lang.String property) {
        int i = 0;
        if (this.extras == null) {
            return i;
        }
        java.lang.String str = (java.lang.String) this.extras.get(property);
        if (str == null) {
            return i;
        }
        try {
            return java.lang.Integer.parseInt(str);
        } catch (java.lang.NumberFormatException e) {
            return i;
        }
    }

    public void copyAndPosition(@org.jetbrains.annotations.NotNull android.widget.ImageView imageC, @org.jetbrains.annotations.NotNull android.widget.TextView titleC, @org.jetbrains.annotations.NotNull android.widget.TextView subTitleC, @org.jetbrains.annotations.NotNull android.widget.TextView subTitle2C, boolean setImage) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(imageC, "imageC");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(titleC, "titleC");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(subTitleC, "subTitleC");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(subTitle2C, "subTitle2C");
    }

    public final void drawSwitch(boolean isOn2, boolean isEnabled2) {
        if (isEnabled2) {
            this.switchThumb.setBackgroundResource(com.navdy.hud.app.R.drawable.switch_thumb_enabled);
        } else {
            this.switchThumb.setBackgroundResource(com.navdy.hud.app.R.drawable.switch_thumb_disabled);
        }
        if (!isEnabled2 || !isOn2) {
            this.switchBackground.setBackgroundResource(com.navdy.hud.app.R.drawable.switch_button_off);
            android.graphics.drawable.Drawable background = this.switchBackground.getBackground();
            if (background == null) {
                throw new kotlin.TypeCastException("null cannot be cast to non-null type android.graphics.drawable.LayerDrawable");
            }
            android.graphics.drawable.Drawable drawable = ((android.graphics.drawable.LayerDrawable) background).getDrawable(1);
            if (drawable == null) {
                throw new kotlin.TypeCastException("null cannot be cast to non-null type android.graphics.drawable.ClipDrawable");
            }
            ((android.graphics.drawable.ClipDrawable) drawable).setLevel(0);
            android.view.ViewGroup.LayoutParams layoutParams = this.switchThumb.getLayoutParams();
            if (layoutParams == null) {
                throw new kotlin.TypeCastException("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
            }
            android.view.ViewGroup.MarginLayoutParams layoutParams2 = (android.view.ViewGroup.MarginLayoutParams) layoutParams;
            layoutParams2.leftMargin = 0;
            this.switchThumb.setLayoutParams(layoutParams2);
            return;
        }
        this.switchBackground.setBackgroundResource(com.navdy.hud.app.R.drawable.switch_button_off);
        android.graphics.drawable.Drawable background2 = this.switchBackground.getBackground();
        if (background2 == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type android.graphics.drawable.LayerDrawable");
        }
        android.graphics.drawable.Drawable drawable2 = ((android.graphics.drawable.LayerDrawable) background2).getDrawable(1);
        if (drawable2 == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type android.graphics.drawable.ClipDrawable");
        }
        ((android.graphics.drawable.ClipDrawable) drawable2).setLevel(5000);
        android.view.ViewGroup.LayoutParams layoutParams3 = this.switchThumb.getLayoutParams();
        if (layoutParams3 == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
        }
        android.view.ViewGroup.MarginLayoutParams layoutParams4 = (android.view.ViewGroup.MarginLayoutParams) layoutParams3;
        layoutParams4.leftMargin = this.thumbMargin;
        this.switchThumb.setLayoutParams(layoutParams4);
    }

    public void preBind(@org.jetbrains.annotations.NotNull com.navdy.hud.app.ui.component.vlist.VerticalList.Model model, @org.jetbrains.annotations.NotNull com.navdy.hud.app.ui.component.vlist.VerticalList.ModelState modelState) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(model, "model");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(modelState, "modelState");
        this.isOn = model.isOn;
        this.isEnabled = model.isEnabled;
        drawSwitch(this.isOn, this.isEnabled);
        android.view.ViewGroup.LayoutParams layoutParams = this.title.getLayoutParams();
        if (layoutParams == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
        }
        ((android.view.ViewGroup.MarginLayoutParams) layoutParams).topMargin = (int) model.fontInfo.titleFontTopMargin;
        this.title.setTextSize(model.fontInfo.titleFontSize);
        this.titleSelectedTopMargin = (float) ((int) model.fontInfo.titleFontTopMargin);
        android.view.ViewGroup.LayoutParams layoutParams2 = this.subTitle.getLayoutParams();
        if (layoutParams2 == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
        }
        ((android.view.ViewGroup.MarginLayoutParams) layoutParams2).topMargin = (int) model.fontInfo.subTitleFontTopMargin;
        if (!model.subTitle_2Lines) {
            this.subTitle.setTextAppearance(com.navdy.hud.app.HudApplication.getAppContext(), com.navdy.hud.app.R.style.vlist_subtitle);
            this.subTitle.setSingleLine(true);
            this.subTitle.setEllipsize(null);
        } else {
            this.subTitle.setTextAppearance(com.navdy.hud.app.HudApplication.getAppContext(), com.navdy.hud.app.R.style.vlist_subtitle_2_line);
            this.subTitle.setSingleLine(false);
            this.subTitle.setMaxLines(2);
            this.subTitle.setEllipsize(android.text.TextUtils.TruncateAt.END);
        }
        this.subTitle.setTextSize(model.fontInfo.subTitleFontSize);
        android.view.ViewGroup.LayoutParams layoutParams3 = this.subTitle2.getLayoutParams();
        if (layoutParams3 == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
        }
        ((android.view.ViewGroup.MarginLayoutParams) layoutParams3).topMargin = (int) model.fontInfo.subTitle2FontTopMargin;
        this.subTitle2.setTextSize(model.fontInfo.subTitle2FontSize);
        this.titleUnselectedScale = model.fontInfo.titleScale;
    }

    public void bind(@org.jetbrains.annotations.NotNull com.navdy.hud.app.ui.component.vlist.VerticalList.Model model, @org.jetbrains.annotations.NotNull com.navdy.hud.app.ui.component.vlist.VerticalList.ModelState modelState) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(model, "model");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(modelState, "modelState");
        this.isOn = model.isOn;
        this.isEnabled = model.isEnabled;
        drawSwitch(this.isOn, this.isEnabled);
        if (modelState.updateTitle) {
            java.lang.String str = model.title;
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(str, "model.title");
            setTitle(str);
        }
        if (modelState.updateSubTitle) {
            if (model.subTitle != null) {
                setSubTitle(model.subTitle, model.subTitleFormatted);
            } else {
                setSubTitle(null, false);
            }
        }
        if (modelState.updateSubTitle2) {
            if (model.subTitle2 != null) {
                setSubTitle2(model.subTitle2, model.subTitle2Formatted);
            } else {
                setSubTitle2(null, false);
            }
        }
        this.textAnimationDisabled = model.noTextAnimation;
        this.iconScaleAnimationDisabled = model.noImageScaleAnimation;
        setIconFluctuatorColor(model.iconFluctuatorColor);
    }

    @org.jetbrains.annotations.NotNull
    public com.navdy.hud.app.ui.component.vlist.VerticalList.ModelType getModelType() {
        return com.navdy.hud.app.ui.component.vlist.VerticalList.ModelType.SWITCH;
    }
}
