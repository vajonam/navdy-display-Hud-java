package com.navdy.hud.app.ui.component.image;

public class InitialsImageView extends android.widget.ImageView {
    private static int greyColor;
    private static int largeStyleTextSize;
    private static int mediumStyleTextSize;
    private static int smallStyleTextSize;
    private static int tinyStyleTextSize;
    private static android.graphics.Typeface typefaceLarge;
    private static android.graphics.Typeface typefaceSmall;
    private static boolean valuesSet;
    private static int whiteColor;
    private int bkColor;
    private java.lang.String initials;
    private android.graphics.Paint paint;
    private boolean scaled;
    private com.navdy.hud.app.ui.component.image.InitialsImageView.Style style;

    public enum Style {
        TINY,
        SMALL,
        LARGE,
        MEDIUM,
        DEFAULT
    }

    public InitialsImageView(android.content.Context context) {
        this(context, null, 0);
    }

    public InitialsImageView(android.content.Context context, android.util.AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public InitialsImageView(android.content.Context context, android.util.AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.style = com.navdy.hud.app.ui.component.image.InitialsImageView.Style.DEFAULT;
        this.bkColor = greyColor;
        init();
    }

    private void init() {
        if (!valuesSet) {
            valuesSet = true;
            android.content.res.Resources resources = getResources();
            greyColor = resources.getColor(com.navdy.hud.app.R.color.grey_4a);
            whiteColor = resources.getColor(17170443);
            tinyStyleTextSize = (int) resources.getDimension(com.navdy.hud.app.R.dimen.contact_image_tiny_text);
            smallStyleTextSize = (int) resources.getDimension(com.navdy.hud.app.R.dimen.contact_image_small_text);
            mediumStyleTextSize = (int) resources.getDimension(com.navdy.hud.app.R.dimen.contact_image_medium_text);
            largeStyleTextSize = (int) resources.getDimension(com.navdy.hud.app.R.dimen.contact_image_large_text);
            typefaceSmall = android.graphics.Typeface.create("sans-serif-medium", 0);
            typefaceLarge = android.graphics.Typeface.create("sans-serif", 1);
        }
        this.paint = new android.graphics.Paint();
        this.paint.setStrokeWidth(0.0f);
        this.paint.setAntiAlias(true);
    }

    public void setInitials(java.lang.String initials2, com.navdy.hud.app.ui.component.image.InitialsImageView.Style style2) {
        this.initials = initials2;
        this.style = style2;
        invalidate();
    }

    public void setImage(int resourceId, java.lang.String initials2, com.navdy.hud.app.ui.component.image.InitialsImageView.Style style2) {
        setImageResource(resourceId);
        setInitials(initials2, style2);
    }

    public com.navdy.hud.app.ui.component.image.InitialsImageView.Style getStyle() {
        return this.style;
    }

    /* access modifiers changed from: protected */
    public void onDraw(android.graphics.Canvas canvas) {
        super.onDraw(canvas);
        if (this.style != com.navdy.hud.app.ui.component.image.InitialsImageView.Style.DEFAULT) {
            int width = getWidth();
            int height = getHeight();
            switch (this.style) {
                case SMALL:
                case TINY:
                    canvas.drawColor(0);
                    if (this.bkColor != 0) {
                        this.paint.setColor(this.bkColor);
                        canvas.drawCircle((float) (width / 2), (float) (height / 2), (float) (width / 2), this.paint);
                        break;
                    }
                    break;
            }
            com.navdy.hud.app.ui.component.image.InitialsImageView.Style s = this.style;
            if (this.scaled) {
                s = com.navdy.hud.app.ui.component.image.InitialsImageView.Style.SMALL;
            }
            if (this.initials != null) {
                this.paint.setColor(whiteColor);
                switch (s) {
                    case SMALL:
                        this.paint.setTextSize((float) smallStyleTextSize);
                        this.paint.setTypeface(typefaceSmall);
                        break;
                    case TINY:
                        this.paint.setTextSize((float) tinyStyleTextSize);
                        this.paint.setTypeface(typefaceSmall);
                        break;
                    case MEDIUM:
                        this.paint.setTextSize((float) mediumStyleTextSize);
                        this.paint.setTypeface(typefaceLarge);
                        break;
                    case LARGE:
                        this.paint.setTextSize((float) largeStyleTextSize);
                        this.paint.setTypeface(typefaceLarge);
                        break;
                }
                this.paint.setTextAlign(android.graphics.Paint.Align.CENTER);
                canvas.drawText(this.initials, (float) (width / 2), (float) ((int) (((float) (height / 2)) - ((this.paint.descent() + this.paint.ascent()) / 2.0f))), this.paint);
            }
        }
    }

    public void setScaled(boolean val) {
        this.scaled = val;
    }

    public void setBkColor(int color) {
        this.bkColor = color;
    }
}
