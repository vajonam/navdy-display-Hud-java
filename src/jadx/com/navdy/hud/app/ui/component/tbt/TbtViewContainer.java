package com.navdy.hud.app.ui.component.tbt;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000j\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\n\u0018\u0000 .2\u00020\u00012\u00020\u0002:\u0001.B\u000f\b\u0016\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005B\u0017\b\u0016\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\bB\u001f\b\u0016\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\t\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000bJ\u0006\u0010\u001f\u001a\u00020 J\u001a\u0010!\u001a\u00020 2\u0006\u0010\"\u001a\u00020\u00192\n\u0010#\u001a\u00060$R\u00020%J\b\u0010&\u001a\u0004\u0018\u00010\u0012J\u000e\u0010'\u001a\u00020 2\u0006\u0010\u000e\u001a\u00020\u000fJ\b\u0010(\u001a\u00020 H\u0014J\b\u0010)\u001a\u00020 H\u0016J\b\u0010*\u001a\u00020 H\u0016J\u000e\u0010+\u001a\u00020 2\u0006\u0010\"\u001a\u00020\u0019J\u0010\u0010,\u001a\u00020 2\u0006\u0010-\u001a\u00020\u0012H\u0007R\u0010\u0010\f\u001a\u0004\u0018\u00010\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000e\u001a\u0004\u0018\u00010\u000fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0010\u001a\u0004\u0018\u00010\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0011\u001a\u0004\u0018\u00010\u0012X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0013\u001a\u0004\u0018\u00010\u0012X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0014\u001a\u0004\u0018\u00010\u0015X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0016\u001a\u0004\u0018\u00010\u0017X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0018\u001a\u0004\u0018\u00010\u0019X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u001a\u001a\u0004\u0018\u00010\u0015X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u001c\u001a\u0004\u0018\u00010\u0015X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001d\u001a\u00020\u001eX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006/"}, d2 = {"Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;", "Landroid/widget/FrameLayout;", "Lcom/navdy/hud/app/ui/component/homescreen/IHomeScreenLifecycle;", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "attrs", "Landroid/util/AttributeSet;", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "defStyleAttr", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "activeView", "Lcom/navdy/hud/app/ui/component/tbt/TbtView;", "homeScreenView", "Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;", "inactiveView", "lastEvent", "Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;", "lastManeuverDisplay", "lastManeuverId", "", "lastManeuverState", "Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;", "lastMode", "Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;", "lastRoadText", "lastTurnIconId", "lastTurnText", "paused", "", "clearState", "", "getCustomAnimator", "mode", "mainBuilder", "Landroid/animation/AnimatorSet$Builder;", "Landroid/animation/AnimatorSet;", "getLastManeuverDisplay", "init", "onFinishInflate", "onPause", "onResume", "setView", "updateDisplay", "event", "Companion", "app_hudDebug"}, k = 1, mv = {1, 1, 6})
/* compiled from: TbtViewContainer.kt */
public final class TbtViewContainer extends android.widget.FrameLayout implements com.navdy.hud.app.ui.component.homescreen.IHomeScreenLifecycle {
    public static final com.navdy.hud.app.ui.component.tbt.TbtViewContainer.Companion Companion = new com.navdy.hud.app.ui.component.tbt.TbtViewContainer.Companion(null);
    /* access modifiers changed from: private */
    public static final long MANEUVER_PROGRESS_ANIMATION_DURATION = 250;
    /* access modifiers changed from: private */
    public static final long MANEUVER_SWAP_DELAY = MANEUVER_SWAP_DELAY;
    /* access modifiers changed from: private */
    public static final long MANEUVER_SWAP_DURATION = MANEUVER_SWAP_DURATION;
    /* access modifiers changed from: private */
    @org.jetbrains.annotations.NotNull
    public static final com.squareup.otto.Bus bus;
    /* access modifiers changed from: private */
    public static final float itemHeight = Companion.getResources().getDimension(com.navdy.hud.app.R.dimen.tbt_view_ht);
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger("TbtViewContainer");
    /* access modifiers changed from: private */
    public static final android.view.animation.AccelerateDecelerateInterpolator maneuverSwapInterpolator = new android.view.animation.AccelerateDecelerateInterpolator();
    /* access modifiers changed from: private */
    @org.jetbrains.annotations.NotNull
    public static final com.navdy.hud.app.manager.RemoteDeviceManager remoteDeviceManager;
    /* access modifiers changed from: private */
    @org.jetbrains.annotations.NotNull
    public static final android.content.res.Resources resources;
    /* access modifiers changed from: private */
    @org.jetbrains.annotations.NotNull
    public static final com.navdy.hud.app.ui.framework.UIStateManager uiStateManager;
    private java.util.HashMap _$_findViewCache;
    private com.navdy.hud.app.ui.component.tbt.TbtView activeView;
    private com.navdy.hud.app.ui.component.homescreen.HomeScreenView homeScreenView;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.ui.component.tbt.TbtView inactiveView;
    private com.navdy.hud.app.maps.MapEvents.ManeuverDisplay lastEvent;
    private com.navdy.hud.app.maps.MapEvents.ManeuverDisplay lastManeuverDisplay;
    private java.lang.String lastManeuverId;
    private com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.ManeuverState lastManeuverState;
    private com.navdy.hud.app.view.MainView.CustomAnimationMode lastMode;
    private java.lang.String lastRoadText;
    private int lastTurnIconId = -1;
    private java.lang.String lastTurnText;
    private boolean paused;

    @kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0007\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\u00020\u0004X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u0014\u0010\u0007\u001a\u00020\u0004X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\b\u0010\u0006R\u0014\u0010\t\u001a\u00020\u0004X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u0006R\u0011\u0010\u000b\u001a\u00020\f\u00a2\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0011\u0010\u000f\u001a\u00020\u0010\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012R\u0014\u0010\u0013\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0016R\u0014\u0010\u0017\u001a\u00020\u0018X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u001aR\u0011\u0010\u001b\u001a\u00020\u001c\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u001eR\u0011\u0010\u001f\u001a\u00020 \u00a2\u0006\b\n\u0000\u001a\u0004\b!\u0010\"R\u0011\u0010#\u001a\u00020$\u00a2\u0006\b\n\u0000\u001a\u0004\b%\u0010&\u00a8\u0006'"}, d2 = {"Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer$Companion;", "", "()V", "MANEUVER_PROGRESS_ANIMATION_DURATION", "", "getMANEUVER_PROGRESS_ANIMATION_DURATION", "()J", "MANEUVER_SWAP_DELAY", "getMANEUVER_SWAP_DELAY", "MANEUVER_SWAP_DURATION", "getMANEUVER_SWAP_DURATION", "bus", "Lcom/squareup/otto/Bus;", "getBus", "()Lcom/squareup/otto/Bus;", "itemHeight", "", "getItemHeight", "()F", "logger", "Lcom/navdy/service/library/log/Logger;", "getLogger", "()Lcom/navdy/service/library/log/Logger;", "maneuverSwapInterpolator", "Landroid/view/animation/AccelerateDecelerateInterpolator;", "getManeuverSwapInterpolator", "()Landroid/view/animation/AccelerateDecelerateInterpolator;", "remoteDeviceManager", "Lcom/navdy/hud/app/manager/RemoteDeviceManager;", "getRemoteDeviceManager", "()Lcom/navdy/hud/app/manager/RemoteDeviceManager;", "resources", "Landroid/content/res/Resources;", "getResources", "()Landroid/content/res/Resources;", "uiStateManager", "Lcom/navdy/hud/app/ui/framework/UIStateManager;", "getUiStateManager", "()Lcom/navdy/hud/app/ui/framework/UIStateManager;", "app_hudDebug"}, k = 1, mv = {1, 1, 6})
    /* compiled from: TbtViewContainer.kt */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(kotlin.jvm.internal.DefaultConstructorMarker $constructor_marker) {
            this();
        }

        /* access modifiers changed from: private */
        public final com.navdy.service.library.log.Logger getLogger() {
            return com.navdy.hud.app.ui.component.tbt.TbtViewContainer.logger;
        }

        @org.jetbrains.annotations.NotNull
        public final android.content.res.Resources getResources() {
            return com.navdy.hud.app.ui.component.tbt.TbtViewContainer.resources;
        }

        public final float getItemHeight() {
            return com.navdy.hud.app.ui.component.tbt.TbtViewContainer.itemHeight;
        }

        public final long getMANEUVER_PROGRESS_ANIMATION_DURATION() {
            return com.navdy.hud.app.ui.component.tbt.TbtViewContainer.MANEUVER_PROGRESS_ANIMATION_DURATION;
        }

        public final long getMANEUVER_SWAP_DURATION() {
            return com.navdy.hud.app.ui.component.tbt.TbtViewContainer.MANEUVER_SWAP_DURATION;
        }

        public final long getMANEUVER_SWAP_DELAY() {
            return com.navdy.hud.app.ui.component.tbt.TbtViewContainer.MANEUVER_SWAP_DELAY;
        }

        private final android.view.animation.AccelerateDecelerateInterpolator getManeuverSwapInterpolator() {
            return com.navdy.hud.app.ui.component.tbt.TbtViewContainer.maneuverSwapInterpolator;
        }

        @org.jetbrains.annotations.NotNull
        public final com.navdy.hud.app.manager.RemoteDeviceManager getRemoteDeviceManager() {
            return com.navdy.hud.app.ui.component.tbt.TbtViewContainer.remoteDeviceManager;
        }

        @org.jetbrains.annotations.NotNull
        public final com.squareup.otto.Bus getBus() {
            return com.navdy.hud.app.ui.component.tbt.TbtViewContainer.bus;
        }

        @org.jetbrains.annotations.NotNull
        public final com.navdy.hud.app.ui.framework.UIStateManager getUiStateManager() {
            return com.navdy.hud.app.ui.component.tbt.TbtViewContainer.uiStateManager;
        }
    }

    public void _$_clearFindViewByIdCache() {
        if (this._$_findViewCache != null) {
            this._$_findViewCache.clear();
        }
    }

    public android.view.View _$_findCachedViewById(int i) {
        if (this._$_findViewCache == null) {
            this._$_findViewCache = new java.util.HashMap();
        }
        android.view.View view = (android.view.View) this._$_findViewCache.get(java.lang.Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        android.view.View findViewById = findViewById(i);
        this._$_findViewCache.put(java.lang.Integer.valueOf(i), findViewById);
        return findViewById;
    }

    static {
        android.content.res.Resources resources2 = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(resources2, "HudApplication.getAppContext().resources");
        resources = resources2;
        com.navdy.hud.app.manager.RemoteDeviceManager instance = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance();
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(instance, "RemoteDeviceManager.getInstance()");
        remoteDeviceManager = instance;
        com.squareup.otto.Bus bus2 = Companion.getRemoteDeviceManager().getBus();
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(bus2, "remoteDeviceManager.bus");
        bus = bus2;
        com.navdy.hud.app.ui.framework.UIStateManager uiStateManager2 = Companion.getRemoteDeviceManager().getUiStateManager();
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(uiStateManager2, "remoteDeviceManager.uiStateManager");
        uiStateManager = uiStateManager2;
    }

    public TbtViewContainer(@org.jetbrains.annotations.NotNull android.content.Context context) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(context, "context");
        super(context);
    }

    public TbtViewContainer(@org.jetbrains.annotations.NotNull android.content.Context context, @org.jetbrains.annotations.NotNull android.util.AttributeSet attrs) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(context, "context");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(attrs, "attrs");
        super(context, attrs);
    }

    public TbtViewContainer(@org.jetbrains.annotations.NotNull android.content.Context context, @org.jetbrains.annotations.NotNull android.util.AttributeSet attrs, int defStyleAttr) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(context, "context");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(attrs, "attrs");
        super(context, attrs, defStyleAttr);
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
        android.view.View findViewById = findViewById(com.navdy.hud.app.R.id.view1);
        if (findViewById == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type com.navdy.hud.app.ui.component.tbt.TbtView");
        }
        this.activeView = (com.navdy.hud.app.ui.component.tbt.TbtView) findViewById;
        android.view.View findViewById2 = findViewById(com.navdy.hud.app.R.id.view2);
        if (findViewById2 == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type com.navdy.hud.app.ui.component.tbt.TbtView");
        }
        this.inactiveView = (com.navdy.hud.app.ui.component.tbt.TbtView) findViewById2;
        com.navdy.hud.app.ui.component.tbt.TbtView tbtView = this.activeView;
        if (tbtView != null) {
            tbtView.setTranslationY(-Companion.getItemHeight());
        }
    }

    public final void init(@org.jetbrains.annotations.NotNull com.navdy.hud.app.ui.component.homescreen.HomeScreenView homeScreenView2) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(homeScreenView2, "homeScreenView");
        this.homeScreenView = homeScreenView2;
        Companion.getBus().register(this);
    }

    public final void clearState() {
        this.lastTurnIconId = -1;
        this.lastTurnText = null;
        this.lastRoadText = null;
        com.navdy.hud.app.ui.component.tbt.TbtView tbtView = this.activeView;
        if (tbtView != null) {
            tbtView.clear();
        }
        com.navdy.hud.app.ui.component.tbt.TbtView tbtView2 = this.inactiveView;
        if (tbtView2 != null) {
            tbtView2.clear();
        }
    }

    public final void setView(@org.jetbrains.annotations.NotNull com.navdy.hud.app.view.MainView.CustomAnimationMode mode) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(mode, net.hockeyapp.android.LoginActivity.EXTRA_MODE);
        this.lastMode = mode;
        com.navdy.hud.app.ui.component.tbt.TbtView tbtView = this.activeView;
        if (tbtView != null) {
            tbtView.setView(mode);
        }
        com.navdy.hud.app.ui.component.tbt.TbtView tbtView2 = this.inactiveView;
        if (tbtView2 != null) {
            tbtView2.setView(mode);
        }
    }

    public final void getCustomAnimator(@org.jetbrains.annotations.NotNull com.navdy.hud.app.view.MainView.CustomAnimationMode mode, @org.jetbrains.annotations.NotNull android.animation.AnimatorSet.Builder mainBuilder) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(mode, net.hockeyapp.android.LoginActivity.EXTRA_MODE);
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(mainBuilder, "mainBuilder");
        this.lastMode = mode;
        com.navdy.hud.app.ui.component.tbt.TbtView tbtView = this.activeView;
        if (tbtView != null) {
            tbtView.getCustomAnimator(mode, mainBuilder);
        }
        com.navdy.hud.app.ui.component.tbt.TbtView tbtView2 = this.inactiveView;
        if (tbtView2 != null) {
            tbtView2.getCustomAnimator(mode, mainBuilder);
        }
    }

    @com.squareup.otto.Subscribe
    public final void updateDisplay(@org.jetbrains.annotations.NotNull com.navdy.hud.app.maps.MapEvents.ManeuverDisplay event) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(event, "event");
        this.lastManeuverDisplay = event;
        com.navdy.hud.app.ui.component.homescreen.HomeScreenView homeScreenView2 = this.homeScreenView;
        if (!(homeScreenView2 != null ? homeScreenView2.isNavigationActive() : false)) {
            return;
        }
        if (event.empty) {
            clearState();
        } else if (event.pendingTurn == null) {
            Companion.getLogger().v("null pending turn");
            this.lastEvent = null;
            this.lastManeuverId = null;
        } else {
            this.lastEvent = event;
            if (this.paused) {
                this.lastManeuverId = event.maneuverId;
                return;
            }
            if (!android.text.TextUtils.equals(this.lastManeuverId, event.maneuverId)) {
                com.navdy.hud.app.ui.component.tbt.TbtView temp = this.activeView;
                this.activeView = this.inactiveView;
                this.inactiveView = temp;
                com.navdy.hud.app.ui.component.tbt.TbtView tbtView = this.activeView;
                if (tbtView != null) {
                    tbtView.setTranslationY(-Companion.getItemHeight());
                }
                com.navdy.hud.app.ui.component.tbt.TbtView tbtView2 = this.activeView;
                if (tbtView2 != null) {
                    tbtView2.updateDisplay(event);
                }
                com.navdy.hud.app.ui.component.tbt.TbtView tbtView3 = this.activeView;
                if (tbtView3 != null) {
                    android.view.ViewPropertyAnimator animate = tbtView3.animate();
                    if (animate != null) {
                        android.view.ViewPropertyAnimator translationY = animate.translationY(0.0f);
                        if (translationY != null) {
                            android.view.ViewPropertyAnimator duration = translationY.setDuration(Companion.getMANEUVER_SWAP_DURATION());
                            if (duration != null) {
                                android.view.ViewPropertyAnimator startDelay = duration.setStartDelay(Companion.getMANEUVER_SWAP_DELAY());
                                if (startDelay != null) {
                                    startDelay.start();
                                }
                            }
                        }
                    }
                }
                com.navdy.hud.app.ui.component.tbt.TbtView tbtView4 = this.inactiveView;
                if (tbtView4 != null) {
                    android.view.ViewPropertyAnimator animate2 = tbtView4.animate();
                    if (animate2 != null) {
                        android.view.ViewPropertyAnimator translationY2 = animate2.translationY(Companion.getItemHeight());
                        if (translationY2 != null) {
                            android.view.ViewPropertyAnimator duration2 = translationY2.setDuration(Companion.getMANEUVER_SWAP_DURATION());
                            if (duration2 != null) {
                                android.view.ViewPropertyAnimator startDelay2 = duration2.setStartDelay(Companion.getMANEUVER_SWAP_DELAY());
                                if (startDelay2 != null) {
                                    android.view.ViewPropertyAnimator listener = startDelay2.setListener(new com.navdy.hud.app.ui.component.tbt.TbtViewContainer$updateDisplay$Anon1(this));
                                    if (listener != null) {
                                        listener.start();
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                com.navdy.hud.app.ui.component.tbt.TbtView tbtView5 = this.activeView;
                if (tbtView5 != null) {
                    tbtView5.updateDisplay(event);
                }
            }
            this.lastManeuverId = event.maneuverId;
            this.lastManeuverState = event.maneuverState;
        }
    }

    public void onPause() {
        if (!this.paused) {
            this.paused = true;
            Companion.getLogger().v("::onPause:tbt");
        }
    }

    public void onResume() {
        if (this.paused) {
            this.paused = false;
            Companion.getLogger().v("::onResume:tbt");
            com.navdy.hud.app.maps.MapEvents.ManeuverDisplay event = this.lastEvent;
            if (event != null) {
                this.lastEvent = null;
                Companion.getLogger().v("::onResume:tbt updated last event");
                updateDisplay(event);
            }
        }
    }

    @org.jetbrains.annotations.Nullable
    public final com.navdy.hud.app.maps.MapEvents.ManeuverDisplay getLastManeuverDisplay() {
        return this.lastManeuverDisplay;
    }
}
