package com.navdy.hud.app.ui.component.homescreen;

public class TbtView extends android.widget.RelativeLayout implements com.navdy.hud.app.ui.component.homescreen.IHomeScreenLifecycle {
    static final long MANEUVER_PROGRESS_ANIMATION_DURATION = 250;
    private static final long MANEUVER_SWAP_DURATION = 500;
    private static final android.animation.TimeInterpolator maneuverSwapInterpolator = new android.view.animation.AccelerateDecelerateInterpolator();
    @butterknife.InjectView(2131624378)
    android.widget.TextView activeMapDistance;
    @butterknife.InjectView(2131624380)
    android.widget.ImageView activeMapIcon;
    @butterknife.InjectView(2131624384)
    android.widget.TextView activeMapInstruction;
    private com.squareup.otto.Bus bus;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.ui.component.homescreen.HomeScreenView homeScreenView;
    private long initialProgressBarDistance;
    private java.lang.String lastDistance;
    private com.navdy.hud.app.maps.MapEvents.ManeuverDisplay lastEvent;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.ManeuverState lastManeuverState;
    private com.navdy.hud.app.view.MainView.CustomAnimationMode lastMode;
    private java.lang.String lastRoadText;
    private int lastTurnIconId;
    private java.lang.String lastTurnText;
    private com.navdy.service.library.log.Logger logger;
    @butterknife.InjectView(2131624383)
    android.widget.ImageView nextMapIcon;
    @butterknife.InjectView(2131624382)
    android.view.View nextMapIconContainer;
    @butterknife.InjectView(2131624386)
    android.widget.TextView nextMapInstruction;
    @butterknife.InjectView(2131624385)
    android.view.View nextMapInstructionContainer;
    @butterknife.InjectView(2131624379)
    android.view.View nowIcon;
    private boolean paused;
    @butterknife.InjectView(2131624381)
    android.widget.ProgressBar progressBar;
    private com.navdy.hud.app.ui.framework.UIStateManager uiStateManager;

    class Anon1 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
        Anon1() {
        }

        public void onAnimationStart(android.animation.Animator animation) {
        }

        public void onAnimationEnd(android.animation.Animator animation) {
        }
    }

    class Anon2 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
        Anon2() {
        }

        public void onAnimationStart(android.animation.Animator animation) {
        }

        public void onAnimationEnd(android.animation.Animator animation) {
            if (com.navdy.hud.app.ui.component.homescreen.TbtView.this.homeScreenView.isNavigationActive() && !com.navdy.hud.app.ui.component.homescreen.TbtView.this.homeScreenView.isRecalculating() && com.navdy.hud.app.ui.component.homescreen.TbtView.this.lastManeuverState != com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.ManeuverState.NOW) {
                com.navdy.hud.app.ui.component.homescreen.TbtView.this.progressBar.setVisibility(0);
                com.navdy.hud.app.ui.component.homescreen.TbtView.this.activeMapDistance.setVisibility(0);
            }
        }
    }

    class Anon3 implements android.animation.Animator.AnimatorListener {
        final /* synthetic */ int val$imageResourceId;

        Anon3(int i) {
            this.val$imageResourceId = i;
        }

        public void onAnimationStart(android.animation.Animator animator) {
        }

        public void onAnimationEnd(android.animation.Animator animator) {
            com.navdy.hud.app.ui.component.homescreen.TbtView.this.activeMapIcon.setImageResource(this.val$imageResourceId);
            com.navdy.hud.app.ui.component.homescreen.TbtView.this.activeMapIcon.setTranslationY(0.0f);
            com.navdy.hud.app.ui.component.homescreen.TbtView.this.nextMapIconContainer.setTranslationY(0.0f);
            com.navdy.hud.app.ui.component.homescreen.TbtView.this.nextMapIconContainer.setVisibility(8);
        }

        public void onAnimationCancel(android.animation.Animator animator) {
        }

        public void onAnimationRepeat(android.animation.Animator animator) {
        }
    }

    class Anon4 implements android.animation.Animator.AnimatorListener {
        final /* synthetic */ android.text.SpannableStringBuilder val$spannableStringBuilder;

        Anon4(android.text.SpannableStringBuilder spannableStringBuilder) {
            this.val$spannableStringBuilder = spannableStringBuilder;
        }

        public void onAnimationStart(android.animation.Animator animator) {
        }

        public void onAnimationEnd(android.animation.Animator animator) {
            com.navdy.hud.app.ui.component.homescreen.TbtView.this.activeMapInstruction.setText(this.val$spannableStringBuilder);
            com.navdy.hud.app.ui.component.homescreen.TbtView.this.activeMapInstruction.setTranslationY(0.0f);
            com.navdy.hud.app.ui.component.homescreen.TbtView.this.nextMapInstructionContainer.setTranslationY(0.0f);
            com.navdy.hud.app.ui.component.homescreen.TbtView.this.nextMapInstructionContainer.setVisibility(8);
        }

        public void onAnimationCancel(android.animation.Animator animator) {
        }

        public void onAnimationRepeat(android.animation.Animator animator) {
        }
    }

    public TbtView(android.content.Context context) {
        this(context, null);
    }

    public TbtView(android.content.Context context, android.util.AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TbtView(android.content.Context context, android.util.AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.lastTurnIconId = -1;
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        this.logger = com.navdy.hud.app.ui.component.homescreen.HomeScreenView.sLogger;
        super.onFinishInflate();
        butterknife.ButterKnife.inject((android.view.View) this);
        com.navdy.hud.app.manager.RemoteDeviceManager remoteDeviceManager = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance();
        this.bus = remoteDeviceManager.getBus();
        this.uiStateManager = remoteDeviceManager.getUiStateManager();
        setLayoutTransition(new android.animation.LayoutTransition());
    }

    public void init(com.navdy.hud.app.ui.component.homescreen.HomeScreenView homeScreenView2) {
        this.homeScreenView = homeScreenView2;
        this.bus.register(this);
    }

    public void clearState() {
        this.lastTurnIconId = -1;
        this.lastDistance = null;
        this.lastTurnText = null;
        this.lastRoadText = null;
        cleanInstructions();
    }

    public void setView(com.navdy.hud.app.view.MainView.CustomAnimationMode mode) {
        this.lastMode = mode;
        android.view.ViewGroup.MarginLayoutParams margins = (android.view.ViewGroup.MarginLayoutParams) this.activeMapInstruction.getLayoutParams();
        android.view.ViewGroup.MarginLayoutParams nextMargins = (android.view.ViewGroup.MarginLayoutParams) this.nextMapInstructionContainer.getLayoutParams();
        switch (mode) {
            case EXPAND:
                if (this.lastManeuverState == com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.ManeuverState.NOW) {
                    this.nowIcon.setScaleX(1.0f);
                    this.nowIcon.setScaleY(1.0f);
                    this.nowIcon.setAlpha(1.0f);
                }
                this.activeMapDistance.setX((float) com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadInfoDistanceX);
                this.activeMapIcon.setX((float) com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadInfoIconX);
                this.nextMapIconContainer.setX((float) com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadInfoIconX);
                this.activeMapInstruction.setX((float) com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadInfoInstructionX);
                this.nextMapInstructionContainer.setX((float) com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadInfoInstructionX);
                margins.width = com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadInfoInstructionWidth;
                nextMargins.width = com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadInfoInstructionWidth;
                return;
            case SHRINK_LEFT:
                this.nowIcon.setScaleX(0.9f);
                this.nowIcon.setScaleY(0.9f);
                this.nowIcon.setAlpha(0.0f);
                this.activeMapDistance.setX((float) com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadInfoDistanceX);
                this.activeMapIcon.setX((float) com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadInfoIconShrinkLeftX);
                this.nextMapIconContainer.setX((float) com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadInfoIconShrinkLeftX);
                this.activeMapInstruction.setX((float) com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadInfoInstructionShrinkLeftX);
                this.nextMapInstructionContainer.setX((float) com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadInfoInstructionShrinkLeftX);
                android.view.ViewGroup.MarginLayoutParams nextMargins2 = (android.view.ViewGroup.MarginLayoutParams) this.nextMapInstructionContainer.getLayoutParams();
                ((android.view.ViewGroup.MarginLayoutParams) this.activeMapInstruction.getLayoutParams()).width = com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadInfoInstructionShrinkWidth;
                nextMargins2.width = com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadInfoInstructionShrinkWidth;
                return;
            default:
                return;
        }
    }

    public void getCustomAnimator(com.navdy.hud.app.view.MainView.CustomAnimationMode mode, android.animation.AnimatorSet.Builder mainBuilder) {
        this.lastMode = mode;
        switch (mode) {
            case EXPAND:
                android.animation.AnimatorSet animatorSet = new android.animation.AnimatorSet();
                android.animation.AnimatorSet.Builder builder = animatorSet.play(com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getXPositionAnimator(this.activeMapIcon, (float) com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadInfoIconX));
                builder.with(com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getXPositionAnimator(this.nextMapIconContainer, (float) com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadInfoIconX));
                android.animation.ObjectAnimator activeInstructionXAnimator = com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getXPositionAnimator(this.activeMapInstruction, (float) com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadInfoInstructionX);
                android.animation.ValueAnimator activeInstructionWidthAnimator = com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getWidthAnimator(this.activeMapInstruction, com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadInfoInstructionWidth);
                builder.with(activeInstructionXAnimator);
                builder.with(activeInstructionWidthAnimator);
                android.animation.ObjectAnimator nextInstructionXAnimator = com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getXPositionAnimator(this.nextMapInstructionContainer, (float) com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadInfoInstructionX);
                android.animation.ValueAnimator nextInstructionWidthAnimator = com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getWidthAnimator(this.nextMapInstructionContainer, com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadInfoInstructionWidth);
                builder.with(nextInstructionXAnimator);
                builder.with(nextInstructionWidthAnimator);
                if (this.lastManeuverState == com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.ManeuverState.NOW) {
                    builder.with(com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getFadeInAndScaleUpAnimator(this.nowIcon));
                } else {
                    builder.with(com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getAlphaAnimator(this.activeMapDistance, 1));
                }
                animatorSet.addListener(new com.navdy.hud.app.ui.component.homescreen.TbtView.Anon2());
                mainBuilder.with(animatorSet);
                return;
            case SHRINK_LEFT:
                android.animation.AnimatorSet animatorSet2 = new android.animation.AnimatorSet();
                android.animation.AnimatorSet.Builder builder2 = animatorSet2.play(com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getXPositionAnimator(this.activeMapIcon, (float) com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadInfoIconShrinkLeftX));
                builder2.with(com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getXPositionAnimator(this.nextMapIconContainer, (float) com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadInfoIconShrinkLeftX));
                android.animation.ObjectAnimator activeInstructionXAnimator2 = com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getXPositionAnimator(this.activeMapInstruction, (float) com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadInfoInstructionShrinkLeftX);
                android.animation.ValueAnimator activeInstructionWidthAnimator2 = com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getWidthAnimator(this.activeMapInstruction, com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadInfoInstructionShrinkWidth);
                builder2.with(activeInstructionXAnimator2);
                builder2.with(activeInstructionWidthAnimator2);
                android.animation.ObjectAnimator nextInstructionXAnimator2 = com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getXPositionAnimator(this.nextMapInstructionContainer, (float) com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadInfoInstructionShrinkLeftX);
                android.animation.ValueAnimator nextInstructionWidthAnimator2 = com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getWidthAnimator(this.nextMapInstructionContainer, com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadInfoInstructionShrinkWidth);
                builder2.with(nextInstructionXAnimator2);
                builder2.with(nextInstructionWidthAnimator2);
                if (this.lastManeuverState == com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.ManeuverState.NOW) {
                    builder2.with(com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getFadeOutAndScaleDownAnimator(this.nowIcon));
                } else {
                    builder2.with(com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getAlphaAnimator(this.activeMapDistance, 0));
                }
                animatorSet2.addListener(new com.navdy.hud.app.ui.component.homescreen.TbtView.Anon1());
                mainBuilder.with(animatorSet2);
                return;
            default:
                return;
        }
    }

    public void showHideActiveDistance() {
        if (!this.uiStateManager.isMainUIShrunk()) {
            this.activeMapDistance.setVisibility(0);
            this.progressBar.setVisibility(0);
            return;
        }
        this.activeMapDistance.setVisibility(4);
        this.progressBar.setVisibility(4);
    }

    @com.squareup.otto.Subscribe
    public void updateDisplay(com.navdy.hud.app.maps.MapEvents.ManeuverDisplay event) {
        if (!this.homeScreenView.isNavigationActive()) {
            return;
        }
        if (event.empty) {
            clearState();
        } else if (event.pendingTurn == null) {
            this.logger.v("null pending turn");
            this.lastEvent = null;
        } else {
            this.lastEvent = event;
            if (!this.paused) {
                boolean hasSameInstruction = android.text.TextUtils.equals(event.pendingTurn, this.lastTurnText) && android.text.TextUtils.equals(event.pendingRoad, this.lastRoadText);
                setDistance(event.distanceToPendingRoadText, event.maneuverState);
                setProgressBar(event.maneuverState, event.distanceInMeters);
                android.animation.AnimatorSet setIcon = setIcon(event.turnIconId, hasSameInstruction);
                android.animation.AnimatorSet setInstruction = setInstruction(event.pendingTurn, event.pendingRoad);
                android.animation.AnimatorSet animation = null;
                if (setIcon != null && setInstruction != null) {
                    animation = new android.animation.AnimatorSet();
                    animation.play(setIcon).with(setInstruction);
                } else if (setIcon != null) {
                    animation = setIcon;
                } else if (setInstruction != null) {
                    animation = setInstruction;
                }
                if (animation != null) {
                    animation.setDuration(MANEUVER_SWAP_DURATION).setInterpolator(maneuverSwapInterpolator);
                    animation.start();
                }
                this.lastManeuverState = event.maneuverState;
            }
        }
    }

    private void cleanInstructions() {
        this.activeMapDistance.setText("");
        com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getFadeOutAndScaleDownAnimator(this.nowIcon).setDuration(250).start();
        com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.setWidth(this.progressBar, 0);
        this.activeMapIcon.setImageDrawable(null);
        this.nextMapIcon.setImageDrawable(null);
        this.activeMapInstruction.setText("");
        this.nextMapInstruction.setText("");
    }

    @android.support.annotation.Nullable
    private android.animation.AnimatorSet setIcon(int imageResourceId, boolean hasSameInstruction) {
        if (this.lastTurnIconId == imageResourceId) {
            return null;
        }
        this.lastTurnIconId = imageResourceId;
        if (imageResourceId == -1) {
            this.activeMapIcon.setImageDrawable(null);
            return null;
        }
        this.nextMapIcon.setImageResource(imageResourceId);
        if (!hasSameInstruction) {
            return animateSetIcon(imageResourceId);
        }
        this.activeMapIcon.setImageResource(imageResourceId);
        return null;
    }

    @android.support.annotation.Nullable
    private android.animation.AnimatorSet animateSetIcon(int imageResourceId) {
        this.nextMapIconContainer.setVisibility(0);
        android.animation.ObjectAnimator sendActiveDownAnim = com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getTranslationYPositionAnimator(this.activeMapIcon, com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadInfoInstructionTranslationYAnimation);
        android.animation.ObjectAnimator sendNextDownAnim = com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getTranslationYPositionAnimator(this.nextMapIconContainer, com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadInfoInstructionTranslationYAnimation);
        sendActiveDownAnim.addListener(new com.navdy.hud.app.ui.component.homescreen.TbtView.Anon3(imageResourceId));
        android.animation.AnimatorSet swapIcons = new android.animation.AnimatorSet();
        swapIcons.play(sendActiveDownAnim).with(sendNextDownAnim);
        return swapIcons;
    }

    private void setDistance(java.lang.String text, com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.ManeuverState maneuverState) {
        if (!android.text.TextUtils.equals(text, this.lastDistance)) {
            this.lastDistance = text;
            this.activeMapDistance.setText(text);
            if (this.activeMapDistance.getAlpha() == 0.0f && this.nowIcon.getAlpha() == 0.0f && !this.uiStateManager.isMainUIShrunk()) {
                this.activeMapDistance.setAlpha(1.0f);
            }
            animateSetDistance(maneuverState);
        }
    }

    private void animateSetDistance(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.ManeuverState maneuverState) {
        boolean isTransitioningToNowState;
        boolean isTransitioningFromNowState;
        if (this.lastManeuverState == com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.ManeuverState.NOW || maneuverState != com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.ManeuverState.NOW) {
            isTransitioningToNowState = false;
        } else {
            isTransitioningToNowState = true;
        }
        if (this.lastManeuverState != com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.ManeuverState.NOW || maneuverState == com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.ManeuverState.NOW) {
            isTransitioningFromNowState = false;
        } else {
            isTransitioningFromNowState = true;
        }
        if (isTransitioningToNowState) {
            android.animation.AnimatorSet fadeInAndScaleUpAnimator = com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getFadeInAndScaleUpAnimator(this.nowIcon);
            android.animation.ObjectAnimator fadeOutActiveMapDistanceAnimator = com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getAlphaAnimator(this.activeMapDistance, 0);
            android.animation.AnimatorSet animatorSet = new android.animation.AnimatorSet();
            animatorSet.play(fadeInAndScaleUpAnimator).with(fadeOutActiveMapDistanceAnimator);
            animatorSet.setDuration(250).start();
        } else if (isTransitioningFromNowState) {
            android.animation.AnimatorSet fadeOutAndScaleDownAnimator = com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getFadeOutAndScaleDownAnimator(this.nowIcon);
            android.animation.ObjectAnimator fadeInActiveMapDistanceAnimator = com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getAlphaAnimator(this.activeMapDistance, 1);
            android.animation.AnimatorSet animatorSet2 = new android.animation.AnimatorSet();
            animatorSet2.play(fadeOutAndScaleDownAnimator).with(fadeInActiveMapDistanceAnimator);
            animatorSet2.setDuration(250).start();
        }
    }

    private android.animation.AnimatorSet setInstruction(java.lang.String turnText, java.lang.String roadText) {
        if (android.text.TextUtils.equals(turnText, this.lastTurnText) && android.text.TextUtils.equals(roadText, this.lastRoadText)) {
            return null;
        }
        this.lastTurnText = turnText;
        this.lastRoadText = roadText;
        android.text.SpannableStringBuilder spannableStringBuilder = new android.text.SpannableStringBuilder();
        if (this.lastTurnText != null && com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.canShowTurnText(this.lastTurnText)) {
            spannableStringBuilder.append(this.lastTurnText);
            spannableStringBuilder.append(" ");
        }
        if (this.lastRoadText != null) {
            spannableStringBuilder.setSpan(new android.text.style.StyleSpan(1), 0, spannableStringBuilder.length(), 33);
            spannableStringBuilder.append(this.lastRoadText);
        }
        return animateSetInstruction(spannableStringBuilder);
    }

    private android.animation.AnimatorSet animateSetInstruction(android.text.SpannableStringBuilder spannableStringBuilder) {
        this.nextMapInstructionContainer.setVisibility(0);
        this.nextMapInstruction.setText(spannableStringBuilder);
        android.animation.ObjectAnimator sendActiveDownAnim = com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getTranslationYPositionAnimator(this.activeMapInstruction, com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadInfoInstructionTranslationYAnimation);
        android.animation.ObjectAnimator sendNextDownAnim = com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getTranslationYPositionAnimator(this.nextMapInstructionContainer, com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadInfoInstructionTranslationYAnimation);
        sendActiveDownAnim.addListener(new com.navdy.hud.app.ui.component.homescreen.TbtView.Anon4(spannableStringBuilder));
        android.animation.AnimatorSet swapInstructions = new android.animation.AnimatorSet();
        swapInstructions.play(sendActiveDownAnim).with(sendNextDownAnim);
        return swapInstructions;
    }

    private void setProgressBar(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.ManeuverState maneuverState, long distanceInMeters) {
        boolean isTransitioningToApproachingState;
        boolean isTransitioningFromApproachingState;
        if (this.lastManeuverState == com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.ManeuverState.SOON || maneuverState != com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.ManeuverState.SOON) {
            isTransitioningToApproachingState = false;
        } else {
            isTransitioningToApproachingState = true;
        }
        if (this.lastManeuverState != com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.ManeuverState.SOON || maneuverState == com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.ManeuverState.SOON) {
            isTransitioningFromApproachingState = false;
        } else {
            isTransitioningFromApproachingState = true;
        }
        if (isTransitioningFromApproachingState) {
            com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.setWidth(this.progressBar, 0);
            this.initialProgressBarDistance = 0;
        } else if (isTransitioningToApproachingState) {
            com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.setWidth(this.progressBar, com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadProgressBarWidth);
            this.initialProgressBarDistance = distanceInMeters;
        } else if (maneuverState == com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.ManeuverState.SOON) {
            com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getProgressBarAnimator(this.progressBar, (int) (100.0d * (1.0d - (((double) distanceInMeters) / ((double) this.initialProgressBarDistance))))).setDuration(250).start();
        }
    }

    public void onPause() {
        if (!this.paused) {
            this.paused = true;
            this.logger.v("::onPause:tbt");
        }
    }

    public void onResume() {
        if (this.paused) {
            this.paused = false;
            this.logger.v("::onResume:tbt");
            com.navdy.hud.app.maps.MapEvents.ManeuverDisplay event = this.lastEvent;
            if (event != null) {
                this.lastEvent = null;
                this.logger.v("::onResume:tbt updated last event");
                updateDisplay(event);
            }
        }
    }
}
