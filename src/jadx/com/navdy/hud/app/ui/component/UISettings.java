package com.navdy.hud.app.ui.component;

public class UISettings {
    private static final java.lang.String ADVANCED_GPS_STATS = "persist.sys.gps.stats";
    private static final java.lang.String DIAL_LONG_PRESS_ACTION_PLACE_SEARCH = "persist.sys.dlpress_search";
    private static final boolean dialLongPressPlaceSearchAction = com.navdy.hud.app.util.os.SystemProperties.getBoolean(DIAL_LONG_PRESS_ACTION_PLACE_SEARCH, false);

    public static boolean isMusicBrowsingEnabled() {
        return true;
    }

    public static boolean isVerticalListNoCloseTimeout() {
        return true;
    }

    public static boolean isLongPressActionPlaceSearch() {
        return dialLongPressPlaceSearchAction;
    }

    public static boolean supportsIosSms() {
        return true;
    }

    public static boolean advancedGpsStatsEnabled() {
        return com.navdy.hud.app.util.os.SystemProperties.getBoolean(ADVANCED_GPS_STATS, false);
    }
}
