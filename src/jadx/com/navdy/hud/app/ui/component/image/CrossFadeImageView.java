package com.navdy.hud.app.ui.component.image;

public class CrossFadeImageView extends android.widget.FrameLayout {
    android.view.View big;
    com.navdy.hud.app.ui.component.image.CrossFadeImageView.Mode mode;
    android.view.View small;
    float smallAlpha;
    boolean smallAlphaSet;

    class Anon1 implements android.animation.ValueAnimator.AnimatorUpdateListener {
        Anon1() {
        }

        public void onAnimationUpdate(android.animation.ValueAnimator animation) {
            com.navdy.hud.app.ui.component.image.CrossFadeImageView.this.big.setAlpha(((java.lang.Float) animation.getAnimatedValue()).floatValue());
        }
    }

    class Anon2 implements android.animation.ValueAnimator.AnimatorUpdateListener {
        Anon2() {
        }

        public void onAnimationUpdate(android.animation.ValueAnimator animation) {
            com.navdy.hud.app.ui.component.image.CrossFadeImageView.this.small.setAlpha(((java.lang.Float) animation.getAnimatedValue()).floatValue());
        }
    }

    class Anon3 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
        Anon3() {
        }

        public void onAnimationEnd(android.animation.Animator animation) {
            if (com.navdy.hud.app.ui.component.image.CrossFadeImageView.this.mode == com.navdy.hud.app.ui.component.image.CrossFadeImageView.Mode.BIG) {
                com.navdy.hud.app.ui.component.image.CrossFadeImageView.this.mode = com.navdy.hud.app.ui.component.image.CrossFadeImageView.Mode.SMALL;
                return;
            }
            com.navdy.hud.app.ui.component.image.CrossFadeImageView.this.mode = com.navdy.hud.app.ui.component.image.CrossFadeImageView.Mode.BIG;
        }
    }

    public enum Mode {
        BIG,
        SMALL
    }

    public CrossFadeImageView(android.content.Context context) {
        this(context, null);
    }

    public CrossFadeImageView(android.content.Context context, android.util.AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CrossFadeImageView(android.content.Context context, android.util.AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void inject(com.navdy.hud.app.ui.component.image.CrossFadeImageView.Mode mode2) {
        if (this.big == null) {
            this.big = findViewById(com.navdy.hud.app.R.id.big);
            this.small = findViewById(com.navdy.hud.app.R.id.small);
            this.mode = mode2;
            if (mode2 == com.navdy.hud.app.ui.component.image.CrossFadeImageView.Mode.BIG) {
                this.big.setAlpha(1.0f);
                this.small.setAlpha(0.0f);
                return;
            }
            if (!this.smallAlphaSet) {
                this.small.setAlpha(1.0f);
            } else {
                this.small.setAlpha(this.smallAlpha);
            }
            this.big.setAlpha(0.0f);
        }
    }

    public android.view.View getBig() {
        return this.big;
    }

    public android.view.View getSmall() {
        return this.small;
    }

    public android.animation.AnimatorSet getCrossFadeAnimator() {
        float bigFrom;
        float bigTo;
        float smallFrom;
        float smallTo;
        android.animation.AnimatorSet set = new android.animation.AnimatorSet();
        if (this.mode == com.navdy.hud.app.ui.component.image.CrossFadeImageView.Mode.BIG) {
            bigFrom = 1.0f;
            bigTo = 0.0f;
            smallFrom = 0.0f;
            if (!this.smallAlphaSet) {
                smallTo = 1.0f;
            } else {
                smallTo = this.smallAlpha;
            }
        } else {
            bigFrom = 0.0f;
            bigTo = 1.0f;
            smallFrom = 1.0f;
            smallTo = 0.0f;
        }
        android.animation.ObjectAnimator a = android.animation.ObjectAnimator.ofFloat(this.big, android.view.View.ALPHA, new float[]{bigFrom, bigTo});
        a.addUpdateListener(new com.navdy.hud.app.ui.component.image.CrossFadeImageView.Anon1());
        android.animation.AnimatorSet.Builder builder = set.play(a);
        android.animation.ObjectAnimator a2 = android.animation.ObjectAnimator.ofFloat(this.small, android.view.View.ALPHA, new float[]{smallFrom, smallTo});
        a2.addUpdateListener(new com.navdy.hud.app.ui.component.image.CrossFadeImageView.Anon2());
        builder.with(a2);
        set.addListener(new com.navdy.hud.app.ui.component.image.CrossFadeImageView.Anon3());
        return set;
    }

    public void setMode(com.navdy.hud.app.ui.component.image.CrossFadeImageView.Mode newMode) {
        if (this.mode != newMode) {
            if (newMode == com.navdy.hud.app.ui.component.image.CrossFadeImageView.Mode.BIG) {
                this.big.setAlpha(1.0f);
                this.small.setAlpha(0.0f);
            } else {
                if (!this.smallAlphaSet) {
                    this.small.setAlpha(1.0f);
                } else {
                    this.small.setAlpha(this.smallAlpha);
                }
                this.big.setAlpha(0.0f);
            }
            this.mode = newMode;
        }
    }

    public com.navdy.hud.app.ui.component.image.CrossFadeImageView.Mode getMode() {
        return this.mode;
    }

    public void setSmallAlpha(float alpha) {
        if (alpha == -1.0f) {
            this.smallAlphaSet = false;
            this.smallAlpha = 0.0f;
            return;
        }
        this.smallAlphaSet = true;
        this.smallAlpha = alpha;
    }
}
