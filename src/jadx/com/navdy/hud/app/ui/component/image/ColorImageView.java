package com.navdy.hud.app.ui.component.image;

public class ColorImageView extends android.widget.ImageView {
    private int color;
    private android.graphics.Paint paint;

    public ColorImageView(android.content.Context context) {
        this(context, null, 0);
    }

    public ColorImageView(android.content.Context context, android.util.AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ColorImageView(android.content.Context context, android.util.AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        this.paint = new android.graphics.Paint();
        this.paint.setStrokeWidth(0.0f);
        this.paint.setAntiAlias(true);
    }

    public void setColor(int color2) {
        this.color = color2;
        invalidate();
    }

    /* access modifiers changed from: protected */
    public void onDraw(android.graphics.Canvas canvas) {
        super.onDraw(canvas);
        int width = getWidth();
        int height = getHeight();
        canvas.drawColor(0);
        this.paint.setColor(this.color);
        canvas.drawCircle((float) (width / 2), (float) (height / 2), (float) (width / 2), this.paint);
    }
}
