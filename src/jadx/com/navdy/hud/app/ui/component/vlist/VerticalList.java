package com.navdy.hud.app.ui.component.vlist;

public class VerticalList {
    private static final com.navdy.hud.app.ui.component.vlist.VerticalList.Model BLANK_ITEM_BOTTOM = com.navdy.hud.app.ui.component.vlist.viewholder.BlankViewHolder.buildModel();
    private static final com.navdy.hud.app.ui.component.vlist.VerticalList.Model BLANK_ITEM_TOP = com.navdy.hud.app.ui.component.vlist.viewholder.BlankViewHolder.buildModel();
    private static final com.navdy.hud.app.ui.component.vlist.VerticalList.FontSize[] FONT_SIZES = {com.navdy.hud.app.ui.component.vlist.VerticalList.FontSize.FONT_SIZE_26, com.navdy.hud.app.ui.component.vlist.VerticalList.FontSize.FONT_SIZE_26, com.navdy.hud.app.ui.component.vlist.VerticalList.FontSize.FONT_SIZE_22, com.navdy.hud.app.ui.component.vlist.VerticalList.FontSize.FONT_SIZE_22_2, com.navdy.hud.app.ui.component.vlist.VerticalList.FontSize.FONT_SIZE_18, com.navdy.hud.app.ui.component.vlist.VerticalList.FontSize.FONT_SIZE_18_2, com.navdy.hud.app.ui.component.vlist.VerticalList.FontSize.FONT_SIZE_16, com.navdy.hud.app.ui.component.vlist.VerticalList.FontSize.FONT_SIZE_16_2};
    public static final float ICON_BK_COLOR_SCALE_FACTOR = 0.83f;
    public static final float ICON_SCALE_FACTOR = 0.6f;
    public static final int ITEM_BACK_FADE_IN_DURATION = 100;
    public static final int ITEM_INIT_ANIMATION_DURATION = 100;
    public static final int ITEM_MOVE_ANIMATION_DURATION = 230;
    public static final int ITEM_SCROLL_ANIMATION = 150;
    public static final int ITEM_SELECT_ANIMATION_DURATION = 50;
    public static final int LOADING_ADAPTER_POS = 1;
    private static final int MAX_OFF_SCREEN_VIEWS = 5;
    private static final float MAX_SCROLL_BY = 2.0f;
    private static final float SCROLL_BY_INCREMENT = 0.5f;
    private static final int[] SINGLE_LINE_MAX_LINES = {1, 1, 1, 1};
    private static final float START_SCROLL_BY = 1.0f;
    public static final float TEXT_SCALE_FACTOR_16 = 1.0f;
    public static final float TEXT_SCALE_FACTOR_18 = 1.0f;
    public static final float TEXT_SCALE_FACTOR_22 = 0.81f;
    public static final float TEXT_SCALE_FACTOR_26 = 0.69f;
    private static final float[] TITLE_SIZES = {vlistTitle_26, vlistTitle_22, vlistTitle_18, vlistTitle_16};
    private static final int[] TWO_LINE_MAX_LINES = {1, 2, 2, 2};
    public static final android.widget.TextView fontSizeTextView;
    private static android.os.Handler handler = new android.os.Handler(android.os.Looper.getMainLooper());
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.vlist.VerticalList.class);
    private static final java.util.HashMap<com.navdy.hud.app.ui.component.vlist.VerticalList.FontSize, com.navdy.hud.app.ui.component.vlist.VerticalList.FontInfo> tit_subt_map = new java.util.HashMap<>();
    private static final java.util.HashMap<com.navdy.hud.app.ui.component.vlist.VerticalList.FontSize, com.navdy.hud.app.ui.component.vlist.VerticalList.FontInfo> tit_subt_map_2_lines = new java.util.HashMap<>();
    private static final java.util.HashMap<com.navdy.hud.app.ui.component.vlist.VerticalList.FontSize, com.navdy.hud.app.ui.component.vlist.VerticalList.FontInfo> tit_subt_subt2_map = new java.util.HashMap<>();
    public static final int vlistTitleTextW;
    public static final float vlistTitle_16;
    public static final float vlistTitle_16_16_subtitle_top;
    public static final float vlistTitle_16_16_title_top;
    public static final float vlistTitle_16_top_m_2;
    public static final float vlistTitle_16_top_m_3;
    public static final float vlistTitle_18;
    public static final float vlistTitle_18_16_subtitle_top;
    public static final float vlistTitle_18_16_title_top;
    public static final float vlistTitle_18_top_m_2;
    public static final float vlistTitle_18_top_m_3;
    public static final float vlistTitle_22;
    public static final float vlistTitle_22_16_subtitle_top;
    public static final float vlistTitle_22_16_title_top;
    public static final float vlistTitle_22_18_subtitle_top;
    public static final float vlistTitle_22_18_title_top;
    public static final float vlistTitle_22_top_m_2;
    public static final float vlistTitle_22_top_m_3;
    public static final float vlistTitle_26;
    public static final float vlistTitle_26_16_subtitle_top;
    public static final float vlistTitle_26_16_title_top;
    public static final float vlistTitle_26_18_subtitle_top;
    public static final float vlistTitle_26_18_title_top;
    public static final float vlistTitle_26_top_m_2;
    public static final float vlistTitle_26_top_m_3;
    public static final float vlistsubTitle2_16_top_m_3;
    public static final float vlistsubTitle2_18_top_m_3;
    public static final float vlistsubTitle2_22_top_m_3;
    public static final float vlistsubTitle2_26_top_m_3;
    public static final float vlistsubTitle_16_top_m_2;
    public static final float vlistsubTitle_16_top_m_3;
    public static final float vlistsubTitle_18_top_m_2;
    public static final float vlistsubTitle_18_top_m_3;
    public static final float vlistsubTitle_22_top_m_2;
    public static final float vlistsubTitle_22_top_m_3;
    public static final float vlistsubTitle_26_top_m_2;
    public static final float vlistsubTitle_26_top_m_3;
    /* access modifiers changed from: private */
    public int actualScrollY;
    public com.navdy.hud.app.ui.component.vlist.VerticalAdapter adapter;
    final int animationDuration;
    volatile boolean bindCallbacks;
    com.navdy.hud.app.ui.component.vlist.VerticalList.Callback callback;
    com.navdy.hud.app.ui.component.vlist.VerticalList.ContainerCallback containerCallback;
    private java.util.ArrayList<android.support.v7.widget.RecyclerView.ViewHolder> copyList;
    /* access modifiers changed from: private */
    public int currentMiddlePosition;
    private volatile float currentScrollBy;
    boolean firstEntryBlank;
    boolean hasScrollableElement;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.ui.component.carousel.CarouselIndicator indicator;
    private com.navdy.hud.app.ui.framework.DefaultAnimationListener indicatorAnimationListener;
    /* access modifiers changed from: private */
    public android.animation.AnimatorSet indicatorAnimatorSet;
    /* access modifiers changed from: private */
    public volatile boolean initialPosChanged;
    private java.lang.Runnable initialPosCheckRunnable;
    /* access modifiers changed from: private */
    public volatile boolean isScrollBy;
    /* access modifiers changed from: private */
    public volatile boolean isScrolling;
    /* access modifiers changed from: private */
    public volatile boolean isSelectedOperationPending;
    private final com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState itemSelectionState;
    private final com.navdy.hud.app.ui.component.vlist.VerticalList.KeyHandlerState keyHandlerState;
    /* access modifiers changed from: private */
    public int lastScrollState;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.ui.component.vlist.VerticalLayoutManager layoutManager;
    private volatile boolean lockList;
    private java.util.List<com.navdy.hud.app.ui.component.vlist.VerticalList.Model> modelList;
    boolean reCalcScrollPos;
    private com.navdy.hud.app.ui.component.vlist.VerticalRecyclerView recyclerView;
    /* access modifiers changed from: private */
    public volatile int scrollByUpReceived;
    int scrollItemEndY;
    int scrollItemHeight;
    int scrollItemIndex;
    int scrollItemStartY;
    private android.support.v7.widget.RecyclerView.OnScrollListener scrollListener;
    /* access modifiers changed from: private */
    public volatile int scrollPendingPos;
    public java.util.HashSet<java.lang.Integer> selectedList;
    boolean sendScrollIdleEvent;
    /* access modifiers changed from: private */
    public boolean switchingScrollBoundary;
    private boolean targetFound;
    public int targetPos;
    private boolean twoLineTitles;
    public boolean waitForTarget;

    class Anon1 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
        Anon1() {
        }

        public void onAnimationEnd(android.animation.Animator animation) {
            if (com.navdy.hud.app.ui.component.vlist.VerticalList.this.containerCallback == null || !com.navdy.hud.app.ui.component.vlist.VerticalList.this.containerCallback.isFastScrolling()) {
                com.navdy.hud.app.ui.component.vlist.VerticalList.this.indicator.setCurrentItem(com.navdy.hud.app.ui.component.vlist.VerticalList.this.getCurrentPosition());
            }
            com.navdy.hud.app.ui.component.vlist.VerticalList.this.indicatorAnimatorSet = null;
        }
    }

    class Anon2 implements java.lang.Runnable {
        Anon2() {
        }

        public void run() {
            if (!com.navdy.hud.app.ui.component.vlist.VerticalList.this.initialPosChanged) {
                int n = com.navdy.hud.app.ui.component.vlist.VerticalList.this.layoutManager.findFirstCompletelyVisibleItemPosition();
                int raw = com.navdy.hud.app.ui.component.vlist.VerticalList.this.getRawPosition();
                if (n != 0 || com.navdy.hud.app.ui.component.vlist.VerticalList.this.adapter.getItemCount() <= 3) {
                    com.navdy.hud.app.ui.component.vlist.VerticalList.sLogger.w("initial scroll worked firstV=" + n + " raw=" + raw + " scrollY=" + com.navdy.hud.app.ui.component.vlist.VerticalList.this.actualScrollY);
                } else {
                    com.navdy.hud.app.ui.component.vlist.VerticalList.sLogger.w("initial scroll did not work, firstV=" + n + " raw=" + raw + " scrollY=" + com.navdy.hud.app.ui.component.vlist.VerticalList.this.actualScrollY);
                }
                com.navdy.hud.app.ui.component.vlist.VerticalList.this.onItemSelected();
                return;
            }
            com.navdy.hud.app.ui.component.vlist.VerticalList.sLogger.v("initial pos changed");
        }
    }

    class Anon3 extends android.support.v7.widget.RecyclerView.OnScrollListener {
        Anon3() {
        }

        public void onScrollStateChanged(android.support.v7.widget.RecyclerView recyclerView, int newState) {
            boolean z;
            com.navdy.hud.app.ui.component.vlist.VerticalList.this.initialPosChanged = true;
            com.navdy.hud.app.ui.component.vlist.VerticalList.this.lastScrollState = newState;
            super.onScrollStateChanged(recyclerView, newState);
            if (newState == 0) {
                if (com.navdy.hud.app.ui.component.vlist.VerticalList.this.reCalcScrollPos) {
                    com.navdy.hud.app.ui.component.vlist.VerticalList.this.calculateScrollRange();
                }
                com.navdy.hud.app.ui.component.vlist.VerticalList.this.isScrolling = false;
                com.navdy.hud.app.ui.component.vlist.VerticalList.this.isScrollBy = false;
                com.navdy.hud.app.ui.component.vlist.VerticalList.this.switchingScrollBoundary = false;
                com.navdy.hud.app.ui.component.vlist.VerticalList.this.scrollByUpReceived = 0;
                int pos = com.navdy.hud.app.ui.component.vlist.VerticalList.this.getPositionFromScrollY(com.navdy.hud.app.ui.component.vlist.VerticalList.this.actualScrollY);
                com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder vhCurrent = (com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder) recyclerView.findViewHolderForAdapterPosition(pos);
                com.navdy.service.library.log.Logger access$Anon500 = com.navdy.hud.app.ui.component.vlist.VerticalList.sLogger;
                java.lang.StringBuilder append = new java.lang.StringBuilder().append("onScrollStateChanged idle pos:").append(pos).append(" current=").append(com.navdy.hud.app.ui.component.vlist.VerticalList.this.currentMiddlePosition).append(" scrollY=").append(com.navdy.hud.app.ui.component.vlist.VerticalList.this.actualScrollY).append(" vh=");
                if (vhCurrent != null) {
                    z = true;
                } else {
                    z = false;
                }
                access$Anon500.v(append.append(z).toString());
                moveItemToSelectedState(vhCurrent, pos);
                int upPos = com.navdy.hud.app.ui.component.vlist.VerticalList.this.currentMiddlePosition - 1;
                if (upPos >= 0) {
                    moveItemToUnSelectedState((com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder) recyclerView.findViewHolderForAdapterPosition(upPos), upPos);
                }
                int downPos = com.navdy.hud.app.ui.component.vlist.VerticalList.this.currentMiddlePosition + 1;
                moveItemToUnSelectedState((com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder) recyclerView.findViewHolderForAdapterPosition(downPos), downPos);
                com.navdy.hud.app.ui.component.vlist.VerticalList.this.onItemSelected();
                if (com.navdy.hud.app.ui.component.vlist.VerticalList.this.isSelectedOperationPending) {
                    com.navdy.hud.app.ui.component.vlist.VerticalList.this.isSelectedOperationPending = false;
                    com.navdy.hud.app.ui.component.vlist.VerticalList.this.select(true);
                } else if (com.navdy.hud.app.ui.component.vlist.VerticalList.this.scrollPendingPos != -1) {
                    int p = com.navdy.hud.app.ui.component.vlist.VerticalList.this.scrollPendingPos;
                    com.navdy.hud.app.ui.component.vlist.VerticalList.this.scrollPendingPos = -1;
                    com.navdy.hud.app.ui.component.vlist.VerticalList.this.scrollToPosition(p);
                    com.navdy.hud.app.ui.component.vlist.VerticalList.sLogger.v("onScrolled idle scrollToPos:" + p);
                }
                if (com.navdy.hud.app.ui.component.vlist.VerticalList.this.sendScrollIdleEvent) {
                    com.navdy.hud.app.ui.component.vlist.VerticalList.this.callback.onScrollIdle();
                }
            }
        }

        public void onScrolled(android.support.v7.widget.RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            com.navdy.hud.app.ui.component.vlist.VerticalList.this.actualScrollY = com.navdy.hud.app.ui.component.vlist.VerticalList.this.actualScrollY + dy;
            if (com.navdy.hud.app.ui.component.vlist.VerticalList.this.waitForTarget) {
                com.navdy.hud.app.ui.component.vlist.VerticalList.this.targetFound(0);
            }
        }

        private void moveItemToSelectedState(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder vh, int pos) {
            if (vh == null) {
                return;
            }
            if (pos != com.navdy.hud.app.ui.component.vlist.VerticalList.this.currentMiddlePosition || vh.getState() == com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.State.UNSELECTED) {
                com.navdy.hud.app.ui.component.vlist.VerticalList.this.currentMiddlePosition = pos;
                if (!com.navdy.hud.app.ui.component.vlist.VerticalList.this.isCloseMenuVisible()) {
                    com.navdy.hud.app.ui.component.vlist.VerticalList.sLogger.v("onScrollStateChanged idle-select pos:" + pos);
                    vh.setState(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.State.SELECTED, com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.AnimationType.NONE, com.navdy.hud.app.ui.component.vlist.VerticalList.ITEM_MOVE_ANIMATION_DURATION);
                    return;
                }
                com.navdy.hud.app.ui.component.vlist.VerticalList.sLogger.v("onScrollStateChanged idle-select-un pos:" + pos);
                vh.setState(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.State.UNSELECTED, com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.AnimationType.NONE, com.navdy.hud.app.ui.component.vlist.VerticalList.ITEM_MOVE_ANIMATION_DURATION);
            } else if (pos == com.navdy.hud.app.ui.component.vlist.VerticalList.this.currentMiddlePosition && com.navdy.hud.app.ui.component.vlist.VerticalList.this.isCloseMenuVisible() && vh.getState() == com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.State.SELECTED) {
                com.navdy.hud.app.ui.component.vlist.VerticalList.sLogger.v("onScrollStateChanged idle-select-un-2 pos:" + pos);
                vh.setState(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.State.UNSELECTED, com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.AnimationType.NONE, com.navdy.hud.app.ui.component.vlist.VerticalList.ITEM_MOVE_ANIMATION_DURATION);
            }
        }

        private void moveItemToUnSelectedState(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder vh, int pos) {
            if (vh != null && vh.getModelType() != com.navdy.hud.app.ui.component.vlist.VerticalList.ModelType.BLANK && vh.getState() == com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.State.SELECTED) {
                com.navdy.hud.app.ui.component.vlist.VerticalList.sLogger.v("onScrollStateChanged idle-unselect pos:" + pos);
                vh.setState(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.State.UNSELECTED, com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.AnimationType.NONE, com.navdy.hud.app.ui.component.vlist.VerticalList.ITEM_MOVE_ANIMATION_DURATION);
            }
        }
    }

    public interface Callback {
        void onBindToView(com.navdy.hud.app.ui.component.vlist.VerticalList.Model model, android.view.View view, int i, com.navdy.hud.app.ui.component.vlist.VerticalList.ModelState modelState);

        void onItemSelected(com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState itemSelectionState);

        void onLoad();

        void onScrollIdle();

        void select(com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState itemSelectionState);
    }

    public interface ContainerCallback {
        void hideToolTips();

        boolean isCloseMenuVisible();

        boolean isFastScrolling();

        void showToolTips();
    }

    public enum Direction {
        UP,
        DOWN
    }

    public static class FontInfo {
        public float subTitle2FontSize;
        public float subTitle2FontTopMargin;
        public float subTitleFontSize;
        public float subTitleFontTopMargin;
        public float titleFontSize;
        public float titleFontTopMargin;
        public float titleScale;
        public boolean titleSingleLine = true;
    }

    public enum FontSize {
        FONT_SIZE_26,
        FONT_SIZE_22,
        FONT_SIZE_22_2,
        FONT_SIZE_18,
        FONT_SIZE_18_2,
        FONT_SIZE_16,
        FONT_SIZE_16_2,
        FONT_SIZE_26_18,
        FONT_SIZE_22_18,
        FONT_SIZE_22_2_18,
        FONT_SIZE_18_16,
        FONT_SIZE_18_2_16,
        FONT_SIZE_16_16,
        FONT_SIZE_16_2_16,
        FONT_SIZE_26_16,
        FONT_SIZE_22_16,
        FONT_SIZE_22_2_16
    }

    public static class ItemSelectionState {
        public int id = -1;
        public com.navdy.hud.app.ui.component.vlist.VerticalList.Model model;
        public int pos = -1;
        public int subId = -1;
        public int subPosition = -1;

        public void set(com.navdy.hud.app.ui.component.vlist.VerticalList.Model model2, int id2, int pos2, int subId2, int subPosition2) {
            this.model = model2;
            this.id = id2;
            this.pos = pos2;
            this.subId = subId2;
            this.subPosition = subPosition2;
        }
    }

    public static class KeyHandlerState {
        public boolean keyHandled;
        public boolean listMoved;

        /* access modifiers changed from: 0000 */
        public void clear() {
            this.keyHandled = false;
            this.listMoved = false;
        }
    }

    public static class Model {
        public static final java.lang.String INITIALS = "INITIAL";
        public static final java.lang.String SUBTITLE_2_COLOR = "SUBTITLE_2_COLOR";
        public static final java.lang.String SUBTITLE_COLOR = "SUBTITLE_COLOR";
        public int currentIconSelection;
        boolean dontStartFluctuator;
        public java.util.HashMap<java.lang.String, java.lang.String> extras;
        public com.navdy.hud.app.ui.component.vlist.VerticalList.FontInfo fontInfo;
        public com.navdy.hud.app.ui.component.vlist.VerticalList.FontSize fontSize;
        boolean fontSizeCheckDone;
        public int icon = 0;
        public int iconDeselectedColor = -1;
        public int[] iconDeselectedColors;
        public int iconFluctuatorColor = -1;
        public int[] iconFluctuatorColors;
        public int[] iconIds;
        public int[] iconList;
        public int iconSelectedColor = -1;
        public int[] iconSelectedColors;
        public com.navdy.hud.app.ui.component.image.IconColorImageView.IconShape iconShape;
        public int iconSize = -1;
        public int iconSmall = -1;
        public int id;
        public boolean isEnabled;
        public boolean isOn;
        boolean needsRebind;
        public boolean noImageScaleAnimation;
        public boolean noTextAnimation;
        public int scrollItemLayoutId = -1;
        public java.lang.Object state;
        public java.lang.String subTitle;
        public java.lang.String subTitle2;
        public boolean subTitle2Formatted;
        public boolean subTitleFormatted;
        public boolean subTitle_2Lines;
        public boolean supportsToolTip;
        public java.lang.String title;
        public com.navdy.hud.app.ui.component.vlist.VerticalList.ModelType type;

        public Model() {
        }

        public Model(com.navdy.hud.app.ui.component.vlist.VerticalList.Model m) {
            this.type = m.type;
            this.id = m.id;
            this.icon = m.icon;
            this.iconSelectedColor = m.iconSelectedColor;
            this.iconDeselectedColor = m.iconDeselectedColor;
            this.iconSmall = m.iconSmall;
            this.iconFluctuatorColor = m.iconFluctuatorColor;
            this.title = m.title;
            this.subTitle = m.subTitle;
            this.subTitle2 = m.subTitle2;
            this.subTitle_2Lines = m.subTitle_2Lines;
            this.subTitleFormatted = m.subTitleFormatted;
            this.subTitle2Formatted = m.subTitle2Formatted;
            this.scrollItemLayoutId = m.scrollItemLayoutId;
            this.iconList = m.iconList;
            this.iconIds = m.iconIds;
            this.iconSelectedColors = m.iconSelectedColors;
            this.iconDeselectedColors = m.iconDeselectedColors;
            this.iconFluctuatorColors = m.iconFluctuatorColors;
            this.currentIconSelection = m.currentIconSelection;
            this.supportsToolTip = m.supportsToolTip;
            this.extras = m.extras;
            this.state = m.state;
            this.iconShape = m.iconShape;
            this.needsRebind = m.needsRebind;
            this.dontStartFluctuator = m.dontStartFluctuator;
            this.fontSizeCheckDone = m.fontSizeCheckDone;
            this.fontInfo = m.fontInfo;
            this.fontSize = m.fontSize;
            this.noTextAnimation = m.noTextAnimation;
            this.noImageScaleAnimation = m.noImageScaleAnimation;
            this.iconSize = m.iconSize;
        }

        public void clear() {
            this.id = 0;
            this.icon = 0;
            this.iconSelectedColor = -1;
            this.iconDeselectedColor = -1;
            this.iconSmall = -1;
            this.iconFluctuatorColor = -1;
            this.title = null;
            this.subTitle = null;
            this.subTitle2 = null;
            this.subTitle_2Lines = false;
            this.subTitleFormatted = false;
            this.subTitle2Formatted = false;
            this.scrollItemLayoutId = -1;
            this.iconList = null;
            this.iconIds = null;
            this.currentIconSelection = 0;
            this.extras = null;
            this.state = null;
            this.iconShape = null;
            this.needsRebind = false;
            this.dontStartFluctuator = false;
            this.fontSizeCheckDone = false;
            this.fontInfo = null;
            this.fontSize = null;
            this.noTextAnimation = false;
            this.noImageScaleAnimation = false;
            this.iconSize = -1;
        }
    }

    public static class ModelState {
        public boolean updateImage;
        public boolean updateSmallImage;
        public boolean updateSubTitle;
        public boolean updateSubTitle2;
        public boolean updateTitle;

        ModelState() {
            reset();
        }

        /* access modifiers changed from: 0000 */
        public void reset() {
            this.updateImage = true;
            this.updateSmallImage = true;
            this.updateTitle = true;
            this.updateSubTitle = true;
            this.updateSubTitle2 = true;
        }
    }

    public enum ModelType {
        BLANK,
        TITLE,
        TITLE_SUBTITLE,
        ICON_BKCOLOR,
        TWO_ICONS,
        ICON,
        LOADING,
        ICON_OPTIONS,
        SCROLL_CONTENT,
        LOADING_CONTENT,
        SWITCH
    }

    static {
        android.content.Context context = com.navdy.hud.app.HudApplication.getAppContext();
        android.content.res.Resources resources = context.getResources();
        vlistTitleTextW = resources.getDimensionPixelSize(com.navdy.hud.app.R.dimen.vlist_title_text_len);
        vlistTitle_26 = resources.getDimension(com.navdy.hud.app.R.dimen.vlist_title);
        vlistTitle_22 = resources.getDimension(com.navdy.hud.app.R.dimen.vlist_title_22);
        vlistTitle_18 = resources.getDimension(com.navdy.hud.app.R.dimen.vlist_title_18);
        vlistTitle_16 = resources.getDimension(com.navdy.hud.app.R.dimen.vlist_title_16);
        vlistTitle_16_top_m_3 = resources.getDimension(com.navdy.hud.app.R.dimen.vlist_16_title_top_m_3);
        vlistsubTitle_16_top_m_3 = resources.getDimension(com.navdy.hud.app.R.dimen.vlist_16_subtitle_top_m_3);
        vlistsubTitle2_16_top_m_3 = resources.getDimension(com.navdy.hud.app.R.dimen.vlist_16_subtitle2_top_m_3);
        vlistTitle_18_top_m_3 = resources.getDimension(com.navdy.hud.app.R.dimen.vlist_18_title_top_m_3);
        vlistsubTitle_18_top_m_3 = resources.getDimension(com.navdy.hud.app.R.dimen.vlist_18_subtitle_top_m_3);
        vlistsubTitle2_18_top_m_3 = resources.getDimension(com.navdy.hud.app.R.dimen.vlist_18_subtitle2_top_m_3);
        vlistTitle_22_top_m_3 = resources.getDimension(com.navdy.hud.app.R.dimen.vlist_22_title_top_m_3);
        vlistsubTitle_22_top_m_3 = resources.getDimension(com.navdy.hud.app.R.dimen.vlist_22_subtitle_top_m_3);
        vlistsubTitle2_22_top_m_3 = resources.getDimension(com.navdy.hud.app.R.dimen.vlist_22_subtitle2_top_m_3);
        vlistTitle_26_top_m_3 = resources.getDimension(com.navdy.hud.app.R.dimen.vlist_26_title_top_m_3);
        vlistsubTitle_26_top_m_3 = resources.getDimension(com.navdy.hud.app.R.dimen.vlist_26_subtitle_top_m_3);
        vlistsubTitle2_26_top_m_3 = resources.getDimension(com.navdy.hud.app.R.dimen.vlist_26_subtitle2_top_m_3);
        vlistTitle_16_top_m_2 = resources.getDimension(com.navdy.hud.app.R.dimen.vlist_16_title_top_m_2);
        vlistsubTitle_16_top_m_2 = resources.getDimension(com.navdy.hud.app.R.dimen.vlist_16_subtitle_top_m_2);
        vlistTitle_18_top_m_2 = resources.getDimension(com.navdy.hud.app.R.dimen.vlist_18_title_top_m_2);
        vlistsubTitle_18_top_m_2 = resources.getDimension(com.navdy.hud.app.R.dimen.vlist_18_subtitle_top_m_2);
        vlistTitle_22_top_m_2 = resources.getDimension(com.navdy.hud.app.R.dimen.vlist_22_title_top_m_2);
        vlistsubTitle_22_top_m_2 = resources.getDimension(com.navdy.hud.app.R.dimen.vlist_22_subtitle_top_m_2);
        vlistTitle_26_top_m_2 = resources.getDimension(com.navdy.hud.app.R.dimen.vlist_26_title_top_m_2);
        vlistsubTitle_26_top_m_2 = resources.getDimension(com.navdy.hud.app.R.dimen.vlist_26_subtitle_top_m_2);
        vlistTitle_26_18_title_top = resources.getDimension(com.navdy.hud.app.R.dimen.vlist_26_18_title_top);
        vlistTitle_26_18_subtitle_top = resources.getDimension(com.navdy.hud.app.R.dimen.vlist_26_18_subtitle_top);
        vlistTitle_22_18_title_top = resources.getDimension(com.navdy.hud.app.R.dimen.vlist_22_18_title_top);
        vlistTitle_22_18_subtitle_top = resources.getDimension(com.navdy.hud.app.R.dimen.vlist_22_18_subtitle_top);
        vlistTitle_18_16_title_top = resources.getDimension(com.navdy.hud.app.R.dimen.vlist_18_16_title_top);
        vlistTitle_18_16_subtitle_top = resources.getDimension(com.navdy.hud.app.R.dimen.vlist_18_16_subtitle_top);
        vlistTitle_16_16_title_top = resources.getDimension(com.navdy.hud.app.R.dimen.vlist_16_16_title_top);
        vlistTitle_16_16_subtitle_top = resources.getDimension(com.navdy.hud.app.R.dimen.vlist_16_16_subtitle_top);
        vlistTitle_26_16_title_top = resources.getDimension(com.navdy.hud.app.R.dimen.vlist_26_16_title_top);
        vlistTitle_26_16_subtitle_top = resources.getDimension(com.navdy.hud.app.R.dimen.vlist_26_16_subtitle_top);
        vlistTitle_22_16_title_top = resources.getDimension(com.navdy.hud.app.R.dimen.vlist_22_16_title_top);
        vlistTitle_22_16_subtitle_top = resources.getDimension(com.navdy.hud.app.R.dimen.vlist_22_16_subtitle_top);
        com.navdy.hud.app.ui.component.vlist.VerticalList.FontInfo fontInfo = new com.navdy.hud.app.ui.component.vlist.VerticalList.FontInfo();
        fontInfo.titleFontSize = vlistTitle_26;
        fontInfo.titleFontTopMargin = vlistTitle_26_top_m_3;
        fontInfo.titleScale = 0.69f;
        fontInfo.subTitleFontSize = vlistTitle_16;
        fontInfo.subTitleFontTopMargin = vlistsubTitle_26_top_m_3;
        fontInfo.subTitle2FontSize = vlistTitle_16;
        fontInfo.subTitle2FontTopMargin = vlistsubTitle2_26_top_m_3;
        tit_subt_subt2_map.put(com.navdy.hud.app.ui.component.vlist.VerticalList.FontSize.FONT_SIZE_26, fontInfo);
        com.navdy.hud.app.ui.component.vlist.VerticalList.FontInfo fontInfo2 = new com.navdy.hud.app.ui.component.vlist.VerticalList.FontInfo();
        fontInfo2.titleFontSize = vlistTitle_22;
        fontInfo2.titleFontTopMargin = vlistTitle_22_top_m_3;
        fontInfo2.titleScale = 0.81f;
        fontInfo2.titleSingleLine = false;
        fontInfo2.subTitleFontSize = vlistTitle_16;
        fontInfo2.subTitleFontTopMargin = vlistsubTitle_22_top_m_3;
        fontInfo2.subTitle2FontSize = vlistTitle_16;
        fontInfo2.subTitle2FontTopMargin = vlistsubTitle2_22_top_m_3;
        tit_subt_subt2_map.put(com.navdy.hud.app.ui.component.vlist.VerticalList.FontSize.FONT_SIZE_22, fontInfo2);
        com.navdy.hud.app.ui.component.vlist.VerticalList.FontInfo fontInfo3 = new com.navdy.hud.app.ui.component.vlist.VerticalList.FontInfo();
        fontInfo3.titleFontSize = vlistTitle_22;
        fontInfo3.titleFontTopMargin = vlistTitle_22_top_m_3;
        fontInfo3.titleScale = 0.81f;
        fontInfo3.titleSingleLine = false;
        fontInfo3.subTitleFontSize = vlistTitle_16;
        fontInfo3.subTitleFontTopMargin = vlistsubTitle_22_top_m_3 + 3.0f;
        fontInfo3.subTitle2FontSize = vlistTitle_16;
        fontInfo3.subTitle2FontTopMargin = vlistsubTitle2_22_top_m_3 + 3.0f;
        tit_subt_subt2_map.put(com.navdy.hud.app.ui.component.vlist.VerticalList.FontSize.FONT_SIZE_22_2, fontInfo3);
        com.navdy.hud.app.ui.component.vlist.VerticalList.FontInfo fontInfo4 = new com.navdy.hud.app.ui.component.vlist.VerticalList.FontInfo();
        fontInfo4.titleFontSize = vlistTitle_18;
        fontInfo4.titleFontTopMargin = vlistTitle_18_top_m_3;
        fontInfo4.titleScale = 1.0f;
        fontInfo4.subTitleFontSize = vlistTitle_16;
        fontInfo4.subTitleFontTopMargin = vlistsubTitle_18_top_m_3;
        fontInfo4.subTitle2FontSize = vlistTitle_16;
        fontInfo4.subTitle2FontTopMargin = vlistsubTitle2_18_top_m_3;
        tit_subt_subt2_map.put(com.navdy.hud.app.ui.component.vlist.VerticalList.FontSize.FONT_SIZE_18, fontInfo4);
        com.navdy.hud.app.ui.component.vlist.VerticalList.FontInfo fontInfo5 = new com.navdy.hud.app.ui.component.vlist.VerticalList.FontInfo();
        fontInfo5.titleFontSize = vlistTitle_18;
        fontInfo5.titleFontTopMargin = vlistTitle_18_top_m_3;
        fontInfo5.titleScale = 1.0f;
        fontInfo5.titleSingleLine = false;
        fontInfo5.subTitleFontSize = vlistTitle_16;
        fontInfo5.subTitleFontTopMargin = vlistsubTitle_18_top_m_3;
        fontInfo5.subTitle2FontSize = vlistTitle_16;
        fontInfo5.subTitle2FontTopMargin = vlistsubTitle2_18_top_m_3;
        tit_subt_subt2_map.put(com.navdy.hud.app.ui.component.vlist.VerticalList.FontSize.FONT_SIZE_18_2, fontInfo5);
        com.navdy.hud.app.ui.component.vlist.VerticalList.FontInfo fontInfo6 = new com.navdy.hud.app.ui.component.vlist.VerticalList.FontInfo();
        fontInfo6.titleFontSize = vlistTitle_16;
        fontInfo6.titleFontTopMargin = vlistTitle_16_top_m_3;
        fontInfo6.titleScale = 1.0f;
        fontInfo6.subTitleFontSize = vlistTitle_16;
        fontInfo6.subTitleFontTopMargin = vlistsubTitle_16_top_m_3;
        fontInfo6.subTitle2FontSize = vlistTitle_16;
        fontInfo6.subTitle2FontTopMargin = vlistsubTitle2_16_top_m_3;
        tit_subt_subt2_map.put(com.navdy.hud.app.ui.component.vlist.VerticalList.FontSize.FONT_SIZE_16, fontInfo6);
        com.navdy.hud.app.ui.component.vlist.VerticalList.FontInfo fontInfo7 = new com.navdy.hud.app.ui.component.vlist.VerticalList.FontInfo();
        fontInfo7.titleFontSize = vlistTitle_16;
        fontInfo7.titleFontTopMargin = vlistTitle_16_top_m_3;
        fontInfo7.titleScale = 1.0f;
        fontInfo7.titleSingleLine = false;
        fontInfo7.subTitleFontSize = vlistTitle_16;
        fontInfo7.subTitleFontTopMargin = vlistsubTitle_16_top_m_3;
        fontInfo7.subTitle2FontSize = vlistTitle_16;
        fontInfo7.subTitle2FontTopMargin = vlistsubTitle2_16_top_m_3;
        tit_subt_subt2_map.put(com.navdy.hud.app.ui.component.vlist.VerticalList.FontSize.FONT_SIZE_16_2, fontInfo7);
        com.navdy.hud.app.ui.component.vlist.VerticalList.FontInfo fontInfo8 = new com.navdy.hud.app.ui.component.vlist.VerticalList.FontInfo();
        fontInfo8.titleFontSize = vlistTitle_26;
        fontInfo8.titleFontTopMargin = vlistTitle_26_top_m_2;
        fontInfo8.titleScale = 0.69f;
        fontInfo8.subTitleFontSize = vlistTitle_16;
        fontInfo8.subTitleFontTopMargin = vlistsubTitle_26_top_m_2;
        tit_subt_map.put(com.navdy.hud.app.ui.component.vlist.VerticalList.FontSize.FONT_SIZE_26, fontInfo8);
        com.navdy.hud.app.ui.component.vlist.VerticalList.FontInfo fontInfo9 = new com.navdy.hud.app.ui.component.vlist.VerticalList.FontInfo();
        fontInfo9.titleFontSize = vlistTitle_22;
        fontInfo9.titleFontTopMargin = vlistTitle_22_top_m_2;
        fontInfo9.titleScale = 0.81f;
        fontInfo9.subTitleFontSize = vlistTitle_16;
        fontInfo9.subTitleFontTopMargin = vlistsubTitle_22_top_m_2;
        tit_subt_map.put(com.navdy.hud.app.ui.component.vlist.VerticalList.FontSize.FONT_SIZE_22, fontInfo9);
        com.navdy.hud.app.ui.component.vlist.VerticalList.FontInfo fontInfo10 = new com.navdy.hud.app.ui.component.vlist.VerticalList.FontInfo();
        fontInfo10.titleFontSize = vlistTitle_22;
        fontInfo10.titleFontTopMargin = vlistTitle_22_top_m_2 + 3.0f;
        fontInfo10.titleScale = 0.81f;
        fontInfo10.titleSingleLine = false;
        fontInfo10.subTitleFontSize = vlistTitle_16;
        fontInfo10.subTitleFontTopMargin = vlistsubTitle_22_top_m_2 + 9.0f;
        tit_subt_map.put(com.navdy.hud.app.ui.component.vlist.VerticalList.FontSize.FONT_SIZE_22_2, fontInfo10);
        com.navdy.hud.app.ui.component.vlist.VerticalList.FontInfo fontInfo11 = new com.navdy.hud.app.ui.component.vlist.VerticalList.FontInfo();
        fontInfo11.titleFontSize = vlistTitle_18;
        fontInfo11.titleFontTopMargin = vlistTitle_18_top_m_2;
        fontInfo11.titleScale = 1.0f;
        fontInfo11.subTitleFontSize = vlistTitle_16;
        fontInfo11.subTitleFontTopMargin = vlistsubTitle_18_top_m_2;
        tit_subt_map.put(com.navdy.hud.app.ui.component.vlist.VerticalList.FontSize.FONT_SIZE_18, fontInfo11);
        com.navdy.hud.app.ui.component.vlist.VerticalList.FontInfo fontInfo12 = new com.navdy.hud.app.ui.component.vlist.VerticalList.FontInfo();
        fontInfo12.titleFontSize = vlistTitle_18;
        fontInfo12.titleFontTopMargin = vlistTitle_18_top_m_2 + 1.0f;
        fontInfo12.titleScale = 1.0f;
        fontInfo12.titleSingleLine = false;
        fontInfo12.subTitleFontSize = vlistTitle_16;
        fontInfo12.subTitleFontTopMargin = vlistsubTitle_18_top_m_2 + 7.0f;
        tit_subt_map.put(com.navdy.hud.app.ui.component.vlist.VerticalList.FontSize.FONT_SIZE_18_2, fontInfo12);
        com.navdy.hud.app.ui.component.vlist.VerticalList.FontInfo fontInfo13 = new com.navdy.hud.app.ui.component.vlist.VerticalList.FontInfo();
        fontInfo13.titleFontSize = vlistTitle_16;
        fontInfo13.titleFontTopMargin = vlistTitle_16_top_m_2;
        fontInfo13.titleScale = 1.0f;
        fontInfo13.subTitleFontSize = vlistTitle_16;
        fontInfo13.subTitleFontTopMargin = vlistsubTitle_16_top_m_2;
        tit_subt_map.put(com.navdy.hud.app.ui.component.vlist.VerticalList.FontSize.FONT_SIZE_16, fontInfo13);
        com.navdy.hud.app.ui.component.vlist.VerticalList.FontInfo fontInfo14 = new com.navdy.hud.app.ui.component.vlist.VerticalList.FontInfo();
        fontInfo14.titleFontSize = vlistTitle_16;
        fontInfo14.titleFontTopMargin = vlistTitle_16_top_m_2 + 1.0f;
        fontInfo14.titleScale = 1.0f;
        fontInfo14.titleSingleLine = false;
        fontInfo14.subTitleFontSize = vlistTitle_16;
        fontInfo14.subTitleFontTopMargin = vlistsubTitle_16_top_m_2 + 7.0f;
        tit_subt_map.put(com.navdy.hud.app.ui.component.vlist.VerticalList.FontSize.FONT_SIZE_16_2, fontInfo14);
        com.navdy.hud.app.ui.component.vlist.VerticalList.FontInfo fontInfo15 = new com.navdy.hud.app.ui.component.vlist.VerticalList.FontInfo();
        fontInfo15.titleFontSize = vlistTitle_26;
        fontInfo15.titleFontTopMargin = vlistTitle_26_18_title_top;
        fontInfo15.titleScale = 0.69f;
        fontInfo15.subTitleFontSize = vlistTitle_18;
        fontInfo15.subTitleFontTopMargin = vlistTitle_26_18_subtitle_top;
        tit_subt_map_2_lines.put(com.navdy.hud.app.ui.component.vlist.VerticalList.FontSize.FONT_SIZE_26_18, fontInfo15);
        com.navdy.hud.app.ui.component.vlist.VerticalList.FontInfo fontInfo16 = new com.navdy.hud.app.ui.component.vlist.VerticalList.FontInfo();
        fontInfo16.titleFontSize = vlistTitle_22;
        fontInfo16.titleFontTopMargin = vlistTitle_22_18_title_top;
        fontInfo16.titleScale = 0.81f;
        fontInfo16.subTitleFontSize = vlistTitle_18;
        fontInfo16.subTitleFontTopMargin = vlistTitle_22_18_subtitle_top;
        tit_subt_map_2_lines.put(com.navdy.hud.app.ui.component.vlist.VerticalList.FontSize.FONT_SIZE_22_18, fontInfo16);
        com.navdy.hud.app.ui.component.vlist.VerticalList.FontInfo fontInfo17 = new com.navdy.hud.app.ui.component.vlist.VerticalList.FontInfo();
        fontInfo17.titleFontSize = vlistTitle_22;
        fontInfo17.titleFontTopMargin = vlistTitle_22_18_title_top;
        fontInfo17.titleScale = 0.81f;
        fontInfo17.titleSingleLine = false;
        fontInfo17.subTitleFontSize = vlistTitle_18;
        fontInfo17.subTitleFontTopMargin = vlistTitle_22_18_subtitle_top;
        tit_subt_map_2_lines.put(com.navdy.hud.app.ui.component.vlist.VerticalList.FontSize.FONT_SIZE_22_2_18, fontInfo17);
        com.navdy.hud.app.ui.component.vlist.VerticalList.FontInfo fontInfo18 = new com.navdy.hud.app.ui.component.vlist.VerticalList.FontInfo();
        fontInfo18.titleFontSize = vlistTitle_18;
        fontInfo18.titleFontTopMargin = vlistTitle_18_16_title_top;
        fontInfo18.titleScale = 1.0f;
        fontInfo18.subTitleFontSize = vlistTitle_16;
        fontInfo18.subTitleFontTopMargin = vlistTitle_18_16_subtitle_top;
        tit_subt_map_2_lines.put(com.navdy.hud.app.ui.component.vlist.VerticalList.FontSize.FONT_SIZE_18_16, fontInfo18);
        com.navdy.hud.app.ui.component.vlist.VerticalList.FontInfo fontInfo19 = new com.navdy.hud.app.ui.component.vlist.VerticalList.FontInfo();
        fontInfo19.titleFontSize = vlistTitle_18;
        fontInfo19.titleFontTopMargin = vlistTitle_18_top_m_2 + 1.0f;
        fontInfo19.titleScale = 1.0f;
        fontInfo19.titleSingleLine = false;
        fontInfo19.subTitleFontSize = vlistTitle_16;
        fontInfo19.subTitleFontTopMargin = vlistsubTitle_18_top_m_2 + 7.0f;
        tit_subt_map_2_lines.put(com.navdy.hud.app.ui.component.vlist.VerticalList.FontSize.FONT_SIZE_18_2_16, fontInfo19);
        com.navdy.hud.app.ui.component.vlist.VerticalList.FontInfo fontInfo20 = new com.navdy.hud.app.ui.component.vlist.VerticalList.FontInfo();
        fontInfo20.titleFontSize = vlistTitle_16;
        fontInfo20.titleFontTopMargin = vlistTitle_16_16_title_top;
        fontInfo20.titleScale = 1.0f;
        fontInfo20.subTitleFontSize = vlistTitle_16;
        fontInfo20.subTitleFontTopMargin = vlistTitle_16_16_subtitle_top;
        tit_subt_map_2_lines.put(com.navdy.hud.app.ui.component.vlist.VerticalList.FontSize.FONT_SIZE_16_16, fontInfo20);
        com.navdy.hud.app.ui.component.vlist.VerticalList.FontInfo fontInfo21 = new com.navdy.hud.app.ui.component.vlist.VerticalList.FontInfo();
        fontInfo21.titleFontSize = vlistTitle_16;
        fontInfo21.titleFontTopMargin = vlistTitle_16_16_title_top;
        fontInfo21.titleScale = 1.0f;
        fontInfo21.titleSingleLine = false;
        fontInfo21.subTitleFontSize = vlistTitle_16;
        fontInfo21.subTitleFontTopMargin = vlistTitle_16_16_subtitle_top;
        tit_subt_map_2_lines.put(com.navdy.hud.app.ui.component.vlist.VerticalList.FontSize.FONT_SIZE_16_2_16, fontInfo21);
        com.navdy.hud.app.ui.component.vlist.VerticalList.FontInfo fontInfo22 = new com.navdy.hud.app.ui.component.vlist.VerticalList.FontInfo();
        fontInfo22.titleFontSize = vlistTitle_26;
        fontInfo22.titleFontTopMargin = vlistTitle_26_16_title_top;
        fontInfo22.titleScale = 0.69f;
        fontInfo22.subTitleFontSize = vlistTitle_16;
        fontInfo22.subTitleFontTopMargin = vlistTitle_26_16_subtitle_top;
        tit_subt_map_2_lines.put(com.navdy.hud.app.ui.component.vlist.VerticalList.FontSize.FONT_SIZE_26_16, fontInfo22);
        com.navdy.hud.app.ui.component.vlist.VerticalList.FontInfo fontInfo23 = new com.navdy.hud.app.ui.component.vlist.VerticalList.FontInfo();
        fontInfo23.titleFontSize = vlistTitle_22;
        fontInfo23.titleFontTopMargin = vlistTitle_22_16_title_top;
        fontInfo23.titleScale = 0.81f;
        fontInfo23.subTitleFontSize = vlistTitle_16;
        fontInfo23.subTitleFontTopMargin = vlistTitle_22_16_subtitle_top;
        tit_subt_map_2_lines.put(com.navdy.hud.app.ui.component.vlist.VerticalList.FontSize.FONT_SIZE_22_16, fontInfo23);
        com.navdy.hud.app.ui.component.vlist.VerticalList.FontInfo fontInfo24 = new com.navdy.hud.app.ui.component.vlist.VerticalList.FontInfo();
        fontInfo24.titleFontSize = vlistTitle_22;
        fontInfo24.titleFontTopMargin = vlistTitle_22_top_m_2 + 1.0f;
        fontInfo24.titleScale = 0.81f;
        fontInfo24.titleSingleLine = false;
        fontInfo24.subTitleFontSize = vlistTitle_16;
        fontInfo24.subTitleFontTopMargin = vlistTitle_22_16_subtitle_top + 6.0f;
        tit_subt_map_2_lines.put(com.navdy.hud.app.ui.component.vlist.VerticalList.FontSize.FONT_SIZE_22_2_16, fontInfo24);
        fontSizeTextView = new android.widget.TextView(context);
        fontSizeTextView.setTextAppearance(context, com.navdy.hud.app.R.style.vlist_title);
    }

    public static com.navdy.hud.app.ui.component.vlist.VerticalList.FontInfo getFontInfo(com.navdy.hud.app.ui.component.vlist.VerticalList.FontSize fontSize) {
        return (com.navdy.hud.app.ui.component.vlist.VerticalList.FontInfo) tit_subt_map.get(fontSize);
    }

    /* access modifiers changed from: private */
    public void onItemSelected() {
        int userPos = getCurrentPosition();
        com.navdy.hud.app.ui.component.vlist.VerticalList.Model current = this.adapter.getModel(this.currentMiddlePosition);
        if (current != null) {
            int subId = -1;
            int subPos = -1;
            if (current.type == com.navdy.hud.app.ui.component.vlist.VerticalList.ModelType.ICON_OPTIONS) {
                com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder iconOptionsViewHolder = (com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder) this.recyclerView.findViewHolderForAdapterPosition(this.currentMiddlePosition);
                if (iconOptionsViewHolder instanceof com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder) {
                    com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder iconOptionsVH = (com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder) iconOptionsViewHolder;
                    subId = iconOptionsVH.getCurrentSelectionId();
                    subPos = iconOptionsVH.getCurrentSelection();
                }
            }
            this.itemSelectionState.set(current, current.id, userPos, subId, subPos);
            this.callback.onItemSelected(this.itemSelectionState);
        }
    }

    public VerticalList(com.navdy.hud.app.ui.component.vlist.VerticalRecyclerView recyclerView2, com.navdy.hud.app.ui.component.carousel.CarouselIndicator indicator2, com.navdy.hud.app.ui.component.vlist.VerticalList.Callback callback2, com.navdy.hud.app.ui.component.vlist.VerticalList.ContainerCallback containerCallback2) {
        this(recyclerView2, indicator2, callback2, containerCallback2, false);
    }

    public VerticalList(com.navdy.hud.app.ui.component.vlist.VerticalRecyclerView recyclerView2, com.navdy.hud.app.ui.component.carousel.CarouselIndicator indicator2, com.navdy.hud.app.ui.component.vlist.VerticalList.Callback callback2, com.navdy.hud.app.ui.component.vlist.VerticalList.ContainerCallback containerCallback2, boolean twoLineTitles2) {
        this.scrollPendingPos = -1;
        this.indicatorAnimationListener = new com.navdy.hud.app.ui.component.vlist.VerticalList.Anon1();
        this.scrollItemIndex = -1;
        this.scrollItemStartY = -1;
        this.scrollItemEndY = -1;
        this.scrollItemHeight = -1;
        this.initialPosCheckRunnable = new com.navdy.hud.app.ui.component.vlist.VerticalList.Anon2();
        this.keyHandlerState = new com.navdy.hud.app.ui.component.vlist.VerticalList.KeyHandlerState();
        this.itemSelectionState = new com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState();
        this.copyList = new java.util.ArrayList<>();
        this.scrollListener = new com.navdy.hud.app.ui.component.vlist.VerticalList.Anon3();
        this.selectedList = new java.util.HashSet<>();
        if (recyclerView2 == null || indicator2 == null || callback2 == null) {
            throw new java.lang.IllegalArgumentException();
        }
        sLogger.v("ctor");
        this.twoLineTitles = twoLineTitles2;
        this.animationDuration = ITEM_MOVE_ANIMATION_DURATION;
        android.content.Context context = recyclerView2.getContext();
        this.recyclerView = recyclerView2;
        this.recyclerView.setItemViewCacheSize(5);
        android.support.v7.widget.RecyclerView.RecycledViewPool viewPool = this.recyclerView.getRecycledViewPool();
        viewPool.setMaxRecycledViews(com.navdy.hud.app.ui.component.vlist.VerticalList.ModelType.BLANK.ordinal(), 2);
        viewPool.setMaxRecycledViews(com.navdy.hud.app.ui.component.vlist.VerticalList.ModelType.ICON.ordinal(), 10);
        viewPool.setMaxRecycledViews(com.navdy.hud.app.ui.component.vlist.VerticalList.ModelType.ICON_BKCOLOR.ordinal(), 10);
        viewPool.setMaxRecycledViews(com.navdy.hud.app.ui.component.vlist.VerticalList.ModelType.TWO_ICONS.ordinal(), 10);
        viewPool.setMaxRecycledViews(com.navdy.hud.app.ui.component.vlist.VerticalList.ModelType.TITLE.ordinal(), 1);
        viewPool.setMaxRecycledViews(com.navdy.hud.app.ui.component.vlist.VerticalList.ModelType.TITLE_SUBTITLE.ordinal(), 1);
        viewPool.setMaxRecycledViews(com.navdy.hud.app.ui.component.vlist.VerticalList.ModelType.LOADING.ordinal(), 1);
        viewPool.setMaxRecycledViews(com.navdy.hud.app.ui.component.vlist.VerticalList.ModelType.ICON_OPTIONS.ordinal(), 1);
        viewPool.setMaxRecycledViews(com.navdy.hud.app.ui.component.vlist.VerticalList.ModelType.SCROLL_CONTENT.ordinal(), 0);
        viewPool.setMaxRecycledViews(com.navdy.hud.app.ui.component.vlist.VerticalList.ModelType.LOADING_CONTENT.ordinal(), 10);
        this.recyclerView.setOverScrollMode(2);
        this.recyclerView.setVerticalScrollBarEnabled(false);
        this.recyclerView.setHorizontalScrollBarEnabled(false);
        this.indicator = indicator2;
        this.indicator.setOrientation(com.navdy.hud.app.ui.component.carousel.CarouselIndicator.Orientation.VERTICAL);
        this.callback = callback2;
        this.containerCallback = containerCallback2;
        this.layoutManager = new com.navdy.hud.app.ui.component.vlist.VerticalLayoutManager(context, this, callback2, recyclerView2);
        this.layoutManager.setOrientation(1);
        this.recyclerView.setLayoutManager(this.layoutManager);
        this.recyclerView.addOnScrollListener(this.scrollListener);
    }

    public void setBindCallbacks(boolean enable) {
        this.bindCallbacks = enable;
    }

    public void setScrollIdleEvent(boolean send) {
        this.sendScrollIdleEvent = send;
    }

    public void updateView(java.util.List<com.navdy.hud.app.ui.component.vlist.VerticalList.Model> list, int initialSelection, boolean firstEntryBlank2) {
        updateView(list, initialSelection, firstEntryBlank2, false, -1);
    }

    public void updateViewWithScrollableContent(java.util.List<com.navdy.hud.app.ui.component.vlist.VerticalList.Model> list, int initialSelection, boolean firstEntryBlank2) {
        int scrollItemCount = 0;
        int index = -1;
        int counter = 0;
        for (com.navdy.hud.app.ui.component.vlist.VerticalList.Model model : list) {
            if (model.type == com.navdy.hud.app.ui.component.vlist.VerticalList.ModelType.SCROLL_CONTENT) {
                scrollItemCount++;
                index = counter;
            }
            counter++;
        }
        if (list.size() == 1 || scrollItemCount == 0 || scrollItemCount > 1) {
            throw new java.lang.RuntimeException("invalid scroll item model size=" + list.size() + " count=" + scrollItemCount);
        }
        updateView(list, initialSelection, firstEntryBlank2, true, index);
    }

    public boolean allowsTwoLineTitles() {
        return this.twoLineTitles;
    }

    private void updateView(java.util.List<com.navdy.hud.app.ui.component.vlist.VerticalList.Model> list, int initialSelection, boolean firstEntryBlank2, boolean hasScrollableElement2, int scrollIndex) {
        int[] cacheIndex;
        if (!com.navdy.hud.app.util.GenericUtil.isMainThread()) {
            throw new java.lang.RuntimeException("updateView can only be called from main thread");
        } else if (isLocked()) {
            sLogger.w("cannot update vlist during lock");
        } else {
            int len = list.size();
            if (len == 0) {
                throw new java.lang.IllegalArgumentException("empty list now allowed");
            }
            clearAllAnimations();
            this.recyclerView.stopScroll();
            sLogger.v("updateView [" + len + "] sel:" + initialSelection + " firstEntryBlank:" + firstEntryBlank2 + " scrollContent:" + hasScrollableElement2 + " scrollIndex:" + scrollIndex);
            this.modelList = new java.util.ArrayList(list);
            if (len <= 1) {
                initialSelection = 0;
            }
            int origSelection = initialSelection;
            this.firstEntryBlank = firstEntryBlank2;
            this.hasScrollableElement = hasScrollableElement2;
            this.scrollItemIndex = scrollIndex;
            this.scrollItemStartY = -1;
            this.scrollItemEndY = -1;
            this.scrollItemHeight = -1;
            addExtraItems(this.modelList);
            this.currentMiddlePosition = initialSelection + 1;
            if (hasScrollableElement2 && firstEntryBlank2) {
                this.scrollItemIndex++;
            }
            int indicatorLen = len;
            if (!firstEntryBlank2) {
                indicatorLen--;
            }
            this.indicator.setItemCount(indicatorLen);
            this.indicator.setCurrentItem(origSelection);
            this.layoutManager.clear();
            this.adapter = new com.navdy.hud.app.ui.component.vlist.VerticalAdapter(this.modelList, this);
            this.adapter.setHasStableIds(true);
            int cacheIndexSize = 0;
            if (hasScrollableElement2) {
                if (list.size() == this.scrollItemIndex) {
                    cacheIndex = new int[]{this.scrollItemIndex - 1};
                    cacheIndexSize = 1;
                } else {
                    cacheIndex = new int[]{this.scrollItemIndex - 1, this.scrollItemIndex + 1};
                    cacheIndexSize = 2;
                }
                this.adapter.setViewHolderCacheIndex(cacheIndex);
            }
            this.recyclerView.setAdapter(this.adapter);
            this.initialPosChanged = false;
            int scrollPos = this.currentMiddlePosition - 1;
            boolean specialScrollPos = false;
            if (!hasScrollableElement2) {
                this.actualScrollY = com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.listItemHeight * initialSelection;
            } else {
                sLogger.v("updateView initial:" + initialSelection + " scrollIndex=" + scrollIndex);
                if (initialSelection == scrollIndex) {
                    scrollPos = this.currentMiddlePosition;
                    this.actualScrollY = (initialSelection + 1) * com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.listItemHeight;
                    sLogger.v("updateView: scroll index selected:" + this.actualScrollY);
                } else if (initialSelection < this.scrollItemIndex) {
                    this.actualScrollY = com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.listItemHeight * initialSelection;
                } else {
                    this.reCalcScrollPos = true;
                    sLogger.v("reCalcScrollPos no ht");
                    this.actualScrollY = com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.listItemHeight * initialSelection;
                    if (initialSelection == scrollIndex + 1) {
                        specialScrollPos = true;
                    }
                }
            }
            if (specialScrollPos) {
                sLogger.v("updateView special scroll");
                this.layoutManager.scrollToPositionWithOffset(scrollPos + 1, com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.listItemHeight);
            } else {
                this.layoutManager.scrollToPositionWithOffset(scrollPos, 0);
            }
            sLogger.v("updateView scroll to " + scrollPos + " scrollPos=" + this.actualScrollY + " middle=" + getCurrentPosition() + " raw=" + getRawPosition() + " adapter-cache:" + cacheIndexSize + " scrollIndex:" + scrollIndex);
            handler.post(this.initialPosCheckRunnable);
        }
    }

    public void scrollToPosition(int pos) {
        com.navdy.hud.app.ui.component.vlist.VerticalList.Model model = this.adapter.getModel(pos);
        if (model != null) {
            if (isLocked()) {
                sLogger.v("scrollToPos list locked:" + pos);
            } else if (this.isScrolling) {
                sLogger.v("scrollToPos wait for scroll idle:" + pos);
                this.scrollPendingPos = pos;
            } else {
                sLogger.v("scrollToPos:" + pos + " , " + model.title);
                this.currentMiddlePosition = pos;
                int scrollPos = this.currentMiddlePosition - 1;
                if (scrollPos < 0) {
                    scrollPos = 0;
                }
                this.actualScrollY = com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.listItemHeight * scrollPos;
                this.adapter.setInitialState(false);
                this.layoutManager.scrollToPositionWithOffset(scrollPos, 0);
                sLogger.v("scrollToPos scroll to " + scrollPos + " scrollPos=" + this.actualScrollY + " middle=" + getCurrentPosition() + " raw=" + getRawPosition());
                this.scrollListener.onScrollStateChanged(this.recyclerView, 0);
            }
        }
    }

    private void addExtraItems(java.util.List<com.navdy.hud.app.ui.component.vlist.VerticalList.Model> list) {
        if (this.firstEntryBlank) {
            list.add(0, BLANK_ITEM_TOP);
        }
        list.add(BLANK_ITEM_BOTTOM);
    }

    public com.navdy.hud.app.ui.component.vlist.VerticalList.KeyHandlerState up() {
        this.keyHandlerState.clear();
        if (isLocked()) {
            sLogger.v("up locked");
            return this.keyHandlerState;
        } else if (this.switchingScrollBoundary) {
            this.keyHandlerState.keyHandled = true;
            return this.keyHandlerState;
        } else if (!this.reCalcScrollPos || !this.isScrolling) {
            if (this.hasScrollableElement && isScrollPosInScrollItem()) {
                int scrollDistance = canScrollUp();
                if (scrollDistance != -1) {
                    if (this.isScrolling) {
                        this.currentScrollBy += 0.5f;
                        if (this.currentScrollBy > 2.0f) {
                            this.currentScrollBy = 2.0f;
                        }
                    } else {
                        this.currentScrollBy = 1.0f;
                    }
                    float distance = ((float) com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.scrollDistance) * this.currentScrollBy;
                    float scrollBy = ((float) scrollDistance) > distance ? distance : (float) scrollDistance;
                    this.isScrolling = true;
                    this.recyclerView.smoothScrollBy(0, (int) (-scrollBy));
                    com.navdy.hud.app.audio.SoundUtils.playSound(com.navdy.hud.app.audio.SoundUtils.Sound.MENU_MOVE);
                    this.keyHandlerState.keyHandled = true;
                    return this.keyHandlerState;
                }
            }
            if (!isTop()) {
                this.keyHandlerState.keyHandled = true;
                if (isOverrideKey(com.navdy.hud.app.manager.InputManager.CustomKeyEvent.LEFT)) {
                    com.navdy.hud.app.audio.SoundUtils.playSound(com.navdy.hud.app.audio.SoundUtils.Sound.MENU_MOVE);
                    this.keyHandlerState.listMoved = false;
                    return this.keyHandlerState;
                }
                this.keyHandlerState.listMoved = true;
                up(this.currentMiddlePosition, this.currentMiddlePosition - 1);
                return this.keyHandlerState;
            } else if (isOverrideKey(com.navdy.hud.app.manager.InputManager.CustomKeyEvent.LEFT)) {
                com.navdy.hud.app.audio.SoundUtils.playSound(com.navdy.hud.app.audio.SoundUtils.Sound.MENU_MOVE);
                this.keyHandlerState.keyHandled = true;
                this.keyHandlerState.listMoved = false;
                return this.keyHandlerState;
            } else {
                if (this.isScrollBy) {
                    this.scrollByUpReceived++;
                    this.keyHandlerState.keyHandled = true;
                }
                if (this.isScrolling) {
                    this.keyHandlerState.keyHandled = true;
                }
                sLogger.v("cannot go up:" + this.currentMiddlePosition + " isScrolling:" + this.isScrolling);
                return this.keyHandlerState;
            }
        } else {
            this.keyHandlerState.keyHandled = true;
            return this.keyHandlerState;
        }
    }

    private void up(int oldPos, int newPos) {
        com.navdy.hud.app.audio.SoundUtils.playSound(com.navdy.hud.app.audio.SoundUtils.Sound.MENU_MOVE);
        this.currentMiddlePosition = newPos;
        this.isScrolling = true;
        this.keyHandlerState.keyHandled = true;
        if (this.hasScrollableElement) {
            boolean updateIndicator = true;
            boolean highlightItem = false;
            boolean checkAdapterCache = false;
            if (isScrollItemInMiddle()) {
                this.switchingScrollBoundary = true;
                this.recyclerView.smoothScrollBy(0, -com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.listItemHeight);
                highlightItem = true;
            } else if (isScrollItemAlignedToTopEdge()) {
                this.isScrollBy = true;
                this.switchingScrollBoundary = true;
                updateIndicator = true;
                highlightItem = true;
                checkAdapterCache = true;
                this.layoutManager.smoothScrollToPosition(this.currentMiddlePosition - 1, -1);
            } else if (this.currentMiddlePosition == this.scrollItemIndex) {
                float distance = (float) (com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.listItemHeight * 2);
                this.switchingScrollBoundary = true;
                this.recyclerView.smoothScrollBy(0, (int) (-distance));
                updateIndicator = true;
                highlightItem = true;
            } else {
                this.layoutManager.smoothScrollToPosition(oldPos, 1);
            }
            animate(oldPos, newPos, updateIndicator, highlightItem, checkAdapterCache);
            return;
        }
        animate(oldPos, newPos);
        this.layoutManager.smoothScrollToPosition(oldPos, 1);
    }

    public com.navdy.hud.app.ui.component.vlist.VerticalList.KeyHandlerState down() {
        this.keyHandlerState.clear();
        if (isLocked()) {
            sLogger.v("down locked");
            return this.keyHandlerState;
        } else if (this.switchingScrollBoundary) {
            this.keyHandlerState.keyHandled = true;
            return this.keyHandlerState;
        } else {
            if (this.hasScrollableElement) {
                if (this.currentMiddlePosition + 1 == this.scrollItemIndex) {
                    int oldPos = this.currentMiddlePosition;
                    this.currentMiddlePosition++;
                    com.navdy.hud.app.audio.SoundUtils.playSound(com.navdy.hud.app.audio.SoundUtils.Sound.MENU_MOVE);
                    this.switchingScrollBoundary = true;
                    this.isScrolling = true;
                    this.layoutManager.smoothScrollToPosition(this.currentMiddlePosition, -1);
                    animate(oldPos, this.currentMiddlePosition, true, true, false);
                    this.keyHandlerState.listMoved = true;
                    this.keyHandlerState.keyHandled = true;
                    return this.keyHandlerState;
                } else if (isScrollPosInScrollItem()) {
                    int scrollDistance = canScrollDown();
                    if (scrollDistance != -1) {
                        com.navdy.hud.app.audio.SoundUtils.playSound(com.navdy.hud.app.audio.SoundUtils.Sound.MENU_MOVE);
                        if (this.isScrolling) {
                            this.currentScrollBy += 0.5f;
                            if (this.currentScrollBy > 2.0f) {
                                this.currentScrollBy = 2.0f;
                            }
                        } else {
                            this.currentScrollBy = 1.0f;
                        }
                        float distance = ((float) com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.scrollDistance) * this.currentScrollBy;
                        float scrollBy = ((float) scrollDistance) > distance ? distance : (float) scrollDistance;
                        this.isScrolling = true;
                        this.recyclerView.smoothScrollBy(0, (int) scrollBy);
                        this.keyHandlerState.keyHandled = true;
                        return this.keyHandlerState;
                    } else if (!isBottom()) {
                        com.navdy.hud.app.audio.SoundUtils.playSound(com.navdy.hud.app.audio.SoundUtils.Sound.MENU_MOVE);
                        float distance2 = (float) (com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.listItemHeight * 2);
                        int oldPos2 = this.currentMiddlePosition;
                        int newPos = this.currentMiddlePosition + 1;
                        this.switchingScrollBoundary = true;
                        this.isScrolling = true;
                        this.currentMiddlePosition = newPos;
                        this.recyclerView.smoothScrollBy(0, (int) distance2);
                        animate(oldPos2, newPos, true, true, true);
                        this.keyHandlerState.listMoved = true;
                        this.keyHandlerState.keyHandled = true;
                        return this.keyHandlerState;
                    }
                }
            }
            if (isBottom()) {
                sLogger.v("cannot go down:" + this.currentMiddlePosition);
                return this.keyHandlerState;
            }
            this.keyHandlerState.keyHandled = true;
            if (isOverrideKey(com.navdy.hud.app.manager.InputManager.CustomKeyEvent.RIGHT)) {
                com.navdy.hud.app.audio.SoundUtils.playSound(com.navdy.hud.app.audio.SoundUtils.Sound.MENU_MOVE);
                this.keyHandlerState.listMoved = false;
                return this.keyHandlerState;
            }
            this.keyHandlerState.listMoved = true;
            down(this.currentMiddlePosition, this.currentMiddlePosition + 1);
            return this.keyHandlerState;
        }
    }

    private void down(int oldPos, int newPos) {
        com.navdy.hud.app.audio.SoundUtils.playSound(com.navdy.hud.app.audio.SoundUtils.Sound.MENU_MOVE);
        this.currentMiddlePosition = newPos;
        animate(oldPos, newPos);
        this.isScrolling = true;
        this.layoutManager.smoothScrollToPosition(oldPos, -1);
    }

    public void select() {
        select(false);
    }

    /* access modifiers changed from: private */
    public void select(boolean ignoreLock) {
        if (!isLocked() || ignoreLock) {
            this.lockList = true;
            if (this.isScrolling) {
                this.isSelectedOperationPending = true;
                return;
            }
            com.navdy.hud.app.audio.SoundUtils.playSound(com.navdy.hud.app.audio.SoundUtils.Sound.MENU_SELECT);
            int pos = getRawPosition();
            com.navdy.hud.app.ui.component.vlist.VerticalList.Model model = (com.navdy.hud.app.ui.component.vlist.VerticalList.Model) this.modelList.get(pos);
            com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder vh = (com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder) this.recyclerView.findViewHolderForAdapterPosition(pos);
            if (vh != null) {
                vh.select(model, getCurrentPosition(), 50);
            }
        }
    }

    public void lock() {
        this.lockList = true;
    }

    public void unlock() {
        unlock(true);
    }

    public void unlock(boolean startFluctuator) {
        this.lockList = false;
        com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder vh = (com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder) this.recyclerView.findViewHolderForAdapterPosition(getRawPosition());
        if (vh == null) {
            return;
        }
        if (startFluctuator) {
            vh.startFluctuator();
        } else {
            vh.clearAnimation();
        }
    }

    public boolean isTop() {
        if (this.currentMiddlePosition == 1) {
            return true;
        }
        return false;
    }

    public boolean isBottom() {
        if (this.currentMiddlePosition == this.modelList.size() - 2) {
            return true;
        }
        return false;
    }

    public int getCurrentPosition() {
        return this.currentMiddlePosition - 1;
    }

    public com.navdy.hud.app.ui.component.vlist.VerticalList.Model getCurrentModel() {
        return this.adapter.getModel(getRawPosition());
    }

    public com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder getCurrentViewHolder() {
        return getViewHolder(this.currentMiddlePosition);
    }

    public com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder getViewHolder(int pos) {
        if (this.adapter.getModel(pos) != null) {
            return (com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder) this.recyclerView.findViewHolderForAdapterPosition(this.currentMiddlePosition);
        }
        sLogger.i("getViewHolder: invalid pos:" + pos);
        return null;
    }

    public void setViewHolderState(int pos, com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.State state, com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.AnimationType animationType, int animationDuration2) {
        com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder vh = getViewHolder(pos);
        if (vh != null && vh.getState() != state) {
            sLogger.v("setViewHolderState:" + pos + com.navdy.hud.app.framework.glance.GlanceConstants.COLON_SEPARATOR + state);
            vh.setState(state, animationType, animationDuration2);
        }
    }

    public int getRawPosition() {
        return this.currentMiddlePosition;
    }

    private void animate(int oldPos, int newPos) {
        animate(oldPos, newPos, true, false, false);
    }

    private void animate(int oldPos, int newPos, boolean updateIndicator, boolean performSelection, boolean checkAdapterCache) {
        if (oldPos != -1) {
            android.support.v7.widget.RecyclerView.ViewHolder viewHolder = this.recyclerView.findViewHolderForAdapterPosition(oldPos);
            if (viewHolder != null) {
                com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder vh = (com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder) viewHolder;
                if (vh.getState() == com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.State.SELECTED) {
                    vh.setState(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.State.UNSELECTED, com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.AnimationType.MOVE, ITEM_MOVE_ANIMATION_DURATION);
                }
            } else {
                sLogger.v("viewHolder {" + oldPos + "} not found");
            }
            if (performSelection) {
                android.support.v7.widget.RecyclerView.ViewHolder viewHolder2 = this.recyclerView.findViewHolderForAdapterPosition(newPos);
                if (viewHolder2 != null) {
                    com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder vh2 = (com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder) viewHolder2;
                    if (vh2.getState() == com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.State.UNSELECTED && !isCloseMenuVisible()) {
                        vh2.setState(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.State.SELECTED, com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.AnimationType.MOVE, ITEM_MOVE_ANIMATION_DURATION);
                    }
                } else if (checkAdapterCache) {
                    com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder vh3 = this.adapter.getHolderForPos(newPos);
                    if (vh3 == null) {
                        sLogger.v("viewHolder-sel {" + newPos + "} not found in adapter");
                        this.adapter.setHighlightIndex(newPos);
                    } else if (vh3.getState() == com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.State.UNSELECTED && !isCloseMenuVisible()) {
                        vh3.setState(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.State.SELECTED, com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.AnimationType.MOVE, ITEM_MOVE_ANIMATION_DURATION);
                    }
                } else {
                    sLogger.v("viewHolder-sel {" + newPos + "} not found");
                }
            }
        }
        if (updateIndicator) {
            animateIndicator(getCurrentPosition(), this.animationDuration);
        }
    }

    public void animate(int pos, boolean selected, int duration, boolean notAllowStateChange) {
        animate(pos, selected, duration, notAllowStateChange, false);
    }

    public void animate(int pos, boolean selected, int duration, boolean notAllowStateChange, boolean changeToolTips) {
        if (duration == -1) {
            duration = this.animationDuration;
        }
        android.support.v7.widget.RecyclerView.ViewHolder viewHolder = this.recyclerView.findViewHolderForAdapterPosition(pos);
        if (viewHolder != null) {
            com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder vh = (com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder) viewHolder;
            if (selected) {
                if (sLogger.isLoggable(2)) {
                    sLogger.v("animate:" + pos + " selected");
                }
                vh.setState(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.State.SELECTED, com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.AnimationType.MOVE, duration);
                if (vh.hasToolTip() && this.containerCallback != null) {
                    this.containerCallback.showToolTips();
                    return;
                }
                return;
            }
            if (sLogger.isLoggable(2)) {
                sLogger.v("animate:" + pos + " un-selected");
            }
            vh.setState(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.State.UNSELECTED, com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.AnimationType.MOVE, duration);
            if (vh.hasToolTip() && this.containerCallback != null) {
                this.containerCallback.hideToolTips();
            }
        } else if (sLogger.isLoggable(2)) {
            sLogger.v("animate:" + pos + " cannot find viewholder");
        }
    }

    public void animateIndicator(int pos, int duration) {
        if (this.containerCallback == null || !this.containerCallback.isFastScrolling()) {
            if (this.indicatorAnimatorSet != null && this.indicatorAnimatorSet.isRunning()) {
                this.indicatorAnimatorSet.removeAllListeners();
                this.indicatorAnimatorSet.cancel();
                this.indicator.setCurrentItem(getCurrentPosition());
            }
            this.indicatorAnimatorSet = this.indicator.getItemMoveAnimator(pos, -1);
            if (this.indicatorAnimatorSet != null) {
                this.indicatorAnimatorSet.setDuration((long) duration);
                this.indicatorAnimatorSet.addListener(this.indicatorAnimationListener);
                this.indicatorAnimatorSet.start();
            }
        }
    }

    public static android.view.animation.Interpolator getInterpolator() {
        return new com.navdy.hud.app.ui.interpolator.FastOutSlowInInterpolator();
    }

    public void targetFound(int dy) {
        if (!this.hasScrollableElement || !isScrollPosInScrollItem()) {
            int middlePos = getPositionFromScrollY(this.actualScrollY + dy);
            boolean verbose = sLogger.isLoggable(2);
            if (dy != 0 || !this.waitForTarget || middlePos == this.targetPos) {
                this.waitForTarget = false;
                this.targetFound = false;
                this.targetPos = -1;
                com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder viewHolderSel = (com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder) this.recyclerView.findViewHolderForAdapterPosition(middlePos);
                if (viewHolderSel == null) {
                    this.waitForTarget = true;
                    this.targetPos = middlePos;
                    return;
                }
                this.targetFound = true;
                this.currentMiddlePosition = middlePos;
                java.util.HashSet<java.lang.Integer> temp = this.selectedList;
                this.selectedList = new java.util.HashSet<>();
                if (verbose) {
                    sLogger.v("targetFound selected list size = " + temp.size());
                }
                if (temp.size() > 0) {
                    java.util.Iterator<java.lang.Integer> iterator = temp.iterator();
                    while (iterator.hasNext()) {
                        com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder viewHolder = (com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder) this.recyclerView.findViewHolderForAdapterPosition(((java.lang.Integer) iterator.next()).intValue());
                        if (viewHolder != null && viewHolder.getState() == com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.State.SELECTED) {
                            viewHolder.setState(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.State.UNSELECTED, com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.AnimationType.MOVE, ITEM_MOVE_ANIMATION_DURATION);
                        }
                    }
                }
                if (viewHolderSel != null && viewHolderSel.getState() == com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.State.UNSELECTED && !isCloseMenuVisible()) {
                    viewHolderSel.setState(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.State.SELECTED, com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.AnimationType.MOVE, ITEM_MOVE_ANIMATION_DURATION);
                }
            }
        }
    }

    private boolean isScrollItemInMiddle() {
        if (!this.hasScrollableElement || this.scrollItemIndex <= 0 || this.actualScrollY != com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.listItemHeight * (this.scrollItemIndex - 1)) {
            return false;
        }
        return true;
    }

    private boolean isScrollItemAlignedToTopEdge() {
        if (this.hasScrollableElement && this.actualScrollY == com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.listItemHeight * this.scrollItemIndex) {
            return true;
        }
        return false;
    }

    private boolean isScrollItemAlignedToBottomEdge() {
        if (this.hasScrollableElement && this.actualScrollY == this.scrollItemEndY + com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.listItemHeight) {
            return true;
        }
        return false;
    }

    private boolean isScrollPosInScrollItem() {
        if (!this.hasScrollableElement) {
            return false;
        }
        calculateScrollRange();
        if (this.scrollItemStartY == -1 || this.actualScrollY < this.scrollItemStartY || this.actualScrollY > this.scrollItemEndY) {
            return false;
        }
        return true;
    }

    private int canScrollDown() {
        if (!this.hasScrollableElement) {
            return -1;
        }
        calculateScrollRange();
        if (this.scrollItemStartY == -1 || this.actualScrollY >= this.scrollItemEndY) {
            return -1;
        }
        return this.scrollItemEndY - this.actualScrollY;
    }

    private int canScrollUp() {
        if (!this.hasScrollableElement) {
            return -1;
        }
        calculateScrollRange();
        if (this.scrollItemStartY == -1) {
            return -1;
        }
        int scrollItemY = com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.listItemHeight * this.scrollItemIndex;
        if (this.actualScrollY > scrollItemY) {
            return this.actualScrollY - scrollItemY;
        }
        return -1;
    }

    private int getExtraItemCount() {
        return this.adapter.getItemCount() - 3;
    }

    private boolean isLocked() {
        return this.lockList;
    }

    public void performSelectAction(com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState selection) {
        this.callback.select(selection);
    }

    public void addCurrentHighlightAnimation(android.animation.AnimatorSet.Builder builder) {
        com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder vh = (com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder) this.recyclerView.findViewHolderForAdapterPosition(this.currentMiddlePosition - 1);
        if (vh != null) {
            builder.with(android.animation.ObjectAnimator.ofFloat(vh.layout, android.view.View.ALPHA, new float[]{0.0f}));
        }
        com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder vh2 = (com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder) this.recyclerView.findViewHolderForAdapterPosition(this.currentMiddlePosition + 1);
        if (vh2 != null) {
            builder.with(android.animation.ObjectAnimator.ofFloat(vh2.layout, android.view.View.ALPHA, new float[]{0.0f}));
        }
    }

    public void copyAndPosition(android.widget.ImageView imageView, android.widget.TextView title, android.widget.TextView subTitle, android.widget.TextView subTitle2, boolean setImage) {
        com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder vh = (com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder) this.recyclerView.findViewHolderForAdapterPosition(this.currentMiddlePosition);
        if (vh != null) {
            vh.copyAndPosition(imageView, title, subTitle, subTitle2, setImage);
        }
    }

    public void refreshData(int pos) {
        refreshData(pos, (com.navdy.hud.app.ui.component.vlist.VerticalList.Model) null);
    }

    public void refreshData(int pos, com.navdy.hud.app.ui.component.vlist.VerticalList.Model newModel) {
        refreshData(pos, newModel, false);
    }

    public void refreshData(int pos, com.navdy.hud.app.ui.component.vlist.VerticalList.Model newModel, boolean dontStartFluctuator) {
        if (this.firstEntryBlank) {
            pos++;
        }
        com.navdy.hud.app.ui.component.vlist.VerticalList.Model currentModel = this.adapter.getModel(pos);
        if (currentModel == null) {
            sLogger.i("refreshData: invalid model pos:" + pos);
            return;
        }
        if (newModel == null) {
            currentModel.needsRebind = true;
            currentModel.dontStartFluctuator = dontStartFluctuator;
        } else {
            newModel.needsRebind = true;
            this.adapter.updateModel(pos, newModel);
        }
        this.adapter.notifyItemChanged(pos);
    }

    public void refreshData(int pos, com.navdy.hud.app.ui.component.vlist.VerticalList.Model[] newModels) {
        if (this.firstEntryBlank) {
            pos++;
        }
        int itemsUpdated = 0;
        for (int i = 0; i < newModels.length; i++) {
            int newPos = pos + i;
            if (this.adapter.getModel(newPos) == null) {
                sLogger.i("refreshData(s): invalid model pos:" + newPos);
            } else {
                itemsUpdated++;
                this.adapter.updateModel(newPos, newModels[i]);
            }
        }
        this.adapter.setInitialState(false);
        this.adapter.notifyItemRangeChanged(pos, itemsUpdated);
        sLogger.i("refreshData(s) pos=" + pos + " len=" + itemsUpdated);
    }

    public static android.widget.ImageView findImageView(android.view.View layout) {
        return (android.widget.ImageView) layout.findViewById(com.navdy.hud.app.R.id.big);
    }

    public static android.widget.ImageView findSmallImageView(android.view.View layout) {
        return (android.widget.ImageView) layout.findViewById(com.navdy.hud.app.R.id.small);
    }

    public static android.view.View findCrossFadeImageView(android.view.View layout) {
        return layout.findViewById(com.navdy.hud.app.R.id.vlist_image);
    }

    public void cancelLoadingAnimation(int pos) {
        int userpos = pos;
        if (this.firstEntryBlank) {
            userpos++;
        }
        com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder vh = (com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder) this.recyclerView.findViewHolderForAdapterPosition(userpos);
        if (vh != null && vh.getModelType() == com.navdy.hud.app.ui.component.vlist.VerticalList.ModelType.LOADING) {
            vh.clearAnimation();
        }
    }

    private boolean isOverrideKey(com.navdy.hud.app.manager.InputManager.CustomKeyEvent keyEvent) {
        com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder vh = (com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder) this.recyclerView.findViewHolderForAdapterPosition(this.currentMiddlePosition);
        if (vh == null || !vh.handleKey(keyEvent)) {
            return false;
        }
        return true;
    }

    public boolean isFirstEntryBlank() {
        return this.firstEntryBlank;
    }

    public boolean hasScrollItem() {
        return this.hasScrollableElement;
    }

    public com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState getItemSelectionState() {
        return this.itemSelectionState;
    }

    public void callItemSelected(com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState itemSelectionState2) {
        if (this.callback != null) {
            this.callback.onItemSelected(itemSelectionState2);
        }
    }

    public void clearAllAnimations() {
        int count = this.recyclerView.getChildCount();
        int cleared = 0;
        for (int i = 0; i < count; i++) {
            try {
                com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder holder = (com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder) this.recyclerView.findViewHolderForAdapterPosition(i);
                if (holder != null) {
                    holder.clearAnimation();
                    cleared++;
                }
            } catch (Throwable t) {
                sLogger.e(t);
            }
        }
        sLogger.v("clearAllAnimations current:" + cleared);
        android.support.v7.widget.RecyclerView.RecycledViewPool pool = this.recyclerView.getRecycledViewPool();
        if (pool != null) {
            this.copyList.clear();
            clearViewAnimation(pool, com.navdy.hud.app.ui.component.vlist.VerticalList.ModelType.ICON_BKCOLOR.ordinal());
            clearViewAnimation(pool, com.navdy.hud.app.ui.component.vlist.VerticalList.ModelType.TWO_ICONS.ordinal());
            clearViewAnimation(pool, com.navdy.hud.app.ui.component.vlist.VerticalList.ModelType.ICON.ordinal());
            clearViewAnimation(pool, com.navdy.hud.app.ui.component.vlist.VerticalList.ModelType.LOADING.ordinal());
            clearViewAnimation(pool, com.navdy.hud.app.ui.component.vlist.VerticalList.ModelType.LOADING_CONTENT.ordinal());
            clearViewAnimation(pool, com.navdy.hud.app.ui.component.vlist.VerticalList.ModelType.ICON_OPTIONS.ordinal());
            cleanupView(pool, com.navdy.hud.app.ui.component.vlist.VerticalList.ModelType.SCROLL_CONTENT.ordinal());
            int n = this.copyList.size();
            if (n > 0) {
                java.util.Iterator it = this.copyList.iterator();
                while (it.hasNext()) {
                    pool.putRecycledView((android.support.v7.widget.RecyclerView.ViewHolder) it.next());
                }
                sLogger.v("clearAllAnimations pool:" + n);
                this.copyList.clear();
            }
        }
    }

    private void clearViewAnimation(android.support.v7.widget.RecyclerView.RecycledViewPool pool, int viewType) {
        while (true) {
            com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder viewHolder = (com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder) pool.getRecycledView(viewType);
            if (viewHolder != null) {
                viewHolder.clearAnimation();
                this.copyList.add(viewHolder);
            } else {
                return;
            }
        }
    }

    private void cleanupView(android.support.v7.widget.RecyclerView.RecycledViewPool pool, int viewType) {
        while (true) {
            com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder viewHolder = (com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder) pool.getRecycledView(viewType);
            if (viewHolder != null) {
                viewHolder.clearAnimation();
                viewHolder.layout.removeAllViews();
            } else {
                return;
            }
        }
    }

    public static void setFontSize(com.navdy.hud.app.ui.component.vlist.VerticalList.Model model, boolean allowTwoLineTitles) {
        model.fontSizeCheckDone = true;
        model.subTitle_2Lines = false;
        if (model.title == null) {
            model.fontInfo = (com.navdy.hud.app.ui.component.vlist.VerticalList.FontInfo) tit_subt_map.get(com.navdy.hud.app.ui.component.vlist.VerticalList.FontSize.FONT_SIZE_26);
            model.fontSize = com.navdy.hud.app.ui.component.vlist.VerticalList.FontSize.FONT_SIZE_26;
        } else if (model.subTitle2 != null || model.subTitle == null) {
            setTitleSize(model, allowTwoLineTitles);
        } else {
            setTitleSubTitleSize(model, allowTwoLineTitles);
        }
    }

    private static void setTitleSubTitleSize(com.navdy.hud.app.ui.component.vlist.VerticalList.Model model, boolean allowTwoLineTitles) {
        android.text.TextPaint paint = fontSizeTextView.getPaint();
        fontSizeTextView.setTextSize(vlistTitle_18);
        android.text.StaticLayout layout = new android.text.StaticLayout(model.subTitle, paint, vlistTitleTextW, android.text.Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);
        int subTitleLines = layout.getLineCount();
        int subTitleTotal = subTitleLines;
        if (!android.text.TextUtils.isEmpty(model.subTitle2)) {
            subTitleTotal++;
        }
        if (subTitleTotal > 1) {
            allowTwoLineTitles = false;
        }
        setTitleSize(model, allowTwoLineTitles);
        if (subTitleLines != 1) {
            model.subTitle_2Lines = true;
            com.navdy.hud.app.ui.component.vlist.VerticalList.FontSize newFontSize = null;
            if (layout.getLineCount() != 2) {
                switch (model.fontSize) {
                    case FONT_SIZE_26:
                        newFontSize = com.navdy.hud.app.ui.component.vlist.VerticalList.FontSize.FONT_SIZE_26_16;
                        break;
                    case FONT_SIZE_22:
                        newFontSize = com.navdy.hud.app.ui.component.vlist.VerticalList.FontSize.FONT_SIZE_22_16;
                        break;
                    case FONT_SIZE_22_2:
                        newFontSize = com.navdy.hud.app.ui.component.vlist.VerticalList.FontSize.FONT_SIZE_22_2_16;
                        break;
                    case FONT_SIZE_18:
                        newFontSize = com.navdy.hud.app.ui.component.vlist.VerticalList.FontSize.FONT_SIZE_18_16;
                        break;
                    case FONT_SIZE_18_2:
                        newFontSize = com.navdy.hud.app.ui.component.vlist.VerticalList.FontSize.FONT_SIZE_18_2_16;
                        break;
                    case FONT_SIZE_16:
                        newFontSize = com.navdy.hud.app.ui.component.vlist.VerticalList.FontSize.FONT_SIZE_16_16;
                        break;
                    case FONT_SIZE_16_2:
                        newFontSize = com.navdy.hud.app.ui.component.vlist.VerticalList.FontSize.FONT_SIZE_16_2_16;
                        break;
                }
            } else {
                switch (model.fontSize) {
                    case FONT_SIZE_26:
                        newFontSize = com.navdy.hud.app.ui.component.vlist.VerticalList.FontSize.FONT_SIZE_26_18;
                        break;
                    case FONT_SIZE_22:
                        newFontSize = com.navdy.hud.app.ui.component.vlist.VerticalList.FontSize.FONT_SIZE_22_18;
                        break;
                    case FONT_SIZE_22_2:
                        newFontSize = com.navdy.hud.app.ui.component.vlist.VerticalList.FontSize.FONT_SIZE_22_2_16;
                        break;
                    case FONT_SIZE_18:
                        newFontSize = com.navdy.hud.app.ui.component.vlist.VerticalList.FontSize.FONT_SIZE_18_16;
                        break;
                    case FONT_SIZE_18_2:
                        newFontSize = com.navdy.hud.app.ui.component.vlist.VerticalList.FontSize.FONT_SIZE_18_2_16;
                        break;
                    case FONT_SIZE_16:
                        newFontSize = com.navdy.hud.app.ui.component.vlist.VerticalList.FontSize.FONT_SIZE_16_16;
                        break;
                    case FONT_SIZE_16_2:
                        newFontSize = com.navdy.hud.app.ui.component.vlist.VerticalList.FontSize.FONT_SIZE_16_2_16;
                        break;
                }
            }
            if (newFontSize != null) {
                model.fontInfo = (com.navdy.hud.app.ui.component.vlist.VerticalList.FontInfo) tit_subt_map_2_lines.get(newFontSize);
                model.fontSize = newFontSize;
                sLogger.d("Adjusting size for " + model.title + " to " + newFontSize);
            }
        }
    }

    private static void setTitleSize(com.navdy.hud.app.ui.component.vlist.VerticalList.Model model, boolean allowTwoLines) {
        fontSizeTextView.setText(model.title);
        int[] index = new int[2];
        com.navdy.hud.app.util.ViewUtil.autosize(fontSizeTextView, allowTwoLines ? TWO_LINE_MAX_LINES : SINGLE_LINE_MAX_LINES, vlistTitleTextW, TITLE_SIZES, index);
        com.navdy.hud.app.ui.component.vlist.VerticalList.FontSize fontSize = FONT_SIZES[(index[0] * 2) + (index[1] - 1)];
        sLogger.d("Setting size for " + model.title + " to " + fontSize);
        if (model.subTitle == null || model.subTitle2 == null) {
            model.fontInfo = (com.navdy.hud.app.ui.component.vlist.VerticalList.FontInfo) tit_subt_map.get(fontSize);
        } else {
            model.fontInfo = (com.navdy.hud.app.ui.component.vlist.VerticalList.FontInfo) tit_subt_subt2_map.get(fontSize);
        }
        model.fontSize = fontSize;
    }

    /* access modifiers changed from: private */
    public boolean isCloseMenuVisible() {
        if (this.containerCallback != null) {
            return this.containerCallback.isCloseMenuVisible();
        }
        return false;
    }

    /* access modifiers changed from: private */
    public void calculateScrollRange() {
        com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder viewHolder = (com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder) this.recyclerView.findViewHolderForAdapterPosition(this.scrollItemIndex);
        if (!(viewHolder instanceof com.navdy.hud.app.ui.component.vlist.viewholder.ScrollableViewHolder) || viewHolder.layout.getChildCount() <= 0) {
            sLogger.v("calculateScrollRange:" + this.scrollItemIndex + " vh not found");
            return;
        }
        this.scrollItemStartY = com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.listItemHeight * this.scrollItemIndex;
        this.scrollItemHeight = viewHolder.layout.getChildAt(0).getMeasuredHeight();
        this.scrollItemEndY = (this.scrollItemStartY + this.scrollItemHeight) - (com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.listItemHeight * 3);
        if (this.reCalcScrollPos) {
            sLogger.v("calculateScrollPos: recalc scrollItemHeight=" + this.scrollItemHeight + " scrollItemStartY=" + this.scrollItemStartY + " scrollItemEndY=" + this.scrollItemEndY);
            calculateScrollPos(this.currentMiddlePosition);
        }
    }

    /* access modifiers changed from: private */
    public int getPositionFromScrollY(int scrollPos) {
        if (!this.hasScrollableElement) {
            return (scrollPos / com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.listItemHeight) + 1;
        }
        if (this.scrollItemStartY == -1) {
            calculateScrollRange();
        }
        if (scrollPos < this.scrollItemStartY) {
            return (scrollPos / com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.listItemHeight) + 1;
        }
        if (scrollPos > this.scrollItemEndY) {
            return (((scrollPos - this.scrollItemEndY) - (com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.listItemHeight * 2)) / com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.listItemHeight) + 1 + this.scrollItemIndex;
        }
        return this.scrollItemIndex;
    }

    private void calculateScrollPos(int pos) {
        if (this.scrollItemHeight != -1 && pos == this.scrollItemIndex + 1) {
            this.reCalcScrollPos = false;
            this.actualScrollY = this.scrollItemIndex * com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.listItemHeight;
            this.actualScrollY += com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.listItemHeight * 2;
            sLogger.v("onScrollStateChanged(( calculateScrollPos:" + this.actualScrollY + " pos=" + pos + " scrollIndex=" + this.scrollItemIndex);
        }
    }
}
