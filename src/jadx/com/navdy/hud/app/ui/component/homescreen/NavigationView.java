package com.navdy.hud.app.ui.component.homescreen;

public class NavigationView extends android.widget.FrameLayout implements com.navdy.hud.app.manager.InputManager.IInputHandler, com.navdy.hud.app.gesture.GestureDetector.GestureListener, com.navdy.hud.app.ui.component.homescreen.IHomeScreenLifecycle {
    private static final int MAP_MASK_OFFSET = 1;
    private static final com.navdy.hud.app.maps.MapEvents.DialMapZoom ZOOM_IN = new com.navdy.hud.app.maps.MapEvents.DialMapZoom(com.navdy.hud.app.maps.MapEvents.DialMapZoom.Type.IN);
    private static final com.navdy.hud.app.maps.MapEvents.DialMapZoom ZOOM_OUT = new com.navdy.hud.app.maps.MapEvents.DialMapZoom(com.navdy.hud.app.maps.MapEvents.DialMapZoom.Type.OUT);
    /* access modifiers changed from: private */
    public com.squareup.otto.Bus bus;
    private java.lang.Runnable cleanupMapOverviewRunnable;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.profile.DriverProfileManager driverProfileManager;
    /* access modifiers changed from: private */
    public com.here.android.mpa.mapping.MapMarker endMarker;
    private com.here.android.mpa.mapping.Map.OnTransformListener fluctuatorPosListener;
    /* access modifiers changed from: private */
    public android.os.Handler handler;
    private com.navdy.hud.app.maps.here.HereLocationFixManager hereLocationFixManager;
    private com.navdy.hud.app.maps.here.HereMapsManager hereMapsManager;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.ui.component.homescreen.HomeScreenView homeScreenView;
    private boolean initComplete;
    private boolean isRenderingEnabled;
    /* access modifiers changed from: private */
    public com.navdy.service.library.log.Logger logger;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.maps.here.HereMapController mapController;
    @butterknife.InjectView(2131624339)
    android.widget.ImageView mapIconIndicator;
    private java.util.List<com.here.android.mpa.mapping.MapObject> mapMarkerList;
    @butterknife.InjectView(2131624121)
    android.widget.ImageView mapMask;
    @butterknife.InjectView(2131624337)
    com.here.android.mpa.mapping.MapView mapView;
    private boolean networkCheckRequired;
    @butterknife.InjectView(2131624340)
    com.navdy.hud.app.ui.component.homescreen.NoLocationView noLocationView;
    private java.util.ArrayList<com.here.android.mpa.mapping.MapObject> overviewMapRouteObjects;
    private boolean paused;
    private java.lang.Runnable resetMapOverviewRunnable;
    private com.here.android.mpa.common.Image selectedDestinationImage;
    private boolean setInitialCenter;
    /* access modifiers changed from: private */
    public java.lang.Runnable showMapIconIndicatorRunnable;
    @butterknife.InjectView(2131624338)
    com.navdy.hud.app.ui.component.FluctuatorAnimatorView startDestinationFluctuatorView;
    /* access modifiers changed from: private */
    public com.here.android.mpa.mapping.MapMarker startMarker;
    private com.navdy.hud.app.ui.framework.UIStateManager uiStateManager;
    private com.here.android.mpa.common.Image unselectedDestinationImage;

    class Anon1 implements com.here.android.mpa.mapping.Map.OnTransformListener {
        Anon1() {
        }

        public void onMapTransformStart() {
        }

        public void onMapTransformEnd(com.here.android.mpa.mapping.MapState mapState) {
            com.navdy.hud.app.ui.component.homescreen.NavigationView.this.mapController.removeTransformListener(this);
            com.navdy.hud.app.ui.component.homescreen.NavigationView.this.positionFluctuator();
        }
    }

    class Anon10 implements java.lang.Runnable {

        class Anon1 implements java.lang.Runnable {
            final /* synthetic */ int val$x;
            final /* synthetic */ int val$y;

            Anon1(int i, int i2) {
                this.val$x = i;
                this.val$y = i2;
            }

            public void run() {
                com.navdy.hud.app.maps.here.HereMapController.State state = com.navdy.hud.app.ui.component.homescreen.NavigationView.this.mapController.getState();
                if (state == com.navdy.hud.app.maps.here.HereMapController.State.ROUTE_PICKER) {
                    com.navdy.hud.app.ui.component.homescreen.NavigationView.this.startDestinationFluctuatorView.setX((float) (this.val$x - com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.transformCenterMoveX));
                    com.navdy.hud.app.ui.component.homescreen.NavigationView.this.startDestinationFluctuatorView.setY((float) this.val$y);
                    com.navdy.hud.app.ui.component.homescreen.NavigationView.this.startDestinationFluctuatorView.setVisibility(0);
                    com.navdy.hud.app.ui.component.homescreen.NavigationView.this.startDestinationFluctuatorView.start();
                    return;
                }
                com.navdy.hud.app.ui.component.homescreen.NavigationView.this.logger.v("fluctuator state from route search mode:" + state);
            }
        }

        Anon10() {
        }

        public void run() {
            com.here.android.mpa.mapping.Map.PixelResult result = com.navdy.hud.app.ui.component.homescreen.NavigationView.this.mapController.projectToPixel(com.navdy.hud.app.ui.component.homescreen.NavigationView.this.startMarker.getCoordinate());
            if (result != null) {
                android.graphics.PointF pointF = result.getResult();
                if (pointF != null) {
                    com.navdy.hud.app.ui.component.homescreen.NavigationView.this.handler.post(new com.navdy.hud.app.ui.component.homescreen.NavigationView.Anon10.Anon1(((int) pointF.x) - (com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.startFluctuatorDimension / 2), ((int) pointF.y) - (com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.startFluctuatorDimension / 2)));
                }
            }
        }
    }

    class Anon11 implements java.lang.Runnable {
        final /* synthetic */ com.here.android.mpa.common.GeoCoordinate val$center;

        Anon11(com.here.android.mpa.common.GeoCoordinate geoCoordinate) {
            this.val$center = geoCoordinate;
        }

        public void run() {
            try {
                com.navdy.hud.app.ui.component.homescreen.NavigationView.this.logger.v("check if prev route was active");
                com.navdy.hud.app.maps.here.HereMapUtil.SavedRouteData savedRouteData = com.navdy.hud.app.maps.here.HereMapUtil.getSavedRouteData(com.navdy.hud.app.ui.component.homescreen.NavigationView.this.logger);
                com.navdy.service.library.events.navigation.NavigationRouteRequest request = savedRouteData.navigationRouteRequest;
                if (request == null) {
                    com.navdy.hud.app.ui.component.homescreen.NavigationView.this.logger.v("no saved route info");
                    com.navdy.hud.app.framework.destinations.DestinationsManager.getInstance().launchSuggestedDestination();
                } else {
                    com.navdy.hud.app.ui.component.homescreen.NavigationView.this.logger.v("saved route found");
                    com.navdy.hud.app.maps.here.HereRouteManager.printRouteRequest(request);
                    if (com.navdy.hud.app.ui.component.homescreen.NavigationView.this.homeScreenView.isNavigationActive()) {
                        com.navdy.hud.app.ui.component.homescreen.NavigationView.this.logger.v("navigation is already active or route being calculated,ignore the saved route");
                        com.navdy.hud.app.framework.destinations.DestinationsManager.getInstance().clearSuggestedDestination();
                        com.navdy.hud.app.maps.here.HereMapUtil.removeRouteInfo(com.navdy.hud.app.ui.component.homescreen.NavigationView.this.logger, false);
                        return;
                    }
                    com.here.android.mpa.common.GeoCoordinate target = new com.here.android.mpa.common.GeoCoordinate(request.destination.latitude.doubleValue(), request.destination.longitude.doubleValue(), 0.0d);
                    double distance = this.val$center.distanceTo(target);
                    com.navdy.hud.app.ui.component.homescreen.NavigationView.this.logger.v("distance remaining to destination is [" + distance + "] current[" + this.val$center + "] target [" + target + "]");
                    boolean isGasRoute = request.routeAttributes.contains(com.navdy.service.library.events.navigation.NavigationRouteRequest.RouteAttribute.ROUTE_ATTRIBUTE_GAS);
                    if (isGasRoute && distance >= 0.0d && distance <= 500.0d) {
                        com.navdy.hud.app.ui.component.homescreen.NavigationView.this.logger.v("gas route and distance to destination is <= 500.0, don't navigate");
                        com.navdy.hud.app.ui.component.homescreen.NavigationView.this.logger.v("remove gas route and look for non gas route");
                        com.navdy.hud.app.maps.here.HereMapUtil.removeRouteInfo(com.navdy.hud.app.ui.component.homescreen.NavigationView.this.logger, false);
                        savedRouteData = com.navdy.hud.app.maps.here.HereMapUtil.getSavedRouteData(com.navdy.hud.app.ui.component.homescreen.NavigationView.this.logger);
                        request = savedRouteData.navigationRouteRequest;
                        if (request == null) {
                            com.navdy.hud.app.ui.component.homescreen.NavigationView.this.logger.v("no non gas route");
                            com.navdy.hud.app.framework.destinations.DestinationsManager.getInstance().launchSuggestedDestination();
                            com.navdy.hud.app.maps.here.HereMapUtil.removeRouteInfo(com.navdy.hud.app.ui.component.homescreen.NavigationView.this.logger, false);
                            return;
                        }
                        isGasRoute = false;
                        distance = this.val$center.distanceTo(new com.here.android.mpa.common.GeoCoordinate(request.destination.latitude.doubleValue(), request.destination.longitude.doubleValue(), 0.0d));
                    }
                    if (isGasRoute || distance < 0.0d || distance > 500.0d) {
                        com.navdy.hud.app.framework.destinations.DestinationsManager.getInstance().clearSuggestedDestination();
                        com.navdy.service.library.events.navigation.NavigationRouteRequest request2 = new com.navdy.service.library.events.navigation.NavigationRouteRequest.Builder(request).originDisplay(java.lang.Boolean.valueOf(true)).cancelCurrent(java.lang.Boolean.valueOf(false)).build();
                        com.navdy.hud.app.ui.component.homescreen.NavigationView.this.logger.v("sleep before starting navigation");
                        com.navdy.hud.app.util.GenericUtil.sleep(2000);
                        com.navdy.hud.app.ui.component.homescreen.NavigationView.this.logger.v("slept");
                        if (!savedRouteData.isGasRoute || !com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getFeatureUtil().isFeatureEnabled(com.navdy.hud.app.util.FeatureUtil.Feature.FUEL_ROUTING)) {
                            com.navdy.hud.app.ui.component.homescreen.NavigationView.this.logger.v("start saveroute navigation");
                        } else {
                            com.navdy.hud.app.ui.component.homescreen.NavigationView.this.logger.v("start saveroute navigation (gas route)");
                        }
                        com.navdy.hud.app.ui.component.homescreen.NavigationView.this.bus.post(request2);
                        com.navdy.hud.app.ui.component.homescreen.NavigationView.this.bus.post(new com.navdy.hud.app.event.RemoteEvent(request2));
                    } else {
                        com.navdy.hud.app.ui.component.homescreen.NavigationView.this.logger.v("distance to destination is <= 500, don't navigate");
                        com.navdy.hud.app.framework.destinations.DestinationsManager.getInstance().launchSuggestedDestination();
                        com.navdy.hud.app.maps.here.HereMapUtil.removeRouteInfo(com.navdy.hud.app.ui.component.homescreen.NavigationView.this.logger, false);
                    }
                }
            } catch (Throwable t) {
                com.navdy.hud.app.ui.component.homescreen.NavigationView.this.logger.e(t);
            } finally {
                com.navdy.hud.app.maps.here.HereMapUtil.removeRouteInfo(com.navdy.hud.app.ui.component.homescreen.NavigationView.this.logger, false);
            }
        }
    }

    class Anon2 implements java.lang.Runnable {
        Anon2() {
        }

        public void run() {
            com.navdy.hud.app.ui.component.homescreen.NavigationView.this.mapIconIndicator.setVisibility(0);
        }
    }

    class Anon3 implements java.lang.Runnable {
        final /* synthetic */ boolean val$animate;
        final /* synthetic */ java.lang.Runnable val$endAction;
        final /* synthetic */ com.here.android.mpa.common.GeoCoordinate val$geoCoordinate;
        final /* synthetic */ com.here.android.mpa.routing.Route val$route;
        final /* synthetic */ double val$zoomLevel;

        class Anon1 implements com.here.android.mpa.mapping.Map.OnTransformListener {
            Anon1() {
            }

            public void onMapTransformStart() {
            }

            public void onMapTransformEnd(com.here.android.mpa.mapping.MapState mapState) {
                com.navdy.hud.app.ui.component.homescreen.NavigationView.this.logger.v("switchToOverviewMode:no route  tranform end");
                com.navdy.hud.app.ui.component.homescreen.NavigationView.this.mapController.removeTransformListener(this);
                com.navdy.hud.app.ui.component.homescreen.NavigationView.this.handler.post(com.navdy.hud.app.ui.component.homescreen.NavigationView.Anon3.this.val$endAction);
            }
        }

        class Anon2 implements com.here.android.mpa.mapping.Map.OnTransformListener {
            Anon2() {
            }

            public void onMapTransformStart() {
            }

            public void onMapTransformEnd(com.here.android.mpa.mapping.MapState mapState) {
                com.navdy.hud.app.ui.component.homescreen.NavigationView.this.logger.v("switchToOverviewMode:map tranform end");
                com.navdy.hud.app.ui.component.homescreen.NavigationView.this.mapController.removeTransformListener(this);
                com.navdy.hud.app.ui.component.homescreen.NavigationView.this.handler.post(com.navdy.hud.app.ui.component.homescreen.NavigationView.Anon3.this.val$endAction);
            }
        }

        Anon3(com.here.android.mpa.routing.Route route, java.lang.Runnable runnable, com.here.android.mpa.common.GeoCoordinate geoCoordinate, boolean z, double d) {
            this.val$route = route;
            this.val$endAction = runnable;
            this.val$geoCoordinate = geoCoordinate;
            this.val$animate = z;
            this.val$zoomLevel = d;
        }

        public void run() {
            long l1 = android.os.SystemClock.elapsedRealtime();
            boolean isTrafficEnabled = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getDriverProfileManager().isTrafficEnabled();
            if (this.val$route == null) {
                com.navdy.hud.app.ui.component.homescreen.NavigationView.this.logger.v("switchToOverviewMode:no route traffic=" + isTrafficEnabled);
                com.navdy.hud.app.ui.component.homescreen.NavigationView.this.setTransformCenter(com.navdy.hud.app.maps.here.HereMapController.State.OVERVIEW);
                com.navdy.hud.app.ui.component.homescreen.NavigationView.this.mapController.addTransformListener(new com.navdy.hud.app.ui.component.homescreen.NavigationView.Anon3.Anon1());
                com.navdy.hud.app.ui.component.homescreen.NavigationView.this.mapController.setCenterForState(com.navdy.hud.app.maps.here.HereMapController.State.OVERVIEW, this.val$geoCoordinate, this.val$animate ? com.here.android.mpa.mapping.Map.Animation.BOW : com.here.android.mpa.mapping.Map.Animation.NONE, this.val$zoomLevel, 0.0f, 0.0f);
                if (com.navdy.hud.app.ui.component.homescreen.NavigationView.this.startMarker != null) {
                    com.navdy.hud.app.ui.component.homescreen.NavigationView.this.startMarker.setCoordinate(this.val$geoCoordinate);
                    com.navdy.hud.app.ui.component.homescreen.NavigationView.this.mapController.addMapObject(com.navdy.hud.app.ui.component.homescreen.NavigationView.this.startMarker);
                    return;
                }
                return;
            }
            com.navdy.hud.app.ui.component.homescreen.NavigationView.this.logger.v("switchToOverviewMode:route traffic=" + isTrafficEnabled);
            if (com.navdy.hud.app.ui.component.homescreen.NavigationView.this.startMarker != null) {
                com.navdy.hud.app.ui.component.homescreen.NavigationView.this.startMarker.setCoordinate(this.val$geoCoordinate);
                com.navdy.hud.app.ui.component.homescreen.NavigationView.this.mapController.addMapObject(com.navdy.hud.app.ui.component.homescreen.NavigationView.this.startMarker);
            }
            com.navdy.hud.app.ui.component.homescreen.NavigationView.this.logger.v("switchToOverviewMode:map:executeSynchronized");
            com.navdy.hud.app.ui.component.homescreen.NavigationView.this.mapController.setTilt(0.0f);
            if (this.val$endAction != null) {
                com.navdy.hud.app.ui.component.homescreen.NavigationView.this.mapController.addTransformListener(new com.navdy.hud.app.ui.component.homescreen.NavigationView.Anon3.Anon2());
            }
            com.navdy.hud.app.ui.component.homescreen.NavigationView.this.setTransformCenter(com.navdy.hud.app.maps.here.HereMapController.State.OVERVIEW);
            com.navdy.hud.app.ui.component.homescreen.NavigationView.this.mapController.zoomTo(this.val$route.getBoundingBox(), com.navdy.hud.app.ui.component.homescreen.HomeScreenConstants.routeOverviewRect, this.val$animate ? com.here.android.mpa.mapping.Map.Animation.BOW : com.here.android.mpa.mapping.Map.Animation.NONE, 0.0f);
            com.navdy.hud.app.ui.component.homescreen.NavigationView.this.logger.v("switchToOverviewMode took [" + (android.os.SystemClock.elapsedRealtime() - l1) + "]");
        }
    }

    class Anon4 implements java.lang.Runnable {
        final /* synthetic */ java.lang.Runnable val$endAction;

        Anon4(java.lang.Runnable runnable) {
            this.val$endAction = runnable;
        }

        public void run() {
            com.navdy.hud.app.ui.component.homescreen.NavigationView.this.setMapToArMode(true, false, false, null, -1.0d, -1.0d, false);
            com.navdy.hud.app.ui.component.homescreen.NavigationView.this.logger.v("animateBackfromOverview: add transform");
            com.navdy.hud.app.ui.component.homescreen.NavigationView.this.mapController.addTransformListener(com.navdy.hud.app.ui.component.homescreen.NavigationView.this.animateBackfromOverviewMap(this.val$endAction));
        }
    }

    class Anon5 implements java.lang.Runnable {
        Anon5() {
        }

        public void run() {
            com.navdy.hud.app.ui.component.homescreen.NavigationView.this.logger.v("resetMapOverview: " + com.navdy.hud.app.ui.component.homescreen.NavigationView.this.mapController.getState());
            com.navdy.hud.app.ui.component.homescreen.NavigationView.this.cleanupMapOverview();
            if (com.navdy.hud.app.maps.here.HereMapsManager.getInstance().isInitialized()) {
                com.navdy.hud.app.maps.here.HereMapCameraManager hereMapCameraManager = com.navdy.hud.app.maps.here.HereMapCameraManager.getInstance();
                if (hereMapCameraManager.isOverviewZoomLevel()) {
                    com.navdy.hud.app.ui.component.homescreen.NavigationView.this.showOverviewMap(null, null, hereMapCameraManager.getLastGeoCoordinate(), false);
                }
            }
        }
    }

    class Anon6 implements java.lang.Runnable {
        Anon6() {
        }

        public void run() {
            com.navdy.hud.app.ui.component.homescreen.NavigationView.this.logger.v("cleanupMapOverview");
            if (com.navdy.hud.app.ui.component.homescreen.NavigationView.this.startMarker != null) {
                com.navdy.hud.app.ui.component.homescreen.NavigationView.this.mapController.removeMapObject(com.navdy.hud.app.ui.component.homescreen.NavigationView.this.startMarker);
            }
            if (com.navdy.hud.app.ui.component.homescreen.NavigationView.this.endMarker != null) {
                com.navdy.hud.app.ui.component.homescreen.NavigationView.this.mapController.removeMapObject(com.navdy.hud.app.ui.component.homescreen.NavigationView.this.endMarker);
            }
            com.navdy.hud.app.ui.component.homescreen.NavigationView.this.cleanupMapOverviewRoutes();
            com.navdy.hud.app.ui.component.homescreen.NavigationView.this.cleanupFluctuator();
        }
    }

    class Anon7 implements java.lang.Runnable {
        final /* synthetic */ boolean val$showRawLocation;

        Anon7(boolean z) {
            this.val$showRawLocation = z;
        }

        public void run() {
            com.here.android.mpa.mapping.PositionIndicator positionIndicator = com.navdy.hud.app.ui.component.homescreen.NavigationView.this.mapController.getPositionIndicator();
            try {
                if (this.val$showRawLocation) {
                    com.here.android.mpa.common.Image image = new com.here.android.mpa.common.Image();
                    image.setImageResource(com.navdy.hud.app.R.drawable.here_pos_indicator);
                    positionIndicator.setMarker(image);
                }
                positionIndicator.setVisible(this.val$showRawLocation);
            } catch (Throwable t) {
                com.navdy.hud.app.ui.component.homescreen.NavigationView.this.logger.e(t);
            }
        }
    }

    class Anon8 implements java.lang.Runnable {
        final /* synthetic */ boolean val$animate;
        final /* synthetic */ boolean val$changeState;
        final /* synthetic */ boolean val$cleanupMapOverview;
        final /* synthetic */ com.here.android.mpa.common.GeoPosition val$geoPosition;
        final /* synthetic */ double val$tilt;
        final /* synthetic */ boolean val$useLastZoom;
        final /* synthetic */ double val$zoom;

        Anon8(boolean z, boolean z2, boolean z3, com.here.android.mpa.common.GeoPosition geoPosition, double d, double d2, boolean z4) {
            this.val$animate = z;
            this.val$changeState = z2;
            this.val$useLastZoom = z3;
            this.val$geoPosition = geoPosition;
            this.val$zoom = d;
            this.val$tilt = d2;
            this.val$cleanupMapOverview = z4;
        }

        public void run() {
            com.navdy.hud.app.ui.component.homescreen.NavigationView.this.setMapToArModeInternal(this.val$animate, this.val$changeState, this.val$useLastZoom, this.val$geoPosition, this.val$zoom, (float) this.val$tilt, this.val$cleanupMapOverview);
        }
    }

    class Anon9 implements com.here.android.mpa.mapping.Map.OnTransformListener {
        final /* synthetic */ java.lang.Runnable val$endAction;

        class Anon1 implements java.lang.Runnable {
            Anon1() {
            }

            public void run() {
                com.navdy.hud.app.ui.component.homescreen.NavigationView.this.handler.post(com.navdy.hud.app.ui.component.homescreen.NavigationView.this.showMapIconIndicatorRunnable);
                if (com.navdy.hud.app.ui.component.homescreen.NavigationView.this.startMarker != null) {
                    com.navdy.hud.app.ui.component.homescreen.NavigationView.this.mapController.removeMapObject(com.navdy.hud.app.ui.component.homescreen.NavigationView.this.startMarker);
                }
                com.here.android.mpa.mapping.MapRoute currentMapRoute = com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().getCurrentMapRoute();
                com.navdy.hud.app.ui.component.homescreen.NavigationView.this.logger.v("animateBackfromOverview traffic=" + com.navdy.hud.app.ui.component.homescreen.NavigationView.this.driverProfileManager.isTrafficEnabled());
                com.navdy.hud.app.ui.component.homescreen.NavigationView.this.mapController.setState(com.navdy.hud.app.maps.here.HereMapController.State.AR_MODE);
                if (com.navdy.hud.app.ui.component.homescreen.NavigationView.Anon9.this.val$endAction != null) {
                    com.navdy.hud.app.ui.component.homescreen.NavigationView.Anon9.this.val$endAction.run();
                }
            }
        }

        Anon9(java.lang.Runnable runnable) {
            this.val$endAction = runnable;
        }

        public void onMapTransformStart() {
        }

        public void onMapTransformEnd(com.here.android.mpa.mapping.MapState mapState) {
            com.navdy.hud.app.ui.component.homescreen.NavigationView.this.logger.v("animateBackfromOverview: transform end");
            com.navdy.hud.app.ui.component.homescreen.NavigationView.this.mapController.removeTransformListener(this);
            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.ui.component.homescreen.NavigationView.Anon9.Anon1(), 17);
        }
    }

    public NavigationView(android.content.Context context) {
        this(context, null);
    }

    public NavigationView(android.content.Context context, android.util.AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public NavigationView(android.content.Context context, android.util.AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.logger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.homescreen.NavigationView.class);
        this.handler = new android.os.Handler();
        this.overviewMapRouteObjects = new java.util.ArrayList<>();
        this.mapMarkerList = new java.util.ArrayList();
        this.fluctuatorPosListener = new com.navdy.hud.app.ui.component.homescreen.NavigationView.Anon1();
        this.showMapIconIndicatorRunnable = new com.navdy.hud.app.ui.component.homescreen.NavigationView.Anon2();
        this.resetMapOverviewRunnable = new com.navdy.hud.app.ui.component.homescreen.NavigationView.Anon5();
        this.cleanupMapOverviewRunnable = new com.navdy.hud.app.ui.component.homescreen.NavigationView.Anon6();
        if (!isInEditMode()) {
            this.uiStateManager = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getUiStateManager();
            this.hereMapsManager = com.navdy.hud.app.maps.here.HereMapsManager.getInstance();
            this.hereLocationFixManager = this.hereMapsManager.getLocationFixManager();
        }
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
        butterknife.ButterKnife.inject((android.view.View) this);
        this.mapMask.setX(0.0f);
        this.bus = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getBus();
        this.driverProfileManager = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getDriverProfileManager();
        initViews();
    }

    public void init(com.navdy.hud.app.ui.component.homescreen.HomeScreenView homeScreenView2) {
        this.homeScreenView = homeScreenView2;
        this.bus.register(this);
    }

    @com.squareup.otto.Subscribe
    public void onGeoPositionChange(com.here.android.mpa.common.GeoPosition geoPosition) {
        if (this.startMarker != null) {
            switch (this.mapController.getState()) {
                case OVERVIEW:
                    this.startMarker.setCoordinate(geoPosition.getCoordinate());
                    return;
                case ROUTE_PICKER:
                    this.startMarker.setCoordinate(geoPosition.getCoordinate());
                    return;
                default:
                    return;
            }
        }
    }

    public com.navdy.hud.app.maps.here.HereMapController getMapController() {
        return this.mapController;
    }

    private void initViews() {
        if (!isInEditMode()) {
            if (this.hereLocationFixManager != null) {
                this.hereLocationFixManager.hasLocationFix();
            }
            if (0 == 0 || !this.initComplete) {
                this.logger.v("first layout: location fix[" + false + "] initComplete[" + this.initComplete + "]");
                this.noLocationView.showLocationUI();
                return;
            }
            this.logger.v("first layout: location fix available");
            setMapCenter();
        }
    }

    public void setView(com.navdy.hud.app.view.MainView.CustomAnimationMode mode) {
        this.logger.v("setview: " + mode);
        setViewMapIconIndicator(mode);
        setViewMap(mode);
        setViewMapMask(mode);
        this.noLocationView.setView(mode);
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.hereMapsManager.isInitializing()) {
            this.logger.v("engine initializing, wait");
        } else {
            handleEngineInit();
        }
    }

    @com.squareup.otto.Subscribe
    public void onEngineInitialized(com.navdy.hud.app.maps.MapEvents.MapEngineInitialize event) {
        handleEngineInit();
    }

    private synchronized void handleEngineInit() {
        if (!this.hereMapsManager.isRenderingAllowed()) {
            this.logger.v("onMapEngineInitializationCompleted: Maps Rendering not allowed, Quiet mode On!");
        } else if (this.hereMapsManager.isInitialized()) {
            this.logger.v("engine initialized");
            onMapEngineInitializationCompleted();
        } else {
            this.logger.e("onCreate() : Cannot initialize map engine: " + this.hereMapsManager.getError());
            this.noLocationView.hideLocationUI();
        }
    }

    public synchronized void enableRendering() {
        if (this.isRenderingEnabled) {
            this.logger.v("enableRendering:Maps Rendering already enabled");
        } else if (!this.initComplete) {
            this.logger.v("enableRendering:Maps Rendering init-engine not complete yet");
        } else {
            this.hereMapsManager.initNavigation();
            this.mapController.setState(com.navdy.hud.app.maps.here.HereMapController.State.AR_MODE);
            this.hereMapsManager.installPositionListener();
            setTransformCenter(com.navdy.hud.app.maps.here.HereMapController.State.AR_MODE);
            this.mapView.setMap(this.mapController.getMap());
            this.logger.v("enableRendering:Maps Rendering enabled");
            this.isRenderingEnabled = true;
        }
    }

    @com.squareup.otto.Subscribe
    public void onWakeup(com.navdy.hud.app.event.Wakeup event) {
        handleEngineInit();
    }

    private synchronized void onMapEngineInitializationCompleted() {
        if (!this.initComplete) {
            this.mapController = this.hereMapsManager.getMapController();
            this.initComplete = true;
            this.logger.v("onMapEngineInitializationCompleted: Maps Rendering allowed");
            enableRendering();
            this.mapView.setCopyrightLogoPosition(com.here.android.mpa.common.CopyrightLogoPosition.TOP_RIGHT);
            this.mapView.onResume();
            this.hereMapsManager.setMapView(this.mapView, this);
            try {
                com.here.android.mpa.common.Image image = new com.here.android.mpa.common.Image();
                image.setImageResource(com.navdy.hud.app.R.drawable.icon_driver_position);
                com.here.android.mpa.common.GeoCoordinate geoCoordinate = this.hereMapsManager.getLocationFixManager().getLastGeoCoordinate();
                if (geoCoordinate == null) {
                    geoCoordinate = new com.here.android.mpa.common.GeoCoordinate(37.802086d, -122.419015d);
                }
                this.startMarker = new com.here.android.mpa.mapping.MapMarker(geoCoordinate, image);
            } catch (Throwable t) {
                this.logger.e(t);
            }
            this.logger.v("view rendered");
            if (this.noLocationView.isAcquiringLocation() && this.initComplete && setMapCenter()) {
                this.noLocationView.hideLocationUI();
            }
            this.bus.post(new com.navdy.hud.app.maps.MapEvents.MapUIReady());
        }
        return;
    }

    @com.squareup.otto.Subscribe
    public void onLocationFixChangeEvent(com.navdy.hud.app.maps.MapEvents.LocationFix event) {
        this.logger.v("location fix event locationAvailable[" + event.locationAvailable + "] phone[" + event.usingPhoneLocation + "] gps [" + event.usingLocalGpsLocation + "]");
        if (isAcquiringLocation() && this.initComplete && setMapCenter()) {
            this.noLocationView.hideLocationUI();
        }
    }

    public void layoutMap() {
        setTransformCenter();
    }

    public void adjustMaplayoutHeight(int height) {
        int y;
        if (height == com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeMapHeight) {
            y = com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.iconIndicatorActiveY;
        } else {
            y = com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.iconIndicatorOpenY;
        }
        this.mapIconIndicator.setY(((float) y) - (((float) com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.transformCenterIconHeight) * 0.6f));
        ((android.view.ViewGroup.MarginLayoutParams) this.mapMask.getLayoutParams()).height = height;
        layoutMap();
        invalidate();
        requestLayout();
    }

    public void updateLayoutForMode(com.navdy.hud.app.maps.NavigationMode navigationMode) {
        adjustMaplayoutHeight(this.homeScreenView.isNavigationActive() ? com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeMapHeight : com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.openMapHeight);
    }

    private void setTransformCenter() {
        if (this.mapController == null) {
            this.logger.v("setTransformCenter:no map");
        } else {
            setTransformCenter(this.mapController.getState());
        }
    }

    /* access modifiers changed from: private */
    public void setTransformCenter(com.navdy.hud.app.maps.here.HereMapController.State state) {
        if (this.mapController == null) {
            this.logger.v("setTransformCenter:no map");
            return;
        }
        switch (state) {
            case OVERVIEW:
                if (this.homeScreenView.isNavigationActive()) {
                    this.logger.v("setTransformCenter:overview:nav");
                    this.mapController.setTransformCenter(com.navdy.hud.app.ui.component.homescreen.HomeScreenConstants.transformCenterSmallMiddle);
                    return;
                }
                this.logger.v("setTransformCenter:overview:open");
                this.mapController.setTransformCenter(com.navdy.hud.app.ui.component.homescreen.HomeScreenConstants.transformCenterLargeMiddle);
                return;
            case ROUTE_PICKER:
                this.logger.v("setTransformCenter:routeSearch");
                this.mapController.setTransformCenter(com.navdy.hud.app.ui.component.homescreen.HomeScreenConstants.transformCenterPicker);
                return;
            case AR_MODE:
                if (this.homeScreenView.isNavigationActive()) {
                    this.logger.v("setTransformCenter:ar:nav");
                    this.mapController.setTransformCenter(com.navdy.hud.app.ui.component.homescreen.HomeScreenConstants.transformCenterSmallBottom);
                    return;
                }
                this.logger.v("setTransformCenter:ar:open");
                this.mapController.setTransformCenter(com.navdy.hud.app.ui.component.homescreen.HomeScreenConstants.transformCenterLargeBottom);
                return;
            default:
                return;
        }
    }

    public void animateToOverview(com.here.android.mpa.common.GeoCoordinate geoCoordinate, com.here.android.mpa.routing.Route route, com.here.android.mpa.mapping.MapRoute currentMapRoute, java.lang.Runnable endAction, boolean isArrived) {
        switchToOverviewMode(geoCoordinate, route, currentMapRoute, endAction, isArrived, true, -1.0d);
    }

    private void switchToOverviewMode(com.here.android.mpa.common.GeoCoordinate geoCoordinate, com.here.android.mpa.routing.Route route, com.here.android.mpa.mapping.MapRoute currentMapRoute, java.lang.Runnable endAction, boolean isArrived, boolean animate, double zoomLevel) {
        this.logger.v("switchToOverviewMode:" + animate);
        this.mapController.setState(com.navdy.hud.app.maps.here.HereMapController.State.OVERVIEW);
        cleanupFluctuator();
        this.mapIconIndicator.setVisibility(4);
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.ui.component.homescreen.NavigationView.Anon3(route, endAction, geoCoordinate, animate, zoomLevel), 17);
    }

    public void animateBackfromOverview(java.lang.Runnable endAction) {
        this.logger.v("animateBackfromOverview");
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.ui.component.homescreen.NavigationView.Anon4(endAction), 17);
    }

    public void switchToRouteSearchMode(com.here.android.mpa.common.GeoCoordinate from, com.here.android.mpa.common.GeoCoordinate to, com.here.android.mpa.common.GeoBoundingBox boundingBox, com.here.android.mpa.routing.Route route, boolean startFluctuator, java.util.List<com.here.android.mpa.common.GeoCoordinate> markers, int destinationIcon) {
        this.logger.v("switchToRouteSearchMode: start[" + from + "] end [" + to + "] x=" + getMapViewX());
        if (com.navdy.hud.app.maps.here.HereMapsManager.getInstance().isInitialized()) {
            com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().getSafetySpotListener().setSafetySpotsEnabledOnMap(false);
        }
        setMapMaskVisibility(4);
        setMapViewX(-com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.transformCenterMoveX);
        this.homeScreenView.navigationViewsContainer.setVisibility(4);
        this.homeScreenView.openMapRoadInfoContainer.setVisibility(4);
        this.homeScreenView.mapViewSpeedContainer.setVisibility(4);
        this.mapController.setState(com.navdy.hud.app.maps.here.HereMapController.State.ROUTE_PICKER);
        this.mapIconIndicator.setVisibility(4);
        cleanupMapOverview();
        try {
            setTransformCenter(com.navdy.hud.app.maps.here.HereMapController.State.ROUTE_PICKER);
            com.here.android.mpa.common.GeoCoordinate center = from;
            if (center == null) {
                center = this.hereLocationFixManager.getLastGeoCoordinate();
            }
            this.mapController.setCenterForState(com.navdy.hud.app.maps.here.HereMapController.State.ROUTE_PICKER, center, com.here.android.mpa.mapping.Map.Animation.NONE, 15.5d, 0.0f, 0.0f);
        } catch (Throwable t) {
            this.logger.e(t);
        }
        com.navdy.hud.app.maps.here.HereNavigationManager hereNavigationManager = com.navdy.hud.app.maps.here.HereNavigationManager.getInstance();
        if (hereNavigationManager.removeCurrentRoute()) {
            this.logger.v("map-route- switchToRouteSearchMode: nav route removed");
        } else {
            this.logger.v("map-route- switchToRouteSearchMode: nav route not removed, does not exist");
        }
        com.here.android.mpa.mapping.MapMarker destMarker = hereNavigationManager.getDestinationMarker();
        if (destMarker != null) {
            this.mapController.removeMapObject(destMarker);
            this.logger.v("switchToRouteSearchMode: dest removed");
        }
        try {
            if (this.startMarker == null) {
                com.here.android.mpa.common.Image image = new com.here.android.mpa.common.Image();
                image.setImageResource(com.navdy.hud.app.R.drawable.icon_driver_position);
                this.startMarker = new com.here.android.mpa.mapping.MapMarker();
                this.startMarker.setIcon(image);
            }
            if (from != null) {
                this.startMarker.setCoordinate(from);
                this.mapController.addMapObject(this.startMarker);
            }
        } catch (Throwable t2) {
            this.logger.e(t2);
        }
        try {
            if (this.endMarker == null) {
                com.here.android.mpa.common.Image image2 = new com.here.android.mpa.common.Image();
                image2.setImageResource(com.navdy.hud.app.R.drawable.icon_pin_dot_destination);
                this.endMarker = new com.here.android.mpa.mapping.MapMarker();
                this.endMarker.setIcon(image2);
            }
            if (to != null) {
                this.endMarker.setCoordinate(to);
                this.mapController.addMapObject(this.endMarker);
            }
        } catch (Throwable t3) {
            this.logger.e(t3);
        }
        if (this.mapMarkerList.size() > 0) {
            this.mapController.removeMapObjects(this.mapMarkerList);
            this.mapMarkerList = new java.util.ArrayList();
        }
        if (markers != null) {
            int len = markers.size();
            try {
                this.selectedDestinationImage = new com.here.android.mpa.common.Image();
                com.here.android.mpa.common.Image image3 = this.selectedDestinationImage;
                if (destinationIcon == -1) {
                    destinationIcon = com.navdy.hud.app.R.drawable.icon_pin_dot_destination_blue;
                }
                image3.setImageResource(destinationIcon);
            } catch (Throwable t4) {
                this.logger.e(t4);
            }
            if (this.unselectedDestinationImage == null) {
                try {
                    this.unselectedDestinationImage = new com.here.android.mpa.common.Image();
                    this.unselectedDestinationImage.setImageResource(com.navdy.hud.app.R.drawable.icon_destination_pin_unselected);
                } catch (Throwable t5) {
                    this.logger.e(t5);
                }
            }
            for (int i = 0; i < len; i++) {
                com.here.android.mpa.common.GeoCoordinate g = (com.here.android.mpa.common.GeoCoordinate) markers.get(i);
                if (g != null) {
                    com.here.android.mpa.mapping.MapMarker mapMarker = new com.here.android.mpa.mapping.MapMarker(g, this.unselectedDestinationImage);
                    this.mapController.addMapObject(mapMarker);
                    this.mapMarkerList.add(mapMarker);
                } else {
                    this.mapMarkerList.add(null);
                }
            }
            this.logger.v("addded dest marker:" + len);
        }
        zoomToBoundBox(boundingBox, route, startFluctuator, true);
    }

    public void zoomToBoundBox(com.here.android.mpa.common.GeoBoundingBox boundingBox, com.here.android.mpa.routing.Route route, boolean startFluctuator, boolean cleanExistingRoutes) {
        com.navdy.hud.app.maps.here.HereMapController.State state = this.mapController.getState();
        if (state != com.navdy.hud.app.maps.here.HereMapController.State.ROUTE_PICKER) {
            this.logger.i("zoomToBoundBox: incorrect state:" + state);
            return;
        }
        try {
            cleanupFluctuator();
            if (startFluctuator) {
                startFluctuator();
            }
            if (boundingBox != null) {
                if (cleanExistingRoutes) {
                    cleanupMapOverviewRoutes();
                }
                if (route != null) {
                    com.here.android.mpa.mapping.MapRoute mapRoute = new com.here.android.mpa.mapping.MapRoute(route);
                    mapRoute.setTrafficEnabled(true);
                    addMapOverviewRoutes(mapRoute);
                }
                this.mapController.zoomTo(boundingBox, com.navdy.hud.app.ui.component.homescreen.HomeScreenConstants.routePickerViewRect, com.here.android.mpa.mapping.Map.Animation.BOW, -1.0f);
                this.logger.v("zoomed to bbox");
                return;
            }
            this.logger.v("zoomed to bbox: null");
        } catch (Throwable t) {
            this.logger.e(t);
        }
    }

    public void changeMarkerSelection(int selected, int unselected) {
        com.navdy.hud.app.maps.here.HereMapController.State state = this.mapController.getState();
        if (state != com.navdy.hud.app.maps.here.HereMapController.State.ROUTE_PICKER) {
            this.logger.i("changeMarkerSelection: incorrect state:" + state);
            return;
        }
        int len = this.mapMarkerList.size();
        if (len > 0) {
            this.logger.i("changeMarkerSelection {" + selected + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA + unselected + "}");
            if (unselected >= 0 && unselected < len) {
                com.here.android.mpa.mapping.MapMarker old = (com.here.android.mpa.mapping.MapMarker) this.mapMarkerList.get(unselected);
                if (old != null) {
                    com.here.android.mpa.mapping.MapMarker marker = new com.here.android.mpa.mapping.MapMarker(old.getCoordinate(), this.unselectedDestinationImage);
                    this.mapMarkerList.set(unselected, marker);
                    this.mapController.removeMapObject(old);
                    this.mapController.addMapObject(marker);
                }
            }
            if (selected >= 0 && selected < len) {
                com.here.android.mpa.mapping.MapMarker old2 = (com.here.android.mpa.mapping.MapMarker) this.mapMarkerList.get(selected);
                if (old2 != null) {
                    com.here.android.mpa.mapping.MapMarker marker2 = new com.here.android.mpa.mapping.MapMarker(old2.getCoordinate(), this.selectedDestinationImage);
                    this.mapMarkerList.set(selected, marker2);
                    this.mapController.removeMapObject(old2);
                    this.mapController.addMapObject(marker2);
                }
            }
        }
    }

    public void switchBackfromRouteSearchMode(boolean addCurrentRoute) {
        com.navdy.hud.app.maps.here.HereMapController.State state = this.mapController.getState();
        if (state != com.navdy.hud.app.maps.here.HereMapController.State.ROUTE_PICKER) {
            this.logger.v("switchBackfromRouteSearchMode: invalid state:" + state);
            return;
        }
        this.logger.v("map-route- switchBackfromRouteSearchMode:" + addCurrentRoute);
        if (com.navdy.hud.app.maps.here.HereMapsManager.getInstance().isInitialized()) {
            com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().getSafetySpotListener().setSafetySpotsEnabledOnMap(true);
        }
        int len = this.mapMarkerList.size();
        if (len > 0) {
            this.logger.v("switchBackfromRouteSearchMode remove map markers:" + len);
            this.mapController.removeMapObjects(this.mapMarkerList);
            this.mapMarkerList = new java.util.ArrayList();
        }
        setMapMaskVisibility(0);
        setMapViewX(0);
        this.homeScreenView.navigationViewsContainer.setVisibility(0);
        if (this.homeScreenView.getDisplayMode() == com.navdy.hud.app.ui.component.homescreen.HomeScreen.DisplayMode.MAP) {
            this.homeScreenView.mapViewSpeedContainer.setVisibility(0);
        }
        this.mapController.setState(com.navdy.hud.app.maps.here.HereMapController.State.TRANSITION);
        if (!com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getDriverProfileManager().isManualZoom()) {
            setMapToArMode(false, true, true, null, -1.0d, -1.0d, false);
            this.mapIconIndicator.setVisibility(0);
        }
        if (this.startMarker != null) {
            this.mapController.removeMapObject(this.startMarker);
        }
        if (this.endMarker != null) {
            this.mapController.removeMapObject(this.endMarker);
        }
        this.logger.v("map-route- switchBackfromRouteSearchMode cleanup routes");
        cleanupMapOverviewRoutes();
        com.navdy.hud.app.maps.here.HereNavigationManager hereNavigationManager = com.navdy.hud.app.maps.here.HereNavigationManager.getInstance();
        com.here.android.mpa.routing.Route navRoute = hereNavigationManager.getCurrentRoute();
        if (navRoute != null && hereNavigationManager.isNavigationModeOn() && !hereNavigationManager.hasArrived()) {
            if (addCurrentRoute) {
                com.here.android.mpa.mapping.MapRoute mapRoute = new com.here.android.mpa.mapping.MapRoute(navRoute);
                mapRoute.setTrafficEnabled(true);
                hereNavigationManager.addCurrentRoute(mapRoute);
                this.logger.v("map-route- switchBackfromRouteSearchMode: nav route added");
            } else {
                this.logger.v("map-route- switchBackfromRouteSearchMode: nav route not added back");
            }
        }
        com.here.android.mpa.mapping.MapMarker destinationMarker = com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().getDestinationMarker();
        if (destinationMarker != null) {
            this.logger.v("switchBackfromRouteSearchMode: destination marker added");
            this.mapController.addMapObject(destinationMarker);
        }
        cleanupFluctuator();
        com.navdy.hud.app.maps.here.HereMapCameraManager.getInstance().setZoom();
    }

    public void showStartMarker() {
        this.logger.v("showStartMarker");
        if (this.startMarker != null) {
            this.mapController.addMapObject(this.startMarker);
        }
    }

    public void resetMapOverview() {
        if (android.os.Looper.getMainLooper() == android.os.Looper.myLooper()) {
            this.resetMapOverviewRunnable.run();
        } else {
            this.handler.post(this.resetMapOverviewRunnable);
        }
    }

    public void cleanupMapOverview() {
        if (android.os.Looper.getMainLooper() == android.os.Looper.myLooper()) {
            this.cleanupMapOverviewRunnable.run();
        } else {
            this.handler.post(this.cleanupMapOverviewRunnable);
        }
    }

    public void addMapOverviewRoutes(com.here.android.mpa.mapping.MapRoute mapRoute) {
        this.logger.v("addMapOverviewRoutes:" + mapRoute);
        this.mapController.addMapObject(mapRoute);
        this.overviewMapRouteObjects.add(mapRoute);
    }

    public void cleanupMapOverviewRoutes() {
        this.logger.v("cleanupMapOverviewRoutes");
        if (this.overviewMapRouteObjects.size() > 0) {
            java.util.List<com.here.android.mpa.mapping.MapObject> copy = new java.util.ArrayList<>(this.overviewMapRouteObjects);
            for (com.here.android.mpa.mapping.MapObject mapObject : copy) {
                this.logger.v("cleanupMapOverviewRoutes:" + mapObject);
            }
            this.mapController.removeMapObjects(copy);
            this.overviewMapRouteObjects.clear();
        }
    }

    private boolean setMapCenter() {
        if (this.setInitialCenter) {
            return true;
        }
        if (!this.initComplete) {
            this.logger.w("setMapCenter:init not complete yet, deferring");
            return false;
        }
        if (this.hereLocationFixManager == null) {
            this.hereLocationFixManager = this.hereMapsManager.getLocationFixManager();
        }
        com.here.android.mpa.common.GeoCoordinate center = this.hereLocationFixManager.getLastGeoCoordinate();
        if (center == null) {
            return false;
        }
        this.setInitialCenter = true;
        this.mapController.setCenter(center, com.here.android.mpa.mapping.Map.Animation.NONE, -1.0d, -1.0f, -1.0f);
        this.logger.w("setMapCenter:setcenterInit done " + center);
        if (com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getFeatureUtil().isFeatureEnabled(com.navdy.hud.app.util.FeatureUtil.Feature.FUEL_ROUTING)) {
            com.navdy.hud.app.framework.fuel.FuelRoutingManager.getInstance().markAvailable();
        }
        updateMapIndicator();
        if (com.navdy.hud.app.framework.network.NetworkStateManager.getInstance().isNetworkStateInitialized()) {
            this.logger.v("checkPrevRoute:n/w is initialized");
            startPreviousRoute(center);
            this.logger.v("send map engine ready event-1");
            this.bus.post(new com.navdy.hud.app.event.RemoteEvent(new com.navdy.service.library.events.navigation.NavigationSessionStatusEvent(com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_ENGINE_READY, null, null, null, null)));
            this.bus.post(new com.navdy.hud.app.maps.MapEvents.MapEngineReady());
            return true;
        }
        this.networkCheckRequired = true;
        this.logger.v("checkPrevRoute:n/w is initialized, defer and wait");
        return true;
    }

    private void updateMapIndicator() {
        boolean showRawLocation = com.navdy.hud.app.device.gps.GpsUtils.isDebugRawGpsPosEnabled();
        this.mapIconIndicator.setAlpha(showRawLocation ? 0.4f : 1.0f);
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.ui.component.homescreen.NavigationView.Anon7(showRawLocation), 2);
        if (showRawLocation) {
            this.hereLocationFixManager.addMarkers(this.mapController);
        } else {
            this.hereLocationFixManager.removeMarkers(this.mapController);
        }
    }

    @com.squareup.otto.Subscribe
    public void onSettingsChange(com.navdy.hud.app.config.SettingsManager.SettingsChanged event) {
        if (event.setting == com.navdy.hud.app.device.gps.GpsUtils.SHOW_RAW_GPS) {
            updateMapIndicator();
        }
    }

    @com.squareup.otto.Subscribe
    public void onHudNetworkInit(com.navdy.hud.app.framework.network.NetworkStateManager.HudNetworkInitialized event) {
        this.logger.v("checkPrevRoute:hud n/w is initialized: " + this.networkCheckRequired);
        if (this.networkCheckRequired) {
            this.networkCheckRequired = false;
            if (this.hereLocationFixManager == null) {
                this.hereLocationFixManager = this.hereMapsManager.getLocationFixManager();
            }
            startPreviousRoute(this.hereLocationFixManager.getLastGeoCoordinate());
            this.logger.v("send map engine ready event-2");
            this.bus.post(new com.navdy.hud.app.event.RemoteEvent(new com.navdy.service.library.events.navigation.NavigationSessionStatusEvent(com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_ENGINE_READY, null, null, null, null)));
        }
    }

    public void switchScreen() {
        com.navdy.hud.app.screen.BaseScreen screen = this.uiStateManager.getCurrentScreen();
        if (screen != null) {
            com.navdy.hud.app.ui.component.homescreen.HomeScreenView view = this.uiStateManager.getHomescreenView();
            if (view != null) {
                com.navdy.hud.app.ui.activity.Main.saveHomeScreenPreference(view.globalPreferences, com.navdy.hud.app.ui.component.homescreen.HomeScreen.DisplayMode.MAP.ordinal());
                view.setDisplayMode(com.navdy.hud.app.ui.component.homescreen.HomeScreen.DisplayMode.MAP);
            }
            if (screen.getScreen() != com.navdy.service.library.events.ui.Screen.SCREEN_HOME) {
                this.logger.v("current screen is not map:" + screen.getScreen());
                com.navdy.hud.app.ui.activity.Main main = this.uiStateManager.getRootScreen();
                if (main.isNotificationViewShowing() || main.isNotificationExpanding()) {
                    this.logger.v("switching, but collpasing notif first");
                    this.homeScreenView.setShowCollapsedNotification(true);
                } else {
                    this.logger.v("switched to map");
                }
                this.bus.post(new com.navdy.service.library.events.ui.ShowScreen.Builder().screen(com.navdy.service.library.events.ui.Screen.SCREEN_HYBRID_MAP).build());
                return;
            }
            this.logger.v("current screen is map no switch reqd");
            return;
        }
        this.logger.w("current screen is null");
    }

    public void showOverviewMap(com.here.android.mpa.routing.Route route, com.here.android.mpa.mapping.MapRoute currentMapRoute, com.here.android.mpa.common.GeoCoordinate geoCoordinate, boolean isArrived) {
        this.logger.v("showOverviewMap");
        switchToOverviewMode(geoCoordinate, route, currentMapRoute, null, isArrived, false, 12.0d);
        this.mapIconIndicator.setVisibility(4);
    }

    public void showRouteMap(com.here.android.mpa.common.GeoPosition geoPosition, double zoom, float tilt) {
        this.logger.v("showRouteMap");
        setMapToArMode(false, true, false, geoPosition, zoom, (double) tilt, true);
        this.mapIconIndicator.setVisibility(0);
    }

    public void setMapToArMode(boolean animate, boolean changeState, boolean useLastZoom, com.here.android.mpa.common.GeoPosition geoPosition, double zoom, double tilt, boolean cleanupMapOverview) {
        if (com.navdy.hud.app.util.GenericUtil.isMainThread()) {
            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.ui.component.homescreen.NavigationView.Anon8(animate, changeState, useLastZoom, geoPosition, zoom, tilt, cleanupMapOverview), 17);
            return;
        }
        setMapToArModeInternal(animate, changeState, useLastZoom, geoPosition, zoom, (float) tilt, cleanupMapOverview);
    }

    /* access modifiers changed from: private */
    public void setMapToArModeInternal(boolean animate, boolean changeState, boolean useLastZoom, com.here.android.mpa.common.GeoPosition geoPosition, double zoomLevel, float tiltLevel, boolean cleanupMapOverview) {
        com.here.android.mpa.common.GeoCoordinate center;
        float heading;
        float tilt;
        float tilt2;
        double zoom;
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        this.logger.v("setMapToArModeInternal animate:" + animate + " changeState:" + changeState);
        com.navdy.hud.app.maps.here.HereMapCameraManager hereMapCameraManager = com.navdy.hud.app.maps.here.HereMapCameraManager.getInstance();
        if (geoPosition == null) {
            geoPosition = hereMapCameraManager.getLastGeoPosition();
        }
        if (cleanupMapOverview) {
            cleanupMapOverview();
        }
        if (geoPosition != null) {
            center = geoPosition.getCoordinate();
            heading = (float) geoPosition.getHeading();
            if (tiltLevel == -1.0f) {
                tilt2 = hereMapCameraManager.getLastTilt();
            } else {
                tilt2 = tiltLevel;
            }
            if (useLastZoom) {
                zoom = hereMapCameraManager.getLastZoom();
            } else if (zoomLevel == -1.0d) {
                zoom = 12.0d;
            } else {
                zoom = zoomLevel;
            }
        } else {
            center = this.mapController.getCenter();
            heading = -1.0f;
            if (tiltLevel == -1.0f) {
                tilt = 60.0f;
            } else {
                tilt = tiltLevel;
            }
            if (zoomLevel == -1.0d) {
                zoom = 16.5d;
            } else {
                zoom = zoomLevel;
            }
        }
        setTransformCenter(com.navdy.hud.app.maps.here.HereMapController.State.AR_MODE);
        this.mapController.setCenterForState(this.mapController.getState(), center, animate ? com.here.android.mpa.mapping.Map.Animation.BOW : com.here.android.mpa.mapping.Map.Animation.NONE, zoom, heading, tilt2);
        if (changeState) {
            this.mapController.setState(com.navdy.hud.app.maps.here.HereMapController.State.AR_MODE);
        } else {
            this.mapController.setState(com.navdy.hud.app.maps.here.HereMapController.State.TRANSITION);
        }
    }

    public android.widget.ImageView getMapIconIndicatorView() {
        return this.mapIconIndicator;
    }

    private void setViewMapIconIndicator(com.navdy.hud.app.view.MainView.CustomAnimationMode mode) {
        switch (mode) {
            case EXPAND:
                this.mapIconIndicator.setX((float) com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.iconIndicatorX);
                return;
            case SHRINK_LEFT:
                this.mapIconIndicator.setX((float) com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.iconIndicatorShrinkLeft_X);
                return;
            default:
                return;
        }
    }

    private void setViewMap(com.navdy.hud.app.view.MainView.CustomAnimationMode mode) {
        switch (mode) {
            case EXPAND:
                this.mapView.setX((float) com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.mapViewX);
                return;
            case SHRINK_LEFT:
                this.mapView.setX((float) com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.mapViewShrinkLeftX);
                return;
            default:
                return;
        }
    }

    private void setViewMapMask(com.navdy.hud.app.view.MainView.CustomAnimationMode mode) {
        android.view.ViewGroup.MarginLayoutParams lytParams = (android.view.ViewGroup.MarginLayoutParams) this.mapMask.getLayoutParams();
        switch (mode) {
            case EXPAND:
                this.mapMask.setX((float) com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.mapMaskX);
                lytParams.width = com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.fullWidth;
                return;
            case SHRINK_LEFT:
                this.mapMask.setX((float) com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.mapMaskShrinkLeftX);
                lytParams.width = com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.mapMaskShrinkWidth;
                return;
            default:
                return;
        }
    }

    public void clearState() {
    }

    public void getCustomAnimator(com.navdy.hud.app.view.MainView.CustomAnimationMode mode, android.animation.AnimatorSet.Builder mainBuilder) {
        int iconIndicatorX;
        int mapViewX;
        int mapMaskX;
        int mapMaskWidth;
        this.logger.v("getCustomAnimator: " + mode);
        switch (mode) {
            case EXPAND:
                mainBuilder.with(com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getXPositionAnimator(this.mapIconIndicator, (float) com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.iconIndicatorX));
                mainBuilder.with(com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getXPositionAnimator(this.mapView, (float) com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.mapViewX));
                android.animation.ObjectAnimator mapMaskXAnimator = com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getXPositionAnimator(this.mapMask, (float) com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.mapMaskX);
                android.animation.ValueAnimator mapMaskWidthAnimator = com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getWidthAnimator(this.mapMask, this.mapMask.getMeasuredWidth() + 1, com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.fullWidth + 1);
                mainBuilder.with(mapMaskXAnimator);
                mainBuilder.with(mapMaskWidthAnimator);
                if (isAcquiringLocation()) {
                    this.noLocationView.getCustomAnimator(com.navdy.hud.app.view.MainView.CustomAnimationMode.EXPAND, mainBuilder);
                    return;
                } else {
                    this.noLocationView.setView(com.navdy.hud.app.view.MainView.CustomAnimationMode.EXPAND);
                    return;
                }
            case SHRINK_LEFT:
            case SHRINK_MODE:
                if (mode == com.navdy.hud.app.view.MainView.CustomAnimationMode.SHRINK_LEFT) {
                    iconIndicatorX = com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.iconIndicatorShrinkLeft_X;
                    mapViewX = com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.mapViewShrinkLeftX;
                    mapMaskX = com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.mapMaskShrinkLeftX;
                    mapMaskWidth = com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.mapMaskShrinkWidth;
                } else {
                    iconIndicatorX = com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.iconIndicatorShrinkModeX;
                    mapViewX = com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.mapViewShrinkModeX;
                    mapMaskX = 0;
                    mapMaskWidth = com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.mapMaskShrinkWidth;
                }
                mainBuilder.with(com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getXPositionAnimator(this.mapIconIndicator, (float) iconIndicatorX));
                mainBuilder.with(com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getXPositionAnimator(this.mapView, (float) mapViewX));
                android.animation.ObjectAnimator mapMaskXAnimator2 = com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getXPositionAnimator(this.mapMask, (float) mapMaskX);
                android.animation.ValueAnimator mapMaskWidthAnimator2 = com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getWidthAnimator(this.mapMask, mapMaskWidth);
                mainBuilder.with(mapMaskXAnimator2);
                mainBuilder.with(mapMaskWidthAnimator2);
                if (mode != com.navdy.hud.app.view.MainView.CustomAnimationMode.SHRINK_LEFT) {
                    return;
                }
                if (isAcquiringLocation()) {
                    this.noLocationView.getCustomAnimator(com.navdy.hud.app.view.MainView.CustomAnimationMode.SHRINK_LEFT, mainBuilder);
                    return;
                } else {
                    this.noLocationView.setView(com.navdy.hud.app.view.MainView.CustomAnimationMode.SHRINK_LEFT);
                    return;
                }
            default:
                return;
        }
    }

    public void getTopAnimator(android.animation.AnimatorSet.Builder builder, boolean out) {
    }

    public void resetTopViewsAnimator() {
    }

    public void onClick() {
    }

    public void onTrackHand(float normalized) {
    }

    public boolean onGesture(com.navdy.service.library.events.input.GestureEvent event) {
        return false;
    }

    public boolean onKey(com.navdy.hud.app.manager.InputManager.CustomKeyEvent event) {
        switch (event) {
            case RIGHT:
                this.bus.post(ZOOM_IN);
                return true;
            case LEFT:
                this.bus.post(ZOOM_OUT);
                return true;
            default:
                return false;
        }
    }

    /* access modifiers changed from: private */
    public com.here.android.mpa.mapping.Map.OnTransformListener animateBackfromOverviewMap(java.lang.Runnable endAction) {
        return new com.navdy.hud.app.ui.component.homescreen.NavigationView.Anon9(endAction);
    }

    public com.navdy.hud.app.manager.InputManager.IInputHandler nextHandler() {
        return null;
    }

    public boolean isOverviewMapMode() {
        if (this.mapController == null) {
            return false;
        }
        switch (this.mapController.getState()) {
            case OVERVIEW:
            case ROUTE_PICKER:
                return true;
            default:
                return false;
        }
    }

    public boolean isAcquiringLocation() {
        return this.noLocationView.isAcquiringLocation();
    }

    public void cleanupFluctuator() {
        this.logger.v("cleanupFluctuator");
        this.mapController.removeTransformListener(this.fluctuatorPosListener);
        this.startDestinationFluctuatorView.stop();
        this.startDestinationFluctuatorView.setVisibility(8);
    }

    public void startFluctuator() {
        this.mapController.addTransformListener(getFluctuatorTransformListener());
    }

    /* access modifiers changed from: private */
    public void positionFluctuator() {
        try {
            if (this.startMarker != null) {
                com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.ui.component.homescreen.NavigationView.Anon10(), 3);
            }
        } catch (Throwable t) {
            this.logger.e(t);
        }
    }

    public com.here.android.mpa.mapping.Map.OnTransformListener getFluctuatorTransformListener() {
        return this.fluctuatorPosListener;
    }

    private void startPreviousRoute(com.here.android.mpa.common.GeoCoordinate center) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.ui.component.homescreen.NavigationView.Anon11(center), 3);
    }

    public void onPause() {
        if (!this.paused) {
            this.logger.v("::onPause");
            this.paused = true;
            this.hereMapsManager.stopMapRendering();
        }
    }

    public void onResume() {
        if (this.paused) {
            this.logger.v("::onResume");
            this.paused = false;
            this.hereMapsManager.startMapRendering();
        }
    }

    public void setMapMaskVisibility(int visibility) {
        this.mapMask.setVisibility(visibility);
    }

    public void setMapViewX(int x) {
        this.mapView.setX((float) x);
    }

    public int getMapViewX() {
        return (int) this.mapView.getX();
    }
}
