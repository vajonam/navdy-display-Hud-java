package com.navdy.hud.app.ui.component.vlist.viewholder;

public class IconOptionsViewHolder extends com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder {
    private static final int iconMargin = com.navdy.hud.app.HudApplication.getAppContext().getResources().getDimensionPixelOffset(com.navdy.hud.app.R.dimen.vlist_icon_list_margin);
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder.class);
    protected android.animation.AnimatorSet.Builder animatorSetBuilder;
    /* access modifiers changed from: private */
    public int currentSelection = -1;
    /* access modifiers changed from: private */
    public java.lang.Runnable fluctuatorRunnable = new com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder.Anon2();
    private com.navdy.hud.app.ui.framework.DefaultAnimationListener fluctuatorStartListener = new com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder.Anon1();
    private boolean itemNotSelected;
    private com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.State lastState;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.ui.component.vlist.VerticalList.Model model;
    private java.util.ArrayList<com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder.ViewContainer> viewContainers = new java.util.ArrayList<>();

    class Anon1 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
        Anon1() {
        }

        public void onAnimationEnd(android.animation.Animator animation) {
            com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder.this.itemAnimatorSet = null;
            com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder.this.handler.removeCallbacks(com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder.this.fluctuatorRunnable);
            com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder.this.handler.postDelayed(com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder.this.fluctuatorRunnable, 100);
        }
    }

    class Anon2 implements java.lang.Runnable {
        Anon2() {
        }

        public void run() {
            com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder.this.startFluctuator();
        }
    }

    class Anon3 implements java.lang.Runnable {
        final /* synthetic */ int val$pos;

        Anon3(int i) {
            this.val$pos = i;
        }

        public void run() {
            com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState selectionState = com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder.this.vlist.getItemSelectionState();
            selectionState.set(com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder.this.model, com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder.this.model.id, this.val$pos, com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder.this.model.iconIds[com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder.this.currentSelection], com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder.this.currentSelection);
            com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder.this.vlist.performSelectAction(selectionState);
        }
    }

    private static class ViewContainer {
        com.navdy.hud.app.ui.component.image.IconColorImageView big;
        com.navdy.hud.app.ui.component.image.CrossFadeImageView crossFadeImageView;
        com.navdy.hud.app.ui.component.HaloView haloView;
        android.view.ViewGroup iconContainer;
        com.navdy.hud.app.ui.component.image.IconColorImageView small;

        private ViewContainer() {
        }

        /* synthetic */ ViewContainer(com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder.Anon1 x0) {
            this();
        }
    }

    public static com.navdy.hud.app.ui.component.vlist.VerticalList.Model buildModel(int id, int[] icons, int[] ids, int[] iconSelectedColors, int[] iconDeselectedColors, int[] iconFluctuatorColors, int currentSelection2, boolean supportsToolTip) {
        com.navdy.hud.app.ui.component.vlist.VerticalList.Model model2 = new com.navdy.hud.app.ui.component.vlist.VerticalList.Model();
        model2.type = com.navdy.hud.app.ui.component.vlist.VerticalList.ModelType.ICON_OPTIONS;
        model2.id = id;
        model2.iconList = icons;
        model2.iconIds = ids;
        model2.iconSelectedColors = iconSelectedColors;
        model2.iconDeselectedColors = iconDeselectedColors;
        model2.iconFluctuatorColors = iconFluctuatorColors;
        model2.currentIconSelection = currentSelection2;
        model2.supportsToolTip = supportsToolTip;
        return model2;
    }

    public static com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder buildViewHolder(android.view.ViewGroup parent, com.navdy.hud.app.ui.component.vlist.VerticalList vlist, android.os.Handler handler) {
        return new com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder((android.view.ViewGroup) android.view.LayoutInflater.from(parent.getContext()).inflate(com.navdy.hud.app.R.layout.vlist_icon_options, parent, false), vlist, handler);
    }

    public IconOptionsViewHolder(android.view.ViewGroup layout, com.navdy.hud.app.ui.component.vlist.VerticalList vlist, android.os.Handler handler) {
        super(layout, vlist, handler);
        layout.setPivotX((float) (com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.listItemHeight / 2));
        layout.setPivotY((float) (com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.listItemHeight / 2));
    }

    public com.navdy.hud.app.ui.component.vlist.VerticalList.ModelType getModelType() {
        return com.navdy.hud.app.ui.component.vlist.VerticalList.ModelType.ICON_OPTIONS;
    }

    public void setItemState(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.State state, com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.AnimationType animation, int duration, boolean startFluctuator) {
        if (this.lastState != state) {
            boolean initialMove = false;
            if (this.itemNotSelected) {
                initialMove = true;
                this.itemNotSelected = false;
                animation = com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.AnimationType.MOVE;
            }
            this.animatorSetBuilder = null;
            float iconScaleFactor = 0.0f;
            com.navdy.hud.app.ui.component.image.CrossFadeImageView.Mode mode = null;
            switch (state) {
                case SELECTED:
                    iconScaleFactor = 1.0f;
                    mode = com.navdy.hud.app.ui.component.image.CrossFadeImageView.Mode.BIG;
                    break;
                case UNSELECTED:
                    iconScaleFactor = 0.6f;
                    mode = com.navdy.hud.app.ui.component.image.CrossFadeImageView.Mode.SMALL;
                    break;
            }
            switch (animation) {
                case NONE:
                case INIT:
                    for (int i = 0; i < this.model.iconList.length; i++) {
                        com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder.ViewContainer viewContainer = (com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder.ViewContainer) this.viewContainers.get(i);
                        viewContainer.iconContainer.setScaleX(iconScaleFactor);
                        viewContainer.iconContainer.setScaleY(iconScaleFactor);
                        viewContainer.crossFadeImageView.setMode(mode);
                        if (i == this.currentSelection) {
                            viewContainer.big.setIcon(this.model.iconList[i], this.model.iconSelectedColors[i], null, 0.83f);
                        } else {
                            viewContainer.big.setIcon(this.model.iconList[i], this.model.iconDeselectedColors[i], null, 0.83f);
                        }
                    }
                    break;
                case MOVE:
                    if (!initialMove) {
                        this.itemAnimatorSet = new android.animation.AnimatorSet();
                        boolean currentSelected = false;
                        if (this.currentSelection != -1) {
                            com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder.ViewContainer viewContainer2 = (com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder.ViewContainer) this.viewContainers.get(this.currentSelection);
                            if (viewContainer2.crossFadeImageView.getMode() != mode) {
                                currentSelected = true;
                                viewContainer2.crossFadeImageView.setMode(mode);
                            }
                        }
                        for (int i2 = 0; i2 < this.model.iconList.length; i2++) {
                            com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder.ViewContainer viewContainer3 = (com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder.ViewContainer) this.viewContainers.get(i2);
                            if ((i2 != this.currentSelection || !currentSelected) && viewContainer3.crossFadeImageView.getMode() != mode) {
                                if (this.animatorSetBuilder == null) {
                                    this.animatorSetBuilder = this.itemAnimatorSet.play(viewContainer3.crossFadeImageView.getCrossFadeAnimator());
                                } else {
                                    this.animatorSetBuilder.with(viewContainer3.crossFadeImageView.getCrossFadeAnimator());
                                }
                            }
                        }
                        android.animation.PropertyValuesHolder p1 = android.animation.PropertyValuesHolder.ofFloat(android.view.View.SCALE_X, new float[]{iconScaleFactor});
                        android.animation.PropertyValuesHolder p2 = android.animation.PropertyValuesHolder.ofFloat(android.view.View.SCALE_Y, new float[]{iconScaleFactor});
                        if (this.animatorSetBuilder != null) {
                            this.animatorSetBuilder.with(android.animation.ObjectAnimator.ofPropertyValuesHolder(this.layout, new android.animation.PropertyValuesHolder[]{p1, p2}));
                            break;
                        } else {
                            this.animatorSetBuilder = this.itemAnimatorSet.play(android.animation.ObjectAnimator.ofPropertyValuesHolder(this.layout, new android.animation.PropertyValuesHolder[]{p1, p2}));
                            break;
                        }
                    } else {
                        for (int i3 = 0; i3 < this.model.iconList.length; i3++) {
                            com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder.ViewContainer viewContainer4 = (com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder.ViewContainer) this.viewContainers.get(i3);
                            viewContainer4.big.setIcon(this.model.iconList[i3], this.model.iconDeselectedColors[i3], null, 0.83f);
                            if (viewContainer4.crossFadeImageView.getMode() != mode) {
                                viewContainer4.crossFadeImageView.setMode(mode);
                            }
                        }
                        this.layout.setScaleX(iconScaleFactor);
                        this.layout.setScaleY(iconScaleFactor);
                        break;
                    }
            }
            if (state == com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.State.SELECTED) {
                if (this.itemAnimatorSet != null) {
                    this.itemAnimatorSet.addListener(this.fluctuatorStartListener);
                } else {
                    startFluctuator();
                }
            }
            this.lastState = state;
        }
    }

    public void preBind(com.navdy.hud.app.ui.component.vlist.VerticalList.Model model2, com.navdy.hud.app.ui.component.vlist.VerticalList.ModelState modelState) {
        this.model = model2;
        android.content.Context context = this.layout.getContext();
        if (this.layout.getChildCount() != (model2.iconList.length + model2.iconList.length) - 1) {
            this.currentSelection = model2.currentIconSelection;
            sLogger.v("preBind: createIcons:" + model2.iconList.length);
            this.layout.removeAllViews();
            this.viewContainers.clear();
            android.view.LayoutInflater inflater = android.view.LayoutInflater.from(this.layout.getContext());
            for (int i = 0; i < model2.iconList.length; i++) {
                if (i != 0) {
                    android.view.View view = new android.view.View(context);
                    view.setLayoutParams(new android.view.ViewGroup.MarginLayoutParams(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.iconMargin, com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.mainIconSize));
                    this.layout.addView(view);
                }
                com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder.ViewContainer viewContainer = new com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder.ViewContainer(null);
                android.view.ViewGroup viewgroup = (android.view.ViewGroup) inflater.inflate(com.navdy.hud.app.R.layout.vlist_halo_image, this.layout, false);
                viewContainer.iconContainer = (android.view.ViewGroup) viewgroup.findViewById(com.navdy.hud.app.R.id.iconContainer);
                viewContainer.crossFadeImageView = (com.navdy.hud.app.ui.component.image.CrossFadeImageView) viewgroup.findViewById(com.navdy.hud.app.R.id.crossFadeImageView);
                viewContainer.crossFadeImageView.inject(com.navdy.hud.app.ui.component.image.CrossFadeImageView.Mode.SMALL);
                viewContainer.big = (com.navdy.hud.app.ui.component.image.IconColorImageView) viewgroup.findViewById(com.navdy.hud.app.R.id.big);
                viewContainer.small = (com.navdy.hud.app.ui.component.image.IconColorImageView) viewgroup.findViewById(com.navdy.hud.app.R.id.small);
                viewContainer.haloView = (com.navdy.hud.app.ui.component.HaloView) viewgroup.findViewById(com.navdy.hud.app.R.id.halo);
                this.viewContainers.add(viewContainer);
                viewgroup.setLayoutParams(new android.view.ViewGroup.MarginLayoutParams(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.mainIconSize, com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.mainIconSize));
                this.layout.addView(viewgroup);
            }
            return;
        }
        sLogger.v("preBind: createIcons: already created");
    }

    public void bind(com.navdy.hud.app.ui.component.vlist.VerticalList.Model model2, com.navdy.hud.app.ui.component.vlist.VerticalList.ModelState modelState) {
        if (this.vlist.adapter.getModel(this.vlist.getRawPosition()) != model2) {
            this.itemNotSelected = true;
        }
        for (int i = 0; i < model2.iconList.length; i++) {
            com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder.ViewContainer viewContainer = (com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder.ViewContainer) this.viewContainers.get(i);
            viewContainer.big.setIcon(model2.iconList[i], model2.iconSelectedColors[i], null, 0.83f);
            viewContainer.small.setIcon(model2.iconList[i], model2.iconDeselectedColors[i], null, 0.83f);
            int fluctuatorColor = android.graphics.Color.argb(com.navdy.hud.app.ui.component.vlist.viewholder.IconBaseViewHolder.FLUCTUATOR_OPACITY_ALPHA, android.graphics.Color.red(model2.iconFluctuatorColors[i]), android.graphics.Color.green(model2.iconFluctuatorColors[i]), android.graphics.Color.blue(model2.iconFluctuatorColors[i]));
            viewContainer.haloView.setVisibility(4);
            viewContainer.haloView.setStrokeColor(fluctuatorColor);
        }
    }

    public void clearAnimation() {
        stopFluctuator(this.currentSelection, true);
        stopAnimation();
        this.currentState = null;
        this.layout.setVisibility(0);
        this.layout.setAlpha(1.0f);
    }

    public void select(com.navdy.hud.app.ui.component.vlist.VerticalList.Model model2, int pos, int duration) {
        if (isItemInBounds(this.currentSelection)) {
            stopFluctuator(this.currentSelection, false);
            com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils.performClick(((com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder.ViewContainer) this.viewContainers.get(this.currentSelection)).iconContainer, duration, new com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder.Anon3(pos));
        }
    }

    public void copyAndPosition(android.widget.ImageView imageC, android.widget.TextView titleC, android.widget.TextView subTitleC, android.widget.TextView subTitle2C, boolean setImage) {
        if (this.currentSelection >= 0) {
            android.view.View view = ((com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder.ViewContainer) this.viewContainers.get(this.currentSelection)).crossFadeImageView.getBig();
            if (setImage) {
                com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils.copyImage((android.widget.ImageView) view, imageC);
            }
            imageC.setX((float) (selectedImageX + getOffSetFromPos(this.currentSelection)));
            imageC.setY((float) selectedImageY);
        }
    }

    public void startFluctuator() {
        startFluctuator(this.currentSelection, true);
    }

    private void startFluctuator(int item, boolean changeColor) {
        if (isItemInBounds(item)) {
            com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder.ViewContainer viewContainer = (com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder.ViewContainer) this.viewContainers.get(item);
            if (changeColor) {
                viewContainer.big.setIcon(this.model.iconList[item], this.model.iconSelectedColors[item], null, 0.83f);
            }
            viewContainer.haloView.setVisibility(0);
            viewContainer.haloView.start();
        }
    }

    private void stopFluctuator(int item, boolean changeColor) {
        if (isItemInBounds(item)) {
            this.handler.removeCallbacks(this.fluctuatorRunnable);
            com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder.ViewContainer viewContainer = (com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder.ViewContainer) this.viewContainers.get(item);
            if (changeColor) {
                viewContainer.big.setIcon(this.model.iconList[item], this.model.iconDeselectedColors[item], null, 0.83f);
            }
            viewContainer.haloView.setVisibility(8);
            viewContainer.haloView.stop();
        }
    }

    private boolean isItemInBounds(int item) {
        if (this.model == null || item < 0 || item >= this.model.iconList.length) {
            return false;
        }
        return true;
    }

    public boolean handleKey(com.navdy.hud.app.manager.InputManager.CustomKeyEvent event) {
        if (!isItemInBounds(this.currentSelection)) {
            return false;
        }
        switch (event) {
            case LEFT:
                if (this.currentSelection == 0) {
                    return false;
                }
                this.currentSelection--;
                stopFluctuator(this.currentSelection + 1, true);
                startFluctuator(this.currentSelection, true);
                com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState itemSelectionState = this.vlist.getItemSelectionState();
                itemSelectionState.set(this.model, this.model.id, this.vlist.getCurrentPosition(), this.model.iconIds[this.currentSelection], this.currentSelection);
                this.vlist.callItemSelected(itemSelectionState);
                return true;
            case RIGHT:
                if (this.currentSelection == this.model.iconList.length - 1) {
                    return false;
                }
                this.currentSelection++;
                stopFluctuator(this.currentSelection - 1, true);
                startFluctuator(this.currentSelection, true);
                com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState itemSelectionState2 = this.vlist.getItemSelectionState();
                itemSelectionState2.set(this.model, this.model.id, this.vlist.getCurrentPosition(), this.model.iconIds[this.currentSelection], this.currentSelection);
                this.vlist.callItemSelected(itemSelectionState2);
                return true;
            default:
                return false;
        }
    }

    public int getCurrentSelection() {
        return this.currentSelection;
    }

    public int getCurrentSelectionId() {
        if (this.currentSelection < 0 || this.currentSelection >= this.model.iconIds.length) {
            return -1;
        }
        return this.model.iconIds[this.currentSelection];
    }

    public void selectionDown() {
        sLogger.v("selectionDown");
        if (!isItemInBounds(this.currentSelection)) {
            sLogger.v("selectionDown:invalid pos:" + this.currentSelection);
            return;
        }
        stopFluctuator(this.currentSelection, false);
        com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils.performClickDown(((com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder.ViewContainer) this.viewContainers.get(this.currentSelection)).iconContainer, 25, null, false);
    }

    public void selectionUp() {
        sLogger.v("selectionUp");
        if (!isItemInBounds(this.currentSelection)) {
            sLogger.v("selectionUp:invalid pos:" + this.currentSelection);
            return;
        }
        startFluctuator(this.currentSelection, false);
        com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils.performClickUp(((com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder.ViewContainer) this.viewContainers.get(this.currentSelection)).iconContainer, 25, null);
    }

    public static int getOffSetFromPos(int currentSelection2) {
        if (currentSelection2 <= 0) {
            return 0;
        }
        return (com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.mainIconSize * currentSelection2) + (iconMargin * currentSelection2);
    }

    public boolean hasToolTip() {
        return this.model.supportsToolTip;
    }
}
