package com.navdy.hud.app.ui.component.vlist.viewholder;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\b\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\b\u0002"}, d2 = {"<anonymous>", "", "run"}, k = 3, mv = {1, 1, 6})
/* compiled from: SwitchViewHolder.kt */
final class SwitchViewHolder$fluctuatorRunnable$Anon1 implements java.lang.Runnable {
    final /* synthetic */ com.navdy.hud.app.ui.component.vlist.viewholder.SwitchViewHolder this$Anon0;

    SwitchViewHolder$fluctuatorRunnable$Anon1(com.navdy.hud.app.ui.component.vlist.viewholder.SwitchViewHolder switchViewHolder) {
        this.this$Anon0 = switchViewHolder;
    }

    public final void run() {
        this.this$Anon0.startFluctuator();
    }
}
