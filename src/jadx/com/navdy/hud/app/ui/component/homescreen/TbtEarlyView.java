package com.navdy.hud.app.ui.component.homescreen;

public class TbtEarlyView extends android.widget.LinearLayout implements com.navdy.hud.app.ui.component.homescreen.IHomeScreenLifecycle {
    private static final java.lang.String TBT_EARLY_SHRUNK_MODE_VISIBLE = "persist.sys.tbtearlyshrunk";
    private com.navdy.hud.app.view.MainView.CustomAnimationMode currentMode;
    @butterknife.InjectView(2131624388)
    android.widget.ImageView earlyManeuverImage;
    private com.navdy.hud.app.ui.component.homescreen.HomeScreenView homeScreenView;
    private final boolean isShrunkVisible;
    private int lastNextTurnIconId;
    private com.navdy.service.library.log.Logger logger;
    private boolean paused;
    private com.navdy.hud.app.ui.framework.UIStateManager uiStateManager;

    public TbtEarlyView(android.content.Context context) {
        this(context, null);
    }

    public TbtEarlyView(android.content.Context context, android.util.AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TbtEarlyView(android.content.Context context, android.util.AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.lastNextTurnIconId = -1;
        this.isShrunkVisible = com.navdy.hud.app.util.os.SystemProperties.getBoolean(TBT_EARLY_SHRUNK_MODE_VISIBLE, true);
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        this.logger = com.navdy.hud.app.ui.component.homescreen.HomeScreenView.sLogger;
        super.onFinishInflate();
        butterknife.ButterKnife.inject((android.view.View) this);
        com.navdy.hud.app.manager.RemoteDeviceManager remoteDeviceManager = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance();
        this.uiStateManager = remoteDeviceManager.getUiStateManager();
        remoteDeviceManager.getBus().register(this);
    }

    private void setEarlyManeuver(int iconId) {
        if (this.lastNextTurnIconId != iconId) {
            this.lastNextTurnIconId = iconId;
            if (!this.paused) {
                if (this.lastNextTurnIconId != -1) {
                    this.earlyManeuverImage.setImageResource(this.lastNextTurnIconId);
                }
                showHideEarlyManeuver();
            }
        }
    }

    public void showHideEarlyManeuver() {
        if (this.lastNextTurnIconId != -1) {
            if (this.homeScreenView == null) {
                this.homeScreenView = this.uiStateManager.getHomescreenView();
            }
            if (this.currentMode != com.navdy.hud.app.view.MainView.CustomAnimationMode.EXPAND && !this.isShrunkVisible) {
                setVisibility(4);
            } else {
                setVisibility(0);
            }
        } else {
            setVisibility(4);
            this.earlyManeuverImage.setImageResource(0);
        }
    }

    @com.squareup.otto.Subscribe
    public void onManeuverDisplay(com.navdy.hud.app.maps.MapEvents.ManeuverDisplay event) {
        setEarlyManeuver(event.nextTurnIconId);
    }

    public void setViewXY() {
        setView(this.uiStateManager.getCustomAnimateMode());
    }

    public void setView(com.navdy.hud.app.view.MainView.CustomAnimationMode mode) {
        this.currentMode = mode;
        switch (mode) {
            case EXPAND:
                setX((float) com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadEarlyManeuverX);
                setY((float) com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadEarlyManeuverY);
                return;
            case SHRINK_LEFT:
                setX((float) com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadEarlyManeuverShrinkLeftX);
                setY((float) com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadEarlyManeuverYShrunk);
                return;
            default:
                return;
        }
    }

    public void getCustomAnimator(com.navdy.hud.app.view.MainView.CustomAnimationMode mode, android.animation.AnimatorSet.Builder builder) {
        this.currentMode = mode;
        switch (mode) {
            case EXPAND:
                if (!this.isShrunkVisible) {
                    builder.with(com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getAlphaAnimator(this, 1));
                }
                builder.with(com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getWidthAnimator(this, com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadEarlyManeuverWidth));
                builder.with(com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getXPositionAnimator(this, (float) com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadEarlyManeuverX));
                builder.with(com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getYPositionAnimator(this, (float) com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadEarlyManeuverY));
                return;
            case SHRINK_LEFT:
                if (!this.isShrunkVisible) {
                    builder.with(com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getAlphaAnimator(this, 0));
                }
                builder.with(com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getWidthAnimator(this, com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadEarlyManeuverWidthShrunk));
                builder.with(com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getXPositionAnimator(this, (float) com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadEarlyManeuverShrinkLeftX));
                builder.with(com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getYPositionAnimator(this, (float) com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadEarlyManeuverYShrunk));
                return;
            default:
                return;
        }
    }

    public void clearState() {
        this.lastNextTurnIconId = -1;
        showHideEarlyManeuver();
    }

    public void onPause() {
        if (!this.paused) {
            this.paused = true;
            this.logger.v("::onPause:tbtearly");
        }
    }

    public void onResume() {
        if (this.paused) {
            this.paused = false;
            this.logger.v("::onResume:tbtearly");
            int iconId = this.lastNextTurnIconId;
            if (iconId != -1) {
                this.logger.v("::onResume:tbtearly set last turn icon");
                this.lastNextTurnIconId = -1;
                setEarlyManeuver(iconId);
            }
        }
    }
}
