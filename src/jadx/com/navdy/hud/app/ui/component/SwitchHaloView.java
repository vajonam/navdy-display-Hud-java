package com.navdy.hud.app.ui.component;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0007\n\u0002\b\b\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\u000f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004B\u0017\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007B\u001f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\b\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0010\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0017H\u0016J\u0012\u0010\u0018\u001a\u00020\u00152\b\u0010\u0019\u001a\u0004\u0018\u00010\u001aH\u0016J\u0010\u0010\u001b\u001a\u00020\u00152\u0006\u0010\u001c\u001a\u00020\tH\u0016R\u001a\u0010\u000b\u001a\u00020\fX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u000e\"\u0004\b\u000f\u0010\u0010R\u001a\u0010\u0011\u001a\u00020\fX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0012\u0010\u000e\"\u0004\b\u0013\u0010\u0010\u00a8\u0006\u001d"}, d2 = {"Lcom/navdy/hud/app/ui/component/SwitchHaloView;", "Lcom/navdy/hud/app/ui/component/HaloView;", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "attrs", "Landroid/util/AttributeSet;", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "defStyleAttr", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "maxScalingFactorX", "", "getMaxScalingFactorX", "()F", "setMaxScalingFactorX", "(F)V", "maxScalingFactorY", "getMaxScalingFactorY", "setMaxScalingFactorY", "onAnimationUpdateInternal", "", "animation", "Landroid/animation/ValueAnimator;", "onDrawInternal", "canvas", "Landroid/graphics/Canvas;", "setStrokeColor", "color", "app_hudDebug"}, k = 1, mv = {1, 1, 6})
/* compiled from: SwitchHaloView.kt */
public final class SwitchHaloView extends com.navdy.hud.app.ui.component.HaloView {
    private java.util.HashMap _$_findViewCache;
    private float maxScalingFactorX;
    private float maxScalingFactorY;

    public void _$_clearFindViewByIdCache() {
        if (this._$_findViewCache != null) {
            this._$_findViewCache.clear();
        }
    }

    public android.view.View _$_findCachedViewById(int i) {
        if (this._$_findViewCache == null) {
            this._$_findViewCache = new java.util.HashMap();
        }
        android.view.View view = (android.view.View) this._$_findViewCache.get(java.lang.Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        android.view.View findViewById = findViewById(i);
        this._$_findViewCache.put(java.lang.Integer.valueOf(i), findViewById);
        return findViewById;
    }

    public final float getMaxScalingFactorX() {
        return this.maxScalingFactorX;
    }

    public final void setMaxScalingFactorX(float f) {
        this.maxScalingFactorX = f;
    }

    public final float getMaxScalingFactorY() {
        return this.maxScalingFactorY;
    }

    public final void setMaxScalingFactorY(float f) {
        this.maxScalingFactorY = f;
    }

    public SwitchHaloView(@org.jetbrains.annotations.NotNull android.content.Context context) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(context, "context");
        super(context);
    }

    public SwitchHaloView(@org.jetbrains.annotations.NotNull android.content.Context context, @org.jetbrains.annotations.NotNull android.util.AttributeSet attrs) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(context, "context");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(attrs, "attrs");
        super(context, attrs);
    }

    public SwitchHaloView(@org.jetbrains.annotations.NotNull android.content.Context context, @org.jetbrains.annotations.NotNull android.util.AttributeSet attrs, int defStyleAttr) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(context, "context");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(attrs, "attrs");
        super(context, attrs, defStyleAttr);
    }

    public void onAnimationUpdateInternal(@org.jetbrains.annotations.NotNull android.animation.ValueAnimator animation) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(animation, "animation");
        super.onAnimationUpdateInternal(animation);
        java.lang.Object animatedValue = animation.getAnimatedValue();
        if (animatedValue == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type kotlin.Float");
        }
        float scaleFactor = ((((java.lang.Float) animatedValue).floatValue() - this.startRadius) / (this.endRadius - this.startRadius)) * ((float) 2);
        this.maxScalingFactorX = (this.endRadius - this.startRadius) / ((float) getWidth());
        this.maxScalingFactorY = (this.endRadius - this.startRadius) / ((float) getHeight());
        setScaleX(((float) 1) + (this.maxScalingFactorX * scaleFactor));
        setScaleY(((float) 1) + (this.maxScalingFactorY * scaleFactor));
        setPivotX((float) (getWidth() / 2));
        setPivotY((float) (getHeight() / 2));
        invalidate();
    }

    public void onDrawInternal(@org.jetbrains.annotations.Nullable android.graphics.Canvas canvas) {
    }

    public void setStrokeColor(int color) {
        super.setStrokeColor(color);
        android.graphics.drawable.Drawable background = getBackground();
        if (background == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type android.graphics.drawable.GradientDrawable");
        }
        ((android.graphics.drawable.GradientDrawable) background).setColor(color);
    }
}
