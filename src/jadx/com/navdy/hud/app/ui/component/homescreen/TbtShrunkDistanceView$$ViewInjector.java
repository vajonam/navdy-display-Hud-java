package com.navdy.hud.app.ui.component.homescreen;

public class TbtShrunkDistanceView$$ViewInjector {
    public static void inject(butterknife.ButterKnife.Finder finder, com.navdy.hud.app.ui.component.homescreen.TbtShrunkDistanceView target, java.lang.Object source) {
        target.tbtShrunkDistance = (android.widget.TextView) finder.findRequiredView(source, com.navdy.hud.app.R.id.tbtShrunkDistance, "field 'tbtShrunkDistance'");
        target.nowShrunkIcon = finder.findRequiredView(source, com.navdy.hud.app.R.id.now_shrunk_icon, "field 'nowShrunkIcon'");
        target.progressBarShrunk = (android.widget.ProgressBar) finder.findRequiredView(source, com.navdy.hud.app.R.id.progress_bar_shrunk, "field 'progressBarShrunk'");
    }

    public static void reset(com.navdy.hud.app.ui.component.homescreen.TbtShrunkDistanceView target) {
        target.tbtShrunkDistance = null;
        target.nowShrunkIcon = null;
        target.progressBarShrunk = null;
    }
}
