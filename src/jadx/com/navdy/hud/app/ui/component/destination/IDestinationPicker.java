package com.navdy.hud.app.ui.component.destination;

public interface IDestinationPicker {
    void onDestinationPickerClosed();

    boolean onItemClicked(int i, int i2, com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.DestinationPickerState destinationPickerState);

    boolean onItemSelected(int i, int i2, com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.DestinationPickerState destinationPickerState);
}
