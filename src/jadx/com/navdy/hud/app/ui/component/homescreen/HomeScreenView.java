package com.navdy.hud.app.ui.component.homescreen;

public class HomeScreenView extends android.widget.RelativeLayout implements com.navdy.hud.app.view.ICustomAnimator, com.navdy.hud.app.manager.InputManager.IInputHandler, com.navdy.hud.app.gesture.GestureDetector.GestureListener {
    private static final int MIN_SPEED_LIMIT_VISIBLE_DURATION = ((int) java.util.concurrent.TimeUnit.SECONDS.toMillis(3));
    static final java.lang.String SYSPROP_ALWAYS_SHOW_SPEED_LIMIT_SIGN = "persist.sys.speedlimit.always";
    private static final java.util.List<com.navdy.hud.app.ui.component.homescreen.HomeScreenView.HomeScreenWidgetInfo> editableHomeScreenWidgetInfos = new java.util.ArrayList();
    static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.homescreen.HomeScreenView.class);
    private boolean alwaysShowSpeedLimitSign;
    @javax.inject.Inject
    com.squareup.otto.Bus bus;
    private com.navdy.hud.app.maps.NavigationMode currentNavigationMode;
    com.navdy.hud.app.view.DriveScoreGaugePresenter driveScoreGaugePresenter;
    @butterknife.InjectView(2131624329)
    com.navdy.hud.app.view.DashboardWidgetView driveScoreWidgetView;
    private android.content.SharedPreferences driverPreferences;
    @butterknife.InjectView(2131624332)
    com.navdy.hud.app.ui.component.homescreen.EtaView etaClockView;
    @javax.inject.Inject
    android.content.SharedPreferences globalPreferences;
    /* access modifiers changed from: private */
    public android.os.Handler handler;
    private java.lang.Runnable hideSpeedLimitRunnable;
    private boolean isRecalculating;
    private boolean isTopInit;
    /* access modifiers changed from: private */
    public boolean isTopViewAnimationOut;
    /* access modifiers changed from: private */
    public boolean isTopViewAnimationRunning;
    @butterknife.InjectView(2131624331)
    android.widget.ImageView laneGuidanceIconIndicator;
    @butterknife.InjectView(2131624335)
    com.navdy.hud.app.ui.component.homescreen.LaneGuidanceView laneGuidanceView;
    private long lastSpeedLimitShown;
    @butterknife.InjectView(2131624326)
    com.navdy.hud.app.ui.component.homescreen.HomeScreenRightSectionView mainscreenRightSection;
    @butterknife.InjectView(2131624336)
    com.navdy.hud.app.ui.component.homescreen.NavigationView mapContainer;
    @butterknife.InjectView(2131624121)
    android.widget.ImageView mapMask;
    @butterknife.InjectView(2131624327)
    android.view.ViewGroup mapViewSpeedContainer;
    private com.navdy.hud.app.ui.component.homescreen.HomeScreen.DisplayMode mode;
    @butterknife.InjectView(2131624330)
    protected android.widget.RelativeLayout navigationViewsContainer;
    private com.navdy.hud.app.framework.notifications.NotificationManager notificationManager;
    @butterknife.InjectView(2131624349)
    com.navdy.hud.app.ui.component.homescreen.OpenRoadView openMapRoadInfoContainer;
    private boolean paused;
    @javax.inject.Inject
    com.navdy.hud.app.ui.component.homescreen.HomeScreen.Presenter presenter;
    @butterknife.InjectView(2131624361)
    com.navdy.hud.app.ui.component.homescreen.RecalculatingView recalcRouteContainer;
    private android.animation.AnimatorSet rightScreenAnimator;
    private com.navdy.hud.app.ui.framework.IScreenAnimationListener screenAnimationListener;
    /* access modifiers changed from: private */
    public boolean showCollapsedNotif;
    @butterknife.InjectView(2131624365)
    com.navdy.hud.app.ui.component.homescreen.BaseSmartDashView smartDashContainer;
    private int speedLimit;
    @butterknife.InjectView(2131624328)
    com.navdy.hud.app.view.SpeedLimitSignView speedLimitSignView;
    private com.navdy.hud.app.manager.SpeedManager speedManager;
    @butterknife.InjectView(2131624343)
    com.navdy.hud.app.ui.component.homescreen.SpeedView speedView;
    @butterknife.InjectView(2131624377)
    com.navdy.hud.app.ui.component.tbt.TbtViewContainer tbtView;
    /* access modifiers changed from: private */
    public java.lang.Runnable topViewAnimationRunnable;
    private android.animation.AnimatorSet topViewAnimator;
    private com.navdy.hud.app.ui.framework.UIStateManager uiStateManager;

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            com.navdy.hud.app.ui.component.homescreen.HomeScreenView.sLogger.v("out animation");
            com.navdy.hud.app.ui.component.homescreen.HomeScreenView.this.animateTopViewsOut();
        }
    }

    class Anon10 implements java.lang.Runnable {
        Anon10() {
        }

        public void run() {
            com.navdy.hud.app.ui.component.homescreen.HomeScreenView.this.setDisplayMode(true);
        }
    }

    class Anon11 implements java.lang.Runnable {
        Anon11() {
        }

        public void run() {
            com.navdy.hud.app.ui.component.homescreen.HomeScreenView.this.updateSpeedLimitSign();
        }
    }

    class Anon2 implements com.navdy.hud.app.ui.framework.IScreenAnimationListener {
        Anon2() {
        }

        public void onStart(com.navdy.hud.app.screen.BaseScreen in, com.navdy.hud.app.screen.BaseScreen out) {
        }

        public void onStop(com.navdy.hud.app.screen.BaseScreen in, com.navdy.hud.app.screen.BaseScreen out) {
            if (com.navdy.hud.app.ui.component.homescreen.HomeScreenView.this.showCollapsedNotif && in != null && in.getScreen() == com.navdy.service.library.events.ui.Screen.SCREEN_HYBRID_MAP) {
                com.navdy.hud.app.ui.component.homescreen.HomeScreenView.this.showCollapsedNotif = false;
                com.navdy.hud.app.ui.component.homescreen.HomeScreenView.sLogger.v("expanding collapse notifi");
                com.navdy.hud.app.framework.notifications.NotificationManager notificationManager = com.navdy.hud.app.framework.notifications.NotificationManager.getInstance();
                if (notificationManager.makeNotificationCurrent(false)) {
                    notificationManager.showNotification();
                }
            }
        }
    }

    class Anon3 implements java.lang.Runnable {
        Anon3() {
        }

        public void run() {
            com.navdy.hud.app.ui.component.homescreen.HomeScreenView.this.hideSpeedLimit();
        }
    }

    class Anon4 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
        Anon4() {
        }

        public void onAnimationEnd(android.animation.Animator animation) {
            com.navdy.hud.app.ui.component.homescreen.HomeScreenView.this.setWidgetVisibility(com.navdy.hud.app.ui.component.homescreen.HomeScreenView.this.etaClockView, false);
        }
    }

    class Anon5 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
        Anon5() {
        }

        public void onAnimationEnd(android.animation.Animator animation) {
            com.navdy.hud.app.ui.component.homescreen.HomeScreenView.this.setWidgetVisibility(com.navdy.hud.app.ui.component.homescreen.HomeScreenView.this.driveScoreWidgetView, false);
        }
    }

    class Anon6 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
        Anon6() {
        }

        public void onAnimationStart(android.animation.Animator animation) {
            com.navdy.hud.app.ui.component.homescreen.HomeScreenView.this.updateClockWidgetsVisibility(true);
        }

        public void onAnimationEnd(android.animation.Animator animation) {
        }
    }

    class Anon7 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
        Anon7() {
        }

        public void onAnimationEnd(android.animation.Animator animation) {
            com.navdy.hud.app.ui.component.homescreen.HomeScreenView.this.isTopViewAnimationRunning = false;
            com.navdy.hud.app.ui.component.homescreen.HomeScreenView.this.isTopViewAnimationOut = true;
        }
    }

    class Anon8 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
        Anon8() {
        }

        public void onAnimationEnd(android.animation.Animator animation) {
            com.navdy.hud.app.ui.component.homescreen.HomeScreenView.this.isTopViewAnimationRunning = false;
            com.navdy.hud.app.ui.component.homescreen.HomeScreenView.this.isTopViewAnimationOut = false;
            com.navdy.hud.app.ui.component.homescreen.HomeScreenView.this.handler.removeCallbacks(com.navdy.hud.app.ui.component.homescreen.HomeScreenView.this.topViewAnimationRunnable);
            com.navdy.hud.app.ui.component.homescreen.HomeScreenView.this.handler.postDelayed(com.navdy.hud.app.ui.component.homescreen.HomeScreenView.this.topViewAnimationRunnable, 10000);
        }
    }

    class Anon9 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
        Anon9() {
        }

        public void onAnimationEnd(android.animation.Animator animation) {
            com.navdy.hud.app.ui.component.homescreen.HomeScreenView.this.mainscreenRightSection.ejectRightSection();
        }
    }

    public interface HomeScreenWidgetConstants {
        public static final java.lang.String HOME_SCREEN_ALWAYS_SHOW_SPEED_LIMIT_WIDGET = "home_screen_always_show_speed_limit";
        public static final java.lang.String HOME_SCREEN_DRIVE_SCORE_WIDGET = "home_screen_drive_score_widget";
        public static final java.lang.String HOME_SCREEN_ETA_WIDGET = "home_screen_eta_widget";
        public static final java.lang.String HOME_SCREEN_SPEED_LIMIT_WIDGET = "home_screen_speed_limit_widget";
        public static final java.lang.String HOME_SCREEN_SPEED_WIDGET = "home_screen_speed_widget";
        public static final java.lang.String HOME_SCREEN_TIME_WIDGET = "home_screen_time_widget";
    }

    public static class HomeScreenWidgetInfo {
        public int id;
        public int labelResourceId;

        public HomeScreenWidgetInfo(int id2, int labelResourceId2) {
            this.id = id2;
            this.labelResourceId = labelResourceId2;
        }
    }

    static class HomeScreenWidgetsPreferenceUpdated {
        HomeScreenWidgetsPreferenceUpdated() {
        }
    }

    static {
        editableHomeScreenWidgetInfos.add(new com.navdy.hud.app.ui.component.homescreen.HomeScreenView.HomeScreenWidgetInfo(com.navdy.hud.app.R.id.activeEtaContainer, com.navdy.hud.app.R.string.eta_clock_widget));
        editableHomeScreenWidgetInfos.add(new com.navdy.hud.app.ui.component.homescreen.HomeScreenView.HomeScreenWidgetInfo(com.navdy.hud.app.R.id.drive_score_events, com.navdy.hud.app.R.string.drive_score_widget));
    }

    public HomeScreenView(android.content.Context context) {
        this(context, null);
    }

    public HomeScreenView(android.content.Context context, android.util.AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public HomeScreenView(android.content.Context context, android.util.AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.isTopInit = true;
        this.speedManager = com.navdy.hud.app.manager.SpeedManager.getInstance();
        this.speedLimit = 0;
        this.alwaysShowSpeedLimitSign = false;
        this.topViewAnimationRunnable = new com.navdy.hud.app.ui.component.homescreen.HomeScreenView.Anon1();
        this.handler = new android.os.Handler();
        this.screenAnimationListener = new com.navdy.hud.app.ui.component.homescreen.HomeScreenView.Anon2();
        this.mode = com.navdy.hud.app.ui.component.homescreen.HomeScreen.DisplayMode.MAP;
        this.hideSpeedLimitRunnable = new com.navdy.hud.app.ui.component.homescreen.HomeScreenView.Anon3();
        this.uiStateManager = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getUiStateManager();
        this.notificationManager = com.navdy.hud.app.framework.notifications.NotificationManager.getInstance();
        this.alwaysShowSpeedLimitSign = isAlwaysShowSpeedLimitSign();
        if (!isInEditMode()) {
            this.driveScoreGaugePresenter = new com.navdy.hud.app.view.DriveScoreGaugePresenter(context, com.navdy.hud.app.R.layout.drive_score_map_layout, false);
            mortar.Mortar.inject(context, this);
        }
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
        android.view.LayoutInflater.from(getContext()).inflate(com.navdy.hud.app.R.layout.screen_home_smartdash, (android.view.ViewGroup) findViewById(com.navdy.hud.app.R.id.smart_dash_place_holder), true);
        butterknife.ButterKnife.inject((android.view.View) this);
        updateDriverPrefs();
        setDisplayMode(false);
        this.mapMask.setImageResource(com.navdy.hud.app.R.drawable.map_mask);
        this.mapContainer.init(this);
        this.smartDashContainer.init(this);
        this.recalcRouteContainer.init(this);
        this.tbtView.init(this);
        this.openMapRoadInfoContainer.init(this);
        this.etaClockView.init(this);
        this.laneGuidanceView.init(this);
        this.mainscreenRightSection.setX((float) com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.mainScreenRightSectionX);
        setViews(this.uiStateManager.getCustomAnimateMode());
        sLogger.v("setting navigation mode to MAP");
        setMode(com.navdy.hud.app.maps.NavigationMode.MAP);
        if (this.presenter != null) {
            this.presenter.takeView(this);
        }
        android.os.Bundle args = new android.os.Bundle();
        args.putInt(com.navdy.hud.app.view.DashboardWidgetPresenter.EXTRA_GRAVITY, 0);
        args.putBoolean(com.navdy.hud.app.view.DashboardWidgetPresenter.EXTRA_IS_ACTIVE, true);
        this.driveScoreGaugePresenter.setView(this.driveScoreWidgetView, args);
        this.driveScoreGaugePresenter.setWidgetVisibleToUser(true);
        this.uiStateManager.addScreenAnimationListener(this.screenAnimationListener);
        this.bus.register(this);
    }

    private void setViews(com.navdy.hud.app.view.MainView.CustomAnimationMode mode2) {
        this.etaClockView.setView(mode2);
        this.openMapRoadInfoContainer.setView(mode2);
        com.navdy.hud.app.view.MainView.CustomAnimationMode mainScreenMode = mode2;
        if (mode2 == com.navdy.hud.app.view.MainView.CustomAnimationMode.EXPAND && isModeVisible()) {
            mainScreenMode = com.navdy.hud.app.view.MainView.CustomAnimationMode.SHRINK_MODE;
        }
        this.mapContainer.setView(mainScreenMode);
        this.smartDashContainer.setView(mainScreenMode);
        setSpeedWidgets(mode2);
        this.tbtView.setView(mode2);
        this.recalcRouteContainer.setView(mode2);
        this.laneGuidanceView.setView(mode2);
    }

    public boolean isNavigationActive() {
        return this.currentNavigationMode == com.navdy.hud.app.maps.NavigationMode.MAP_ON_ROUTE || this.currentNavigationMode == com.navdy.hud.app.maps.NavigationMode.TBT_ON_ROUTE;
    }

    public com.navdy.hud.app.ui.component.homescreen.HomeScreen.DisplayMode getDisplayMode() {
        return this.mode;
    }

    public void setDisplayMode(com.navdy.hud.app.ui.component.homescreen.HomeScreen.DisplayMode mode2) {
        if (this.mode != mode2) {
            this.mode = mode2;
            setDisplayMode(true);
        }
    }

    /* access modifiers changed from: private */
    public void setDisplayMode(boolean resetTopViews) {
        sLogger.v("setDisplayMode:" + this.mode);
        switch (this.mode) {
            case MAP:
                sLogger.v("dash invisible");
                setViewsBackground(-16777216);
                this.mapContainer.onResume();
                this.laneGuidanceView.onResume();
                com.navdy.hud.app.obd.ObdManager.getInstance().enableInstantaneousMode(false);
                this.smartDashContainer.setVisibility(4);
                this.smartDashContainer.onPause();
                setWidgetVisibility(this.driveScoreWidgetView, true);
                if (getIsWidgetEnabled(this.driveScoreWidgetView.getId())) {
                    this.driveScoreGaugePresenter.onResume();
                }
                updateClockWidgetsVisibility(false);
                this.navigationViewsContainer.setVisibility(0);
                setWidgetVisibility(this.speedView, true);
                this.laneGuidanceView.showLastEvent();
                resumeNavigationViewContainer();
                break;
            case SMART_DASH:
                sLogger.v("dash visible");
                setViewsBackground(0);
                this.driveScoreGaugePresenter.onPause();
                com.navdy.hud.app.obd.ObdManager.getInstance().enableInstantaneousMode(true);
                this.smartDashContainer.onResume();
                this.smartDashContainer.setVisibility(0);
                removeWidget(this.driveScoreWidgetView);
                removeWidget(this.speedView);
                removeWidget(this.speedLimitSignView);
                removeWidget(this.etaClockView);
                this.laneGuidanceView.setVisibility(8);
                this.laneGuidanceIconIndicator.setVisibility(8);
                this.mapContainer.onPause();
                this.laneGuidanceView.onPause();
                this.navigationViewsContainer.setVisibility(0);
                resumeNavigationViewContainer();
                break;
        }
        updateRoadInfoView();
        if (resetTopViews) {
            resetTopViewsAnimator();
        }
    }

    @com.squareup.otto.Subscribe
    public void onNavigationModeChange(com.navdy.hud.app.maps.MapEvents.NavigationModeChange navigationModeChange) {
        setMode(com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().getCurrentNavigationMode());
    }

    @com.squareup.otto.Subscribe
    public void onMapEvent(com.navdy.hud.app.maps.MapEvents.ManeuverDisplay event) {
        this.speedLimit = java.lang.Math.round((float) com.navdy.hud.app.manager.SpeedManager.convert((double) event.currentSpeedLimit, com.navdy.hud.app.manager.SpeedManager.SpeedUnit.METERS_PER_SECOND, com.navdy.hud.app.manager.SpeedManager.getInstance().getSpeedUnit()));
        this.speedView.setSpeedLimit(this.speedLimit);
        updateSpeedLimitSign();
    }

    @com.squareup.otto.Subscribe
    public void onSpeedUnitChanged(com.navdy.hud.app.manager.SpeedManager.SpeedUnitChanged speedUnitChanged) {
        updateSpeedLimitSign();
    }

    @com.squareup.otto.Subscribe
    public void ObdPidChangeEvent(com.navdy.hud.app.obd.ObdManager.ObdPidChangeEvent event) {
        switch (event.pid.getId()) {
            case 13:
                updateSpeedLimitSign();
                return;
            default:
                return;
        }
    }

    @com.squareup.otto.Subscribe
    public void GPSSpeedChangeEvent(com.navdy.hud.app.maps.MapEvents.GPSSpeedEvent event) {
        updateSpeedLimitSign();
    }

    @com.squareup.otto.Subscribe
    public void onSpeedDataExpired(com.navdy.hud.app.manager.SpeedManager.SpeedDataExpired speedDataExpired) {
        updateSpeedLimitSign();
    }

    /* access modifiers changed from: private */
    public void updateSpeedLimitSign() {
        if (this.speedLimit <= 0) {
            hideSpeedLimit();
        } else if (this.speedManager.getCurrentSpeed() > this.speedLimit || this.alwaysShowSpeedLimitSign) {
            this.handler.removeCallbacks(this.hideSpeedLimitRunnable);
            this.speedLimitSignView.setSpeedLimitUnit(this.speedManager.getSpeedUnit());
            this.speedLimitSignView.setSpeedLimit(this.speedLimit);
            if (this.mode == com.navdy.hud.app.ui.component.homescreen.HomeScreen.DisplayMode.MAP) {
                setWidgetVisibility(this.speedLimitSignView, true);
                this.lastSpeedLimitShown = android.os.SystemClock.elapsedRealtime();
            }
        } else {
            hideSpeedLimit();
        }
    }

    /* access modifiers changed from: private */
    public void hideSpeedLimit() {
        this.handler.removeCallbacks(this.hideSpeedLimitRunnable);
        long diff = android.os.SystemClock.elapsedRealtime() - this.lastSpeedLimitShown;
        if (diff > ((long) MIN_SPEED_LIMIT_VISIBLE_DURATION)) {
            setWidgetVisibility(this.speedLimitSignView, false);
            this.speedLimitSignView.setSpeedLimit(0);
            this.lastSpeedLimitShown = 0;
            return;
        }
        this.handler.postDelayed(this.hideSpeedLimitRunnable, diff);
    }

    @com.squareup.otto.Subscribe
    public void onDriverProfileChanged(com.navdy.hud.app.event.DriverProfileChanged event) {
        updateDriverPrefs();
        setDisplayMode(true);
    }

    @com.squareup.otto.Subscribe
    public void onArrivalEvent(com.navdy.hud.app.maps.MapEvents.ArrivalEvent event) {
        if (getDisplayMode() == com.navdy.hud.app.ui.component.homescreen.HomeScreen.DisplayMode.MAP) {
            updateClockWidgetsVisibility(false);
        }
        resetTopViewsAnimator();
    }

    public com.navdy.hud.app.ui.component.tbt.TbtViewContainer getTbtView() {
        return this.tbtView;
    }

    public com.navdy.hud.app.ui.component.homescreen.NavigationView getNavigationView() {
        return this.mapContainer;
    }

    public com.navdy.hud.app.ui.component.homescreen.BaseSmartDashView getSmartDashView() {
        return this.smartDashContainer;
    }

    public com.navdy.hud.app.ui.component.homescreen.RecalculatingView getRecalculatingView() {
        return this.recalcRouteContainer;
    }

    public com.navdy.hud.app.ui.component.homescreen.EtaView getEtaView() {
        return this.etaClockView;
    }

    public com.navdy.hud.app.ui.component.homescreen.OpenRoadView getOpenRoadView() {
        return this.openMapRoadInfoContainer;
    }

    public android.content.SharedPreferences getDriverPreferences() {
        return this.driverPreferences;
    }

    /* access modifiers changed from: 0000 */
    public void updateDriverPrefs() {
        this.driverPreferences = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getDriverProfileManager().getLocalPreferencesForCurrentDriverProfile(com.navdy.hud.app.HudApplication.getAppContext());
    }

    public void setMode(com.navdy.hud.app.maps.NavigationMode navigationMode) {
        if (this.currentNavigationMode != navigationMode || this.mapContainer.isOverviewMapMode()) {
            sLogger.v("navigation mode changed to " + navigationMode);
            this.currentNavigationMode = navigationMode;
            updateLayoutForMode(navigationMode);
            this.mapContainer.layoutMap();
            this.bus.post(navigationMode);
        }
    }

    private void updateLayoutForMode(com.navdy.hud.app.maps.NavigationMode navigationMode) {
        int time;
        this.handler.removeCallbacks(this.topViewAnimationRunnable);
        this.isRecalculating = false;
        boolean navigationActive = isNavigationActive();
        this.mapContainer.updateLayoutForMode(navigationMode);
        this.smartDashContainer.updateLayoutForMode(navigationMode, false);
        this.mainscreenRightSection.updateLayoutForMode(navigationMode, this);
        this.tbtView.clearState();
        this.mapContainer.clearState();
        this.speedView.clearState();
        this.etaClockView.clearState();
        if (this.mode != com.navdy.hud.app.ui.component.homescreen.HomeScreen.DisplayMode.MAP) {
            removeWidget(this.speedView);
            removeWidget(this.etaClockView);
            removeWidget(this.speedLimitSignView);
            removeWidget(this.driveScoreWidgetView);
        }
        updateClockWidgetsVisibility(false);
        if (navigationActive) {
            this.tbtView.setVisibility(0);
        } else {
            this.recalcRouteContainer.hideRecalculating();
            this.tbtView.clearState();
            this.tbtView.setVisibility(8);
        }
        updateRoadInfoView();
        resetTopViewsAnimator();
        if (this.isTopInit) {
            time = 30000;
            this.isTopInit = false;
        } else {
            time = 10000;
        }
        this.handler.postDelayed(this.topViewAnimationRunnable, (long) time);
    }

    private void updateRoadInfoView() {
        if (isNavigationActive()) {
            this.openMapRoadInfoContainer.setVisibility(8);
            return;
        }
        this.openMapRoadInfoContainer.setRoad();
        this.openMapRoadInfoContainer.setVisibility(0);
    }

    /* access modifiers changed from: private */
    public void updateClockWidgetsVisibility(boolean expanding) {
        if (this.mode != com.navdy.hud.app.ui.component.homescreen.HomeScreen.DisplayMode.MAP) {
            removeWidget(this.etaClockView);
        } else if (!this.uiStateManager.isMainUIShrunk() || expanding) {
            if (!isNavigationActive() || com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().hasArrived()) {
                this.etaClockView.setShowEta(false);
            } else {
                this.etaClockView.setShowEta(true);
            }
            setWidgetVisibility(this.etaClockView, true);
        } else {
            setWidgetVisibility(this.etaClockView, false);
        }
    }

    public void onClick() {
    }

    public void onTrackHand(float normalized) {
    }

    public boolean onGesture(com.navdy.service.library.events.input.GestureEvent event) {
        if (event.gesture != com.navdy.service.library.events.input.Gesture.GESTURE_SWIPE_RIGHT || !isModeVisible() || !hasModeView() || (this.rightScreenAnimator != null && this.rightScreenAnimator.isRunning())) {
            switch (this.mode) {
                case MAP:
                    return this.mapContainer.onGesture(event);
                case SMART_DASH:
                    return this.smartDashContainer.onGesture(event);
                default:
                    return false;
            }
        } else {
            sLogger.v("swipe_right animateOutModeView");
            animateOutModeView(true);
            return true;
        }
    }

    public boolean onKey(com.navdy.hud.app.manager.InputManager.CustomKeyEvent event) {
        if (shouldAnimateTopViews()) {
            switch (event) {
                case LEFT:
                    animateTopViewsIn();
                    break;
                case RIGHT:
                    animateTopViewsOut();
                    break;
            }
        }
        switch (this.mode) {
            case MAP:
                return this.mapContainer.onKey(event);
            case SMART_DASH:
                return this.smartDashContainer.onKey(event);
            default:
                return false;
        }
    }

    public com.navdy.hud.app.manager.InputManager.IInputHandler nextHandler() {
        return com.navdy.hud.app.manager.InputManager.nextContainingHandler(this);
    }

    public android.animation.Animator getCustomAnimator(com.navdy.hud.app.view.MainView.CustomAnimationMode customAnimationMode) {
        switch (customAnimationMode) {
            case SHRINK_LEFT:
                return getShrinkLeftAnimator();
            case EXPAND:
                return getExpandAnimator();
            default:
                return null;
        }
    }

    private android.animation.Animator getShrinkLeftAnimator() {
        boolean isNavigating = isNavigationActive();
        android.animation.AnimatorSet shrinkAnimatorSet = new android.animation.AnimatorSet();
        android.animation.AnimatorSet.Builder builder = shrinkAnimatorSet.play(android.animation.ValueAnimator.ofFloat(new float[]{1.0f, 100.0f}));
        this.mapContainer.getCustomAnimator(com.navdy.hud.app.view.MainView.CustomAnimationMode.SHRINK_LEFT, builder);
        if (this.mode == com.navdy.hud.app.ui.component.homescreen.HomeScreen.DisplayMode.MAP) {
            builder.with(com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getXPositionAnimator(this.mapViewSpeedContainer, (float) com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.speedShrinkLeftX));
        }
        this.smartDashContainer.getCustomAnimator(com.navdy.hud.app.view.MainView.CustomAnimationMode.SHRINK_LEFT, builder);
        if (!isNavigating) {
            this.tbtView.setView(com.navdy.hud.app.view.MainView.CustomAnimationMode.SHRINK_LEFT);
            this.openMapRoadInfoContainer.getCustomAnimator(com.navdy.hud.app.view.MainView.CustomAnimationMode.SHRINK_LEFT, builder);
        } else {
            this.openMapRoadInfoContainer.setView(com.navdy.hud.app.view.MainView.CustomAnimationMode.SHRINK_LEFT);
            this.tbtView.getCustomAnimator(com.navdy.hud.app.view.MainView.CustomAnimationMode.SHRINK_LEFT, builder);
            this.laneGuidanceView.getCustomAnimator(com.navdy.hud.app.view.MainView.CustomAnimationMode.SHRINK_LEFT, builder);
        }
        if (this.mode == com.navdy.hud.app.ui.component.homescreen.HomeScreen.DisplayMode.MAP) {
            if (getIsWidgetEnabled(this.etaClockView.getId())) {
                android.animation.AnimatorSet etaAnimator = new android.animation.AnimatorSet();
                etaAnimator.playTogether(new android.animation.Animator[]{android.animation.ObjectAnimator.ofFloat(this.etaClockView, android.view.View.ALPHA, new float[]{1.0f, 0.0f})});
                etaAnimator.addListener(new com.navdy.hud.app.ui.component.homescreen.HomeScreenView.Anon4());
                builder.with(etaAnimator);
            }
            if (getIsWidgetEnabled(this.driveScoreWidgetView.getId())) {
                android.animation.Animator driveScoreEventsAnimator = android.animation.ObjectAnimator.ofFloat(this.driveScoreWidgetView, android.view.View.ALPHA, new float[]{1.0f, 0.0f});
                driveScoreEventsAnimator.addListener(new com.navdy.hud.app.ui.component.homescreen.HomeScreenView.Anon5());
                builder.with(driveScoreEventsAnimator);
            }
        }
        if (this.isRecalculating) {
            builder.with(com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getXPositionAnimator(this.recalcRouteContainer, (float) com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.recalcShrinkLeftX));
        } else {
            this.recalcRouteContainer.setView(com.navdy.hud.app.view.MainView.CustomAnimationMode.SHRINK_LEFT);
        }
        animateTopViewsOut();
        return shrinkAnimatorSet;
    }

    private android.animation.Animator getExpandAnimator() {
        boolean isNavigating = isNavigationActive();
        android.animation.AnimatorSet expandAnimatorSet = new android.animation.AnimatorSet();
        android.animation.AnimatorSet.Builder builder = expandAnimatorSet.play(android.animation.ValueAnimator.ofFloat(new float[]{1.0f, 100.0f}));
        if (!this.isTopViewAnimationOut) {
        }
        if (this.mode == com.navdy.hud.app.ui.component.homescreen.HomeScreen.DisplayMode.MAP) {
            if (this.etaClockView.getAlpha() == 0.0f) {
                builder.with(android.animation.ObjectAnimator.ofFloat(this.etaClockView, ALPHA, new float[]{0.0f, 1.0f}));
            }
            setWidgetVisibility(this.driveScoreWidgetView, true);
            if (this.driveScoreWidgetView.getAlpha() == 0.0f) {
                builder.with(android.animation.ObjectAnimator.ofFloat(this.driveScoreWidgetView, android.view.View.ALPHA, new float[]{0.0f, 1.0f}));
            }
        }
        this.mapContainer.getCustomAnimator(com.navdy.hud.app.view.MainView.CustomAnimationMode.EXPAND, builder);
        if (this.mode == com.navdy.hud.app.ui.component.homescreen.HomeScreen.DisplayMode.MAP) {
            this.mapViewSpeedContainer.setVisibility(0);
            if (this.mapViewSpeedContainer.getX() != ((float) com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.speedX)) {
                builder.with(com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getXPositionAnimator(this.mapViewSpeedContainer, (float) com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.speedX));
            }
        }
        this.smartDashContainer.getCustomAnimator(com.navdy.hud.app.view.MainView.CustomAnimationMode.EXPAND, builder);
        if (!isNavigating) {
            this.tbtView.setView(com.navdy.hud.app.view.MainView.CustomAnimationMode.EXPAND);
            this.openMapRoadInfoContainer.getCustomAnimator(com.navdy.hud.app.view.MainView.CustomAnimationMode.EXPAND, builder);
        } else {
            this.openMapRoadInfoContainer.setView(com.navdy.hud.app.view.MainView.CustomAnimationMode.EXPAND);
            this.tbtView.getCustomAnimator(com.navdy.hud.app.view.MainView.CustomAnimationMode.EXPAND, builder);
            this.laneGuidanceView.getCustomAnimator(com.navdy.hud.app.view.MainView.CustomAnimationMode.EXPAND, builder);
        }
        if (this.isRecalculating) {
            builder.with(com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getXPositionAnimator(this.recalcRouteContainer, (float) com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.recalcX));
        } else {
            this.recalcRouteContainer.setView(com.navdy.hud.app.view.MainView.CustomAnimationMode.EXPAND);
        }
        expandAnimatorSet.addListener(new com.navdy.hud.app.ui.component.homescreen.HomeScreenView.Anon6());
        animateTopViewsIn();
        return expandAnimatorSet;
    }

    public boolean isRecalculating() {
        return this.isRecalculating;
    }

    public void setRecalculating(boolean val) {
        this.isRecalculating = val;
    }

    public boolean isShowCollapsedNotification() {
        return this.showCollapsedNotif;
    }

    public void setShowCollapsedNotification(boolean val) {
        this.showCollapsedNotif = val;
    }

    /* access modifiers changed from: 0000 */
    public void animateTopViewsOut() {
        this.handler.removeCallbacks(this.topViewAnimationRunnable);
        if (!this.isTopViewAnimationRunning && !this.isTopViewAnimationOut) {
            this.topViewAnimator = new android.animation.AnimatorSet();
            android.animation.AnimatorSet.Builder builder = this.topViewAnimator.play(android.animation.ValueAnimator.ofInt(new int[]{0, 0}));
            switch (this.mode) {
                case MAP:
                    this.mapContainer.getTopAnimator(builder, true);
                    if (getIsWidgetEnabled(this.speedView.getId())) {
                        this.speedView.getTopAnimator(builder, true);
                        break;
                    }
                    break;
                case SMART_DASH:
                    this.smartDashContainer.getTopAnimator(builder, true);
                    break;
            }
            this.isTopViewAnimationRunning = true;
            this.topViewAnimator.addListener(new com.navdy.hud.app.ui.component.homescreen.HomeScreenView.Anon7());
            this.topViewAnimator.start();
            sLogger.v("started out animation");
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean isTopViewAnimationOut() {
        return this.isTopViewAnimationOut;
    }

    /* access modifiers changed from: 0000 */
    public void animateTopViewsIn() {
        if (!this.isTopViewAnimationRunning && shouldAnimateTopViews() && this.isTopViewAnimationOut) {
            this.topViewAnimator = new android.animation.AnimatorSet();
            android.animation.AnimatorSet.Builder builder = this.topViewAnimator.play(android.animation.ValueAnimator.ofInt(new int[]{0, 0}));
            switch (this.mode) {
                case MAP:
                    this.mapContainer.getTopAnimator(builder, false);
                    if (getIsWidgetEnabled(this.speedView.getId())) {
                        this.speedView.getTopAnimator(builder, false);
                        break;
                    }
                    break;
                case SMART_DASH:
                    this.smartDashContainer.getTopAnimator(builder, false);
                    break;
            }
            this.isTopViewAnimationRunning = true;
            this.topViewAnimator.addListener(new com.navdy.hud.app.ui.component.homescreen.HomeScreenView.Anon8());
            this.topViewAnimator.start();
            sLogger.v("started in animation");
        }
    }

    private void resetTopViewsAnimator() {
        sLogger.v("resetTopViewsAnimator");
        if (this.topViewAnimator != null && this.topViewAnimator.isRunning()) {
            this.topViewAnimator.removeAllListeners();
            this.topViewAnimator.cancel();
        }
        this.mapContainer.resetTopViewsAnimator();
        if (getIsWidgetEnabled(this.speedView.getId())) {
            this.speedView.resetTopViewsAnimator();
        }
        this.smartDashContainer.resetTopViewsAnimator();
        if (!shouldAnimateTopViews()) {
            this.handler.removeCallbacks(this.topViewAnimationRunnable);
            animateTopViewsOut();
            return;
        }
        this.etaClockView.setView(com.navdy.hud.app.view.MainView.CustomAnimationMode.EXPAND);
        this.etaClockView.setAlpha(1.0f);
        this.isTopViewAnimationOut = false;
        this.isTopViewAnimationRunning = false;
        this.handler.removeCallbacks(this.topViewAnimationRunnable);
        this.handler.postDelayed(this.topViewAnimationRunnable, 10000);
    }

    public boolean shouldAnimateTopViews() {
        return this.mode != com.navdy.hud.app.ui.component.homescreen.HomeScreen.DisplayMode.SMART_DASH || this.smartDashContainer.shouldShowTopViews();
    }

    public android.widget.ImageView getLaneGuidanceIconIndicator() {
        return this.laneGuidanceIconIndicator;
    }

    public void onPause() {
        if (!this.paused) {
            this.paused = true;
            this.smartDashContainer.onPause();
            this.mapContainer.onPause();
            pauseNavigationViewContainer();
            sLogger.v("::onPause");
        }
    }

    public void onResume() {
        if (this.paused) {
            this.paused = false;
            switch (this.mode) {
                case MAP:
                    this.mapContainer.onResume();
                    this.laneGuidanceView.onResume();
                    this.smartDashContainer.onPause();
                    break;
                case SMART_DASH:
                    this.mapContainer.onPause();
                    this.laneGuidanceView.onPause();
                    this.smartDashContainer.onResume();
                    break;
            }
            resumeNavigationViewContainer();
            sLogger.v("::onResume");
        }
    }

    private void pauseNavigationViewContainer() {
        sLogger.v("::onPause:pauseNavigationViewContainer");
        this.tbtView.onPause();
        this.recalcRouteContainer.onPause();
    }

    private void resumeNavigationViewContainer() {
        sLogger.v("::onResume:resumeNavigationViewContainer");
        this.tbtView.onResume();
        this.recalcRouteContainer.onResume();
    }

    private void setViewsBackground(int color) {
        sLogger.v("setViewsBackground:" + color);
        this.tbtView.setBackgroundColor(color);
        this.recalcRouteContainer.setBackgroundColor(color);
    }

    public void injectRightSection(android.view.View view) {
        sLogger.v("injectRightSection");
        this.mainscreenRightSection.injectRightSection(view);
        clearRightSectionAnimation();
        com.navdy.hud.app.ui.activity.Main mainScreen = this.uiStateManager.getRootScreen();
        if (this.mainscreenRightSection.isViewVisible()) {
            sLogger.v("injectRightSection: right section view already visible");
        } else if (mainScreen.isNotificationExpanding() || mainScreen.isNotificationViewShowing()) {
            sLogger.v("injectRightSection: cannot animate");
        } else {
            animateInModeView();
        }
    }

    public void ejectRightSection() {
        sLogger.v("ejectRightSection");
        clearRightSectionAnimation();
        if (this.mainscreenRightSection.isViewVisible()) {
            com.navdy.hud.app.ui.activity.Main mainScreen = this.uiStateManager.getRootScreen();
            if (mainScreen.isNotificationExpanding() || mainScreen.isNotificationViewShowing()) {
                sLogger.v("ejectRightSection: cannot animate");
                this.mainscreenRightSection.ejectRightSection();
                return;
            }
            animateOutModeView(true);
            return;
        }
        this.mainscreenRightSection.ejectRightSection();
    }

    private void clearRightSectionAnimation() {
        if (this.rightScreenAnimator != null && this.rightScreenAnimator.isRunning()) {
            this.rightScreenAnimator.removeAllListeners();
            this.rightScreenAnimator.cancel();
            sLogger.v("stopped rightscreen animation");
        }
        this.rightScreenAnimator = null;
    }

    public boolean hasModeView() {
        return this.mainscreenRightSection.hasView();
    }

    public boolean isModeVisible() {
        return this.mainscreenRightSection.isViewVisible();
    }

    public void animateInModeView() {
        if (hasModeView() && !isModeVisible()) {
            sLogger.v("mode-view animateInModeView");
            this.rightScreenAnimator = new android.animation.AnimatorSet();
            android.animation.AnimatorSet.Builder builder = this.rightScreenAnimator.play(android.animation.ValueAnimator.ofFloat(new float[]{0.0f, 0.0f}));
            this.mapContainer.getCustomAnimator(com.navdy.hud.app.view.MainView.CustomAnimationMode.SHRINK_MODE, builder);
            this.smartDashContainer.getCustomAnimator(com.navdy.hud.app.view.MainView.CustomAnimationMode.SHRINK_MODE, builder);
            this.mainscreenRightSection.getCustomAnimator(com.navdy.hud.app.view.MainView.CustomAnimationMode.SHRINK_MODE, builder);
            this.rightScreenAnimator.start();
        }
    }

    public void animateOutModeView(boolean removeView) {
        if (isModeVisible()) {
            sLogger.v("mode-view animateOutModeView:" + removeView);
            this.rightScreenAnimator = new android.animation.AnimatorSet();
            android.animation.AnimatorSet.Builder builder = this.rightScreenAnimator.play(android.animation.ValueAnimator.ofFloat(new float[]{0.0f, 0.0f}));
            this.mapContainer.getCustomAnimator(com.navdy.hud.app.view.MainView.CustomAnimationMode.EXPAND, builder);
            this.smartDashContainer.getCustomAnimator(com.navdy.hud.app.view.MainView.CustomAnimationMode.EXPAND, builder);
            this.mainscreenRightSection.getCustomAnimator(com.navdy.hud.app.view.MainView.CustomAnimationMode.EXPAND, builder);
            if (removeView) {
                this.rightScreenAnimator.addListener(new com.navdy.hud.app.ui.component.homescreen.HomeScreenView.Anon9());
            }
            this.rightScreenAnimator.start();
        }
    }

    public void resetModeView() {
        if (isModeVisible()) {
            sLogger.v("mode-view resetModeView");
            this.mapContainer.setView(com.navdy.hud.app.view.MainView.CustomAnimationMode.EXPAND);
            this.smartDashContainer.setView(com.navdy.hud.app.view.MainView.CustomAnimationMode.EXPAND);
            this.mainscreenRightSection.setView(com.navdy.hud.app.view.MainView.CustomAnimationMode.EXPAND);
        }
    }

    private void setSpeedWidgets(com.navdy.hud.app.view.MainView.CustomAnimationMode mode2) {
        switch (mode2) {
            case SHRINK_LEFT:
                this.mapViewSpeedContainer.setX((float) com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.speedShrinkLeftX);
                return;
            case EXPAND:
                this.mapViewSpeedContainer.setX((float) com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.speedX);
                return;
            default:
                return;
        }
    }

    private void removeWidget(android.view.View widget) {
        widget.setVisibility(8);
    }

    /* access modifiers changed from: private */
    public void setWidgetVisibility(android.view.View widget, boolean visible) {
        if (!getIsWidgetEnabled(widget.getId())) {
            removeWidget(widget);
        } else if (visible) {
            widget.setVisibility(0);
        } else if (widget.getVisibility() != 8) {
            widget.setVisibility(4);
        }
    }

    public boolean getIsWidgetEnabled(int id) {
        java.lang.String widgetPersistentId = getWidgetPersistentId(id);
        if (widgetPersistentId != null) {
            return com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfilePreferences().getBoolean(widgetPersistentId, true);
        }
        return false;
    }

    private java.lang.String getWidgetPersistentId(int id) {
        switch (id) {
            case com.navdy.hud.app.R.id.home_screen_speed_limit_sign /*2131624328*/:
                return com.navdy.hud.app.ui.component.homescreen.HomeScreenView.HomeScreenWidgetConstants.HOME_SCREEN_SPEED_LIMIT_WIDGET;
            case com.navdy.hud.app.R.id.drive_score_events /*2131624329*/:
                return com.navdy.hud.app.ui.component.homescreen.HomeScreenView.HomeScreenWidgetConstants.HOME_SCREEN_DRIVE_SCORE_WIDGET;
            case com.navdy.hud.app.R.id.activeEtaContainer /*2131624332*/:
                return com.navdy.hud.app.ui.component.homescreen.HomeScreenView.HomeScreenWidgetConstants.HOME_SCREEN_TIME_WIDGET;
            case com.navdy.hud.app.R.id.speedContainer /*2131624343*/:
                return com.navdy.hud.app.ui.component.homescreen.HomeScreenView.HomeScreenWidgetConstants.HOME_SCREEN_SPEED_WIDGET;
            default:
                return null;
        }
    }

    public java.util.List<com.navdy.hud.app.ui.component.homescreen.HomeScreenView.HomeScreenWidgetInfo> getHomeScreenWidgetInfoList() {
        return editableHomeScreenWidgetInfos;
    }

    public void setIsWidgetEnabled(int id, boolean enabled) {
        java.lang.String widgetPersistentId = getWidgetPersistentId(id);
        if (widgetPersistentId != null) {
            com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfilePreferences().edit().putBoolean(widgetPersistentId, enabled).apply();
        }
        this.handler.post(new com.navdy.hud.app.ui.component.homescreen.HomeScreenView.Anon10());
    }

    public boolean isAHomeScreenWidgetId(int id) {
        switch (id) {
            case com.navdy.hud.app.R.id.home_screen_speed_limit_sign /*2131624328*/:
            case com.navdy.hud.app.R.id.drive_score_events /*2131624329*/:
            case com.navdy.hud.app.R.id.activeEtaContainer /*2131624332*/:
            case com.navdy.hud.app.R.id.speedContainer /*2131624343*/:
                return true;
            default:
                return false;
        }
    }

    public void setAlwaysShowSpeedLimitSign(boolean alwaysShowSpeedLimitSign2) {
        if (alwaysShowSpeedLimitSign2) {
            setIsWidgetEnabled(com.navdy.hud.app.R.id.home_screen_speed_limit_sign, true);
        }
        com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfilePreferences().edit().putBoolean(com.navdy.hud.app.ui.component.homescreen.HomeScreenView.HomeScreenWidgetConstants.HOME_SCREEN_ALWAYS_SHOW_SPEED_LIMIT_WIDGET, alwaysShowSpeedLimitSign2).apply();
        this.alwaysShowSpeedLimitSign = alwaysShowSpeedLimitSign2;
        this.handler.post(new com.navdy.hud.app.ui.component.homescreen.HomeScreenView.Anon11());
    }

    public boolean isAlwaysShowSpeedLimitSign() {
        return com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfilePreferences().getBoolean(com.navdy.hud.app.ui.component.homescreen.HomeScreenView.HomeScreenWidgetConstants.HOME_SCREEN_ALWAYS_SHOW_SPEED_LIMIT_WIDGET, false);
    }
}
