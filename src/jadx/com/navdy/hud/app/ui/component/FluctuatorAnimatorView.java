package com.navdy.hud.app.ui.component;

public class FluctuatorAnimatorView extends android.view.View {
    private android.animation.ValueAnimator alphaAnimator;
    /* access modifiers changed from: private */
    public int animationDelay;
    private int animationDuration;
    private com.navdy.hud.app.ui.framework.DefaultAnimationListener animationListener;
    /* access modifiers changed from: private */
    public android.animation.AnimatorSet animatorSet;
    float currentCircle;
    private float endRadius;
    private int fillColor;
    private boolean fillEnabled;
    /* access modifiers changed from: private */
    public android.os.Handler handler;
    private android.view.animation.LinearInterpolator interpolator;
    private android.graphics.Paint paint;
    private android.animation.ValueAnimator radiusAnimator;
    private float startRadius;
    public java.lang.Runnable startRunnable;
    boolean started;
    private int strokeColor;
    private float strokeWidth;

    class Anon1 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
        Anon1() {
        }

        public void onAnimationEnd(android.animation.Animator animation) {
            com.navdy.hud.app.ui.component.FluctuatorAnimatorView.this.handler.postDelayed(com.navdy.hud.app.ui.component.FluctuatorAnimatorView.this.startRunnable, (long) com.navdy.hud.app.ui.component.FluctuatorAnimatorView.this.animationDelay);
        }
    }

    class Anon2 implements java.lang.Runnable {
        Anon2() {
        }

        public void run() {
            com.navdy.hud.app.ui.component.FluctuatorAnimatorView.this.animatorSet.start();
        }
    }

    class Anon3 implements android.animation.ValueAnimator.AnimatorUpdateListener {
        Anon3() {
        }

        public void onAnimationUpdate(android.animation.ValueAnimator animation) {
            com.navdy.hud.app.ui.component.FluctuatorAnimatorView.this.currentCircle = ((java.lang.Float) animation.getAnimatedValue()).floatValue();
            com.navdy.hud.app.ui.component.FluctuatorAnimatorView.this.invalidate();
        }
    }

    class Anon4 implements android.animation.ValueAnimator.AnimatorUpdateListener {
        Anon4() {
        }

        public void onAnimationUpdate(android.animation.ValueAnimator animation) {
            com.navdy.hud.app.ui.component.FluctuatorAnimatorView.this.setAlpha(((java.lang.Float) animation.getAnimatedValue()).floatValue());
        }
    }

    public FluctuatorAnimatorView(android.content.Context context) {
        this(context, null);
    }

    public FluctuatorAnimatorView(android.content.Context context, android.util.AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public FluctuatorAnimatorView(android.content.Context context, android.util.AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.interpolator = new android.view.animation.LinearInterpolator();
        this.handler = new android.os.Handler(android.os.Looper.getMainLooper());
        this.animationListener = new com.navdy.hud.app.ui.component.FluctuatorAnimatorView.Anon1();
        this.startRunnable = new com.navdy.hud.app.ui.component.FluctuatorAnimatorView.Anon2();
        this.started = false;
        android.content.res.TypedArray a = context.obtainStyledAttributes(attrs, com.navdy.hud.app.R.styleable.FluctuatorAnimatorView, defStyleAttr, 0);
        if (a != null) {
            this.strokeColor = a.getColor(0, -1);
            this.fillColor = a.getColor(1, -1);
            this.startRadius = a.getDimension(2, 0.0f);
            this.endRadius = a.getDimension(3, 0.0f);
            this.strokeWidth = a.getDimension(4, 0.0f);
            this.animationDuration = a.getInteger(5, 0);
            this.animationDelay = a.getInteger(6, 0);
            a.recycle();
        }
        this.paint = new android.graphics.Paint();
        this.paint.setStrokeWidth(this.strokeWidth);
        this.paint.setAntiAlias(true);
        this.animatorSet = new android.animation.AnimatorSet();
        this.animatorSet.setDuration((long) this.animationDuration);
        this.radiusAnimator = android.animation.ValueAnimator.ofFloat(new float[]{this.startRadius, this.endRadius});
        this.alphaAnimator = android.animation.ValueAnimator.ofFloat(new float[]{1.0f, 0.0f});
        this.animatorSet.setInterpolator(this.interpolator);
        this.radiusAnimator.addUpdateListener(new com.navdy.hud.app.ui.component.FluctuatorAnimatorView.Anon3());
        this.alphaAnimator.addUpdateListener(new com.navdy.hud.app.ui.component.FluctuatorAnimatorView.Anon4());
        this.animatorSet.playTogether(new android.animation.Animator[]{this.radiusAnimator, this.alphaAnimator});
    }

    public void onDraw(android.graphics.Canvas canvas) {
        super.onDraw(canvas);
        if (this.fillColor != -1 || this.fillEnabled) {
            this.paint.setStyle(android.graphics.Paint.Style.FILL);
            this.paint.setColor(this.fillColor);
            canvas.drawCircle((float) (getWidth() / 2), (float) (getHeight() / 2), this.currentCircle, this.paint);
        }
        this.paint.setStyle(android.graphics.Paint.Style.STROKE);
        this.paint.setColor(this.strokeColor);
        canvas.drawCircle((float) (getWidth() / 2), (float) (getHeight() / 2), this.currentCircle, this.paint);
    }

    public void start() {
        stop();
        this.animatorSet.addListener(this.animationListener);
        this.animatorSet.start();
        this.started = true;
    }

    public void stop() {
        this.handler.removeCallbacks(this.startRunnable);
        if (this.animatorSet.isRunning()) {
            this.animatorSet.removeAllListeners();
            this.animatorSet.cancel();
        }
        this.started = false;
    }

    public void setFillColor(int color) {
        this.fillColor = color;
        this.fillEnabled = true;
    }

    public void setStrokeColor(int color) {
        this.strokeColor = color;
    }

    public boolean isStarted() {
        return this.started;
    }
}
