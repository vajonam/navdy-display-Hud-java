package com.navdy.hud.app.ui.component.homescreen;

public class OpenRoadView extends android.widget.RelativeLayout {
    private com.squareup.otto.Bus bus;
    private com.navdy.hud.app.ui.component.homescreen.HomeScreenView homeScreenView;
    private com.navdy.service.library.log.Logger logger;
    @butterknife.InjectView(2131624350)
    android.widget.TextView openMapRoadInfo;

    public OpenRoadView(android.content.Context context) {
        this(context, null);
    }

    public OpenRoadView(android.content.Context context, android.util.AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public OpenRoadView(android.content.Context context, android.util.AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        this.logger = com.navdy.hud.app.ui.component.homescreen.HomeScreenView.sLogger;
        super.onFinishInflate();
        butterknife.ButterKnife.inject((android.view.View) this);
        this.bus = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getBus();
    }

    public void init(com.navdy.hud.app.ui.component.homescreen.HomeScreenView homeScreenView2) {
        this.homeScreenView = homeScreenView2;
        this.bus.register(this);
    }

    @com.squareup.otto.Subscribe
    public void onManeuverDisplay(com.navdy.hud.app.maps.MapEvents.ManeuverDisplay event) {
        setRoad(event.currentRoad);
    }

    public void setView(com.navdy.hud.app.view.MainView.CustomAnimationMode mode) {
        android.view.ViewGroup.MarginLayoutParams margins = (android.view.ViewGroup.MarginLayoutParams) this.openMapRoadInfo.getLayoutParams();
        switch (mode) {
            case EXPAND:
                margins.leftMargin = com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.openMapRoadInfoMargin;
                margins.rightMargin = com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.openMapRoadInfoMargin;
                return;
            case SHRINK_LEFT:
                margins.leftMargin = com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.openMapRoadInfoShrinkLeft_L_Margin;
                margins.rightMargin = com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.openMapRoadInfoShrinkLeft_R_Margin;
                return;
            default:
                return;
        }
    }

    public void getCustomAnimator(com.navdy.hud.app.view.MainView.CustomAnimationMode mode, android.animation.AnimatorSet.Builder builder) {
        switch (mode) {
            case EXPAND:
                builder.with(com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getMarginAnimator(this.openMapRoadInfo, com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.openMapRoadInfoMargin, com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.openMapRoadInfoMargin));
                return;
            case SHRINK_LEFT:
                builder.with(com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getMarginAnimator(this.openMapRoadInfo, com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.openMapRoadInfoShrinkLeft_L_Margin, com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.openMapRoadInfoShrinkLeft_R_Margin));
                return;
            default:
                return;
        }
    }

    public void setRoad() {
        setRoad(com.navdy.hud.app.maps.here.HereMapUtil.getCurrentRoadName());
    }

    private void setRoad(java.lang.String str) {
        if (str == null) {
            str = "";
        }
        this.openMapRoadInfo.setText(str);
    }
}
