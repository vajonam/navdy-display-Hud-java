package com.navdy.hud.app.ui.component;

public class ShrinkingBorderView extends android.view.View {
    private static final int FORCE_COLLAPSE_INTERVAL = 300;
    private static final int RESET_TIMEOUT_INITIAL_INTERVAL = 300;
    private static final int RESET_TIMEOUT_INTERVAL = 100;
    /* access modifiers changed from: private */
    public android.animation.ValueAnimator animator;
    private android.animation.Animator.AnimatorListener animatorListener;
    private android.animation.Animator.AnimatorListener animatorResetListener;
    private android.animation.ValueAnimator.AnimatorUpdateListener animatorResetUpdateListener;
    private android.animation.ValueAnimator.AnimatorUpdateListener animatorUpdateListener;
    /* access modifiers changed from: private */
    public java.lang.Runnable forceCompleteCallback;
    private android.animation.Animator.AnimatorListener forceCompleteListener;
    private android.view.animation.Interpolator interpolator;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.ui.component.ShrinkingBorderView.IListener listener;
    private android.view.animation.Interpolator restoreInterpolator;
    /* access modifiers changed from: private */
    public int timeoutVal;

    class Anon1 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
        Anon1() {
        }

        public void onAnimationEnd(android.animation.Animator animation) {
            com.navdy.hud.app.ui.component.ShrinkingBorderView.this.animator = null;
            if (com.navdy.hud.app.ui.component.ShrinkingBorderView.this.forceCompleteCallback != null) {
                com.navdy.hud.app.ui.component.ShrinkingBorderView.this.forceCompleteCallback.run();
                com.navdy.hud.app.ui.component.ShrinkingBorderView.this.forceCompleteCallback = null;
            }
        }
    }

    class Anon2 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
        Anon2() {
        }

        public void onAnimationEnd(android.animation.Animator animation) {
            com.navdy.hud.app.ui.component.ShrinkingBorderView.this.animator = null;
            if (com.navdy.hud.app.ui.component.ShrinkingBorderView.this.listener != null) {
                com.navdy.hud.app.ui.component.ShrinkingBorderView.this.listener.timeout();
            }
        }
    }

    class Anon3 implements android.animation.ValueAnimator.AnimatorUpdateListener {
        Anon3() {
        }

        public void onAnimationUpdate(android.animation.ValueAnimator animation) {
            android.view.ViewGroup.MarginLayoutParams lytParams = (android.view.ViewGroup.MarginLayoutParams) com.navdy.hud.app.ui.component.ShrinkingBorderView.this.getLayoutParams();
            int val = ((java.lang.Integer) animation.getAnimatedValue()).intValue();
            if (lytParams.height != val) {
                lytParams.height = val;
                com.navdy.hud.app.ui.component.ShrinkingBorderView.this.requestLayout();
            }
        }
    }

    class Anon4 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
        Anon4() {
        }

        public void onAnimationEnd(android.animation.Animator animation) {
            com.navdy.hud.app.ui.component.ShrinkingBorderView.this.animator = null;
            com.navdy.hud.app.ui.component.ShrinkingBorderView.this.startTimeout(com.navdy.hud.app.ui.component.ShrinkingBorderView.this.timeoutVal, true);
        }
    }

    class Anon5 implements android.animation.ValueAnimator.AnimatorUpdateListener {
        Anon5() {
        }

        public void onAnimationUpdate(android.animation.ValueAnimator animation) {
            ((android.view.ViewGroup.MarginLayoutParams) com.navdy.hud.app.ui.component.ShrinkingBorderView.this.getLayoutParams()).height = ((java.lang.Integer) animation.getAnimatedValue()).intValue();
            com.navdy.hud.app.ui.component.ShrinkingBorderView.this.requestLayout();
        }
    }

    public interface IListener {
        void timeout();
    }

    public ShrinkingBorderView(android.content.Context context) {
        this(context, null);
    }

    public ShrinkingBorderView(android.content.Context context, android.util.AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ShrinkingBorderView(android.content.Context context, android.util.AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.restoreInterpolator = new android.view.animation.LinearInterpolator();
        this.interpolator = new android.view.animation.AccelerateInterpolator();
        this.forceCompleteListener = new com.navdy.hud.app.ui.component.ShrinkingBorderView.Anon1();
        this.animatorListener = new com.navdy.hud.app.ui.component.ShrinkingBorderView.Anon2();
        this.animatorUpdateListener = new com.navdy.hud.app.ui.component.ShrinkingBorderView.Anon3();
        this.animatorResetListener = new com.navdy.hud.app.ui.component.ShrinkingBorderView.Anon4();
        this.animatorResetUpdateListener = new com.navdy.hud.app.ui.component.ShrinkingBorderView.Anon5();
    }

    public void setListener(com.navdy.hud.app.ui.component.ShrinkingBorderView.IListener listener2) {
        this.listener = listener2;
    }

    public void startTimeout(int timeout) {
        startTimeout(timeout, false);
    }

    /* access modifiers changed from: private */
    public void startTimeout(int timeout, boolean reset) {
        if (timeout != 0 && this.animator == null) {
            this.timeoutVal = timeout;
            if (reset) {
                this.animator = android.animation.ValueAnimator.ofInt(new int[]{((android.view.ViewGroup.MarginLayoutParams) getLayoutParams()).height, 0});
                this.animator.addUpdateListener(this.animatorUpdateListener);
                this.animator.addListener(this.animatorListener);
                this.animator.setDuration((long) timeout);
                this.animator.setInterpolator(this.interpolator);
                this.animator.start();
                return;
            }
            ((android.view.ViewGroup.MarginLayoutParams) getLayoutParams()).height = 0;
            requestLayout();
            resetTimeout(true);
        }
    }

    public void resetTimeout() {
        resetTimeout(false);
    }

    public void resetTimeout(boolean initial) {
        int interval;
        clearAnimator();
        if (initial) {
            interval = 300;
        } else {
            interval = 100;
        }
        android.view.ViewGroup.MarginLayoutParams lytParams = (android.view.ViewGroup.MarginLayoutParams) getLayoutParams();
        this.animator = android.animation.ValueAnimator.ofInt(new int[]{lytParams.height, ((android.view.ViewGroup) getParent()).getHeight()});
        this.animator.addUpdateListener(this.animatorResetUpdateListener);
        this.animator.addListener(this.animatorResetListener);
        this.animator.setDuration((long) interval);
        this.animator.setInterpolator(this.restoreInterpolator);
        if (initial) {
            this.animator.setStartDelay(300);
        }
        this.animator.start();
    }

    public void stopTimeout(boolean force, java.lang.Runnable forceCompleteCallback2) {
        if (force) {
            clearAnimator();
            if (forceCompleteCallback2 != null) {
                this.forceCompleteCallback = forceCompleteCallback2;
                this.animator = android.animation.ValueAnimator.ofInt(new int[]{((android.view.ViewGroup.MarginLayoutParams) getLayoutParams()).height, 0});
                this.animator.addUpdateListener(this.animatorResetUpdateListener);
                this.animator.addListener(this.forceCompleteListener);
                this.animator.setDuration(300);
                this.animator.setInterpolator(this.interpolator);
                this.animator.start();
                return;
            }
            ((android.view.ViewGroup.MarginLayoutParams) getLayoutParams()).height = 0;
            requestLayout();
        } else if (this.animator == null) {
            ((android.view.ViewGroup.MarginLayoutParams) getLayoutParams()).height = 0;
            requestLayout();
        }
    }

    public boolean isVisible() {
        if (((android.view.ViewGroup.MarginLayoutParams) getLayoutParams()).height == 0) {
            return false;
        }
        return true;
    }

    private void clearAnimator() {
        if (this.animator != null) {
            this.animator.removeAllListeners();
            this.animator.cancel();
            this.animator = null;
            this.forceCompleteCallback = null;
        }
    }
}
