package com.navdy.hud.app.ui.component.homescreen;

public class BaseSmartDashView extends android.widget.FrameLayout implements com.navdy.hud.app.manager.InputManager.IInputHandler, com.navdy.hud.app.gesture.GestureDetector.GestureListener, com.navdy.hud.app.ui.component.homescreen.IHomeScreenLifecycle {
    protected com.squareup.otto.Bus bus;
    private android.os.Handler handler;
    protected com.navdy.hud.app.ui.component.homescreen.HomeScreenView homeScreenView;
    private int lastObdSpeed;
    private com.navdy.service.library.log.Logger logger;
    private com.navdy.hud.app.manager.SpeedManager speedManager;
    protected com.navdy.hud.app.ui.framework.UIStateManager uiStateManager;

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            java.lang.String str;
            if (com.navdy.hud.app.manager.SpeedManager.getInstance().getObdSpeed() != -1) {
                str = com.navdy.hud.app.framework.voice.TTSUtils.DEBUG_TTS_USING_OBD_SPEED;
            } else {
                str = com.navdy.hud.app.framework.voice.TTSUtils.DEBUG_TTS_USING_GPS_SPEED;
            }
            com.navdy.hud.app.framework.voice.TTSUtils.debugShowSpeedInput(str);
        }
    }

    class Anon2 {
        Anon2() {
        }

        @com.squareup.otto.Subscribe
        public void GPSSpeedChangeEvent(com.navdy.hud.app.maps.MapEvents.GPSSpeedEvent event) {
            com.navdy.hud.app.ui.component.homescreen.BaseSmartDashView.this.updateSpeed(false);
        }

        @com.squareup.otto.Subscribe
        public void onSpeedDataExpired(com.navdy.hud.app.manager.SpeedManager.SpeedDataExpired speedDataExpired) {
            com.navdy.hud.app.ui.component.homescreen.BaseSmartDashView.this.updateSpeed(true);
        }

        @com.squareup.otto.Subscribe
        public void onMapEvent(com.navdy.hud.app.maps.MapEvents.ManeuverDisplay event) {
            com.navdy.hud.app.ui.component.homescreen.BaseSmartDashView.this.updateSpeedLimitInternal(event.currentSpeedLimit);
        }

        @com.squareup.otto.Subscribe
        public void onSpeedUnitChanged(com.navdy.hud.app.manager.SpeedManager.SpeedUnitChanged event) {
            com.navdy.hud.app.ui.component.homescreen.BaseSmartDashView.this.updateSpeed(true);
        }

        @com.squareup.otto.Subscribe
        public void ObdPidChangeEvent(com.navdy.hud.app.obd.ObdManager.ObdPidChangeEvent event) {
            com.navdy.hud.app.ui.component.homescreen.BaseSmartDashView.this.ObdPidChangeEvent(event);
        }
    }

    public BaseSmartDashView(android.content.Context context) {
        this(context, null);
    }

    public BaseSmartDashView(android.content.Context context, android.util.AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public BaseSmartDashView(android.content.Context context, android.util.AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.lastObdSpeed = -1;
        this.handler = new android.os.Handler();
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        this.logger = com.navdy.hud.app.ui.component.homescreen.HomeScreenView.sLogger;
        super.onFinishInflate();
        if (!isInEditMode()) {
            butterknife.ButterKnife.inject((android.view.View) this);
            this.speedManager = com.navdy.hud.app.manager.SpeedManager.getInstance();
            com.navdy.hud.app.manager.RemoteDeviceManager remoteDeviceManager = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance();
            this.bus = remoteDeviceManager.getBus();
            this.uiStateManager = remoteDeviceManager.getUiStateManager();
            initViews();
        }
    }

    private void initViews() {
        if (this.uiStateManager != null) {
            if (this.uiStateManager.getCurrentDashboardType() == null) {
                this.logger.v("setting initial dashtype = " + com.navdy.hud.app.ui.component.homescreen.SmartDashViewConstants.Type.Smart);
                this.uiStateManager.setCurrentDashboardType(com.navdy.hud.app.ui.component.homescreen.SmartDashViewConstants.Type.Smart);
            }
            setDashboard(this.uiStateManager.getCurrentDashboardType());
        }
        if (com.navdy.hud.app.framework.voice.TTSUtils.isDebugTTSEnabled()) {
            this.handler.postDelayed(new com.navdy.hud.app.ui.component.homescreen.BaseSmartDashView.Anon1(), 60000);
        }
    }

    public void setView(com.navdy.hud.app.view.MainView.CustomAnimationMode mode) {
        setMiddleGaugeView(mode);
    }

    public void setMiddleGaugeView(com.navdy.hud.app.view.MainView.CustomAnimationMode mode) {
    }

    public void setDashboard(com.navdy.hud.app.ui.component.homescreen.SmartDashViewConstants.Type dashBoardType) {
        switch (dashBoardType) {
            case Smart:
                this.uiStateManager.setCurrentDashboardType(dashBoardType);
                return;
            case Eco:
                this.uiStateManager.setCurrentDashboardType(dashBoardType);
                return;
            default:
                return;
        }
    }

    public com.navdy.hud.app.ui.component.homescreen.SmartDashViewConstants.Type getDashBoardType() {
        return this.uiStateManager.getCurrentDashboardType();
    }

    public void init(com.navdy.hud.app.ui.component.homescreen.HomeScreenView homeScreenView2) {
        this.homeScreenView = homeScreenView2;
        initializeView();
        this.bus.register(new com.navdy.hud.app.ui.component.homescreen.BaseSmartDashView.Anon2());
    }

    /* access modifiers changed from: protected */
    public void initializeView() {
        updateSpeedLimitInternal(com.navdy.hud.app.maps.here.HereMapUtil.getCurrentSpeedLimit());
        updateSpeed(true);
    }

    /* access modifiers changed from: protected */
    public void adjustDashHeight(int height) {
        ((android.view.ViewGroup.MarginLayoutParams) getLayoutParams()).height = height;
        invalidate();
        requestLayout();
        if (height == com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.openMapHeight && this.homeScreenView.getDisplayMode() == com.navdy.hud.app.ui.component.homescreen.HomeScreen.DisplayMode.SMART_DASH) {
            animateToFullMode();
        }
    }

    public void updateLayoutForMode(com.navdy.hud.app.maps.NavigationMode navigationMode, boolean forced) {
        boolean navigationActive = this.homeScreenView.isNavigationActive();
        if (forced) {
            adjustDashHeight(navigationActive ? com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeMapHeight : com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.openMapHeight);
        }
    }

    public void getCustomAnimator(com.navdy.hud.app.view.MainView.CustomAnimationMode mode, android.animation.AnimatorSet.Builder mainBuilder) {
    }

    public void getTopAnimator(android.animation.AnimatorSet.Builder builder, boolean out) {
    }

    public void resetTopViewsAnimator() {
        com.navdy.hud.app.view.MainView.CustomAnimationMode mode = this.uiStateManager.getCustomAnimateMode();
        this.logger.v("mode-view:" + mode);
        setView(mode);
    }

    public void onClick() {
    }

    public void onTrackHand(float normalized) {
    }

    public boolean onGesture(com.navdy.service.library.events.input.GestureEvent event) {
        return false;
    }

    public boolean onKey(com.navdy.hud.app.manager.InputManager.CustomKeyEvent event) {
        return false;
    }

    public com.navdy.hud.app.manager.InputManager.IInputHandler nextHandler() {
        return null;
    }

    /* access modifiers changed from: private */
    public void updateSpeedLimitInternal(float speedLimitMetersPerSec) {
        updateSpeedLimit(java.lang.Math.round((float) com.navdy.hud.app.manager.SpeedManager.convert((double) speedLimitMetersPerSec, com.navdy.hud.app.manager.SpeedManager.SpeedUnit.METERS_PER_SECOND, com.navdy.hud.app.manager.SpeedManager.getInstance().getSpeedUnit())));
    }

    /* access modifiers changed from: protected */
    public void updateSpeed(boolean force) {
        int speed = this.speedManager.getCurrentSpeed();
        if (speed == -1) {
            speed = 0;
        }
        boolean changed = false;
        if (force || speed != this.lastObdSpeed) {
            this.lastObdSpeed = speed;
            updateSpeed(speed);
            changed = true;
        }
        if (changed && this.logger.isLoggable(2)) {
            this.logger.v("SPEED updated :" + speed);
        }
    }

    @com.squareup.otto.Subscribe
    public void ObdStateChangeEvent(com.navdy.hud.app.obd.ObdManager.ObdConnectionStatusEvent event) {
        updateSpeed(true);
    }

    public void ObdPidChangeEvent(com.navdy.hud.app.obd.ObdManager.ObdPidChangeEvent event) {
        switch (event.pid.getId()) {
            case 13:
                updateSpeed(false);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void animateToFullMode() {
    }

    /* access modifiers changed from: protected */
    public void animateToFullModeInternal(android.animation.AnimatorSet.Builder builder) {
    }

    public boolean shouldShowTopViews() {
        return true;
    }

    /* access modifiers changed from: protected */
    public void updateSpeed(int speed) {
    }

    /* access modifiers changed from: protected */
    public void updateSpeedLimit(int speedLimit) {
    }

    public void onPause() {
    }

    public void onResume() {
    }
}
