package com.navdy.hud.app.ui.component.homescreen;

public class HomeScreenConstants {
    public static final java.lang.String EMPTY = "";
    public static final int INITIAL_ROUTE_TIMEOUT = 5000;
    public static final int MINUTES_DAY = 1440;
    public static final int MINUTES_HOUR = 60;
    public static final int MIN_RENAVIGATION_DISTANCE_METERS = 500;
    public static final double MIN_RENAVIGATION_DISTANCE_METERS_GAS = 500.0d;
    public static final int SELECTION_ROUTE_TIMEOUT = 10000;
    public static final java.lang.String SPACE = " ";
    public static final java.lang.String SPEED_KM;
    public static final java.lang.String SPEED_METERS;
    public static final java.lang.String SPEED_MPH;
    public static final int TOP_ANIMATION_INITIAL_OUT_INTERVAL = 30000;
    public static final int TOP_ANIMATION_OUT_INTERVAL = 10000;
    public static final com.here.android.mpa.common.ViewRect routeOverviewRect = new com.here.android.mpa.common.ViewRect(com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.routeOverviewBoxX, com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.routeOverviewBoxY, com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.routeOverviewBoxWidth, com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.routeOverviewBoxHeight);
    public static final com.here.android.mpa.common.ViewRect routePickerViewRect = new com.here.android.mpa.common.ViewRect(com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.routePickerBoxX, com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.routePickerBoxY, com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.routePickerBoxWidth, com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.routePickerBoxHeight);
    public static final android.graphics.PointF transformCenterLargeBottom = new android.graphics.PointF((float) (com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.transformCenterX + (com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.transformCenterIconWidth / 2)), (float) com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.transformCenterOverviewRouteY);
    public static final android.graphics.PointF transformCenterLargeMiddle = new android.graphics.PointF((float) com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.transformCenterOverviewX, (float) com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.transformCenterOverviewY);
    public static final android.graphics.PointF transformCenterPicker = new android.graphics.PointF((float) com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.transformCenterOverviewX, (float) com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.transformCenterPickerY);
    public static final android.graphics.PointF transformCenterSmallBottom = new android.graphics.PointF((float) (com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.transformCenterX + (com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.transformCenterIconWidth / 2)), (float) com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.transformCenterMapOnRouteY);
    public static final android.graphics.PointF transformCenterSmallMiddle = new android.graphics.PointF((float) com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.transformCenterOverviewX, (float) com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.transformCenterMapOnRouteOverviewY);

    static {
        android.content.res.Resources resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        SPEED_MPH = resources.getString(com.navdy.hud.app.R.string.miles_per_hour);
        SPEED_KM = resources.getString(com.navdy.hud.app.R.string.kilometers_per_hour);
        SPEED_METERS = resources.getString(com.navdy.hud.app.R.string.meters_per_second);
    }
}
