package com.navdy.hud.app.ui.component.vmenu;

public class VerticalAnimationUtils {

    public static class ActionRunnable implements java.lang.Runnable {
        private android.os.Bundle args;
        private com.squareup.otto.Bus bus;
        private boolean ignoreAnimation;
        private com.navdy.service.library.events.ui.Screen screen;

        public ActionRunnable(com.squareup.otto.Bus bus2, com.navdy.service.library.events.ui.Screen screen2) {
            this(bus2, screen2, null, false);
        }

        public ActionRunnable(com.squareup.otto.Bus bus2, com.navdy.service.library.events.ui.Screen screen2, android.os.Bundle args2, boolean ignoreAnimation2) {
            this.bus = bus2;
            this.screen = screen2;
            this.args = args2;
            this.ignoreAnimation = ignoreAnimation2;
        }

        public void run() {
            if (this.args == null) {
                this.bus.post(new com.navdy.service.library.events.ui.ShowScreen.Builder().screen(this.screen).build());
            } else {
                this.bus.post(new com.navdy.hud.app.event.ShowScreenWithArgs(this.screen, this.args, this.ignoreAnimation));
            }
        }
    }

    static class Anon1 implements java.lang.Runnable {
        final /* synthetic */ boolean val$clickup;
        final /* synthetic */ int val$duration;
        final /* synthetic */ java.lang.Runnable val$endAction;
        final /* synthetic */ android.view.View val$view;

        Anon1(boolean z, android.view.View view, int i, java.lang.Runnable runnable) {
            this.val$clickup = z;
            this.val$view = view;
            this.val$duration = i;
            this.val$endAction = runnable;
        }

        public void run() {
            if (this.val$clickup) {
                com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils.performClickUp(this.val$view, this.val$duration, this.val$endAction);
            } else if (this.val$endAction != null) {
                this.val$endAction.run();
            }
        }
    }

    static class Anon2 implements java.lang.Runnable {
        final /* synthetic */ java.lang.Runnable val$endAction;

        Anon2(java.lang.Runnable runnable) {
            this.val$endAction = runnable;
        }

        public void run() {
            if (this.val$endAction != null) {
                this.val$endAction.run();
            }
        }
    }

    static class Anon3 implements android.animation.ValueAnimator.AnimatorUpdateListener {
        final /* synthetic */ android.view.ViewGroup.MarginLayoutParams val$layoutParams;
        final /* synthetic */ android.view.View val$view;

        Anon3(android.view.ViewGroup.MarginLayoutParams marginLayoutParams, android.view.View view) {
            this.val$layoutParams = marginLayoutParams;
            this.val$view = view;
        }

        public void onAnimationUpdate(android.animation.ValueAnimator animation) {
            this.val$layoutParams.topMargin = ((java.lang.Integer) animation.getAnimatedValue()).intValue();
            this.val$view.requestLayout();
        }
    }

    static class Anon4 implements android.animation.ValueAnimator.AnimatorUpdateListener {
        final /* synthetic */ android.view.ViewGroup.MarginLayoutParams val$layoutParams;
        final /* synthetic */ android.view.View val$view;

        Anon4(android.view.ViewGroup.MarginLayoutParams marginLayoutParams, android.view.View view) {
            this.val$layoutParams = marginLayoutParams;
            this.val$view = view;
        }

        public void onAnimationUpdate(android.animation.ValueAnimator animation) {
            int size = ((java.lang.Integer) animation.getAnimatedValue()).intValue();
            this.val$layoutParams.width = size;
            this.val$layoutParams.height = size;
            this.val$view.requestLayout();
        }
    }

    public static void performClick(android.view.View view, int duration, java.lang.Runnable endAction) {
        performClickDown(view, duration / 2, endAction, true);
    }

    public static void performClickDown(android.view.View view, int duration, java.lang.Runnable endAction, boolean clickup) {
        view.animate().scaleX(0.8f).scaleY(0.8f).setDuration((long) duration).withEndAction(new com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils.Anon1(clickup, view, duration, endAction));
    }

    public static void performClickUp(android.view.View view, int duration, java.lang.Runnable endAction) {
        view.animate().scaleX(1.0f).scaleY(1.0f).setDuration((long) duration).withEndAction(new com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils.Anon2(endAction)).start();
    }

    public static void copyImage(android.widget.ImageView from, android.widget.ImageView to) {
        android.graphics.Bitmap bitmap = getBitmapFromImageView(from);
        if (bitmap != null) {
            to.setImageBitmap(bitmap);
        }
    }

    public static android.graphics.Bitmap getBitmapFromImageView(android.widget.ImageView imageView) {
        android.graphics.drawable.Drawable drawable = imageView.getDrawable();
        android.graphics.drawable.BitmapDrawable bitmapDrawable = null;
        if (drawable instanceof android.graphics.drawable.BitmapDrawable) {
            bitmapDrawable = (android.graphics.drawable.BitmapDrawable) drawable;
        }
        if (!(imageView instanceof com.navdy.hud.app.ui.component.image.IconColorImageView) && bitmapDrawable != null) {
            return bitmapDrawable.getBitmap();
        }
        android.graphics.Bitmap bitmap = android.graphics.Bitmap.createBitmap(imageView.getWidth(), imageView.getHeight(), android.graphics.Bitmap.Config.ARGB_8888);
        imageView.draw(new android.graphics.Canvas(bitmap));
        return bitmap;
    }

    public static android.animation.Animator animateMargin(android.view.View view, int toMargin) {
        android.view.ViewGroup.MarginLayoutParams layoutParams = (android.view.ViewGroup.MarginLayoutParams) view.getLayoutParams();
        android.animation.ValueAnimator marginAnimator = android.animation.ValueAnimator.ofInt(new int[]{layoutParams.topMargin, toMargin});
        marginAnimator.addUpdateListener(new com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils.Anon3(layoutParams, view));
        return marginAnimator;
    }

    public static android.animation.Animator animateDimension(android.view.View view, int toDimension) {
        android.view.ViewGroup.MarginLayoutParams layoutParams = (android.view.ViewGroup.MarginLayoutParams) view.getLayoutParams();
        android.animation.ValueAnimator dimensionAnimator = android.animation.ValueAnimator.ofInt(new int[]{layoutParams.width, toDimension});
        dimensionAnimator.addUpdateListener(new com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils.Anon4(layoutParams, view));
        return dimensionAnimator;
    }
}
