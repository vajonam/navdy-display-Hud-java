package com.navdy.hud.app.ui.component.mainmenu;

public class ActiveTripMenu implements com.navdy.hud.app.ui.component.mainmenu.IMenu {
    private static final int ARRIVAL_EXPAND = 100;
    private static final int MIN_MANEUVER_DISTANCE = 100;
    private static final int TITLE_UPDATE_INTERVAL = 1000;
    private static final java.lang.String TOWARDS_PATTERN = (com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.TOWARDS + " ");
    private static final java.lang.String activeTripTitle = resources.getString(com.navdy.hud.app.R.string.mm_active_trip);
    private static final java.lang.String arriveTitle = resources.getString(com.navdy.hud.app.R.string.mm_active_trip_arrive);
    private static final java.lang.String arrivedDistance = resources.getString(com.navdy.hud.app.R.string.mm_active_trip_arrived_dist);
    private static final java.lang.String arrivedTitle = resources.getString(com.navdy.hud.app.R.string.mm_active_trip_arrived_no_label);
    private static final com.navdy.hud.app.ui.component.vlist.VerticalList.Model back;
    private static final com.navdy.hud.app.ui.component.vlist.VerticalList.Model endTrip;
    private static final java.lang.String feetLabel = resources.getString(com.navdy.hud.app.R.string.unit_feet);
    private static final android.os.Handler handler = new android.os.Handler(android.os.Looper.getMainLooper());
    private static final com.navdy.hud.app.ui.component.vlist.VerticalList.Model infoModel = com.navdy.hud.app.ui.component.vlist.viewholder.ScrollableViewHolder.buildModel(com.navdy.hud.app.R.layout.active_trip_menu_lyt);
    private static final java.lang.String kiloMetersLabel = resources.getString(com.navdy.hud.app.R.string.unit_kilometers);
    public static final int maneuverIconSize = resources.getDimensionPixelSize(com.navdy.hud.app.R.dimen.active_trip_maneuver_icon_size);
    private static final java.lang.String metersLabel = resources.getString(com.navdy.hud.app.R.string.unit_meters);
    private static final java.lang.String milesLabel = resources.getString(com.navdy.hud.app.R.string.unit_miles);
    private static final com.navdy.hud.app.ui.component.vlist.VerticalList.Model muteTbtAudio;
    private static final com.navdy.hud.app.ui.component.vlist.VerticalList.Model reportIssue;
    private static final android.content.res.Resources resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.mainmenu.ActiveTripMenu.class);
    private static final int size_18 = resources.getDimensionPixelSize(com.navdy.hud.app.R.dimen.active_trip_18);
    private static final int size_22 = resources.getDimensionPixelSize(com.navdy.hud.app.R.dimen.active_trip_22);
    private static final java.lang.StringBuilder timeBuilder = new java.lang.StringBuilder();
    private static final com.navdy.hud.app.common.TimeHelper timeHelper = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getTimeHelper();
    private static final com.navdy.hud.app.ui.component.vlist.VerticalList.Model tripManeuvers = com.navdy.hud.app.ui.component.vlist.viewholder.TitleViewHolder.buildModel(resources.getString(com.navdy.hud.app.R.string.trip_maneuvers));
    private static final int tripOptionsColor = resources.getColor(com.navdy.hud.app.R.color.mm_active_trip);
    private static final java.util.HashSet<java.lang.String> turnNotShown = new java.util.HashSet<>();
    private static final com.navdy.hud.app.ui.component.vlist.VerticalList.Model unmuteTbtAudio;
    private android.widget.TextView activeTripDistance;
    private android.widget.TextView activeTripDuration;
    private android.widget.TextView activeTripEta;
    private android.view.View activeTripProgress;
    private android.widget.TextView activeTripSubTitleView;
    private android.widget.TextView activeTripTitleView;
    private boolean arrived;
    private int backSelection;
    private com.squareup.otto.Bus bus;
    private java.util.List<com.navdy.hud.app.ui.component.vlist.VerticalList.Model> cachedList;
    private java.util.List<com.navdy.hud.app.framework.contacts.Contact> contactList;
    private com.navdy.hud.app.ui.component.mainmenu.ContactOptionsMenu contactOptionsMenu;
    private int currentSelection = -1;
    private com.navdy.hud.app.view.drawable.ETAProgressDrawable etaProgressDrawable;
    /* access modifiers changed from: private */
    public boolean getItemsCalled;
    /* access modifiers changed from: private */
    public java.lang.String label;
    private boolean maneuverSelected;
    private com.navdy.hud.app.ui.component.mainmenu.IMenu parent;
    /* access modifiers changed from: private */
    public com.navdy.service.library.events.contacts.PhoneNumber phoneNumber;
    private boolean positionOnFirstManeuver;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.Presenter presenter;
    private boolean registered;
    private com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu reportIssueMenu;
    private com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent vscrollComponent;

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            if (com.navdy.hud.app.ui.component.mainmenu.ActiveTripMenu.this.getItemsCalled && com.navdy.hud.app.ui.component.mainmenu.ActiveTripMenu.this.presenter.isMapShown()) {
                com.navdy.hud.app.ui.component.mainmenu.ActiveTripMenu.this.presenter.setViewBackgroundColor(0);
                com.navdy.hud.app.ui.component.mainmenu.ActiveTripMenu.this.presenter.cleanMapFluctuator();
            }
        }
    }

    class Anon2 implements java.lang.Runnable {

        class Anon1 implements java.lang.Runnable {
            Anon1() {
            }

            public void run() {
                com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().stopAndRemoveAllRoutes();
            }
        }

        Anon2() {
        }

        public void run() {
            com.navdy.hud.app.ui.component.mainmenu.ActiveTripMenu.this.presenter.close();
            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.ui.component.mainmenu.ActiveTripMenu.Anon2.Anon1(), 3);
        }
    }

    class Anon3 implements java.lang.Runnable {
        Anon3() {
        }

        public void run() {
            com.navdy.hud.app.ui.component.mainmenu.ActiveTripMenu.this.presenter.close();
        }
    }

    class Anon4 implements java.lang.Runnable {
        Anon4() {
        }

        public void run() {
            com.navdy.hud.app.ui.component.mainmenu.ActiveTripMenu.this.presenter.close();
        }
    }

    class Anon5 implements java.lang.Runnable {

        class Anon1 implements java.lang.Runnable {
            Anon1() {
            }

            public void run() {
                if (com.navdy.hud.app.ui.component.mainmenu.ActiveTripMenu.this.phoneNumber != null) {
                    com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getCallManager().dial(com.navdy.hud.app.ui.component.mainmenu.ActiveTripMenu.this.phoneNumber.number, null, com.navdy.hud.app.ui.component.mainmenu.ActiveTripMenu.this.label);
                }
            }
        }

        Anon5() {
        }

        public void run() {
            com.navdy.hud.app.ui.component.mainmenu.ActiveTripMenu.this.presenter.close(new com.navdy.hud.app.ui.component.mainmenu.ActiveTripMenu.Anon5.Anon1());
        }
    }

    static {
        int backColor = resources.getColor(com.navdy.hud.app.R.color.mm_back);
        back = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.menu_back, com.navdy.hud.app.R.drawable.icon_mm_back, backColor, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, backColor, resources.getString(com.navdy.hud.app.R.string.back), null);
        java.lang.String title = resources.getString(com.navdy.hud.app.R.string.end_trip);
        int fluctuatorColor = resources.getColor(com.navdy.hud.app.R.color.mm_options_end_trip);
        endTrip = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.main_menu_options_end_trip, com.navdy.hud.app.R.drawable.icon_active_nav_end_2, fluctuatorColor, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, fluctuatorColor, title, null);
        java.lang.String title2 = resources.getString(com.navdy.hud.app.R.string.mute_tbt_2);
        int fluctuatorColor2 = resources.getColor(com.navdy.hud.app.R.color.mm_options_mute_tbt_audio);
        muteTbtAudio = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.main_menu_options_mute_tbt, com.navdy.hud.app.R.drawable.icon_active_nav_mute_2, fluctuatorColor2, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, fluctuatorColor2, title2, null);
        java.lang.String title3 = resources.getString(com.navdy.hud.app.R.string.unmute_tbt_2);
        int fluctuatorColor3 = resources.getColor(com.navdy.hud.app.R.color.mm_options_unmute_tbt_audio);
        unmuteTbtAudio = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.main_menu_options_unmute_tbt, com.navdy.hud.app.R.drawable.icon_active_nav_unmute_2, fluctuatorColor3, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, fluctuatorColor3, title3, null);
        java.lang.String title4 = resources.getString(com.navdy.hud.app.R.string.report_navigation_issue);
        int fluctuatorColor4 = resources.getColor(com.navdy.hud.app.R.color.mm_options_report_issue);
        reportIssue = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.main_menu_options_report_issue, com.navdy.hud.app.R.drawable.icon_options_report_issue_2, fluctuatorColor4, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, fluctuatorColor4, title4, null);
        tripManeuvers.id = com.navdy.hud.app.R.id.active_trip_maneuver_title;
        turnNotShown.add(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.START_TURN);
        turnNotShown.add(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.ARRIVED);
    }

    ActiveTripMenu(com.squareup.otto.Bus bus2, com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent vscrollComponent2, com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.Presenter presenter2, com.navdy.hud.app.ui.component.mainmenu.IMenu parent2) {
        this.bus = bus2;
        this.vscrollComponent = vscrollComponent2;
        this.presenter = presenter2;
        this.parent = parent2;
    }

    public java.util.List<com.navdy.hud.app.ui.component.vlist.VerticalList.Model> getItems() {
        int delay;
        this.getItemsCalled = true;
        this.phoneNumber = null;
        this.contactList = null;
        this.label = null;
        java.util.List<com.navdy.hud.app.ui.component.vlist.VerticalList.Model> list = new java.util.ArrayList<>();
        list.add(back);
        list.add(infoModel);
        com.navdy.hud.app.maps.here.HereNavigationManager hereNavigationManager = com.navdy.hud.app.maps.here.HereNavigationManager.getInstance();
        boolean navigationOn = hereNavigationManager.isNavigationModeOn();
        if (navigationOn) {
            com.navdy.service.library.events.navigation.NavigationRouteRequest routeRequest = hereNavigationManager.getCurrentNavigationRouteRequest();
            if (!(routeRequest == null || routeRequest.requestDestination == null)) {
                java.util.List<com.navdy.service.library.events.contacts.PhoneNumber> phoneNumbers = routeRequest.requestDestination.phoneNumbers;
                if (phoneNumbers != null && phoneNumbers.size() > 0) {
                    com.navdy.service.library.events.contacts.PhoneNumber firstNumber = (com.navdy.service.library.events.contacts.PhoneNumber) phoneNumbers.get(0);
                    int callColor = resources.getColor(com.navdy.hud.app.R.color.mm_places);
                    if (!android.text.TextUtils.isEmpty(routeRequest.label)) {
                        this.label = routeRequest.label;
                    } else if (!android.text.TextUtils.isEmpty(routeRequest.streetAddress)) {
                        this.label = routeRequest.streetAddress;
                    } else {
                        this.label = "";
                    }
                    list.add(com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.menu_call, com.navdy.hud.app.R.drawable.icon_mm_contacts_2, callColor, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, callColor, resources.getString(com.navdy.hud.app.R.string.call_person, new java.lang.Object[]{this.label}), com.navdy.hud.app.util.PhoneUtil.formatPhoneNumber(firstNumber.number)));
                    this.phoneNumber = firstNumber;
                } else if (routeRequest.requestDestination.contacts != null && routeRequest.requestDestination.contacts.size() > 0) {
                    this.contactList = new java.util.ArrayList(routeRequest.requestDestination.contacts.size());
                    for (com.navdy.service.library.events.contacts.Contact c : routeRequest.requestDestination.contacts) {
                        this.contactList.add(new com.navdy.hud.app.framework.contacts.Contact(c));
                    }
                    list.add(buildContactModel((com.navdy.hud.app.framework.contacts.Contact) this.contactList.get(0), this.contactList.size()));
                }
            }
            list.add(endTrip);
            if (!com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile().isDefaultProfile()) {
                if (com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().getNavigationSessionPreference().spokenTurnByTurn) {
                    muteTbtAudio.title = resources.getString(com.navdy.hud.app.R.string.mute_tbt_2);
                    list.add(muteTbtAudio);
                } else {
                    unmuteTbtAudio.title = resources.getString(com.navdy.hud.app.R.string.unmute_tbt_2);
                    list.add(unmuteTbtAudio);
                }
            }
        }
        if (navigationOn && com.navdy.hud.app.util.ReportIssueService.canReportIssue()) {
            list.add(reportIssue);
        }
        if (navigationOn && !com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().hasArrived()) {
            buildManeuvers(list);
        }
        this.presenter.showScrimCover();
        this.cachedList = list;
        sLogger.v("getItems:" + list.size());
        if (!this.presenter.isMapShown()) {
            this.presenter.showMap();
            delay = 1000;
        } else {
            this.presenter.enableMapViews();
            delay = 100;
        }
        handler.postDelayed(new com.navdy.hud.app.ui.component.mainmenu.ActiveTripMenu.Anon1(), (long) delay);
        return list;
    }

    public int getInitialSelection() {
        if (this.positionOnFirstManeuver) {
            this.positionOnFirstManeuver = false;
            if (this.cachedList != null) {
                int index = getFirstManeuverIndex();
                if (index > 0) {
                    sLogger.v("initialSelection-m:" + index);
                    return index;
                }
            }
        }
        if (this.currentSelection == -1) {
            return 1;
        }
        sLogger.v("initialSelection:" + this.currentSelection);
        int ret = this.currentSelection;
        this.currentSelection = -1;
        return ret;
    }

    public com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex getScrollIndex() {
        return null;
    }

    public void setBackSelectionPos(int n) {
        this.backSelection = n;
    }

    public void setBackSelectionId(int id) {
    }

    public void setSelectedIcon() {
        int color = tripOptionsColor;
        java.lang.String title = activeTripTitle;
        this.vscrollComponent.setSelectedIconColorImage(com.navdy.hud.app.R.drawable.icon_badge_active_trip, color, null, 1.0f);
        this.vscrollComponent.selectedText.setText(title);
    }

    public boolean selectItem(com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState selection) {
        sLogger.v("select id:" + selection.id + " pos:" + selection.pos);
        switch (selection.id) {
            case com.navdy.hud.app.R.id.active_trip_contact /*2131623936*/:
                sLogger.v("contact");
                if (this.contactOptionsMenu == null) {
                    this.contactOptionsMenu = new com.navdy.hud.app.ui.component.mainmenu.ContactOptionsMenu(this.contactList, this.vscrollComponent, this.presenter, this, this.bus);
                    this.contactOptionsMenu.setScrollModel(true);
                }
                this.presenter.loadMenu(this.contactOptionsMenu, com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel.SUB_LEVEL, selection.pos, 0);
                break;
            case com.navdy.hud.app.R.id.main_menu_options_end_trip /*2131623978*/:
                sLogger.v("end trip");
                if (this.presenter.isMapShown()) {
                    this.presenter.setNavEnded();
                    this.presenter.setViewBackgroundColor(-16777216);
                }
                com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("End_Trip");
                this.presenter.performSelectionAnimation(new com.navdy.hud.app.ui.component.mainmenu.ActiveTripMenu.Anon2());
                break;
            case com.navdy.hud.app.R.id.main_menu_options_mute_tbt /*2131623980*/:
                sLogger.v("mute tbt");
                if (this.presenter.isMapShown()) {
                    this.presenter.setViewBackgroundColor(-16777216);
                }
                com.navdy.hud.app.analytics.AnalyticsSupport.recordOptionSelection("mute_Turn_By_Turn");
                this.presenter.performSelectionAnimation(new com.navdy.hud.app.ui.component.mainmenu.ActiveTripMenu.Anon3(), 1000);
                muteTbtAudio.title = resources.getString(com.navdy.hud.app.R.string.navigation_muted);
                this.vscrollComponent.verticalList.unlock(false);
                this.presenter.refreshDataforPos(selection.pos, false);
                this.vscrollComponent.verticalList.lock();
                setSpokenTurnByTurn(false);
                break;
            case com.navdy.hud.app.R.id.main_menu_options_report_issue /*2131623984*/:
                sLogger.v("report-issue");
                com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("report-issue");
                if (this.reportIssueMenu == null) {
                    this.reportIssueMenu = new com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu(this.vscrollComponent, this.presenter, this);
                }
                this.presenter.loadMenu(this.reportIssueMenu, com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel.SUB_LEVEL, selection.pos, 0);
                break;
            case com.navdy.hud.app.R.id.main_menu_options_unmute_tbt /*2131623992*/:
                sLogger.v("unmute tbt");
                if (this.presenter.isMapShown()) {
                    this.presenter.setViewBackgroundColor(-16777216);
                }
                com.navdy.hud.app.analytics.AnalyticsSupport.recordOptionSelection("unmute_Turn_By_Turn");
                this.presenter.performSelectionAnimation(new com.navdy.hud.app.ui.component.mainmenu.ActiveTripMenu.Anon4(), 1000);
                unmuteTbtAudio.title = resources.getString(com.navdy.hud.app.R.string.navigation_unmuted);
                this.vscrollComponent.verticalList.unlock(false);
                this.presenter.refreshDataforPos(selection.pos, false);
                this.vscrollComponent.verticalList.lock();
                setSpokenTurnByTurn(true);
                break;
            case com.navdy.hud.app.R.id.menu_back /*2131624007*/:
                sLogger.v("back");
                if (this.presenter.isMapShown()) {
                    this.presenter.setViewBackgroundColor(-16777216);
                }
                com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("back");
                this.presenter.loadMenu(this.parent, com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel.BACK_TO_PARENT, this.backSelection, 0, false, com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder.getOffSetFromPos(((com.navdy.hud.app.ui.component.mainmenu.MainMenu) this.parent).getActivityTraySelection()));
                break;
            case com.navdy.hud.app.R.id.menu_call /*2131624008*/:
                sLogger.v("call place");
                if (this.presenter.isMapShown()) {
                    this.presenter.setViewBackgroundColor(-16777216);
                }
                this.presenter.performSelectionAnimation(new com.navdy.hud.app.ui.component.mainmenu.ActiveTripMenu.Anon5());
                break;
        }
        return true;
    }

    private void setSpokenTurnByTurn(boolean enabled) {
        com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().getNavigationSessionPreference().spokenTurnByTurn = enabled;
        com.navdy.hud.app.profile.DriverProfile userProfile = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile();
        if (!userProfile.isDefaultProfile()) {
            com.navdy.service.library.events.preferences.NavigationPreferences updatedPreferences = new com.navdy.service.library.events.preferences.NavigationPreferences.Builder(userProfile.getNavigationPreferences()).spokenTurnByTurn(java.lang.Boolean.valueOf(enabled)).build();
            com.navdy.service.library.events.preferences.NavigationPreferencesUpdate update = new com.navdy.service.library.events.preferences.NavigationPreferencesUpdate.Builder().status(com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS).serial_number(updatedPreferences.serial_number).preferences(updatedPreferences).build();
            this.bus.post(update);
            this.bus.post(new com.navdy.hud.app.event.RemoteEvent(update));
        }
    }

    public com.navdy.hud.app.ui.component.mainmenu.IMenu.Menu getType() {
        return com.navdy.hud.app.ui.component.mainmenu.IMenu.Menu.ACTIVE_TRIP;
    }

    public boolean isItemClickable(int id, int pos) {
        if (pos == 1) {
            return false;
        }
        com.navdy.hud.app.ui.component.vlist.VerticalList.Model m = getModelfromPos(pos);
        if (m != null) {
            switch (m.id) {
                case com.navdy.hud.app.R.id.active_trip_maneuver /*2131623937*/:
                case com.navdy.hud.app.R.id.active_trip_maneuver_title /*2131623938*/:
                    return false;
            }
        }
        return true;
    }

    public com.navdy.hud.app.ui.component.vlist.VerticalList.Model getModelfromPos(int pos) {
        if (this.cachedList == null || this.cachedList.size() <= pos) {
            return null;
        }
        return (com.navdy.hud.app.ui.component.vlist.VerticalList.Model) this.cachedList.get(pos);
    }

    public boolean isBindCallsEnabled() {
        return true;
    }

    public void onBindToView(com.navdy.hud.app.ui.component.vlist.VerticalList.Model model, android.view.View view, int pos, com.navdy.hud.app.ui.component.vlist.VerticalList.ModelState state) {
        if (model.type == com.navdy.hud.app.ui.component.vlist.VerticalList.ModelType.SCROLL_CONTENT) {
            android.view.ViewGroup viewGroup = (android.view.ViewGroup) ((android.view.ViewGroup) view).getChildAt(0);
            this.activeTripTitleView = (android.widget.TextView) viewGroup.findViewById(com.navdy.hud.app.R.id.active_trip_title);
            this.activeTripSubTitleView = (android.widget.TextView) viewGroup.findViewById(com.navdy.hud.app.R.id.active_trip_subtitle);
            this.activeTripDuration = (android.widget.TextView) viewGroup.findViewById(com.navdy.hud.app.R.id.active_trip_duration);
            this.activeTripDistance = (android.widget.TextView) viewGroup.findViewById(com.navdy.hud.app.R.id.active_trip_distance);
            this.activeTripEta = (android.widget.TextView) viewGroup.findViewById(com.navdy.hud.app.R.id.active_trip_eta);
            this.activeTripProgress = viewGroup.findViewById(com.navdy.hud.app.R.id.active_trip_progress);
            this.etaProgressDrawable = new com.navdy.hud.app.view.drawable.ETAProgressDrawable(this.activeTripProgress.getContext());
            this.etaProgressDrawable.setMinValue(0.0f);
            this.etaProgressDrawable.setMaxGaugeValue(100.0f);
            this.activeTripProgress.setBackground(this.etaProgressDrawable);
            update();
            if (!this.registered) {
                this.bus.register(this);
                this.registered = true;
                sLogger.v("registered bus");
            }
            com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().postManeuverDisplay();
        }
    }

    public com.navdy.hud.app.ui.component.mainmenu.IMenu getChildMenu(com.navdy.hud.app.ui.component.mainmenu.IMenu parent2, java.lang.String args, java.lang.String path) {
        return null;
    }

    public void onUnload(com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel level) {
        this.getItemsCalled = false;
        this.presenter.hideScrimCover();
        if (this.presenter.isMapShown()) {
            this.presenter.setViewBackgroundColor(-16777216);
            this.presenter.disableMapViews();
        }
        if (this.registered) {
            this.registered = false;
            this.bus.unregister(this);
            sLogger.v("unregistered bus");
        }
        this.activeTripTitleView = null;
        this.activeTripSubTitleView = null;
        this.activeTripDuration = null;
        this.activeTripDistance = null;
        this.activeTripEta = null;
        this.activeTripProgress = null;
    }

    public void onItemSelected(com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState selection) {
        if (selection.id == com.navdy.hud.app.R.id.active_trip_maneuver) {
            this.maneuverSelected = true;
            com.navdy.hud.app.ui.component.vlist.VerticalList.Model model = getModelfromPos(selection.pos);
            if (model == null || model.id != com.navdy.hud.app.R.id.active_trip_maneuver || !(model.state instanceof com.here.android.mpa.common.GeoBoundingBox)) {
                sLogger.v("leg - no bbox");
            } else {
                this.presenter.showBoundingBox((com.here.android.mpa.common.GeoBoundingBox) model.state);
            }
        } else if (this.maneuverSelected) {
            this.maneuverSelected = false;
            this.presenter.showBoundingBox(this.presenter.getCurrentRouteBoundingBox());
            sLogger.v("route bbox");
        }
    }

    public void onScrollIdle() {
    }

    public void onFastScrollStart() {
    }

    public void onFastScrollEnd() {
    }

    public void showToolTip() {
    }

    public boolean isFirstItemEmpty() {
        return true;
    }

    private void update() {
        if (isMenuLoaded()) {
            com.navdy.hud.app.maps.here.HereNavigationManager hereNavigationManager = com.navdy.hud.app.maps.here.HereNavigationManager.getInstance();
            java.lang.String label2 = hereNavigationManager.getDestinationLabel();
            java.lang.String address = hereNavigationManager.getFullStreetAddress();
            if (android.text.TextUtils.isEmpty(label2)) {
                label2 = address;
                if (android.text.TextUtils.isEmpty(label2)) {
                    label2 = null;
                } else {
                    address = null;
                }
            }
            if (label2 != null) {
                this.activeTripTitleView.setText(label2);
                this.activeTripTitleView.setVisibility(0);
            } else {
                this.activeTripTitleView.setVisibility(8);
            }
            if (address != null) {
                this.activeTripSubTitleView.setText(address);
                this.activeTripSubTitleView.setVisibility(0);
                this.activeTripSubTitleView.requestLayout();
            } else {
                this.activeTripSubTitleView.setVisibility(8);
            }
            if (hereNavigationManager.hasArrived()) {
                switchToArriveState();
            }
        }
    }

    private boolean isMenuLoaded() {
        return this.activeTripTitleView != null;
    }

    private void fillDurationVia(java.util.Date etaDate) {
        java.lang.String duration = com.navdy.hud.app.maps.here.HereMapUtil.convertDateToTta(etaDate);
        java.lang.String via = com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().getCurrentVia();
        if (duration != null) {
            android.text.SpannableStringBuilder durationVia = new android.text.SpannableStringBuilder();
            durationVia.append(duration);
            int len = durationVia.length();
            durationVia.setSpan(new android.text.style.StyleSpan(1), 0, len, 34);
            durationVia.setSpan(new android.text.style.AbsoluteSizeSpan(size_22), 0, len, 34);
            if (via != null) {
                durationVia.append(" ");
                int start = duration.length();
                durationVia.append(resources.getString(com.navdy.hud.app.R.string.via_desc, new java.lang.Object[]{via}));
                durationVia.setSpan(new android.text.style.AbsoluteSizeSpan(size_18), start, durationVia.length(), 34);
            }
            this.activeTripDuration.setText(durationVia);
            return;
        }
        this.activeTripDuration.setText("");
    }

    @com.squareup.otto.Subscribe
    public void onManeuverDisplayEvent(com.navdy.hud.app.maps.MapEvents.ManeuverDisplay maneuverDisplay) {
        java.lang.String eta;
        int percentage;
        if (isMenuLoaded() && !maneuverDisplay.isEmpty() && maneuverDisplay.isNavigating()) {
            if (com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().hasArrived()) {
                if (this.arrived) {
                    sLogger.v("already arrived");
                    return;
                }
                this.arrived = true;
                this.currentSelection = -1;
                this.presenter.loadMenu(this, com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel.REFRESH_CURRENT, 0, 0, true);
            } else if (maneuverDisplay.etaDate != null) {
                fillDurationVia(maneuverDisplay.etaDate);
                if (timeHelper.getFormat() == com.navdy.service.library.events.settings.DateTimeConfiguration.Clock.CLOCK_24_HOUR) {
                    eta = maneuverDisplay.eta;
                } else {
                    timeBuilder.setLength(0);
                    eta = timeHelper.formatTime12Hour(maneuverDisplay.etaDate, timeBuilder, false) + " " + timeBuilder.toString();
                }
                if (eta != null) {
                    this.activeTripEta.setText(eta);
                } else {
                    this.activeTripEta.setText("");
                }
                float totalDistanceInMeters = com.navdy.hud.app.maps.util.DistanceConverter.convertToMeters(maneuverDisplay.totalDistance, maneuverDisplay.totalDistanceUnit);
                float coveredDistanceInMeters = totalDistanceInMeters - com.navdy.hud.app.maps.util.DistanceConverter.convertToMeters(maneuverDisplay.totalDistanceRemaining, maneuverDisplay.totalDistanceRemainingUnit);
                if (totalDistanceInMeters <= 0.0f || totalDistanceInMeters <= coveredDistanceInMeters) {
                    percentage = 0;
                } else {
                    percentage = (int) ((coveredDistanceInMeters / totalDistanceInMeters) * 100.0f);
                }
                if (this.etaProgressDrawable != null) {
                    this.etaProgressDrawable.setGaugeValue((float) percentage);
                    this.activeTripProgress.invalidate();
                }
                java.lang.String unit = "";
                switch (maneuverDisplay.totalDistanceRemainingUnit) {
                    case DISTANCE_METERS:
                        unit = metersLabel;
                        break;
                    case DISTANCE_KMS:
                        unit = kiloMetersLabel;
                        break;
                    case DISTANCE_MILES:
                        unit = milesLabel;
                        break;
                    case DISTANCE_FEET:
                        unit = feetLabel;
                        break;
                }
                this.activeTripDistance.setText(java.lang.Float.toString(maneuverDisplay.totalDistanceRemaining) + " " + unit);
            }
        }
    }

    @com.squareup.otto.Subscribe
    public void onRouteChangeEvent(com.navdy.service.library.events.navigation.NavigationSessionRouteChange event) {
        int curPos = this.presenter.getCurrentSelection();
        sLogger.v("onRouteChangeEvent reason=" + event.reason + " curPos=" + curPos);
        if (curPos > 1) {
            int maneuverPos = getFirstManeuverIndex();
            if (curPos >= maneuverPos) {
                this.positionOnFirstManeuver = true;
                sLogger.v("onRouteChangeEvent showing maneuver curPos=" + curPos + " maneuverPos=" + maneuverPos);
            } else {
                sLogger.v("onRouteChangeEvent not showing maneuver curPos=" + curPos + " maneuverPos=" + maneuverPos);
            }
        }
        this.currentSelection = curPos;
        this.presenter.loadMenu(this, com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel.REFRESH_CURRENT, 0, 0, true);
    }

    @com.squareup.otto.Subscribe
    public void arrivalEvent(com.navdy.hud.app.maps.MapEvents.ArrivalEvent event) {
        if (this.arrived) {
            sLogger.v("arrival event");
            return;
        }
        this.arrived = true;
        this.currentSelection = -1;
        this.positionOnFirstManeuver = false;
        this.presenter.loadMenu(this, com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel.REFRESH_CURRENT, 0, 0, true);
    }

    @com.squareup.otto.Subscribe
    public void onManeuverChangeEvent(com.navdy.hud.app.maps.MapEvents.ManeuverEvent event) {
        sLogger.v("onManeuverChangeEvent:" + event.type);
        switch (event.type) {
            case INTERMEDIATE:
                int curPos = this.presenter.getCurrentSelection();
                if (curPos > 1 && curPos >= getFirstManeuverIndex()) {
                    this.positionOnFirstManeuver = true;
                }
                this.currentSelection = curPos;
                this.presenter.loadMenu(this, com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel.REFRESH_CURRENT, 0, 0, true);
                return;
            default:
                return;
        }
    }

    private void switchToArriveState() {
        if (!android.text.TextUtils.equals(this.activeTripDuration.getText(), arrivedTitle)) {
            android.text.SpannableStringBuilder durationVia = new android.text.SpannableStringBuilder();
            durationVia.append(arrivedTitle);
            int len = durationVia.length();
            durationVia.setSpan(new android.text.style.StyleSpan(1), 0, len, 34);
            durationVia.setSpan(new android.text.style.AbsoluteSizeSpan(size_22), 0, len, 34);
            this.activeTripDuration.setText(durationVia);
            this.activeTripDistance.setText(arrivedDistance);
            this.activeTripEta.setText("");
            this.etaProgressDrawable.setGaugeValue(100.0f);
        }
    }

    private void buildManeuvers(java.util.List<com.navdy.hud.app.ui.component.vlist.VerticalList.Model> list) {
        java.lang.String roadInfo;
        try {
            long l1 = android.os.SystemClock.elapsedRealtime();
            com.navdy.hud.app.maps.here.HereNavigationManager hereNavigationManager = com.navdy.hud.app.maps.here.HereNavigationManager.getInstance();
            com.here.android.mpa.routing.Route route = hereNavigationManager.getCurrentRoute();
            java.lang.String label2 = hereNavigationManager.getDestinationLabel();
            java.lang.String address = hereNavigationManager.getFullStreetAddress();
            if (android.text.TextUtils.isEmpty(label2)) {
                label2 = address;
                if (android.text.TextUtils.isEmpty(label2)) {
                    label2 = null;
                } else {
                    address = null;
                }
            }
            if (route != null) {
                java.util.ArrayList arrayList = new java.util.ArrayList();
                java.util.List<com.here.android.mpa.routing.Maneuver> allManeuvers = route.getManeuvers();
                int len = allManeuvers.size();
                com.here.android.mpa.routing.Maneuver current = null;
                com.here.android.mpa.routing.Maneuver currentNavigationManeuver = hereNavigationManager.getNavManeuver();
                if (currentNavigationManeuver == null) {
                    sLogger.v("current maneuver not found");
                }
                com.navdy.service.library.events.navigation.NavigationRouteRequest routeRequest = hereNavigationManager.getCurrentNavigationRouteRequest();
                boolean foundCurrent = false;
                list.add(tripManeuvers);
                for (int i = 0; i < len; i++) {
                    com.here.android.mpa.routing.Maneuver previous = current;
                    current = (com.here.android.mpa.routing.Maneuver) allManeuvers.get(i);
                    if (!com.navdy.hud.app.maps.here.HereMapUtil.isStartManeuver(current)) {
                        if (foundCurrent) {
                            arrayList.add(current);
                        } else if (currentNavigationManeuver == null) {
                            arrayList.add(current);
                        } else if (com.navdy.hud.app.maps.here.HereMapUtil.areManeuverEqual(currentNavigationManeuver, current)) {
                            arrayList.add(current);
                            foundCurrent = true;
                        }
                    }
                }
                int len2 = arrayList.size();
                com.here.android.mpa.routing.Maneuver current2 = null;
                for (int i2 = 0; i2 < len2; i2++) {
                    com.here.android.mpa.routing.Maneuver previous2 = current2;
                    current2 = (com.here.android.mpa.routing.Maneuver) arrayList.get(i2);
                    com.here.android.mpa.routing.Maneuver after = null;
                    if (i2 < len2 - 1) {
                        after = (com.here.android.mpa.routing.Maneuver) arrayList.get(i2 + 1);
                    }
                    com.navdy.hud.app.maps.MapEvents.ManeuverDisplay display = com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.getManeuverDisplay(current2, after == null, (long) current2.getDistanceToNextManeuver(), address, previous2, routeRequest, after, true, false, null);
                    if (com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.shouldShowTurnText(display.pendingTurn) && !turnNotShown.contains(display.pendingTurn)) {
                        roadInfo = display.pendingTurn + " " + display.pendingRoad;
                    } else if (display.pendingRoad.indexOf(TOWARDS_PATTERN) == 0) {
                        roadInfo = display.pendingRoad.substring(TOWARDS_PATTERN.length());
                    } else {
                        roadInfo = display.pendingRoad;
                    }
                    com.navdy.hud.app.ui.component.vlist.VerticalList.Model maneuverModel = com.navdy.hud.app.ui.component.vlist.viewholder.IconsTwoViewHolder.buildModel(com.navdy.hud.app.R.id.active_trip_maneuver, display.turnIconNowId, display.turnIconSoonId, 0, 0, roadInfo, display.distanceToPendingRoadText);
                    if (i2 == len2 - 1) {
                        maneuverModel.state = buildArrivalBoundingBox(routeRequest);
                    } else {
                        int distance = current2.getDistanceToNextManeuver();
                        if (distance < 100) {
                            maneuverModel.state = expandBoundingBox(current2.getBoundingBox(), 100 - distance);
                        } else {
                            maneuverModel.state = current2.getBoundingBox();
                        }
                    }
                    maneuverModel.noTextAnimation = true;
                    maneuverModel.noImageScaleAnimation = true;
                    if (display.navigationTurn == com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_END) {
                        maneuverModel.title = arriveTitle;
                        if (label2 != null && address != null) {
                            maneuverModel.title = arriveTitle;
                            maneuverModel.subTitle = label2;
                            maneuverModel.subTitle2 = address;
                        } else if (label2 != null) {
                            maneuverModel.title = arriveTitle;
                            maneuverModel.subTitle = label2;
                        } else if (address != null) {
                            maneuverModel.title = arriveTitle;
                            maneuverModel.subTitle = address;
                        } else {
                            maneuverModel.title = arriveTitle;
                        }
                    } else {
                        maneuverModel.iconSize = maneuverIconSize;
                    }
                    list.add(maneuverModel);
                }
            }
            sLogger.v("time to build maneuvers:" + (android.os.SystemClock.elapsedRealtime() - l1));
        } catch (Throwable t) {
            sLogger.e("addManeuversToList", t);
        }
    }

    private int getFirstManeuverIndex() {
        int index = this.cachedList.indexOf(reportIssue);
        if (index >= 0 && index + 2 < this.cachedList.size()) {
            return index + 2;
        }
        int index2 = this.cachedList.indexOf(unmuteTbtAudio);
        if (index2 >= 0 && index2 + 2 < this.cachedList.size()) {
            return index2 + 2;
        }
        int index3 = this.cachedList.indexOf(muteTbtAudio);
        if (index3 < 0 || index3 + 2 >= this.cachedList.size()) {
            return -1;
        }
        return index3 + 2;
    }

    private com.here.android.mpa.common.GeoBoundingBox buildArrivalBoundingBox(com.navdy.service.library.events.navigation.NavigationRouteRequest request) {
        com.here.android.mpa.common.GeoCoordinate coordinate = null;
        try {
            if (request.destination.latitude.doubleValue() != 0.0d && request.destination.longitude.doubleValue() != 0.0d) {
                coordinate = new com.here.android.mpa.common.GeoCoordinate(request.destination.latitude.doubleValue(), request.destination.longitude.doubleValue());
            } else if (!(request.destinationDisplay == null || request.destinationDisplay.latitude.doubleValue() == 0.0d || request.destinationDisplay.longitude.doubleValue() == 0.0d)) {
                coordinate = new com.here.android.mpa.common.GeoCoordinate(request.destinationDisplay.latitude.doubleValue(), request.destinationDisplay.longitude.doubleValue());
            }
            if (coordinate != null) {
                com.here.android.mpa.common.GeoBoundingBox bbox = new com.here.android.mpa.common.GeoBoundingBox(coordinate, coordinate);
                bbox.expand(100.0f, 100.0f);
                return bbox;
            }
        } catch (Throwable t) {
            sLogger.e(t);
        }
        return null;
    }

    private com.here.android.mpa.common.GeoBoundingBox expandBoundingBox(com.here.android.mpa.common.GeoBoundingBox bbox, int distance) {
        try {
            bbox.expand((float) distance, (float) distance);
        } catch (Throwable t) {
            sLogger.e(t);
        }
        return bbox;
    }

    /* access modifiers changed from: 0000 */
    public com.navdy.hud.app.ui.component.vlist.VerticalList.Model buildContactModel(com.navdy.hud.app.framework.contacts.Contact c, int size) {
        com.navdy.hud.app.ui.component.vlist.VerticalList.Model model;
        com.navdy.hud.app.framework.contacts.ContactImageHelper contactImageHelper = com.navdy.hud.app.framework.contacts.ContactImageHelper.getInstance();
        if (android.text.TextUtils.isEmpty(c.name)) {
            model = com.navdy.hud.app.ui.component.vlist.viewholder.IconsTwoViewHolder.buildModel(com.navdy.hud.app.R.id.active_trip_contact, com.navdy.hud.app.R.drawable.icon_user_bg_4, com.navdy.hud.app.R.drawable.icon_user_numberonly, resources.getColor(com.navdy.hud.app.R.color.icon_user_bg_4), -1, c.formattedNumber, null);
        } else {
            int largeImageRes = contactImageHelper.getResourceId(c.defaultImageIndex);
            int fluctuator = contactImageHelper.getResourceColor(c.defaultImageIndex);
            java.lang.String subTitle = null;
            if (size == 1) {
                if (c.numberType != com.navdy.hud.app.framework.contacts.NumberType.OTHER) {
                    subTitle = c.numberTypeStr;
                } else {
                    subTitle = c.formattedNumber;
                }
            }
            model = com.navdy.hud.app.ui.component.vlist.viewholder.IconsTwoViewHolder.buildModel(com.navdy.hud.app.R.id.active_trip_contact, largeImageRes, com.navdy.hud.app.R.drawable.icon_user_grey, fluctuator, -1, resources.getString(com.navdy.hud.app.R.string.contact_person, new java.lang.Object[]{c.name}), subTitle);
        }
        model.extras = new java.util.HashMap<>();
        model.extras.put(com.navdy.hud.app.ui.component.vlist.VerticalList.Model.INITIALS, c.initials);
        return model;
    }
}
