package com.navdy.hud.app.ui.component.bluetooth;

public class BluetoothPairing implements com.navdy.hud.app.framework.toast.IToastCallback {
    public static final java.lang.String ARG_AUTO_ACCEPT = "auto";
    public static final java.lang.String ARG_DEVICE = "device";
    public static final java.lang.String ARG_PIN = "pin";
    public static final java.lang.String ARG_VARIANT = "variant";
    private static final java.lang.String BLUETOOTH_PAIRING_TOAST_ID = "bt-pairing";
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.bluetooth.BluetoothPairing.class);
    private boolean acceptDone;
    private boolean autoAccept;
    private com.squareup.otto.Bus bus;
    private java.util.ArrayList<com.navdy.hud.app.ui.component.ChoiceLayout.Choice> choices = new java.util.ArrayList<>(2);
    private boolean confirmationRequired;
    private android.bluetooth.BluetoothDevice device;
    private boolean initialized;
    private int pin;
    private int variant;

    public static class BondStateChange {
        public final android.bluetooth.BluetoothDevice device;
        public final int newState;
        public final int oldState;

        public BondStateChange(android.bluetooth.BluetoothDevice device2, int oldState2, int newState2) {
            this.device = device2;
            this.oldState = oldState2;
            this.newState = newState2;
        }
    }

    public static class PairingCancelled {
        public final android.bluetooth.BluetoothDevice device;

        public PairingCancelled(android.bluetooth.BluetoothDevice device2) {
            this.device = device2;
        }
    }

    public static void showBluetoothPairingToast(android.os.Bundle bundle) {
        sLogger.v("showBluetoothPairingToast");
        android.bluetooth.BluetoothDevice device2 = (android.bluetooth.BluetoothDevice) bundle.getParcelable(ARG_DEVICE);
        int pin2 = bundle.getInt(ARG_PIN);
        int variant2 = bundle.getInt(ARG_VARIANT);
        boolean autoAccept2 = bundle.getBoolean("auto");
        if (!java.lang.Boolean.TRUE.equals(java.lang.Boolean.valueOf(com.navdy.hud.app.device.dial.DialManager.getInstance().isDialConnected()))) {
            autoAccept2 = true;
        }
        com.navdy.hud.app.ui.component.bluetooth.BluetoothPairing pairingToast = new com.navdy.hud.app.ui.component.bluetooth.BluetoothPairing(device2, pin2, variant2, autoAccept2, com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getBus());
        sLogger.v("show()");
        pairingToast.show();
    }

    BluetoothPairing(android.bluetooth.BluetoothDevice device2, int pin2, int variant2, boolean autoAccept2, com.squareup.otto.Bus bus2) {
        this.device = device2;
        this.pin = pin2;
        this.variant = variant2;
        this.autoAccept = autoAccept2;
        this.bus = bus2;
    }

    /* access modifiers changed from: 0000 */
    public void show() {
        boolean required;
        android.content.res.Resources resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        android.os.Bundle data = new android.os.Bundle();
        data.putInt(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_IMAGE, com.navdy.hud.app.R.drawable.icon_bluetooth_connecting);
        data.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE, resources.getString(com.navdy.hud.app.R.string.bluetooth_pairing_title));
        data.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_1, resources.getString(com.navdy.hud.app.R.string.bluetooth_pairing_code));
        java.lang.String displayName = null;
        if (this.device != null) {
            displayName = this.device.getName();
            if (android.text.TextUtils.isEmpty(displayName)) {
                displayName = this.device.getAddress();
            }
        }
        if (displayName == null) {
            displayName = resources.getString(com.navdy.hud.app.R.string.unknown_device);
        }
        data.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_3, resources.getString(com.navdy.hud.app.R.string.bluetooth_device, new java.lang.Object[]{displayName}));
        data.putString(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE_2, formatPin(this.pin, this.variant));
        if (!this.autoAccept) {
            required = true;
        } else {
            required = false;
        }
        if (required != this.confirmationRequired || !this.initialized) {
            this.initialized = true;
            this.confirmationRequired = required;
            if (required) {
                this.choices.add(new com.navdy.hud.app.ui.component.ChoiceLayout.Choice(resources.getString(com.navdy.hud.app.R.string.bluetooth_confirm), 0));
                this.choices.add(new com.navdy.hud.app.ui.component.ChoiceLayout.Choice(resources.getString(com.navdy.hud.app.R.string.cancel), 0));
            } else {
                this.choices.add(new com.navdy.hud.app.ui.component.ChoiceLayout.Choice(resources.getString(com.navdy.hud.app.R.string.cancel), 0));
            }
            data.putParcelableArrayList(com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_CHOICE_LIST, this.choices);
        }
        com.navdy.hud.app.framework.toast.ToastManager toastManager = com.navdy.hud.app.framework.toast.ToastManager.getInstance();
        toastManager.dismissCurrentToast();
        toastManager.clearAllPendingToast();
        toastManager.addToast(new com.navdy.hud.app.framework.toast.ToastManager.ToastParams(BLUETOOTH_PAIRING_TOAST_ID, data, this, false, true, true));
    }

    public void onStart(com.navdy.hud.app.view.ToastView view) {
        sLogger.v("registerBus");
        this.bus.register(this);
        if (!this.acceptDone && this.autoAccept) {
            this.acceptDone = true;
            sLogger.v("auto-accept");
            com.navdy.hud.app.util.BluetoothUtil.confirmPairing(this.device, this.variant, this.pin);
        }
    }

    public void onStop() {
        sLogger.v("unregisterBus");
        this.bus.unregister(this);
    }

    public boolean onKey(com.navdy.hud.app.manager.InputManager.CustomKeyEvent event) {
        return false;
    }

    public void executeChoiceItem(int pos, int id) {
        sLogger.v("execute:" + pos);
        if (!this.confirmationRequired) {
            finish();
        } else if (pos == 0) {
            confirm();
        } else {
            finish();
        }
    }

    private java.lang.String formatPin(int pin2, int variant2) {
        if (variant2 == 5) {
            return java.lang.String.format("%04d", new java.lang.Object[]{java.lang.Integer.valueOf(pin2)});
        }
        return java.lang.String.format("%06d", new java.lang.Object[]{java.lang.Integer.valueOf(pin2)});
    }

    @com.squareup.otto.Subscribe
    public void onPairingCancelled(com.navdy.hud.app.ui.component.bluetooth.BluetoothPairing.PairingCancelled event) {
        sLogger.v("onPairingCancelled");
        finish();
    }

    @com.squareup.otto.Subscribe
    public void onBondStateChange(com.navdy.hud.app.ui.component.bluetooth.BluetoothPairing.BondStateChange event) {
        sLogger.v("onBondStateChange");
        if (!event.device.equals(this.device)) {
            return;
        }
        if (event.newState == 10 || event.newState == 12) {
            sLogger.i("ending pairing dialog because of bond state change");
            finish();
            return;
        }
        sLogger.i("Ignoring bond state change for " + event.device.getAddress() + " state: " + event.newState + " dialog device:" + this.device.getAddress());
    }

    private void finish() {
        sLogger.v("finish()");
        com.navdy.hud.app.framework.toast.ToastManager.getInstance().dismissCurrentToast();
    }

    private void confirm() {
        sLogger.v("confirm()");
        com.navdy.hud.app.util.BluetoothUtil.confirmPairing(this.device, this.variant, this.pin);
        finish();
    }
}
