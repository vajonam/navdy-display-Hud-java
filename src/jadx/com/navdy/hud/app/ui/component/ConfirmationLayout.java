package com.navdy.hud.app.ui.component;

public class ConfirmationLayout extends android.widget.RelativeLayout {
    public com.navdy.hud.app.ui.component.ChoiceLayout choiceLayout;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.ui.component.ChoiceLayout.IListener choiceListener;
    private com.navdy.hud.app.ui.component.ChoiceLayout.IListener defaultChoiceListener;
    /* access modifiers changed from: private */
    public boolean finished;
    public com.navdy.hud.app.ui.component.FluctuatorAnimatorView fluctuatorView;
    private android.os.Handler handler;
    public android.widget.ImageView leftSwipe;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.ui.component.ConfirmationLayout.Listener listener;
    public android.view.ViewGroup mainSection;
    public android.widget.ImageView rightSwipe;
    public com.navdy.hud.app.ui.component.image.InitialsImageView screenImage;
    public com.navdy.hud.app.ui.component.image.IconColorImageView screenImageIconBkColor;
    public android.widget.TextView screenTitle;
    public android.widget.ImageView sideImage;
    private int timeout;
    private java.lang.Runnable timeoutRunnable;
    public android.widget.TextView title1;
    public android.widget.TextView title2;
    public android.widget.TextView title3;
    public android.widget.TextView title4;
    public android.view.ViewGroup titleContainer;

    class Anon1 implements com.navdy.hud.app.ui.component.ChoiceLayout.IListener {
        Anon1() {
        }

        public void executeItem(int pos, int id) {
            com.navdy.hud.app.ui.component.ConfirmationLayout.this.clearTimeout();
            com.navdy.hud.app.ui.component.ConfirmationLayout.this.choiceListener.executeItem(pos, id);
        }

        public void itemSelected(int pos, int id) {
            com.navdy.hud.app.ui.component.ConfirmationLayout.this.choiceListener.itemSelected(pos, id);
        }
    }

    class Anon2 implements java.lang.Runnable {
        Anon2() {
        }

        public void run() {
            com.navdy.hud.app.ui.component.ConfirmationLayout.this.finished = true;
            com.navdy.hud.app.ui.component.ConfirmationLayout.this.listener.timeout();
        }
    }

    public interface Listener {
        void timeout();
    }

    public ConfirmationLayout(android.content.Context context) {
        this(context, null);
    }

    public ConfirmationLayout(android.content.Context context, android.util.AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ConfirmationLayout(android.content.Context context, android.util.AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.handler = new android.os.Handler(android.os.Looper.getMainLooper());
        this.defaultChoiceListener = new com.navdy.hud.app.ui.component.ConfirmationLayout.Anon1();
        this.timeoutRunnable = new com.navdy.hud.app.ui.component.ConfirmationLayout.Anon2();
        init();
    }

    private void init() {
        android.view.LayoutInflater.from(getContext()).inflate(com.navdy.hud.app.R.layout.confirmation_lyt, this, true);
        this.screenTitle = (android.widget.TextView) findViewById(com.navdy.hud.app.R.id.mainTitle);
        this.screenImage = (com.navdy.hud.app.ui.component.image.InitialsImageView) findViewById(com.navdy.hud.app.R.id.image);
        this.screenImageIconBkColor = (com.navdy.hud.app.ui.component.image.IconColorImageView) findViewById(com.navdy.hud.app.R.id.imageIconBkColor);
        this.sideImage = (android.widget.ImageView) findViewById(com.navdy.hud.app.R.id.sideImage);
        this.titleContainer = (android.view.ViewGroup) findViewById(com.navdy.hud.app.R.id.infoContainer);
        this.title1 = (android.widget.TextView) findViewById(com.navdy.hud.app.R.id.title1);
        this.title2 = (android.widget.TextView) findViewById(com.navdy.hud.app.R.id.title2);
        this.title3 = (android.widget.TextView) findViewById(com.navdy.hud.app.R.id.title3);
        this.title4 = (android.widget.TextView) findViewById(com.navdy.hud.app.R.id.title4);
        this.choiceLayout = (com.navdy.hud.app.ui.component.ChoiceLayout) findViewById(com.navdy.hud.app.R.id.choiceLayout);
        this.fluctuatorView = (com.navdy.hud.app.ui.component.FluctuatorAnimatorView) findViewById(com.navdy.hud.app.R.id.fluctuator);
        this.leftSwipe = (android.widget.ImageView) findViewById(com.navdy.hud.app.R.id.leftSwipe);
        this.rightSwipe = (android.widget.ImageView) findViewById(com.navdy.hud.app.R.id.rightSwipe);
        this.mainSection = (android.view.ViewGroup) findViewById(com.navdy.hud.app.R.id.mainSection);
    }

    public void setTimeout(int millis, com.navdy.hud.app.ui.component.ConfirmationLayout.Listener listener2) {
        if (!this.finished) {
            if (listener2 == null || millis <= 0) {
                throw new java.lang.IllegalArgumentException();
            }
            this.timeout = millis;
            this.listener = listener2;
            resetTimeout();
        }
    }

    private void resetTimeout() {
        if (this.timeout > 0) {
            this.handler.removeCallbacks(this.timeoutRunnable);
            this.handler.postDelayed(this.timeoutRunnable, (long) this.timeout);
        }
    }

    /* access modifiers changed from: private */
    public void clearTimeout() {
        this.handler.removeCallbacks(this.timeoutRunnable);
    }

    public void setChoices(java.util.List<java.lang.String> choiceLabels, int initialSelection, com.navdy.hud.app.ui.component.ChoiceLayout.IListener listener2) {
        this.finished = false;
        clearTimeout();
        if (choiceLabels == null || choiceLabels.size() == 0 || listener2 == null) {
            throw new java.lang.IllegalArgumentException();
        }
        this.choiceListener = listener2;
        java.util.List<com.navdy.hud.app.ui.component.ChoiceLayout.Choice> list = new java.util.ArrayList<>();
        for (java.lang.String str : choiceLabels) {
            list.add(new com.navdy.hud.app.ui.component.ChoiceLayout.Choice(str, 0));
        }
        this.choiceLayout.setChoices(com.navdy.hud.app.ui.component.ChoiceLayout.Mode.LABEL, list, initialSelection, this.defaultChoiceListener);
    }

    public void setChoicesList(java.util.List<com.navdy.hud.app.ui.component.ChoiceLayout.Choice> choices, int initialSelection, com.navdy.hud.app.ui.component.ChoiceLayout.IListener listener2) {
        this.finished = false;
        clearTimeout();
        if (choices == null || choices.size() == 0 || listener2 == null) {
            throw new java.lang.IllegalArgumentException();
        }
        this.choiceListener = listener2;
        this.choiceLayout.setChoices(com.navdy.hud.app.ui.component.ChoiceLayout.Mode.LABEL, choices, initialSelection, this.defaultChoiceListener);
    }

    public void moveSelectionLeft() {
        if (!this.finished) {
            resetTimeout();
            this.choiceLayout.moveSelectionLeft();
        }
    }

    public void moveSelectionRight() {
        if (!this.finished) {
            resetTimeout();
            this.choiceLayout.moveSelectionRight();
        }
    }

    public void executeSelectedItem(boolean animated) {
        if (!this.finished) {
            resetTimeout();
            this.choiceLayout.executeSelectedItem(animated);
        }
    }

    public boolean handleKey(com.navdy.hud.app.manager.InputManager.CustomKeyEvent event) {
        switch (event) {
            case LEFT:
                moveSelectionLeft();
                return true;
            case RIGHT:
                moveSelectionRight();
                return true;
            case SELECT:
                executeSelectedItem(true);
                return true;
            default:
                return false;
        }
    }
}
