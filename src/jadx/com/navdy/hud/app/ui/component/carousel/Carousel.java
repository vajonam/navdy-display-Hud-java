package com.navdy.hud.app.ui.component.carousel;

public final class Carousel {

    public static class InitParams {
        public com.navdy.hud.app.ui.component.carousel.AnimationStrategy animator;
        public com.navdy.hud.app.ui.component.carousel.CarouselIndicator carouselIndicator;
        public boolean exitOnDoubleClick;
        public boolean fastScrollAnimation;
        public int imageLytResourceId;
        public int infoLayoutResourceId;
        public java.util.List<com.navdy.hud.app.ui.component.carousel.Carousel.Model> model;
        public android.view.View rootContainer;
        public com.navdy.hud.app.ui.component.carousel.Carousel.ViewProcessor viewProcessor;
    }

    public interface Listener {
        void onCurrentItemChanged(int i, int i2);

        void onCurrentItemChanging(int i, int i2, int i3);

        void onExecuteItem(int i, int i2);

        void onExit();
    }

    public static class Model {
        public java.lang.Object extras;
        public int id;
        public java.util.HashMap<java.lang.Integer, java.lang.String> infoMap;
        public int largeImageRes;
        public int smallImageColor;
        public int smallImageRes;
    }

    public static class ViewCacheManager {
        private int maxCacheCount;
        private java.util.ArrayList<android.view.View> middleLeftViewCache = new java.util.ArrayList<>();
        private java.util.ArrayList<android.view.View> middleRightViewCache = new java.util.ArrayList<>();
        private java.util.ArrayList<android.view.View> sideViewCache = new java.util.ArrayList<>();

        public ViewCacheManager(int maxCacheCount2) {
            this.maxCacheCount = maxCacheCount2;
        }

        public android.view.View getView(com.navdy.hud.app.ui.component.carousel.Carousel.ViewType viewType) {
            android.view.View cachedView = null;
            switch (viewType) {
                case SIDE:
                    if (this.sideViewCache.size() > 0) {
                        cachedView = (android.view.View) this.sideViewCache.remove(0);
                        break;
                    }
                    break;
                case MIDDLE_LEFT:
                    if (this.middleLeftViewCache.size() > 0) {
                        cachedView = (android.view.View) this.middleLeftViewCache.remove(0);
                        break;
                    }
                    break;
                case MIDDLE_RIGHT:
                    if (this.middleRightViewCache.size() > 0) {
                        cachedView = (android.view.View) this.middleRightViewCache.remove(0);
                        break;
                    }
                    break;
            }
            if (!(cachedView == null || cachedView.getParent() == null)) {
                android.util.Log.e("CAROUSEL", "uhoh");
            }
            return cachedView;
        }

        public void putView(com.navdy.hud.app.ui.component.carousel.Carousel.ViewType viewType, android.view.View view) {
            if (view == null || view.getParent() != null) {
                android.util.Log.e("CAROUSEL", "uhoh");
            }
            switch (viewType) {
                case SIDE:
                    if (this.sideViewCache.size() < this.maxCacheCount) {
                        this.sideViewCache.add(view);
                        return;
                    }
                    return;
                case MIDDLE_LEFT:
                    if (this.middleLeftViewCache.size() < this.maxCacheCount) {
                        this.middleLeftViewCache.add(view);
                        return;
                    }
                    return;
                case MIDDLE_RIGHT:
                    if (this.middleRightViewCache.size() < this.maxCacheCount) {
                        this.middleRightViewCache.add(view);
                        return;
                    }
                    return;
                default:
                    return;
            }
        }

        public void clearCache() {
            this.sideViewCache.clear();
            this.middleLeftViewCache.clear();
            this.middleRightViewCache.clear();
        }
    }

    public interface ViewProcessor {
        void processInfoView(com.navdy.hud.app.ui.component.carousel.Carousel.Model model, android.view.View view, int i);

        void processLargeImageView(com.navdy.hud.app.ui.component.carousel.Carousel.Model model, android.view.View view, int i, int i2, int i3);

        void processSmallImageView(com.navdy.hud.app.ui.component.carousel.Carousel.Model model, android.view.View view, int i, int i2, int i3);
    }

    public enum ViewType {
        SIDE,
        MIDDLE_LEFT,
        MIDDLE_RIGHT
    }
}
