package com.navdy.hud.app.ui.component.mainmenu;

class ShareTripMenu implements com.navdy.hud.app.ui.component.destination.IDestinationPicker {
    private static final int SEND_WITHOUT_MESSAGE_POS = 0;
    private static final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.mainmenu.ShareTripMenu.class);
    private final com.navdy.hud.app.framework.contacts.Contact contact;
    private java.lang.String failedDestinationLabel;
    private double failedLatitude;
    private double failedLongitude;
    private java.lang.String failedMessage;
    private final com.navdy.hud.app.framework.glympse.GlympseManager glympseManager = com.navdy.hud.app.framework.glympse.GlympseManager.getInstance();
    private boolean hasFailed;
    private boolean itemSelected = false;
    private final java.lang.String notificationId;

    ShareTripMenu(com.navdy.hud.app.framework.contacts.Contact contact2, java.lang.String notificationId2) {
        this.contact = contact2;
        this.notificationId = notificationId2;
        this.hasFailed = false;
    }

    public boolean onItemClicked(int id, int pos, com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.DestinationPickerState state) {
        java.lang.String message = com.navdy.hud.app.framework.glympse.GlympseManager.getInstance().getMessages()[pos];
        logger.v("selected option: " + message);
        com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("glympse_menu_" + message);
        this.itemSelected = true;
        if (pos == 0) {
            sendShare(this.contact);
        } else {
            sendShare(this.contact, message);
        }
        return true;
    }

    public boolean onItemSelected(int id, int pos, com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.DestinationPickerState state) {
        return true;
    }

    public void onDestinationPickerClosed() {
        if (!this.itemSelected) {
            com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("back");
        }
        if (this.hasFailed) {
            addGlympseOfflineGlance(this.failedMessage, this.contact, this.failedDestinationLabel, this.failedLatitude, this.failedLongitude);
        }
    }

    private void addGlympseOfflineGlance(java.lang.String message, com.navdy.hud.app.framework.contacts.Contact contact2, java.lang.String destinationLabel, double latitude, double longitude) {
        com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().addNotification(new com.navdy.hud.app.framework.glympse.GlympseNotification(contact2, com.navdy.hud.app.framework.glympse.GlympseNotification.Type.OFFLINE, message, destinationLabel, latitude, longitude));
    }

    private void sendShare(com.navdy.hud.app.framework.contacts.Contact contact2) {
        sendShare(contact2, null);
    }

    private void sendShare(com.navdy.hud.app.framework.contacts.Contact contact2, java.lang.String message) {
        java.lang.String shareType;
        com.navdy.hud.app.maps.here.HereNavigationManager hereNavigationManager = com.navdy.hud.app.maps.here.HereNavigationManager.getInstance();
        java.lang.String destinationLabel = null;
        double latitude = 0.0d;
        double longitude = 0.0d;
        if (this.notificationId != null) {
            com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().removeNotification(this.notificationId);
        }
        com.here.android.mpa.common.GeoCoordinate geoCoordinate = null;
        if (hereNavigationManager.isNavigationModeOn()) {
            shareType = com.navdy.hud.app.framework.glympse.GlympseManager.GLYMPSE_TYPE_SHARE_TRIP;
            destinationLabel = hereNavigationManager.getDestinationLabel();
            com.here.android.mpa.mapping.MapMarker destinationMarker = hereNavigationManager.getDestinationMarker();
            if (destinationMarker != null) {
                geoCoordinate = destinationMarker.getCoordinate();
            } else {
                com.navdy.service.library.events.navigation.NavigationRouteRequest navigationRouteRequest = hereNavigationManager.getCurrentNavigationRouteRequest();
                if (!(navigationRouteRequest == null || navigationRouteRequest.destination == null)) {
                    geoCoordinate = new com.here.android.mpa.common.GeoCoordinate(navigationRouteRequest.destination.latitude.doubleValue(), navigationRouteRequest.destination.longitude.doubleValue());
                }
            }
        } else {
            shareType = "Location";
        }
        if (geoCoordinate != null) {
            latitude = geoCoordinate.getLatitude();
            longitude = geoCoordinate.getLongitude();
        }
        java.lang.StringBuilder builder = new java.lang.StringBuilder();
        this.hasFailed = this.glympseManager.addMessage(contact2, message, destinationLabel, latitude, longitude, builder) != com.navdy.hud.app.framework.glympse.GlympseManager.Error.NONE;
        if (this.hasFailed) {
            this.failedMessage = message;
            this.failedDestinationLabel = destinationLabel;
            this.failedLatitude = latitude;
            this.failedLongitude = longitude;
        }
        com.navdy.hud.app.analytics.AnalyticsSupport.recordGlympseSent(!this.hasFailed, shareType, message, builder.toString());
        logger.v("Glympse: success= " + (!this.hasFailed) + "share=" + shareType);
    }
}
