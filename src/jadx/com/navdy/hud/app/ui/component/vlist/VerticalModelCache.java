package com.navdy.hud.app.ui.component.vlist;

public class VerticalModelCache {
    private static final java.util.Map<com.navdy.hud.app.ui.component.vlist.VerticalList.ModelType, com.navdy.hud.app.ui.component.vlist.VerticalModelCache.CacheEntry> map = new java.util.HashMap();
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.vlist.VerticalModelCache.class);

    private static class CacheEntry {
        java.util.ArrayList<com.navdy.hud.app.ui.component.vlist.VerticalList.Model> items;
        int maxItems;

        CacheEntry(int maxItems2, int initialItems) {
            this.maxItems = maxItems2;
            this.items = new java.util.ArrayList<>(maxItems2);
            if (initialItems > 0) {
                for (int i = 0; i < initialItems; i++) {
                    this.items.add(new com.navdy.hud.app.ui.component.vlist.VerticalList.Model());
                }
            }
        }
    }

    static {
        map.put(com.navdy.hud.app.ui.component.vlist.VerticalList.ModelType.ICON_BKCOLOR, new com.navdy.hud.app.ui.component.vlist.VerticalModelCache.CacheEntry(500, 100));
        map.put(com.navdy.hud.app.ui.component.vlist.VerticalList.ModelType.TWO_ICONS, new com.navdy.hud.app.ui.component.vlist.VerticalModelCache.CacheEntry(70, 10));
        map.put(com.navdy.hud.app.ui.component.vlist.VerticalList.ModelType.LOADING_CONTENT, new com.navdy.hud.app.ui.component.vlist.VerticalModelCache.CacheEntry(500, 100));
    }

    public static void addToCache(com.navdy.hud.app.ui.component.vlist.VerticalList.ModelType modelType, com.navdy.hud.app.ui.component.vlist.VerticalList.Model model) {
        if (model != null && modelType != null) {
            com.navdy.hud.app.ui.component.vlist.VerticalModelCache.CacheEntry entry = (com.navdy.hud.app.ui.component.vlist.VerticalModelCache.CacheEntry) map.get(modelType);
            if (entry != null && entry.items.size() < entry.maxItems) {
                model.clear();
                entry.items.add(model);
            }
        }
    }

    public static void addToCache(java.util.List<com.navdy.hud.app.ui.component.vlist.VerticalList.Model> list) {
        if (list != null) {
            for (com.navdy.hud.app.ui.component.vlist.VerticalList.Model model : list) {
                addToCache(model.type, model);
            }
        }
    }

    public static com.navdy.hud.app.ui.component.vlist.VerticalList.Model getFromCache(com.navdy.hud.app.ui.component.vlist.VerticalList.ModelType modelType) {
        if (modelType == null) {
            return null;
        }
        com.navdy.hud.app.ui.component.vlist.VerticalModelCache.CacheEntry entry = (com.navdy.hud.app.ui.component.vlist.VerticalModelCache.CacheEntry) map.get(modelType);
        if (entry == null || entry.items.size() <= 0) {
            return null;
        }
        return (com.navdy.hud.app.ui.component.vlist.VerticalList.Model) entry.items.remove(0);
    }
}
