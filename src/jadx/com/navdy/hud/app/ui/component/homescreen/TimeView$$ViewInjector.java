package com.navdy.hud.app.ui.component.homescreen;

public class TimeView$$ViewInjector {
    public static void inject(butterknife.ButterKnife.Finder finder, com.navdy.hud.app.ui.component.homescreen.TimeView target, java.lang.Object source) {
        target.dayTextView = (android.widget.TextView) finder.findRequiredView(source, com.navdy.hud.app.R.id.txt_day, "field 'dayTextView'");
        target.timeTextView = (android.widget.TextView) finder.findRequiredView(source, com.navdy.hud.app.R.id.txt_time, "field 'timeTextView'");
        target.ampmTextView = (android.widget.TextView) finder.findRequiredView(source, com.navdy.hud.app.R.id.txt_ampm, "field 'ampmTextView'");
    }

    public static void reset(com.navdy.hud.app.ui.component.homescreen.TimeView target) {
        target.dayTextView = null;
        target.timeTextView = null;
        target.ampmTextView = null;
    }
}
