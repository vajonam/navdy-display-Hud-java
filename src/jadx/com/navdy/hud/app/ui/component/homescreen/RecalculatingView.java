package com.navdy.hud.app.ui.component.homescreen;

public class RecalculatingView extends android.widget.FrameLayout implements com.navdy.hud.app.ui.component.homescreen.IHomeScreenLifecycle {
    private com.squareup.otto.Bus bus;
    private com.navdy.hud.app.ui.component.homescreen.HomeScreenView homeScreenView;
    private com.navdy.hud.app.maps.MapEvents.RerouteEvent lastRerouteEvent;
    private com.navdy.service.library.log.Logger logger;
    private boolean paused;
    @butterknife.InjectView(2131624363)
    com.navdy.hud.app.ui.component.FluctuatorAnimatorView recalcAnimator;
    @butterknife.InjectView(2131624364)
    com.navdy.hud.app.ui.component.image.ColorImageView recalcImageView;

    public RecalculatingView(android.content.Context context) {
        this(context, null);
    }

    public RecalculatingView(android.content.Context context, android.util.AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RecalculatingView(android.content.Context context, android.util.AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        this.logger = com.navdy.hud.app.ui.component.homescreen.HomeScreenView.sLogger;
        super.onFinishInflate();
        butterknife.ButterKnife.inject((android.view.View) this);
        this.recalcImageView.setColor(com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.reCalcAnimColor);
        this.bus = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getBus();
    }

    public void init(com.navdy.hud.app.ui.component.homescreen.HomeScreenView homeScreenView2) {
        this.homeScreenView = homeScreenView2;
        this.bus.register(this);
    }

    @com.squareup.otto.Subscribe
    public void onRerouteEvent(com.navdy.hud.app.maps.MapEvents.RerouteEvent event) {
        this.logger.v("rerouteEvent:" + event.routeEventType);
        if (!this.homeScreenView.isNavigationActive() || com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().hasArrived()) {
            this.logger.w("rerouteEvent:navigation not active|hasArrived|routeCalcOn");
            return;
        }
        this.lastRerouteEvent = event;
        if (event.routeEventType == com.navdy.hud.app.maps.MapEvents.RouteEventType.STARTED) {
            this.homeScreenView.setRecalculating(true);
            if (!this.paused) {
                showRecalculating();
                return;
            }
            return;
        }
        this.homeScreenView.setRecalculating(false);
        if (!this.paused || getVisibility() == 0) {
            hideRecalculating();
            this.homeScreenView.getTbtView().setVisibility(0);
        }
    }

    public void showRecalculating() {
        this.homeScreenView.getTbtView().setVisibility(8);
        setVisibility(0);
        this.recalcAnimator.start();
    }

    public void hideRecalculating() {
        setVisibility(8);
        this.recalcAnimator.stop();
        this.lastRerouteEvent = null;
    }

    public void setView(com.navdy.hud.app.view.MainView.CustomAnimationMode mode) {
        switch (mode) {
            case EXPAND:
                setX((float) com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.recalcX);
                return;
            case SHRINK_LEFT:
                setX((float) com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.recalcShrinkLeftX);
                return;
            default:
                return;
        }
    }

    public void onPause() {
        if (!this.paused) {
            this.paused = true;
            this.logger.v("::onPause:recalc");
        }
    }

    public void onResume() {
        if (this.paused) {
            this.paused = false;
            this.logger.v("::onResume:recalc");
            com.navdy.hud.app.maps.MapEvents.RerouteEvent event = this.lastRerouteEvent;
            if (event != null) {
                this.lastRerouteEvent = null;
                this.logger.v("::onResume:recalc set last reroute event");
                onRerouteEvent(event);
            }
        }
    }
}
