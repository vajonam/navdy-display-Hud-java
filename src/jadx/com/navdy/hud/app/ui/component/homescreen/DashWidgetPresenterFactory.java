package com.navdy.hud.app.ui.component.homescreen;

public class DashWidgetPresenterFactory {
    private static final java.util.HashMap<java.lang.String, java.lang.Integer> RESOURCE_MAP = new java.util.HashMap<>();

    static {
        RESOURCE_MAP.put(com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.EMPTY_WIDGET_ID, java.lang.Integer.valueOf(0));
        RESOURCE_MAP.put(com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.MPG_GRAPH_WIDGET_ID, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.asset_dash20_proto_gauge_mpg));
        RESOURCE_MAP.put(com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.MPG_AVG_WIDGET_ID, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.asset_dash20_proto_gauge_clock_analog));
        RESOURCE_MAP.put(com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.WEATHER_WIDGET_ID, java.lang.Integer.valueOf(com.navdy.hud.app.R.drawable.asset_dash20_proto_gauge_weather));
    }

    public static com.navdy.hud.app.view.DashboardWidgetPresenter createDashWidgetPresenter(android.content.Context context, java.lang.String widgetIdentifier) {
        char c = 65535;
        switch (widgetIdentifier.hashCode()) {
            case -2114596188:
                if (widgetIdentifier.equals(com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.ANALOG_CLOCK_WIDGET_ID)) {
                    c = 3;
                    break;
                }
                break;
            case -1874661839:
                if (widgetIdentifier.equals(com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.COMPASS_WIDGET_ID)) {
                    c = 2;
                    break;
                }
                break;
            case -1515555394:
                if (widgetIdentifier.equals(com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.MUSIC_WIDGET_ID)) {
                    c = 10;
                    break;
                }
                break;
            case -1158963060:
                if (widgetIdentifier.equals(com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.MPG_AVG_WIDGET_ID)) {
                    c = 9;
                    break;
                }
                break;
            case -856367506:
                if (widgetIdentifier.equals(com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.ETA_GAUGE_ID)) {
                    c = 8;
                    break;
                }
                break;
            case -538792631:
                if (widgetIdentifier.equals(com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.DIGITAL_CLOCK_2_WIDGET_ID)) {
                    c = 5;
                    break;
                }
                break;
            case -131933527:
                if (widgetIdentifier.equals(com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.ENGINE_TEMPERATURE_GAUGE_ID)) {
                    c = 13;
                    break;
                }
                break;
            case 460083427:
                if (widgetIdentifier.equals(com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.DRIVE_SCORE_GAUGE_ID)) {
                    c = 14;
                    break;
                }
                break;
            case 641719909:
                if (widgetIdentifier.equals(com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.CALENDAR_WIDGET_ID)) {
                    c = 7;
                    break;
                }
                break;
            case 662413980:
                if (widgetIdentifier.equals(com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.DIGITAL_CLOCK_WIDGET_ID)) {
                    c = 4;
                    break;
                }
                break;
            case 811991446:
                if (widgetIdentifier.equals(com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.EMPTY_WIDGET_ID)) {
                    c = 0;
                    break;
                }
                break;
            case 1119776967:
                if (widgetIdentifier.equals(com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.SPEED_LIMIT_SIGN_GAUGE_ID)) {
                    c = 11;
                    break;
                }
                break;
            case 1168400042:
                if (widgetIdentifier.equals(com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.FUEL_GAUGE_ID)) {
                    c = 1;
                    break;
                }
                break;
            case 1872543020:
                if (widgetIdentifier.equals(com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.TRAFFIC_INCIDENT_GAUGE_ID)) {
                    c = 6;
                    break;
                }
                break;
            case 2057821183:
                if (widgetIdentifier.equals(com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.GFORCE_WIDGET_ID)) {
                    c = 12;
                    break;
                }
                break;
        }
        switch (c) {
            case 0:
                return new com.navdy.hud.app.view.EmptyGaugePresenter(context);
            case 1:
                return new com.navdy.hud.app.view.FuelGaugePresenter2(context);
            case 2:
                return new com.navdy.hud.app.view.CompassPresenter(context);
            case 3:
                return new com.navdy.hud.app.view.ClockWidgetPresenter(context, com.navdy.hud.app.view.ClockWidgetPresenter.ClockType.ANALOG);
            case 4:
                return new com.navdy.hud.app.view.ClockWidgetPresenter(context, com.navdy.hud.app.view.ClockWidgetPresenter.ClockType.DIGITAL1);
            case 5:
                return new com.navdy.hud.app.view.ClockWidgetPresenter(context, com.navdy.hud.app.view.ClockWidgetPresenter.ClockType.DIGITAL2);
            case 6:
                return new com.navdy.hud.app.maps.widget.TrafficIncidentWidgetPresenter();
            case 7:
                return new com.navdy.hud.app.view.CalendarWidgetPresenter(context);
            case 8:
                return new com.navdy.hud.app.view.ETAGaugePresenter(context);
            case 9:
                return new com.navdy.hud.app.view.MPGGaugePresenter(context);
            case 10:
                return new com.navdy.hud.app.view.MusicWidgetPresenter(context);
            case 11:
                return new com.navdy.hud.app.view.SpeedLimitSignPresenter(context);
            case 12:
                return new com.navdy.hud.app.view.GForcePresenter(context);
            case 13:
                return new com.navdy.hud.app.view.EngineTemperaturePresenter(context);
            case 14:
                return new com.navdy.hud.app.view.DriveScoreGaugePresenter(context, com.navdy.hud.app.R.layout.drive_score_gauge_layout, true);
            default:
                return new com.navdy.hud.app.ui.component.homescreen.ImageWidgetPresenter(context, ((java.lang.Integer) RESOURCE_MAP.get(widgetIdentifier)).intValue(), widgetIdentifier);
        }
    }
}
