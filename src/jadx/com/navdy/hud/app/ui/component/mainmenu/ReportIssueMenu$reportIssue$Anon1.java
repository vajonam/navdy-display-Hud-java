package com.navdy.hud.app.ui.component.mainmenu;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\b\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\b\u0002"}, d2 = {"<anonymous>", "", "run"}, k = 3, mv = {1, 1, 6})
/* compiled from: ReportIssueMenu.kt */
final class ReportIssueMenu$reportIssue$Anon1 implements java.lang.Runnable {
    final /* synthetic */ com.navdy.hud.app.util.ReportIssueService.IssueType $issueType;
    final /* synthetic */ com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu this$Anon0;

    @kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\b\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\b\u0002"}, d2 = {"<anonymous>", "", "run"}, k = 3, mv = {1, 1, 6})
    /* compiled from: ReportIssueMenu.kt */
    static final class Anon1 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$reportIssue$Anon1 this$Anon0;

        Anon1(com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$reportIssue$Anon1 reportIssueMenu$reportIssue$Anon1) {
            this.this$Anon0 = reportIssueMenu$reportIssue$Anon1;
        }

        public final void run() {
            com.navdy.hud.app.util.ReportIssueService.dispatchReportNewIssue(this.this$Anon0.$issueType);
            this.this$Anon0.this$Anon0.showSentToast();
        }
    }

    ReportIssueMenu$reportIssue$Anon1(com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu reportIssueMenu, com.navdy.hud.app.util.ReportIssueService.IssueType issueType) {
        this.this$Anon0 = reportIssueMenu;
        this.$issueType = issueType;
    }

    public final void run() {
        this.this$Anon0.presenter.close(new com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$reportIssue$Anon1.Anon1(this));
    }
}
