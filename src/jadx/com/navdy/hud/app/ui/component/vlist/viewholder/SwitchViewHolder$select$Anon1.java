package com.navdy.hud.app.ui.component.vlist.viewholder;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u0019\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\b\n\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0012\u0010\u0003\u001a\u00020\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u0016\u00a8\u0006\u0007"}, d2 = {"com/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$select$Anon1", "Landroid/animation/ValueAnimator$AnimatorUpdateListener;", "(Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;Landroid/graphics/drawable/ClipDrawable;)V", "onAnimationUpdate", "", "animation", "Landroid/animation/ValueAnimator;", "app_hudDebug"}, k = 1, mv = {1, 1, 6})
/* compiled from: SwitchViewHolder.kt */
public final class SwitchViewHolder$select$Anon1 implements android.animation.ValueAnimator.AnimatorUpdateListener {
    final /* synthetic */ android.graphics.drawable.ClipDrawable $clipDrawable;
    final /* synthetic */ com.navdy.hud.app.ui.component.vlist.viewholder.SwitchViewHolder this$Anon0;

    SwitchViewHolder$select$Anon1(com.navdy.hud.app.ui.component.vlist.viewholder.SwitchViewHolder $outer, android.graphics.drawable.ClipDrawable $captured_local_variable$Anon1) {
        this.this$Anon0 = $outer;
        this.$clipDrawable = $captured_local_variable$Anon1;
    }

    public void onAnimationUpdate(@org.jetbrains.annotations.Nullable android.animation.ValueAnimator animation) {
        java.lang.Object obj = animation != null ? animation.getAnimatedValue() : null;
        if (obj == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type kotlin.Float");
        }
        this.$clipDrawable.setLevel((int) ((java.lang.Float) obj).floatValue());
        this.this$Anon0.getSwitchBackground().invalidate();
    }
}
