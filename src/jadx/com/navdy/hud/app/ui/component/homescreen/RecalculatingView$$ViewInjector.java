package com.navdy.hud.app.ui.component.homescreen;

public class RecalculatingView$$ViewInjector {
    public static void inject(butterknife.ButterKnife.Finder finder, com.navdy.hud.app.ui.component.homescreen.RecalculatingView target, java.lang.Object source) {
        target.recalcImageView = (com.navdy.hud.app.ui.component.image.ColorImageView) finder.findRequiredView(source, com.navdy.hud.app.R.id.recalcImageView, "field 'recalcImageView'");
        target.recalcAnimator = (com.navdy.hud.app.ui.component.FluctuatorAnimatorView) finder.findRequiredView(source, com.navdy.hud.app.R.id.recalcAnimator, "field 'recalcAnimator'");
    }

    public static void reset(com.navdy.hud.app.ui.component.homescreen.RecalculatingView target) {
        target.recalcImageView = null;
        target.recalcAnimator = null;
    }
}
