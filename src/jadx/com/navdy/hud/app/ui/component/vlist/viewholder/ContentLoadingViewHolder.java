package com.navdy.hud.app.ui.component.vlist.viewholder;

public class ContentLoadingViewHolder extends com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder {
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.vlist.viewholder.ContentLoadingViewHolder.class);
    private android.animation.AnimatorSet.Builder animatorSetBuilder;
    private com.navdy.hud.app.ui.component.image.CrossFadeImageView crossFadeImageView;
    private android.view.ViewGroup iconContainer;
    private android.view.ViewGroup imageContainer;
    /* access modifiers changed from: private */
    public android.graphics.drawable.AnimationDrawable loopAnimation;
    private com.navdy.hud.app.ui.framework.DefaultAnimationListener loopEnd = new com.navdy.hud.app.ui.component.vlist.viewholder.ContentLoadingViewHolder.Anon2();
    /* access modifiers changed from: private */
    public android.widget.ImageView loopImageView;
    private com.navdy.hud.app.ui.framework.DefaultAnimationListener loopStart = new com.navdy.hud.app.ui.component.vlist.viewholder.ContentLoadingViewHolder.Anon1();
    /* access modifiers changed from: private */
    public android.widget.ImageView unSelImageView;

    class Anon1 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
        Anon1() {
        }

        public void onAnimationStart(android.animation.Animator animation) {
            com.navdy.hud.app.ui.component.vlist.viewholder.ContentLoadingViewHolder.this.loopImageView.setVisibility(0);
        }

        public void onAnimationEnd(android.animation.Animator animation) {
            com.navdy.hud.app.ui.component.vlist.viewholder.ContentLoadingViewHolder.this.unSelImageView.setVisibility(8);
            if (com.navdy.hud.app.ui.component.vlist.viewholder.ContentLoadingViewHolder.this.loopAnimation != null && !com.navdy.hud.app.ui.component.vlist.viewholder.ContentLoadingViewHolder.this.loopAnimation.isRunning()) {
                com.navdy.hud.app.ui.component.vlist.viewholder.ContentLoadingViewHolder.this.loopAnimation.start();
            }
        }
    }

    class Anon2 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
        Anon2() {
        }

        public void onAnimationStart(android.animation.Animator animation) {
            com.navdy.hud.app.ui.component.vlist.viewholder.ContentLoadingViewHolder.this.unSelImageView.setVisibility(0);
            if (com.navdy.hud.app.ui.component.vlist.viewholder.ContentLoadingViewHolder.this.loopAnimation != null && com.navdy.hud.app.ui.component.vlist.viewholder.ContentLoadingViewHolder.this.loopAnimation.isRunning()) {
                com.navdy.hud.app.ui.component.vlist.viewholder.ContentLoadingViewHolder.this.loopAnimation.stop();
            }
        }

        public void onAnimationEnd(android.animation.Animator animation) {
            com.navdy.hud.app.ui.component.vlist.viewholder.ContentLoadingViewHolder.this.loopImageView.setVisibility(8);
        }
    }

    public static com.navdy.hud.app.ui.component.vlist.VerticalList.Model buildModel(int icon, int iconSelectedColor, int iconDeselectedColor) {
        com.navdy.hud.app.ui.component.vlist.VerticalList.Model model = com.navdy.hud.app.ui.component.vlist.VerticalModelCache.getFromCache(com.navdy.hud.app.ui.component.vlist.VerticalList.ModelType.LOADING_CONTENT);
        if (model == null) {
            model = new com.navdy.hud.app.ui.component.vlist.VerticalList.Model();
        }
        model.type = com.navdy.hud.app.ui.component.vlist.VerticalList.ModelType.LOADING_CONTENT;
        model.id = com.navdy.hud.app.R.id.vlist_content_loading;
        model.icon = icon;
        model.iconSelectedColor = iconSelectedColor;
        model.iconDeselectedColor = iconDeselectedColor;
        return model;
    }

    public static com.navdy.hud.app.ui.component.vlist.viewholder.ContentLoadingViewHolder buildViewHolder(android.view.ViewGroup parent, com.navdy.hud.app.ui.component.vlist.VerticalList vlist, android.os.Handler handler) {
        android.view.ViewGroup layout = (android.view.ViewGroup) android.view.LayoutInflater.from(parent.getContext()).inflate(com.navdy.hud.app.R.layout.vlist_content_loading, parent, false);
        com.navdy.hud.app.ui.component.image.CrossFadeImageView image = (com.navdy.hud.app.ui.component.image.CrossFadeImageView) ((android.view.ViewGroup) layout.findViewById(com.navdy.hud.app.R.id.iconContainer)).findViewById(com.navdy.hud.app.R.id.crossFadeImageView);
        image.inject(com.navdy.hud.app.ui.component.image.CrossFadeImageView.Mode.SMALL);
        image.setId(com.navdy.hud.app.R.id.vlist_image);
        return new com.navdy.hud.app.ui.component.vlist.viewholder.ContentLoadingViewHolder(layout, vlist, handler);
    }

    private ContentLoadingViewHolder(android.view.ViewGroup layout, com.navdy.hud.app.ui.component.vlist.VerticalList vlist, android.os.Handler handler) {
        super(layout, vlist, handler);
        this.imageContainer = (android.view.ViewGroup) layout.findViewById(com.navdy.hud.app.R.id.imageContainer);
        this.iconContainer = (android.view.ViewGroup) layout.findViewById(com.navdy.hud.app.R.id.iconContainer);
        this.crossFadeImageView = (com.navdy.hud.app.ui.component.image.CrossFadeImageView) layout.findViewById(com.navdy.hud.app.R.id.vlist_image);
        this.loopImageView = (android.widget.ImageView) layout.findViewById(com.navdy.hud.app.R.id.img_loop);
        this.unSelImageView = (android.widget.ImageView) layout.findViewById(com.navdy.hud.app.R.id.img);
        this.loopAnimation = (android.graphics.drawable.AnimationDrawable) this.loopImageView.getDrawable();
    }

    public com.navdy.hud.app.ui.component.vlist.VerticalList.ModelType getModelType() {
        return com.navdy.hud.app.ui.component.vlist.VerticalList.ModelType.LOADING_CONTENT;
    }

    public void clearAnimation() {
        if (this.loopAnimation != null && this.loopAnimation.isRunning()) {
            this.loopAnimation.stop();
        }
        stopAnimation();
        this.currentState = null;
        this.layout.setVisibility(0);
        this.layout.setAlpha(1.0f);
    }

    public void select(com.navdy.hud.app.ui.component.vlist.VerticalList.Model model, int pos, int duration) {
    }

    public void copyAndPosition(android.widget.ImageView imageC, android.widget.TextView titleC, android.widget.TextView subTitleC, android.widget.TextView subTitle2C, boolean setImage) {
    }

    public void setItemState(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.State state, com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.AnimationType animation, int duration, boolean startFluctuator) {
        float iconScaleFactor = 0.0f;
        com.navdy.hud.app.ui.component.image.CrossFadeImageView.Mode mode = null;
        this.animatorSetBuilder = null;
        switch (state) {
            case SELECTED:
                iconScaleFactor = 1.0f;
                mode = com.navdy.hud.app.ui.component.image.CrossFadeImageView.Mode.BIG;
                break;
            case UNSELECTED:
                iconScaleFactor = 0.6f;
                mode = com.navdy.hud.app.ui.component.image.CrossFadeImageView.Mode.SMALL;
                break;
        }
        switch (animation) {
            case NONE:
            case INIT:
                this.imageContainer.setScaleX(iconScaleFactor);
                this.imageContainer.setScaleY(iconScaleFactor);
                this.crossFadeImageView.setMode(mode);
                if (state == com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.State.SELECTED) {
                    this.loopImageView.setVisibility(0);
                    this.loopImageView.setAlpha(1.0f);
                    this.unSelImageView.setVisibility(8);
                    this.unSelImageView.setAlpha(0.0f);
                    if (this.loopAnimation != null && !this.loopAnimation.isRunning()) {
                        this.loopAnimation.start();
                        return;
                    }
                    return;
                }
                this.loopImageView.setVisibility(8);
                this.loopImageView.setAlpha(0.0f);
                this.unSelImageView.setVisibility(0);
                this.unSelImageView.setAlpha(1.0f);
                if (this.loopAnimation != null && this.loopAnimation.isRunning()) {
                    this.loopAnimation.stop();
                    return;
                }
                return;
            case MOVE:
                this.itemAnimatorSet = new android.animation.AnimatorSet();
                android.animation.PropertyValuesHolder p1 = android.animation.PropertyValuesHolder.ofFloat(android.view.View.SCALE_X, new float[]{iconScaleFactor});
                android.animation.PropertyValuesHolder p2 = android.animation.PropertyValuesHolder.ofFloat(android.view.View.SCALE_Y, new float[]{iconScaleFactor});
                this.animatorSetBuilder = this.itemAnimatorSet.play(android.animation.ObjectAnimator.ofPropertyValuesHolder(this.imageContainer, new android.animation.PropertyValuesHolder[]{p1, p2}));
                if (this.crossFadeImageView.getMode() != mode) {
                    this.animatorSetBuilder.with(this.crossFadeImageView.getCrossFadeAnimator());
                }
                if (state == com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.State.SELECTED) {
                    this.itemAnimatorSet.addListener(this.loopStart);
                    this.animatorSetBuilder.with(android.animation.ObjectAnimator.ofFloat(this.loopImageView, android.view.View.ALPHA, new float[]{1.0f}));
                    this.animatorSetBuilder.with(android.animation.ObjectAnimator.ofFloat(this.unSelImageView, android.view.View.ALPHA, new float[]{0.0f}));
                    return;
                }
                this.itemAnimatorSet.addListener(this.loopEnd);
                this.animatorSetBuilder.with(android.animation.ObjectAnimator.ofFloat(this.unSelImageView, android.view.View.ALPHA, new float[]{1.0f}));
                this.animatorSetBuilder.with(android.animation.ObjectAnimator.ofFloat(this.loopImageView, android.view.View.ALPHA, new float[]{0.0f}));
                return;
            default:
                return;
        }
    }

    public void preBind(com.navdy.hud.app.ui.component.vlist.VerticalList.Model model, com.navdy.hud.app.ui.component.vlist.VerticalList.ModelState modelState) {
        this.crossFadeImageView.setSmallAlpha(-1.0f);
        ((com.navdy.hud.app.ui.component.image.IconColorImageView) this.crossFadeImageView.getBig()).setIconShape(model.iconShape);
        ((com.navdy.hud.app.ui.component.image.IconColorImageView) this.crossFadeImageView.getSmall()).setIconShape(model.iconShape);
    }

    public void bind(com.navdy.hud.app.ui.component.vlist.VerticalList.Model model, com.navdy.hud.app.ui.component.vlist.VerticalList.ModelState modelState) {
        setIcon(model.icon, model.iconSelectedColor, model.iconDeselectedColor);
    }

    private void setIcon(int icon, int iconSelectedColor, int iconDeselectedColor) {
        com.navdy.hud.app.ui.component.image.IconColorImageView big = (com.navdy.hud.app.ui.component.image.IconColorImageView) this.crossFadeImageView.getBig();
        big.setDraw(true);
        big.setIcon(icon, iconSelectedColor, null, 0.83f);
        com.navdy.hud.app.ui.component.image.IconColorImageView small = (com.navdy.hud.app.ui.component.image.IconColorImageView) this.crossFadeImageView.getSmall();
        small.setDraw(true);
        small.setIcon(icon, iconDeselectedColor, null, 0.83f);
    }
}
