package com.navdy.hud.app.ui.component.mainmenu;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u0084\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0010!\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\u0001\u00a2\u0006\u0002\u0010\tJ(\u0010\u0019\u001a\u0004\u0018\u00010\u00012\b\u0010\b\u001a\u0004\u0018\u00010\u00012\b\u0010\u001a\u001a\u0004\u0018\u00010\u00182\b\u0010\u001b\u001a\u0004\u0018\u00010\u0018H\u0016J\b\u0010\u001c\u001a\u00020\u000bH\u0016J\u0010\u0010\u001d\u001a\n\u0012\u0004\u0012\u00020\u000f\u0018\u00010\u001eH\u0016J\u0012\u0010\u001f\u001a\u0004\u0018\u00010\u000f2\u0006\u0010 \u001a\u00020\u000bH\u0016J\n\u0010!\u001a\u0004\u0018\u00010\"H\u0016J\n\u0010#\u001a\u0004\u0018\u00010$H\u0016J\b\u0010%\u001a\u00020&H\u0016J\b\u0010'\u001a\u00020&H\u0016J\u0018\u0010(\u001a\u00020&2\u0006\u0010)\u001a\u00020\u000b2\u0006\u0010 \u001a\u00020\u000bH\u0016J.\u0010*\u001a\u00020+2\b\u0010,\u001a\u0004\u0018\u00010\u000f2\b\u0010-\u001a\u0004\u0018\u00010.2\u0006\u0010 \u001a\u00020\u000b2\b\u0010/\u001a\u0004\u0018\u000100H\u0016J\b\u00101\u001a\u00020+H\u0016J\b\u00102\u001a\u00020+H\u0016J\u0012\u00103\u001a\u00020+2\b\u00104\u001a\u0004\u0018\u000105H\u0016J\b\u00106\u001a\u00020+H\u0016J\u0012\u00107\u001a\u00020+2\b\u00108\u001a\u0004\u0018\u000109H\u0016J\u0010\u0010:\u001a\u00020&2\u0006\u00104\u001a\u000205H\u0016J\u0010\u0010;\u001a\u00020+2\u0006\u0010)\u001a\u00020\u000bH\u0016J\u0010\u0010<\u001a\u00020+2\u0006\u0010=\u001a\u00020\u000bH\u0016J\b\u0010>\u001a\u00020+H\u0016J\b\u0010?\u001a\u00020+H\u0016R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u000bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0017\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u000f0\u000e\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0001X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0012\u001a\u00020\u0013\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0015R\u000e\u0010\u0016\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0018X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006@"}, d2 = {"Lcom/navdy/hud/app/ui/component/mainmenu/SpeedLimitSettingMenu;", "Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;", "homeScreenView", "Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;", "vscrollComponent", "Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;", "presenter", "Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;", "parent", "(Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;)V", "backSelection", "", "backSelectionId", "homeScreenSpeedLimitSignWidgetSettings", "Ljava/util/ArrayList;", "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;", "getHomeScreenSpeedLimitSignWidgetSettings", "()Ljava/util/ArrayList;", "resources", "Landroid/content/res/Resources;", "getResources", "()Landroid/content/res/Resources;", "settingsColor", "settingsTitle", "", "getChildMenu", "args", "path", "getInitialSelection", "getItems", "", "getModelfromPos", "pos", "getScrollIndex", "Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;", "getType", "Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;", "isBindCallsEnabled", "", "isFirstItemEmpty", "isItemClickable", "id", "onBindToView", "", "model", "view", "Landroid/view/View;", "state", "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;", "onFastScrollEnd", "onFastScrollStart", "onItemSelected", "selection", "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;", "onScrollIdle", "onUnload", "level", "Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;", "selectItem", "setBackSelectionId", "setBackSelectionPos", "n", "setSelectedIcon", "showToolTip", "app_hudDebug"}, k = 1, mv = {1, 1, 6})
/* compiled from: SpeedLimitSettingMenu.kt */
public final class SpeedLimitSettingMenu implements com.navdy.hud.app.ui.component.mainmenu.IMenu {
    private int backSelection;
    private int backSelectionId;
    @org.jetbrains.annotations.NotNull
    private final java.util.ArrayList<com.navdy.hud.app.ui.component.vlist.VerticalList.Model> homeScreenSpeedLimitSignWidgetSettings = new java.util.ArrayList<>();
    private final com.navdy.hud.app.ui.component.homescreen.HomeScreenView homeScreenView;
    private final com.navdy.hud.app.ui.component.mainmenu.IMenu parent;
    private final com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.Presenter presenter;
    @org.jetbrains.annotations.NotNull
    private final android.content.res.Resources resources;
    private final int settingsColor;
    private final java.lang.String settingsTitle;
    private final com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent vscrollComponent;

    public SpeedLimitSettingMenu(@org.jetbrains.annotations.NotNull com.navdy.hud.app.ui.component.homescreen.HomeScreenView homeScreenView2, @org.jetbrains.annotations.NotNull com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent vscrollComponent2, @org.jetbrains.annotations.NotNull com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.Presenter presenter2, @org.jetbrains.annotations.NotNull com.navdy.hud.app.ui.component.mainmenu.IMenu parent2) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(homeScreenView2, "homeScreenView");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(vscrollComponent2, "vscrollComponent");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(presenter2, "presenter");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(parent2, "parent");
        this.homeScreenView = homeScreenView2;
        this.vscrollComponent = vscrollComponent2;
        this.presenter = presenter2;
        this.parent = parent2;
        android.content.res.Resources resources2 = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(resources2, "HudApplication.getAppContext().resources");
        this.resources = resources2;
        java.lang.String string = this.resources.getString(com.navdy.hud.app.R.string.show_speed_limit);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(string, "resources.getString(R.string.show_speed_limit)");
        this.settingsTitle = string;
        this.settingsColor = this.resources.getColor(com.navdy.hud.app.R.color.mm_map_options);
        int backColor = this.resources.getColor(com.navdy.hud.app.R.color.mm_back);
        com.navdy.hud.app.ui.component.vlist.VerticalList.Model back = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.menu_back, com.navdy.hud.app.R.drawable.icon_mm_back, backColor, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, backColor, this.resources.getString(com.navdy.hud.app.R.string.back), null);
        java.lang.String title = this.resources.getString(com.navdy.hud.app.R.string.always);
        int fluctuatorColor = this.resources.getColor(com.navdy.hud.app.R.color.mm_map_options);
        com.navdy.hud.app.ui.component.vlist.VerticalList.Model always = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.show_speed_limit_always, com.navdy.hud.app.R.drawable.icon_side_gauges, fluctuatorColor, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, fluctuatorColor, title, null);
        com.navdy.hud.app.ui.component.vlist.VerticalList.Model onlyWhenSpeeding = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.show_speed_limit_when_speeding, com.navdy.hud.app.R.drawable.icon_side_gauges, fluctuatorColor, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, fluctuatorColor, this.resources.getString(com.navdy.hud.app.R.string.only_when_speeding), null);
        com.navdy.hud.app.ui.component.vlist.VerticalList.Model never = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.show_speed_limit_never, com.navdy.hud.app.R.drawable.icon_side_gauges, fluctuatorColor, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, fluctuatorColor, this.resources.getString(com.navdy.hud.app.R.string.never), null);
        this.homeScreenSpeedLimitSignWidgetSettings.add(back);
        this.homeScreenSpeedLimitSignWidgetSettings.add(always);
        this.homeScreenSpeedLimitSignWidgetSettings.add(onlyWhenSpeeding);
        this.homeScreenSpeedLimitSignWidgetSettings.add(never);
    }

    @org.jetbrains.annotations.NotNull
    public final java.util.ArrayList<com.navdy.hud.app.ui.component.vlist.VerticalList.Model> getHomeScreenSpeedLimitSignWidgetSettings() {
        return this.homeScreenSpeedLimitSignWidgetSettings;
    }

    @org.jetbrains.annotations.NotNull
    public final android.content.res.Resources getResources() {
        return this.resources;
    }

    @org.jetbrains.annotations.Nullable
    public java.util.List<com.navdy.hud.app.ui.component.vlist.VerticalList.Model> getItems() {
        return this.homeScreenSpeedLimitSignWidgetSettings;
    }

    public int getInitialSelection() {
        return 1;
    }

    @org.jetbrains.annotations.Nullable
    public com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex getScrollIndex() {
        return null;
    }

    public void setBackSelectionPos(int n) {
        this.backSelection = n;
    }

    public void setBackSelectionId(int id) {
        this.backSelectionId = id;
    }

    public void setSelectedIcon() {
        this.vscrollComponent.setSelectedIconColorImage(com.navdy.hud.app.R.drawable.icon_side_gauges, this.settingsColor, null, 1.0f);
        this.vscrollComponent.selectedText.setText(this.settingsTitle);
    }

    public boolean selectItem(@org.jetbrains.annotations.NotNull com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState selection) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(selection, "selection");
        switch (selection.id) {
            case com.navdy.hud.app.R.id.menu_back /*2131624007*/:
                this.presenter.loadMenu(this.parent, com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel.BACK_TO_PARENT, this.backSelection, 0);
                break;
            case com.navdy.hud.app.R.id.show_speed_limit_always /*2131624069*/:
                this.homeScreenView.setAlwaysShowSpeedLimitSign(true);
                this.presenter.loadMenu(this.parent, com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel.BACK_TO_PARENT, this.backSelection, 0);
                break;
            case com.navdy.hud.app.R.id.show_speed_limit_never /*2131624070*/:
                this.homeScreenView.setIsWidgetEnabled(com.navdy.hud.app.R.id.home_screen_speed_limit_sign, false);
                this.presenter.loadMenu(this.parent, com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel.BACK_TO_PARENT, this.backSelection, 0);
                break;
            case com.navdy.hud.app.R.id.show_speed_limit_when_speeding /*2131624071*/:
                this.homeScreenView.setIsWidgetEnabled(com.navdy.hud.app.R.id.home_screen_speed_limit_sign, true);
                this.homeScreenView.setAlwaysShowSpeedLimitSign(false);
                this.presenter.loadMenu(this.parent, com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel.BACK_TO_PARENT, this.backSelection, 0);
                break;
        }
        return false;
    }

    @org.jetbrains.annotations.Nullable
    public com.navdy.hud.app.ui.component.mainmenu.IMenu.Menu getType() {
        return com.navdy.hud.app.ui.component.mainmenu.IMenu.Menu.MAIN_OPTIONS;
    }

    public boolean isItemClickable(int id, int pos) {
        return true;
    }

    @org.jetbrains.annotations.Nullable
    public com.navdy.hud.app.ui.component.vlist.VerticalList.Model getModelfromPos(int pos) {
        return (com.navdy.hud.app.ui.component.vlist.VerticalList.Model) this.homeScreenSpeedLimitSignWidgetSettings.get(pos);
    }

    public boolean isBindCallsEnabled() {
        return false;
    }

    public void onBindToView(@org.jetbrains.annotations.Nullable com.navdy.hud.app.ui.component.vlist.VerticalList.Model model, @org.jetbrains.annotations.Nullable android.view.View view, int pos, @org.jetbrains.annotations.Nullable com.navdy.hud.app.ui.component.vlist.VerticalList.ModelState state) {
    }

    @org.jetbrains.annotations.Nullable
    public com.navdy.hud.app.ui.component.mainmenu.IMenu getChildMenu(@org.jetbrains.annotations.Nullable com.navdy.hud.app.ui.component.mainmenu.IMenu parent2, @org.jetbrains.annotations.Nullable java.lang.String args, @org.jetbrains.annotations.Nullable java.lang.String path) {
        return null;
    }

    public void onUnload(@org.jetbrains.annotations.Nullable com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel level) {
    }

    public void onItemSelected(@org.jetbrains.annotations.Nullable com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState selection) {
    }

    public void onScrollIdle() {
    }

    public void onFastScrollStart() {
    }

    public void onFastScrollEnd() {
    }

    public void showToolTip() {
    }

    public boolean isFirstItemEmpty() {
        return false;
    }
}
