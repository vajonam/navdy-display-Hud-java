package com.navdy.hud.app.ui.component.destination;

public final class DestinationPickerScreen$Module$$ModuleAdapter extends dagger.internal.ModuleAdapter<com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.Module> {
    private static final java.lang.Class<?>[] INCLUDES = new java.lang.Class[0];
    private static final java.lang.String[] INJECTS = {"members/com.navdy.hud.app.ui.component.destination.DestinationPickerView"};
    private static final java.lang.Class<?>[] STATIC_INJECTIONS = new java.lang.Class[0];

    public DestinationPickerScreen$Module$$ModuleAdapter() {
        super(com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.Module.class, INJECTS, STATIC_INJECTIONS, false, INCLUDES, true, false);
    }
}
