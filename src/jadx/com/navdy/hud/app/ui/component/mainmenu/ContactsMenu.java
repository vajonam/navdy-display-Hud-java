package com.navdy.hud.app.ui.component.mainmenu;

class ContactsMenu implements com.navdy.hud.app.ui.component.mainmenu.IMenu {
    private static final float CONTACT_PHOTO_OPACITY = 0.6f;
    private static final com.navdy.hud.app.ui.component.vlist.VerticalList.Model back;
    private static final int backColor = resources.getColor(com.navdy.hud.app.R.color.mm_back);
    private static final int contactColor = resources.getColor(com.navdy.hud.app.R.color.mm_contacts);
    private static final java.lang.String contactStr = resources.getString(com.navdy.hud.app.R.string.carousel_menu_contacts);
    /* access modifiers changed from: private */
    public static final android.os.Handler handler = new android.os.Handler(android.os.Looper.getMainLooper());
    private static final int noContactColor = resources.getColor(com.navdy.hud.app.R.color.icon_user_bg_4);
    private static final java.lang.String recentContactStr = resources.getString(com.navdy.hud.app.R.string.carousel_menu_recent_contacts_title);
    private static final com.navdy.hud.app.ui.component.vlist.VerticalList.Model recentContacts;
    private static final android.content.res.Resources resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
    private static final com.squareup.picasso.Transformation roundTransformation = new com.makeramen.RoundedTransformationBuilder().oval(true).build();
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.mainmenu.ContactsMenu.class);
    private int backSelection;
    private int backSelectionId;
    private com.squareup.otto.Bus bus;
    private java.util.List<com.navdy.hud.app.ui.component.vlist.VerticalList.Model> cachedList;
    private com.navdy.hud.app.ui.component.mainmenu.IMenu parent;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.Presenter presenter;
    private com.navdy.hud.app.ui.component.mainmenu.RecentContactsMenu recentContactsMenu;
    private java.util.List<com.navdy.hud.app.ui.component.vlist.VerticalList.Model> returnToCacheList;
    private com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent vscrollComponent;

    class Anon1 implements java.lang.Runnable {
        final /* synthetic */ java.io.File val$imagePath;
        final /* synthetic */ com.navdy.hud.app.ui.component.image.InitialsImageView val$imageView;
        final /* synthetic */ com.navdy.hud.app.ui.component.vlist.VerticalList.Model val$model;
        final /* synthetic */ int val$pos;

        /* renamed from: com.navdy.hud.app.ui.component.mainmenu.ContactsMenu$Anon1$Anon1 reason: collision with other inner class name */
        class C0034Anon1 implements java.lang.Runnable {
            C0034Anon1() {
            }

            public void run() {
                if (com.navdy.hud.app.ui.component.mainmenu.ContactsMenu.Anon1.this.val$imageView.getTag() == com.navdy.hud.app.ui.component.mainmenu.ContactsMenu.Anon1.this.val$model.state) {
                    com.navdy.hud.app.ui.component.mainmenu.ContactsMenu.this.setImage(com.navdy.hud.app.ui.component.mainmenu.ContactsMenu.Anon1.this.val$imagePath, com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.selectedIconSize, com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.selectedIconSize, com.navdy.hud.app.ui.component.mainmenu.ContactsMenu.Anon1.this.val$pos);
                }
            }
        }

        Anon1(java.io.File file, com.navdy.hud.app.ui.component.image.InitialsImageView initialsImageView, com.navdy.hud.app.ui.component.vlist.VerticalList.Model model, int i) {
            this.val$imagePath = file;
            this.val$imageView = initialsImageView;
            this.val$model = model;
            this.val$pos = i;
        }

        public void run() {
            if (this.val$imagePath.exists()) {
                com.navdy.hud.app.ui.component.mainmenu.ContactsMenu.handler.post(new com.navdy.hud.app.ui.component.mainmenu.ContactsMenu.Anon1.C0034Anon1());
            }
        }
    }

    class Anon2 implements com.squareup.picasso.Target {
        final /* synthetic */ int val$position;

        Anon2(int i) {
            this.val$position = i;
        }

        public void onBitmapLoaded(android.graphics.Bitmap bitmap, com.squareup.picasso.Picasso.LoadedFrom from) {
            com.navdy.hud.app.ui.component.mainmenu.ContactsMenu.this.presenter.refreshDataforPos(this.val$position);
        }

        public void onBitmapFailed(android.graphics.drawable.Drawable errorDrawable) {
        }

        public void onPrepareLoad(android.graphics.drawable.Drawable placeHolderDrawable) {
        }
    }

    static {
        java.lang.String title = resources.getString(com.navdy.hud.app.R.string.back);
        int fluctuatorColor = backColor;
        back = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.menu_back, com.navdy.hud.app.R.drawable.icon_mm_back, fluctuatorColor, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, fluctuatorColor, title, null);
        java.lang.String title2 = recentContactStr;
        int fluctuatorColor2 = contactColor;
        recentContacts = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.contacts_menu_recent, com.navdy.hud.app.R.drawable.icon_place_recent_2, fluctuatorColor2, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, fluctuatorColor2, title2, null);
    }

    ContactsMenu(com.squareup.otto.Bus bus2, com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent vscrollComponent2, com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.Presenter presenter2, com.navdy.hud.app.ui.component.mainmenu.IMenu parent2) {
        this.bus = bus2;
        this.vscrollComponent = vscrollComponent2;
        this.presenter = presenter2;
        this.parent = parent2;
    }

    public java.util.List<com.navdy.hud.app.ui.component.vlist.VerticalList.Model> getItems() {
        if (this.cachedList != null) {
            return this.cachedList;
        }
        java.util.List<com.navdy.hud.app.ui.component.vlist.VerticalList.Model> list = new java.util.ArrayList<>();
        this.returnToCacheList = new java.util.ArrayList();
        com.navdy.hud.app.framework.contacts.FavoriteContactsManager favoriteContactsManager = com.navdy.hud.app.framework.contacts.FavoriteContactsManager.getInstance();
        if (favoriteContactsManager.isAllowedToReceiveContacts()) {
            list.add(back);
            list.add(recentContacts);
            int counter = 0;
            java.util.List<com.navdy.hud.app.framework.contacts.Contact> favContacts = favoriteContactsManager.getFavoriteContacts();
            if (favContacts != null) {
                sLogger.v("favorite contacts:" + favContacts.size());
                for (com.navdy.hud.app.framework.contacts.Contact contact : favContacts) {
                    int counter2 = counter + 1;
                    com.navdy.hud.app.ui.component.vlist.VerticalList.Model model = buildModel(counter, contact.name, contact.formattedNumber, contact.numberTypeStr, contact.defaultImageIndex, contact.numberType, contact.initials);
                    model.state = contact;
                    list.add(model);
                    this.returnToCacheList.add(model);
                    counter = counter2;
                }
            } else {
                sLogger.v("no favorite contacts");
            }
            this.cachedList = list;
            return list;
        }
        list.add(getContactsNotAllowed());
        return list;
    }

    public int getInitialSelection() {
        if (this.cachedList == null) {
            return 0;
        }
        if (this.cachedList.size() >= 3) {
            return 2;
        }
        return 1;
    }

    public com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex getScrollIndex() {
        return null;
    }

    public void setBackSelectionPos(int n) {
        this.backSelection = n;
    }

    public void setBackSelectionId(int id) {
        this.backSelectionId = id;
    }

    public void setSelectedIcon() {
        this.vscrollComponent.setSelectedIconColorImage(com.navdy.hud.app.R.drawable.icon_mm_contacts_2, contactColor, null, 1.0f);
        this.vscrollComponent.selectedText.setText(contactStr);
    }

    public com.navdy.hud.app.ui.component.vlist.VerticalList.Model getModelfromPos(int pos) {
        if (this.cachedList == null || this.cachedList.size() <= pos) {
            return null;
        }
        return (com.navdy.hud.app.ui.component.vlist.VerticalList.Model) this.cachedList.get(pos);
    }

    public boolean isBindCallsEnabled() {
        return true;
    }

    public void onBindToView(com.navdy.hud.app.ui.component.vlist.VerticalList.Model model, android.view.View view, int pos, com.navdy.hud.app.ui.component.vlist.VerticalList.ModelState state) {
        java.lang.String number;
        if (model.state != null) {
            com.navdy.hud.app.ui.component.image.InitialsImageView imageView = (com.navdy.hud.app.ui.component.image.InitialsImageView) com.navdy.hud.app.ui.component.vlist.VerticalList.findImageView(view);
            com.navdy.hud.app.ui.component.image.InitialsImageView smallImageView = (com.navdy.hud.app.ui.component.image.InitialsImageView) com.navdy.hud.app.ui.component.vlist.VerticalList.findSmallImageView(view);
            com.navdy.hud.app.ui.component.image.CrossFadeImageView crossFadeImageView = (com.navdy.hud.app.ui.component.image.CrossFadeImageView) com.navdy.hud.app.ui.component.vlist.VerticalList.findCrossFadeImageView(view);
            imageView.setTag(null);
            if (model.state instanceof com.navdy.hud.app.framework.recentcall.RecentCall) {
                number = ((com.navdy.hud.app.framework.recentcall.RecentCall) model.state).number;
            } else {
                number = ((com.navdy.hud.app.framework.contacts.Contact) model.state).number;
            }
            java.io.File imagePath = com.navdy.hud.app.framework.contacts.PhoneImageDownloader.getInstance().getImagePath(number, com.navdy.service.library.events.photo.PhotoType.PHOTO_CONTACT);
            android.graphics.Bitmap bitmap = com.navdy.hud.app.util.picasso.PicassoUtil.getBitmapfromCache(imagePath);
            if (bitmap != null) {
                imageView.setTag(null);
                imageView.setInitials(null, com.navdy.hud.app.ui.component.image.InitialsImageView.Style.DEFAULT);
                imageView.setImageBitmap(bitmap);
                smallImageView.setInitials(null, com.navdy.hud.app.ui.component.image.InitialsImageView.Style.DEFAULT);
                smallImageView.setImageBitmap(bitmap);
                smallImageView.setAlpha(0.6f);
                crossFadeImageView.setSmallAlpha(0.6f);
                state.updateImage = false;
                state.updateSmallImage = false;
                return;
            }
            imageView.setTag(model.state);
            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.ui.component.mainmenu.ContactsMenu.Anon1(imagePath, imageView, model, pos), 1);
        }
    }

    public com.navdy.hud.app.ui.component.mainmenu.IMenu getChildMenu(com.navdy.hud.app.ui.component.mainmenu.IMenu parent2, java.lang.String args, java.lang.String path) {
        return null;
    }

    public void onUnload(com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel level) {
        switch (level) {
            case CLOSE:
                if (this.returnToCacheList != null) {
                    sLogger.v("cm:unload add to cache");
                    com.navdy.hud.app.ui.component.vlist.VerticalModelCache.addToCache(this.returnToCacheList);
                    this.returnToCacheList = null;
                }
                if (this.recentContactsMenu != null) {
                    this.recentContactsMenu.onUnload(com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel.CLOSE);
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void onItemSelected(com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState selection) {
    }

    public void onScrollIdle() {
    }

    public void onFastScrollStart() {
    }

    public void onFastScrollEnd() {
    }

    public void showToolTip() {
    }

    public boolean isFirstItemEmpty() {
        return true;
    }

    public boolean selectItem(com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState selection) {
        sLogger.v("select id:" + selection.id + " pos:" + selection.pos);
        switch (selection.id) {
            case com.navdy.hud.app.R.id.contacts_menu_recent /*2131623945*/:
                sLogger.v("recent contacts");
                com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("recent_contacts");
                if (this.recentContactsMenu == null) {
                    this.recentContactsMenu = new com.navdy.hud.app.ui.component.mainmenu.RecentContactsMenu(this.bus, this.vscrollComponent, this.presenter, this);
                }
                this.presenter.loadMenu(this.recentContactsMenu, com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel.SUB_LEVEL, selection.pos, 0);
                break;
            case com.navdy.hud.app.R.id.menu_back /*2131624007*/:
                sLogger.v("back");
                com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("back");
                this.presenter.loadMenu(this.parent, com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel.BACK_TO_PARENT, this.backSelection, this.backSelectionId);
                this.backSelectionId = 0;
                break;
            default:
                sLogger.v("contact options");
                com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("contact_options");
                com.navdy.hud.app.framework.contacts.Contact contact = (com.navdy.hud.app.framework.contacts.Contact) ((com.navdy.hud.app.ui.component.vlist.VerticalList.Model) this.cachedList.get(selection.pos)).state;
                java.util.List<com.navdy.hud.app.framework.contacts.Contact> list = new java.util.ArrayList<>(1);
                list.add(contact);
                this.presenter.loadMenu(new com.navdy.hud.app.ui.component.mainmenu.ContactOptionsMenu(list, this.vscrollComponent, this.presenter, this, this.bus), com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel.SUB_LEVEL, selection.pos, 0);
                break;
        }
        return true;
    }

    public com.navdy.hud.app.ui.component.mainmenu.IMenu.Menu getType() {
        return com.navdy.hud.app.ui.component.mainmenu.IMenu.Menu.CONTACTS;
    }

    public boolean isItemClickable(int id, int pos) {
        return true;
    }

    /* access modifiers changed from: 0000 */
    public com.navdy.hud.app.ui.component.vlist.VerticalList.Model buildModel(int id, java.lang.String contactName, java.lang.String formattedNumber, java.lang.String numberTypeStr, int defaultImageIndex, com.navdy.hud.app.framework.contacts.NumberType numberType, java.lang.String initials) {
        java.lang.String subTitle;
        com.navdy.hud.app.ui.component.vlist.VerticalList.Model model;
        com.navdy.hud.app.framework.contacts.ContactImageHelper contactImageHelper = com.navdy.hud.app.framework.contacts.ContactImageHelper.getInstance();
        if (android.text.TextUtils.isEmpty(contactName)) {
            model = com.navdy.hud.app.ui.component.vlist.viewholder.IconsTwoViewHolder.buildModel(id, com.navdy.hud.app.R.drawable.icon_user_bg_4, com.navdy.hud.app.R.drawable.icon_user_numberonly, noContactColor, -1, formattedNumber, null);
        } else {
            int largeImageRes = contactImageHelper.getResourceId(defaultImageIndex);
            int fluctuator = contactImageHelper.getResourceColor(defaultImageIndex);
            if (numberType != com.navdy.hud.app.framework.contacts.NumberType.OTHER) {
                subTitle = numberTypeStr;
            } else {
                subTitle = formattedNumber;
            }
            model = com.navdy.hud.app.ui.component.vlist.viewholder.IconsTwoViewHolder.buildModel(id, largeImageRes, com.navdy.hud.app.R.drawable.icon_user_grey, fluctuator, -1, contactName, subTitle);
        }
        model.extras = new java.util.HashMap<>();
        model.extras.put(com.navdy.hud.app.ui.component.vlist.VerticalList.Model.INITIALS, initials);
        return model;
    }

    /* access modifiers changed from: private */
    public void setImage(java.io.File path, int width, int height, int position) {
        com.navdy.hud.app.util.picasso.PicassoUtil.getInstance().load(path).resize(width, height).transform(roundTransformation).into((com.squareup.picasso.Target) new com.navdy.hud.app.ui.component.mainmenu.ContactsMenu.Anon2(position));
    }

    private com.navdy.hud.app.ui.component.vlist.VerticalList.Model getContactsNotAllowed() {
        java.lang.String title;
        android.content.res.Resources resources2 = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        if (com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().isRemoteDeviceIos()) {
            title = resources2.getString(com.navdy.hud.app.R.string.allow_contact_access_iphone_title);
        } else {
            title = resources2.getString(com.navdy.hud.app.R.string.allow_contact_access_android_title);
        }
        return com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.R.id.menu_back, com.navdy.hud.app.R.drawable.icon_mm_back, contactColor, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, contactColor, title, null);
    }
}
