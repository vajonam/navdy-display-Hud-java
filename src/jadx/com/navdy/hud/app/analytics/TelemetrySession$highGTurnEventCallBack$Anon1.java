package com.navdy.hud.app.analytics;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\b\u0004"}, d2 = {"<anonymous>", "", "started", "", "invoke"}, k = 3, mv = {1, 1, 6})
/* compiled from: TelemetrySession.kt */
final class TelemetrySession$highGTurnEventCallBack$Anon1 extends kotlin.jvm.internal.Lambda implements kotlin.jvm.functions.Function1<java.lang.Boolean, kotlin.Unit> {
    public static final com.navdy.hud.app.analytics.TelemetrySession$highGTurnEventCallBack$Anon1 INSTANCE = new com.navdy.hud.app.analytics.TelemetrySession$highGTurnEventCallBack$Anon1();

    TelemetrySession$highGTurnEventCallBack$Anon1() {
        super(1);
    }

    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj) {
        invoke(((java.lang.Boolean) obj).booleanValue());
        return kotlin.Unit.INSTANCE;
    }

    public final void invoke(boolean started) {
        if (started) {
            com.navdy.hud.app.analytics.TelemetrySession telemetrySession = com.navdy.hud.app.analytics.TelemetrySession.INSTANCE;
            telemetrySession.setSessionHighGCount(telemetrySession.getSessionHighGCount() + 1);
            com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getDataSource().interestingEventDetected(com.navdy.hud.app.analytics.TelemetrySession.InterestingEvent.HIGH_G_STARTED);
            com.navdy.hud.app.analytics.TelemetrySession.logger.d("High G maneuver confirmed and notified");
            return;
        }
        com.navdy.hud.app.analytics.TelemetrySession.logger.d("High G maneuver ended");
        com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getDataSource().interestingEventDetected(com.navdy.hud.app.analytics.TelemetrySession.InterestingEvent.HIGH_G_ENDED);
    }
}
