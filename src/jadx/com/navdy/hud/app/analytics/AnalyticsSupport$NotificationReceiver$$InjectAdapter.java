package com.navdy.hud.app.analytics;

public final class AnalyticsSupport$NotificationReceiver$$InjectAdapter extends dagger.internal.Binding<com.navdy.hud.app.analytics.AnalyticsSupport.NotificationReceiver> implements javax.inject.Provider<com.navdy.hud.app.analytics.AnalyticsSupport.NotificationReceiver>, dagger.MembersInjector<com.navdy.hud.app.analytics.AnalyticsSupport.NotificationReceiver> {
    private dagger.internal.Binding<android.content.SharedPreferences> appPreferences;
    private dagger.internal.Binding<com.navdy.hud.app.device.PowerManager> powerManager;

    public AnalyticsSupport$NotificationReceiver$$InjectAdapter() {
        super("com.navdy.hud.app.analytics.AnalyticsSupport$NotificationReceiver", "members/com.navdy.hud.app.analytics.AnalyticsSupport$NotificationReceiver", false, com.navdy.hud.app.analytics.AnalyticsSupport.NotificationReceiver.class);
    }

    public void attach(dagger.internal.Linker linker) {
        this.powerManager = linker.requestBinding("com.navdy.hud.app.device.PowerManager", com.navdy.hud.app.analytics.AnalyticsSupport.NotificationReceiver.class, getClass().getClassLoader());
        this.appPreferences = linker.requestBinding("android.content.SharedPreferences", com.navdy.hud.app.analytics.AnalyticsSupport.NotificationReceiver.class, getClass().getClassLoader());
    }

    public void getDependencies(java.util.Set<dagger.internal.Binding<?>> set, java.util.Set<dagger.internal.Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.powerManager);
        injectMembersBindings.add(this.appPreferences);
    }

    public com.navdy.hud.app.analytics.AnalyticsSupport.NotificationReceiver get() {
        com.navdy.hud.app.analytics.AnalyticsSupport.NotificationReceiver result = new com.navdy.hud.app.analytics.AnalyticsSupport.NotificationReceiver();
        injectMembers(result);
        return result;
    }

    public void injectMembers(com.navdy.hud.app.analytics.AnalyticsSupport.NotificationReceiver object) {
        object.powerManager = (com.navdy.hud.app.device.PowerManager) this.powerManager.get();
        object.appPreferences = (android.content.SharedPreferences) this.appPreferences.get();
    }
}
