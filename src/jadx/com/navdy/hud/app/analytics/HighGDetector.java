package com.navdy.hud.app.analytics;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0007\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\b\n\u0002\u0010\u000b\n\u0002\b\n\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\bJd\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u00032\u0006\u0010\u001c\u001a\u00020\u00032\u0006\u0010\u001d\u001a\u00020\u00062!\u0010\u001e\u001a\u001d\u0012\u0013\u0012\u00110\u0003\u00a2\u0006\f\b \u0012\b\b!\u0012\u0004\b\b(\"\u0012\u0004\u0012\u00020\u000f0\u001f2!\u0010#\u001a\u001d\u0012\u0013\u0012\u00110\u000f\u00a2\u0006\f\b \u0012\b\b!\u0012\u0004\b\b($\u0012\u0004\u0012\u00020\u001a0\u001fJ\u0006\u0010%\u001a\u00020\u001aR\u001a\u0010\t\u001a\u00020\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\n\u0010\u000b\"\u0004\b\f\u0010\rR\u001a\u0010\u000e\u001a\u00020\u000fX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0010\u0010\u0011\"\u0004\b\u0012\u0010\u0013R\u000e\u0010\u0004\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0014\u001a\u00020\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0015\u0010\u000b\"\u0004\b\u0016\u0010\rR\u000e\u0010\u0007\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0017\u001a\u00020\u000fX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0017\u0010\u0011\"\u0004\b\u0018\u0010\u0013R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006&"}, d2 = {"Lcom/navdy/hud/app/analytics/HighGDetector;", "", "gUpperWaterMark", "", "gLowerWaterMark", "minimumTimeBetweenEvents", "", "highGThresholdTime", "(FFJJ)V", "confirmedHighGManeuverEndTime", "getConfirmedHighGManeuverEndTime", "()J", "setConfirmedHighGManeuverEndTime", "(J)V", "confirmedHighGManeuverOngoing", "", "getConfirmedHighGManeuverOngoing", "()Z", "setConfirmedHighGManeuverOngoing", "(Z)V", "highGManeuverStartTime", "getHighGManeuverStartTime", "setHighGManeuverStartTime", "isAboveWaterMark", "setAboveWaterMark", "newGValue", "", "g", "gAngle", "timeStamp", "angleFilter", "Lkotlin/Function1;", "Lkotlin/ParameterName;", "name", "angle", "highGEventCallBack", "started", "reset", "app_hudDebug"}, k = 1, mv = {1, 1, 6})
/* compiled from: HighGDetector.kt */
public final class HighGDetector {
    private long confirmedHighGManeuverEndTime;
    private boolean confirmedHighGManeuverOngoing;
    private final float gLowerWaterMark;
    private final float gUpperWaterMark;
    private long highGManeuverStartTime;
    private final long highGThresholdTime;
    private boolean isAboveWaterMark;
    private final long minimumTimeBetweenEvents;

    public HighGDetector(float gUpperWaterMark2, float gLowerWaterMark2, long minimumTimeBetweenEvents2, long highGThresholdTime2) {
        this.gUpperWaterMark = gUpperWaterMark2;
        this.gLowerWaterMark = gLowerWaterMark2;
        this.minimumTimeBetweenEvents = minimumTimeBetweenEvents2;
        this.highGThresholdTime = highGThresholdTime2;
    }

    public final boolean isAboveWaterMark() {
        return this.isAboveWaterMark;
    }

    public final void setAboveWaterMark(boolean z) {
        this.isAboveWaterMark = z;
    }

    public final long getHighGManeuverStartTime() {
        return this.highGManeuverStartTime;
    }

    public final void setHighGManeuverStartTime(long j) {
        this.highGManeuverStartTime = j;
    }

    public final long getConfirmedHighGManeuverEndTime() {
        return this.confirmedHighGManeuverEndTime;
    }

    public final void setConfirmedHighGManeuverEndTime(long j) {
        this.confirmedHighGManeuverEndTime = j;
    }

    public final boolean getConfirmedHighGManeuverOngoing() {
        return this.confirmedHighGManeuverOngoing;
    }

    public final void setConfirmedHighGManeuverOngoing(boolean z) {
        this.confirmedHighGManeuverOngoing = z;
    }

    public final void newGValue(float g, float gAngle, long timeStamp, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.lang.Float, java.lang.Boolean> angleFilter, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.lang.Boolean, kotlin.Unit> highGEventCallBack) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(angleFilter, "angleFilter");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(highGEventCallBack, "highGEventCallBack");
        if (g > this.gUpperWaterMark) {
            if (((java.lang.Boolean) angleFilter.invoke(java.lang.Float.valueOf(gAngle))).booleanValue()) {
                if (this.highGManeuverStartTime == 0 && com.navdy.hud.app.analytics.TelemetrySessionKt.timeSince(timeStamp, this.confirmedHighGManeuverEndTime) > this.minimumTimeBetweenEvents) {
                    this.highGManeuverStartTime = timeStamp;
                }
                if (this.isAboveWaterMark && !this.confirmedHighGManeuverOngoing && this.highGManeuverStartTime != 0 && com.navdy.hud.app.analytics.TelemetrySessionKt.timeSince(timeStamp, this.highGManeuverStartTime) > this.highGThresholdTime) {
                    this.confirmedHighGManeuverOngoing = true;
                    highGEventCallBack.invoke(java.lang.Boolean.valueOf(true));
                }
                this.isAboveWaterMark = true;
            }
        } else if (g < this.gLowerWaterMark) {
            if (this.highGManeuverStartTime != 0) {
                this.highGManeuverStartTime = 0;
                if (this.confirmedHighGManeuverOngoing) {
                    this.confirmedHighGManeuverEndTime = timeStamp;
                    this.confirmedHighGManeuverOngoing = false;
                    highGEventCallBack.invoke(java.lang.Boolean.valueOf(false));
                }
            }
            this.isAboveWaterMark = false;
        }
    }

    public final void reset() {
        this.highGManeuverStartTime = 0;
        this.confirmedHighGManeuverEndTime = 0;
        this.isAboveWaterMark = false;
        this.confirmedHighGManeuverOngoing = false;
    }
}
