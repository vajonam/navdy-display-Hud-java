package com.navdy.hud.app.analytics;

public class AnalyticsSupport {
    private static final java.lang.String ANALYTICS_EVENT_ADAPTIVE_BRIGHTNESS_ADJUSTMENT_CHANGE = "Adaptive_Brightness_Adjustment_Change";
    private static final java.lang.String ANALYTICS_EVENT_ATTEMPT_TO_CONNECT = "Attempt_To_Connect";
    private static final java.lang.String ANALYTICS_EVENT_BAD_ROUTE_POSITION = "Route_Bad_Pos";
    private static final java.lang.String ANALYTICS_EVENT_BRIGHTNESS_ADJUSTMENT_CHANGE = "Brightness_Adjustment_Change";
    private static final java.lang.String ANALYTICS_EVENT_BRIGHTNESS_CHANGE = "Brightness_Change";
    private static final java.lang.String ANALYTICS_EVENT_CPU_OVERHEAT = "Cpu_Overheat";
    private static final java.lang.String ANALYTICS_EVENT_DASH_GAUGE_SETTING = "Dash_Gauge_Setting";
    private static final java.lang.String ANALYTICS_EVENT_DESTINATION_SUGGESTION = "Destination_Suggestion_Show";
    private static final java.lang.String ANALYTICS_EVENT_DIAL_UPDATE_PROMPT = "Dial_Update_Prompt";
    private static final java.lang.String ANALYTICS_EVENT_DISPLAY_UPDATE_PROMPT = "Display_Update_Prompt";
    private static final java.lang.String ANALYTICS_EVENT_DISTANCE_TRAVELLED = "Distance_Travelled";
    private static final java.lang.String ANALYTICS_EVENT_DRIVER_PREFERENCES = "Driver_Preferences";
    private static final java.lang.String ANALYTICS_EVENT_FUEL_LEVEL = "Fuel_Level";
    public static final java.lang.String ANALYTICS_EVENT_GLANCE_ADVANCE = "Glance_Advance";
    public static final java.lang.String ANALYTICS_EVENT_GLANCE_DELETE_ALL = "Glance_Delete_All";
    public static final java.lang.String ANALYTICS_EVENT_GLANCE_DISMISS = "Glance_Dismiss";
    public static final java.lang.String ANALYTICS_EVENT_GLANCE_OPEN_FULL = "Glance_Open_Full";
    public static final java.lang.String ANALYTICS_EVENT_GLANCE_OPEN_MINI = "Glance_Open_Mini";
    private static final java.lang.String ANALYTICS_EVENT_GLYMPSE_SENT = "Glympse_Sent";
    private static final java.lang.String ANALYTICS_EVENT_GLYMPSE_VIEWED = "Glympse_Viewed";
    public static final java.lang.String ANALYTICS_EVENT_GPS_ACCURACY_STATISTICS = "GPS_Accuracy";
    public static final java.lang.String ANALYTICS_EVENT_GPS_ACQUIRE_LOCATION = "GPS_Acquired_Location";
    public static final java.lang.String ANALYTICS_EVENT_GPS_ATTEMPT_ACQUIRE_LOCATION = "GPS_Attempt_Acquire_Location";
    public static final java.lang.String ANALYTICS_EVENT_GPS_CALIBRATION_IMU_DONE = "GPS_Calibration_IMU_Done";
    public static final java.lang.String ANALYTICS_EVENT_GPS_CALIBRATION_LOST = "GPS_Calibration_Lost";
    public static final java.lang.String ANALYTICS_EVENT_GPS_CALIBRATION_SENSOR_DONE = "GPS_Calibration_Sensor_Done";
    public static final java.lang.String ANALYTICS_EVENT_GPS_CALIBRATION_START = "GPS_Calibration_Start";
    public static final java.lang.String ANALYTICS_EVENT_GPS_CALIBRATION_VIN_SWITCH = "GPS_Calibration_VinSwitch";
    public static final java.lang.String ANALYTICS_EVENT_GPS_LOST_LOCATION = "GPS_Lost_Signal";
    public static final java.lang.String ANALYTICS_EVENT_IAP_FAILURE = "Mobile_iAP_Failure";
    public static final java.lang.String ANALYTICS_EVENT_IAP_RETRY_SUCCESS = "Mobile_iAP_Retry_Success";
    private static final java.lang.String ANALYTICS_EVENT_KEY_FAILED = "Key_Failed";
    private static final java.lang.String ANALYTICS_EVENT_MENU_SELECTION = "Menu_Selection";
    private static final java.lang.String ANALYTICS_EVENT_MILEAGE = "Navdy_Mileage";
    private static final java.lang.String ANALYTICS_EVENT_MOBILE_APP_CONNECT = "Mobile_App_Connect";
    private static final java.lang.String ANALYTICS_EVENT_MOBILE_APP_DISCONNECT = "Mobile_App_Disconnect";
    private static final java.lang.String ANALYTICS_EVENT_MUSIC_ACTION = "Music_Action";
    private static final java.lang.String ANALYTICS_EVENT_NAVIGATION_ARRIVED = "Navigation_Arrived";
    private static final java.lang.String ANALYTICS_EVENT_NAVIGATION_DESTINATION_SET = "Navigation_Destination_Set";
    private static final java.lang.String ANALYTICS_EVENT_NAVIGATION_MANEUVER_MISSED = "Navigation_Maneuver_Missed";
    private static final java.lang.String ANALYTICS_EVENT_NAVIGATION_MANEUVER_REQUIRED = "Navigation_Maneuver_Required";
    private static final java.lang.String ANALYTICS_EVENT_NAVIGATION_MANEUVER_WARNING = "Navigation_Maneuver_Warning";
    private static final java.lang.String ANALYTICS_EVENT_NAVIGATION_PREFERENCES = "Navigation_Preferences";
    private static final java.lang.String ANALYTICS_EVENT_NAVIGATION_REGION_CHANGE = "Map_Region_Change";
    private static final java.lang.String ANALYTICS_EVENT_NEARBY_SEARCH_ARRIVED = "Nearby_Search_Arrived";
    private static final java.lang.String ANALYTICS_EVENT_NEARBY_SEARCH_CLOSED = "Nearby_Search_Closed";
    private static final java.lang.String ANALYTICS_EVENT_NEARBY_SEARCH_DISMISS = "Nearby_Search_Dismiss";
    private static final java.lang.String ANALYTICS_EVENT_NEARBY_SEARCH_ENDED = "Nearby_Search_Ended";
    private static final java.lang.String ANALYTICS_EVENT_NEARBY_SEARCH_NO_RESULTS = "Nearby_Search_No_Results";
    private static final java.lang.String ANALYTICS_EVENT_NEARBY_SEARCH_RESULTS_CLOSE = "Nearby_Search_Results_Close";
    private static final java.lang.String ANALYTICS_EVENT_NEARBY_SEARCH_RESULTS_DISMISS = "Nearby_Search_Results_Dismiss";
    private static final java.lang.String ANALYTICS_EVENT_NEARBY_SEARCH_RESULTS_VIEW = "Nearby_Search_Results_View";
    private static final java.lang.String ANALYTICS_EVENT_NEARBY_SEARCH_RETURN_MM = "Nearby_Search_Return_Main_Menu";
    private static final java.lang.String ANALYTICS_EVENT_NEARBY_SEARCH_SELECTION = "Nearby_Search_Selection";
    private static final java.lang.String ANALYTICS_EVENT_NEARBY_SEARCH_TRIP_CANCELLED = "Nearby_Search_Trip_Cancelled";
    private static final java.lang.String ANALYTICS_EVENT_NEARBY_SEARCH_TRIP_INITIATED = "Nearby_Search_Trip_Initiated";
    public static final java.lang.String ANALYTICS_EVENT_NEW_DEVICE = "Analytics_New_Device";
    private static final java.lang.String ANALYTICS_EVENT_OBD_CONNECTED = "OBD_Connected";
    private static final java.lang.String ANALYTICS_EVENT_OBD_LISTENING_POSTED = "OBD_Listening_Posted";
    private static final java.lang.String ANALYTICS_EVENT_OBD_LISTENING_STATUS = "OBD_Listening_Status";
    private static final java.lang.String ANALYTICS_EVENT_OFFLINE_MAPS_UPDATED = "Offline_Maps_Updated";
    private static final java.lang.String ANALYTICS_EVENT_OPTION_SELECTED = "Option_Selected";
    private static final java.lang.String ANALYTICS_EVENT_OTADOWNLOAD = "OTA_Download";
    private static final java.lang.String ANALYTICS_EVENT_OTAINSTALL = "OTA_Install";
    private static final java.lang.String ANALYTICS_EVENT_OTA_INSTALL_RESULT = "OTA_Install_Result";
    private static final java.lang.String ANALYTICS_EVENT_PHONE_DISCONNECTED = "Phone_Disconnected";
    private static final java.lang.String ANALYTICS_EVENT_ROUTE_COMPLETED = "Route_Completed";
    private static final java.lang.String ANALYTICS_EVENT_SHUTDOWN = "Shutdown";
    private static final java.lang.String ANALYTICS_EVENT_SMS_SENT = "sms_sent";
    private static final java.lang.String ANALYTICS_EVENT_SWIPED_PREFIX = "Swiped_";
    private static final java.lang.String ANALYTICS_EVENT_TEMPERATURE_CHANGE = "Temperature_Change";
    private static final java.lang.String ANALYTICS_EVENT_VEHICLE_TELEMETRY_DATA = "Vehicle_Telemetry_Data";
    private static final java.lang.String ANALYTICS_EVENT_VOICE_SEARCH_ANDROID = "Voice_Search_Android";
    private static final java.lang.String ANALYTICS_EVENT_VOICE_SEARCH_IOS = "Voice_Search_iOS";
    private static final java.lang.String ANALYTICS_EVENT_WAKEUP = "Wakeup";
    private static final java.lang.String ANALYTICS_EVENT_ZERO_FUEL_DATA = "OBD_Zero_Fuel_Level";
    public static final java.lang.String ANALYTICS_INTENT = "com.navdy.hud.app.analytics.AnalyticsEvent";
    public static final java.lang.String ANALYTICS_INTENT_EXTRA_ARGUMENTS = "arguments";
    public static final java.lang.String ANALYTICS_INTENT_EXTRA_INCLUDE_REMOTE_DEVICE_INFO = "include_device_info";
    public static final java.lang.String ANALYTICS_INTENT_EXTRA_TAG_NAME = "tag";
    private static final java.lang.String ATTR_BRIGHTNESS_ADJ_NEW = "New";
    private static final java.lang.String ATTR_BRIGHTNESS_ADJ_PREVIOUS = "Previous";
    private static final java.lang.String ATTR_BRIGHTNESS_BRIGHTNESS = "Brightness";
    private static final java.lang.String ATTR_BRIGHTNESS_LIGHT_SENSOR = "Light_Sensor";
    private static final java.lang.String ATTR_BRIGHTNESS_NEW = "New";
    private static final java.lang.String ATTR_BRIGHTNESS_PREVIOUS = "Previous";
    private static final java.lang.String ATTR_BRIGHTNESS_RAW1_SENSOR = "Raw1_Sensor";
    private static final java.lang.String ATTR_BRIGHTNESS_RAW_SENSOR = "Raw_Sensor";
    private static final java.lang.String ATTR_CONN_ATTEMPT_NEW_DEVICE = "Was_New_Device";
    private static final java.lang.String ATTR_CONN_ATTEMPT_TIME = "Time_To_Connect";
    private static final java.lang.String ATTR_CONN_CAR_MAKE = "Car_Make";
    private static final java.lang.String ATTR_CONN_CAR_MODEL = "Car_Model";
    private static final java.lang.String ATTR_CONN_CAR_YEAR = "Car_Year";
    private static final java.lang.String ATTR_CONN_DEVICE_ID = "Device_Id";
    private static final java.lang.String ATTR_CONN_DEVICE_INFO_DELAY = "Device_Info_Delay";
    private static final java.lang.String ATTR_CONN_LINK_DELAY = "Link_Delay";
    private static final java.lang.String ATTR_CONN_REMOTE_CLIENT_VERSION = "Remote_Client_Version";
    private static final java.lang.String ATTR_CONN_REMOTE_DEVICE_ID = "Remote_Device_Id";
    private static final java.lang.String ATTR_CONN_REMOTE_DEVICE_NAME = "Remote_Device_Name";
    private static final java.lang.String ATTR_CONN_REMOTE_PLATFORM = "Remote_Platform";
    private static final java.lang.String ATTR_CONN_VIN = "VIN";
    private static final java.lang.String ATTR_CPU_OVERHEAT_STATE = "State";
    private static final java.lang.String ATTR_CPU_OVERHEAT_UPTIME = "Uptime";
    private static final java.lang.String ATTR_DESTINATION_SUGGESTION_ACCEPTED = "Accepted";
    private static final java.lang.String ATTR_DESTINATION_SUGGESTION_TYPE = "Type";
    private static final java.lang.String ATTR_DISC_CAR_MAKE = "Car_Make";
    private static final java.lang.String ATTR_DISC_CAR_MODEL = "Car_Model";
    private static final java.lang.String ATTR_DISC_CAR_YEAR = "Car_Year";
    private static final java.lang.String ATTR_DISC_ELAPSED_TIME = "Elapsed_Time";
    private static final java.lang.String ATTR_DISC_FORCE_RECONNECT = "Force_Reconnect";
    private static final java.lang.String ATTR_DISC_FORCE_RECONNECT_REASON = "Force_Reconnect_Reason";
    private static final java.lang.String ATTR_DISC_MOVING = "While_Moving";
    private static final java.lang.String ATTR_DISC_PHONE_MAKE = "Phone_Make";
    private static final java.lang.String ATTR_DISC_PHONE_MODEL = "Phone_Model";
    private static final java.lang.String ATTR_DISC_PHONE_OS = "Phone_OS";
    private static final java.lang.String ATTR_DISC_PHONE_TYPE = "Phone_Type";
    private static final java.lang.String ATTR_DISC_REASON = "Reason";
    private static final java.lang.String ATTR_DISC_STATUS = "Status";
    private static final java.lang.String ATTR_DISC_VIN = "VIN";
    private static final java.lang.String ATTR_DISTANCE = "Distance";
    private static final java.lang.String ATTR_DRIVER_PREFERENCES_AUTO_ON = "Auto_On";
    private static final java.lang.String ATTR_DRIVER_PREFERENCES_DISPLAY_FORMAT = "Display_Format";
    private static final java.lang.String ATTR_DRIVER_PREFERENCES_DRIVING_BEHAVIOR_ALERTS = "Driving_Behavior_Alerts";
    private static final java.lang.String ATTR_DRIVER_PREFERENCES_FEATURE_MODE = "Feature_Mode";
    private static final java.lang.String ATTR_DRIVER_PREFERENCES_GESTURES = "Gestures";
    private static final java.lang.String ATTR_DRIVER_PREFERENCES_GLANCES = "Glances";
    private static final java.lang.String ATTR_DRIVER_PREFERENCES_LIMIT_BANDWIDTH = "Limit_Bandwidth";
    private static final java.lang.String ATTR_DRIVER_PREFERENCES_LOCALE = "Locale";
    private static final java.lang.String ATTR_DRIVER_PREFERENCES_MANUAL_ZOOM = "Manual_Zoom";
    private static final java.lang.String ATTR_DRIVER_PREFERENCES_OBD = "OBD";
    private static final java.lang.String ATTR_DRIVER_PREFERENCES_UNITS = "Units";
    private static final java.lang.String ATTR_DURATION = "Duration";
    private static final java.lang.String ATTR_EVENT_AVERAGE_MPG = "Event_Average_Mpg";
    private static final java.lang.String ATTR_EVENT_AVERAGE_RPM = "Event_Average_Rpm";
    private static final java.lang.String ATTR_EVENT_ENGINE_TEMPERATURE = "Event_Engine_Temperature";
    private static final java.lang.String ATTR_EVENT_FUEL_LEVEL = "Event_Fuel_Level";
    private static final java.lang.String ATTR_EVENT_MAX_G = "Event_Max_G";
    private static final java.lang.String ATTR_EVENT_MAX_G_ANGLE = "Event_Max_G_Angle";
    private static final java.lang.String ATTR_FUEL_CONSUMPTION = "Fuel_Consumption";
    private static final java.lang.String ATTR_FUEL_DISTANCE = "Distance";
    private static final java.lang.String ATTR_FUEL_LEVEL = "Fuel_Level";
    private static final java.lang.String ATTR_FUEL_VIN = "VIN";
    private static final java.lang.String ATTR_GAUGE_ID = "Gauge_name";
    private static final java.lang.String ATTR_GAUGE_SIDE = "Side";
    private static final java.lang.String ATTR_GAUGE_SIDE_LEFT = "Left";
    private static final java.lang.String ATTR_GAUGE_SIDE_RIGHT = "Right";
    private static final java.lang.String ATTR_GLANCE_NAME = "Glance_Name";
    private static final java.lang.String ATTR_GLANCE_NAV_METHOD = "Nav_Method";
    private static final java.lang.String ATTR_GLYMPSE_ID = "Id";
    private static final java.lang.String ATTR_GLYMPSE_MESSAGE = "Message";
    private static final java.lang.String ATTR_GLYMPSE_SHARE = "Share";
    private static final java.lang.String ATTR_GLYMPSE_SUCCESS = "Success";
    private static final java.lang.String ATTR_KEY_FAIL_ERROR = "Error";
    private static final java.lang.String ATTR_KEY_FAIL_KEY_TYPE = "Key_Type";
    private static final java.lang.String ATTR_MENU_NAME = "Name";
    private static final java.lang.String ATTR_MUSIC_ACTION_TYPE = "Type";
    private static final java.lang.String ATTR_NAVDY_KILOMETERS = "Navdy_Kilometers";
    private static final java.lang.String ATTR_NAVDY_USAGE_RATE = "Navdy_Usage_Rate";
    private static final java.lang.String ATTR_NAVIGATING = "Navigating";
    private static final java.lang.String ATTR_NAVIGATION_PREFERENCES_ALLOW_AUTO_TRAINS = "Allow_Auto_Trains";
    private static final java.lang.String ATTR_NAVIGATION_PREFERENCES_ALLOW_FERRIES = "Allow_Ferries";
    private static final java.lang.String ATTR_NAVIGATION_PREFERENCES_ALLOW_HIGHWAYS = "Allow_Highways";
    private static final java.lang.String ATTR_NAVIGATION_PREFERENCES_ALLOW_HOV_LANES = "Allow_HOV_Lanes";
    private static final java.lang.String ATTR_NAVIGATION_PREFERENCES_ALLOW_TOLL_ROADS = "Allow_Toll_Roads";
    private static final java.lang.String ATTR_NAVIGATION_PREFERENCES_ALLOW_TUNNELS = "Allow_Tunnels";
    private static final java.lang.String ATTR_NAVIGATION_PREFERENCES_ALLOW_UNPAVED_ROADS = "Allow_Unpaved_Roads";
    private static final java.lang.String ATTR_NAVIGATION_PREFERENCES_REROUTE_FOR_TRAFFIC = "Reroute_For_Traffic";
    private static final java.lang.String ATTR_NAVIGATION_PREFERENCES_ROUTING_TYPE = "Routing_Type";
    private static final java.lang.String ATTR_NAVIGATION_PREFERENCES_SHOW_TRAFFIC_NAVIGATING = "Show_Traffic_Navigating";
    private static final java.lang.String ATTR_NAVIGATION_PREFERENCES_SHOW_TRAFFIC_OPEN_MAP = "Show_Traffic_Open_Map";
    private static final java.lang.String ATTR_NAVIGATION_PREFERENCES_SPOKEN_SPEED_WARNINGS = "Spoken_Speed_Warnings";
    private static final java.lang.String ATTR_NAVIGATION_PREFERENCES_SPOKEN_TBT = "Spoken_Turn_By_Turn";
    private static final java.lang.String ATTR_NEARBY_SEARCH_DEST_SCROLLED = "Scrolled";
    private static final java.lang.String ATTR_NEARBY_SEARCH_DEST_SELECTED_POSITION = "Selected_Position";
    private static final java.lang.String ATTR_NEARBY_SEARCH_NO_RESULTS_ACTION = "Action";
    private static final java.lang.String ATTR_NEARBY_SEARCH_PLACE_TYPE = "Place_Type";
    private static final java.lang.String ATTR_OBD_CONN_ECU = "ECU";
    private static final java.lang.String ATTR_OBD_CONN_FIRMWARE_VERSION = "Obd_Chip_Firmware";
    private static final java.lang.String ATTR_OBD_CONN_PROTOCOL = "Protocol";
    private static final java.lang.String ATTR_OBD_CONN_VIN = "VIN";
    private static final java.lang.String ATTR_OBD_DATA_CAR_MAKE = "Car_Make";
    private static final java.lang.String ATTR_OBD_DATA_CAR_MODEL = "Car_Model";
    private static final java.lang.String ATTR_OBD_DATA_CAR_VIN = "Vin";
    private static final java.lang.String ATTR_OBD_DATA_CAR_YEAR = "Car_Year";
    private static final java.lang.String ATTR_OBD_DATA_MONITORING_DATA_SIZE = "Data_Size";
    private static final java.lang.String ATTR_OBD_DATA_MONITORING_SUCCESS = "Success";
    private static final java.lang.String ATTR_OBD_DATA_VERSION = "Version";
    private static final java.lang.String ATTR_OFFLINE_MAPS_UPDATED_NEW_DATE = "New_Date";
    private static final java.lang.String ATTR_OFFLINE_MAPS_UPDATED_NEW_PACKAGES = "New_Packages";
    private static final java.lang.String ATTR_OFFLINE_MAPS_UPDATED_NEW_VERSION = "New_Version";
    private static final java.lang.String ATTR_OFFLINE_MAPS_UPDATED_OLD_DATE = "Old_Date";
    private static final java.lang.String ATTR_OFFLINE_MAPS_UPDATED_OLD_PACKAGES = "Old_Packages";
    private static final java.lang.String ATTR_OFFLINE_MAPS_UPDATED_OLD_VERSION = "Old_Version";
    private static final java.lang.String ATTR_OFFLINE_MAPS_UPDATED_UPDATE_COUNT = "Update_Count";
    private static final java.lang.String ATTR_OPTION_NAME = "Name";
    private static final java.lang.String ATTR_OTA_CURRENT_VERSION = "Current_Version";
    private static final java.lang.String ATTR_OTA_INSTALL_INCREMENTAL = "Incremental";
    private static final java.lang.String ATTR_OTA_INSTALL_PRIOR_VERSION = "Prior_Version";
    private static final java.lang.String ATTR_OTA_INSTALL_SUCCESS = "Success";
    private static final java.lang.String ATTR_OTA_UPDATE_VERSION = "Update_Version";
    private static final java.lang.String ATTR_PHONE_CONNECTION = "Connection";
    public static final java.lang.String ATTR_PLATFORM_ANDROID = "Android";
    public static final java.lang.String ATTR_PLATFORM_IOS = "iOS";
    private static final java.lang.String ATTR_PROFILE_BUILD_TYPE = "Build_Type";
    private static final java.lang.String ATTR_PROFILE_CAR_MAKE = "Car_Make";
    private static final java.lang.String ATTR_PROFILE_CAR_MODEL = "Car_Model";
    private static final java.lang.String ATTR_PROFILE_CAR_YEAR = "Car_Year";
    private static final java.lang.String ATTR_PROFILE_HUD_VERSION = "Hud_Version_Number";
    private static final java.lang.String ATTR_PROFILE_SERIAL_NUMBER = "Hud_Serial_Number";
    private static final java.lang.String ATTR_REGION_COUNTRY = "Country";
    private static final java.lang.String ATTR_REGION_STATE = "State";
    private static final java.lang.String ATTR_REMOTE_DEVICE_HARDWARE = "Remote_Device_Hardware";
    private static final java.lang.String ATTR_REMOTE_DEVICE_PLATFORM = "Remote_Device_Platform";
    private static final java.lang.String ATTR_REMOTE_DEVICE_SW_VERSION = "Remote_Device_SW_Version";
    private static final java.lang.String ATTR_ROUTE_COMPLETED_ACTUAL_DISTANCE = "Actual_Distance";
    private static final java.lang.String ATTR_ROUTE_COMPLETED_ACTUAL_DURATION = "Actual_Duration";
    private static final java.lang.String ATTR_ROUTE_COMPLETED_DISTANCE_VARIANCE = "Distance_PCT_Diff";
    private static final java.lang.String ATTR_ROUTE_COMPLETED_DURATION_VARIANCE = "Duration_PCT_Diff";
    private static final java.lang.String ATTR_ROUTE_COMPLETED_EXPECTED_DISTANCE = "Expected_Distance";
    private static final java.lang.String ATTR_ROUTE_COMPLETED_EXPECTED_DURATION = "Expected_Duration";
    private static final java.lang.String ATTR_ROUTE_COMPLETED_RECALCULATIONS = "Recalculations";
    private static final java.lang.String ATTR_ROUTE_COMPLETED_TRAFFIC_LEVEL = "Traffic_Level";
    private static final java.lang.String ATTR_ROUTE_DISPLAY_POS = "Display_Pos";
    private static final java.lang.String ATTR_ROUTE_NAV_POS = "Nav_Pos";
    private static final java.lang.String ATTR_ROUTE_STREET_ADDRESS = "Street_Addr";
    private static final java.lang.String ATTR_SESSION_ACCELERATION_COUNT = "Session_Hard_Acceleration_Count";
    private static final java.lang.String ATTR_SESSION_AVERAGE_MPG = "Session_Average_Mpg";
    private static final java.lang.String ATTR_SESSION_AVERAGE_ROLLING_SPEED = "Session_Average_Rolling_Speed";
    private static final java.lang.String ATTR_SESSION_AVERAGE_SPEED = "Session_Average_Speed";
    private static final java.lang.String ATTR_SESSION_DISTANCE = "Session_Distance";
    private static final java.lang.String ATTR_SESSION_DURATION = "Session_Duration";
    private static final java.lang.String ATTR_SESSION_EXCESSIVE_SPEEDING_FREQUENCY = "Session_Excessive_Speeding_Frequency";
    private static final java.lang.String ATTR_SESSION_HARD_BRAKING_COUNT = "Session_Hard_Braking_Count";
    private static final java.lang.String ATTR_SESSION_HIGH_G_COUNT = "Session_High_G_Count";
    private static final java.lang.String ATTR_SESSION_MAX_RPM = "Session_Max_RPM";
    private static final java.lang.String ATTR_SESSION_MAX_SPEED = "Session_Max_Speed";
    private static final java.lang.String ATTR_SESSION_ROLLING_DURATION = "Session_Rolling_Duration";
    private static final java.lang.String ATTR_SESSION_SPEEDING_FREQUENCY = "Session_Speeding_Frequency";
    private static final java.lang.String ATTR_SESSION_TROUBLE_CODE_COUNT = "Session_Trouble_Code_Count";
    private static final java.lang.String ATTR_SESSION_TROUBLE_CODE_LAST = "Session_Trouble_Code_Last";
    private static final java.lang.String ATTR_SHUTDOWN_BATTERY_LEVEL = "Battery_Level";
    private static final java.lang.String ATTR_SHUTDOWN_REASON = "Reason";
    private static final java.lang.String ATTR_SHUTDOWN_SLEEP_TIME = "Sleep_Time";
    private static final java.lang.String ATTR_SHUTDOWN_TYPE = "Type";
    private static final java.lang.String ATTR_SHUTDOWN_UPTIME = "Uptime";
    private static final java.lang.String ATTR_SMS_MESSAGE_TYPE = "Message_Type";
    private static final java.lang.String ATTR_SMS_MESSAGE_TYPE_CANNED = "Canned";
    private static final java.lang.String ATTR_SMS_MESSAGE_TYPE_CUSTOM = "Custom";
    private static final java.lang.String ATTR_SMS_PLATFORM = "Platform";
    private static final java.lang.String ATTR_SMS_SUCCESS = "Success";
    private static final java.lang.String ATTR_SPEEDING_AMOUNT = "Session_Speeding_Amount";
    private static final java.lang.String ATTR_TEMP_CPU_AVERAGE = "CPU_Average";
    private static final java.lang.String ATTR_TEMP_CPU_MAX = "CPU_Max";
    private static final java.lang.String ATTR_TEMP_CPU_TEMP = "CPU_Temperature";
    private static final java.lang.String ATTR_TEMP_DMD_AVERAGE = "DMD_Average";
    private static final java.lang.String ATTR_TEMP_DMD_MAX = "DMD_Max";
    private static final java.lang.String ATTR_TEMP_DMD_TEMP = "DMD_Temperature";
    private static final java.lang.String ATTR_TEMP_FAN_SPEED = "Fan_Speed";
    private static final java.lang.String ATTR_TEMP_INLET_AVERAGE = "Inlet_Average";
    private static final java.lang.String ATTR_TEMP_INLET_MAX = "Inlet_Max";
    private static final java.lang.String ATTR_TEMP_INLET_TEMP = "Inlet_Temperature";
    private static final java.lang.String ATTR_TEMP_LED_AVERAGE = "LED_Average";
    private static final java.lang.String ATTR_TEMP_LED_MAX = "LED_Max";
    private static final java.lang.String ATTR_TEMP_LED_TEMP = "LED_Temperature";
    private static final java.lang.String ATTR_TEMP_WARNING = "Warning";
    private static final java.lang.String ATTR_UPDATE_ACCEPTED = "Accepted";
    private static final java.lang.String ATTR_UPDATE_TO_VERSION = "Update_To_Version";
    private static final java.lang.String ATTR_VEHICLE_KILO_METERS = "Vehicle_Kilo_Meters";
    private static final java.lang.String ATTR_VIN = "Vin";
    private static final java.lang.String ATTR_VOICE_SEARCH_ADDITIONAL_RESULTS_GLANCE = "Additional_Results_Glance";
    private static final java.lang.String ATTR_VOICE_SEARCH_CANCEL_TYPE = "Cancel_Type";
    private static final java.lang.String ATTR_VOICE_SEARCH_CLIENT_VERSION = "Client_Version";
    private static final java.lang.String ATTR_VOICE_SEARCH_CONFIDENCE_LEVEL = "Confidence_Level";
    private static final java.lang.String ATTR_VOICE_SEARCH_DESTINATION_TYPE = "Destination_Type";
    private static final java.lang.String ATTR_VOICE_SEARCH_EXPLICIT_RETRY = "Explicit_Retry";
    private static final java.lang.String ATTR_VOICE_SEARCH_LIST_SELECTION = "Additional_Results_List_Item_Selected";
    private static final java.lang.String ATTR_VOICE_SEARCH_MICROPHONE_USED = "Microphone_Used";
    private static final java.lang.String ATTR_VOICE_SEARCH_NAVIGATION_INITIATED = "Navigation_Initiated";
    private static final java.lang.String ATTR_VOICE_SEARCH_NAV_CANCELLED_QUICKLY = "Navigation_Cancelled_Quickly";
    private static final java.lang.String ATTR_VOICE_SEARCH_NUMBER_OF_WORDS = "Number_Of_Words";
    private static final java.lang.String ATTR_VOICE_SEARCH_NUM_RESULTS = "Num_Total_Results";
    private static final java.lang.String ATTR_VOICE_SEARCH_PREFIX = "Prefix";
    private static final java.lang.String ATTR_VOICE_SEARCH_RESPONSE = "Response";
    private static final java.lang.String ATTR_VOICE_SEARCH_RETRY_COUNT = "Retry_Count";
    private static final java.lang.String ATTR_VOICE_SEARCH_SUGGESTION = "Suggestion";
    private static final java.lang.String ATTR_WAKEUP_BATTERY_DRAIN = "Battery_Drain";
    private static final java.lang.String ATTR_WAKEUP_BATTERY_LEVEL = "Battery_Level";
    private static final java.lang.String ATTR_WAKEUP_MAX_BATTERY = "Max_Battery";
    private static final java.lang.String ATTR_WAKEUP_REASON = "Reason";
    private static final java.lang.String ATTR_WAKEUP_SLEEP_TIME = "Sleep_Time";
    private static final java.lang.String ATTR_WAKEUP_VIN = "VIN";
    private static final int CUSTOM_DIMENSION_HW_VERSION = 0;
    private static final java.lang.String DESTINATION_SUGGESTION_TYPE_CALENDAR = "Calendar";
    private static final java.lang.String DESTINATION_SUGGESTION_TYPE_SMART_SUGGESTION = "SmartSuggestion";
    private static final java.lang.String FALSE = "false";
    private static final long FUEL_REPORTING_INTERVAL = 180000;
    private static final java.lang.String LIGHT_SENSOR_DEVICE = "/sys/bus/i2c/drivers/tsl2x7x/2-0039/iio:device0/in_illuminance0_input";
    private static final java.lang.String LIGHT_SENSOR_RAW1_DEVICE = "/sys/bus/i2c/drivers/tsl2x7x/2-0039/iio:device0/in_intensity0_raw1";
    private static final java.lang.String LIGHT_SENSOR_RAW_DEVICE = "/sys/bus/i2c/drivers/tsl2x7x/2-0039/iio:device0/in_intensity0_raw";
    private static final int MAX_CPU_TEMP = 93;
    /* access modifiers changed from: private */
    public static final long MAX_DMD_HIGH_TIME_MS = java.util.concurrent.TimeUnit.MINUTES.toMillis(10);
    private static final int MAX_DMD_TEMP = 65;
    /* access modifiers changed from: private */
    public static final long MAX_INLET_HIGH_TIME_MS = java.util.concurrent.TimeUnit.MINUTES.toMillis(2);
    private static final int MAX_INLET_TEMP = 72;
    private static final long NAVIGATION_CANCEL_TIMEOUT = java.util.concurrent.TimeUnit.MINUTES.toMillis(1);
    private static final int NOTEMP = -1000;
    private static final java.lang.String OFFLINE_MAPS_UPDATE_COUNT = "offlineMapsUpdateCount";
    private static final int PREF_CHANGE_DELAY_MS = 15000;
    private static final java.lang.String PREVIOUS_OFFLINE_MAPS_DATE = "previousOfflineMapsDate";
    private static final java.lang.String PREVIOUS_OFFLINE_MAPS_PACKAGES = "previousOfflineMapsPackages";
    private static final java.lang.String PREVIOUS_OFFLINE_MAPS_VERSION = "previousOfflineMapsVersion";
    /* access modifiers changed from: private */
    public static final long TEMP_WARNING_BOOT_DELAY_S = java.util.concurrent.TimeUnit.MINUTES.toSeconds(3);
    /* access modifiers changed from: private */
    public static final long TEMP_WARNING_INTERVAL_MS = java.util.concurrent.TimeUnit.MINUTES.toMillis(10);
    private static final java.lang.String TRUE = "true";
    private static final long UPLOAD_INTERVAL = java.util.concurrent.TimeUnit.MINUTES.toMillis(5);
    private static final long UPLOAD_INTERVAL_BOOT = java.util.concurrent.TimeUnit.MINUTES.toMillis(5);
    private static final long UPLOAD_INTERVAL_LIMITED_BANDWIDTH = java.util.concurrent.TimeUnit.MINUTES.toMillis(20);
    private static final boolean VERBOSE = false;
    /* access modifiers changed from: private */
    public static int currentCpuAvgTemperature = -1;
    private static java.util.ArrayList<com.navdy.hud.app.analytics.AnalyticsSupport.DeferredLocalyticsEvent> deferredEvents = null;
    private static com.navdy.hud.app.analytics.AnalyticsSupport.DeferredWakeup deferredWakeup = null;
    private static boolean destinationSet = false;
    /* access modifiers changed from: private */
    public static boolean disconnectDueToForceReconnect = false;
    /* access modifiers changed from: private */
    public static java.lang.String glanceNavigationSource = null;
    /* access modifiers changed from: private */
    public static android.os.Handler handler = new android.os.Handler(android.os.Looper.getMainLooper());
    /* access modifiers changed from: private */
    public static int lastDiscReason = 0;
    /* access modifiers changed from: private */
    public static java.lang.String lastForceReconnectReason = "";
    private static volatile boolean localyticsInitialized = false;
    private static int nearbySearchDestScrollPosition = -1;
    private static com.navdy.service.library.events.places.PlaceType nearbySearchPlaceType = null;
    private static boolean newDevicePaired = false;
    private static boolean phonePaired = false;
    /* access modifiers changed from: private */
    public static boolean quietMode = false;
    private static com.navdy.service.library.events.DeviceInfo remoteDevice = null;
    private static com.navdy.hud.app.analytics.AnalyticsSupport.FuelMonitor sFuelMonitor;
    /* access modifiers changed from: private */
    public static com.navdy.hud.app.analytics.AnalyticsSupport.PreferenceWatcher sLastNavigationPreferences = null;
    /* access modifiers changed from: private */
    public static com.navdy.hud.app.profile.DriverProfile sLastProfile = null;
    /* access modifiers changed from: private */
    public static com.navdy.hud.app.analytics.AnalyticsSupport.PreferenceWatcher sLastProfilePreferences = null;
    private static java.lang.String sLastVIN = null;
    /* access modifiers changed from: private */
    public static com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.analytics.AnalyticsSupport.class);
    private static com.navdy.hud.app.analytics.AnalyticsSupport.NotificationReceiver sNotificationReceiver;
    /* access modifiers changed from: private */
    public static com.navdy.hud.app.obd.ObdManager sObdManager;
    /* access modifiers changed from: private */
    public static java.lang.String sRemoteDeviceId = null;
    private static com.navdy.hud.app.manager.RemoteDeviceManager sRemoteDeviceManager = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance();
    /* access modifiers changed from: private */
    public static java.lang.Runnable sUploader = new com.navdy.hud.app.analytics.AnalyticsSupport.Anon1();
    private static long startTime;
    private static double startingBatteryVoltage = -1.0d;
    private static int startingDistance;
    /* access modifiers changed from: private */
    public static com.navdy.hud.app.analytics.AnalyticsSupport.VoiceResultsRunnable voiceResultsRunnable = null;
    /* access modifiers changed from: private */
    public static com.navdy.hud.app.analytics.AnalyticsSupport.VoiceSearchAdditionalResultsAction voiceSearchAdditionalResultsAction = null;
    /* access modifiers changed from: private */
    public static java.lang.Integer voiceSearchConfidence = null;
    /* access modifiers changed from: private */
    public static int voiceSearchCount = 0;
    /* access modifiers changed from: private */
    public static boolean voiceSearchExplicitRetry = false;
    /* access modifiers changed from: private */
    public static java.lang.Integer voiceSearchListSelection = null;
    /* access modifiers changed from: private */
    public static boolean voiceSearchListeningOverBluetooth = false;
    /* access modifiers changed from: private */
    public static java.lang.String voiceSearchPrefix = null;
    private static com.navdy.hud.app.analytics.AnalyticsSupport.VoiceSearchProgress voiceSearchProgress = null;
    /* access modifiers changed from: private */
    public static java.util.List<com.navdy.service.library.events.destination.Destination> voiceSearchResults = null;
    /* access modifiers changed from: private */
    public static java.lang.String voiceSearchWords = null;
    private static long welcomeScreenTime = -1;

    public static class AnalyticsIntentsReceiver extends android.content.BroadcastReceiver {
        public void onReceive(android.content.Context context, android.content.Intent intent) {
            java.lang.String tagName = intent.getStringExtra(com.navdy.hud.app.analytics.AnalyticsSupport.ANALYTICS_INTENT_EXTRA_TAG_NAME);
            boolean includeDeviceInfo = intent.getBooleanExtra(com.navdy.hud.app.analytics.AnalyticsSupport.ANALYTICS_INTENT_EXTRA_INCLUDE_REMOTE_DEVICE_INFO, false);
            if (tagName != null) {
                java.lang.String[] args = intent.getStringArrayExtra(com.navdy.hud.app.analytics.AnalyticsSupport.ANALYTICS_INTENT_EXTRA_ARGUMENTS);
                if (args == null) {
                    args = new java.lang.String[0];
                }
                if (tagName.equals(com.navdy.hud.app.analytics.AnalyticsSupport.ANALYTICS_EVENT_NEW_DEVICE)) {
                    com.navdy.hud.app.analytics.AnalyticsSupport.recordNewDevicePaired();
                } else {
                    com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent(tagName, includeDeviceInfo, args);
                }
            }
        }
    }

    static class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            if (com.navdy.hud.app.framework.network.NetworkBandwidthController.getInstance().isNetworkAccessAllowed(com.navdy.hud.app.framework.network.NetworkBandwidthController.Component.LOCALYTICS)) {
                com.navdy.hud.app.analytics.AnalyticsSupport.sLogger.v("called Localytics.upload");
                com.localytics.android.Localytics.upload();
            }
            com.navdy.hud.app.analytics.AnalyticsSupport.handler.postDelayed(this, com.navdy.hud.app.analytics.AnalyticsSupport.getUpdateInterval());
        }
    }

    static class Anon2 extends android.content.BroadcastReceiver {
        Anon2() {
        }

        public void onReceive(android.content.Context context, android.content.Intent intent) {
            java.lang.String action = intent.getAction();
            if ("android.bluetooth.device.action.ACL_DISCONNECTED".equals(action)) {
                android.bluetooth.BluetoothDevice device = (android.bluetooth.BluetoothDevice) intent.getParcelableExtra("android.bluetooth.device.extra.DEVICE");
                if (device == null || !(device.getType() == 1 || device.getType() == 3)) {
                    com.navdy.hud.app.analytics.AnalyticsSupport.sLogger.d("ACTION_ACL_DISCONNECTED, Non classic device , Disconnect reason " + intent.getIntExtra("android.bluetooth.device.extra.STATUS", 0));
                    return;
                }
                com.navdy.hud.app.analytics.AnalyticsSupport.lastDiscReason = intent.getIntExtra("android.bluetooth.device.extra.STATUS", 0);
                com.navdy.hud.app.analytics.AnalyticsSupport.sLogger.d("ACTION_ACL_DISCONNECTED, Disconnect reason " + com.navdy.hud.app.analytics.AnalyticsSupport.lastDiscReason + " " + intent);
            } else if (com.navdy.hud.app.service.HudConnectionService.ACTION_DEVICE_FORCE_RECONNECT.equals(action)) {
                com.navdy.hud.app.analytics.AnalyticsSupport.disconnectDueToForceReconnect = true;
                com.navdy.hud.app.analytics.AnalyticsSupport.lastForceReconnectReason = intent.getStringExtra(com.navdy.hud.app.service.HudConnectionService.EXTRA_HUD_FORCE_RECONNECT_REASON);
                com.navdy.hud.app.analytics.AnalyticsSupport.sLogger.d("ACTION_DEVICE_FORCE_RECONNECT, Reason : " + com.navdy.hud.app.analytics.AnalyticsSupport.lastForceReconnectReason + " " + intent);
            }
        }
    }

    private static class DeferredLocalyticsEvent {
        public java.lang.String[] args;
        public java.lang.String tag;

        DeferredLocalyticsEvent(java.lang.String tag2, java.lang.String[] args2) {
            this.tag = tag2;
            this.args = args2;
        }
    }

    private static class DeferredWakeup {
        java.lang.String batteryDrain;
        java.lang.String batteryLevel;
        java.lang.String maxBattery;
        public com.navdy.hud.app.analytics.AnalyticsSupport.WakeupReason reason;
        java.lang.String sleepTime;

        DeferredWakeup(com.navdy.hud.app.analytics.AnalyticsSupport.WakeupReason r, java.lang.String bLevel, java.lang.String bDrain, java.lang.String sTime, java.lang.String maxBat) {
            this.reason = r;
            this.batteryLevel = bLevel;
            this.batteryDrain = bDrain;
            this.sleepTime = sTime;
            this.maxBattery = maxBat;
        }
    }

    private static class EventSender implements java.lang.Runnable {
        private com.navdy.hud.app.analytics.Event event;

        EventSender(com.navdy.hud.app.analytics.Event event2) {
            this.event = event2;
        }

        public void run() {
            com.localytics.android.Localytics.tagEvent(this.event.tag, this.event.argMap);
        }
    }

    private static class FuelMonitor implements java.lang.Runnable {
        private FuelMonitor() {
        }

        /* synthetic */ FuelMonitor(com.navdy.hud.app.analytics.AnalyticsSupport.Anon1 x0) {
            this();
        }

        public void run() {
            try {
                int distance = com.navdy.hud.app.analytics.AnalyticsSupport.sObdManager.getDistanceTravelled();
                int fuelLevel = com.navdy.hud.app.analytics.AnalyticsSupport.sObdManager.getFuelLevel();
                double fuelConsumption = com.navdy.hud.app.analytics.AnalyticsSupport.sObdManager.getInstantFuelConsumption();
                java.lang.String vin = com.navdy.hud.app.analytics.AnalyticsSupport.sObdManager.getVin();
                if (fuelLevel != -1) {
                    com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent("Fuel_Level", "VIN", vin, "Fuel_Level", java.lang.Integer.toString(fuelLevel), com.navdy.hud.app.analytics.AnalyticsSupport.ATTR_FUEL_CONSUMPTION, java.lang.Double.toString(fuelConsumption), "Distance", java.lang.Integer.toString(distance));
                }
            } finally {
                com.navdy.hud.app.analytics.AnalyticsSupport.handler.postDelayed(this, com.navdy.hud.app.analytics.AnalyticsSupport.FUEL_REPORTING_INTERVAL);
            }
        }
    }

    public static class NotificationReceiver implements com.navdy.hud.app.ui.framework.INotificationAnimationListener {
        @javax.inject.Inject
        public android.content.SharedPreferences appPreferences;
        private com.here.android.mpa.routing.Maneuver lastRequiredManeuver = null;
        private com.navdy.hud.app.maps.MapEvents.ManeuverDisplay lastWarningEvent = null;
        @javax.inject.Inject
        public com.navdy.hud.app.device.PowerManager powerManager;

        class Anon1 implements java.lang.Runnable {
            Anon1() {
            }

            public void run() {
                com.navdy.hud.app.analytics.AnalyticsSupport.recordOfflineMapsChange(com.navdy.hud.app.analytics.AnalyticsSupport.NotificationReceiver.this.appPreferences);
            }
        }

        class Anon2 implements com.navdy.hud.app.analytics.AnalyticsSupport.PreferenceAdapter {
            Anon2() {
            }

            public com.navdy.hud.app.analytics.Event calculateState(com.navdy.hud.app.profile.DriverProfile profile) {
                return com.navdy.hud.app.analytics.AnalyticsSupport.recordPreference(profile);
            }
        }

        class Anon3 implements com.navdy.hud.app.analytics.AnalyticsSupport.PreferenceAdapter {
            Anon3() {
            }

            public com.navdy.hud.app.analytics.Event calculateState(com.navdy.hud.app.profile.DriverProfile profile) {
                return com.navdy.hud.app.analytics.AnalyticsSupport.recordNavigationPreference(profile);
            }
        }

        NotificationReceiver() {
            com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getBus().register(this);
            com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getUiStateManager().addNotificationAnimationListener(this);
            mortar.Mortar.inject(com.navdy.hud.app.HudApplication.getAppContext(), this);
        }

        public void onStart(java.lang.String id, com.navdy.hud.app.framework.notifications.NotificationType type, com.navdy.hud.app.ui.framework.UIStateManager.Mode mode) {
            if (mode == com.navdy.hud.app.ui.framework.UIStateManager.Mode.EXPAND) {
                com.navdy.hud.app.analytics.AnalyticsSupport.recordGlanceAction(com.navdy.hud.app.analytics.AnalyticsSupport.ANALYTICS_EVENT_GLANCE_OPEN_MINI, com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().getCurrentNotification(), com.navdy.hud.app.analytics.AnalyticsSupport.glanceNavigationSource != null ? com.navdy.hud.app.analytics.AnalyticsSupport.glanceNavigationSource : "auto");
            }
            com.navdy.hud.app.analytics.AnalyticsSupport.glanceNavigationSource = null;
        }

        public void onStop(java.lang.String id, com.navdy.hud.app.framework.notifications.NotificationType type, com.navdy.hud.app.ui.framework.UIStateManager.Mode mode) {
        }

        @com.squareup.otto.Subscribe
        public void onMapEngineInitialized(com.navdy.hud.app.maps.MapEvents.MapEngineInitialize event) {
            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.analytics.AnalyticsSupport.NotificationReceiver.Anon1(), 1);
        }

        @com.squareup.otto.Subscribe
        public void onDateTimeAvailable(com.navdy.hud.app.common.TimeHelper.DateTimeAvailableEvent event) {
            com.navdy.hud.app.analytics.AnalyticsSupport.maybeOpenSession();
        }

        @com.squareup.otto.Subscribe
        public void onShutdown(com.navdy.hud.app.event.Shutdown event) {
            if (event.state == com.navdy.hud.app.event.Shutdown.State.SHUTTING_DOWN) {
                com.navdy.hud.app.analytics.AnalyticsSupport.closeSession();
            }
        }

        @com.squareup.otto.Subscribe
        public void onDriverProfileChange(com.navdy.hud.app.event.DriverProfileChanged event) {
            com.navdy.hud.app.profile.DriverProfile profile = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile();
            if (!profile.isDefaultProfile()) {
                com.navdy.hud.app.analytics.AnalyticsSupport.sLastProfile = profile;
                com.navdy.hud.app.analytics.AnalyticsSupport.sLogger.d("Remembering cached preferences");
                com.navdy.hud.app.analytics.AnalyticsSupport.sLastProfilePreferences = new com.navdy.hud.app.analytics.AnalyticsSupport.PreferenceWatcher(profile, new com.navdy.hud.app.analytics.AnalyticsSupport.NotificationReceiver.Anon2());
                com.navdy.hud.app.analytics.AnalyticsSupport.sLastNavigationPreferences = new com.navdy.hud.app.analytics.AnalyticsSupport.PreferenceWatcher(profile, new com.navdy.hud.app.analytics.AnalyticsSupport.NotificationReceiver.Anon3());
            }
        }

        @com.squareup.otto.Subscribe
        public void onConnectionStatusChange(com.navdy.service.library.events.connection.ConnectionStateChange event) {
            if (event.state == com.navdy.service.library.events.connection.ConnectionStateChange.ConnectionState.CONNECTION_DISCONNECTED) {
                com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent(com.navdy.hud.app.analytics.AnalyticsSupport.ANALYTICS_EVENT_MOBILE_APP_DISCONNECT, com.navdy.hud.app.analytics.AnalyticsSupport.ATTR_CONN_DEVICE_ID, android.os.Build.SERIAL, com.navdy.hud.app.analytics.AnalyticsSupport.ATTR_CONN_REMOTE_DEVICE_ID, com.navdy.hud.app.analytics.AnalyticsSupport.sRemoteDeviceId);
                com.navdy.hud.app.analytics.AnalyticsSupport.clearVoiceSearch();
            }
        }

        @com.squareup.otto.Subscribe
        public void onShowToast(com.navdy.hud.app.framework.toast.ToastManager.ShowToast toast) {
            if (toast.name.equals(com.navdy.hud.app.framework.connection.ConnectionNotification.DISCONNECT_ID)) {
                com.navdy.hud.app.analytics.AnalyticsSupport.recordPhoneDisconnect(false, null);
            }
        }

        @com.squareup.otto.Subscribe
        public void onRegionEvent(com.navdy.hud.app.maps.MapEvents.RegionEvent event) {
            com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent(com.navdy.hud.app.analytics.AnalyticsSupport.ANALYTICS_EVENT_NAVIGATION_REGION_CHANGE, "State", event.state, com.navdy.hud.app.analytics.AnalyticsSupport.ATTR_REGION_COUNTRY, event.country);
        }

        private boolean objEquals(java.lang.Object s1, java.lang.Object s2) {
            return s1 == s2 || (s1 != null && s1.equals(s2));
        }

        @com.squareup.otto.Subscribe
        public void onManeuverWarning(com.navdy.hud.app.maps.MapEvents.ManeuverDisplay event) {
            if (this.lastWarningEvent == null || event.turnIconId != this.lastWarningEvent.turnIconId || !objEquals(event.pendingTurn, this.lastWarningEvent.pendingTurn) || !objEquals(event.pendingRoad, this.lastWarningEvent.pendingRoad) || !objEquals(event.currentRoad, this.lastWarningEvent.currentRoad) || !objEquals(event.navigationTurn, this.lastWarningEvent.navigationTurn)) {
                com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent(com.navdy.hud.app.analytics.AnalyticsSupport.ANALYTICS_EVENT_NAVIGATION_MANEUVER_WARNING, new java.lang.String[0]);
                this.lastWarningEvent = event;
            }
        }

        @com.squareup.otto.Subscribe
        public void onManeuverRequired(com.navdy.hud.app.maps.MapEvents.ManeuverSoonEvent event) {
            if (event.maneuver != this.lastRequiredManeuver) {
                com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent(com.navdy.hud.app.analytics.AnalyticsSupport.ANALYTICS_EVENT_NAVIGATION_MANEUVER_REQUIRED, new java.lang.String[0]);
                this.lastRequiredManeuver = event.maneuver;
            }
        }

        @com.squareup.otto.Subscribe
        public void onManeuverMissed(com.navdy.hud.app.maps.MapEvents.RerouteEvent event) {
            com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent(com.navdy.hud.app.analytics.AnalyticsSupport.ANALYTICS_EVENT_NAVIGATION_MANEUVER_MISSED, new java.lang.String[0]);
        }

        @com.squareup.otto.Subscribe
        public void onArrived(com.navdy.hud.app.maps.MapEvents.ManeuverEvent event) {
            if (event.type == com.navdy.hud.app.maps.MapEvents.ManeuverEvent.Type.LAST) {
                com.navdy.hud.app.analytics.AnalyticsSupport.sLogger.v("NAVIGATION_ARRIVED");
                com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent(com.navdy.hud.app.analytics.AnalyticsSupport.ANALYTICS_EVENT_NAVIGATION_ARRIVED, new java.lang.String[0]);
            }
        }

        @com.squareup.otto.Subscribe
        public void onDismissScreen(com.navdy.service.library.events.ui.DismissScreen event) {
        }

        @com.squareup.otto.Subscribe
        public void onWakeup(com.navdy.hud.app.event.Wakeup event) {
            com.navdy.hud.app.analytics.AnalyticsSupport.recordWakeup(event.reason);
            com.navdy.hud.app.analytics.AnalyticsSupport.quietMode = false;
            com.navdy.hud.app.analytics.AnalyticsSupport.maybeOpenSession();
        }

        @com.squareup.otto.Subscribe
        public void onBandwidthSettingChanged(com.navdy.hud.app.framework.network.NetworkBandwidthController.UserBandwidthSettingChanged event) {
            long interval = com.navdy.hud.app.analytics.AnalyticsSupport.getUpdateInterval();
            com.navdy.hud.app.analytics.AnalyticsSupport.sLogger.v("uploader changed to " + interval);
            com.navdy.hud.app.analytics.AnalyticsSupport.handler.removeCallbacks(com.navdy.hud.app.analytics.AnalyticsSupport.sUploader);
            com.navdy.hud.app.analytics.AnalyticsSupport.handler.postDelayed(com.navdy.hud.app.analytics.AnalyticsSupport.sUploader, interval);
        }

        @com.squareup.otto.Subscribe
        public void ObdStateChangeEvent(com.navdy.hud.app.obd.ObdManager.ObdConnectionStatusEvent event) {
            if (event.connected) {
                com.navdy.hud.app.analytics.AnalyticsSupport.reportObdState();
            }
        }

        @com.squareup.otto.Subscribe
        public void onRouteSearchRequest(com.navdy.service.library.events.navigation.NavigationRouteRequest event) {
            com.navdy.hud.app.analytics.AnalyticsSupport.processNavigationRouteRequest(event);
        }

        @com.squareup.otto.Subscribe
        public void onArrived(com.navdy.hud.app.maps.MapEvents.ArrivalEvent event) {
            com.navdy.hud.app.analytics.AnalyticsSupport.processNavigationArrivalEvent(event);
        }
    }

    interface PreferenceAdapter {
        com.navdy.hud.app.analytics.Event calculateState(com.navdy.hud.app.profile.DriverProfile driverProfile);
    }

    private static class PreferenceWatcher {
        com.navdy.hud.app.analytics.AnalyticsSupport.PreferenceAdapter adapter;
        com.navdy.hud.app.analytics.Event oldState;
        java.lang.Runnable queuedUpdate;

        class Anon1 implements java.lang.Runnable {
            final /* synthetic */ com.navdy.hud.app.profile.DriverProfile val$profile;

            Anon1(com.navdy.hud.app.profile.DriverProfile driverProfile) {
                this.val$profile = driverProfile;
            }

            public void run() {
                com.navdy.hud.app.analytics.Event newState = com.navdy.hud.app.analytics.AnalyticsSupport.PreferenceWatcher.this.adapter.calculateState(this.val$profile);
                java.lang.String changedFields = newState.getChangedFields(com.navdy.hud.app.analytics.AnalyticsSupport.PreferenceWatcher.this.oldState);
                if (!android.text.TextUtils.isEmpty(changedFields)) {
                    newState.argMap.put("Preferences_Updated", changedFields);
                    com.navdy.hud.app.analytics.AnalyticsSupport.PreferenceWatcher.this.oldState = newState;
                    com.navdy.hud.app.analytics.AnalyticsSupport.sLogger.d("Recording preference change: " + newState);
                    com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent(newState, false);
                    return;
                }
                com.navdy.hud.app.analytics.AnalyticsSupport.sLogger.d("No change in " + newState.tag);
            }
        }

        PreferenceWatcher(com.navdy.hud.app.profile.DriverProfile profile, com.navdy.hud.app.analytics.AnalyticsSupport.PreferenceAdapter adapter2) {
            this.oldState = adapter2.calculateState(profile);
            this.adapter = adapter2;
        }

        /* access modifiers changed from: 0000 */
        public void enqueue(com.navdy.hud.app.profile.DriverProfile profile) {
            if (this.queuedUpdate != null) {
                com.navdy.hud.app.analytics.AnalyticsSupport.handler.removeCallbacks(this.queuedUpdate);
            }
            this.queuedUpdate = new com.navdy.hud.app.analytics.AnalyticsSupport.PreferenceWatcher.Anon1(profile);
            com.navdy.hud.app.analytics.AnalyticsSupport.handler.postDelayed(this.queuedUpdate, com.glympse.android.lib.StaticConfig.PERMISSION_CHECK_INTERVAL);
        }
    }

    public static class TemperatureReporter extends java.lang.Thread {
        private static final java.lang.String SOCKET_NAME = "tempstatus";
        @javax.inject.Inject
        com.navdy.hud.app.device.PowerManager powerManager;

        public void run() {
            int nFields;
            java.lang.String str;
            try {
                android.net.LocalSocket socket = new android.net.LocalSocket();
                socket.connect(new android.net.LocalSocketAddress(SOCKET_NAME, android.net.LocalSocketAddress.Namespace.RESERVED));
                java.io.InputStream socketInputStream = socket.getInputStream();
                java.io.InputStreamReader inputStreamReader = new java.io.InputStreamReader(socketInputStream);
                java.io.BufferedReader bufferedReader = new java.io.BufferedReader(inputStreamReader);
                int retries = 0;
                try {
                    com.navdy.hud.app.analytics.AnalyticsSupport.Threshold inletThreshold = new com.navdy.hud.app.analytics.AnalyticsSupport.Threshold(72, com.navdy.hud.app.analytics.AnalyticsSupport.MAX_INLET_HIGH_TIME_MS);
                    com.navdy.hud.app.analytics.AnalyticsSupport.Threshold dmdThreshold = new com.navdy.hud.app.analytics.AnalyticsSupport.Threshold(com.navdy.hud.app.analytics.AnalyticsSupport.MAX_DMD_TEMP, com.navdy.hud.app.analytics.AnalyticsSupport.MAX_DMD_HIGH_TIME_MS);
                    while (true) {
                        java.lang.String origData = bufferedReader.readLine();
                        if (origData != null) {
                            java.lang.String origData2 = origData.trim();
                            java.lang.String data = origData2;
                            java.lang.String[] fields = new java.lang.String[13];
                            int nFields2 = 0;
                            while (true) {
                                nFields = nFields2;
                                if (nFields >= fields.length - 1 || !data.contains(" ")) {
                                    int nFields3 = nFields + 1;
                                } else {
                                    java.lang.String[] split = data.split(" +", 2);
                                    nFields2 = nFields + 1;
                                    fields[nFields] = split[0];
                                    data = split[1];
                                }
                            }
                            int nFields32 = nFields + 1;
                            fields[nFields] = data;
                            if (nFields32 != fields.length) {
                                com.navdy.hud.app.analytics.AnalyticsSupport.sLogger.e(java.lang.String.format(java.util.Locale.US, "missing fields in temperature reporting data, found = %d, expected = %d data: %s", new java.lang.Object[]{java.lang.Integer.valueOf(nFields32), java.lang.Integer.valueOf(fields.length), origData2}));
                            } else {
                                if (fields[nFields32 - 1].contains(" ")) {
                                    com.navdy.hud.app.analytics.AnalyticsSupport.sLogger.w("too many fields in temperature reporting data: " + origData2);
                                    fields[nFields32 - 1] = fields[nFields32 - 1].split(" ")[0];
                                }
                                int avgCpuTemp = com.navdy.hud.app.analytics.AnalyticsSupport.parseTemp(fields[5]);
                                com.navdy.hud.app.analytics.AnalyticsSupport.currentCpuAvgTemperature = avgCpuTemp;
                                int inletTemp = com.navdy.hud.app.analytics.AnalyticsSupport.parseTemp(fields[2]);
                                int dmdTemp = com.navdy.hud.app.analytics.AnalyticsSupport.parseTemp(fields[3]);
                                long now = android.os.SystemClock.elapsedRealtime();
                                dmdThreshold.update(dmdTemp);
                                boolean doShutdown = dmdThreshold.isHit();
                                boolean doWarning = false;
                                if (!this.powerManager.inQuietMode()) {
                                    inletThreshold.update(inletTemp);
                                    doWarning = avgCpuTemp >= com.navdy.hud.app.analytics.AnalyticsSupport.MAX_CPU_TEMP || inletThreshold.isHit();
                                    long lastWarningTime = com.navdy.hud.app.view.TemperatureWarningView.getLastDismissedTime();
                                    if (lastWarningTime == 0 || (lastWarningTime != -1 && now - lastWarningTime <= com.navdy.hud.app.analytics.AnalyticsSupport.TEMP_WARNING_INTERVAL_MS)) {
                                        doWarning = false;
                                    }
                                    if (doWarning && com.navdy.hud.app.analytics.AnalyticsSupport.uptime() < com.navdy.hud.app.analytics.AnalyticsSupport.TEMP_WARNING_BOOT_DELAY_S) {
                                        doWarning = false;
                                    }
                                }
                                if (doWarning) {
                                    com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getBus().post(new com.navdy.service.library.events.ui.ShowScreen.Builder().screen(com.navdy.service.library.events.ui.Screen.SCREEN_TEMPERATURE_WARNING).build());
                                }
                                if (this.powerManager.inQuietMode() && doShutdown) {
                                    com.navdy.hud.app.analytics.AnalyticsSupport.sLogger.d("Shutting down due to high DMD temperature");
                                    com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getBus().post(new com.navdy.hud.app.event.Shutdown(com.navdy.hud.app.event.Shutdown.Reason.HIGH_TEMPERATURE));
                                }
                                java.lang.String str2 = com.navdy.hud.app.analytics.AnalyticsSupport.ANALYTICS_EVENT_TEMPERATURE_CHANGE;
                                java.lang.String[] strArr = new java.lang.String[28];
                                strArr[0] = com.navdy.hud.app.analytics.AnalyticsSupport.ATTR_TEMP_FAN_SPEED;
                                strArr[1] = fields[0];
                                strArr[2] = com.navdy.hud.app.analytics.AnalyticsSupport.ATTR_TEMP_CPU_TEMP;
                                strArr[3] = fields[1];
                                strArr[4] = com.navdy.hud.app.analytics.AnalyticsSupport.ATTR_TEMP_INLET_TEMP;
                                strArr[5] = fields[2];
                                strArr[6] = com.navdy.hud.app.analytics.AnalyticsSupport.ATTR_TEMP_DMD_TEMP;
                                strArr[7] = fields[3];
                                strArr[8] = com.navdy.hud.app.analytics.AnalyticsSupport.ATTR_TEMP_LED_TEMP;
                                strArr[9] = fields[4];
                                strArr[10] = com.navdy.hud.app.analytics.AnalyticsSupport.ATTR_TEMP_CPU_AVERAGE;
                                strArr[11] = fields[5];
                                strArr[12] = com.navdy.hud.app.analytics.AnalyticsSupport.ATTR_TEMP_INLET_AVERAGE;
                                strArr[13] = fields[6];
                                strArr[14] = com.navdy.hud.app.analytics.AnalyticsSupport.ATTR_TEMP_DMD_AVERAGE;
                                strArr[15] = fields[7];
                                strArr[16] = com.navdy.hud.app.analytics.AnalyticsSupport.ATTR_TEMP_LED_AVERAGE;
                                strArr[17] = fields[8];
                                strArr[18] = com.navdy.hud.app.analytics.AnalyticsSupport.ATTR_TEMP_CPU_MAX;
                                strArr[19] = fields[9];
                                strArr[20] = com.navdy.hud.app.analytics.AnalyticsSupport.ATTR_TEMP_INLET_MAX;
                                strArr[21] = fields[10];
                                strArr[22] = com.navdy.hud.app.analytics.AnalyticsSupport.ATTR_TEMP_DMD_MAX;
                                strArr[23] = fields[11];
                                strArr[24] = com.navdy.hud.app.analytics.AnalyticsSupport.ATTR_TEMP_LED_MAX;
                                strArr[25] = fields[12];
                                strArr[26] = com.navdy.hud.app.analytics.AnalyticsSupport.ATTR_TEMP_WARNING;
                                if (doWarning) {
                                    str = com.navdy.hud.app.analytics.AnalyticsSupport.TRUE;
                                } else {
                                    str = "false";
                                }
                                strArr[27] = str;
                                com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent(str2, strArr);
                            }
                        }
                    }
                } catch (java.lang.Exception e) {
                    com.navdy.hud.app.analytics.AnalyticsSupport.sLogger.e("error reading from temperature reporting socket", e);
                    retries++;
                    if (retries > 3) {
                        com.navdy.hud.app.analytics.AnalyticsSupport.sLogger.e("too many errors reading temperature reporting socket, giving up");
                        com.navdy.service.library.util.IOUtils.closeStream(socketInputStream);
                        com.navdy.service.library.util.IOUtils.closeStream(socket);
                    }
                } catch (Throwable th) {
                    com.navdy.service.library.util.IOUtils.closeStream(socketInputStream);
                    com.navdy.service.library.util.IOUtils.closeStream(socket);
                    throw th;
                }
            } catch (java.lang.Exception e2) {
                com.navdy.hud.app.analytics.AnalyticsSupport.sLogger.e("error connecting to temperature reporting socket", e2);
            }
        }
    }

    private static class Threshold {
        private final int max;
        private final long minimumTime;
        private long triggerTime = -1;

        Threshold(int max2, long minimumTime2) {
            this.max = max2;
            this.minimumTime = minimumTime2;
        }

        public void update(int val) {
            if (val < this.max) {
                this.triggerTime = -1;
            } else if (this.triggerTime == -1) {
                this.triggerTime = android.os.SystemClock.elapsedRealtime();
            }
        }

        /* access modifiers changed from: 0000 */
        public boolean isHit() {
            return this.triggerTime != -1 && android.os.SystemClock.elapsedRealtime() - this.triggerTime >= this.minimumTime;
        }
    }

    private static class VoiceResultsRunnable implements java.lang.Runnable {
        com.navdy.hud.app.analytics.AnalyticsSupport.VoiceSearchAdditionalResultsAction additionalResultsAction;
        private boolean done = false;
        boolean explicitRetry;
        java.lang.Integer listItemSelected = null;
        java.lang.String microphoneUsed;
        java.lang.String prefix = null;
        private com.navdy.service.library.events.navigation.NavigationRouteRequest request;
        private int retryCount;
        private java.lang.Integer searchConfidence;
        java.util.List<com.navdy.service.library.events.destination.Destination> searchResults;
        private java.lang.String searchWords;

        VoiceResultsRunnable(com.navdy.service.library.events.navigation.NavigationRouteRequest req) {
            this.request = req;
            this.searchWords = com.navdy.hud.app.analytics.AnalyticsSupport.voiceSearchWords;
            this.searchConfidence = com.navdy.hud.app.analytics.AnalyticsSupport.voiceSearchConfidence;
            this.retryCount = com.navdy.hud.app.analytics.AnalyticsSupport.voiceSearchCount - 1;
            this.microphoneUsed = com.navdy.hud.app.analytics.AnalyticsSupport.voiceSearchListeningOverBluetooth ? "bluetooth" : com.navdy.hud.app.device.gps.GpsConstants.USING_PHONE_LOCATION;
            this.searchResults = com.navdy.hud.app.analytics.AnalyticsSupport.voiceSearchResults;
            this.additionalResultsAction = com.navdy.hud.app.analytics.AnalyticsSupport.voiceSearchAdditionalResultsAction;
            this.explicitRetry = com.navdy.hud.app.analytics.AnalyticsSupport.voiceSearchExplicitRetry;
            this.listItemSelected = com.navdy.hud.app.analytics.AnalyticsSupport.voiceSearchListSelection;
            this.prefix = com.navdy.hud.app.analytics.AnalyticsSupport.voiceSearchPrefix;
        }

        public synchronized void run() {
            if (!this.done) {
                com.navdy.hud.app.analytics.AnalyticsSupport.sLogger.i("sending voice search event: " + this);
                com.navdy.hud.app.analytics.AnalyticsSupport.sendVoiceSearchEvent(null, this.request, this.searchWords, this.searchConfidence, this.retryCount, this.microphoneUsed, this.searchResults, this.additionalResultsAction, this.explicitRetry, this.listItemSelected, this.prefix);
                com.navdy.hud.app.analytics.AnalyticsSupport.voiceResultsRunnable = null;
                this.done = true;
            }
        }

        public synchronized void cancel(com.navdy.hud.app.analytics.AnalyticsSupport.VoiceSearchCancelReason reason) {
            if (!this.done) {
                com.navdy.hud.app.analytics.AnalyticsSupport.handler.removeCallbacks(this);
                com.navdy.hud.app.analytics.AnalyticsSupport.voiceResultsRunnable = null;
                this.done = true;
                com.navdy.hud.app.analytics.AnalyticsSupport.sLogger.i("cancelling voice search reason: " + reason + " data:" + this);
                com.navdy.hud.app.analytics.AnalyticsSupport.sendVoiceSearchEvent(reason, this.request, this.searchWords, this.searchConfidence, this.retryCount, this.microphoneUsed, this.searchResults, this.additionalResultsAction, this.explicitRetry, this.listItemSelected, this.prefix);
            }
        }

        public java.lang.String toString() {
            return "VoiceResultsRunnable{done=" + this.done + ", request=" + this.request + ", searchWords='" + this.searchWords + '\'' + ", searchConfidence=" + this.searchConfidence + ", retryCount=" + this.retryCount + ", microphoneUsed='" + this.microphoneUsed + '\'' + ", searchResults=" + this.searchResults + ", additionalResultsAction=" + this.additionalResultsAction + ", explicitRetry=" + this.explicitRetry + ", listItemSelected=" + this.listItemSelected + ", prefix='" + this.prefix + '\'' + '}';
        }
    }

    public enum VoiceSearchAdditionalResultsAction {
        ADDITIONAL_RESULTS_TIMEOUT("timeout"),
        ADDITIONAL_RESULTS_LIST("list"),
        ADDITIONAL_RESULTS_GO("go");
        
        public final java.lang.String tag;

        private VoiceSearchAdditionalResultsAction(java.lang.String tag2) {
            this.tag = tag2;
        }
    }

    private enum VoiceSearchCancelReason {
        end_trip,
        new_voice_search,
        new_non_voice_search,
        cancel_search,
        cancel_route_calculation,
        cancel_list
    }

    private enum VoiceSearchProgress {
        PROGRESS_SEARCHING,
        PROGRESS_DESTINATION_FOUND,
        PROGRESS_LIST_DISPLAYED,
        PROGRESS_ROUTE_SELECTED
    }

    public enum WakeupReason {
        POWERBUTTON("Power_Button"),
        VOLTAGE_SPIKE("Voltage_Spike"),
        DIAL("Dial"),
        PHONE("Phone");
        
        public final java.lang.String attr;

        private WakeupReason(java.lang.String attr2) {
            this.attr = attr2;
        }
    }

    public static void setGlanceNavigationSource(java.lang.String source) {
        glanceNavigationSource = source;
    }

    public static void recordNavigation(boolean started) {
        if (started) {
            localyticsSendEvent(ANALYTICS_EVENT_NAVIGATION_DESTINATION_SET, new java.lang.String[0]);
            destinationSet = true;
        }
    }

    /* access modifiers changed from: private */
    public static void recordOfflineMapsChange(android.content.SharedPreferences preferences) {
        java.lang.String previousVersion = preferences.getString(PREVIOUS_OFFLINE_MAPS_VERSION, null);
        java.lang.String previousPackages = preferences.getString(PREVIOUS_OFFLINE_MAPS_PACKAGES, null);
        java.lang.String previousBuildDate = preferences.getString(PREVIOUS_OFFLINE_MAPS_DATE, null);
        com.navdy.hud.app.maps.here.HereMapsManager mapsManager = com.navdy.hud.app.maps.here.HereMapsManager.getInstance();
        if (mapsManager.isInitialized()) {
            com.navdy.hud.app.maps.here.HereOfflineMapsVersion offlineMapsVersion = mapsManager.getOfflineMapsVersion();
            if (offlineMapsVersion != null) {
                java.util.List<java.lang.String> packageList = offlineMapsVersion.getPackages();
                java.lang.String packages = packageList != null ? android.text.TextUtils.join(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA, packageList) : "";
                java.lang.String version = offlineMapsVersion.getVersion();
                java.lang.String buildDate = offlineMapsVersion.getRawDate();
                if (version == null) {
                    version = "";
                }
                if (buildDate == null) {
                    buildDate = "";
                }
                if (!packages.equals(previousPackages) || !buildDate.equals(previousBuildDate) || !version.equals(previousVersion)) {
                    int count = preferences.getInt(OFFLINE_MAPS_UPDATE_COUNT, 0) + 1;
                    com.navdy.hud.app.analytics.Event event = new com.navdy.hud.app.analytics.Event(ANALYTICS_EVENT_OFFLINE_MAPS_UPDATED, ATTR_OFFLINE_MAPS_UPDATED_OLD_DATE, previousBuildDate, ATTR_OFFLINE_MAPS_UPDATED_OLD_VERSION, previousVersion, ATTR_OFFLINE_MAPS_UPDATED_OLD_PACKAGES, previousPackages, ATTR_OFFLINE_MAPS_UPDATED_NEW_VERSION, version, ATTR_OFFLINE_MAPS_UPDATED_NEW_PACKAGES, packages, ATTR_OFFLINE_MAPS_UPDATED_NEW_DATE, buildDate, ATTR_OFFLINE_MAPS_UPDATED_UPDATE_COUNT, java.lang.String.valueOf(count));
                    sLogger.i("Recording offline maps change:" + event);
                    localyticsSendEvent(event, false);
                    preferences.edit().putString(PREVIOUS_OFFLINE_MAPS_DATE, buildDate).putString(PREVIOUS_OFFLINE_MAPS_PACKAGES, packages).putString(PREVIOUS_OFFLINE_MAPS_VERSION, version).putInt(OFFLINE_MAPS_UPDATE_COUNT, count).apply();
                }
            }
        }
    }

    public static void recordCpuOverheat(java.lang.String state) {
        java.lang.String uptime = java.lang.Long.toString(uptime());
        sLogger.i("Recording CPU overheat event uptime:" + uptime + " state:" + state);
        localyticsSendEvent(ANALYTICS_EVENT_CPU_OVERHEAT, "Uptime", uptime, "State", state);
    }

    public static void recordOptionSelection(java.lang.String option) {
        localyticsSendEvent(ANALYTICS_EVENT_OPTION_SELECTED, "Name", option);
    }

    public static void recordPhoneDisconnect(boolean reconnected, java.lang.String elapsedTime) {
        boolean moving;
        if (com.navdy.hud.app.manager.SpeedManager.getInstance().getCurrentSpeed() > 0) {
            moving = true;
        } else {
            moving = false;
        }
        if (remoteDevice != null) {
            java.lang.String make = null;
            java.lang.String model = null;
            java.lang.String year = null;
            if (sLastProfile != null) {
                make = sLastProfile.getCarMake();
                model = sLastProfile.getCarModel();
                year = sLastProfile.getCarYear();
            }
            java.lang.String str = ANALYTICS_EVENT_PHONE_DISCONNECTED;
            java.lang.String[] strArr = new java.lang.String[28];
            strArr[0] = ATTR_DISC_STATUS;
            strArr[1] = reconnected ? "reconnected" : "failed";
            strArr[2] = "Reason";
            strArr[3] = java.lang.Integer.toString(lastDiscReason);
            strArr[4] = ATTR_DISC_FORCE_RECONNECT;
            strArr[5] = java.lang.Boolean.toString(disconnectDueToForceReconnect);
            strArr[6] = ATTR_DISC_FORCE_RECONNECT_REASON;
            strArr[7] = lastForceReconnectReason;
            strArr[8] = ATTR_DISC_MOVING;
            strArr[9] = java.lang.Boolean.toString(moving);
            strArr[10] = ATTR_DISC_PHONE_TYPE;
            strArr[11] = remoteDevice.platform.toString();
            strArr[12] = ATTR_DISC_PHONE_OS;
            strArr[13] = remoteDevice.systemVersion;
            strArr[14] = ATTR_DISC_PHONE_MAKE;
            strArr[15] = remoteDevice.deviceMake;
            strArr[16] = ATTR_DISC_PHONE_MODEL;
            strArr[17] = remoteDevice.model;
            strArr[18] = ATTR_DISC_ELAPSED_TIME;
            strArr[19] = elapsedTime;
            strArr[20] = "Car_Make";
            strArr[21] = make;
            strArr[22] = "Car_Model";
            strArr[23] = model;
            strArr[24] = "Car_Year";
            strArr[25] = year;
            strArr[26] = "VIN";
            strArr[27] = sLastVIN;
            localyticsSendEvent(str, strArr);
            sLastVIN = null;
            sLastProfile = null;
            remoteDevice = null;
            lastDiscReason = 0;
            lastForceReconnectReason = "";
            disconnectDueToForceReconnect = false;
        }
    }

    private static java.lang.String isEnabled(java.lang.Boolean val) {
        return (val == null || !val.booleanValue()) ? com.amazonaws.services.s3.model.BucketLifecycleConfiguration.DISABLED : "Enabled";
    }

    /* access modifiers changed from: private */
    public static com.navdy.hud.app.analytics.Event recordPreference(com.navdy.hud.app.profile.DriverProfile profile) {
        java.lang.String glances;
        com.navdy.service.library.events.preferences.InputPreferences inputPreferences = profile.getInputPreferences();
        com.navdy.service.library.events.preferences.NotificationPreferences notificationPreferences = profile.getNotificationPreferences();
        com.navdy.service.library.events.preferences.LocalPreferences localPreferences = profile.getLocalPreferences();
        com.navdy.service.library.events.preferences.AudioPreferences audioPreferences = profile.getAudioPreferences();
        java.lang.String units = profile.getUnitSystem() == com.navdy.service.library.events.preferences.DriverProfilePreferences.UnitSystem.UNIT_SYSTEM_IMPERIAL ? "Imperial" : "Metric";
        java.lang.String displayFormat = profile.getDisplayFormat() == com.navdy.service.library.events.preferences.DriverProfilePreferences.DisplayFormat.DISPLAY_FORMAT_COMPACT ? "Compact" : "Normal";
        java.lang.String featureMode = "Unknown";
        switch (profile.getFeatureMode()) {
            case FEATURE_MODE_BETA:
                featureMode = "Beta";
                break;
            case FEATURE_MODE_RELEASE:
                featureMode = "Release";
                break;
            case FEATURE_MODE_EXPERIMENTAL:
                featureMode = "Experimental";
                break;
        }
        if (!notificationPreferences.enabled.booleanValue()) {
            glances = com.amazonaws.services.s3.model.BucketLifecycleConfiguration.DISABLED;
        } else if (notificationPreferences.readAloud.booleanValue() && !notificationPreferences.showContent.booleanValue()) {
            glances = "Read_Aloud";
        } else if (notificationPreferences.readAloud.booleanValue() || !notificationPreferences.showContent.booleanValue()) {
            glances = "Read_and_Show";
        } else {
            glances = "Show_Content";
        }
        com.navdy.service.library.events.preferences.DriverProfilePreferences.ObdScanSetting scanSetting = profile.getObdScanSetting();
        java.lang.String str = ANALYTICS_EVENT_DRIVER_PREFERENCES;
        java.lang.String[] strArr = new java.lang.String[22];
        strArr[0] = ATTR_DRIVER_PREFERENCES_LOCALE;
        strArr[1] = profile.getLocale().toString();
        strArr[2] = ATTR_DRIVER_PREFERENCES_UNITS;
        strArr[3] = units;
        strArr[4] = ATTR_DRIVER_PREFERENCES_AUTO_ON;
        strArr[5] = isEnabled(java.lang.Boolean.valueOf(profile.isAutoOnEnabled()));
        strArr[6] = ATTR_DRIVER_PREFERENCES_DISPLAY_FORMAT;
        strArr[7] = displayFormat;
        strArr[8] = ATTR_DRIVER_PREFERENCES_FEATURE_MODE;
        strArr[9] = featureMode;
        strArr[10] = ATTR_DRIVER_PREFERENCES_GESTURES;
        strArr[11] = isEnabled(inputPreferences.use_gestures);
        strArr[12] = ATTR_DRIVER_PREFERENCES_GLANCES;
        strArr[13] = glances;
        strArr[14] = ATTR_DRIVER_PREFERENCES_OBD;
        strArr[15] = scanSetting != null ? scanSetting.name() : com.amazonaws.services.s3.internal.Constants.NULL_VERSION_ID;
        strArr[16] = ATTR_DRIVER_PREFERENCES_LIMIT_BANDWIDTH;
        strArr[17] = isEnabled(java.lang.Boolean.valueOf(profile.isLimitBandwidthModeOn()));
        strArr[18] = ATTR_DRIVER_PREFERENCES_MANUAL_ZOOM;
        strArr[19] = isEnabled(localPreferences.manualZoom);
        strArr[20] = ATTR_DRIVER_PREFERENCES_DRIVING_BEHAVIOR_ALERTS;
        strArr[21] = isEnabled(audioPreferences.drivingBehaviorAlerts);
        return new com.navdy.hud.app.analytics.Event(str, strArr);
    }

    public static void recordPreferenceChange(com.navdy.hud.app.profile.DriverProfile profile) {
        if (sLastProfilePreferences != null) {
            sLastProfilePreferences.enqueue(profile);
        }
    }

    public static void recordNavigationPreferenceChange(com.navdy.hud.app.profile.DriverProfile profile) {
        if (sLastNavigationPreferences != null) {
            sLastNavigationPreferences.enqueue(profile);
        }
    }

    /* access modifiers changed from: private */
    public static com.navdy.hud.app.analytics.Event recordNavigationPreference(com.navdy.hud.app.profile.DriverProfile profile) {
        com.navdy.service.library.events.preferences.NavigationPreferences prefs = profile.getNavigationPreferences();
        java.lang.String routingType = prefs.routingType == com.navdy.service.library.events.preferences.NavigationPreferences.RoutingType.ROUTING_FASTEST ? "Fastest" : "Shortest";
        java.lang.String reroute = "Unknown";
        switch (prefs.rerouteForTraffic) {
            case REROUTE_AUTOMATIC:
                reroute = "Automatic";
                break;
            case REROUTE_CONFIRM:
                reroute = "Confirm";
                break;
            case REROUTE_NEVER:
                reroute = "Never";
                break;
        }
        return new com.navdy.hud.app.analytics.Event(ANALYTICS_EVENT_NAVIGATION_PREFERENCES, ATTR_NAVIGATION_PREFERENCES_REROUTE_FOR_TRAFFIC, reroute, ATTR_NAVIGATION_PREFERENCES_ROUTING_TYPE, routingType, ATTR_NAVIGATION_PREFERENCES_SPOKEN_TBT, isEnabled(prefs.spokenTurnByTurn), ATTR_NAVIGATION_PREFERENCES_SPOKEN_SPEED_WARNINGS, isEnabled(prefs.spokenSpeedLimitWarnings), ATTR_NAVIGATION_PREFERENCES_SHOW_TRAFFIC_OPEN_MAP, isEnabled(prefs.showTrafficInOpenMap), ATTR_NAVIGATION_PREFERENCES_SHOW_TRAFFIC_NAVIGATING, isEnabled(prefs.showTrafficWhileNavigating), ATTR_NAVIGATION_PREFERENCES_ALLOW_HIGHWAYS, isEnabled(prefs.allowHighways), ATTR_NAVIGATION_PREFERENCES_ALLOW_TOLL_ROADS, isEnabled(prefs.allowTollRoads), ATTR_NAVIGATION_PREFERENCES_ALLOW_FERRIES, isEnabled(prefs.allowFerries), ATTR_NAVIGATION_PREFERENCES_ALLOW_TUNNELS, isEnabled(prefs.allowTunnels), ATTR_NAVIGATION_PREFERENCES_ALLOW_UNPAVED_ROADS, isEnabled(prefs.allowUnpavedRoads), ATTR_NAVIGATION_PREFERENCES_ALLOW_AUTO_TRAINS, isEnabled(prefs.allowAutoTrains), ATTR_NAVIGATION_PREFERENCES_ALLOW_HOV_LANES, isEnabled(prefs.allowHOVLanes));
    }

    public static void recordIncorrectFuelData() {
        java.lang.String make = null;
        java.lang.String model = null;
        java.lang.String year = null;
        com.navdy.hud.app.profile.DriverProfile currentProfile = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getDriverProfileManager().getCurrentProfile();
        if (currentProfile != null) {
            make = currentProfile.getCarMake();
            model = currentProfile.getCarModel();
            year = currentProfile.getCarYear();
        }
        localyticsSendEvent(ANALYTICS_EVENT_ZERO_FUEL_DATA, "Car_Make", make, "Car_Model", model, "Car_Year", year);
    }

    /* access modifiers changed from: private */
    public static int parseTemp(java.lang.String tempStr) {
        int result = -1000;
        try {
            return java.lang.Integer.parseInt(tempStr);
        } catch (java.lang.NumberFormatException e) {
            sLogger.e("invalid temperature: " + tempStr);
            return result;
        }
    }

    public static int getCurrentTemperature() {
        return currentCpuAvgTemperature;
    }

    private static java.lang.String getHwVersion() {
        return com.navdy.hud.app.util.SerialNumber.instance.configurationCode + com.navdy.hud.app.util.SerialNumber.instance.revisionCode;
    }

    private static void setupCustomDimensions() {
        com.localytics.android.Localytics.setCustomDimension(0, getHwVersion());
    }

    private static boolean isTimeValid() {
        if (java.util.Calendar.getInstance().get(1) >= 2016) {
            return true;
        }
        return false;
    }

    public static void analyticsApplicationInit(android.app.Application app) {
        app.registerActivityLifecycleCallbacks(new com.localytics.android.LocalyticsActivityLifecycleCallbacks(app));
        com.localytics.android.Localytics.setPushDisabled(true);
        setupCustomDimensions();
        sNotificationReceiver = new com.navdy.hud.app.analytics.AnalyticsSupport.NotificationReceiver();
        startTime = java.util.concurrent.TimeUnit.NANOSECONDS.toSeconds(java.lang.System.nanoTime());
        com.navdy.hud.app.analytics.AnalyticsSupport.TemperatureReporter tempReporter = new com.navdy.hud.app.analytics.AnalyticsSupport.TemperatureReporter();
        tempReporter.setName("TemperatureReporter");
        tempReporter.start();
        mortar.Mortar.inject(com.navdy.hud.app.HudApplication.getAppContext(), tempReporter);
        sObdManager = com.navdy.hud.app.obd.ObdManager.getInstance();
        sFuelMonitor = new com.navdy.hud.app.analytics.AnalyticsSupport.FuelMonitor(null);
        quietMode = sNotificationReceiver.powerManager.inQuietMode();
        maybeOpenSession();
        android.content.IntentFilter intentFilter = new android.content.IntentFilter("android.bluetooth.device.action.ACL_DISCONNECTED");
        intentFilter.addAction(com.navdy.hud.app.service.HudConnectionService.ACTION_DEVICE_FORCE_RECONNECT);
        com.navdy.hud.app.HudApplication.getAppContext().registerReceiver(new com.navdy.hud.app.analytics.AnalyticsSupport.Anon2(), intentFilter);
    }

    /* access modifiers changed from: private */
    public static void maybeOpenSession() {
        boolean isTimeValid;
        if ((!quietMode) && isTimeValid()) {
            openSession();
        }
    }

    private static synchronized void openSession() {
        synchronized (com.navdy.hud.app.analytics.AnalyticsSupport.class) {
            if (!localyticsInitialized) {
                sLogger.i("openSession()");
                com.localytics.android.Localytics.openSession();
                localyticsInitialized = true;
                sendDeferredLocalyticsEvents();
                handler.postDelayed(sFuelMonitor, FUEL_REPORTING_INTERVAL);
                handler.postDelayed(sUploader, UPLOAD_INTERVAL_BOOT);
            }
        }
    }

    public static void recordDeviceConnect(com.navdy.service.library.events.DeviceInfo devInfo, long connectionTime, long linkTime) {
        remoteDevice = devInfo;
        long now = android.os.SystemClock.elapsedRealtime();
        com.navdy.hud.app.profile.DriverProfile driverProfile = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile();
        java.lang.String make = null;
        java.lang.String model = null;
        java.lang.String year = null;
        if (!driverProfile.isDefaultProfile()) {
            com.localytics.android.Localytics.setCustomerFullName(driverProfile.getDriverName());
            com.localytics.android.Localytics.setCustomerEmail(driverProfile.getDriverEmail());
            make = driverProfile.getCarMake();
            model = driverProfile.getCarModel();
            year = driverProfile.getCarYear();
            com.localytics.android.Localytics.setProfileAttribute(ATTR_PROFILE_SERIAL_NUMBER, android.os.Build.SERIAL);
            com.localytics.android.Localytics.setProfileAttribute("Car_Make", make);
            com.localytics.android.Localytics.setProfileAttribute("Car_Model", model);
            com.localytics.android.Localytics.setProfileAttribute("Car_Year", year);
            com.localytics.android.Localytics.setProfileAttribute(ATTR_PROFILE_HUD_VERSION, com.navdy.hud.app.util.os.SystemProperties.get("ro.build.description", ""));
            com.localytics.android.Localytics.setProfileAttribute(ATTR_PROFILE_BUILD_TYPE, android.os.Build.TYPE);
        }
        java.lang.String vin = sObdManager.getVin();
        com.navdy.hud.app.analytics.Event event = new com.navdy.hud.app.analytics.Event(ANALYTICS_EVENT_MOBILE_APP_CONNECT, ATTR_CONN_DEVICE_ID, android.os.Build.SERIAL, "Car_Make", make, "Car_Model", model, "Car_Year", year, "VIN", vin, ATTR_CONN_LINK_DELAY, java.lang.Long.toString(connectionTime - linkTime), ATTR_CONN_DEVICE_INFO_DELAY, java.lang.Long.toString(now - linkTime));
        if (devInfo != null) {
            sRemoteDeviceId = devInfo.deviceId;
            event.add(ATTR_CONN_REMOTE_DEVICE_ID, sRemoteDeviceId, ATTR_CONN_REMOTE_DEVICE_NAME, devInfo.deviceName, ATTR_CONN_REMOTE_CLIENT_VERSION, devInfo.clientVersion, ATTR_CONN_REMOTE_PLATFORM, devInfo.platform.toString(), ATTR_REMOTE_DEVICE_HARDWARE, devInfo.deviceMake + " " + devInfo.model, ATTR_REMOTE_DEVICE_SW_VERSION, devInfo.deviceMake + " " + devInfo.systemVersion);
        } else {
            sRemoteDeviceId = "unknown";
        }
        sLogger.i("recordDeviceConnect: " + event);
        localyticsSendEvent(event, false);
    }

    /* access modifiers changed from: private */
    public static synchronized void closeSession() {
        synchronized (com.navdy.hud.app.analytics.AnalyticsSupport.class) {
            if (localyticsInitialized) {
                sendDistance();
                sLogger.v("closeSession()");
                com.localytics.android.Localytics.closeSession();
                sRemoteDeviceId = "unknown";
            }
        }
    }

    public static void recordDownloadOTAUpdate(android.content.SharedPreferences sharedPreferences) {
        java.lang.String currentVersion = android.os.Build.VERSION.INCREMENTAL;
        java.lang.String updateVersion = com.navdy.hud.app.util.OTAUpdateService.getIncrementalUpdateVersion(sharedPreferences);
        localyticsSendEvent(ANALYTICS_EVENT_OTADOWNLOAD, ATTR_OTA_CURRENT_VERSION, currentVersion, ATTR_OTA_UPDATE_VERSION, updateVersion);
    }

    public static void recordInstallOTAUpdate(android.content.SharedPreferences sharedPreferences) {
        java.lang.String currentVersion = android.os.Build.VERSION.INCREMENTAL;
        java.lang.String updateVersion = com.navdy.hud.app.util.OTAUpdateService.getIncrementalUpdateVersion(sharedPreferences);
        localyticsSendEvent(ANALYTICS_EVENT_OTAINSTALL, ATTR_OTA_CURRENT_VERSION, currentVersion, ATTR_OTA_UPDATE_VERSION, updateVersion);
    }

    public static void recordOTAInstallResult(boolean incremental, java.lang.String priorVersion, boolean success) {
        java.lang.String str = ANALYTICS_EVENT_OTA_INSTALL_RESULT;
        java.lang.String[] strArr = new java.lang.String[6];
        strArr[0] = ATTR_OTA_INSTALL_INCREMENTAL;
        strArr[1] = incremental ? TRUE : "false";
        strArr[2] = ATTR_OTA_INSTALL_PRIOR_VERSION;
        strArr[3] = priorVersion;
        strArr[4] = com.navdy.hud.app.device.dial.DialConstants.DIAL_PAIRING_SUCCESS;
        strArr[5] = success ? TRUE : "false";
        localyticsSendEvent(str, strArr);
    }

    private static java.lang.String readLightSensor(java.lang.String devicePath) {
        java.lang.String result = "";
        try {
            result = com.navdy.service.library.util.IOUtils.convertFileToString(devicePath);
        } catch (java.lang.Exception e) {
            sLogger.e("exception reading light sensor device: " + devicePath, e);
        }
        return result.trim();
    }

    public static void recordBrightnessChanged(int prevBrightness, int newBrightness) {
        if (prevBrightness != newBrightness) {
            java.lang.String lightSensorValue = readLightSensor(LIGHT_SENSOR_DEVICE);
            java.lang.String lightSensorRawValue = readLightSensor(LIGHT_SENSOR_RAW_DEVICE);
            java.lang.String lightSensorRaw1Value = readLightSensor(LIGHT_SENSOR_RAW1_DEVICE);
            localyticsSendEvent(ANALYTICS_EVENT_BRIGHTNESS_CHANGE, "Previous", java.lang.Integer.toString(prevBrightness), "New", java.lang.Integer.toString(newBrightness), ATTR_BRIGHTNESS_LIGHT_SENSOR, lightSensorValue, ATTR_BRIGHTNESS_RAW_SENSOR, lightSensorRawValue, ATTR_BRIGHTNESS_RAW1_SENSOR, lightSensorRaw1Value);
        }
    }

    public static void recordAutobrightnessAdjustmentChanged(int brightness, int prevAdjustment, int newAdjustment) {
        if (prevAdjustment != newAdjustment) {
            java.lang.String lightSensorValue = readLightSensor(LIGHT_SENSOR_DEVICE);
            java.lang.String lightSensorRawValue = readLightSensor(LIGHT_SENSOR_RAW_DEVICE);
            java.lang.String lightSensorRaw1Value = readLightSensor(LIGHT_SENSOR_RAW1_DEVICE);
            localyticsSendEvent(ANALYTICS_EVENT_BRIGHTNESS_ADJUSTMENT_CHANGE, ATTR_BRIGHTNESS_BRIGHTNESS, java.lang.Integer.toString(brightness), "Previous", java.lang.Integer.toString(prevAdjustment), "New", java.lang.Integer.toString(newAdjustment), ATTR_BRIGHTNESS_LIGHT_SENSOR, lightSensorValue, ATTR_BRIGHTNESS_RAW_SENSOR, lightSensorRawValue, ATTR_BRIGHTNESS_RAW1_SENSOR, lightSensorRaw1Value);
        }
    }

    public static void recordAdaptiveAutobrightnessAdjustmentChanged(int brightness) {
        java.lang.String lightSensorValue = readLightSensor(LIGHT_SENSOR_DEVICE);
        java.lang.String lightSensorRawValue = readLightSensor(LIGHT_SENSOR_RAW_DEVICE);
        java.lang.String lightSensorRaw1Value = readLightSensor(LIGHT_SENSOR_RAW1_DEVICE);
        localyticsSendEvent(ANALYTICS_EVENT_ADAPTIVE_BRIGHTNESS_ADJUSTMENT_CHANGE, ATTR_BRIGHTNESS_BRIGHTNESS, java.lang.Integer.toString(brightness), ATTR_BRIGHTNESS_LIGHT_SENSOR, lightSensorValue, ATTR_BRIGHTNESS_RAW_SENSOR, lightSensorRawValue, ATTR_BRIGHTNESS_RAW1_SENSOR, lightSensorRaw1Value);
    }

    public static void recordKeyFailure(java.lang.String keyType, java.lang.String error) {
        localyticsSendEvent(ANALYTICS_EVENT_KEY_FAILED, ATTR_KEY_FAIL_KEY_TYPE, keyType, ATTR_KEY_FAIL_ERROR, error);
    }

    private static java.lang.String pidListToHexString(java.util.List<com.navdy.obd.Pid> pList) {
        long[] bits = new long[5];
        for (com.navdy.obd.Pid p : pList) {
            int v = p.getId();
            int i = v / 64;
            bits[i] = bits[i] | (1 << (v % 64));
        }
        java.lang.String sep = "";
        java.lang.String res = "";
        for (int i2 = bits.length - 1; i2 >= 0; i2--) {
            res = res + sep + java.lang.String.format(java.util.Locale.US, "%08x", new java.lang.Object[]{java.lang.Long.valueOf(bits[i2])});
            sep = " ";
        }
        return res;
    }

    /* access modifiers changed from: private */
    public static void reportObdState() {
        java.util.List<com.navdy.obd.ECU> ecus = sObdManager.getEcus();
        int ecuCount = ecus == null ? 0 : ecus.size();
        if (ecuCount == 0) {
            sLogger.e("Obd connection with no ECUs");
        }
        sLastVIN = sObdManager.getVin();
        java.lang.String obdChipFirmwareVersion = sObdManager.getObdChipFirmwareVersion();
        java.lang.String[] attrs = new java.lang.String[((ecuCount * 2) + 6)];
        int idx = 0 + 1;
        attrs[0] = ATTR_OBD_CONN_FIRMWARE_VERSION;
        int idx2 = idx + 1;
        if (obdChipFirmwareVersion == null) {
            obdChipFirmwareVersion = "";
        }
        attrs[idx] = obdChipFirmwareVersion;
        int idx3 = idx2 + 1;
        attrs[idx2] = "VIN";
        int idx4 = idx3 + 1;
        attrs[idx3] = sLastVIN;
        int idx5 = idx4 + 1;
        attrs[idx4] = ATTR_OBD_CONN_PROTOCOL;
        int idx6 = idx5 + 1;
        attrs[idx5] = sObdManager.getProtocol();
        int idx7 = idx6;
        for (int i = 0; i < ecuCount; i++) {
            com.navdy.obd.ECU e = (com.navdy.obd.ECU) ecus.get(i);
            java.util.List<com.navdy.obd.Pid> pList = e.supportedPids.asList();
            int idx8 = idx7 + 1;
            attrs[idx7] = java.lang.String.format(java.util.Locale.US, "%s%d", new java.lang.Object[]{ATTR_OBD_CONN_ECU, java.lang.Integer.valueOf(i + 1)});
            idx7 = idx8 + 1;
            attrs[idx8] = java.lang.String.format(java.util.Locale.US, "Address: 0x%x, Pids: %s", new java.lang.Object[]{java.lang.Integer.valueOf(e.address), pidListToHexString(pList)});
        }
        localyticsSendEvent(ANALYTICS_EVENT_OBD_CONNECTED, attrs);
        if (deferredWakeup != null) {
            localyticsSendEvent(ANALYTICS_EVENT_WAKEUP, false, true, "Reason", deferredWakeup.reason.attr, com.navdy.hud.app.device.dial.DialConstants.DIAL_BATTERY_LEVEL, deferredWakeup.batteryLevel, ATTR_WAKEUP_BATTERY_DRAIN, deferredWakeup.batteryDrain, "Sleep_Time", deferredWakeup.sleepTime, ATTR_WAKEUP_MAX_BATTERY, deferredWakeup.maxBattery, "VIN", sLastVIN);
            deferredWakeup = null;
        }
    }

    public static void recordMenuSelection(java.lang.String name) {
        localyticsSendEvent(ANALYTICS_EVENT_MENU_SELECTION, "Name", name);
    }

    public static void recordBadRoutePosition(java.lang.String displayPos, java.lang.String navPos, java.lang.String streetAddress) {
        localyticsSendEvent(ANALYTICS_EVENT_BAD_ROUTE_POSITION, ATTR_ROUTE_DISPLAY_POS, displayPos, ATTR_ROUTE_NAV_POS, navPos, ATTR_ROUTE_STREET_ADDRESS, streetAddress);
    }

    public static void recordNearbySearchReturnMainMenu() {
        recordMenuSelection("back");
        localyticsSendEvent(ANALYTICS_EVENT_NEARBY_SEARCH_RETURN_MM, new java.lang.String[0]);
    }

    public static void recordNearbySearchClosed() {
        localyticsSendEvent(ANALYTICS_EVENT_NEARBY_SEARCH_CLOSED, new java.lang.String[0]);
    }

    public static void recordNearbySearchDismiss() {
        localyticsSendEvent(ANALYTICS_EVENT_NEARBY_SEARCH_DISMISS, new java.lang.String[0]);
    }

    public static void recordNearbySearchNoResult(boolean retry) {
        java.lang.String str = ANALYTICS_EVENT_NEARBY_SEARCH_NO_RESULTS;
        java.lang.String[] strArr = new java.lang.String[2];
        strArr[0] = "Action";
        strArr[1] = retry ? "Retry" : "Dismiss";
        localyticsSendEvent(str, strArr);
    }

    public static void recordNearbySearchResultsDismiss() {
        localyticsSendEvent(ANALYTICS_EVENT_NEARBY_SEARCH_RESULTS_DISMISS, new java.lang.String[0]);
    }

    public static void recordNearbySearchResultsView() {
        localyticsSendEvent(ANALYTICS_EVENT_NEARBY_SEARCH_RESULTS_VIEW, new java.lang.String[0]);
        nearbySearchDestScrollPosition = -1;
        nearbySearchPlaceType = null;
    }

    public static void recordNearbySearchSelection(com.navdy.service.library.events.places.PlaceType placeType, int pos) {
        localyticsSendEvent(ANALYTICS_EVENT_MENU_SELECTION, "Name", "nearby_places_results");
        java.lang.String str = ANALYTICS_EVENT_NEARBY_SEARCH_SELECTION;
        java.lang.String[] strArr = new java.lang.String[6];
        strArr[0] = ATTR_NEARBY_SEARCH_PLACE_TYPE;
        strArr[1] = placeType.toString();
        strArr[2] = ATTR_NEARBY_SEARCH_DEST_SELECTED_POSITION;
        strArr[3] = java.lang.Integer.toString(pos);
        strArr[4] = ATTR_NEARBY_SEARCH_DEST_SCROLLED;
        strArr[5] = nearbySearchDestScrollPosition > 0 ? TRUE : "false";
        localyticsSendEvent(str, strArr);
        nearbySearchPlaceType = placeType;
    }

    public static void recordNearbySearchResultsClose() {
        localyticsSendEvent(ANALYTICS_EVENT_NEARBY_SEARCH_RESULTS_CLOSE, new java.lang.String[0]);
    }

    public static void recordNearbySearchDestinationScroll(int pos) {
        if (pos > nearbySearchDestScrollPosition) {
            nearbySearchDestScrollPosition = pos;
        }
    }

    private static java.lang.String screenName(com.navdy.hud.app.screen.BaseScreen screen) {
        com.navdy.service.library.events.ui.Screen scr = screen.getScreen();
        java.lang.String name = scr.toString();
        if (scr != com.navdy.service.library.events.ui.Screen.SCREEN_HOME) {
            return name;
        }
        com.navdy.hud.app.ui.component.homescreen.HomeScreen.DisplayMode dm = ((com.navdy.hud.app.ui.component.homescreen.HomeScreen) screen).getDisplayMode();
        if (dm == null || !com.navdy.hud.app.maps.here.HereMapsManager.getInstance().isInitialized()) {
            return name;
        }
        return dm.toString() + (com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().isNavigationModeOn() ? "_ROUTE" : "");
    }

    /* access modifiers changed from: private */
    public static void recordNewDevicePaired() {
        newDevicePaired = true;
    }

    public static void recordScreen(com.navdy.hud.app.screen.BaseScreen screen) {
        if (screen.getScreen() == com.navdy.service.library.events.ui.Screen.SCREEN_WELCOME) {
            welcomeScreenTime = android.os.SystemClock.elapsedRealtime();
        } else if (screen.getScreen() == com.navdy.service.library.events.ui.Screen.SCREEN_HOME && !phonePaired && welcomeScreenTime != -1 && sRemoteDeviceManager.isRemoteDeviceConnected()) {
            long curTime = android.os.SystemClock.elapsedRealtime();
            java.lang.String str = ANALYTICS_EVENT_ATTEMPT_TO_CONNECT;
            java.lang.String[] strArr = new java.lang.String[4];
            strArr[0] = ATTR_CONN_ATTEMPT_NEW_DEVICE;
            strArr[1] = newDevicePaired ? TRUE : "false";
            strArr[2] = ATTR_CONN_ATTEMPT_TIME;
            strArr[3] = java.lang.Long.toString(curTime - welcomeScreenTime);
            localyticsSendEvent(str, strArr);
            welcomeScreenTime = -1;
            newDevicePaired = false;
            phonePaired = true;
        }
        if (localyticsInitialized) {
            com.localytics.android.Localytics.tagScreen(screenName(screen));
        }
    }

    public static void recordGlanceAction(java.lang.String action, com.navdy.hud.app.framework.notifications.INotification notification, java.lang.String navMethod) {
        java.lang.String glanceName;
        if (navMethod == null) {
            navMethod = glanceNavigationSource != null ? glanceNavigationSource : "unknown";
        }
        glanceNavigationSource = null;
        if (notification != null) {
            com.navdy.hud.app.framework.notifications.NotificationType nt = notification.getType();
            if (notification instanceof com.navdy.hud.app.framework.glance.GlanceNotification) {
                glanceName = ((com.navdy.hud.app.framework.glance.GlanceNotification) notification).getGlanceApp().toString();
            } else {
                glanceName = nt.toString();
            }
            localyticsSendEvent(action, ATTR_GLANCE_NAME, glanceName, ATTR_GLANCE_NAV_METHOD, navMethod);
            return;
        }
        localyticsSendEvent(action, ATTR_GLANCE_NAV_METHOD, navMethod);
    }

    /* access modifiers changed from: private */
    public static void clearVoiceSearch() {
        voiceSearchProgress = null;
        voiceSearchWords = null;
        voiceSearchConfidence = null;
        voiceSearchListeningOverBluetooth = false;
        voiceSearchResults = null;
        voiceSearchAdditionalResultsAction = null;
        voiceSearchExplicitRetry = false;
        voiceSearchListSelection = null;
        voiceSearchPrefix = null;
    }

    public static void recordVoiceSearchEntered() {
        if (voiceSearchProgress == com.navdy.hud.app.analytics.AnalyticsSupport.VoiceSearchProgress.PROGRESS_LIST_DISPLAYED) {
            clearVoiceSearch();
            voiceSearchExplicitRetry = true;
        }
    }

    public static void recordVoiceSearchRetry() {
        voiceSearchExplicitRetry = true;
    }

    public static void recordVoiceSearchAdditionalResultsAction(com.navdy.hud.app.analytics.AnalyticsSupport.VoiceSearchAdditionalResultsAction action) {
        voiceSearchAdditionalResultsAction = action;
        if (action == com.navdy.hud.app.analytics.AnalyticsSupport.VoiceSearchAdditionalResultsAction.ADDITIONAL_RESULTS_LIST) {
            voiceSearchProgress = com.navdy.hud.app.analytics.AnalyticsSupport.VoiceSearchProgress.PROGRESS_LIST_DISPLAYED;
        } else {
            voiceSearchProgress = com.navdy.hud.app.analytics.AnalyticsSupport.VoiceSearchProgress.PROGRESS_ROUTE_SELECTED;
        }
    }

    public static void recordVoiceSearchListItemSelection(int selection) {
        if (selection == -1 || selection == -2) {
            voiceSearchExplicitRetry = true;
            return;
        }
        voiceSearchListSelection = java.lang.Integer.valueOf(selection);
        voiceSearchProgress = com.navdy.hud.app.analytics.AnalyticsSupport.VoiceSearchProgress.PROGRESS_ROUTE_SELECTED;
    }

    public static void recordVoiceSearchResult(com.navdy.service.library.events.audio.VoiceSearchResponse resp) {
        switch (resp.state) {
            case VOICE_SEARCH_STARTING:
                voiceSearchCount++;
                if (voiceSearchProgress != null) {
                    sLogger.w("starting voice search with unexpected progress state " + voiceSearchProgress);
                    clearVoiceSearch();
                }
                sLogger.v("starting voice search");
                voiceSearchProgress = com.navdy.hud.app.analytics.AnalyticsSupport.VoiceSearchProgress.PROGRESS_SEARCHING;
                return;
            case VOICE_SEARCH_LISTENING:
                if (resp.listeningOverBluetooth != null) {
                    voiceSearchListeningOverBluetooth = resp.listeningOverBluetooth.booleanValue();
                    return;
                }
                return;
            case VOICE_SEARCH_SUCCESS:
                sLogger.v("voice search successful");
                voiceSearchProgress = com.navdy.hud.app.analytics.AnalyticsSupport.VoiceSearchProgress.PROGRESS_DESTINATION_FOUND;
                if (resp.listeningOverBluetooth != null) {
                    voiceSearchListeningOverBluetooth = resp.listeningOverBluetooth.booleanValue();
                }
                voiceSearchResults = resp.results;
                break;
            case VOICE_SEARCH_SEARCHING:
                break;
            case VOICE_SEARCH_ERROR:
                sLogger.v("voice search error: " + resp.error);
                clearVoiceSearch();
                sendVoiceSearchErrorEvent(resp.error, voiceSearchCount - 1);
                return;
            default:
                return;
        }
        if (resp.recognizedWords != null) {
            voiceSearchWords = resp.recognizedWords;
        }
        if (resp.confidenceLevel != null) {
            voiceSearchConfidence = resp.confidenceLevel;
        }
        if (resp.prefix != null) {
            voiceSearchPrefix = resp.prefix;
        }
    }

    private static int countWords(java.lang.String s) {
        int count = 0;
        if (s != null) {
            java.lang.String s2 = s.trim();
            int i = 0;
            int slen = s2.length();
            while (i < slen) {
                count++;
                while (i < slen && s2.charAt(i) != ' ') {
                    i++;
                }
                while (i < slen && s2.charAt(i) == ' ') {
                    i++;
                }
            }
        }
        return count;
    }

    private static java.lang.String getVoiceSearchEvent() {
        java.lang.String phone_type = remoteDevice == null ? null : remoteDevice.platform.toString();
        if (phone_type != null) {
            return phone_type.equals("PLATFORM_Android") ? ANALYTICS_EVENT_VOICE_SEARCH_ANDROID : ANALYTICS_EVENT_VOICE_SEARCH_IOS;
        }
        return null;
    }

    private static java.lang.String getClientVersion() {
        com.navdy.service.library.events.DeviceInfo devInfo = sRemoteDeviceManager.getRemoteDeviceInfo();
        java.lang.String clientVersion = devInfo == null ? null : devInfo.clientVersion;
        if (clientVersion != null) {
            return clientVersion;
        }
        sLogger.e("failed to get client version");
        return "";
    }

    private static void sendVoiceSearchErrorEvent(com.navdy.service.library.events.audio.VoiceSearchResponse.VoiceSearchError error, int retryCount) {
        java.lang.String voiceSearchError;
        java.lang.String event = getVoiceSearchEvent();
        if (event == null) {
            sLogger.e("cannot determine phone_type for voice search");
            return;
        }
        java.lang.String[] strArr = new java.lang.String[6];
        strArr[0] = ATTR_VOICE_SEARCH_RESPONSE;
        if (error == null) {
            voiceSearchError = "";
        } else {
            voiceSearchError = error.toString();
        }
        strArr[1] = voiceSearchError;
        strArr[2] = ATTR_VOICE_SEARCH_RETRY_COUNT;
        strArr[3] = java.lang.Integer.toString(retryCount);
        strArr[4] = ATTR_VOICE_SEARCH_CLIENT_VERSION;
        strArr[5] = getClientVersion();
        localyticsSendEvent(event, strArr);
    }

    /* access modifiers changed from: private */
    public static void sendVoiceSearchEvent(com.navdy.hud.app.analytics.AnalyticsSupport.VoiceSearchCancelReason reason, com.navdy.service.library.events.navigation.NavigationRouteRequest request, java.lang.String words, java.lang.Integer confidence, int retryCount, java.lang.String microphoneUsed, java.util.List<com.navdy.service.library.events.destination.Destination> searchResults, com.navdy.hud.app.analytics.AnalyticsSupport.VoiceSearchAdditionalResultsAction additionalResultsAction, boolean explicitRetry, java.lang.Integer listItemSelection, java.lang.String prefix) {
        java.lang.String event = getVoiceSearchEvent();
        if (event == null) {
            sLogger.e("cannot determine phone_type for voice search");
            return;
        }
        java.util.Map<java.lang.String, java.lang.String> args = new java.util.HashMap<>();
        args.put(ATTR_VOICE_SEARCH_RESPONSE, com.navdy.hud.app.device.dial.DialConstants.DIAL_PAIRING_SUCCESS);
        args.put(ATTR_VOICE_SEARCH_CLIENT_VERSION, getClientVersion());
        if (confidence != null) {
            args.put(ATTR_VOICE_SEARCH_CONFIDENCE_LEVEL, confidence.toString());
        }
        if (words != null) {
            args.put(ATTR_VOICE_SEARCH_NUMBER_OF_WORDS, java.lang.Integer.toString(countWords(words)));
        }
        boolean cancelledQuickly = reason == com.navdy.hud.app.analytics.AnalyticsSupport.VoiceSearchCancelReason.end_trip || reason == com.navdy.hud.app.analytics.AnalyticsSupport.VoiceSearchCancelReason.new_voice_search || reason == com.navdy.hud.app.analytics.AnalyticsSupport.VoiceSearchCancelReason.new_non_voice_search;
        args.put(ATTR_VOICE_SEARCH_NAVIGATION_INITIATED, (reason == null || cancelledQuickly) ? TRUE : "false");
        args.put(ATTR_VOICE_SEARCH_NAV_CANCELLED_QUICKLY, cancelledQuickly ? TRUE : "false");
        args.put(ATTR_VOICE_SEARCH_RETRY_COUNT, java.lang.Integer.toString(retryCount));
        args.put(ATTR_VOICE_SEARCH_MICROPHONE_USED, microphoneUsed);
        args.put(ATTR_VOICE_SEARCH_NUM_RESULTS, java.lang.Integer.toString(searchResults == null ? 0 : searchResults.size()));
        if (additionalResultsAction != null) {
            args.put(ATTR_VOICE_SEARCH_ADDITIONAL_RESULTS_GLANCE, additionalResultsAction.tag);
        }
        args.put(ATTR_VOICE_SEARCH_EXPLICIT_RETRY, explicitRetry ? TRUE : "false");
        if (reason != null) {
            args.put(ATTR_VOICE_SEARCH_CANCEL_TYPE, reason.toString());
        }
        if (listItemSelection != null) {
            args.put(ATTR_VOICE_SEARCH_LIST_SELECTION, listItemSelection.toString());
        }
        boolean isSuggestion = false;
        java.lang.String destType = "ADDRESS";
        if (request != null) {
            com.navdy.service.library.events.destination.Destination dest = request.requestDestination;
            isSuggestion = (dest == null || dest.suggestion_type == null || dest.suggestion_type == com.navdy.service.library.events.destination.Destination.SuggestionType.SUGGESTION_NONE) ? false : true;
            if (request.destinationType != null && request.destinationType != com.navdy.service.library.events.destination.Destination.FavoriteType.FAVORITE_NONE) {
                destType = request.destinationType.toString();
            } else if (request.label != null) {
                destType = "PLACE";
            }
            args.put(ATTR_VOICE_SEARCH_DESTINATION_TYPE, destType);
        }
        args.put(ATTR_VOICE_SEARCH_SUGGESTION, isSuggestion ? TRUE : "false");
        if (prefix != null) {
            args.put(ATTR_VOICE_SEARCH_PREFIX, prefix);
        }
        localyticsSendEvent(new com.navdy.hud.app.analytics.Event(event, args), false);
    }

    /* access modifiers changed from: private */
    public static void processNavigationRouteRequest(com.navdy.service.library.events.navigation.NavigationRouteRequest request) {
        boolean isVoiceSearchRoute;
        if (nearbySearchPlaceType != null) {
            localyticsSendEvent(ANALYTICS_EVENT_NEARBY_SEARCH_TRIP_INITIATED, new java.lang.String[0]);
            return;
        }
        if (voiceSearchProgress == com.navdy.hud.app.analytics.AnalyticsSupport.VoiceSearchProgress.PROGRESS_ROUTE_SELECTED || voiceSearchProgress == com.navdy.hud.app.analytics.AnalyticsSupport.VoiceSearchProgress.PROGRESS_DESTINATION_FOUND) {
            isVoiceSearchRoute = true;
        } else {
            isVoiceSearchRoute = false;
        }
        if (voiceResultsRunnable != null) {
            voiceResultsRunnable.cancel(isVoiceSearchRoute ? com.navdy.hud.app.analytics.AnalyticsSupport.VoiceSearchCancelReason.new_voice_search : com.navdy.hud.app.analytics.AnalyticsSupport.VoiceSearchCancelReason.new_non_voice_search);
        }
        if (isVoiceSearchRoute) {
            voiceResultsRunnable = new com.navdy.hud.app.analytics.AnalyticsSupport.VoiceResultsRunnable(request);
            handler.postDelayed(voiceResultsRunnable, NAVIGATION_CANCEL_TIMEOUT);
            clearVoiceSearch();
        } else if (voiceSearchProgress != null) {
            sLogger.w("Received NavigationRouteRequest with unexpected voice progress state: " + voiceSearchProgress);
        }
        voiceSearchCount = 0;
    }

    /* access modifiers changed from: private */
    public static void processNavigationArrivalEvent(com.navdy.hud.app.maps.MapEvents.ArrivalEvent unused) {
        if (nearbySearchPlaceType != null) {
            localyticsSendEvent(ANALYTICS_EVENT_NEARBY_SEARCH_ARRIVED, ATTR_NEARBY_SEARCH_PLACE_TYPE, nearbySearchPlaceType.toString());
            nearbySearchPlaceType = null;
        }
    }

    public static void recordRouteSelectionCancelled() {
        if (nearbySearchPlaceType != null) {
            sLogger.v("nearby search route calculation cancelled");
            localyticsSendEvent(ANALYTICS_EVENT_NEARBY_SEARCH_TRIP_CANCELLED, new java.lang.String[0]);
            nearbySearchPlaceType = null;
        }
        if (voiceSearchProgress != null) {
            com.navdy.hud.app.analytics.AnalyticsSupport.VoiceSearchCancelReason reason = null;
            if (voiceSearchProgress == com.navdy.hud.app.analytics.AnalyticsSupport.VoiceSearchProgress.PROGRESS_SEARCHING) {
                reason = com.navdy.hud.app.analytics.AnalyticsSupport.VoiceSearchCancelReason.cancel_search;
            } else if (voiceSearchProgress == com.navdy.hud.app.analytics.AnalyticsSupport.VoiceSearchProgress.PROGRESS_LIST_DISPLAYED) {
                reason = com.navdy.hud.app.analytics.AnalyticsSupport.VoiceSearchCancelReason.cancel_list;
            }
            if (reason == null) {
                sLogger.e("unexpected progress " + voiceSearchProgress + " in recordRouteSelectionCancelled()");
                clearVoiceSearch();
                return;
            }
            sLogger.v("route selection cancelled, reason = " + reason);
            sendVoiceSearchEvent(reason, null, null, voiceSearchConfidence, voiceSearchCount - 1, voiceSearchListeningOverBluetooth ? "bluetooth" : com.navdy.hud.app.device.gps.GpsConstants.USING_PHONE_LOCATION, voiceSearchResults, voiceSearchAdditionalResultsAction, voiceSearchExplicitRetry, null, voiceSearchPrefix);
        } else if (voiceResultsRunnable != null) {
            sLogger.v("route calculation cancelled");
            voiceResultsRunnable.cancel(com.navdy.hud.app.analytics.AnalyticsSupport.VoiceSearchCancelReason.cancel_route_calculation);
        }
    }

    public static void recordNavigationCancelled() {
        if (nearbySearchPlaceType != null) {
            localyticsSendEvent(ANALYTICS_EVENT_NEARBY_SEARCH_ENDED, new java.lang.String[0]);
            sLogger.v("navigation ended for nearby search trip");
            nearbySearchPlaceType = null;
        } else if (voiceResultsRunnable != null) {
            sLogger.v("navigation ended, cancelling voice search callback");
            voiceResultsRunnable.cancel(com.navdy.hud.app.analytics.AnalyticsSupport.VoiceSearchCancelReason.end_trip);
        }
    }

    private static java.lang.String normalizeName(java.lang.String name) {
        char[] ca = name.toCharArray();
        boolean wordStart = true;
        for (int i = 0; i < ca.length; i++) {
            char c = ca[i];
            if (wordStart) {
                c = java.lang.Character.toUpperCase(c);
            }
            wordStart = c == '_';
            ca[i] = c;
        }
        return new java.lang.String(ca);
    }

    public static void recordSwipedCalibration(java.lang.String eventName, java.lang.String[] args) {
        java.lang.String eventName2 = ANALYTICS_EVENT_SWIPED_PREFIX + normalizeName(eventName);
        for (int i = 0; i < args.length - 1; i += 2) {
            args[i] = normalizeName(args[i]);
        }
        localyticsSendEvent(eventName2, args);
    }

    public static void recordDestinationSuggestion(boolean accepted, boolean calendar) {
        java.lang.String str = ANALYTICS_EVENT_DESTINATION_SUGGESTION;
        java.lang.String[] strArr = new java.lang.String[4];
        strArr[0] = "Accepted";
        strArr[1] = accepted ? TRUE : "false";
        strArr[2] = com.navdy.hud.app.device.dial.DialConstants.DIAL_EVENT_TYPE;
        strArr[3] = calendar ? DESTINATION_SUGGESTION_TYPE_CALENDAR : DESTINATION_SUGGESTION_TYPE_SMART_SUGGESTION;
        localyticsSendEvent(str, strArr);
    }

    public static void recordUpdatePrompt(boolean accepted, boolean isDial, java.lang.String toVersion) {
        java.lang.String event = isDial ? ANALYTICS_EVENT_DIAL_UPDATE_PROMPT : ANALYTICS_EVENT_DISPLAY_UPDATE_PROMPT;
        java.lang.String[] strArr = new java.lang.String[4];
        strArr[0] = "Accepted";
        strArr[1] = accepted ? TRUE : "false";
        strArr[2] = ATTR_UPDATE_TO_VERSION;
        strArr[3] = toVersion;
        localyticsSendEvent(event, strArr);
    }

    public static void recordMusicAction(java.lang.String action) {
        localyticsSendEvent(ANALYTICS_EVENT_MUSIC_ACTION, com.navdy.hud.app.device.dial.DialConstants.DIAL_EVENT_TYPE, action);
    }

    public static void recordMusicBrowsePlayAction(java.lang.String collectionType) {
        localyticsSendEvent(collectionType + "_played", true, new java.lang.String[0]);
    }

    /* access modifiers changed from: private */
    public static long uptime() {
        return java.util.concurrent.TimeUnit.NANOSECONDS.toSeconds(java.lang.System.nanoTime()) - startTime;
    }

    private static void sendDistance() {
        int totalDistance = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getTripManager().getTotalDistanceTravelled() - startingDistance;
        if (totalDistance > 10 && !quietMode) {
            long duration = uptime();
            java.lang.String str = ANALYTICS_EVENT_DISTANCE_TRAVELLED;
            java.lang.String[] strArr = new java.lang.String[10];
            strArr[0] = "Distance";
            strArr[1] = java.lang.Long.toString((long) totalDistance);
            strArr[2] = ATTR_DURATION;
            strArr[3] = java.lang.Long.toString(duration);
            strArr[4] = ATTR_NAVIGATING;
            strArr[5] = destinationSet ? TRUE : "false";
            strArr[6] = ATTR_PHONE_CONNECTION;
            strArr[7] = phonePaired ? TRUE : "false";
            strArr[8] = "Vin";
            strArr[9] = sLastVIN;
            localyticsSendEvent(str, false, true, strArr);
        }
    }

    public static void recordStartingBatteryVoltage(double voltage) {
        if (startingBatteryVoltage == -1.0d) {
            startingBatteryVoltage = voltage;
            sLogger.i(java.lang.String.format(java.util.Locale.US, "starting battery voltage = %6.1f", new java.lang.Object[]{java.lang.Double.valueOf(voltage)}));
        }
    }

    public static void recordShutdown(com.navdy.hud.app.event.Shutdown.Reason reason, boolean isQuietMode) {
        localyticsSendEvent(ANALYTICS_EVENT_SHUTDOWN, false, true, com.navdy.hud.app.device.dial.DialConstants.DIAL_EVENT_TYPE, isQuietMode ? com.navdy.hud.app.device.PowerManager.QUIET_MODE : "full", "Reason", reason.attr, com.navdy.hud.app.device.dial.DialConstants.DIAL_BATTERY_LEVEL, java.lang.String.format(java.util.Locale.US, "%6.1f", new java.lang.Object[]{java.lang.Double.valueOf(sObdManager.getBatteryVoltage())}), quietMode ? "Sleep_Time" : "Uptime", java.lang.Long.toString(uptime()));
    }

    /* access modifiers changed from: private */
    public static void recordWakeup(com.navdy.hud.app.analytics.AnalyticsSupport.WakeupReason reason) {
        java.lang.String sleepTimeStr = java.lang.Long.toString(uptime());
        java.lang.String batteryLevelStr = java.lang.String.format(java.util.Locale.US, "%6.1f", new java.lang.Object[]{java.lang.Double.valueOf(sObdManager.getBatteryVoltage())});
        java.lang.String maxBatteryStr = java.lang.String.format(java.util.Locale.US, "%6.1f", new java.lang.Object[]{java.lang.Double.valueOf(sObdManager.getMaxBatteryVoltage())});
        java.lang.String vin = sObdManager.getVin();
        double batteryDrain = 0.0d;
        if (startingBatteryVoltage != -1.0d) {
            batteryDrain = startingBatteryVoltage - sObdManager.getMinBatteryVoltage();
        }
        java.lang.String batteryDrainStr = java.lang.String.format(java.util.Locale.US, "%6.1f", new java.lang.Object[]{java.lang.Double.valueOf(batteryDrain)});
        if (vin != null) {
            localyticsSendEvent(ANALYTICS_EVENT_WAKEUP, false, true, "Reason", reason.attr, com.navdy.hud.app.device.dial.DialConstants.DIAL_BATTERY_LEVEL, batteryLevelStr, ATTR_WAKEUP_BATTERY_DRAIN, batteryDrainStr, "Sleep_Time", sleepTimeStr, ATTR_WAKEUP_MAX_BATTERY, maxBatteryStr, "VIN", vin);
        } else {
            deferredWakeup = new com.navdy.hud.app.analytics.AnalyticsSupport.DeferredWakeup(reason, batteryLevelStr, batteryDrainStr, sleepTimeStr, maxBatteryStr);
        }
        startTime = java.util.concurrent.TimeUnit.NANOSECONDS.toSeconds(java.lang.System.nanoTime());
        startingDistance = sRemoteDeviceManager.getTripManager().getTotalDistanceTravelled();
    }

    public static void recordDashPreference(java.lang.String gauge, boolean leftSide) {
        java.lang.String str = ANALYTICS_EVENT_DASH_GAUGE_SETTING;
        java.lang.String[] strArr = new java.lang.String[4];
        strArr[0] = ATTR_GAUGE_SIDE;
        strArr[1] = leftSide ? ATTR_GAUGE_SIDE_LEFT : ATTR_GAUGE_SIDE_RIGHT;
        strArr[2] = ATTR_GAUGE_ID;
        strArr[3] = gauge;
        localyticsSendEvent(str, strArr);
    }

    public static void recordNavdyMileageEvent(double vehicleKiloMeters, double navdyKiloMeters, double navdyUsageRate) {
        localyticsSendEvent(ANALYTICS_EVENT_MILEAGE, ATTR_VEHICLE_KILO_METERS, java.lang.Double.toString(vehicleKiloMeters), ATTR_NAVDY_KILOMETERS, java.lang.Double.toString(navdyKiloMeters), ATTR_NAVDY_USAGE_RATE, java.lang.Double.toString(navdyUsageRate));
    }

    private static synchronized boolean deferLocalyticsEvent(java.lang.String tag, java.lang.String[] args) {
        boolean z;
        synchronized (com.navdy.hud.app.analytics.AnalyticsSupport.class) {
            if (!localyticsInitialized) {
                if (deferredEvents == null) {
                    deferredEvents = new java.util.ArrayList<>(5);
                }
                deferredEvents.add(new com.navdy.hud.app.analytics.AnalyticsSupport.DeferredLocalyticsEvent(tag, args));
                z = true;
            } else {
                z = false;
            }
        }
        return z;
    }

    private static synchronized void sendDeferredLocalyticsEvents() {
        synchronized (com.navdy.hud.app.analytics.AnalyticsSupport.class) {
            if (!localyticsInitialized) {
                sLogger.e("sendDeferredLocalyticsEvents() called before Localytics was initialized");
            } else if (deferredEvents != null) {
                sLogger.v("sending " + deferredEvents.size() + " deferred localytics events");
                java.util.Iterator it = deferredEvents.iterator();
                while (it.hasNext()) {
                    com.navdy.hud.app.analytics.AnalyticsSupport.DeferredLocalyticsEvent de = (com.navdy.hud.app.analytics.AnalyticsSupport.DeferredLocalyticsEvent) it.next();
                    localyticsSendEvent(de.tag, de.args);
                }
                deferredEvents = null;
            }
        }
    }

    public static void localyticsSendEvent(java.lang.String tag, java.lang.String... args) {
        localyticsSendEvent(tag, false, false, args);
    }

    /* access modifiers changed from: private */
    public static void localyticsSendEvent(java.lang.String tag, boolean includeDeviceInfo, java.lang.String... args) {
        localyticsSendEvent(tag, includeDeviceInfo, false, args);
    }

    private static void localyticsSendEvent(java.lang.String tag, boolean includeDeviceInfo, boolean immediate, java.lang.String... args) {
        if (immediate || !deferLocalyticsEvent(tag, args)) {
            com.navdy.hud.app.analytics.Event event = new com.navdy.hud.app.analytics.Event(tag, args);
            java.util.Map<java.lang.String, java.lang.String> argMap = event.argMap;
            if (includeDeviceInfo) {
                com.navdy.service.library.events.DeviceInfo deviceInfo = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getRemoteDeviceInfo();
                if (deviceInfo != null) {
                    if (deviceInfo.platform != null) {
                        argMap.put(ATTR_REMOTE_DEVICE_PLATFORM, deviceInfo.platform.name());
                    }
                    argMap.put(ATTR_REMOTE_DEVICE_HARDWARE, deviceInfo.deviceMake + " " + deviceInfo.model);
                    argMap.put(ATTR_REMOTE_DEVICE_SW_VERSION, deviceInfo.deviceMake + " " + deviceInfo.systemVersion);
                }
            }
            localyticsSendEvent(event, immediate);
        }
    }

    /* access modifiers changed from: private */
    public static void localyticsSendEvent(com.navdy.hud.app.analytics.Event event, boolean immediate) {
        if (immediate) {
            com.localytics.android.Localytics.tagEvent(event.tag, event.argMap);
        } else {
            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.analytics.AnalyticsSupport.EventSender(event), 1);
        }
    }

    /* access modifiers changed from: private */
    public static long getUpdateInterval() {
        if (com.navdy.hud.app.framework.network.NetworkBandwidthController.getInstance().isLimitBandwidthModeOn()) {
            return UPLOAD_INTERVAL_LIMITED_BANDWIDTH;
        }
        return UPLOAD_INTERVAL;
    }

    static void submitNavigationQualityReport(com.navdy.hud.app.analytics.NavigationQualityTracker.Report report) {
        localyticsSendEvent(ANALYTICS_EVENT_ROUTE_COMPLETED, ATTR_ROUTE_COMPLETED_EXPECTED_DURATION, java.lang.String.valueOf(report.expectedDuration), ATTR_ROUTE_COMPLETED_ACTUAL_DURATION, java.lang.String.valueOf(report.actualDuration), ATTR_ROUTE_COMPLETED_EXPECTED_DISTANCE, java.lang.String.valueOf(report.expectedDistance), ATTR_ROUTE_COMPLETED_ACTUAL_DISTANCE, java.lang.String.valueOf(report.actualDistance), ATTR_ROUTE_COMPLETED_RECALCULATIONS, java.lang.String.valueOf(report.nRecalculations), ATTR_ROUTE_COMPLETED_DURATION_VARIANCE, java.lang.String.valueOf(report.getDurationVariance()), ATTR_ROUTE_COMPLETED_DISTANCE_VARIANCE, java.lang.String.valueOf(report.getDistanceVariance()), ATTR_ROUTE_COMPLETED_TRAFFIC_LEVEL, report.trafficLevel.name());
    }

    public static void recordGlympseSent(boolean success, java.lang.String share, java.lang.String message, java.lang.String id) {
        sLogger.v("glympse sent id=" + id + " sucess=" + success + " share=" + share + " msg=" + message);
        localyticsSendEvent(ANALYTICS_EVENT_GLYMPSE_SENT, com.navdy.hud.app.device.dial.DialConstants.DIAL_PAIRING_SUCCESS, java.lang.String.valueOf(success), ATTR_GLYMPSE_SHARE, share, ATTR_GLYMPSE_MESSAGE, message, "Id", id);
    }

    public static void recordGlympseViewed(java.lang.String id) {
        sLogger.v("glympse viewed id=" + id);
        localyticsSendEvent(ANALYTICS_EVENT_GLYMPSE_VIEWED, "Id", id);
    }

    public static void recordObdCanBusDataSent(java.lang.String version) {
        if (sLastProfile != null) {
            java.lang.String make = sLastProfile.getCarMake();
            java.lang.String model = sLastProfile.getCarModel();
            java.lang.String year = sLastProfile.getCarYear();
            localyticsSendEvent(ANALYTICS_EVENT_OBD_LISTENING_POSTED, "Car_Make", make, "Car_Model", model, "Car_Year", year, "Version", version);
            return;
        }
        localyticsSendEvent(ANALYTICS_EVENT_OBD_LISTENING_POSTED, "Version", version);
    }

    public static void recordObdCanBusMonitoringState(java.lang.String vin, boolean success, int approxDataSize, int version) {
        if (sLastProfile != null) {
            java.lang.String make = sLastProfile.getCarMake();
            java.lang.String model = sLastProfile.getCarModel();
            java.lang.String year = sLastProfile.getCarYear();
            java.lang.String str = ANALYTICS_EVENT_OBD_LISTENING_STATUS;
            java.lang.String[] strArr = new java.lang.String[14];
            strArr[0] = "Vin";
            strArr[1] = vin;
            strArr[2] = "Car_Make";
            strArr[3] = make;
            strArr[4] = "Car_Model";
            strArr[5] = model;
            strArr[6] = "Car_Year";
            strArr[7] = year;
            strArr[8] = com.navdy.hud.app.device.dial.DialConstants.DIAL_PAIRING_SUCCESS;
            strArr[9] = success ? TRUE : "false";
            strArr[10] = "Version";
            strArr[11] = java.lang.Integer.toString(version);
            strArr[12] = ATTR_OBD_DATA_MONITORING_DATA_SIZE;
            strArr[13] = java.lang.Integer.toString(approxDataSize);
            localyticsSendEvent(str, strArr);
            return;
        }
        java.lang.String str2 = ANALYTICS_EVENT_OBD_LISTENING_STATUS;
        java.lang.String[] strArr2 = new java.lang.String[8];
        strArr2[0] = "Vin";
        strArr2[1] = vin;
        strArr2[2] = com.navdy.hud.app.device.dial.DialConstants.DIAL_PAIRING_SUCCESS;
        strArr2[3] = success ? TRUE : "false";
        strArr2[4] = "Version";
        strArr2[5] = java.lang.Integer.toString(version);
        strArr2[6] = ATTR_OBD_DATA_MONITORING_DATA_SIZE;
        strArr2[7] = java.lang.Integer.toString(approxDataSize);
        localyticsSendEvent(str2, strArr2);
    }

    public static void recordSmsSent(boolean success, java.lang.String platform, boolean cannedMessage) {
        sLogger.v("sms-sent success=" + success + " plaform=" + platform + " canned=" + cannedMessage);
        java.lang.String str = ANALYTICS_EVENT_SMS_SENT;
        java.lang.String[] strArr = new java.lang.String[6];
        strArr[0] = com.navdy.hud.app.device.dial.DialConstants.DIAL_PAIRING_SUCCESS;
        strArr[1] = java.lang.String.valueOf(success);
        strArr[2] = ATTR_SMS_PLATFORM;
        strArr[3] = platform;
        strArr[4] = ATTR_SMS_MESSAGE_TYPE;
        strArr[5] = cannedMessage ? ATTR_SMS_MESSAGE_TYPE_CANNED : ATTR_SMS_MESSAGE_TYPE_CUSTOM;
        localyticsSendEvent(str, strArr);
    }

    public static void recordVehicleTelemetryData(long sessionDistance, long sessionDuration, long sessionRollingDuration, float sessionAverageSpeed, float sessionAverageRollingSpeed, float sessionMaxSpeed, int sessionHardBrakingCount, int sessionHardAccelerationCount, float sessionSpeedingFrequency, float sessionExcessiveSpeedingFrequency, int sessionTroubleCodeCount, java.lang.String sessionTroubleCodes, int averageMpg, int maxRpm, int eventAverageMpg, int eventAverageRpm, int eventFuelLevel, float eventEngineTemperature, float maxG, float maxGAngle, int sessionHighGCount) {
        com.navdy.hud.app.profile.DriverProfile currentProfile = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getDriverProfileManager().getCurrentProfile();
        java.lang.String make = "";
        java.lang.String model = "";
        java.lang.String year = "";
        if (currentProfile != null) {
            make = currentProfile.getCarMake();
            model = currentProfile.getCarModel();
            year = currentProfile.getCarYear();
        }
        localyticsSendEvent(ANALYTICS_EVENT_VEHICLE_TELEMETRY_DATA, ATTR_SESSION_DISTANCE, java.lang.Long.toString(sessionDistance), ATTR_SESSION_DURATION, java.lang.Long.toString(sessionDuration), ATTR_SESSION_ROLLING_DURATION, java.lang.Long.toString(sessionRollingDuration), ATTR_SESSION_AVERAGE_SPEED, java.lang.Float.toString(sessionAverageSpeed), ATTR_SESSION_AVERAGE_ROLLING_SPEED, java.lang.Float.toString(sessionAverageRollingSpeed), ATTR_SESSION_MAX_SPEED, java.lang.Float.toString(sessionMaxSpeed), ATTR_SESSION_HARD_BRAKING_COUNT, java.lang.Integer.toString(sessionHardBrakingCount), ATTR_SESSION_ACCELERATION_COUNT, java.lang.Integer.toString(sessionHardAccelerationCount), ATTR_SESSION_SPEEDING_FREQUENCY, java.lang.Float.toString(sessionSpeedingFrequency), ATTR_SESSION_EXCESSIVE_SPEEDING_FREQUENCY, java.lang.Float.toString(sessionExcessiveSpeedingFrequency), ATTR_SESSION_TROUBLE_CODE_COUNT, java.lang.Integer.toString(sessionTroubleCodeCount), ATTR_SESSION_TROUBLE_CODE_LAST, sessionTroubleCodes, ATTR_SESSION_AVERAGE_MPG, java.lang.Integer.toString(averageMpg), ATTR_SESSION_MAX_RPM, java.lang.Integer.toString(maxRpm), ATTR_EVENT_AVERAGE_MPG, java.lang.Integer.toString(eventAverageMpg), ATTR_EVENT_AVERAGE_RPM, java.lang.Integer.toString(eventAverageRpm), ATTR_EVENT_FUEL_LEVEL, java.lang.Integer.toString(eventFuelLevel), ATTR_EVENT_ENGINE_TEMPERATURE, java.lang.Float.toString(eventEngineTemperature), ATTR_EVENT_MAX_G, java.lang.Float.toString(maxG), ATTR_EVENT_MAX_G_ANGLE, java.lang.Float.toString(maxGAngle), ATTR_SESSION_HIGH_G_COUNT, java.lang.Integer.toString(sessionHighGCount), "Car_Make", make, "Car_Model", model, "Car_Year", year);
    }
}
