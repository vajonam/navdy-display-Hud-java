package com.navdy.hud.app.analytics;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u0012\n\u0000\n\u0002\u0010\u0007\n\u0002\b\u0004\n\u0002\u0010\t\n\u0002\b\u0003\u001a\n\u0010\u0000\u001a\u00020\u0001*\u00020\u0001\u001a\n\u0010\u0002\u001a\u00020\u0001*\u00020\u0001\u001a\n\u0010\u0003\u001a\u00020\u0001*\u00020\u0001\u001a\n\u0010\u0004\u001a\u00020\u0001*\u00020\u0001\u001a\n\u0010\u0005\u001a\u00020\u0006*\u00020\u0006\u001a\u0012\u0010\u0007\u001a\u00020\u0006*\u00020\u00062\u0006\u0010\b\u001a\u00020\u0006\u00a8\u0006\t"}, d2 = {"KMPHToMetersPerSecond", "", "MPHToMetersPerSecond", "MetersPerSecondToKMPH", "MetersPerSecondToMPH", "milliSecondsToSeconds", "", "timeSince", "time", "app_hudDebug"}, k = 2, mv = {1, 1, 6})
/* compiled from: TelemetrySession.kt */
public final class TelemetrySessionKt {
    public static final float MPHToMetersPerSecond(float $receiver) {
        return 0.44704f * $receiver;
    }

    public static final float KMPHToMetersPerSecond(float $receiver) {
        return 0.277778f * $receiver;
    }

    public static final float MetersPerSecondToMPH(float $receiver) {
        return 2.23694f * $receiver;
    }

    public static final float MetersPerSecondToKMPH(float $receiver) {
        return 3.6f * $receiver;
    }

    public static final long milliSecondsToSeconds(long $receiver) {
        return $receiver / ((long) 1000);
    }

    public static final long timeSince(long $receiver, long time) {
        return $receiver - time;
    }
}
