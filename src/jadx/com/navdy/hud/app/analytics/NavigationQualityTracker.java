package com.navdy.hud.app.analytics;

public class NavigationQualityTracker {
    private static final int INVALID = -1;
    private static final com.navdy.hud.app.analytics.NavigationQualityTracker instance = new com.navdy.hud.app.analytics.NavigationQualityTracker();
    private static final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.analytics.NavigationQualityTracker.class);
    private boolean isRoutingWithTraffic;
    private com.navdy.hud.app.analytics.NavigationQualityTracker.Report report;
    private long tripDurationIntervalInit = -1;
    private long tripStartUtc = -1;

    static class Report {
        private static final double HEAVY_TRAFFIC_RATIO = 1.25d;
        private static final double MODERATE_TRAFFIC_RATIO = 1.1d;
        long actualDistance;
        long actualDuration;
        final long expectedDistance;
        final long expectedDuration;
        int nRecalculations;
        final com.navdy.hud.app.analytics.NavigationQualityTracker.TrafficLevel trafficLevel;

        private Report(long durationWithTraffic, long duration, long expectedDistance2, boolean isTrafficEnabled) {
            this.expectedDuration = durationWithTraffic;
            this.expectedDistance = expectedDistance2;
            this.nRecalculations = 0;
            if (isTrafficEnabled) {
                double trafficRatio = ((double) durationWithTraffic) / ((double) duration);
                if (trafficRatio < MODERATE_TRAFFIC_RATIO) {
                    this.trafficLevel = com.navdy.hud.app.analytics.NavigationQualityTracker.TrafficLevel.LIGHT;
                } else if (trafficRatio < MODERATE_TRAFFIC_RATIO || trafficRatio >= HEAVY_TRAFFIC_RATIO) {
                    this.trafficLevel = com.navdy.hud.app.analytics.NavigationQualityTracker.TrafficLevel.HEAVY;
                } else {
                    this.trafficLevel = com.navdy.hud.app.analytics.NavigationQualityTracker.TrafficLevel.MODERATE;
                }
            } else {
                this.trafficLevel = com.navdy.hud.app.analytics.NavigationQualityTracker.TrafficLevel.NO_DATA;
            }
        }

        /* access modifiers changed from: 0000 */
        public double getDurationVariance() {
            return ((double) (100 * (this.actualDuration - this.expectedDuration))) / ((double) this.expectedDuration);
        }

        /* access modifiers changed from: 0000 */
        public double getDistanceVariance() {
            return ((double) (100 * (this.actualDistance - this.expectedDistance))) / ((double) this.expectedDuration);
        }

        public java.lang.String toString() {
            return "Report{trafficLevel=" + this.trafficLevel + ", expectedDuration=" + this.expectedDuration + ", expectedDistance=" + this.expectedDistance + ", actualDuration=" + this.actualDuration + ", actualDistance=" + this.actualDistance + ", nRecalculations=" + this.nRecalculations + '}';
        }

        public boolean isValid() {
            return this.expectedDistance > 0 && this.expectedDuration > 0 && this.actualDistance > 0 && this.actualDuration > 0;
        }
    }

    enum TrafficLevel {
        NO_DATA,
        LIGHT,
        MODERATE,
        HEAVY
    }

    public static com.navdy.hud.app.analytics.NavigationQualityTracker getInstance() {
        return instance;
    }

    private NavigationQualityTracker() {
    }

    public synchronized void trackTripStarted(long durationWithTraffic, long duration, long distance) {
        logger.i("trip started, init new navigation quality report");
        this.tripStartUtc = java.lang.System.currentTimeMillis();
        this.tripDurationIntervalInit = android.os.SystemClock.elapsedRealtime();
        logger.v("tripStartUtc: " + this.tripStartUtc);
        this.report = new com.navdy.hud.app.analytics.NavigationQualityTracker.Report(durationWithTraffic, duration, distance, this.isRoutingWithTraffic);
    }

    public synchronized void trackTripRecalculated() {
        if (this.report == null) {
            logger.w("report is null, no-op");
        } else {
            this.report.nRecalculations++;
        }
    }

    public synchronized void cancelTrip() {
        this.tripStartUtc = -1;
        this.tripDurationIntervalInit = -1;
        this.isRoutingWithTraffic = false;
        this.report = null;
    }

    public synchronized void trackTripEnded(long distanceCovered) {
        if (this.report == null) {
            logger.w("report is null, no-op");
        } else {
            long tripEndUtc = java.lang.System.currentTimeMillis();
            logger.v("tripEndUtc: " + tripEndUtc);
            logger.v("tripEndUtc - tripStartUtc, should be same as actualDuration: " + (tripEndUtc - this.tripStartUtc));
            this.report.actualDuration = (android.os.SystemClock.elapsedRealtime() - this.tripDurationIntervalInit) / 1000;
            this.report.actualDistance = distanceCovered;
            logger.i("route has ended, submitting navigation quality report: " + this.report);
            if (this.report.isValid()) {
                com.navdy.hud.app.analytics.AnalyticsSupport.submitNavigationQualityReport(this.report);
            }
            this.report = null;
        }
    }

    public synchronized void trackCalculationWithTraffic(boolean factoringInTraffic) {
        this.isRoutingWithTraffic = factoringInTraffic;
    }

    public long getTripStartUtc() {
        return this.tripStartUtc;
    }

    public int getActualDurationSoFar() {
        if (this.tripDurationIntervalInit != -1) {
            return (int) ((android.os.SystemClock.elapsedRealtime() - this.tripDurationIntervalInit) / 1000);
        }
        return -1;
    }

    public int getExpectedDuration() {
        if (this.report == null) {
            return -1;
        }
        return (int) this.report.expectedDuration;
    }

    public int getExpectedDistance() {
        if (this.report == null) {
            return -1;
        }
        return (int) this.report.expectedDistance;
    }
}
