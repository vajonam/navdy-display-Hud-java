package com.navdy.hud.app.analytics;

class Event {
    public static final java.lang.String TAG = "Localytics";
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(TAG);
    public java.util.Map<java.lang.String, java.lang.String> argMap;
    public java.lang.String tag;

    public Event(java.lang.String tag2, java.util.Map<java.lang.String, java.lang.String> argMap2) {
        this.tag = tag2;
        this.argMap = argMap2;
    }

    public Event(java.lang.String tag2, java.lang.String... fieldAndValues) {
        this.tag = tag2;
        this.argMap = new java.util.HashMap();
        add(fieldAndValues);
    }

    public void add(java.lang.String... fieldAndValues) {
        int i = 0;
        while (i < fieldAndValues.length - 1) {
            java.lang.String a = fieldAndValues[i + 1];
            java.util.Map<java.lang.String, java.lang.String> map = this.argMap;
            java.lang.String str = fieldAndValues[i];
            if (a == null) {
                a = "";
            }
            map.put(str, a);
            i += 2;
        }
        if (i < fieldAndValues.length) {
            sLogger.e("Odd number of event arguments for tag " + this.tag);
        }
    }

    /* access modifiers changed from: 0000 */
    public java.lang.String getChangedFields(com.navdy.hud.app.analytics.Event oldEvent) {
        java.lang.StringBuilder builder = new java.lang.StringBuilder();
        for (java.util.Map.Entry<java.lang.String, java.lang.String> entry : this.argMap.entrySet()) {
            java.lang.String fieldName = (java.lang.String) entry.getKey();
            java.lang.String oldValue = (java.lang.String) oldEvent.argMap.get(fieldName);
            java.lang.String newValue = (java.lang.String) entry.getValue();
            if ((newValue != null && !newValue.equals(oldValue)) || (newValue == null && oldValue != null)) {
                if (builder.length() == 0) {
                    builder.append("|");
                }
                builder.append(fieldName);
                builder.append("|");
            }
        }
        return builder.toString();
    }

    public java.lang.String toString() {
        return "Event{tag='" + this.tag + '\'' + ", argMap=" + this.argMap + '}';
    }
}
