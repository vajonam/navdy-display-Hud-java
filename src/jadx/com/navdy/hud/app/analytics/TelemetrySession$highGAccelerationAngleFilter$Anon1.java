package com.navdy.hud.app.analytics;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u000e\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0007\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\b\u0004"}, d2 = {"<anonymous>", "", "angle", "", "invoke"}, k = 3, mv = {1, 1, 6})
/* compiled from: TelemetrySession.kt */
final class TelemetrySession$highGAccelerationAngleFilter$Anon1 extends kotlin.jvm.internal.Lambda implements kotlin.jvm.functions.Function1<java.lang.Float, java.lang.Boolean> {
    public static final com.navdy.hud.app.analytics.TelemetrySession$highGAccelerationAngleFilter$Anon1 INSTANCE = new com.navdy.hud.app.analytics.TelemetrySession$highGAccelerationAngleFilter$Anon1();

    TelemetrySession$highGAccelerationAngleFilter$Anon1() {
        super(1);
    }

    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj) {
        return java.lang.Boolean.valueOf(invoke(((java.lang.Number) obj).floatValue()));
    }

    public final boolean invoke(float angle) {
        return 56.0f <= angle && angle <= 125.0f;
    }
}
