package com.navdy.hud.app.analytics;

public final class AnalyticsSupport$TemperatureReporter$$InjectAdapter extends dagger.internal.Binding<com.navdy.hud.app.analytics.AnalyticsSupport.TemperatureReporter> implements javax.inject.Provider<com.navdy.hud.app.analytics.AnalyticsSupport.TemperatureReporter>, dagger.MembersInjector<com.navdy.hud.app.analytics.AnalyticsSupport.TemperatureReporter> {
    private dagger.internal.Binding<com.navdy.hud.app.device.PowerManager> powerManager;

    public AnalyticsSupport$TemperatureReporter$$InjectAdapter() {
        super("com.navdy.hud.app.analytics.AnalyticsSupport$TemperatureReporter", "members/com.navdy.hud.app.analytics.AnalyticsSupport$TemperatureReporter", false, com.navdy.hud.app.analytics.AnalyticsSupport.TemperatureReporter.class);
    }

    public void attach(dagger.internal.Linker linker) {
        this.powerManager = linker.requestBinding("com.navdy.hud.app.device.PowerManager", com.navdy.hud.app.analytics.AnalyticsSupport.TemperatureReporter.class, getClass().getClassLoader());
    }

    public void getDependencies(java.util.Set<dagger.internal.Binding<?>> set, java.util.Set<dagger.internal.Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.powerManager);
    }

    public com.navdy.hud.app.analytics.AnalyticsSupport.TemperatureReporter get() {
        com.navdy.hud.app.analytics.AnalyticsSupport.TemperatureReporter result = new com.navdy.hud.app.analytics.AnalyticsSupport.TemperatureReporter();
        injectMembers(result);
        return result;
    }

    public void injectMembers(com.navdy.hud.app.analytics.AnalyticsSupport.TemperatureReporter object) {
        object.powerManager = (com.navdy.hud.app.device.PowerManager) this.powerManager.get();
    }
}
