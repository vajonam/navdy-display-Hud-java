package com.navdy.hud.app.analytics;

@kotlin.Metadata(bv = {1, 0, 1}, k = 3, mv = {1, 1, 6})
/* compiled from: TelemetryDataManager.kt */
final class TelemetryDataManagerKt$sam$Runnable$cc6381d8 implements java.lang.Runnable {
    private final /* synthetic */ kotlin.jvm.functions.Function0 function;

    TelemetryDataManagerKt$sam$Runnable$cc6381d8(kotlin.jvm.functions.Function0 function0) {
        this.function = function0;
    }

    public final /* synthetic */ void run() {
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(this.function.invoke(), "invoke(...)");
    }
}
