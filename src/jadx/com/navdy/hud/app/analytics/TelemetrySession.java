package com.navdy.hud.app.analytics;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000|\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0007\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\b\f\n\u0002\u0010\u0006\n\u0002\b\u000b\n\u0002\u0010\b\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u001a\n\u0002\u0018\u0002\n\u0002\b\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b%\n\u0002\u0018\u0002\n\u0002\b\u0017\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b1\n\u0002\u0010\u000e\n\u0002\b\u001a\b\u00c6\u0002\u0018\u00002\u00020\u0001:\u0004\u00f4\u0001\u00f5\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0007\u0010\u00e8\u0001\u001a\u00020kJ\u0010\u0010\u00e9\u0001\u001a\u00020b2\u0007\u0010\u00ea\u0001\u001a\u00020\bJ\u0010\u0010\u00eb\u0001\u001a\u00020b2\u0007\u0010\u00ea\u0001\u001a\u00020\bJ\"\u0010\u00ec\u0001\u001a\u00020k2\u0007\u0010\u00ed\u0001\u001a\u00020\u00042\u0007\u0010\u00ee\u0001\u001a\u00020\u00042\u0007\u0010\u00ef\u0001\u001a\u00020\u0004J\u0007\u0010\u00f0\u0001\u001a\u00020kJ\u0007\u0010\u00f1\u0001\u001a\u00020kJ\u0007\u0010\u00f2\u0001\u001a\u00020kJ\t\u0010\u00f3\u0001\u001a\u00020kH\u0002R\u0014\u0010\u0003\u001a\u00020\u0004X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u0014\u0010\u0007\u001a\u00020\bX\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0014\u0010\u000b\u001a\u00020\bX\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\f\u0010\nR\u0014\u0010\r\u001a\u00020\u0004X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u0006R\u0014\u0010\u000f\u001a\u00020\u0004X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0006R\u000e\u0010\u0011\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0012\u001a\u00020\u0004X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0006R\u0014\u0010\u0014\u001a\u00020\u0015X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0017R\u0014\u0010\u0018\u001a\u00020\u0015X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u0017R\u0014\u0010\u001a\u001a\u00020\bX\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001b\u0010\nR\u0014\u0010\u001c\u001a\u00020\bX\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\nR\u0014\u0010\u001e\u001a\u00020\u0004X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001f\u0010\u0006R\u0014\u0010 \u001a\u00020!X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\"\u0010#R\u000e\u0010$\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010%\u001a\u00020!X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b&\u0010#R \u0010(\u001a\u00020\b2\u0006\u0010'\u001a\u00020\b8B@BX\u0082\u000e\u00a2\u0006\b\n\u0000\u001a\u0004\b)\u0010\nR \u0010+\u001a\u00020*2\u0006\u0010'\u001a\u00020*8B@BX\u0082\u000e\u00a2\u0006\b\n\u0000\u001a\u0004\b,\u0010-R\u000e\u0010.\u001a\u00020\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010/\u001a\u00020\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u00100\u001a\u00020\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R$\u00102\u001a\u00020\u00152\u0006\u00101\u001a\u00020\u0015@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b3\u0010\u0017\"\u0004\b4\u00105R$\u00106\u001a\u00020!2\u0006\u00101\u001a\u00020!@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b7\u0010#\"\u0004\b8\u00109R$\u0010:\u001a\u00020*2\u0006\u00101\u001a\u00020*@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b;\u0010-\"\u0004\b<\u0010=R$\u0010>\u001a\u00020\u00042\u0006\u00101\u001a\u00020\u0004@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b?\u0010\u0006\"\u0004\b@\u0010AR\u0011\u0010B\u001a\u00020\b8F\u00a2\u0006\u0006\u001a\u0004\bC\u0010\nR\u001a\u0010D\u001a\u00020EX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bF\u0010G\"\u0004\bH\u0010IR\u001a\u0010J\u001a\u00020\u0015X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bK\u0010\u0017\"\u0004\bL\u00105R\u001a\u0010M\u001a\u00020!X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bN\u0010#\"\u0004\bO\u00109R\u001a\u0010P\u001a\u00020\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bQ\u0010\n\"\u0004\bR\u0010SR\u001a\u0010T\u001a\u00020!X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bU\u0010#\"\u0004\bV\u00109R\u001c\u0010W\u001a\u00020\b8FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bX\u0010\n\"\u0004\bY\u0010SR\u001a\u0010Z\u001a\u00020\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b[\u0010\n\"\u0004\b\\\u0010SR,\u0010]\u001a\u001d\u0012\u0013\u0012\u00110\u0004\u00a2\u0006\f\b_\u0012\b\b`\u0012\u0004\b\b(a\u0012\u0004\u0012\u00020b0^\u00a2\u0006\b\n\u0000\u001a\u0004\bc\u0010dR\u0011\u0010e\u001a\u00020f\u00a2\u0006\b\n\u0000\u001a\u0004\bg\u0010hR,\u0010i\u001a\u001d\u0012\u0013\u0012\u00110b\u00a2\u0006\f\b_\u0012\b\b`\u0012\u0004\b\b(j\u0012\u0004\u0012\u00020k0^\u00a2\u0006\b\n\u0000\u001a\u0004\bl\u0010dR,\u0010m\u001a\u001d\u0012\u0013\u0012\u00110\u0004\u00a2\u0006\f\b_\u0012\b\b`\u0012\u0004\b\b(a\u0012\u0004\u0012\u00020b0^\u00a2\u0006\b\n\u0000\u001a\u0004\bn\u0010dR\u0011\u0010o\u001a\u00020f\u00a2\u0006\b\n\u0000\u001a\u0004\bp\u0010hR,\u0010q\u001a\u001d\u0012\u0013\u0012\u00110b\u00a2\u0006\f\b_\u0012\b\b`\u0012\u0004\b\b(j\u0012\u0004\u0012\u00020k0^\u00a2\u0006\b\n\u0000\u001a\u0004\br\u0010dR,\u0010s\u001a\u001d\u0012\u0013\u0012\u00110\u0004\u00a2\u0006\f\b_\u0012\b\b`\u0012\u0004\b\b(a\u0012\u0004\u0012\u00020b0^\u00a2\u0006\b\n\u0000\u001a\u0004\bt\u0010dR\u0011\u0010u\u001a\u00020f\u00a2\u0006\b\n\u0000\u001a\u0004\bv\u0010hR,\u0010w\u001a\u001d\u0012\u0013\u0012\u00110b\u00a2\u0006\f\b_\u0012\b\b`\u0012\u0004\b\b(j\u0012\u0004\u0012\u00020k0^\u00a2\u0006\b\n\u0000\u001a\u0004\bx\u0010dR$\u0010y\u001a\u00020b2\u0006\u00101\u001a\u00020b8F@FX\u0086\u000e\u00a2\u0006\f\u001a\u0004\by\u0010z\"\u0004\b{\u0010|R$\u0010}\u001a\u00020b2\u0006\u00101\u001a\u00020b8F@FX\u0086\u000e\u00a2\u0006\f\u001a\u0004\b}\u0010z\"\u0004\b~\u0010|R%\u0010\u007f\u001a\u00020b2\u0006\u00101\u001a\u00020b8F@FX\u0086\u000e\u00a2\u0006\r\u001a\u0004\b\u007f\u0010z\"\u0005\b\u0080\u0001\u0010|R\u001d\u0010\u0081\u0001\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u0010\n\u0000\u001a\u0005\b\u0082\u0001\u0010\u0006\"\u0005\b\u0083\u0001\u0010AR\u001d\u0010\u0084\u0001\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u0010\n\u0000\u001a\u0005\b\u0085\u0001\u0010\u0006\"\u0005\b\u0086\u0001\u0010AR\u001d\u0010\u0087\u0001\u001a\u00020\bX\u0086\u000e\u00a2\u0006\u0010\n\u0000\u001a\u0005\b\u0088\u0001\u0010\n\"\u0005\b\u0089\u0001\u0010SR\u001d\u0010\u008a\u0001\u001a\u00020\bX\u0086\u000e\u00a2\u0006\u0010\n\u0000\u001a\u0005\b\u008b\u0001\u0010\n\"\u0005\b\u008c\u0001\u0010SR\u001d\u0010\u008d\u0001\u001a\u00020\bX\u0086\u000e\u00a2\u0006\u0010\n\u0000\u001a\u0005\b\u008e\u0001\u0010\n\"\u0005\b\u008f\u0001\u0010SR\u0010\u0010\u0090\u0001\u001a\u00030\u0091\u0001X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001d\u0010\u0092\u0001\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u0010\n\u0000\u001a\u0005\b\u0093\u0001\u0010\u0006\"\u0005\b\u0094\u0001\u0010AR\u001d\u0010\u0095\u0001\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u0010\n\u0000\u001a\u0005\b\u0096\u0001\u0010\u0006\"\u0005\b\u0097\u0001\u0010AR\u001d\u0010\u0098\u0001\u001a\u00020!X\u0086\u000e\u00a2\u0006\u0010\n\u0000\u001a\u0005\b\u0099\u0001\u0010#\"\u0005\b\u009a\u0001\u00109R\u000f\u0010\u009b\u0001\u001a\u00020\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001d\u0010\u009c\u0001\u001a\u00020\u0015X\u0086\u000e\u00a2\u0006\u0010\n\u0000\u001a\u0005\b\u009d\u0001\u0010\u0017\"\u0005\b\u009e\u0001\u00105R\u001d\u0010\u009f\u0001\u001a\u00020\bX\u0086\u000e\u00a2\u0006\u0010\n\u0000\u001a\u0005\b\u00a0\u0001\u0010\n\"\u0005\b\u00a1\u0001\u0010SR\u001d\u0010\u00a2\u0001\u001a\u00020\u0015X\u0086\u000e\u00a2\u0006\u0010\n\u0000\u001a\u0005\b\u00a3\u0001\u0010\u0017\"\u0005\b\u00a4\u0001\u00105R\u001d\u0010\u00a5\u0001\u001a\u00020\bX\u0086\u000e\u00a2\u0006\u0010\n\u0000\u001a\u0005\b\u00a6\u0001\u0010\n\"\u0005\b\u00a7\u0001\u0010SR(\u0010\u00a8\u0001\u001a\u0016\u0012\u0011\u0012\u000f\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\u00040\u00aa\u00010\u00a9\u0001\u00a2\u0006\n\n\u0000\u001a\u0006\b\u00ab\u0001\u0010\u00ac\u0001R\u001d\u0010\u00ad\u0001\u001a\u00020\u0015X\u0086\u000e\u00a2\u0006\u0010\n\u0000\u001a\u0005\b\u00ae\u0001\u0010\u0017\"\u0005\b\u00af\u0001\u00105R)\u0010\u00b0\u0001\u001a\u00020\u00042\u0006\u0010'\u001a\u00020\u00048F@BX\u0086\u000e\u00a2\u0006\u0010\n\u0000\u001a\u0005\b\u00b1\u0001\u0010\u0006\"\u0005\b\u00b2\u0001\u0010AR)\u0010\u00b3\u0001\u001a\u00020\u00042\u0006\u0010'\u001a\u00020\u00048F@BX\u0086\u000e\u00a2\u0006\u0010\n\u0000\u001a\u0005\b\u00b4\u0001\u0010\u0006\"\u0005\b\u00b5\u0001\u0010AR\u001f\u0010\u00b6\u0001\u001a\u00020\b8FX\u0086\u000e\u00a2\u0006\u0010\n\u0000\u001a\u0005\b\u00b7\u0001\u0010\n\"\u0005\b\u00b8\u0001\u0010SR)\u0010\u00b9\u0001\u001a\u00020\b2\u0006\u0010'\u001a\u00020\b8F@BX\u0086\u000e\u00a2\u0006\u0010\n\u0000\u001a\u0005\b\u00ba\u0001\u0010\n\"\u0005\b\u00bb\u0001\u0010SR'\u0010\u00bc\u0001\u001a\u00020\u00042\u0006\u00101\u001a\u00020\u00048F@BX\u0086\u000e\u00a2\u0006\u000e\u001a\u0005\b\u00bd\u0001\u0010\u0006\"\u0005\b\u00be\u0001\u0010AR\u001d\u0010\u00bf\u0001\u001a\u00020!X\u0086\u000e\u00a2\u0006\u0010\n\u0000\u001a\u0005\b\u00c0\u0001\u0010#\"\u0005\b\u00c1\u0001\u00109R\u001d\u0010\u00c2\u0001\u001a\u00020!X\u0086\u000e\u00a2\u0006\u0010\n\u0000\u001a\u0005\b\u00c3\u0001\u0010#\"\u0005\b\u00c4\u0001\u00109R\u001d\u0010\u00c5\u0001\u001a\u00020!X\u0086\u000e\u00a2\u0006\u0010\n\u0000\u001a\u0005\b\u00c6\u0001\u0010#\"\u0005\b\u00c7\u0001\u00109R\u001d\u0010\u00c8\u0001\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u0010\n\u0000\u001a\u0005\b\u00c9\u0001\u0010\u0006\"\u0005\b\u00ca\u0001\u0010AR\u001d\u0010\u00cb\u0001\u001a\u00020\bX\u0086\u000e\u00a2\u0006\u0010\n\u0000\u001a\u0005\b\u00cc\u0001\u0010\n\"\u0005\b\u00cd\u0001\u0010SR\u001f\u0010\u00ce\u0001\u001a\u00020\b8FX\u0086\u000e\u00a2\u0006\u0010\n\u0000\u001a\u0005\b\u00cf\u0001\u0010\n\"\u0005\b\u00d0\u0001\u0010SR'\u0010\u00d1\u0001\u001a\u00020\u00042\u0006\u00101\u001a\u00020\u00048F@BX\u0086\u000e\u00a2\u0006\u000e\u001a\u0005\b\u00d2\u0001\u0010\u0006\"\u0005\b\u00d3\u0001\u0010AR\u000f\u0010\u00d4\u0001\u001a\u00020\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001d\u0010\u00d5\u0001\u001a\u00020bX\u0086\u000e\u00a2\u0006\u0010\n\u0000\u001a\u0005\b\u00d6\u0001\u0010z\"\u0005\b\u00d7\u0001\u0010|R\u001d\u0010\u00d8\u0001\u001a\u00020!X\u0086\u000e\u00a2\u0006\u0010\n\u0000\u001a\u0005\b\u00d9\u0001\u0010#\"\u0005\b\u00da\u0001\u00109R \u0010\u00db\u0001\u001a\u00030\u00dc\u0001X\u0086\u000e\u00a2\u0006\u0012\n\u0000\u001a\u0006\b\u00dd\u0001\u0010\u00de\u0001\"\u0006\b\u00df\u0001\u0010\u00e0\u0001R\u001f\u0010\u00e1\u0001\u001a\u00020\b8FX\u0086\u000e\u00a2\u0006\u0010\n\u0000\u001a\u0005\b\u00e2\u0001\u0010\n\"\u0005\b\u00e3\u0001\u0010SR\u000f\u0010\u00e4\u0001\u001a\u00020\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000f\u0010\u00e5\u0001\u001a\u00020\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0013\u0010\u00e6\u0001\u001a\u00020\b8F\u00a2\u0006\u0007\u001a\u0005\b\u00e7\u0001\u0010\n\u00a8\u0006\u00f6\u0001"}, d2 = {"Lcom/navdy/hud/app/analytics/TelemetrySession;", "", "()V", "ACCELERATION_G_LOWER_THRESHOLD_THRESHOLD", "", "getACCELERATION_G_LOWER_THRESHOLD_THRESHOLD", "()F", "ACCELERATION_HIGH_G_MINIMUM_INTERVAL_BETWEEN_EVENTS_MILLIS", "", "getACCELERATION_HIGH_G_MINIMUM_INTERVAL_BETWEEN_EVENTS_MILLIS", "()J", "ACCELERATION_HIGH_G_THRESHOLD_TIME_MILLIS", "getACCELERATION_HIGH_G_THRESHOLD_TIME_MILLIS", "ACCELERATION_MAX_G_THRESHOLD", "getACCELERATION_MAX_G_THRESHOLD", "BRAKING_MAX_G_THRESHOLD", "getBRAKING_MAX_G_THRESHOLD", "EXCESSIVE_SPEEDING_SPEED_OVER_SPEED_LIMIT_MS", "G_LOWER_THRESHOLD_THRESHOLD", "getG_LOWER_THRESHOLD_THRESHOLD", "HARD_ACCELERATION_THRESHOLD_METERS_PER_SECOND", "", "getHARD_ACCELERATION_THRESHOLD_METERS_PER_SECOND", "()D", "HARD_BRAKING_THRESHOLD_METERS_PER_SECOND", "getHARD_BRAKING_THRESHOLD_METERS_PER_SECOND", "HIGH_G_MINIMUM_INTERVAL_BETWEEN_EVENTS_MILLIS", "getHIGH_G_MINIMUM_INTERVAL_BETWEEN_EVENTS_MILLIS", "HIGH_G_THRESHOLD_TIME_MILLIS", "getHIGH_G_THRESHOLD_TIME_MILLIS", "MAX_G_THRESHOLD", "getMAX_G_THRESHOLD", "MINIMUM_TIME_BETWEEN_HARD_ACCELERATION_DECELERATION", "", "getMINIMUM_TIME_BETWEEN_HARD_ACCELERATION_DECELERATION", "()I", "SPEEDING_SPEED_OVER_SPEED_LIMIT_MS", "SPEED_DATA_ROLLING_WINDOW_SIZE", "getSPEED_DATA_ROLLING_WINDOW_SIZE", "<set-?>", "_currentDistance", "get_currentDistance", "Lcom/navdy/hud/app/analytics/RawSpeed;", "_currentSpeed", "get_currentSpeed", "()Lcom/navdy/hud/app/analytics/RawSpeed;", "_excessiveSpeedingDuration", "_rollingDuration", "_speedingDuration", "value", "currentMpg", "getCurrentMpg", "setCurrentMpg", "(D)V", "currentRpm", "getCurrentRpm", "setCurrentRpm", "(I)V", "currentSpeed", "getCurrentSpeed", "setCurrentSpeed", "(Lcom/navdy/hud/app/analytics/RawSpeed;)V", "currentSpeedLimit", "getCurrentSpeedLimit", "setCurrentSpeedLimit", "(F)V", "currentTime", "getCurrentTime", "dataSource", "Lcom/navdy/hud/app/analytics/TelemetrySession$DataSource;", "getDataSource", "()Lcom/navdy/hud/app/analytics/TelemetrySession$DataSource;", "setDataSource", "(Lcom/navdy/hud/app/analytics/TelemetrySession$DataSource;)V", "eventAverageMpg", "getEventAverageMpg", "setEventAverageMpg", "eventAverageRpm", "getEventAverageRpm", "setEventAverageRpm", "eventMpgSamples", "getEventMpgSamples", "setEventMpgSamples", "(J)V", "eventRpmSamples", "getEventRpmSamples", "setEventRpmSamples", "excessiveSpeedingDuration", "getExcessiveSpeedingDuration", "setExcessiveSpeedingDuration", "excessiveSpeedingStartTime", "getExcessiveSpeedingStartTime", "setExcessiveSpeedingStartTime", "highGAccelerationAngleFilter", "Lkotlin/Function1;", "Lkotlin/ParameterName;", "name", "angle", "", "getHighGAccelerationAngleFilter", "()Lkotlin/jvm/functions/Function1;", "highGAccelerationDetector", "Lcom/navdy/hud/app/analytics/HighGDetector;", "getHighGAccelerationDetector", "()Lcom/navdy/hud/app/analytics/HighGDetector;", "highGAccelerationEventCallBack", "started", "", "getHighGAccelerationEventCallBack", "highGBrakingAngleFilter", "getHighGBrakingAngleFilter", "highGBrakingDetector", "getHighGBrakingDetector", "highGBrakingEventCallBack", "getHighGBrakingEventCallBack", "highGTurnAngleFilter", "getHighGTurnAngleFilter", "highGTurnDetector", "getHighGTurnDetector", "highGTurnEventCallBack", "getHighGTurnEventCallBack", "isDoingHighGManeuver", "()Z", "setDoingHighGManeuver", "(Z)V", "isExcessiveSpeeding", "setExcessiveSpeeding", "isSpeeding", "setSpeeding", "lastG", "getLastG", "setLastG", "lastGAngle", "getLastGAngle", "setLastGAngle", "lastGTime", "getLastGTime", "setLastGTime", "lastHardAccelerationTime", "getLastHardAccelerationTime", "setLastHardAccelerationTime", "lastHardBrakingTime", "getLastHardBrakingTime", "setLastHardBrakingTime", "logger", "Lcom/navdy/service/library/log/Logger;", "maxG", "getMaxG", "setMaxG", "maxGAngle", "getMaxGAngle", "setMaxGAngle", "maxRpm", "getMaxRpm", "setMaxRpm", "movingStartTime", "pendingHardAcceleration", "getPendingHardAcceleration", "setPendingHardAcceleration", "pendingHardAccelerationTime", "getPendingHardAccelerationTime", "setPendingHardAccelerationTime", "pendingHardBraking", "getPendingHardBraking", "setPendingHardBraking", "pendingHardBrakingTime", "getPendingHardBrakingTime", "setPendingHardBrakingTime", "rollingSpeedData", "Ljava/util/LinkedList;", "Lkotlin/Pair;", "getRollingSpeedData", "()Ljava/util/LinkedList;", "sessionAverageMpg", "getSessionAverageMpg", "setSessionAverageMpg", "sessionAverageRollingSpeed", "getSessionAverageRollingSpeed", "setSessionAverageRollingSpeed", "sessionAverageSpeed", "getSessionAverageSpeed", "setSessionAverageSpeed", "sessionDistance", "getSessionDistance", "setSessionDistance", "sessionDuration", "getSessionDuration", "setSessionDuration", "sessionExcessiveSpeedingPercentage", "getSessionExcessiveSpeedingPercentage", "setSessionExcessiveSpeedingPercentage", "sessionHardAccelerationCount", "getSessionHardAccelerationCount", "setSessionHardAccelerationCount", "sessionHardBrakingCount", "getSessionHardBrakingCount", "setSessionHardBrakingCount", "sessionHighGCount", "getSessionHighGCount", "setSessionHighGCount", "sessionMaxSpeed", "getSessionMaxSpeed", "setSessionMaxSpeed", "sessionMpgSamples", "getSessionMpgSamples", "setSessionMpgSamples", "sessionRollingDuration", "getSessionRollingDuration", "setSessionRollingDuration", "sessionSpeedingPercentage", "getSessionSpeedingPercentage", "setSessionSpeedingPercentage", "sessionStartTime", "sessionStarted", "getSessionStarted", "setSessionStarted", "sessionTroubleCodeCount", "getSessionTroubleCodeCount", "setSessionTroubleCodeCount", "sessionTroubleCodesReport", "", "getSessionTroubleCodesReport", "()Ljava/lang/String;", "setSessionTroubleCodesReport", "(Ljava/lang/String;)V", "speedingDuration", "getSpeedingDuration", "setSpeedingDuration", "speedingStartTime", "startDistance", "timeWindowForHighGMatching", "getTimeWindowForHighGMatching", "eventReported", "isHighGAccelerationDetected", "within", "isHighGBrakingDetected", "setGForce", "x", "y", "z", "speedSourceChanged", "startSession", "stopSession", "updateValuesBasedOnSpeed", "DataSource", "InterestingEvent", "app_hudDebug"}, k = 1, mv = {1, 1, 6})
/* compiled from: TelemetrySession.kt */
public final class TelemetrySession {
    private static final float ACCELERATION_G_LOWER_THRESHOLD_THRESHOLD = 0.3f;
    private static final long ACCELERATION_HIGH_G_MINIMUM_INTERVAL_BETWEEN_EVENTS_MILLIS = 2000;
    private static final long ACCELERATION_HIGH_G_THRESHOLD_TIME_MILLIS = 300;
    private static final float ACCELERATION_MAX_G_THRESHOLD = 0.36f;
    private static final float BRAKING_MAX_G_THRESHOLD = 0.399f;
    private static final float EXCESSIVE_SPEEDING_SPEED_OVER_SPEED_LIMIT_MS = 0.0f;
    private static final float G_LOWER_THRESHOLD_THRESHOLD = 0.3f;
    private static final double HARD_ACCELERATION_THRESHOLD_METERS_PER_SECOND = 3.53d;
    private static final double HARD_BRAKING_THRESHOLD_METERS_PER_SECOND = 3.919d;
    private static final long HIGH_G_MINIMUM_INTERVAL_BETWEEN_EVENTS_MILLIS = 3000;
    private static final long HIGH_G_THRESHOLD_TIME_MILLIS = 500;
    public static final com.navdy.hud.app.analytics.TelemetrySession INSTANCE = null;
    private static final float MAX_G_THRESHOLD = 0.45f;
    private static final int MINIMUM_TIME_BETWEEN_HARD_ACCELERATION_DECELERATION = 2000;
    private static final float SPEEDING_SPEED_OVER_SPEED_LIMIT_MS = 0.0f;
    private static final int SPEED_DATA_ROLLING_WINDOW_SIZE = 500;
    private static long _currentDistance;
    private static com.navdy.hud.app.analytics.RawSpeed _currentSpeed;
    private static long _excessiveSpeedingDuration;
    private static long _rollingDuration;
    private static long _speedingDuration;
    private static double currentMpg;
    private static int currentRpm;
    @org.jetbrains.annotations.NotNull
    private static com.navdy.hud.app.analytics.RawSpeed currentSpeed;
    private static float currentSpeedLimit;
    @org.jetbrains.annotations.NotNull
    private static com.navdy.hud.app.analytics.TelemetrySession.DataSource dataSource;
    private static double eventAverageMpg;
    private static int eventAverageRpm;
    private static long eventMpgSamples;
    private static int eventRpmSamples;
    private static long excessiveSpeedingDuration;
    private static long excessiveSpeedingStartTime;
    @org.jetbrains.annotations.NotNull
    private static final kotlin.jvm.functions.Function1<java.lang.Float, java.lang.Boolean> highGAccelerationAngleFilter = null;
    @org.jetbrains.annotations.NotNull
    private static final com.navdy.hud.app.analytics.HighGDetector highGAccelerationDetector = null;
    @org.jetbrains.annotations.NotNull
    private static final kotlin.jvm.functions.Function1<java.lang.Boolean, kotlin.Unit> highGAccelerationEventCallBack = null;
    @org.jetbrains.annotations.NotNull
    private static final kotlin.jvm.functions.Function1<java.lang.Float, java.lang.Boolean> highGBrakingAngleFilter = null;
    @org.jetbrains.annotations.NotNull
    private static final com.navdy.hud.app.analytics.HighGDetector highGBrakingDetector = null;
    @org.jetbrains.annotations.NotNull
    private static final kotlin.jvm.functions.Function1<java.lang.Boolean, kotlin.Unit> highGBrakingEventCallBack = null;
    @org.jetbrains.annotations.NotNull
    private static final kotlin.jvm.functions.Function1<java.lang.Float, java.lang.Boolean> highGTurnAngleFilter = null;
    @org.jetbrains.annotations.NotNull
    private static final com.navdy.hud.app.analytics.HighGDetector highGTurnDetector = null;
    @org.jetbrains.annotations.NotNull
    private static final kotlin.jvm.functions.Function1<java.lang.Boolean, kotlin.Unit> highGTurnEventCallBack = null;
    private static float lastG;
    private static float lastGAngle;
    private static long lastGTime;
    private static long lastHardAccelerationTime;
    private static long lastHardBrakingTime;
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger logger = null;
    private static float maxG;
    private static float maxGAngle;
    private static int maxRpm;
    private static long movingStartTime;
    private static double pendingHardAcceleration;
    private static long pendingHardAccelerationTime;
    private static double pendingHardBraking;
    private static long pendingHardBrakingTime;
    @org.jetbrains.annotations.NotNull
    private static final java.util.LinkedList<kotlin.Pair<java.lang.Long, java.lang.Float>> rollingSpeedData = null;
    private static double sessionAverageMpg;
    private static float sessionAverageRollingSpeed;
    private static float sessionAverageSpeed;
    private static long sessionDistance;
    private static long sessionDuration;
    private static int sessionHardAccelerationCount;
    private static int sessionHardBrakingCount;
    private static int sessionHighGCount;
    private static float sessionMaxSpeed;
    private static long sessionMpgSamples;
    private static long sessionRollingDuration;
    private static long sessionStartTime;
    private static boolean sessionStarted;
    private static int sessionTroubleCodeCount;
    @org.jetbrains.annotations.NotNull
    private static java.lang.String sessionTroubleCodesReport;
    private static long speedingDuration;
    private static long speedingStartTime;
    private static long startDistance;

    @kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0004\bf\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H&J\b\u0010\u0004\u001a\u00020\u0005H&J\u0010\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\tH&J\b\u0010\n\u001a\u00020\u000bH&J\b\u0010\f\u001a\u00020\u000bH&J\b\u0010\r\u001a\u00020\u000bH&J\b\u0010\u000e\u001a\u00020\u0003H&\u00a8\u0006\u000f"}, d2 = {"Lcom/navdy/hud/app/analytics/TelemetrySession$DataSource;", "", "absoluteCurrentTime", "", "currentSpeed", "Lcom/navdy/hud/app/analytics/RawSpeed;", "interestingEventDetected", "", "event", "Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;", "isExperimental", "", "isHighAccuracySpeedAvailable", "isVerboseLoggingNeeded", "totalDistanceTravelledWithMeters", "app_hudDebug"}, k = 1, mv = {1, 1, 6})
    /* compiled from: TelemetrySession.kt */
    public interface DataSource {
        long absoluteCurrentTime();

        @org.jetbrains.annotations.NotNull
        com.navdy.hud.app.analytics.RawSpeed currentSpeed();

        void interestingEventDetected(@org.jetbrains.annotations.NotNull com.navdy.hud.app.analytics.TelemetrySession.InterestingEvent interestingEvent);

        boolean isExperimental();

        boolean isHighAccuracySpeedAvailable();

        boolean isVerboseLoggingNeeded();

        long totalDistanceTravelledWithMeters();
    }

    @kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u000b\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006j\u0002\b\u0007j\u0002\b\bj\u0002\b\tj\u0002\b\nj\u0002\b\u000b\u00a8\u0006\f"}, d2 = {"Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;", "", "(Ljava/lang/String;I)V", "NONE", "HARD_BRAKING", "HARD_ACCELERATION", "HIGH_G_STARTED", "HIGH_G_ENDED", "SPEEDING_STARTED", "SPEEDING_STOPPED", "EXCESSIVE_SPEEDING_STARTED", "EXCESSIVE_SPEEDING_STOPPED", "app_hudDebug"}, k = 1, mv = {1, 1, 6})
    /* compiled from: TelemetrySession.kt */
    public enum InterestingEvent {
        NONE,
        HARD_BRAKING,
        HARD_ACCELERATION,
        HIGH_G_STARTED,
        HIGH_G_ENDED,
        SPEEDING_STARTED,
        SPEEDING_STOPPED,
        EXCESSIVE_SPEEDING_STARTED,
        EXCESSIVE_SPEEDING_STOPPED
    }

    static {
        new com.navdy.hud.app.analytics.TelemetrySession();
    }

    private TelemetrySession() {
        INSTANCE = this;
        logger = new com.navdy.service.library.log.Logger(INSTANCE.getClass().getSimpleName());
        SPEEDING_SPEED_OVER_SPEED_LIMIT_MS = com.navdy.hud.app.analytics.TelemetrySessionKt.MPHToMetersPerSecond(8.0f);
        EXCESSIVE_SPEEDING_SPEED_OVER_SPEED_LIMIT_MS = com.navdy.hud.app.analytics.TelemetrySessionKt.MPHToMetersPerSecond(16.0f);
        MAX_G_THRESHOLD = 0.45f;
        G_LOWER_THRESHOLD_THRESHOLD = 0.3f;
        HIGH_G_THRESHOLD_TIME_MILLIS = HIGH_G_THRESHOLD_TIME_MILLIS;
        HIGH_G_MINIMUM_INTERVAL_BETWEEN_EVENTS_MILLIS = 3000;
        ACCELERATION_MAX_G_THRESHOLD = ACCELERATION_MAX_G_THRESHOLD;
        BRAKING_MAX_G_THRESHOLD = BRAKING_MAX_G_THRESHOLD;
        ACCELERATION_G_LOWER_THRESHOLD_THRESHOLD = 0.3f;
        ACCELERATION_HIGH_G_THRESHOLD_TIME_MILLIS = ACCELERATION_HIGH_G_THRESHOLD_TIME_MILLIS;
        ACCELERATION_HIGH_G_MINIMUM_INTERVAL_BETWEEN_EVENTS_MILLIS = 2000;
        dataSource = new com.navdy.hud.app.analytics.TelemetrySession$dataSource$Anon1();
        sessionMpgSamples = 1;
        sessionTroubleCodesReport = "";
        _currentSpeed = new com.navdy.hud.app.analytics.RawSpeed((float) -1, 0);
        SPEED_DATA_ROLLING_WINDOW_SIZE = 500;
        HARD_ACCELERATION_THRESHOLD_METERS_PER_SECOND = HARD_ACCELERATION_THRESHOLD_METERS_PER_SECOND;
        HARD_BRAKING_THRESHOLD_METERS_PER_SECOND = HARD_BRAKING_THRESHOLD_METERS_PER_SECOND;
        MINIMUM_TIME_BETWEEN_HARD_ACCELERATION_DECELERATION = 2000;
        currentSpeed = new com.navdy.hud.app.analytics.RawSpeed((float) -1, 0);
        rollingSpeedData = new java.util.LinkedList<>();
        eventMpgSamples = 1;
        eventRpmSamples = 1;
        highGTurnDetector = new com.navdy.hud.app.analytics.HighGDetector(MAX_G_THRESHOLD, ACCELERATION_G_LOWER_THRESHOLD_THRESHOLD, ACCELERATION_HIGH_G_MINIMUM_INTERVAL_BETWEEN_EVENTS_MILLIS, ACCELERATION_HIGH_G_THRESHOLD_TIME_MILLIS);
        highGTurnAngleFilter = com.navdy.hud.app.analytics.TelemetrySession$highGTurnAngleFilter$Anon1.INSTANCE;
        highGTurnEventCallBack = com.navdy.hud.app.analytics.TelemetrySession$highGTurnEventCallBack$Anon1.INSTANCE;
        highGAccelerationDetector = new com.navdy.hud.app.analytics.HighGDetector(ACCELERATION_MAX_G_THRESHOLD, ACCELERATION_G_LOWER_THRESHOLD_THRESHOLD, ACCELERATION_HIGH_G_MINIMUM_INTERVAL_BETWEEN_EVENTS_MILLIS, ACCELERATION_HIGH_G_THRESHOLD_TIME_MILLIS);
        highGAccelerationAngleFilter = com.navdy.hud.app.analytics.TelemetrySession$highGAccelerationAngleFilter$Anon1.INSTANCE;
        highGAccelerationEventCallBack = com.navdy.hud.app.analytics.TelemetrySession$highGAccelerationEventCallBack$Anon1.INSTANCE;
        highGBrakingDetector = new com.navdy.hud.app.analytics.HighGDetector(BRAKING_MAX_G_THRESHOLD, ACCELERATION_G_LOWER_THRESHOLD_THRESHOLD, ACCELERATION_HIGH_G_MINIMUM_INTERVAL_BETWEEN_EVENTS_MILLIS, ACCELERATION_HIGH_G_THRESHOLD_TIME_MILLIS);
        highGBrakingAngleFilter = com.navdy.hud.app.analytics.TelemetrySession$highGBrakingAngleFilter$Anon1.INSTANCE;
        highGBrakingEventCallBack = com.navdy.hud.app.analytics.TelemetrySession$highGBrakingEventCallBack$Anon1.INSTANCE;
    }

    public final float getMAX_G_THRESHOLD() {
        return MAX_G_THRESHOLD;
    }

    public final float getG_LOWER_THRESHOLD_THRESHOLD() {
        return G_LOWER_THRESHOLD_THRESHOLD;
    }

    public final long getHIGH_G_THRESHOLD_TIME_MILLIS() {
        return HIGH_G_THRESHOLD_TIME_MILLIS;
    }

    public final long getHIGH_G_MINIMUM_INTERVAL_BETWEEN_EVENTS_MILLIS() {
        return HIGH_G_MINIMUM_INTERVAL_BETWEEN_EVENTS_MILLIS;
    }

    public final float getACCELERATION_MAX_G_THRESHOLD() {
        return ACCELERATION_MAX_G_THRESHOLD;
    }

    public final float getBRAKING_MAX_G_THRESHOLD() {
        return BRAKING_MAX_G_THRESHOLD;
    }

    public final float getACCELERATION_G_LOWER_THRESHOLD_THRESHOLD() {
        return ACCELERATION_G_LOWER_THRESHOLD_THRESHOLD;
    }

    public final long getACCELERATION_HIGH_G_THRESHOLD_TIME_MILLIS() {
        return ACCELERATION_HIGH_G_THRESHOLD_TIME_MILLIS;
    }

    public final long getACCELERATION_HIGH_G_MINIMUM_INTERVAL_BETWEEN_EVENTS_MILLIS() {
        return ACCELERATION_HIGH_G_MINIMUM_INTERVAL_BETWEEN_EVENTS_MILLIS;
    }

    @org.jetbrains.annotations.NotNull
    public final com.navdy.hud.app.analytics.TelemetrySession.DataSource getDataSource() {
        return dataSource;
    }

    public final void setDataSource(@org.jetbrains.annotations.NotNull com.navdy.hud.app.analytics.TelemetrySession.DataSource dataSource2) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(dataSource2, "<set-?>");
        dataSource = dataSource2;
    }

    public final double getSessionAverageMpg() {
        return sessionAverageMpg;
    }

    public final void setSessionAverageMpg(double d) {
        sessionAverageMpg = d;
    }

    public final long getSessionMpgSamples() {
        return sessionMpgSamples;
    }

    public final void setSessionMpgSamples(long j) {
        sessionMpgSamples = j;
    }

    public final double getCurrentMpg() {
        return currentMpg;
    }

    public final void setCurrentMpg(double value) {
        currentMpg = value;
        if (value > ((double) 0)) {
            sessionAverageMpg += (value - sessionAverageMpg) / ((double) sessionMpgSamples);
            sessionMpgSamples += (long) 1;
            eventAverageMpg += (value - eventAverageMpg) / ((double) eventMpgSamples);
            eventMpgSamples += (long) 1;
        }
    }

    public final long getCurrentTime() {
        return dataSource.absoluteCurrentTime();
    }

    public final int getMaxRpm() {
        return maxRpm;
    }

    public final void setMaxRpm(int i) {
        maxRpm = i;
    }

    public final int getCurrentRpm() {
        return currentRpm;
    }

    public final void setCurrentRpm(int value) {
        currentRpm = value;
        if (value > 0) {
            maxRpm = java.lang.Math.max(value, maxRpm);
            eventAverageRpm += (value - eventAverageRpm) / eventRpmSamples;
            eventRpmSamples++;
        }
    }

    private final void setSessionAverageSpeed(float f) {
        sessionAverageSpeed = f;
    }

    public final float getSessionAverageSpeed() {
        if (getSessionDuration() > ((long) 0)) {
            return ((float) getSessionDistance()) / (((float) getSessionDuration()) / ((float) 1000));
        }
        return 0.0f;
    }

    private final void setSessionAverageRollingSpeed(float f) {
        sessionAverageRollingSpeed = f;
    }

    public final float getSessionAverageRollingSpeed() {
        if (getSessionRollingDuration() > ((long) 0)) {
            return ((float) getSessionDistance()) / (((float) getSessionRollingDuration()) / ((float) 1000));
        }
        return 0.0f;
    }

    public final float getSessionSpeedingPercentage() {
        if (getSessionRollingDuration() == 0) {
            return 0.0f;
        }
        return ((float) getSpeedingDuration()) / ((float) getSessionRollingDuration());
    }

    private final void setSessionSpeedingPercentage(float value) {
    }

    public final float getSessionExcessiveSpeedingPercentage() {
        if (getSessionRollingDuration() == 0) {
            return 0.0f;
        }
        return ((float) getExcessiveSpeedingDuration()) / ((float) getSessionRollingDuration());
    }

    private final void setSessionExcessiveSpeedingPercentage(float value) {
    }

    public final int getSessionTroubleCodeCount() {
        return sessionTroubleCodeCount;
    }

    public final void setSessionTroubleCodeCount(int i) {
        sessionTroubleCodeCount = i;
    }

    @org.jetbrains.annotations.NotNull
    public final java.lang.String getSessionTroubleCodesReport() {
        return sessionTroubleCodesReport;
    }

    public final void setSessionTroubleCodesReport(@org.jetbrains.annotations.NotNull java.lang.String str) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(str, "<set-?>");
        sessionTroubleCodesReport = str;
    }

    private final long get_currentDistance() {
        return dataSource.totalDistanceTravelledWithMeters();
    }

    private final com.navdy.hud.app.analytics.RawSpeed get_currentSpeed() {
        return dataSource.currentSpeed();
    }

    public final int getSPEED_DATA_ROLLING_WINDOW_SIZE() {
        return SPEED_DATA_ROLLING_WINDOW_SIZE;
    }

    public final double getHARD_ACCELERATION_THRESHOLD_METERS_PER_SECOND() {
        return HARD_ACCELERATION_THRESHOLD_METERS_PER_SECOND;
    }

    public final double getHARD_BRAKING_THRESHOLD_METERS_PER_SECOND() {
        return HARD_BRAKING_THRESHOLD_METERS_PER_SECOND;
    }

    public final int getMINIMUM_TIME_BETWEEN_HARD_ACCELERATION_DECELERATION() {
        return MINIMUM_TIME_BETWEEN_HARD_ACCELERATION_DECELERATION;
    }

    public final long getLastHardAccelerationTime() {
        return lastHardAccelerationTime;
    }

    public final void setLastHardAccelerationTime(long j) {
        lastHardAccelerationTime = j;
    }

    public final long getLastHardBrakingTime() {
        return lastHardBrakingTime;
    }

    public final void setLastHardBrakingTime(long j) {
        lastHardBrakingTime = j;
    }

    public final int getSessionHardAccelerationCount() {
        return sessionHardAccelerationCount;
    }

    public final void setSessionHardAccelerationCount(int i) {
        sessionHardAccelerationCount = i;
    }

    public final int getSessionHardBrakingCount() {
        return sessionHardBrakingCount;
    }

    public final void setSessionHardBrakingCount(int i) {
        sessionHardBrakingCount = i;
    }

    public final double getPendingHardAcceleration() {
        return pendingHardAcceleration;
    }

    public final void setPendingHardAcceleration(double d) {
        pendingHardAcceleration = d;
    }

    public final long getPendingHardAccelerationTime() {
        return pendingHardAccelerationTime;
    }

    public final void setPendingHardAccelerationTime(long j) {
        pendingHardAccelerationTime = j;
    }

    public final double getPendingHardBraking() {
        return pendingHardBraking;
    }

    public final void setPendingHardBraking(double d) {
        pendingHardBraking = d;
    }

    public final long getPendingHardBrakingTime() {
        return pendingHardBrakingTime;
    }

    public final void setPendingHardBrakingTime(long j) {
        pendingHardBrakingTime = j;
    }

    @org.jetbrains.annotations.NotNull
    public final com.navdy.hud.app.analytics.RawSpeed getCurrentSpeed() {
        return currentSpeed;
    }

    public final void setCurrentSpeed(@org.jetbrains.annotations.NotNull com.navdy.hud.app.analytics.RawSpeed value) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(value, com.glympse.android.hal.NotificationListener.INTENT_EXTRA_VALUE);
        currentSpeed = value;
        updateValuesBasedOnSpeed();
        if (((int) value.getSpeed()) == -1) {
            rollingSpeedData.clear();
            return;
        }
        boolean ignoreAccelerometer = !dataSource.isExperimental();
        if (dataSource.isHighAccuracySpeedAvailable() || !ignoreAccelerometer) {
            long timeStamp = value.getTimeStamp();
            rollingSpeedData.add(new kotlin.Pair(java.lang.Long.valueOf(timeStamp), java.lang.Float.valueOf(value.getSpeed())));
            if (rollingSpeedData.size() != 1) {
                boolean outDatedDataExists = true;
                kotlin.Pair lastSamplePastWindow = null;
                do {
                    kotlin.Pair oldestSample = (kotlin.Pair) rollingSpeedData.peek();
                    if (oldestSample == null || timeStamp - ((java.lang.Number) oldestSample.getFirst()).longValue() <= ((long) SPEED_DATA_ROLLING_WINDOW_SIZE)) {
                        outDatedDataExists = false;
                        if (lastSamplePastWindow != null) {
                            rollingSpeedData.addFirst(lastSamplePastWindow);
                            continue;
                        } else {
                            continue;
                        }
                    } else {
                        lastSamplePastWindow = (kotlin.Pair) rollingSpeedData.remove();
                        continue;
                    }
                } while (outDatedDataExists);
                kotlin.Pair firstSampleInWindow = (kotlin.Pair) rollingSpeedData.peek();
                double timeDifference = ((double) (timeStamp - ((java.lang.Number) firstSampleInWindow.getFirst()).longValue())) / ((double) 1000);
                if (timeDifference < ((double) 0.5f)) {
                    return;
                }
                if (value.getSpeed() > ((java.lang.Number) firstSampleInWindow.getSecond()).floatValue()) {
                    double acceleration = ((double) (value.getSpeed() - ((java.lang.Number) firstSampleInWindow.getSecond()).floatValue())) / timeDifference;
                    if (dataSource.isVerboseLoggingNeeded()) {
                        logger.d("RawSpeed " + value + ", Change is speed : " + (value.getSpeed() - ((java.lang.Number) firstSampleInWindow.getSecond()).floatValue()) + " Acceleration : " + acceleration + " " + com.navdy.hud.app.framework.glance.GlanceConstants.NEWLINE + " Rolling data : " + rollingSpeedData);
                    }
                    if (acceleration <= HARD_ACCELERATION_THRESHOLD_METERS_PER_SECOND) {
                        return;
                    }
                    if (lastHardAccelerationTime == 0 || timeStamp - lastHardAccelerationTime > ((long) MINIMUM_TIME_BETWEEN_HARD_ACCELERATION_DECELERATION)) {
                        com.navdy.service.library.log.Logger logger2 = logger;
                        com.navdy.service.library.log.Logger logger3 = logger2;
                        logger3.d(("Hard Acceleration detected, Acceleration " + acceleration + ", In time :" + timeDifference + " , ") + ("Current RawSpeed : " + value + ", Old RawSpeed : " + ((java.lang.Number) firstSampleInWindow.getSecond()).floatValue() + " ") + ("Last G , g : " + lastG + " , gAngle : " + lastGAngle + " , time : " + lastGTime));
                        if (ignoreAccelerometer || isHighGAccelerationDetected(getTimeWindowForHighGMatching())) {
                            lastHardAccelerationTime = timeStamp;
                            sessionHardAccelerationCount++;
                            dataSource.interestingEventDetected(com.navdy.hud.app.analytics.TelemetrySession.InterestingEvent.HARD_ACCELERATION);
                            return;
                        }
                        pendingHardAccelerationTime = timeStamp;
                        pendingHardAcceleration = acceleration;
                        return;
                    }
                    return;
                }
                double deceleration = ((double) (((java.lang.Number) firstSampleInWindow.getSecond()).floatValue() - value.getSpeed())) / timeDifference;
                if (dataSource.isVerboseLoggingNeeded()) {
                    logger.d("RawSpeed " + value + ", Change is speed : " + (value.getSpeed() - ((java.lang.Number) firstSampleInWindow.getSecond()).floatValue()) + " Braking : " + deceleration + " " + com.navdy.hud.app.framework.glance.GlanceConstants.NEWLINE + " Rolling data : " + rollingSpeedData);
                }
                if (deceleration <= HARD_BRAKING_THRESHOLD_METERS_PER_SECOND) {
                    return;
                }
                if (lastHardBrakingTime == 0 || timeStamp - lastHardBrakingTime > ((long) MINIMUM_TIME_BETWEEN_HARD_ACCELERATION_DECELERATION)) {
                    com.navdy.service.library.log.Logger logger4 = logger;
                    com.navdy.service.library.log.Logger logger5 = logger4;
                    logger5.d(("Hard Deceleration detected, Acceleration " + deceleration + ", In time :" + timeDifference + " ") + (",Current RawSpeed : " + value + ", Old RawSpeed : " + ((java.lang.Number) firstSampleInWindow.getSecond()).floatValue() + " ,") + ("Rolling Window : " + rollingSpeedData + " ") + (",Last G , g : " + lastG + " , gAngle : " + lastGAngle + " , time : " + lastGTime));
                    if (ignoreAccelerometer || isHighGBrakingDetected(getTimeWindowForHighGMatching())) {
                        lastHardBrakingTime = timeStamp;
                        sessionHardBrakingCount++;
                        dataSource.interestingEventDetected(com.navdy.hud.app.analytics.TelemetrySession.InterestingEvent.HARD_BRAKING);
                        return;
                    }
                    pendingHardBraking = deceleration;
                    pendingHardBrakingTime = timeStamp;
                }
            }
        }
    }

    public final void speedSourceChanged() {
        if (sessionStarted) {
            rollingSpeedData.clear();
        }
    }

    private final void updateValuesBasedOnSpeed() {
        if (sessionStarted) {
            com.navdy.hud.app.analytics.RawSpeed speedValue = currentSpeed;
            long now = getCurrentTime();
            if (speedValue.getSpeed() > ((float) 0)) {
                if (movingStartTime == 0) {
                    movingStartTime = now;
                }
            } else if (movingStartTime != 0) {
                _rollingDuration += now - movingStartTime;
                movingStartTime = 0;
            }
            sessionMaxSpeed = java.lang.Math.max(speedValue.getSpeed(), sessionMaxSpeed);
            if (currentSpeedLimit != 0.0f) {
                if (speedValue.getSpeed() >= currentSpeedLimit + SPEEDING_SPEED_OVER_SPEED_LIMIT_MS) {
                    if (speedingStartTime == 0) {
                        speedingStartTime = now;
                        dataSource.interestingEventDetected(com.navdy.hud.app.analytics.TelemetrySession.InterestingEvent.SPEEDING_STARTED);
                    }
                } else if (speedingStartTime != 0) {
                    _speedingDuration += now - speedingStartTime;
                    speedingStartTime = 0;
                    dataSource.interestingEventDetected(com.navdy.hud.app.analytics.TelemetrySession.InterestingEvent.SPEEDING_STOPPED);
                }
                if (speedValue.getSpeed() >= currentSpeedLimit + EXCESSIVE_SPEEDING_SPEED_OVER_SPEED_LIMIT_MS) {
                    if (excessiveSpeedingStartTime == 0) {
                        excessiveSpeedingStartTime = now;
                        dataSource.interestingEventDetected(com.navdy.hud.app.analytics.TelemetrySession.InterestingEvent.EXCESSIVE_SPEEDING_STARTED);
                    }
                } else if (excessiveSpeedingStartTime != 0) {
                    _excessiveSpeedingDuration += now - excessiveSpeedingStartTime;
                    excessiveSpeedingStartTime = 0;
                    dataSource.interestingEventDetected(com.navdy.hud.app.analytics.TelemetrySession.InterestingEvent.EXCESSIVE_SPEEDING_STOPPED);
                }
            } else {
                if (speedingStartTime != 0) {
                    _speedingDuration += now - speedingStartTime;
                    speedingStartTime = 0;
                    dataSource.interestingEventDetected(com.navdy.hud.app.analytics.TelemetrySession.InterestingEvent.SPEEDING_STOPPED);
                }
                if (excessiveSpeedingStartTime != 0) {
                    _excessiveSpeedingDuration += now - excessiveSpeedingStartTime;
                    excessiveSpeedingStartTime = 0;
                    dataSource.interestingEventDetected(com.navdy.hud.app.analytics.TelemetrySession.InterestingEvent.EXCESSIVE_SPEEDING_STOPPED);
                }
            }
        }
    }

    public final float getCurrentSpeedLimit() {
        return currentSpeedLimit;
    }

    public final void setCurrentSpeedLimit(float value) {
        if (value <= ((float) 0)) {
            value = 0.0f;
        }
        currentSpeedLimit = value;
        updateValuesBasedOnSpeed();
    }

    public final long getExcessiveSpeedingStartTime() {
        return excessiveSpeedingStartTime;
    }

    public final void setExcessiveSpeedingStartTime(long j) {
        excessiveSpeedingStartTime = j;
    }

    public final boolean isSpeeding() {
        return speedingStartTime > ((long) 0);
    }

    public final void setSpeeding(boolean value) {
    }

    public final void setSpeedingDuration(long j) {
        speedingDuration = j;
    }

    public final long getSpeedingDuration() {
        long j = 0;
        long j2 = _speedingDuration;
        if (speedingStartTime != 0) {
            j = getCurrentTime() - speedingStartTime;
        }
        return j + j2;
    }

    public final boolean isExcessiveSpeeding() {
        return excessiveSpeedingStartTime > ((long) 0);
    }

    public final void setExcessiveSpeeding(boolean value) {
    }

    public final void setExcessiveSpeedingDuration(long j) {
        excessiveSpeedingDuration = j;
    }

    public final long getExcessiveSpeedingDuration() {
        long j = 0;
        long j2 = _excessiveSpeedingDuration;
        if (excessiveSpeedingStartTime != 0) {
            j = getCurrentTime() - excessiveSpeedingStartTime;
        }
        return j + j2;
    }

    public final float getSessionMaxSpeed() {
        return sessionMaxSpeed;
    }

    public final void setSessionMaxSpeed(float f) {
        sessionMaxSpeed = f;
    }

    private final void setSessionDuration(long j) {
        sessionDuration = j;
    }

    public final long getSessionDuration() {
        return getCurrentTime() - sessionStartTime;
    }

    public final void setSessionRollingDuration(long j) {
        sessionRollingDuration = j;
    }

    public final long getSessionRollingDuration() {
        long j = 0;
        long j2 = _rollingDuration;
        if (movingStartTime != 0) {
            j = getCurrentTime() - movingStartTime;
        }
        return j + j2;
    }

    public final void setSessionDistance(long j) {
        sessionDistance = j;
    }

    public final long getSessionDistance() {
        return get_currentDistance() - startDistance;
    }

    @org.jetbrains.annotations.NotNull
    public final java.util.LinkedList<kotlin.Pair<java.lang.Long, java.lang.Float>> getRollingSpeedData() {
        return rollingSpeedData;
    }

    public final boolean getSessionStarted() {
        return sessionStarted;
    }

    public final void setSessionStarted(boolean z) {
        sessionStarted = z;
    }

    public final void startSession() {
        sessionStartTime = getCurrentTime();
        sessionDuration = 0;
        startDistance = get_currentDistance();
        rollingSpeedData.clear();
        setCurrentSpeed(get_currentSpeed());
        sessionRollingDuration = 0;
        _speedingDuration = 0;
        _rollingDuration = 0;
        _excessiveSpeedingDuration = 0;
        excessiveSpeedingStartTime = 0;
        sessionMaxSpeed = 0.0f;
        movingStartTime = 0;
        speedingStartTime = 0;
        sessionStarted = true;
        sessionHighGCount = 0;
        sessionHardAccelerationCount = 0;
        lastHardAccelerationTime = 0;
        sessionHardBrakingCount = 0;
        lastHardBrakingTime = 0;
        highGAccelerationDetector.reset();
        highGBrakingDetector.reset();
        highGTurnDetector.reset();
    }

    public final void stopSession() {
        sessionStarted = false;
    }

    public final double getEventAverageMpg() {
        return eventAverageMpg;
    }

    public final void setEventAverageMpg(double d) {
        eventAverageMpg = d;
    }

    public final long getEventMpgSamples() {
        return eventMpgSamples;
    }

    public final void setEventMpgSamples(long j) {
        eventMpgSamples = j;
    }

    public final int getEventAverageRpm() {
        return eventAverageRpm;
    }

    public final void setEventAverageRpm(int i) {
        eventAverageRpm = i;
    }

    public final int getEventRpmSamples() {
        return eventRpmSamples;
    }

    public final void setEventRpmSamples(int i) {
        eventRpmSamples = i;
    }

    public final void eventReported() {
        eventAverageMpg = 0.0d;
        eventMpgSamples = 1;
        eventAverageRpm = 0;
        eventRpmSamples = 1;
        maxG = 0.0f;
        maxGAngle = 0.0f;
    }

    public final float getMaxG() {
        return maxG;
    }

    public final void setMaxG(float f) {
        maxG = f;
    }

    public final float getMaxGAngle() {
        return maxGAngle;
    }

    public final void setMaxGAngle(float f) {
        maxGAngle = f;
    }

    public final int getSessionHighGCount() {
        return sessionHighGCount;
    }

    public final void setSessionHighGCount(int i) {
        sessionHighGCount = i;
    }

    public final float getLastG() {
        return lastG;
    }

    public final void setLastG(float f) {
        lastG = f;
    }

    public final float getLastGAngle() {
        return lastGAngle;
    }

    public final void setLastGAngle(float f) {
        lastGAngle = f;
    }

    public final long getLastGTime() {
        return lastGTime;
    }

    public final void setLastGTime(long j) {
        lastGTime = j;
    }

    @org.jetbrains.annotations.NotNull
    public final com.navdy.hud.app.analytics.HighGDetector getHighGTurnDetector() {
        return highGTurnDetector;
    }

    @org.jetbrains.annotations.NotNull
    public final kotlin.jvm.functions.Function1<java.lang.Float, java.lang.Boolean> getHighGTurnAngleFilter() {
        return highGTurnAngleFilter;
    }

    @org.jetbrains.annotations.NotNull
    public final kotlin.jvm.functions.Function1<java.lang.Boolean, kotlin.Unit> getHighGTurnEventCallBack() {
        return highGTurnEventCallBack;
    }

    @org.jetbrains.annotations.NotNull
    public final com.navdy.hud.app.analytics.HighGDetector getHighGAccelerationDetector() {
        return highGAccelerationDetector;
    }

    @org.jetbrains.annotations.NotNull
    public final kotlin.jvm.functions.Function1<java.lang.Float, java.lang.Boolean> getHighGAccelerationAngleFilter() {
        return highGAccelerationAngleFilter;
    }

    @org.jetbrains.annotations.NotNull
    public final kotlin.jvm.functions.Function1<java.lang.Boolean, kotlin.Unit> getHighGAccelerationEventCallBack() {
        return highGAccelerationEventCallBack;
    }

    @org.jetbrains.annotations.NotNull
    public final com.navdy.hud.app.analytics.HighGDetector getHighGBrakingDetector() {
        return highGBrakingDetector;
    }

    @org.jetbrains.annotations.NotNull
    public final kotlin.jvm.functions.Function1<java.lang.Float, java.lang.Boolean> getHighGBrakingAngleFilter() {
        return highGBrakingAngleFilter;
    }

    @org.jetbrains.annotations.NotNull
    public final kotlin.jvm.functions.Function1<java.lang.Boolean, kotlin.Unit> getHighGBrakingEventCallBack() {
        return highGBrakingEventCallBack;
    }

    public final long getTimeWindowForHighGMatching() {
        if (dataSource.isHighAccuracySpeedAvailable()) {
            return HIGH_G_THRESHOLD_TIME_MILLIS;
        }
        return 1000;
    }

    public final void setGForce(float x, float y, float z) {
        float g = java.lang.Math.max(java.lang.Math.abs(x), java.lang.Math.abs(y));
        float gAngle = (float) ((java.lang.Math.atan((double) ((-y) / x)) * ((double) 180.0f)) / 3.141592653589793d);
        if (x < ((float) 0)) {
            gAngle += 180.0f;
        }
        float gAngle2 = (((float) 360) + gAngle) % ((float) 360);
        lastG = g;
        lastGAngle = gAngle2;
        long now = getCurrentTime();
        lastGTime = now;
        if (g > maxG) {
            maxG = g;
            maxGAngle = gAngle2;
        }
        highGTurnDetector.newGValue(g, gAngle2, now, highGTurnAngleFilter, highGTurnEventCallBack);
        highGAccelerationDetector.newGValue(g, gAngle2, now, highGAccelerationAngleFilter, highGAccelerationEventCallBack);
        highGBrakingDetector.newGValue(g, gAngle2, now, highGBrakingAngleFilter, highGBrakingEventCallBack);
    }

    public final boolean isDoingHighGManeuver() {
        return highGTurnDetector.getConfirmedHighGManeuverOngoing();
    }

    public final void setDoingHighGManeuver(boolean value) {
    }

    public final boolean isHighGAccelerationDetected(long within) {
        return highGAccelerationDetector.getConfirmedHighGManeuverOngoing() || (highGAccelerationDetector.getConfirmedHighGManeuverEndTime() > ((long) 0) && com.navdy.hud.app.analytics.TelemetrySessionKt.timeSince(getCurrentTime(), highGAccelerationDetector.getConfirmedHighGManeuverEndTime()) <= within);
    }

    public final boolean isHighGBrakingDetected(long within) {
        return highGBrakingDetector.getConfirmedHighGManeuverOngoing() || (highGBrakingDetector.getConfirmedHighGManeuverEndTime() > ((long) 0) && com.navdy.hud.app.analytics.TelemetrySessionKt.timeSince(getCurrentTime(), highGBrakingDetector.getConfirmedHighGManeuverEndTime()) <= within);
    }
}
