package com.navdy.hud.app.analytics;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000-\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0004*\u0001\u0000\b\n\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u00020\u0004H\u0016J\b\u0010\u0005\u001a\u00020\u0006H\u0016J\u0010\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nH\u0016J\b\u0010\u000b\u001a\u00020\fH\u0016J\b\u0010\r\u001a\u00020\fH\u0016J\b\u0010\u000e\u001a\u00020\fH\u0016J\b\u0010\u000f\u001a\u00020\u0004H\u0016\u00a8\u0006\u0010"}, d2 = {"com/navdy/hud/app/analytics/TelemetrySession$dataSource$Anon1", "Lcom/navdy/hud/app/analytics/TelemetrySession$DataSource;", "()V", "absoluteCurrentTime", "", "currentSpeed", "Lcom/navdy/hud/app/analytics/RawSpeed;", "interestingEventDetected", "", "event", "Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;", "isExperimental", "", "isHighAccuracySpeedAvailable", "isVerboseLoggingNeeded", "totalDistanceTravelledWithMeters", "app_hudDebug"}, k = 1, mv = {1, 1, 6})
/* compiled from: TelemetrySession.kt */
public final class TelemetrySession$dataSource$Anon1 implements com.navdy.hud.app.analytics.TelemetrySession.DataSource {
    TelemetrySession$dataSource$Anon1() {
    }

    public long totalDistanceTravelledWithMeters() {
        return 0;
    }

    @org.jetbrains.annotations.NotNull
    public com.navdy.hud.app.analytics.RawSpeed currentSpeed() {
        return new com.navdy.hud.app.analytics.RawSpeed((float) -1, 0);
    }

    public void interestingEventDetected(@org.jetbrains.annotations.NotNull com.navdy.hud.app.analytics.TelemetrySession.InterestingEvent event) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(event, "event");
    }

    public boolean isVerboseLoggingNeeded() {
        return false;
    }

    public boolean isExperimental() {
        return false;
    }

    public long absoluteCurrentTime() {
        return 0;
    }

    public boolean isHighAccuracySpeedAvailable() {
        return false;
    }
}
