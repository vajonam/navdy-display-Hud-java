package com.navdy.hud.app.analytics;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u00c2\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u0000 P2\u00020\u0001:\u0002PQB-\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\fJ\u0010\u0010.\u001a\u00020+2\u0006\u0010/\u001a\u000200H\u0007J\u0010\u00101\u001a\u00020+2\u0006\u0010/\u001a\u000202H\u0007J\u0018\u00103\u001a\u0004\u0018\u0001042\u0006\u00105\u001a\u00020\u00172\u0006\u00106\u001a\u00020\u0018J\u0010\u00107\u001a\u00020+2\u0006\u00108\u001a\u000209H\u0007J\u0010\u0010:\u001a\u00020+2\u0006\u0010;\u001a\u00020<H\u0007J\u0010\u0010=\u001a\u00020+2\u0006\u0010/\u001a\u00020>H\u0007J\u0010\u0010?\u001a\u00020+2\u0006\u0010/\u001a\u00020@H\u0007J\u0010\u0010A\u001a\u00020+2\u0006\u0010B\u001a\u00020CH\u0007J\u0010\u0010D\u001a\u00020+2\u0006\u0010E\u001a\u00020FH\u0007J\u0010\u0010G\u001a\u00020+2\u0006\u0010H\u001a\u00020IH\u0007J\b\u0010J\u001a\u00020+H\u0002J\u0006\u0010K\u001a\u00020+J\u0010\u0010L\u001a\u00020+2\b\b\u0002\u00105\u001a\u00020\u0017J\b\u0010M\u001a\u00020+H\u0002J\f\u0010N\u001a\u0004\u0018\u00010O*\u00020\u0017R\u000e\u0010\r\u001a\u00020\u000eX\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u000f\u001a\u00020\u0010X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\u0012\"\u0004\b\u0013\u0010\u0014R*\u0010\u0015\u001a\u001e\u0012\u0004\u0012\u00020\u0017\u0012\u0004\u0012\u00020\u00180\u0016j\u000e\u0012\u0004\u0012\u00020\u0017\u0012\u0004\u0012\u00020\u0018`\u0019X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u001bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u001c\u001a\u00020\u001dX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001e\u0010\u001f\"\u0004\b \u0010!R\u000e\u0010\"\u001a\u00020#X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010$\u001a\u00020\u000eX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b%\u0010&\"\u0004\b'\u0010(R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010)\u001a\b\u0012\u0004\u0012\u00020+0*X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010,\u001a\u00020-X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006R"}, d2 = {"Lcom/navdy/hud/app/analytics/TelemetryDataManager;", "", "uiStateManager", "Lcom/navdy/hud/app/ui/framework/UIStateManager;", "powerManager", "Lcom/navdy/hud/app/device/PowerManager;", "bus", "Lcom/squareup/otto/Bus;", "sharedPreferences", "Landroid/content/SharedPreferences;", "tripManager", "Lcom/navdy/hud/app/framework/trips/TripManager;", "(Lcom/navdy/hud/app/ui/framework/UIStateManager;Lcom/navdy/hud/app/device/PowerManager;Lcom/squareup/otto/Bus;Landroid/content/SharedPreferences;Lcom/navdy/hud/app/framework/trips/TripManager;)V", "TIME_BETWEEN_SPEED_AUDIO_REQUESTS", "", "driveScoreUpdatedEvent", "Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;", "getDriveScoreUpdatedEvent", "()Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;", "setDriveScoreUpdatedEvent", "(Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;)V", "eventTypeSentTimeMap", "Ljava/util/HashMap;", "Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;", "", "Lkotlin/collections/HashMap;", "handler", "Landroid/os/Handler;", "highAccuracySpeedAvailable", "", "getHighAccuracySpeedAvailable", "()Z", "setHighAccuracySpeedAvailable", "(Z)V", "id", "Ljava/util/concurrent/atomic/AtomicLong;", "lastDriveScore", "getLastDriveScore", "()I", "setLastDriveScore", "(I)V", "reportRunnable", "Lkotlin/Function0;", "", "updateDriveScoreRunnable", "Ljava/lang/Runnable;", "GPSSpeedChangeEvent", "event", "Lcom/navdy/hud/app/maps/MapEvents$GPSSpeedEvent;", "ObdPidChangeEvent", "Lcom/navdy/hud/app/obd/ObdManager$ObdPidChangeEvent;", "getPlayAudioRequest", "Lcom/navdy/service/library/events/audio/PlayAudioRequest;", "interestingEvent", "elapsedRealTime", "onCalibratedGForceData", "calibratedGForceData", "Lcom/navdy/hud/app/device/gps/CalibratedGForceData;", "onGpsSwitched", "gpsSwitch", "Lcom/navdy/hud/app/device/gps/GpsUtils$GpsSwitch;", "onInit", "Lcom/navdy/hud/app/event/InitEvents$InitPhase;", "onMapEvent", "Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;", "onObdConnectionStateChanged", "connectionStateChangeEvent", "Lcom/navdy/hud/app/obd/ObdManager$ObdConnectionStatusEvent;", "onSpeedDataExpired", "speedDataExpired", "Lcom/navdy/hud/app/manager/SpeedManager$SpeedDataExpired;", "onWakeup", "wakeUpEvent", "Lcom/navdy/hud/app/event/Wakeup;", "reportTelemetryData", "startTelemetrySession", "updateDriveScore", "updateSpeed", "audio", "Lcom/navdy/service/library/events/audio/Audio;", "Companion", "DriveScoreUpdated", "app_hudDebug"}, k = 1, mv = {1, 1, 6})
/* compiled from: TelemetryDataManager.kt */
public final class TelemetryDataManager {
    /* access modifiers changed from: private */
    public static final long ANALYTICS_REPORTING_INTERVAL = ANALYTICS_REPORTING_INTERVAL;
    public static final com.navdy.hud.app.analytics.TelemetryDataManager.Companion Companion = new com.navdy.hud.app.analytics.TelemetryDataManager.Companion(null);
    /* access modifiers changed from: private */
    public static final long DRIVE_SCORE_PUBLISH_INTERVAL = 3000;
    /* access modifiers changed from: private */
    public static final boolean LOG_TELEMETRY_DATA = com.navdy.hud.app.util.os.SystemProperties.getBoolean("persist.sys.prop.log.telemetry", false);
    /* access modifiers changed from: private */
    public static final float MAX_ACCEL = 2.0f;
    /* access modifiers changed from: private */
    public static final java.lang.String PREFERENCE_TROUBLE_CODES = PREFERENCE_TROUBLE_CODES;
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger("TelemetryDataManager");
    private final int TIME_BETWEEN_SPEED_AUDIO_REQUESTS = 60000;
    private final com.squareup.otto.Bus bus;
    @org.jetbrains.annotations.NotNull
    private com.navdy.hud.app.analytics.TelemetryDataManager.DriveScoreUpdated driveScoreUpdatedEvent;
    private final java.util.HashMap<com.navdy.hud.app.analytics.TelemetrySession.InterestingEvent, java.lang.Long> eventTypeSentTimeMap = new java.util.HashMap<>();
    private android.os.Handler handler;
    private boolean highAccuracySpeedAvailable;
    private final java.util.concurrent.atomic.AtomicLong id = new java.util.concurrent.atomic.AtomicLong(0);
    private int lastDriveScore;
    private final com.navdy.hud.app.device.PowerManager powerManager;
    private final kotlin.jvm.functions.Function0<kotlin.Unit> reportRunnable = new com.navdy.hud.app.analytics.TelemetryDataManager$reportRunnable$Anon1(this);
    private final android.content.SharedPreferences sharedPreferences;
    /* access modifiers changed from: private */
    public final com.navdy.hud.app.framework.trips.TripManager tripManager;
    /* access modifiers changed from: private */
    public final com.navdy.hud.app.ui.framework.UIStateManager uiStateManager;
    private final java.lang.Runnable updateDriveScoreRunnable;

    @kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000-\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0004*\u0001\u0000\b\n\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u00020\u0004H\u0016J\b\u0010\u0005\u001a\u00020\u0006H\u0016J\u0010\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nH\u0016J\b\u0010\u000b\u001a\u00020\fH\u0016J\b\u0010\r\u001a\u00020\fH\u0016J\b\u0010\u000e\u001a\u00020\fH\u0016J\b\u0010\u000f\u001a\u00020\u0004H\u0016\u00a8\u0006\u0010"}, d2 = {"com/navdy/hud/app/analytics/TelemetryDataManager$Anon1", "Lcom/navdy/hud/app/analytics/TelemetrySession$DataSource;", "(Lcom/navdy/hud/app/analytics/TelemetryDataManager;)V", "absoluteCurrentTime", "", "currentSpeed", "Lcom/navdy/hud/app/analytics/RawSpeed;", "interestingEventDetected", "", "event", "Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;", "isExperimental", "", "isHighAccuracySpeedAvailable", "isVerboseLoggingNeeded", "totalDistanceTravelledWithMeters", "app_hudDebug"}, k = 1, mv = {1, 1, 6})
    /* compiled from: TelemetryDataManager.kt */
    public static final class Anon1 implements com.navdy.hud.app.analytics.TelemetrySession.DataSource {
        final /* synthetic */ com.navdy.hud.app.analytics.TelemetryDataManager this$Anon0;

        Anon1(com.navdy.hud.app.analytics.TelemetryDataManager $outer) {
            this.this$Anon0 = $outer;
        }

        public boolean isExperimental() {
            return !com.navdy.hud.app.util.DeviceUtil.isUserBuild();
        }

        public boolean isHighAccuracySpeedAvailable() {
            return this.this$Anon0.getHighAccuracySpeedAvailable();
        }

        public long absoluteCurrentTime() {
            return android.os.SystemClock.elapsedRealtime();
        }

        public boolean isVerboseLoggingNeeded() {
            return !com.navdy.hud.app.util.DeviceUtil.isUserBuild() && kotlin.jvm.internal.Intrinsics.areEqual((java.lang.Object) this.this$Anon0.uiStateManager.getHomescreenView().getDisplayMode(), (java.lang.Object) com.navdy.hud.app.ui.component.homescreen.HomeScreen.DisplayMode.SMART_DASH) && this.this$Anon0.uiStateManager.getSmartDashView().isShowingDriveScoreGauge;
        }

        public void interestingEventDetected(@org.jetbrains.annotations.NotNull com.navdy.hud.app.analytics.TelemetrySession.InterestingEvent event) {
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(event, "event");
            this.this$Anon0.updateDriveScore(event);
        }

        public long totalDistanceTravelledWithMeters() {
            return this.this$Anon0.tripManager.getTotalDistanceTravelledWithNavdy();
        }

        @org.jetbrains.annotations.NotNull
        public com.navdy.hud.app.analytics.RawSpeed currentSpeed() {
            com.navdy.hud.app.analytics.RawSpeed currentSpeedInMetersPerSecond = com.navdy.hud.app.manager.SpeedManager.getInstance().getCurrentSpeedInMetersPerSecond();
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(currentSpeedInMetersPerSecond, "SpeedManager.getInstance\u2026entSpeedInMetersPerSecond");
            return currentSpeedInMetersPerSecond;
        }
    }

    @kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\u0007\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J(\u0010\u0019\u001a\u000e\u0012\u0004\u0012\u00020\u0012\u0012\u0004\u0012\u00020\u00120\u001a2\f\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\u00120\u001c2\u0006\u0010\u001d\u001a\u00020\u0012R\u0014\u0010\u0003\u001a\u00020\u0004X\u0082D\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u0014\u0010\u0007\u001a\u00020\u0004X\u0082D\u00a2\u0006\b\n\u0000\u001a\u0004\b\b\u0010\u0006R\u0014\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0014\u0010\r\u001a\u00020\u000eX\u0082D\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0014\u0010\u0011\u001a\u00020\u0012X\u0082D\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014R\u0014\u0010\u0015\u001a\u00020\u0016X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0018\u00a8\u0006\u001e"}, d2 = {"Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;", "", "()V", "ANALYTICS_REPORTING_INTERVAL", "", "getANALYTICS_REPORTING_INTERVAL", "()J", "DRIVE_SCORE_PUBLISH_INTERVAL", "getDRIVE_SCORE_PUBLISH_INTERVAL", "LOG_TELEMETRY_DATA", "", "getLOG_TELEMETRY_DATA", "()Z", "MAX_ACCEL", "", "getMAX_ACCEL", "()F", "PREFERENCE_TROUBLE_CODES", "", "getPREFERENCE_TROUBLE_CODES", "()Ljava/lang/String;", "sLogger", "Lcom/navdy/service/library/log/Logger;", "getSLogger", "()Lcom/navdy/service/library/log/Logger;", "serializeTroubleCodes", "Lkotlin/Pair;", "troubleCodes", "", "savedTroubleCodesStringFromPreviousSession", "app_hudDebug"}, k = 1, mv = {1, 1, 6})
    /* compiled from: TelemetryDataManager.kt */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(kotlin.jvm.internal.DefaultConstructorMarker $constructor_marker) {
            this();
        }

        /* access modifiers changed from: private */
        public final com.navdy.service.library.log.Logger getSLogger() {
            return com.navdy.hud.app.analytics.TelemetryDataManager.sLogger;
        }

        /* access modifiers changed from: private */
        public final java.lang.String getPREFERENCE_TROUBLE_CODES() {
            return com.navdy.hud.app.analytics.TelemetryDataManager.PREFERENCE_TROUBLE_CODES;
        }

        /* access modifiers changed from: private */
        public final long getANALYTICS_REPORTING_INTERVAL() {
            return com.navdy.hud.app.analytics.TelemetryDataManager.ANALYTICS_REPORTING_INTERVAL;
        }

        /* access modifiers changed from: private */
        public final long getDRIVE_SCORE_PUBLISH_INTERVAL() {
            return com.navdy.hud.app.analytics.TelemetryDataManager.DRIVE_SCORE_PUBLISH_INTERVAL;
        }

        private final float getMAX_ACCEL() {
            return com.navdy.hud.app.analytics.TelemetryDataManager.MAX_ACCEL;
        }

        /* access modifiers changed from: private */
        public final boolean getLOG_TELEMETRY_DATA() {
            return com.navdy.hud.app.analytics.TelemetryDataManager.LOG_TELEMETRY_DATA;
        }

        @org.jetbrains.annotations.NotNull
        public final kotlin.Pair<java.lang.String, java.lang.String> serializeTroubleCodes(@org.jetbrains.annotations.NotNull java.util.List<java.lang.String> troubleCodes, @org.jetbrains.annotations.NotNull java.lang.String savedTroubleCodesStringFromPreviousSession) {
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(troubleCodes, "troubleCodes");
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(savedTroubleCodesStringFromPreviousSession, "savedTroubleCodesStringFromPreviousSession");
            java.util.HashSet hashSet = new java.util.HashSet();
            if (!android.text.TextUtils.isEmpty(savedTroubleCodesStringFromPreviousSession)) {
                for (java.lang.String troubleCode : kotlin.text.StringsKt.split$default((java.lang.CharSequence) savedTroubleCodesStringFromPreviousSession, new java.lang.String[]{" "}, false, 0, 6, (java.lang.Object) null)) {
                    hashSet.add(troubleCode);
                }
            }
            java.lang.StringBuilder updatedTroubleCodesString = new java.lang.StringBuilder();
            java.lang.StringBuilder troubleCodesDiffString = new java.lang.StringBuilder();
            if (troubleCodes.size() > 0) {
                com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.setSessionTroubleCodeCount(troubleCodes.size());
                for (java.lang.String newTroubleCode : troubleCodes) {
                    if (!hashSet.contains(newTroubleCode)) {
                        troubleCodesDiffString.append(newTroubleCode + " ");
                    }
                    updatedTroubleCodesString.append(newTroubleCode + " ");
                }
            }
            java.lang.String sb = updatedTroubleCodesString.toString();
            if (sb == null) {
                throw new kotlin.TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
            }
            java.lang.String obj = kotlin.text.StringsKt.trim((java.lang.CharSequence) sb).toString();
            java.lang.String sb2 = troubleCodesDiffString.toString();
            if (sb2 != null) {
                return new kotlin.Pair<>(obj, kotlin.text.StringsKt.trim((java.lang.CharSequence) sb2).toString());
            }
            throw new kotlin.TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
        }
    }

    @kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0018\n\u0002\u0010\u000e\n\u0000\b\u0086\b\u0018\u00002\u00020\u0001B-\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0005\u0012\u0006\u0010\b\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\t\u0010\u0018\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0019\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u001a\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u001b\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u001c\u001a\u00020\tH\u00c6\u0003J;\u0010\u001d\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00052\b\b\u0002\u0010\u0007\u001a\u00020\u00052\b\b\u0002\u0010\b\u001a\u00020\tH\u00c6\u0001J\u0013\u0010\u001e\u001a\u00020\u00052\b\u0010\u001f\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010 \u001a\u00020\tH\u00d6\u0001J\t\u0010!\u001a\u00020\"H\u00d6\u0001R\u001a\u0010\b\u001a\u00020\tX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR\u001a\u0010\u0002\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000f\u0010\u0010\"\u0004\b\u0011\u0010\u0012R\u001a\u0010\u0007\u001a\u00020\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\u0013\"\u0004\b\u0014\u0010\u0015R\u001a\u0010\u0006\u001a\u00020\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\u0013\"\u0004\b\u0016\u0010\u0015R\u001a\u0010\u0004\u001a\u00020\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0004\u0010\u0013\"\u0004\b\u0017\u0010\u0015\u00a8\u0006#"}, d2 = {"Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;", "", "interestingEvent", "Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;", "isSpeeding", "", "isExcessiveSpeeding", "isDoingHighGManeuver", "driveScore", "", "(Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;ZZZI)V", "getDriveScore", "()I", "setDriveScore", "(I)V", "getInterestingEvent", "()Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;", "setInterestingEvent", "(Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;)V", "()Z", "setDoingHighGManeuver", "(Z)V", "setExcessiveSpeeding", "setSpeeding", "component1", "component2", "component3", "component4", "component5", "copy", "equals", "other", "hashCode", "toString", "", "app_hudDebug"}, k = 1, mv = {1, 1, 6})
    /* compiled from: TelemetryDataManager.kt */
    public static final class DriveScoreUpdated {
        private int driveScore;
        @org.jetbrains.annotations.NotNull
        private com.navdy.hud.app.analytics.TelemetrySession.InterestingEvent interestingEvent;
        private boolean isDoingHighGManeuver;
        private boolean isExcessiveSpeeding;
        private boolean isSpeeding;

        @org.jetbrains.annotations.NotNull
        public static /* bridge */ /* synthetic */ com.navdy.hud.app.analytics.TelemetryDataManager.DriveScoreUpdated copy$default(com.navdy.hud.app.analytics.TelemetryDataManager.DriveScoreUpdated driveScoreUpdated, com.navdy.hud.app.analytics.TelemetrySession.InterestingEvent interestingEvent2, boolean z, boolean z2, boolean z3, int i, int i2, java.lang.Object obj) {
            return driveScoreUpdated.copy((i2 & 1) != 0 ? driveScoreUpdated.interestingEvent : interestingEvent2, (i2 & 2) != 0 ? driveScoreUpdated.isSpeeding : z, (i2 & 4) != 0 ? driveScoreUpdated.isExcessiveSpeeding : z2, (i2 & 8) != 0 ? driveScoreUpdated.isDoingHighGManeuver : z3, (i2 & 16) != 0 ? driveScoreUpdated.driveScore : i);
        }

        @org.jetbrains.annotations.NotNull
        public final com.navdy.hud.app.analytics.TelemetrySession.InterestingEvent component1() {
            return this.interestingEvent;
        }

        public final boolean component2() {
            return this.isSpeeding;
        }

        public final boolean component3() {
            return this.isExcessiveSpeeding;
        }

        public final boolean component4() {
            return this.isDoingHighGManeuver;
        }

        public final int component5() {
            return this.driveScore;
        }

        @org.jetbrains.annotations.NotNull
        public final com.navdy.hud.app.analytics.TelemetryDataManager.DriveScoreUpdated copy(@org.jetbrains.annotations.NotNull com.navdy.hud.app.analytics.TelemetrySession.InterestingEvent interestingEvent2, boolean isSpeeding2, boolean isExcessiveSpeeding2, boolean isDoingHighGManeuver2, int driveScore2) {
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(interestingEvent2, "interestingEvent");
            return new com.navdy.hud.app.analytics.TelemetryDataManager.DriveScoreUpdated(interestingEvent2, isSpeeding2, isExcessiveSpeeding2, isDoingHighGManeuver2, driveScore2);
        }

        public boolean equals(java.lang.Object obj) {
            if (this != obj) {
                if (!(obj instanceof com.navdy.hud.app.analytics.TelemetryDataManager.DriveScoreUpdated)) {
                    return false;
                }
                com.navdy.hud.app.analytics.TelemetryDataManager.DriveScoreUpdated driveScoreUpdated = (com.navdy.hud.app.analytics.TelemetryDataManager.DriveScoreUpdated) obj;
                if (!kotlin.jvm.internal.Intrinsics.areEqual((java.lang.Object) this.interestingEvent, (java.lang.Object) driveScoreUpdated.interestingEvent)) {
                    return false;
                }
                if (!(this.isSpeeding == driveScoreUpdated.isSpeeding)) {
                    return false;
                }
                if (!(this.isExcessiveSpeeding == driveScoreUpdated.isExcessiveSpeeding)) {
                    return false;
                }
                if (!(this.isDoingHighGManeuver == driveScoreUpdated.isDoingHighGManeuver)) {
                    return false;
                }
                if (!(this.driveScore == driveScoreUpdated.driveScore)) {
                    return false;
                }
            }
            return true;
        }

        public int hashCode() {
            int i = 1;
            com.navdy.hud.app.analytics.TelemetrySession.InterestingEvent interestingEvent2 = this.interestingEvent;
            int hashCode = (interestingEvent2 != null ? interestingEvent2.hashCode() : 0) * 31;
            int i2 = this.isSpeeding;
            if (i2 != 0) {
                i2 = 1;
            }
            int i3 = (i2 + hashCode) * 31;
            int i4 = this.isExcessiveSpeeding;
            if (i4 != 0) {
                i4 = 1;
            }
            int i5 = (i4 + i3) * 31;
            boolean z = this.isDoingHighGManeuver;
            if (!z) {
                i = z;
            }
            return ((i5 + i) * 31) + this.driveScore;
        }

        public java.lang.String toString() {
            return "DriveScoreUpdated(interestingEvent=" + this.interestingEvent + ", isSpeeding=" + this.isSpeeding + ", isExcessiveSpeeding=" + this.isExcessiveSpeeding + ", isDoingHighGManeuver=" + this.isDoingHighGManeuver + ", driveScore=" + this.driveScore + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.CLOSE_BRACKET;
        }

        public DriveScoreUpdated(@org.jetbrains.annotations.NotNull com.navdy.hud.app.analytics.TelemetrySession.InterestingEvent interestingEvent2, boolean isSpeeding2, boolean isExcessiveSpeeding2, boolean isDoingHighGManeuver2, int driveScore2) {
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(interestingEvent2, "interestingEvent");
            this.interestingEvent = interestingEvent2;
            this.isSpeeding = isSpeeding2;
            this.isExcessiveSpeeding = isExcessiveSpeeding2;
            this.isDoingHighGManeuver = isDoingHighGManeuver2;
            this.driveScore = driveScore2;
        }

        public final int getDriveScore() {
            return this.driveScore;
        }

        @org.jetbrains.annotations.NotNull
        public final com.navdy.hud.app.analytics.TelemetrySession.InterestingEvent getInterestingEvent() {
            return this.interestingEvent;
        }

        public final boolean isDoingHighGManeuver() {
            return this.isDoingHighGManeuver;
        }

        public final boolean isExcessiveSpeeding() {
            return this.isExcessiveSpeeding;
        }

        public final boolean isSpeeding() {
            return this.isSpeeding;
        }

        public final void setDoingHighGManeuver(boolean z) {
            this.isDoingHighGManeuver = z;
        }

        public final void setDriveScore(int i) {
            this.driveScore = i;
        }

        public final void setExcessiveSpeeding(boolean z) {
            this.isExcessiveSpeeding = z;
        }

        public final void setInterestingEvent(@org.jetbrains.annotations.NotNull com.navdy.hud.app.analytics.TelemetrySession.InterestingEvent interestingEvent2) {
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(interestingEvent2, "<set-?>");
            this.interestingEvent = interestingEvent2;
        }

        public final void setSpeeding(boolean z) {
            this.isSpeeding = z;
        }
    }

    public TelemetryDataManager(@org.jetbrains.annotations.NotNull com.navdy.hud.app.ui.framework.UIStateManager uiStateManager2, @org.jetbrains.annotations.NotNull com.navdy.hud.app.device.PowerManager powerManager2, @org.jetbrains.annotations.NotNull com.squareup.otto.Bus bus2, @org.jetbrains.annotations.NotNull android.content.SharedPreferences sharedPreferences2, @org.jetbrains.annotations.NotNull com.navdy.hud.app.framework.trips.TripManager tripManager2) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(uiStateManager2, "uiStateManager");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(powerManager2, "powerManager");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(bus2, "bus");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(sharedPreferences2, "sharedPreferences");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(tripManager2, "tripManager");
        this.uiStateManager = uiStateManager2;
        this.powerManager = powerManager2;
        this.bus = bus2;
        this.sharedPreferences = sharedPreferences2;
        this.tripManager = tripManager2;
        com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.setDataSource(new com.navdy.hud.app.analytics.TelemetryDataManager.Anon1(this));
        this.bus.register(this);
        this.handler = new android.os.Handler(android.os.Looper.getMainLooper());
        this.updateDriveScoreRunnable = new com.navdy.hud.app.analytics.TelemetryDataManager$updateDriveScoreRunnable$Anon1(this);
        this.driveScoreUpdatedEvent = new com.navdy.hud.app.analytics.TelemetryDataManager.DriveScoreUpdated(com.navdy.hud.app.analytics.TelemetrySession.InterestingEvent.NONE, false, false, false, 100);
    }

    @org.jetbrains.annotations.NotNull
    public final com.navdy.hud.app.analytics.TelemetryDataManager.DriveScoreUpdated getDriveScoreUpdatedEvent() {
        return this.driveScoreUpdatedEvent;
    }

    public final void setDriveScoreUpdatedEvent(@org.jetbrains.annotations.NotNull com.navdy.hud.app.analytics.TelemetryDataManager.DriveScoreUpdated driveScoreUpdated) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(driveScoreUpdated, "<set-?>");
        this.driveScoreUpdatedEvent = driveScoreUpdated;
    }

    public final int getLastDriveScore() {
        return this.lastDriveScore;
    }

    public final void setLastDriveScore(int i) {
        this.lastDriveScore = i;
    }

    public final boolean getHighAccuracySpeedAvailable() {
        return this.highAccuracySpeedAvailable;
    }

    public final void setHighAccuracySpeedAvailable(boolean z) {
        this.highAccuracySpeedAvailable = z;
    }

    public static /* bridge */ /* synthetic */ void updateDriveScore$default(com.navdy.hud.app.analytics.TelemetryDataManager telemetryDataManager, com.navdy.hud.app.analytics.TelemetrySession.InterestingEvent interestingEvent, int i, java.lang.Object obj) {
        if ((i & 1) != 0) {
            interestingEvent = com.navdy.hud.app.analytics.TelemetrySession.InterestingEvent.NONE;
        }
        telemetryDataManager.updateDriveScore(interestingEvent);
    }

    public final void updateDriveScore(@org.jetbrains.annotations.NotNull com.navdy.hud.app.analytics.TelemetrySession.InterestingEvent interestingEvent) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(interestingEvent, "interestingEvent");
        com.navdy.hud.app.analytics.TelemetrySession $receiver = com.navdy.hud.app.analytics.TelemetrySession.INSTANCE;
        double hardAccelerationFactor = $receiver.getSessionRollingDuration() > ((long) 0) ? (((double) (($receiver.getSessionHardAccelerationCount() + $receiver.getSessionHardBrakingCount()) + $receiver.getSessionHighGCount())) * 540.0d) / java.lang.Math.sqrt((((double) $receiver.getSessionRollingDuration()) / ((double) 1000.0f)) + ((double) 60)) : 0.0d;
        float speedingFactor = $receiver.getSessionSpeedingPercentage() * ((float) 50);
        float excessiveSpeedingFactor = $receiver.getSessionExcessiveSpeedingPercentage() * ((float) 50);
        int drivingScore = (int) java.lang.Math.ceil(java.lang.Math.max(0.0d, ((double) 100) - ((((double) speedingFactor) + hardAccelerationFactor) + ((double) excessiveSpeedingFactor))));
        this.driveScoreUpdatedEvent = new com.navdy.hud.app.analytics.TelemetryDataManager.DriveScoreUpdated(interestingEvent, $receiver.isSpeeding(), $receiver.isExcessiveSpeeding(), $receiver.isDoingHighGManeuver(), drivingScore);
        if ((!kotlin.jvm.internal.Intrinsics.areEqual((java.lang.Object) interestingEvent, (java.lang.Object) com.navdy.hud.app.analytics.TelemetrySession.InterestingEvent.NONE)) || drivingScore != this.lastDriveScore) {
            Companion.getSLogger().d("HA : " + $receiver.getSessionHardAccelerationCount() + " , HB : " + $receiver.getSessionHardBrakingCount() + " , HG : " + $receiver.getSessionHighGCount());
            Companion.getSLogger().d("Speeding percentage : " + $receiver.getSessionSpeedingPercentage());
            Companion.getSLogger().d("Excessive Speeding percentage : " + $receiver.getSessionExcessiveSpeedingPercentage());
            Companion.getSLogger().d("Hard Acceleration Factor : " + hardAccelerationFactor + " , Speeding Factor : " + speedingFactor + " , excessive Speeding factor " + excessiveSpeedingFactor);
            Companion.getSLogger().d("DriveScoreUpdate : " + this.driveScoreUpdatedEvent);
            this.bus.post(this.driveScoreUpdatedEvent);
        }
        if (!com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile().isDefaultProfile()) {
            java.lang.Boolean playDrivingBehaviorAlerts = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile().getAudioPreferences().drivingBehaviorAlerts;
            if (com.navdy.hud.app.util.RemoteCapabilitiesUtil.supportsPlayAudioRequest()) {
                kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(playDrivingBehaviorAlerts, "playDrivingBehaviorAlerts");
                if (playDrivingBehaviorAlerts.booleanValue()) {
                    com.navdy.service.library.events.audio.PlayAudioRequest playAudioRequest = getPlayAudioRequest(interestingEvent, android.os.SystemClock.elapsedRealtime());
                    if (playAudioRequest != null) {
                        this.bus.post(new com.navdy.hud.app.event.RemoteEvent(playAudioRequest));
                    }
                }
            }
        }
        this.lastDriveScore = drivingScore;
        this.handler.removeCallbacks(this.updateDriveScoreRunnable);
        this.handler.postDelayed(this.updateDriveScoreRunnable, Companion.getDRIVE_SCORE_PUBLISH_INTERVAL());
    }

    @org.jetbrains.annotations.Nullable
    public final com.navdy.service.library.events.audio.PlayAudioRequest getPlayAudioRequest(@org.jetbrains.annotations.NotNull com.navdy.hud.app.analytics.TelemetrySession.InterestingEvent interestingEvent, long elapsedRealTime) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(interestingEvent, "interestingEvent");
        switch (interestingEvent) {
            case HARD_ACCELERATION:
            case HARD_BRAKING:
            case HIGH_G_STARTED:
                this.eventTypeSentTimeMap.put(interestingEvent, java.lang.Long.valueOf(elapsedRealTime));
                return new com.navdy.service.library.events.audio.PlayAudioRequest.Builder().category(com.navdy.service.library.events.audio.Category.AUDIO_DRIVE_BEHAVIOR).id(java.lang.Long.valueOf(this.id.incrementAndGet())).audio(audio(interestingEvent)).timeStamp(java.lang.Long.valueOf(java.lang.System.currentTimeMillis())).build();
            case SPEEDING_STARTED:
                java.lang.Long l = (java.lang.Long) this.eventTypeSentTimeMap.get(interestingEvent);
                java.lang.Long lastSentTime = l != null ? l : java.lang.Long.valueOf(0);
                if (!kotlin.jvm.internal.Intrinsics.areEqual((java.lang.Object) lastSentTime, (java.lang.Object) java.lang.Long.valueOf(0))) {
                    kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(lastSentTime, "lastSentTime");
                    if (com.navdy.hud.app.analytics.TelemetrySessionKt.timeSince(elapsedRealTime, lastSentTime.longValue()) <= ((long) this.TIME_BETWEEN_SPEED_AUDIO_REQUESTS)) {
                        return null;
                    }
                }
                this.eventTypeSentTimeMap.put(interestingEvent, java.lang.Long.valueOf(elapsedRealTime));
                return new com.navdy.service.library.events.audio.PlayAudioRequest.Builder().category(com.navdy.service.library.events.audio.Category.AUDIO_DRIVE_BEHAVIOR).id(java.lang.Long.valueOf(this.id.incrementAndGet())).audio(audio(interestingEvent)).timeStamp(java.lang.Long.valueOf(java.lang.System.currentTimeMillis())).build();
            default:
                return null;
        }
    }

    @com.squareup.otto.Subscribe
    public final void onInit(@org.jetbrains.annotations.NotNull com.navdy.hud.app.event.InitEvents.InitPhase event) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(event, "event");
        com.navdy.hud.app.event.InitEvents.Phase phase = event.phase;
        if (phase != null) {
            switch (phase) {
                case POST_START:
                    Companion.getSLogger().i("Post Start , Quiet mode : " + this.powerManager.inQuietMode());
                    if (!this.powerManager.inQuietMode()) {
                        startTelemetrySession();
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    @com.squareup.otto.Subscribe
    public final void onWakeup(@org.jetbrains.annotations.NotNull com.navdy.hud.app.event.Wakeup wakeUpEvent) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(wakeUpEvent, "wakeUpEvent");
        startTelemetrySession();
    }

    public final void startTelemetrySession() {
        if (!com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getSessionStarted()) {
            Companion.getSLogger().i("Starting telemetry session");
            com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.startSession();
            android.os.Handler handler2 = this.handler;
            kotlin.jvm.functions.Function0<kotlin.Unit> function0 = this.reportRunnable;
            handler2.removeCallbacks(function0 == null ? null : new com.navdy.hud.app.analytics.TelemetryDataManagerKt$sam$Runnable$cc6381d8(function0));
            android.os.Handler handler3 = this.handler;
            kotlin.jvm.functions.Function0<kotlin.Unit> function02 = this.reportRunnable;
            handler3.postDelayed(function02 == null ? null : new com.navdy.hud.app.analytics.TelemetryDataManagerKt$sam$Runnable$cc6381d8(function02), Companion.getANALYTICS_REPORTING_INTERVAL());
            this.handler.postDelayed(this.updateDriveScoreRunnable, Companion.getDRIVE_SCORE_PUBLISH_INTERVAL());
            return;
        }
        Companion.getSLogger().d("Telemetry session is already started");
    }

    @com.squareup.otto.Subscribe
    public final void GPSSpeedChangeEvent(@org.jetbrains.annotations.NotNull com.navdy.hud.app.maps.MapEvents.GPSSpeedEvent event) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(event, "event");
        updateSpeed();
    }

    @com.squareup.otto.Subscribe
    public final void onSpeedDataExpired(@org.jetbrains.annotations.NotNull com.navdy.hud.app.manager.SpeedManager.SpeedDataExpired speedDataExpired) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(speedDataExpired, "speedDataExpired");
        com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.speedSourceChanged();
        updateSpeed();
    }

    @com.squareup.otto.Subscribe
    public final void onGpsSwitched(@org.jetbrains.annotations.NotNull com.navdy.hud.app.device.gps.GpsUtils.GpsSwitch gpsSwitch) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(gpsSwitch, "gpsSwitch");
        Companion.getSLogger().d("onGpsSwitched " + gpsSwitch);
        com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.speedSourceChanged();
    }

    @com.squareup.otto.Subscribe
    public final void onMapEvent(@org.jetbrains.annotations.NotNull com.navdy.hud.app.maps.MapEvents.ManeuverDisplay event) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(event, "event");
        com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.setCurrentSpeedLimit(event.currentSpeedLimit);
    }

    @com.squareup.otto.Subscribe
    public final void onObdConnectionStateChanged(@org.jetbrains.annotations.NotNull com.navdy.hud.app.obd.ObdManager.ObdConnectionStatusEvent connectionStateChangeEvent) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(connectionStateChangeEvent, "connectionStateChangeEvent");
        com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.speedSourceChanged();
        if (connectionStateChangeEvent.connected) {
            com.navdy.obd.PidSet supportedPids = com.navdy.hud.app.obd.ObdManager.getInstance().getSupportedPids();
            this.highAccuracySpeedAvailable = true;
            java.lang.String troubleCodesString = this.sharedPreferences.getString(Companion.getPREFERENCE_TROUBLE_CODES(), "");
            java.util.List troubleCodes = com.navdy.hud.app.obd.ObdManager.getInstance().getTroubleCodes();
            if (kotlin.jvm.internal.Intrinsics.areEqual((java.lang.Object) troubleCodes, (java.lang.Object) null)) {
                this.sharedPreferences.edit().putString(Companion.getPREFERENCE_TROUBLE_CODES(), "").apply();
                com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.setSessionTroubleCodesReport("");
                return;
            }
            com.navdy.hud.app.analytics.TelemetryDataManager.Companion companion = Companion;
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(troubleCodesString, "troubleCodesString");
            kotlin.Pair serializeTroubleCodes = companion.serializeTroubleCodes(troubleCodes, troubleCodesString);
            java.lang.String str = (java.lang.String) serializeTroubleCodes.component1();
            java.lang.String str2 = (java.lang.String) serializeTroubleCodes.component2();
            this.sharedPreferences.edit().putString(Companion.getPREFERENCE_TROUBLE_CODES(), str).apply();
            com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.setSessionTroubleCodesReport(str2);
            return;
        }
        this.highAccuracySpeedAvailable = false;
    }

    @com.squareup.otto.Subscribe
    public final void ObdPidChangeEvent(@org.jetbrains.annotations.NotNull com.navdy.hud.app.obd.ObdManager.ObdPidChangeEvent event) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(event, "event");
        switch (event.pid.getId()) {
            case 12:
                com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.setCurrentRpm((int) event.pid.getValue());
                return;
            case 13:
                updateSpeed();
                return;
            case 256:
                com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.setCurrentMpg(com.navdy.hud.app.util.ConversionUtil.convertLpHundredKmToMPG(event.pid.getValue()));
                return;
            default:
                return;
        }
    }

    @com.squareup.otto.Subscribe
    public final void onCalibratedGForceData(@org.jetbrains.annotations.NotNull com.navdy.hud.app.device.gps.CalibratedGForceData calibratedGForceData) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(calibratedGForceData, "calibratedGForceData");
        if (com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getSessionStarted()) {
            com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.setGForce(calibratedGForceData.getXAccel(), calibratedGForceData.getYAccel(), calibratedGForceData.getZAccel());
        }
    }

    private final void updateSpeed() {
        if (com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getSessionStarted()) {
            com.navdy.hud.app.analytics.TelemetrySession telemetrySession = com.navdy.hud.app.analytics.TelemetrySession.INSTANCE;
            com.navdy.hud.app.analytics.RawSpeed currentSpeedInMetersPerSecond = com.navdy.hud.app.manager.SpeedManager.getInstance().getCurrentSpeedInMetersPerSecond();
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(currentSpeedInMetersPerSecond, "SpeedManager.getInstance\u2026entSpeedInMetersPerSecond");
            telemetrySession.setCurrentSpeed(currentSpeedInMetersPerSecond);
        }
    }

    /* access modifiers changed from: private */
    public final void reportTelemetryData() {
        if (Companion.getLOG_TELEMETRY_DATA()) {
            Companion.getSLogger().d(("Distance = " + com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getSessionDistance() + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA) + ("Duration = " + com.navdy.hud.app.analytics.TelemetrySessionKt.milliSecondsToSeconds(com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getSessionDuration()) + ", ") + ("RollingDuration = " + com.navdy.hud.app.analytics.TelemetrySessionKt.milliSecondsToSeconds(com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getSessionRollingDuration()) + ", ") + ("AverageSpeed = " + com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getSessionAverageSpeed() + ", ") + ("AverageRollingSpeed = " + com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getSessionAverageRollingSpeed() + ", ") + ("MaxSpeed = " + com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getSessionMaxSpeed() + ", ") + ("HardBrakingCount = " + com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getSessionHardBrakingCount() + ", ") + ("HardAccelerationCount = " + com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getSessionHardAccelerationCount() + ", ") + ("SpeedingPercentage = " + com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getSessionSpeedingPercentage() + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA) + ("TroubleCodeCount = " + com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getSessionTroubleCodeCount() + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA) + ("TroubleCodesReport = " + com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getSessionTroubleCodesReport()) + ("MaxRPM = " + com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getMaxRpm() + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA));
            Companion.getSLogger().d(("EVENT , averageMpg = " + com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getEventAverageMpg() + com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.COMMA) + ("averageRPM = " + com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getEventAverageRpm() + ", ") + ("Fuel level = " + com.navdy.hud.app.obd.ObdManager.getInstance().getFuelLevel() + ", ") + ("Engine Temperature = " + com.navdy.hud.app.obd.ObdManager.getInstance().getPidValue(5) + ", "));
        }
        com.navdy.hud.app.analytics.AnalyticsSupport.recordVehicleTelemetryData(com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getSessionDistance(), com.navdy.hud.app.analytics.TelemetrySessionKt.milliSecondsToSeconds(com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getSessionDuration()), com.navdy.hud.app.analytics.TelemetrySessionKt.milliSecondsToSeconds(com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getSessionRollingDuration()), com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getSessionAverageSpeed(), com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getSessionAverageRollingSpeed(), com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getSessionMaxSpeed(), com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getSessionHardBrakingCount(), com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getSessionHardAccelerationCount(), com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getSessionSpeedingPercentage(), com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getSessionExcessiveSpeedingPercentage(), com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getSessionTroubleCodeCount(), com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getSessionTroubleCodesReport(), (int) com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getSessionAverageMpg(), com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getMaxRpm(), (int) com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getEventAverageMpg(), com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getEventAverageRpm(), com.navdy.hud.app.obd.ObdManager.getInstance().getFuelLevel(), (float) com.navdy.hud.app.obd.ObdManager.getInstance().getPidValue(5), com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getMaxG(), com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getMaxGAngle(), com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getSessionHighGCount());
        com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.eventReported();
        android.os.Handler handler2 = this.handler;
        kotlin.jvm.functions.Function0<kotlin.Unit> function0 = this.reportRunnable;
        handler2.postDelayed(function0 == null ? null : new com.navdy.hud.app.analytics.TelemetryDataManagerKt$sam$Runnable$cc6381d8(function0), Companion.getANALYTICS_REPORTING_INTERVAL());
    }

    @org.jetbrains.annotations.Nullable
    public final com.navdy.service.library.events.audio.Audio audio(@org.jetbrains.annotations.NotNull com.navdy.hud.app.analytics.TelemetrySession.InterestingEvent $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        switch ($receiver) {
            case HARD_BRAKING:
                return com.navdy.service.library.events.audio.Audio.DRIVE_BEHAVIOR_HARD_BRAKING;
            case HARD_ACCELERATION:
                return com.navdy.service.library.events.audio.Audio.DRIVE_BEHAVIOR_HARD_ACCELERATION;
            case HIGH_G_STARTED:
                return com.navdy.service.library.events.audio.Audio.DRIVE_BEHAVIOR_HIGH_G;
            case SPEEDING_STARTED:
                return com.navdy.service.library.events.audio.Audio.DRIVE_BEHAVIOR_SPEEDING;
            case EXCESSIVE_SPEEDING_STARTED:
                return com.navdy.service.library.events.audio.Audio.DRIVE_BEHAVIOR_EXCESSIVE_SPEEDING;
            default:
                return null;
        }
    }
}
