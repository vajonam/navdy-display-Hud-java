package com.navdy.hud.app.presenter;

@javax.inject.Singleton
public class NotificationPresenter extends com.navdy.hud.app.ui.framework.BasePresenter<com.navdy.hud.app.view.NotificationView> {
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.presenter.NotificationPresenter.class);
    @javax.inject.Inject
    com.squareup.otto.Bus bus;

    public void onLoad(android.os.Bundle savedInstanceState) {
        super.onLoad(savedInstanceState);
        this.bus.register(this);
    }
}
