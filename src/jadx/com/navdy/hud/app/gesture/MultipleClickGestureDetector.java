package com.navdy.hud.app.gesture;

public class MultipleClickGestureDetector implements com.navdy.hud.app.manager.InputManager.IInputHandler {
    public static final int DOUBLE_CLICK = 2;
    private static final int MULTIPLE_CLICK_THRESHOLD = 400;
    private static final com.navdy.hud.app.manager.InputManager.CustomKeyEvent MULTIPLIABLE_EVENT = com.navdy.hud.app.manager.InputManager.CustomKeyEvent.SELECT;
    public static final int TRIPLE_CLICK = 3;
    private final android.os.Handler handler = new android.os.Handler(android.os.Looper.getMainLooper());
    private com.navdy.hud.app.gesture.MultipleClickGestureDetector.IMultipleClickKeyGesture listener;
    private int maxAcceptableClickCount;
    private int multipleClickCount;
    private final java.lang.Runnable multipleClickTrigger = new com.navdy.hud.app.gesture.MultipleClickGestureDetector.Anon1();

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            com.navdy.hud.app.gesture.MultipleClickGestureDetector.this.processEvent(null);
        }
    }

    public interface IMultipleClickKeyGesture extends com.navdy.hud.app.manager.InputManager.IInputHandler {
        void onMultipleClick(int i);
    }

    public MultipleClickGestureDetector(int maxAcceptableClickCount2, com.navdy.hud.app.gesture.MultipleClickGestureDetector.IMultipleClickKeyGesture listener2) {
        if (listener2 == null || maxAcceptableClickCount2 <= 1) {
            throw new java.lang.IllegalArgumentException();
        }
        this.maxAcceptableClickCount = maxAcceptableClickCount2;
        this.listener = listener2;
    }

    public boolean onGesture(com.navdy.service.library.events.input.GestureEvent event) {
        return false;
    }

    public boolean onKey(com.navdy.hud.app.manager.InputManager.CustomKeyEvent event) {
        return processEvent(event);
    }

    public com.navdy.hud.app.manager.InputManager.IInputHandler nextHandler() {
        return null;
    }

    /* access modifiers changed from: private */
    public synchronized boolean processEvent(com.navdy.hud.app.manager.InputManager.CustomKeyEvent receivedEvent) {
        boolean z = false;
        synchronized (this) {
            this.handler.removeCallbacks(this.multipleClickTrigger);
            if (this.listener != null) {
                boolean isSelectEvent = MULTIPLIABLE_EVENT.equals(receivedEvent);
                if (isSelectEvent) {
                    this.multipleClickCount++;
                }
                while (this.multipleClickCount >= this.maxAcceptableClickCount) {
                    this.listener.onMultipleClick(this.maxAcceptableClickCount);
                    this.multipleClickCount -= this.maxAcceptableClickCount;
                }
                if (!isSelectEvent) {
                    if (this.multipleClickCount > 1) {
                        this.listener.onMultipleClick(this.multipleClickCount);
                    } else if (this.multipleClickCount == 1) {
                        this.listener.onKey(MULTIPLIABLE_EVENT);
                    }
                    this.multipleClickCount = 0;
                    if (receivedEvent != null) {
                        z = this.listener.onKey(receivedEvent);
                        switch (receivedEvent) {
                            case POWER_BUTTON_LONG_PRESS:
                            case LONG_PRESS:
                                break;
                        }
                    }
                } else if (this.multipleClickCount > 0) {
                    this.handler.postDelayed(this.multipleClickTrigger, 400);
                }
                z = true;
            }
        }
        return z;
    }
}
