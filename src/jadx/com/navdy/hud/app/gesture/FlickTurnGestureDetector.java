package com.navdy.hud.app.gesture;

public class FlickTurnGestureDetector extends com.navdy.hud.app.gesture.MultipleClickGestureDetector {
    private static final java.util.List<com.navdy.hud.app.manager.InputManager.CustomKeyEvent> FLICKABLE_EVENTS = new com.navdy.hud.app.gesture.FlickTurnGestureDetector.Anon1();
    private static final int FLICK_EVENT_COUNT = 4;
    private static final int FLICK_PAIRED_EVENT_TIMEOUT_MS = 500;
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.gesture.FlickTurnGestureDetector.class);
    private short consecutiveTurnEventCount = 0;
    private com.navdy.hud.app.gesture.FlickTurnGestureDetector.IFlickTurnKeyGesture listener;
    private com.navdy.hud.app.manager.InputManager.CustomKeyEvent previousKeyEvent = null;
    private long previousKeyEventTime = 0;

    static class Anon1 extends java.util.ArrayList {
        Anon1() {
            add(com.navdy.hud.app.manager.InputManager.CustomKeyEvent.LEFT);
            add(com.navdy.hud.app.manager.InputManager.CustomKeyEvent.RIGHT);
        }
    }

    public interface IFlickTurnKeyGesture extends com.navdy.hud.app.gesture.MultipleClickGestureDetector.IMultipleClickKeyGesture {
        void onFlickLeft();

        void onFlickRight();
    }

    public FlickTurnGestureDetector(com.navdy.hud.app.gesture.MultipleClickGestureDetector.IMultipleClickKeyGesture listener2) {
        super(2, listener2);
        if (listener2 == null || !(listener2 instanceof com.navdy.hud.app.gesture.FlickTurnGestureDetector.IFlickTurnKeyGesture)) {
            throw new java.lang.IllegalArgumentException();
        }
        this.listener = (com.navdy.hud.app.gesture.FlickTurnGestureDetector.IFlickTurnKeyGesture) listener2;
    }

    public boolean onGesture(com.navdy.service.library.events.input.GestureEvent event) {
        return false;
    }

    public boolean onKey(com.navdy.hud.app.manager.InputManager.CustomKeyEvent event) {
        boolean result = super.onKey(event);
        long time = java.lang.System.currentTimeMillis();
        if (!FLICKABLE_EVENTS.contains(event) || this.previousKeyEvent != event || time >= this.previousKeyEventTime + 500) {
            this.consecutiveTurnEventCount = 1;
        } else {
            short s = (short) (this.consecutiveTurnEventCount + 1);
            this.consecutiveTurnEventCount = s;
            if (s == 4) {
                sLogger.d("Combined key event recognized from a single " + event);
                this.consecutiveTurnEventCount = 0;
                if (com.navdy.hud.app.manager.InputManager.CustomKeyEvent.LEFT.equals(event)) {
                    this.listener.onFlickLeft();
                } else if (com.navdy.hud.app.manager.InputManager.CustomKeyEvent.RIGHT.equals(event)) {
                    this.listener.onFlickRight();
                } else {
                    throw new java.lang.UnsupportedOperationException("Flick for " + event + " not handled");
                }
            }
        }
        this.previousKeyEvent = event;
        this.previousKeyEventTime = time;
        return result;
    }

    public com.navdy.hud.app.manager.InputManager.IInputHandler nextHandler() {
        return null;
    }
}
