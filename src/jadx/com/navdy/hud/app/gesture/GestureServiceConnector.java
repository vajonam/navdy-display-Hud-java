package com.navdy.hud.app.gesture;

public class GestureServiceConnector {
    private static final java.lang.String AUTO_EXPOSURE_OFF = "autoexpo off";
    private static final java.lang.String AUTO_EXPOSURE_ON = "autoexpo on";
    private static final java.lang.String CALIBRATE_MASK = "calibrate-mask";
    private static final java.lang.String CALIBRATE_MASK_FORCE = "calibrate-mask-force";
    private static final java.lang.String CALIBRATE_SAVE_SNAPSHOT_ON = "calibrate-save-snapshot on";
    private static final java.lang.String DEV_SOCKET_SWIPED = "/dev/socket/swiped";
    private static final java.lang.String EXPOSURE = "exposure";
    private static final java.lang.String FORMAT = "format";
    private static final java.lang.String GAIN = "gain";
    public static final java.lang.String GESTURE_ENABLED = "gesture.enabled";
    public static final java.lang.String GESTURE_LEFT = "gesture left";
    public static final java.lang.String GESTURE_RIGHT = "gesture right";
    public static final java.lang.String GESTURE_VERSION = "persist.sys.gesture.version";
    public static final java.lang.String LEFT = "left";
    public static final char LEFT_FIRST_CHAR = 'l';
    public static final int MAX_RECORD_TIME = 6000;
    private static final int MINIMUM_INTERVAL_BETWEEN_SNAPSHOTS = 5000;
    private static final java.lang.String NOTIFY = "notify";
    private static final int PID_CHECK_INTERVAL = 10000;
    public static final java.lang.String RECORDING_SAVED = "recording-saved:";
    public static final java.lang.String RECORD_SAVE = "record save";
    public static final java.lang.String RIGHT = "right";
    public static final char RIGHT_FIRST_CHAR = 'r';
    public static final java.lang.String SNAPSHOT = "snapshot ";
    public static final java.lang.String SNAPSHOT_PATH = "/data/misc/swiped/camera.png";
    private static final java.lang.String SOCKET_NAME = "swiped";
    private static final int SO_TIMEOUT = 2000;
    public static final java.lang.String SWIPED_DISABLE_CALIBRATION = "calibration pause";
    public static final java.lang.String SWIPED_ENABLE_CALIBRATION = "calibration resume";
    private static final java.lang.String SWIPED_PROCESS_NAME = "/system/xbin/swiped";
    public static final java.lang.String SWIPED_RECORD_MODE_ONE_SHOT = "record-mode oneshot";
    public static final java.lang.String SWIPED_RECORD_MODE_ROLLING = "record-mode rolling";
    public static final java.lang.String SWIPED_START_RECORDING = "record on";
    public static final java.lang.String SWIPED_STOP_RECORDING = "record off";
    public static final java.lang.String SWIPE_PROGRESS = "swipe-progress:";
    public static final int SWIPE_PROGRESS_COMMAND_DATA_OFFSET = SWIPE_PROGRESS.length();
    public static final java.lang.String SWIPE_PROGRESS_LEFT = "swipe-progress: left,";
    public static final java.lang.String SWIPE_PROGRESS_RIGHT = "swipe-progress: right,";
    public static final java.lang.String SWIPE_PROGRESS_UNKNOWN = "swipe-progress: unknown,";
    public static final java.lang.String UNKNOWN = "unknown";
    private static final java.nio.charset.CharsetEncoder encoder = java.nio.charset.Charset.defaultCharset().newEncoder();
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.gesture.GestureServiceConnector.class);
    private static final byte[] temp = new byte[1024];
    private com.squareup.otto.Bus bus;
    private long lastSnapshotTime = 0;
    private com.navdy.hud.app.device.PowerManager powerManager;
    private java.lang.String recordingPath;
    /* access modifiers changed from: private */
    public volatile boolean running;
    private boolean shuttingDown;
    private volatile java.lang.Thread swipedReader;
    /* access modifiers changed from: private */
    public volatile android.net.LocalSocket swipedSocket;
    private java.lang.String vin = null;

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            while (com.navdy.hud.app.gesture.GestureServiceConnector.this.running) {
                try {
                    com.navdy.hud.app.gesture.GestureServiceConnector.sLogger.v("creating socket");
                    com.navdy.hud.app.gesture.GestureServiceConnector.this.swipedSocket = new android.net.LocalSocket(1);
                    com.navdy.hud.app.gesture.GestureServiceConnector.sLogger.v("bind socket");
                    com.navdy.hud.app.gesture.GestureServiceConnector.this.swipedSocket.bind(new android.net.LocalSocketAddress(""));
                    com.navdy.hud.app.gesture.GestureServiceConnector.sLogger.v("calling communicateWithSwipeDaemon");
                    com.navdy.hud.app.gesture.GestureServiceConnector.Error error = com.navdy.hud.app.gesture.GestureServiceConnector.this.communicateWithSwipeDaemon(com.navdy.hud.app.gesture.GestureServiceConnector.this.swipedSocket);
                    com.navdy.hud.app.gesture.GestureServiceConnector.sLogger.v("called communicateWithSwipeDaemon");
                    if (com.navdy.hud.app.gesture.GestureServiceConnector.this.running) {
                        com.navdy.hud.app.gesture.GestureServiceConnector.this.closeSocket();
                        switch (com.navdy.hud.app.gesture.GestureServiceConnector.Anon7.$SwitchMap$com$navdy$hud$app$gesture$GestureServiceConnector$Error[error.ordinal()]) {
                            case 1:
                                com.navdy.hud.app.gesture.GestureServiceConnector.sLogger.v("communicateWithSwipeDaemon  cannot connect");
                                java.lang.Thread.sleep(5000);
                                break;
                            case 2:
                                com.navdy.hud.app.gesture.GestureServiceConnector.sLogger.v("communicateWithSwipeDaemon  communication lost, restart");
                                com.navdy.hud.app.gesture.GestureServiceConnector.this.reStart();
                                com.navdy.hud.app.gesture.GestureServiceConnector.sLogger.v("swipedconnector connect thread exit");
                                return;
                            case 3:
                                com.navdy.hud.app.gesture.GestureServiceConnector.sLogger.v("communicateWithSwipeDaemon  swiped restarted, restart");
                                com.navdy.hud.app.gesture.GestureServiceConnector.this.reStart();
                                com.navdy.hud.app.gesture.GestureServiceConnector.sLogger.v("swipedconnector connect thread exit");
                                return;
                        }
                    }
                } catch (Throwable t) {
                    com.navdy.hud.app.gesture.GestureServiceConnector.sLogger.e(t);
                    if (com.navdy.hud.app.gesture.GestureServiceConnector.this.running) {
                        com.navdy.hud.app.util.GenericUtil.sleep(5000);
                    }
                } finally {
                    com.navdy.hud.app.gesture.GestureServiceConnector.sLogger.v("swipedconnector connect thread exit");
                }
            }
        }
    }

    class Anon2 implements java.lang.Runnable {
        Anon2() {
        }

        public void run() {
            com.navdy.hud.app.gesture.GestureServiceConnector.this.stop();
            com.navdy.hud.app.util.GenericUtil.sleep(2000);
            com.navdy.hud.app.gesture.GestureServiceConnector.this.start();
        }
    }

    class Anon3 implements java.lang.Runnable {
        final /* synthetic */ java.lang.String val$command;

        Anon3(java.lang.String str) {
            this.val$command = str;
        }

        public void run() {
            try {
                com.navdy.hud.app.gesture.GestureServiceConnector.this.sendCommand(this.val$command);
            } catch (Throwable t) {
                com.navdy.hud.app.gesture.GestureServiceConnector.sLogger.e(t);
            }
        }
    }

    class Anon4 implements java.lang.Runnable {
        Anon4() {
        }

        public void run() {
            com.navdy.hud.app.gesture.GestureServiceConnector.this.start();
        }
    }

    class Anon5 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.service.library.events.preferences.InputPreferences val$event;

        Anon5(com.navdy.service.library.events.preferences.InputPreferences inputPreferences) {
            this.val$event = inputPreferences;
        }

        public void run() {
            if (this.val$event.use_gestures.booleanValue()) {
                if (com.navdy.hud.app.gesture.GestureServiceConnector.this.swipedSocket != null) {
                    com.navdy.hud.app.gesture.GestureServiceConnector.sLogger.v("already running");
                } else {
                    com.navdy.hud.app.gesture.GestureServiceConnector.this.start();
                }
            } else if (com.navdy.hud.app.gesture.GestureServiceConnector.this.swipedSocket == null) {
                com.navdy.hud.app.gesture.GestureServiceConnector.sLogger.v("not running");
            } else {
                com.navdy.hud.app.gesture.GestureServiceConnector.this.stop();
            }
        }
    }

    class Anon6 implements java.lang.Runnable {
        Anon6() {
        }

        public void run() {
            com.navdy.hud.app.gesture.GestureServiceConnector.this.stop();
        }
    }

    static /* synthetic */ class Anon7 {
        static final /* synthetic */ int[] $SwitchMap$com$navdy$hud$app$gesture$GestureServiceConnector$Error = new int[com.navdy.hud.app.gesture.GestureServiceConnector.Error.values().length];

        static {
            try {
                $SwitchMap$com$navdy$hud$app$gesture$GestureServiceConnector$Error[com.navdy.hud.app.gesture.GestureServiceConnector.Error.CANNOT_CONNECT.ordinal()] = 1;
            } catch (java.lang.NoSuchFieldError e) {
            }
            try {
                $SwitchMap$com$navdy$hud$app$gesture$GestureServiceConnector$Error[com.navdy.hud.app.gesture.GestureServiceConnector.Error.COMMUNICATION_LOST.ordinal()] = 2;
            } catch (java.lang.NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$com$navdy$hud$app$gesture$GestureServiceConnector$Error[com.navdy.hud.app.gesture.GestureServiceConnector.Error.SWIPED_RESTARTED.ordinal()] = 3;
            } catch (java.lang.NoSuchFieldError e3) {
            }
        }
    }

    public static class ConnectedEvent {
    }

    private enum Error {
        CANNOT_CONNECT,
        COMMUNICATION_LOST,
        SWIPED_RESTARTED
    }

    public enum GestureDirection {
        UNKNOWN,
        LEFT,
        RIGHT
    }

    public static class GestureProgress {
        public com.navdy.hud.app.gesture.GestureServiceConnector.GestureDirection direction;
        public float progress;

        public GestureProgress(com.navdy.hud.app.gesture.GestureServiceConnector.GestureDirection direction2, float progress2) {
            this.direction = direction2;
            this.progress = progress2;
        }
    }

    public static class RecordingSaved {
        public java.lang.String path;

        public RecordingSaved(java.lang.String path2) {
            this.path = path2;
        }
    }

    public static class TakeSnapshot {
    }

    public GestureServiceConnector(com.squareup.otto.Bus bus2, com.navdy.hud.app.device.PowerManager powerManager2) {
        this.bus = bus2;
        this.powerManager = powerManager2;
        bus2.register(this);
    }

    public synchronized void start() {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        if (com.navdy.hud.app.util.DeviceUtil.isNavdyDevice()) {
            if (this.running) {
                sLogger.v("swipedconnector already running");
            } else {
                closeSocket();
                closeThread();
                if (this.powerManager.inQuietMode()) {
                    sLogger.v("Not starting gesture engine due to quiet mode");
                } else {
                    this.running = true;
                    com.navdy.hud.app.util.os.SystemProperties.set(GESTURE_ENABLED, com.navdy.hud.app.util.os.SystemProperties.get(GESTURE_VERSION, com.navdy.hud.app.util.DeviceUtil.isUserBuild() ? com.navdy.hud.app.framework.toast.ToastPresenter.EXTRA_MAIN_TITLE : "beta"));
                    this.swipedReader = new java.lang.Thread(new com.navdy.hud.app.gesture.GestureServiceConnector.Anon1());
                    this.swipedReader.setName("SwipedReaderThread");
                    this.swipedReader.start();
                    sLogger.v("swipedconnector connect thread started");
                }
            }
        }
    }

    public synchronized void stop() {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        if (com.navdy.hud.app.util.DeviceUtil.isNavdyDevice()) {
            if (this.running) {
                this.running = false;
                com.navdy.hud.app.util.os.SystemProperties.set(GESTURE_ENABLED, "0");
                closeSocket();
                closeThread();
            }
        }
    }

    /* access modifiers changed from: private */
    public void reStart() {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.gesture.GestureServiceConnector.Anon2(), 1);
    }

    /* access modifiers changed from: private */
    public com.navdy.hud.app.gesture.GestureServiceConnector.Error communicateWithSwipeDaemon(android.net.LocalSocket sock) {
        int swipedPid;
        long lastPidCheckTime;
        boolean connected = false;
        try {
            sock.connect(new android.net.LocalSocketAddress(SOCKET_NAME, android.net.LocalSocketAddress.Namespace.RESERVED));
            sendCommand(NOTIFY);
            connected = true;
            sendCommand(SWIPED_ENABLE_CALIBRATION);
            sendCommand(SWIPED_STOP_RECORDING);
            sendCommand(SWIPED_RECORD_MODE_ROLLING);
        } catch (Throwable th) {
            sLogger.d("socket connect to filesystem namespace failed", e);
        }
        if (!this.running) {
            sLogger.v("swipedconnector is not running");
            return com.navdy.hud.app.gesture.GestureServiceConnector.Error.CANNOT_CONNECT;
        } else if (!connected) {
            sLogger.v("swipedconnector could not connect");
            return com.navdy.hud.app.gesture.GestureServiceConnector.Error.CANNOT_CONNECT;
        } else {
            try {
                swipedPid = com.navdy.service.library.util.SystemUtils.getNativeProcessId(SWIPED_PROCESS_NAME);
                sLogger.v("swipedconnector connected");
                this.bus.post(new com.navdy.hud.app.gesture.GestureServiceConnector.ConnectedEvent());
                java.io.InputStream ins = sock.getInputStream();
                sLogger.d("read starting");
                byte[] buf = new byte[1000];
                sLogger.v("setting timeout");
                this.swipedSocket.setSoTimeout(2000);
                sLogger.v("set timeout");
                lastPidCheckTime = android.os.SystemClock.elapsedRealtime();
                sLogger.v("swipedconnector pid  [" + swipedPid + "]");
                while (this.running) {
                    int br = ins.read(buf);
                    if (br < 0) {
                        sLogger.v("swipedconnector closed the socket");
                        return com.navdy.hud.app.gesture.GestureServiceConnector.Error.COMMUNICATION_LOST;
                    } else if (br > 1) {
                        java.lang.String str = new java.lang.String(buf, 0, br - 1);
                        char c = 65535;
                        switch (str.hashCode()) {
                            case -1253681019:
                                if (str.equals(GESTURE_RIGHT)) {
                                    c = 1;
                                }
                            case 2037586046:
                                if (str.equals(GESTURE_LEFT)) {
                                    c = 0;
                                }
                        }
                        switch (c) {
                            case 0:
                                this.bus.post(new com.navdy.service.library.events.input.GestureEvent(com.navdy.service.library.events.input.Gesture.GESTURE_SWIPE_LEFT, java.lang.Integer.valueOf(0), java.lang.Integer.valueOf(0)));
                                break;
                            case 1:
                                this.bus.post(new com.navdy.service.library.events.input.GestureEvent(com.navdy.service.library.events.input.Gesture.GESTURE_SWIPE_RIGHT, java.lang.Integer.valueOf(0), java.lang.Integer.valueOf(0)));
                                break;
                            default:
                                if (!str.startsWith(SWIPE_PROGRESS) || str.length() <= SWIPE_PROGRESS_COMMAND_DATA_OFFSET + 1) {
                                    if (!str.startsWith(RECORDING_SAVED)) {
                                        if (!str.startsWith("swiped event: ")) {
                                            break;
                                        } else {
                                            java.lang.String[] parts = str.split(" ");
                                            if (parts.length < 3) {
                                                break;
                                            } else {
                                                java.lang.String eventName = parts[2];
                                                java.util.List<java.lang.String> args = new java.util.ArrayList<>();
                                                for (int i = 3; i < parts.length; i++) {
                                                    java.lang.String[] parts2 = parts[i].split("=");
                                                    java.lang.String key = parts2[0];
                                                    java.lang.String value = parts2[1];
                                                    args.add(key);
                                                    args.add(value);
                                                }
                                                com.navdy.hud.app.analytics.AnalyticsSupport.recordSwipedCalibration(eventName, (java.lang.String[]) args.toArray(new java.lang.String[args.size()]));
                                                break;
                                            }
                                        }
                                    } else {
                                        this.recordingPath = str.substring(RECORDING_SAVED.length() + 1);
                                        sLogger.i("recording saved at " + this.recordingPath);
                                        synchronized (this) {
                                            notify();
                                        }
                                        this.bus.post(new com.navdy.hud.app.gesture.GestureServiceConnector.RecordingSaved(this.recordingPath));
                                        break;
                                    }
                                } else {
                                    switch (str.charAt(SWIPE_PROGRESS_COMMAND_DATA_OFFSET + 1)) {
                                        case 'l':
                                            try {
                                                float progress = java.lang.Float.valueOf(str.substring(SWIPE_PROGRESS_LEFT.length())).floatValue();
                                                com.squareup.otto.Bus bus2 = this.bus;
                                                com.navdy.hud.app.gesture.GestureServiceConnector.GestureProgress gestureProgress = new com.navdy.hud.app.gesture.GestureServiceConnector.GestureProgress(com.navdy.hud.app.gesture.GestureServiceConnector.GestureDirection.LEFT, progress);
                                                bus2.post(gestureProgress);
                                                break;
                                            } catch (Throwable t) {
                                                sLogger.e("Error parsing the progress " + str, t);
                                                break;
                                            }
                                        case 'r':
                                            try {
                                                float progress2 = java.lang.Float.valueOf(str.substring(SWIPE_PROGRESS_RIGHT.length())).floatValue();
                                                com.squareup.otto.Bus bus3 = this.bus;
                                                com.navdy.hud.app.gesture.GestureServiceConnector.GestureProgress gestureProgress2 = new com.navdy.hud.app.gesture.GestureServiceConnector.GestureProgress(com.navdy.hud.app.gesture.GestureServiceConnector.GestureDirection.RIGHT, progress2);
                                                bus3.post(gestureProgress2);
                                                break;
                                            } catch (Throwable t2) {
                                                sLogger.e("Error parsing the progress " + str, t2);
                                                break;
                                            }
                                    }
                                }
                                break;
                        }
                    } else {
                        continue;
                    }
                }
            } catch (java.io.IOException io) {
                if (this.running) {
                    if ("Try again".equals(io.getMessage())) {
                        long clockTime = android.os.SystemClock.elapsedRealtime();
                        if (clockTime - lastPidCheckTime > 10000) {
                            int pid = com.navdy.service.library.util.SystemUtils.getNativeProcessId(SWIPED_PROCESS_NAME);
                            if (pid != swipedPid) {
                                sLogger.e("swipedconnector pid has changed from [" + swipedPid + "] to [" + pid + "]");
                                return com.navdy.hud.app.gesture.GestureServiceConnector.Error.SWIPED_RESTARTED;
                            }
                            lastPidCheckTime = clockTime;
                        } else {
                            continue;
                        }
                    }
                }
            } catch (Throwable e) {
                sLogger.e("swipedconnector", e);
            }
            return com.navdy.hud.app.gesture.GestureServiceConnector.Error.COMMUNICATION_LOST;
        }
    }

    public synchronized void sendCommand(java.lang.String command) throws java.io.IOException {
        if (this.swipedSocket != null) {
            java.nio.ByteBuffer out = java.nio.ByteBuffer.wrap(temp);
            encoder.encode(java.nio.CharBuffer.wrap(command), out, true);
            out.put(0);
            this.swipedSocket.getOutputStream().write(temp, 0, out.position());
        }
    }

    public void sendCommandAsync(java.lang.String command) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.gesture.GestureServiceConnector.Anon3(command), 10);
    }

    public void sendCommand(java.lang.Object... args) throws java.io.IOException {
        if (args != null && args.length > 0) {
            java.lang.StringBuilder builder = new java.lang.StringBuilder();
            for (java.lang.Object arg : args) {
                if (arg != null) {
                    if (arg == args[args.length - 1]) {
                        builder.append(arg.toString());
                    } else {
                        builder.append(arg.toString() + " ");
                    }
                }
            }
            sendCommand(builder.toString());
        }
    }

    public void takeSnapShot(java.lang.String absolutePath) throws java.io.IOException {
        long time = android.os.SystemClock.elapsedRealtime();
        if (time - this.lastSnapshotTime > 5000) {
            this.lastSnapshotTime = time;
            sendCommand(SNAPSHOT + absolutePath);
        }
    }

    public void setRecordMode(boolean oneShot) {
        sLogger.d("setRecordMode " + oneShot);
        try {
            sendCommand(oneShot ? SWIPED_RECORD_MODE_ONE_SHOT : SWIPED_RECORD_MODE_ROLLING);
        } catch (java.io.IOException e) {
            sLogger.e("Exception while setting the record mode ", e);
        }
    }

    public void setCalibrationEnabled(boolean enabled) {
        try {
            sendCommand(enabled ? SWIPED_ENABLE_CALIBRATION : SWIPED_DISABLE_CALIBRATION);
        } catch (java.io.IOException e) {
            sLogger.e("Exception while setting calibration state ", e);
        }
    }

    public void startRecordingVideo() {
        try {
            sLogger.d("startRecordingVideo");
            sendCommand(SWIPED_START_RECORDING);
        } catch (java.io.IOException e) {
        }
    }

    public void stopRecordingVideo() {
        sLogger.d("stopRecordingVideo");
        try {
            sendCommand(SWIPED_STOP_RECORDING);
        } catch (java.io.IOException e) {
        }
    }

    public synchronized java.lang.String dumpRecording() {
        java.lang.String str;
        try {
            sendCommand(RECORD_SAVE);
            wait(6000);
            str = this.recordingPath;
        } catch (java.io.IOException | java.lang.InterruptedException e) {
            str = null;
        }
        return str;
    }

    public boolean isRunning() {
        return this.swipedSocket != null;
    }

    public void setDiscreteMode(boolean discreteMode) {
    }

    public void enablePreview(boolean on) {
    }

    @com.squareup.otto.Subscribe
    public void ObdStateChangeEvent(com.navdy.hud.app.obd.ObdManager.ObdConnectionStatusEvent event) {
        if (com.navdy.hud.app.util.DeviceUtil.isNavdyDevice() && event.connected) {
            java.lang.String newVin = com.navdy.hud.app.obd.ObdManager.getInstance().getVin();
            if (newVin != null && !newVin.equals(this.vin)) {
                this.vin = newVin;
                sendCommandAsync("set-id " + this.vin);
            }
        }
    }

    @com.squareup.otto.Subscribe
    public void onWakeup(com.navdy.hud.app.event.Wakeup event) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.gesture.GestureServiceConnector.Anon4(), 1);
    }

    @com.squareup.otto.Subscribe
    public void onDriverProfileChanged(com.navdy.hud.app.event.DriverProfileChanged event) {
        if (com.navdy.hud.app.util.DeviceUtil.isNavdyDevice()) {
            com.navdy.service.library.events.preferences.InputPreferences inputPreferences = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile().getInputPreferences();
            if (inputPreferences != null) {
                onInputPreferenceUpdate(inputPreferences);
            }
        }
    }

    @com.squareup.otto.Subscribe
    public void onTakeSnapshot(com.navdy.hud.app.gesture.GestureServiceConnector.TakeSnapshot takeSnapshot) {
        if (com.navdy.hud.app.util.DeviceUtil.isNavdyDevice()) {
            sLogger.d("Request to take snap shot");
            try {
                takeSnapShot(SNAPSHOT_PATH);
            } catch (java.io.IOException e) {
                sLogger.e("Exception while taking the snapshot ", e);
            }
        }
    }

    @com.squareup.otto.Subscribe
    public void onInputPreferenceUpdate(com.navdy.service.library.events.preferences.InputPreferences event) {
        if (com.navdy.hud.app.util.DeviceUtil.isNavdyDevice() && !this.shuttingDown && !this.powerManager.inQuietMode()) {
            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.gesture.GestureServiceConnector.Anon5(event), 1);
        }
    }

    @com.squareup.otto.Subscribe
    public void onShutdown(com.navdy.hud.app.event.Shutdown event) {
        if (event.state == com.navdy.hud.app.event.Shutdown.State.CONFIRMED) {
            this.shuttingDown = true;
            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.gesture.GestureServiceConnector.Anon6(), 1);
        }
    }

    /* access modifiers changed from: private */
    public void closeSocket() {
        if (this.swipedSocket != null) {
            java.io.InputStream ip = null;
            try {
                ip = this.swipedSocket.getInputStream();
            } catch (Throwable th) {
            }
            com.navdy.service.library.util.IOUtils.closeStream(ip);
            java.io.OutputStream op = null;
            try {
                op = this.swipedSocket.getOutputStream();
            } catch (Throwable th2) {
            }
            com.navdy.service.library.util.IOUtils.closeStream(op);
            com.navdy.service.library.util.IOUtils.closeStream(this.swipedSocket);
            this.swipedSocket = null;
            sLogger.v("swipedconnector socket closed");
        }
    }

    private void closeThread() {
        if (this.swipedReader != null) {
            if (this.swipedReader.isAlive()) {
                sLogger.v("swipedconnector thread alive");
                this.swipedReader.interrupt();
                sLogger.v("swipedconnector thread waiting");
                try {
                    this.swipedReader.join();
                } catch (Throwable th) {
                }
                sLogger.v("swipedconnector thread waited");
            }
            this.swipedReader = null;
        }
    }
}
