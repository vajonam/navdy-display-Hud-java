package com.navdy.hud.app.audio;

public class SoundUtils {
    private static boolean initialized;
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.audio.SoundUtils.class);
    private static java.util.HashMap<com.navdy.hud.app.audio.SoundUtils.Sound, java.lang.Integer> soundIdMap = new java.util.HashMap<>();
    private static android.media.SoundPool soundPool;

    public enum Sound {
        MENU_MOVE,
        MENU_SELECT,
        STARTUP,
        SHUTDOWN,
        ALERT_POSITIVE,
        ALERT_NEGATIVE
    }

    public static void init() {
        if (!initialized) {
            com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
            initialized = true;
            android.content.Context context = com.navdy.hud.app.HudApplication.getAppContext();
            soundPool = new android.media.SoundPool.Builder().setAudioAttributes(new android.media.AudioAttributes.Builder().setUsage(13).setContentType(4).build()).build();
            soundIdMap.put(com.navdy.hud.app.audio.SoundUtils.Sound.MENU_MOVE, java.lang.Integer.valueOf(soundPool.load(context, com.navdy.hud.app.R.raw.sound_menu_move, 1)));
            soundIdMap.put(com.navdy.hud.app.audio.SoundUtils.Sound.MENU_SELECT, java.lang.Integer.valueOf(soundPool.load(context, com.navdy.hud.app.R.raw.sound_menu_select, 1)));
            soundIdMap.put(com.navdy.hud.app.audio.SoundUtils.Sound.STARTUP, java.lang.Integer.valueOf(soundPool.load(context, com.navdy.hud.app.R.raw.sound_startup, 1)));
            soundIdMap.put(com.navdy.hud.app.audio.SoundUtils.Sound.SHUTDOWN, java.lang.Integer.valueOf(soundPool.load(context, com.navdy.hud.app.R.raw.sound_shutdown, 1)));
            soundIdMap.put(com.navdy.hud.app.audio.SoundUtils.Sound.ALERT_POSITIVE, java.lang.Integer.valueOf(soundPool.load(context, com.navdy.hud.app.R.raw.sound_alert_positive, 1)));
            soundIdMap.put(com.navdy.hud.app.audio.SoundUtils.Sound.ALERT_NEGATIVE, java.lang.Integer.valueOf(soundPool.load(context, com.navdy.hud.app.R.raw.sound_alert_negative, 1)));
            sLogger.v("sound pool created");
        }
    }

    public static void playSound(com.navdy.hud.app.audio.SoundUtils.Sound sound) {
        try {
            if (initialized) {
                java.lang.Integer soundId = (java.lang.Integer) soundIdMap.get(sound);
                if (soundId == null) {
                    sLogger.v("soundId not found:" + sound);
                } else {
                    soundPool.play(soundId.intValue(), 1.0f, 1.0f, 0, 0, 1.0f);
                }
            }
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }
}
