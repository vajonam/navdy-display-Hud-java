package com.navdy.hud.app.event;

public class Wakeup {
    public com.navdy.hud.app.analytics.AnalyticsSupport.WakeupReason reason;

    public Wakeup(com.navdy.hud.app.analytics.AnalyticsSupport.WakeupReason reason2) {
        this.reason = reason2;
    }
}
