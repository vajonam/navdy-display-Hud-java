package com.navdy.hud.app.event;

public class DeviceInfoAvailable {
    public final com.navdy.service.library.events.DeviceInfo deviceInfo;

    public DeviceInfoAvailable(com.navdy.service.library.events.DeviceInfo deviceInfo2) {
        this.deviceInfo = deviceInfo2;
    }
}
