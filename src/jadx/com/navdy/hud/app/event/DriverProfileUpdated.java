package com.navdy.hud.app.event;

public class DriverProfileUpdated {
    public com.navdy.hud.app.event.DriverProfileUpdated.State state;

    public enum State {
        UP_TO_DATE,
        UPDATED
    }

    public DriverProfileUpdated(com.navdy.hud.app.event.DriverProfileUpdated.State state2) {
        this.state = state2;
    }
}
