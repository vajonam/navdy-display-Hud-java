package com.navdy.hud.app.event;

public class RemoteEvent {
    protected com.squareup.wire.Message message;

    public RemoteEvent() {
    }

    public RemoteEvent(com.squareup.wire.Message message2) {
        this.message = message2;
    }

    public com.squareup.wire.Message getMessage() {
        return this.message;
    }

    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        com.navdy.hud.app.event.RemoteEvent that = (com.navdy.hud.app.event.RemoteEvent) o;
        if (this.message != null) {
            if (this.message.equals(that.message)) {
                return true;
            }
        } else if (that.message == null) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        if (this.message != null) {
            return this.message.hashCode();
        }
        return 0;
    }
}
