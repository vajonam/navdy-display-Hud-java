package com.navdy.hud.app.event;

public class ShowScreenWithArgs {
    public android.os.Bundle args;
    public java.lang.Object args2;
    public boolean ignoreAnimation;
    public com.navdy.service.library.events.ui.Screen screen;

    public ShowScreenWithArgs(com.navdy.service.library.events.ui.Screen screen2, android.os.Bundle args3, boolean ignoreAnimation2) {
        this(screen2, args3, null, ignoreAnimation2);
    }

    public ShowScreenWithArgs(com.navdy.service.library.events.ui.Screen screen2, android.os.Bundle args3, java.lang.Object args22, boolean ignoreAnimation2) {
        this.screen = screen2;
        this.args = args3;
        this.args2 = args22;
        this.ignoreAnimation = ignoreAnimation2;
    }
}
