package com.navdy.hud.app.event;

public class DisplayScaleChange {
    public final com.navdy.service.library.events.preferences.DriverProfilePreferences.DisplayFormat format;

    public DisplayScaleChange(com.navdy.service.library.events.preferences.DriverProfilePreferences.DisplayFormat format2) {
        this.format = format2;
    }
}
