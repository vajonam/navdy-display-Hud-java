package com.navdy.hud.app.event;

public class DrivingStateChange {
    public final boolean driving;

    public DrivingStateChange(boolean driving2) {
        this.driving = driving2;
    }
}
