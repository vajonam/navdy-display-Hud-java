package com.navdy.hud.app.event;

public class InitEvents {

    public static class BluetoothStateChanged {
        public final boolean enabled;

        public BluetoothStateChanged(boolean enabled2) {
            this.enabled = enabled2;
        }
    }

    public static class ConnectionServiceStarted {
    }

    public static class InitPhase {
        public final com.navdy.hud.app.event.InitEvents.Phase phase;

        public InitPhase(com.navdy.hud.app.event.InitEvents.Phase phase2) {
            this.phase = phase2;
        }
    }

    public enum Phase {
        EARLY,
        PRE_USER_INTERACTION,
        CONNECTION_SERVICE_STARTED,
        POST_START,
        LATE,
        SWITCHING_LOCALE,
        LOCALE_UP_TO_DATE
    }
}
