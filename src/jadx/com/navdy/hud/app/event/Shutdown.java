package com.navdy.hud.app.event;

public class Shutdown {
    public static final java.lang.String EXTRA_SHUTDOWN_CAUSE = "SHUTDOWN_CAUSE";
    public final com.navdy.hud.app.event.Shutdown.Reason reason;
    public final com.navdy.hud.app.event.Shutdown.State state;

    public enum Reason {
        UNKNOWN("Unknown", false),
        CRITICAL_VOLTAGE("Critical_Voltage", false),
        LOW_VOLTAGE("Low_Voltage", false),
        HIGH_TEMPERATURE("High_Temperature", false),
        TIMEOUT("Timeout", false),
        OTA("OTA", false),
        DIAL_OTA("Dial_OTA", false),
        FORCED_UPDATE("Forced_Update", false),
        FACTORY_RESET("Factory_Reset", false),
        POWER_LOSS("Power_Loss", false),
        MENU("Menu", true),
        POWER_BUTTON("Power_Button", true),
        ENGINE_OFF("Engine_Off", true),
        ACCELERATE_SHUTDOWN("Accelerate_Shutdown", true),
        INACTIVITY("Inactivity", true);
        
        public final java.lang.String attr;
        public final boolean requireConfirmation;

        private Reason(java.lang.String attr2, boolean requireConfirmation2) {
            this.attr = attr2;
            this.requireConfirmation = requireConfirmation2;
        }

        public android.os.Bundle asBundle() {
            android.os.Bundle b = new android.os.Bundle();
            b.putString(com.navdy.hud.app.event.Shutdown.EXTRA_SHUTDOWN_CAUSE, toString());
            return b;
        }
    }

    public enum State {
        CONFIRMATION,
        CANCELED,
        CONFIRMED,
        SHUTTING_DOWN
    }

    public Shutdown(com.navdy.hud.app.event.Shutdown.Reason reason2) {
        this.reason = reason2;
        this.state = reason2.requireConfirmation ? com.navdy.hud.app.event.Shutdown.State.CONFIRMATION : com.navdy.hud.app.event.Shutdown.State.CONFIRMED;
    }

    public Shutdown(com.navdy.hud.app.event.Shutdown.Reason reason2, com.navdy.hud.app.event.Shutdown.State state2) {
        this.reason = reason2;
        this.state = state2;
    }

    public java.lang.String toString() {
        java.lang.StringBuilder sb = new java.lang.StringBuilder("Shutdown{");
        sb.append("reason=").append(this.reason);
        sb.append(", state=").append(this.state);
        sb.append('}');
        return sb.toString();
    }
}
