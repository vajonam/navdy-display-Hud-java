package com.navdy.hud.app.manager;

public final class SpeedManager$$InjectAdapter extends dagger.internal.Binding<com.navdy.hud.app.manager.SpeedManager> implements javax.inject.Provider<com.navdy.hud.app.manager.SpeedManager>, dagger.MembersInjector<com.navdy.hud.app.manager.SpeedManager> {
    private dagger.internal.Binding<com.squareup.otto.Bus> bus;
    private dagger.internal.Binding<com.navdy.hud.app.profile.DriverProfileManager> driverProfileManager;

    public SpeedManager$$InjectAdapter() {
        super("com.navdy.hud.app.manager.SpeedManager", "members/com.navdy.hud.app.manager.SpeedManager", false, com.navdy.hud.app.manager.SpeedManager.class);
    }

    public void attach(dagger.internal.Linker linker) {
        this.driverProfileManager = linker.requestBinding("com.navdy.hud.app.profile.DriverProfileManager", com.navdy.hud.app.manager.SpeedManager.class, getClass().getClassLoader());
        this.bus = linker.requestBinding("com.squareup.otto.Bus", com.navdy.hud.app.manager.SpeedManager.class, getClass().getClassLoader());
    }

    public void getDependencies(java.util.Set<dagger.internal.Binding<?>> set, java.util.Set<dagger.internal.Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.driverProfileManager);
        injectMembersBindings.add(this.bus);
    }

    public com.navdy.hud.app.manager.SpeedManager get() {
        com.navdy.hud.app.manager.SpeedManager result = new com.navdy.hud.app.manager.SpeedManager();
        injectMembers(result);
        return result;
    }

    public void injectMembers(com.navdy.hud.app.manager.SpeedManager object) {
        object.driverProfileManager = (com.navdy.hud.app.profile.DriverProfileManager) this.driverProfileManager.get();
        object.bus = (com.squareup.otto.Bus) this.bus.get();
    }
}
