package com.navdy.hud.app.manager;

public class InitManager {
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.manager.InitManager.class);
    /* access modifiers changed from: private */
    public com.squareup.otto.Bus bus;
    private java.lang.Runnable checkConnectionServiceReady = new com.navdy.hud.app.manager.InitManager.Anon1();
    /* access modifiers changed from: private */
    public com.navdy.hud.app.service.ConnectionHandler connectionHandler;
    /* access modifiers changed from: private */
    public android.os.Handler handler;

    class Anon1 implements java.lang.Runnable {
        public boolean notified;

        Anon1() {
        }

        public void run() {
            if (!this.notified && com.navdy.hud.app.manager.InitManager.this.connectionHandler.serviceConnected() && android.bluetooth.BluetoothAdapter.getDefaultAdapter().isEnabled()) {
                com.navdy.hud.app.manager.InitManager.sLogger.i("bt and connection service ready");
                this.notified = true;
                com.navdy.hud.app.manager.InitManager.this.bus.post(new com.navdy.hud.app.event.InitEvents.InitPhase(com.navdy.hud.app.event.InitEvents.Phase.CONNECTION_SERVICE_STARTED));
                com.navdy.hud.app.manager.InitManager.this.handler.post(new com.navdy.hud.app.manager.InitManager.PhaseEmitter(com.navdy.hud.app.event.InitEvents.Phase.POST_START));
                com.navdy.hud.app.manager.InitManager.this.handler.post(new com.navdy.hud.app.manager.InitManager.PhaseEmitter(com.navdy.hud.app.event.InitEvents.Phase.LATE));
            }
        }
    }

    private class PhaseEmitter implements java.lang.Runnable {
        private com.navdy.hud.app.event.InitEvents.Phase phase;

        public PhaseEmitter(com.navdy.hud.app.event.InitEvents.Phase phase2) {
            this.phase = phase2;
        }

        public void run() {
            com.navdy.hud.app.manager.InitManager.sLogger.i("Triggering init phase:" + this.phase);
            com.navdy.hud.app.manager.InitManager.this.bus.post(new com.navdy.hud.app.event.InitEvents.InitPhase(this.phase));
        }
    }

    public InitManager(com.squareup.otto.Bus bus2, com.navdy.hud.app.service.ConnectionHandler connectionHandler2) {
        this.bus = bus2;
        this.handler = new android.os.Handler(android.os.Looper.getMainLooper());
        this.bus.register(this);
        this.connectionHandler = connectionHandler2;
    }

    public void start() {
        this.handler.post(new com.navdy.hud.app.manager.InitManager.PhaseEmitter(com.navdy.hud.app.event.InitEvents.Phase.PRE_USER_INTERACTION));
        this.handler.post(this.checkConnectionServiceReady);
    }

    @com.squareup.otto.Subscribe
    public void onBluetoothChanged(com.navdy.hud.app.event.InitEvents.BluetoothStateChanged event) {
        this.handler.post(this.checkConnectionServiceReady);
    }

    @com.squareup.otto.Subscribe
    public void onConnectionService(com.navdy.hud.app.event.InitEvents.ConnectionServiceStarted event) {
        this.handler.post(this.checkConnectionServiceReady);
    }
}
