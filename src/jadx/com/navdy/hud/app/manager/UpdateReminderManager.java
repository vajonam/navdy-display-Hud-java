package com.navdy.hud.app.manager;

public class UpdateReminderManager {
    /* access modifiers changed from: private */
    public static long DIAL_CONNECT_CHECK_INTERVAL = java.util.concurrent.TimeUnit.MINUTES.toMillis(15);
    public static final java.lang.String DIAL_REMINDER_TIME = "dial_reminder_time";
    public static final java.lang.String EXTRA_UPDATE_REMINDER = "UPDATE_REMINDER";
    private static long INITIAL_REMINDER_INTERVAL = java.util.concurrent.TimeUnit.DAYS.toMillis(10);
    public static final java.lang.String OTA_REMINDER_TIME = "ota_reminder_time";
    /* access modifiers changed from: private */
    public static long REMINDER_INTERVAL = java.util.concurrent.TimeUnit.DAYS.toMillis(1);
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.manager.UpdateReminderManager.class);
    @javax.inject.Inject
    com.squareup.otto.Bus bus;
    private boolean dialUpdateAvailable = false;
    private android.os.Handler handler = new android.os.Handler(android.os.Looper.getMainLooper());
    private com.navdy.hud.app.manager.UpdateReminderManager.ReminderRunnable reminderRunnable = null;
    @javax.inject.Inject
    android.content.SharedPreferences sharedPreferences;
    private boolean timeAvailable = false;

    private class ReminderRunnable implements java.lang.Runnable {
        boolean isDialUpdate;

        ReminderRunnable(boolean isDial) {
            this.isDialUpdate = isDial;
        }

        public void run() {
            android.os.Bundle args = new android.os.Bundle();
            if (this.isDialUpdate) {
                com.navdy.hud.app.device.dial.DialManager dialManager = com.navdy.hud.app.device.dial.DialManager.getInstance();
                if (!dialManager.isDialConnected()) {
                    com.navdy.hud.app.manager.UpdateReminderManager.this.sharedPreferences.edit().putLong(com.navdy.hud.app.manager.UpdateReminderManager.DIAL_REMINDER_TIME, java.lang.System.currentTimeMillis() + com.navdy.hud.app.manager.UpdateReminderManager.DIAL_CONNECT_CHECK_INTERVAL).apply();
                } else if (dialManager.getDialFirmwareUpdater().getVersions().isUpdateAvailable()) {
                    args.putBoolean(com.navdy.hud.app.manager.UpdateReminderManager.EXTRA_UPDATE_REMINDER, true);
                    if (!com.navdy.hud.app.screen.DialUpdateProgressScreen.updateStarted) {
                        com.navdy.hud.app.manager.UpdateReminderManager.this.bus.post(new com.navdy.hud.app.event.ShowScreenWithArgs(com.navdy.service.library.events.ui.Screen.SCREEN_DIAL_UPDATE_CONFIRMATION, args, false));
                    }
                    com.navdy.hud.app.manager.UpdateReminderManager.this.sharedPreferences.edit().putLong(com.navdy.hud.app.manager.UpdateReminderManager.DIAL_REMINDER_TIME, java.lang.System.currentTimeMillis() + com.navdy.hud.app.manager.UpdateReminderManager.REMINDER_INTERVAL).apply();
                } else {
                    com.navdy.hud.app.manager.UpdateReminderManager.this.sharedPreferences.edit().remove(com.navdy.hud.app.manager.UpdateReminderManager.DIAL_REMINDER_TIME).apply();
                }
            } else if (com.navdy.hud.app.manager.UpdateReminderManager.this.isOTAUpdateRequired()) {
                args.putBoolean(com.navdy.hud.app.manager.UpdateReminderManager.EXTRA_UPDATE_REMINDER, true);
                com.navdy.hud.app.manager.UpdateReminderManager.this.bus.post(new com.navdy.hud.app.event.ShowScreenWithArgs(com.navdy.service.library.events.ui.Screen.SCREEN_OTA_CONFIRMATION, args, false));
                com.navdy.hud.app.manager.UpdateReminderManager.this.sharedPreferences.edit().putLong(com.navdy.hud.app.manager.UpdateReminderManager.OTA_REMINDER_TIME, java.lang.System.currentTimeMillis() + com.navdy.hud.app.manager.UpdateReminderManager.REMINDER_INTERVAL).apply();
            } else {
                com.navdy.hud.app.manager.UpdateReminderManager.this.sharedPreferences.edit().remove(com.navdy.hud.app.manager.UpdateReminderManager.OTA_REMINDER_TIME).apply();
            }
            com.navdy.hud.app.manager.UpdateReminderManager.this.scheduleReminder();
        }
    }

    public UpdateReminderManager(android.content.Context context) {
        mortar.Mortar.inject(context, this);
        this.bus.register(this);
    }

    /* access modifiers changed from: private */
    public boolean isOTAUpdateRequired() {
        return com.navdy.hud.app.util.OTAUpdateService.isUpdateAvailable();
    }

    /* access modifiers changed from: private */
    public void scheduleReminder() {
        if (this.timeAvailable) {
            clearReminder();
            long ota_time = this.sharedPreferences.getLong(OTA_REMINDER_TIME, 0);
            long dial_time = 0;
            if (this.dialUpdateAvailable) {
                dial_time = this.sharedPreferences.getLong(DIAL_REMINDER_TIME, 0);
            }
            long time = 0;
            boolean dialUpdate = false;
            if (ota_time != 0 && (dial_time == 0 || ota_time <= dial_time)) {
                time = ota_time;
            } else if (dial_time != 0) {
                time = dial_time;
                dialUpdate = true;
            }
            long curTime = java.lang.System.currentTimeMillis();
            if (time != 0) {
                long delta = time - curTime;
                if (dialUpdate && delta < java.util.concurrent.TimeUnit.SECONDS.toMillis(1)) {
                    delta = java.util.concurrent.TimeUnit.SECONDS.toMillis(1);
                } else if (delta < 0) {
                    delta = 0;
                }
                com.navdy.service.library.log.Logger logger = sLogger;
                java.lang.String str = "new scheduling %s reminder for +%d seconds.";
                java.lang.Object[] objArr = new java.lang.Object[2];
                objArr[0] = dialUpdate ? "dial" : "OTA";
                objArr[1] = java.lang.Long.valueOf(java.util.concurrent.TimeUnit.MILLISECONDS.toSeconds(delta));
                logger.v(java.lang.String.format(str, objArr));
                this.reminderRunnable = new com.navdy.hud.app.manager.UpdateReminderManager.ReminderRunnable(dialUpdate);
                this.handler.postDelayed(this.reminderRunnable, delta);
            }
        }
    }

    private void clearReminder() {
        if (this.reminderRunnable != null) {
            this.handler.removeCallbacks(this.reminderRunnable);
            this.reminderRunnable = null;
        }
    }

    @com.squareup.otto.Subscribe
    public void handleDialUpdate(com.navdy.hud.app.device.dial.DialManager.DialUpdateStatus event) {
        long time = this.sharedPreferences.getLong(DIAL_REMINDER_TIME, 0);
        if (event.available) {
            long curTime = java.lang.System.currentTimeMillis();
            if (time == 0) {
                this.sharedPreferences.edit().putLong(DIAL_REMINDER_TIME, curTime + INITIAL_REMINDER_INTERVAL).apply();
                sLogger.v(java.lang.String.format("handleDialUpdate(): setting dial update reminder time to +%d seconds", new java.lang.Object[]{java.lang.Long.valueOf(java.util.concurrent.TimeUnit.MILLISECONDS.toSeconds(INITIAL_REMINDER_INTERVAL))}));
            } else {
                sLogger.v(java.lang.String.format("handleDialUpdate(): dial update reminder time already set to +%d seconds", new java.lang.Object[]{java.lang.Long.valueOf(java.util.concurrent.TimeUnit.MILLISECONDS.toSeconds(time - curTime))}));
            }
            this.dialUpdateAvailable = true;
            scheduleReminder();
        } else if (time != 0) {
            this.sharedPreferences.edit().remove(DIAL_REMINDER_TIME).apply();
            sLogger.v("handleDialUpdate(): clearing dial reminder");
        }
    }

    @com.squareup.otto.Subscribe
    public void onDateTimeAvailable(com.navdy.hud.app.common.TimeHelper.DateTimeAvailableEvent event) {
        this.timeAvailable = true;
        long reminderTime = this.sharedPreferences.getLong(OTA_REMINDER_TIME, 0);
        if (isOTAUpdateRequired()) {
            long curTime = java.lang.System.currentTimeMillis();
            if (reminderTime == 0) {
                this.sharedPreferences.edit().putLong(OTA_REMINDER_TIME, INITIAL_REMINDER_INTERVAL + curTime).apply();
                sLogger.v(java.lang.String.format("onDateTimeAvailable(): setting OTA reminder time to +%d seconds ", new java.lang.Object[]{java.lang.Long.valueOf(java.util.concurrent.TimeUnit.MILLISECONDS.toSeconds(INITIAL_REMINDER_INTERVAL))}));
            } else {
                sLogger.v(java.lang.String.format("onDateTimeAvailable() OTA reminder time already set to +%d seconds", new java.lang.Object[]{java.lang.Long.valueOf(java.util.concurrent.TimeUnit.MILLISECONDS.toSeconds(reminderTime - curTime))}));
            }
        } else if (reminderTime != 0) {
            this.sharedPreferences.edit().remove(OTA_REMINDER_TIME).apply();
            sLogger.v("onDateTimeAvailable(): clearing ota reminder");
        }
        scheduleReminder();
    }

    @com.squareup.otto.Subscribe
    public void handleOTAUpdate(com.navdy.hud.app.util.OTAUpdateService.UpdateVerified event) {
        long reminderTime = this.sharedPreferences.getLong(OTA_REMINDER_TIME, 0);
        long curTime = java.lang.System.currentTimeMillis();
        if (reminderTime == 0) {
            this.sharedPreferences.edit().putLong(OTA_REMINDER_TIME, INITIAL_REMINDER_INTERVAL + curTime).apply();
            sLogger.v(java.lang.String.format("handleOTAUpdate() setting OTA reminder for +%d seconds ", new java.lang.Object[]{java.lang.Long.valueOf(java.util.concurrent.TimeUnit.MILLISECONDS.toSeconds(INITIAL_REMINDER_INTERVAL))}));
        } else {
            sLogger.v(java.lang.String.format("handleOTAUpdate() OTA reminder already set for +%d seconds", new java.lang.Object[]{java.lang.Long.valueOf(java.util.concurrent.TimeUnit.MILLISECONDS.toSeconds(reminderTime - curTime))}));
        }
        scheduleReminder();
    }
}
