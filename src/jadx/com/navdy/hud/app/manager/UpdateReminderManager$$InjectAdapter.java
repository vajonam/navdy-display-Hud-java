package com.navdy.hud.app.manager;

public final class UpdateReminderManager$$InjectAdapter extends dagger.internal.Binding<com.navdy.hud.app.manager.UpdateReminderManager> implements dagger.MembersInjector<com.navdy.hud.app.manager.UpdateReminderManager> {
    private dagger.internal.Binding<com.squareup.otto.Bus> bus;
    private dagger.internal.Binding<android.content.SharedPreferences> sharedPreferences;

    public UpdateReminderManager$$InjectAdapter() {
        super(null, "members/com.navdy.hud.app.manager.UpdateReminderManager", false, com.navdy.hud.app.manager.UpdateReminderManager.class);
    }

    public void attach(dagger.internal.Linker linker) {
        this.bus = linker.requestBinding("com.squareup.otto.Bus", com.navdy.hud.app.manager.UpdateReminderManager.class, getClass().getClassLoader());
        this.sharedPreferences = linker.requestBinding("android.content.SharedPreferences", com.navdy.hud.app.manager.UpdateReminderManager.class, getClass().getClassLoader());
    }

    public void getDependencies(java.util.Set<dagger.internal.Binding<?>> set, java.util.Set<dagger.internal.Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.bus);
        injectMembersBindings.add(this.sharedPreferences);
    }

    public void injectMembers(com.navdy.hud.app.manager.UpdateReminderManager object) {
        object.bus = (com.squareup.otto.Bus) this.bus.get();
        object.sharedPreferences = (android.content.SharedPreferences) this.sharedPreferences.get();
    }
}
