package com.navdy.hud.app.manager;

public class SpeedManager {
    private static final double EPSILON = 1.0E-5d;
    private static final long GPS_SPEED_EXPIRY_INTERVAL = 5000;
    private static final double MAX_VALID_METERS_PER_SEC = 89.41333333333333d;
    private static final double METERS_PER_KILOMETER = 1000.0d;
    private static final double METERS_PER_MILE = 1609.44d;
    private static final int SECONDS_PER_HOUR = 3600;
    public static final int SPEED_NOT_AVAILABLE = -1;
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.manager.SpeedManager.class);
    private static final com.navdy.hud.app.manager.SpeedManager singleton = new com.navdy.hud.app.manager.SpeedManager();
    @javax.inject.Inject
    com.squareup.otto.Bus bus;
    @javax.inject.Inject
    com.navdy.hud.app.profile.DriverProfileManager driverProfileManager;
    /* access modifiers changed from: private */
    public volatile int gpsSpeed;
    private java.lang.Runnable gpsSpeedExpiryRunnable;
    private android.os.Handler handler;
    private volatile int obdSpeed = -1;
    private volatile float rawGpsSpeed = -1.0f;
    private volatile long rawGpsSpeedTimeStamp = 0;
    private volatile int rawObdSpeed = -1;
    private volatile long rawObdSpeedTimeStamp = 0;
    private com.navdy.hud.app.manager.SpeedManager.SpeedUnit speedUnit = com.navdy.hud.app.manager.SpeedManager.SpeedUnit.MILES_PER_HOUR;

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            com.navdy.hud.app.manager.SpeedManager.sLogger.d("Gps RawSpeed data expired, resetting to zero");
            com.navdy.hud.app.manager.SpeedManager.this.gpsSpeed = -1;
            com.navdy.hud.app.HudApplication.getApplication().getBus().post(new com.navdy.hud.app.manager.SpeedManager.SpeedDataExpired());
        }
    }

    public static class SpeedDataExpired {
        public com.navdy.hud.app.manager.SpeedManager.SpeedDataSource source = com.navdy.hud.app.manager.SpeedManager.SpeedDataSource.GPS;
    }

    public enum SpeedDataSource {
        GPS,
        OBD
    }

    public enum SpeedUnit {
        MILES_PER_HOUR,
        KILOMETERS_PER_HOUR,
        METERS_PER_SECOND
    }

    public static class SpeedUnitChanged {
    }

    public static com.navdy.hud.app.manager.SpeedManager getInstance() {
        return singleton;
    }

    @javax.inject.Inject
    public SpeedManager() {
        mortar.Mortar.inject(com.navdy.hud.app.HudApplication.getAppContext(), this);
        this.handler = new android.os.Handler(android.os.Looper.getMainLooper());
        this.bus.register(this);
        setUnit();
    }

    public com.navdy.hud.app.manager.SpeedManager.SpeedUnit getSpeedUnit() {
        return this.speedUnit;
    }

    public void setSpeedUnit(com.navdy.hud.app.manager.SpeedManager.SpeedUnit speedUnit2) {
        if (speedUnit2 != this.speedUnit) {
            this.gpsSpeed = convert((double) this.gpsSpeed, this.speedUnit, speedUnit2);
            this.obdSpeed = convert((double) this.obdSpeed, this.speedUnit, speedUnit2);
            this.speedUnit = speedUnit2;
        }
    }

    public synchronized int getGpsSpeed() {
        return this.gpsSpeed;
    }

    public synchronized boolean setGpsSpeed(float metersPerSec, long rawGpsSpeedTimeStamp2) {
        if (this.gpsSpeedExpiryRunnable == null) {
            this.gpsSpeedExpiryRunnable = new com.navdy.hud.app.manager.SpeedManager.Anon1();
        }
        this.handler.removeCallbacks(this.gpsSpeedExpiryRunnable);
        this.handler.postDelayed(this.gpsSpeedExpiryRunnable, 5000);
        return updateSpeed(metersPerSec, rawGpsSpeedTimeStamp2);
    }

    private static boolean almostEqual(float a, float b) {
        return ((double) java.lang.Math.abs(a - b)) < EPSILON;
    }

    private synchronized boolean updateSpeed(float metersPerSec, long timeStamp) {
        boolean z = false;
        synchronized (this) {
            if (almostEqual(this.rawGpsSpeed, metersPerSec)) {
                this.rawGpsSpeedTimeStamp = timeStamp;
            } else if (metersPerSec >= 0.0f && ((double) metersPerSec) <= MAX_VALID_METERS_PER_SEC) {
                this.rawGpsSpeedTimeStamp = timeStamp;
                this.rawGpsSpeed = metersPerSec;
                this.gpsSpeed = convert((double) metersPerSec, com.navdy.hud.app.manager.SpeedManager.SpeedUnit.METERS_PER_SECOND, this.speedUnit);
                z = true;
            }
        }
        return z;
    }

    public int getObdSpeed() {
        return this.obdSpeed;
    }

    public int getRawObdSpeed() {
        return this.rawObdSpeed;
    }

    public synchronized void setObdSpeed(int kph, long timeStamp) {
        if (kph < -1 || kph > 256) {
            sLogger.w("Invalid OBD speed ignored:" + kph);
        } else {
            this.rawObdSpeed = kph;
            this.rawObdSpeedTimeStamp = timeStamp;
            this.obdSpeed = convert((double) kph, com.navdy.hud.app.manager.SpeedManager.SpeedUnit.KILOMETERS_PER_HOUR, this.speedUnit);
        }
    }

    public static int convert(double source, com.navdy.hud.app.manager.SpeedManager.SpeedUnit sourceUnit, com.navdy.hud.app.manager.SpeedManager.SpeedUnit desiredUnit) {
        return java.lang.Math.round(convertWithPrecision(source, sourceUnit, desiredUnit));
    }

    public static float convertWithPrecision(double source, com.navdy.hud.app.manager.SpeedManager.SpeedUnit sourceUnit, com.navdy.hud.app.manager.SpeedManager.SpeedUnit desiredUnit) {
        double metersPerSecond;
        if (source == 0.0d || source == -1.0d) {
            return (float) source;
        }
        if (sourceUnit == desiredUnit) {
            return (float) source;
        }
        switch (sourceUnit) {
            case MILES_PER_HOUR:
                metersPerSecond = (source * METERS_PER_MILE) / 3600.0d;
                break;
            case KILOMETERS_PER_HOUR:
                metersPerSecond = (source * METERS_PER_KILOMETER) / 3600.0d;
                break;
            case METERS_PER_SECOND:
                metersPerSecond = source;
                break;
            default:
                metersPerSecond = 0.0d;
                break;
        }
        switch (desiredUnit) {
            case MILES_PER_HOUR:
                return (float) ((metersPerSecond * 3600.0d) / METERS_PER_MILE);
            case KILOMETERS_PER_HOUR:
                return (float) ((metersPerSecond * 3600.0d) / METERS_PER_KILOMETER);
            case METERS_PER_SECOND:
                return (float) metersPerSecond;
            default:
                return 0.0f;
        }
    }

    public synchronized int getCurrentSpeed() {
        int obdSpeed2;
        obdSpeed2 = getObdSpeed();
        if (obdSpeed2 == -1) {
            obdSpeed2 = getGpsSpeed();
        }
        return obdSpeed2;
    }

    public synchronized com.navdy.hud.app.analytics.RawSpeed getCurrentSpeedInMetersPerSecond() {
        com.navdy.hud.app.analytics.RawSpeed rawSpeed;
        if (this.rawObdSpeed != -1) {
            rawSpeed = new com.navdy.hud.app.analytics.RawSpeed(convertWithPrecision((double) this.rawObdSpeed, com.navdy.hud.app.manager.SpeedManager.SpeedUnit.KILOMETERS_PER_HOUR, com.navdy.hud.app.manager.SpeedManager.SpeedUnit.METERS_PER_SECOND), this.rawObdSpeedTimeStamp);
        } else {
            rawSpeed = new com.navdy.hud.app.analytics.RawSpeed(this.rawGpsSpeed, this.rawGpsSpeedTimeStamp);
        }
        return rawSpeed;
    }

    public synchronized float getGpsSpeedInMetersPerSecond() {
        return this.rawGpsSpeed;
    }

    public static int convert(double metersPerSec, com.navdy.hud.app.manager.SpeedManager.SpeedUnit unit) {
        return convert(metersPerSec, com.navdy.hud.app.manager.SpeedManager.SpeedUnit.METERS_PER_SECOND, unit);
    }

    @com.squareup.otto.Subscribe
    public void onDriverProfileUpdated(com.navdy.hud.app.event.DriverProfileUpdated event) {
        if (event.state == com.navdy.hud.app.event.DriverProfileUpdated.State.UPDATED) {
            setUnit();
        }
    }

    @com.squareup.otto.Subscribe
    public void onDriverProfileChanged(com.navdy.hud.app.event.DriverProfileChanged event) {
        setUnit();
    }

    private void setUnit() {
        com.navdy.hud.app.manager.SpeedManager.SpeedUnit speedUnit2 = this.driverProfileManager.getCurrentProfile().getUnitSystem() == com.navdy.service.library.events.preferences.DriverProfilePreferences.UnitSystem.UNIT_SYSTEM_METRIC ? com.navdy.hud.app.manager.SpeedManager.SpeedUnit.KILOMETERS_PER_HOUR : com.navdy.hud.app.manager.SpeedManager.SpeedUnit.MILES_PER_HOUR;
        if (this.speedUnit != speedUnit2) {
            sLogger.i("ondriverprofileupdate speed unit has changed to " + speedUnit2.name());
            setSpeedUnit(speedUnit2);
            this.bus.post(new com.navdy.hud.app.manager.SpeedManager.SpeedUnitChanged());
            return;
        }
        sLogger.v("ondriverprofileupdated speed unit is still " + speedUnit2.name());
    }
}
