package com.navdy.hud.app.manager;

public final class RemoteDeviceManager {
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.manager.RemoteDeviceManager.class);
    private static final com.navdy.hud.app.manager.RemoteDeviceManager singleton = new com.navdy.hud.app.manager.RemoteDeviceManager();
    @javax.inject.Inject
    com.squareup.otto.Bus bus;
    @javax.inject.Inject
    com.navdy.hud.app.framework.calendar.CalendarManager calendarManager;
    @javax.inject.Inject
    com.navdy.hud.app.framework.phonecall.CallManager callManager;
    @javax.inject.Inject
    com.navdy.hud.app.service.ConnectionHandler connectionHandler;
    @javax.inject.Inject
    com.navdy.hud.app.debug.DriveRecorder driveRecorder;
    @javax.inject.Inject
    com.navdy.hud.app.util.FeatureUtil featureUtil;
    @javax.inject.Inject
    com.navdy.hud.app.gesture.GestureServiceConnector gestureServiceConnector;
    @javax.inject.Inject
    com.navdy.service.library.network.http.IHttpManager httpManager;
    @javax.inject.Inject
    com.navdy.hud.app.manager.InputManager inputManager;
    @javax.inject.Inject
    com.navdy.hud.app.manager.MusicManager musicManager;
    @javax.inject.Inject
    android.content.SharedPreferences preferences;
    @javax.inject.Inject
    com.navdy.hud.app.analytics.TelemetryDataManager telemetryDataManager;
    @javax.inject.Inject
    com.navdy.hud.app.common.TimeHelper timeHelper;
    @javax.inject.Inject
    com.navdy.hud.app.framework.trips.TripManager tripManager;
    @javax.inject.Inject
    com.navdy.hud.app.ui.framework.UIStateManager uiStateManager;
    @javax.inject.Inject
    com.navdy.hud.app.framework.voice.VoiceSearchHandler voiceSearchHandler;

    public static com.navdy.hud.app.manager.RemoteDeviceManager getInstance() {
        return singleton;
    }

    private RemoteDeviceManager() {
        android.content.Context appContext = com.navdy.hud.app.HudApplication.getAppContext();
        if (appContext != null) {
            mortar.Mortar.inject(appContext, this);
        }
    }

    public com.navdy.service.library.events.DeviceInfo getRemoteDeviceInfo() {
        com.navdy.service.library.device.RemoteDevice remoteDevice = this.connectionHandler.getRemoteDevice();
        if (remoteDevice != null) {
            return remoteDevice.getDeviceInfo();
        }
        return null;
    }

    public boolean doesRemoteDeviceHasCapability(com.navdy.service.library.events.LegacyCapability capability) {
        com.navdy.service.library.device.RemoteDevice remoteDevice = this.connectionHandler.getRemoteDevice();
        if (remoteDevice == null) {
            return false;
        }
        com.navdy.service.library.events.DeviceInfo deviceInfo = remoteDevice.getDeviceInfo();
        if (deviceInfo == null || deviceInfo.legacyCapabilities == null || !deviceInfo.legacyCapabilities.contains(capability)) {
            return false;
        }
        return true;
    }

    public com.navdy.service.library.device.NavdyDeviceId getDeviceId() {
        com.navdy.service.library.device.RemoteDevice remoteDevice = this.connectionHandler.getRemoteDevice();
        if (remoteDevice != null) {
            return remoteDevice.getDeviceId();
        }
        return null;
    }

    public boolean isRemoteDeviceConnected() {
        return getRemoteDeviceInfo() != null;
    }

    public boolean isAppConnected() {
        if (!isRemoteDeviceConnected() || this.connectionHandler.isAppClosed()) {
            return false;
        }
        return true;
    }

    public com.squareup.otto.Bus getBus() {
        return this.bus;
    }

    public com.navdy.hud.app.common.TimeHelper getTimeHelper() {
        return this.timeHelper;
    }

    public com.navdy.service.library.events.DeviceInfo.Platform getRemoteDevicePlatform() {
        com.navdy.service.library.device.RemoteDevice remoteDevice = this.connectionHandler.getRemoteDevice();
        if (remoteDevice == null) {
            return null;
        }
        com.navdy.service.library.events.DeviceInfo deviceInfo = remoteDevice.getDeviceInfo();
        if (deviceInfo != null) {
            return deviceInfo.platform;
        }
        return com.navdy.service.library.events.DeviceInfo.Platform.PLATFORM_iOS;
    }

    public boolean isRemoteDeviceIos() {
        return com.navdy.service.library.events.DeviceInfo.Platform.PLATFORM_iOS.equals(getRemoteDevicePlatform());
    }

    public boolean isNetworkLinkReady() {
        return this.connectionHandler.isNetworkLinkReady();
    }

    public com.navdy.hud.app.service.ConnectionHandler getConnectionHandler() {
        return this.connectionHandler;
    }

    public com.navdy.hud.app.ui.framework.UIStateManager getUiStateManager() {
        return this.uiStateManager;
    }

    public com.navdy.hud.app.framework.phonecall.CallManager getCallManager() {
        return this.callManager;
    }

    public android.content.SharedPreferences getSharedPreferences() {
        return this.preferences;
    }

    public com.navdy.hud.app.manager.InputManager getInputManager() {
        return this.inputManager;
    }

    public com.navdy.hud.app.gesture.GestureServiceConnector getGestureServiceConnector() {
        return this.gestureServiceConnector;
    }

    public com.navdy.hud.app.framework.calendar.CalendarManager getCalendarManager() {
        return this.calendarManager;
    }

    public com.navdy.hud.app.framework.trips.TripManager getTripManager() {
        return this.tripManager;
    }

    public com.navdy.hud.app.debug.DriveRecorder getDriveRecorder() {
        return this.driveRecorder;
    }

    public com.navdy.hud.app.manager.MusicManager getMusicManager() {
        return this.musicManager;
    }

    public com.navdy.hud.app.framework.voice.VoiceSearchHandler getVoiceSearchHandler() {
        return this.voiceSearchHandler;
    }

    public com.navdy.hud.app.util.FeatureUtil getFeatureUtil() {
        return this.featureUtil;
    }

    public com.navdy.service.library.network.http.IHttpManager getHttpManager() {
        return this.httpManager;
    }

    public com.navdy.hud.app.analytics.TelemetryDataManager getTelemetryDataManager() {
        return this.telemetryDataManager;
    }
}
