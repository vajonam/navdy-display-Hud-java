package com.navdy.hud.app.manager;

public class MusicManager {
    public static final com.navdy.hud.app.manager.MusicManager.MediaControl[] CONTROLS = com.navdy.hud.app.manager.MusicManager.MediaControl.values();
    public static final int DEFAULT_ARTWORK_SIZE = 200;
    public static final com.navdy.service.library.events.audio.MusicTrackInfo EMPTY_TRACK = new com.navdy.service.library.events.audio.MusicTrackInfo.Builder().playbackState(com.navdy.service.library.events.audio.MusicPlaybackState.PLAYBACK_NONE).name(com.navdy.hud.app.HudApplication.getAppContext().getString(com.navdy.hud.app.R.string.music_init_title)).author("").album("").isPreviousAllowed(java.lang.Boolean.valueOf(true)).isNextAllowed(java.lang.Boolean.valueOf(true)).build();
    public static final long PAUSED_DUE_TO_HFP_EXPIRY_TIME = java.util.concurrent.TimeUnit.MINUTES.toMillis(10);
    public static final java.lang.String PREFERENCE_MUSIC_CACHE_COLLECTION_SOURCE = "PREFERENCE_MUSIC_CACHE_COLLECTION_SOURCE";
    public static final java.lang.String PREFERENCE_MUSIC_CACHE_PROFILE = "PREFERENCE_MUSIC_CACHE_PROFILE";
    public static final java.lang.String PREFERENCE_MUSIC_CACHE_SERIAL = "PREFERENCE_MUSIC_CACHE_SERIAL";
    private static final int PROGRESS_BAR_UPDATE_ON_ANDROID = 2000;
    public static final int THRESHOLD_TIME_BETWEEN_PAUSE_AND_POSSIBLE_CAUSE_FOR_HFP = 10000;
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.manager.MusicManager.class);
    private boolean acceptingResumes = false;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.util.MusicArtworkCache artworkCache;
    /* access modifiers changed from: private */
    public com.squareup.otto.Bus bus;
    private boolean clientSupportsArtworkCaching;
    private java.util.Set<com.navdy.hud.app.manager.MusicManager.MediaControl> currentControls;
    /* access modifiers changed from: private */
    public okio.ByteString currentPhoto;
    /* access modifiers changed from: private */
    public int currentPosition;
    /* access modifiers changed from: private */
    @android.support.annotation.NonNull
    public com.navdy.service.library.events.audio.MusicTrackInfo currentTrack;
    /* access modifiers changed from: private */
    public android.os.Handler handler = new android.os.Handler();
    private volatile long interestingEventTime;
    private java.util.concurrent.atomic.AtomicBoolean isLongPress = new java.util.concurrent.atomic.AtomicBoolean(false);
    private boolean isMusicLibraryCachingEnabled = false;
    private volatile long lastPausedTime;
    /* access modifiers changed from: private */
    public long lastTrackUpdateTime;
    private com.navdy.service.library.events.audio.MusicCapabilitiesResponse musicCapabilities;
    private java.util.Map<com.navdy.service.library.events.audio.MusicCollectionSource, java.util.List<com.navdy.service.library.events.audio.MusicCollectionType>> musicCapabilityTypeMap;
    private com.navdy.hud.app.storage.cache.MessageCache<com.navdy.service.library.events.audio.MusicCollectionResponse> musicCollectionResponseMessageCache;
    private java.lang.String musicMenuPath = null;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.framework.music.MusicNotification musicNotification;
    /* access modifiers changed from: private */
    public java.util.Set<com.navdy.hud.app.manager.MusicManager.MusicUpdateListener> musicUpdateListeners = new java.util.HashSet();
    private final java.lang.Runnable openNotificationRunnable = new com.navdy.hud.app.manager.MusicManager.Anon3();
    private com.navdy.hud.app.service.pandora.PandoraManager pandoraManager;
    private boolean pausedByUs = false;
    private volatile long pausedDueToHfpDetectedTime = 0;
    @android.support.annotation.NonNull
    private com.navdy.service.library.events.audio.MusicDataSource previousDataSource = com.navdy.service.library.events.audio.MusicDataSource.MUSIC_SOURCE_NONE;
    /* access modifiers changed from: private */
    public java.lang.Runnable progressBarUpdater = new com.navdy.hud.app.manager.MusicManager.Anon2();
    /* access modifiers changed from: private */
    public com.navdy.hud.app.ui.framework.UIStateManager uiStateManager;
    private boolean updateListeners = true;
    private volatile boolean wasPossiblyPausedDueToHFP = false;

    class Anon1 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.service.library.events.audio.MusicTrackInfo val$trackInfo;

        /* renamed from: com.navdy.hud.app.manager.MusicManager$Anon1$Anon1 reason: collision with other inner class name */
        class C0013Anon1 implements com.navdy.hud.app.util.MusicArtworkCache.Callback {
            C0013Anon1() {
            }

            public void onHit(@android.support.annotation.NonNull byte[] artwork) {
                com.navdy.hud.app.manager.MusicManager.sLogger.d("CACHE HIT");
                com.navdy.hud.app.manager.MusicManager.this.currentPhoto = okio.ByteString.of(artwork);
                com.navdy.hud.app.manager.MusicManager.this.callAlbumArtCallbacks();
            }

            public void onMiss() {
                com.navdy.hud.app.manager.MusicManager.sLogger.d("CACHE MISS");
                com.navdy.service.library.events.audio.MusicArtworkRequest musicArtworkRequest = new com.navdy.service.library.events.audio.MusicArtworkRequest.Builder().name(com.navdy.hud.app.manager.MusicManager.Anon1.this.val$trackInfo.name).album(com.navdy.hud.app.manager.MusicManager.Anon1.this.val$trackInfo.album).author(com.navdy.hud.app.manager.MusicManager.Anon1.this.val$trackInfo.author).size(java.lang.Integer.valueOf(200)).build();
                com.navdy.hud.app.manager.MusicManager.sLogger.d("requesting artwork: " + musicArtworkRequest);
                com.navdy.hud.app.manager.MusicManager.this.bus.post(new com.navdy.hud.app.event.RemoteEvent(musicArtworkRequest));
            }
        }

        Anon1(com.navdy.service.library.events.audio.MusicTrackInfo musicTrackInfo) {
            this.val$trackInfo = musicTrackInfo;
        }

        public void run() {
            com.navdy.hud.app.manager.MusicManager.this.artworkCache.getArtwork(this.val$trackInfo.author, this.val$trackInfo.album, this.val$trackInfo.name, new com.navdy.hud.app.manager.MusicManager.Anon1.C0013Anon1());
        }
    }

    class Anon2 implements java.lang.Runnable {
        Anon2() {
        }

        public void run() {
            if (com.navdy.service.library.events.audio.MusicPlaybackState.PLAYBACK_PLAYING.equals(com.navdy.hud.app.manager.MusicManager.this.currentTrack.playbackState) && com.navdy.hud.app.manager.MusicManager.this.currentTrack.currentPosition != null) {
                com.navdy.hud.app.manager.MusicManager.this.currentPosition = com.navdy.hud.app.manager.MusicManager.this.currentTrack.currentPosition.intValue() + ((int) (android.os.SystemClock.elapsedRealtime() - com.navdy.hud.app.manager.MusicManager.this.lastTrackUpdateTime));
                com.navdy.hud.app.manager.MusicManager.this.musicNotification.updateProgressBar(com.navdy.hud.app.manager.MusicManager.this.currentPosition);
                com.navdy.hud.app.manager.MusicManager.this.handler.postDelayed(com.navdy.hud.app.manager.MusicManager.this.progressBarUpdater, 2000);
            }
        }
    }

    class Anon3 implements java.lang.Runnable {
        Anon3() {
        }

        public void run() {
            com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().addNotification(com.navdy.hud.app.manager.MusicManager.this.musicNotification, com.navdy.hud.app.manager.MusicManager.this.uiStateManager.getMainScreensSet());
        }
    }

    class Anon4 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.hud.app.manager.MusicManager.MusicUpdateListener val$listener;

        class Anon1 implements java.lang.Runnable {
            Anon1() {
            }

            public void run() {
                com.navdy.hud.app.manager.MusicManager.Anon4.this.val$listener.onAlbumArtUpdate(com.navdy.hud.app.manager.MusicManager.this.currentPhoto, false);
                com.navdy.hud.app.manager.MusicManager.Anon4.this.val$listener.onTrackUpdated(com.navdy.hud.app.manager.MusicManager.this.getCurrentTrack(), com.navdy.hud.app.manager.MusicManager.this.getCurrentControls(), false);
            }
        }

        Anon4(com.navdy.hud.app.manager.MusicManager.MusicUpdateListener musicUpdateListener) {
            this.val$listener = musicUpdateListener;
        }

        public void run() {
            com.navdy.hud.app.manager.MusicManager.this.handler.post(new com.navdy.hud.app.manager.MusicManager.Anon4.Anon1());
            com.navdy.hud.app.manager.MusicManager.sLogger.v("album art listeners:");
            for (com.navdy.hud.app.manager.MusicManager.MusicUpdateListener l : com.navdy.hud.app.manager.MusicManager.this.musicUpdateListeners) {
                com.navdy.hud.app.manager.MusicManager.sLogger.d("- " + l);
            }
        }
    }

    class Anon5 implements java.lang.Runnable {

        class Anon1 implements java.lang.Runnable {
            Anon1() {
            }

            public void run() {
                for (com.navdy.hud.app.manager.MusicManager.MusicUpdateListener listener : com.navdy.hud.app.manager.MusicManager.this.musicUpdateListeners) {
                    listener.onAlbumArtUpdate(com.navdy.hud.app.manager.MusicManager.this.currentPhoto, true);
                }
            }
        }

        Anon5() {
        }

        public void run() {
            com.navdy.hud.app.manager.MusicManager.this.handler.post(new com.navdy.hud.app.manager.MusicManager.Anon5.Anon1());
        }
    }

    class Anon6 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.service.library.events.input.MediaRemoteKey val$key;

        Anon6(com.navdy.service.library.events.input.MediaRemoteKey mediaRemoteKey) {
            this.val$key = mediaRemoteKey;
        }

        public void run() {
            com.navdy.hud.app.manager.MusicManager.this.bus.post(new com.navdy.hud.app.event.RemoteEvent(new com.navdy.service.library.events.input.MediaRemoteKeyEvent(this.val$key, com.navdy.service.library.events.input.KeyEvent.KEY_UP)));
        }
    }

    public enum MediaControl {
        PLAY,
        PAUSE,
        NEXT,
        PREVIOUS,
        MUSIC_MENU,
        MUSIC_MENU_DEEP
    }

    public interface MusicUpdateListener {
        void onAlbumArtUpdate(@android.support.annotation.Nullable okio.ByteString byteString, boolean z);

        void onTrackUpdated(com.navdy.service.library.events.audio.MusicTrackInfo musicTrackInfo, java.util.Set<com.navdy.hud.app.manager.MusicManager.MediaControl> set, boolean z);
    }

    public MusicManager(com.navdy.hud.app.storage.cache.MessageCache<com.navdy.service.library.events.audio.MusicCollectionResponse> musicCollectionResponseMessageCache2, com.squareup.otto.Bus bus2, android.content.res.Resources resources, com.navdy.hud.app.ui.framework.UIStateManager uiStateManager2, com.navdy.hud.app.service.pandora.PandoraManager pandoraManager2, com.navdy.hud.app.util.MusicArtworkCache artworkCache2) {
        this.musicCollectionResponseMessageCache = musicCollectionResponseMessageCache2;
        this.bus = bus2;
        this.uiStateManager = uiStateManager2;
        this.pandoraManager = pandoraManager2;
        this.currentTrack = EMPTY_TRACK;
        this.currentControls = new java.util.HashSet();
        this.currentControls.add(com.navdy.hud.app.manager.MusicManager.MediaControl.PLAY);
        this.musicNotification = new com.navdy.hud.app.framework.music.MusicNotification(this, bus2);
        this.artworkCache = artworkCache2;
    }

    public com.navdy.service.library.events.audio.MusicTrackInfo getCurrentTrack() {
        return this.currentTrack;
    }

    public java.util.Set<com.navdy.hud.app.manager.MusicManager.MediaControl> getCurrentControls() {
        return this.currentControls;
    }

    public int getCurrentPosition() {
        return this.currentPosition;
    }

    public static boolean tryingToPlay(com.navdy.service.library.events.audio.MusicPlaybackState playbackState) {
        return !com.navdy.service.library.events.audio.MusicPlaybackState.PLAYBACK_NONE.equals(playbackState) && !com.navdy.service.library.events.audio.MusicPlaybackState.PLAYBACK_CONNECTING.equals(playbackState) && !com.navdy.service.library.events.audio.MusicPlaybackState.PLAYBACK_ERROR.equals(playbackState) && !com.navdy.service.library.events.audio.MusicPlaybackState.PLAYBACK_PAUSED.equals(playbackState) && !com.navdy.service.library.events.audio.MusicPlaybackState.PLAYBACK_STOPPED.equals(playbackState);
    }

    @com.squareup.otto.Subscribe
    public void onTrackInfo(com.navdy.service.library.events.audio.MusicTrackInfo trackInfo) {
        sLogger.d("updateState " + trackInfo);
        boolean isPlaying = com.navdy.service.library.events.audio.MusicPlaybackState.PLAYBACK_PLAYING.equals(trackInfo.playbackState);
        boolean isOldStatePlaying = com.navdy.service.library.events.audio.MusicPlaybackState.PLAYBACK_PLAYING.equals(this.currentTrack.playbackState);
        if (trackInfo.collectionSource == com.navdy.service.library.events.audio.MusicCollectionSource.COLLECTION_SOURCE_UNKNOWN) {
            setMusicMenuPath(null);
        }
        if (isPlaying || !isOldStatePlaying || com.navdy.service.library.events.audio.MusicDataSource.MUSIC_SOURCE_PANDORA_API.equals(this.currentTrack.dataSource) || !com.navdy.service.library.events.audio.MusicDataSource.MUSIC_SOURCE_PANDORA_API.equals(trackInfo.dataSource)) {
            boolean trackChanged = !android.text.TextUtils.equals(com.navdy.service.library.util.MusicDataUtils.songIdentifierFromTrackInfo(trackInfo), com.navdy.service.library.util.MusicDataUtils.songIdentifierFromTrackInfo(this.currentTrack));
            if (trackChanged && this.clientSupportsArtworkCaching) {
                requestArtwork(trackInfo);
            }
            boolean isNewSongPlaying = isPlaying && trackChanged;
            boolean isNewStatePlaying = isPlaying && !isOldStatePlaying;
            boolean isPaused = com.navdy.service.library.events.audio.MusicPlaybackState.PLAYBACK_PAUSED.equals(trackInfo.playbackState);
            boolean isOldStatePaused = com.navdy.service.library.events.audio.MusicPlaybackState.PLAYBACK_PAUSED.equals(this.currentTrack.playbackState);
            boolean isNewStatePaused = isPaused && !isOldStatePaused;
            if ((isNewStatePlaying || isNewStatePaused) && com.navdy.service.library.events.DeviceInfo.Platform.PLATFORM_iOS.equals(com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getRemoteDevicePlatform())) {
                com.squareup.otto.Bus bus2 = this.bus;
                com.navdy.hud.app.event.RemoteEvent remoteEvent = new com.navdy.hud.app.event.RemoteEvent(trackInfo);
                bus2.post(remoteEvent);
            }
            if (isNewStatePlaying) {
                sLogger.d("Music started playing");
            }
            if (isNewStatePlaying && this.pausedByUs) {
                sLogger.d("User started playback after we paused - setting pauseByUs = false");
                this.pausedByUs = false;
            }
            if (isNewStatePaused) {
                sLogger.d("New state is paused");
                long pausedTime = android.os.SystemClock.elapsedRealtime();
                if (!this.wasPossiblyPausedDueToHFP) {
                    if (pausedTime - this.interestingEventTime < 10000) {
                        sLogger.d("Interesting event happened just before the song was paused, possibly playing through HFP");
                        this.wasPossiblyPausedDueToHFP = true;
                        this.pausedDueToHfpDetectedTime = pausedTime;
                    } else {
                        this.lastPausedTime = pausedTime;
                    }
                }
            }
            this.lastTrackUpdateTime = android.os.SystemClock.elapsedRealtime();
            boolean shouldSuppressNotification = isNewStatePlaying && this.wasPossiblyPausedDueToHFP && this.lastTrackUpdateTime - this.pausedDueToHfpDetectedTime < PAUSED_DUE_TO_HFP_EXPIRY_TIME;
            if (shouldSuppressNotification) {
                sLogger.d("Should suppress Notification");
            }
            if (shouldSuppressNotification || this.lastTrackUpdateTime - this.pausedDueToHfpDetectedTime > PAUSED_DUE_TO_HFP_EXPIRY_TIME) {
                this.wasPossiblyPausedDueToHFP = false;
                this.pausedDueToHfpDetectedTime = 0;
            }
            if (trackInfo.dataSource != null && !com.navdy.service.library.events.audio.MusicDataSource.MUSIC_SOURCE_NONE.equals(trackInfo.dataSource)) {
                this.previousDataSource = trackInfo.dataSource;
            }
            updateControls(trackInfo);
            boolean hasProgressInfo = (trackInfo.duration == null || trackInfo.duration.intValue() <= 0 || trackInfo.currentPosition == null) ? false : true;
            if (hasProgressInfo) {
                this.currentPosition = trackInfo.currentPosition.intValue();
                this.musicNotification.updateProgressBar(this.currentPosition);
            }
            if (com.navdy.service.library.events.DeviceInfo.Platform.PLATFORM_Android.equals(com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getRemoteDevicePlatform())) {
                this.handler.removeCallbacks(this.progressBarUpdater);
                if (com.navdy.service.library.events.audio.MusicPlaybackState.PLAYBACK_PLAYING.equals(trackInfo.playbackState) && hasProgressInfo) {
                    this.handler.postDelayed(this.progressBarUpdater, 2000);
                }
            }
            boolean interestingPauseTransition = isPaused && !(hasProgressInfo && trackInfo.currentPosition.equals(trackInfo.duration)) && ((isNewStatePaused && !trackChanged) || ((isOldStatePaused && trackChanged) || (com.navdy.service.library.events.audio.MusicPlaybackState.PLAYBACK_PAUSED.equals(this.currentTrack.playbackState) || com.navdy.service.library.events.audio.MusicPlaybackState.PLAYBACK_NONE.equals(this.currentTrack.playbackState))));
            boolean shuffleModeChange = this.currentTrack.shuffleMode != trackInfo.shuffleMode;
            this.updateListeners = isPlaying || interestingPauseTransition || shuffleModeChange;
            if (this.updateListeners) {
                this.currentTrack = trackInfo;
                boolean interestingPlayTransition = isNewSongPlaying || isNewStatePlaying;
                boolean willShowMusicNotification = !shouldSuppressNotification && com.navdy.hud.app.framework.glance.GlanceHelper.isMusicNotificationEnabled() && interestingPlayTransition;
                if (interestingPlayTransition || interestingPauseTransition || shuffleModeChange) {
                    callTrackUpdateCallbacks(willShowMusicNotification);
                    if (!this.clientSupportsArtworkCaching) {
                        callAlbumArtCallbacks();
                    }
                }
                if (willShowMusicNotification) {
                    sLogger.d("Showing notification New Song ? : " + isNewSongPlaying + ", New state playing : " + isNewStatePlaying);
                    this.handler.removeCallbacks(this.openNotificationRunnable);
                    this.handler.post(this.openNotificationRunnable);
                    return;
                }
                return;
            }
            sLogger.d("Ignoring updates");
            return;
        }
        sLogger.d("Ignoring update from non playing Pandora");
    }

    public boolean isShuffling() {
        return (this.currentTrack.shuffleMode == null || this.currentTrack.shuffleMode == com.navdy.service.library.events.audio.MusicShuffleMode.MUSIC_SHUFFLE_MODE_UNKNOWN || this.currentTrack.shuffleMode == com.navdy.service.library.events.audio.MusicShuffleMode.MUSIC_SHUFFLE_MODE_OFF) ? false : true;
    }

    private void requestArtwork(com.navdy.service.library.events.audio.MusicTrackInfo trackInfo) {
        sLogger.d("requestArtwork: " + trackInfo);
        if (!android.text.TextUtils.isEmpty(trackInfo.author) || !android.text.TextUtils.isEmpty(trackInfo.album) || !android.text.TextUtils.isEmpty(trackInfo.name)) {
            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.manager.MusicManager.Anon1(trackInfo), 22);
            return;
        }
        this.currentPhoto = null;
        callAlbumArtCallbacks();
        sLogger.d("Empty track, returning");
    }

    @com.squareup.otto.Subscribe
    public void onMusicArtworkResponse(com.navdy.service.library.events.audio.MusicArtworkResponse musicArtworkResponse) {
        sLogger.d("onMusicArtworkResponse " + musicArtworkResponse);
        if (musicArtworkResponse == null || musicArtworkResponse.photo == null) {
            this.currentPhoto = null;
        } else {
            cacheAlbumArt(musicArtworkResponse);
            if (android.text.TextUtils.equals(com.navdy.service.library.util.MusicDataUtils.songIdentifierFromTrackInfo(this.currentTrack), com.navdy.service.library.util.MusicDataUtils.songIdentifierFromArtworkResponse(musicArtworkResponse))) {
                this.currentPhoto = musicArtworkResponse.photo;
            }
        }
        callAlbumArtCallbacks();
    }

    private void updateControls(com.navdy.service.library.events.audio.MusicTrackInfo trackInfo) {
        this.currentControls = new java.util.HashSet();
        if (java.lang.Boolean.TRUE.equals(trackInfo.isPreviousAllowed)) {
            this.currentControls.add(com.navdy.hud.app.manager.MusicManager.MediaControl.PREVIOUS);
        }
        if (tryingToPlay(trackInfo.playbackState)) {
            this.currentControls.add(com.navdy.hud.app.manager.MusicManager.MediaControl.PAUSE);
        } else {
            this.currentControls.add(com.navdy.hud.app.manager.MusicManager.MediaControl.PLAY);
        }
        if (java.lang.Boolean.TRUE.equals(trackInfo.isNextAllowed)) {
            this.currentControls.add(com.navdy.hud.app.manager.MusicManager.MediaControl.NEXT);
        }
    }

    public void handleKeyEvent(android.view.KeyEvent event, com.navdy.hud.app.manager.MusicManager.MediaControl control, boolean isKeyPressedDown) {
        com.navdy.service.library.events.DeviceInfo.Platform remotePlatform = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getRemoteDevicePlatform();
        if (remotePlatform != null && com.navdy.hud.app.manager.InputManager.isCenterKey(event.getKeyCode())) {
            recordAnalytics(event, control, isKeyPressedDown);
            switch (remotePlatform) {
                case PLATFORM_Android:
                    switch (event.getAction()) {
                        case 0:
                            if (event.isLongPress() && this.isLongPress.compareAndSet(false, true)) {
                                switch (control) {
                                    case NEXT:
                                        if (canSeekBackward()) {
                                            runAndroidAction(com.navdy.service.library.events.audio.MusicEvent.Action.MUSIC_ACTION_FAST_FORWARD_START);
                                            return;
                                        } else {
                                            sLogger.w("Fast-forward is not allowed");
                                            return;
                                        }
                                    case PREVIOUS:
                                        if (canSeekForward()) {
                                            runAndroidAction(com.navdy.service.library.events.audio.MusicEvent.Action.MUSIC_ACTION_REWIND_START);
                                            return;
                                        } else {
                                            sLogger.w("Rewind is not allowed");
                                            return;
                                        }
                                    default:
                                        return;
                                }
                            } else {
                                return;
                            }
                        case 1:
                            if (!isKeyPressedDown) {
                                return;
                            }
                            if (this.isLongPress.compareAndSet(true, false)) {
                                switch (control) {
                                    case NEXT:
                                        runAndroidAction(com.navdy.service.library.events.audio.MusicEvent.Action.MUSIC_ACTION_FAST_FORWARD_STOP);
                                        return;
                                    case PREVIOUS:
                                        runAndroidAction(com.navdy.service.library.events.audio.MusicEvent.Action.MUSIC_ACTION_REWIND_STOP);
                                        return;
                                    default:
                                        return;
                                }
                            } else {
                                switch (control) {
                                    case NEXT:
                                        if (java.lang.Boolean.TRUE.equals(this.currentTrack.isNextAllowed)) {
                                            runAndroidAction(com.navdy.service.library.events.audio.MusicEvent.Action.MUSIC_ACTION_NEXT);
                                            return;
                                        } else {
                                            sLogger.w("Next song action is not allowed");
                                            return;
                                        }
                                    case PREVIOUS:
                                        if (java.lang.Boolean.TRUE.equals(this.currentTrack.isPreviousAllowed)) {
                                            runAndroidAction(com.navdy.service.library.events.audio.MusicEvent.Action.MUSIC_ACTION_PREVIOUS);
                                            return;
                                        } else {
                                            sLogger.w("Previous song action is not allowed");
                                            return;
                                        }
                                    default:
                                        return;
                                }
                            }
                        default:
                            return;
                    }
                case PLATFORM_iOS:
                    switch (event.getAction()) {
                        case 0:
                            if (isKeyPressedDown) {
                                return;
                            }
                            if (control == com.navdy.hud.app.manager.MusicManager.MediaControl.PLAY || control == com.navdy.hud.app.manager.MusicManager.MediaControl.PAUSE) {
                                sendMediaKeyEvent(control, true);
                                sendMediaKeyEvent(control, false);
                                return;
                            }
                            sendMediaKeyEvent(control, true);
                            return;
                        case 1:
                            if (isKeyPressedDown) {
                                sendMediaKeyEvent(control, false);
                                return;
                            }
                            return;
                        default:
                            return;
                    }
                default:
                    return;
            }
        }
    }

    public void executeMediaControl(com.navdy.hud.app.manager.MusicManager.MediaControl control, boolean isKeyPressedDown) {
        com.navdy.service.library.events.DeviceInfo.Platform remotePlatform = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getRemoteDevicePlatform();
        if (remotePlatform != null) {
            recordAnalytics(null, control, isKeyPressedDown);
            switch (remotePlatform) {
                case PLATFORM_Android:
                    switch (control) {
                        case PLAY:
                            runAndroidAction(com.navdy.service.library.events.audio.MusicEvent.Action.MUSIC_ACTION_PLAY);
                            return;
                        case PAUSE:
                            runAndroidAction(com.navdy.service.library.events.audio.MusicEvent.Action.MUSIC_ACTION_PAUSE);
                            return;
                        default:
                            return;
                    }
                case PLATFORM_iOS:
                    sendMediaKeyEvent(control, true);
                    sendMediaKeyEvent(control, false);
                    return;
                default:
                    return;
            }
        }
    }

    private void recordAnalytics(@android.support.annotation.Nullable android.view.KeyEvent event, com.navdy.hud.app.manager.MusicManager.MediaControl control, boolean isKeyPressedDown) {
        if (event == null) {
            switch (control) {
                case PLAY:
                    com.navdy.hud.app.analytics.AnalyticsSupport.recordMusicAction("Play");
                    return;
                case PAUSE:
                    com.navdy.hud.app.analytics.AnalyticsSupport.recordMusicAction("Pause");
                    return;
                default:
                    return;
            }
        } else {
            switch (event.getAction()) {
                case 0:
                    if (event.isLongPress() && !this.isLongPress.get()) {
                        switch (control) {
                            case NEXT:
                                com.navdy.hud.app.analytics.AnalyticsSupport.recordMusicAction("Fast-forward");
                                return;
                            case PREVIOUS:
                                com.navdy.hud.app.analytics.AnalyticsSupport.recordMusicAction("Rewind");
                                return;
                            default:
                                return;
                        }
                    } else {
                        return;
                    }
                case 1:
                    if (isKeyPressedDown && !this.isLongPress.get()) {
                        switch (control) {
                            case NEXT:
                                com.navdy.hud.app.analytics.AnalyticsSupport.recordMusicAction("Next");
                                return;
                            case PREVIOUS:
                                com.navdy.hud.app.analytics.AnalyticsSupport.recordMusicAction("Previous");
                                return;
                            default:
                                return;
                        }
                    } else {
                        return;
                    }
                default:
                    return;
            }
        }
    }

    private boolean canSeekForward() {
        return java.lang.Boolean.TRUE.equals(this.currentTrack.isNextAllowed) && !java.lang.Boolean.TRUE.equals(this.currentTrack.isOnlineStream);
    }

    private boolean canSeekBackward() {
        return java.lang.Boolean.TRUE.equals(this.currentTrack.isPreviousAllowed) && !java.lang.Boolean.TRUE.equals(this.currentTrack.isOnlineStream);
    }

    private boolean runAndroidAction(com.navdy.service.library.events.audio.MusicEvent.Action action) {
        com.navdy.service.library.events.audio.MusicDataSource targetDataSource = dataSourceToUse();
        if (!com.navdy.service.library.events.audio.MusicEvent.Action.MUSIC_ACTION_PLAY.equals(action) || !com.navdy.service.library.events.audio.MusicDataSource.MUSIC_SOURCE_PANDORA_API.equals(targetDataSource)) {
            sLogger.d("Media action: " + action);
            this.bus.post(new com.navdy.hud.app.event.RemoteEvent(new com.navdy.service.library.events.audio.MusicEvent.Builder().action(action).dataSource(targetDataSource).build()));
        } else {
            this.pandoraManager.startAndPlay();
        }
        return true;
    }

    private com.navdy.service.library.events.audio.MusicDataSource dataSourceToUse() {
        com.navdy.service.library.events.audio.MusicTrackInfo currentTrackAvailable = this.currentTrack;
        if (currentTrackAvailable == null || currentTrackAvailable.dataSource == null || com.navdy.service.library.events.audio.MusicDataSource.MUSIC_SOURCE_NONE.equals(currentTrackAvailable.dataSource)) {
            return this.previousDataSource;
        }
        return currentTrackAvailable.dataSource;
    }

    private boolean sendMediaKeyEvent(com.navdy.hud.app.manager.MusicManager.MediaControl control, boolean down) {
        com.navdy.service.library.events.input.MediaRemoteKey key = null;
        switch (control) {
            case NEXT:
                key = com.navdy.service.library.events.input.MediaRemoteKey.MEDIA_REMOTE_KEY_NEXT;
                break;
            case PREVIOUS:
                key = com.navdy.service.library.events.input.MediaRemoteKey.MEDIA_REMOTE_KEY_PREV;
                break;
            case PLAY:
            case PAUSE:
                key = com.navdy.service.library.events.input.MediaRemoteKey.MEDIA_REMOTE_KEY_PLAY;
                break;
        }
        this.bus.post(new com.navdy.hud.app.event.RemoteEvent(new com.navdy.service.library.events.input.MediaRemoteKeyEvent.Builder().key(key).action(down ? com.navdy.service.library.events.input.KeyEvent.KEY_DOWN : com.navdy.service.library.events.input.KeyEvent.KEY_UP).build()));
        return true;
    }

    public void initialize() {
        this.bus.post(new com.navdy.hud.app.event.RemoteEvent(new com.navdy.service.library.events.audio.MusicTrackInfoRequest()));
        sLogger.d("Request for music track info sent to the client.");
    }

    public void addMusicUpdateListener(@android.support.annotation.NonNull com.navdy.hud.app.manager.MusicManager.MusicUpdateListener listener) {
        sLogger.d("addMusicUpdateListener");
        if (!this.musicUpdateListeners.contains(listener)) {
            if (this.musicUpdateListeners.size() == 0) {
                sLogger.d("Enabling album art updates (PhotoUpdate)");
                requestPhotoUpdates(true);
            }
            this.musicUpdateListeners.add(listener);
        } else {
            sLogger.w("Tried to add a listener that's already listening");
        }
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.manager.MusicManager.Anon4(listener), 1);
    }

    public void removeMusicUpdateListener(@android.support.annotation.NonNull com.navdy.hud.app.manager.MusicManager.MusicUpdateListener listener) {
        sLogger.d("removeMusicUpdateListener");
        if (this.musicUpdateListeners.contains(listener)) {
            this.musicUpdateListeners.remove(listener);
            if (this.musicUpdateListeners.size() == 0) {
                sLogger.d("Disabling album art updates");
                requestPhotoUpdates(false);
            }
        } else {
            sLogger.w("Tried to remove a non-existent listener");
        }
        sLogger.d("album art listeners:");
        for (com.navdy.hud.app.manager.MusicManager.MusicUpdateListener l : this.musicUpdateListeners) {
            sLogger.v("- " + l);
        }
    }

    private void requestPhotoUpdates(boolean start) {
        if (!start || !this.clientSupportsArtworkCaching) {
            this.bus.post(new com.navdy.hud.app.event.RemoteEvent(new com.navdy.service.library.events.photo.PhotoUpdatesRequest.Builder().start(java.lang.Boolean.valueOf(start)).photoType(com.navdy.service.library.events.photo.PhotoType.PHOTO_ALBUM_ART).build()));
        } else {
            sLogger.d("client supports caching, no need to request photo updates");
        }
    }

    @com.squareup.otto.Subscribe
    public void onPhotoUpdate(com.navdy.service.library.events.photo.PhotoUpdate photoUpdate) {
        sLogger.d("onPhotoUpdate " + photoUpdate);
        if (photoUpdate == null || photoUpdate.photo == null) {
            this.currentPhoto = null;
        } else {
            this.currentPhoto = photoUpdate.photo;
            java.lang.String currentTrackPhotoId = com.navdy.service.library.util.MusicDataUtils.songIdentifierFromTrackInfo(this.currentTrack);
            if (android.text.TextUtils.equals(currentTrackPhotoId, photoUpdate.identifier)) {
                cacheAlbumArt(this.currentPhoto.toByteArray(), this.currentTrack);
            } else {
                sLogger.w("Received PhotoUpdate with wrong identifier: " + currentTrackPhotoId + " != " + photoUpdate.identifier);
            }
        }
        callAlbumArtCallbacks();
    }

    private void callTrackUpdateCallbacks(boolean willOpenNotification) {
        if (this.updateListeners) {
            sLogger.d("callTrackUpdateCallbacks");
            for (com.navdy.hud.app.manager.MusicManager.MusicUpdateListener listener : this.musicUpdateListeners) {
                listener.onTrackUpdated(this.currentTrack, this.currentControls, willOpenNotification);
            }
            return;
        }
        sLogger.w("callTrackUpdateCallbacks but updateListeners was false");
    }

    /* access modifiers changed from: private */
    public void callAlbumArtCallbacks() {
        if (this.updateListeners) {
            sLogger.d("callAlbumArtCallbacks");
            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.hud.app.manager.MusicManager.Anon5(), 1);
            return;
        }
        sLogger.w("callAlbumArtCallbacks but updateListeners was false");
    }

    public void showMusicNotification() {
        com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().addNotification(this.musicNotification);
    }

    public boolean isPandoraDataSource() {
        return com.navdy.service.library.events.audio.MusicDataSource.MUSIC_SOURCE_PANDORA_API.equals(this.currentTrack.dataSource);
    }

    @com.squareup.otto.Subscribe
    public void onPhoneEvent(com.navdy.service.library.events.callcontrol.PhoneEvent phoneEvent) {
        if (phoneEvent != null && phoneEvent.status != null) {
            sLogger.d("PhoneEvent : new status " + phoneEvent.status);
            if (phoneEvent.status == com.navdy.service.library.events.callcontrol.PhoneStatus.PHONE_DIALING || phoneEvent.status == com.navdy.service.library.events.callcontrol.PhoneStatus.PHONE_RINGING || phoneEvent.status == com.navdy.service.library.events.callcontrol.PhoneStatus.PHONE_OFFHOOK) {
                long currentTime = android.os.SystemClock.elapsedRealtime();
                if (this.wasPossiblyPausedDueToHFP) {
                    return;
                }
                if (this.currentTrack == null || !this.currentTrack.playbackState.equals(com.navdy.service.library.events.audio.MusicPlaybackState.PLAYBACK_PAUSED) || this.lastPausedTime - currentTime >= 10000) {
                    sLogger.d("Recording the phone event as interesting event");
                    this.interestingEventTime = currentTime;
                    return;
                }
                sLogger.d("Current state is paused recently, possibly due to call");
                this.wasPossiblyPausedDueToHFP = true;
                this.pausedDueToHfpDetectedTime = currentTime;
            }
        }
    }

    @com.squareup.otto.Subscribe
    public void onLocalSpeechRequest(com.navdy.hud.app.event.LocalSpeechRequest localSpeechRequest) {
        long currentTime = android.os.SystemClock.elapsedRealtime();
        if (!this.wasPossiblyPausedDueToHFP) {
            sLogger.d("Recording the speech request as an interesting event");
            this.interestingEventTime = currentTime;
        }
    }

    @com.squareup.otto.Subscribe
    public void onDriverProfileChanged(com.navdy.hud.app.event.DriverProfileChanged driverProfileChanged) {
        sLogger.d("onDriverProfileChanged - " + driverProfileChanged);
        if (!com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getDriverProfileManager().getCurrentProfile().isDefaultProfile() && this.musicUpdateListeners.size() > 0) {
            requestPhotoUpdates(true);
        }
    }

    @com.squareup.otto.Subscribe
    public void onConnectionStateChange(com.navdy.service.library.events.connection.ConnectionStateChange event) {
        if (event.state == com.navdy.service.library.events.connection.ConnectionStateChange.ConnectionState.CONNECTION_DISCONNECTED) {
            com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().removeNotification(this.musicNotification.getId());
            this.currentTrack = EMPTY_TRACK;
            this.currentPhoto = null;
            this.currentControls = null;
            this.updateListeners = true;
            callAlbumArtCallbacks();
            callTrackUpdateCallbacks(false);
            this.musicCapabilities = null;
            this.musicCapabilityTypeMap = null;
            this.musicMenuPath = null;
            com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.clearMenuData();
        }
    }

    private void cacheAlbumArt(com.navdy.service.library.events.audio.MusicArtworkResponse musicArtworkResponse) {
        if (musicArtworkResponse.author == null && musicArtworkResponse.album == null && musicArtworkResponse.name == null) {
            sLogger.d("Not a now playing update, returning");
        } else {
            this.artworkCache.putArtwork(musicArtworkResponse.author, musicArtworkResponse.album, musicArtworkResponse.name, musicArtworkResponse.photo.toByteArray());
        }
    }

    private void cacheAlbumArt(byte[] photo, com.navdy.service.library.events.audio.MusicTrackInfo musicTrackInfo) {
        if (musicTrackInfo != null) {
            this.artworkCache.putArtwork(musicTrackInfo.author, musicTrackInfo.album, musicTrackInfo.name, photo);
        }
    }

    @com.squareup.otto.Subscribe
    public void onDeviceInfo(com.navdy.service.library.events.DeviceInfo deviceInfo) {
        sLogger.d("onDeviceInfo - " + deviceInfo);
        this.bus.post(new com.navdy.hud.app.event.RemoteEvent(new com.navdy.service.library.events.audio.MusicCapabilitiesRequest.Builder().build()));
        com.navdy.service.library.events.Capabilities capabilities = deviceInfo.capabilities;
        if (deviceInfo.platform == com.navdy.service.library.events.DeviceInfo.Platform.PLATFORM_iOS || (capabilities != null && ((java.lang.Boolean) com.squareup.wire.Wire.get(capabilities.musicArtworkCache, java.lang.Boolean.valueOf(false))).booleanValue())) {
            this.clientSupportsArtworkCaching = true;
        }
    }

    @com.squareup.otto.Subscribe
    public void onMusicCollectionSourceUpdate(com.navdy.service.library.events.audio.MusicCollectionSourceUpdate musicCollectionSourceUpdate) {
        sLogger.d("onMusicCollectionSourceUpdate " + musicCollectionSourceUpdate);
        if (musicCollectionSourceUpdate.collectionSource != null && musicCollectionSourceUpdate.serial_number != null) {
            checkCache(musicCollectionSourceUpdate.collectionSource.name(), musicCollectionSourceUpdate.serial_number.longValue());
        }
    }

    private void checkCache(java.lang.String musicCollectionSource, long serialNumber) {
        android.content.SharedPreferences sharedPreferences = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getSharedPreferences();
        java.lang.String profileName = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile().getProfileName();
        java.lang.String cacheProfileName = sharedPreferences.getString(PREFERENCE_MUSIC_CACHE_PROFILE, null);
        long cacheSerialNumber = sharedPreferences.getLong(PREFERENCE_MUSIC_CACHE_SERIAL, 0);
        java.lang.String cachedMusicCollectionSource = sharedPreferences.getString(PREFERENCE_MUSIC_CACHE_COLLECTION_SOURCE, null);
        sLogger.d("Current Cache details " + cacheProfileName + ", Source : " + cachedMusicCollectionSource + ", Serial : " + cacheSerialNumber);
        sLogger.d("Current Profile details " + profileName + ", Source : " + musicCollectionSource + ", Serial : " + serialNumber);
        if (!android.text.TextUtils.equals(profileName, cacheProfileName) || cacheSerialNumber != serialNumber || !android.text.TextUtils.equals(musicCollectionSource, cachedMusicCollectionSource)) {
            sLogger.d("Clearing the Music data cache");
            this.musicCollectionResponseMessageCache.clear();
            sharedPreferences.edit().putString(PREFERENCE_MUSIC_CACHE_PROFILE, profileName).putLong(PREFERENCE_MUSIC_CACHE_SERIAL, serialNumber).putString(PREFERENCE_MUSIC_CACHE_COLLECTION_SOURCE, musicCollectionSource).apply();
        }
    }

    public boolean hasMusicCapabilities() {
        return (this.musicCapabilities == null || this.musicCapabilities.capabilities == null || this.musicCapabilities.capabilities.size() == 0) ? false : true;
    }

    public com.navdy.service.library.events.audio.MusicCapabilitiesResponse getMusicCapabilities() {
        return this.musicCapabilities;
    }

    public java.util.List<com.navdy.service.library.events.audio.MusicCollectionType> getCollectionTypesForSource(com.navdy.service.library.events.audio.MusicCollectionSource collectionSource) {
        return (java.util.List) this.musicCapabilityTypeMap.get(collectionSource);
    }

    @com.squareup.otto.Subscribe
    public void onMusicCapabilitiesResponse(com.navdy.service.library.events.audio.MusicCapabilitiesResponse musicCapabilitiesResponse) {
        sLogger.d("onMusicCapabilitiesResponse " + musicCapabilitiesResponse);
        this.musicCapabilities = musicCapabilitiesResponse;
        java.lang.String musicCollectionSource = null;
        this.isMusicLibraryCachingEnabled = false;
        long serialNumber = -1;
        if (hasMusicCapabilities()) {
            this.musicCapabilityTypeMap = new java.util.HashMap();
            for (com.navdy.service.library.events.audio.MusicCapability capability : this.musicCapabilities.capabilities) {
                this.musicCapabilityTypeMap.put(capability.collectionSource, capability.collectionTypes);
                if (!this.isMusicLibraryCachingEnabled && capability.serial_number != null) {
                    this.isMusicLibraryCachingEnabled = true;
                    musicCollectionSource = capability.collectionSource.name();
                    serialNumber = capability.serial_number.longValue();
                    sLogger.d("Enabling caching with Source " + musicCollectionSource + ", Serial :" + serialNumber);
                }
            }
        }
        if (this.isMusicLibraryCachingEnabled) {
            checkCache(musicCollectionSource, serialNumber);
        } else {
            sLogger.d("Cache is not enabled");
        }
    }

    @com.squareup.otto.Subscribe
    public void onMediaKeyEventFromClient(com.navdy.service.library.events.input.MediaRemoteKeyEvent mediaRemoteKeyEvent) {
        this.bus.post(new com.navdy.hud.app.event.RemoteEvent(mediaRemoteKeyEvent));
    }

    public java.lang.String getMusicMenuPath() {
        return this.musicMenuPath;
    }

    public void setMusicMenuPath(java.lang.String musicMenuPath2) {
        sLogger.d("setMusicMenuPath: " + musicMenuPath2);
        this.musicMenuPath = musicMenuPath2;
    }

    public void softPause() {
        if (this.currentTrack.playbackState == com.navdy.service.library.events.audio.MusicPlaybackState.PLAYBACK_PLAYING) {
            sLogger.d("Pausing music");
            this.pausedByUs = true;
            this.acceptingResumes = false;
            postKeyDownUp(com.navdy.service.library.events.input.MediaRemoteKey.MEDIA_REMOTE_KEY_PLAY);
        }
    }

    public void acceptResumes() {
        this.acceptingResumes = true;
        sLogger.d("Now accepting music resume events");
    }

    @com.squareup.otto.Subscribe
    public void onResumeMusicRequest(com.navdy.service.library.events.audio.ResumeMusicRequest resumeMusicRequest) {
        softResume();
    }

    public void softResume() {
        if (this.pausedByUs && this.acceptingResumes) {
            postKeyDownUp(com.navdy.service.library.events.input.MediaRemoteKey.MEDIA_REMOTE_KEY_PLAY);
            this.pausedByUs = false;
            sLogger.d("Unpausing music (resuming)");
        }
    }

    public boolean isMusicLibraryCachingEnabled() {
        return this.isMusicLibraryCachingEnabled;
    }

    private void postKeyDownUp(com.navdy.service.library.events.input.MediaRemoteKey key) {
        this.bus.post(new com.navdy.hud.app.event.RemoteEvent(new com.navdy.service.library.events.input.MediaRemoteKeyEvent(key, com.navdy.service.library.events.input.KeyEvent.KEY_DOWN)));
        this.handler.postDelayed(new com.navdy.hud.app.manager.MusicManager.Anon6(key), 100);
    }
}
