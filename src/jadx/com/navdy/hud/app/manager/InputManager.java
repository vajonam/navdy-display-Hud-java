package com.navdy.hud.app.manager;

public class InputManager {
    /* access modifiers changed from: private */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.manager.InputManager.class);
    private com.squareup.otto.Bus bus;
    private com.navdy.hud.app.manager.InputManager.IInputHandler defaultHandler;
    private com.navdy.hud.app.device.dial.DialManager dialManager;
    private android.os.Handler handler = new android.os.Handler(android.os.Looper.getMainLooper());
    private com.navdy.hud.app.manager.InputManager.IInputHandler inputHandler;
    private boolean isLongPressDetected;
    private long lastInputEventTime = 0;
    private com.navdy.hud.app.device.PowerManager powerManager;
    /* access modifiers changed from: private */
    public com.navdy.hud.app.ui.framework.UIStateManager uiStateManager;

    class Anon1 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.hud.app.manager.InputManager.CustomKeyEvent val$event;

        Anon1(com.navdy.hud.app.manager.InputManager.CustomKeyEvent customKeyEvent) {
            this.val$event = customKeyEvent;
        }

        public void run() {
            if (this.val$event != null) {
                com.navdy.hud.app.manager.InputManager.sLogger.v("inject key event:" + this.val$event);
                if (!com.navdy.hud.app.manager.InputManager.this.invokeHandler(null, this.val$event)) {
                    com.navdy.hud.app.ui.activity.Main main = com.navdy.hud.app.manager.InputManager.this.uiStateManager.getRootScreen();
                    if (main != null) {
                        main.handleKey(this.val$event);
                    }
                }
            }
        }
    }

    public enum CustomKeyEvent {
        LEFT,
        RIGHT,
        SELECT,
        LONG_PRESS,
        POWER_BUTTON_CLICK,
        POWER_BUTTON_DOUBLE_CLICK,
        POWER_BUTTON_LONG_PRESS
    }

    public interface IInputHandler {
        com.navdy.hud.app.manager.InputManager.IInputHandler nextHandler();

        boolean onGesture(com.navdy.service.library.events.input.GestureEvent gestureEvent);

        boolean onKey(com.navdy.hud.app.manager.InputManager.CustomKeyEvent customKeyEvent);
    }

    public InputManager(com.squareup.otto.Bus bus2, com.navdy.hud.app.device.PowerManager powerManager2, com.navdy.hud.app.ui.framework.UIStateManager uiStateManager2) {
        this.powerManager = powerManager2;
        this.bus = bus2;
        this.bus.register(this);
        this.uiStateManager = uiStateManager2;
    }

    public void setFocus(com.navdy.hud.app.manager.InputManager.IInputHandler inputHandler2) {
        this.inputHandler = inputHandler2;
    }

    public com.navdy.hud.app.manager.InputManager.IInputHandler getFocus() {
        return this.inputHandler;
    }

    public long getLastInputEventTime() {
        return this.lastInputEventTime;
    }

    @com.squareup.otto.Subscribe
    public void onGestureEvent(com.navdy.service.library.events.input.GestureEvent event) {
        invokeHandler(event, null);
    }

    public boolean onKeyDown(int keyCode, android.view.KeyEvent event) {
        if (sLogger.isLoggable(3)) {
            sLogger.v("onKeyDown:" + keyCode + " isLongPress:" + event.isLongPress() + " " + event);
        }
        this.bus.post(event);
        if (this.dialManager == null) {
            this.dialManager = com.navdy.hud.app.device.dial.DialManager.getInstance();
        }
        if (this.dialManager.reportInputEvent(event)) {
            com.navdy.hud.app.device.light.HUDLightUtils.dialActionDetected(com.navdy.hud.app.HudApplication.getAppContext(), com.navdy.hud.app.device.light.LightManager.getInstance());
        }
        if (event.isLongPress() || this.isLongPressDetected) {
            return true;
        }
        com.navdy.hud.app.manager.InputManager.CustomKeyEvent customKeyEvent = convertKeyToCustomEvent(keyCode, event);
        if (customKeyEvent == null) {
            return false;
        }
        if (customKeyEvent != com.navdy.hud.app.manager.InputManager.CustomKeyEvent.SELECT) {
            return invokeHandler(null, customKeyEvent);
        }
        event.startTracking();
        return true;
    }

    public boolean onKeyUp(int keyCode, android.view.KeyEvent event) {
        if (sLogger.isLoggable(3)) {
            sLogger.v("onKeyUp:" + keyCode + " isLongPress:" + event.isLongPress() + " " + event);
        }
        this.bus.post(event);
        if (this.isLongPressDetected) {
            this.isLongPressDetected = false;
            return true;
        }
        com.navdy.hud.app.manager.InputManager.CustomKeyEvent customKeyEvent = convertKeyToCustomEvent(keyCode, event);
        if (customKeyEvent == null) {
            return false;
        }
        switch (customKeyEvent) {
            case SELECT:
            case LONG_PRESS:
                return invokeHandler(null, customKeyEvent);
            default:
                return false;
        }
    }

    public boolean onKeyLongPress(int keyCode, android.view.KeyEvent event) {
        if (sLogger.isLoggable(3)) {
            sLogger.v("onKeyLongPress:" + keyCode + " " + event);
        }
        this.isLongPressDetected = true;
        return invokeHandler(null, convertKeyToCustomEvent(keyCode, event));
    }

    private com.navdy.hud.app.manager.InputManager.CustomKeyEvent convertKeyToCustomEvent(int keyCode, android.view.KeyEvent event) {
        switch (keyCode) {
            case 8:
            case 21:
            case 29:
            case 38:
            case 45:
            case 54:
                return com.navdy.hud.app.manager.InputManager.CustomKeyEvent.LEFT;
            case 9:
            case 20:
            case 39:
            case 47:
            case 51:
            case 52:
            case com.navdy.hud.app.bluetooth.obex.HeaderSet.TYPE /*66*/:
                if (event.isLongPress()) {
                    return com.navdy.hud.app.manager.InputManager.CustomKeyEvent.LONG_PRESS;
                }
                return com.navdy.hud.app.manager.InputManager.CustomKeyEvent.SELECT;
            case 10:
            case 22:
            case 31:
            case 32:
            case 33:
            case 40:
                return com.navdy.hud.app.manager.InputManager.CustomKeyEvent.RIGHT;
            default:
                return null;
        }
    }

    public static boolean isCenterKey(int keyCode) {
        return keyCode == 47 || keyCode == 52 || keyCode == 51 || keyCode == 66 || keyCode == 20 || keyCode == 39 || keyCode == 9;
    }

    public static boolean isLeftKey(int keyCode) {
        return keyCode == 29 || keyCode == 54 || keyCode == 45 || keyCode == 21 || keyCode == 38 || keyCode == 8;
    }

    public static boolean isRightKey(int keyCode) {
        return keyCode == 32 || keyCode == 31 || keyCode == 33 || keyCode == 22 || keyCode == 40 || keyCode == 10;
    }

    /* access modifiers changed from: private */
    public boolean invokeHandler(com.navdy.service.library.events.input.GestureEvent gesture, com.navdy.hud.app.manager.InputManager.CustomKeyEvent keyEvent) {
        if (gesture == null && keyEvent == null) {
            return false;
        }
        boolean result = false;
        if (this.inputHandler != null) {
            com.navdy.hud.app.manager.InputManager.IInputHandler handler2 = this.inputHandler;
            while (true) {
                if (handler2 != null) {
                    if (gesture == null) {
                        if (handler2.onKey(keyEvent)) {
                            result = true;
                            break;
                        }
                        handler2 = handler2.nextHandler();
                    } else if (handler2.onGesture(gesture)) {
                        com.navdy.hud.app.device.light.HUDLightUtils.showGestureDetected(com.navdy.hud.app.HudApplication.getAppContext(), com.navdy.hud.app.device.light.LightManager.getInstance());
                        result = true;
                        break;
                    } else {
                        handler2 = handler2.nextHandler();
                    }
                } else {
                    break;
                }
            }
        }
        if (!result && keyEvent != null) {
            switch (keyEvent) {
                case LONG_PRESS:
                    sLogger.v("long press not handled");
                    if (this.defaultHandler == null) {
                        com.navdy.hud.app.ui.activity.Main main = this.uiStateManager.getRootScreen();
                        if (main != null) {
                            this.defaultHandler = main.getInputHandler();
                        }
                    }
                    if (this.defaultHandler != null) {
                        result = this.defaultHandler.onKey(keyEvent);
                        break;
                    }
                    break;
            }
        }
        if (!result) {
            return result;
        }
        this.lastInputEventTime = android.os.SystemClock.elapsedRealtime();
        com.navdy.hud.app.service.ShutdownMonitor.getInstance().recordInputEvent();
        return result;
    }

    public static com.navdy.hud.app.manager.InputManager.IInputHandler nextContainingHandler(android.view.View v) {
        android.view.ViewParent parent = v.getParent();
        while (parent != null && !(parent instanceof com.navdy.hud.app.manager.InputManager.IInputHandler)) {
            parent = parent.getParent();
        }
        if (parent != null) {
            return (com.navdy.hud.app.manager.InputManager.IInputHandler) parent;
        }
        return null;
    }

    public void injectKey(com.navdy.hud.app.manager.InputManager.CustomKeyEvent event) {
        this.powerManager.wakeUp(com.navdy.hud.app.analytics.AnalyticsSupport.WakeupReason.POWERBUTTON);
        this.handler.post(new com.navdy.hud.app.manager.InputManager.Anon1(event));
    }
}
