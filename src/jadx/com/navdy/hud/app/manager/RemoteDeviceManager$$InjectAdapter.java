package com.navdy.hud.app.manager;

public final class RemoteDeviceManager$$InjectAdapter extends dagger.internal.Binding<com.navdy.hud.app.manager.RemoteDeviceManager> implements dagger.MembersInjector<com.navdy.hud.app.manager.RemoteDeviceManager> {
    private dagger.internal.Binding<com.squareup.otto.Bus> bus;
    private dagger.internal.Binding<com.navdy.hud.app.framework.calendar.CalendarManager> calendarManager;
    private dagger.internal.Binding<com.navdy.hud.app.framework.phonecall.CallManager> callManager;
    private dagger.internal.Binding<com.navdy.hud.app.service.ConnectionHandler> connectionHandler;
    private dagger.internal.Binding<com.navdy.hud.app.debug.DriveRecorder> driveRecorder;
    private dagger.internal.Binding<com.navdy.hud.app.util.FeatureUtil> featureUtil;
    private dagger.internal.Binding<com.navdy.hud.app.gesture.GestureServiceConnector> gestureServiceConnector;
    private dagger.internal.Binding<com.navdy.service.library.network.http.IHttpManager> httpManager;
    private dagger.internal.Binding<com.navdy.hud.app.manager.InputManager> inputManager;
    private dagger.internal.Binding<com.navdy.hud.app.manager.MusicManager> musicManager;
    private dagger.internal.Binding<android.content.SharedPreferences> preferences;
    private dagger.internal.Binding<com.navdy.hud.app.analytics.TelemetryDataManager> telemetryDataManager;
    private dagger.internal.Binding<com.navdy.hud.app.common.TimeHelper> timeHelper;
    private dagger.internal.Binding<com.navdy.hud.app.framework.trips.TripManager> tripManager;
    private dagger.internal.Binding<com.navdy.hud.app.ui.framework.UIStateManager> uiStateManager;
    private dagger.internal.Binding<com.navdy.hud.app.framework.voice.VoiceSearchHandler> voiceSearchHandler;

    public RemoteDeviceManager$$InjectAdapter() {
        super(null, "members/com.navdy.hud.app.manager.RemoteDeviceManager", false, com.navdy.hud.app.manager.RemoteDeviceManager.class);
    }

    public void attach(dagger.internal.Linker linker) {
        this.bus = linker.requestBinding("com.squareup.otto.Bus", com.navdy.hud.app.manager.RemoteDeviceManager.class, getClass().getClassLoader());
        this.connectionHandler = linker.requestBinding("com.navdy.hud.app.service.ConnectionHandler", com.navdy.hud.app.manager.RemoteDeviceManager.class, getClass().getClassLoader());
        this.callManager = linker.requestBinding("com.navdy.hud.app.framework.phonecall.CallManager", com.navdy.hud.app.manager.RemoteDeviceManager.class, getClass().getClassLoader());
        this.inputManager = linker.requestBinding("com.navdy.hud.app.manager.InputManager", com.navdy.hud.app.manager.RemoteDeviceManager.class, getClass().getClassLoader());
        this.preferences = linker.requestBinding("android.content.SharedPreferences", com.navdy.hud.app.manager.RemoteDeviceManager.class, getClass().getClassLoader());
        this.timeHelper = linker.requestBinding("com.navdy.hud.app.common.TimeHelper", com.navdy.hud.app.manager.RemoteDeviceManager.class, getClass().getClassLoader());
        this.calendarManager = linker.requestBinding("com.navdy.hud.app.framework.calendar.CalendarManager", com.navdy.hud.app.manager.RemoteDeviceManager.class, getClass().getClassLoader());
        this.gestureServiceConnector = linker.requestBinding("com.navdy.hud.app.gesture.GestureServiceConnector", com.navdy.hud.app.manager.RemoteDeviceManager.class, getClass().getClassLoader());
        this.uiStateManager = linker.requestBinding("com.navdy.hud.app.ui.framework.UIStateManager", com.navdy.hud.app.manager.RemoteDeviceManager.class, getClass().getClassLoader());
        this.tripManager = linker.requestBinding("com.navdy.hud.app.framework.trips.TripManager", com.navdy.hud.app.manager.RemoteDeviceManager.class, getClass().getClassLoader());
        this.driveRecorder = linker.requestBinding("com.navdy.hud.app.debug.DriveRecorder", com.navdy.hud.app.manager.RemoteDeviceManager.class, getClass().getClassLoader());
        this.musicManager = linker.requestBinding("com.navdy.hud.app.manager.MusicManager", com.navdy.hud.app.manager.RemoteDeviceManager.class, getClass().getClassLoader());
        this.voiceSearchHandler = linker.requestBinding("com.navdy.hud.app.framework.voice.VoiceSearchHandler", com.navdy.hud.app.manager.RemoteDeviceManager.class, getClass().getClassLoader());
        this.featureUtil = linker.requestBinding("com.navdy.hud.app.util.FeatureUtil", com.navdy.hud.app.manager.RemoteDeviceManager.class, getClass().getClassLoader());
        this.httpManager = linker.requestBinding("com.navdy.service.library.network.http.IHttpManager", com.navdy.hud.app.manager.RemoteDeviceManager.class, getClass().getClassLoader());
        this.telemetryDataManager = linker.requestBinding("com.navdy.hud.app.analytics.TelemetryDataManager", com.navdy.hud.app.manager.RemoteDeviceManager.class, getClass().getClassLoader());
    }

    public void getDependencies(java.util.Set<dagger.internal.Binding<?>> set, java.util.Set<dagger.internal.Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.bus);
        injectMembersBindings.add(this.connectionHandler);
        injectMembersBindings.add(this.callManager);
        injectMembersBindings.add(this.inputManager);
        injectMembersBindings.add(this.preferences);
        injectMembersBindings.add(this.timeHelper);
        injectMembersBindings.add(this.calendarManager);
        injectMembersBindings.add(this.gestureServiceConnector);
        injectMembersBindings.add(this.uiStateManager);
        injectMembersBindings.add(this.tripManager);
        injectMembersBindings.add(this.driveRecorder);
        injectMembersBindings.add(this.musicManager);
        injectMembersBindings.add(this.voiceSearchHandler);
        injectMembersBindings.add(this.featureUtil);
        injectMembersBindings.add(this.httpManager);
        injectMembersBindings.add(this.telemetryDataManager);
    }

    public void injectMembers(com.navdy.hud.app.manager.RemoteDeviceManager object) {
        object.bus = (com.squareup.otto.Bus) this.bus.get();
        object.connectionHandler = (com.navdy.hud.app.service.ConnectionHandler) this.connectionHandler.get();
        object.callManager = (com.navdy.hud.app.framework.phonecall.CallManager) this.callManager.get();
        object.inputManager = (com.navdy.hud.app.manager.InputManager) this.inputManager.get();
        object.preferences = (android.content.SharedPreferences) this.preferences.get();
        object.timeHelper = (com.navdy.hud.app.common.TimeHelper) this.timeHelper.get();
        object.calendarManager = (com.navdy.hud.app.framework.calendar.CalendarManager) this.calendarManager.get();
        object.gestureServiceConnector = (com.navdy.hud.app.gesture.GestureServiceConnector) this.gestureServiceConnector.get();
        object.uiStateManager = (com.navdy.hud.app.ui.framework.UIStateManager) this.uiStateManager.get();
        object.tripManager = (com.navdy.hud.app.framework.trips.TripManager) this.tripManager.get();
        object.driveRecorder = (com.navdy.hud.app.debug.DriveRecorder) this.driveRecorder.get();
        object.musicManager = (com.navdy.hud.app.manager.MusicManager) this.musicManager.get();
        object.voiceSearchHandler = (com.navdy.hud.app.framework.voice.VoiceSearchHandler) this.voiceSearchHandler.get();
        object.featureUtil = (com.navdy.hud.app.util.FeatureUtil) this.featureUtil.get();
        object.httpManager = (com.navdy.service.library.network.http.IHttpManager) this.httpManager.get();
        object.telemetryDataManager = (com.navdy.hud.app.analytics.TelemetryDataManager) this.telemetryDataManager.get();
    }
}
