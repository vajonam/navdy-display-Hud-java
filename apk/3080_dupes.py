# java -jar apktool_2.4.0.jar d ../apk/Hud-3080.apk
# python3 ./3080_dupes.py
# java -jar apktool_2.4.0.jar b Hud-3080
# apksigner.bat sign --ks ..\navdy_alelec.jks --out ..\apk\Hud-3080-Anon.apk Hud-3080\dist\Hud-3080.apk

import sys
import os, re
import shutil
import fileinput
from pathlib import Path

for root, dirs, files in os.walk("./Hud-3080"):
   for name in files:

        fpath = os.path.join(root, name)
        fname = origname = Path(fpath)

        if 'com/navdy/' not in fpath:
            # print('-', f)
            continue

        if not fpath.endswith('.smali'):
            # print('-', f)
            continue
       
        print(fname.name)

        if re.search(r'\$\d', fname.name):
            target = fname.with_name(fname.name.replace('$', '$Anon'))
            
            print(fname, "->", target)
            fname.rename(target)
            
        else:
            target = fname

        text = target.read_text()

        def add_anon(match):
            return "$Anon" + match.group(1)

        text = re.sub(r'\$(\d+)', add_anon, text)

        target.write_text(text)
